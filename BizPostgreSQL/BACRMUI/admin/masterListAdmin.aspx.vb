'================================================================================
' 
' Module Name:      masterListAdmin
' Description:      This interface is the admin screen for Master List (ListMaster and Territory).
' Created By:       Debasish Nag
' Creation Date:    13/03/2003
'================================================================================
Imports System.Data.SqlClient
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin
    Public Class masterListAdmin
        Inherits BACRMPage
        Protected WithEvents ddlMasterList As System.Web.UI.WebControls.DropDownList
        Protected WithEvents tblMasterListDetails As System.Web.UI.HtmlControls.HtmlTable
        Protected WithEvents imgBtnAddMasterItem As System.Web.UI.WebControls.ImageButton
        Protected WithEvents hidMasterItemId As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hidMasterItemType As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents hidMasterItemName As System.Web.UI.HtmlControls.HtmlInputHidden
        Protected WithEvents txtNewMasterItemName As System.Web.UI.WebControls.TextBox
        Protected WithEvents phMasterListDetails As System.Web.UI.WebControls.PlaceHolder
        Protected WithEvents reqValidatorNewMasterItemName As System.Web.UI.WebControls.RequiredFieldValidator
        Protected WithEvents divMasterListDetails As System.Web.UI.HtmlControls.HtmlGenericControl
        Protected WithEvents lblError As System.Web.UI.WebControls.Label
        Protected WithEvents Dropdownlist1 As System.Web.UI.WebControls.DropDownList
        Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
        Protected WithEvents btnAddNew As System.Web.UI.WebControls.Button
        Protected WithEvents btnAddTerr As System.Web.UI.WebControls.Button
        Protected WithEvents Table1 As System.Web.UI.WebControls.Table
        Protected WithEvents Bdy As System.Web.UI.HtmlControls.HtmlControl
        Protected WithEvents btnState As System.Web.UI.WebControls.Button
        Protected WithEvents btnRelPro As System.Web.UI.WebControls.Button
        Dim m_aryRightsForPage() As Integer
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            '================================================================================
            ' Purpose: Actions to be done when the page loads
            '
            ' History
            ' Ver   Date        Ref     Author              Reason
            ' 1     13/03/2003          Debaish         Created
            ' 2     01/04/2003      Anup Jishnu         The SP was allowing users to delet list 
            '                                       details even when it was referenced within 
            '                                       some fields in the data of companies. This 
            '                                       used to throw errors later. It was fixed by 
            '                                       not allowin the users to delete referenced 
            '                                       records and display Javascript Alert to inform 
            '                                       the user about teh same.
            '
            ' Non Compliance (any deviation from standards)
            ' No deviations from the standards.
            '================================================================================
            lblError.Text = ""
            m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights("masterListAdmin.aspx", Session("userID"), 13, 1)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            End If
            If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnAddNew.Visible = False
            End If
            btnCancel.Attributes.Add("onclick", "return GoBack();")
            If Page.IsPostBack Then
                Session("Help") = "MasterList"
                lblError.Text = ""  'cleaar the error message
                If Trim(Request.QueryString("strAction")) = "Save" Then 'Save button clicked to save the master item name
                    Dim intReturn As Integer
                    Dim intItemId As Integer = Left(ddlMasterList.SelectedItem.Value, InStr(ddlMasterList.SelectedItem.Value, "_") - 1) 'Storing the item id
                    If (Left(Request.Form("ddlMasterList"), InStr(Request.Form("ddlMasterList"), "_") - 1) = "2" And IsNumeric(hidMasterItemName.Value) = True) Or Left(Request.Form("ddlMasterList"), InStr(Request.Form("ddlMasterList"), "_") - 1) <> "2" Then
                        Dim cnnListItemsDelete As New SqlConnection(clsConnection.fn_GetConnectionString) 'Creating the co\nneciton object
                        cnnListItemsDelete.Open() 'Opening  connection
                        Dim strItemName As String = hidMasterItemName.Value 'to contain the item name
                        'connection string to contain the sql query to update the master item
                        Dim StrQuery As String = "EXEC  usp_ManageMasterListDetails"
                        StrQuery = StrQuery & " @numListItemID  = " & Request.QueryString("numItemID")
                        StrQuery = StrQuery & ", @vcItemName = '" & AddQuotes(strItemName) & "' "
                        StrQuery = StrQuery & ", @vcItemType = '" & Request.QueryString("strItemType") & "' "
                        StrQuery = StrQuery & ", @actionItemType = 'Save' "
                        StrQuery = StrQuery & ", @numDomainId = " & Session("DomainID")
                        StrQuery = StrQuery & ", @numListID = " & intItemId

                        Dim cmdListItemsDelete As New SqlCommand(StrQuery, cnnListItemsDelete) 'Opening Command
                        intReturn = cmdListItemsDelete.ExecuteScalar  'Execute the query
                        cnnListItemsDelete.Close()  'Closing the connection object
                        cmdListItemsDelete.Dispose()    'Dispose the connection object
                        cnnListItemsDelete = Nothing    'Remove reference of the connection
                        cmdListItemsDelete = Nothing   'Remove reference of the command
                        reqValidatorNewMasterItemName.Enabled = False
                        If intReturn > 0 Then
                            lblError.Text = "This values already exists"

                        End If
                    Else
                        Response.Write("<script language=javascript>")
                        Response.Write("alert('Please Enter Only Numeric Values for Company Rating.');")
                        Response.Write("</script>")
                    End If
                    fn_FillListItems()  'Calling function to display master item details
                    'Check if it is the Delete mode
                ElseIf Trim(Request.QueryString("strAction")) = "Delete" Then
                    'Declare variable to hold the return value from the Stored Proc.
                    Dim intRetValue As Integer
                    Dim cnnListItemsDelete As New SqlConnection(clsConnection.fn_GetConnectionString) 'Creating the co\nneciton object
                    cnnListItemsDelete.Open() 'Opening  connection
                    'connection string to contain the sql query to delete the master item
                    Dim StrQuery As String = "EXEC  usp_ManageMasterListDetails"
                    StrQuery = StrQuery & " @numListItemID = " & Request.QueryString("numItemID")
                    StrQuery = StrQuery & ", @vcItemType = '" & Request.QueryString("strItemType") & "' "
                    StrQuery = StrQuery & ", @actionItemType = 'Delete' "
                    StrQuery = StrQuery & ", @numDomainId = " & Session("DomainID")

                    Dim cmdListItemsDelete As New SqlCommand(StrQuery, cnnListItemsDelete) 'Opening Command
                    intRetValue = cmdListItemsDelete.ExecuteScalar()    'Execute the query
                    cnnListItemsDelete.Close()  'Closing the connection object
                    cmdListItemsDelete.Dispose()    'Dispose the Command object
                    cnnListItemsDelete = Nothing    'Remove reference of the connection
                    cmdListItemsDelete = Nothing    'Remove reference of the command
                    'reqValidatorNewMasterItemName.EnableClientScript = False
                    reqValidatorNewMasterItemName.Enabled = False
                    If intRetValue = 0 Then
                        'Value Deleted from list details successfully.
                        fn_FillListItems()  'Calling function to display master item details
                    Else
                        'The value was referenced within existing data elsewhere for some company and hence cannot be deleted.
                        'Display a JavaScript Alert detailing teh same and display the existing values back.
                        Response.Write("<script language=javascript>")
                        Response.Write("alert('The specified Item Detail is already referenced within some Companies and hence cannot be deleted.');")
                        Response.Write("</script>")
                        fn_FillListItems()  'Calling function to display master item details
                    End If
                    'reqValidatorNewMasterItemName.EnableClientScript = True

                End If
                'reqValidatorNewMasterItemName.EnableClientScript = True
            End If
            If Not Page.IsPostBack Then
                ddlMasterList.Items.RemoveAt(0) 'For dumping the Items after removing the first item (---Select One---)
                Dim objCommon As New CCommon
                objCommon.LoadListMaster(ddlMasterList, Session("DomainID"))
                hidMasterItemId.Value = ddlMasterList.Items(0).Value
                fn_FillListItems()  'Calling function to display master item details
            End If
            btnAddTerr.Attributes.Add("onclick", "return NewTerritory();")
            btnState.Attributes.Add("onclick", "return State();")
            btnRelPro.Attributes.Add("onclick", "return Profile();")
            btnRelFol.Attributes.Add("onclick", "return Follow();")
        End Sub

        Private Sub ddlMasterList_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlMasterList.SelectedIndexChanged
            '================================================================================
            ' Purpose: Actions to be done when the Master List Drop down is selected
            '
            ' History
            ' Ver   Date        Ref     Author              Reason
            ' 1     13/03/2003          Debaish         Created
            '
            ' Non Compliance (any deviation from standards)
            ' No deviations from the standards.
            '================================================================================
            hidMasterItemId.Value = ddlMasterList.SelectedItem.Value
            txtNewMasterItemName.Text = ""
            tblMasterListDetails.Controls.Clear()
            fn_FillListItems()  'Calling function to display master item details
        End Sub

        Private Function fn_FillListItems()
            '================================================================================
            ' Purpose: Populate the Master Details
            '
            ' History
            ' Ver   Date        Ref     Author              Reason
            ' 1     13/03/2003          Debaish         Created
            '
            ' Non Compliance (any deviation from standards)
            ' No deviations from the standards.
            '================================================================================
            Dim intItemId As Integer = ddlMasterList.SelectedItem.Value

            hidMasterItemType.Value = 1
            Dim cnnListItemsDetails As New SqlConnection(clsConnection.fn_GetConnectionString) 'Creating the conneciton object
            cnnListItemsDetails.Open() 'Opening  connection
            ' The query string
            Dim StrQuery As String = "EXEC  usp_GetMasterListDetails"
            StrQuery = StrQuery & " @numListID = " & intItemId
            StrQuery = StrQuery & ", @vcItemType = '" & hidMasterItemType.Value & "' "
            StrQuery = StrQuery & ", @numDomainId = " & Session("DomainID")

            Dim dtblListItemDetails As New DataTable      'Declare a data table to hold the data.
            Dim dadrListItemDetails As New SqlDataAdapter(StrQuery, cnnListItemsDetails) 'data adapter
            dadrListItemDetails.Fill(dtblListItemDetails)
            Dim drowListItemDetails As DataRow 'DataRow
            Dim objCell1 As HtmlTableCell
            Dim objCell3 As HtmlTableCell
            Dim objRow As HtmlTableRow
            Dim txtBoxControl As TextBox
            Dim btnSave As Button
            Dim btnDelete As Button
            Dim lblMessage As Label
            Dim intRowCounter, intAlternateRowCounter As Integer
            Dim intRowCountForHgt As Integer
            intAlternateRowCounter = 1
            For intRowCounter = 0 To dtblListItemDetails.Rows.Count() - 1
                drowListItemDetails = dtblListItemDetails.Rows(intRowCounter)   'The row object to store the data table rows
                'Item Details TextBox
                objCell1 = New HtmlTableCell
                txtBoxControl = New TextBox
                txtBoxControl.ID = "txtItemDetails_" & intRowCounter
                txtBoxControl.Text = drowListItemDetails("vcItemName")
                txtBoxControl.CssClass = "signup"
                If drowListItemDetails("flagConst") = 1 Then
                    txtBoxControl.Enabled = False
                End If
                'Save Button
                btnSave = New Button
                btnSave.Text = "Save"
                btnSave.Width = Unit.Pixel(50)
                btnSave.CssClass = "button"

                If drowListItemDetails("flagConst") = 1 Or m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                    btnSave.Attributes.Add("onclick", "return alertMesg('S');")
                Else
                    btnSave.Attributes.Add("onclick", "return fnSave(" & drowListItemDetails("numItemID") & ",'" & drowListItemDetails("vcItemType") & "','" & intRowCounter & "');")
                End If

                'Delete Button
                btnDelete = New Button
                lblMessage = New Label
                If intItemId = 5 And drowListItemDetails("numItemID") = 46 Then
                    btnDelete.Visible = False
                    lblMessage.Text = "<font class=normal1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(Prospect default relationship)</font>"
                ElseIf intItemId = 27 And drowListItemDetails("numItemID") = 287 Then
                    btnDelete.Visible = False
                    lblMessage.Text = "<font class=normal1>&nbsp;&nbsp;&nbsp;&nbsp;(Default BizDoc used for calculating Balance Amount)</font>"
                ElseIf intItemId = 27 And drowListItemDetails("numItemID") = 644 Then
                    btnDelete.Visible = False
                    lblMessage.Text = "<font class=normal1>&nbsp;&nbsp;&nbsp;&nbsp;(Default BizDoc created when order is created through portal)</font>"
                Else
                    btnDelete.Text = "Delete"
                    btnDelete.Width = Unit.Pixel(50)
                    btnDelete.CssClass = "button"
                    If drowListItemDetails("flagConst") = 1 Or m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                        btnDelete.Attributes.Add("onclick", "return alertMesg('D');")
                    Else
                        btnDelete.Attributes.Add("onclick", "return fnDelete(" & drowListItemDetails("numItemID") & ",'" & drowListItemDetails("vcItemType") & "');")
                    End If
                End If
                If drowListItemDetails("flagConst") = 1 Then
                    btnSave.Enabled = False
                    btnDelete.Enabled = False
                End If
                objCell1.Controls.Add(txtBoxControl)
                objCell1.Controls.Add(New LiteralControl("&nbsp;"))
                objCell1.Controls.Add(btnSave)
                objCell1.Controls.Add(New LiteralControl("&nbsp;"))
                objCell1.Controls.Add(btnDelete)
                objCell1.Controls.Add(lblMessage)

                intRowCounter += 1
                If intRowCounter <= dtblListItemDetails.Rows.Count() - 1 Then
                    drowListItemDetails = dtblListItemDetails.Rows(intRowCounter)   'The row object to store the data table rows
                    'Item Details TextBox
                    objCell3 = New HtmlTableCell
                    txtBoxControl = New TextBox
                    txtBoxControl.ID = "txtItemDetails_" & intRowCounter
                    txtBoxControl.Text = drowListItemDetails("vcItemName")
                    txtBoxControl.CssClass = "signup"
                    'Save Button
                    btnSave = New Button
                    btnSave.Text = "Save"
                    btnSave.Width = Unit.Pixel(50)
                    btnSave.CssClass = "button"
                    If drowListItemDetails("flagConst") = 1 Or m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                        btnSave.Attributes.Add("onclick", "return alertMesg('S');")
                    Else
                        btnSave.Attributes.Add("onclick", "return fnSave(" & drowListItemDetails("numItemID") & ",'" & drowListItemDetails("vcItemType") & "','" & intRowCounter & "');")
                    End If
                    'Delete Button
                    btnDelete = New Button
                    btnDelete.Text = "Delete"
                    btnDelete.Width = Unit.Pixel(50)
                    btnDelete.CssClass = "button"
                    If drowListItemDetails("flagConst") = 1 Or m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                        btnDelete.Attributes.Add("onclick", "return alertMesg('D');")
                    Else
                        btnDelete.Attributes.Add("onclick", "return fnDelete(" & drowListItemDetails("numItemID") & ",'" & drowListItemDetails("vcItemType") & "');")
                    End If
                    objCell3.Controls.Add(txtBoxControl)
                    objCell3.Controls.Add(New LiteralControl("&nbsp;"))
                    objCell3.Controls.Add(btnSave)
                    objCell3.Controls.Add(New LiteralControl("&nbsp;"))
                    objCell3.Controls.Add(btnDelete)
                Else
                    'Blank Columns
                    objCell3 = New HtmlTableCell
                    objCell3.Controls.Add(New LiteralControl("&nbsp;"))
                    objCell3 = New HtmlTableCell
                    objCell3.Controls.Add(New LiteralControl("&nbsp;"))
                    objCell3 = New HtmlTableCell
                    objCell3.Controls.Add(New LiteralControl("&nbsp;"))
                End If

                objRow = New HtmlTableRow   'Create a new row
                If intAlternateRowCounter Mod 2 Then
                    objRow.BgColor = "#C6D3E7"
                Else
                    objRow.BgColor = "#FFFFFF"
                End If
                objRow.Cells.Add(objCell1) 'Add table cells to the tablerow
                objRow.Cells.Add(objCell3) 'Add table cells to the tablerow

                tblMasterListDetails.Rows.Add(objRow)   'Add tablerow to the htmltable
                intAlternateRowCounter = intAlternateRowCounter + 1
                intRowCountForHgt = intRowCountForHgt + 1
            Next




            If intRowCountForHgt <= 2 Then
                divMasterListDetails.Style.Item("height") = "50px"
            ElseIf intRowCountForHgt = 3 Then
                divMasterListDetails.Style.Item("height") = "75px"
            ElseIf intRowCountForHgt = 4 Then
                divMasterListDetails.Style.Item("height") = "100px"
            ElseIf intRowCountForHgt = 5 Then
                divMasterListDetails.Style.Item("height") = "120px"
            ElseIf intRowCountForHgt = 6 Then
                divMasterListDetails.Style.Item("height") = "145px"
            ElseIf intRowCountForHgt = 7 Then
                divMasterListDetails.Style.Item("height") = "170px"
            ElseIf intRowCountForHgt = 8 Then
                divMasterListDetails.Style.Item("height") = "192px"
            ElseIf intRowCountForHgt = 9 Then
                divMasterListDetails.Style.Item("height") = "215px"
            ElseIf intRowCountForHgt = 10 Then
                divMasterListDetails.Style.Item("height") = "240px"
            ElseIf intRowCountForHgt = 11 Then
                divMasterListDetails.Style.Item("height") = "265px"
            ElseIf intRowCountForHgt = 12 Then
                divMasterListDetails.Style.Item("height") = "290px"
            ElseIf intRowCountForHgt = 13 Then
                divMasterListDetails.Style.Item("height") = "315px"
            ElseIf intRowCountForHgt = 14 Then
                divMasterListDetails.Style.Item("height") = "337px"
            ElseIf intRowCountForHgt = 15 Then
                divMasterListDetails.Style.Item("height") = "362px"
            ElseIf intRowCountForHgt = 16 Then
                divMasterListDetails.Style.Item("height") = "385px"
            ElseIf intRowCountForHgt = 17 Then
                divMasterListDetails.Style.Item("height") = "410px"
            ElseIf intRowCountForHgt >= 18 Then
                divMasterListDetails.Style.Item("height") = "435px"
            End If


            dadrListItemDetails.Dispose()   'Closing the datareader
            cnnListItemsDetails.Close()    'closing connection
            dadrListItemDetails = Nothing  'Removing references of datareader object
            cnnListItemsDetails = Nothing  'removing references of connection object
        End Function
    

        Function AddQuotes(ByVal strValue)
            '================================================================================
            ' Purpose: This is called to append quotes around the string at all times.
            '
            ' History
            ' Ver   Date        Ref     Author                  Reason
            ' 1     20/02/2003          Debasish          Created
            '
            ' Non Compliance (any deviation from standards)
            '   No deviations from the standards.
            '================================================================================
            ' Given a string, wrap it in quotes, doubling
            ' any quotes within the string.
            Const QUOTE = """"
            AddQuotes = _
               QUOTE _
               & Replace(strValue, QUOTE, QUOTE & QUOTE) _
               & QUOTE

            AddQuotes = Replace(strValue, "'", "''")
        End Function


        Private Sub btnAddNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddNew.Click
            '================================================================================
            ' Purpose: This is called to add a new item
            '
            ' History
            ' Ver   Date        Ref     Author                  Reason
            ' 1     20/02/2003          Debasish          Created
            '
            ' Non Compliance (any deviation from standards)
            '   No deviations from the standards.
            '================================================================================
            If txtNewMasterItemName.Text = "" Then
                '    fn_FillListItems()
                lblError.Text = "Please Enter Value"
                Exit Sub
            End If
            'If ddlMasterList.SelectedItem.Text = "Contact Location" Then
            '    Dim intRowCount As Integer
            '    Dim intCnt As Integer
            '    intCnt = 0
            '    'Dim txtBoxControl = New TextBox()
            '    For intRowCount = 0 To tblMasterListDetails.Rows.Count - 1
            '        'If tblMasterListDetails.Rows(intRowCount).Cells(0).Controls(0).GetType.ToString = "System.Web.UI.WebControls.TextBox" Then
            '        If tblMasterListDetails.Rows(intRowCount).GetType.ToString = "System.Web.UI.WebControls.TextBox" Then
            '            'For intRowCount = 0 To tblMasterListDetails.Controls.GetType
            '            'If txtBoxControl.count > 5 Then

            '            'End If
            '            intCnt = intCnt + 1
            '        End If
            '    Next
            'End If
            'reqValidatorNewMasterItemName.EnableClientScript = True
            'reqValidatorNewMasterItemName.Enabled = True
            Dim intFlag1 As Integer
            intFlag1 = 0
            Dim cnnListAddItems As New SqlConnection(clsConnection.fn_GetConnectionString) 'Creating the conneciton object
            cnnListAddItems.Open() 'Opening  connection
            Dim intItemId As Integer = Left(ddlMasterList.SelectedItem.Value, InStr(ddlMasterList.SelectedItem.Value, "_") - 1) 'Storing the item id
            'Added for contact location validation
            If ddlMasterList.SelectedItem.Text = "Contact Location" Then
                Dim strSql1 As String
                Dim intCnt As Integer
                Dim daContact As SqlDataAdapter
                Dim dsContact As New DataSet

                strSql1 = "Select * from ListDetails where numListId = " & intItemId
                Dim cmdContact As New SqlCommand(strSql1, cnnListAddItems)
                daContact = New SqlDataAdapter(cmdContact)
                daContact.Fill(dsContact, "AnyTable")
                intCnt = dsContact.Tables("AnyTable").Rows.Count
                If intCnt >= 5 Then
                    intFlag1 = 1
                End If
            End If
            If intFlag1 = 0 Then
                If (intItemId = 2 And IsNumeric(txtNewMasterItemName.Text) = True) Or intItemId <> 2 Then
                    ' The query string containing the query to add the Master item
                    Dim StrQuery As String = "EXEC  usp_AddMasterListDetails"
                    StrQuery = StrQuery & " @numListID = " & intItemId
                    StrQuery = StrQuery & ", @vcItemName = '" & AddQuotes(txtNewMasterItemName.Text) & "' "
                    StrQuery = StrQuery & ", @vcItemType = '" & hidMasterItemType.Value & "' "
                    StrQuery = StrQuery & ", @numDomainId = " & Session("DomainID")
                    StrQuery = StrQuery & ", @numCreatedBy = " & Session("UserID")
                    StrQuery = StrQuery & ", @bintCreatedDate = '" & Now() & "'"
                    Dim cmdListItemsAdd As New SqlCommand(StrQuery, cnnListAddItems) 'Opening Command
                    Dim intCheck As Integer = cmdListItemsAdd.ExecuteScalar()  'Execute the query
                    cmdListItemsAdd.Dispose()   'Dispose the command object
                    cnnListAddItems.Close() 'Close the connection object
                    If (intCheck = 0) Then
                        lblError.Text = "value already exist"
                    End If
                    txtNewMasterItemName.Text = ""  'Setting the new item field to blank
                    tblMasterListDetails.Controls.Clear()
                    fn_FillListItems()
                Else
                    Response.Write("<script language=javascript>")
                    Response.Write("alert('Please Enter Only Numeric Values for Company Rating.');")
                    Response.Write("</script>")
                    txtNewMasterItemName.Text = ""
                    tblMasterListDetails.Controls.Clear()
                    fn_FillListItems()
                End If
            Else
                Response.Write("<script language=javascript>")
                Response.Write("alert('Contact location Address cannot be more than 5.');")
                Response.Write("</script>")
                txtNewMasterItemName.Text = ""
                tblMasterListDetails.Controls.Clear()
                fn_FillListItems()
            End If
            'fn_FillListItems()  'Calling function to display master item details
        End Sub
    End Class
End Namespace