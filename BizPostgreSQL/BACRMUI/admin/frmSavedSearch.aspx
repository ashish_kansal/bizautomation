﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSavedSearch.aspx.vb"
    Inherits=".frmSavedSearch" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" Text="Save" CssClass="button" runat="server"></asp:Button>&nbsp;
            <asp:Button ID="btnClose" Text="Close" CssClass="button" runat="server" OnClientClick="return Close();">
            </asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Enter Search Name
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="600px">
        <tr>
            <td class="normal4" align="center" colspan="2">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Name
            </td>
            <td>
                <asp:TextBox runat="server" ID="txtSearchName" CssClass="signup" />
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
