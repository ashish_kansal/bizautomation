﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmFormValidation.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmFormValidation" MasterPageFile="~/common/GridMasterRegular.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Form Fields</title>
    <script language="javascript">
        function OpenValidation(a, b, c) {
            window.open("../admin/frmFormFieldValidationSet.aspx?FormId=" + a + "&fldid=" + b + "&FType=" + c, '', 'width=400,height=280,status=no,titlebar=no,resizable=0,scrollbar=yes,top=110,left=150')
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <label>Form Type:</label>

                    <asp:DropDownList runat="server" AutoPostBack="true" ID="ddlFilter" CssClass="form-control">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="pull-right">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Form Fields&nbsp;<a href="#" onclick="return OpenHelpPopUp('admin/frmFormValidation.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvFormFields" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered"
                    AllowSorting="True" Width="100%" DataKeyNames="numFormFieldId"
                    ItemStyle-HorizontalAlign="Center" UseAccessibleHeader="true">

                    <Columns>
                        <asp:BoundField DataField="vcFormFieldName" Visible="true" HeaderText="Field Name"
                            ItemStyle-Width="20%" />
                        <asp:TemplateField HeaderText="Set Field Name" ItemStyle-Width="25%">
                            <ItemTemplate>
                                <asp:TextBox ID="txtvcNewFormFieldName" runat="server" Text='<%# Eval("vcNewFormFieldName") %>'
                                    CssClass="form-control" AutoComplete="OFF" MaxLength="50"></asp:TextBox>
                                <asp:HiddenField ID="hdvcNewFormFieldName" runat="server" Value='<%# Eval("vcNewFormFieldName") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="vcAssociatedControlType" Visible="true" HeaderText="Field Type" />
                        <asp:BoundField Visible="true" HeaderText="Validation" />
                        <asp:TemplateField HeaderText="ToolTip">
                            <ItemTemplate>
                                <asp:TextBox ID="txtvcToolTip" runat="server" Text='<%# Eval("vcToolTip") %>' CssClass="form-control"
                                    AutoComplete="OFF" MaxLength="1000"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="vcToolTip" Visible="false" HeaderText="vcToolTip" />
                        <asp:TemplateField HeaderText="Set Validation" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <a href="#" onclick="javascript:OpenValidation('<%# DataBinder.Eval(Container.DataItem, "numFormID") %>','<%# DataBinder.Eval(Container.DataItem, "numFormFieldId") %>','<%# DataBinder.Eval(Container.DataItem, "tintFlag") %>')"
                                    class="btn btn-default btn-sm">Set Validation</a>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle ForeColor="#000066" BackColor="White"></PagerStyle>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
