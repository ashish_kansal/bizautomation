Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Marketing
Imports System.IO
Imports ClosedXML.Excel

Namespace BACRM.UserInterface.Admin
    Public Class frmAdvancedSearchRes
        Inherits BACRMPage
        Dim strColumn As String
        Dim boolDelete, boolExport, boolUpdate, boolPrepareMessage As Boolean
        Dim RegularSearch As String
        Dim CustomSearch As String

        Dim lngBroadcastID, lngSearchID As Long
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                'CLEAR ALERT MESSAGE
                divMessage.Style.Add("display", "none")
                litMessage.Text = ""

                lngBroadcastID = CCommon.ToLong(GetQueryStringVal("BroadcastId"))
                lngSearchID = CCommon.ToLong(GetQueryStringVal("SearchID"))
                If GetQueryStringVal("frm") = "Profile" Then
                    GetUserRightsForPage(6, 3)
                Else : GetUserRightsForPage(9, 1)
                End If

                If Not IsPostBack Then
                    ViewState("AdvancedSearchCondition") = Session("AdvancedSearchCondition")
                    ViewState("AdvancedSearchSavedSearchID") = Session("AdvancedSearchSavedSearchID")
                    Session("AdvancedSearchCondition") = Nothing
                    Session("AdvancedSearchSavedSearchID") = Nothing

                    Dim m_aryRightsEditSearchViews(), m_aryRightsMDelete(), m_aryRightsMUpdate(), m_aryRightsEmailGrp(), m_aryRightsPrepareMess(), m_aryRightsPrepareOwner() As Integer
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS", False)
                    m_aryRightsEditSearchViews = GetUserRightsForPage_Other(9, 2)
                    m_aryRightsMDelete = GetUserRightsForPage_Other(9, 3)
                    m_aryRightsMUpdate = GetUserRightsForPage_Other(9, 4)
                    m_aryRightsEmailGrp = GetUserRightsForPage_Other(9, 5)
                    m_aryRightsPrepareMess = GetUserRightsForPage_Other(9, 6)
                    m_aryRightsPrepareOwner = GetUserRightsForPage_Other(9, 7)

                    If m_aryRightsEditSearchViews(RIGHTSTYPE.VIEW) <> 0 Then hplEditResView.Attributes.Add("onclick", "return EditSearchView();")
                    If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then btnExportToExcel.Visible = False
                    If m_aryRightsMDelete(RIGHTSTYPE.VIEW) = 0 Then btnMassDelete.Visible = False
                    If m_aryRightsMUpdate(RIGHTSTYPE.VIEW) = 0 Then btnMassUpdate.Visible = False
                    If m_aryRightsEmailGrp(RIGHTSTYPE.VIEW) = 0 Then btnAddEmailGroup.Visible = False
                    If m_aryRightsPrepareMess(RIGHTSTYPE.VIEW) = 0 Then btnPrepareMsg.Visible = False
                    If m_aryRightsPrepareOwner(RIGHTSTYPE.VIEW) = 0 Then btnTransfer.Visible = False

                    'Saved search
                    If CCommon.ToString(Session("SavedSearchCondition")) = "" Then hplSaveSearch.Visible = False

                    If lngSearchID > 0 Then
                        Dim objSearch As New FormGenericAdvSearch
                        objSearch.SetSavedSearchQuery(lngSearchID, 1)
                    End If
                    If Session("WhereCondition") = "DUP" Then
                        hplSaveSearch.Visible = False
                    End If

                    txtCurrrentPage.Text = 1

                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                        txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                        txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))
                        ddlSort.ClearSelection()
                        If Not ddlSort.Items.FindByValue(CCommon.ToString(PersistTable(PersistKey.FilterBy))) Is Nothing Then ddlSort.Items.FindByValue(CCommon.ToString(PersistTable(PersistKey.FilterBy))).Selected = True
                        txtGridColumnFilter.Text = CCommon.ToString(PersistTable(PersistKey.GridColumnSearch))
                    End If

                    BindGridView(True)
                    btnMassUpdate.Attributes.Add("onclick", "return MassUpdate()")
                    btnPrepareMsg.Attributes.Add("onclick", "return PMessage()")
                    chkSelectAll.Attributes.Add("onclick", "return SelectAll(" & chkSelectAll.ClientID & ")")
                    btnAddEmailGroup.Attributes.Add("onclick", "return getContacts()")
                End If

                If txtReload.Text = "true" Then
                    BindGridView(True)
                    txtReload.Text = "false"
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Function GetCheckedValues() As Boolean
            Try
                Session("ContIDs") = ""
                Dim isCheckboxchecked As Boolean = False
                Dim gvRow As GridViewRow
                For Each gvRow In gvSearch.Rows
                    If CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = True Then
                        Session("ContIDs") = Session("ContIDs") & CType(gvRow.FindControl("lbl1"), Label).Text & ","
                        isCheckboxchecked = True
                    End If
                Next
                Session("ContIDs") = Session("ContIDs").ToString.TrimEnd(",")
                Return isCheckboxchecked
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Sub BindGridView(ByVal CreateCol As Boolean)
            Try
                Dim objAdmin As New FormGenericAdvSearch
                Dim ds As DataSet
                Dim dtResults As DataTable
                With objAdmin
                    .QueryWhereCondition = Session("WhereCondition") & CCommon.ToString(Session("TimeQuery")) 'when sliding date is selected then we need to store where condition and time condition seperately
                    If cbPrimaryContactsOnly.Checked = True Then
                        If (.QueryWhereCondition = "DUP") Then
                            .QueryWhereCondition = "DUP-PRIMARY"
                        Else
                            .QueryWhereCondition = .QueryWhereCondition & " and ADC.bitPrimaryContact=true "
                        End If
                    End If
                    .AreasOfInt = Session("AreasOfInt")
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                    .PageSize = Session("PagingRows")
                    .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                    .FormID = 1
                    .DisplayColumns = CCommon.ToString(GetQueryStringVal("displayColumns"))
                    'Set the View which is being configure
                    If Not IsPostBack Then
                        ddlSort.DataSource = .getSearchFieldList.Tables(1)              'Set the datasource for the available fields
                        ddlSort.DataTextField = "vcFormFieldName"                       'set the text field
                        ddlSort.DataValueField = "vcDbColumnName"                       'set the value attribut
                        ddlSort.DataBind()
                    End If
                    If ddlSort.SelectedIndex <> -1 Then .SortOrder = ddlSort.SelectedItem.Value
                    .SortCharacter = txtSortChar.Text.Trim()
                    .SortcolumnName = IIf(txtSortColumn.Text.ToLower() = "bintcreateddate", "DM.bintCreatedDate", txtSortColumn.Text)
                    .SortcolumnName = .SortcolumnName.Replace("cmp", "c")
                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()

                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0

                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If

                    If cbShowAll.Checked = False Then
                        .columnSearch = txtColValue.Value
                        .columnName = txtColumnName.Text
                    ElseIf ddlSort.SelectedIndex <> -1 Then
                        .columnName = ddlSort.SelectedItem.Value
                    Else
                        .columnSearch = ""
                        .columnName = ""
                    End If

                    Dim i As Integer
                    If boolDelete = True Then
                        .GetAll = True
                        ds = .AdvancedSearch()
                        dtResults = ds.Tables(0)

                        Dim strError As String
                        Dim objContact As New CContacts
                        Dim lngCntID As Long
                        For i = 0 To dtResults.Rows.Count - 1
                            strError = ""
                            If hdnDeleteMode.Value = "2" Then
                                Try
                                    'delete organization
                                    Dim objAccounts As New CAccounts
                                    With objAccounts
                                        .DivisionID = CCommon.ToLong(dtResults.Rows(i)("numDivisionID"))
                                        .DomainID = Session("DomainID")
                                    End With
                                    strError = objAccounts.DeleteOrg()
                                    If strError.Length > 0 Then
                                        ShowMessage(strError)
                                    End If
                                Catch ex As Exception
                                    ShowMessage("Some of the records cannot be deleted")
                                End Try
                            End If

                            If hdnDeleteMode.Value = "1" Then
                                Try
                                    lngCntID = dtResults.Rows(i).Item(0)
                                    If lngCntID <> Session("AdminID") Then
                                        objContact.ContactID = dtResults.Rows(i).Item(0)
                                        objContact.DomainID = Session("DomainID")
                                        strError = objContact.DelContact()
                                        If strError.Length > 0 Then
                                            ShowMessage(strError)
                                        End If
                                    End If

                                Catch ex As Exception
                                    ShowMessage("Some of the records cannot be deleted")
                                End Try
                            End If

                        Next
                        .GetAll = False

                    End If
                    If boolPrepareMessage = True Then
                        .GetAll = True
                        ds = .AdvancedSearch()
                        dtResults = ds.Tables(0)
                        For i = 0 To dtResults.Rows.Count - 1
                            Session("ContIDs") = Session("ContIDs") + CCommon.ToString(dtResults.Rows(i).Item(0)) + ","
                        Next
                        Session("ContIDs") = Session("ContIDs").ToString().TrimEnd(",")
                        .GetAll = False
                    End If


                    If boolExport = True Then
                        .GetAll = True
                        ds = .AdvancedSearch()
                        dtResults = ds.Tables(0)
                        Dim iExportCount As Integer
                        dtResults.Columns.RemoveAt(4)
                        dtResults.Columns.RemoveAt(3)
                        dtResults.Columns.RemoveAt(2)
                        dtResults.Columns.RemoveAt(1)
                        dtResults.Columns.RemoveAt(0)
                        Response.Clear()



                        Dim fs As New MemoryStream()
                        Dim workbook As New XLWorkbook
                        Dim workSheet As IXLWorksheet = workbook.Worksheets.Add("Data")



                        Dim k As Int32 = 1
                        Dim n As Int32 = 1

                        For Each column As DataColumn In dtResults.Columns
                            If column.ColumnName.StartsWith("Cust") AndAlso column.ColumnName.EndsWith("~1") Then
                                Dim arrRows As DataRow() = ds.Tables(1).Select("vcDbColumnName='" & CCommon.ToString(column.ColumnName.Split("~")(0)) & "' AND numFieldID=" & CCommon.ToLong(column.ColumnName.Split("~")(1)) & " AND bitCustomField=1")

                                If Not arrRows Is Nothing AndAlso arrRows.Count > 0 Then
                                    column.ColumnName = CCommon.ToString(arrRows(0)("vcFieldName"))
                                End If
                            Else
                                Dim arrRows As DataRow() = ds.Tables(1).Select("vcDbColumnName='" & CCommon.ToString(column.ColumnName.Split("~")(0)) & "' AND numFieldID=" & CCommon.ToLong(column.ColumnName.Split("~")(1)) & " AND bitCustomField=0")

                                If Not arrRows Is Nothing AndAlso arrRows.Count > 0 Then
                                    column.ColumnName = CCommon.ToString(arrRows(0)("vcFieldName"))
                                End If
                            End If

                            workSheet.Cell(k, n).SetValue(column.ColumnName).SetDataType(XLCellValues.Text)
                            workSheet.Cell(k, n).Style.Font.Bold = True
                            workSheet.Cell(k, n).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center

                            n = n + 1
                        Next

                        k = k + 1
                        n = 1
                        For Each dr As DataRow In dtResults.Rows
                            For Each column As DataColumn In dtResults.Columns
                                If Double.TryParse(CCommon.ToString(dr(column.ColumnName)), Nothing) Then
                                    workSheet.Cell(k, n).SetValue(CCommon.ToString(dr(column.ColumnName))).SetDataType(XLCellValues.Number)
                                ElseIf IsDate(CCommon.ToString(dr(column.ColumnName))) Then
                                    workSheet.Cell(k, n).SetValue(CCommon.ToString(dr(column.ColumnName))).SetDataType(XLCellValues.DateTime)
                                Else
                                    workSheet.Cell(k, n).SetValue(CCommon.ToString(dr(column.ColumnName))).SetDataType(XLCellValues.Text)
                                End If

                                n = n + 1
                            Next
                            k = k + 1
                            n = 1
                        Next

                        workSheet.Columns.AdjustToContents()

                        Dim httpResponse As HttpResponse = Response
                        httpResponse.Clear()
                        httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                        httpResponse.AddHeader("content-disposition", String.Format("attachment;filename=File{0}.xlsx", Format(Now, "ddmmyyyyhhmmss")))


                        Using MemoryStream As New MemoryStream
                            workbook.SaveAs(MemoryStream)
                            MemoryStream.WriteTo(httpResponse.OutputStream)
                            MemoryStream.Close()
                        End Using

                        httpResponse.Flush()
                        httpResponse.End()

                        .GetAll = False
                    End If
                    If boolUpdate = True Then
                        If chkSelectAll.Checked = False Then
                            GetCheckedValues()
                            If Session("ContIDs") <> "" Then
                                .strMassUpdate = Replace(Session("UpdateQuery"), "@$replace", "@$replace where ADC.numContactID in (" & Session("ContIDs") & "));")
                                .bitMassUpdate = 1
                            Else
                                .bitMassUpdate = 0
                            End If
                        Else
                            Session("ContIDs") = Nothing
                            .strMassUpdate = Replace(Session("UpdateQuery"), "@$replace", "@$replace join #tempTable T on T.numContactId=ADC.numContactId);")
                            .bitMassUpdate = 1
                        End If
                        .lookTable = Session("LookTable")
                        Session("UpdateQuery") = Nothing
                        Session("LookTable") = Nothing
                    End If

                    GridColumnSearchCriteria()
                    .RegularSearchCriteria = RegularSearch
                    .CustomSearchCriteria = CustomSearch
                    .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    ds = .AdvancedSearch()

                    dtResults = ds.Tables(0)
                    Dim dtColumns As DataTable = ds.Tables(1)

                    bizPager.PageSize = Session("PagingRows")
                    bizPager.CurrentPageIndex = txtCurrrentPage.Text
                    bizPager.RecordCount = objAdmin.TotalRecords


                    'If .TotalRecords = 0 Then
                    '    hidenav.Visible = False
                    '    lblRecordCount.Text = 0
                    'Else
                    '    hidenav.Visible = True
                    '    lblRecordCount.Text = String.Format("{0:#,###}", .TotalRecords)
                    '    Dim strTotalPage As String()
                    '    Dim decTotalPage As Decimal
                    '    decTotalPage = lblRecordCount.Text / Session("PagingRows")
                    '    decTotalPage = Math.Round(decTotalPage, 2)
                    '    strTotalPage = CStr(decTotalPage).Split(".")
                    '    If (lblRecordCount.Text Mod Session("PagingRows")) = 0 Then
                    '        lblTotal.Text = strTotalPage(0)
                    '        txtTotalPage.Text = strTotalPage(0)
                    '    Else
                    '        lblTotal.Text = strTotalPage(0) + 1
                    '        txtTotalPage.Text = strTotalPage(0) + 1
                    '    End Ift 
                    '    txtTotalRecords.Text = lblRecordCount.Text
                    'End If
                    'Dim m_aryRightsForInlineEdit() As Integer = GetUserRightsForPage_Other(1,'')
                    'Dim j As Integer
                    'For j = 0 To dtColumns.Columns.Count - 1
                    '    dtColumns.Columns(j).ColumnName = dtColumns.Columns(j).ColumnName.Replace(".", "")
                    'Next

                    PersistTable.Clear()
                    PersistTable.Add(PersistKey.CurrentPage, txtCurrrentPage.Text)
                    PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())
                    PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
                    PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text.Trim())
                    PersistTable.Add(PersistKey.FilterBy, ddlSort.SelectedValue)
                    PersistTable.Add(PersistKey.GridColumnSearch, txtGridColumnFilter.Text)
                    PersistTable.Save()

                    Dim htGridColumnSearch As New Hashtable

                    If txtGridColumnFilter.Text.Trim.Length > 0 Then
                        Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")
                        Dim strIDValue() As String

                        For j = 0 To strValues.Length - 1
                            strIDValue = strValues(j).Split(":")

                            htGridColumnSearch.Add(strIDValue(0), strIDValue(1))
                        Next
                    End If

                    If CreateCol = True Then
                        If gvSearch.Columns.Count > 0 Then
                            gvSearch.Columns.Clear()
                        End If

                        Dim bField As BoundField
                        Dim Tfield As TemplateField

                        'For i = 0 To dtColumns.Columns.Count - 1
                        '    'Dim str As String()
                        '    'Dim numFieldID As Long
                        '    'Dim numFormFieldID As Long
                        '    'Dim intColumnWidth As Int32
                        '    'Dim bitCustomField As Boolean
                        '    'str = dtResults.Columns(i).ColumnName.Split("~")
                        '    Tfield = New TemplateField

                        '    Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, dtColumns.Rows(i), 0, htGridColumnSearch, 1, objAdmin.columnName, objAdmin.columnSortOrder)

                        '    Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, dtColumns.Rows(i), 0, htGridColumnSearch, 1, objAdmin.columnName, objAdmin.columnSortOrder)
                        '    gvSearch.Columns.Add(Tfield)
                        '    'If dtResults.Columns(i).ColumnName = "Contact Email" Or dtResults.Columns(i).ColumnName = "Organization Name" Or dtResults.Columns(i).ColumnName = "First Name" Or dtResults.Columns(i).ColumnName = "Last Name" Then
                        '    '    Tfield = New TemplateField
                        '    '    Tfield.HeaderTemplate = New MyTemp(ListItemType.Header, 1, str(0), str(1), dtResults.Columns(0).ColumnName, dtResults.Columns(1).ColumnName, dtResults.Columns(4).ColumnName, numFormFieldID:=numFormFieldID, numFieldID:=numFieldID, bitCustomField:=bitCustomField, intColumnWidth:=intColumnWidth)
                        '    '    Tfield.ItemTemplate = New MyTemp(ListItemType.Item, 1, str(0), str(1), dtResults.Columns(0).ColumnName, dtResults.Columns(1).ColumnName, dtResults.Columns(4).ColumnName)
                        '    '    gvSearch.Columns.Add(Tfield)
                        '    'Else
                        '    '    If i < 5 Then
                        '    '        bField = New BoundField
                        '    '        bField.DataField = dtResults.Columns(i).ColumnName
                        '    '        bField.Visible = False
                        '    '        gvSearch.Columns.Add(bField)
                        '    '    Else
                        '    '        Dim dr As DataRow() = dtColumns.Select("vcDbColumnName='" & str(1) & "'")

                        '    '        If dr.Length > 0 Then
                        '    '            numFieldID = CCommon.ToLong(dr(0)("numFieldID"))
                        '    '            numFormFieldID = CCommon.ToLong(dr(0)("numFormFieldID"))
                        '    '            bitCustomField = CCommon.ToBool(dr(0)("bitCustom"))
                        '    '            intColumnWidth = CCommon.ToInteger(dr(0)("intColumnWidth"))
                        '    '        End If

                        '    '        Tfield = New TemplateField
                        '    '        Tfield.HeaderTemplate = New MyTemp(ListItemType.Header, 1, str(0), str(1), dtResults.Columns(0).ColumnName, dtResults.Columns(1).ColumnName, dtResults.Columns(4).ColumnName, numFormFieldID:=numFormFieldID, numFieldID:=numFieldID, bitCustomField:=bitCustomField, intColumnWidth:=intColumnWidth)
                        '    '        Tfield.ItemTemplate = New MyTemp(ListItemType.Item, 1, str(0), str(1), dtResults.Columns(0).ColumnName, dtResults.Columns(1).ColumnName, dtResults.Columns(4).ColumnName)
                        '    '        gvSearch.Columns.Add(Tfield)
                        '    '    End If
                        '    'End If
                        'Next


                        For Each drRow As DataRow In dtColumns.Rows
                            Tfield = New TemplateField

                            Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, drRow, 0, htGridColumnSearch, 1, objAdmin.columnName, objAdmin.columnSortOrder)

                            Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, drRow, 0, htGridColumnSearch, 1, objAdmin.columnName, objAdmin.columnSortOrder)
                            gvSearch.Columns.Add(Tfield)
                        Next


                        'If i >= 5 Then
                        'Tfield = New TemplateField
                        'Tfield.ItemStyle.Width = New Unit(25)
                        'Tfield.HeaderTemplate = New MyTemp(ListItemType.Header, 1, "CheckBox", "", dtResults.Columns(0).ColumnName, dtResults.Columns(1).ColumnName, dtResults.Columns(4).ColumnName)
                        'Tfield.ItemTemplate = New MyTemp(ListItemType.Item, 1, "CheckBox", "", dtResults.Columns(0).ColumnName, dtResults.Columns(1).ColumnName, dtResults.Columns(4).ColumnName)
                        'gvSearch.Columns.Add(Tfield)
                        Dim dr As DataRow
                        dr = dtColumns.NewRow()
                        dr("vcAssociatedControlType") = "DeleteCheckBox"
                        dr("intColumnWidth") = "30"

                        'Tfield = New TemplateField
                        'Tfield.ItemStyle.Width = New Unit(25)
                        'Tfield.HeaderTemplate = New MyTemp(ListItemType.Header, 1, "CheckBox", "", dtResults.Columns(0).ColumnName, dtResults.Columns(1).ColumnName, dtResults.Columns(4).ColumnName)
                        'Tfield.ItemTemplate = New MyTemp(ListItemType.Item, 1, "CheckBox", "", dtResults.Columns(0).ColumnName, dtResults.Columns(1).ColumnName, dtResults.Columns(4).ColumnName)
                        'gvSearch.Columns.Add(Tfield)
                        Tfield = New TemplateField
                        Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, dr, 0, htGridColumnSearch, 1)
                        Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, dr, 0, htGridColumnSearch, 1)
                        gvSearch.Columns.Add(Tfield)
                        'End If
                    End If

                    gvSearch.DataSource = dtResults
                    gvSearch.DataBind()

                End With
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub GridColumnSearchCriteria()
            Try
                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")

                    Dim strIDValue(), strID(), strCustom As String
                    Dim strRegularCondition As New ArrayList
                    Dim strCustomCondition As New ArrayList


                    For i As Integer = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")
                        strID = strIDValue(0).Split("~")

                        If strID(0).Contains("CFW.Cust") Then
                            Select Case strID(3).Trim()
                                Case "TextBox", "TextArea"
                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & " ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                Case "CheckBox"
                                    If strIDValue(1).ToLower() = "yes" Then
                                        strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and COALESCE(CFW.Fld_Value,'0') " & "='1')")
                                    ElseIf strIDValue(1).ToLower() = "no" Then
                                        strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND 1 = (CASE WHEN (SELECT COUNT(*) FROM CFW_Fld_Values CFWInner WHERE CFWInner.RecId=DivisionMaster.numDivisionID AND CFWInner.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND CFWInner.Fld_Value='1') > 0 THEN 0 ELSE 1 END))")
                                    End If
                                Case "SelectBox"
                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & "=" & strIDValue(1) & ")")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " >= '" & fromDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " <= '" & toDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    End If
                                Case "CheckBoxList"
                                    Dim items As String() = strIDValue(1).Split(",")
                                    Dim searchString As String = ""

                                    For Each item As String In items
                                        searchString = searchString & If(searchString.Length > 0, " OR ", "") & " fn_GetCustFldStringValue(" & strID(0).Replace("CFW.Cust", "") & ",DivisionMaster.numDivisionID,CFW.Fld_Value) " & " ilike '%" & item.Replace("'", "''") & "%'"
                                    Next

                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND (" & searchString & "))")
                                Case Else
                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & strCustom & ")")
                            End Select
                        Else
                            Select Case strID(3).Trim()
                                Case "Website", "Email", "TextBox"
                                    strRegularCondition.Add(strID(0) & " ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                Case "SelectBox"
                                    strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                                Case "TextArea"
                                    strRegularCondition.Add(" Cast(" & strID(0) & " as varchar(5000)) ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strRegularCondition.Add(strID(0) & " >= '" & fromDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strRegularCondition.Add(strID(0) & " <= '" & toDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    End If
                            End Select
                        End If
                    Next
                    RegularSearch = String.Join(" and ", strRegularCondition.ToArray()).Replace("cmp", "c")
                    CustomSearch = String.Join(" and ", strCustomCondition.ToArray())
                Else
                    RegularSearch = ""
                    CustomSearch = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub gvSearch_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSearch.RowDataBound
            Try
                If e.Row.RowType = DataControlRowType.DataRow Then
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnNew1Ok_ServerClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew1Ok.ServerClick
            Try
                'ViewState("SortChar") = ""
                txtSortChar.Text = ""
                BindGridView(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub cbPrimaryContactsOnly_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cbPrimaryContactsOnly.CheckedChanged
            Try
                BindGridView(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnMassDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMassDelete.Click
            Try
                If chkSelectAll.Checked = True Then
                    boolDelete = True
                    BindGridView(False)
                Else
                    Dim gvRow As GridViewRow
                    Dim lngCntID, lngDivId As Long
                    Dim objContact As New CContacts
                    Dim strError As String
                    For Each gvRow In gvSearch.Rows
                        If CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = True Then
                            lngCntID = CType(gvRow.FindControl("lbl1"), Label).Text
                            lngDivId = CType(gvRow.FindControl("lblDivID"), Label).Text
                            strError = ""
                            If hdnDeleteMode.Value = "2" Then
                                'delete organization
                                Dim objAccounts As New CAccounts
                                With objAccounts
                                    .DivisionID = lngDivId
                                    .DomainID = Session("DomainID")
                                End With
                                strError = objAccounts.DeleteOrg()
                                If strError.Length > 0 Then
                                    ShowMessage(strError)
                                End If
                            End If
                            If hdnDeleteMode.Value = "1" Then
                                If lngCntID <> Session("AdminID") Then
                                    objContact.ContactID = CType(gvRow.FindControl("lbl1"), Label).Text
                                    objContact.DomainID = Session("DomainID")
                                    strError = objContact.DelContact()
                                    If strError.Length > 2 Then
                                        ShowMessage("Some contacts can not be deleted,you need to delete those from Account Lists, " & strError)
                                    End If
                                End If
                            End If
                        End If
                    Next
                    BindGridView(False)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnPrepareMsg_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrepareMsg.Click
            Try
                Dim isCheckboxChecked As Boolean
                If chkSelectAll.Checked = False Then
                    isCheckboxChecked = GetCheckedValues()
                Else
                    Session("ContIDs") = Nothing
                    boolPrepareMessage = True
                    BindGridView(True) 'get all records in Session("ContIDs")
                    isCheckboxChecked = True
                End If
                If isCheckboxChecked Then
                    Session("EMailCampCondition") = Session("WhereCondition")
                    Response.Redirect("../Marketing/frmEmailBroadCasting.aspx?PC=" & cbPrimaryContactsOnly.Checked & "&SearchID=" & lngSearchID.ToString() & "&ID=" & lngBroadcastID.ToString() & "&SAll=" & IIf(chkSelectAll.Checked = True, 1, 0), False)
                Else
                    ShowMessage("Select atleast one checkbox to broadcast E-mail.")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportToExcel.Click
            Try
                boolExport = True
                BindGridView(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnUpdateValues_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateValues.Click
            Try
                boolUpdate = True
                BindGridView(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnUpdateDripCampaign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateDripCampaign.Click
            Try
                boolUpdate = True
                GetCheckedValues()

                Dim objAdmin As New FormGenericAdvSearch
                objAdmin.UserCntID = Session("UserContactID")
                objAdmin.SetDripCampaign(Session("ContIDs"), Session("UpdateQuery"))
                Session("UpdateQuery") = ""
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnUpdateCampaign_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateCampaign.Click
            Try
                boolUpdate = True
                GetCheckedValues()

                Dim objAdmin As New FormGenericAdvSearch
                objAdmin.UserCntID = Session("UserContactID")
                objAdmin.DomainID = Session("DomainID")
                objAdmin.UpdateOrgCampaigns(Session("ContIDs"), Session("UpdateQuery"))
                Session("UpdateQuery") = ""
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnGo1_Click(sender As Object, e As System.EventArgs) Handles btnGo1.Click
            Try
                txtCurrrentPage.Text = "1"
                BindGridView(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                BindGridView(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Private Sub ShowMessage(ByVal message As String)
            Try
                divMessage.Style.Add("display", "")
                litMessage.Text = message
                divMessage.Focus()
            Catch ex As Exception
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub lkbBackToSearchCriteria_Click(sender As Object, e As EventArgs) Handles lkbBackToSearchCriteria.Click
            Try
                If GetQueryStringVal("frm") = "Profile" Then
                    Response.Redirect("~/Marketing/frmEmailCampaignProfileSearch.aspx?I=1", False)
                ElseIf GetQueryStringVal("frm") = "EmailBroadcasting" Then
                    Response.Redirect("~/Marketing/frmEmailBroadCasting.aspx?ID=" + ID, False)
                ElseIf GetQueryStringVal("frm") = "SavedSearch" Then
                    Response.Redirect("~/admin/frmSavedSearchList.aspx", False)
                ElseIf GetQueryStringVal("frm") = "Import" Then
                    Response.Redirect("~/admin/frmImportList.aspx", False)
                Else
                    Session("AdvancedSearchCondition") = ViewState("AdvancedSearchCondition")
                    Session("AdvancedSearchSavedSearchID") = ViewState("AdvancedSearchSavedSearchID")
                    Response.Redirect("~/admin/frmAdvancedSearchNew.aspx?I=1", False)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
    End Class

    Public Class MyTemp
        Implements ITemplate

        Dim TemplateType As ListItemType
        Dim Field1, Field2, Field3, Field4, Field5, FormID, FormFieldId As String
        Dim Custom As Boolean
        Dim ColumnWidth As Integer

        Sub New(ByVal type As ListItemType, ByVal numFormID As Long, ByVal fld1 As String, ByVal fld2 As String, ByVal fld3 As String, ByVal fld4 As String, ByVal fld5 As String _
                , Optional ByVal numFieldID As Long = Nothing, Optional ByVal numFormFieldID As Long = Nothing, Optional ByVal intColumnWidth As Integer = Nothing, Optional ByVal bitCustomField As Boolean = Nothing)
            Try
                TemplateType = type
                FormID = numFormID
                Field1 = fld1
                Field2 = fld2
                Field3 = fld3
                Field4 = fld4
                Field5 = fld5

                If numFieldID > 0 Then
                    FormFieldId = numFieldID
                    ColumnWidth = intColumnWidth
                    Custom = bitCustomField
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub InstantiateIn(ByVal Container As Control) Implements ITemplate.InstantiateIn
            Try
                Dim lbl1 As Label = New Label()
                Dim lnkButton As New LinkButton
                Dim lnk As New HyperLink
                Select Case TemplateType
                    Case ListItemType.Header
                        Dim cell As DataControlFieldHeaderCell = CType(Container, DataControlFieldHeaderCell)

                        If Field1 <> "CheckBox" Then
                            lnk.Attributes.Add("onclick", "return FilterWithinRecords('" & Field1 & "','" & Field2 & "','divColHeader')")
                            lnk.CssClass = "LinkArrow1"
                            lnk.Text = ">>"
                            Container.Controls.Add(lnk)
                            lnkButton.ID = Field2
                            lnkButton.Text = Field1
                            lnkButton.Attributes.Add("onclick", "return SortColumn('" & Field2 & "')")
                            Container.Controls.Add(lnkButton)
                        Else
                            Dim chk As New CheckBox
                            chk.ID = "chkGridSelectAll"
                            chk.Attributes.Add("onclick", "return SelectAll('chkGridSelectAll','chkSelect')")
                            Container.Controls.Add(chk)
                        End If

                        If ColumnWidth > 0 Then
                            cell.Width = ColumnWidth
                        End If
                        cell.Attributes.Add("id", FormID & "~" & FormFieldId & "~" & Custom)
                    Case ListItemType.Item
                        If Field1 <> "CheckBox" Then
                            AddHandler lbl1.DataBinding, AddressOf BindStringColumn
                            Container.Controls.Add(lbl1)
                        Else
                            Dim chk As New CheckBox
                            Dim lblDivID As Label = New Label()
                            lblDivID.ID = "lblDivID"
                            lblDivID.Attributes.Add("style", "display:none;")
                            chk.ID = "chk"
                            chk.CssClass = "chkSelect"

                            AddHandler lbl1.DataBinding, AddressOf Bindvalue
                            AddHandler lblDivID.DataBinding, AddressOf BindStringColumn1
                            Container.Controls.Add(chk)
                            Container.Controls.Add(lbl1)
                            Container.Controls.Add(lblDivID)
                        End If
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub Bindvalue(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
                lbl1.ID = "lbl1"
                lbl1.Text = DataBinder.Eval(Container.DataItem, Field3)
                lbl1.Attributes.Add("style", "display:none")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindStringColumn(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
                'If Field1 = "Contact Email" Or Field1 = "Organization Name" Or Field1 = "First Name" Or Field1 = "Last Name" Then
                '    lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2)), "", DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2))
                '    Dim intermediatory As Integer
                '    intermediatory = IIf(System.Web.HttpContext.Current.Session("EnableIntMedPage") = 1, 1, 0)
                '    If Field1 = "Organization Name" OrElse Field1 = "Company Name" Then
                '        lbl1.Text = "<a  href='javascript:OpenWindow(" & DataBinder.Eval(Container.DataItem, Field4) & "," & DataBinder.Eval(Container.DataItem, Field5) & "," & intermediatory & ",0)'>" & lbl1.Text & "</a> &nbsp;&nbsp;"
                '        lbl1.Text += "<a  href='javascript:OpenWindow(" & DataBinder.Eval(Container.DataItem, Field4) & "," & DataBinder.Eval(Container.DataItem, Field5) & "," & intermediatory & ",1)'><img src='../images/open_new_window_notify.gif' width='15px' /></a>"
                '    ElseIf Field1 = "First Name" Or Field1 = "Last Name" Then
                '        lbl1.Text = "<a  href='javascript:OpenContact(" & DataBinder.Eval(Container.DataItem, Field3) & "," & intermediatory & ",0)'>" & lbl1.Text & "</a> &nbsp;&nbsp;"
                '        lbl1.Text += "<a  href='javascript:OpenContact(" & DataBinder.Eval(Container.DataItem, Field3) & "," & intermediatory & ",1)'><img src='../images/open_new_window_notify.gif' width='15px' /></a>"
                '    ElseIf Field1 = "Contact Email" Then
                '        lbl1.Attributes.Add("onclick", "return OpemEmail('" & lbl1.Text.ToString & "'," & DataBinder.Eval(Container.DataItem, Field3) & ")")
                '        lbl1.Text = "<a  href=#>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                '    End If
                'Else : lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2)), "", DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2))
                'End If

                If Field2 = "vcEmail" Or Field2 = "vcCompanyName" Or Field2 = "vcFirstName" Or Field2 = "vcLastName" Then
                    lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2)), "", DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2))
                    Dim intermediatory As Integer
                    intermediatory = IIf(System.Web.HttpContext.Current.Session("EnableIntMedPage") = 1, 1, 0)
                    If Field2 = "vcCompanyName" Then
                        lbl1.Text = "<a  href='javascript:OpenWindow(" & DataBinder.Eval(Container.DataItem, Field4) & "," & DataBinder.Eval(Container.DataItem, Field5) & "," & intermediatory & ",0)'>" & lbl1.Text & "</a> &nbsp;&nbsp;"
                        lbl1.Text += "<a  href='javascript:OpenWindow(" & DataBinder.Eval(Container.DataItem, Field4) & "," & DataBinder.Eval(Container.DataItem, Field5) & "," & intermediatory & ",1)'><img src='../images/open_new_window_notify.gif' width='15px' /></a>"
                    ElseIf Field2 = "vcFirstName" Or Field2 = "vcLastName" Then
                        lbl1.Text = "<a  href='javascript:OpenContact(" & DataBinder.Eval(Container.DataItem, Field3) & "," & intermediatory & ",0)'>" & lbl1.Text & "</a> &nbsp;&nbsp;"
                        lbl1.Text += "<a  href='javascript:OpenContact(" & DataBinder.Eval(Container.DataItem, Field3) & "," & intermediatory & ",1)'><img src='../images/open_new_window_notify.gif' width='15px' /></a>"
                    ElseIf Field2 = "vcEmail" Then
                        lbl1.Attributes.Add("onclick", "return OpemEmail('" & lbl1.Text.ToString & "'," & DataBinder.Eval(Container.DataItem, Field3) & ")")
                        lbl1.Text = "<a  href=#>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                    End If
                Else : lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2)), "", DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2))
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindStringColumn1(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
                lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, Field4)), 0, DataBinder.Eval(Container.DataItem, Field4))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace
