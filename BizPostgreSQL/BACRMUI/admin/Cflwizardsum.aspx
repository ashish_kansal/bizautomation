<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="Cflwizardsum.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.Cflwizardsum" MasterPageFile="~/common/Popup.Master"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Custom Field Wizard</title>
    <script language="javascript">
        function Close() {
            window.close()
            return false;
        }
        function Back() {
            window.location.href = "../admin/cflwizard3.aspx"
            return false;
        }
        function Finish() {
            if (document.getElementById('ddlTab').value == -1) {
                alert("Select Tab")
                document.getElementById('ddlTab').focus()
                return false;
            }
            if (document.getElementById('txtFldLabel').value == '') {
                alert("Enter Label")
                document.getElementById('txtFldLabel').focus()
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
  
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
                Custom Field Summary
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
 <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    <table  height="100%" width ="400px" cellpadding="5" cellspacing="5" class="aspTableDTL">
      
        <tr>
            <td align="right" class="text_bold">
                Selected Group
            </td>
            <td align="left">
                <asp:Label ID="lblgrp" runat="server" CssClass="normal1"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right" class="text_bold">
                Selected Tab
            </td>
            <td align="left">
                <asp:DropDownList ID="ddlTab" Width="150" runat="server" CssClass="signup">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right" class="text_bold">
                Field Type
            </td>
            <td align="left">
                <asp:Label ID="lblfldtyp" runat="server" CssClass="normal1"></asp:Label>
                <asp:CheckBox ID="chkAutoComplete" Text="AutoComplete" runat="server"/>
            </td>
        </tr>
        <tr>
            <td align="right" class="text_bold">
                Field Label
            </td>
            <td align="left">
                <asp:TextBox ID="txtFldLabel" runat="server" Width="150" CssClass="signup" MaxLength="45"></asp:TextBox>
            </td>
        </tr>
        <tr id="trURL" visible="false" runat="server">
            <td align="right" class="text_bold">
                URL
            </td>
            <td align="left">
                <asp:TextBox ID="txtURL" runat="server" Width="250" CssClass="signup" MaxLength="500"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="right" class="text_bold">
                ToolTip
            </td>
            <td class="normal1" align="left">
                <asp:TextBox runat="server" ID="txtToolTip" CssClass="signup" Width="250" MaxLength="1000"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2">
                <asp:Button ID="btnBack" runat="server" CssClass="btn btn-primary" Text="Back">
                </asp:Button>
                <asp:Button ID="btnFinish" runat="server" CssClass="btn btn-primary" Text="Finish">
                </asp:Button>
                <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary" Text="Close">
                </asp:Button>
                <br/>
                <br/>
                <br/>
                <br/>
                
            </td>
        </tr>
    </table>
</asp:Content>
