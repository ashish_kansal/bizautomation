﻿<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmItemsFieldSetting.aspx.vb" Inherits="BACRM.UserInterface.Admin.frmItemsFieldSetting"
    MasterPageFile="~/common/PopupBootstrap.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Elective Item Fields</title>
    <style>
        #table9 tr td {
            border: 0px;
        }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class=" pull-right">
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" OnClientClick="return Save();"></asp:Button>
        <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" OnClientClick="return Close()" Text="Close"></asp:Button>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Elective Item Fields
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
  <b>  Elective Fields</b> (Un-Check to hide)
    <telerik:RadComboBox ID="rcbItems" runat="server" CheckBoxes="true">
        <Items>
            <telerik:RadComboBoxItem Text="Model ID" Value="1" />
            <telerik:RadComboBoxItem Text="SKU" Value="2" />
            <telerik:RadComboBoxItem Text="UPC" Value="3" />
            <telerik:RadComboBoxItem Text="ASIN" Value="4" />
            <telerik:RadComboBoxItem Text="Child Membership" Value="6" />
            <telerik:RadComboBoxItem Text="Item Class" Value="7" />
        </Items>
    </telerik:RadComboBox>
</asp:Content>
