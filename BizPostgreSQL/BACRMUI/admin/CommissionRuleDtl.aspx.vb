﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Accounting
Public Class CommissionRuleDtl
    Inherits BACRMPage
    Dim objCommon As CCommon
    Dim lngRuleId, itemCount, orgCount As Long
    Dim dtItems As New DataTable
    Dim objCommissionRule As CommissionRule
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            lngRuleId = CCommon.ToLong(GetQueryStringVal("RuleId"))

            If Not IsPostBack Then
                If lngRuleId > 0 Then
                    pnlSel.Visible = True

                    LoadDetails()

                    hplIndividualItem.Attributes.Add("onclick", "return OpenConfig(3,1," & lngRuleId & ")")
                    hplClassification.Attributes.Add("onclick", "return OpenConfig(3,2," & lngRuleId & ")")
                    hplIndividualOrg.Attributes.Add("onclick", "return OpenConfig(4,3," & lngRuleId & ")")
                    hplRelProfile.Attributes.Add("onclick", "return OpenConfig(4,4," & lngRuleId & ")")
                End If
            End If

            litMessage.Text = ""
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Sub LoadDetails()
        Try
            Dim dtTable As DataTable
            objCommissionRule = New CommissionRule
            objCommissionRule.RuleID = lngRuleId
            objCommissionRule.byteMode = 0
            dtTable = objCommissionRule.GetCommissionRule
            If dtTable.Rows.Count > 0 Then
                txtRuleName.Text = dtTable.Rows(0).Item("vcCommissionName")

                'SETP 1
                If Not ddlBasedOn.Items.FindByValue(dtTable.Rows(0).Item("tintComBasedOn")) Is Nothing Then
                    ddlBasedOn.Items.FindByValue(dtTable.Rows(0).Item("tintComBasedOn")).Selected = True
                End If

                'If Not ddDuration.Items.FindByValue(dtTable.Rows(0).Item("tinComDuration")) Is Nothing Then
                '    ddDuration.Items.FindByValue(dtTable.Rows(0).Item("tinComDuration")).Selected = True
                'End If

                If Not ddlCommsionType.Items.FindByValue(dtTable.Rows(0).Item("tintComType")) Is Nothing Then
                    ddlCommsionType.Items.FindByValue(dtTable.Rows(0).Item("tintComType")).Selected = True
                End If

                dtItems = objCommissionRule.GetCommissionTable()

                Dim dr As DataRow
                For i As Integer = dtItems.Rows.Count To 10
                    dr = dtItems.NewRow
                    dtItems.Rows.Add(dr)
                Next

                dgCommissionTable.DataSource = dtItems
                dgCommissionTable.DataBind()


                'STEP 2
                If CCommon.ToShort(dtTable.Rows(0).Item("tintComAppliesTo")) = 1 Then
                    rbIndividualItem.Checked = True
                    hplIndividualItem.Text = hplIndividualItem.Text + " <label id=""lblIndividualItem"">(" + CCommon.ToInteger(dtTable.Rows(0).Item("ItemCount")).ToString + ")</lable>"
                    hplClassification.Text = hplClassification.Text + " <label id=""lblClassification"">(0)</lable>"
                ElseIf CCommon.ToShort(dtTable.Rows(0).Item("tintComAppliesTo")) = 2 Then
                    rbItemClassification.Checked = True
                    hplClassification.Text = hplClassification.Text + " <label id=""lblClassification"">(" + CCommon.ToInteger(dtTable.Rows(0).Item("ItemCount")).ToString + ")</lable>"
                    hplIndividualItem.Text = hplIndividualItem.Text + " <label id=""lblIndividualItem"">(0)</lable>"
                ElseIf CCommon.ToShort(dtTable.Rows(0).Item("tintComAppliesTo")) = 3 Then
                    rbAllItems.Checked = True
                    hplIndividualItem.Text = hplIndividualItem.Text + " <label id=""lblIndividualItem"">(0)</lable>"
                    hplClassification.Text = hplClassification.Text + " <label id=""lblClassification"">(0)</lable>"
                Else
                    hplIndividualItem.Text = hplIndividualItem.Text + " <label id=""lblIndividualItem"">(0)</lable>"
                    hplClassification.Text = hplClassification.Text + " <label id=""lblClassification"">(0)</lable>"
                End If

                'STEP 3
                If CCommon.ToShort(dtTable.Rows(0).Item("tintComOrgType")) = 1 Then
                    rbIndividualOrg.Checked = True
                    hplIndividualOrg.Text = hplIndividualOrg.Text + " <label id=""lblIndividualOrg"">(" + CCommon.ToInteger(dtTable.Rows(0).Item("OrganizationCount")).ToString + ")</lable>"
                    hplRelProfile.Text = hplRelProfile.Text + " <label id=""lblRelProfile"">(0)</lable>"
                ElseIf CCommon.ToShort(dtTable.Rows(0).Item("tintComOrgType")) = 2 Then
                    rbRelProfile.Checked = True
                    hplRelProfile.Text = hplRelProfile.Text + " <label id=""lblRelProfile"">(" + CCommon.ToInteger(dtTable.Rows(0).Item("OrganizationCount")).ToString + ")</lable>"
                    hplIndividualOrg.Text = hplIndividualOrg.Text + " <label id=""lblIndividualOrg"">(0)</lable>"
                ElseIf CCommon.ToShort(dtTable.Rows(0).Item("tintComOrgType")) = 3 Then
                    rbAllOrg.Checked = True
                    hplIndividualOrg.Text = hplIndividualOrg.Text + " <label id=""lblIndividualOrg"">(0)</lable>"
                    hplRelProfile.Text = hplRelProfile.Text + " <label id=""lblRelProfile"">(0)</lable>"
                Else
                    hplIndividualOrg.Text = hplIndividualOrg.Text + " <label id=""lblIndividualOrg"">(0)</lable>"
                    hplRelProfile.Text = hplRelProfile.Text + " <label id=""lblRelProfile"">(0)</lable>"
                End If


                'STEP 5
                If dtTable.Rows(0).Item("tintAssignTo") = 1 Then
                    rbAssign.Checked = True
                ElseIf dtTable.Rows(0).Item("tintAssignTo") = 2 Then
                    rbOwner.Checked = True
                ElseIf dtTable.Rows(0).Item("tintAssignTo") = 3 Then
                    rbPartner.Checked = True
                End If

                

                hdnSelectedItemOrClassifiction.Value = CCommon.ToInteger(dtTable.Rows(0).Item("ItemCount")).ToString
                hdnSelectedOrgOrProfile.Value = CCommon.ToInteger(dtTable.Rows(0).Item("OrganizationCount")).ToString
            End If

            
            LoadOtherContact()

            lstSelectedContact.DataSource = objCommissionRule.GetCommissionRuleContacts
            lstSelectedContact.DataTextField = "vcUserName"
            lstSelectedContact.DataValueField = "numContactID"
            lstSelectedContact.DataBind()

            Dim iCount As Integer
            For iCount = 0 To lstSelectedContact.Items.Count - 1
                If Not lstAvailableContacts.Items.FindByValue(lstSelectedContact.Items(iCount).Value) Is Nothing Then
                    lstAvailableContacts.Items.Remove(lstSelectedContact.Items(iCount))
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If CCommon.ToLong(ChartOfAccounting.GetDefaultAccount("CP", Session("DomainID"))) = 0 Then
                DisplayError("Contract Employee Payroll is not set. Go to Global Settings | Accounting | Change Default Accounting Mapping | Default Accounts |  Contract Employee Payroll and select account.")
                Exit Sub
            End If

            lngRuleId = Save()
            If lngRuleId > 0 Then
                'If GetQueryStringVal( "RuleId") = "" Then
                Response.Redirect("../admin/CommissionRuleDtl.aspx?RuleId=" & lngRuleId)
                'Else
                'LoadDetails()
                'End If
            Else
                itemCount = CCommon.ToLong(hdnSelectedItemOrClassifiction.Value)
                orgCount = CCommon.ToLong(hdnSelectedOrgOrProfile.Value)


                If rbIndividualItem.Checked Then
                    hplIndividualItem.Text = "<label for='rbIndividualItem'>Apply to items individually</label> <label id=""lblIndividualItem"">(" + CCommon.ToInteger(hdnSelectedItemOrClassifiction.Value).ToString + ")</lable>"
                    hplClassification.Text = "<label for='rbItemClassification'>Apply to items with the selected item classifications </label> <label id=""lblClassification"">(0)</lable>"
                ElseIf rbItemClassification.Checked Then
                    hplClassification.Text = "<label for='rbItemClassification'>Apply to items with the selected item classifications</label> <label id=""lblClassification"">(" + CCommon.ToInteger(hdnSelectedItemOrClassifiction.Value).ToString + ")</lable>"
                    hplIndividualItem.Text = "<label for='rbIndividualItem'>Apply to items individually</label> <label id=""lblIndividualItem"">(0)</lable>"
                ElseIf rbAllItems.Checked Then
                    hplIndividualItem.Text = "<label for='rbIndividualItem'>Apply to items individually</label> <label id=""lblIndividualItem"">(0)</lable>"
                    hplClassification.Text = "<label for='rbItemClassification'>Apply to items with the selected item classifications</label> <label id=""lblClassification"">(0)</lable>"
                Else
                    hplIndividualItem.Text = "<label for='rbIndividualItem'>Apply to items individually</label> <label id=""lblIndividualItem"">(0)</lable>"
                    hplClassification.Text = "<label for='rbItemClassification'>Apply to items with the selected item classifications</label> <label id=""lblClassification"">(0)</lable>"
                End If

                If rbIndividualOrg.Checked Then
                    hplIndividualOrg.Text = "<label for='rbIndividualOrg'>Apply to customers individually</label> <label id=""lblIndividualOrg"">(" + CCommon.ToInteger(hdnSelectedOrgOrProfile.Value).ToString + ")</lable>"
                    hplRelProfile.Text = "<label for='rbRelProfile'>Apply to customers with the following Relationships & Profiles</label> <label id=""lblRelProfile"">(0)</lable>"
                ElseIf rbRelProfile.Checked Then
                    hplRelProfile.Text = "<label for='rbRelProfile'>Apply to customers with the following Relationships & Profiles</label> <label id=""lblRelProfile"">(" + CCommon.ToInteger(hdnSelectedOrgOrProfile.Value).ToString + ")</lable>"
                    hplIndividualOrg.Text = "<label for='rbIndividualOrg'>Apply to customers individually</label> <label id=""lblIndividualOrg"">(0)</lable>"
                ElseIf rbAllOrg.Checked Then
                    hplIndividualOrg.Text = "<label for='rbIndividualOrg'>Apply to customers individually</label> <label id=""lblIndividualOrg"">(0)</lable>"
                    hplRelProfile.Text = "<label for='rbRelProfile'>Apply to customers with the following Relationships & Profiles</label> <label id=""lblRelProfile"">(0)</lable>"
                Else
                    hplIndividualOrg.Text = "<label for='rbIndividualOrg'>Apply to customers individually</label> <label id=""lblIndividualOrg"">(0)</lable>"
                    hplRelProfile.Text = "<label for='rbRelProfile'>Apply to customers with the following Relationships & Profiles</label> <label id=""lblRelProfile"">(0)</lable>"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            If CCommon.ToLong(ChartOfAccounting.GetDefaultAccount("CP", Session("DomainID"))) = 0 Then
                DisplayError("Contract Employee Payroll is not set. Go to Global Settings | Accounting | Change Default Accounting Mapping | Default Accounts |  Contract Employee Payroll and select account.")
                Exit Sub
            End If

            lngRuleId = Save()
            If lngRuleId > 0 Then
                Response.Redirect("../admin/CommissionRuleList.aspx")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Function Save() As Long
        Try
            objCommissionRule = New CommissionRule
            objCommissionRule.DomainID = Session("DomainID")
            objCommissionRule.RuleID = lngRuleId
            objCommissionRule.RuleName = txtRuleName.Text

            If lngRuleId > 0 Then
                'STEP 1
                objCommissionRule.BasedOn = ddlBasedOn.SelectedValue
                'objCommissionRule.Duration = ddDuration.SelectedValue
                objCommissionRule.CommissionType = ddlCommsionType.SelectedValue

                Dim dtgriditem As DataGridItem
                Dim dtrow As DataRow
                Dim ds As New DataSet
                LoadDefaultColumns()
                Dim intFromQty, intToQty As Decimal
                Dim decCommission As Decimal
                For i As Integer = 0 To dgCommissionTable.Items.Count - 1
                    dtgriditem = dgCommissionTable.Items(i)
                    intFromQty = CCommon.ToDecimal(CType(dtgriditem.FindControl("txtFrom"), TextBox).Text)
                    intToQty = CCommon.ToDecimal(CType(dtgriditem.FindControl("txtTo"), TextBox).Text)
                    decCommission = CCommon.ToDecimal(CType(dtgriditem.FindControl("txtComAmount"), TextBox).Text)

                    If decCommission > 0 And intFromQty >= 0 And intToQty > 0 Then
                        dtrow = dtItems.NewRow

                        dtrow.Item("intFrom") = intFromQty
                        dtrow.Item("intTo") = intToQty
                        dtrow.Item("decCommission") = decCommission
                        dtItems.Rows.Add(dtrow)
                    End If
                Next

                If dtItems.Rows.Count = 0 Then
                    litMessage.Text = "Set atleat one entry of from, to and commission in step 1."
                    Exit Function
                End If

                ds.Tables.Add(dtItems.Copy)
                ds.Tables(0).TableName = "CommissionTable"

                'STEP 2
                If rbIndividualItem.Checked Then
                    objCommissionRule.AppliesTo = 1
                    If CCommon.ToLong(hdnSelectedItemOrClassifiction.Value) = 0 Then
                        litMessage.Text = "Select atleast one item in step 2."
                        Exit Function
                    End If
                ElseIf rbItemClassification.Checked Then
                    objCommissionRule.AppliesTo = 2
                    If CCommon.ToLong(hdnSelectedItemOrClassifiction.Value) = 0 Then
                        litMessage.Text = "Select atleast one item classification in step 2."
                        Exit Function
                    End If
                ElseIf rbAllItems.Checked Then
                    objCommissionRule.AppliesTo = 3
                Else
                    litMessage.Text = "Select atleast one option in step 2."
                    Exit Function
                End If


                'STEP 3
                If rbIndividualOrg.Checked Then
                    objCommissionRule.tintComOrgType = 1
                    If CCommon.ToLong(hdnSelectedOrgOrProfile.Value) = 0 Then
                        litMessage.Text = "Select atleast one organization in step 2."
                        Exit Function
                    End If
                ElseIf rbRelProfile.Checked Then
                    objCommissionRule.tintComOrgType = 2
                    If CCommon.ToLong(hdnSelectedOrgOrProfile.Value) = 0 Then
                        litMessage.Text = "Select atleast one organization relationship or profile in step 3."
                        Exit Function
                    End If
                ElseIf rbAllOrg.Checked Then
                    objCommissionRule.tintComOrgType = 3
                Else
                    litMessage.Text = "Select atleast one option in step 3."
                    Exit Function
                End If

                'STEP 4
                If Not rbAssign.Checked AndAlso Not rbOwner.Checked AndAlso Not rbPartner.Checked Then
                    litMessage.Text = "Select atleast one option in step 4."
                    Exit Function
                End If

                objCommissionRule.AssignTo = IIf(rbAssign.Checked, 1, IIf(rbPartner.Checked, 3, IIf(rbOwner.Checked, 2, 0)))

                Dim dtTable As New DataTable
                dtTable.Columns.Add("numValue")
                dtTable.Columns.Add("bitCommContact")

                Dim dr As DataRow
                Dim str As String()
                str = txtContactsHidden.Text.Split(",")
                For i As Integer = 0 To str.Length - 2
                    dr = dtTable.NewRow
                    dr("numvalue") = str(i).Split("~")(0)
                    dr("bitCommContact") = str(i).Split("~")(1)
                    dtTable.Rows.Add(dr)
                Next

                If dtTable.Rows.Count = 0 Then
                    litMessage.Text = "Select atleast one value from available contacts in step 4."
                    Exit Function
                End If

                ds.Tables.Add(dtTable.Copy)
                ds.Tables(1).TableName = "ContactTable"

                objCommissionRule.strItems = ds.GetXml()

            End If

            Return objCommissionRule.ManageCommissionRule()
        Catch ex As Exception
            If ex.Message = "DUPLICATE" Then
                litMessage.Text = "A Commission Rule with this same Item/Classification(s), Customer/Relationship & Profile(s) and Contact(s) already exists. Please modify your Commison rule to make sure it's unique"
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End If
        End Try
    End Function

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../admin/CommissionRuleList.aspx")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Sub LoadDefaultColumns()
        Try
            dtItems.Columns.Add("intFrom")
            dtItems.Columns.Add("intTo")
            dtItems.Columns.Add("decCommission")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub rbPartner_CheckedChanged(sender As Object, e As EventArgs)
        Try
            lstSelectedContact.Items.Clear()
            LoadOtherContact()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Public Function LoadOtherContact()
        'STEP 5 - CONTACT LIST BOX
        objCommon = New CCommon
        Dim dt As DataTable
        Dim item As ListItem
        lstAvailableContacts.Items.Clear()
        dt = objCommon.ConEmpList(Session("DomainID"), 0, 0)
        For Each dr In dt.Rows
            item = New ListItem()
            item.Text = CCommon.ToString(dr("vcUserName"))
            item.Value = dr("numContactID") & "~0~0"
            lstAvailableContacts.Items.Add(item)
        Next

        Dim objUser As New UserAccess
        objUser.DomainID = Session("DomainID")
        objUser.byteMode = 1
        dt = objUser.GetCommissionsContacts

        For Each dr In dt.Rows
            item = New ListItem()
            item.Text = dr("vcUserName")
            item.Value = dr("numContactID") & "~1~0"
            lstAvailableContacts.Items.Add(item)
        Next


        objCommon = New CCommon
        objCommon.DomainID = Session("DomainID")
        Dim dtData As DataTable = objCommon.GetPartnerSource()

        If Not dtData Is Nothing AndAlso dtData.Rows.Count > 0 Then
            For Each dr In dtData.Rows
                item = New ListItem()
                item.Text = CCommon.ToString(dr("vcPartner"))
                item.Value = dr("numDivisionID") & "~1~1"
                lstAvailableContacts.Items.Add(item)
            Next
        End If
    End Function

    Protected Sub rbAssign_CheckedChanged(sender As Object, e As EventArgs)
        Try
            lstSelectedContact.Items.Clear()
            LoadOtherContact()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try


    End Sub

    Protected Sub rbOwner_CheckedChanged(sender As Object, e As EventArgs)
        Try
            lstSelectedContact.Items.Clear()
            LoadOtherContact()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try

    End Sub
End Class