﻿Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Partial Public Class frmCreditCardMapping
    Inherits BACRMPage
    Dim lngTransChargeID As Long
   

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            GetUserRightsForPage(13, 1)

            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnSave.Visible = False
            End If
            litMessage.Text = ""
            If Not IsPostBack Then

                BindCardType()
                BindCOA()
            End If
            If Not ViewState("TransChargeID") Is Nothing Then
                lngTransChargeID = ViewState("TransChargeID")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub BindCardType()
        Try

            objCommon.DomainID = Session("DomainID")
            objCommon.tinyOrder = 1
            ddlCreditCard.DataTextField = "CardType"
            ddlCreditCard.DataValueField = "numListItemID"
            ddlCreditCard.DataSource = objCommon.GetCardTypes()
            ddlCreditCard.DataBind()
            ddlCreditCard.Items.Insert(0, "--Select One--")
            ddlCreditCard.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub BindData()
        Try
            Dim objCOA As New ChartOfAccounting
            Dim ds As DataSet
            With objCOA
                .TransChargeID = 0
                .CreditCardTypeId = ddlCreditCard.SelectedValue
                .DomainId = Session("DomainID")
                ds = .GetCOACreditCardCharge()
            End With

            If ds.Tables(0).Rows.Count > 0 Then
                ViewState("TransChargeID") = ds.Tables(0).Rows(0).Item("numTransChargeID")
                If Not IsDBNull(ds.Tables(0).Rows(0).Item("numAccountId")) Then
                    If Not ddlAPAccount.Items.FindByValue(ds.Tables(0).Rows(0).Item("numAccountId")) Is Nothing Then
                        ddlAPAccount.Items.FindByValue(ds.Tables(0).Rows(0).Item("numAccountId")).Selected = True
                    End If
                End If
                rblBareBy.SelectedValue = CCommon.ToInteger(ds.Tables(0).Rows(0).Item("tintBareBy"))
                txtCharge.Text = ds.Tables(0).Rows(0).Item("fltTransactionCharge")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub BindCOA()
        Try
            ddlAPAccount.Items.Clear()
            Dim objCOA As New ChartOfAccounting
            objCOA.DomainId = Session("DomainId")
            Dim dtAccountList As DataTable
            'Modified by chintan Reason: Bug id 1752
            If rblBareBy.SelectedValue = 0 Then 'Employer
                objCOA.AccountCode = "0104"
                dtAccountList = objCOA.GetParentCategory()
            ElseIf rblBareBy.SelectedValue = 1 Then ' Customer
                objCOA.AccountCode = "01020102"
                dtAccountList = objCOA.GetParentCategory()
            End If

            Dim item As ListItem
            For Each dr As DataRow In dtAccountList.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlAPAccount.Items.Add(item)
            Next
            ddlAPAccount.Items.Insert(0, New ListItem("--Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If CCommon.ToDouble(txtCharge.Text) >= 0 And CCommon.ToDouble(txtCharge.Text) < 100 Then
                Dim objCOA As New ChartOfAccounting
                With objCOA
                    .TransChargeID = lngTransChargeID
                    .AccountId = ddlAPAccount.SelectedValue
                    .TransactionCharge = txtCharge.Text
                    .CreditCardTypeId = ddlCreditCard.SelectedValue
                    .TransactionChargeBareBy = rblBareBy.SelectedValue
                    .DomainId = Session("DomainID")
                    lngTransChargeID = .ManageCOACreditCardCharge()
                End With
                BindCardType()
                ddlCreditCard.SelectedValue = objCOA.CreditCardTypeId
                litMessage.Text = "Saved Sucessfully."
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlCreditCard_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCreditCard.SelectedIndexChanged
        Try
            If ddlCreditCard.SelectedValue > 0 Then
                ddlAPAccount.ClearSelection()
                txtCharge.Text = ""
                lngTransChargeID = 0
                ViewState("TransChargeID") = 0
                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub rblBareBy_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles rblBareBy.SelectedIndexChanged
        Try
            BindCOA()
            ddlAPAccount.SelectedValue = 0
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class