﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Public Class frmBillPaymentBizDoc
    Inherits BACRMPage
    Dim objCommon As CCommon

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindData()
            End If

            If IsPostBack Then

            End If
            btnCancel.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindData()
        Try
            Dim objCommon As New CCommon()
            Dim dtBizDocStatus As DataTable = objCommon.GetMasterListItems(11, Session("DomainID"), 644, 0)

            FillBizDocStatus(ddlBizDocStatus15, dtBizDocStatus)
            FillBizDocStatus(ddlBizDocStatus16, dtBizDocStatus)
            Dim objAdmin As New CAdmin
            objAdmin.DomainID = Session("DomainID")
            objAdmin.byteMode = 0
            objAdmin.OppType = 2
            Dim dtRules As DataTable = objAdmin.ManageOpportunityAutomationRules()

            'Rule 15
            Dim foundRows As DataRow() = dtRules.Select("numRuleID=15")
            If foundRows.Length > 0 Then
                If Not ddlBizDocStatus15.Items.FindByValue(foundRows(0)("numBizDocStatus1")) Is Nothing Then
                    ddlBizDocStatus15.Items.FindByValue(foundRows(0)("numBizDocStatus1")).Selected = True
                End If
            End If

            'Rule 16
            foundRows = dtRules.Select("numRuleID=16")
            If foundRows.Length > 0 Then
                If Not ddlBizDocStatus16.Items.FindByValue(foundRows(0)("numBizDocStatus1")) Is Nothing Then
                    ddlBizDocStatus16.Items.FindByValue(foundRows(0)("numBizDocStatus1")).Selected = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub FillBizDocStatus(ByRef ddlBizDocStatus As DropDownList, ByVal dtBizDocStatus As DataTable)
        Try
            ddlBizDocStatus.DataSource = dtBizDocStatus
            ddlBizDocStatus.DataTextField = "vcData"
            ddlBizDocStatus.DataValueField = "numListItemID"
            ddlBizDocStatus.DataBind()
            ddlBizDocStatus.Items.Insert(0, "--Select One--")
            ddlBizDocStatus.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If Page.IsValid Then
            Try
                save()
                BindData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        If Page.IsValid Then
            Try
                save()
                Dim strScript As String = "<script language=JavaScript>self.close()</script>"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End If
    End Sub

    Private Sub save()
        Try
            Dim dtTable As New DataTable
            dtTable.TableName = "Table"

            CCommon.AddColumnsToDataTable(dtTable, "numRuleID,numBizDocStatus1,numBizDocStatus2,numOrderStatus")

            Dim dr As DataRow

            'Rule 15
            If ddlBizDocStatus15.SelectedValue > 0 Then
                dr = dtTable.NewRow
                dr("numRuleID") = 15
                dr("numBizDocStatus1") = ddlBizDocStatus15.SelectedValue
                dtTable.Rows.Add(dr)
            End If

            'Rule 16
            If ddlBizDocStatus16.SelectedValue > 0 Then
                dr = dtTable.NewRow
                dr("numRuleID") = 16
                dr("numBizDocStatus1") = ddlBizDocStatus16.SelectedValue
                dtTable.Rows.Add(dr)
            End If

            Dim ds As New DataSet
            ds.Tables.Add(dtTable)

            Dim objAdmin As New CAdmin
            objAdmin.DomainID = Session("DomainID")
            objAdmin.strItems = ds.GetXml
            objAdmin.byteMode = 1
            objAdmin.OppType = 2
            objAdmin.ManageOpportunityAutomationRules()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub



End Class