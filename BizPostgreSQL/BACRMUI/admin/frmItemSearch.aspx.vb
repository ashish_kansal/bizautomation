Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Namespace BACRM.UserInterface.Admin
    Partial Public Class frmItemSearch
        Inherits BACRMPage

        Dim objGenericAdvSearch As New FormGenericAdvSearch
        Public dtGenericFormConfig As DataTable
        Public dsGenericFormConfig As DataSet
        Dim objCommon As CCommon


        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                objCommon = New CCommon
                GetUserRightsForPage(9, 7)

                If Not IsPostBack Then
                    calFrom.SelectedDate = DateAdd(DateInterval.Month, -3, Now)
                    calTo.SelectedDate = Now
                End If

                GetFormFieldList()
                dtGenericFormConfig = dsGenericFormConfig.Tables(1)

                GenericFormControlsGeneration.FormID = 15                                        'set the form id to Advance Search
                GenericFormControlsGeneration.DomainID = Session("DomainID")                    'Set the domain id
                GenericFormControlsGeneration.UserCntID = Session("UserContactID")                        'Set the User id
                GenericFormControlsGeneration.AuthGroupId = Session("UserGroupID")              'Set the User Authentication Group Id

                callFuncForFormGenerationNonAOI(dsGenericFormConfig)                        'Calls function for form generation and display for non AOI fields
                litMessage.Text = GenericFormControlsGeneration.getJavascriptArray(dsGenericFormConfig.Tables(1)) 'Create teh javascript array and store
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub GetFormFieldList()
            Try
                If objGenericAdvSearch Is Nothing Then objGenericAdvSearch = New FormGenericAdvSearch

                objGenericAdvSearch.AuthenticationGroupID = Session("UserGroupID")
                objGenericAdvSearch.FormID = 15
                objGenericAdvSearch.DomainID = Session("DomainID")
                dsGenericFormConfig = objGenericAdvSearch.GetAdvancedSearchFieldList()

                dsGenericFormConfig.Tables(0).TableName = "SectionInfo"                           'give a name to the datatable
                dsGenericFormConfig.Tables(1).TableName = "AdvSearchFields"                           'give a name to the datatable

                dsGenericFormConfig.Tables(1).Columns.Add("vcNewFormFieldName", System.Type.GetType("System.String"), "[vcFormFieldName]") 'This columsn has to be an integer to tryign to manipupate the datatype as Compute(Max) does not work on string datatypes
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub callFuncForFormGenerationNonAOI(ByVal dsGenericFormConfig As DataSet)
            Try
                GenericFormControlsGeneration.boolAOIField = 0                                      'Set the AOI flag to non AOI
                GenericFormControlsGeneration.createDynamicFormControlsSectionWise(dsGenericFormConfig, plhControls, 15, Session("DomainID"), Session("UserContactID"))

                'Add Handler for Bill/Ship Country change event
                Dim dvConfig As DataView
                dvConfig = New DataView(dtGenericFormConfig)
                dvConfig.RowFilter = " vcDbColumnName like '%Country'"
                If dvConfig.Count > 0 Then

                    Dim i As Integer
                    For i = 0 To dvConfig.Count - 1
                        Dim strControlID As String
                        Dim drState() As DataRow

                        drState = dtGenericFormConfig.Select("vcDbColumnName='" & Replace(dvConfig(i).Item("vcDbColumnName"), "Country", "State") & "'")

                        If drState.Length > 0 Then
                            strControlID = drState(0)("numFormFieldID").ToString & "_" & drState(0)("vcDbColumnName").ToString.Trim.Replace(" ", "_")

                            If Not CType(tblMain.FindControl(strControlID), Telerik.Web.UI.RadComboBox) Is Nothing Then
                                Dim dl As DropDownList
                                dl = CType(tblMain.FindControl(dvConfig(i).Item("numFormFieldID").ToString & "_" & dvConfig(i).Item("vcDbColumnName")), DropDownList)
                                dl.AutoPostBack = True
                                AddHandler dl.SelectedIndexChanged, AddressOf FillStateDyn
                                If Not IsPostBack Then
                                    'Set default country
                                    If Not dl.Items.FindByValue(Session("DefCountry")) Is Nothing Then
                                        dl.Items.FindByValue(Session("DefCountry")).Selected = True
                                        If dl.SelectedIndex > 0 Then
                                            FillState(CType(tblMain.FindControl(strControlID), Telerik.Web.UI.RadComboBox), dl.SelectedItem.Value, Session("DomainID"))
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub FillStateDyn(ByVal sender As Object, ByVal e As EventArgs)
            Try
                Dim drState() As DataRow

                drState = dtGenericFormConfig.Select("vcDbColumnName='" & sender.ID.ToString.Split("_")(1).Replace("Country", "State") & "'")

                If drState.Length > 0 Then
                    Dim strControlID As String

                    strControlID = drState(0)("numFormFieldID").ToString & "_" & drState(0)("vcDbColumnName").ToString.Trim.Replace(" ", "_")

                    FillState(CType(tblMain.FindControl(strControlID), Telerik.Web.UI.RadComboBox), sender.SelectedItem.Value, Session("DomainID"))
                End If

                'Response.Write(sender.ID)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub
    End Class
End Namespace