'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace BACRM.UserInterface.Admin

    Partial Public Class frmEditRoutingRules

        '''<summary>
        '''tblRoutingRuleDetails control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tblRoutingRuleDetails As Global.System.Web.UI.HtmlControls.HtmlTable

        '''<summary>
        '''trCountry control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents trCountry As Global.System.Web.UI.HtmlControls.HtmlTableRow

        '''<summary>
        '''ddlCountry control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlCountry As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''trState control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents trState As Global.System.Web.UI.HtmlControls.HtmlTableRow

        '''<summary>
        '''lstAvailablefld control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lstAvailablefld As Global.System.Web.UI.WebControls.ListBox

        '''<summary>
        '''btnAdd control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnAdd As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnRemove control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnRemove As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''lstAddfld control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lstAddfld As Global.System.Web.UI.WebControls.ListBox

        '''<summary>
        '''trEqualTo control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents trEqualTo As Global.System.Web.UI.HtmlControls.HtmlTableRow

        '''<summary>
        '''trValues control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents trValues As Global.System.Web.UI.HtmlControls.HtmlTableRow

        '''<summary>
        '''trAOI control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents trAOI As Global.System.Web.UI.HtmlControls.HtmlTableRow

        '''<summary>
        '''txtAOI control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtAOI As Global.System.Web.UI.HtmlControls.HtmlInputHidden

        '''<summary>
        '''txtEmp control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtEmp As Global.System.Web.UI.HtmlControls.HtmlInputHidden

        '''<summary>
        '''txtState control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtState As Global.System.Web.UI.HtmlControls.HtmlInputHidden
    End Class
End Namespace
