Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin

    Partial Public Class frmTransfer
        Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    
                    
                    objCommon.sb_FillConEmpFromDBSel(ddlEmployee, Session("DomainID"), 0, Session("UserContactID"))
                    objCommon.sb_FillConEmpFromDBSel(ddlAssign, Session("DomainID"), 0, Session("UserContactID"))
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnTransfer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTransfer.Click
            Try
                Dim objAdmin As New FormGenericAdvSearch
                Dim mode As Integer = CCommon.ToInteger(GetQueryStringVal("mode"))
                Dim strContactIds As String = ""
                If mode = 1 Then
                    If Session("WhereCondition") <> "" Then
                        Dim ds As DataSet
                        Dim dtResults As DataTable
                        With objAdmin
                            .QueryWhereCondition = Session("WhereCondition")
                            .AreasOfInt = Session("AreasOfInt")
                            .DomainID = Session("DomainID")
                            .UserCntID = Session("UserContactID")
                            .AuthenticationGroupID = Session("UserGroupID")
                            .UserRightType = 3
                            .FormID = 1
                            .ViewID = 0                    'Set the View which is being configure
                            .columnSortOrder = "Desc"
                            .GetAll = True
                            ds = .AdvancedSearch()
                            dtResults = ds.Tables(0)
                        End With
                        Dim i As Integer
                        For i = 0 To dtResults.Rows.Count - 1
                            If i = 0 Then
                                strContactIds = IIf(IsDBNull(dtResults.Rows(i).Item("numContactId")), 0, dtResults.Rows(i).Item("numContactId"))
                            Else : strContactIds = strContactIds & "," & IIf(IsDBNull(dtResults.Rows(i).Item("numContactId")), 0, dtResults.Rows(i).Item("numContactId"))
                            End If
                        Next
                    End If
                Else : strContactIds = GetQueryStringVal( "CntIds")
                End If
                If strContactIds <> "" Then
                    Dim objUserAccess As New UserAccess
                    If ddlEmployee.SelectedIndex <> 0 Then
                        With objUserAccess
                            .UserCntID = ddlEmployee.SelectedValue
                            .DomainID = Session("DomainId")
                            .byteMode = 1
                            .xmlStr = strContactIds
                            .TransferOwnerAssigneAdv()
                        End With
                    End If

                    If ddlAssign.SelectedValue <> 0 Then
                        With objUserAccess
                            .UserCntID = ddlAssign.SelectedValue
                            .DomainID = Session("DomainId")
                            .byteMode = 2
                            .xmlStr = strContactIds
                            .TransferOwnerAssigneAdv()
                        End With
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace