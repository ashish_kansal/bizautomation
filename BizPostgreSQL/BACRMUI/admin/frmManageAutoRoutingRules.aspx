<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmManageAutoRoutingRules.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmManageAutoRoutingRules" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="ie" Namespace="Microsoft.Web.UI.WebControls" Assembly="Microsoft.Web.UI.WebControls" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Manage Auto Routing Rules</title>
    <script type="text/javascript" src="../javascript/AutoLeadsRoutingRules.js"></script>
    <style type="text/css">
         .hs {
            background-color: #ebebeb;
            font-weight:bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnAddNewRule" runat="server" OnClientClick="javascript:EditAutoRoutingRules(-1)" CssClass="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Add New Rule</asp:LinkButton>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Routing Rules&nbsp;<a href="#" onclick="return OpenHelpPopUp('admin/frmManageAutoRoutingRules.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table ID="tblRoutingRuleListLeads" CellPadding="0" CellSpacing="0" runat="server"
        Width="100%" GridLines="None" CssClass="table table-responsive table-bordered">
        <asp:TableHeaderRow Height="23" CssClass="hs">
            <asp:TableHeaderCell VerticalAlign="Middle" HorizontalAlign="Left" Width="49%">
						Rule Description
            </asp:TableHeaderCell>
            <asp:TableHeaderCell VerticalAlign="Middle" HorizontalAlign="Left" Width="40%">
						Priority
            </asp:TableHeaderCell>
        </asp:TableHeaderRow>
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top" ColumnSpan="2" style="padding:0px;">
                <asp:DataGrid ID="dgRoutingRulesLeadsDefault" runat="server" Width="100%" AutoGenerateColumns="False"
                    CssClass="table table-responsive table-bordered" DataKeyField="numRoutID" CellPadding="2" CellSpacing="2" ShowHeader="False" Style="margin-bottom:0px;">
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numRoutID" ItemStyle-Width="50%"></asp:BoundColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:RequiredFieldValidator ControlToValidate="txtDefaultRuleDesc" EnableClientScript="True"
                                    Display="None" ErrorMessage="Please enter the default rule name." runat="server"
                                    ID="Requiredfieldvalidator1"></asp:RequiredFieldValidator>
                                <asp:TextBox ID="txtDefaultRuleDesc" runat="server" CssClass="form-control"
                                    MaxLength="50"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn ItemStyle-Width="40%">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlDefaultRuleEmployee" runat="server" CssClass="form-control">
                                </asp:DropDownList>
                                &nbsp; (will receive leads that don't conform to any other rules)
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn ItemStyle-Width="5%">
                            <ItemTemplate>
                                <asp:Button ID="btnUpdt" runat="server" CssClass="btn btn-primary .btn-sm" Text="Ok" CommandName="Update"
                                    OnClick="btnUpdt_Command" CausesValidation="True"></asp:Button>&nbsp;
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <asp:ValidationSummary ID="valSummaryRules" runat="server" DisplayMode="SingleParagraph"
                    EnableClientScript="True" Visible="True" ShowMessageBox="True" ShowSummary="False"></asp:ValidationSummary>
                <asp:DataGrid ID="dgRoutingRulesLeads" runat="server" Width="100%" AutoGenerateColumns="False"
                    CssClass="table table-responsive table-bordered" DataKeyField="numRoutID" BorderWidth="1px"
                    CellPadding="2" CellSpacing="2" ShowHeader="False" Style="margin-bottom:0px;">
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numRoutID" ItemStyle-Width="50%"></asp:BoundColumn>
                        <asp:HyperLinkColumn SortExpression="vcRoutName" DataNavigateUrlField="numRoutID"
                            DataTextField="vcRoutName"></asp:HyperLinkColumn>
                        <asp:BoundColumn DataField="tintPriority" ItemStyle-Width="40%"></asp:BoundColumn>
                        <asp:TemplateColumn ItemStyle-Width="5%" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDeleteAction" runat="server" CssClass="btn btn-danger btn-xs"
                                    CommandName="Delete" Visible="True" OnClick="btnDeleteAction_Command" CausesValidation="False"><i class="fa fa-trash"></i></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
