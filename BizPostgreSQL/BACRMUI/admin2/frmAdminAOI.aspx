<%@ Register TagPrefix="menu1" TagName="webmenu" src="../include/webmenu.ascx" %>
<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAdminAOI.aspx.vb" Inherits="BACRM.UserInterface.Admin.frmAdminAOI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head    runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <script language="javascript" type="text/javascript" >
		function Back()
		{
			window.location.href="../admin/frmAdminSection.aspx"
			return false;
		}
        function DeleteRecord()
		{
			var str;
				str='Are you sure, you want to delete the selected record?'
			if(confirm(str))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	</script>
</head>
<body>

    <form id="form1" runat="server">
    	 <menu1:webmenu id="webmenu1" runat="server"></menu1:webmenu>
  <%-- <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server"></asp:ScriptManager>--%>
    <br />
<asp:updatepanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional" EnableViewState="true" >
                                    <ContentTemplate>					
			<table cellspacing="0" cellpadding="0" width="100%" border="0">
				<tr>
					<td valign="bottom">
						<table class="TabStyle">
							<tr>
								<td>&nbsp;&nbsp;&nbsp;Areas of Interest&nbsp;&nbsp;&nbsp;
								</td>
							</tr>
						</table>
					</td>
				
				</tr>
			</table>
			<asp:table id="Table2" CellPadding="0" CellSpacing="0" BorderWidth="1" Runat="server" Width="100%" CssClass="aspTable"
				BorderColor="black" GridLines="None" Height="300">
				<asp:TableRow>
					<asp:TableCell VerticalAlign="Top">
					<br />
						<table width="80%" align="center" >
							
							<tr>
								<td class="normal1" align="right">
									Area of Interest	&nbsp;							</td>
								<td>
									<asp:TextBox ID="txtInterest" Runat="server" Width="200" CssClass="signup"></asp:TextBox>
								&nbsp;
									<asp:Button ID="btnAdd" Runat="server" Text="Add Area of Interest" CssClass="button"></asp:Button>
								</td>
							</tr>
							<tr>
								<td>
									<br>
								</td>
							</tr>
						</table>
						<table width="100%" cellpadding="0" cellspacing="0">
							<tr>
								<td colspan="5">
								
                                    
									<asp:datagrid id="dgAreasInt" runat="server" CssClass="dg"  AutoGenerateColumns="False"
										AllowSorting="True" Width="100%">
										<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
										<ItemStyle CssClass="is"></ItemStyle>
										<HeaderStyle CssClass="hs"></HeaderStyle>
										<Columns>
										<asp:BoundColumn Visible="false" DataField="numAOIId"></asp:BoundColumn>
											<asp:TemplateColumn>
												<ItemTemplate>
													<asp:LinkButton runat="server" Text="Edit" CommandName="Edit" ID="lnkbtnEdt"></asp:LinkButton>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:LinkButton ID="lnkbtnUpdt" runat="server" Text="Update" CommandName="Update"></asp:LinkButton>&nbsp;
													<asp:LinkButton runat="server" Text="Cancel" CommandName="Cancel" ID="lnkbtnCncl"></asp:LinkButton>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="No">
												<ItemTemplate>
													<asp:Label ID="lblAOIID" Runat="server" Visible="False" Text= '<%# DataBinder.Eval(Container,"DataItem.numAOIId") %>'>
													</asp:Label>
													<%# Container.ItemIndex +1 %>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Areas of Interest">
												<ItemTemplate>
													<asp:Label ID="lblAOIName" Runat="server" Text= '<%# DataBinder.Eval(Container,"DataItem.vcAOIName") %>'>
													</asp:Label>
												</ItemTemplate>
												<EditItemTemplate>
													<asp:TextBox ID="txtEAOINAme" Runat="server" CssClass="signup" Text= '<%# DataBinder.Eval(Container,"DataItem.vcAOIName") %>'>
													</asp:TextBox>
												</EditItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderStyle-Width="10" ItemStyle-Width="10">
												<ItemTemplate>
													<asp:Button ID="btnDelete" Runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
													
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
									</asp:datagrid>
									
									</td>
							</tr>
						</table>
					</asp:TableCell>
				</asp:TableRow>
			</asp:table>
			<table width="100%">
				<tr>
					<td class="normal4" align="center">
						<asp:Literal ID="litMessage" Runat="server" EnableViewState="False" ></asp:Literal>
					</td>
				</tr>
			</table>
			</ContentTemplate>
			<Triggers>
			<asp:AsyncPostBackTrigger ControlID="btnAdd" />
			</Triggers>
		</asp:updatepanel>
    </form>
</body>
</html>
