﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCurrencyRates.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmCurrencyRates" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Currency</title>
    <style type="text/css">
        .VisibleNone {
            display: none;
        }
    </style>
    
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            FillDropDowns();
           
        });

        function FillDropDowns() {
            
            $('#<%=dgCurrency.ClientID%> tr').each(function () {
                $(this).find('#ddlCountry').each(function () {
                    $(this).html($('#ddlCountry1').html())
                });
            });

            $('#<%=dgCurrency.ClientID%> tr').each(function () {
                var $ctl = $(this).find('#ddlCountry');
                var lblCountrySelected = $(this).find('#lblCountrySelected').text();
                if ($ctl != null) {
                    if (lblCountrySelected > 0) {
                        $(this).find('#ddlCountry option').each(function () {
                            if ($(this).val() == lblCountrySelected) {
                               // alert("lblCountrySelected : " + lblCountrySelected);
                                $(this).attr("selected", true);
                            }
                        });
                    }
                }
             });
        }

        function GetSelectedCountyList()
        {
            var hdnValue = '';
            $('#<%=dgCurrency.ClientID%> tr').each(function () {
                var isChecked = $(this).find('#chkEnable').is(':checked');
                if (isChecked == true) {
                    var lblnumCurrencyID = $(this).find('#lblnumCurrencyID').text();
                    var $ctl = $(this).find('#ddlCountry');
                    if ($ctl != null) {
                        hdnValue += lblnumCurrencyID + ',' + $ctl.val() + '|';
                    }
                    
                }
            });
            $("#hdnFldSelectedCountries").val(hdnValue);
        }

        </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Update Exchange Rate">
            </asp:Button>
            <asp:Button ID="btnSave" runat="server" CssClass="button" OnClientClick="GetSelectedCountyList();" Text="Save" Width="50">
            </asp:Button>
            <asp:Button ID="btnClose" runat="server" OnClientClick="Close()" CssClass="button"
                Text="Close"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Foreign Currencies
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="Table2" CellPadding="2" CellSpacing="2" BorderWidth="1" runat="server"
        Width="600px" CssClass="aspTable" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell ID="tcActive" runat="server">
                <br />
                <table>
                    <tr>
                        <td>
                            <asp:HiddenField runat="server" ID="hdnFldSelectedCountries" />
                            <label for="chkShowActive">
                                Show only active currencies</label>
                        </td>
                        <td>
                            <asp:CheckBox runat="server" ID="chkShowActive" AutoPostBack="true" />
                        </td>
                        <td><asp:DropDownList ID="ddlCountry1" CssClass="VisibleNone" runat="server">
                                </asp:DropDownList></td>
                    </tr>
                    <tr></tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgCurrency" AutoGenerateColumns="false" CssClass="dg" runat="server">
                    <AlternatingItemStyle CssClass="ais" />
                    <ItemStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <Columns>
                        
                        <asp:BoundColumn DataField="numCurrencyID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="chrCurrency" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="bitEnabled" Visible="false"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Currency Desc" HeaderStyle-Width="100px">
                            <ItemTemplate>
                                <asp:TextBox ID="txtCurrencyDesc" CssClass="signup" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcCurrencyDesc") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Currency">
                            <ItemTemplate>
                                <asp:TextBox ID="txtCurrency" CssClass="signup" Width="40" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.varCurrSymbol") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Exchange Rate(USD as Base)" HeaderStyle-Width="200px">
                            <ItemTemplate>
                                <asp:TextBox ID="txtFXRate" CssClass="signup" Width="50" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.fltExchangeRate") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Enabled">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkEnable" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Country">
                            <ItemTemplate>
                                <asp:Label runat="server" CssClass="VisibleNone" ID ="lblnumCurrencyID" Text='<%# DataBinder.Eval(Container, "DataItem.numCurrencyID")%>'></asp:Label>
                                 <asp:Label runat="server" CssClass="VisibleNone" ID ="lblCountrySelected" Text=""></asp:Label>
                                <asp:DropDownList ID="ddlCountry" runat="server" >
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                <br />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
