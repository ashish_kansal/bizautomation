﻿Imports BACRM.BusinessLogic.Common
Partial Public Class frmDropdownRelationship
    Inherits BACRMPage
    Dim FieldRelID As Long
    Dim objCommon As CCommon

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            FieldRelID = CCommon.ToLong(GetQueryStringVal("FieldRelID"))
            If Not IsPostBack Then
                

                BindModule()
                ddlModule_SelectedIndexChanged(sender, e)
                'If FieldRelID = 0 Then
                '    objCommon.LoadListMaster(ddlPrimaryDropDown, Session("DomainID"))
                '    objCommon.LoadListMaster(ddlSecondaryDropDown, Session("DomainID"))
                '    tblData.Visible = False
                '    lblCRel.Text = "Create Dropdown Relationship"
                'Else
                '    btnCreate.Visible = False
                '    tblDropdownSel.Visible = False
                '    Dim dtFieldRelInfo As DataTable
                '    objCommon.Mode = 2 ' To get Header Table information
                '    objCommon.FieldRelID = FieldRelID
                '    dtFieldRelInfo = objCommon.GetFieldRelationships.Tables(0)
                '    lblCRel.Text = dtFieldRelInfo.Rows(0).Item("RelationshipName")
                '    lblPrimaryDropDown.Text = dtFieldRelInfo.Rows(0).Item("Item1")
                '    lblSecondaryDropdown.Text = dtFieldRelInfo.Rows(0).Item("Item2")
                '    objCommon.sb_FillComboFromDBwithSel(ddlPrimaryDropdownItems, dtFieldRelInfo.Rows(0).Item("numPrimaryListID"), Session("DomainID"))
                '    objCommon.sb_FillComboFromDBwithSel(ddlSecondaryDropdownItems, dtFieldRelInfo.Rows(0).Item("numSecondaryListID"), Session("DomainID"))
                'End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub

    Private Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        Try
            objCommon = New CCommon
            objCommon.Mode = 1  '' To Insert data to Header Table
            objCommon.DomainID = Session("DomainID")
            objCommon.PrimaryListID = ddlPrimaryDropDown.SelectedValue
            objCommon.SecondaryListID = ddlSecondaryDropDown.SelectedValue
            FieldRelID = objCommon.ManageFieldRelationships
            If FieldRelID = 0 Then
                litMessage.Text = "Selected relationship already exist!"
                Exit Sub
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
        Response.Redirect("../admin2/frmDropdownRelationship.aspx?FieldRelID=" & FieldRelID)
    End Sub
    Private Sub BindModule()
        Try
            objCommon = New CCommon
            Dim dt As DataTable = objCommon.GetListModule()
            dt.DefaultView.RowFilter = "numModuleID<>8"
            ddlModule.DataSource = dt.DefaultView
            ddlModule.DataTextField = "vcModuleName"
            ddlModule.DataValueField = "numModuleID"
            ddlModule.DataBind()
            'ddlModule.Items.Insert(0, "---Select---")
        Catch ex As Exception
            Throw ex
        End Try
        
    End Sub
  
    Private Sub ddlPrimaryDropdownItems_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPrimaryDropdownItems.SelectedIndexChanged

        Try
            objCommon = New CCommon
            BindItems()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


    Sub BindItems()
        Try
            Dim dtFieldRelationshipItems As DataTable
            objCommon.Mode = 2 ' To get Header Table information
            objCommon.FieldRelID = FieldRelID
            objCommon.PrimaryListItemID = ddlPrimaryDropdownItems.SelectedValue
            dtFieldRelationshipItems = objCommon.GetFieldRelationships.Tables(1)
            dgListItems.DataSource = dtFieldRelationshipItems
            dgListItems.DataBind()
            dgListItems.Columns(2).HeaderText = lblPrimaryDropDown.Text
            dgListItems.Columns(3).HeaderText = lblSecondaryDropdown.Text

        Catch ex As Exception
            Throw ex
        End Try
       
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        objCommon = New CCommon
        objCommon.Mode = 2 ' Insert into Detail Table
        objCommon.FieldRelID = FieldRelID
        objCommon.PrimaryListItemID = ddlPrimaryDropdownItems.SelectedValue
        objCommon.SecondaryListItemID = ddlSecondaryDropdownItems.SelectedValue
        If objCommon.ManageFieldRelationships() > 0 Then

            BindItems()
        Else
            litMessage.Text = "Duplicate Record"
        End If

    End Sub

    Private Sub dgListItems_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgListItems.ItemCommand
        If e.CommandName = "Delete" Then
            objCommon = New CCommon
            objCommon.Mode = 3 ' Delete only from detail table
            objCommon.FieldRelDTLID = e.Item.Cells(1).Text
            objCommon.ManageFieldRelationships()
            BindItems()
        End If
    End Sub

    Private Sub ddlModule_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlModule.SelectedIndexChanged
        If ddlModule.SelectedValue >= 0 Then
            objCommon = New CCommon
            If FieldRelID = 0 Then
                objCommon.LoadListMaster(ddlPrimaryDropDown, Session("DomainID"), CCommon.ToLong(ddlModule.SelectedValue), True)
                objCommon.LoadListMaster(ddlSecondaryDropDown, Session("DomainID"), CCommon.ToLong(ddlModule.SelectedValue), True)
                tblData.Visible = False
                lblCRel.Text = "Create Dropdown Relationship"
            Else
                btnCreate.Visible = False
                tblDropdownSel.Visible = False
                Dim dtFieldRelInfo As DataTable
                objCommon.Mode = 2 ' To get Header Table information
                objCommon.FieldRelID = FieldRelID
                dtFieldRelInfo = objCommon.GetFieldRelationships.Tables(0)
                lblCRel.Text = dtFieldRelInfo.Rows(0).Item("RelationshipName")
                lblPrimaryDropDown.Text = dtFieldRelInfo.Rows(0).Item("Item1")
                lblSecondaryDropdown.Text = dtFieldRelInfo.Rows(0).Item("Item2")
                objCommon.sb_FillComboFromDBwithSel(ddlPrimaryDropdownItems, dtFieldRelInfo.Rows(0).Item("numPrimaryListID"), Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(ddlSecondaryDropdownItems, dtFieldRelInfo.Rows(0).Item("numSecondaryListID"), Session("DomainID"))
            End If
        End If
    End Sub
End Class