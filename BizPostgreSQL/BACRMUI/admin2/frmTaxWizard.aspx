﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmTaxWizard.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmTaxWizard" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Tax Wizard</title>
    <script language="javascript" type="text/javascript">
        function Close() {
            window.close();
        }
        function Save() {

            if (document.form1.txtTaxName.value == "") {
                alert("Enter Tax Type");
                document.form1.txtTaxName.focus();
                return false;
            }
            if (document.form1.ddlTaxAccount.value == 0) {
                alert("Please Select Tax Account");
                document.form1.ddlTaxAccount.focus();
                return false;
            }
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="50">
                        </asp:Button>&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Tax Type
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="Table2" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="500px" CssClass="aspTable" BorderColor="black" GridLines="None" Height="300">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <table width="100%">
                    <tr>
                        <td class="normal1" align="right">
                            Tax Type
                        </td>
                        <td>
                            <asp:TextBox ID="txtTaxName" runat="server" CssClass="signup" Width="195"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="normal1">
                            Tax Account&nbsp;
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlTaxAccount" runat="server" CssClass="signup" Width="200">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td align="left">
                            <asp:Button ID="btnCreate" runat="server" Text="Create Tax Item" CssClass="button"
                                OnClientClick="return Save();" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                            <asp:DataGrid ID="dgTaxItems" AutoGenerateColumns="false" CssClass="dg" runat="server" Width="100%">
                                <AlternatingItemStyle CssClass="ais" />
                                <ItemStyle CssClass="is" />
                                <HeaderStyle CssClass="hs" />
                                <Columns>
                                    <asp:BoundColumn DataField="" Visible="false"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Tax Type" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTaxItemID" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.numTaxItemID") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-Width="100" ItemStyle-Width="100">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" Text="Edit" CommandName="Edit" ID="lnkbtnEdt"></asp:LinkButton>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:LinkButton ID="lnkbtnUpdt" runat="server" Text="Update" CommandName="Update"></asp:LinkButton>&nbsp;
                                            <asp:LinkButton runat="server" Text="Cancel" CommandName="Cancel" ID="lnkbtnCncl"></asp:LinkButton>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Tax Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTaxName" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcTaxName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:TextBox ID="txtTaxName" CssClass="signup" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcTaxName") %>'></asp:TextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Account Name">
                                        <ItemTemplate>
                                            <%# Eval("vcAccountName") %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                        </EditItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete">
                                            </asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
