﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmDropDownRelationshipList.aspx.vb"
    Inherits=".frmDropDownRelationshipList" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script language="javascript" type="text/javascript">
        function Close() {
            window.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnNew" Text="Add New Dropdown Relationship" CssClass="button" runat="server">
            </asp:Button>
            <asp:Button ID="btnClose" Text="Close" OnClientClick="Close()" CssClass="button"
                runat="server" Width="50"></asp:Button>
        </div>
    </div>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Dropdown Relationship
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:DataGrid ID="dgDropdownRelationship" runat="server" CssClass="dg" 
        AutoGenerateColumns="False" AllowSorting="True" Width="300px">
        <HeaderStyle CssClass="hs" />
        <Columns>
            <asp:TemplateColumn HeaderText="No" Visible="false">
                <ItemTemplate>
                    <%# Container.ItemIndex +1 %>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="numFieldRelID" Visible="false"></asp:BoundColumn>
            <asp:ButtonColumn CommandName="Name" HeaderText="Relationship Name" DataTextField="RelationshipName">
            </asp:ButtonColumn>
            <asp:TemplateColumn HeaderStyle-Width="10" ItemStyle-Width="10">
                <HeaderTemplate>
                    <asp:Button ID="btnHdelete" runat="server" CssClass="button Delete" Text="X"></asp:Button>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete">
                    </asp:Button>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
    <asp:Button ID="Button1" runat="server" Style="display: none" />
</asp:Content>
