﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin

    Partial Public Class frmCreateLeadBoxURL
        Inherits BACRMPage

        Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            Try
                CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            
        End Sub



        Public Function FillContact(ByVal ddlCombo As DropDownList)
            Try
                Dim fillCombo As New COpportunities
                With fillCombo
                    .DivisionID = radCmbCompany.SelectedValue
                    ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                    ddlCombo.DataTextField = "Name"
                    ddlCombo.DataValueField = "numcontactId"
                    ddlCombo.DataBind()
                End With
                ddlCombo.Items.Insert(0, New ListItem("---Select One---", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
            Try
                If radCmbCompany.SelectedValue <> "" Then
                    FillContact(ddlContacts)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnOpenForm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOpenForm.Click
            If radRecordOwner.Checked = True Then
                If ddlContacts.SelectedIndex > 0 Then
                    Response.Redirect(ConfigurationManager.AppSettings("LeadBoxForm") & "?D=" & Session("DomainID") & "&RecID=" & ddlContacts.SelectedItem.Value)
                End If
            ElseIf radRoutingRules.Checked = True Then
                Response.Redirect(ConfigurationManager.AppSettings("LeadBoxForm") & "?D=" & Session("DomainID"))
            End If
        End Sub
    End Class

End Namespace