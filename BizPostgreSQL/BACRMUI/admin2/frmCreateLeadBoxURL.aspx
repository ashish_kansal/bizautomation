﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCreateLeadBoxURL.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmCreateLeadBoxURL" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table cellpadding="2" cellspacing="2">
                <tr>
                    <td align="center">
                        <asp:Button ID="btnOpenForm" CssClass="button" runat="server" Text="Open LeadBox Form" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    LeadBox From HTML
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <table width="600px" cellpadding="2" cellspacing="2">
        <tr>
            <td class="normal1">
                <asp:RadioButton ID="radRoutingRules" runat="server" GroupName="rad" Text="Determine Record Owner using Routing Rules" />
                <br />
                <asp:RadioButton ID="radRecordOwner" GroupName="rad" runat="server" Text="Create WebLead for the Contact Selected" />
            </td>
        </tr>
        <tr>
            <td class="normal1">Company&nbsp;
                <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                    OnClientItemsRequested="OnClientItemsRequestedOrganization"
                    ClientIDMode="Static"
                    ShowMoreResultsBox="true"
                    Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True">
                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                </telerik:RadComboBox>
                <%--<rad:RadComboBox ID="radCmbCompany" ExternalCallBackPage="../include/LoadCompany.aspx"
                    Width="195px" DropDownWidth="200px" Skin="WindowsXP" runat="server" AutoPostBack="True"
                    AllowCustomText="True" EnableLoadOnDemand="True" SkinsPath="~/RadControls/ComboBox/Skins">
                </rad:RadComboBox>--%>
                &nbsp;Contact&nbsp;
                <asp:DropDownList ID="ddlContacts" runat="server" Width="200px" CssClass="signup">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>
