﻿Imports BACRM.BusinessLogic.Common
Partial Public Class frmDropDownRelationshipList
    Inherits BACRMPage
    
   
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            GetUserRightsForPage(13, 1)

            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AS")
            ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnNew.Visible = False
            End If

            If Not IsPostBack Then
                
                
                objCommon.DomainID = Session("DomainID")
                objCommon.Mode = 1  ''To get all the relationships
                dgDropdownRelationship.DataSource = objCommon.GetFieldRelationships
                dgDropdownRelationship.DataBind()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgDropdownRelationship_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgDropdownRelationship.ItemCommand
        If e.CommandName = "Name" Then
            Response.Redirect("../admin2/frmDropdownRelationship.aspx?FieldRelID=" & e.Item.Cells(1).Text)
        ElseIf e.CommandName = "Delete" Then
            
            objCommon.DomainID = Session("DomainID")
            objCommon.FieldRelID = e.Item.Cells(1).Text
            objCommon.Mode = 4
            objCommon.ManageFieldRelationships()


            objCommon.Mode = 1  ''To get all the relationships
            dgDropdownRelationship.DataSource = objCommon.GetFieldRelationships
            dgDropdownRelationship.DataBind()
        End If
    End Sub

    Private Sub btnNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNew.Click
        Response.Redirect("../admin2/frmDropdownRelationship.aspx?FieldRelID=0")
    End Sub
End Class