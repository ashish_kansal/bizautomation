﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace BACRM.UserInterface.Admin

    Partial Public Class frmCurrencyRates

        '''<summary>
        '''btnUpdate control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnUpdate As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnSave control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnSave As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnClose control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnClose As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''Table2 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents Table2 As Global.System.Web.UI.WebControls.Table

        '''<summary>
        '''tcActive control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tcActive As Global.System.Web.UI.WebControls.TableCell

        '''<summary>
        '''hdnFldSelectedCountries control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdnFldSelectedCountries As Global.System.Web.UI.WebControls.HiddenField

        '''<summary>
        '''chkShowActive control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents chkShowActive As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''ddlCountry1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlCountry1 As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''dgCurrency control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents dgCurrency As Global.System.Web.UI.WebControls.DataGrid
    End Class
End Namespace
