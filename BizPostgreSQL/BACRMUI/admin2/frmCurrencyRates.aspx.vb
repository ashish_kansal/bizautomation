﻿'Created BY Anoop Jayaraj
Imports WSExchangeRate
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin
    Partial Public Class frmCurrencyRates
        Inherits BACRMPage
        
       


        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                GetUserRightsForPage(13, 1)

                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AS")
                ElseIf m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                    btnSave.Visible = False
                    btnUpdate.Visible = False
                End If

                If IsPostBack Then
                    ClientScript.RegisterHiddenField("hdnPostBack", "1")

                End If




                'BindDropDown()
                If Not IsPostBack Then

                    'If DirectCast(Session("MultiCurrency"), Boolean) = True Then
                    'tcActive.Visible = True
                    PersistTable.Load()
                    If PersistTable.Count = 0 Then chkShowActive.Checked = True
                    chkShowActive.Checked = CCommon.ToBool(PersistTable(chkShowActive.ID))
                    'Else
                    'tcActive.Visible = False
                    'chkShowActive.Checked = False
                    'End If
                    BindDropDown()
                    BindGrid(chkShowActive.Checked)
                    'Dim dtTable As DataTable
                    'Dim objCurrency As New CurrencyRates
                    'objCurrency.DomainID = Session("DomainID")
                    'objCurrency.GetAll = 1
                    'dtTable = objCurrency.GetCurrencyWithRates()
                    'dgCurrency.DataSource = dtTable
                    'dgCurrency.DataBind()
                    btnSave.Attributes.Add("click", "GetSelectedCountyList();")

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try


        End Sub

        Private Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
            Try

                'Dim objExchangeRate As New WSExchangeRate.CurrencyConvertor
                'For Each dgGridItem As DataGridItem In dgCurrency.Items
                '    If CType(dgGridItem.FindControl("chkEnable"), CheckBox).Checked = True Then
                '        If dgGridItem.Cells(1).Text = "USD" Then
                '            CType(dgGridItem.FindControl("txtFXRate"), TextBox).Text = 1
                '        Else
                '            CType(dgGridItem.FindControl("txtFXRate"), TextBox).Text = objExchangeRate.ConversionRate(CType(System.Enum.Parse(GetType(Currency), dgGridItem.Cells(1).Text), Currency), Currency.USD)
                '        End If
                '    End If

                'Next

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try

                Dim SelectedCountries As String
                Dim CurrencyId As Long
                Dim CountryId As Long
                SelectedCountries = hdnFldSelectedCountries.Value

                Dim rowValue As String()
                rowValue = SelectedCountries.Split("|")


                Dim objCurrency As New CurrencyRates
                For Each dgGridItem As DataGridItem In dgCurrency.Items
                    Dim lblCountrySelected As Label = CType(dgGridItem.FindControl("lblCountrySelected"), Label)
                    If CType(dgGridItem.FindControl("chkEnable"), CheckBox).Checked = True Then


                        objCurrency.CurrencyID = dgGridItem.Cells(0).Text
                        objCurrency.CurrencyDesc = CType(dgGridItem.FindControl("txtCurrencyDesc"), TextBox).Text
                        objCurrency.CurrencySymbol = CType(dgGridItem.FindControl("txtCurrency"), TextBox).Text
                        objCurrency.ExchangeRate = IIf(IsNumeric(CType(dgGridItem.FindControl("txtFXRate"), TextBox).Text) = False, 1, CType(dgGridItem.FindControl("txtFXRate"), TextBox).Text)
                        objCurrency.Enabled = True

                        For Each ColValue As String In rowValue
                            Dim d As String() = ColValue.Split(",")
                            If d.Length > 1 Then
                                CurrencyId = CCommon.ToLong(d(0))
                                CountryId = CCommon.ToLong(d(1))
                                If objCurrency.CurrencyID = CurrencyId Then
                                    objCurrency.CountryID = CountryId
                                    lblCountrySelected.Text = CCommon.ToString(CountryId)

                                End If
                            End If
                        Next

                        'objCurrency.CountryID = CCommon.ToLong(CType(dgGridItem.FindControl("ddlCountry"), DropDownList).SelectedValue)
                        objCurrency.UpdateCurrencyRate()
                    End If

                Next
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Dim tempdtTable As New DataTable

        Private Sub dgCurrency_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCurrency.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    If e.Item.Cells(2).Text = "True" Then
                        CType(e.Item.FindControl("chkEnable"), CheckBox).Checked = True
                    End If

                    'Dim ddlCountry As New DropDownList
                    'ddlCountry = e.Item.FindControl("ddlCountry")

                    'If Cache("tempCountry") Is Nothing Then
                    '    tempdtTable = objCommon.GetMasterListItems(40, Session("DomainID"))
                    '    Cache.Add("tempCountry", tempdtTable, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, 0, 60), System.Web.Caching.CacheItemPriority.[Default], Nothing)
                    'Else
                    '    tempdtTable = DirectCast(Cache("tempCountry"), DataTable)
                    'End If

                    'ddlCountry.DataSource = tempdtTable
                    'ddlCountry.DataTextField = "vcData"
                    'ddlCountry.DataValueField = "numListItemID"
                    'ddlCountry.DataBind()
                    'ddlCountry.Items.Insert(0, "--Select One--")
                    'ddlCountry.Items.FindByText("--Select One--").Value = "0"
                    'Dim CountryId As Integer = 0
                    Dim lblCountrySelected As Label = CType(e.Item.FindControl("lblCountrySelected"), Label)
                    'CountryId = CCommon.ToInteger(DataBinder.Eval(e.Item.DataItem, "numCountryId"))
                    'lblCountrySelected.Text = CCommon.ToString(CountryId)
                    lblCountrySelected.Text = CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "numCountryId"))

                    'objCommon.sb_FillComboFromDBwithSel(ddlCountry, 40, Session("DomainID"))
                    ' lblCountrySelected.Text = CCommon.ToString(CCommon.ToInteger(())

                    'If ddlCountry.Items.FindByValue(DataBinder.Eval(e.Item.DataItem, "numCountryId")) IsNot Nothing Then
                    '    ddlCountry.Items.FindByValue(DataBinder.Eval(e.Item.DataItem, "numCountryId")).Selected = True
                    'End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub BindGrid(Optional ByVal IsActive As Boolean = False)
            Try
                Dim dtTable As DataTable
                Dim objCurrency As New CurrencyRates
                objCurrency.DomainID = Session("DomainID")
                objCurrency.GetAll = 1
                dtTable = objCurrency.GetCurrencyWithRates()
                dgCurrency.DataSource = dtTable
                dgCurrency.DataBind()

                PersistTable.Clear()
                PersistTable.Add(chkShowActive.ID, chkShowActive.Checked)
                PersistTable.Save()

                If IsActive = True Then
                    For Each dgRow As DataGridItem In dgCurrency.Items

                        If chkShowActive.Checked = True Then
                            If DirectCast(dgRow.FindControl("chkEnable"), CheckBox).Checked = True Then
                                dgRow.Visible = True
                            Else
                                dgRow.Visible = False
                            End If
                        End If

                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub chkShowActive_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkShowActive.CheckedChanged
            Try
                BindGrid(chkShowActive.Checked)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub BindDropDown()
            Try
                If Cache("tempCountry") Is Nothing Then
                    tempdtTable = objCommon.GetMasterListItems(40, Session("DomainID"))
                    Cache.Add("tempCountry", tempdtTable, Nothing, System.Web.Caching.Cache.NoAbsoluteExpiration, New TimeSpan(0, 0, 60), System.Web.Caching.CacheItemPriority.[Default], Nothing)
                Else
                    tempdtTable = DirectCast(Cache("tempCountry"), DataTable)
                End If

                ddlCountry1.DataSource = tempdtTable
                ddlCountry1.DataTextField = "vcData"
                ddlCountry1.DataValueField = "numListItemID"
                ddlCountry1.DataBind()
                ddlCountry1.Items.Insert(0, "--Select One--")
                ddlCountry1.Items.FindByText("--Select One--").Value = "0"
            Catch ex As Exception
                Throw ex
            End Try

        End Sub

    End Class

End Namespace