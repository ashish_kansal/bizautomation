﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmDropdownRelationship.aspx.vb"
    Inherits=".frmDropdownRelationship" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script language="javascript" type="text/javascript">
        function Close() {
            window.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnCreate" Text="Create Relationship" CssClass="button" runat="server">
            </asp:Button>
            <asp:Button ID="btnClose" Text="Close" OnClientClick="Close()" CssClass="button"
                runat="server" Width="50"></asp:Button>
        </div>
    </div>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" EnableViewState="false" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblCRel" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="Table2" runat="server" Height="300" GridLines="None" BorderColor="black"
        Width="400px" CssClass="aspTable" BorderWidth="1" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br>
                <table align="center" id="tblDropdownSel" runat="server">
                    <tr>
                        <td class="normal1" align="right">
                            List Module
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlModule" runat="server" CssClass="signup" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Primary Dropdown
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlPrimaryDropDown" runat="server" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Secondary Dropdown
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlSecondaryDropDown" runat="server" CssClass="signup">
                            </asp:DropDownList>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                        </td>
                    </tr>
                </table>
                <table width="100%" cellpadding="0" cellspacing="0" runat="server" id="tblData">
                    <tr>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblPrimaryDropDown" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlPrimaryDropdownItems" AutoPostBack="True" runat="server"
                                CssClass="signup">
                            </asp:DropDownList>
                        </td>
                        <td class="normal1" align="right">
                            <asp:Label ID="lblSecondaryDropdown" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlSecondaryDropdownItems" runat="server" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btnAdd" Text="Add" CssClass="button" runat="server" Width="50"></asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <asp:DataGrid ID="dgListItems" runat="server" CssClass="dg"  AutoGenerateColumns="False"
                                AllowSorting="True" Width="100%">
                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                <ItemStyle CssClass="is"></ItemStyle>
                                <HeaderStyle CssClass="hs"></HeaderStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderText="No" Visible="false">
                                        <ItemTemplate>
                                            <%# Container.ItemIndex +1 %>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="numFieldRelDTLID" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="PrimaryListItem"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="SecondaryListItem"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderStyle-Width="10" ItemStyle-Width="10">
                                        <HeaderTemplate>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete">
                                            </asp:Button>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
