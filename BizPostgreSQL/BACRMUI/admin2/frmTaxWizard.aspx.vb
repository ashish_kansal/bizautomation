﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Namespace BACRM.UserInterface.Admin

    Partial Public Class frmTaxWizard
        Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    
                    BindGrid()
                    BindCOA()
                    btnClose.Attributes.Add("onclick", "return Close()")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Private Sub BindGrid()
            Try
                Dim objTaxDetails As New TaxDetails
                objTaxDetails.DomainId = Session("DomainID")
                dgTaxItems.DataSource = objTaxDetails.GetTaxItems
                dgTaxItems.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub BindCOA()
            Try
                Dim objCOA As New ChartOfAccounting
                objCOA.DomainId = Session("DomainId")
                objCOA.AccountCode = "0102"
                ddlTaxAccount.DataSource = objCOA.GetParentCategory()
                ddlTaxAccount.DataTextField = "vcAccountName1"
                ddlTaxAccount.DataValueField = "numAccountId"
                ddlTaxAccount.DataBind()
                ddlTaxAccount.Items.Insert(0, "--Select One--")
                ddlTaxAccount.Items.FindByText("--Select One--").Value = "0"
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub btnCreate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreate.Click
            Try
                Dim objTaxDetails As New TaxDetails
                objTaxDetails.TaxName = txtTaxName.Text
                objTaxDetails.DomainId = Session("DomainID")
                objTaxDetails.AccountId = ddlTaxAccount.SelectedValue
                objTaxDetails.ManageTaxItems()
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try

        End Sub

        Private Sub dgTaxItems_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgTaxItems.CancelCommand
            Try
                dgTaxItems.EditItemIndex = -1
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgTaxItems_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgTaxItems.EditCommand
            Try
                Dim objTaxDetails As New TaxDetails
                objTaxDetails.DomainId = Session("DomainID")
                dgTaxItems.EditItemIndex = e.Item.ItemIndex
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub


        Private Sub dgTaxItems_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgTaxItems.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    Dim objTaxDetails As New TaxDetails
                    objTaxDetails.TaxItemID = CType(e.Item.FindControl("lblTaxItemID"), Label).Text
                    objTaxDetails.DomainId = Session("DomainID")
                    objTaxDetails.DeleteTaxItems()
                    BindGrid()
                ElseIf e.CommandName = "Update" Then
                    Dim objTaxDetails As New TaxDetails
                    objTaxDetails.TaxItemID = CType(e.Item.FindControl("lblTaxItemID"), Label).Text
                    objTaxDetails.TaxName = CType(e.Item.FindControl("txtTaxName"), TextBox).Text.Trim
                    objTaxDetails.AccountId = ddlTaxAccount.SelectedValue
                    objTaxDetails.DomainId = Session("DomainID")
                    objTaxDetails.ManageTaxItems()
                    dgTaxItems.EditItemIndex = -1
                    BindGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try

        End Sub

        Private Sub dgTaxItems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgTaxItems.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim btnDelete As Button
                    btnDelete = e.Item.FindControl("btnDelete")
                    btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class

End Namespace