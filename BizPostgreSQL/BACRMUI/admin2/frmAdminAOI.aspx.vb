''created by anoop jayaraj
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin
    Partial Public Class frmAdminAOI
        Inherits BACRMPage
        Dim m_aryRightsForAdd() As Integer

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    
                    GetUserRightsForPage(13, 4)
                    If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then btnAdd.Visible = False

                    BindGrid()
                End If
                ' btnClose.Attributes.Add("onclick", "return Back()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindGrid()
            Try
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                dgAreasInt.DataSource = objAdmin.GetAOIs
                dgAreasInt.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Try
                Dim objAdmin As New CAdmin
                objAdmin.AOIName = txtInterest.Text
                objAdmin.DomainID = Session("DomainID")
                objAdmin.UserCntID = Session("UserContactID")
                objAdmin.ManageAOIs()
                Call BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgAreasInt_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAreasInt.EditCommand
            Try
                dgAreasInt.EditItemIndex = e.Item.ItemIndex
                Call BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgAreasInt_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAreasInt.ItemCommand
            Try
                If e.CommandName = "Cancel" Then
                    dgAreasInt.EditItemIndex = e.Item.ItemIndex
                    dgAreasInt.EditItemIndex = -1
                    Call BindGrid()
                End If

                If e.CommandName = "Delete" Then
                    Dim objAdmin As New CAdmin
                    objAdmin.AOIID = e.Item.Cells(0).Text
                    objAdmin.bitDeleted = 1
                    If Not objAdmin.ManageAOIs() = True Then litMessage.Text = "Depenedent Record exists.Cannot be deleted."
                    Call BindGrid()
                End If
                If e.CommandName = "Update" Then
                    Dim objAdmin As New CAdmin
                    objAdmin.DomainID = Session("DomainID")
                    objAdmin.UserCntID = Session("UserContactID")
                    objAdmin.AOIName = CType(e.Item.FindControl("txtEAOINAme"), TextBox).Text
                    objAdmin.AOIID = CType(e.Item.FindControl("lblAOIID"), Label).Text
                    objAdmin.ManageAOIs()
                    dgAreasInt.EditItemIndex = -1
                    objAdmin.DomainID = Session("DomainID")
                    dgAreasInt.DataSource = objAdmin.GetAOIs
                    dgAreasInt.DataBind()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgAreasInt_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAreasInt.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.EditItem Then
                    Dim btnDelete As Button
                    btnDelete = e.Item.FindControl("btnDelete")
                    btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace