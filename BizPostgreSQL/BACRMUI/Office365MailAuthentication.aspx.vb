﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports Newtonsoft.Json
Imports System.Net
Imports System.IO
Imports System.Text
Imports Microsoft.Graph
Imports Microsoft.Graph.Core

Public Class Office365MailAuthenticate
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim office365MailAppClientID As String = CCommon.ToString(ConfigurationManager.AppSettings("Office365MailAppClientID"))
            Dim office365MailAppClientSecret As String = CCommon.ToString(ConfigurationManager.AppSettings("Office365MailAppClientSecret"))
            Dim office365MailAppRedirectURL As String = CCommon.ToString(ConfigurationManager.AppSettings("office365MailAppRedirectURL"))

            If Not Page.IsPostBack Then
                If Session("GoogleEmailId") IsNot Nothing AndAlso Session("GoogleContactId") IsNot Nothing Then
                    If Not String.IsNullOrEmpty(GetQueryStringVal("code")) Then
                        btnContinue.Visible = False

    
                        Dim postData As String = ""
                        postData += "client_id" + "=" + HttpUtility.UrlEncode(office365MailAppClientID) + "&"
                        postData += "code" + "=" + HttpUtility.UrlEncode(GetQueryStringVal("code")) + "&"
                        postData += "scope" + "=" + HttpUtility.UrlEncode("https://outlook.office.com/SMTP.Send https://outlook.office.com/IMAP.AccessAsUser.All") + "&"
                        postData += "redirect_uri" + "=" + HttpUtility.UrlEncode(office365MailAppRedirectURL) + "&"
                        postData += "grant_type" + "=" + HttpUtility.UrlEncode("authorization_code") + "&"
                        postData += "client_secret" + "=" + HttpUtility.UrlEncode(office365MailAppClientSecret)

                        Dim request As HttpWebRequest = HttpWebRequest.Create("https://login.microsoftonline.com/common/oauth2/v2.0/token")
                        request.Method = "POST"
                        request.Accept = "application/json"
                        request.ContentType = "application/x-www-form-urlencoded"

                        request.ContentLength = postData.Length

                        Using requestStream As Stream = request.GetRequestStream()
                            Dim writer As New StreamWriter(requestStream)
                            writer.Write(postData)
                            writer.Close()
                        End Using

                        Try
                            Dim response As HttpWebResponse = request.GetResponse()
                            Dim responseString As String = New StreamReader(response.GetResponseStream()).ReadToEnd()
                            response.Close()

                            Dim objOffice365TokenResponse As Office365TokenResponse = JsonConvert.DeserializeObject(Of Office365TokenResponse)(responseString)

                            If Not objOffice365TokenResponse Is Nothing Then
                                Dim emailAddress As String = ""
                                Dim jwt As New System.IdentityModel.Tokens.Jwt.JwtSecurityTokenHandler()
                                Dim token = jwt.ReadJwtToken(objOffice365TokenResponse.IdToken)

                                emailAddress = token.Claims.FirstOrDefault(Function(x) x.Type = "preferred_username").Value

                                Dim objUserAccess As New UserAccess
                                objUserAccess.UserCntID = Session("GoogleContactId")
                                objUserAccess.DomainID = Session("DomainID")
                                objUserAccess.Email = emailAddress
                                objUserAccess.UpdateMailRefreshToken(2, objOffice365TokenResponse.AccessToken, objOffice365TokenResponse.RefreshToken, objOffice365TokenResponse.ExpiresIn)

                                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "CloseAuthenticationWindow", "alert('Microsoft office 365 authentication completed successfully'); self.close();", True)
                            End If
                        Catch exWeb As WebException
                            If exWeb.Response IsNot Nothing Then
                                Dim tokenResponse As String = New StreamReader(exWeb.Response.GetResponseStream()).ReadToEnd()
                                Dim objOffice365TokenErrorResponse As Office365TokenErrorResponse = JsonConvert.DeserializeObject(Of Office365TokenErrorResponse)(tokenResponse)
                                DisplayError(objOffice365TokenErrorResponse.ErrorMessage)
                            End If
                        Catch ex As Exception
                            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Page.Request)
                            DisplayError(CCommon.ToString(ex))
                        End Try
                    ElseIf Not String.IsNullOrEmpty(GetQueryStringVal("error")) Then
                        Dim errorMessage As String = CCommon.ToString(GetQueryStringVal("error"))
                        Dim errorDescription As String = CCommon.ToString(GetQueryStringVal("error_description"))

                        btnContinue.Visible = False
                        DisplayError(errorMessage & ": " & errorDescription)
                    Else
                        lblInstruction.Text = "You will be redirected to Office 365 Login Page for authorization on continue click.<br /><br /> Please login using your ""<b>" & Session("GoogleEmailId") & "</b>"" email and say ""Allow"" when prompted for authorization."
                    End If
                Else
                    btnContinue.Visible = False
                    DisplayError("Configure smtp settings")
                End If
            Else
                Dim url As String = String.Format("https://login.microsoftonline.com/common/oauth2/v2.0/authorize?client_id={0}&response_type=code&redirect_uri={1}&response_mode=query&scope=offline_access%20https://outlook.office.com/IMAP.AccessAsUser.All%20https://outlook.office.com/SMTP.Send%20openid%20profile", office365MailAppClientID, office365MailAppRedirectURL)
                Response.Redirect(url, False)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

#Region "Private Methods"

    Private Sub DisplayError(ByVal exception As String)
        Try
            lblPageError.Text = exception
            divPageError.Style.Add("display", "")
            divPageError.Focus()
        Catch ex As Exception

        End Try
    End Sub

    Private ReadOnly UriRfc3986CharsToEscape As String() = New String() {"!", "*", "'", "(", ")"}
    Private Function EscapeUriDataStringRfc3986(value As String) As String
        Dim escaped As New StringBuilder(Uri.EscapeDataString(value))

        ' Upgrade the escaping to RFC 3986, if necessary.
        For i As Integer = 0 To UriRfc3986CharsToEscape.Length - 1
            escaped.Replace(UriRfc3986CharsToEscape(i), Uri.HexEscape(UriRfc3986CharsToEscape(i)(0)))
        Next

        ' Return the fully-RFC3986-escaped string.
        Return escaped.ToString()
    End Function

    Public Class ExtendedMicrosoftClientUserData
        Public Property businessPhones As String()
        Public Property displayName As String
        Public Property givenName As String
        Public Property jobTitle As String
        Public Property mail As String
        Public Property mobilePhone As String
        Public Property officeLocation As String
        Public Property preferredLanguage As String
        Public Property surname As String
        Public Property userPrincipalName As String
        Public Property id As String
    End Class

#End Region

    Private Class Office365TokenResponse
        <JsonProperty("access_token")>
        Public AccessToken As String

        <JsonProperty("token_type")>
        Public TokenType As String

        <JsonProperty("expires_in")>
        Public ExpiresIn As Long

        <JsonProperty("scope")>
        Public Scope As String

        <JsonProperty("refresh_token")>
        Public RefreshToken As String

        <JsonProperty("id_token")>
        Public IdToken As String
    End Class

    Private Class Office365TokenErrorResponse
        <JsonProperty("error")>
        Public ErrorMessage As String

        <JsonProperty("error_description")>
        Public ErrorDescription As String

        <JsonProperty("error_codes")>
        Public ErrorCodes As System.Collections.Generic.List(Of String)

        <JsonProperty("timestamp")>
        Public TimeStamp As String

        <JsonProperty("trace_id")>
        Public TraceID As String

        <JsonProperty("correlation_id")>
        Public CorrelationId As String
    End Class

End Class