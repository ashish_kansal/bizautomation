<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmSolution.aspx.vb"
    Inherits="BACRM.UserInterface.Cases.frmSolution" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="menu1" TagName="webmenu" Src="../include/webmenu.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Solution</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <script language="javascript">
        function fn_GoToURL(varURL) {

            if ((varURL != '') && (varURL.substr(0, 7) == 'http://') && (varURL.length > 7)) {
                var LoWindow = window.open(varURL, "", "");
                LoWindow.focus();
            }
            return false;
        }
        function OpenDocuments(a) {
            window.open("../Documents/frmSpecDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=S&yunWE=" + a, '', 'toolbar=no,titlebar=no,top=200,width=900,height=450,scrollbars=yes,resizable=yes')
            return false;
        }
        function SelectCase() {
            if (document.getElementById('ddlCaseNo').selectedIndex == -1 || document.getElementById('ddlCaseNo').selectedIndex == 0) {
                alert("Please Select a Case")
                return false;
            }
        }

    </script>
     <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                        <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save &amp; Close</asp:LinkButton>
                        <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-primary"><i class="fa fa-times-circle-o"></i>&nbsp;&nbsp;Cancel</asp:LinkButton>
                <a href="#" class="help">&nbsp;</a>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Solution Details
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-3">
                            <label>Soluton Name<font color="red">*</font></label>
                        </div>
                        <div class="col-md-4 form-group">
                            <asp:TextBox ID="txtSolution" CssClass="form-control" runat="server"></asp:TextBox>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-3">
                            <label>Solution Category<font color="red">*</font></label>
                        </div>
                        <div class="col-md-4 form-group">
                            <asp:DropDownList ID="ddlCategory" runat="server" Width="130" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-3">
                            <label>Solution Description<font color="red">*</font></label>
                        </div>
                        <div class="col-md-4 form-group">
                            <asp:TextBox ID="txtSolDesc" CssClass="form-control" runat="server" TextMode="MultiLine" Height="250"></asp:TextBox>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-3">
                            <label>URL</label>
                        </div>
                        <div class="col-md-4 form-inline">
                             <asp:TextBox ID="txtLink" runat="server" value="http://"  CssClass="form-control"></asp:TextBox>&nbsp;
                            <asp:Button ID="btnLinkGo" runat="server" CssClass="btn btn-primary" Text="Go" ></asp:Button>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-offset-3 col-md-4">
                            <asp:HyperLink ID="hplDocuments" runat="server" CssClass="hyperlink">
										Documents</asp:HyperLink>
                        </div>
                        <div class="clearfix"></div>

                    </div>
                </div>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
