'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports Telerik.Web.UI
Namespace BACRM.UserInterface.Cases

    Public Class frmCaseList
        Inherits BACRMPage
        Dim strColumn As String
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim RegularSearch As String
        Dim CustomSearch As String
        Dim m_aryRightsForViewGridConfiguration() As Integer

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If GetQueryStringVal("SI") <> "" Then
                    SI = GetQueryStringVal("SI")
                End If
                If GetQueryStringVal("SI1") <> "" Then
                    SI1 = GetQueryStringVal("SI1")
                Else : SI1 = 0
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    SI2 = GetQueryStringVal("SI2")
                Else : SI2 = 0
                End If
                If GetQueryStringVal("frm") <> "" Then
                    frm = GetQueryStringVal("frm")
                Else : frm = ""
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = GetQueryStringVal("frm1")
                Else : frm1 = ""
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    frm2 = GetQueryStringVal("frm2")
                Else : frm2 = ""
                End If

                ' Checking Rights for View
                GetUserRightsForPage(7, 2)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AC")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                btnCloseCase.Attributes.Add("onclick", "return CloseCase();")

                If Not IsPostBack Then
                    objCommon.sb_FillComboFromDB(ddlStatus, 14, Session("DomainID"))
                    Dim lstNewItem As New ListItem
                    lstNewItem.Text = "-- Select One --"
                    lstNewItem.Value = 0
                    ddlStatus.Items.Insert(0, lstNewItem)

                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                        txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                        txtCustomer.Text = CCommon.ToString(PersistTable(PersistKey.OrgName))
                        txtFirstName.Text = CCommon.ToString(PersistTable(PersistKey.FirstName))
                        txtLastName.Text = CCommon.ToString(PersistTable(PersistKey.LastName))
                        txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                        txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))
                        ddlSort.ClearSelection()
                        If Not ddlSort.Items.FindByValue(CCommon.ToString(PersistTable(PersistKey.FilterBy))) Is Nothing Then ddlSort.Items.FindByValue(CCommon.ToString(PersistTable(PersistKey.FilterBy))).Selected = True
                        txtGridColumnFilter.Text = CCommon.ToString(PersistTable(PersistKey.GridColumnSearch))
                    End If

                    'Check if user has rights to edit grid configuration
                    m_aryRightsForViewGridConfiguration = GetUserRightsForPage_Other(7, 13)
                    If m_aryRightsForViewGridConfiguration(RIGHTSTYPE.VIEW) = 0 Then
                        Dim tdGridConfiguration As HtmlAnchor = CType(CCommon.FindControlRecursive(Page.Master, "tdGridConfiguration"), HtmlAnchor)

                        If Not tdGridConfiguration Is Nothing Then
                            tdGridConfiguration.Visible = False
                        End If
                    End If

                    If CCommon.ToShort(GetQueryStringVal("IsFormDashboard")) = 1 Then
                        ddlSort.SelectedValue = "1"
                        txtGridColumnFilter.Text = ""
                    End If

                    BindDatagrid()
                End If
                ClientScript.RegisterClientScriptBlock(Me.GetType, "SetTab", "if (parent.parent.frames.length > 0) { parent.parent.SelectTabByValue('4');}else{ parent.SelectTabByValue('4'); } ", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        'Private Function GetFilterData() As String()
        '    Try
        '        Dim objcontact As New CContacts
        '        Dim dtTable As DataTable
        '        objcontact.DomainID = Session("DomainId")
        '        objcontact.UserCntID = Session("UsercontactId")
        '        objcontact.FormId = 12
        '        objcontact.ContactType = 0
        '        dtTable = objcontact.GetDefaultfilter()
        '        Dim ListDetails As String()
        '        If dtTable.Rows.Count = 1 Then
        '            If CCommon.ToString(dtTable.Rows(0)("vcFilter")).IndexOf("~") > 0 Then
        '                ListDetails = CCommon.ToString(dtTable.Rows(0)("vcFilter")).Split("~")
        '                If Not ddlSort.Items.FindByValue(dtTable.Rows(0).Item("numfilterId")) Is Nothing Then
        '                    ddlSort.ClearSelection()
        '                    ddlSort.Items.FindByValue(dtTable.Rows(0).Item("numfilterId")).Selected = True
        '                End If
        '            End If
        '        End If
        '        If ListDetails Is Nothing Then
        '            Dim ListDetailsTemp(6) As String
        '            Return ListDetailsTemp
        '        End If
        '        Return ListDetails
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function
        'Private Sub SaveFilter(ByVal ListDetails As String())
        '    Try
        '        Dim strFilter As New System.Text.StringBuilder
        '        Dim objContact As New CContacts
        '        objContact.DomainID = Session("DomainID")
        '        objContact.UserCntID = Session("UserContactID")
        '        objContact.ContactType = 0
        '        objContact.bitDefault = False
        '        objContact.blnFlag = True
        '        objContact.FormId = 12
        '        objContact.GroupID = 0
        '        objContact.FilterID = ddlSort.SelectedValue
        '        strFilter.Append(ListDetails(0))
        '        strFilter.Append("~")
        '        strFilter.Append(ListDetails(1))
        '        strFilter.Append("~")
        '        strFilter.Append(ListDetails(2))
        '        strFilter.Append("~")
        '        strFilter.Append(ListDetails(3))
        '        strFilter.Append("~")
        '        strFilter.Append(ListDetails(4))
        '        strFilter.Append("~")
        '        strFilter.Append(ListDetails(5))
        '        strFilter.Append("~")
        '        strFilter.Append(ListDetails(6))
        '        objContact.strFilter = strFilter.ToString
        '        objContact.SaveDefaultFilter()
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub

        Protected Sub radCmbSearch_ItemsRequested(sender As Object, e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs)
            Try
                If e.Text.Trim().Length > 0 Then
                    Dim searchCriteria As String
                    Dim itemOffset As Integer = e.NumberOfItems
                    Dim dtCases As DataTable
                    Dim objCases As New CCases

                    With objCases
                        .UserCntID = Session("UserContactID")
                        .DomainID = Session("DomainID")
                        .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                        .SortOrder = ddlSort.SelectedItem.Value
                        '.FirstName = txtFirstName.Text
                        '.LastName = txtLastName.Text
                        .SortCharacter = txtSortChar.Text.Trim()
                        '.CustName = txtCustomer.Text

                        If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                        .CurrentPage = txtCurrrentPage.Text.Trim()

                        .PageSize = Session("PagingRows")
                        .TotalRecords = 0

                        If txtSortColumn.Text <> "" Then
                            .columnName = txtSortColumn.Text
                        Else : .columnName = "cs.bintcreateddate"
                        End If

                        If txtSortOrder.Text = "D" Then
                            .columnSortOrder = "Desc"
                        Else : .columnSortOrder = "Asc"
                        End If

                        GridColumnSearchCriteria()
                        .SearchText = e.Text
                        .RegularSearchCriteria = RegularSearch
                        .CustomSearchCriteria = CustomSearch
                    End With

                    objCases.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    objCases.Status = CCommon.ToLong(ddlStatus.SelectedValue)

                    Dim dsList As DataSet

                    dsList = objCases.GetCaseList
                    dtCases = dsList.Tables(0)

                    Dim dtTableInfo As DataTable
                    dtTableInfo = dsList.Tables(1)

                    InitializeSearchClientSideTemplate(dsList.Tables(1).AsEnumerable().Take(3).CopyToDataTable())

                    If Not dsList Is Nothing AndAlso dsList.Tables.Count > 0 Then
                        Dim endOffset As Integer

                        endOffset = Math.Min(itemOffset + radCmbSearch.ItemsPerRequest, dtCases.Rows.Count)
                        e.EndOfItems = endOffset = dtCases.Rows.Count
                        e.Message = IIf(dtCases.Rows.Count <= 0, "No matches", String.Format("Case <b>1</b>-<b>{0}</b> out of <b>{1}</b>", endOffset, dtCases.Rows.Count))

                        For Each dr As DataRow In dtCases.Rows
                            Dim item As New RadComboBoxItem()
                            item.Text = DirectCast(dr("vcCaseNumber"), String)
                            item.Value = dr("numCaseID").ToString()
                            item.Attributes.Add("orderType", ddlSort.SelectedValue)

                            item.DataItem = dr
                            radCmbSearch.Items.Add(item)
                            item.DataBind()
                        Next
                    End If
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                e.Message = "<b style=""color:Red"">Error occured while searching.</b>"
            End Try
        End Sub


        Private Sub InitializeSearchClientSideTemplate(dt As DataTable)
            Try
                'objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                'objCommon.UserCntID = CCommon.ToLong(Session("UserContactID"))
                'Dim ds As DataSet = objCommon.SearchOrders("", ddlSort.SelectedValue, IIf(ddlSort.SelectedValue = "1", m_aryRightsForPage(RIGHTSTYPE.VIEW), m_aryRightsForPage(RIGHTSTYPE.VIEW)), 0, 2)

                'If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                '    Dim dtTable As New DataTable
                '    dtTable = ds.Tables(0)

                'genrate header template dynamically
                radCmbSearch.HeaderTemplate = New SearchTemplate(ListItemType.Header, dt, DropDownWidth:=radCmbSearch.DropDownWidth.Value)

                'generate Clientside Template dynamically
                radCmbSearch.ItemTemplate = New SearchTemplate(ListItemType.Item, dt, DropDownWidth:=radCmbSearch.DropDownWidth.Value)
                'End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                'DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
#Region "RadComboBox Template"
        Public Class SearchTemplate
            Implements ITemplate

            Dim ItemTemplateType As ListItemType
            Dim dtTable1 As DataTable
            Dim i As Integer = 0
            Dim _DropDownWidth As Double

            Sub New(ByVal type As ListItemType, ByVal dtTable As DataTable, Optional ByVal DropDownWidth As Double = 600)
                Try
                    ItemTemplateType = type
                    dtTable1 = dtTable
                    _DropDownWidth = DropDownWidth
                Catch ex As Exception
                    Throw ex
                End Try
            End Sub

            Public Sub InstantiateIn(ByVal container As Control) Implements ITemplate.InstantiateIn
                Try


                    i = 0
                    Dim label1 As Label
                    Dim label2 As Label
                    Dim img As System.Web.UI.WebControls.Image
                    Dim table1 As New HtmlTable
                    Dim tblCell As HtmlTableCell
                    Dim tblRow As HtmlTableRow
                    table1.Width = "100%"
                    Dim ul As New HtmlGenericControl("ul")

                    Select Case ItemTemplateType
                        Case ListItemType.Header
                            For Each dr As DataRow In dtTable1.Rows
                                Dim li As New HtmlGenericControl("li")
                                li.Style.Add("width", Unit.Pixel(CCommon.ToInteger((_DropDownWidth - 50) / dtTable1.Rows.Count)).Value & "px")
                                li.Style.Add("float", "left")
                                li.Style.Add("text-align", "center")
                                li.Style.Add("font-weight", "bold")
                                li.InnerText = dr("vcFieldName").ToString
                                ul.Controls.Add(li)
                            Next

                            container.Controls.Add(ul)
                        Case ListItemType.Item
                            For Each dr As DataRow In dtTable1.Rows
                                Dim li As New HtmlGenericControl("li")
                                Dim width As Integer = CCommon.ToInteger((_DropDownWidth - 50) / dtTable1.Rows.Count)
                                li.Style.Add("width", Unit.Pixel(width).Value & "px")
                                li.Style.Add("float", "left")

                                If dr("bitCustomField") = "1" Then
                                    label2 = New Label
                                    label2.CssClass = "normal1"
                                    label2.Attributes.Add("style", "display:inline-block;width:" & width)
                                    AddHandler label2.DataBinding, AddressOf label2_DataBinding
                                    li.Controls.Add(label2)
                                Else
                                    label1 = New Label
                                    label1.CssClass = "normal1"
                                    label1.Attributes.Add("style", "display:inline-block;width:" & width)
                                    AddHandler label1.DataBinding, AddressOf label1_DataBinding
                                    li.Controls.Add(label1)
                                End If

                                ul.Controls.Add(li)
                            Next

                            container.Controls.Add(ul)
                    End Select
                Catch ex As Exception
                    Throw
                End Try
            End Sub

            Private Sub label1_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
                Dim target As Label = CType(sender, Label)
                Dim item As RadComboBoxItem = CType(target.NamingContainer, RadComboBoxItem)
                Dim itemText As String = Convert.ToString(DirectCast(item.DataItem, DataRow)(dtTable1.Rows(i).Item("vcDbColumnName") & "~" & dtTable1.Rows(i).Item("numFieldId") & "~" & CCommon.ToInteger(dtTable1.Rows(i).Item("bitCustomField"))))
                'Dim itemText As String = CType(DataBinder.Eval(item.DataItem, dtTable1.Rows(i).Item("vcDbColumnName").ToString()), String)
                target.Text = itemText
                i = i + 1
            End Sub

            Private Sub label2_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
                Dim target As Label = CType(sender, Label)
                Dim item As RadComboBoxItem = CType(target.NamingContainer, RadComboBoxItem)
                Dim itemText As String = Convert.ToString(DirectCast(item.DataItem, DataRow)(dtTable1.Rows(i).Item("vcDbColumnName") & "~" & dtTable1.Rows(i).Item("numFieldId") & "~" & CCommon.ToInteger(dtTable1.Rows(i).Item("bitCustomField"))))
                'Dim itemText As String = CType(DataBinder.Eval(item.DataItem, dtTable1.Rows(i).Item("vcFieldName").ToString()), String)
                target.Text = itemText
                i = i + 1
            End Sub

        End Class
#End Region
        Sub BindDatagrid(Optional ByVal CreateCol As Boolean = True)
            Try
                Dim dtCases As DataTable
                Dim objCases As New CCases

                With objCases
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")
                    .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                    .SortOrder = ddlSort.SelectedItem.Value
                    '.FirstName = txtFirstName.Text
                    '.LastName = txtLastName.Text
                    .SortCharacter = txtSortChar.Text.Trim()
                    '.CustName = txtCustomer.Text

                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()

                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0

                    If txtSortColumn.Text <> "" Then
                        .columnName = txtSortColumn.Text
                    Else : .columnName = "cs.bintcreateddate"
                    End If

                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If

                    GridColumnSearchCriteria()

                    .SearchText = radCmbSearch.Text
                    .CustomSearchCriteria = CustomSearch
                    .RegularSearchCriteria = RegularSearch
                End With

                objCases.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objCases.Status = CCommon.ToLong(ddlStatus.SelectedValue)

                Dim dsList As DataSet

                dsList = objCases.GetCaseList
                dtCases = dsList.Tables(0)

                Dim dtTableInfo As DataTable
                dtTableInfo = dsList.Tables(1)

                bizPager.PageSize = Session("PagingRows")
                bizPager.RecordCount = objCases.TotalRecords
                bizPager.CurrentPageIndex = txtCurrrentPage.Text

                'Persist Form Settings
                PersistTable.Clear()
                PersistTable.Add(PersistKey.CurrentPage, IIf(dtCases.Rows.Count > 0, txtCurrrentPage.Text, "1"))
                PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())
                PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
                PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text)
                PersistTable.Add(PersistKey.OrgName, txtCustomer.Text.Trim())
                PersistTable.Add(PersistKey.FirstName, txtFirstName.Text.Trim())
                PersistTable.Add(PersistKey.LastName, txtLastName.Text.Trim())
                PersistTable.Add(PersistKey.FilterBy, ddlSort.SelectedValue)
                PersistTable.Add(PersistKey.GridColumnSearch, txtGridColumnFilter.Text)
                PersistTable.Save()

                If objCases.TotalRecords = 0 Then
                    lblRecordCountCases.Text = 0
                Else
                    lblRecordCountCases.Text = String.Format("{0:#,###}", objCases.TotalRecords)
                    Dim strTotalPage As String()
                    Dim decTotalPage As Decimal
                    decTotalPage = lblRecordCountCases.Text / Session("PagingRows")
                    decTotalPage = Math.Round(decTotalPage, 2)
                    strTotalPage = CStr(decTotalPage).Split(".")
                    If (lblRecordCountCases.Text Mod Session("PagingRows")) = 0 Then
                        txtTotalPage.Text = strTotalPage(0)
                    Else
                        txtTotalPage.Text = strTotalPage(0) + 1
                    End If
                    txtTotalRecords.Text = lblRecordCountCases.Text
                End If

                Dim m_aryRightsForInlineEdit() As Integer = GetUserRightsForPage_Other(7, 3)

                Dim i As Integer

                For i = 0 To dtCases.Columns.Count - 1
                    dtCases.Columns(i).ColumnName = dtCases.Columns(i).ColumnName.Replace(".", "")
                Next

                Dim htGridColumnSearch As New Hashtable

                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")
                    Dim strIDValue() As String

                    For i = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")

                        htGridColumnSearch.Add(strIDValue(0), strIDValue(1))
                    Next
                End If

                If CreateCol = True Then
                    Dim bField As BoundField
                    Dim Tfield As TemplateField
                    gvSearch.Columns.Clear()

                    For Each drRow As DataRow In dtTableInfo.Rows
                        Tfield = New TemplateField

                        Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, drRow, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 12, objCases.columnName, objCases.columnSortOrder)

                        Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, drRow, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 12, objCases.columnName, objCases.columnSortOrder)
                        gvSearch.Columns.Add(Tfield)
                    Next

                    Dim dr As DataRow
                    dr = dtTableInfo.NewRow()
                    dr("vcAssociatedControlType") = "DeleteCheckBox"
                    dr("intColumnWidth") = "30"

                    Tfield = New TemplateField
                    Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, dr, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 12)
                    Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, dr, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 12)
                    gvSearch.Columns.Add(Tfield)
                End If
                InitializeSearchClientSideTemplate(dsList.Tables(1).AsEnumerable().Take(3).CopyToDataTable())
                gvSearch.DataSource = dtCases
                gvSearch.DataBind()

                If m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE) <> 0 Then
                    Dim objPageControls As New PageControls
                    Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtTableInfo)
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "InlineEditValidation", strValidation, True)
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlSort_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSort.SelectedIndexChanged
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
                Session("SCases") = ddlSort.SelectedIndex
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub txtCurrrentPage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Function ReturnName(ByVal bintCreatedDate) As String
            Try
                Dim strCreateDate As String = ""
                If Not IsDBNull(bintCreatedDate) Then
                    strCreateDate = FormattedDateFromDate(bintCreatedDate, Session("DateFormat"))
                    If Format(bintCreatedDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                        strCreateDate = "<font color=red>Today</font>"
                    ElseIf Format(bintCreatedDate, "yyyyMMdd") < Format(Now(), "yyyyMMdd") Then
                        strCreateDate = "<font color=red>" & strCreateDate & "</font>"
                    End If
                End If
                Return strCreateDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Private Sub btnGotoRecord_Click(sender As Object, e As EventArgs) Handles btnGotoRecord.Click
            Try
                Response.Redirect("~/cases/frmCases.aspx?frm=Caselist&CaseID=" & radCmbSearch.SelectedValue, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Function ReturnDateTime(ByVal CloseDate) As String
            Try
                Dim strTargetResolveDate As String = ""
                If Not IsDBNull(CloseDate) Then strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnGoCases_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGoCases.Click
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
            Try
                Dim strCaseid As String() = txtDelCaseId.Text.Split(",")
                Dim i As Int16 = 0
                Dim lngCaseID As Long
                Dim lngRecOwnID As Long
                Dim lngTerrID As Long
                Dim objCases As New CCases
                If String.IsNullOrEmpty(txtDelCaseId.Text) = True Then Exit Sub

                For i = 0 To strCaseid.Length - 1
                    lngCaseID = CCommon.ToLong(strCaseid(i).Split("~")(0))
                    lngRecOwnID = CCommon.ToLong(strCaseid(i).Split("~")(1))
                    lngTerrID = CCommon.ToLong(strCaseid(i).Split("~")(2))

                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 1 Then
                        Try
                            If lngRecOwnID = Session("UserContactID") Then
                                objCases.CaseID = lngCaseID
                                objCases.DomainID = Session("DomainID")
                                If objCases.DeleteCase() = False Then litMessage.Text = "Dependent Records Exists.Cannot be deleted."
                            End If
                        Catch ex As Exception
                        End Try
                    ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 2 Then
                        Try
                            Dim j As Integer
                            Dim dtTerritory As DataTable
                            Dim chkDelete As Boolean = False
                            dtTerritory = Session("UserTerritory")
                            If lngTerrID = 0 Then
                                chkDelete = True
                            Else
                                For j = 0 To dtTerritory.Rows.Count - 1
                                    If lngTerrID = dtTerritory.Rows(j).Item("numTerritoryId") Then chkDelete = True
                                Next
                            End If
                            If chkDelete = True Then
                                objCases.CaseID = lngCaseID
                                objCases.DomainID = Session("DomainID")
                                If objCases.DeleteCase() = False Then litMessage.Text = "Dependent Records Exists.Cannot be deleted."
                            End If
                        Catch ex As Exception
                        End Try
                    ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 3 Then
                        objCases.CaseID = lngCaseID
                        objCases.DomainID = Session("DomainID")
                        If objCases.DeleteCase() = False Then litMessage.Text = "Dependent Records Exists.Cannot be deleted."
                    End If
                Next
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                radCmbSearch.ClearSelection()
                radCmbSearch.Text = ""
                BindDatagrid(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Sub GridColumnSearchCriteria()
            Try
                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")

                    Dim strIDValue(), strID(), strCustom As String
                    Dim strRegularCondition As New ArrayList
                    Dim strCustomCondition As New ArrayList


                    For i As Integer = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")
                        strID = strIDValue(0).Split("~")

                        If strID(0).Contains("CFW.Cust") Then

                            Select Case strID(3).Trim()
                                Case "TextBox"
                                    strCustom = " ilike '%" & strIDValue(1).Replace("'", "''") & "%'"
                                Case "SelectBox"
                                    strCustom = "=" & strIDValue(1)
                            End Select

                            strCustomCondition.Add("CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & strCustom)
                        Else
                            Select Case strID(3).Trim()
                                Case "Website", "Email", "TextBox"
                                    If strID(0) = "ADC.vcCompactContactDetails" Then
                                        strRegularCondition.Add("(ADC.vcFirstName ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR ADC.vcLastName ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR ADC.numPhone ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR ADC.numPhoneExtension ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                    Else
                                        strRegularCondition.Add(strID(0) & " ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                    End If
                                Case "SelectBox"
                                    strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                                Case "TextArea"
                                    strRegularCondition.Add(" Cast(" & strID(0) & " as varchar(5000)) ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strRegularCondition.Add(strID(0) & " >= '" & fromDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strRegularCondition.Add(strID(0) & " <= '" & toDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    End If
                            End Select
                        End If
                    Next
                    RegularSearch = String.Join(" and ", strRegularCondition.ToArray())
                    CustomSearch = String.Join(" and ", strCustomCondition.ToArray())
                Else
                    RegularSearch = ""
                    CustomSearch = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnCloseCase_Click(sender As Object, e As EventArgs) Handles btnCloseCase.Click
            Try
                Dim strCaseid As String() = txtClosingCaseId.Text.Split(",")
                Dim i As Int16 = 0
                Dim lngCaseID As Long
                Dim objCases As New CCases
                If String.IsNullOrEmpty(txtClosingCaseId.Text) = True Then Exit Sub

                For i = 0 To strCaseid.Length - 1
                    lngCaseID = CCommon.ToLong(strCaseid(i).Split("~")(0))

                    Dim objCommon As New CCommon
                    objCommon.Mode = 43
                    objCommon.DomainID = Session("DomainID")
                    objCommon.UpdateRecordID = lngCaseID
                    objCommon.Comments = ""
                    objCommon.UpdateValueID = 136
                    objCommon.UpdateSingleFieldValue()
                Next

                BindDatagrid(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                txtCurrrentPage_TextChanged(Nothing, Nothing)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
    End Class
End Namespace