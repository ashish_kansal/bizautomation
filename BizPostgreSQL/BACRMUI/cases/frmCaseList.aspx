<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmCaseList.aspx.vb"
    MasterPageFile="~/common/GridMaster.Master" Inherits="BACRM.UserInterface.Cases.frmCaseList" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Case</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <script language="javascript">

        function CloseCase() {
            if (confirm('�This will remove selected Case(s) from the Cases module. You will still be able to access Case(s) from Case history, by navigating to the Organization the Case is for, and clicking on its �Cases� sub-tab. Are you sure you want to proceed ?')) {
                var RecordIDs = GetCheckedRowValues();

                document.getElementById('txtClosingCaseId').value = RecordIDs;
                if (RecordIDs.length > 0)
                    return true;
                else {
                    alert('Please select atleast one case to close!');
                    return false
                }

                return true;
            }
            else {
                return false;
            }
        }

        function DeleteRecord() {
            var RecordIDs = GetCheckedRowValues();

            document.getElementById('txtDelCaseId').value = RecordIDs;
            if (RecordIDs.length > 0)
                return true
            else {
                alert('Please select atleast one record!!');
                return false
            }
        }

        function OpenCase(a, b) {
            var str;

            str = "../cases/frmCases.aspx?frm=Caselist&CaseID=" + a

            document.location.href = str;
        }
        function AssignCaseOwner(a) {
            var str = "../cases/frmAssignCaseOwner.aspx?CaseID=" + a

            window.open(str, '', "width=340,height=150,status=no,top=100,left=150");
        }

        function OpenContact(a, b) {
            var str;
            str = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Caselist&ft6ty=oiuy&CntId=" + a;
            document.location.href = str;
        }
        function OpenWindow(a, b, c) {
            var str;
            if (b == 0) {

                str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CaselistDivID=" + a;

            }
            else if (b == 1) {

                str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Caselist&DivID=" + a;

            }
            else if (b == 2) {

                str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Caselist&klds+7kldf=fjk-las&DivId=" + a;

            }

            document.location.href = str;

        }
        function OpenSetting() {
            window.open('../Prospects/frmConfCompanyList.aspx?FormId=12', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function DeleteRecord1() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function OrderSearchClientSelectedIndexChanged(sender, eventArgs) {
            $("[id$=btnGotoRecord]").click();
        }
        function OpenHelp() {
            window.open('../Help/Initial_Grid_Cases_.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function OpenCaseEmail(a,b) {
            window.open('../contact/frmComposeWindow.aspx?Lsemail=' + a + '&fghTY=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
            return false;
        }

        function openSolutionPopup(caseId) {
            window.open("../cases/frmAddCaseSolution.aspx?CaseId=" + caseId, '', 'toolbar=no,titlebar=no,top=200,left=200,width=1000,height=650,scrollbars=yes,resizable=yes');
        }
    </script>
    <style>
        .tblDataGrid tr:first-child td {
            background:#e5e5e5;
        }
    </style>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12">
            <div class="pull-left">
                <div class="form-group">
                    <div class="form-inline">
                    <label>
                        View</label>
               
                    <asp:DropDownList ID="ddlSort" runat="server" CssClass="form-control" AutoPostBack="True">
                        <asp:ListItem Value="0">My Cases</asp:ListItem>
                        <asp:ListItem Value="1">All Open Cases</asp:ListItem>
                        <asp:ListItem Value="2">Added in Last 7 Days</asp:ListItem>
                        <asp:ListItem Value="3">Last 20 Added by me</asp:ListItem>
                        <asp:ListItem Value="4">Last 20 Modified by me</asp:ListItem>
                        <asp:ListItem Value="5">Closed Cases</asp:ListItem>
                        <asp:ListItem Value="6">New Email to Case</asp:ListItem>
                    </asp:DropDownList>
                </div>
            
                    </div>
                </div>
                <div class="pull-right">
                    <div class="form-group">

                        <div class="form-inline" >.
                          <div style="display:none">
                    <label>
                            Status</label>
                     <asp:DropDownList ID="ddlStatus" runat="server" CssClass="form-control"></asp:DropDownList>
                    
                    <label>
                            Organization</label>
                    <asp:TextBox ID="txtCustomer" runat="server" Width="55px" CssClass="form-control"></asp:TextBox>
                    <label>
                            First Name</label>
                    <asp:TextBox ID="txtFirstName" runat="server" Width="55px" CssClass="form-control"></asp:TextBox>
                    <label>
                            Last Name</label>
                    <asp:TextBox ID="txtLastName" runat="server" Width="55px" CssClass="form-control"></asp:TextBox>
                                </div>

                    <telerik:RadComboBox ID="radCmbSearch" runat="server" DropDownWidth="600px" Width="200"
                        Skin="Default" AllowCustomText="false" ShowMoreResultsBox="true" EnableLoadOnDemand="true"
                        EnableScreenBoundaryDetection="true" ItemsPerRequest="10" EnableVirtualScrolling="true"
                        OnItemsRequested="radCmbSearch_ItemsRequested" HighlightTemplatedItems="true" ClientIDMode="Static"
                        TabIndex="503" MaxHeight="230" OnClientSelectedIndexChanged="OrderSearchClientSelectedIndexChanged" Style="display: none">
                    </telerik:RadComboBox>
                    <asp:LinkButton ID="btnGoCases" CssClass="btn btn-primary" runat="server" Text="" Style="display: none">Search</asp:LinkButton>
                    <button id="btnAddNewCase" onclick="return OpenPopUp('../cases/frmAddCases.aspx');" class="btn btn-primary" ><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New</button>                   
                    <asp:LinkButton ID="btnCloseCase" runat="server" CssClass="btn btn-primary"><i class="fa fa-times-circle-o"></i>&nbsp;&nbsp;Close Case</asp:LinkButton>
                    <asp:LinkButton ID="btnDelete" CssClass="btn btn-danger" runat="server" Text="X"></asp:LinkButton>
                            <asp:Button ID="btnGotoRecord" runat="server" Style="display: none;" />
                    <a href="#" class="print">&nbsp;</a>
                    <a href="#" class="help">&nbsp;</a>

                </div>
                        </div>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table cellspacing="0" cellpadding="2" width="100%" border="0">
        <tr style="line-height: 43px">
            <td class="normal4" align="left">
                <asp:Label ID="lblRecordCountCases" Visible="false" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Cases&nbsp;<a href="#" onclick="return OpenHelpPopUp('cases/frmcaselist.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
        ShowPageIndexBox="Never"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
    <asp:GridView ID="gvSearch" runat="server" EnableViewState="true" AutoGenerateColumns="false"
        CssClass="table table-responsive table-bordered tblDataGrid" Width="100%" ShowHeaderWhenEmpty="true">
        <Columns>
        </Columns>
        <EmptyDataTemplate>
            No records Found.
        </EmptyDataTemplate>
    </asp:GridView>
    <asp:TextBox ID="txtDelCaseId" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtClosingCaseId" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
        </div>
    <%--<asp:UpdatePanel ID="updatepanel0" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
            <table width="100%">
                <tr>
                    <td>
                        <asp:HyperLink Text="Settings" CssClass="hyperlink" runat="server" ID="hplSettings"></asp:HyperLink>
                    </td>
                    <td align="right">
                        <table align="right">
                            <%--<tr>
                                <asp:Panel ID="pnlBases1" runat="server">
                                    <td class="normal1" width="150">
                                        No of Records:
                                        <asp:Label ID="lblRecordSolution" runat="server"></asp:Label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                    <td class="normal1" nowrap align="right">
                                        &nbsp;Find Solution using Name or Keyword
                                    </td>
                                    <td>
                                        &nbsp;
                                        <asp:TextBox ID="txtKeyWord" runat="server" CssClass="signup" Width="200"></asp:TextBox>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnGoBases" runat="server" CssClass="button" Text="Go" Width="25">
                                        </asp:Button>
                                    </td>
                                </asp:Panel>
                            </tr>--%>
    <%--</table>
                    </td>
                </tr>
            </table>
            <table align="right" width="600" border="0">
                <tr>
                    <td valign="bottom" width="100">
                    </td>
                    <asp:Panel ID="pnlBases2" runat="server">
                        <td class="normal1" nowrap align="right">
                            &nbsp;Solution Category
                        </td>
                        <td>
                            &nbsp;
                            <asp:DropDownList ID="ddlCategory" runat="server" CssClass="signup" Width="130" AutoPostBack="True">
                            </asp:DropDownList>
                            &nbsp;
                        </td>
                    </asp:Panel>
                </tr>
            </table>
            <br />
            <br />
            <telerik:RadTabStrip ID="radOppTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="false"
                Skin="BizSkin" ClickSelectedTab="True" AutoPostBack="True" SelectedIndex="0"
                MultiPageID="radMultiPage_OppTab">
                <Tabs>
                    <telerik:RadTab Text="&nbsp;&nbsp;Cases&nbsp;&nbsp;" Value="Cases" PageViewID="radPageView_Cases">
                    </telerik:RadTab>
                    <telerik:RadTab Text="&nbsp;&nbsp;Knowledge Base&nbsp;&nbsp;" Value="KnowledgeBase"
                        PageViewID="radPageView_KnowledgeBase">
                    </telerik:RadTab>
                </Tabs>
            </telerik:RadTabStrip>
            <telerik:RadMultiPage ID="radMultiPage_OppTab" runat="server" CssClass="pageView"
                Width="100%">
                <telerik:RadPageView ID="radPageView_Cases" runat="server">
                </telerik:RadPageView>
                <telerik:RadPageView ID="radPageView_KnowledgeBase" runat="server">
                    <table cellspacing="1" cellpadding="1" width="100%" border="0">
                        <tr valign="top">
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="a" href="javascript:fnSortByCharKnowledge('a')">
                                    <div class="A2Z">
                                        A</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="b" href="javascript:fnSortByCharKnowledge('b')">
                                    <div class="A2Z">
                                        B</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="c" href="javascript:fnSortByCharKnowledge('c')">
                                    <div class="A2Z">
                                        C</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="d" href="javascript:fnSortByCharKnowledge('d')">
                                    <div class="A2Z">
                                        D</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="e" href="javascript:fnSortByCharKnowledge('e')">
                                    <div class="A2Z">
                                        E</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="f" href="javascript:fnSortByCharKnowledge('f')">
                                    <div class="A2Z">
                                        F</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="g" href="javascript:fnSortByCharKnowledge('g')">
                                    <div class="A2Z">
                                        G</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="h" href="javascript:fnSortByCharKnowledge('h')">
                                    <div class="A2Z">
                                        H</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="I" href="javascript:fnSortByCharKnowledge('i')">
                                    <div class="A2Z">
                                        I</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="j" href="javascript:fnSortByCharKnowledge('j')">
                                    <div class="A2Z">
                                        J</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="k" href="javascript:fnSortByCharKnowledge('k')">
                                    <div class="A2Z">
                                        K</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="l" href="javascript:fnSortByCharKnowledge('l')">
                                    <div class="A2Z">
                                        L</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="m" href="javascript:fnSortByCharKnowledge('m')">
                                    <div class="A2Z">
                                        M</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="n" href="javascript:fnSortByCharKnowledge('n')">
                                    <div class="A2Z">
                                        N</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="o" href="javascript:fnSortByCharKnowledge('o')">
                                    <div class="A2Z">
                                        O</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="p" href="javascript:fnSortByCharKnowledge('p')">
                                    <div class="A2Z">
                                        P</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="q" href="javascript:fnSortByCharKnowledge('q')">
                                    <div class="A2Z">
                                        Q</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="r" href="javascript:fnSortByCharKnowledge('r')">
                                    <div class="A2Z">
                                        R</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="s" href="javascript:fnSortByCharKnowledge('s')">
                                    <div class="A2Z">
                                        S</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="t" href="javascript:fnSortByCharKnowledge('t')">
                                    <div class="A2Z">
                                        T</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="u" href="javascript:fnSortByCharKnowledge('u')">
                                    <div class="A2Z">
                                        U</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="v" href="javascript:fnSortByCharKnowledge('v')">
                                    <div class="A2Z">
                                        V</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="w" href="javascript:fnSortByCharKnowledge('w')">
                                    <div class="A2Z">
                                        W</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="x" href="javascript:fnSortByCharKnowledge('x')">
                                    <div class="A2Z">
                                        X</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="y" href="javascript:fnSortByCharKnowledge('y')">
                                    <div class="A2Z">
                                        Y</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="z" href="javascript:fnSortByCharKnowledge('z')">
                                    <div class="A2Z">
                                        Z</div>
                                </a>
                            </td>
                            <td onmousemove="bgColor='#c6d3e7'; this.color='black'" onmouseout="bgColor='#52658C'; this.color='white'"
                                bgcolor="#52658c">
                                <a id="all" href="javascript:fnSortByCharKnowledge('0')">
                                    <div class="A2Z">
                                        All</div>
                                </a>
                            </td>
                        </tr>
                    </table>
                    <asp:Table ID="Table1" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
                        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="350">
                        <asp:TableRow>
                            <asp:TableCell VerticalAlign="Top">
                                <asp:DataGrid ID="dgBases" runat="server" Width="100%" CssClass="dg" AllowSorting="True"
                                    AutoGenerateColumns="False" BorderColor="white">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn Visible="False" DataField="numSolnID"></asp:BoundColumn>
                                        <asp:ButtonColumn DataTextField="vcSolnTitle" SortExpression="vcSolnTitle" HeaderText="<font color=white>Solution Name</font>"
                                            CommandName="Solution"></asp:ButtonColumn>
                                        <asp:BoundColumn DataField="txtSolution" SortExpression="txtSolution" HeaderText="<font color=white>Solution Description</font>">
                                        </asp:BoundColumn>
                                        <asp:TemplateColumn>
                                            <ItemStyle Width="10"></ItemStyle>
                                            <ItemTemplate>
                                                <asp:Button ID="btnSolDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete">
                                                </asp:Button>
                                                <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
													<font color="#730000">*</font></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </telerik:RadPageView>
            </telerik:RadMultiPage>
        </ContentTemplate>
    </asp:UpdatePanel>--%>
</asp:Content>
