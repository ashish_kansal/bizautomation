﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmLinkedOppItems.aspx.vb"
    Inherits=".frmLinkedOppItems" MasterPageFile="~/common/Popup.Master" %>

<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Link items from sales order</title>
    <script type="text/javascript">
        function SelectAll(a) {
            var str;

            if (typeof (a) == 'string') {

                a = document.getElementById(a);

            }
            var dgOppItems = document.getElementById('dgOppItems');
            if (a.checked == true) {
                for (var i = 1; i <= dgOppItems.rows.length; i++) {
                    if (i < 10) {
                        str = '0' + i
                    }
                    else {
                        str = i
                    }
                    document.getElementById('dgOppItems_ctl' + str + '_chk').checked = true;

                }
            }
            else if (a.checked == false) {
                for (var i = 1; i <= dgOppItems.rows.length; i++) {
                    if (i < 10) {
                        str = '0' + i
                    }
                    else {
                        str = i
                    }

                    document.getElementById('dgOppItems_ctl' + str + '_chk').checked = false;
                }
            }
        }
        function SelectAll1(a) {
            var str;

            if (typeof (a) == 'string') {

                a = document.getElementById(a);

            }
            var dgLinkedItems = document.getElementById('dgLinkedItems');
            if (a.checked == true) {
                for (var i = 1; i <= dgLinkedItems.rows.length; i++) {
                    if (i < 10) {
                        str = '0' + i
                    }
                    else {
                        str = i
                    }
                    document.getElementById('dgLinkedItems_ctl' + str + '_chk').checked = true;

                }
            }
            else if (a.checked == false) {
                for (var i = 1; i <= dgLinkedItems.rows.length; i++) {
                    if (i < 10) {
                        str = '0' + i
                    }
                    else {
                        str = i
                    }

                    document.getElementById('dgLinkedItems_ctl' + str + '_chk').checked = false;
                }
            }
        }
        function GetSelectedItems() {
            var dgLinkedItems = document.getElementById('dgLinkedItems');
            var OppItemID = '';
            for (var i = 1; i <= dgLinkedItems.rows.length; i++) {
                if (i < 10) {
                    str = '0' + i
                }
                else {
                    str = i
                }
                if (str != '01')
                    if (document.getElementById('dgLinkedItems_ctl' + str + '_chk').checked) {
                        OppItemID = OppItemID + document.getElementById('dgLinkedItems_ctl' + str + '_lblOppItemID').innerHTML + ',';
                    }
            }
            document.getElementById('hdnOppItemIDs').value = OppItemID;
            return true;
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="50"
                            OnClientClick="javascript:opener.location.reload(true);window.close();"></asp:Button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Link items from sales order
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell>
                <br />
                <table border="0" width="100%">
                    <tr>
                        <td class="normal1" align="right">
                            Enter Order ID
                        </td>
                        <td>
                            <asp:TextBox ID="txtOrder" CssClass="signup" runat="server" Width="100"></asp:TextBox>
                            &nbsp;<asp:Button ID="btnGo" runat="server" Text="Go" CssClass="button"></asp:Button>
                        </td>
                    </tr>
                    <tr id="trOppItems" runat="server" visible="false">
                        <td class="normal1" align="left">
                            Select Item
                        </td>
                        <td align="right">
                            <asp:Button ID="btnAdd" runat="server" Text="Add Selected" CssClass="button"></asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right" colspan="2">
                            <asp:DataGrid ID="dgOppItems" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False">
                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                <ItemStyle CssClass="is"></ItemStyle>
                                <HeaderStyle CssClass="hs"></HeaderStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderText="">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chk" runat="server" onclick="SelectAll('dgOppItems_ctl01_chk')" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk" runat="server" />
                                            <asp:Label runat="server" ID="lblOppItemID" Text='<%# Eval("numoppitemtCode") %>'
                                                Style="display: none"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="numoppitemtCode" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numOppId" Visible="false"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Image">
                                        <ItemTemplate>
                                            <asp:Image runat="server" ID="imgItem" ImageUrl='<%# CCommon.GetDocumentPath(Session("DomainID")) & Eval("vcPathForTImage") %>'
                                                Width="100px" Height="100px" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn HeaderText="Item Name" DataField="vcItemName"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Model #" DataField="vcModelId"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Description" DataField="vcItemDesc"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Units" DataField="numUnitHour"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Total Amount" DataField="monTotAmount" DataFormatString="{0:#,###.00}">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Serial No" DataField="vcSerialNo"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="left">
                            <b>Linked Items</b>
                        </td>
                        <td align="right">
                            <asp:Button ID="btnRemove" runat="server" Text="Remove Selected" CssClass="button"
                                OnClientClick="GetSelectedItems();"></asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right" colspan="2">
                            <asp:DataGrid ID="dgLinkedItems" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False">
                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                <ItemStyle CssClass="is"></ItemStyle>
                                <HeaderStyle CssClass="hs"></HeaderStyle>
                                <Columns>
                                    <asp:TemplateColumn HeaderText="">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chk" runat="server" onclick="SelectAll1('dgLinkedItems_ctl01_chk')" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chk" runat="server" />
                                            <asp:Label runat="server" ID="lblOppItemID" Text='<%# Eval("numoppitemtCode") %>'
                                                Style="display: none"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="numoppitemtCode" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numOppId" Visible="false"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Image">
                                        <ItemTemplate>
                                            <asp:Image runat="server" ID="imgItem" ImageUrl='<%# CCommon.GetDocumentPath(Session("DomainID")) & Eval("vcPathForTImage") %>'
                                                Width="100px" Height="100px" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn HeaderText="Item Name" DataField="vcItemName"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Model #" DataField="vcModelId"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Description" DataField="vcItemDesc"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Units" DataField="numUnitHour"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Total Amount" DataField="monTotAmount" DataFormatString="{0:#,###.00}">
                                    </asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="Serial No" DataField="vcSerialNo"></asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
                <br />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:HiddenField ID="hdnOppItemIDs" runat="server" />
</asp:Content>
