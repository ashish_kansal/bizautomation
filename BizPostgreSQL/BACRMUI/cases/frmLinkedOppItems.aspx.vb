﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Common
Imports System.Web.UI.WebControls
Partial Public Class frmLinkedOppItems
    Inherits BACRMPage
    Dim lngCaseID As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngCaseID = CCommon.ToLong(GetQueryStringVal("CaseId"))
            litMessage.Text = ""
            If Not IsPostBack Then
                BindLinkedItems()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            Dim objCase As New CCases
            objCase.DomainID = Session("DomainID")
            objCase.CaseID = lngCaseID
            objCase.strOppSel = txtOrder.Text.Trim()
            Dim dtOppItem As DataTable = objCase.GetCaseOpportunities()
            If dtOppItem.Rows.Count > 0 Then
                dgOppItems.DataSource = dtOppItem
                dgOppItems.DataBind()
                trOppItems.Visible = True
            Else
                litMessage.Text = "No records found!"
                trOppItems.Visible = False
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            Dim objCase As New CCases
            objCase.DomainID = Session("DomainID")
            objCase.CaseID = lngCaseID
            objCase.byteMode = 2 'Add selected , only insert
            objCase.strOppSel = GetOppItemsAsString()
            objCase.SaveCaseOpportunities()

            BindLinkedItems()
            dgOppItems.DataBind()
            trOppItems.Visible = False
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click
        Try
            If hdnOppItemIDs.Value.Trim() <> "" Then
                Dim objCase As New CCases
                objCase.DomainID = Session("DomainID")
                objCase.CaseID = lngCaseID
                objCase.byteMode = 1 ' delete selected Opp items
                objCase.strOppSel = hdnOppItemIDs.Value
                objCase.SaveCaseOpportunities()
            End If
            BindLinkedItems()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub BindLinkedItems()
        Try
            Dim objCase As New CCases
            objCase.DomainID = Session("DomainID")
            objCase.CaseID = lngCaseID
            objCase.strOppSel = ""
            dgLinkedItems.DataSource = objCase.GetCaseOpportunities()
            dgLinkedItems.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Function GetOppItemsAsString() As String
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            dt.Columns.Add("numOppId")
            dt.Columns.Add("numOppItemID")
            Dim dr As DataRow

            For Each Item As DataGridItem In dgOppItems.Items
                If CType(Item.FindControl("chk"), CheckBox).Checked = True Then
                    dr = dt.NewRow
                    dr("numOppId") = Item.Cells(2).Text
                    dr("numOppItemID") = Item.Cells(1).Text
                    dt.Rows.Add(dr)
                End If
            Next
            ds.Tables.Add(dt)
            Return ds.GetXml()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class