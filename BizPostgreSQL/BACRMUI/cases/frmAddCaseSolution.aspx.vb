﻿Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Cases
    Public Class frmAddCaseSolution
        Inherits BACRMPage
        Dim lngCaseId As Long

        Dim objSolution As Solution
        Dim objCases As CaseIP
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            lngCaseId = GetQueryStringVal("CaseId")
            GetUserRightsForPage(7, 2)
            If Not IsPostBack Then
                BindSolutions()
                BindDatagrid()
                BindCaseDetails()
            End If
        End Sub
        Sub BindCaseDetails()
            objCases = New CaseIP
            objCases.CaseID = lngCaseId
            objCases.DomainID = Session("DomainID")
            objCases.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            objCases.GetCaseDetails()
            txtSolutionName.Text = objCases.SoultionName
            lblSolutionCount.Text = objCases.SolutionCount
        End Sub
        Private Sub BindSolutions()
            Try
                objSolution = New Solution
                Dim dtSolution As DataTable
                objSolution.CaseID = lngCaseId
                dtSolution = objSolution.GetSolutionForCases
                dgSolution.DataSource = dtSolution
                dgSolution.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub BindDatagrid(Optional ByVal CreateCol As Boolean = True)
            Try

                Dim SortChar As Char
                If ViewState("SortChar") <> "" Then
                    SortChar = ViewState("SortChar")
                Else : SortChar = "0"
                End If


                Dim objCases As New CCases

                Dim dtSolution As DataTable
                Dim objSolution As New Solution

                objSolution.Category = 0
                objSolution.KeyWord = txtSearchKnowledgeBase.Text
                objSolution.SortCharacter = SortChar
                objSolution.DomainID = Session("DomainID")
                objSolution.DeleteSolution()
                If Session("Asc") = 1 Then
                    objSolution.columnSortOrder = "Asc"
                Else : objSolution.columnSortOrder = "Desc"
                End If
                dtSolution = objSolution.GetKnowledgeBases

                dgBases.DataSource = dtSolution
                dgBases.DataBind()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub dgSolution_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSolution.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim btnDelete As Button
                    btnDelete = e.Item.FindControl("btnSolDelete")
                    btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub dgSolution_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSolution.ItemCommand
            Try
                'If e.CommandName = "Solution" Then
                '    Dim lngSolId As Long = e.Item.Cells(0).Text
                '    Response.Redirect("../Cases/frmSolution.aspx?SolId=" & lngSolId & "&SI1=" & radOppTab.SelectedIndex & "&SI2=" & SI1 & "&frm=frmCases" & "&frm1=" & frm1 & "&frm1=" & frm2 & "&CaseId=" & lngCaseId.ToString)
                'End If
                If e.CommandName = "Delete" Then
                    Dim lngSolId As Long = e.Item.Cells(0).Text

                    If objSolution Is Nothing Then objSolution = New Solution
                    objSolution.CaseID = lngCaseId
                    objSolution.SolID = lngSolId
                    objSolution.Mode = 1 'delete
                    objSolution.LinkSolToCases()
                    BindSolutions()
                    BindCaseDetails()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            End Try
        End Sub

        Private Sub dgBases_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgBases.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim btnDelete As LinkButton
                    Dim lnkDelete As LinkButton
                    lnkDelete = e.Item.FindControl("lnkdelete")
                    btnDelete = e.Item.FindControl("btnSolDelete")
                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                        btnDelete.Visible = False
                        lnkDelete.Visible = True
                        lnkDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    Else : btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgBases_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgBases.DeleteCommand
            Try
                Dim objSolution As New Solution
                Dim lblnumSolID As Label = CType(e.Item.FindControl("lblNumSolID"), Label)
                objSolution.SolID = CCommon.ToInteger(lblnumSolID.Text)
                objSolution.DeleteSolution()

                BindDatagrid()
            Catch ex As Exception
                Throw ex
            End Try


        End Sub

        Private Sub dgBases_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgBases.ItemCommand
            Try
                'If e.CommandName = "Solution" Then
                '    Dim lngSolId As Long = e.Item.Cells(0).Text
                '    Response.Redirect("../Cases/frmSolution.aspx?SolId=" & lngSolId & "&SI1=" & radOppTab.SelectedIndex & "&SI2=" & SI1 & "&frm=frmCases" & "&frm1=" & frm1 & "&frm1=" & frm2 & "&CaseId=" & lngCaseId.ToString)
                'End If
                If e.CommandName = "Edit" Then
                    Dim lblnumSolID As Label = CType(e.Item.FindControl("lblNumSolID"), Label)
                    Dim objSolution As New Solution
                    Dim dtSolDetails As DataTable
                    objSolution.SolID = CCommon.ToInteger(lblnumSolID.Text)
                    dtSolDetails = objSolution.GetSolutionForEdit
                    hdnSelectedSolutionId.Value = lblnumSolID.Text
                    txtSearchKnowledgeBase.Text = dtSolDetails.Rows(0).Item("vcSolnTitle")
                End If
                If e.CommandName = "AddSolutionToCase" Then
                    'Dim lngSolId As Long = e.Item.Cells(0).Text
                    Dim lblnumSolID As Label = CType(e.Item.FindControl("lblNumSolID"), Label)
                    If objSolution Is Nothing Then objSolution = New Solution
                    objSolution.CaseID = lngCaseId
                    objSolution.SolID = CCommon.ToInteger(lblnumSolID.Text)
                    objSolution.Mode = 0 'delete
                    objSolution.LinkSolToCases()
                    Page.Response.Redirect(Page.Request.Url.ToString(), True)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            End Try
        End Sub

        Protected Sub btnSearch_Click(sender As Object, e As EventArgs)
            BindDatagrid()
        End Sub

        Protected Sub btnSave_Click(sender As Object, e As EventArgs)
            Dim objSolution As New Solution
            objSolution.Category = 0
            objSolution.SolName = txtSearchKnowledgeBase.Text
            objSolution.SolDesc = ""
            objSolution.SolID = CCommon.ToInteger(hdnSelectedSolutionId.Value)
            objSolution.Link = ""
            objSolution.UserCntID = Session("UserContactID")
            objSolution.SaveSolution()
            hdnSelectedSolutionId.Value = "0"
            txtSearchKnowledgeBase.Text = ""
            BindDatagrid()
        End Sub

        Protected Sub btnAddSolution_Click(sender As Object, e As EventArgs)
            Dim objSolution As New Solution
            objSolution.SolName = txtSolutionName.Text
            objSolution.CaseID = lngCaseId
            objSolution.DomainID = Session("DomainID")
            objSolution.UserCntID = Session("UserContactID")
            objSolution.SaveCaseSolutionShortMessage()
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "RedirectClose", "window.opener.location.reload(true); window.close();", True)
        End Sub
    End Class

End Namespace