'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Cases

    Public Class frmSolution
        Inherits BACRMPage
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim objCommon As CCommon
        Dim lngSolId As Long

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                lngSolId = CCommon.ToLong(GetQueryStringVal("SolId"))
                If GetQueryStringVal("SI") <> "" Then
                    SI = GetQueryStringVal("SI")
                End If
                If GetQueryStringVal("SI1") <> "" Then
                    SI1 = GetQueryStringVal("SI1")
                Else : SI1 = 0
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    SI2 = GetQueryStringVal("SI2")
                Else : SI2 = 0
                End If
                If GetQueryStringVal("frm") <> "" Then
                    frm = ""
                    frm = GetQueryStringVal("frm")
                Else : frm = ""
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = ""
                    frm1 = GetQueryStringVal("frm1")
                Else : frm1 = ""
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    frm2 = ""
                    frm2 = GetQueryStringVal("frm2")
                Else : frm2 = ""
                End If
                If Not IsPostBack Then
                    ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "Support"
                    objCommon = New CCommon
                    Dim m_aryRightsForLinkToCases() As Integer
                    GetUserRightsForPage(7, 8)

                    If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                        btnSave.Visible = False
                        btnSaveClose.Visible = False
                    End If
                    m_aryRightsForLinkToCases = GetUserRightsForPage(7, 9)
                    'If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then btnlnkCase.Visible = False
                    LoadInformation()
                End If
                btnLinkGo.Attributes.Add("onclick", "return fn_GoToURL(txtLink.value);")
                hplDocuments.Attributes.Add("onclick", "return OpenDocuments(" & lngSolId & ");")
                ClientScript.RegisterClientScriptBlock(Me.GetType, "SetTab", "if (parent.parent.frames.length > 0) { parent.parent.SelectTabByValue('4');}else{ parent.SelectTabByValue('4'); } ", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Sub LoadInformation()
            Try
                objCommon.sb_FillComboFromDBwithSel(ddlCategory, 34, Session("DomainID"))
                Dim objSolution As New Solution
                Dim dtSolDetails As DataTable
                objSolution.SolID = lngSolId
                dtSolDetails = objSolution.GetSolutionForEdit
                txtSolution.Text = dtSolDetails.Rows(0).Item("vcSolnTitle")
                txtLink.Text = dtSolDetails.Rows(0).Item("vcLink")
                If Not ddlCategory.Items.FindByValue(dtSolDetails.Rows(0).Item("numCategory")) Is Nothing Then
                    ddlCategory.Items.FindByValue(dtSolDetails.Rows(0).Item("numCategory")).Selected = True
                End If
                txtSolDesc.Text = dtSolDetails.Rows(0).Item("txtSolution")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Sub LoadCases()
        '    Try
        '        ddlCaseNo.Items.Clear()
        '        Dim i As Integer
        '        Dim ObjCases As New CCases
        '        Dim dtCaseNo As DataTable
        '        ObjCases.DivisionID = ddlCompany.SelectedItem.Value
        '        dtCaseNo = ObjCases.GetOpenCases
        '        For i = 0 To dtCaseNo.Rows.Count - 1
        '            ddlCaseNo.Items.Add(New ListItem(dtCaseNo.Rows(i).Item("vcCaseNumber"), dtCaseNo.Rows(i).Item("numCaseId")))
        '        Next
        '        ddlCaseNo.Items.Insert(0, "--Select One--")
        '        ddlCaseNo.Items.FindByText("--Select One--").Value = 0
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Private Sub btnlnkCase_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnlnkCase.Click
        '    Try
        '        Dim str As String()
        '        str = CStr(ddlCaseNo.SelectedItem.Value).Split("~")
        '        Dim objSolution As New Solution
        '        objSolution.CaseID = str(0)
        '        objSolution.SolID = lngSolId
        '        objSolution.LinkSolToCases()
        '        Response.Redirect("../cases/frmCases.aspx?frm=Solution&frm=cases&CaseID=" & str(0) & "&DivID=" & str(1) & "&CntID=" & str(2) & "&SolId=" & lngSolId & "&Index=1")
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub

        Sub Save()
            Try
                Dim objSolution As New Solution
                objSolution.Category = ddlCategory.SelectedItem.Value
                objSolution.SolName = txtSolution.Text
                objSolution.SolDesc = txtSolDesc.Text
                objSolution.SolID = lngSolId
                objSolution.Link = txtLink.Text
                objSolution.UserCntID = Session("UserContactID")
                objSolution.SaveSolution()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Save()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                Save()
                Page_redirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                Page_redirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub Page_redirect()
            Try
                If frm = "frmCases" Then
                    Response.Redirect("../cases/frmCases.aspx?CaseID=" & GetQueryStringVal("CaseID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2, False)
                Else
                    Response.Redirect("../cases/frmKnowledgeBaseList.aspx?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2, False)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        'Private Sub ddlCompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCompany.SelectedIndexChanged
        '    Try
        '        LoadCases()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub

        'Public Function FillDDL()
        '    Try

        '        If objCommon Is Nothing Then objCommon = New CCommon
        '        With objCommon
        '            .DomainID = Session("DomainID")
        '            .Filter = Trim(txtCompName.Text) & "%"
        '            .UserCntID = Session("UserContactID")
        '            ddlCompany.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
        '            ddlCompany.DataTextField = "vcCompanyname"
        '            ddlCompany.DataValueField = "numDivisionID"
        '            ddlCompany.DataBind()
        '            ddlCompany.Items.Insert(0, "--Select One--")
        '            ddlCompany.Items.FindByText("--Select One--").Value = 0
        '        End With
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

        'Private Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        '    Try
        '        FillDDL()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub

    End Class
End Namespace
