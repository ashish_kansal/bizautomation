'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Contract
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Workflow
Namespace BACRM.UserInterface.Cases

    Public Class frmAddCases
        Inherits BACRMPage
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
            Try
                CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

#End Region

        

        Dim dtTableInfo As DataTable
        Dim objPageControls As New PageControls
        Dim objCases As New CaseIP
        Dim objContracts As CContracts
        Dim CaseId As Integer

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    radCmbCompany.Focus()
                     ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "Support"
                    
                    ' Checking the rights for View
                    GetUserRightsForPage(7, 1)
                    If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                        btnSaveClose.Visible = False
                        Exit Sub
                    Else : btnSaveClose.Visible = True
                    End If

                    If CCommon.ToLong(Session("UserContactID")) > 0 AndAlso CCommon.ToLong(Session("UserID")) = 0 Then
                        objCommon.DivisionID = CCommon.ToLong(Session("UserDivisionID"))
                        objCommon.charModule = "D"
                        radCmbCompany.Enabled = False

                        objCommon.GetCompanySpecificValues1()
                        Dim strCompany, strDivision As String
                        strCompany = objCommon.GetCompanyName
                        radCmbCompany.Text = strCompany
                        radCmbCompany.SelectedValue = objCommon.DivisionID
                        'txtSubject.Focus()
                        FillContact()
                        If Not ddlContacts.Items.FindByValue(objCommon.ContactID) Is Nothing Then ddlContacts.Items.FindByValue(objCommon.ContactID).Selected = True
                        BindRecentOrders()
                    ElseIf GetQueryStringVal("uihTR") <> "" Or GetQueryStringVal("fghTY") <> "" Or GetQueryStringVal("rtyWR") <> "" Or GetQueryStringVal("tyrCV") <> "" Or GetQueryStringVal("pluYR") <> "" Then
                        If GetQueryStringVal("uihTR") <> "" Then
                            objCommon.ContactID = CCommon.ToLong(GetQueryStringVal("uihTR"))
                            objCommon.charModule = "C"
                        ElseIf GetQueryStringVal("rtyWR") <> "" Then
                            objCommon.DivisionID = CCommon.ToLong(GetQueryStringVal("rtyWR"))
                            objCommon.charModule = "D"
                        ElseIf GetQueryStringVal("tyrCV") <> "" Then
                            objCommon.ProID = CCommon.ToLong(GetQueryStringVal("tyrCV"))
                            objCommon.charModule = "P"
                        ElseIf GetQueryStringVal("pluYR") <> "" Then
                            objCommon.OppID = CCommon.ToLong(GetQueryStringVal("pluYR"))
                            objCommon.charModule = "O"
                        ElseIf GetQueryStringVal("fghTY") <> "" Then
                            objCommon.CaseID = CCommon.ToLong(GetQueryStringVal("fghTY"))
                            objCommon.charModule = "S"
                        End If
                        objCommon.GetCompanySpecificValues1()
                        Dim strCompany, strDivision As String
                        strCompany = objCommon.GetCompanyName
                        radCmbCompany.Text = strCompany
                        radCmbCompany.SelectedValue = objCommon.DivisionID
                        'txtSubject.Focus()
                        FillContact()
                        If Not ddlContacts.Items.FindByValue(objCommon.ContactID) Is Nothing Then ddlContacts.Items.FindByValue(objCommon.ContactID).Selected = True
                        BindRecentOrders()
                    End If
                    If GetQueryStringVal("frm") = "outlook" Then
                        Dim objOutlook As New COutlook
                        Dim dttable As DataTable
                        objOutlook.numEmailHstrID = CCommon.ToLong(GetQueryStringVal("EmailHstrId"))
                        objOutlook.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                        dttable = objOutlook.getMail()
                        If dttable.Rows.Count > 0 Then
                            'If dttable.Rows(0).Item("Source") = "I" Then
                            '    Dim objEmails As New Email
                            '    Dim dt As DataTable
                            '    dt = objEmails.GetEmail(GetQueryStringVal( "EmailHstrId"), Session("DomainId"))
                            '    If dt.Rows.Count > 0 Then txtDescription.Text = dt.Rows(0).Item("BodyText")
                            'Else
                            'txtDescription.Text = dttable.Rows(0).Item("vcBodyText")
                            'End If
                            'txtSubject.Text = dttable.Rows(0).Item("Subject")
                        End If
                    End If

                    LoadControls()
                End If
                btnSaveClose.Attributes.Add("onclick", "return Save()")
                btnClose.Attributes.Add("onclick", "return Close()")

                If IsPostBack Then
                    LoadControls()
                End If

                DisplayDynamicFlds()

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub DisplayDynamicFlds()
            Try
                objPageControls.DisplayDynamicFlds(CaseId, 0, Session("DomainID"), objPageControls.Location.Support)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub LoadControls()
            Try
                tblMain.Controls.Clear()
                Dim ds As DataSet
                Dim objPageLayout As New CPageLayout
                Dim fields() As String

                objPageLayout.UserCntID = 0
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.PageId = 3
                objPageLayout.numRelCntType = 0
                objPageLayout.FormId = 12
                objPageLayout.PageType = 2

                ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
                dtTableInfo = ds.Tables(0)

                Dim intCol1Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=1")
                Dim intCol2Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=2")
                Dim intCol1 As Int32 = 0
                Dim intCol2 As Int32 = 0
                Dim tblCell As TableCell

                Dim tblRow As TableRow
                Const NoOfColumns As Short = 2
                Dim ColCount As Int32 = 0

                ''If intermediatory Page is enabled then Add the text "Name" to dropdown fields
                objPageControls.CreateTemplateRow(tblMain)
                For Each dr As DataRow In dtTableInfo.Rows
                    If (dr("fld_type") <> "Popup" And dr("fld_type") <> "Label" And dr("fld_type") <> "Website") Then

                        If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False Then
                            dr("vcValue") = objCases.GetType.GetProperty(dr("vcPropertyName")).GetValue(objCases, Nothing)
                        End If

                        If ColCount = 0 Then
                            tblRow = New TableRow
                        End If

                        If intCol1Count = intCol1 And intCol2Count > intCol1Count Then
                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)
                            ColCount = 1
                        End If

                        If (dr("fld_type") = "ListBox" Or dr("fld_type") = "SelectBox") Then
                            Dim dtData As DataTable
                            'Dim dtSelOpportunity As DataTable

                            If Not IsDBNull(dr("numListID")) And dr("numListID") <> 0 Then
                                If dr("ListRelID") > 0 Then
                                    objCommon.Mode = 3
                                    objCommon.DomainID = Session("DomainID")
                                    objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objCases)
                                    objCommon.SecondaryListID = dr("numListId")
                                    dtData = objCommon.GetFieldRelationships.Tables(0)
                                Else
                                    dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                End If
                            End If

                            If dr("vcPropertyName") = "AssignTo" Then
                                If Session("PopulateUserCriteria") = 1 Then
                                    dtData = objCommon.ConEmpListFromTerritories(Session("DomainID"), 0, 0, 0)

                                ElseIf Session("PopulateUserCriteria") = 2 Then
                                    dtData = objCommon.ConEmpList(Session("DomainID"), Session("UserContactID"))
                                Else
                                    dtData = objCommon.ConEmpList(Session("DomainID"), 0, 0)
                                End If

                                'Commissionable Contacts
                                Dim objUser As New UserAccess
                                objUser.DomainID = Session("DomainID")
                                objUser.byteMode = 1

                                Dim dt As DataTable = objUser.GetCommissionsContacts

                                Dim item As ListItem
                                Dim dr1 As DataRow

                                For Each dr2 As DataRow In dt.Rows
                                    dr1 = dtData.NewRow
                                    dr1(0) = dr2("numContactID")
                                    dr1(1) = dr2("vcUserName")
                                    dtData.Rows.Add(dr1)
                                Next
                            End If

                            Dim ddl As DropDownList
                            ddl = objPageControls.CreateCells(tblRow, dr, False, dtData, boolAdd:=True)
                            If CLng(dr("DependentFields")) > 0 And Not False Then
                                ddl.AutoPostBack = True
                                AddHandler ddl.SelectedIndexChanged, AddressOf PopulateDependentDropdown
                            End If
                        Else
                            If dr("fld_type") = "DateField" Then
                                dr("vcValue") = Date.Now
                            End If
                            objPageControls.CreateCells(tblRow, dr, False, RecordID:=0, boolAdd:=True)
                        End If


                        If intCol2Count = intCol2 And intCol1Count > intCol2Count Then
                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            ColCount = 1
                        End If


                        If dr("intcoulmn") = 2 Then
                            If intCol1 <> intCol1Count Then
                                intCol1 = intCol1 + 1
                            End If

                            If intCol2 <> intCol2Count Then
                                intCol2 = intCol2 + 1
                            End If
                        End If

                        ColCount = ColCount + 1

                        If NoOfColumns = ColCount Then
                            ColCount = 0
                            tblMain.Rows.Add(tblRow)
                        End If
                    End If
                Next

                If ColCount > 0 Then
                    tblMain.Rows.Add(tblRow)
                End If

                'objPageControls.CreateComments(tblMain, objLeads.Comments, False)

                'Add Client Side validation for custom fields
                Dim strValidation As String = objPageControls.GenerateValidationScript(dtTableInfo)
                ClientScript.RegisterClientScriptBlock(Me.GetType, "CFvalidation", strValidation, True)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub PopulateDependentDropdown(ByVal sender As Object, ByVal e As EventArgs)
            objPageControls.PopulateDropdowns(CType(sender, DropDownList), dtTableInfo, objCommon, tblMain, Session("DomainID"))
        End Sub


        Public Function FillContact()
            Try
                Dim fillCombo As New COpportunities
                With fillCombo
                    .DivisionID = radCmbCompany.SelectedValue
                    ddlContacts.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                    ddlContacts.DataTextField = "Name"
                    ddlContacts.DataValueField = "numcontactId"
                    ddlContacts.DataBind()
                End With
                ddlContacts.Items.Insert(0, New ListItem("---Select One---", "0"))
                If ddlContacts.Items.Count = 2 Then
                    ddlContacts.Items(1).Selected = True
                End If
            Catch ex As Exception
                Throw
            End Try
        End Function


        Private Function GetOppItemsAsString() As String
            Try
                Dim ds As New DataSet
                Dim dt As New DataTable
                dt.Columns.Add("numOppId")
                dt.Columns.Add("numOppItemID")
                Dim dr As DataRow
                Dim en As IEnumerator
                'Dim row As UltraGridRow
                'For Each item As UltraGridRow In gvOrders.Rows
                '    'If CCommon.ToBool(item.Cells(0).Value) = True Then
                '    en = item.Rows.GetEnumerator()
                '    While en.MoveNext()
                '        row = en.Current
                '        If CCommon.ToBool(row.Cells(0).Value) = True Then
                '            dr = dt.NewRow
                '            dr("numOppId") = item.Cells(0).Value.ToString
                '            dr("numOppItemID") = row.Cells(1).Value.ToString
                '            dt.Rows.Add(dr)
                '        End If
                '    End While
                '    'End If
                'Next

                For Each drOrder As GridDataItem In gvOrders.Items
                    If drOrder.HasChildItems Then
                        Dim item As GridNestedViewItem = drOrder.ChildItem()

                        For Each drItem As GridDataItem In item.NestedTableViews(0).Items
                            If CType(drItem.FindControl("chkSelect"), CheckBox).Checked = True Then
                                dr = dt.NewRow
                                dr("numOppId") = drItem.GetDataKeyValue("numOppId")
                                dr("numOppItemID") = drItem.GetDataKeyValue("numoppitemtCode")
                                dt.Rows.Add(dr)
                            End If
                        Next
                    End If
                Next

                ds.Tables.Add(dt)
                Return ds.GetXml()
            Catch ex As Exception
                Throw
            End Try
        End Function
        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try

                Dim CaseNo As Long

                CaseNo = objCases.GetMaxCaseID

                With objCases
                    ''Modified by Shaziya on 28th March 07
                    ''Support key Type has been removed

                    'If radDealSupportKey.Checked = True Then
                    '    .OppID = ddlDealSupport.SelectedItem.Value
                    'End If

                    .CaseNo = Format(CaseNo, "00000000000")
                    .DivisionID = radCmbCompany.SelectedValue
                    .ContactID = ddlContacts.SelectedItem.Value
                    '.Subject = txtSubject.Text
                    '.Desc = txtDescription.Text
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                    '.TargetResolveDate = calResolve.SelectedDate

                    ''Modified By Shaziya On 27th March 07
                    ''SupportKeyType is by default Universal 

                    .SupportKeyType = 0

                    'If radUniversalSupportKey.Checked = True Then
                    '    .SupportKeyType = 0
                    'Else
                    '    .SupportKeyType = 1
                    'End If

                    For Each dr As DataRow In dtTableInfo.Rows
                        If (dr("fld_type") <> "Popup" And dr("fld_type") <> "Label" And dr("fld_type") <> "Website") Then
                            If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False And dr("bitCanBeUpdated") = True And dr("vcPropertyName") <> "ContactID" Then
                                objPageControls.SetValueForStaticFields(dr, objCases, tblMain)
                            End If
                        End If
                    Next
                End With
                If objCases.TargetResolveDate.Year = 1753 Then
                    objCases.TargetResolveDate = Date.Now
                End If

                CaseId = objCases.SaveCases


                objCases.CaseID = CaseId

                'Added By Sachin Sadhu||Date:30Jul2014
                'Purpose :To Added Projects data in work Flow queue based on created Rules
                '          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = CCommon.ToLong(CaseId)
                objWfA.SaveWFCasesQueue()
                'end of code


                objCases.DomainID = Session("DomainID")
                objCases.strOppSel = GetOppItemsAsString()
                objCases.SaveCaseOpportunities()

                SaveCusField()

                ''Sending mail to Account holder and Manager
                Try
                    Dim objAlerts As New CAlerts
                    Dim dtDetails As DataTable
                    objAlerts.AlertDTLID = 11 'Alert DTL ID for sending alerts in opportunities
                    objAlerts.DomainID = Session("DomainID")
                    dtDetails = objAlerts.GetIndAlertDTL
                    If dtDetails.Rows.Count > 0 Then
                        If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
                            Dim dtEmailTemplate As DataTable
                            Dim objDocuments As New DocumentList
                            objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
                            objDocuments.DomainID = Session("DomainId")
                            dtEmailTemplate = objDocuments.GetDocByGenDocID
                            If dtEmailTemplate.Rows.Count > 0 Then
                                Dim objSendMail As New Email
                                Dim dtMergeFields As New DataTable
                                Dim drNew As DataRow
                                dtMergeFields.Columns.Add("CaseID")
                                dtMergeFields.Columns.Add("Organization")
                                dtMergeFields.Columns.Add("Employee")
                                drNew = dtMergeFields.NewRow
                                drNew("CaseID") = CaseId
                                drNew("Organization") = radCmbCompany.Text
                                drNew("Employee") = Session("ContactName")
                                dtMergeFields.Rows.Add(drNew)

                                objCommon.byteMode = 0
                                objCommon.ContactID = Session("UserContactID")
                                objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtDetails.Rows(0).Item("tintCCManager") = 1, objCommon.GetManagerEmail, ""), Session("UserEmail"), Session("UserEmail"), dtMergeFields)
                            End If
                        End If
                    End If
                Catch ex As Exception

                End Try
                Dim strScript As String = "<script language=JavaScript>"
                strScript += "window.opener.reDirectPage('../cases/frmCases.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&CaseID=" & CaseId & "'); self.close();"
                strScript += "</script>"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub SaveCusField()
            Try
                objPageControls.SaveCusField(CaseId, 0, Session("DomainID"), objPageControls.Location.Support, tblMain)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        ''Modified By Shaziya On 27th March 07
        ''SupportKeyType is by default Universal 
        'Private Sub radDealSupportKey_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radDealSupportKey.CheckedChanged
        '    If radDealSupportKey.Checked = True Then
        '        pnlDeal.Visible = True
        '    End If
        '    ddlDealSupport.Items.Clear()
        '    Dim objCases As New CCases
        '    Dim dtDealSupportKey As New DataTable
        '    Dim i As Integer
        '    If ddlCompany.SelectedIndex > 0 Then
        '        objCases.DivisionID = ddlCompany.SelectedItem.Value
        '        dtDealSupportKey = objCases.GetDealSupportKey
        '    End If
        '    For i = 0 To dtDealSupportKey.Rows.Count - 1
        '        ddlDealSupport.Items.Add(New ListItem("D" & Format(dtDealSupportKey.Rows(i).Item("numOppID"), "00000000000"), dtDealSupportKey.Rows(i).Item("numOppID")))
        '    Next
        '    ddlDealSupport.Items.Insert(0, "--Select One--")
        '    ddlDealSupport.Items.FindByText("--Select One--").Value = 0
        'End Sub

        'Private Sub radUniversalSupportKey_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radUniversalSupportKey.CheckedChanged
        '    If radUniversalSupportKey.Checked = True Then
        '        pnlDeal.Visible = False
        '    End If
        'End Sub

        Public Function StripTags(ByVal HTML As String) As String
            Try
                ' Removes tags from passed HTML
                Dim objRegEx As System.Text.RegularExpressions.Regex
                Dim str As String = objRegEx.Replace(HTML, "<[^>]*>", "").Replace("P {", "")
                str = str.Replace("MARGIN-TOP: 0px; MARGIN-BOTTOM: 0px", "")
                Return str.Trim
            Catch ex As Exception
                Throw
            End Try
        End Function

        'Get top 5 orders placed by selected customer
        Private Sub BindRecentOrders()
            Try
                Dim objOpp As New COpportunities
                Dim ds As DataSet
                objOpp.DomainID = Session("DomainID")
                objOpp.ContactID = CCommon.ToLong(ddlContacts.SelectedValue)
                objOpp.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                objOpp.OppType = 1 'sales orders
                objOpp.PageSize = 5
                ds = objOpp.GetTopOrders()
                If ds.Relations.Count = 0 And ds.Tables.Count = 2 Then
                    ds.Tables(0).TableName = "Orders"
                    ds.Tables(1).TableName = "OrderItems"
                    ds.Relations.Add("Orders", ds.Tables(0).Columns("numOppId"), ds.Tables(1).Columns("numOppId"), False)
                End If
                gvOrders.DataSource = ds
                gvOrders.DataBind()
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub ddlContacts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContacts.SelectedIndexChanged
            Try
                If ddlContacts.SelectedValue <> "" Then
                    BindRecentOrders()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        ''RadioButton code KB http://devcenter.infragistics.com/Support/KnowledgeBaseArticle.aspx?ArticleID=7945

        'Private Sub uwOrders_InitializeLayout(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.LayoutEventArgs) Handles uwOrders.InitializeLayout
        '    e.Layout.Bands(0).Columns.Insert(0, "RADIO")
        '    e.Layout.Bands(0).Columns.FromKey("RADIO").Header.ClickAction = HeaderClickAction.Select
        '    e.Layout.Bands(0).Columns(0).Width = 20
        'End Sub

        'Private Sub initRadioButtons()
        '    If (Me.uwOrders.Rows.Count > 0) Then
        '        'Add the HTML to create the radio button and connect the onclick event to our JavaScript
        '        For i As Integer = 0 To Me.uwOrders.Rows.Count - 1
        '            Me.uwOrders.Rows(i).Cells.FromKey("RADIO").Value = _
        '             "<input type=radio name='TheRadioGroup' onclick='activateGridRow(" + _
        '             Me.uwOrders.Rows(i).Index.ToString() + ");'>"
        '        Next
        '    End If
        'End Sub

        'Private Sub uwOrders_SortColumn(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.SortColumnEventArgs) Handles uwOrders.SortColumn
        '    'Sorting throws off the Index, so set the radio buttons up again in this event
        '    Me.initRadioButtons()
        'End Sub

        Private Sub radCmbCompany_SelectedIndexChanged1(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
            Try
                If radCmbCompany.SelectedValue <> "" Then
                    FillContact()
                    BindRecentOrders()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class
End Namespace