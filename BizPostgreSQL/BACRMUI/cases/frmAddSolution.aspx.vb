'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Cases

Public Class frmAddSolution
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnSaveClose As System.Web.UI.WebControls.Button
        Protected WithEvents tbl As System.Web.UI.WebControls.Table
        Protected WithEvents ddlCaseNo As System.Web.UI.WebControls.DropDownList
        Protected WithEvents txtSolution As System.Web.UI.WebControls.TextBox
        Protected WithEvents txtSolDesc As System.Web.UI.WebControls.TextBox
        Protected WithEvents ddlCategory As System.Web.UI.WebControls.DropDownList
        Protected WithEvents btnlnkCase As System.Web.UI.WebControls.Button

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                    
                    ' Checking the rights for View
                   
                    GetUserRightsForPage(7, 6)
                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS")
                    If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                        btnSaveClose.Visible = False
                        Exit Sub
                    Else : btnSaveClose.Visible = True
                    End If
                     ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "Support"
                    objCommon.sb_FillComboFromDBwithSel(ddlCategory, 34, Session("DomainID"))
                End If
                btnSaveClose.Attributes.Add("onclick", "return Save()")
                btnClose.Attributes.Add("onclick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                Dim objSolution As New Solution
                Dim lngSol As Long
                objSolution.Category = ddlCategory.SelectedItem.Value
                objSolution.SolName = txtSolution.Text
                objSolution.SolDesc = txtSolDesc.Text
                objSolution.DomainID = Session("DomainID")
                lngSol = objSolution.SaveSolution()
                Dim strScript As String = "<script language=JavaScript>"
                strScript += "window.opener.reDirectPage('../cases/frmSolution.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&SolId=" & lngSol & "'); self.close();"
                strScript += "</script>"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
