﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Case

Namespace BACRM.UserInterface.Cases
    Public Class frmAssignCaseOwner
        Inherits BACRMPage

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then sb_FillEmpList()

                btnCancel.Attributes.Add("onclick", "return Close()")
                btnTransfer.Attributes.Add("onclick", "return check()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub sb_FillEmpList()
            Try
                Dim dtUser As New DataTable
                If Session("PopulateUserCriteria") = 1 Then
                    objCommon.CaseID = CCommon.ToLong(GetQueryStringVal("fghTY"))
                    objCommon.charModule = "S"
                    objCommon.GetCompanySpecificValues1()

                    dtUser = objCommon.ConEmpListFromTerritories(Session("DomainID"), False, 0, objCommon.TerittoryID)
                ElseIf Session("PopulateUserCriteria") = 2 Then
                    dtUser = objCommon.ConEmpList(Session("DomainID"), Session("UserContactID"))
                Else
                    dtUser = objCommon.ConEmpList(Session("DomainID"), 0, 0)
                End If

                ddlRecordOwner.DataSource = dtUser
                ddlRecordOwner.DataTextField = "vcUserName"
                ddlRecordOwner.DataValueField = "numContactID"
                ddlRecordOwner.DataBind()
                ddlRecordOwner.Items.Insert(0, New ListItem("--Select One--", "0"))

                ddlAssignTo.DataSource = dtUser
                ddlAssignTo.DataTextField = "vcUserName"
                ddlAssignTo.DataValueField = "numContactID"
                ddlAssignTo.DataBind()
                ddlAssignTo.Items.Insert(0, New ListItem("--Select One--", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnTransfer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTransfer.Click
            Try
                Dim objCases As New CaseIP
                objCases.AssignTo = ddlAssignTo.SelectedValue
                objCases.CaseID = CCommon.ToLong(GetQueryStringVal("CaseID"))
                objCases.DomainID = Session("DomainID")
                objCases.AssignCaseRecOwner(ddlRecordOwner.SelectedValue)
                litMessage.Text = "Sucessfully transfered"

                Response.Write("<script language='javascript'>opener.location.reload(true); self.close();</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace