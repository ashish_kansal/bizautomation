﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmAssignCaseOwner.aspx.vb" Inherits="BACRM.UserInterface.Cases.frmAssignCaseOwner" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
 <title>Case Assign Record Owner</title>
    <script type="text/javascript">
        function check() {
            if (document.form1.ddlRecordOwner.value == 0) {
                alert("Please Select Record Owner")
                document.form1.ddlRecordOwner.focus()
                return false;
            }
            if (document.form1.ddlAssignTo.value == 0) {
                alert("Please Select Assign To")
                document.form1.ddlAssignTo.focus()
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnTransfer" runat="server" Text="Save & Close" CssClass="button"></asp:Button>
            <asp:Button ID="btnCancel" runat="server" Text="Close" CssClass="button"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblTab" runat="server" Text="Assign Case Record Owner & Assignee"></asp:Label>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="600px">
        <tr>
            <td class="normal4" align="center" colspan="2">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Owner
            </td>
            <td class="normal1">
                <asp:DropDownList ID="ddlRecordOwner" CssClass="signup" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Assignee
            </td>
            <td class="normal1">
                <asp:DropDownList ID="ddlAssignTo" CssClass="signup" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
</asp:Content>

