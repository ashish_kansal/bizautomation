<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmAddSolution.aspx.vb"
    Inherits="BACRM.UserInterface.Cases.frmAddSolution" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Solution</title>
    <script language="javascript" type="text/javascript">

        $(document).ready(
 function () {
     document.getElementById('txtSolution').focus();
 }
);
        function Save() {

            if (document.getElementById('txtSolution').value == "") {
                alert("Enter Solution Name")
                document.getElementById('txtSolution').focus();
                return false;
            }
            if (document.getElementById('ddlCategory').value == 0) {
                alert("Select Category")
                document.getElementById('ddlCategory').focus();
                return false;
            }
            if (document.getElementById('txtSolDesc').value == "") {
                alert("Enter Solution Description")
                document.getElementById('txtSolDesc').focus();
                return false;
            }
        }
        function Close() {
            window.close()
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="right-input">
        <div class="input-part">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSaveClose" CssClass="button" Text="Save &amp; Close" runat="server">
                        </asp:Button>
                        &nbsp;
                        <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="50">
                        </asp:Button>
                        &nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    New Solution
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="tbl" runat="server" GridLines="None" BorderColor="black" Width="600px"
        BorderWidth="1" CssClass="aspTable" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell>
                <br>
                <table border="0" width="100%">
                    <tr>
                        <td class="normal1" align="right">
                            Soluton Name<font color="red">*</font>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSolution" CssClass="signup" runat="server" Width="400"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Solution Category<font color="red">*</font>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCategory" runat="server" Width="130" CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">
                            Solution Description<font color="red">*</font>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSolDesc" CssClass="signup" runat="server" TextMode="MultiLine"
                                Width="500" Height="200"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <br>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
