﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master"
    CodeBehind="frmKnowledgeBaseList.aspx.vb" Inherits="BACRM.UserInterface.Cases.frmKnowledgeBaseList" %>

<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        function DeleteRecord1() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <div class="row">
        <div class="col-md-12">
            <div class="pull-right form-group form-inline">

                <label>
                    No of Records:
                </label>
                <asp:Label ID="lblRecordSolution" runat="server"></asp:Label>
                <label>
                    Solution Category
                </label>
                <asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control" AutoPostBack="True">
                </asp:DropDownList>
                <label>
                    Find Solution using Name or Keyword</label>
                <asp:TextBox ID="txtKeyWord" runat="server" CssClass="form-control" Width="200"></asp:TextBox>
                <asp:LinkButton ID="btnGoBases" runat="server" CssClass="btn btn-primary">Go
                </asp:LinkButton>
                <a href="#" class="help">&nbsp;</a>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <table cellspacing="0" cellpadding="2" width="100%" border="0">
        <tr style="line-height: 43px">
            <td class="normal4" align="left">
                <asp:Label ID="lblRecordCountCases" Visible="false" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Knowledge Base&nbsp;<a href="#" onclick="return OpenHelpPopUp('cases/frmknowledgebaselist.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
    &nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:DataGrid ID="dgBases" runat="server" Width="100%" CssClass="table table-striped table-bordered" AllowSorting="True"
                    AutoGenerateColumns="False" UseAccessibleHeader="true">
                    <Columns>
                        <asp:TemplateColumn HeaderText="Solution Name">
                            <ItemTemplate>
                                <asp:Label ID="lblNumSolID" runat="server" Text='<%#Eval("numSolnID") %>' Visible="false"></asp:Label>
                                <asp:HyperLink ID="hlSolutionName" Text='<%# Eval("vcSolnTitle") %>' NavigateUrl='<%#Eval("numSolnID", "../cases/frmSolution.aspx?SolId={0}") %>' runat="server">
                                </asp:HyperLink>

                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="<font>Solution Description</font>">
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text='<%# CCommon.Truncate(Eval("txtSolution").toString(),100) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>

                        <asp:TemplateColumn>
                            <ItemStyle Width="25px"></ItemStyle>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnSolDelete" runat="server" CssClass="btn btn-danger btn-xs" CommandName="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                                <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
													<font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>

    <asp:TextBox ID="txtDelCaseId" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
