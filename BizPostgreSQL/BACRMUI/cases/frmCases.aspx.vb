'Created By Anoop Jayaraj
Imports System.IO
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Workflow

Namespace BACRM.UserInterface.Cases
    Public Class frmCases
        Inherits BACRMPage

        Dim objContacts As CContacts
        Dim objCases As New CaseIP
        Dim objSolution As Solution
        Dim objCus As CustomFields
        Dim objContracts As CContracts
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim lngCaseId As Long
        Dim lngCntId As Long
        Dim m_aryRightsForCusFlds(), m_aryRightsForViewLayoutButton() As Integer 'm_aryRightsForLinkSol()
        Dim strColumn As String
        Dim boolIntermediatoryPage As Boolean = False
        Dim dtCustomFieldTable As DataTable
        Dim dtTableInfo As DataTable
        Dim objPageControls As New PageControls

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    If Session("EnableIntMedPage") = 1 Then
                        ViewState("IntermediatoryPage") = True
                    Else
                        ViewState("IntermediatoryPage") = False
                    End If

                    m_aryRightsForViewLayoutButton = GetUserRightsForPage_Other(7, 12)
                    If m_aryRightsForViewLayoutButton(RIGHTSTYPE.VIEW) = 0 Then
                        btnLayout.Visible = False
                    End If


                End If
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                boolIntermediatoryPage = ViewState("IntermediatoryPage")
                ControlSettings()
                If GetQueryStringVal("SI") <> "" Then
                    SI = GetQueryStringVal("SI")
                End If
                If GetQueryStringVal("SI1") <> "" Then
                    SI1 = GetQueryStringVal("SI1")
                Else : SI1 = 0
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    SI2 = GetQueryStringVal("SI2")
                Else : SI2 = 0
                End If
                If GetQueryStringVal("frm") <> "" Then
                    frm = ""
                    frm = GetQueryStringVal("frm")
                Else : frm = ""
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = ""
                    frm1 = GetQueryStringVal("frm1")
                Else : frm1 = ""
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    frm2 = ""
                    frm2 = GetQueryStringVal("frm2")
                Else : frm2 = ""
                End If
                lngCaseId = CCommon.ToLong(GetQueryStringVal("CaseID"))
                Dim hdnCollabarationLoggedInUser As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnLoggedInUser"), HiddenField)
                Dim hdnCollabarationRecordId As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnRecordId"), HiddenField)
                Dim hdnCollabarationRecordType As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnRecordType"), HiddenField)
                Dim hdnCollabarationDomainId As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnDomainId"), HiddenField)
                Dim hdnCollabarationPortalDocUrl As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnCollabarationPortalDocUrl"), HiddenField)
                Dim hdnClientMachineUTCTimeOffset As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnClientMachineUTCTimeOffset"), HiddenField)
                hdnCollabarationLoggedInUser.Value = CCommon.ToString(Session("UserContactID"))
                hdnCollabarationDomainId.Value = CCommon.ToString(Session("DomainID"))
                hdnClientMachineUTCTimeOffset.Value = CCommon.ToString(Session("ClientMachineUTCTimeOffset"))
                hdnCollabarationRecordType.Value = "2"
                hdnCollabarationRecordId.Value = CCommon.ToString(lngCaseId)
                hdnCollabarationPortalDocUrl.Value = CCommon.GetDocumentPath(Convert.ToInt64(Session("DomainID")))
                frmAssociatedContacts1.TypeId = lngCaseId

                Correspondence1.lngRecordID = lngCaseId
                Correspondence1.Mode = 6

                btnSave.Attributes.Add("onclick", "return Save()")
                btnSaveClose.Attributes.Add("onclick", "return Save()")
                GetUserRightsForPage(7, 3)

                If Not IsPostBack Then
                    hdnCaseID.Value = lngCaseId
                    AddToRecentlyViewed(RecetlyViewdRecordType.Support, lngCaseId)

                    Session("show") = 1
                    ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "Support"
                    ''''sub-tab management  - added on 16/09/2009 by chintan
                    objCommon.GetAuthorizedSubTabs(radOppTab, Session("DomainID"), 3, Session("UserGroupID"))
                    '''''
                    If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                        btnSave.Visible = False
                        btnSaveClose.Visible = False
                        ibChangeCustomer.Visible = False
                    End If
                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnActDelete.Visible = False
                    LoadInformation()
                    LoadDetails()
                    LoadDueCaseList()
                    PersistTable.Load(boolOnlyURL:=True)
                    If PersistTable.Count > 0 Then
                        radOppTab.SelectedIndex = CCommon.ToLong(PersistTable(PersistKey.SelectedTab))
                        If PersistTable("chkPersistCase") IsNot Nothing Then
                            chkPersistCase.Checked = CCommon.ToBool(PersistTable("chkPersistCase"))
                            If CCommon.ToBool(PersistTable("chkPersistCase")) = True Then
                                sectionDueCase.Attributes.Add("style", "display:inline")
                            Else
                                sectionDueCase.Attributes.Add("style", "display:none")
                            End If
                        Else
                            chkPersistCase.Checked = True
                            sectionDueCase.Attributes.Add("style", "display:inline")
                        End If
                    Else
                        chkPersistCase.Checked = True
                        sectionDueCase.Attributes.Add("style", "display:inline")
                    End If

                    If radOppTab.Tabs.Count > SI AndAlso SI > 0 Then radOppTab.SelectedIndex = SI

                    radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True

                    If radOppTab.SelectedIndex = 2 Then
                        Correspondence1.lngRecordID = lngCaseId
                        Correspondence1.Mode = 6
                        Correspondence1.CorrespondenceModule = 7
                        Correspondence1.getCorrespondance()
                    End If

                End If

                If IsPostBack Then
                    LoadInformation()
                    LoadDueCaseList()
                End If
                m_aryRightsForCusFlds = GetUserRightsForPage_Other(7, 5)
                If m_aryRightsForCusFlds(RIGHTSTYPE.VIEW) <> 0 Then
                    DisplayDynamicFlds()
                End If
                frmComments1.CaseID = lngCaseId
                btnLayout.Attributes.Add("onclick", "return ShowLayout('S','" & lngCaseId & "','12');")
                'btnLinkSol.Attributes.Add("onclick", "return openSol(" & lngCaseId & ")")
                btnActDelete.Attributes.Add("onclick", "return DeleteRecord()")
                'btnAddContact.Attributes.Add("onclick", "return AddContact()")
                btnAddItem.Attributes.Add("onclick", "return OpenItems(" & lngCaseId & ");")
                hplTransfer.Attributes.Add("onclick", "return OpenTransfer('" & "../admin/transferrecord.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Cases&fghTY=" & lngCaseId & "')")
                ' ClientScript.RegisterClientScriptBlock(Me.GetType, "SetTab", "if (parent.parent.frames.length > 0) { parent.parent.SelectTabByValue('4');}else{ parent.SelectTabByValue('4'); } ", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub LoadDetails()
            Try
                If radOppTab.SelectedIndex = 0 Then
                    If IsPostBack Then LoadInformation()
                    'ElseIf radOppTab.SelectedIndex= 3 Then
                    '    EmailHistory()
                ElseIf radOppTab.SelectedIndex = 2 Then
                    Correspondence1.lngRecordID = lngCaseId
                    Correspondence1.Mode = 6
                    Correspondence1.CorrespondenceModule = 7
                    Correspondence1.getCorrespondance()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub BindSolutions()
            Try
                If objSolution Is Nothing Then objSolution = New Solution
                Dim dtSolution As DataTable
                objSolution.CaseID = lngCaseId
                dtSolution = objSolution.GetSolutionForCases
                dgSolution.DataSource = dtSolution
                dgSolution.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadInformation()
            Try
                objCases.CaseID = lngCaseId
                objCases.DomainID = Session("DomainID")
                objCases.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objCases.GetCaseDetails()

                'Added by Neelam on 02/16/2018 - Fetch and displays document and link name on the page*/
                GetDocumentFilesLink()

                If Not IsPostBack Then

                    Dim dtCaseDetails As DataTable
                    dtCaseDetails = objCases.GetCaseDTL

                    If dtCaseDetails.Rows.Count > 0 Then
                        hplCustomer.Text = dtCaseDetails.Rows(0).Item("vcCompanyName")
                        lblWarrantyDaysLeft.Text = dtCaseDetails.Rows(0).Item("numWarrantyDaysLeft")
                        lblWarrantyTooltip.Attributes.Add("title", dtCaseDetails.Rows(0).Item("vcContractNotes"))
                        lblWarrantyItemName.Text = dtCaseDetails.Rows(0).Item("vcWarrantyItemName")
                        hrefWarrantyImageName.Attributes.Add("href", "javascript:OpenItem(" & dtCaseDetails.Rows(0).Item("numWarrantyItemCode") & ")")
                        lblIncidentLeft.Text = dtCaseDetails.Rows(0).Item("numIncidentLeft")
                        If CCommon.ToString(dtCaseDetails.Rows(0).Item("vcName")) <> "" Then
                            lblContactName.Text = dtCaseDetails.Rows(0).Item("vcName")
                        Else
                            lblContactName.Text = ""
                        End If
                        If CCommon.ToBool(HttpContext.Current.Session("bitDisplayContractElement")) = True AndAlso CheckUserExist(Convert.ToString(HttpContext.Current.Session("UserContactID")), Convert.ToString(HttpContext.Current.Session("vcEmployeeForContractTimeElement"))) Then
                            divContractsDetails.Visible = True
                        Else
                            divContractsDetails.Visible = False
                        End If
                        If CCommon.ToString(dtCaseDetails.Rows(0).Item("numPhone")) <> "" Then
                            lblPhone.Text = dtCaseDetails.Rows(0).Item("numPhone")
                        Else
                            lblPhone.Text = ""
                        End If
                        If CCommon.ToString(dtCaseDetails.Rows(0).Item("PhoneExtension")) <> "" Then
                            lblPhone.Text = lblPhone.Text & "(" & CCommon.ToString(dtCaseDetails.Rows(0).Item("PhoneExtension")) & ")"
                        End If
                        If CCommon.ToString(dtCaseDetails.Rows(0).Item("vcEmail")) <> "" Then
                            btnSendEmail.Visible = True
                            btnSendEmail.Attributes.Add("onclick", "return OpenContEmail('" & CCommon.ToString(dtCaseDetails.Rows(0).Item("vcEmail")) & "','" & dtCaseDetails.Rows(0).Item("numContactID") & "')")
                        Else
                            btnSendEmail.Visible = False
                        End If
                        If CCommon.ToString(dtCaseDetails.Rows(0).Item("vcCompanyType")) <> "" Then
                            lblRelationCustomerType.Text = "(" & dtCaseDetails.Rows(0).Item("vcCompanyType") & ")"
                        Else
                            lblRelationCustomerType.Text = ""
                        End If

                        txtCntId.Text = dtCaseDetails.Rows(0).Item("numContactID")
                        lngCntId = dtCaseDetails.Rows(0).Item("numContactID")
                        txtDivId.Text = dtCaseDetails.Rows(0).Item("numDivisionID")
                        Session("DivisionID") = txtDivId.Text

                        If CCommon.ToLong(dtCaseDetails.Rows(0).Item("numAssignedTo")) <> CCommon.ToLong(Session("UserContactID")) AndAlso CCommon.ToLong(Session("UserDivisionID")) <> CCommon.ToLong(dtCaseDetails.Rows(0).Item("numDivisionID")) Then
                            GetUserRightsForRecord(MODULEID.Support, 3, CCommon.ToLong(dtCaseDetails.Rows(0).Item("numRecOwner")), CCommon.ToLong(dtCaseDetails.Rows(0).Item("numTerID")))
                        End If

                        If dtCaseDetails.Rows(0).Item("tintCRMType") = 0 Then
                            hplCustomer.NavigateUrl = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CaseDetail&DivID=" & dtCaseDetails.Rows(0).Item("numDivisionID") & "&CaseID=" & lngCaseId & "&frm1=" & GetQueryStringVal("frm")

                        ElseIf dtCaseDetails.Rows(0).Item("tintCRMType") = 1 Then
                            hplCustomer.NavigateUrl = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CaseDetail&DivID=" & dtCaseDetails.Rows(0).Item("numDivisionID") & "&CaseID=" & lngCaseId & "&frm1=" & GetQueryStringVal("frm")

                        ElseIf dtCaseDetails.Rows(0).Item("tintCRMType") = 2 Then
                            hplCustomer.NavigateUrl = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CaseDetail&klds+7kldf=fjk-las&DivId=" & dtCaseDetails.Rows(0).Item("numDivisionID") & "&CaseID=" & lngCaseId & "&frm1=" & GetQueryStringVal("frm")

                        End If

                        'lblLinkedItemCount.Text = objCases.LinkedItemCount
                        'If dtCaseDetails.Rows(0).Item("tintSupportKeyType") = 0 Then
                        '    If Not IsDBNull(dtCaseDetails.Rows(0).Item("numUniversalSupportKey")) Then
                        '        lblsupportkey.Text = "U" & Format(dtCaseDetails.Rows(0).Item("numUniversalSupportKey"), "00000000000")
                        '    End If
                        'End If
                        lblcasenumber.Text = dtCaseDetails.Rows(0).Item("vcCaseNumber")
                        lblCasedetails.Text = "(" & dtCaseDetails.Rows(0).Item("vcCaseNumber") & ")"
                        lblCaseStatus.Text = dtCaseDetails.Rows(0).Item("vcCaseStatus")
                        If Not IsDBNull(dtCaseDetails.Rows(0).Item("numStatus")) Then
                            hdnStatus.Value = CCommon.ToLong(dtCaseDetails.Rows(0).Item("numStatus"))
                            If CCommon.ToLong(hdnStatus.Value) = 136 Then
                                btnOpenCloseCase.Text = "Re-Open Case"
                            Else
                                btnOpenCloseCase.Text = "Close Case"
                                btnOpenCloseCase.Attributes.Add("onclick", "return CloseCase();")
                            End If
                            'If Not ddlStatus.Items.FindByValue(dtCaseDetails.Rows(0).Item("numStatus")) Is Nothing Then
                            '    ddlStatus.ClearSelection()
                            '    ddlStatus.Items.FindByValue(dtCaseDetails.Rows(0).Item("numStatus")).Selected = True
                            'End If
                        End If

                        'btnAddTask.Attributes.Add("onclick", "NewTask('" & lngCntId & "','" & lngCaseId & "')")
                        lblCreatedBy.Text = dtCaseDetails.Rows(0).Item("CreatedBy")
                        lblRecordOwner.Text = dtCaseDetails.Rows(0).Item("RecOwner")
                        lblLastModifiedBy.Text = dtCaseDetails.Rows(0).Item("ModifiedBy")
                        hdnRecordOwner.Value = dtCaseDetails.Rows(0).Item("numRecOwner")
                        ViewState("AssignedTo") = CCommon.ToLong(dtCaseDetails.Rows(0).Item("numAssignedTo"))
                        ViewState("ContactID") = CCommon.ToLong(dtCaseDetails.Rows(0).Item("numContactID"))

                        LoadContractsInfo(dtCaseDetails.Rows(0).Item("numDivisionID"))

                        If CCommon.ToLong(dtCaseDetails.Rows(0).Item("numContractID")) Then
                            BindContractinfo(CCommon.ToLong(dtCaseDetails.Rows(0).Item("numContractID")))
                            lblContractName.Text = dtCaseDetails.Rows(0).Item("vcContractName")
                        End If

                        'Commented by Neelam - Obsolete functionality
                        'hplDocumentCount.Attributes.Add("onclick", "return OpenDocuments(" & lngCaseId & ")")
                        'hplDocumentCount.Text = "(" & CCommon.ToLong(dtCaseDetails.Rows(0)("DocumentCount")) & ")"
                        'imgOpenDocument.Attributes.Add("onclick", "return OpenDocuments(" & lngCaseId & ")")
                        imgOpenDocument.Attributes.Add("onclick", "OpenDocumentFiles(" & txtDivId.Text & ");return false;")
                        hplOpenDocument.Attributes.Add("onclick", "OpenDocumentFiles(" & txtDivId.Text & ");return false;")

                    End If



                    'dgContact.DataSource = objCases.AssCntsByCaseID
                    'dgContact.DataBind()

                    BindSolutions()

                    BindLinkedItems()

                End If
                LoadControls()
                'LoadStaticControls()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function CheckUserExist(ByVal UserContactID As String, ByVal SelectedContactIds As String) As Boolean
            Dim strArr As String()
            Dim result As Boolean
            result = False
            strArr = SelectedContactIds.Split(",")
            For Each s As String In strArr
                If UserContactID = s Then
                    result = True
                End If
            Next
            Return result
        End Function
        'Added by Neelam on 02/16/2018 - Fetch and displays document and link name on the page*/
        Private Sub GetDocumentFilesLink()
            Try
                Dim objOpp As New BusinessLogic.Opportunities.OppotunitiesIP
                Dim dtDocLinkName As New DataTable
                With objOpp
                    .DomainID = Session("DomainID")
                    .DivisionID = lngCaseId 'CCommon.ToLong(txtDivId.Text)
                    dtDocLinkName = .GetDocumentFilesLink("P")
                End With

                If dtDocLinkName IsNot Nothing AndAlso dtDocLinkName.Rows.Count > 0 Then

                    Dim docRows = (From dRow In dtDocLinkName.Rows
                                   Where CCommon.ToString(dRow("cUrlType")) = "L"
                                   Select New With {Key .fileName = dRow("VcFileName"), .docName = dRow("VcDocName"), .extention = dRow("vcFileType"), .fileId = dRow("numGenericDocID")}).Distinct()
                    If docRows.Count() > 0 Then
                        Dim _docName = String.Empty
                        Dim countId = 0
                        For Each dr In docRows
                            Dim strOriginalFileName = CCommon.ToString(dr.docName) & CCommon.ToString(dr.extention)
                            Dim strDocName = CCommon.ToString(dr.fileName)
                            Dim fileID = CCommon.ToString(dr.fileId)
                            Dim strFileLogicalPath = String.Empty
                            If File.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName) Then
                                Dim f As FileInfo = New FileInfo(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName)
                                Dim strFileSize As String = f.Length.ToString
                                strFileLogicalPath = CCommon.GetDocumentPath(Session("DomainID")) & HttpUtility.UrlEncode(strDocName).Replace("+", " ")
                            End If

                            Dim docLink As New HyperLink

                            Dim imgDelete As New ImageButton
                            imgDelete.ImageUrl = "../images/delete2.gif"
                            imgDelete.Height = 10
                            imgDelete.ToolTip = "Delete"
                            imgDelete.ID = "imgDelete" + CCommon.ToString(countId)
                            imgDelete.Attributes.Add("onclick", "DeleteDocumentOrLink(" & fileID & ");return false;")

                            docLink.Text = strOriginalFileName
                            docLink.NavigateUrl = strFileLogicalPath
                            docLink.ID = "hpl" + CCommon.ToString(countId)
                            docLink.Target = "_blank"
                            Dim createDocDiv As HtmlGenericControl = New HtmlGenericControl("DIV")
                            createDocDiv.Controls.Add(docLink)
                            createDocDiv.Controls.Add(imgDelete)
                            tdDocument.Controls.Add(createDocDiv)
                            countId = countId + 1
                        Next
                        imgDocument.Visible = True
                    Else
                        imgDocument.Visible = False
                    End If

                    Dim linkRows = (From dRow In dtDocLinkName.Rows
                                    Where CCommon.ToString(dRow("cUrlType")) = "U"
                                    Select New With {Key .linkURL = dRow("VcFileName"), Key .linkName = dRow("VcDocName"), .fileId = dRow("numGenericDocID")}).Distinct()
                    If linkRows.Count() > 0 Then
                        Dim _linkName = String.Empty
                        Dim _linkURL = String.Empty
                        Dim fileLinkID = String.Empty
                        Dim countLinkId = 0

                        For Each dr In linkRows
                            '_linkName = _linkName + ", " + CCommon.ToString(dr.linkName)
                            '_linkURL = _linkURL + ",<br/> " + CCommon.ToString(dr.linkURL)

                            _linkName = CCommon.ToString(dr.linkName)
                            _linkURL = CCommon.ToString(dr.linkURL)
                            fileLinkID = CCommon.ToString(dr.fileId)
                            Dim uriBuilder As UriBuilder = New UriBuilder(_linkURL)
                            'UriBuilder builder = New UriBuilder(myUrl)
                            'charityNameText.NavigateUrl = builder.Uri.AbsoluteUri

                            Dim link As New HyperLink
                            Dim imgLinkDelete As New ImageButton
                            imgLinkDelete.ImageUrl = "../images/delete2.gif"
                            imgLinkDelete.Height = 10
                            imgLinkDelete.ToolTip = "Delete"
                            imgLinkDelete.ID = "imgLinkDelete" + CCommon.ToString(countLinkId)
                            imgLinkDelete.Attributes.Add("onclick", "DeleteDocumentOrLink(" & fileLinkID & ");return false;")

                            link.Text = _linkName
                            link.NavigateUrl = uriBuilder.Uri.AbsoluteUri
                            'link.NavigateUrl = _linkURL
                            link.ID = "hplLink" + CCommon.ToString(countLinkId)
                            link.Target = "_blank"
                            Dim createLinkDiv As HtmlGenericControl = New HtmlGenericControl("DIV")
                            createLinkDiv.Controls.Add(link)
                            createLinkDiv.Controls.Add(imgLinkDelete)
                            tdLink.Controls.Add(createLinkDiv)
                            countLinkId = countLinkId + 1
                        Next
                        imgLink.Visible = True
                    Else
                        imgLink.Visible = False
                    End If
                Else
                    imgDocument.Visible = False
                    imgLink.Visible = False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub btnDeleteDocOrLink_Click(sender As Object, e As EventArgs)
            Dim objDocuments As New DocumentList()
            objDocuments.GenDocID = CCommon.ToLong(hdnFileId.Value)
            objDocuments.DeleteDocumentOrLink()
            GetDocumentFilesLink()
        End Sub

        Protected Sub btnAttach_Click(sender As Object, e As EventArgs)
            Try
                If (fileupload.PostedFile.ContentLength > 0) Then
                    Dim strFName As String()
                    Dim strFilePath, strFileName, strFileType As String
                    strFileName = Path.GetFileName(fileupload.PostedFile.FileName)
                    If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                        Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
                    End If
                    Dim newFileName As String = Path.GetFileNameWithoutExtension(strFileName) & DateTime.UtcNow.ToString("yyyyMMddhhmmssfff") & Path.GetExtension(strFileName)
                    strFilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & newFileName
                    strFName = Split(strFileName, ".")
                    strFileType = "." & strFName(strFName.Length - 1)                     'Getting the Extension of the File
                    fileupload.PostedFile.SaveAs(strFilePath)
                    UploadFile("L", strFileType, newFileName, strFName(0), "", 0, 0, "A")
                    Dim script As String = "function f(){$find(""" + RadWinDocumentFiles.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, True)
                    Response.Redirect(Request.RawUrl, False)
                Else
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "NoFiles", "alert('Please attach a file to upload.');return false;", True)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub btnLinkClose_Click(sender As Object, e As EventArgs)
            UploadFile("U", "", txtLinkURL.Text, txtLinkName.Text, "", 0, 0, "A")
            Response.Redirect(Request.RawUrl, False)
        End Sub

        'Modified by Neelam on 02/16/2018 - Added function to save File/Link data in the database*/
        Sub UploadFile(ByVal URLType As String, ByVal strFileType As String, ByVal strFileName As String, ByVal DocName As String, ByVal DocDesc As String,
                       ByVal DocumentStatus As Long, DocCategory As Long, ByVal strType As String)
            Try
                Dim lngDivId As Long = CCommon.ToLong(txtDivId.Text)
                Dim arrOutPut As String()
                Dim objDocuments As New DocumentList()
                With objDocuments
                    .DomainID = Session("DomainId")
                    .UserCntID = Session("UserContactID")
                    .UrlType = URLType
                    .DocumentStatus = DocumentStatus
                    .DocCategory = DocCategory
                    .FileType = strFileType
                    .DocName = DocName
                    .DocDesc = DocDesc
                    .FileName = strFileName
                    .DocumentSection = strType
                    .RecID = lngCaseId
                    'If lngDivId > 0 Then
                    '    .RecID = lngDivId
                    'ElseIf Session("DivisionID") IsNot Nothing And Session("DivisionID") > 0 Then
                    '    .RecID = Session("DivisionID")
                    'Else
                    '    .RecID = 0
                    'End If
                    .DocumentType = IIf(lngCaseId > 0, 2, 1) '1=generic,2=specific
                    arrOutPut = .SaveDocuments()
                End With
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadStaticControls()
            Try
                tblMain.Controls.Clear()
                Dim ds As DataSet
                Dim objPageLayout As New CPageLayout
                Dim fields() As String

                objPageLayout.CoType = "S"
                objPageLayout.UserCntID = Session("UserContactId")
                objPageLayout.RecordId = lngCaseId
                'objPageLayout.ContactID = lngCaseId
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.PageId = 3

                objPageLayout.FormId = 12
                objPageLayout.PageType = 3

                ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
                dtTableInfo = ds.Tables(0)

                Dim intCol1Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=1")
                Dim intCol2Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=2")
                Dim intCol1 As Int32 = 0
                Dim intCol2 As Int32 = 0
                Dim tblCell As TableCell

                Dim tblRow As TableRow
                Const NoOfColumns As Short = 2
                Dim ColCount As Int32 = 0
                ''If intermediatory Page is enabled then Add the text "Name" to dropdown fields
                'objPageControls.CreateTemplateRow(tblMain)

                objPageControls.CaseId = objCases.CaseID
                objPageControls.DivisionID = txtDivId.Text
                objPageControls.TerritoryID = objCases.TerritoryID
                objPageControls.strPageType = "Case"
                objPageControls.EditPermission = m_aryRightsForPage(RIGHTSTYPE.UPDATE)

                tblRow = New TableRow
                tblCell = New TableCell
                tblCell.Style.Add("width", "25%")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Style.Add("width", "25%")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Style.Add("width", "25%")
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell
                tblCell.Style.Add("width", "25%")
                tblRow.Cells.Add(tblCell)
                tblMain.Controls.Add(tblRow)

                tblRow = New TableRow()
                tblCell = New TableCell()
                tblCell.CssClass = "normal1Right"
                tblCell.HorizontalAlign = HorizontalAlign.Right
                tblCell.Font.Bold = True
                tblCell.Text = "Requested Resolve Date : "
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell()
                tblCell.ID = "Case~131~False~" & objCases.CaseID & "~1~0~0"
                tblCell.CssClass = "editable_DateField"
                tblCell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                tblCell.Attributes.Add("onmouseout", "bgColor=''")
                tblCell.Attributes.Add("title", "Double click to edit...")
                tblCell.Attributes.Add("bgcolor", "")
                tblCell.HorizontalAlign = HorizontalAlign.Left
                tblCell.Text = objCases.TargetResolveDate
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell()
                tblCell.CssClass = "normal1Right"
                tblCell.HorizontalAlign = HorizontalAlign.Right
                tblCell.Font.Bold = True
                tblCell.Text = "Assigned-to : "
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell()
                tblCell.ID = "Case~134~False~" & objCases.CaseID & "~1~0~0"
                tblCell.CssClass = "editable_select"
                tblCell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                tblCell.Attributes.Add("onmouseout", "bgColor=''")
                tblCell.Attributes.Add("title", "Double click to edit...")
                tblCell.Attributes.Add("bgcolor", "")
                tblCell.HorizontalAlign = HorizontalAlign.Left
                tblCell.Text = objCases.AssignToName
                tblRow.Cells.Add(tblCell)
                tblMain.Rows.Add(tblRow)

                tblRow = New TableRow()
                tblCell = New TableCell()
                tblCell.CssClass = "normal1Right"
                tblCell.HorizontalAlign = HorizontalAlign.Right
                tblCell.Font.Bold = True
                tblCell.Text = "Priority : "
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell()
                tblCell.ID = "Case~129~False~" & objCases.CaseID & "~1~0~0"
                tblCell.CssClass = "editable_select"
                tblCell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                tblCell.Attributes.Add("onmouseout", "bgColor=''")
                tblCell.Attributes.Add("title", "Double click to edit...")
                tblCell.Attributes.Add("bgcolor", "")
                tblCell.HorizontalAlign = HorizontalAlign.Left
                tblCell.Text = objCases.PriorityName
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell()
                tblCell.CssClass = "normal1Right"
                tblCell.HorizontalAlign = HorizontalAlign.Right
                tblCell.Font.Bold = True
                tblCell.Text = "Status : "
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell()
                tblCell.ID = "Case~137~False~" & objCases.CaseID & "~1~0~0"
                tblCell.CssClass = "editable_select"
                tblCell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                tblCell.Attributes.Add("onmouseout", "bgColor=''")
                tblCell.Attributes.Add("title", "Double click to edit...")
                tblCell.Attributes.Add("bgcolor", "")
                tblCell.HorizontalAlign = HorizontalAlign.Left
                tblCell.Text = objCases.CaseStatus
                tblRow.Cells.Add(tblCell)
                tblMain.Rows.Add(tblRow)

                tblRow = New TableRow()
                tblCell = New TableCell()
                tblCell.CssClass = "normal1Right"
                tblCell.HorizontalAlign = HorizontalAlign.Right
                tblCell.Font.Bold = True
                tblCell.Text = "Description : "
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell()
                tblCell.ID = "Case~145~False~" & objCases.CaseID & "~1~0~0"
                tblCell.CssClass = "editable_textarea"
                tblCell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                tblCell.Attributes.Add("onmouseout", "bgColor=''")
                tblCell.Attributes.Add("title", "Double click to edit...")
                tblCell.Attributes.Add("bgcolor", "")
                tblCell.HorizontalAlign = HorizontalAlign.Left
                tblCell.Text = objCases.Desc
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell()
                tblCell.CssClass = "normal1Right"
                tblCell.HorizontalAlign = HorizontalAlign.Right
                tblCell.Font.Bold = True
                tblCell.Text = "<img src='../images/solutions.png' /> <a href='#' onclick='openSolutionPopup(" & objCases.CaseID & ")'>Solutions(" & objCases.SolutionCount & ")</a> : "
                tblRow.Cells.Add(tblCell)

                tblCell = New TableCell()
                'tblCell.ID = "Case~137~False~438~1~0~0"
                'tblCell.CssClass = "editable_select"
                'tblCell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                'tblCell.Attributes.Add("onmouseout", "bgColor=''")
                'tblCell.Attributes.Add("title", "Double click to edit...")
                'tblCell.Attributes.Add("bgcolor", "")
                tblCell.HorizontalAlign = HorizontalAlign.Left
                tblCell.Style.Add("padding-top", "13px")
                tblCell.Text = objCases.SoultionName
                tblRow.Cells.Add(tblCell)
                tblMain.Rows.Add(tblRow)

                If Not boolIntermediatoryPage Then
                    Dim strValidation As String = objPageControls.GenerateValidationScript(dtTableInfo)
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "CFvalidation", strValidation, True)
                ElseIf Session("InlineEdit") = True And m_aryRightsForPage(RIGHTSTYPE.UPDATE) <> 0 Then
                    Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtTableInfo)
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "InlineEditValidation", strValidation, True)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadDueCaseList()
            Try
                objCases = New CaseIP()
                objCases.DomainID = Session("DomainID")
                objCases.ContactID = Session("UserContactId")
                objCases.CaseID = lngCaseId
                Dim dt As New DataTable()
                dt = objCases.GetDueCaseList()
                rptDueCaseForThisWeek.DataSource = dt
                rptDueCaseForThisWeek.DataBind()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Sub LoadControls()
            Try
                tblMain.Controls.Clear()
                Dim ds As DataSet
                Dim objPageLayout As New CPageLayout
                Dim fields() As String

                objPageLayout.CoType = "S"
                objPageLayout.UserCntID = Session("UserContactId")
                objPageLayout.RecordId = lngCaseId
                'objPageLayout.ContactID = lngCaseId
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.PageId = 3

                objPageLayout.FormId = 12
                objPageLayout.PageType = 3

                ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
                dtTableInfo = ds.Tables(0)

                Dim intCol1Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=1")
                Dim intCol2Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=2")
                Dim intCol1 As Int32 = 0
                Dim intCol2 As Int32 = 0
                Dim tblCell As TableCell

                Dim tblRow As TableRow
                Const NoOfColumns As Short = 2
                Dim ColCount As Int32 = 0
                ''If intermediatory Page is enabled then Add the text "Name" to dropdown fields
                objPageControls.CreateTemplateRow(tblMain)

                objPageControls.CaseId = objCases.CaseID
                objPageControls.DivisionID = txtDivId.Text
                objPageControls.TerritoryID = objCases.TerritoryID
                objPageControls.strPageType = "Case"
                objPageControls.EditPermission = m_aryRightsForPage(RIGHTSTYPE.UPDATE)

                Dim dv As DataView = dtTableInfo.DefaultView
                Dim dvExcutedView As DataView = dtTableInfo.DefaultView
                Dim dvExcutedGroupsView As DataView = dtTableInfo.DefaultView
                Dim DataRows = (From row In dtTableInfo.AsEnumerable() Select row)
                Dim distinctNames = DataRows.OrderBy(Function(x) x("numOrder")).Select(Function(x) x("vcGroupName")).Distinct().ToList()
                For Each drGroupName As String In distinctNames
                    If drGroupName Is Nothing Then
                        drGroupName = ""
                    End If
                    tblCell = New TableCell()
                    tblRow = New TableRow
                    tblCell.ColumnSpan = 4
                    tblCell.CssClass = "tableGroupHeader"
                    tblCell.Text = drGroupName
                    tblRow.Cells.Add(tblCell)
                    tblMain.Rows.Add(tblRow)
                    dvExcutedView.RowFilter = "vcGroupName = '" & drGroupName & "'"
                    tblRow = New TableRow
                    tblCell = New TableCell
                    intCol1Count = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=1 AND vcGroupName='" & drGroupName & "'")
                    intCol2Count = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=2 AND vcGroupName='" & drGroupName & "'")
                    intCol1 = 0
                    intCol2 = 0
                    For Each drData As DataRowView In dvExcutedView
                        Dim dr As DataRow
                        dr = drData.Row
                        If boolIntermediatoryPage = True Then
                            If Not IsDBNull(dr("vcPropertyName")) And (dr("fld_type") = "SelectBox") And dr("bitCustomField") = False Then
                                dr("vcPropertyName") = dr("vcPropertyName") & "Name"
                            End If
                        End If

                        If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False Then
                            dr("vcValue") = objCases.GetType.GetProperty(dr("vcPropertyName")).GetValue(objCases, Nothing)
                        End If

                        If ColCount = 0 Then
                            tblRow = New TableRow
                        End If

                        If intCol1Count = intCol1 And intCol2Count > intCol1Count Then
                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)
                            ColCount = 1
                        End If

                        If (dr("fld_type") = "ListBox" Or dr("fld_type") = "SelectBox") Then
                            Dim dtData As DataTable
                            Dim dtSelOpportunity As DataTable

                            'If dr("numFieldId") = 255 Then
                            '     objCommon.CaseID = lngCaseId
                            '     objCommon.charModule = "S"
                            '     objCommon.GetCompanySpecificValues1()
                            '     objCases.DomainID = Session("DomainId")
                            '     objCases.DivisionID = objCommon.DivisionID
                            '     objCases.byteMode = 1
                            '     objCases.CaseID = lngCaseId
                            '     If boolIntermediatoryPage = False Then
                            '         dtData = objCases.GetOpportunities
                            '     End If
                            '     dtSelOpportunity = objCases.GetCaseOpportunities

                            If dr("vcPropertyName") = "AssignTo" Then ' dr("numFieldId") = 137 Then
                                If boolIntermediatoryPage = False Then
                                    If Session("PopulateUserCriteria") = 1 Then
                                        objCommon.CaseID = lngCaseId
                                        objCommon.charModule = "S"
                                        objCommon.GetCompanySpecificValues1()
                                        dtData = objCommon.ConEmpListFromTerritories(Session("DomainID"), 0, 0, objCommon.TerittoryID)
                                    ElseIf Session("PopulateUserCriteria") = 2 Then
                                        dtData = objCommon.ConEmpList(Session("DomainID"), Session("UserContactID"))
                                    Else
                                        dtData = objCommon.ConEmpList(Session("DomainID"), 0, 0)
                                    End If

                                End If
                            ElseIf dr("vcPropertyName") = "ContactID" Then
                                Dim fillCombo As New COpportunities
                                With fillCombo
                                    .DivisionID = txtDivId.Text
                                    dtData = fillCombo.ListContact().Tables(0)
                                End With
                            ElseIf dr("vcPropertyName") = "ContractID" Then ' dr("numFieldId") = 299 Then
                                If objContracts Is Nothing Then objContracts = New CContracts
                                objContracts.DivisionId = txtDivId.Text
                                objContracts.UserCntID = Session("UserContactId")
                                objContracts.DomainID = Session("DomainId")
                                dtData = objContracts.GetContractDdlList()
                            ElseIf Not IsDBNull(dr("numListID")) And dr("numListID") <> 0 Then
                                If boolIntermediatoryPage = False Then
                                    If dr("ListRelID") > 0 Then
                                        objCommon.Mode = 3
                                        objCommon.DomainID = Session("DomainID")
                                        objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objCases)
                                        objCommon.SecondaryListID = dr("numListId")
                                        dtData = objCommon.GetFieldRelationships.Tables(0)
                                    Else
                                        dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                    End If
                                End If
                            End If

                            If boolIntermediatoryPage Then
                                objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData, dtSelOpportunity, lngCaseID:=lngCaseId)
                            Else
                                Dim ddl As DropDownList
                                ddl = objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData, dtSelOpportunity, lngCaseID:=lngCaseId)
                                If CLng(dr("DependentFields")) > 0 And Not boolIntermediatoryPage Then
                                    ddl.AutoPostBack = True
                                    AddHandler ddl.SelectedIndexChanged, AddressOf PopulateDependentDropdown
                                End If
                            End If
                        ElseIf dr("fld_type") = "CheckBoxList" Then
                            If Not IsDBNull(dr("vcPropertyName")) Then
                                Dim dtData As DataTable

                                If boolIntermediatoryPage = False Then
                                    If dr("ListRelID") > 0 Then
                                        objCommon.Mode = 3
                                        objCommon.DomainID = Session("DomainID")
                                        objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objCases)
                                        objCommon.SecondaryListID = dr("numListId")
                                        dtData = objCommon.GetFieldRelationships.Tables(0)
                                    Else
                                        dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                    End If
                                End If

                                objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData)
                            End If
                        Else
                            objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, RecordID:=lngCntId, lngCaseID:=lngCaseId)
                        End If

                        If intCol2Count = intCol2 And intCol1Count > intCol2Count Then
                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            ColCount = 1
                        End If


                        If dr("intcoulmn") = 2 Then
                            If intCol1 <> intCol1Count Then
                                intCol1 = intCol1 + 1
                            End If

                            If intCol2 <> intCol2Count Then
                                intCol2 = intCol2 + 1
                            End If
                        End If
                        ColCount = ColCount + 1
                        If NoOfColumns = ColCount Then
                            ColCount = 0
                            tblMain.Rows.Add(tblRow)
                        End If
                    Next
                Next
                If ColCount > 0 Then
                    tblMain.Rows.Add(tblRow)
                End If
                For Each r As TableRow In tblMain.Rows
                    For Each d As TableCell In r.Cells
                        Dim hypData As HyperLink = d.FindControl("SolutionForCase")
                        If hypData IsNot Nothing Then
                            If hypData.Text.Contains("##SolutionCount##") Then
                                hypData.Text = hypData.Text.Replace("##SolutionCount##", objCases.SolutionCount)
                            End If
                        End If
                    Next
                Next
                'objPageControls.CreateComments(tblMain, objCases.Comments, boolIntermediatoryPage)
                'objPageControls.CreateInternalComments(tblMain, objCases.InternalComments, boolIntermediatoryPage)
                'Add Client Side validation for custom fields
                If Not boolIntermediatoryPage Then
                    Dim strValidation As String = objPageControls.GenerateValidationScript(dtTableInfo)
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "CFvalidation", strValidation, True)
                ElseIf Session("InlineEdit") = True And m_aryRightsForPage(RIGHTSTYPE.UPDATE) <> 0 Then
                    Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtTableInfo)
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "InlineEditValidation", strValidation, True)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        'Sub LoadDropdowns()
        '    Try
        '        'objCommon.sb_FillComboFromDBwithSel(ddlContactRole, 26, Session("DomainID"))
        '        'objCommon.sb_FillComboFromDBwithSel(ddlStatus, 14, Session("DomainID"))

        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        Function save() As Integer
            Try
                With objCases
                    .DivisionID = txtDivId.Text
                    .ContactID = txtCntId.Text
                    .UserCntID = Session("UserContactID")
                    .CaseID = lngCaseId
                End With
                For Each dr As DataRow In dtTableInfo.Rows
                    If dr("numFieldID") = 255 Then
                        Dim i As Integer = 0
                        Dim strOppSelected As String = ""
                        Dim lst As ListBox
                        If Not radOppTab.MultiPage.FindControl(dr("numFieldID") & dr("vcFieldName")) Is Nothing Then
                            lst = radOppTab.MultiPage.FindControl(dr("numFieldID") & dr("vcFieldName"))
                            For i = 0 To lst.Items.Count - 1
                                If lst.Items(i).Selected = True Then
                                    strOppSelected = strOppSelected & lst.Items(i).Value.ToString & ","
                                End If
                            Next

                            objCases.CaseID = lngCaseId
                            objCases.DomainID = Session("DomainID")
                            objCases.strOppSel = strOppSelected
                            objCases.SaveCaseOpportunities()
                        End If

                    ElseIf Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False And dr("bitCanBeUpdated") = True Then
                        objPageControls.SetValueForStaticFields(dr, objCases, radOppTab.MultiPage)
                    End If
                Next
                'objPageControls.SetValueForComments(objCases.Comments, radOppTab)

                'objPageControls.SetValueForInternalComments(objCases.InternalComments, radOppTab)


                If objCases.SaveCases() = 0 Then
                    Return 1
                    Exit Function
                End If

                'Added By Sachin Sadhu||Date:30Jul2014
                'Purpose :To Added Projects data in work Flow queue based on created Rules
                '          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = CCommon.ToLong(objCases.CaseID)
                objWfA.SaveWFCasesQueue()
                'end of code



                If objCases.AssignTo > 0 And ViewState("AssignedTo") <> objCases.AssignTo Then
                    CAlerts.SendAlertToAssignee(5, hdnRecordOwner.Value, objCases.AssignTo, objCases.CaseID, Session("DomainID")) 'bugid 767
                    ViewState("AssignedTo") = objCases.AssignTo
                End If

                Return 0
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                If save() = 1 Then
                    litMessage.Text = " No of Incidents Not Sufficient"
                    Exit Sub
                End If
                SaveCusField()

                'update ContractName and Case Status Labels
                If Not radOppTab.MultiPage.FindControl("299Contract~0") Is Nothing Then
                    Dim ddl As DropDownList = CType(radOppTab.MultiPage.FindControl("299Contract~0"), DropDownList)
                    If ddl.SelectedIndex > 0 Then
                        lblContractName.Text = ddl.SelectedItem.Text
                        BindContractinfo(CCommon.ToLong(ddl.SelectedValue))
                    Else
                        lblContractName.Text = "-"
                    End If

                End If
                If Not radOppTab.MultiPage.FindControl("298Status~14") Is Nothing Then
                    Dim ddl As DropDownList = CType(radOppTab.MultiPage.FindControl("298Status~14"), DropDownList)
                    If ddl.SelectedIndex > 0 Then
                        lblCaseStatus.Text = ddl.SelectedItem.Text
                    Else
                        lblCaseStatus.Text = "-"
                    End If
                End If
                LoadInformation()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                If save() = 1 Then
                    litMessage.Text = " No of Incidents Not Sufficient"
                    Exit Sub
                End If
                SaveCusField()
                PageRedirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub btnSaveNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNext.Click
            Try

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub DisplayDynamicFlds()
            Try
                objPageControls.DisplayDynamicFlds(lngCaseId, 0, Session("DomainID"), objPageControls.Location.Support, radOppTab, 12)


            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveCusField()
            Try
                objPageControls.SaveCusField(lngCaseId, 0, Session("DomainID"), objPageControls.Location.Support, radOppTab.MultiPage)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                PageRedirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub PageRedirect()
            Try
                If GetQueryStringVal("frm") = "Solution" Then
                    Response.Redirect("../cases/frmSolution.aspx?SolId=" & GetQueryStringVal("SolId") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "tickler" Then
                    Response.Redirect("../common/frmticklerdisplay.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("Index") <> "" Then
                    Response.Redirect("../cases/frmCaseList.aspx?Index=" & GetQueryStringVal("Index") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "accounts" Then
                    objCommon.CaseID = lngCaseId
                    objCommon.charModule = "S"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../Account/frmAccounts.aspx?klds+7kldf=fjk-las&DivId=" & objCommon.DivisionID & "&CaseID=" & lngCaseId & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "CaseReport" Then
                    Response.Redirect("../reports/frmCases.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "AdvSer" Then
                    Response.Redirect("../Admin/frmAdvCaseRes.aspx")
                Else : Response.Redirect("../cases/frmCaseList.aspx")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActDelete.Click
            Try

                objCases.CaseID = lngCaseId
                objCases.DomainID = Session("DomainID")
                If objCases.DeleteCase() = False Then
                    litMessage.Text = "Dependent Records Exists.Cannot be deleted."
                Else : Response.Redirect("../cases/frmCaseList.aspx")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub FillCustomer(ByVal ddlCombo As DropDownList, ByVal strName As String)
            Try

                If objCommon Is Nothing Then objCommon = New CCommon
                With objCommon
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                    .Filter = Trim(strName) & "%"
                    ddlCombo.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
                    ddlCombo.DataTextField = "vcCompanyname"
                    ddlCombo.DataValueField = "numDivisionID"
                    ddlCombo.DataBind()
                End With
                ddlCombo.Items.Insert(0, New ListItem("--Select One--", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Private Sub btnAddContact_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddContact.Click
        '    Try
        '        objCases.byteMode = 0
        '        objCases.CaseID = lngCaseId
        '        objCases.DomainID = Session("DomainID")
        '        objCases.ContactID = CInt(ddlAssocContactId.SelectedItem.Value)
        '        objCases.ContactRole = ddlContactRole.SelectedValue
        '        objCases.bitPartner = chkShare.Checked
        '        dgContact.DataSource = objCases.AddCaseContacts
        '        dgContact.DataBind()
        '        ddlContactRole.SelectedIndex = 0
        '        chkShare.Checked = False
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        Public Sub FillContact(ByVal ddlCombo As DropDownList, ByVal lngDivision As Long)
            Try
                Dim fillCombo As New COpportunities
                With fillCombo
                    .DivisionID = lngDivision
                    ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                    ddlCombo.DataTextField = "Name"
                    ddlCombo.DataValueField = "numcontactId"
                    ddlCombo.DataBind()
                End With
                ddlCombo.Items.Insert(0, New ListItem("---Select One---", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Private Sub ddlcompany_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlcompany.SelectedIndexChanged
        '    Try
        '        FillContact(ddlAssocContactId, CInt(ddlcompany.SelectedItem.Value))
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub dgContact_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgContact.ItemCommand
        '    Try
        '        If e.CommandName = "Delete" Then
        '            objCases.CaseID = lngCaseId
        '            objCases.ContactID = e.Item.Cells(0).Text
        '            objCases.byteMode = 1
        '            objCases.DomainID = Session("DomainID")
        '            dgContact.DataSource = objCases.AddCaseContacts()
        '            dgContact.DataBind()
        '        End If
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub dgContact_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgContact.ItemDataBound
        '    Try
        '        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
        '            Dim btnDelete As Button
        '            Dim lnkDelete As LinkButton
        '            lnkDelete = e.Item.FindControl("lnkDeleteCnt")
        '            btnDelete = e.Item.FindControl("btnDeleteCnt")
        '            If e.Item.Cells(1).Text = 1 Then CType(e.Item.FindControl("lblShare"), Label).Text = "a"
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        Sub LoadContractsInfo(ByVal intDivId As Integer)
            Try
                If Not IsPostBack Then
                    'If objContracts Is Nothing Then objContracts = New CContracts
                    'Dim dtTable As DataTable
                    'objContracts.DivisionId = txtDivId.Text
                    'objContracts.UserCntId = Session("UserContactId")
                    'objContracts.DomainId = Session("DomainId")
                    'dtTable = objContracts.GetContractDdlList()
                    'ddlContract.DataSource = dtTable
                    'pnlContract.Visible = True
                    'ddlContract.DataTextField = "vcContractName"
                    'ddlContract.DataValueField = "numcontractId"
                    'ddlContract.DataBind()
                    'ddlContract.Items.Insert(0, "--Select One--")
                    'ddlContract.Items.FindByText("--Select One--").Value = "0"
                    lblRemAmount.Text = "0"
                    lblRemDays.Text = "0"
                    lblRemInci.Text = "0"
                    lblRemHours.Text = "0"
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Private Sub ddlContract_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContract.SelectedIndexChanged
        '    Try
        '        BindContractinfo()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        Sub BindContractinfo(ByVal ContractID As Long)
            Try
                If objContracts Is Nothing Then objContracts = New CContracts
                Dim dtTable As DataTable
                objContracts.ContractID = ContractID
                objContracts.UserCntID = Session("UserContactId")
                objContracts.DomainID = Session("DomainId")
                dtTable = objContracts.GetContractDtl()
                If dtTable.Rows.Count > 0 Then
                    lblRemAmount.Text = IIf(IsDBNull(dtTable.Rows(0).Item("RemAmount")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemAmount")))
                    lblRemHours.Text = IIf(IsDBNull(dtTable.Rows(0).Item("RemHours")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemHours")))
                    lblRemDays.Text = IIf(IsDBNull(dtTable.Rows(0).Item("days")), 0, String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("days")))
                    If dtTable.Rows(0).Item("bitincidents") = True Then
                        lblRemInci.Text = IIf(IsDBNull(dtTable.Rows(0).Item("Incidents")), 0, dtTable.Rows(0).Item("Incidents"))
                    Else : lblRemInci.Text = 0
                    End If
                Else
                    lblRemAmount.Text = 0
                    lblRemHours.Text = 0
                    lblRemDays.Text = 0
                    lblRemInci.Text = 0
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub radOppTab_TabClick(sender As Object, e As Telerik.Web.UI.RadTabStripEventArgs) Handles radOppTab.TabClick
            Try
                PersistTable.Clear()
                PersistTable.Add(PersistKey.SelectedTab, radOppTab.SelectedIndex)
                PersistTable.Add("chkPersistCase", chkPersistCase.Checked)
                PersistTable.Save(boolOnlyURL:=True)

                LoadDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub


        Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
            Try
                ViewState("IntermediatoryPage") = False
                boolIntermediatoryPage = False
                tblMain.Controls.Clear()
                LoadInformation()
                ControlSettings()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub ControlSettings()
            If boolIntermediatoryPage = True Then
                btnSave.Visible = False
                btnSaveClose.Visible = False
                btnEdit.Visible = True
            Else
                'objCommon.CheckdirtyForm(Page)

                btnSave.Visible = True
                btnSaveClose.Visible = True
                btnEdit.Visible = False
            End If
        End Sub


        Sub PopulateDependentDropdown(ByVal sender As Object, ByVal e As EventArgs)
            objPageControls.PopulateDropdowns(CType(sender, DropDownList), dtTableInfo, objCommon, radOppTab.MultiPage, Session("DomainID"))
        End Sub


        'Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.WebControls.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
        '    Try
        '        If radCmbCompany.Value <> "" Then
        '            FillContact(ddlAssocContactId, CInt(radCmbCompany.Value))
        '        End If
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        Private Sub radCmbSolution_ItemsRequested(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs) Handles radCmbSolution.ItemsRequested
            If e.Text.Trim().Length > 1 Then
                Dim dtSolution As DataTable
                Dim objSolution As New Solution
                objSolution.Category = 0
                objSolution.KeyWord = e.Text.Trim()
                objSolution.SortCharacter = "0"
                objSolution.DomainID = Session("DomainID")
                objSolution.columnSortOrder = "Asc"
                dtSolution = objSolution.GetKnowledgeBases
                radCmbSolution.DataSource = dtSolution
                radCmbSolution.DataTextField = "vcSolnTitle"
                radCmbSolution.DataValueField = "numSolnID"
                radCmbSolution.DataBind()
            End If
        End Sub


        Private Sub btnAddSolution_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddSolution.Click
            Try
                If radCmbSolution.SelectedValue <> "" Then
                    If objSolution Is Nothing Then objSolution = New Solution
                    objSolution.CaseID = lngCaseId
                    objSolution.SolID = radCmbSolution.SelectedValue
                    objSolution.Mode = 0
                    objSolution.LinkSolToCases()
                    BindSolutions()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgSolution_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSolution.ItemCommand
            Try
                If e.CommandName = "Solution" Then
                    Dim lngSolId As Long = e.Item.Cells(0).Text
                    Response.Redirect("../Cases/frmSolution.aspx?SolId=" & lngSolId & "&SI1=" & radOppTab.SelectedIndex & "&SI2=" & SI1 & "&frm=frmCases" & "&frm1=" & frm1 & "&frm1=" & frm2 & "&CaseId=" & lngCaseId.ToString)
                End If
                If e.CommandName = "Delete" Then
                    Dim lngSolId As Long = e.Item.Cells(0).Text

                    If objSolution Is Nothing Then objSolution = New Solution
                    objSolution.CaseID = lngCaseId
                    objSolution.SolID = lngSolId
                    objSolution.Mode = 1 'delete
                    objSolution.LinkSolToCases()
                    BindSolutions()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgSolution_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSolution.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim btnDelete As Button
                    btnDelete = e.Item.FindControl("btnSolDelete")
                    btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub BindLinkedItems()
            Try
                Dim objCase As New CCases
                objCase.DomainID = Session("DomainID")
                objCase.CaseID = lngCaseId
                objCase.strOppSel = ""
                dgLinkedItems.DataSource = objCase.GetCaseOpportunities()
                dgLinkedItems.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click
            Try
                If hdnOppItemIDs.Value.Trim() <> "" Then
                    Dim objCase As New CCases
                    objCase.DomainID = Session("DomainID")
                    objCase.CaseID = lngCaseId
                    objCase.byteMode = 1 ' delete selected Opp items
                    objCase.strOppSel = hdnOppItemIDs.Value
                    objCase.SaveCaseOpportunities()
                End If
                BindLinkedItems()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)

            End Try
        End Sub

        Private Sub dgLinkedItems_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgLinkedItems.ItemCommand
            If e.CommandName = "Order" Then
                Dim lngOrderId As Long = CCommon.ToLong(e.Item.Cells(2).Text)
                Response.Redirect("../opportunity/frmOpportunities.aspx?OpID=" & lngOrderId & "&SI1=" & radOppTab.SelectedIndex & "&SI2=" & SI1 & "&frm=frmCases" & "&frm1=" & frm1 & "&frm1=" & frm2 & "&CaseId=" & lngCaseId.ToString, False)
            End If
        End Sub

        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub

        Protected Sub chkPersistCase_CheckedChanged(sender As Object, e As EventArgs)
            PersistTable.Add("chkPersistCase", chkPersistCase.Checked)
            PersistTable.Save(boolOnlyURL:=True)
            If chkPersistCase.Checked = True Then
                sectionDueCase.Attributes.Add("style", "display:inline")
            Else
                sectionDueCase.Attributes.Add("style", "display:none")
            End If
        End Sub

        Private Sub btnOpenCloseCase_Click(sender As Object, e As EventArgs) Handles btnOpenCloseCase.Click
            Try
                Dim objCommon As New CCommon
                objCommon.Mode = 43
                objCommon.DomainID = Session("DomainID")
                objCommon.UpdateRecordID = lngCaseId
                objCommon.Comments = ""
                If CCommon.ToLong(hdnStatus.Value) = 136 Then
                    objCommon.UpdateValueID = 1008
                    btnOpenCloseCase.Text = "Close Case"
                Else
                    objCommon.UpdateValueID = 136
                    btnOpenCloseCase.Text = "Re-Open Case"
                End If
                objCommon.UpdateSingleFieldValue()
                PageRedirect()
                'Response.Redirect(Context.Request.Url.ToString())
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
    End Class
End Namespace

