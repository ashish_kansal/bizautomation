<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmCases.aspx.vb"
    Inherits="BACRM.UserInterface.Cases.frmCases" MasterPageFile="~/common/DetailPage.Master" %>

<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Src="../common/frmAssociatedContacts.ascx" TagName="frmAssociatedContacts"
    TagPrefix="uc1" %>
<%@ Register Src="../common/frmComments.ascx" TagName="frmComments" TagPrefix="uc2" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Src="../common/frmCorrespondence.ascx" TagName="frmCorrespondence" TagPrefix="uc1" %>
<%@ Register Src="../common/frmCollaborationUserControl.ascx" TagName="frmCollaborationUserControl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <link href="~/images/BizSkin/TabStrip.css" rel="stylesheet" type="text/css" />
    <link href="../css/lists.css" type="text/css" rel="STYLESHEET" />
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)" />
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script type="text/javascript" src="../javascript/CustomFieldValidation.js"></script>
    <script src="../JavaScript/jquery.Tooltip.js" type="text/javascript"></script>
    <script src="../JavaScript/jquery.jeditable.js" type="text/javascript"></script>
    <script src="../JavaScript/CommonInlineEdit.js" type="text/javascript"></script>
    <%--<script src="../JavaScript/jquery.ae.image.resize.min.js" type="text/javascript"></script>--%>

    <script language="javascript" type="text/javascript">
        //function reDirectPage(a) {
        //    document.location.href = a;
        //} 

        /*Commented by Neelam - Obsolete function*/
        function OpenDocuments(a) {
            window.open("../Documents/frmSpecDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=CS&yunWE=" + a, '', 'toolbar=no,titlebar=no,top=10,width=1000,height=450,left=10,scrollbars=yes,resizable=yes')
            return false;
        }

        function CloseCase() {
            if (confirm('�This will remove Case from the Cases module. You will still be able to access Case(s) from Case history, by navigating to the Organization the Case is for, and clicking on its �Cases� sub-tab. Are you sure you want to proceed ?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function pageLoad(sender, args) {
            BindInlineEdit();
        }

        function Save() {

            if (document.getElementById("132Resolve Date_txtDate").value == "") {
                alert("Enter Resolve Date");
                document.getElementById("132Resolve Date_txtDate").focus;
                return false
            }
            return validateCustomFields();
        }
        function openActionItem(a, b, c, d, e, f) {
            if (e == 'Email') {
                window.open("../outlook/frmMailDtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&numEmailHstrID=" + a, '', 'titlebar=no,top=100,left=250,width=850,height=550,scrollbars=yes,resizable=yes');
                return false;
            }
            else {
                window.location.href = "../admin/ActionItemDetailsOld.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Cases&CommId=" + a + "&CaseId=" + b + "&CaseTimeId=" + c + "&CaseExpId=" + d;
                return false;
            }

        }


        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function NewTask(a, b) {
            //				alert("cntid : "+a);
            //				alert("caseId : "+b);
            window.location = "../Admin/ActionItemDetailsOld.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Cases&CntID=" + a + "&CaseID=" + b;
        }
        function ShowWindow(Page, q, att) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                document.Form1.ddlStatus.style.display = 'none';
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                document.Form1.ddlStatus.style.display = '';
                return false;

            }
        }
        function openSol(a) {
            window.open("../cases/frmCaseLinkSol.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&CaseID=" + a, '', 'width=800,height=500,status=no,titlebar=no,scrollbar=yes,top=110,left=150,resizable=yes')
            //document.getElementById('cntDoc').src="../cases/frmCaseLinkSol.aspx?CaseID="+a
            //document.getElementById('divSol').style.visibility = "visible";
            return false;

        }
        function OpenItems(a) {
            window.open("../cases/frmLinkedOppItems.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&CaseID=" + a, '', 'width=800,height=500,status=no,titlebar=no,scrollbar=yes,top=110,left=150,resizable=yes')
            return false;

        }

        function ShowLayout(a, b, d) {
            window.open("../pagelayout/frmCustomisePageLayout.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Ctype=" + a + "&PType=3&FormId=" + d, '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function AddContact() {
            if (document.Form1.ddlAssocContactId.value == 0) {
                alert("Select Contact");

                var radTab = $find("radOppTab");
                radTab.set_selectedIndex(2);

                document.Form1.ddlAssocContactId.focus();
                return false;
            }
            var str;
            for (i = 0; i < document.Form1.elements.length; i++) {
                if (i <= 9) {
                    str = '0' + (i + 1)
                }
                else {
                    str = i + 1
                }
                if (document.getElementById('dgContact_ctl' + str + '_txtContactID') != null) {
                    if (document.getElementById('dgContact_ctl' + str + '_txtContactID').value == document.Form1.ddlAssocContactId.value) {
                        alert("Associated contact is already added");
                        return false;
                    }
                }
            }

        }
        function OpenContEmail(a, b) {

            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenEmail(txtMailAddr, a, b, c) {
            var Email = document.getElementById('' + txtMailAddr)

            if (document.getElementById('' + txtMailAddr) != null) {
                if (a == 1) {

                    window.open('mailto:' + Email.value);
                }
                else if (a == 2) {
                    window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail=' + Email.value + '&ContID=' + b + '&CaseID=' + c, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes');
                }

            }
            else {
                if (a == 1) {

                    window.open('mailto:' + txtMailAddr);
                }
                else if (a == 2) {
                    window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail=' + txtMailAddr + '&ContID=' + b + '&CaseID=' + c, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes');
                }
            }
            return false;
        }
        function SelectAll1(a) {
            var str;

            if (typeof (a) == 'string') {

                a = document.getElementById(a);

            }
            var dgLinkedItems = document.getElementById('dgLinkedItems');
            if (a.checked == true) {
                for (var i = 1; i <= dgLinkedItems.rows.length; i++) {
                    if (i < 10) {
                        str = '0' + i
                    }
                    else {
                        str = i
                    }
                    document.getElementById('dgLinkedItems_ctl' + str + '_chk').checked = true;

                }
            }
            else if (a.checked == false) {
                for (var i = 1; i <= dgLinkedItems.rows.length; i++) {
                    if (i < 10) {
                        str = '0' + i
                    }
                    else {
                        str = i
                    }

                    document.getElementById('dgLinkedItems_ctl' + str + '_chk').checked = false;
                }
            }
        }
        function GetSelectedItems() {
            var dgLinkedItems = document.getElementById('dgLinkedItems');
            var OppItemID = '';
            for (var i = 1; i <= dgLinkedItems.rows.length; i++) {
                if (i < 10) {
                    str = '0' + i
                }
                else {
                    str = i
                }
                if (str != '01')
                    if (document.getElementById('dgLinkedItems_ctl' + str + '_chk').checked) {
                        OppItemID = OppItemID + document.getElementById('dgLinkedItems_ctl' + str + '_lblOppItemID').innerHTML + ',';
                    }
            }
            document.getElementById('hdnOppItemIDs').value = OppItemID;
            return true;
        }
        function OpenTransfer(url) {
            window.open(url, '', "width=340,height=150,status=no,top=100,left=150");
            return false;
        }
        //        function OpenOrder(OrderId) {
        //            var CaseId = document.getElementById('hdnCaseId').value;
        //           location.href=("../opportunity/frmOpportunities.aspx?frm=cases&OpID="+OrderId+"CaseId="+CaseId+"");
        //        }

        $(function () {
            //$(".resizeme").aeImageResize({ height: 100, width: 100 });
        });

        function OpenShareRecord(a) {
            window.open("../common/frmShareRecord.aspx?RecordID=" + a + "&ModuleID=7", '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenHelp() {
            window.open('../Help/Cases.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        /*Function added by Neelam on 10/07/2017 - Added functionality to open Child Record window in modal pop up*/
        function OpenDocumentFiles(divID) {
            $find("<%= RadWinDocumentFiles.ClientID%>").show();
        }

        /*Function added by Neelam on 02/14/2018 - Added functionality to close document modal pop up*/
        function CloseDocumentFiles(mode) {
            var radwindow = $find('<%=RadWinDocumentFiles.ClientID %>');
            radwindow.close();

            if (mode == 'C') { return false; }
            else if (mode == 'L') {
                __doPostBack("<%= btnLinkClose.UniqueID%>", "");
            }
        }

        function DeleteDocumentOrLink(fileId) {
            if (confirm('Are you sure, you want to delete the selected item?')) {
                $('#hdnFileId').val(fileId);
                __doPostBack("<%= btnDeleteDocOrLink.UniqueID%>", "");
            }
            else {
                return false;
            }
        }

        function OpenCustomerChangeWindow() {
            var h = screen.height;
            var w = screen.width;

            window.open('../common/frmChangeOrganization.aspx?numCaseID=' + $("[id$=hdnCaseID]").val(), '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }

        function pageLoaded() {
            if ($("#sectionDueCase").css('display') == 'none') {
                $("#sectionCaseDetails").removeClass("col-md-9");
                $("#sectionCaseDetails").addClass("col-md-12");
            } else {
                $("#sectionCaseDetails").removeClass("col-md-12");
                $("#sectionCaseDetails").addClass("col-md-9");
            }
        }
        $(document).ready(function () {
            pageLoaded();
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        })
        function openCaseDueList() {
            $("#sectionDueCase").toggle();
            if ($("#sectionDueCase").css('display') == 'none') {
                $("#sectionCaseDetails").removeClass("col-md-9");
                $("#sectionCaseDetails").addClass("col-md-12");
            } else {
                $("#sectionCaseDetails").removeClass("col-md-12");
                $("#sectionCaseDetails").addClass("col-md-9");
            }
        }
    </script>
    <style type="text/css" runat="server">
        #tdDocument div {
            float: left;
            padding: 10px;
        }

        #tblMain tr td {
            border-bottom: 1px solid #e1e1e1 !important;
        }

        #tblMain tbody tr:first-child td {
            border-top: 1px solid #fff !important;
            border-left: 1px solid #fff !important;
            border-right: 1px solid #fff !important;
        }

        .tableGroupHeader {
            background-color: #f5f5f5 !important;
            font-weight: bold;
        }

        .box.box-solid.box-default > .box-header .btn {
            color: #fff;
        }

        .resizeme {
            height: 60px;
            width: 60px;
        }

        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }

        /*div.RadUpload .ruFakeInput {
            visibility: hidden;
            width: 0;
            padding: 0;
        }

        div.RadUpload .ruFileInput {
            width: 1;
        }*/

        .hidden {
            display: none;
        }

        .VerticalAligned {
            vertical-align: top;
        }
    </style>
    <style>
        .tblNoBorder td, .tblNoBorder th {
            border: 0px solid #fff !important;
        }

        @media (max-width:40em) {
            .tblNoBorder table, .tblNoBorder thead, .tblNoBorder tbody, .tblNoBorder tfoot, .tblNoBorder th, .tblNoBorder td, .tblNoBorder tr {
                display: block;
            }

            .tblNoBorder .editable_select {
                min-height: 30px;
            }

            .tblNoBorder td, .tblNoBorder th {
                text-align: left;
            }
        }

        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
    <style>
        .tblDataGrid tr:first-child td {
            background: #e5e5e5;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
    <%--<div class="row">
        <div class="col-md-12">
            <div class="col-sm-4">
                <div class="record-small-box bg-green">
                    <div class="inner">
                        <asp:Label ID="lblRecordOwner" runat="server"></asp:Label>
                    </div>
                    <asp:HyperLink ID="hplTransfer" CssClass="small-box-footer" runat="server" Visible="true" Text="Record Owner:"
                        ToolTip="Transfer Ownership">Record Owner <i class="fa fa-user"></i>
                    </asp:HyperLink>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="record-small-box bg-aqua">
                    <div class="inner">
                        <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
                    </div>
                    <a href="#" class="small-box-footer">Created By <i class="fa fa-user"></i></a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="record-small-box bg-yellow">
                    <div class="inner">
                        <asp:Label ID="lblLastModifiedBy" runat="server"></asp:Label>
                    </div>
                    <a href="#" class="small-box-footer">Last Modified By <i class="fa fa-user"></i></a>
                </div>
            </div>
        </div>
    </div>--%>
    <asp:Button ID="btnDeleteDocOrLink" OnClick="btnDeleteDocOrLink_Click" runat="server" CssClass="hidden"></asp:Button>
    <asp:HiddenField runat="server" ID="hdnFileId" />
    <asp:HiddenField runat="server" ID="hdnCaseID" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-3">
                <div class="form-inline">
                    <div class="pull-left" style="width: 90%;">
                    <div class="callout calloutGroup bg-theme">
                        <asp:Label ID="lblCustomerType" CssClass="customerType" Text="Customer" runat="server"></asp:Label>
                        <span>
                            <u> <asp:HyperLink ID="hplCustomer" runat="server" CssClass="hyperlink" Font-Size="Larger">
                            </asp:HyperLink>
                            </u>
                            <asp:Label ID="lblRelationCustomerType" CssClass="customerType" runat="server"></asp:Label>       
                         </span>
                    </div>
                      <div class="record-small-box record-small-group-box">
                        <strong>
                            <asp:Label ID="lblContactName" runat="server" CssClass="text-color-theme" Text=""></asp:Label></strong>
                        <span class="contact">
                            <asp:Label ID="lblPhone" runat="server" Text=""></asp:Label></span>
                        <a id="btnSendEmail" runat="server" href="#"><img src="../images/msg_unread_small.gif" /> </a>
                    </div>
                    </div>
                    <div class="pull-left">
                    <asp:ImageButton runat="server" style="height: 20px;
    margin-top: 13px;" ID="ibChangeCustomer" OnClientClick="return OpenCustomerChangeWindow();" ImageUrl="~/images/edit28.png" />
                        </div>
                </div>
            </div>
        <div class="col-xs-12 col-sm-5">

                <div class="record-small-box record-small-group-box createdBySection">
                     <asp:HyperLink ID="hplTransfer" runat="server" Visible="true" Text="Record Owner:"
                        ToolTip="Transfer Ownership">Owner
                    </asp:HyperLink>
                    <span class="innerCreated">
                        <asp:Label ID="lblRecordOwner" runat="server"></asp:Label>
                    </span>
                    <a href="#">Created</a>
                    <span class="innerCreated">
                        <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
                    </span>
                    <a href="#">Modified</a>
                    <span class="innerCreated">
                        <asp:Label ID="lblLastModifiedBy" runat="server"></asp:Label>
                    </span>
                </div>

        </div>

        <div class="col-md-4 ">
            <div class="pull-right createdBySection">
                <div class="customerdetail-button">
                    <asp:LinkButton ID="btnOpenCloseCase" runat="server" CssClass="btn btn-pinterest"></asp:LinkButton>
                    <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary" Text=""><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                    <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary" Text=""><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save &amp; Close
                    </asp:LinkButton><asp:LinkButton ID="btnSaveNext" runat="server" CssClass="btn btn-primary" Text=""><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save &amp; Next
                    </asp:LinkButton>
                    <asp:LinkButton ID="btnEdit" runat="server" CssClass="btn btn-primary" style="display:none" Visible="false"><i class="fa fa-pencil-square-o"></i>&nbsp;&nbsp;Edit</asp:LinkButton>
                    <%-- <asp:UpdatePanel runat="server" ID="updatedit">
                    <ContentTemplate>
                        <asp:LinkButton ID="btnEdit" runat="server" CssClass="btn btn-primary"><i class="fa fa-pencil-square-o"></i>&nbsp;&nbsp;Edit</asp:LinkButton>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnEdit" />
                    </Triggers>
                </asp:UpdatePanel>--%>

                    <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-primary" Text=""><i class="fa fa-times"></i>&nbsp;&nbsp;Close</asp:LinkButton>
                    <asp:LinkButton ID="btnActDelete" runat="server" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="display:none">
         <div class="col-sm-4">
                <div class="record-small-box bg-gray">
                    <div class="inner">
                        <asp:Label ID="lblcasenumber" ForeColor="Red" runat="server"></asp:Label>
                    </div>
                    <a href="#" class="small-box-footer">Case Number</a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="record-small-box bg-gray">
                    <div class="inner">
                        <asp:Label Text="" ID="lblCaseStatus" runat="server" CssClass="normal1" />
                    </div>
                    <a href="#" class="small-box-footer">Status</a>
                </div>
            </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="col-md-3" id="sectionDueCase" runat="server">
        <div class="box box-default">
            <div class="box-header">
                <div class="pull-left">
                    <h3 class="box-title">
                        <img src="../images/pinIcon.png" style="height: 30px;" />
                        &nbsp;Cases Due This Week</h3>
                </div>
                <div class="pull-right">
                    <asp:CheckBox ID="chkPersistCase" runat="server" OnCheckedChanged="chkPersistCase_CheckedChanged" Text="Persist " AutoPostBack="true" TextAlign="Left" />
                </div>
            </div>
            <div class="box-body">
                <asp:Repeater ID="rptDueCaseForThisWeek" runat="server">
                    <ItemTemplate>
                        <div class="row" style="padding: 10px; margin-left: 1px; border-top: 1px solid #e1e1e1; margin-right: 1px;">
                            <div class="col-md-12">
                                <div class="pull-left"><a href="../cases/frmCases.aspx?frm=Caselist&CaseID=<%#Eval("numCaseId") %>"><%#Eval("vcCaseNumber") %></a></div>
                                <div class="pull-right">
                                    <label>Due</label>: <%#Eval("TargetResolveDate") %>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <%#Eval("textDesc") %>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
    <div class="col-md-9" id="sectionCaseDetails">
        <div class="table-responsive">
            <table border="0" cellpadding="0" cellspacing="0" width="100%"  style="display:none">
                <tr>
                    <td>
                        <asp:Panel runat="server" ID="pnlContract">
                            <table width="100%" cssclass="table table-responsive tblNoBorder" align="center">
                                <tr>
                                    <td>
                                        <table width="100%" class="tr1 table table-responsive table-bordered">
                                            <tr>
                                                <td width="10%" align="right">Contract :
                                                </td>
                                                <td width="20%">
                                                    <asp:Label Text="" ID="lblContractName" runat="server" CssClass="normal1" />
                                                    <%--<asp:DropDownList ID="ddlContract" AutoPostBack="true" Width="180" runat="server"
									CssClass="signup">
								</asp:DropDownList>--%>
                                                </td>
                                                <td width="10%" align="right">Amount Balance :
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblRemAmount" CssClass="normal1"></asp:Label>
                                                </td>
                                                <td width="10%" align="right">Days Remaining :
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblRemDays" CssClass="normal1"></asp:Label>
                                                </td>
                                                <td width="10%" align="right">Incidents Remaining :
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblRemInci" CssClass="normal1"></asp:Label>
                                                </td>
                                                <td width="10%" align="right">Hours Remaining :
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblRemHours" CssClass="normal1"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                    </td>
                </tr>
            </table>
        </div>
          <asp:Table ID="tblMain" class="table table-responsive tblNoBorder" CellPadding="3" CellSpacing="3" align="center" Width="100%"
                                                GridLines="none" border="0" runat="server">
                                            </asp:Table>
         <asp:Table ID="tblOppr" style="display:none" runat="server" BorderColor="black" BorderWidth="0" CellPadding="0"
                        CellSpacing="0" CssClass="table table-responsive tblNoBorder" GridLines="None" Height="300" Width="100%">
                        <asp:TableRow>
                            <asp:TableCell VerticalAlign="Top">
                                <table width="100%" class="table table-responsive tblNoBorder" style="padding-bottom:0px;">

                                 
                                    <tr>
                                        <td valign="top" colspan="2" style="padding:0px;">
                                          
                                        </td>
                                    </tr>
                                    <tr style="display:none">
                                        <td colspan="2" align="left">
                                            <table width="100%" class="table table-responsive tblNoBorder" border="0">
                                                <asp:Panel ID="pnlKnowledgeBase" runat="server" Visible="false">
                                                    <tr>
                                                        <td class="normal1" align="left">
                                                            <%--Solutions added from<br>
															the Knowledge Base--%>
                                                            <telerik:RadComboBox ID="radCmbSolution" Width="195px" DropDownWidth="200px" Skin="Default"
                                                                runat="server" AllowCustomText="True" EnableLoadOnDemand="True" Text="Enter Solution Name">
                                                            </telerik:RadComboBox>
                                                            <%--    <rad:RadComboBox ID="radCmbSolution" Width="195px" DropDownWidth="200px" Skin="WindowsXP"
                                                        runat="server" AllowCustomText="True" EnableLoadOnDemand="True" SkinsPath="~/RadControls/ComboBox/Skins"
                                                        Text="Enter Solution Name">
                                                    </rad:RadComboBox>--%>
                                                    &nbsp;
                                                    <asp:Button ID="btnAddSolution" runat="server" CssClass="btn btn-primary" Text="Add" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:DataGrid ID="dgSolution" runat="server" CssClass="table table-responsive table-bordered tblDataGrid" Width="100%" ShowFooter="False"
                                                                AutoGenerateColumns="false">
                                                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>

                                                                <Columns>
                                                                    <asp:BoundColumn Visible="False" DataField="numSolnID"></asp:BoundColumn>
                                                                    <asp:ButtonColumn DataTextField="Solution Name" HeaderText="Solution Name" CommandName="Solution"></asp:ButtonColumn>
                                                                    <asp:TemplateColumn HeaderText="Solution Description">
                                                                        <ItemTemplate>
                                                                            <%#Eval("ShortDesc").ToString()%>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                    <asp:TemplateColumn>
                                                                        <ItemStyle Width="10"></ItemStyle>
                                                                        <ItemTemplate>
                                                                            <asp:Button ID="btnSolDelete" runat="server" CssClass="btn btn-danger" Text="X" CommandName="Delete"></asp:Button>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                            <br>
                                                        </td>
                                                    </tr>
                                                </asp:Panel>
                                                <tr>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td class="normal1" align="left">
                                                        <div class="box box-default box-solid">
                                                            <div class="box-header with-border">
                                                                <h3 class="box-title">Linked Items</h3>

                                                                <div class="box-tools pull-right">
                                                                    <asp:Button ID="btnAddItem" runat="server" Text="Add" CssClass="btn btn-primary"></asp:Button>&nbsp;
                                                <%--<asp:HyperLink runat="server" ID="hplLinkedItems" CssClass="normal1Right" Text="Associate Order Item"
															Font-Bold="true"></asp:HyperLink>&nbsp;<asp:Label runat="server" ID="lblLinkedItemCount"
																CssClass="normal1" Font-Bold="true"></asp:Label>--%>
                                                                    <asp:Button ID="btnRemove" runat="server" Text="Remove Selected" CssClass="btn btn-danger"
                                                                        OnClientClick="GetSelectedItems();"></asp:Button>
                                                                </div>
                                                                <!-- /.box-tools -->
                                                            </div>
                                                            <!-- /.box-header -->
                                                            <div class="box-body">
                                                                <asp:DataGrid ID="dgLinkedItems" runat="server" CssClass="table table-responsive table-bordered tblDataGrid" Width="100%" AutoGenerateColumns="False">
                                                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>

                                                                    <Columns>
                                                                        <asp:TemplateColumn HeaderText="">
                                                                            <HeaderTemplate>
                                                                                <asp:CheckBox ID="chk" runat="server" onclick="SelectAll1('dgLinkedItems_ctl01_chk')" />
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chk" runat="server" />
                                                                                <asp:Label runat="server" ID="lblOppItemID" Text='<%# Eval("numoppitemtCode") %>'
                                                                                    Style="display: none"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateColumn>
                                                                        <asp:BoundColumn DataField="numoppitemtCode" Visible="false"></asp:BoundColumn>
                                                                        <asp:BoundColumn DataField="numOppId" Visible="false"></asp:BoundColumn>
                                                                        <asp:ButtonColumn DataTextField="numOppId" HeaderText="Order ID" CommandName="Order"></asp:ButtonColumn>
                                                                        <%--<asp:TemplateColumn HeaderText="Opprtunity ID" >
                                                                <ItemTemplate>
                                                                <a href="#" onclick="javascript:OpenOrder('<%#  Eval("numOppId") %>');"><%#  Eval("numOppId") %></a>
                                                                </ItemTemplate>
                                                                </asp:TemplateColumn>--%>
                                                                        <asp:TemplateColumn HeaderText="Image">
                                                                            <ItemTemplate>
                                                                                <div style="height: 65px; width: 65px;">
                                                                                    <%#CCommon.GetImageHTML(Eval("vcPathForTImage"), 1)%>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateColumn>
                                                                        <asp:BoundColumn HeaderText="Item Name" DataField="vcItemName"></asp:BoundColumn>
                                                                        <asp:BoundColumn HeaderText="Model #" DataField="vcModelId"></asp:BoundColumn>
                                                                        <asp:BoundColumn HeaderText="Description" DataField="vcItemDesc"></asp:BoundColumn>
                                                                        <asp:BoundColumn HeaderText="Units" DataField="numUnitHour"></asp:BoundColumn>
                                                                        <asp:BoundColumn HeaderText="Total Amount" DataField="monTotAmount" DataFormatString="{0:#,###.00}"></asp:BoundColumn>
                                                                        <asp:BoundColumn HeaderText="Serial No" DataField="vcSerialNo"></asp:BoundColumn>
                                                                    </Columns>
                                                                </asp:DataGrid>
                                                            </div>
                                                            <!-- /.box-body -->
                                                        </div>



                                                        <br />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <uc2:frmComments ID="frmComments1" runat="server" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
        <telerik:RadTabStrip ID="radOppTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
            Skin="Default" ClickSelectedTab="True" AutoPostBack="True" SelectedIndex="0"
            MultiPageID="radMultiPage_OppTab">
            <Tabs>
             <%--   <telerik:RadTab Text="&nbsp;&nbsp;Case Details&nbsp;&nbsp;" Value="Details" PageViewID="radPageView_Details">
                </telerik:RadTab>--%>
                <telerik:RadTab Text="&nbsp;&nbsp;Associated Contacts&nbsp;&nbsp;" Value="AssociatedContacts"
                    PageViewID="radPageView_AssociatedContacts">
                </telerik:RadTab>
                <telerik:RadTab Text="Correspondence" Value="Correspondence" PageViewID="radPageView_Correspondence">
                </telerik:RadTab>
                <telerik:RadTab Text="Collaboration" Value="Collaboration" PageViewID="radPageView_Collaboration">
                </telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="radMultiPage_OppTab" runat="server" SelectedIndex="0" CssClass="pageView">
           <%-- <telerik:RadPageView ID="radPageView_Details" runat="server" Visible="false">
                <div class="table-responsive">
                   
                </div>
            </telerik:RadPageView>--%>
            <telerik:RadPageView ID="radPageView_AssociatedContacts" runat="server">
                <div class="table-responsive">
                    <asp:Table ID="Table2" CellPadding="0" CellSpacing="0" BorderWidth="0" runat="server"
                        CssClass="aspTable" Width="100%" BorderColor="black" GridLines="None" Height="300">
                        <asp:TableRow>
                            <asp:TableCell VerticalAlign="Top">
                                <uc1:frmAssociatedContacts ID="frmAssociatedContacts1" runat="server" PageType="CaseIP" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </telerik:RadPageView>
            <telerik:RadPageView ID="radPageView_Correspondence" runat="server">
                <div class="table-responsive">
                    <asp:Table ID="table4" runat="server" BorderWidth="0" Width="100%" BackColor="white"
                        CellSpacing="0" CellPadding="0" BorderColor="black" GridLines="None" Height="350"
                        CssClass="aspTable">
                        <asp:TableRow>
                            <asp:TableCell VerticalAlign="Top">
                                <table width="100%" style="border:1px solid #fff;">
                                    <tr>
                                        <td>
                                            <uc1:frmCorrespondence ID="Correspondence1" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
            </telerik:RadPageView>
             <telerik:RadPageView ID="radPageView_Collaboration" runat="server">
            <uc1:frmCollaborationUserControl ID="frmCollaborationUserControl" runat="server" />
        </telerik:RadPageView>
        </telerik:RadMultiPage>
        <table width="100%">
            <tr>
                <td class="normal4" align="center">
                    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                </td>
            </tr>
        </table>
    </div>
    <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtDivId" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtCntId" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtEmailTotalPage" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtEmailTotalRecords" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:HiddenField ID="hdnOppItemIDs" runat="server" />
    <asp:HiddenField ID="hdnStatus" runat="server" />
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <telerik:RadWindow RenderMode="Lightweight" runat="server" ID="RadWinDocumentFiles" Title="Documents / Files" RestrictionZoneID="ContentTemplateZone" Modal="true" Behaviors="Resize,Close,Move" Width="800" Height="400">
        <ContentTemplate>
            <asp:UpdatePanel ID="Updatepanel2" runat="server" UpdateMode="Conditional">
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnAttach" />
                    <asp:PostBackTrigger ControlID="btnLinkClose" />
                </Triggers>
                <ContentTemplate>
                    <div>
                        <table border="0" style="width: 100%; text-align: right; padding-right: 5px;">
                            <tr>
                                <td>
                                    <input type="button" id="btnCloseDocumentFiles" onclick="return CloseDocumentFiles('C');" class="btn btn-primary" style="color: white" value="Close" /></td>
                            </tr>
                        </table>
                        <div class="col-xs-12" style="padding-top: 40px;">
                            <div class="form-inline">
                                <table border="0" style="width: 100%;">
                                    <tr>
                                        <td>
                                            <img src="../images/icons/Attach.png"></td>
                                        <td style="font-size: small; font-weight: bold; padding-left: 2px; padding-right: 2px;">Upload a Document from your Desktop</td>
                                        <td colspan="2">
                                            <%--<telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" ClientEvents-OnRequestStart="requestStart" LoadingPanelID="LoadingPanel1">--%>
                                            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel1">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <input class="signup" id="fileupload" type="file" name="fileupload" runat="server">
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnAttach" OnClick="btnAttach_Click" runat="server" Text="Attach & Close" CssClass="btn btn-primary"></asp:Button>
                                                            <asp:Button ID="btnLinkClose" OnClick="btnLinkClose_Click" runat="server" CssClass="hidden"></asp:Button></td>
                                                    </tr>
                                                </table>
                                            </telerik:RadAjaxPanel>
                                            <telerik:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server" InitialDelayTime="0" Skin="Default">
                                            </telerik:RadAjaxLoadingPanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="../images/icons/Link.png">&nbsp;</td>
                                        <td style="font-size: small; font-weight: bold; padding-left: 2px; padding-right: 2px;">Link to a Document</td>
                                        <td>
                                            <telerik:RadTextBox runat="server" ID="txtLinkName" CssClass="inlineFormInput" Width="250px" EmptyMessage="Link Name"></telerik:RadTextBox></td>
                                        <td>
                                            <asp:Button ID="btnLink" runat="server" OnClientClick="return CloseDocumentFiles('L');" Text="Link & Close" CssClass="btn btn-primary"></asp:Button></td>
                                        <td>
                                            <asp:Label ID="Label1" Text="[?]" CssClass="tip" runat="server" ToolTip="Linking to a file on Google Drive or any other file repository is a good way to preserve your BizAutomation storage space. 
To avoid login credentials when clicking the  link, just make it public (e.g. On Google Drive you have to make sure that �Link Sharing� is set to �Public on the web�)." /></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;&nbsp;</td>
                                    </tr>
                                    <tr style="text-align: left;">
                                        <td colspan="2"></td>
                                        <td colspan="2">
                                            <telerik:RadTextBox RenderMode="Lightweight" runat="server" ID="txtLinkURL" Width="200px" EmptyMessage="Paste link here" TextMode="MultiLine" Height="100px" Resize="None"></telerik:RadTextBox>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </telerik:RadWindow>


    <asp:HiddenField ID="hdnRecordOwner" runat="server" />
    <script type="text/javascript">
        function OpenItem(a) {
            str = "../Items/frmKitDetails.aspx?ItemCode=" + a;
            var win = window.open(str, '_blank');
            if (win) {
                //Browser has allowed it to be opened
                win.focus();
            } else {
                //Browser has blocked it
                alert('Please allow popups for this website');
            }
            //window.location.href = str;
        }
        // select all desired input fields and attach tooltips to them
        $("#Form1").find("span.tip").tooltip({

            // place tooltip on the right edge
            position: "center right",

            // a little tweaking of the position
            offset: [-2, 10],

            // use the built-in fadeIn/fadeOut effect
            effect: "fade",

            // custom opacity setting
            opacity: 0.7

        });
        function openSolutionPopup(caseId) {
            window.open("../cases/frmAddCaseSolution.aspx?CaseId=" + caseId, '', 'toolbar=no,titlebar=no,top=200,left=200,width=1000,height=650,scrollbars=yes,resizable=yes');
        }
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    <!-- Modal -->

    <a href="#" onclick="openCaseDueList()"><i style="color:#000" class="fa fa-bars"></i></a>&nbsp;&nbsp;Case Details&nbsp;&nbsp;<asp:Label ID="lblCasedetails" runat="server" ForeColor="Gray" />&nbsp;<a href="#" onclick="return OpenHelpPopUp('cases/frmcases.aspx')"><label class="badge bg-yellow">?</label></a>

</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">

      <a href="#" id="hrefWarrantyImageName" runat="server"><img src="../images/tag.png" align="BASELINE" style="float:none;"></a>
        <asp:Label ID="lblWarrantyItemName" runat="server" Text="" style="padding-top: 6px;
    /* margin-top: 36px; */
    display: inline-block;"></asp:Label>

    <div class="btn btn-default btn-sm" runat="server" id="btnLayout" style="float:right">
        <span><i class="fa fa-columns"></i>&nbsp;&nbsp;Layout</span>&nbsp;&nbsp;
    </div>
    <div style="float: right;margin-right:10px; text-align: center;margin-top:2px" id="divContractsDetails" runat="server">
       
        <div class="form-inline">
            <asp:Label ID="lblWarrantyTooltip" Text="[?]" CssClass="tip" runat="server" ToolTip="" />
            <i>Warrantee days left</i>
        <asp:Label ID="lblWarrantyDaysLeft" CssClass="lblHeighlight" runat="server" Text="0"></asp:Label>
            <i>Incidents left</i>
        <asp:Label ID="lblIncidentLeft" CssClass="lblHeighlight" runat="server" Text="0"></asp:Label>
            </div>
    </div>
    <div style="float: right;margin-right:10px; text-align: center;">
                                                <table width="100%" border="0" style="padding-right: 5px;">
                                                    <tr>
                                                        <td>
                                                            <img id="imgDocument" runat="server" src="../images/icons/AttachSmall.png" alt="" />
                                                        </td>
                                                        <td style="text-align: left;" runat="server" id="tdDocument"></td>
                                                        <td>
                                                            <asp:ImageButton ID="imgOpenDocument" runat="server" ImageUrl="~/images/icons/drawer.png" Style="height: 30px;" />
                                                        </td>
                                                        <td style="text-align: left;">
                                                            <asp:HyperLink ID="hplOpenDocument" runat="server" Font-Bold="true" Font-Underline="true" Text="Attach or Link" Style="display: inline-block; vertical-align: top; line-height: normal;"></asp:HyperLink>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: top;">
                                                            <img src="../images/icons/LinkSmall.png" id="imgLink" runat="server">
                                                        </td>
                                                        <td style="text-align: left;" runat="server" id="tdLink"></td>
                                                    </tr>
                                                </table>
                                                <%--<asp:HyperLink ID="hplDocumentCount" runat="server" Style="display: inline-block; vertical-align: middle; line-height: normal;"></asp:HyperLink>--%>
                                            </div>
</asp:Content>
