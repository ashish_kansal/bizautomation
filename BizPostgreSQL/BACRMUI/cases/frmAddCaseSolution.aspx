﻿<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmAddCaseSolution.aspx.vb"
    Inherits="BACRM.UserInterface.Cases.frmAddCaseSolution" MasterPageFile="~/common/PopupBootstrap.Master" %>

<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Case</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script language="javascript" src="../javascript/CustomFieldValidation.js"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
        <%--    <asp:Button ID="btnSaveClose" runat="server" Text="Save &amp; Close" CssClass="button"></asp:Button>--%>
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="50"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Assign Solution For Case
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
    <script>   function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }</script>
    <style>
        .tab-content {
            border: 1px solid #1473b4;
            border-top: 0px;
            margin-top: -21px;
            padding: 10px;
        }

        .nav-tabs {
            border-top: 4px solid #1473b4;
            background: #1473b4;
            border-radius: 4px;
            border-left: 4px solid #1473b4;
        }

            .nav-tabs > li > a {
                color: #fff !important;
            }

            .nav-tabs > li.active > a {
                color: #1473b4 !important;
            }

            .nav-tabs > li > a:hover {
                background-color: #3198de;
                color: #71b3e9;
                border: none !important;
            }
            .tblPrimary tbody tr th{
                color:#fff !important;
            }
            .tab-content>.tab-pane{
                margin-top:24px !important;
            }
    </style>
    <div class="form-inline" style="margin-bottom:10px;">
        <label>Solution message: </label>
        <asp:TextBox ID="txtSolutionName" style="width:50%" CssClass="form-control" runat="server"></asp:TextBox>
        <asp:Button ID="btnAddSolution" OnClick="btnAddSolution_Click" runat="server"  CssClass="btn btn-sm btn-primary" Text="Save & Close" />
    </div>
    <ul class="nav nav-tabs nav-primary">
        <li class="active"><a data-toggle="tab" href="#SolutionArticles">Solution Articles Found (<asp:Label ID="lblSolutionCount" runat="server" Text="0"></asp:Label>)
        </a></li>
        <li><a data-toggle="tab" href="#SearchKnowledgeBase">Search the Knowledgebase for Solutions
        </a></li>
    </ul>

    <div class="tab-content">
        <div id="SolutionArticles" class="tab-pane fade in active">
            <asp:UpdatePanel ID="updatePanelSolution" runat="server">
                <ContentTemplate>
            <asp:DataGrid ID="dgSolution" runat="server" ShowHeader="true" CssClass="table table-responsive tblPrimary table-bordered tblDataGrid" Width="100%" ShowFooter="False"
                AutoGenerateColumns="false">
                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                <HeaderStyle CssClass="bg-theme" />
                <Columns>
                    
                    <asp:BoundColumn Visible="False" DataField="numSolnID"></asp:BoundColumn>
                    <asp:ButtonColumn DataTextField="Solution Name" HeaderText="Solution Name" CommandName="Solution"></asp:ButtonColumn>
                    <asp:TemplateColumn HeaderText="Solution Description">
                        <ItemTemplate>
                            <%#Eval("ShortDesc").ToString()%>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                    <asp:TemplateColumn>
                        <ItemStyle Width="10"></ItemStyle>
                        <ItemTemplate>
                            <asp:Button ID="btnSolDelete" runat="server" CssClass="btn btn-danger" Text="X" CommandName="Delete"></asp:Button>
                        </ItemTemplate>
                    </asp:TemplateColumn>
                </Columns>
            </asp:DataGrid>
                    </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div id="SearchKnowledgeBase" class="tab-pane fade">
              <asp:UpdatePanel ID="updatePanel1" runat="server">
                <ContentTemplate>
            <div class="col-md-12">
            <div class="form-inline" style="margin-bottom:10px !important">
                <label>Search Knowledgebase:</label>
                <asp:TextBox ID="txtSearchKnowledgeBase" style="width:50%" CssClass="form-control" runat="server"></asp:TextBox>
                <asp:Button ID="btnSearch" CssClass="btn btn-sm btn-primary" OnClick="btnSearch_Click" runat="server" Text="Search" />
                <asp:Button ID="btnSave" CssClass="btn btn-sm btn-primary" OnClick="btnSave_Click" runat="server" Text="Save" />
                <asp:HiddenField ID="hdnSelectedSolutionId" value="0" runat="server" />
            </div>
                </div>
            <div class="clearfix"></div>
           <asp:DataGrid ID="dgBases" runat="server" Width="100%" CssClass="table table-striped table-bordered tblPrimary" AllowSorting="True"
                    AutoGenerateColumns="False" UseAccessibleHeader="true">
                    <Columns>
                        <asp:TemplateColumn HeaderText="Solution Name">
                            <ItemTemplate>
                                <asp:Label ID="lblNumSolID" runat="server" Text='<%#Eval("numSolnID") %>' Visible="false"></asp:Label>
                                <asp:HyperLink ID="hlSolutionName" Text='<%# Eval("vcSolnTitle") %>' NavigateUrl='<%#Eval("numSolnID", "../cases/frmSolution.aspx?SolId={0}") %>' runat="server">
                                </asp:HyperLink>

                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="<font>Solution Description</font>">
                            <ItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text='<%# CCommon.Truncate(Eval("txtSolution").toString(),100) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>

                        <asp:TemplateColumn>
                            <ItemStyle Width="150px"></ItemStyle>
                            <ItemTemplate>
                                <asp:Button ID="btnAddToCase" CommandName="AddSolutionToCase" CssClass="btn btn-xs btn-primary" runat="server" Text="Add" />
                                <asp:ImageButton ID="btnEditKnowledgeBase" CommandName="Edit" ImageUrl="~/images/edit28.png" Height="15px" runat="server" />
                                <asp:LinkButton ID="btnSolDelete" runat="server" CssClass="btn btn-danger btn-xs" CommandName="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                                <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
													<font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                    </ContentTemplate>
                </asp:UpdatePanel>
        </div>

    </div>
</asp:Content>
