<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmAddCases.aspx.vb"
    Inherits="BACRM.UserInterface.Cases.frmAddCases" MasterPageFile="~/common/Popup.Master" %>

<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Case</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script language="javascript" src="../javascript/CustomFieldValidation.js"></script>
    <script src="../JavaScript/jquery.ae.image.resize.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            var firstColumnWidth = 0;

            if ($('#tblMain td:first-child') != null) {
                var firstColumnWidth = $('#tblMain td:first-child').width();
            }

            if (firstColumnWidth != 0) {
                $('#tblCustomer td:first-child').width(firstColumnWidth);
                $('#tblOrder td:first-child').width(firstColumnWidth);
            }

            var width = document.getElementById("resizeDiv").offsetWidth;
            var height = document.getElementById("resizeDiv").offsetHeight;
            window.resizeTo(width + 35, height + 150);
        });

        function Save() {
            if ($find('radCmbCompany').get_value() == "") {
                alert("Select Organization")
                return false;
            }
            if ((document.getElementById("ddlContacts").selectedIndex == -1) || (document.getElementById("ddlContacts").value == 0)) {
                alert("Select Contact")
                document.getElementById("ddlContacts").focus();
                return false;
            }
            //            if (document.getElementById("txtSubject").value == "") {
            //                alert("Enter Subject")
            //                document.getElementById("txtSubject").focus();
            //                return false;
            //            }
            //            if (document.getElementById("calResolve_txtDate").value == "") {
            //                alert("Enter Resolve Date")
            //                document.getElementById("calResolve_txtDate").focus();
            //                return false;

            //            }
            //            if (document.getElementById("txtDescription").value == "") {
            //                alert("Enter Description")
            //                document.getElementById("txtDescription").focus();
            //                return false;
            //            }

            return validateCustomFields();
        }
        //This is used to activate the Grid Row whenever we click on a radio button
        function activateGridRow(theIndex) {
            igtbl_getGridById('uwOrders').Rows.getRow(theIndex).activate();
        }

        //resize the images without distorting the proportions
        $(function () {
            $(".resizeme").aeImageResize({ height: 100, width: 100 });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSaveClose" runat="server" Text="Save &amp; Close" CssClass="button"></asp:Button>
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="50"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    New Case
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
    <%--    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>--%>
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="830px" CssClass="aspTable" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell Width="32" VerticalAlign="Top">
                <img src="../images/SuitCase-32.gif" alt="" style="margin-top:5px; margin-left:5px" />
            </asp:TableCell>
            <asp:TableCell>
                <table id="tblCustomer" border="0" width="100%" cellpadding="2">
                    <tr>
                        <td class="normal1Right" align="right">Organization<font color="red">*</font>
                        </td>
                        <td class="normal1" colspan="3">
                            <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                                OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                ClientIDMode="Static"
                                ShowMoreResultsBox="true"
                                Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True">
                                <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                            </telerik:RadComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1Right" align="right">Contact<font color="red">*</font>
                        </td>
                        <td class="normal1" colspan="3">
                            <asp:DropDownList ID="ddlContacts" runat="server" Width="200" CssClass="signup" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <asp:Table ID="tblMain" Width="100%" runat="server" CellPadding="2" Style="margin-top: -8px">
                </asp:Table>
                <table id="tblOrder" border="0" width="100%" cellpadding="2">
                    <tr>
                        <td colspan="4">
                            <div style="float:left;width:10%;padding-top: 5px;">
                                Recent Orders
                            </div>
                            <div  style="float:right;width:90%">
                            <telerik:RadGrid ID="gvOrders" runat="server" Width="100%" AutoGenerateColumns="False"
                                GridLines="None" ShowFooter="false" Skin="windows" EnableEmbeddedSkins="false"
                                CssClass="dg" AlternatingItemStyle-CssClass="ais" ItemStyle-CssClass="is" HeaderStyle-CssClass="hs">
                                <MasterTableView DataKeyNames="numOppId" HierarchyLoadMode="Client" DataMember="Orders">
                                    <DetailTables>
                                        <telerik:GridTableView Width="100%" runat="server" ItemStyle-HorizontalAlign="Center"
                                            AlternatingItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            ShowFooter="true" DataKeyNames="numOppId,numoppitemtCode" DataMember="OrderItems"
                                            CssClass="dg" AlternatingItemStyle-CssClass="ais" ItemStyle-CssClass="is" HeaderStyle-CssClass="hs">
                                            <ParentTableRelation>
                                                <telerik:GridRelationFields DetailKeyField="numOppId" MasterKeyField="numOppId" />
                                            </ParentTableRelation>
                                            <Columns>
                                                <telerik:GridTemplateColumn HeaderStyle-Width="25" ItemStyle-Width="25">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridBoundColumn HeaderText="numoppitemtCode" Visible="false" DataField="numoppitemtCode">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="numOppId" Visible="false" DataField="numOppId">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="numItemCode" Visible="false" DataField="numItemCode">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridTemplateColumn HeaderText="Image">
                                                    <ItemTemplate>
                                                        <%#CCommon.GetImageHTML(Eval("vcPathForTImage"), 1)%>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridBoundColumn HeaderText="Item Name" ItemStyle-Width="70%" DataField="vcItemName">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Model #" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" DataField="vcModelId">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Description" ItemStyle-Width="30%" DataField="vcItemDesc">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Units" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" DataField="numUnitHour">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Total Amount" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" DataField="monTotAmount"
                                                    DataFormatString="{0:0.00}">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Serial No" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" DataField="vcSerialNo">
                                                </telerik:GridBoundColumn>
                                            </Columns>
                                        </telerik:GridTableView>
                                    </DetailTables>
                                    <Columns>
                                        <telerik:GridBoundColumn HeaderText="Date Created" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" DataField="bintCreatedDate">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Order ID" ItemStyle-Width="100%" DataField="vcPOppName">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Order Amount" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" DataField="monDealAmount" DataFormatString="{0:0.00}">
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="true" />
                            </telerik:RadGrid>
                                </div>
                        </td>
                    </tr>
                </table>
                <br />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
