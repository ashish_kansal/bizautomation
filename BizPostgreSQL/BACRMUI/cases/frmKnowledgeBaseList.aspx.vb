﻿
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Namespace BACRM.UserInterface.Cases
    Public Class frmKnowledgeBaseList
        Inherits BACRMPage
        Dim strColumn As String
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim RegularSearch As String
        Dim CustomSearch As String
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If GetQueryStringVal("SI") <> "" Then
                    SI = GetQueryStringVal("SI")
                End If
                If GetQueryStringVal("SI1") <> "" Then
                    SI1 = GetQueryStringVal("SI1")
                Else : SI1 = 0
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    SI2 = GetQueryStringVal("SI2")
                Else : SI2 = 0
                End If
                If GetQueryStringVal("frm") <> "" Then
                    frm = GetQueryStringVal("frm")
                Else : frm = ""
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = GetQueryStringVal("frm1")
                Else : frm1 = ""
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    frm2 = GetQueryStringVal("frm2")
                Else : frm2 = ""
                End If

                ' Checking Rights for View
                GetUserRightsForPage(7, 2)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AC")


                If Not IsPostBack Then
                    'PersistTable.Load()

                    'If PersistTable.Count > 0 Then
                    '    txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                    '    txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                    '    txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                    '    txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))
                    '    'ddlSort.ClearSelection()
                    '    'If Not ddlSort.Items.FindByValue(CCommon.ToString(PersistTable(PersistKey.FilterBy))) Is Nothing Then ddlSort.Items.FindByValue(CCommon.ToString(PersistTable(PersistKey.FilterBy))).Selected = True
                    '    txtGridColumnFilter.Text = CCommon.ToString(PersistTable(PersistKey.GridColumnSearch))
                    'End If

                    objCommon.sb_FillComboFromDBwithSel(ddlCategory, 34, Session("DomainID"))
                    BindDatagrid()
                End If
                ClientScript.RegisterClientScriptBlock(Me.GetType, "SetTab", "if (parent.parent.frames.length > 0) { parent.parent.SelectTabByValue('4');}else{ parent.SelectTabByValue('4'); } ", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Sub BindDatagrid(Optional ByVal CreateCol As Boolean = True)
            Try
                Dim SortChar As Char
                If ViewState("SortChar") <> "" Then
                    SortChar = ViewState("SortChar")
                Else : SortChar = "0"
                End If


                Dim objCases As New CCases

                Dim dtSolution As DataTable
                Dim objSolution As New Solution

                objSolution.Category = ddlCategory.SelectedItem.Value
                objSolution.KeyWord = txtKeyWord.Text
                objSolution.SortCharacter = SortChar
                objSolution.DomainID = Session("DomainID")
                objSolution.DeleteSolution()



                If strColumn <> "" Then objSolution.columnName = strColumn
                If Session("Asc") = 1 Then
                    objSolution.columnSortOrder = "Asc"
                Else : objSolution.columnSortOrder = "Desc"
                End If
                dtSolution = objSolution.GetKnowledgeBases

                'Persist Form Settings
                'PersistTable.Clear()
                'PersistTable.Add(PersistKey.CurrentPage, IIf(dtSolution.Rows.Count > 0, txtCurrrentPage.Text, "1"))
                'PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())
                'PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
                'PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text)

                'PersistTable.Save()



                lblRecordSolution.Text = objSolution.TotalRecords
                dgBases.DataSource = dtSolution
                dgBases.DataBind()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgBases_DeleteCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgBases.DeleteCommand
            Try
                Dim objSolution As New Solution
                Dim lblnumSolID As Label = CType(e.Item.FindControl("lblNumSolID"), Label)
                objSolution.SolID = CCommon.ToInteger(lblnumSolID.Text)
                objSolution.DeleteSolution()

                BindDatagrid()
            Catch ex As Exception
                Throw ex
            End Try


        End Sub


        Private Sub dgBases_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgBases.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim btnDelete As LinkButton
                    Dim lnkDelete As LinkButton
                    lnkDelete = e.Item.FindControl("lnkdelete")
                    btnDelete = e.Item.FindControl("btnSolDelete")
                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                        btnDelete.Visible = False
                        lnkDelete.Visible = True
                        lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    Else : btnDelete.Attributes.Add("onclick", "return DeleteRecord1()")
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub btnGoBases_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGoBases.Click
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub


        'Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
        '    Try
        '        BindDatagrid(True)
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub
        'Sub GridColumnSearchCriteria()
        '    Try
        '        If txtGridColumnFilter.Text.Trim.Length > 0 Then
        '            Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")

        '            Dim strIDValue(), strID(), strCustom As String
        '            Dim strRegularCondition As New ArrayList
        '            Dim strCustomCondition As New ArrayList


        '            For i As Integer = 0 To strValues.Length - 1
        '                strIDValue = strValues(i).Split(":")
        '                strID = strIDValue(0).Split("~")

        '                If strID(0).Contains("CFW.Cust") Then

        '                    Select Case strID(3).Trim()
        '                        Case "TextBox"
        '                            strCustom = " like '%" & strIDValue(1).Replace("'", "''") & "%'"
        '                        Case "SelectBox"
        '                            strCustom = "=" & strIDValue(1)
        '                    End Select

        '                    strCustomCondition.Add("CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & strCustom)
        '                Else
        '                    Select Case strID(3).Trim()
        '                        Case "Website", "Email", "TextBox"
        '                            strRegularCondition.Add(strID(0) & " like '%" & strIDValue(1).Replace("'", "''") & "%'")
        '                        Case "SelectBox"
        '                            strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
        '                        Case "TextArea"
        '                            strRegularCondition.Add(" Cast(" & strID(0) & " as varchar(5000)) like '%" & strIDValue(1).Replace("'", "''") & "%'")
        '                    End Select
        '                End If
        '            Next
        '            RegularSearch = String.Join(" and ", strRegularCondition.ToArray())
        '            CustomSearch = String.Join(" and ", strCustomCondition.ToArray())
        '        Else
        '            RegularSearch = ""
        '            CustomSearch = ""
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub
    End Class
End Namespace


