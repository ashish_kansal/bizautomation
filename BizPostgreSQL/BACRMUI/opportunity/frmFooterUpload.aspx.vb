Imports BACRM.BusinessLogic.Opportunities
Imports System.IO
Imports BACRM.BusinessLogic.Common
Partial Public Class frmFooterUpload
    Inherits BACRMPage

    Dim strFilePath, strFileName As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            btnCancel.Attributes.Add("onclick", "return Close()")
            
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim objOpportunity As New OppBizDocs
            objOpportunity.DomainID = Session("DomainID")
            If GetQueryStringVal( "OppType") = 1 Then
                objOpportunity.byteMode = 2
            Else
                objOpportunity.byteMode = 4
            End If
            'commented by Sachin Sadhu
            'objOpportunity.ManageBizDocFooter()
            Dim iTemplateID As Int32
            iTemplateID = Convert.ToInt32(Server.UrlDecode(CCommon.ToString(GetQueryStringVal("TemplateID"))))

            objOpportunity.SaveBizDocFooter(iTemplateID)

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            MagUpload()
            Dim objOpportunity As New OppBizDocs
            objOpportunity.DomainID = Session("DomainID")
            Dim iTemplateID As Int32
            iTemplateID = Convert.ToInt32(Server.UrlDecode(CCommon.ToString(GetQueryStringVal("TemplateID"))))

            If GetQueryStringVal( "OppType") = 1 Then
                objOpportunity.byteMode = 1
            Else
                objOpportunity.byteMode = 3
            End If
            objOpportunity.vcFooter = strFileName

            'commented by Sachin Sadhu
            'objOpportunity.ManageBizDocFooter()

            'Added By Sachin Sadhu||Date:24thDec2013
            'Purpose:only BizDoc related pages save footer Logo in -BizDocTemplate table ,other pages save in  Domain Table
            If GetQueryStringVal("frm") = "BizDocs" Then
                objOpportunity.SaveBizDocFooter(iTemplateID)
            Else
                objOpportunity.ManageBizDocFooter()
            End If
            'end of code by sachin


            Response.Write("<script>window.close();</script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub MagUpload()
        Try
            If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
            End If
            Dim FilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID"))
            Dim ArrFilePath, ArrFileExt, ArrFileName
            Dim strFileExt As String

            If txtMagFile.PostedFile.FileName = "" Then Exit Sub

            strFilePath = FilePath
            ArrFileExt = Split(txtMagFile.PostedFile.FileName, ".")
            ArrFileName = Split(txtMagFile.PostedFile.FileName, "\")
            strFileExt = ArrFileExt(UBound(ArrFileExt))
            strFileName = "File" & Day(DateTime.Now) & Month(DateTime.Now) & Year(DateTime.Now) & Hour(DateTime.Now) & Minute(DateTime.Now) & Second(DateTime.Now) & "." & strFileExt
            strFilePath = strFilePath & "\" & strFileName

            If Not txtMagFile.PostedFile Is Nothing Then txtMagFile.PostedFile.SaveAs(strFilePath)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class