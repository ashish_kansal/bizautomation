Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common

Partial Public Class frmBizDocAttachments
    Inherits BACRMPage
    Dim OppType As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            OppType = CCommon.ToInteger(GetQueryStringVal("OppType"))

            If Not IsPostBack Then
                
                
                objCommon.sb_FillComboFromDBwithSel(ddlBizDoc, 27, Session("DomainID"))
                If Not ddlBizDoc.Items.FindByValue(CCommon.ToLong(GetQueryStringVal("BizDocID"))) Is Nothing Then
                    ddlBizDoc.ClearSelection()
                    ddlBizDoc.Items.FindByValue(CCommon.ToLong(GetQueryStringVal("BizDocID"))).Selected = True
                End If

                BindBizDocsTemplate()

                If Not ddlBizDocTemplate.Items.FindByValue(CCommon.ToLong(GetQueryStringVal("TempID"))) Is Nothing Then
                    ddlBizDocTemplate.ClearSelection()
                    ddlBizDocTemplate.Items.FindByValue(CCommon.ToLong(GetQueryStringVal("TempID"))).Selected = True
                End If

                LoadBizDocAttachMnts()
                If CCommon.ToInteger(GetQueryStringVal("E")) = 2 Then
                    btnSort.Visible = False
                    btnAdd.Visible = False
                    dgAttachments.Columns(5).Visible = False
                End If
            End If
            btnCancel.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadBizDocAttachMnts()
        Try
            Dim objOpportunity As New OppBizDocs
            objOpportunity.BizDocId = ddlBizDoc.SelectedValue
            objOpportunity.DomainID = Session("DomainID")
            objOpportunity.BizDocTemplateID = ddlBizDocTemplate.SelectedValue
            dgAttachments.DataSource = objOpportunity.GetBizDocAttachments
            dgAttachments.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlBizDoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBizDoc.SelectedIndexChanged
        Try
            BindBizDocsTemplate()
            LoadBizDocAttachMnts()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlBizDocTemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBizDocTemplate.SelectedIndexChanged
        Try
            LoadBizDocAttachMnts()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindBizDocsTemplate()
        Try
            Dim objOppBizDoc As New OppBizDocs
            objOppBizDoc.DomainID = Session("DomainID")
            objOppBizDoc.BizDocId = ddlBizDoc.SelectedValue
            objOppBizDoc.OppType = OppType

            Dim dtBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()

            ddlBizDocTemplate.DataSource = dtBizDocTemplate
            ddlBizDocTemplate.DataTextField = "vcTemplateName"
            ddlBizDocTemplate.DataValueField = "numBizDocTempID"
            ddlBizDocTemplate.DataBind()

            ddlBizDocTemplate.Items.Insert(0, New ListItem("--Select--", 0))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            Response.Redirect("../opportunity/frmBizDocAddAttchmnts.aspx?BizDocID=" & ddlBizDoc.SelectedValue & "&OppType=" & OppType & "&TempID=" & ddlBizDocTemplate.SelectedValue)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSort_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSort.Click
        Try
            Response.Redirect("../opportunity/frmBizDocAttSort.aspx?BizDocID=" & ddlBizDoc.SelectedValue & "&OppType=" & OppType & "&TempID=" & ddlBizDocTemplate.SelectedValue)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgAttachments_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAttachments.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim objOpportunity As New OppBizDocs
                objOpportunity.BizDocAtchID = e.Item.Cells(0).Text
                objOpportunity.DelBizDocAtchmnts()
                LoadBizDocAttachMnts()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgAttachments_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAttachments.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim hplOpenLink As HyperLink
                hplOpenLink = e.Item.FindControl("hplOpenLink")
                hplOpenLink.NavigateUrl = CCommon.GetDocumentPath(Session("DomainID")) & e.Item.Cells(1).Text

                Dim hplDocument As HyperLink
                hplDocument = e.Item.FindControl("hplDocument")
                hplDocument.Text = DataBinder.Eval(e.Item.DataItem, "vcDocName")
                hplDocument.NavigateUrl = String.Format("../opportunity/frmBizDocAddAttchmnts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&AtchID={0}&BizDocID={3}&OppType={1}&TempID={2}", DataBinder.Eval(e.Item.DataItem, "numAttachmntID"), OppType, ddlBizDocTemplate.SelectedValue, ddlBizDoc.SelectedValue)

                If e.Item.Cells(2).Text = "BizDoc" Then
                    hplDocument.Enabled = False
                    'If GetQueryStringVal( "E") = 2 Then
                    '    hplOpenLink.Visible = True
                    '    Dim objHTMLToPDF As New HTMLToPDF
                    '    hplOpenLink.NavigateUrl = "../../" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName") & "/Documents/Docs/" & objHTMLToPDF.Convert("http://localhost/" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName") & "/opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=" & GetQueryStringVal( "OpID") & "&OppBizId=" & GetQueryStringVal( "OppBizId") & "&DomainID=" & GetQueryStringVal( "DomainID") & "&ConID=" & GetQueryStringVal( "ConID"))
                    'Else 
                    hplOpenLink.Visible = False
                    'End If
                    CType(e.Item.FindControl("btnDelete"), Button).Visible = False
                End If
                If CCommon.ToInteger(GetQueryStringVal("E")) = 2 Then hplDocument.Enabled = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


End Class