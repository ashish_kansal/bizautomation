﻿Imports System.Data
'Imports nsoftware.IBizFedEx
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Partial Public Class frmGetShippingRates
    Inherits BACRMPage
    Dim lngOppBizDocID As Long
    Dim lngOppID As Long
    Dim lngShippingCompany As Long
    Dim dgRate As New DataGrid
    Dim intGroupCount As Integer = 0
    Dim dtShipRates As DataTable
    Dim dtTemp As DataTable
    Dim dtRates As DataTable
    Dim ds As New DataSet()
    Dim blVisible As Boolean = True
    Dim ID As Integer = 0
    Dim objCommon As CCommon
    Dim objOppBizDocs As OppBizDocs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngOppID = GetQueryStringVal("OppId")
            lngOppBizDocID = GetQueryStringVal("OppBizDocId")
            lngShippingCompany = GetQueryStringVal("ShipCompID")
            If Not Page.IsPostBack Then

                BindBizDocItems()
                LoadShippingDetails()
                OppBizDocs.LoadServiceTypes(lngShippingCompany, ddlServiceType)
                OppBizDocs.LoadPackagingType(lngShippingCompany, ddlPackagingType, DomainId:=Session("DomainID"))
                btnGetRates.Attributes.Add("onclick", "return Save();")
                lblShippingProvider.Text = GetQueryStringVal("ShipCompName")
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


    Private Sub LoadShippingDetails()
        Try
            Dim objOpp As New MOpportunity
            objOpp.OpportunityId = lngOppID
            objOpp.Mode = 1
            Dim dtTable As DataTable
            dtTable = objOpp.GetOpportunityAddress


            Dim dtShipFrom As DataTable
            Dim objContacts As New CContacts
            objContacts.DivisionID = Session("UserDivisionID")
            objContacts.DomainID = Session("DomainID")
            dtShipFrom = objContacts.GetBillOrgorContAdd
            If objCommon Is Nothing Then objCommon = New CCommon
            objCommon.sb_FillComboFromDBwithSel(ddlFromCountry, 40, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlToCountry, 40, Session("DomainID"))
            If (dtTable.Rows.Count > 0) Then

                If Not IsDBNull(dtTable.Rows(0).Item("Country")) Then
                    If Not ddlToCountry.Items.FindByValue(dtTable.Rows(0).Item("Country")) Is Nothing Then
                        ddlToCountry.Items.FindByValue(dtTable.Rows(0).Item("Country")).Selected = True
                    End If
                End If
                If ddlToCountry.SelectedIndex > 0 Then
                    FillState(ddlToState, ddlToCountry.SelectedItem.Value, Session("DomainID"))
                End If

                If Not IsDBNull(dtTable.Rows(0).Item("State")) Then
                    If Not ddlToState.Items.FindByValue(dtTable.Rows(0).Item("State")) Is Nothing Then
                        ddlToState.Items.FindByValue(dtTable.Rows(0).Item("State")).Selected = True
                    End If
                End If
                txtToZip.Text = IIf(Not IsDBNull(dtTable.Rows(0).Item("PostCode")), dtTable.Rows(0).Item("PostCode"), "")
            End If
            If dtShipFrom.Rows.Count > 0 Then
                If Not IsDBNull(dtShipFrom.Rows(0).Item("vcShipCountry")) Then
                    If Not ddlFromCountry.Items.FindByValue(dtShipFrom.Rows(0).Item("vcShipCountry")) Is Nothing Then
                        ddlFromCountry.Items.FindByValue(dtShipFrom.Rows(0).Item("vcShipCountry")).Selected = True
                    End If
                End If
                If ddlFromCountry.SelectedIndex > 0 Then
                    FillState(ddlFromState, ddlFromCountry.SelectedItem.Value, Session("DomainID"))
                End If

                If Not IsDBNull(dtShipFrom.Rows(0).Item("vcShipState")) Then
                    If Not ddlFromState.Items.FindByValue(dtShipFrom.Rows(0).Item("vcShipState")) Is Nothing Then
                        ddlFromState.Items.FindByValue(dtShipFrom.Rows(0).Item("vcShipState")).Selected = True
                    End If
                End If
                txtFromZip.Text = IIf(Not IsDBNull(dtShipFrom.Rows(0).Item("vcShipPostCode")), dtShipFrom.Rows(0).Item("vcShipPostCode"), "")
            End If
            btnAddToShippingReport.Attributes.Add("onclick", "getSelectedValues();")
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub BindBizDocItems()
        Try
            If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
            Dim ds As DataSet
            objOppBizDocs.DomainID = Session("DomainID")
            objOppBizDocs.OppBizDocId = lngOppBizDocID
            objOppBizDocs.OppId = lngOppID
            ds = objOppBizDocs.GetBizDocInventoryItems
            dgBizDocItems.DataSource = ds.Tables(0)
            dgBizDocItems.DataBind()

            If lngShippingCompany = 91 Then
                chkUseDimensions.Checked = True
                chkUseDimensions.Enabled = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Dim fltHeight, intwidth, fltLength As Integer
    Protected Sub btnGetRates_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGetRates.Click
        Try
            CreateTempTable()
            If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
            objOppBizDocs.ShipCompany = lngShippingCompany
            objOppBizDocs.ShipFromCountry = ddlFromCountry.SelectedValue
            objOppBizDocs.ShipFromState = ddlFromState.SelectedValue
            objOppBizDocs.ShipToCountry = ddlToCountry.SelectedValue
            objOppBizDocs.ShipToState = ddlToState.SelectedValue
            'set string value
            objOppBizDocs.strShipFromCountry = ddlFromCountry.SelectedItem.Text
            objOppBizDocs.strShipFromState = ddlFromState.SelectedItem.Text
            objOppBizDocs.strShipToCountry = ddlToCountry.SelectedItem.Text
            objOppBizDocs.strShipToState = ddlToState.SelectedItem.Text
            objOppBizDocs.GetShippingAbbreviation()

            Dim ds As New DataSet
            objOppBizDocs.DomainID = Session("DomainID")
            objOppBizDocs.OppBizDocId = lngOppBizDocID
            objOppBizDocs.OppId = lngOppID
            ds = objOppBizDocs.GetBizDocInventoryItems
            'update user entered dimentions and weight pkgs in to dataset and bind
            For index As Integer = 0 To dgBizDocItems.Items.Count - 1
                For index1 As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    If ds.Tables(0).Rows(index1)("numItemCode").ToString().Equals(dgBizDocItems.Items(index).Cells(0).Text) Then
                        Dim TotalWeight As Double = Convert.ToDouble(CType(dgBizDocItems.Items(index).FindControl("txtTotalWeight"), TextBox).Text.Trim())
                        If TotalWeight <= 0 Then
                            litMessage.Text = "Package must weigh more than zero pounds."
                            Exit Sub
                        End If
                        ds.Tables(0).Rows(index1)("fltWeight") = TotalWeight
                        intwidth = CInt(CType(dgBizDocItems.Items(index).FindControl("txtWidth"), TextBox).Text.Trim())
                        fltHeight = CInt(CType(dgBizDocItems.Items(index).FindControl("txtHeight"), TextBox).Text.Trim())
                        fltLength = CInt(CType(dgBizDocItems.Items(index).FindControl("txtLength"), TextBox).Text.Trim())

                        If lngShippingCompany = 91 Then 'fedex
                            If fltLength = 0 Or fltHeight = 0 Or intwidth = 0 Then
                                litMessage.Text = "Length, width, and height must be greater than zero."
                                Exit Sub
                            End If
                        End If
                        ds.Tables(0).Rows(index1)("fltWidth") = IIf(chkUseDimensions.Checked, intwidth, 0)
                        ds.Tables(0).Rows(index1)("fltHeight") = IIf(chkUseDimensions.Checked, fltHeight, 0)
                        ds.Tables(0).Rows(index1)("fltLength") = IIf(chkUseDimensions.Checked, fltLength, 0)
                        ds.Tables(0).Rows(index1)("intNoOfBox") = 1 'Convert.ToInt32(CType(dgBizDocItems.Items(index).FindControl("txtPackages"), TextBox).Text.Trim())
                    End If
                Next
            Next
            dgItems.DataSource = ds
            dgItems.DataBind()
            If blVisible = True Then
                tblConfig.Visible = False
                tblItems.Visible = False
                btnAddToShippingReport.Visible = True
                tblShipping.Visible = True
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgItems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgItems.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                dgRate = CType(e.Item.FindControl("gridRates"), DataGrid)

                intGroupCount = intGroupCount + 1




                Dim objShipping As New Shipping
                With objShipping
                    .DomainId = Session("DomainID")
                    .WeightInLbs = Convert.ToDouble(e.Item.Cells(1).Text)
                    .NoOfPackages = 1 'Convert.ToDouble(e.Item.Cells(5).Text)
                    .UseDimentions = chkUseDimensions.Checked
                    .GroupCount = intGroupCount
                    If chkUseDimensions.Checked Then
                        .Height = Convert.ToDouble(e.Item.Cells(2).Text)
                        .Width = Convert.ToDouble(e.Item.Cells(3).Text)
                        .Length = Convert.ToDouble(e.Item.Cells(4).Text)
                    End If
                    .SenderState = objOppBizDocs.strShipFromState
                    .SenderZipCode = txtFromZip.Text
                    .SenderCountryCode = objOppBizDocs.strShipFromCountry
                    .RecepientState = objOppBizDocs.strShipToState
                    .RecepientZipCode = txtToZip.Text
                    .RecepientCountryCode = objOppBizDocs.strShipToCountry

                    .ServiceType = ddlServiceType.SelectedValue
                    '.DropoffType = ddlDropoffType.SelectedValue
                    .PackagingType = ddlPackagingType.SelectedValue
                    .ItemCode = e.Item.Cells(0).Text.Trim()
                    .OppBizDocItemID = CCommon.ToLong(e.Item.Cells(5).Text)
                    .ID = ID
                    .Provider = lngShippingCompany
                    dtShipRates = .GetRates()
                    If .ErrorMsg <> "" Then
                        litMessage.Text = .ErrorMsg
                        blVisible = False
                        Exit Sub
                    End If
                    ID = .ID
                End With

                dgRate.DataSource = dtShipRates
                dgRate.DataBind()
                For Each dr As DataRow In dtShipRates.Rows
                    ds.Tables(0).ImportRow(dr)
                Next
                Session("dtShipRates") = ds.Tables(0)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub btnAddToShippingReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddToShippingReport.Click
        Try
            Dim dt As DataTable = CType(Session("dtShipRates"), DataTable)
            Dim strIDs As String() = hdnSelectedIDs.Value.Split(",")
            Dim intID As Integer
            Dim objOppBizDocs As New OppBizDocs
            Dim ds As New DataSet
            Dim dtFields As New DataTable

            dtFields.Columns.Add("numItemCode")
            dtFields.Columns.Add("tintServiceType")
            dtFields.Columns.Add("dtDeliveryDate")
            dtFields.Columns.Add("monShippingRate")
            dtFields.Columns.Add("fltTotalWeight")
            dtFields.Columns.Add("intNoOfBox")
            dtFields.Columns.Add("fltHeight")
            dtFields.Columns.Add("fltWidth")
            dtFields.Columns.Add("fltLength")
            dtFields.Columns.Add("numOppBizDocItemID")
            dtFields.TableName = "Items"



            ''While getting rates By default Put all items into single box an create box
            'Dim dtBox As New DataTable("Box")
            'Dim dtItem As New DataTable("Item")
            'dtBox.Columns.Add("vcBoxName")
            'dtBox.Columns.Add("fltTotalWeight")
            'dtBox.Columns.Add("fltHeight")
            'dtBox.Columns.Add("fltWidth")
            'dtBox.Columns.Add("fltLength")

            'dtItem.Columns.Add("vcBoxName")
            'dtItem.Columns.Add("numOppBizDocItemID")

            'Dim dr As DataRow
            'Dim dtAvailableItems As DataTable
            'objOppBizDocs.DomainID = Session("DomainID")
            'objOppBizDocs.OppBizDocId = lngOppBizDocID
            'objOppBizDocs.OppId = lngOppID
            'ds = objOppBizDocs.GetBizDocInventoryItems
            'dtAvailableItems = ds.Tables(0)


            'For i As Integer = 0 To dtAvailableItems.Rows.Count - 1
            '    dr = dtBox.NewRow
            '    dr("vcBoxName") = "Box" & (i + 1).ToString
            '    dr("fltTotalWeight") = 0
            '    dr("fltHeight") = 0
            '    dr("fltWidth") = 0
            '    dr("fltLength") = 0
            '    dtBox.Rows.Add(dr)
            'Next

            'For i As Integer = 0 To dtAvailableItems.Rows.Count - 1
            '    dr = dtItem.NewRow
            '    dr("vcBoxName") = "Box" & (i + 1).ToString
            '    dr("numOppBizDocItemID") = dtAvailableItems.Rows(i)("numOppBizDocItemID")
            '    dtItem.Rows.Add(dr)
            'Next


            'ds.Tables.Add(dtBox)
            'ds.Tables.Add(dtItem)

            'With objOppBizDocs
            '    .ShippingReportId = 0 ' will be supplied from stored procedure
            '    .strText = ds.GetXml()
            '    .UserCntID = Session("UserContactID")
            '    .ManageShippingBox()
            'End With

            'Create shipping report
            For index As Integer = 0 To strIDs.Length - 1
                If strIDs(index).Length > 0 Then
                    intID = CInt(strIDs(index))
                    Dim dtRow As DataRow = dtFields.NewRow
                    dtRow("numItemCode") = dt.Rows(intID)("numItemCode").ToString()
                    dtRow("tintServiceType") = dt.Rows(intID)("tintServiceType").ToString()
                    Try
                        dtRow("dtDeliveryDate") = Convert.ToDateTime(dt.Rows(intID)("Date").ToString())
                    Catch ex As Exception
                        dtRow("dtDeliveryDate") = Convert.ToDateTime(Date.Now.ToShortDateString())
                    End Try
                    dtRow("monShippingRate") = dt.Rows(intID)("Rate").ToString()
                    dtRow("fltTotalWeight") = dt.Rows(intID)("intTotalWeight").ToString()
                    dtRow("intNoOfBox") = dt.Rows(intID)("intNoOfBox").ToString()
                    dtRow("fltHeight") = dt.Rows(intID)("fltHeight").ToString()
                    dtRow("fltWidth") = dt.Rows(intID)("fltWidth").ToString()
                    dtRow("fltLength") = dt.Rows(intID)("fltLength").ToString()
                    dtRow("numOppBizDocItemID") = dt.Rows(intID)("numOppBizDocItemID").ToString()

                    dtFields.Rows.Add(dtRow)
                End If
            Next
            With objOppBizDocs
                .DomainID = Session("DomainID")
                .OppBizDocId = lngOppBizDocID
                .ShipCompany = lngShippingCompany
                '.Value1 = ddlCarrierCode.SelectedValue
                .Value2 = ddlServiceType.SelectedValue
                '.Value3 = ddlDropoffType.SelectedValue
                '.Value4 = ddlPackagingType.SelectedValue

                .FromState = ddlFromState.SelectedValue
                .FromZip = txtFromZip.Text.Trim()
                .FromCountry = ddlFromCountry.SelectedValue
                .ToState = ddlToState.SelectedValue
                .ToZip = txtToZip.Text.Trim()
                .ToCountry = ddlToCountry.SelectedValue

                .UserCntID = Session("UserContactID")
                ds.Tables.Add(dtFields.Copy)
                .strText = ds.GetXml()
                .ShippingReportId = .ManageShippingReport
                If .ShippingReportId > 0 Then
                    Response.Redirect("frmShippingReport.aspx?&OppId=" & lngOppID & "&OppBizDocId=" & lngOppBizDocID & "&ShipReportId=" & .ShippingReportId)
                End If
            End With
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub CreateTempTable()
        Try
            'Create Blank Schema of table to merge all Shipping rates
            dtTemp = New DataTable()
            dtTemp.Columns.Add("ID")
            dtTemp.Columns.Add("numItemCode")
            dtTemp.Columns.Add("GroupName")
            dtTemp.Columns.Add("Date")
            dtTemp.Columns.Add("ServiceType")
            dtTemp.Columns.Add("tintServiceType")
            dtTemp.Columns.Add("Rate")
            dtTemp.Columns.Add("intTotalWeight")
            dtTemp.Columns.Add("intNoOfBox")
            dtTemp.Columns.Add("fltHeight")
            dtTemp.Columns.Add("fltWidth")
            dtTemp.Columns.Add("fltLength")
            dtTemp.Columns.Add("numOppBizDocItemID")
            ds.Tables.Add(dtTemp.Copy)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub ddlToCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlToCountry.SelectedIndexChanged
        Try
            If ddlToCountry.SelectedIndex > 0 Then
                FillState(ddlToState, ddlToCountry.SelectedItem.Value, Session("DomainID"))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlFromCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFromCountry.SelectedIndexChanged
        Try
            If ddlFromCountry.SelectedIndex > 0 Then
                FillState(ddlFromState, ddlFromCountry.SelectedItem.Value, Session("DomainID"))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class