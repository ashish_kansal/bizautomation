﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="orderPOS.ascx.vb" Inherits=".orderPOS"
    ClientIDMode="Static" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<script language="javascript" type="text/javascript">
    function OpenInTransit() {
        if ($find('radCmbItem').get_value() == '') {
            alert("Select Item");
            return false;
        }

        window.open('../opportunity/frmOrderTransit.aspx?ItemCode=' + $find('radCmbItem').get_value(), '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=yes,resizable=yes')
        return false;
    }

    function openVendorCostTable(a) {
        if ($find('radCmbItem').get_value() == '') {
            alert("Select Item");
            return false;
        }

        window.open('../opportunity/frmVendorCostTable.aspx?ItemCode=' + $find('radCmbItem').get_value() + '&OppType=' + a + '&Vendor=' + document.getElementById('hdnVendorId').value, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=yes,resizable=yes')
        return false;
    }

    function OpenRelatedItems(a, b, c, d) {
        if ($find('radCmbItem').get_value() == '') {
            alert("Select Item");
            return false;
        }

        if (d == 0) {
            alert("Select Company");
            return false;
        }

        window.open('../opportunity/frmOpenRelatedItems.aspx?ItemCode=' + $find('radCmbItem').get_value() + '&OppType=' + a + '&PageType=' + b + '&opid=' + c + '&DivId=' + d, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=yes,resizable=yes')
        return false;
    }

    function BindOrderGrid() {

        document.getElementById("btnBindOrderGrid").click()
        return false;
    }

    function CheckCharacter(e) {
        var k;
        document.all ? k = e.keyCode : k = e.which;
        //document.Form1.txtdesc.value = document.Form1.txtdesc.value + '-' + k;
        if (e.ctrlKey) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            else
                e.returnValue = false;
            return false;
        }

        if (k == 13) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            else
                e.returnValue = false;
            return false;
        }
    }

    function ChangedUPCSKU(TB, e) {
        var k;
        document.all ? k = e.keyCode : k = e.which;

        if (e.ctrlKey) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            else
                e.returnValue = false;
            return false;
        }

        if (k == 13) {
            if (e.preventDefault) {
                e.preventDefault();
            }
            else
                e.returnValue = false;

            document.getElementById('btnUPCSKU').click();

            return false;
        }
        return false;
    }
</script>
<asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
    EnableViewState="true">
    <ContentTemplate>
        <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td valign="top" align="center">
                    <table border="0">
                        <tr>
                            <td>
                                UPC / SKU
                            </td>
                            <td class="normal1" valign="middle" align="left" colspan="4">
                                <asp:TextBox ID="txtUPCSKU" runat="server" CssClass="signup" MaxLength="25" Width="150"
                                    onkeypress="javascript:ChangedUPCSKU(this,event)"></asp:TextBox>
                                <asp:Button ID="btnUPCSKU" runat="server" CssClass="Button_POS" Text="Add " Style="display: none">
                                </asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Select item
                            </td>
                            <td class="text_bold" colspan="4">
                                <asp:CheckBox ID="chkSearchOrderCustomerHistory" Text="Search within customer history"
                                    runat="server" CssClass="signup" AutoPostBack="true" Font-Bold="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <u>I</u>tem
                            </td>
                            <td class="normal1" valign="middle" align="left" colspan="4">
                                <telerik:RadComboBox runat="server" ID="radCmbItem" AccessKey="I" Skin="Vista" Height="200px"
                                    AutoPostBack="true" Width="300px" DropDownWidth="515px" ShowMoreResultsBox="true"
                                    EnableVirtualScrolling="false" ChangeTextOnKeyBoardNavigation="false" OnClientItemsRequesting="OnClientItemsRequesting"
                                    OnClientItemsRequested="OnClientItemsRequested" OnClientDropDownOpening="OnClientDropDownOpening"
                                    EnableLoadOnDemand="true" ClientIDMode="Static" onkeypress="CheckCharacter(event)">
                                    <WebServiceSettings Path="../common/Common.asmx" Method="GetItemNames" />
                                </telerik:RadComboBox>
                                &nbsp;<a href="#" onclick="return ClearRadComboBox();" class="hyperlink">Clear</a>
                            </td>
                        </tr>
                        <tr runat="server" id="trWarehouse">
                            <td class="normal1" valign="top" align="left" colspan="4">
                                <span id="tdWareHouseLabel" runat="server"><u>W</u>arehouse<font color="red">*</font></span>
                                <span id="tdWareHouse">
                                    <telerik:RadComboBox ID="radWareHouse" runat="server" Width="500" DropDownWidth="550px"
                                        AccessKey="W" AutoPostBack="true" ClientIDMode="Static">
                                        <HeaderTemplate>
                                            <table style="width: 550px; text-align: left">
                                                <tr>
                                                    <td>
                                                    </td>
                                                    <td class="normal1" style="width: 125px;">
                                                        Warehouse
                                                    </td>
                                                    <td class="normal1" style="width: 200px;">
                                                        Attributes
                                                    </td>
                                                    <td class="normal1" style="width: 50px;">
                                                        On Hand
                                                    </td>
                                                    <td class="normal1" style="width: 50px;">
                                                        On Order
                                                    </td>
                                                    <td class="normal1" style="width: 75px;">
                                                        On Allocation
                                                    </td>
                                                    <td class="normal1" style="width: 50px;">
                                                        BackOrder
                                                    </td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <table style="width: 550px; text-align: left">
                                                <tr>
                                                    <td style="display: none">
                                                        <%#DataBinder.Eval(Container.DataItem, "numWareHouseItemId")%>
                                                    </td>
                                                    <td style="width: 125px;">
                                                        <%#DataBinder.Eval(Container.DataItem, "vcWareHouse")%>
                                                    </td>
                                                    <td style="width: 200px;">
                                                        <%#DataBinder.Eval(Container.DataItem, "Attr")%>
                                                    </td>
                                                    <td style="width: 50px;">
                                                        <%#DataBinder.Eval(Container.DataItem, "numOnHand")%>
                                                        <%# DataBinder.Eval(Container.DataItem, "vcUnitName")%>
                                                    </td>
                                                    <td style="width: 50px;">
                                                        <%#DataBinder.Eval(Container.DataItem, "numOnOrder")%>
                                                    </td>
                                                    <td style="width: 75px;">
                                                        <%#DataBinder.Eval(Container.DataItem, "numAllocation")%>
                                                    </td>
                                                    <td style="width: 50px;">
                                                        <%#DataBinder.Eval(Container.DataItem, "numBackOrder")%>
                                                        <asp:Label runat="server" ID="lblWareHouseID" Text='<%#DataBinder.Eval(Container.DataItem, "numWareHouseID")%>'
                                                            Style="display: none"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </ItemTemplate>
                                    </telerik:RadComboBox>
                                </span>
                            </td>
                            <td class="normal1" valign="top" align="left">
                                <asp:CheckBox ID="chkCreatePurchaseOrder" Text="Create Purchase Order" runat="server"
                                    CssClass="signup" Visible="false" />
                                <asp:CheckBox ID="chkDropShip" Text="Drop Ship" runat="server" CssClass="signup" />
                                <asp:Label ID="lblBudget" runat="server" Text="The budget balance for the item group this item belongs to is: "
                                    Visible="False"></asp:Label>
                                &nbsp;&nbsp;
                                <asp:Label ID="lblBudgetMonth" runat="server" Visible="False"></asp:Label>&nbsp;&nbsp;
                                <asp:Label ID="lblBudgetYear" runat="server" Visible="False"></asp:Label>
                                <asp:Button ID="btnConfigureAttribute" runat="server" CssClass="keypad_digit" Text="Configure Attribute"
                                    Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <asp:Panel ID="pnlDiscount" runat="server">
                                <td>
                                    Discount
                                </td>
                                <td class="normal1" valign="top" align="left">
                                    <asp:TextBox ID="txtItemDiscount" runat="server" CssClass="signup" Width="50px"></asp:TextBox>
                                    <asp:RadioButton ID="radPer" runat="server" GroupName="rad1" Checked="true" Text="%" />
                                    <asp:RadioButton ID="radAmt" runat="server" GroupName="rad1" Text="Amount Entered" />
                                </td>
                            </asp:Panel>
                        </tr>
                        <tr>
                            <td class="normal1" valign="top" align="left">
                                UoM
                            </td>
                            <td class="normal1" valign="top" align="left">
                                <asp:DropDownList ID="ddUOM" runat="server" CssClass="signup" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td class="normal1" valign="top" align="left">
                                Description
                            </td>
                            <td class="normal1" valign="top" align="left">
                                <asp:TextBox ID="txtdesc" runat="server" TextMode="MultiLine" Width="500"></asp:TextBox>
                            </td>
                            <td class="normal1" valign="bottom" align="left">
                                <asp:Button AccessKey="A" ID="btnAdd" runat="server" CssClass="Button_POS" Text="Add">
                                </asp:Button>
                            </td>
                        </tr>
                    </table>
                    <table style="visibility: hidden; display: none;">
                        <tr>
                            <td class="normal1" align="right">
                                <span id="divTransit" runat="server" visible="false" class="normal1">&nbsp;Orders in
                                    transit:
                                    <asp:HyperLink ID='hplInTransit' runat="server" CssClass="hyperlink" onclick='return OpenInTransit();'></asp:HyperLink></span>
                                Type
                                <asp:DropDownList ID="ddltype" runat="server" CssClass="signup" Width="200" Enabled="False">
                                    <asp:ListItem Value="P">Inventory Item</asp:ListItem>
                                    <asp:ListItem Value="N">Non-Inventory Item</asp:ListItem>
                                    <asp:ListItem Value="S">Service</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr runat="server" id="trWarehouseTransferTo" visible="false">
                            <td class="normal1" align="right">
                                Transfer To
                            </td>
                            <td class="normal1" colspan="2">
                                <telerik:RadComboBox ID="radWarehouseTo" runat="server" Width="500" DropDownWidth="550px"
                                    AccessKey="T" AutoPostBack="true" ClientIDMode="Static">
                                    <HeaderTemplate>
                                        <table style="width: 550px; text-align: left">
                                            <tr>
                                                <td>
                                                </td>
                                                <td class="normal1" style="width: 125px;">
                                                    Warehouse
                                                </td>
                                                <td class="normal1" style="width: 200px;">
                                                    Attributes
                                                </td>
                                                <td class="normal1" style="width: 50px;">
                                                    On Hand
                                                </td>
                                                <td class="normal1" style="width: 50px;">
                                                    On Order
                                                </td>
                                                <td class="normal1" style="width: 75px;">
                                                    On Allocation
                                                </td>
                                                <td class="normal1" style="width: 50px;">
                                                    BackOrder
                                                </td>
                                            </tr>
                                        </table>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <table style="width: 550px; text-align: left">
                                            <tr>
                                                <td style="display: none">
                                                    <%#DataBinder.Eval(Container.DataItem, "numWareHouseItemId")%>
                                                </td>
                                                <td style="width: 125px;">
                                                    <%#DataBinder.Eval(Container.DataItem, "vcWareHouse")%>
                                                </td>
                                                <td style="width: 200px;">
                                                    <%#DataBinder.Eval(Container.DataItem, "Attr")%>
                                                </td>
                                                <td style="width: 50px;">
                                                    <%#DataBinder.Eval(Container.DataItem, "numOnHand")%>
                                                    <%# DataBinder.Eval(Container.DataItem, "vcUnitName")%>
                                                </td>
                                                <td style="width: 50px;">
                                                    <%#DataBinder.Eval(Container.DataItem, "numOnOrder")%>
                                                </td>
                                                <td style="width: 75px;">
                                                    <%#DataBinder.Eval(Container.DataItem, "numAllocation")%>
                                                </td>
                                                <td style="width: 50px;">
                                                    <%#DataBinder.Eval(Container.DataItem, "numBackOrder")%>
                                                    <asp:Label runat="server" ID="lblWareHouseID" Text='<%#DataBinder.Eval(Container.DataItem, "numWareHouseID")%>'
                                                        Style="display: none"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </telerik:RadComboBox>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr id="trVendorWareHouse" runat="server" visible="false">
                            <td class="normal1" align="right">
                                Vendor's Warehouse
                            </td>
                            <td class="normal1" colspan="3">
                                <asp:DropDownList ID="ddlVendorWarehouse" CssClass="signup" runat="server" AutoPostBack="True">
                                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr id="trVendorShipmentMethod" runat="server" visible="false">
                            <td class="normal1" align="right">
                                Method of Shipment
                            </td>
                            <td class="normal1">
                                <asp:DropDownList ID="ddlVendorShipmentMethod" CssClass="signup" runat="server" AutoPostBack="True">
                                    <asp:ListItem Value="0~0">--Select One--</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td class="normal1" align="right">
                                Expected delivery
                            </td>
                            <td class="normal1">
                                <asp:Label ID="lblExpectedDelivery" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:CheckBox ID="chkWorkOrder" runat="server" Text="Create Work Order" Visible="false"
                                    CssClass="signup" onchange="javascript:WorkOrderChange();" AutoPostBack="true" />&nbsp;
                                <asp:HyperLink ID='hplRelatedItems' runat="server" Text="Related Items" CssClass="hyperlink"
                                    onclick='return OpenRelatedItems();' Visible="false"></asp:HyperLink>
                            </td>
                        </tr>
                        <tr id="trWorkOrder" runat="server" visible="false">
                            <td class="normal1" align="right">
                                Instruction
                            </td>
                            <td class="normal1" nowrap colspan="4">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtInstruction" runat="server" TextMode="MultiLine" CssClass="signup"
                                                MaxLength="1000" Width="300"></asp:TextBox>
                                        </td>
                                        <td class="normal1" valign="top">
                                            Completion date
                                        </td>
                                        <td class="normal1" valign="top">
                                            <BizCalendar:Calendar ID="calCompliationDate" runat="server" />
                                        </td>
                                        <td class="normal1" valign="top">
                                            Assign To
                                        </td>
                                        <td class="normal1" valign="top">
                                            <asp:DropDownList ID="ddlWOAssignTo" Width="200" runat="server" CssClass="signup">
                                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" align="right">
                                <asp:HyperLink ID="hplUnits" runat="server" CssClass="hyperlink">
                        Units</asp:HyperLink><font color="red">*</font>
                            </td>
                            <td class="normal1" nowrap colspan="4">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtunits" runat="server" CssClass="signup" Width="50px" Text="1"
                                                AutoPostBack="true"></asp:TextBox>&nbsp; &nbsp;
                                            <asp:HyperLink ID="hplPrice" runat="server" CssClass="hyperlink">Unit Price</asp:HyperLink><font
                                                color="red">*</font>
                                            <asp:TextBox ID="txtprice" runat="server" CssClass="signup" Width="90px" MaxLength="11"
                                                AutoPostBack="true"></asp:TextBox>&nbsp;&nbsp;
                                        </td>
                                        <td class="normal1">
                                        </td>
                                        <td class="normal1">
                                            &nbsp;&nbsp;&nbsp;<asp:Button ID="btnEditCancel" runat="server" Visible="false" CssClass="button"
                                                Text="Cancel"></asp:Button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:Button ID="btnOptionsNAccessories" runat="server" CssClass="button" Text="Customize"
                                                Visible="false" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="normal1" id="tdSalesProfit" runat="server" visible="false">
                                            <asp:HyperLink ID="hplUnitCost" runat="server" CssClass="hyperlink">Unit-Cost</asp:HyperLink>&nbsp;
                                            <asp:Label ID="lblPUnitCost" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;<strong>Profit
                                                - Unit/Total </strong>
                                            <asp:Label ID="lblProfit" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="normal4" align="center" width="98%">
                    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center">
                    <asp:DataGrid ID="dgItems" runat="server" CssClass="NewGrid" ShowFooter="true" AutoGenerateColumns="false"
                        Width="90%" ClientIDMode="AutoID">
                        <%--   <AlternatingItemStyle CssClass="ais" />--%>
                        <%--  <ItemStyle CssClass="ais1" />--%>
                        <HeaderStyle CssClass="hs" />
                        <FooterStyle CssClass="footer" />
                        <Columns>
                            <asp:TemplateColumn HeaderText="No">
                                <ItemTemplate>
                                    <asp:Label ID="lblOppItemCode" runat="server" Text='<%#Eval("numoppitemtCode")%>'></asp:Label>
                                    <asp:Label ID="lblID" runat="server" Text='<%# "!!" + Eval("numItemCode").ToString() + "," + Eval("numWarehouseItmsID").ToString() + "," + Eval("DropShip").ToString() %>'
                                        Style="display: none"></asp:Label>
                                    <asp:Label ID="lblcharItemType" runat="server" Text='<%#Eval("charItemType")%>' Style="display: none"></asp:Label>
                                    <asp:Label ID="lblnumProjectID" runat="server" Text='<%#Eval("numProjectID")%>' Style="display: none"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <%--<asp:BoundColumn DataField="numoppitemtCode" Visible="true" HeaderText="No"></asp:BoundColumn>--%>
                            <asp:BoundColumn DataField="numWarehouseItmsID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numWareHouseID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numItemCode" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="bitTaxable0" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="vcItemName" HeaderText="Item"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Warehouse" HeaderText="Warehouse"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ItemType" HeaderText="Type" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="DropShip" HeaderText="Drop Ship" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Attributes" HeaderText="Attributes" Visible="false">
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="vcUOMName" HeaderText="UOM"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Units">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtUnits" CssClass="signup" Width="50" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.numUnitHour") %>'>
                                    </asp:TextBox>
                                    <asp:TextBox ID="txtUOMConversionFactor" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.UOMConversionFactor") %>'>></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Unit Price">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtUnitPrice" CssClass="signup" Width="50" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.monPrice") %>'>
                                    </asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Discount">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtDiscountType" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.bitDiscountType") %>'
                                        Style="display: none"></asp:TextBox>
                                    <asp:TextBox ID="txtTotalDiscount" CssClass="signup" Width="50" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.fltDiscount") %>'></asp:TextBox>
                                    <asp:Label ID="lblDiscount" runat="server"></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    Total :
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Total">
                                <ItemTemplate>
                                    <asp:Label ID="lblTotal" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.monTotAmount") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblFTotal" runat="server" Text=""></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Sales Tax">
                                <ItemTemplate>
                                    <asp:Label ID="lblTaxAmt0" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Tax0") %>'></asp:Label>
                                    <asp:Label ID="lblTaxable0" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.bitTaxable0") %>'></asp:Label>
                                </ItemTemplate>
                                <FooterTemplate>
                                    <asp:Label ID="lblFTaxAmt0" runat="server" Text=""></asp:Label>
                                </FooterTemplate>
                            </asp:TemplateColumn>
                            <%-- <asp:TemplateColumn>
            <ItemTemplate>
                <asp:LinkButton ID="lnkEdit" CommandName="Edit" CssClass="hyperlink" runat="server"
                    Text="Edit"></asp:LinkButton>
            </ItemTemplate>
        </asp:TemplateColumn>--%>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkDelete" CommandName="Delete" CssClass="hyperlink" runat="server"
                                        Text="Delete"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <table border="0" align="right" cellpadding="0" cellspacing="0">
                        <tr>
                            <td class="normal1" align="right">
                                <asp:Label ID="lblCurrencyTotal" runat="server" Text=""></asp:Label>&nbsp;
                            </td>
                            <td align="left" colspan="2">
                                <asp:Label ID="lblTotal" CssClass="text" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top">
                                <table id="tblkeypad">
                                    <tr>
                                        <td>
                                            <input type="button" id="btnCal_1" class="keypad_digit" value="1" />
                                        </td>
                                        <td>
                                            <input type="button" id="btnCal_2" class="keypad_digit" value="2" />
                                        </td>
                                        <td>
                                            <input type="button" id="btnCal_3" class="keypad_digit" value="3" />
                                        </td>
                                        <td>
                                            <input type="button" id="btnCal_Qty" class="keypad_ModeSelect" value="Units" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="button" id="btnCal_4" class="keypad_digit" value="4" />
                                        </td>
                                        <td>
                                            <input type="button" id="btnCal_5" class="keypad_digit" value="5" />
                                        </td>
                                        <td>
                                            <input type="button" id="btnCal_6" class="keypad_digit" value="6" />
                                        </td>
                                        <td>
                                            <input type="button" id="btnCal_Price" class="keypad_Mode" value="Price" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="button" id="btnCal_7" class="keypad_digit" value="7" />
                                        </td>
                                        <td>
                                            <input type="button" id="btnCal_8" class="keypad_digit" value="8" />
                                        </td>
                                        <td>
                                            <input type="button" id="btnCal_9" class="keypad_digit" value="9" />
                                        </td>
                                        <td>
                                            <input type="button" id="btnCal_Disc" class="keypad_Mode" value="Disc " />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <input type="button" id="btnCal_0" class="keypad_digit" value="0" />
                                        </td>
                                        <td>
                                            <input type="button" id="btnCal_Dot" class="keypad_digit" value="." />
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            <input type="button" id="btnCal_Del" class="keypad_clear" value="Clear" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td valign="top">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSave" runat="server" CssClass="Button_POS" Text="Save & Create Invoice"
                                                Style="width: 200px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSaveOpenOrder" runat="server" CssClass="Button_POS" Text="Save & Open Order Details"
                                                Style="width: 200px;" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnSaveNew" runat="server" CssClass="Button_POS" Text="Save & New"
                                                Style="width: 200px;" />
                                        </td>
                                    </tr>
                                </table>
                                <br />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:TextBox ID="txtSerialize" runat="server" Style="display: none"></asp:TextBox>
        <asp:TextBox ID="txtDivID" runat="server" Style="display: none"></asp:TextBox>
        <asp:TextBox ID="txtHidValue" runat="server" Style="display: none"></asp:TextBox>
        <asp:TextBox ID="txtTax" runat="server" Style="display: none"></asp:TextBox>
        <input id="Taxable" runat="server" type="hidden" />
        <input id="TaxItemsId" runat="server" type="hidden" />
        <input id="hdKit" runat="server" type="hidden" />
        <asp:HiddenField ID="hdnBillAddressID" runat="server" />
        <asp:HiddenField ID="hdnShipAddressID" runat="server" />
        <asp:HiddenField ID="hdnOrderCreateDate" runat="server" />
        <asp:HiddenField ID="hdnVendorId" runat="server" />
        <asp:HiddenField ID="hdnUnitCost" runat="server" />
        <asp:HiddenField ID="hdnSearchOrderCustomerHistory" runat="server" Value="0" />
        <asp:TextBox ID="txtModelID" runat="server" Style="display: none">
        </asp:TextBox>
        <asp:HiddenField ID="hdvUOMConversionFactor" runat="server" Value="1" />
        <asp:HiddenField ID="hdvPurchaseUOMConversionFactor" runat="server" Value="1" />
        <asp:HiddenField ID="hdvSalesUOMConversionFactor" runat="server" Value="1" />
        <asp:HiddenField ID="hdnPOS" runat="server" Value="1" />
        <asp:TextBox ID="txtHidEditOppItem" runat="server" Style="display: none"></asp:TextBox>
        <asp:Button ID="btnBindOrderGrid" runat="server" CssClass="button" Text="BindOrderGrid"
            Style="display: none" />
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="radCmbItem" />
        <asp:PostBackTrigger ControlID="btnSave" />
        <asp:PostBackTrigger ControlID="btnSaveNew" />
        <asp:PostBackTrigger ControlID="btnSaveOpenOrder" />
    </Triggers>
</asp:UpdatePanel>
