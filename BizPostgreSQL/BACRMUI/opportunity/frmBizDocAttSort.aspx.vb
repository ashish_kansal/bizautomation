Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities

Partial Public Class frmBizDocAttSort
    Inherits BACRMPage
    Dim OppType As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            OppType = GetQueryStringVal( "OppType")

            'btnMoveDown.Attributes.Add("OnClick", "return MoveDown(document.form1.lstAddfld)")
            'btnMoveup.Attributes.Add("OnClick", "return MoveUp(document.form1.lstAddfld)")
            btnSave.Attributes.Add("onclick", "return Save()")
            If Not IsPostBack Then
                
                
                objCommon.sb_FillComboFromDBwithSel(ddlBizDoc, 27, Session("DomainID"))
                If Not ddlBizDoc.Items.FindByValue(GetQueryStringVal( "BizDocID")) Is Nothing Then
                    ddlBizDoc.Items.FindByValue(GetQueryStringVal( "BizDocID")).Selected = True
                End If

                BindBizDocsTemplate()

                If Not ddlBizDocTemplate.Items.FindByValue(GetQueryStringVal( "TempID")) Is Nothing Then
                    ddlBizDocTemplate.ClearSelection()
                    ddlBizDocTemplate.Items.FindByValue(GetQueryStringVal( "TempID")).Selected = True
                End If

                SortDocuments()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub SortDocuments()
        Try
            Dim objBizDocs As New OppBizDocs
            objBizDocs.BizDocId = ddlBizDoc.SelectedValue
            objBizDocs.DomainID = Session("DomainID")
            objBizDocs.BizDocTemplateID = ddlBizDocTemplate.SelectedValue
            lstAddfld.DataSource = objBizDocs.GetBizDocAttachments
            lstAddfld.DataTextField = "vcDocName"
            lstAddfld.DataValueField = "numAttachmntID"
            lstAddfld.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlBizDoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBizDoc.SelectedIndexChanged
        Try
            BindBizDocsTemplate()
            SortDocuments()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlBizDocTemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBizDocTemplate.SelectedIndexChanged
        Try
            SortDocuments()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindBizDocsTemplate()
        Try
            Dim objOppBizDoc As New OppBizDocs
            objOppBizDoc.DomainID = Session("DomainID")
            objOppBizDoc.BizDocId = ddlBizDoc.SelectedValue
            objOppBizDoc.OppType = OppType

            Dim dtBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()

            ddlBizDocTemplate.DataSource = dtBizDocTemplate
            ddlBizDocTemplate.DataTextField = "vcTemplateName"
            ddlBizDocTemplate.DataValueField = "numBizDocTempID"
            ddlBizDocTemplate.DataBind()

            ddlBizDocTemplate.Items.Insert(0, New ListItem("--Select--", 0))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objBizDocs As New OppBizDocs
            Dim dtSort As New DataTable
            Dim i As Integer
            Dim dr As DataRow
            dtSort.Columns.Add("numBizAtchID")
            dtSort.Columns.Add("numOrder")
            Dim strVales As String()
            strVales = txthidden.Value.Split(",")
            Dim ds As New DataSet
            For i = 0 To strVales.Length - 2
                dr = dtSort.NewRow
                dr("numBizAtchID") = strVales(i)
                dr("numOrder") = i + 1
                dtSort.Rows.Add(dr)
            Next
            dtSort.TableName = "Table"
            ds.Tables.Add(dtSort.Copy)
            objBizDocs.vcOrder = ds.GetXml
            ds.Tables.Remove(ds.Tables(0))
            objBizDocs.BizDocId = ddlBizDoc.SelectedValue
            objBizDocs.DomainID = Session("DomainID")
            objBizDocs.BizDocTemplateID = ddlBizDocTemplate.SelectedValue
            objBizDocs.SortBizDocAttachments()
            SortDocuments()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect("../opportunity/frmBizDocAttachments.aspx?BizDocID=" & ddlBizDoc.SelectedValue & "&OppType=" & OppType & "&TempID=" & ddlBizDocTemplate.SelectedValue)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

 
End Class