<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPaymentDetails.aspx.vb"
    Inherits=".frmPaymentDetails" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content2" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Payment Details</title>
    <script language="JavaScript" type="text/javascript" src="../javascript/date-picker.js"></script>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script language="javascript" type="text/javascript">
        function OpenAmtPaid(a, b, c, d) {
            // alert('SP');
            //window.open('../opportunity/frmAmtPaid.aspx?OppBizDocID='+a+'&Amt='+b+'&OppID='+c,'','toolbar=no,titlebar=no,top=300,width=500,height=150,scrollbars=yes,resizable=yes');
            //window.open('../opportunity/frmAmtPaid.aspx?BizDocsPaymentDetId='+a+'&OppBizDocID='+b+'&OppId='+c,'','toolbar=no,titlebar=no,top=300,width=500,height=150,scrollbars=yes,resizable=yes');
            //d===BalanceDue
            RPPopup = window.open('../opportunity/frmAmtPaid.aspx?d=' + a + '&a=' + b + '&b=' + c + '&c=' + d, 'ReceivePayment', 'toolbar=no,titlebar=no,top=300,width=600,height=270,scrollbars=no,resizable=no');
            console.log(RPPopup.name);
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function OpenOpp(a, b) {
            var str;
            str = "../opportunity/frmOpportunities.aspx?frm=OpenBizDoc&OpID=" + a + "&OppType=" + b;

            document.location.href = str;
        }
        function OpenBizInvoice(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }

        function DeleteMessage() {
            alert("You cannot delete Payment Details for which amount is deposited !");
            return false;
        }
    </script>
    <style>
         .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
        .hs {
        border: 1px solid #e4e4e4;
        border-bottom-width: 2px;
            background-color: #ebebeb;
            padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    text-align: center;
    font-weight:bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="view-cases">
        </div>
        <div class="right-input">
            <table align="right" width="100%">
                <tr>
                    <td>
                        
                    </td>
                    <td>
                        
                    </td>
                    <td class="leftBorder">
                       
                    </td>
                    <td valign="bottom">
                        
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="col-md-12" style="z-index:19">
        <div class="pull-left"  id="tblDate" runat="server">
            <div class="form-inline">
                <div class="form-group">
                    <label>From</label>
                    <BizCalendar:Calendar ID="calFrom" runat="server" ClientIDMode="AutoID" />
                </div>
                <div class="form-group">
                    <label>To</label>
                    <BizCalendar:Calendar ID="calTo" runat="server" ClientIDMode="AutoID" />
                </div>
                <div class="form-group">
                    <asp:LinkButton ID="btnGo" runat="server" CssClass="button btn btn-primary" OnClientClick="return Go();"><i class="fa fa-share"></i>&nbsp;&nbsp;Go</asp:LinkButton>
                    &nbsp;
                    <asp:LinkButton ID="btnClose" runat="server" CssClass="button btn btn-danger" Text="">Close</asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="pull-right">
            <div class="form-inline">
                <div class="form-group">
                    <label>
                            Customer/Vendor</label>
                    <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                            OnClientItemsRequested="OnClientItemsRequestedOrganization"
                            ClientIDMode="Static"
                            ShowMoreResultsBox="true"
                            Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True"
                            EnableLoadOnDemand="True">
                            <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                        </telerik:RadComboBox>
                </div>
                <div class="form-group">
                     <asp:UpdatePanel ID="updatepanelexcel" runat="server">
                <ContentTemplate>
                     <asp:LinkButton runat="server" ID="ibExportExcel" AlternateText="Export to Excel" CssClass="button btn btn-success"
                            ToolTip="Export to Excel" ><i class="fa fa-file-excel-o"></i>&nbsp;&nbsp;Export to Excel</asp:LinkButton>
                    <a href="#" class="help">&nbsp;</a>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="ibExportExcel" />
                </Triggers>
            </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12">
      <center><asp:Literal ID="litMessage" runat="server"></asp:Literal></center>  
    </div>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblHeader" runat="server" Text="Received Payments Details"></asp:Label>&nbsp;<a href="#" onclick="return OpenHelpPopUp('opportunity/frmpaymentdetails.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content10" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
    <asp:DataGrid ID="dgPaymentDetails" runat="server" CssClass="dg table table-responsive table-bordered" Width="100%" AutoGenerateColumns="False">
        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:BoundColumn HeaderText="Company Name" DataField="vcCompanyName" ItemStyle-HorizontalAlign="Center">
                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Order ID" SortExpression="vcPOppName">
                <ItemTemplate>
                    <a href="javascript:OpenOpp('<%# Eval("numOppId") %>','<%# Eval("tintopptype") %>')">
                        <%# Eval("vcPOppName") %>
                    </a>
                    <asp:HiddenField runat="server" ID="hfReferenceID" Value='<%# Eval("numReferenceID") %>'></asp:HiddenField>
                    <asp:HiddenField runat="server" ID="hfReferenceDetailID" Value='<%# Eval("numReferenceDetailID") %>'></asp:HiddenField>
                    <asp:HiddenField runat="server" ID="hfOpptype" Value='<%# Eval("tintopptype") %>'></asp:HiddenField>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="BizDoc ID">
                <ItemTemplate>
                    <asp:HyperLink ID="hplName" runat="server" Target="_blank" Text='<%# DataBinder.Eval(Container.DataItem, "vcBizDocID") %>'>
                    </asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="Amount">
                <ItemTemplate>
                    <%# ReturnMoney(DataBinder.Eval(Container.DataItem, "Amount")) %>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="dtPaymentDate" HeaderText="Date"></asp:BoundColumn>
            <asp:BoundColumn DataField="PaymentMethod" HeaderText="Payment Method"></asp:BoundColumn>
            <%--<asp:BoundColumn DataField="CreatedBy" HeaderText="Entered By, on"></asp:BoundColumn>--%>
            <asp:BoundColumn DataField="Memo" HeaderText="Memo"></asp:BoundColumn>
            <asp:BoundColumn DataField="Reference" HeaderText="Reference #"></asp:BoundColumn>
            <asp:TemplateColumn>
                <ItemTemplate>
                    <asp:Button ID="btnDelete" runat="server" CssClass="button Delete btn btn-danger" Text="X" CommandName="Delete"></asp:Button>
                    <asp:LinkButton ID="lnkDelete" runat="server" Visible="false">
													  <font color="#730000">*</font></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
        </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
