﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmRecurrenceDetail.aspx.vb" Inherits=".frmRecurrenceDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .tblOrderRecurDetail tr {
            height: 30px;
        }

        .tblOrderRecurDetail td {
            width: auto;
            white-space: nowrap;
        }

            .tblOrderRecurDetail td:nth-child(1) {
                font-weight: bold;
                font-size: 12px;
                text-align: right;
            }

            .tblOrderRecurDetail td:nth-child(3) {
                font-weight: bold;
                font-size: 12px;
                text-align: right;
            }

            .tblOrderRecurDetail td:nth-child(5) {
                font-weight: bold;
                font-size: 12px;
                text-align: right;
            }

            .tblOrderRecurDetail td:nth-child(2) {
                width: 33.33%;
            }

            .tblOrderRecurDetail td:nth-child(4) {
                width: 33.33%;
            }

            .tblOrderRecurDetail td:nth-child(6) {
                width: 33.33%;
            }
    </style>
    <script type="text/javascript">
        function DeleteConfirm() {
            if (confirm("Are you sure you want to delete recurred record?")) {
                return true;
            } else {
                return false;
            }
        }

        function OpenOpp(a) {
            var str;
            str = "../opportunity/frmOpportunities.aspx?frm=deallist&OpID=" + a;
            window.opener.location.href = str;
        }

        function OpenBizInvoice(a, b, c) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b + '&Print=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div style="text-align: center">
        <asp:Label ID="lblMessage" runat="server" ForeColor="Red"></asp:Label>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Recurrence Detail
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <table style="width: 800px; height: 100%; vertical-align: top; padding: 10px;" class="tblOrderRecurDetail">
        <tr>
            <td>Recurrence Type:
            </td>
            <td>
                <asp:Label ID="lblRecurType" runat="server"></asp:Label>
            </td>
            <td>Frequency:
            </td>
            <td>
                <asp:Label ID="lblFrequency" runat="server"></asp:Label>
            </td>
            <td>Status:
            </td>
            <td>
                <asp:Label ID="lblCompleted" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>Start Date:
            </td>
            <td>
                <asp:Label ID="lblStartDate" runat="server"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblEndDateTitle" runat="server" Text="End Date:"></asp:Label>
            </td>
            <td>
                <asp:Label ID="lblEndDate" runat="server"></asp:Label>
            </td>
            <td>Next Recurrence Date:
            </td>
            <td>
                <asp:Label ID="lblNextRecurrenceDate" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>No of Transactions:</td>
            <td>
                <asp:Label ID="lblTransaction" runat="server"></asp:Label>
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </table>
    <div style="padding: 10px;">
        <asp:Label runat="server" ID="lblRecurTitle" Font-Bold="true" Font-Size="Medium" />
        <asp:GridView ID="gvRecurrence" runat="server" AutoGenerateColumns="false" Width="100%" CssClass="tbl" ShowHeaderWhenEmpty="true" HeaderStyle-Height="28">
            <Columns>
                <asp:TemplateField ItemStyle-Wrap="false">
                    <HeaderTemplate>Recurred Date</HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblDate" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-Wrap="false" ItemStyle-Width="100%">
                    <HeaderTemplate>
                        <asp:Label ID="lblNameHeader" runat="server"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:HyperLink ID="hplName" runat="server"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-Wrap="false">
                    <HeaderTemplate>Status</HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblError" runat="server"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField AccessibleHeaderText="Delete" Visible="false">
                    <ItemTemplate>
                        <asp:HiddenField ID="hdnOppID" runat="server" />
                        <asp:HiddenField ID="hdnOppBizDocID" runat="server" />
                        <asp:Button ID="btnDelete" runat="server" OnClientClick="return DeleteConfirm();" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                No recurrence.
            </EmptyDataTemplate>
        </asp:GridView>
    </div>
    <asp:HiddenField ID="hdnRecurConfigID" runat="server" />
    <asp:HiddenField ID="hdnRecurrenceType" runat="server" />
</asp:Content>
