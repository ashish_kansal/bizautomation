﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmBillofLading.aspx.vb"
    Inherits=".frmBillofLading" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Bill of Lading</title>

    <script language="javascript" type="text/javascript">
        function PrintIt() {
            tblButtons.style.display = 'none';
            window.print();
            return false;
        }

        function Close() {
            window.close();
        }

    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <br>
    <asp:Table ID="Table1" BorderWidth="1" runat="server" Width="100%" BorderColor="black"
        GridLines="None">
        <asp:TableRow>
            <asp:TableCell>
                <table width="100%" border="0" cellpadding="0">
                    <tr>
                        <td valign="top" align="left">
                            <asp:Image ID="imgLogo" runat="server"></asp:Image>
                        </td>
                        <td align="right" colspan="4" nowrap>
                            <asp:Label ID="lblBizDoc" ForeColor="#c0c0c0" Font-Name="Arial black" runat="server"
                                Font-Size="26" Font-Bold="True"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellpadding="0">
                    <tr class="normal1">
                        <td colspan="5">
                            <table height="100%" width="100%">
                                <tr bgcolor="#ACA899">
                                    <td class="normal1">
                                        <a id="hplBillto" runat="server" class="hyperlink"><font color="white">Bill To</font></a>
                                    </td>
                                    <td class="normal1">
                                        <a id="hplShipTo" runat="server" class="hyperlink"><font color="white">Ship To</font></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1">
                                        <asp:Label ID="lblBillTo" runat="server"></asp:Label>
                                    </td>
                                    <td class="normal1">
                                        <asp:Label ID="lblShipTo" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr class="normal1" bgcolor="#ACA899">
                        <td class="normal1" colspan="5">
                            <font color="white">Comments</font>
                        </td>
                    </tr>
                    <tr class="normal1">
                        <td colspan="5">
                            <asp:Label ID="txtComments" runat="server" CssClass="signup" Width="630"></asp:Label>
                        </td>
                    </tr>
                    <tr class="normal1">
                        <td colspan="5">
                            <asp:DataGrid ID="dgBizDocs" runat="server" ForeColor="" Font-Size="10px"
                                Width="100%" BorderStyle="None"  BackColor="White" GridLines="Vertical"
                                HorizontalAlign="Center" AutoGenerateColumns="False">
                                <AlternatingItemStyle Font-Size="8pt" Font-Names="tahoma,verdana,arial,helvetica"
                                    BackColor="White"></AlternatingItemStyle>
                                <ItemStyle Font-Size="8pt" Font-Names="tahoma,verdana,arial,helvetica" HorizontalAlign="Center"
                                    BorderStyle="None"  VerticalAlign="Middle" BackColor="#e0dfe3">
                                </ItemStyle>
                                <HeaderStyle Font-Size="8pt" Font-Names="tahoma,verdana,arial,helvetica" Wrap="False"
                                    HorizontalAlign="Center" Height="20px" ForeColor="White" BorderColor="Black"
                                    VerticalAlign="Middle" BackColor="#9d9da1"></HeaderStyle>
                                <Columns>
                                    <asp:BoundColumn DataField="vcItemName" HeaderText="Item"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="txtItemDesc" HeaderText="Desc"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numUnitHour" DataFormatString="{0:#,##0}" HeaderText="Units">
                                    </asp:BoundColumn>
                                </Columns>
                            </asp:DataGrid>
                        </td>
                    </tr>
                </table>
                <table id="tblButtons" width="100%">
                    <tr>
                        <td align="right" class="normal1" colspan="2">
                            <asp:Button ID="btnPrint" runat="server" Text="Print" Width="50" CssClass="button">
                            </asp:Button>&nbsp;
                            <asp:Button ID="btnClose" runat="server" Text="Close" Width="50" CssClass="button">
                            </asp:Button>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </form>
</body>
</html>
