Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Opportunities
    Partial Public Class frmSelectVendor
        Inherits BACRMPage

        Dim lngItemCode As Long

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lngItemCode = GetQueryStringVal( "ItemCode")
                If Not IsPostBack Then BindGrid()
                
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindGrid()
            Try
                Dim objItems As New CItems
                objItems.DomainID = Session("DomainID")
                objItems.ItemCode = lngItemCode
                dgVendor.DataSource = objItems.GetVendors
                dgVendor.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgVendor_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgVendor.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim txt As TextBox
                    txt = e.Item.FindControl("txtUnits")
                    Dim btnSelect As Button
                    btnSelect = e.Item.FindControl("btnSelect")
                    btnSelect.Attributes.Add("onclick", "return SelectVen('" & e.Item.Cells(2).Text & "','" & e.Item.Cells(0).Text & "','" & e.Item.Cells(8).Text & "','" & e.Item.Cells(7).Text & "','" & e.Item.Cells(3).Text & "','" & txt.ClientID & "')")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace