﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmProductAttribute.aspx.vb"
    Inherits=".frmProductAttribute" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Configure Attribute</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSelectClose" runat="server" Text="Select & Close" CssClass="button"
                Visible="false" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Configure Attribute
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="tblMain" runat="server" Width="600px" GridLines="None" BorderColor="black"
        BorderWidth="1" CssClass="aspTable">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table ID="tblItemAttr" runat="server" Width="100%" GridLines="None" BorderColor="black"
                    BorderWidth="1" CssClass="aspTable">
                </asp:Table>
            </asp:TableCell>
            <asp:TableCell Width="30%">
                <asp:Image ID="imgThumb" runat="server" BorderWidth="1" Width="100" BorderColor="black"
                    Height="100"></asp:Image>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
