﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmReturnDetail.aspx.vb"
    Inherits="BACRM.UserInterface.Opportunities.frmReturnDetail" ClientIDMode="Static"
    MasterPageFile="~/common/DetailPage.Master" %>

<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Src="~/Accounting/TransactionInfo.ascx" TagName="TransactionInfo" TagPrefix="uc1" %>
<%@ Register Src="../Accounting/PaymentHistory.ascx" TagName="PaymentHistory" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Return Detail</title>
    <style type="text/css">
        .column {
            float: left;
        }

        html > body .RadComboBoxDropDown_Vista .rcbItem, html > body .RadComboBoxDropDown_Vista .rcbHovered, html > body .RadComboBoxDropDown_Vista .rcbDisabled, html > body .RadComboBoxDropDown_Vista .rcbLoading {
            display: inline-block;
        }

        /*.tip {
            color: #ffffff !important;
        }*/

        .col1, .col2, .col3, .col4, .col5, .col6 {
            float: left;
            width: 80px;
            margin: 0;
            padding: 0 5px 0 0;
            line-height: 14px;
            display: block; /*border:0px solid red;*/
        }

        .col1 {
            width: 220px;
        }

        .col2 {
            width: 200px;
        }

        .col3 {
            width: 80px;
        }

        .col4 {
            width: 80px;
        }

        .col5 {
            width: 85px;
        }

        .col6 {
            width: 80px;
        }

         .multipleRowsColumns .rcbItem, .multipleRowsColumns .rcbHovered {
            padding: 4px !important;
            border-bottom: 1px solid #d8d8d8;
            min-height: 24px !important;
        }
    </style>
    
    <script src="../JavaScript/jquery.Tooltip.js" type="text/javascript"></script>
    <script type="text/javascript">

        function pageLoaded() {
            InitializeValidation();

            var chkBox = $("input[id$='chkSelectAll']");
            chkBox.click(function () {
                $("[id$=gvProduct] INPUT[type='checkbox']").prop('checked', chkBox.is(':checked'));
            });

            // To deselect CheckAll when a GridView CheckBox        // is unchecked
            $("[id$=gvProduct] INPUT[type='checkbox']").click(function (e) {
                if (!$(this)[0].checked) {
                    chkBox.prop("checked", false);
                }
            });

            SelectAccount(0);

            $("[id$=dgItems] tr").not(".hs,.fs").each(function () {
                txLot = $(this).find("[id*='txLot']");

                if (txLot != null)
                    $(txLot).attr("readonly", "readonly");
            });

            //OVERRIDE EXISTING FUNCTION WHICH IS IN MASTERPAGE : CODE UTILITY USED.
            window.OpenBizInvoice = function (a, b, c) {
                window.open('../opportunity/frmMirrorBizdoc.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&RefID=' + a + '&RefType=' + b + '&Print=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
                return false;
            };
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function IsExists(pagePath, dataString) {
            $.ajax({
                type: "POST",
                url: pagePath,
                data: dataString,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                error:
          function (XMLHttpRequest, textStatus, errorThrown) {
          },
                success: function (result) {
                }
            });
        }

        function OnClientTabSelected(sender, eventArgs) {
            var tab = eventArgs.get_tab();
            PersistTab(tab.get_value());
        }

        function PersistTab(index) {
            var key;
            var value;
            var formName;

            key = "index";
            value = index;

            var pagePath = window.location.pathname + "/PersistTab";
            var dataString = "{strKey:'" + key + "',strValue:'" + value + "'}"
            IsExists(pagePath, dataString);
        }

        function SelectAccount(a) {
            if (a == 0) {
                if ($('[id$=rbtnItem]').is(':checked') == true) {
                    $('[id$=trCreditAccounts]').hide();
                    $('[id$=trItem]').show();
                }

                if ($('[id$=rbtnAccount]').is(':checked') == true) {
                    $('[id$=trCreditAccounts]').show();
                    $('[id$=trItem]').hide();
                }
            }
            else if (a == 1) {
                $('[id$=trCreditAccounts]').hide();
                $('[id$=trItem]').show();
            }
            else if (a == 2) {
                $('[id$=trCreditAccounts]').show();
                $('[id$=trItem]').hide();
            }
        }

        function OpenBizInvoice(a, b, c) {
            window.open('../opportunity/frmMirrorBizdoc.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&RefID=' + a + '&RefType=' + b + '&Print=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }

        function fn_GoToURL(varURL) {

            if ((varURL != '') && (varURL.substr(0, 7) == 'http://') && (varURL.length > 7)) {
                var LoWindow = window.open(varURL, "", "");
                LoWindow.focus();
            }
            return false;
        }
        function ShowlinkedProjects(a) {
            window.open("../opportunity/frmLinkedProjects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&opId=" + a, '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeletMsg() {
            var bln = confirm("You’re about to remove the Stage from this Process, all stage data will be deleted")
            if (bln == true) {
                return true;
            }
            else {
                return false;
            }
        }

        function OpenDocuments(a) {
            window.open("../Documents/frmSpecDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=O&yunWE=" + a, '', 'toolbar=no,titlebar=no,top=200,width=900,height=450,left=200,scrollbars=yes,resizable=yes')
            return false;
        }

        function DealCompleted() {
            if (confirm("This Deal will now be removed from the 'Open Deals' section, and will reside only in the 'Closed Deals' section within the Organization the deal is for. Except for BizDocs or any Projects that depend on BizDocs - Modifications to Deal Details, Milestones & Stages, Associated Contacts, and Products / Services, will no longer be allowed.")) {
                return true;
            }
            else {
                return false;
            }
        }

        function CannotShip() {

            alert("You can't ship at this time because you don't have enough quantity on hand to support your shipment. Your options are to modify your order, or replenish inventory (to check inventory click on the edit link within the line item, then the value in the 'Products/Services' column):")
            return false;
        }

        function AlertMsg() {
            if (confirm("Please note that after a 'Received' or 'Shipped' request is executed, except for the BizDocs and any Projects that depend on BizDocs - Additional changes will not be permitted on this Deal (i.e. it will be frozen). Do you wish to continue ?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function ShowWindow(Page, q, att) {

            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                return false;

            }
        }

        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }

        function Save() {
            return validateCustomFields();
        }

        function OpenBiz(a, b) {
            window.open('../opportunity/frmBizDocs.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppType=' + b, '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenExpense(a, b, c) {
            window.open('../opportunity/frmOppExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OPPStageID=' + a + '&Opid=' + b + '&DivId=' + c, '', 'toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
            return false;
        }

        function ValidateCheckBox(cint) {
            if (cint == 1) {
                if (document.getElementById('chkDClosed').checked == true) {
                    if (document.getElementById('chkDlost').checked == true) {
                        alert("The Deal is already Lost !")
                        document.getElementById('chkDClosed').checked = false
                        return false;
                    }

                }
            }
            if (cint == 2) {
                if (document.getElementById('chkDlost').checked == true) {
                    if (document.getElementById('chkDClosed').checked == true) {
                        alert("The Deal is already Closed !")
                        document.getElementById('chkDlost').checked = false
                        return false;
                    }
                    document.getElementById('chkActive').checked = false;
                }
            }
        }

        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }

        function OpenCreateOpp(a, b) {
            window.open('../opportunity/frmCreateSalesPurFromOpp.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID=' + a + '&OppType=' + b + '&Created=false', '', 'toolbar=no,titlebar=no,left=100,top=100,width=1000,height=700,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenConfSerItem(a, b, c) {
            window.open('../opportunity/frmAddSerializedItem.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID=' + a + '&ItemCode=' + b + '&OppType=' + c, '', 'toolbar=no,titlebar=no,left=100,top=100,width=530,height=400,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenTransfer(url) {
            window.open(url, '', "width=340,height=150,status=no,top=100,left=150");
            return false;
        }

        function beforeSelectEvent(sender, eventArgs) {

        }

        function OpenAddReturn(a, b, c) {
            window.open('../opportunity/frmAddReturn.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&type=' + b + '&OppItemIds=' + c, '', 'toolbar=no,titlebar=no,top=300,width=800,height=350,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenSalesTemplate(a) {
            window.open('../opportunity/frmSalesTemplate.aspx?OppId=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=680,height=300,scrollbars=yes,resizable=yes');
            return false;
        }

        function ShowLayout(a, b, d) {
            window.open("../pagelayout/frmCustomisePageLayout.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Ctype=" + a + "&PType=3&FormId=" + d, '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
            return false;
        }

        function openChild(a) {
            window.open("../opportunity/frmChildOpportunity.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=" + a, '', 'toolbar=no,titlebar=no,width=1000,height=400,top=200,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenRecurringDetailPage(oppId, Shipped) {
            window.open('../Accounting/frmRecurringDetails.aspx?oppId=' + oppId + '&Shipped=' + Shipped, '', 'toolbar=no,titlebar=no,left=300,top=300,width=700,height=350,scrollbars=yes,resizable=no')
            return false;
        }

        function OpenSetting() {
            window.open('../Items/frmConfItemList.aspx?FormID=26', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function EditImage(a, b) {
            window.open('../Items/frmOrderItemImage.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=' + a + '&OppItemCode=' + b, '', 'toolbar=no,titlebar=no,left=300, top=250,width=600,height=500,scrollbars=yes,resizable=yes');
            return false;
        }

        function EditItem(a) {
            document.getElementById('txtHidEditOppItem').value = a;
            document.getElementById('btnEditSelected').click();
            return false;
        }

        function Export() {
            document.getElementById('txtGridHTML').value = $('[id$=gvProduct]').html();
        }

        function SetWareHouseId(id) {
            var combo = $find('radWareHouse')
            var node = combo.findItemByValue(id);
            node.select();
        }

        function AddEditOrder(a, b, c) {
            window.open('../opportunity/frmAddEditOrder.aspx?opid=' + a + '&OppType=' + b + '&DivId=' + c, '', 'toolbar=no,titlebar=no,left=300, top=250,width=1100,height=600,scrollbars=yes,resizable=yes');
            return false;
        }

        function openGrossProfitEstimate(a) {
            window.open('../opportunity/frmGrossProfitEstimate.aspx?oppid=' + a, '', 'toolbar=no,titlebar=no,left=200, top=300,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenProjectStageDetail(a, b) {
            if (a > 0 && b > 0) {
                var str;
                str = "../projects/frmProjects.aspx?ProId=" + a + "&StageId=" + b + "&SelectedIndex=1";
                window.location.href = str;
            }
            return false;
        }

        function ReOpenDeal() {
            if (confirm('Are you sure, you want to Re-Open Deal? It will reverse Inventory of Item.')) {
                return true;
            }
            else {
                return false;
            }
        }

        function OpenSalesOrderClone(OppId) {
            window.open('../opportunity/frmNewOrder.aspx?OppId=' + OppId + '&IsClone=true', '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=1000,height=600,scrollbars=yes,resizable=yes');
            return false;
        }

        function SubmitForPayment(a, b, c) {
            var str;
            str = "../opportunity/frmSubmitReturnForPayment.aspx?OpID=" + a + "&ReturnID=" + b + "&type=" + c;
            window.open(str, '', 'toolbar=no,titlebar=no,left=300,top=200,scrollbars=yes,resizable=yes');
        }

        function OpenShareRecord(a) {
            window.open("../common/frmShareRecord.aspx?RecordID=" + a + "&ModuleID=3", '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenOppKitItems(a, b) {
            window.open('../opportunity/frmOppKitItems.aspx?OppID=' + a + '&OppItemCode=' + b, '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
        }

        function OpenRMAConfSerItem(a, b, c, d, e, f) {
            if (document.getElementById(e).value == "" || parseInt(document.getElementById(e).value) == 0) {
                alert("Please enter Received Qty then try to select Serial/Lot#s.");
                return false;
            }

            window.open('../opportunity/frmRMASerializedItem.aspx?ReturnID=' + a + '&ReturnType=' + b + '&ReturnItemID=' + c + '&LotId=' + d + '&Qty=' + document.getElementById(e).innerText + '&hdnLotId=' + f, '', 'toolbar=no,titlebar=no,left=100,top=100,width=530,height=400,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenBizDoc(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenHelp() {
            window.open('../Help/Sales_Returns.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function ConfirmIssueRefundOrCreditMemo(message) {
            if (confirm("Are you sure you want to issue " + message + "?")) {
                return true;
            } else {
                return false;
            }
        }


    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Returns&nbsp;&nbsp;<asp:Label ID="lblReturns" runat="server" ForeColor="Gray" />&nbsp;<a href="#" onclick="return OpenHelp()"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12 col-sm-4">
            <div class="record-small-box">
                <a href="#" class="small-box-footer">Created By <i class="fa fa-user"></i></a>
                <div class="inner">
                    <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
                </div>

            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
            <div class="record-small-box">
                <a href="#" class="small-box-footer">Last Modified By <i class="fa fa-user"></i></a>
                <div class="inner">
                    <asp:Label ID="lblLastModifiedBy" runat="server"></asp:Label>
                </div>

            </div>
        </div>
        <div class="col-xs-12 col-sm-4">
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <uc1:TransactionInfo ID="TransactionInfo1" runat="server" />

    <div class="row">

        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-inline">
                <telerik:RadToolTip ID="radToolTip" runat="server" Width="500px" ShowEvent="onmouseover"
                    RelativeTo="Element" HideEvent="LeaveTargetAndToolTip" Position="TopRight" TargetControlID="lblTooltip">
                </telerik:RadToolTip>

                <div class="pull-left callout bg-theme">
                    <%--<strong style="font-size:larger">Customer: </strong>--%>
                    <span>
                        <asp:HyperLink ID="hplCustomer" runat="server" Font-Size="Larger"></asp:HyperLink></span><asp:Label ID="lblToolTip" runat="server" CssClass="tip">[?]</asp:Label>
                </div>

            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-inline">
                <label>Return Date:</label>
                <asp:Label ID="lblReturnDate" runat="server"></asp:Label>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-inline">
                <label>Source:</label>
                <asp:HyperLink ID="hplSource" runat="server"></asp:HyperLink>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <div class="form-inline">
                <label>Status:</label>
                <asp:Label ID="lblStatus" runat="server"></asp:Label>
                <asp:HiddenField ID="hdnStatus" runat="server" />
            </div>
        </div>
    </div>
    <div class="alert alert-warning" id="divAlert" runat="server" visible="false">
        <h4><i class="icon fa fa-warning"></i>Alert!</h4>
        <asp:Literal ID="litMessage" runat="server"></asp:Literal>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnReceive" runat="server" CssClass="btn btn-primary">Receive</asp:LinkButton>
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save &amp; Close</asp:LinkButton>
                <asp:LinkButton ID="btnEdit" runat="server" CssClass="btn btn-info"><i class="fa fa-pencil"></i>&nbsp;&nbsp;Edit</asp:LinkButton>
                <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-primary"><i class="fa fa-times"></i>&nbsp;&nbsp;Close</asp:LinkButton>
            </div>
        </div>
    </div>
    <div class="text-aqua text-center" id="litNote" runat="server">
        <strong>Note:</strong> Qty on hand will not update until after you create a refund or credit memo.
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
        
    <div style="float: left;margin-right:10px; text-align: center;margin-top:2px" id="divContractsDetails" runat="server">
         
        <div class="form-inline">
            <asp:Label ID="lblWarrantyTooltip" Text="[?]" CssClass="tip" runat="server" ToolTip="" />
            <i>Warrantee days left</i>
        <asp:Label ID="lblWarrantyDaysLeft" CssClass="lblHeighlight" runat="server" Text="0"></asp:Label>
            
            </div>
    </div>
    <asp:TextBox ID="txtConEmail" runat="server" Style="display: none" />
    <asp:TextBox ID="txtBizDocTemplate" runat="server" Style="display: none" />
    <asp:TextBox ID="txtRefType" runat="server" Style="display: none" />
    <div class="btn-group">
        <asp:LinkButton runat="server" ID="ibtnSendEmail" AlternateText="Email as PDF" ToolTip="Email as PDF" class="btn btn-default" CommandName="Email"><i class="fa fa-envelope"></i></asp:LinkButton>
        <asp:UpdatePanel runat="server" class="btn btn-default" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:LinkButton runat="server" ID="ibtnExportPDF" AlternateText="Export to PDF" ToolTip="Export to PDF" Style="color: inherit !important" CommandName="PDF"><i class="fa fa-file-pdf-o"></i></asp:LinkButton>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="ibtnExportPDF" />
            </Triggers>
        </asp:UpdatePanel>
    
        <asp:LinkButton runat="server" ID="ibtnPrint" AlternateText="Print" ToolTip="Print" class="btn btn-default"><i class="fa fa-print"></i></asp:LinkButton>

        <div class="btn btn-default" style="float: right" runat="server" id="btnLayout">
            <span><i class="fa  fa-columns"></i>&nbsp;&nbsp;Layout</span>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdnBozDocName" />
    <asp:HiddenField runat="server" ID="hdnDivID" />
    <asp:HiddenField runat="server" ID="hdnContactId" />
    <asp:HiddenField runat="server" ID="hdnRecieveType" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <telerik:RadTabStrip ID="radOppTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
        Skin="Default" ClickSelectedTab="True" MultiPageID="radMultiPage_OppTab" SelectedIndex="0"
        OnClientTabSelecting="beforeSelectEvent" OnClientTabSelected="OnClientTabSelected">
        <Tabs>
            <telerik:RadTab Text="Sales Return Details" Value="Details" PageViewID="radPageView_Details">
            </telerik:RadTab>
            <telerik:RadTab Text="Products & Services" Value="ProductsServices" PageViewID="radPageView_ProductsServices">
            </telerik:RadTab>
            <telerik:RadTab Text="Issue Refund / Issue Credit Memo" Value="IssueAndCredit" PageViewID="radPageView_IssueAndCredit">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage_OppTab" runat="server" CssClass="pageView">
        <telerik:RadPageView ID="radPageView_Details" runat="server" Selected="true">
            <asp:PlaceHolder ID="plhControls" runat="server"></asp:PlaceHolder>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_ProductsServices" runat="server">
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <asp:HiddenField ID="hftintshipped" runat="server" />
                        <asp:DataGrid ID="dgItems" runat="server" CssClass="table table-bordered table-striped" ShowFooter="true" AutoGenerateColumns="false" ClientIDMode="AutoID" Width="100%" UseAccessibleHeader="true">
                            <Columns>
                                <asp:BoundColumn HeaderText="Item ID" DataField="numItemCode" HeaderStyle-Wrap="false" ItemStyle-Wrap="false" />
                                <asp:TemplateColumn HeaderText="Item" HeaderStyle-Width="20%">
                                    <ItemTemplate>
                                        <a href="../Items/frmKitDetails.aspx?ItemCode=<%# Eval("numItemCode") %>">
                                            <asp:Label runat="server" ID="lblItemName" Text='<%# Eval("vcItemName") %>'></asp:Label>
                                        </a>
                                        <asp:HiddenField runat="server" ID="hdnItemCode" Value='<%# Eval("numItemCode") %>' />
                                        <asp:HiddenField runat="server" ID="hdnItemType" Value='<%# Eval("charItemType") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn HeaderText="Model ID" DataField="vcModelID" HeaderStyle-Wrap="false" />
                                <asp:BoundColumn HeaderText="SKU" DataField="vcSKU" HeaderStyle-Wrap="false" />
                                <asp:TemplateColumn HeaderText="Description" HeaderStyle-Width="20%">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblItemDesc" Text='<%# Eval("vcItemDesc") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Unit Price" HeaderStyle-Width="10%">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblPrice" Text='<%# ReturnMoney( Eval("monPrice") ) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Amount" HeaderStyle-Width="10%">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblTotAmount" Text='<%# ReturnMoney(Eval("monTotAmount")) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Units" HeaderStyle-Width="10%">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblUnitHour" Text='<%# Eval("numUnitHour") %>'></asp:Label><asp:HiddenField runat="server" ID="hdnUnitHour" Value='<%# Eval("numUnitHour") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Item Type" HeaderStyle-Width="10%">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblItemType" Text='<%# Eval("ItemType") %>'></asp:Label><asp:HiddenField runat="server" ID="hdncharItemType" Value='<%# Eval("charItemType") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Qty to Receive" HeaderStyle-Width="6%" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUnitReceived" runat="server" Text='<%# CCommon.ToDouble(Eval("numUnitHour")) - CCommon.ToDouble(Eval("numUnitHourReceived"))%>'></asp:Label>
                                        <asp:HiddenField runat="server" ID="hdnReturnItemID" Value='<%# Eval("numReturnItemID") %>' ClientIDMode="Static" />
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <asp:Button runat="server" ID="btnSaveReceive" Text="Save Received Qty" CssClass="btn btn-primary"
                                            CommandArgument='<%# Eval("numReturnItemID")%>' CommandName="SaveReceivedQty"
                                            Visible="false" />
                                    </FooterTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Return Warehouse" HeaderStyle-Width="15%">
                                    <ItemTemplate>
                                        <telerik:RadComboBox ID="radWareHouse" runat="server" Width="100%" DropDownWidth="800px" HighlightTemplatedItems="true"
                                            AccessKey="W" AutoPostBack="true" Height="100" DropDownCssClass="multipleRowsColumns" TabIndex="500">
                                            <HeaderTemplate>
                                                <ul style="padding-left: 5px;padding-right: 5px;">
                                                    <li class="col1">Warehouse</li>
                                                    <li class="col2">Attributes</li>
                                                    <li class="col3">On Hand</li>
                                                    <li class="col4">On Order</li>
                                                    <li class="col5">On Allocation</li>
                                                    <li class="col6">BackOrder</li>
                                                </ul>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <ul style="padding-left: 5px;padding-right: 5px;">
                                                    <li class="col1"><%# DataBinder.Eval(Container.DataItem, "vcWareHouse")%></li>
                                                    <li class="col2"><%# DataBinder.Eval(Container.DataItem, "Attr") %>&nbsp;</li>
                                                    <li class="col3"><%# DataBinder.Eval(Container.DataItem, "numOnHand")%>&nbsp;</li>
                                                    <li class="col4"><%# DataBinder.Eval(Container.DataItem, "numOnOrder")%>&nbsp;</li>
                                                    <li class="col5"><%# DataBinder.Eval(Container.DataItem, "numAllocation")%>&nbsp;</li>
                                                    <li class="col6"><%# DataBinder.Eval(Container.DataItem, "numBackOrder")%>&nbsp;</li>
                                                </ul>
                                            </ItemTemplate>
                                        </telerik:RadComboBox>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Serial/Lot #s">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txLot" runat="server" CssClass="signup" Text='<%# Eval("SerialLotNo") %>'></asp:TextBox>&nbsp;
                                        <asp:HyperLink ID="hlSerialized" runat="server" CssClass="BizLink">Add</asp:HyperLink>
                                        <asp:HiddenField ID="hdnSelectedLotSerial" runat="server" />
                                        <asp:HiddenField ID="hfbitSerialized" runat="server" Value='<%# Eval("bitSerialized") %>' />
                                        <asp:HiddenField ID="hfbitLotNo" runat="server" Value='<%# Eval("bitLotNo") %>' />
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_IssueAndCredit" runat="server">
            <div class="row">
                <div class="col-xs-12">
                    <asp:Panel class="box box-primary box-solid" ID="pnlApplyRefund" runat="server">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                <asp:RadioButton ID="rbtnIssueRefund" runat="server" GroupName="Issue" AutoPostBack="true" Checked="true" />Issue Refund
                            </h3>
                            <!-- /.box-tools -->
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <asp:Panel class="row" runat="server" ID="pnlRefund">
                                <div class="col-xs-12 col-sm-6" style="border-right: 2px dotted;">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="form-group">
                                                <label>Refund From</label>
                                                <asp:DropDownList ID="ddlRefundAccounts" AutoPostBack="true" runat="server" CssClass="required_numeric {required:'#rbtnDepositTo:checked', messages:{required:'Please select Refund From Account!'}} form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="form-group">
                                                <label>Balance</label>
                                                <div>
                                                    <asp:Label runat="server" ID="lblRefundBalance"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="form-group">
                                                <label>Check #</label>
                                                <div class="form-inline">
                                                    <asp:TextBox ID="txtCheckNo" runat="server" MaxLength="20" CssClass="form-control" autocomplete="OFF"></asp:TextBox>
                                                    <asp:CheckBox runat="server" ID="chkPrintCheck" Text="To be printed" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="form-group">
                                                <label>Create Refund Receipt</label>
                                                <div>
                                                    <asp:CheckBox ID="chkRefundReceipt1" Text="Yes" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 text-center">
                                            <asp:LinkButton ID="btnSaveRefund" runat="server" CssClass="btn btn-primary" OnClientClick="$(this).disabled = true; $(this).text('Please wait...');"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-inline">
                                        <label>Refund Receipt:</label>
                                        <div runat="server" id="tdRefund">
                                            <asp:HyperLink ID="hrfRefundReceipt" runat="server" CssClass="hyperlink">
                                            </asp:HyperLink>&nbsp;<asp:ImageButton runat="server" ID="ibtnDelRefunct" AlternateText="Delete" ToolTip="Delete"
                                                ImageUrl="~/images/DeleteGray.gif" Style="vertical-align: middle;" />
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <asp:Panel class="box box-primary box-solid" runat="server" ID="pnlApplyCredit">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                <asp:RadioButton ID="rbtnCreditMemo" runat="server" GroupName="Issue" AutoPostBack="true" />
                                Issue Credit Memo
                            </h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <asp:Panel CssClass="row" runat="server" ID="pnlCredit">
                                <div class="col-xs-12 col-sm-6" style="border-right: 2px dotted;">
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="form-group">
                                                <label></label>
                                                <div>
                                                    <asp:RadioButton ID="rbtnAccount" runat="server" GroupName="Credit" Text="Select Account" onclick="SelectAccount(2);" />
                                                    <asp:RadioButton ID="rbtnItem" runat="server" GroupName="Credit" Text="Select Item" onclick="SelectAccount(1);" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="form-group" id="trCreditAccounts" runat="server">
                                                <label>Credit From</label>
                                                <div style="display: inline">
                                                    <asp:DropDownList ID="ddlCreditAccounts" AutoPostBack="true" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                    &nbsp;&nbsp;Balance:&nbsp;
                                                    <asp:Label runat="server" ID="lblCreditBalance"></asp:Label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="form-group" id="trItem" runat="server">
                                                <label>Select a Service Item</label>
                                                <asp:DropDownList ID="ddlServiceItem" runat="server" CssClass="form-control" AutoPostBack="true">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="form-group">
                                                <label>Create Memo</label>
                                                <div>
                                                    <asp:CheckBox ID="chkRefundReceipt2" Text="Yes" runat="server" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-sm-6">
                                            <div class="form-group">
                                                <label></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xs-12 text-center">
                                            <asp:LinkButton ID="btnSaveCreditMemo" runat="server" CssClass="btn btn-primary" OnClientClick="$(this).disabled = true; $(this).text('Please wait...');"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-6">
                                    <div class="form-inline">
                                        <label>Credit Memo:</label>
                                        <div runat="server" id="tdCreditMemo">
                                            <asp:HyperLink ID="hrfCreditMemo" runat="server" CssClass="hyperlink"> 
                                            </asp:HyperLink>&nbsp;
                                            <asp:ImageButton runat="server" ID="ibtnDelCrditMemo" AlternateText="Delete" ToolTip="Delete"
                                                ImageUrl="~/images/DeleteGray.gif" CausesValidation="false" Style="vertical-align: middle;" />
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <asp:Panel class="row" runat="server" ID="pnlApplyDirectRefund" Visible="false">
                <div class="col-xs-12">
                    <div class="box box-primary box-solid">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                <asp:RadioButton ID="rbtnIssueDirectRefund" runat="server" GroupName="Issue" AutoPostBack="true" />Direct Refund</h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <asp:Panel class="row" runat="server" ID="pnlDirectRefund">
                                <div class="table-responsive">
                                    <asp:DataGrid ID="dgPaymentDetails" runat="server" CssClass="table table-bordered table-striped" Width="100%" AutoGenerateColumns="False" CellPadding="2" CellSpacing="2">
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="Transaction#">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTransactionID" runat="server" Text='<%# Eval("vcTransactionID") %>'></asp:Label><asp:HiddenField runat="server" ID="hdnTransHistoryID" Value='<%# Eval("numTransHistoryID") %>' />
                                                    <asp:HiddenField runat="server" ID="hdnTransactionID" Value='<%# Eval("vcTransactionID") %>' />
                                                    <asp:HiddenField runat="server" ID="hdnDivisionID" Value='<%# Eval("numDivisionID") %>' />
                                                    <asp:HiddenField runat="server" ID="hdnContactID" Value='<%# Eval("numContactID") %>' />
                                                    <asp:HiddenField runat="server" ID="hdnOppID" Value='<%# Eval("numOppID") %>' />
                                                    <asp:HiddenField runat="server" ID="hdnOppBizDocID" Value='<%# Eval("numOppBizDocsID") %>' />
                                                    <asp:HiddenField runat="server" ID="hdnCardNo" Value='<%# Eval("vcCreditCardNo") %>' />
                                                    <asp:HiddenField runat="server" ID="hdnCVVNo" Value='<%# Eval("vcCVV2") %>' />
                                                    <asp:HiddenField runat="server" ID="hdnMonth" Value='<%# Eval("tintValidMonth") %>' />
                                                    <asp:HiddenField runat="server" ID="hdnYear" Value='<%# Eval("intValidYear") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblStatus" Text='<%# Eval("vcTransactionStatus") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Authorize Amt" HeaderStyle-Width="80px">
                                                <ItemStyle HorizontalAlign="Right" />
                                                <HeaderStyle HorizontalAlign="Right" />
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblAuthorizeAmt" Text='<%# String.Format("{0:#,##0.00}", Eval("monAuthorizedAmt")) %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Capture Amt" HeaderStyle-Width="80px">
                                                <ItemStyle HorizontalAlign="Right" />
                                                <HeaderStyle HorizontalAlign="Right" />
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCapturedAmt" Text='<%# String.Format("{0:#,##0.00}", Eval("monCapturedAmt")) %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Refund Amt" HeaderStyle-Width="80px">
                                                <ItemStyle HorizontalAlign="Right" />
                                                <HeaderStyle HorizontalAlign="Right" />
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblRefundAmt" Text='<%# String.Format("{0:#,##0.00}", Eval("monRefundAmt")) %>'></asp:Label>&nbsp;
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Date">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblDate" Text='<%# ReturnName(DataBinder.Eval(Container.DataItem, "dtCreatedDate"))%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="BizDoc ID / Order ID">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hplBizDoc" runat="server">
                                                        <asp:Label runat="server" ID="lblBizOrderID" Text='<%# Eval("vcBizOrderID") %>'></asp:Label>
                                                    </asp:HyperLink>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Card No" HeaderStyle-Width="80px">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCardNo"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Card Type" HeaderStyle-Width="80px">
                                                <ItemTemplate>
                                                    <asp:Label runat="server" ID="lblCardType" Text='<%# Eval("vcCardType") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Amount">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtAmount" runat="server" Width="60"></asp:TextBox>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                    <div class="form-inline">
                                        <label>Refund Amount:</label>
                                        <asp:Label ID="lblEstimatedBizDocAmount" Font-Bold="true" runat="server" Text="0.00"></asp:Label>
                                    </div>
                                </div>
                            </asp:Panel>

                            <div class="row">
                                <div class="col-xs-12">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <uc2:PaymentHistory ID="PaymentHistory1" runat="server" />
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <asp:HiddenField ID="OppType" runat="server" />
    <asp:HiddenField ID="OppStatus" runat="server" />
    <asp:HiddenField ID="DivID" runat="server" />
    <asp:HiddenField ID="OppID" runat="server" />
    <asp:TextBox ID="txtrecOwner" Style="display: none" runat="server">
    </asp:TextBox><asp:TextBox ID="txtHidValue" runat="server" Style="display: none">
    </asp:TextBox><asp:TextBox ID="txtHidEditOppItem" runat="server" Style="display: none">
    </asp:TextBox><asp:TextBox ID="txtHidOptValue" runat="server" Style="display: none">
    </asp:TextBox><asp:TextBox ID="txtSerialize" runat="server" Style="display: none">
    </asp:TextBox><asp:TextBox ID="txtTemplateId" Text="0" runat="server" Style="display: none">
    </asp:TextBox><asp:TextBox ID="txtProcessId" Text="0" runat="server" Style="display: none">
    </asp:TextBox><asp:TextBox ID="txtGridHTML" Text="" runat="server" Style="display: none">
    </asp:TextBox><input id="hdKit" runat="server" type="hidden" /><input id="hdOptKit" runat="server" type="hidden" /><asp:TextBox ID="txtModelID" runat="server" Style="display: none">
    </asp:TextBox><asp:DropDownList ID="ddlOpttype" runat="server" CssClass="signup" Width="200" Style="display: none"
        Enabled="False">
        <asp:ListItem Value="P">Product</asp:ListItem>
        <asp:ListItem Value="S">Service</asp:ListItem>
        <asp:ListItem Value="N">Non-Inventory Item</asp:ListItem>
    </asp:DropDownList><asp:Button ID="btnRequestReturn1" runat="server" Text="" Style="display: none" />
    <asp:HiddenField ID="hdnCmbDivisionID" runat="server" />
    <asp:HiddenField ID="hdnCmbOppType" runat="server" />
    <asp:HiddenField ID="hdnCRMType" runat="server" />
    <asp:HiddenField ID="hdnStockTransfer" runat="server" />
    <asp:HiddenField runat="server" ID="hdnBizDocHTML" />
    <asp:HiddenField ID="hfReceiveType" runat="server" />
    <asp:HiddenField ID="hfReturnType" runat="server" />
    <asp:HiddenField ID="hfEstimatedBizDocAmount" runat="server" />
    <asp:HiddenField ID="hdnUnitHourReceived" runat="server" />
    <asp:HiddenField ID="hdnOppBizDocID" runat="server" />
</asp:Content>
