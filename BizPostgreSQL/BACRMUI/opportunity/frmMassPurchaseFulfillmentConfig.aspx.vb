﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Opportunities

Namespace BACRM.UserInterface.Opportunities

    Public Class frmMassPurchaseFulfillmentConfig
        Inherits BACRMPage

#Region "Member Variable"

        Dim objContact As CContacts
        Dim objMassPurchaseFulfillmentConfiguration As MassPurchaseFulfillmentConfiguration

#End Region

#Region "Page Events"

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not Page.IsPostBack Then
                    BindConfiguration()
                    BindData()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

#End Region

#Region "Private Methods"

        Private Sub BindData()
            Try
                chkGroupByOrder.Checked = False
                divShowOnlyFullyReceived.Visible = False
                divReceiveBillOnClose.Visible = False
                divBill.Visible = False

                lbAvailableFieldLeft.Items.Clear()
                lbSelectedFieldLeft.Items.Clear()

                Dim ds As DataSet
                objContact = New CContacts
                objContact.DomainID = Session("DomainId")
                objContact.UserCntID = Session("UserContactId")
                objContact.ViewID = CCommon.ToShort(ddlWhen.SelectedValue)
                objContact.FormId = 135
                ds = objContact.GetColumnConfiguration()

                lbAvailableFieldLeft.DataSource = ds.Tables(0)
                lbAvailableFieldLeft.DataTextField = "vcFieldName"
                lbAvailableFieldLeft.DataValueField = "numFieldID"
                lbAvailableFieldLeft.DataBind()

                lbSelectedFieldLeft.DataSource = ds.Tables(1)
                lbSelectedFieldLeft.DataValueField = "numFieldID"
                lbSelectedFieldLeft.DataTextField = "vcFieldName"
                lbSelectedFieldLeft.DataBind()

                If ddlWhen.SelectedValue = "1" Then
                    objContact.FormId = 143
                    objContact.ViewID = 1
                    ds = objContact.GetColumnConfiguration()

                    lbAvailableFieldRight.DataSource = ds.Tables(0)
                    lbAvailableFieldRight.DataTextField = "vcFieldName"
                    lbAvailableFieldRight.DataValueField = "numFieldID"
                    lbAvailableFieldRight.DataBind()

                    lbSelectedFieldRight.DataSource = ds.Tables(1)
                    lbSelectedFieldRight.DataValueField = "numFieldID"
                    lbSelectedFieldRight.DataTextField = "vcFieldName"
                    lbSelectedFieldRight.DataBind()
                End If

                If ddlWhen.SelectedValue = "1" Then
                    chkGroupByOrder.Checked = CCommon.ToBool(hdnGroupByOrderForReceive.Value)
                    divGroupBy.Style.Remove("display")
                    divRight.Style.Remove("display")
                ElseIf ddlWhen.SelectedValue = "2" Then
                    chkGroupByOrder.Checked = CCommon.ToBool(hdnGroupByOrderForPutAway.Value)
                    divGroupBy.Style.Remove("display")
                    divRight.Style.Add("display", "none")
                ElseIf ddlWhen.SelectedValue = "3" Then
                    chkGroupByOrder.Checked = CCommon.ToBool(hdnGroupByOrderForBill.Value)
                    divBill.Visible = True
                    divGroupBy.Style.Remove("display")
                    divRight.Style.Add("display", "none")
                ElseIf ddlWhen.SelectedValue = "4" Then
                    chkGroupByOrder.Checked = True
                    divShowOnlyFullyReceived.Visible = True
                    divReceiveBillOnClose.Visible = True
                    divGroupBy.Style.Add("display", "none")
                    divRight.Style.Add("display", "none")
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub BindConfiguration()
            Try
                objMassPurchaseFulfillmentConfiguration = New MassPurchaseFulfillmentConfiguration
                objMassPurchaseFulfillmentConfiguration.DomainID = CCommon.ToLong(Session("DomainID"))
                objMassPurchaseFulfillmentConfiguration.UserCntID = CCommon.ToLong(Session("UserContactID"))
                Dim dt As DataTable = objMassPurchaseFulfillmentConfiguration.GetByUser()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    hdnGroupByOrderForReceive.Value = CCommon.ToBool(dt.Rows(0)("bitGroupByOrderForReceive"))
                    hdnGroupByOrderForPutAway.Value = CCommon.ToBool(dt.Rows(0)("bitGroupByOrderForPutAway"))
                    hdnGroupByOrderForBill.Value = CCommon.ToBool(dt.Rows(0)("bitGroupByOrderForBill"))
                    hdnGroupByOrderForClose.Value = CCommon.ToBool(dt.Rows(0)("bitGroupByOrderForClose"))
                    chkShowOnlyFullyReceived.Checked = CCommon.ToBool(dt.Rows(0)("bitShowOnlyFullyReceived"))
                    chkReceiveBillOnClose.Checked = CCommon.ToBool(dt.Rows(0)("bitReceiveBillOnClose"))

                    If Not ddlBillType.Items.FindByValue(dt.Rows(0)("tintBillType")) Is Nothing Then
                        ddlBillType.Items.FindByValue(dt.Rows(0)("tintBillType")).Selected = True
                    End If

                    If CCommon.ToShort(dt.Rows(0)("tintScanValue")) = 3 Then
                        rbItemName.Checked = True
                    ElseIf CCommon.ToShort(dt.Rows(0)("tintScanValue")) = 2 Then
                        rbUPC.Checked = True
                    Else
                        rbSKU.Checked = True
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub DisplayError(ByVal errorMessage As String)
            Try
                lblError.Text = errorMessage
                divError.Style.Add("display", "")
            Catch ex As Exception
                'DO NOT THROW ERROR
            End Try
        End Sub

#End Region

#Region "Event Handlers"

        Protected Sub btnSave_Click(sender As Object, e As EventArgs)
            Try
                objContact = New CContacts
                objContact.DomainID = Session("DomainId")
                objContact.UserCntID = Session("UserContactId")
                objContact.FormId = 135

                Dim i As Integer
                Dim str As String()
                Dim dsNew As DataSet
                Dim dtTable As DataTable
                Dim dr As DataRow

                If hdnReadytoReceive.Value <> "-1" Then
                    dsNew = New DataSet()
                    dtTable = New DataTable()
                    dtTable.Columns.Add("numFieldID")
                    dtTable.Columns.Add("bitCustom")
                    dtTable.Columns.Add("tintOrder")
                    dtTable.TableName = "Table"

                    str = hdnReadytoReceive.Value.Split(",")
                    For i = 0 To str.Length - 1
                        dr = dtTable.NewRow
                        dr("numFieldID") = str(i).Split("~")(0)
                        dr("bitCustom") = str(i).Split("~")(1)
                        dr("tintOrder") = i
                        dtTable.Rows.Add(dr)
                    Next
                    dsNew.Tables.Add(dtTable)

                    objContact.ViewID = 1
                    objContact.strXml = dsNew.GetXml
                    objContact.SaveContactColumnConfiguration()
                End If

                If hdnReadytoPutAway.Value <> "-1" Then
                    dsNew = New DataSet()
                    dtTable = New DataTable()
                    dtTable.Columns.Add("numFieldID")
                    dtTable.Columns.Add("bitCustom")
                    dtTable.Columns.Add("tintOrder")
                    dtTable.TableName = "Table"

                    str = hdnReadytoPutAway.Value.Split(",")
                    For i = 0 To str.Length - 1
                        dr = dtTable.NewRow
                        dr("numFieldID") = str(i).Split("~")(0)
                        dr("bitCustom") = str(i).Split("~")(1)
                        dr("tintOrder") = i
                        dtTable.Rows.Add(dr)
                    Next
                    dsNew.Tables.Add(dtTable)

                    objContact.ViewID = 2
                    objContact.strXml = dsNew.GetXml
                    objContact.SaveContactColumnConfiguration()
                End If

                If hdnReadytoBill.Value <> "-1" Then
                    dsNew = New DataSet()
                    dtTable = New DataTable()
                    dtTable.Columns.Add("numFieldID")
                    dtTable.Columns.Add("bitCustom")
                    dtTable.Columns.Add("tintOrder")
                    dtTable.TableName = "Table"

                    str = hdnReadytoBill.Value.Split(",")
                    For i = 0 To str.Length - 1
                        dr = dtTable.NewRow
                        dr("numFieldID") = str(i).Split("~")(0)
                        dr("bitCustom") = str(i).Split("~")(1)
                        dr("tintOrder") = i
                        dtTable.Rows.Add(dr)
                    Next
                    dsNew.Tables.Add(dtTable)

                    objContact.ViewID = 3
                    objContact.strXml = dsNew.GetXml
                    objContact.SaveContactColumnConfiguration()
                End If

                If hdnPendingClose.Value <> "-1" Then
                    dsNew = New DataSet()
                    dtTable = New DataTable()
                    dtTable.Columns.Add("numFieldID")
                    dtTable.Columns.Add("bitCustom")
                    dtTable.Columns.Add("tintOrder")
                    dtTable.TableName = "Table"

                    str = hdnPendingClose.Value.Split(",")
                    For i = 0 To str.Length - 1
                        dr = dtTable.NewRow
                        dr("numFieldID") = str(i).Split("~")(0)
                        dr("bitCustom") = str(i).Split("~")(1)
                        dr("tintOrder") = i
                        dtTable.Rows.Add(dr)
                    Next
                    dsNew.Tables.Add(dtTable)

                    objContact.ViewID = 4
                    objContact.strXml = dsNew.GetXml
                    objContact.SaveContactColumnConfiguration()
                End If

                If hdnReadytoPickRight.Value <> "-1" Then
                    dsNew = New DataSet()
                    dtTable = New DataTable()
                    dtTable.Columns.Add("numFieldID")
                    dtTable.Columns.Add("bitCustom")
                    dtTable.Columns.Add("tintOrder")
                    dtTable.TableName = "Table"

                    str = hdnReadytoPickRight.Value.Split(",")
                    For i = 0 To str.Length - 1
                        dr = dtTable.NewRow
                        dr("numFieldID") = str(i).Split("~")(0)
                        dr("bitCustom") = str(i).Split("~")(1)
                        dr("tintOrder") = i
                        dtTable.Rows.Add(dr)
                    Next
                    dsNew.Tables.Add(dtTable)

                    objContact.FormId = 143
                    objContact.ViewID = 1
                    objContact.strXml = dsNew.GetXml
                    objContact.SaveContactColumnConfiguration()
                End If

                objMassPurchaseFulfillmentConfiguration = New MassPurchaseFulfillmentConfiguration
                objMassPurchaseFulfillmentConfiguration.DomainID = CCommon.ToLong(Session("DomainID"))
                objMassPurchaseFulfillmentConfiguration.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objMassPurchaseFulfillmentConfiguration.IsGroupByOrderForReceive = If(ddlWhen.SelectedValue = "1", chkGroupByOrder.Checked, CCommon.ToBool(hdnGroupByOrderForReceive.Value))
                objMassPurchaseFulfillmentConfiguration.IsGroupByOrderForPutAway = If(ddlWhen.SelectedValue = "2", chkGroupByOrder.Checked, CCommon.ToBool(hdnGroupByOrderForPutAway.Value))
                objMassPurchaseFulfillmentConfiguration.IsGroupByOrderForBill = If(ddlWhen.SelectedValue = "3", chkGroupByOrder.Checked, CCommon.ToBool(hdnGroupByOrderForBill.Value))
                objMassPurchaseFulfillmentConfiguration.IsGroupByOrderForClose = False
                objMassPurchaseFulfillmentConfiguration.IsShowOnlyFullyReceived = chkShowOnlyFullyReceived.Checked
                objMassPurchaseFulfillmentConfiguration.IsReceiveBillOnClose = chkReceiveBillOnClose.Checked
                objMassPurchaseFulfillmentConfiguration.BillType = CCommon.ToShort(ddlBillType.SelectedValue)
                If rbItemName.Checked Then
                    objMassPurchaseFulfillmentConfiguration.ScanValue = 3
                ElseIf rbUPC.Checked Then
                    objMassPurchaseFulfillmentConfiguration.ScanValue = 2
                Else
                    objMassPurchaseFulfillmentConfiguration.ScanValue = 1
                End If

                objMassPurchaseFulfillmentConfiguration.Save()

                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "RefreshParentAndClose", "RefreshParentAndClose();", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub ddlWhen_SelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                BindData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

#End Region

    End Class

End Namespace

