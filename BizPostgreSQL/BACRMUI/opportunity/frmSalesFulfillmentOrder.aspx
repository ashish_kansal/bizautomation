﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmSalesFulfillmentOrder.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmSalesFulfillmentOrder" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Sales Fulfillment</title>
    <style type="text/css">
        .web_dialog_overlay {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            background: #000000;
            opacity: .15;
            filter: alpha(opacity=15);
            -moz-opacity: .15;
            z-index: 101;
            display: none;
        }

        .web_dialog {
            display: none;
            position: fixed;
            width: 600px;
            height: 300px;
            top: 50%;
            left: 50%;
            margin-left: -250px;
            margin-top: -250px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            overflow: scroll;
        }

        .web_dialog_title {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight: bold;
        }

            .web_dialog_title a {
                color: White;
                text-decoration: none;
            }

        .align_right {
            text-align: right;
        }

        .rcbInput {
            height: 19px !important;
        }

        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }

        .dialog-header {
            position: relative;
            text-indent: 1em;
            height: 4em;
            line-height: 4em;
            background-color: #f7f7f7;
        }

        .dialog-body {
            padding: .5em;
            word-break: break-all;
            overflow-y: auto;
        }
    </style>
    <script type="text/javascript">
        //setTimeout(function () {
        //    window.location.reload(1);
        //}, 60000);
        function OpenSetting() {
            window.open('../Items/frmConfItemList.aspx?FormID=23', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function pageLoaded() {
            var chkBox1 = $("[id$=chkSelectAll]");

            $(chkBox1).click(function () {
                $("[id$=gvSearch] INPUT[type='checkbox']").each(function () {
                    if (this.id.indexOf("chkSelect") >= 0) {
                        $(this).prop('checked', chkBox1.is(':checked'))
                    }
                })
            });

            $("[id$=chkOrderStatusAll]").click(function () {
                var Combo = $find("<%= radOrderStatus.ClientID%>");
                var items = Combo.get_items();

                for (var x = 0; x < Combo.get_items().get_count() ; x++) {
                    if ($(this).is(':checked'))
                        items._array[x].check();
                    else
                        items._array[x].uncheck();
                }
            });

            $("[id$=chkFilterValueAll]").click(function () {
                var Combo = $find("<%= radFilterValue.ClientID%>");
                var items = Combo.get_items();

                for (var x = 0; x < Combo.get_items().get_count() ; x++) {
                    if ($(this).is(':checked'))
                        items._array[x].check();
                    else
                        items._array[x].uncheck();
                }
            });

            $("[id$=btnClose]").click(function (e) {
                HideDialog();
                e.preventDefault();
            });

            $("[id$=btnGoBatch]").click(function () {
                return GetRecords();
            });

            $("[id$=imgBtnAddShippingItem]").click(function () {
                return SelectRecords();
            });

            $("[id$=btnCalculateShipping]").click(function () {
                return SelectRecords();
            });
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function SelectRecords() {
            var OppId = '';
            var i;
            i = 0;

            $("[id$=gvSearch] tr").not(':first').each(function () {
                chk = $(this).find("[id*='chkSelect']");
                if (chk != null && $(chk).is(':checked')) {
                    OppId = OppId + ',' + $(this).find("[id$='txtoppID']").val();
                    i = i + 1;
                }
            });

            if (i > 0) {
                document.getElementById('txtSelOppId').value = OppId;
                return true;
            }
            else {
                alert("Please select atleast one record.");
                return false;
            }

        }

        function GetRecords() {
            var OppId = '';
            var BizDocsId = '';

            $("[id$=gvSearch] tr").not(':first').each(function () {
                chk = $(this).find("[id*='chkSelect']");
                if (chk != null && $(chk).is(':checked')) {
                    OppId = OppId + ',' + $(this).find("[id$='txtoppID']").val();
                    BizDocsId = BizDocsId + ',' + $(this).find("[id$='txtBizDocID']").val();
                }
            });

            document.getElementById('txtSelOppId').value = OppId;
            document.getElementById('txtSelBizDocsId').value = OppId;

            return true;
        }

        function ShowDialog(modal) {
            $("[id$=overlay]").show();
            $("[id$=dialog]").fadeIn(300);

            if (modal) {
                $("[id$=overlay]").unbind("click");
            }
            else {
                $("[id$=overlay]").click(function (e) {
                    HideDialog();
                });
            }
        }

        function HideDialog() {
            $("[id$=overlay]").hide();
            $("[id$=dialog]").fadeOut(300);
            __doPostBack('btnReloadGrid', '');
        }

        function UpdateBizDocStatus(OppID, BizDocID, BizDocStatusval, oldBizDocStatus, iRuleID, vcBizDocID) {
            $.ajax({
                type: "POST",
                url: "../common/Common.asmx/UpdateOrderStatus",
                data: "{lngOppID:'" + OppID + "',lngBizDocStatus:'" + BizDocStatusval + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    // Insert the returned HTML into the <div>.
                    UpdateMsgStatus(BizDocID, vcBizDocID, msg.d);
                }
            });
        }

        function CancelBizDocStatus(OppID, BizDocID, BizDocStatusval, oldBizDocStatus, iRuleID, vcBizDocID) {
            UpdateMsgStatus(BizDocID, vcBizDocID, 'Cancel Action');
        }

        function UpdateMsgStatus(BizDocID, vcBizDocID, message) {
            var liBizDocID = $('.divMessage').find("[id*='" + BizDocID + "']");
            $(liBizDocID).text(vcBizDocID + ' : ' + message);

            if (message.toLowerCase().indexOf("error") >= 0)
                $(liBizDocID).css("color", "red");
            else
                $(liBizDocID).css("color", "green");
        }

        function OpenBizInvoice(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
        }

        function OpenOpp(a) {
            var str;
            str = "../opportunity/frmOpportunities.aspx?frm=SalesFulfillmentList&OpID=" + a;
            document.location.href = str;
        }

        function OpenEdit(a, b) {
            window.location.href = '../opportunity/frmAddBizDocs.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=SalesFulfillment&OpID=' + a + '&OppBizId=' + b + '&OppType=1';
            return false;
        }

        function OpenMirrorBizDoc(a, b) {
            window.open('../opportunity/frmMirrorBizDoc.aspx?RefID=' + a + '&RefType=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=1000,height=600,scrollbars=yes,resizable=yes');
        }

        function OpenCustomer(a, b) {
            var str;
            if (b == 0) {
                str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylistDivID=" + a;
            }
            else if (b == 1) {
                str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylist&DivID=" + a;
            }
            else if (b == 2) {
                str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylist&klds+7kldf=fjk-las&DivId=" + a;
            }

            document.location.href = str;
        }

        function ExpandCollapseGrid() {
            var Grid = $find("<%= gvSearch.ClientID%>");
            var MasterTable = Grid.get_masterTableView();

            var expanded = $('[id$=chkExpandGrid]').is(':checked');

            for (var i = 0; i < MasterTable.get_dataItems().length; i++) {
                var row = MasterTable.get_dataItems()[i];
                row.set_expanded(expanded);
            }
        }

        function OpenConfSerItem(a, b, c, d, e, f) {
            window.open('../opportunity/frmAddSerializedItem.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID=' + a + '&ItemCode=' + b + '&OppType=' + c + '&OppBizDocId=' + d + '&Qty=' + e + '&LotId=' + f, '', 'toolbar=no,titlebar=no,left=100,top=100,width=530,height=400,scrollbars=yes,resizable=yes')
            return false;
        }

        function openReportList(a, b, c) {
            if (c > 0) {
                window.open('../opportunity/frmShippingReport.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppBizDocId=' + b + '&ShipReportId=' + c + '&OppId=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=800,height=300,scrollbars=yes,resizable=yes');
                return false;
            }
            else {
                window.open('../opportunity/frmShippingReportList.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=' + a + '&OppBizDocId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=680,height=250,scrollbars=yes,resizable=yes');
                return false;
            }
        }

        function openShippingLabel(a, b, c, d, e) {
            if (c > 0) {
                window.open('../opportunity/frmShippingReport.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppBizDocId=' + b + '&ShipReportId=' + c + '&OppId=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=800,height=300,scrollbars=yes,resizable=yes');
                return false;
            }
            else {
                window.open('../opportunity/frmShippingBox.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=' + a + '&OppBizDocId=' + b + '&ShipCompID=' + d + '&ShipServiceID=' + e, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=1000,height=350,scrollbars=yes,resizable=yes');
                return false;
            }
        }

        function ConfirmShippingItem() {
            if (confirm('This will add selected shipping charges to the “Products & Services” section”.Do you want to proceed ?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function OpenPrintedShippingLabels(a) {
            window.open('../opportunity/frmShippingLabelImages.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&BoxID=0&ServiceType=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=800,height=300,scrollbars=yes,resizable=yes');
        }

        function OpenAutomationExecutionLog(a) {
            window.open("../opportunity/frmOpportunityAutomationQueueExecutionLog.aspx?OppID=" + a, '', 'toolbar=no,titlebar=no,left=200, top=300,width=1024,height=350,scrollbars=yes,resizable=yes')
            return false;
        }

        function SetCursorToTextEnd(textControlID) {
            var text = document.getElementById(textControlID);
            if (text != null && text.value.length > 0) {
                if (text.createTextRange) {
                    var FieldRange = text.createTextRange();
                    FieldRange.moveStart('character', text.value.length);
                    FieldRange.collapse();
                    FieldRange.select();
                }
                else if (text.setSelectionRange) {
                    var textLength = text.value.length;
                    text.setSelectionRange(textLength, textLength);
                }
            }
        }

        function CloseWarehousePopup() {
            $("[id$=hdnShippingLabelFile]").val("");
            $("[id$=divWarehouse]").css("display", "none");
            return false;
        }

        function ConfirmAction() {
            if (confirm("You’re about to release allocation for the ordered quantities for items within the Fulfillment Orders selected. Are you sure you want to do this?")) {
                return true;
            } else {
                return false;
            }
        }


    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <div class="overlay" id="divWarehouse" runat="server" style="display: none">
        <div class="overlayContent" style="color: #000; background-color: #FFF; width: 350px; border: 1px solid #ddd; margin: 100px auto !important;">
            <div class="dialog-header">
                <b style="font-size: 14px;">Select Warehouse to Print</b>
                <div style="float: right; padding-right: 7px;">
                    <asp:Button ID="btnClosePriceHistory" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="Close Window" OnClientClick="return CloseWarehousePopup();" />
                </div>
            </div>
            <div class="dialog-body" style="max-height: 600px; width: 100%;">
                <div class="form-group">
                    <label>Warehouse:</label>
                    <telerik:RadComboBox runat="server" ID="radcmbWarehouses" Width="200" />
                    <asp:Button ID="btnPrintLabels" runat="server" Text="Print" CssClass="btn btn-primary" />
                </div>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label>Expand Grid</label>
                        <asp:CheckBox ID="chkExpandGrid" runat="server" Text="" AutoPostBack="true" />
                    </div>
                    &nbsp;&nbsp;
                    <div class="form-group">
                        <label>Actions:</label>
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional" style="display: inline;">
                            <ContentTemplate>
                                <asp:DropDownList runat="server" ID="ddlBatchAction" Width="170" CssClass="form-control">
                                    <asp:ListItem Text="Print Pick List" Value="1" />
                                    <asp:ListItem Text="Print Packing Slip" Value="2" />
                                    <asp:ListItem Text="Print Shipping Label" Value="3" />
                                    <asp:ListItem Text="Print Group List" Value="5" />
                                    <asp:ListItem Text="Ship Orders (Add FOs)" Value="6" />
                                </asp:DropDownList>
                                <asp:Button ID="btnGoBatch" CssClass="btn btn-sm btn-primary" runat="server" Text="Go"></asp:Button>
                            </ContentTemplate>
                            <Triggers>
                                <asp:PostBackTrigger ControlID="btnGoBatch" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>

                </div>
            </div>
            <div class="pull-right">
                <div class="form-inline">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional" class="form-group">
                        <ContentTemplate>
                            <ul class="list-inline">
                                <li>
                                    <asp:DropDownList ID="ddlFilterBy" CssClass="form-control" runat="server" AutoPostBack="true">
                                        <asp:ListItem Text="-- Filter By --" Value="0"></asp:ListItem>
                                        <asp:ListItem Text="Item Classification" Value="1"></asp:ListItem>
                                    </asp:DropDownList></li>
                                <li>
                                    <telerik:RadComboBox ID="radFilterValue" Skin="Default" runat="server" CheckBoxes="true" Width="150px" DropDownWidth="250px">
                                        <FooterTemplate>
                                            <asp:CheckBox ID="chkFilterValueAll" runat="server" Text="Select All" />&nbsp;
                                            <asp:Button ID="btnFilterValue" CssClass="button" runat="server" Text="Search" OnClick="btnFilterValue_Click" UseSubmitBehavior="false"></asp:Button>
                                        </FooterTemplate>
                                    </telerik:RadComboBox>
                                </li>
                            </ul>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="form-group">
                        <label>Order Status:</label>
                        <telerik:RadComboBox ID="radOrderStatus"
                            Skin="Default" runat="server" CheckBoxes="true" Width="150px" DropDownWidth="250px">
                            <FooterTemplate>
                                <asp:CheckBox ID="chkOrderStatusAll" runat="server" Text="Select All" />&nbsp;
                                <asp:Button ID="btnOrderStatus" CssClass="button" runat="server" Text="Search" OnClick="btnOrderStatus_Click" UseSubmitBehavior="false"></asp:Button>
                            </FooterTemplate>
                        </telerik:RadComboBox>
                    </div>
                    <div class="form-group">
                        <label>Shipping Zone:</label>
                        <telerik:RadComboBox runat="server" ID="radShippingZone" Skin="Default" AutoPostBack="true" Height="130px" Width="180" DropDownWidth="180">
                        </telerik:RadComboBox>
                    </div>
                    &nbsp;&nbsp;
                    <div class="form-group" id="PanelShipping" runat="server">
                        <label>Set FO Ship Date:</label>
                        <telerik:RadDatePicker ID="radDtShipped" runat="server" DateInput-ReadOnly="true" Width="120" DateInput-DateFormat="MM/dd/yyyy"></telerik:RadDatePicker>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="alert alert-warning" id="divAlert" runat="server" visible="false">
        <h4><i class="icon fa fa-warning"></i>Alert!</h4>
        <asp:Label ID="lblException" runat="server" Text=""></asp:Label>
    </div>
    <div id="overlay" class="web_dialog_overlay"></div>
    <div id="dialog" class="web_dialog">
        <table style="width: 100%; border: 0px;" cellpadding="3" cellspacing="0">
            <tr>
                <td class="web_dialog_title">Fulfillment Action</td>
                <td class="web_dialog_title align_right">
                    <a href="#" id="btnClose">Close</a>
                </td>
            </tr>
            <tr>
                <td colspan="2" style="padding-left: 15px;">
                    <div class="divMessage"></div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Sales Fulfillment
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
    <asp:LinkButton ID="lkbClearFilter" runat="server" OnClick="lkbClearFilter_Click" class="btn-box-tool" ToolTip="Clear All Filters"><i class="fa fa-2x fa-filter"></i></asp:LinkButton>
    <a id="tdGridConfiguration" runat="server" href="#" onclick="return OpenSetting()" title="Show Grid Settings." class="btn-box-tool"><i class="fa fa-2x fa-cog"></i></a>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <div class="table-responsive">
        <telerik:RadGrid ID="gvSearch" runat="server" Width="100%" AutoGenerateColumns="False"
            GridLines="None" ShowFooter="false" Skin="windows" EnableEmbeddedSkins="false">
            <MasterTableView DataKeyNames="numOppID,vcpOppName,numDivisionID" HierarchyLoadMode="Client" CssClass="table table-bordered table-striped">
                <DetailTables>
                    <telerik:GridTableView Width="100%" runat="server" ItemStyle-HorizontalAlign="Center"
                        AlternatingItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                        ShowFooter="false" DataKeyNames="numoppitemtCode,numWarehouseItmsID,vcSKU,numUnitHourOrig,bitSerialized,bitLotNo" CssClass="table table-bordered table-striped">
                        <ParentTableRelation>
                            <telerik:GridRelationFields DetailKeyField="numOppID" MasterKeyField="numOppID" />
                        </ParentTableRelation>
                        <Columns>
                            <telerik:GridTemplateColumn>
                                <ItemTemplate>
                                    <asp:Image ID="imgThumb" runat="server" Width="60" Height="60" />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn HeaderText="Item Release Date" DataField="vcItemReleaseDate">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Item" DataField="vcItemName" UniqueName="vcItemName">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="SKU" DataField="vcSKU">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Attributes" DataField="vcAttributes">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Warehouse" DataField="Warehouse">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Location" DataField="vcLocation">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Qty" DataField="numUnitHourOrig" DataFormatString="{0:0.##############}" UniqueName="numUnitHourOrig">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="DropShip" DataField="DropShip">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Serial/Lot #s" UniqueName="SerialLot" ItemStyle-Width="150px">
                                <ItemTemplate>
                                    <div style="white-space: nowrap">
                                        <asp:TextBox ID="txLot" runat="server" CssClass="signup" Visible="false" AutoPostBack="true" OnTextChanged="txLot_TextChanged"></asp:TextBox>&nbsp;
                                                <asp:HyperLink ID="hlSerialized" runat="server" CssClass="BizLink" Visible="false" TabIndex="999">Add</asp:HyperLink>&nbsp;
                                                <asp:ImageButton ID="imgClearSerial" runat="server" ImageUrl="~/images/filter.png" Visible="false" Height="13" Width="13" OnClick="imgClearSerial_Click" />
                                    </div>
                                    <asp:Label ID="lblSelectedLot" runat="server" Text='<%# Eval("SerialLotNo") %>'></asp:Label>
                                    <asp:HiddenField ID="hfbitSerialized" runat="server" Value='<%# Eval("bitSerialized") %>' />
                                    <asp:HiddenField ID="hfbitLotNo" runat="server" Value='<%# Eval("bitLotNo") %>' />
                                    <asp:HiddenField ID="hfnumWarehouseItmsID" runat="server" Value='<%# Eval("numWarehouseItmsID") %>' />
                                    <asp:HiddenField ID="hfOppItemCode" runat="server" Value='<%# Eval("numoppitemtCode")%>' />


                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </telerik:GridTableView>
                </DetailTables>
                <Columns>
                    <%-- <telerik:GridBoundColumn HeaderText="Order Release Date" ItemStyle-Width="136" DataField="dtFromDate" ItemStyle-Wrap="false">
                    </telerik:GridBoundColumn>
                    <telerik:GridTemplateColumn HeaderText="FO" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <a href="javascript:void(0);" onclick="OpenBizInvoice('<%# Eval("numOppId") %>','<%# Eval("numoppbizdocsid") %>');">
                                <%# Eval("vcBizDocID")%>
                            </a>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Priced/Boxed/Tracked" HeaderStyle-Width="10%" HeaderStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:ImageButton runat="server" Style="vertical-align: middle" ID="imgShippingReport" CommandName="OpenLog" CommandArgument='<%# Eval("numOppId") %>' ImageUrl="~/images/TruckGreen.png" Height="28" />&nbsp;
                            <asp:Literal runat="server" ID="litShipping" />
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn HeaderText="Order / Status" ItemStyle-Width="250px" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <a href="javascript:void(0);" onclick="OpenOpp('<%# Eval("numOppId") %>');"><%# Eval("vcPOppName") %></a>&nbsp;<%# Eval("vcInventoryStatus") %>/&nbsp;
                                    <%# Eval("vcOrderStatus")%>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>

                    <telerik:GridTemplateColumn HeaderStyle-Width="5px">
                        <ItemTemplate>
                            <a href="javascript:void(0);" onclick="OpenAutomationExecutionLog('<%# Eval("numOppId") %>');" style="text-decoration: none;">
                                <img src="../images/GLReport.png" border="0" alt="Automation Execution Log" title="Automation Execution Log" />
                            </a>
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>
                    <telerik:GridTemplateColumn ItemStyle-Width="5px" ItemStyle-Wrap="false">
                        <HeaderTemplate>
                            <asp:CheckBox ID="chkSelectAll" runat="server" />
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="chkSelect" runat="server" />
                            
                        </ItemTemplate>
                    </telerik:GridTemplateColumn>--%>
                </Columns>
            </MasterTableView>
            <ClientSettings AllowExpandCollapse="true" />
        </telerik:RadGrid>
    </div>
    <asp:TextBox ID="txtDelItemIds" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSaveAPIItemIds" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPageItems" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecordsItems" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" Style="display: none" runat="server"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSelOppId" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSelBizDocsId" runat="server" Style="display: none"></asp:TextBox>
    <asp:HiddenField ID="hdnFromSalesOrderID" runat="server" />
    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>

    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
