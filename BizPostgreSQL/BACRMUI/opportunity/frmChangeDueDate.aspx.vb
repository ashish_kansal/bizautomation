﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Accounting

Partial Public Class frmChangeDueDate
    Inherits BACRMPage
    Dim lngOppBizDocID, lngOppId As Long
    Dim strDate As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            lngOppBizDocID = GetQueryStringVal("OppBizId")
            lngOppId = GetQueryStringVal("OppId")
            strDate = GetQueryStringVal("date")
            litScript.Text = GetCalanderScript()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Public Function GetCalanderScript() As String
        Try
            Dim strCalanderScript As New System.Text.StringBuilder
            Dim strFromat As String = CCommon.GetDateFormat()
            strCalanderScript.Append("<script>")
            strCalanderScript.Append("var cal=  Calendar.setup({")
            strCalanderScript.Append("cont: 'calendar-container',")
            strCalanderScript.Append("dateFormat:  '" & strFromat & "',")
            strCalanderScript.Append("selection:  '" & strDate & "',")
            strCalanderScript.Append("onSelect: function() {")
            'strCalanderScript.Append("$('calendar-info').innerHTML ='Selected date is ' + cal.selection.print('" & strFromat & "');")
            strCalanderScript.Append("$('hdnDate').value =  cal.selection.print('" & strFromat & "');")
            strCalanderScript.Append("$('btnSave').click();")
            strCalanderScript.Append("}")
            strCalanderScript.Append("});")
            'strCalanderScript.Append("cal.selection.set(" & strDate & ");")
            strCalanderScript.Append("</script>")
            Return strCalanderScript.ToString
        Catch ex As Exception
            Throw ex
        End Try
    End Function


    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If lngOppBizDocID > 0 And hdnDate.Value <> "" Then
                Dim objOppBizDoc As New OppBizDocs
                objOppBizDoc.DueDate = CDate(DateFromFormattedDate(hdnDate.Value, Session("DateFormat")) & " 23:59:59")
                objOppBizDoc.OppBizDocId = lngOppBizDocID
                objOppBizDoc.OppId = lngOppId
                objOppBizDoc.UpdateDueDate()

                ''Create Journals only for authoritative bizdocs
                ''-------------------------------------------------
                'Dim objOppBizDocs As New OppBizDocs
                'Dim lintAuthorizativeBizDocsId As Long
                'objOppBizDocs.DomainID = Session("DomainID")
                'objOppBizDocs.OppId = lngOppId
                'lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()

                'Dim dtOppBiDocDtl As DataTable
                'objOppBizDocs.OppBizDocId = lngOppBizDocID
                'objOppBizDocs.OppId = lngOppId
                'objOppBizDocs.DomainID = Session("DomainID")
                'objOppBizDocs.UserCntID = Session("UserContactID")
                'objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                'dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl

                'If lintAuthorizativeBizDocsId = dtOppBiDocDtl.Rows(0).Item("numBizDocId") Then

                '    Dim lngDivID, JournalId As Long

                '    Dim ds As New DataSet
                '    Dim dtOppBiDocItems As DataTable

                '    objOppBizDocs.OppBizDocId = lngOppBizDocID
                '    ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                '    dtOppBiDocItems = ds.Tables(0)

                '    Dim objCalculateDealAmount As New CalculateDealAmount

                '    objCalculateDealAmount.CalculateDealAmount(lngOppId, lngOppBizDocID, dtOppBiDocDtl.Rows(0).Item("tintOppType"), Session("DomainID"), dtOppBiDocItems)

                '    lngDivID = CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numDivisionID"))
                '    ''---------------------------------------------------------------------------------
                '    JournalId = objOppBizDocs.GetJournalIdForAuthorizativeBizDocs
                '    'When From COA someone deleted journals then we need to create new journal header entry
                '    If JournalId = 0 Then
                '        JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, Entry_Date:=CDate(dtOppBiDocDtl.Rows(0).Item("dtFromDate")))
                '    Else
                '        JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, lngJournalId:=JournalId, Entry_Date:=CDate(dtOppBiDocDtl.Rows(0).Item("dtFromDate")))
                '    End If

                '    Dim objJournalEntries As New JournalEntry

                '    If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 2 Then
                '        objJournalEntries.SaveJournalEntriesPurchase(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, lngOppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"))
                '    ElseIf dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                '        objJournalEntries.SaveJournalEntriesSales(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, lngOppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), 0)
                '    End If
                'End If

                Response.Write("<script>opener.location.reload(true); self.close();</script>")
            Else
                Response.Write("<script>alert('Please select a due date');</script>")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class