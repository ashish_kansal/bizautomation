<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmCreateSalesPurFromOpp.aspx.vb"
    Inherits=".frmCreateSalesPurFromOpp" MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script type="text/javascript">
        function validateOrganization() {
            if ($find('radCmbCompany').get_value() == '') {
                alert("Select Customer")
                return false;
            }
            if ((document.getElementById("ddlContact").selectedIndex == -1) || (document.getElementById("ddlContact").value == 0)) {
                alert("Select Contact")
                document.getElementById("ddlContact").focus();
                return false;
            }

            if ($("#radBillOther").is(":checked") && $find('radCmbBillOther').get_value() == '') {
                alert("Select bill other location customer.")
                return false;
            }

            if ($("#radShipOther").is(":checked") && $find('radCmbShipOther').get_value() == '') {
                alert("Select ship other location customer.");
                return false;
            }
        }

        function pageLoaded() {
            $('[id$=btnSave]').click(function (e) {
                var i = 0;
                $('[id$=dgItem] tr').not(".hs,.fs").each(function () {
                    if ($(this).find("input[id*='chkSelect']").is(':checked')) {
                        i += 1;

                        if ($(this).find("input[id*='txtPrice']").val() == "") {
                            alert("Enter Price")
                            $(this).find("input[id*='txtPrice']").focus();
                            e.preventDefault();
                            return false;

                        }
                        if ($(this).find("input[id*='txtUnits']").val() == "") {
                            alert("Enter Units")
                            $(this).find("input[id*='txtUnits']").focus();
                            e.preventDefault();
                            return false;
                        }

                        if ($("[id$=cbPreferredVendor]").length > 0 && $("[id$=cbPreferredVendor]").is(':checked') == false) {
                            var ddlVendor = $(this).find("[id$=ddlVendor]");
                            if ($(ddlVendor).length > 0) {
                                if ($(ddlVendor).val() == null) {
                                    console.log($(ddlVendor).val());
                                    alert("Select Vendor")
                                    $(ddlVendor).focus();
                                    e.preventDefault();
                                    return false;
                                }
                            }
                        }
                    }
                });

                if (i == 0) {
                    alert('Please Select At least one item for Create PO from SO.');
                    e.preventDefault();
                }

                if (document.getElementById('cbPreferredVendor') != null) {
                    if (document.getElementById('cbPreferredVendor').checked == true) {
                        return validateOrganization();
                    }
                }
                else {
                    return validateOrganization();
                }
            });

            $('.btn-match-po').click(function () {
                try {
                    $("[id$=UpdateProgressPOSO]").show();

                    $("#divMatchPO #tblPO tr").not(":first").each(function (index, e) {
                        if ($(e).find(".ddlPO option:selected").length > 0) {
                            var rowIndex = parseInt($(e).find(".hdnIndex").val());
                            var row = $("[id$=dgItem] > tbody > tr").eq(rowIndex + 1);

                            if (row != null && parseInt($(e).find(".ddlPO option:selected").attr("po")) > 0) {
                                $(row).find("[id$=divPO]").html("<a href='../opportunity/frmOpportunities.aspx?frm=deallist&amp;OpID=" + parseInt($(e).find(".ddlPO option:selected").attr("po")).toString() + "'>" + $(e).find(".ddlPO option:selected").text() + "</a>");
                                $(row).find("[id$=hdnPOName]").val($(e).find(".ddlPO option:selected").text());
                                $(row).find("[id$=hdnPOID]").val(parseInt($(e).find(".ddlPO option:selected").attr("po")));
                                $(row).find("[id$=hdnPOItemID]").val(parseInt($(e).find(".ddlPO option:selected").val()));
                            }
                        }
                    });

                    $("#divMatchPO").modal("hide");
                    $("[id$=UpdateProgressPOSO]").hide();
                } catch (e) {
                    $("[id$=UpdateProgressPOSO]").hide();
                    alert('Unknown error occurred.');
                }
            });

            $("[id$=chkShowOnlyBOItems]").change(function () {
                try {
                    $("[id$=dgItem] > tbody > tr").not(":first").each(function (index, e) {
                        if ($("[id$=chkShowOnlyBOItems]").is(":checked")) {
                            if (parseFloat($(this).find("[id$=hdnOrigUnitHour]").val()) > parseFloat($(this).find("[id$=hdnOnHand]").val())) {
                                $(this).find("[id$=chkSelect]").prop("checked", true);
                            } else {
                                $(this).find("[id$=chkSelect]").prop("checked", false);
                            }
                        } else {
                            $(this).find("[id$=chkSelect]").prop("checked", true);
                        }
                    });
                } catch (e) {
                    alert('Unknown error occurred.');
                }
            });

            try {
                $("[id$=dgItem] > tbody > tr").not(":first").each(function (index, e) {
                    if (parseInt($(e).closest("tr").find("[id$=hdnPOID]").val()) > 0) {
                        $(e).closest("tr").find("[id$=divPO]").html("<a href='../opportunity/frmOpportunities.aspx?frm=deallist&amp;OpID=" + parseInt($(e).closest("tr").find("[id$=hdnPOID]").val()).toString() + "'>" + $(e).closest("tr").find("[id$=hdnPOName]").val() + "</a>");
                    }

                    if ($(e).find("input[id='chkDropship']").is(":checked")) {
                        $(e).closest("tr").find("[id$=lblShipToAddress]").show();
                        $(e).closest("tr").find("[id$=lblShipToAddress]").html($(e).closest("tr").find("[id$=hdnShipToAddress]").val().replace(/&amp;/g, "&").replace(/&lt;/g, "<").replace(/&gt;/g, ">"));
                        $(e).closest("tr").find("[id$=ddlShipToWarehouse]").hide();
                    } else {
                        $(e).closest("tr").find("[id$=ddlShipToWarehouse]").show();
                        $(e).closest("tr").find("[id$=lblShipToAddress]").hide();
                    }
                });
            } catch (e) {

            }

            if ($("[id$=chkUseSingleShipAddress]").is(":checked")) {
                $("[id$=divShippingAddress]").show();
            }
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function OpenOpp(a) {
            var url;
            url = "../opportunity/frmOpportunities.aspx?frm=deallist&OpID=" + a;
            window.location.href = url;
        }

        function ChangeVendorCost(a, b) {
            document.getElementById(a).value = document.getElementById(b).value.split("~")[2];
        }

        //http://www.cambiaresearch.com/c4/702b8cd1-e5b0-42e6-83ac-25f0306e3e25/Javascript-Char-Codes-Key-Codes.aspx
        function CheckNumber(cint, evt) {
            var k;
            k = (evt.which) ? evt.which : event.keyCode;
            if (cint == 1) {
                if (!((k > 47 && k < 58) || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16 || k == 9 || k == 110 || k == 190 || (k > 95 && k < 106))) {
                    if (evt.preventDefault) {
                        evt.preventDefault();
                    }
                    else
                        evt.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!((k > 47 && k < 58) || k == 8 || k == 46 || k == 37 || k == 39 || k == 16 || k == 9 || (k > 95 && k < 106))) {
                    if (evt.preventDefault) {
                        evt.preventDefault();
                    }
                    else
                        evt.returnValue = false;
                    return false;
                }
            }
        }

        function VendorChecked(status) {
            if (status == true) {
                $("[id$=divVendor1]").show();
                $("[id$=divVendor2]").show();
                $("[id$=divVendor3]").show();
                alert('Selecting vendor will populate items price will be recalculated as per vendor pricebook rule. if any');
            } else {
                $("[id$=divVendor1]").hide();
                $("[id$=divVendor2]").hide();
                $("[id$=divVendor3]").hide();
            }
        }

        function onRadComboBoxLoad(sender) {
            var div = sender.get_element();

            $telerik.$(div).bind('mouseenter', function () {
                if (!sender.get_dropDownVisible())
                    sender.showDropDown();
            });
        }

        function SelectAll(headerCheckBox, ItemCheckboxClass) {
            $("." + ItemCheckboxClass + " input[type = 'checkbox']").each(function () {
                if (!$(this).is(':disabled')) {
                    $(this).prop('checked', $('[id$=' + headerCheckBox + ']').is(':checked'));
                }
            });
        }

        function Close() {
            window.history.back();

            return false;
        }

        function OpenItem(a) {
            str = "../Items/frmKitDetails.aspx?ItemCode=" + a;
            var win = window.open(str, '_blank');
            if (win) {
                win.focus();
            } else {
                //Browser has blocked it
                alert('Please allow popups for this website');
            }
        }

        function UOMSelectionChanged(ddlUOM) {
            try {
                var selectedUOM = $(ddlUOM).val();
                var conversionFactor = parseFloat(selectedUOM.split("~")[1]);


                if (conversionFactor > 1) {
                    $(ddlUOM).closest("td").find("[id$=lblUOMUnits]").text((parseFloat($(ddlUOM).closest("td").find("[id$=txtUnits]").val()) * conversionFactor).toLocaleString('en-US') + " Units");
                } else {
                    $(ddlUOM).closest("td").find("[id$=lblUOMUnits]").text("");
                }
            } catch (e) {
                alert('Unknown error occurred.');
            }
        }

        function DropshipChanged(chkDropship) {
            try {
                if ($(chkDropship).find("input[id='chkDropship']").is(":checked")) {
                    $(chkDropship).closest("tr").find("[id$=lblShipToAddress]").show();
                    $(chkDropship).closest("tr").find("[id$=ddlShipToWarehouse]").hide();
                } else {
                    $(chkDropship).closest("tr").find("[id$=ddlShipToWarehouse]").show();
                    $(chkDropship).closest("tr").find("[id$=lblShipToAddress]").hide();
                }
            } catch (e) {
                alert('Unknown error occurred.');
            }
        }

        function OpenMatchPOPopup() {
            try {
                $("[id$=UpdateProgressPOSO]").show();
                $("#divMatchPO #tblPO tr").not(":first").remove();

                var items = [];

                $("[id$=dgItem] > tbody > tr").not(":first").each(function (index, e) {
                    var obj = {};
                    obj.Index = index;
                    obj.ItemCode = parseInt($(e).find("[id$=hdnItemCode]").val());
                    obj.Item = $(e).find("[id$=lblItemName]").text();
                    obj.Attributes = $(e).find("[id$=lblAttributes]").text();
                    obj.AttributeValues = $(e).find("[id$=hdnAttrValues]").val();
                    obj.UnitHour = parseFloat($(e).find("[id$=hdnOrigUnitHour]").val() || 0);
                    obj.POs = "";
                    items.push(obj);
                });

                if (items.length > 0) {
                    $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/GetMatchedPO',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "items": JSON.stringify(items)
                        }),
                        success: function (data) {
                            try {
                                var obj = $.parseJSON(data.GetMatchedPOResult);

                                if (obj != null && obj.length > 0) {
                                    obj.forEach(function (e) {
                                        var tr = "<tr>";

                                        if (e.POs != null) {
                                            var arrPOs = JSON.parse(e.POs);

                                            if (arrPOs != null && arrPOs.length > 0) {
                                                tr += "<td><select class='ddlPO form-control' onchange='POChanged(this);'>";
                                                tr += "<option value='0' po='0' vendor='' price=''>Do not match</option>"
                                                arrPOs.forEach(function (item, index) {
                                                    tr += "<option " + (index == 0 ? "selected" : "") + " value='" + (item.numOppItemID || 0) + "' po='" + (item.numOppID || "") + "' vendor='" + (item.Vendor || "") + "' price='" + (item.Price || 0) + "'>" + (item.POName || "") + "</option>";
                                                });
                                                tr += "</select></td>";
                                                tr += "<td class='tdVendor'>" + arrPOs[0].Vendor + "</td>";
                                            } else {
                                                tr += "<td><select class='ddlPO form-control'><option value='0'>No PO Found</option></select></td>";
                                                tr += "<td></td>";
                                            }
                                        } else {
                                            tr += "<td><select class='ddlPO form-control'><option value='0'>No PO Found</option></select></td>";
                                            tr += "<td></td>";
                                        }


                                        tr += "<td>" + (e.Item || "") + ((e.Attributes || "") != "" ? (" " + e.Attributes) : "") + "<input type='hidden' class='hdnIndex' value='" + e.Index + "' /></td>";
                                        tr += "</tr>";

                                        $("#divMatchPO #tblPO").append(tr);
                                    });
                                }

                                $("#divMatchPO").modal("show");
                                $("[id$=UpdateProgressPOSO]").hide();
                            } catch (e) {
                                $("[id$=UpdateProgressPOSO]").hide();
                                alert("Unknown error ocurred");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert("Unknown error ocurred");
                        }
                    });
                } else {
                    $("[id$=UpdateProgressPOSO]").hide();
                    alert('No items to match.');
                }
            } catch (e) {
                $("[id$=UpdateProgressPOSO]").hide();
                alert('Unknown error occurred.');
            }

            return false;
        }

        function POChanged(ddlPO) {
            try {
                $(ddlPO).closest("tr").find(".tdVendor").html($(ddlPO).find("option:selected").attr("vendor"));
            } catch (e) {
                alert('Unknown error occurred.');
            }
        }

        function DisassociatePO() {
            try {
                var oppItemIds = "";

                $("[id$=dgItem] > tbody > tr").not(":first").each(function (index, e) {
                    if ($(e).find("[id$=chkSelect]").is(":checked")) {
                        oppItemIds += ((oppItemIds.length > 0 ? "," : "") + parseInt($(e).find("[id$=hdnOppItemID]").val()).toString())
                    }
                });

                if (oppItemIds != "") {
                    if (confirm("You are about to disassociates linked Pos which will reverts the Gross Profit. Do you want to proceed?")) {
                        $.ajax({
                            type: "POST",
                            url: '../WebServices/CommonService.svc/DisassociatePO',
                            contentType: "application/json",
                            dataType: "json",
                            data: JSON.stringify({
                                "oppID": parseInt($("[id$=hdnOppID]").val())
                                , "oppItemIds": oppItemIds
                            }),
                            success: function () {
                                $("[id$=dgItem] > tbody > tr").not(":first").each(function (index, e) {
                                    $(e).find("[id$=divPO]").html("");
                                    $(e).find("[id$=hdnPOID]").html("0");
                                    $(e).find("[id$=hdnOppItemID]").html("0");
                                });
                            },
                            beforeSend: function () {
                                $("[id$=UpdateProgressPOSO]").show();
                            },
                            complete: function () {
                                $("[id$=UpdateProgressPOSO]").hide();
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                alert(jqXHR.responseText);
                            }
                        });
                    }
                } else {
                    alert("Select at least one row.")
                }
            } catch (e) {
                alert('Unknown error occurred.');
            }

            return false;
        }

        function ChangeShipToAddress() {
            window.open('frmChangeDefaultAdd.aspx?AddressType=ShipTo&CompanyId=' + $("[id$=hdnDivID]").val() + "&CompanyName=" + $("[id$=hdnCustomerName]").val(), '', 'toolbar=no,titlebar=no,left=200, top=300,width=800,height=170,scrollbars=no,resizable=no');
            return false;
        }

        function FillModifiedAddress(AddressType, AddressID, FullAddress, warehouseID, isDropshipClicked) {
            try {
                $("[id$=dgItem] > tbody > tr").not(":first").each(function (index, e) {
                    $(e).find("[id$=lblShipToAddress]").html(FullAddress.replace("<pre>", "").replace("</pre>", "<br/>"));
                    $(e).find("[id$=hdnShipToAddress]").val($(e).find("[id$=lblShipToAddress]").html().replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;"));
                    $(e).find("[id$=hdnShipToAddressID]").val(AddressID);
                });
            } catch (e) {
                alert('Unknown error occurred.');
            }

            return false;
        }

        function SingleShipAddressChanged(chkUseSingleShipAddress) {
            try {
                if ($(chkUseSingleShipAddress).find("input[id='chkUseSingleShipAddress']").is(":checked")) {
                    $("[id$=divShippingAddress]").show();
                } else {
                    $("[id$=divShippingAddress]").hide();
                }
            } catch (e) {
                alert('Unknown error occurred.');
            }
        }

        function OpenVendorDetailPopup(img) {
            try {
                var vendorID = parseInt($(img).closest("tr").find("[id$=ddlVendor]").val());
                var itemID = parseInt($(img).closest("tr").find("[id$=hdnItemCode]").val());
                var attrValues = $(img).closest("tr").find("[id$=hdnAttrValues]").val();

                if (vendorID > 0 && itemID > 0) {
                    $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/GetVendorDetail',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "vendorID": vendorID
                            , "itemID": itemID
                            , "attrValues": attrValues
                        }),
                        success: function (data) {
                            var obj = $.parseJSON(data.GetVendorDetailResult);
                            var itemDetails = $.parseJSON(obj.ItemDetails);
                            var vendorDetails = $.parseJSON(obj.VendorDetails);

                            var html = "<div class='row'><div class='col-xs-12'><h4 style='margin-bottom:5px;font-weight: bold;font-size: 16px;'>Items in Transit</h4></div></div>";
                            html += "<div class='row padbottom10'><div class='col-xs-12'><ul class='list-unstyled'>";
                            if (itemDetails != null && itemDetails.length > 0) {
                                itemDetails.forEach(function (n) {
                                    html += ("<li>Qty of " + (n.numQty || "") + " due " + (n.vcDueDate || "") + "</li>");
                                });
                            } else {
                                html += "<li>-</li>";
                            }
                            html += "</ul></div></div>";
                            html += "<div class='row'><div class='col-xs-12'><h4 style='margin-bottom:5px;font-weight: bold;font-size: 16px;'>Open PO Totals</h4></div></div>";
                            html += "<div class='row padbottom10'><div class='col-xs-12'>";
                            if (vendorDetails != null && vendorDetails.length > 0) {
                                html += $("[id$=hdnMSCurrencyID]").val() + (vendorDetails[0].TotalPOAmount || 0).toLocaleString() + ", " + (vendorDetails[0].TotalWeight || 0).toLocaleString() + " Lbs, " + (vendorDetails[0].TotalQty || 0).toLocaleString() + " Qty";                                
                            } else {
                                html += '-'
                            }
                            html += "</div></div>";
                            html += "<div class='row'><div class='col-xs-12'><h4 style='margin-bottom:5px;font-weight: bold;font-size: 16px;'>Purchase Incentives</h4></div></div>";
                            html += "<div class='row padbottom10'><div class='col-xs-12'>";
                            if (vendorDetails != null && vendorDetails.length > 0) {
                                html += (vendorDetails[0].Purchaseincentives || "");
                            } else {
                                html += '-'
                            }
                            html += "</div></div>";

                            $("#divVendorDetail .modal-body").html(html);
                            $("#divVendorDetail").modal("show");
                        },
                        beforeSend: function () {
                            $("[id$=UpdateProgressPOSO]").show();
                        },
                        complete: function () {
                            $("[id$=UpdateProgressPOSO]").hide();
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            alert("Unknown error ocurred");
                        }
                    });
                    
                }
            } catch (e) {
                alert('Unknown error occurred.');
            }
        }

        function UseReplenishmentFormula() {
            try {
                $("[id$=dgItem] > tbody > tr").not(":first").each(function (index, e) {
                    $(this).find("[id$=txtUnits]").val($(this).find("[id$=hdnItemRepQuantity]").val());
                });
            } catch (e) {
                alert('Unknown error occurred.');
            }
        }
    </script>
    <style type="text/css">
        input[type='radio'] {
            background-color: #FFFF99;
            color: Navy;
            position: relative;
            bottom: 3px;
            vertical-align: middle;
        }

        .rcbInput {
            height: 19px !important;
        }

        input[type=checkbox] {
            vertical-align: middle;
            position: relative;
            top: -4px;
        }

        pre {
            white-space: pre-wrap;
        }

        .chkBizDoc > input {
            margin-top: 4px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgressPOSO" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #fff; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="alert alert-warning" id="divAlert" runat="server" visible="false">
        <h4><i class="icon fa fa-warning"></i>Alert!</h4>
        <asp:Literal ID="litMessage" runat="server"></asp:Literal>
    </div>
    <div class="alert alert-success" id="divSuccess" runat="server" visible="false">
        <asp:Literal ID="litSuccessMessage" runat="server"></asp:Literal>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <asp:CheckBox ID="chkFromExisting" runat="server" CssClass="chkBizDoc" />
                    <label>Build list from existing Invoice:</label>
                    <asp:DropDownList ID="ddlInvoices" runat="server" CssClass="form-control" AutoPostBack="true">
                    </asp:DropDownList>
                    <asp:CheckBox ID="chkShowOnlyBOItems" runat="server" Text="Only select line items on back order"  />
                    <asp:Button runat="server" ID="btnUseReplenishmentFormula" CssClass="btn btn-primary" Text="Run item replenishment formula"  OnClientClick="return UseReplenishmentFormula();" />
                </div>
            </div>
            <div class="pull-right">
                <asp:Button ID="btnDisassociate" runat="server" CssClass="btn btn-primary" Text="Disassociate" Visible="false" OnClientClick="return DisassociatePO();"></asp:Button>
                <asp:Button ID="btnMatchPO" runat="server" CssClass="btn btn-primary" Text="Match Purchase Order" Visible="false" OnClientClick="return OpenMatchPOPopup();"></asp:Button>
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary">Create Opportunity</asp:LinkButton>
                <asp:LinkButton ID="btnClose" runat="server" CssClass="btn btn-primary">Cancel</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="DetailPageTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblHeading" runat="server"></asp:Label>&nbsp;<a href="#" id="aHelp" runat="server"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="TabsPlaceHolder" runat="server" ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:DataGrid ID="dgItem" runat="server" CssClass="table table-bordered table-striped" AutoGenerateColumns="False" HeaderStyle-HorizontalAlign="Center" UseAccessibleHeader="true" style="table-layout:fixed; margin-bottom:0px">
                    <Columns>
                        <asp:BoundColumn DataField="vcItemDesc" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="vcPathForTImage" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="vcModelID" HeaderText="ModelID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="vcAttributes" HeaderText="Attributes" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="vcPartNo" HeaderText="Part #" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="vcType" HeaderText="Type" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="txtNotes" Visible="false" HeaderText="Notes"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Select" HeaderStyle-Width="30">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkSelectAll" runat="server" onclick="SelectAll('chkSelectAll','chkSelect')" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" CssClass="chkSelect" />
                                <asp:Label runat="server" ID="lblWarehouseItemId" Text='<%# Eval("numWarehouseItmsID") %>'
                                    Visible="false"></asp:Label>
                                <asp:HiddenField ID="hdnOppItemID" runat="server" Value='<%# Eval("numoppitemtCode") %>' />
                                
                                <asp:HiddenField ID="hdnItemCode" runat="server" Value='<%# Eval("numItemCode") %>' />
                                <asp:HiddenField ID="hdnOrigUnitHour" runat="server" Value='<%# Eval("numOrigUnitHour")%>' />
                                <asp:HiddenField ID="hdnOnHand" runat="server" Value='<%# Eval("numOnHand")%>' />
                                <asp:HiddenField ID="hdnAttributes" runat="server" Value='<%# Eval("vcAttributes")%>' />
                                <asp:HiddenField ID="hdnAttrValues" runat="server" Value='<%# Eval("vcAttrValues")%>' />
                                <asp:HiddenField ID="hdnItemRepQuantity" runat="server" Value='<%# Eval("numItemRepQuantity")%>' />
                                
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Item" HeaderStyle-Width="200">
                            <ItemTemplate>
                                <ul class="list-inline">
                                    <li>
                                        <a href="javascript:OpenItem(<%# Eval("numItemCode")%>)">
                                            <img src="../images/tag.png" align="BASELINE" style="float: none;"></a>
                                    </li>
                                    <li>
                                        <asp:Label ID="lblItemName" runat="server" Text='<%# Eval("vcItemName")%>'></asp:Label>
                                    </li>
                                    <li>
                                        <asp:Label ID="lblAttributes" runat="server" Text='<%# Eval("vcAttributes")%>'></asp:Label>
                                    </li>
                                </ul>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Units" ItemStyle-Wrap="false" HeaderStyle-Width="200">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtUnits" CssClass="form-control" runat="Server" Text='<%# Eval("numUnitHour", "{0:#,##0.00}") %>'
                                                AUTOCOMPLETE="OFF" Width="100px" OnTextChanged="txtunits_TextChanged" AutoPostBack="true"></asp:TextBox></td>
                                        <td style="padding-left: 5px">
                                            <asp:DropDownList ID="ddlUOM" runat="server" CssClass="form-control" onchange="return UOMSelectionChanged(this);"></asp:DropDownList>
                                        </td>
                                        <td style="white-space: nowrap; padding-left: 5px">
                                            <asp:Label ID="lblUOMUnits" runat="server" Font-Bold="true"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Price" HeaderStyle-Width="150">
                            <ItemTemplate>
                                <asp:TextBox ID="txtPrice" CssClass="form-control" runat="Server" Text='<%# IIf(Convert.ToInt32(Session("numCost")) = 2, Eval("numCost"), IIf(OppType = 1, Eval("monVendorCost"), Eval("monPrice")))%>'
                                    AUTOCOMPLETE="OFF"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Required Date" HeaderStyle-Width="120">
                            <ItemTemplate>
                                <telerik:RadDatePicker ID="rdpItemReqDate" runat="server"  Width="100" DateInput-CssClass="form-control" ShowPopupOnFocus="true" DatePopupButton-Visible="false" ></telerik:RadDatePicker>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Vendor" ItemStyle-Wrap="false" HeaderStyle-Width="180">
                            <ItemTemplate>
                                <ul class="list-inline">
                                    <li>
                                        <asp:DropDownList ID="ddlVendor" runat="server" CssClass="form-control" Width="130" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged"
                                            AutoPostBack="True">
                                        </asp:DropDownList>
                                    </li>
                                    <li>
                                        <img src="../images/GLReport.png" onclick="return OpenVendorDetailPopup(this);" />
                                    </li>
                                </ul>
                                
                                <asp:HiddenField ID="hfVendorId" runat="server" Value='<%# CInt(Eval("numVendorID")) %>' />
                                <asp:HiddenField ID="hfSOVendorId" runat="server" Value='<%# CInt(Eval("numSOVendorId")) %>' />
                                <asp:HiddenField ID="hfnumClassID" runat="server" Value='<%# CInt(Eval("numClassID")) %>' />
                                <asp:HiddenField ID="hfnumProjectID" runat="server" Value='<%# CInt(Eval("numProjectID")) %>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Dropship" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="80">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkDropship" runat="server" Checked='<%# CCommon.ToBool(Eval("bitDropShip"))%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Purchase Order" HeaderStyle-Width="200">
                            <ItemTemplate>
                                <div id="divPO" runat="server"></div>
                                <asp:HiddenField ID="hdnPOName" runat="server" Value="" />
                                <asp:HiddenField ID="hdnPOID" runat="server" Value="0" />
                                <asp:HiddenField ID="hdnPOItemID" runat="server" Value="0" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderStyle-Width="200">
                            <HeaderTemplate>
                                <a href="javascript:ChangeShipToAddress();">Ship-To</a>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlShipToWarehouse" runat="server" CssClass="form-control" Style="display: none">
                                </asp:DropDownList>
                                <asp:Label ID="lblShipToAddress" runat="server" Style="display: none"></asp:Label>
                                <asp:HiddenField ID="hdnShipToAddress" runat="server" Value="0" ValidateRequestMode="Disabled" />
                                <asp:HiddenField ID="hdnShipToAddressID" runat="server" Value="0" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12 col-sm-3" id="divPreferredVendor" runat="server" visible ="false">
            <asp:CheckBox ID="cbPreferredVendor" runat="server" CssClass="signup" Text="Use this Vendor instead" onclick="javascript:VendorChecked(this.checked);" Font-Bold="true" />
        </div>
        <div class="col-xs-12 col-sm-3" id="divVendor1" runat="server">
            <div class="form-group">
                <label>
                    <asp:Label runat="server" ID="lblCompany" Font-Bold="true" /></label>
                <div>
                <telerik:RadComboBox ID="radCmbCompany" DropDownWidth="600px"
                    OnClientItemsRequested="OnClientItemsRequestedOrganization"
                    ClientIDMode="Static"
                    Width="100%"
                    ShowMoreResultsBox="true" Skin="Default"
                    runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True" EmptyMessage="Select Customer">
                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                </telerik:RadComboBox>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3" id="divVendor2" runat="server">
            <div class="form-group">
                <label>Contact</label>
                <asp:DropDownList ID="ddlContact" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3" id="divVendor3" runat="server">
            <div class="form-group">
                <label>Order</label>
                <asp:DropDownList ID="ddlOpportunity" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box box-primary box-solid">
                <div class="box-header">
                    <h3 class="box-title">Billing Address</h3>
                </div>
                <div class="box-body">
                    <asp:UpdatePanel runat="server" class="row" ChildrenAsTriggers="true" ID="upBilling" style="margin-left: 0px; margin-right: 0px; display: flex; flex-flow: row wrap;">
                        <ContentTemplate>
                            <div class="col-sm-12 col-md-3" style="background-color: #fff9dd; padding: 10px;">
                                <asp:Label ID="litBillAddress" runat="server" ForeColor="Black"></asp:Label>
                            </div>
                            <div class="col-sm-12 col-md-5">
                                <ul class="list-unstyled">
                                    <li>
                                        <asp:RadioButton ID="radBillEmployer" AutoPostBack="true" GroupName="rad1" runat="server" Text="Employer" Font-Bold="true" OnCheckedChanged="radBillEmployer_CheckedChanged" />
                                        <telerik:RadComboBox ID="radCmbBillEmployerAddress" runat="server" AutoPostBack="true" Width="180" OnSelectedIndexChanged="radCmbBillEmployerAddress_SelectedIndexChanged"></telerik:RadComboBox>
                                    </li>
                                    <li>
                                        <asp:RadioButton ID="radBillSOCustomer" AutoPostBack="true" GroupName="rad1" runat="server" Text="Use Sales Order Customerís address" OnCheckedChanged="radBillSOCustomer_CheckedChanged" />
                                        <asp:RadioButton ID="radBillCustomer" AutoPostBack="true" GroupName="rad1" runat="server" Text="&nbsp;Customer" Font-Bold="true" OnCheckedChanged="radBillCustomer_CheckedChanged" />
                                        <telerik:RadComboBox ID="radCmbBillCustomerAddress" runat="server" AutoPostBack="true" Width="180"></telerik:RadComboBox>
                                    </li>
                                    <li>
                                        <asp:RadioButton ID="radBillOther" AutoPostBack="true" GroupName="rad1" runat="server" Text="Other Location" Font-Bold="true" OnCheckedChanged="radBillOther_CheckedChanged" />

                                        <telerik:RadComboBox ID="radCmbBillOther" Width="180" DropDownWidth="600px"
                                            Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" ShowMoreResultsBox="true" EnableLoadOnDemand="True"
                                            OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                            ClientIDMode="Static" TabIndex="1" OnClientLoad="onRadComboBoxLoad" EmptyMessage="Select Customer">
                                            <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                        </telerik:RadComboBox>

                                        <telerik:RadComboBox ID="radCmbBillOtherAddress" runat="server" Width="120" AutoPostBack="true"></telerik:RadComboBox>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <asp:RadioButton ID="rbBillingContact" runat="server" Font-Bold="true" GroupName="BillingContact" Checked="true" Text="Billing Contact" />
                                        <br />
                                        <asp:DropDownList ID="ddlBillingContact" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <asp:RadioButton ID="rbBillingContactAlt" runat="server" Font-Bold="true" GroupName="BillingContact" Checked="false" Text="Billing Contact Alt" />
                                        <br />
                                        <asp:TextBox ID="txtBillingContactAlt" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
        <div class="col-xs-12 padbottom10" id="divSingleShipAddress" runat="server" visible="false">
            <asp:CheckBox ID="chkUseSingleShipAddress" Text="Shipping Address" runat="server" onchange="SingleShipAddressChanged(this);" />
            - Use this if you want to apply a single shipping address for all selected line items when creating a new Purchase Order
        </div>
        <div class="col-xs-12" id="divShippingAddress" runat="server">
            <div class="box box-primary box-solid">
                <div class="box-header">
                    <h3 class="box-title">Ship To Address</h3>
                </div>
                <div class="box-body">
                    <asp:UpdatePanel runat="server" class="row" ChildrenAsTriggers="true" ID="upShipping" style="margin-left: 0px; margin-right: 0px; display: flex; flex-flow: row wrap;">
                        <ContentTemplate>
                            <div class="col-sm-12 col-md-3" style="background-color: #fff9dd; padding: 10px;">
                                <asp:Label ID="litShipAddress" runat="server" ForeColor="Black"></asp:Label>
                            </div>
                            <div class="col-sm-12 col-md-5">
                                <ul class="list-unstyled">
                                    <li>
                                        <asp:RadioButton ID="radShipEmployer" AutoPostBack="true" GroupName="rad2" runat="server" Text="Employer" Font-Bold="true" OnCheckedChanged="radShipEmployer_CheckedChanged" />
                                        <telerik:RadComboBox ID="radCmbShipEmployerAddress" runat="server" AutoPostBack="true" Width="180" OnSelectedIndexChanged="radCmbShipEmployerAddress_SelectedIndexChanged"></telerik:RadComboBox>
                                    </li>
                                    <li>
                                        <asp:RadioButton ID="radShipSO" AutoPostBack="true" GroupName="rad2" runat="server" Text="Use Sales Order Shipping address" OnCheckedChanged="radShipSO_CheckedChanged" />
                                        <asp:RadioButton ID="radShipCustomer" AutoPostBack="true" GroupName="rad2" runat="server" Text="Customer" Font-Bold="true" OnCheckedChanged="radShipCustomer_CheckedChanged" />
                                        <telerik:RadComboBox ID="radCmbShipCustomerAddress" runat="server" AutoPostBack="true" Width="180"></telerik:RadComboBox>
                                    </li>
                                    <li>
                                        <asp:RadioButton ID="radShipOther" AutoPostBack="true" GroupName="rad2" runat="server" Text="Other Location" Font-Bold="true" Style="margin-top: 3px;" OnCheckedChanged="radShipOther_CheckedChanged" />
                                        <telerik:RadComboBox ID="radCmbShipOther" Width="180" DropDownWidth="600px"
                                            Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" ShowMoreResultsBox="true" EnableLoadOnDemand="True"
                                            OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                            ClientIDMode="Static" TabIndex="1" OnClientLoad="onRadComboBoxLoad" EmptyMessage="Select Customer">
                                            <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                        </telerik:RadComboBox>

                                        <telerik:RadComboBox ID="radCmbShipOtherAddress" runat="server" Width="120" AutoPostBack="true"></telerik:RadComboBox>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-12 col-md-4">
                                <div class="row">
                                    <div class="col-xs-12 col-md-6">
                                        <asp:RadioButton ID="rbShippingContact" runat="server" Font-Bold="true" GroupName="ShippingContact" Checked="true" Text="Shipping Contact" />
                                        <br />
                                        <asp:DropDownList ID="ddlShippingContact" runat="server" CssClass="form-control"></asp:DropDownList>
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <asp:RadioButton ID="rbShippingContactAlt" runat="server" Font-Bold="true" GroupName="ShippingContact" Checked="false" Text="Shipping Contact Alt" />
                                        <br />
                                        <asp:TextBox ID="txtShippingContactAlt" runat="server" CssClass="form-control"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade right" id="divMatchPO" role="dialog">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="color: #ffffff; opacity: 1;">&times;</button>
                    <h4 class="modal-title">Match Purchase Order</h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table id="tblPO" class="table table-bordered table-striped">
                            <tr>
                                <th>Purchase Order</th>
                                <th>Vendor</th>
                                <th>Item Name</th>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-match-po">Match & Close</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade right" id="divVendorDetail" role="dialog">
        <div class="modal-dialog modal-md" style="max-width:500px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" style="color: #ffffff; opacity: 1;">&times;</button>
                    <h4 class="modal-title">Vendor Details</h4>
                </div>
                <div class="modal-body">
                   
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnOppID" runat="server" />
    <asp:HiddenField ID="hdnDivID" runat="server" />
    <asp:HiddenField ID="hdnCustomerName" runat="server" />
    <asp:HiddenField ID="hdnMSCurrencyID" runat="server" Value="$" />
</asp:Content>
