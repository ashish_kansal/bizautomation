﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmVendorCostTable.aspx.vb"
    Inherits="BACRM.UserInterface.VendorCost.frmVendorCostTable" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Vendor Cost</title>
    <script language="javascript">
        function Close() {
            window.close()
            return false;
        }

        function UpdateClose() {
            var radioButtons = document.getElementsByName("ctl00$Content$gvVendorCost$ctl01$gnVendorName");
            var Vendor = 0;
            for (var x = 0; x < radioButtons.length; x++) {
                if (radioButtons[x].checked) {
                    var mySplitResult = radioButtons[x].id.split("~");
                    Vendor = mySplitResult[0].replace('gvVendorCost_ctl01_', '');
                }
            }

            if (Vendor == 0) {
                alert('Please select Vendor');
                return false;
            }

            opener.document.getElementById('hdnVendorId').value = Vendor;
            opener.__doPostBack('btnVendorCostChange', '');
            window.close()
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnUpdate" runat="server" CssClass="button" OnClientClick="return UpdateClose()"
                Text="Update" />&nbsp;
            <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="window.close()"
                Text="Close" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Vendor Cost
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:GridView ID="gvVendorCost" runat="server" AutoGenerateColumns="false" CssClass="tbl"
        Width="500px">
        <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is" />
        <HeaderStyle CssClass="hs" />
    </asp:GridView>
</asp:Content>
