Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports Telerik.Web.UI
Imports System.Web.Services
Imports System
Imports ClosedXML.Excel
Imports System.IO
Imports System.Linq
Namespace BACRM.UserInterface.Opportunities
    Partial Public Class frmDealList
        Inherits BACRMPage

        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim intOrder As Integer = 0
        Dim strColumn As String
        Dim m_aryRightsForPage1() As Integer
        Dim m_aryRightsForPage2(), m_aryRightsForViewGridConfiguration() As Integer
        Dim RegularSearch As String
        Dim CustomSearch As String

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Dim m_aryRightsForSendAnnounceMent() As Integer = GetUserRightsForPage_Other(43, 140)

                If m_aryRightsForSendAnnounceMent(RIGHTSTYPE.VIEW) <> 0 Then
                    btnSendAnnounceMent.Visible = True
                End If
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                'CLEAR ALERT MESSAGE
                divMessage.Style.Add("display", "none")
                litMessage.Text = ""

                If GetQueryStringVal("frm") <> "" Then
                    frm = ""
                    frm = GetQueryStringVal("frm")
                Else : frm = ""
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = ""
                    frm1 = GetQueryStringVal("frm1")
                Else : frm1 = ""
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    frm2 = ""
                    frm2 = GetQueryStringVal("frm2")
                Else : frm2 = ""
                End If

                If Not IsPostBack Then
                    If GetQueryStringVal("ShowClosedDeal") = "True" Then
                        lblDeal.Text = "Closed Orders"
                        ViewState("ClosedDeals") = "True"
                    Else
                        lblDeal.Text = "Open Orders"
                    End If
                    If GetQueryStringVal("Export") <> "" Then
                        btnExport.Visible = CCommon.ToString(GetQueryStringVal("Export"))
                    End If
                    hdnDealType.Value = CCommon.ToLong(GetQueryStringVal("type"))


                    If GetQueryStringVal("type") = "1" Then
                        ddlSortDeals.Items.FindByValue("1").Selected = True
                        lblDeal.Text = "Sales Orders"
                        litTitle.Text = "Sales Orders"
                        'hplSettings.Attributes.Add("onclick", "return OpenSetting(39)")
                        txtFormID.Text = "39"
                        'ddlFilterBy.Items(1).Text = "Partially Invoiced Orders"
                        'ddlFilterBy.Items(2).Text = "Fully Invoiced Orders"
                        'ddlFilterBy.Items(3).Text = "Orders without Invoices"
                        'ddlFilterBy.Items(4).Text = "Orders with items yet to be added to Invoices"

                        btnAddNewOrder.Attributes.Add("onclick", "return OpenPopUp('../opportunity/frmNewOrder.aspx?OppStatus=1&OppType=1');")
                        aHelp.Attributes.Add("onclick", "return OpenHelpPopUp('opportunity/frmdeallist.aspx?opptype=1')")
                    ElseIf GetQueryStringVal("type") = "2" Then
                        ddlSortDeals.Items.FindByValue("2").Selected = True
                        lblDeal.Text = "Purchase Orders"
                        litTitle.Text = "Purchase Orders"
                        'hplSettings.Attributes.Add("onclick", "return OpenSetting(41)")
                        txtFormID.Text = "41"
                        'ddlFilterBy.Items(1).Text = "Partially P.O.d Orders"
                        'ddlFilterBy.Items(2).Text = "Fully P.O.d Orders"
                        'ddlFilterBy.Items(3).Text = "Orders without P.O.(s)"
                        'ddlFilterBy.Items(4).Text = "Orders with items yet to be added to P.O.s"

                        btnAddNewOrder.Attributes.Add("onclick", "return OpenPopUp('../Opportunity/frmNewPurchaseOrder.aspx');")
                        aHelp.Attributes.Add("onclick", "return OpenHelpPopUp('opportunity/frmdeallist.aspx?opptype=2')")
                    End If


                    'Check if user has rights to edit grid configuration
                    m_aryRightsForViewGridConfiguration = GetUserRightsForPage_Other(10, 33)
                    If m_aryRightsForViewGridConfiguration(RIGHTSTYPE.VIEW) = 0 Then
                        Dim tdGridConfiguration As HtmlAnchor = CType(CCommon.FindControlRecursive(Page.Master, "tdGridConfiguration"), HtmlAnchor)

                        If Not tdGridConfiguration Is Nothing Then
                            tdGridConfiguration.Visible = False
                        End If
                    End If

                End If
                If GetQueryStringVal("partner") = "1" Then
                    intOrder = 1
                ElseIf GetQueryStringVal("partner") = "2" Then
                    intOrder = 2
                End If
                m_aryRightsForPage1 = GetUserRightsForPage_Other(10, 10)
                m_aryRightsForPage2 = GetUserRightsForPage_Other(10, 11)

                If m_aryRightsForPage1(RIGHTSTYPE.VIEW) = 0 And m_aryRightsForPage2(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AC")
                If m_aryRightsForPage1(RIGHTSTYPE.DELETE) = 0 And m_aryRightsForPage2(RIGHTSTYPE.DELETE) = 0 Then btnDelete.Visible = False

                If Not IsPostBack Then
                    Session("Data") = Nothing
                    Session("SalesProcessDetails") = Nothing
                    Session("List") = "Opp"
                    Dim strorderstatus As String
                    HideControls()
                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                        txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                        txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))
                        txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                        'ddlFilterBy.ClearSelection()
                        'If PersistTable(PersistKey.FilterBy) IsNot Nothing Then
                        '    ddlFilterBy.Items.FindByValue(CCommon.ToString(PersistTable(PersistKey.FilterBy))).Selected = True
                        'End If

                        txtGridColumnFilter.Text = CCommon.ToString(PersistTable(PersistKey.GridColumnSearch))
                        strorderstatus = CCommon.ToString(PersistTable(PersistKey.OrderStatus))

                        If Not CCommon.ToString(strorderstatus) = "" Then
                            If strorderstatus = "0" Then
                                radOpen.Checked = True
                            ElseIf strorderstatus = "1" Then
                                radClosed.Checked = True
                            Else
                                radAll.Checked = True
                            End If
                        Else
                            radOpen.Checked = True
                        End If
                    End If
                    If GetQueryStringVal("FilterBy") = "1" Then 'assignee
                        If GetQueryStringVal("GroupBy") = "0" Then 'fully invoiced
                            txtGridColumnFilter.Text = "Opp.numAssignedTo~100~0~SelectBox~0:" & GetQueryStringVal("SelectedID") & ";Opp.bintCreatedDate~119~0~DateField~From:" & GetQueryStringVal("From") & ";Opp.bintCreatedDate~119~0~DateField~To:" & GetQueryStringVal("To") & ";" & "Opp.tintInvoicing~50866~0~SelectBox:" & GetQueryStringVal("GroupBy") & ";"
                        ElseIf GetQueryStringVal("SelectedID") = "" Then
                            txtGridColumnFilter.Text = "OI.vcInventoryStatus~537~0~Label:" & GetQueryStringVal("InventorySort") & ";"
                        ElseIf GetQueryStringVal("InventorySort") = "3" Then
                            txtGridColumnFilter.Text = "Opp.numAssignedTo~100~0~SelectBox~0:" & GetQueryStringVal("SelectedID") & ";" & "OI.vcInventoryStatus~537~0~Label:" & GetQueryStringVal("InventorySort") & ";"
                        Else
                            txtGridColumnFilter.Text = "Opp.numAssignedTo~100~0~SelectBox~0:" & GetQueryStringVal("SelectedID") & ";Opp.bintCreatedDate~119~0~DateField~From:" & GetQueryStringVal("From") & ";Opp.bintCreatedDate~119~0~DateField~To:" & GetQueryStringVal("To") & ";"
                        End If
                    ElseIf GetQueryStringVal("FilterBy") = "2" Then 'record owner
                        If GetQueryStringVal("GroupBy") = "0" Then
                            txtGridColumnFilter.Text = "Opp.numRecOwner~111~0~SelectBox~0:" & GetQueryStringVal("SelectedID") & ";" & "Opp.tintInvoicing~50866~0~SelectBox:" & GetQueryStringVal("GroupBy") & ";Opp.bintCreatedDate~119~0~DateField~From:" & GetQueryStringVal("From") & ";Opp.bintCreatedDate~119~0~DateField~To:" & GetQueryStringVal("To") & ";"
                        ElseIf GetQueryStringVal("SelectedID") = "" Then
                            txtGridColumnFilter.Text = "OI.vcInventoryStatus~537~0~Label:" & GetQueryStringVal("InventorySort") & ";"
                        ElseIf GetQueryStringVal("InventorySort") = "3" Then
                            txtGridColumnFilter.Text = "Opp.numRecOwner~111~0~SelectBox~0:" & GetQueryStringVal("SelectedID") & ";" & "OI.vcInventoryStatus~537~0~Label:" & GetQueryStringVal("InventorySort") & ";"
                        Else
                            txtGridColumnFilter.Text = "Opp.numRecOwner~111~0~SelectBox~0:" & GetQueryStringVal("SelectedID") & ";Opp.bintCreatedDate~119~0~DateField~From:" & GetQueryStringVal("From") & ";Opp.bintCreatedDate~119~0~DateField~To:" & GetQueryStringVal("To") & ";"
                        End If
                    ElseIf GetQueryStringVal("FilterBy") = "3" Then
                        txtGridColumnFilter.Text = "Opp.numRecOwner~111~0~SelectBox~0:" & GetQueryStringVal("SelectedID") & ";"
                    End If

                    If (CCommon.ToInteger(txtCurrrentPage.Text) = 0) Then
                        txtCurrrentPage.Text = "1"
                    End If

                    'KEEP IT AT LAST ABOVE BindDatagrid
                    If CCommon.ToShort(GetQueryStringVal("IsFormDashboard")) = 1 Then
                        'ddlFilterBy.SelectedValue = "0"
                        radAll.Checked = True
                        txtGridColumnFilter.Text = ""
                    End If
                    BindDatagrid()

                    If GetQueryStringVal("type") = "2" Then
                        'Check if user has rights if purchase order merging
                        m_aryRightsForViewGridConfiguration = GetUserRightsForPage_Other(10, 34)
                        If m_aryRightsForViewGridConfiguration(RIGHTSTYPE.VIEW) = 0 Then
                            btnMerge.Visible = False
                        End If
                    End If
                End If

                BindDatagrid()
                InitializeSearchClientSideTemplate()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Function GetCheckedValues() As Boolean
            Try
                Session("ContIDs") = ""
                Dim isCheckboxchecked As Boolean = False
                Dim gvRow As GridViewRow
                For Each gvRow In gvSearch.Rows
                    If CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = True Then
                        Session("ContIDs") = Session("ContIDs") & CType(gvRow.FindControl("lblContactId"), Label).Text & ","
                        isCheckboxchecked = True
                    End If
                Next
                Session("ContIDs") = Session("ContIDs").ToString.TrimEnd(",")
                Return isCheckboxchecked
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Private Sub btnSendAnnounceMent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendAnnounceMent.Click
            Try
                Dim isCheckboxChecked As Boolean
                isCheckboxChecked = GetCheckedValues()
                If isCheckboxChecked Then
                    Session("EMailCampCondition") = Session("WhereCondition")
                    Response.Redirect("../Marketing/frmEmailBroadCasting.aspx?PC=true&SearchID=0&ID=0&SAll=true", False)
                Else
                    DisplayError("Select atleast one checkbox to broadcast E-mail.")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        <WebMethod(EnableSession:=True)>
        Public Shared Function FillInvoiceItem(ByVal OppId As Long) As String
            Try
                Dim objOpp As New COpportunities
                Dim dt As DataTable
                objOpp.DomainID = DomainID
                objOpp.OpportID = OppId
                dt = objOpp.ViewItemNotYetInvoiced(OppId, 0)
                If dt.Rows.Count > 0 Then
                    Return (dt.Rows(0)(0))
                Else
                    Return ""
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function


        <WebMethod(EnableSession:=True)>
        Public Shared Function FillPendingApproval(ByVal OppId As Long) As String
            Try
                Dim objOpp As New COpportunities
                Dim dt As DataTable
                objOpp.DomainID = OppId
                objOpp.OpportID = OppId
                dt = objOpp.ViewApproverItemUnitPrice(OppId, OppId)
                If dt.Rows.Count > 0 Then
                    Return (Convert.ToString(dt.Rows(0)(0)))
                Else
                    Return ""
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Sub BindDatagrid(Optional ByVal CreateCol As Boolean = True)
            Try
                Dim dtOpportunity As DataTable
                Dim objOpportunity As New COpportunities
                objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                Dim SortChar As Char

                ' Set Default Filter 
                With objOpportunity
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")
                    .intOrder = intOrder
                    .SortOrder = ddlSortDeals.SelectedItem.Value
                    '.FilterBy = ddlFilterBy.SelectedItem.Value
                    .endDate = Now()
                    .SortCharacter = txtSortChar.Text.Trim()

                    If radOpen.Checked Then
                        .Shipped = 0
                    ElseIf radClosed.Checked Then
                        .Shipped = 1
                    Else
                        .Shipped = 2
                    End If

                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()

                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0

                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If

                    GridColumnSearchCriteria()


                    .RegularSearchCriteria = RegularSearch
                    .CustomSearchCriteria = CustomSearch
                    .DashboardReminderType = If(CCommon.ToString(GetQueryStringVal("IsFormDashboard")) = "1", CCommon.ToInteger(GetQueryStringVal("ReminderType")), 0)
                End With

                If txtSortColumn.Text <> "" Then
                    objOpportunity.columnName = txtSortColumn.Text
                Else : objOpportunity.columnName = "opp.bintcreateddate"
                End If

                If objOpportunity.FilterBy = 5 Then
                    objOpportunity.columnName = "vcPOppName"
                    objOpportunity.columnSortOrder = "Asc"
                End If
                objOpportunity.SalesDealsUserRights = m_aryRightsForPage1(RIGHTSTYPE.VIEW)
                objOpportunity.PurchaseDealsUserRights = m_aryRightsForPage2(RIGHTSTYPE.VIEW)
                If ViewState("ClosedDeals") = "True" Then
                    objOpportunity.intType = ddlSortDeals.SelectedValue
                Else
                    objOpportunity.intType = ddlSortDeals.SelectedValue
                End If

                objOpportunity.SearchText = radCmbSearch.Text.Replace("%", "[%]")

                Dim dsList As DataSet

                dsList = objOpportunity.GetDealsList1()
                dtOpportunity = dsList.Tables(0)

                Dim dtTableInfo As DataTable
                dtTableInfo = dsList.Tables(1)
                Dim strorderstatus As String
                If radOpen.Checked Then
                    strorderstatus = "0"
                ElseIf radClosed.Checked Then
                    strorderstatus = "1"
                Else
                    strorderstatus = "2"
                End If

                'Persist Form Settings
                PersistTable.Clear()
                PersistTable.Add(PersistKey.CurrentPage, IIf(dtOpportunity.Rows.Count > 0, txtCurrrentPage.Text, "1"))
                PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())
                PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
                PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text.Trim())
                'PersistTable.Add(PersistKey.FilterBy, ddlFilterBy.SelectedValue)
                PersistTable.Add(PersistKey.GridColumnSearch, txtGridColumnFilter.Text)
                PersistTable.Add(PersistKey.OrderStatus, strorderstatus)
                PersistTable.Save()

                If objOpportunity.TotalRecords = 0 Then
                    txtTotalRecordsDeals.Text = 0
                Else
                    Dim strTotalPage As String()
                    Dim decTotalPage As Decimal
                    decTotalPage = objOpportunity.TotalRecords / Session("PagingRows")
                    decTotalPage = Math.Round(decTotalPage, 2)
                    strTotalPage = CStr(decTotalPage).Split(".")
                    If (objOpportunity.TotalRecords Mod Session("PagingRows")) = 0 Then
                        txtTotalPageDeals.Text = strTotalPage(0)
                    Else
                        txtTotalPageDeals.Text = strTotalPage(0) + 1
                    End If
                    txtTotalRecordsDeals.Text = objOpportunity.TotalRecords
                End If

                Dim m_aryRightsForInlineEdit() As Integer

                If objOpportunity.OppType = 1 Then
                    m_aryRightsForInlineEdit = GetUserRightsForPage_Other(10, 3)
                Else
                    m_aryRightsForInlineEdit = GetUserRightsForPage_Other(10, 9)
                End If

                Dim i As Integer


                For i = 0 To dtOpportunity.Columns.Count - 1
                    dtOpportunity.Columns(i).ColumnName = dtOpportunity.Columns(i).ColumnName.Replace(".", "")
                Next

                Dim htGridColumnSearch As New Hashtable

                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")
                    Dim strIDValue() As String

                    For i = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")

                        htGridColumnSearch.Add(strIDValue(0), strIDValue(1))
                    Next
                End If

                If CreateCol = True Then
                    Dim bField As BoundField
                    Dim Tfield As TemplateField
                    gvSearch.Columns.Clear()

                    For Each drRow As DataRow In dtTableInfo.Rows
                        Tfield = New TemplateField

                        Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, drRow, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, IIf(ddlSortDeals.SelectedValue = 1, 39, 41), objOpportunity.columnName, objOpportunity.columnSortOrder)

                        Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, drRow, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, IIf(ddlSortDeals.SelectedValue = 1, 39, 41), objOpportunity.columnName, objOpportunity.columnSortOrder)
                        gvSearch.Columns.Add(Tfield)
                    Next

                    Dim dr As DataRow
                    dr = dtTableInfo.NewRow()
                    dr("vcAssociatedControlType") = "DeleteCheckBox"
                    dr("intColumnWidth") = "30"

                    Tfield = New TemplateField
                    Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, dr, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, IIf(ddlSortDeals.SelectedValue = 1, 39, 41))
                    Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, dr, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, IIf(ddlSortDeals.SelectedValue = 1, 39, 41))
                    gvSearch.Columns.Add(Tfield)
                End If

                bizPager.PageSize = Session("PagingRows")
                bizPager.RecordCount = objOpportunity.TotalRecords
                bizPager.CurrentPageIndex = txtCurrrentPage.Text
                gvSearch.DataSource = dtOpportunity
                gvSearch.DataBind()


                If m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE) <> 0 Then
                    Dim objPageControls As New PageControls
                    Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtTableInfo)
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "InlineEditValidation", strValidation, True)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click

            Try


                GetQueryStringVal("SelectedID")
                GetQueryStringVal("FilterBy")

                Dim dtOpportunity As DataTable
                Dim objOpportunity As New COpportunities
                objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                With objOpportunity
                    .DomainID = Session("DomainID")
                    .AssignedTo = IIf(GetQueryStringVal("FilterBy") = "1", CCommon.ToInteger(GetQueryStringVal("SelectedID")), 0)
                    .CreatedBy = IIf(GetQueryStringVal("FilterBy") = "2", CCommon.ToInteger(GetQueryStringVal("SelectedID")), 0)
                End With
                Dim dsList As DataSet

                dsList = objOpportunity.GetDealsListExport(DateTime.Parse(GetQueryStringVal("From")).ToString("MM/dd/yyyy") & "-" & DateTime.Parse(GetQueryStringVal("To")).ToString("MM/dd/yyyy"))

                dtOpportunity = dsList.Tables(0)
                If (Not dtOpportunity Is Nothing) And dtOpportunity.Rows.Count > 0 Then
                    Dim tablestr As String = ConvertDataTableToHTML(dtOpportunity)
                    Dim httpResponse As HttpResponse = Response
                    Using wb As New XLWorkbook()
                        wb.Worksheets.Add(dtOpportunity, "SalesOrder")

                        httpResponse.Clear()
                        httpResponse.Buffer = True
                        httpResponse.Charset = ""
                        httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                        httpResponse.AddHeader("content-disposition", "attachment;filename=SalesOrder.xlsx")
                        Using MyMemoryStream As New MemoryStream()
                            wb.SaveAs(MyMemoryStream)
                            MyMemoryStream.WriteTo(httpResponse.OutputStream)
                            httpResponse.Flush()
                            httpResponse.End()
                        End Using
                    End Using

                End If


            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Public Shared Function ConvertDataTableToHTML(ByVal dt As DataTable) As String
            Dim html As String = "<table>"
            html += "<tr>"

            For i As Integer = 0 To dt.Columns.Count - 1
                html += "<td>" & dt.Columns(i).ColumnName & "</td>"
            Next

            html += "</tr>"

            For i As Integer = 0 To dt.Rows.Count - 1
                html += "<tr>"

                For j As Integer = 0 To dt.Columns.Count - 1
                    html += "<td>" & dt.Rows(i)(j).ToString() & "</td>"
                Next

                html += "</tr>"
            Next

            html += "</table>"
            Return html
        End Function
        'Added by Neelam Kapila || 10/07/2017 - Added functionality to Bind Child grid in modal pop up
        Private Sub btnBindChild_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBindChild.Click
            BindChildGrid(hdnBindChild.Value)
        End Sub

        'Added by Neelam Kapila || 10/07/2017 - Added functionality to Bind Child grid in modal pop up
        Public Sub BindChildGrid(ByVal lngOppId As Long)
            Try
                Dim objOpportunity As New MOpportunity
                With objOpportunity
                    .DomainID = Session("DomainID")
                    .OpportunityId = lngOppId
                End With

                gvChildOpps.DataSource = objOpportunity.GetChildOpportunity()
                gvChildOpps.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Added by Neelam Kapila || 10/07/2017 - Added functionality to show Product image in the child grid
        Private Sub gvChildOpps_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvChildOpps.RowDataBound
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dataRowView As DataRowView = DirectCast(e.Row.DataItem, DataRowView)
                Dim charType As String = CCommon.ToString(dataRowView("tintOppType"))
                Dim img As System.Web.UI.WebControls.Image = e.Row.FindControl("imgType")

                If (charType = "2") Then
                    img.ImageUrl = "~/images/Icon/Purchase order.png"
                Else
                    img.ImageUrl = "~/images/Icon/Sales order.png"
                End If
            End If
        End Sub

        Private Sub txtCurrrentPage_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
            Try
                BindDatagrid(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Function ReturnDateTime(ByVal CloseDate) As String
            Try
                Dim strTargetResolveDate As String = ""
                If Not IsDBNull(CloseDate) Then strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
                Return strTargetResolveDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ReturnMoney(ByVal Money)
            Try
                If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub

        Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                radCmbSearch.ClearSelection()
                radCmbSearch.Text = ""
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
            Try
                If txtDelOppId.Text <> "" Then
                    Dim strOppid As String() = txtDelOppId.Text.Split(",")
                    Dim i As Int16 = 0
                    Dim lngOppID As Long
                    Dim lngRecOwnID As Long
                    Dim lngTerrID As Long
                    Dim objOpport As New COpportunities
                    For i = 0 To strOppid.Length - 1
                        lngOppID = strOppid(i).Split("~")(0)
                        lngRecOwnID = strOppid(i).Split("~")(1)
                        lngTerrID = strOppid(i).Split("~")(2)
                        Dim lintCount As Integer = 0

                        If ddlSortDeals.SelectedValue = 1 Then
                            If m_aryRightsForPage1(RIGHTSTYPE.DELETE) = 1 Then
                                Try
                                    If lngRecOwnID = Session("UserContactID") Then
                                        DeleteOpp(lngOppID, objOpport)
                                    End If
                                Catch ex As Exception

                                End Try
                            ElseIf m_aryRightsForPage1(RIGHTSTYPE.DELETE) = 2 Then
                                Try
                                    Dim j As Integer
                                    Dim dtTerritory As New DataTable
                                    dtTerritory = Session("UserTerritory")
                                    Dim chkDelete As Boolean = False
                                    If lngTerrID = 0 Then
                                        chkDelete = True
                                    Else
                                        For j = 0 To dtTerritory.Rows.Count - 1
                                            If lngTerrID = dtTerritory.Rows(j).Item("numTerritoryId") Then chkDelete = True
                                        Next
                                    End If

                                    If chkDelete = True Then
                                        DeleteOpp(lngOppID, objOpport)
                                    End If
                                Catch ex As Exception

                                End Try
                            ElseIf m_aryRightsForPage1(RIGHTSTYPE.DELETE) = 3 Then
                                DeleteOpp(lngOppID, objOpport)
                            End If
                        ElseIf ddlSortDeals.SelectedValue = 2 Then
                            If m_aryRightsForPage2(RIGHTSTYPE.DELETE) = 1 Then
                                Try
                                    If lngRecOwnID = Session("UserContactID") Then
                                        DeleteOpp(lngOppID, objOpport)
                                    End If
                                Catch ex As Exception

                                End Try
                            ElseIf m_aryRightsForPage2(RIGHTSTYPE.DELETE) = 2 Then
                                Try
                                    Dim j As Integer
                                    Dim dtTerritory As New DataTable
                                    dtTerritory = Session("UserTerritory")
                                    Dim chkDelete As Boolean = False
                                    If lngTerrID = 0 Then
                                        chkDelete = True
                                    Else

                                        For j = 0 To dtTerritory.Rows.Count - 1
                                            If lngTerrID = dtTerritory.Rows(j).Item("numTerritoryId") Then chkDelete = True
                                        Next

                                    End If
                                    If chkDelete = True Then
                                        DeleteOpp(lngOppID, objOpport)
                                    End If
                                Catch ex As Exception

                                End Try
                            ElseIf m_aryRightsForPage2(RIGHTSTYPE.DELETE) = 3 Then
                                DeleteOpp(lngOppID, objOpport)
                            End If
                        End If
                    Next

                End If
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub DeleteOpp(ByVal lngOppID As Long, ByVal objOpport As COpportunities)
            Try
                Dim lintCount As Integer
                With objOpport
                    .OpportID = lngOppID
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                End With
                lintCount = objOpport.GetAuthoritativeOpportunityCount()
                If lintCount = 0 Then
                    Dim objAdmin As New CAdmin

                    objAdmin.DomainID = Session("DomainId")
                    objAdmin.ModeType = MileStone1.ModuleType.Opportunity
                    objAdmin.ProjectID = lngOppID

                    objAdmin.RemoveStagePercentageDetails()

                    objOpport.DelOpp()
                Else : ShowMessage("Some  Opportunities could not be deleted as Authoritative BizDocs is present")
                End If
            Catch ex As Exception
                If ex.Message = "DEPENDANT" Then
                    ShowMessage("Can not remove record, Your option is to remove Time and Expense associated with all stages from ""milestone & stages"" sub-tab and try again.")
                ElseIf ex.Message = "RECURRING ORDER OR BIZDOC" Then
                    ShowMessage("Order can not be deleted because recurrence is set on order or on bizdoc within order.")
                ElseIf ex.Message = "RECURRED ORDER" Then
                    ShowMessage("Can not remove record because it is recurred from another order")
                ElseIf ex.Message = "CASE DEPENDANT" Then
                    ShowMessage("Dependent case exists. Cannot be Deleted.")
                ElseIf ex.Message = "CreditBalance DEPENDANT" Then
                    ShowMessage("Credit balance of current order is being used. Your option is used to credit balance of Organization from Accunting sub tab.")
                ElseIf ex.Message = "OppItems DEPENDANT" Then
                    ShowMessage("Items already used. Cannot be Deleted.")
                ElseIf ex.Message.Contains("FULFILLED_ITEMS") Then
                    ShowMessage("Can not delete record because contains fulfilled items. Your option is to delete fulfillment order bizdoc.")
                ElseIf ex.Message.Contains("WORK_ORDER_EXISTS") Then
                    ShowMessage("Work order exists. You have to first delete work order(s).")
                ElseIf ex.Message = "FY_CLOSED" Then
                    ShowMessage("This transaction can not be posted,Because transactions date belongs to closed financial year.")
                ElseIf ex.Message.Contains("SERIAL/LOT#_USED") Then
                    ShowMessage("Can not delete record because some serial/lot# is used in any sales order.")
                Else
                    Throw ex
                End If
            End Try
        End Sub
        Private Sub radClosed_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radClosed.CheckedChanged
            Try
                BindDatagrid()
                btnDelete.Visible = False
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub radOpen_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radOpen.CheckedChanged
            Try
                BindDatagrid()
                btnDelete.Visible = True
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        'Private Sub ddlFilterBy_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilterBy.SelectedIndexChanged
        '    Try
        '        If GetQueryStringVal("type") = "1" Then
        '            Session("SOFilterBy") = ddlFilterBy.SelectedIndex
        '        Else
        '            Session("POFilterBy") = ddlFilterBy.SelectedIndex
        '        End If
        '        BindDatagrid()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        Sub GridColumnSearchCriteria()
            Try

                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")

                    Dim strIDValue(), strID(), strCustom As String
                    Dim strRegularCondition As New ArrayList
                    Dim strCustomCondition As New ArrayList

                    For i As Integer = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")
                        strID = strIDValue(0).Split("~")

                        If strID(0).Contains("CFW.Cust") Then

                            Select Case strID(3).Trim()
                                Case "TextBox", "TextArea"
                                    strCustomCondition.Add(" EXISTS (select RecId FROM CFW_Fld_Values_Opp CFW where CFW.RecId = Opp.numOppId AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & " ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                Case "CheckBox"
                                    If strIDValue(1).ToLower() = "yes" Then
                                        strCustomCondition.Add(" EXISTS (select RecId FROM CFW_Fld_Values_Opp CFW where CFW.RecId = Opp.numOppId AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and COALESCE(CFW.Fld_Value,'0') " & "='1')")
                                    ElseIf strIDValue(1).ToLower() = "no" Then
                                        strCustomCondition.Add(" NOT EXISTS (select RecId FROM CFW_Fld_Values_Opp CFW where CFW.RecId = Opp.numOppId AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and COALESCE(CFW.Fld_Value,'0') " & "='1')")
                                    End If
                                Case "SelectBox"
                                    strCustomCondition.Add(" EXISTS (select RecId FROM CFW_Fld_Values_Opp CFW where CFW.RecId = Opp.numOppId AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & "=" & strIDValue(1) & "::VARCHAR)")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strCustomCondition.Add(" EXISTS (select RecId FROM CFW_Fld_Values_Opp CFW where CFW.RecId = Opp.numOppId AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " >= '" & fromDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strCustomCondition.Add(" EXISTS (select RecId FROM CFW_Fld_Values_Opp CFW where CFW.RecId = Opp.numOppId AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " <= '" & toDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    End If
                                Case "CheckBoxList"
                                    Dim items As String() = strIDValue(1).Split(",")
                                    Dim searchString As String = ""

                                    For Each item As String In items
                                        searchString = searchString & If(searchString.Length > 0, " OR ", "") & " GetCustFldValueOpp(" & strID(0).Replace("CFW.Cust", "") & ",Opp.numOppID) " & " ilike '%" & item.Replace("'", "''") & "%'"
                                    Next

                                    strCustomCondition.Add(" EXISTS (select RecId FROM CFW_Fld_Values_Opp CFW where CFW.RecId = Opp.numOppId AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND (" & searchString & "))")
                                Case Else
                                    strCustomCondition.Add(" EXISTS (select RecId FROM CFW_Fld_Values_Opp CFW where CFW.RecId = Opp.numOppId AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & strCustom & ")")
                            End Select
                        ElseIf strID(0) = "OBD.vcBizDocsList" Then
                            strRegularCondition.Add("(SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId = Opp.numOppId AND vcBizDocID ilike '%" & strIDValue(1).Replace("'", "").Replace(",", "','") & "%') > 0")
                        ElseIf strID(0) = "Opp.tintInvoicingTextbox" Then
                            strRegularCondition.Add("(SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId = Opp.numOppId AND vcBizDocID ilike '%" & strIDValue(1).Replace("'", "").Replace(",", "','") & "%') > 0")
                        ElseIf strID(0) = "OI.vcInventoryStatus" Then
                            strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                        ElseIf strID(0) = "OBD.monDealAmount" Then
                            strRegularCondition.Add(strID(0) & "> 0") ' & strIDValue(1))
                        Else
                            Select Case strID(3).Trim()
                                Case "Website", "Email", "TextBox", "Label"
                                    If strID(0) = "ADC.vcCompactContactDetails" Then
                                        strRegularCondition.Add("(ADC.vcFirstName ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR ADC.vcLastName ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR ADC.numPhone ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR ADC.numPhoneExtension ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                    Else
                                        strRegularCondition.Add(strID(0) & " ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                    End If
                                Case "SelectBox"
                                    If strID(0) = "OL.numWebApiId" Then
                                        If strIDValue(1).StartsWith("Sites~") Then
                                            strRegularCondition.Add("OL.numSiteID=" & strIDValue(1).Replace("Sites~", ""))
                                        Else
                                            strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                                        End If
                                    ElseIf strID(0) = "Opp.tintSource" Then
                                        strRegularCondition.Add("Opp.tintSource=" & strIDValue(1).ToString.Split("~")(0) & " and Opp.tintSourceType=" & strIDValue(1).ToString.Split("~")(1))
                                    Else
                                        If strID(0) = "Opp.numAssignedTo" AndAlso strIDValue(1) = "-1" Then
                                            strRegularCondition.Add("coalesce(" & strID(0) & ",0)" & "=0")
                                        Else
                                            strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                                        End If
                                    End If
                                Case "TextArea"
                                    strRegularCondition.Add(" Cast(" & strID(0) & " as varchar(5000)) ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strRegularCondition.Add(strID(0) & " >= '" & fromDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strRegularCondition.Add(strID(0) & " <= '" & toDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    End If
                            End Select
                        End If
                    Next
                    RegularSearch = String.Join(" and ", strRegularCondition.ToArray())
                    CustomSearch = String.Join(" and ", strCustomCondition.ToArray())
                Else
                    RegularSearch = ""
                    CustomSearch = ""
                End If
                HideControls()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub HideControls()
            If GetQueryStringVal("Export") <> "" And (txtCurrrentPage.Text = "1" Or txtCurrrentPage.Text.Trim = "") Then
                btnExport.Visible = True
            Else
                btnExport.Visible = False
            End If
        End Sub
        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                txtCurrrentPage_TextChanged(Nothing, Nothing)
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        End Sub

        Private Sub ShowMessage(ByVal message As String)
            Try
                divMessage.Style.Add("display", "")
                litMessage.Text = message
                divMessage.Focus()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnGotoRecord_Click(sender As Object, e As EventArgs) Handles btnGotoRecord.Click
            Try
                Response.Redirect("~/opportunity/frmOpportunities.aspx?frm=deallist&OpID=" & radCmbSearch.SelectedValue, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub radCmbSearch_ItemsRequested(sender As Object, e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs)
            Try
                If e.Text.Trim().Length > 0 Then
                    Dim searchCriteria As String
                    Dim itemOffset As Integer = e.NumberOfItems

                    objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                    objCommon.UserCntID = CCommon.ToLong(Session("UserContactID"))

                    Dim tintShipped As Int16

                    If radOpen.Checked Then
                        tintShipped = 0
                    ElseIf radClosed.Checked Then
                        tintShipped = 1
                    Else
                        tintShipped = 2
                    End If


                    Dim ds As DataSet = objCommon.SearchOrders(e.Text.Trim().Replace("%", "[%]"), ddlSortDeals.SelectedValue, IIf(ddlSortDeals.SelectedValue = "1", m_aryRightsForPage1(RIGHTSTYPE.VIEW), m_aryRightsForPage2(RIGHTSTYPE.VIEW)), 1, tintShipped, 1)

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                        Dim endOffset As Integer

                        endOffset = Math.Min(itemOffset + radCmbSearch.ItemsPerRequest, ds.Tables(0).Rows.Count)
                        e.EndOfItems = endOffset = ds.Tables(0).Rows.Count
                        e.Message = IIf(ds.Tables(0).Rows.Count <= 0, "No matches", String.Format("Orders <b>1</b>-<b>{0}</b> out of <b>{1}</b>", endOffset, ds.Tables(0).Rows.Count))

                        For Each dr As DataRow In ds.Tables(0).Rows
                            Dim item As New RadComboBoxItem()
                            item.Text = DirectCast(dr("vcPOppName"), String)
                            item.Value = dr("numOppID").ToString()
                            item.Attributes.Add("orderType", ddlSortDeals.SelectedValue)

                            item.DataItem = dr
                            radCmbSearch.Items.Add(item)
                            item.DataBind()
                        Next
                    End If
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                e.Message = "<b style=""color:Red"">Error occured while searching.</b>"
            End Try
        End Sub

        Private Sub InitializeSearchClientSideTemplate()
            Try
                objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                objCommon.UserCntID = CCommon.ToLong(Session("UserContactID"))
                Dim ds As DataSet = objCommon.SearchOrders("", ddlSortDeals.SelectedValue, IIf(ddlSortDeals.SelectedValue = "1", m_aryRightsForPage1(RIGHTSTYPE.VIEW), m_aryRightsForPage2(RIGHTSTYPE.VIEW)), 0, 2, 1)

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim dtTable As New DataTable
                    dtTable = ds.Tables(0)

                    'genrate header template dynamically
                    radCmbSearch.HeaderTemplate = New SearchTemplate(ListItemType.Header, dtTable, DropDownWidth:=radCmbSearch.DropDownWidth.Value)

                    'generate Clientside Template dynamically
                    radCmbSearch.ItemTemplate = New SearchTemplate(ListItemType.Item, dtTable, DropDownWidth:=radCmbSearch.DropDownWidth.Value)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub radAll_CheckedChanged(sender As Object, e As EventArgs) Handles radAll.CheckedChanged
            Try
                BindDatagrid()
                btnDelete.Visible = False
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
    End Class

#Region "RadComboBox Template"
    Public Class SearchTemplate
        Implements ITemplate

        Dim ItemTemplateType As ListItemType
        Dim dtTable1 As DataTable
        Dim i As Integer = 0
        Dim _DropDownWidth As Double

        Sub New(ByVal type As ListItemType, ByVal dtTable As DataTable, Optional ByVal DropDownWidth As Double = 600)
            Try
                ItemTemplateType = type
                dtTable1 = dtTable
                _DropDownWidth = DropDownWidth
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub InstantiateIn(ByVal container As Control) Implements ITemplate.InstantiateIn
            i = 0
            Dim label1 As Label
            Dim label2 As Label
            Dim img As System.Web.UI.WebControls.Image
            Dim table1 As New HtmlTable
            Dim tblCell As HtmlTableCell
            Dim tblRow As HtmlTableRow
            table1.Width = "100%"
            Dim ul As New HtmlGenericControl("ul")

            Select Case ItemTemplateType
                Case ListItemType.Header
                    For Each dr As DataRow In dtTable1.Rows
                        Dim li As New HtmlGenericControl("li")
                        li.Style.Add("width", Unit.Pixel(CCommon.ToInteger((_DropDownWidth - 50) / dtTable1.Rows.Count)).Value & "px")
                        li.Style.Add("float", "left")
                        li.Style.Add("text-align", "center")
                        li.Style.Add("font-weight", "bold")
                        li.InnerText = dr("vcFieldName").ToString
                        ul.Controls.Add(li)
                    Next

                    container.Controls.Add(ul)
                Case ListItemType.Item
                    For Each dr As DataRow In dtTable1.Rows
                        Dim li As New HtmlGenericControl("li")
                        Dim width As Integer = CCommon.ToInteger((_DropDownWidth - 50) / dtTable1.Rows.Count)
                        li.Style.Add("width", Unit.Pixel(width).Value & "px")
                        li.Style.Add("float", "left")

                        If dr("Custom") = "1" Then
                            label2 = New Label
                            label2.CssClass = "normal1"
                            label2.Attributes.Add("style", "display:inline-block;width:" & width)
                            AddHandler label2.DataBinding, AddressOf label2_DataBinding
                            li.Controls.Add(label2)
                        Else
                            label1 = New Label
                            label1.CssClass = "normal1"
                            label1.Attributes.Add("style", "display:inline-block;width:" & width)
                            AddHandler label1.DataBinding, AddressOf label1_DataBinding
                            li.Controls.Add(label1)
                        End If

                        ul.Controls.Add(li)
                    Next

                    container.Controls.Add(ul)
            End Select

        End Sub

        Private Sub label1_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
            Dim target As Label = CType(sender, Label)
            Dim item As RadComboBoxItem = CType(target.NamingContainer, RadComboBoxItem)
            Dim itemText As String = Convert.ToString(DirectCast(item.DataItem, DataRow)(dtTable1.Rows(i).Item("vcDbColumnName").ToString()))
            'Dim itemText As String = CType(DataBinder.Eval(item.DataItem, dtTable1.Rows(i).Item("vcDbColumnName").ToString()), String)
            target.Text = itemText
            i = i + 1
        End Sub

        Private Sub label2_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
            Dim target As Label = CType(sender, Label)
            Dim item As RadComboBoxItem = CType(target.NamingContainer, RadComboBoxItem)
            Dim itemText As String = Convert.ToString(DirectCast(item.DataItem, DataRow)(dtTable1.Rows(i).Item("vcFieldName").ToString()))
            'Dim itemText As String = CType(DataBinder.Eval(item.DataItem, dtTable1.Rows(i).Item("vcFieldName").ToString()), String)
            target.Text = itemText
            i = i + 1
        End Sub

    End Class
#End Region
End Namespace
