''created by anoop jayaraj
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Alerts

Namespace BACRM.UserInterface.Opportunities

    Partial Public Class frmNewSalesOrder
        Inherits BACRMPage

        Dim objItems As CItems
        Dim strValues As String
        Dim dtOppAtributes As DataTable
        Dim dsTemp As DataSet
        Dim lngDivId, lngCntID, lngOppId, OppBizDocID, JournalId As Long
        Dim dtOppBiDocItems As DataTable
        Dim strMessage As String
        Dim ResponseMessage As String
        Dim boolPurchased As Boolean = False
        Dim IsFromCreateBizDoc As Boolean = False
        Dim IsFromSaveAndOpenOrderDetails As Boolean = False
        Dim IsFromSaveAndNew As Boolean = False
        Dim boolFlag As Boolean = True

        Private Sub Page_Disposed(sender As Object, e As EventArgs) Handles Me.Disposed
            Try

            Catch ex As Exception

            End Try
        End Sub

        Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            Try
                If Not IsPostBack Then
                    Session("CouponCode") = Nothing
                    Session("TotalDiscount") = Nothing

                    radCmbCompany.Focus()
                End If

                CCommon.UpdateItemRadComboValues("1", radCmbCompany.SelectedValue)
                CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lblException.Text = ""
                order1.OppType = 1
                order1._PageType = order1.PageType.Sales

                If Me.IsPostBack Then
                    Dim eventTarget As String = If((Me.Request("__EVENTTARGET") Is Nothing), String.Empty, Me.Request("__EVENTTARGET"))

                    If eventTarget = "btnReloadAddress" Then
                        FillExistingAddress(CType(order1.FindControl("txtDivID"), TextBox).Text)
                    End If
                End If

                If Not IsPostBack Then


                    BindMultiCurrency()
                    BindSalesTemplate()


                    objCommon.UserCntID = Session("UserContactID")
                    If GetQueryStringVal("uihTR") <> "" Or GetQueryStringVal("fghTY") <> "" Or GetQueryStringVal("rtyWR") <> "" Or GetQueryStringVal("tyrCV") <> "" Or GetQueryStringVal("pluYR") <> "" Then

                        If GetQueryStringVal("uihTR") <> "" Then
                            objCommon.ContactID = CCommon.ToLong(GetQueryStringVal("uihTR"))
                            objCommon.charModule = "C"
                        ElseIf GetQueryStringVal("rtyWR") <> "" Then
                            objCommon.DivisionID = CCommon.ToLong(GetQueryStringVal("rtyWR"))
                            objCommon.charModule = "D"
                        ElseIf GetQueryStringVal("tyrCV") <> "" Then
                            objCommon.ProID = CCommon.ToLong(GetQueryStringVal("tyrCV"))
                            objCommon.charModule = "P"
                        ElseIf GetQueryStringVal("pluYR") <> "" Then
                            objCommon.OppID = CCommon.ToLong(GetQueryStringVal("pluYR"))
                            objCommon.charModule = "O"
                        ElseIf GetQueryStringVal("fghTY") <> "" Then
                            objCommon.CaseID = CCommon.ToLong(GetQueryStringVal("fghTY"))
                            objCommon.charModule = "S"
                        End If

                        objCommon.GetCompanySpecificValues1()
                        Dim strCompany, strContactID As String
                        strCompany = objCommon.GetCompanyName
                        strContactID = objCommon.ContactID
                        radCmbCompany.Text = strCompany
                        radCmbCompany.SelectedValue = objCommon.DivisionID

                        radCmbCompany_SelectedIndexChanged() 'Added by chintan- Reason:remaining credit wasn't showing

                        If Not ddlContact.Items.FindByValue(strContactID) Is Nothing Then
                            ddlContact.ClearSelection()
                            ddlContact.Items.FindByValue(strContactID).Selected = True
                        End If


                    End If


                End If

                If radNewCustomer.Checked = True Then
                    trExistCust.Visible = False
                    trNewCust.Visible = True
                Else
                    trExistCust.Visible = True
                    trNewCust.Visible = False
                End If
                If chkShipDifferent.Checked = True Then
                    pnlShipping.Visible = True
                Else : pnlShipping.Visible = False
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub

        Private Sub FillExistingAddress(ByVal sDivisionId As String)
            Try
                lblBillTo1.Text = ""
                lblShipTo1.Text = ""
                CType(order1.FindControl("hdnBillAddressID"), HiddenField).Value = 0
                CType(order1.FindControl("hdnShipAddressID"), HiddenField).Value = 0
                If Not IsNothing(sDivisionId) Then
                    Dim oCOpportunities As New COpportunities
                    oCOpportunities.DomainID = CType((Session("DomainID")), Long)
                    Dim dtAddress As DataTable = oCOpportunities.GetExistingAddress(CInt(Session("UserID")), CInt(sDivisionId))
                    lblBillTo1.Text = CCommon.ToString(dtAddress(0)("vcBillAddress"))
                    lblShipTo1.Text = CCommon.ToString(dtAddress(0)("vcShipAddress"))

                    CType(order1.FindControl("hdnCountry"), HiddenField).Value = CCommon.ToLong(dtAddress(0)("numShipToCountry"))
                    CType(order1.FindControl("hdnState"), HiddenField).Value = CCommon.ToLong(dtAddress(0)("numShipToState"))
                    CType(order1.FindControl("hdnCity"), HiddenField).Value = CCommon.ToString(dtAddress(0)("vcShipToCity"))
                    CType(order1.FindControl("hdnPostal"), HiddenField).Value = CCommon.ToString(dtAddress(0)("vcShipToPostal"))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public ReadOnly Property GetCompanyName() As String
            Get
                radCmbCompany.Text.Trim()
            End Get
        End Property
        Private Sub CreateClone()
            '' Create Clone Item of order.
            Try
                If GetQueryStringVal("IsClone") <> "" Then
                    Dim dtItems As DataTable
                    Dim objOpportunity As New OppotunitiesIP
                    objOpportunity.DomainID = Session("DomainID")
                    objOpportunity.OpportunityId = CCommon.ToLong(GetQueryStringVal("OppId"))
                    objOpportunity.Mode = 2
                    dtItems = objOpportunity.GetOrderItems().Tables(0)
                    Dim objItems As New CItems
                    Dim ds As New DataSet
                    Dim drow As DataRow
                    Dim i As Integer = 1
                    Dim k As Integer
                    Dim dtItemTax As DataTable
                    objCommon = New CCommon

                    If ViewState("SOItems") Is Nothing Then
                        order1.createSet()
                    End If
                    order1.UpdateDataTable()
                    ds = ViewState("SOItems")
                    ds.Tables(0).Clear()
                    ds.Tables(1).Clear()
                    ds.Tables(2).Clear()
                    objCommon = New CCommon
                    Dim objItem As New CItems
                    Dim strScript As String
                    For Each dr As DataRow In dtItems.Rows
                        If Not objItem.ValidateItemAccount(CCommon.ToLong(dr("numItemCode")), Session("DomainID"), order1.OppType) = True Then
                            strScript = "<script language='JavaScript'>"
                            strScript += "alert('Please Set Income,Asset,COGs(Expense) Account for " + dr("vcItemName") + " from Administration->Inventory->Item Details.');</script>"
                            'Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "AccountValidation", "alert('" + msg + "')", True)
                            Page.RegisterStartupScript("clientScript", strScript)
                            radCmbCompany.SelectedValue = ""
                            radCmbCompany.Text = ""
                            Return
                        End If
                        drow = ds.Tables(0).NewRow()
                        drow("numoppitemtCode") = dr("numoppitemtCode")
                        drow("numItemCode") = dr("numItemCode")
                        drow("numUnitHour") = dr("numUnitHour")
                        drow("monPrice") = String.Format("{0:0.00}", dr("monPrice"))
                        drow("monTotAmount") = String.Format("{0:0.00}", dr("monTotAmount"))
                        drow("numSourceID") = dr("numSourceID")
                        drow("vcItemDesc") = dr("vcItemDesc")
                        drow("numWarehouseID") = dr("numWarehouseID")
                        drow("vcItemName") = dr("vcItemName")
                        drow("Warehouse") = dr("Warehouse")
                        drow("numWarehouseItmsID") = dr("numWarehouseItmsID")
                        drow("ItemType") = dr("vcType")
                        drow("Attributes") = dr("vcAttributes")
                        drow("Op_Flag") = 1 '1 for insertion
                        drow("DropShip") = dr("bitDropShip")
                        drow("bitDiscountType") = dr("bitDiscountType")
                        drow("fltDiscount") = String.Format("{0:0.00}", dr("fltDiscount"))
                        drow("monTotAmtBefDiscount") = String.Format("{0:0.00}", dr("monTotAmtBefDiscount"))
                        drow("numUOM") = dr("numUOMId")
                        drow("vcUOMName") = dr("vcUOMName")
                        drow("UOMConversionFactor") = dr("UOMConversionFactor")
                        drow("bitWorkOrder") = dr("bitWorkOrder")
                        drow("charItemType") = dr("charItemType")

                        drow("numVendorWareHouse") = 0
                        drow("numShipmentMethod") = 0
                        drow("numSOVendorId") = dr("numSOVendorId")
                        drow("numProjectID") = 0
                        drow("numProjectStageID") = 0
                        drow("bitIsAuthBizDoc") = False

                        'Calculate Tax 
                        objItems.DomainID = Session("DomainID")
                        objItems.ItemCode = dr("numItemCode")
                        dtItemTax = objItems.ItemTax()
                        Dim strApplicable1 As String = ""
                        For Each dr1 As DataRow In dtItemTax.Rows
                            strApplicable1 = strApplicable1 & dr1("bitApplicable") & ","
                        Next
                        strApplicable1 = strApplicable1 & dr("bitTaxable")
                        CType(order1.FindControl("Taxable"), HtmlInputHidden).Value = strApplicable1

                        Dim strApplicable(), strTax() As String
                        strApplicable = CType(order1.FindControl("Taxable"), HtmlInputHidden).Value.Split(",")
                        strTax = CType(order1.FindControl("txtTax"), TextBox).Text.Split(",")
                        For k = 0 To chkTaxItems.Items.Count - 1
                            If strApplicable(k) = True Then
                                drow("Tax" & chkTaxItems.Items(k).Value) = String.Format("{0:0.00}", CCommon.ToDouble(strTax(k)) * dr("monTotAmount") / 100)
                                drow("bitTaxable" & chkTaxItems.Items(k).Value) = True
                            Else
                                drow("Tax" & chkTaxItems.Items(k).Value) = 0
                                drow("bitTaxable" & chkTaxItems.Items(k).Value) = False
                            End If
                        Next
                        i += 1
                        ds.Tables(0).Rows.Add(drow)
                    Next
                    ds.Tables(0).AcceptChanges()
                    ViewState("SOItems") = ds

                    order1.UpdateDetails()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
            Try
                radCmbCompany_SelectedIndexChanged()

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub

        Private Sub radCmbCompany_SelectedIndexChanged()
            Try

                If CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then

                    Dim objOpp As New OppBizDocs
                    If objOpp.ValidateARAP(radCmbCompany.SelectedValue, 0, Session("DomainID")) = False Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting->Accounts for RelationShip"" To Save' );", True)
                        radCmbCompany.SelectedValue = ""
                        radCmbCompany.Text = ""
                        Exit Sub
                    End If
                    FillContact(ddlContact)
                    objItems = New CItems
                    objItems.DivisionID = radCmbCompany.SelectedValue
                    CType(order1.FindControl("txtDivID"), TextBox).Text = radCmbCompany.SelectedValue
                    Me.FillExistingAddress(CType(order1.FindControl("txtDivID"), TextBox).Text)
                    Dim dtTable As DataTable
                    dtTable = objItems.GetAmountDue

                    Dim strBaseCurrency As String = ""
                    If (Session("MultiCurrency") = True) Then
                        strBaseCurrency = Session("Currency") + " "
                    End If

                    lblBalanceDue.Text = strBaseCurrency + String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("AmountDueSO"))
                    lblRemaningCredit.Text = strBaseCurrency + String.Format("{0:#,##0.00}", If(dtTable.Rows(0).Item("RemainingCredit") < 0, 0, dtTable.Rows(0).Item("RemainingCredit")))
                    lblTotalAmtPastDue.Text = strBaseCurrency + String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("AmountPastDueSO"))
                    lblCreditBalance.Text = strBaseCurrency + String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("SCreditMemo"))
                    lblCreditLimit.Text = strBaseCurrency + String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("CreditLimit"))

                    ViewState("PastDueAmount") = dtTable.Rows(0).Item("AmountPastDueSO")
                    hdnmonVal1.Value = CCommon.ToDecimal(dtTable.Rows(0).Item("AmountPastDueSO"))
                    hdnmonVal2.Value = CCommon.ToDecimal(Session("AmountPastDue"))
                    'Dim strScript As String
                    'strScript = "window.attachEvent('onload', setInterval('blinkIt()',500)); "

                    If CCommon.ToBool(Session("bitAmountPastDue")) = True AndAlso CCommon.ToDecimal(dtTable.Rows(0).Item("AmountPastDueSO")) > CCommon.ToDecimal(Session("AmountPastDue")) Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "BlinkAmount", "setInterval('blinkIt()',500);", True)
                        lblTotalAmtPastDue.ForeColor = Color.Red
                        lblTotalAmtPastDue.Font.Bold = True
                    Else
                        lblTotalAmtPastDue.ForeColor = Color.Black
                        lblTotalAmtPastDue.Font.Bold = False

                    End If
                    objCommon = New CCommon
                    Try
                        objCommon.DivisionID = radCmbCompany.SelectedValue
                        objCommon.charModule = "D"
                        objCommon.GetCompanySpecificValues1()

                        If objCommon.CurrencyID > 0 Then
                            If Not ddlCurrency.Items.FindByValue(objCommon.CurrencyID) Is Nothing Then
                                ddlCurrency.ClearSelection()
                                ddlCurrency.Items.FindByValue(objCommon.CurrencyID).Selected = True
                            End If
                        Else
                            If Not ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")) Is Nothing Then
                                ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")).Selected = True
                            End If
                        End If

                        If objCommon.OnCreditHold Then
                            CType(order1.FindControl("btnAdd"), Button).Visible = False
                            litMessage.Text = "This customer is on credit hold. To reinstate ability to create sales orders the ""On Credit Hold"" check box within the customerís Accounting sub-tab section must be un-checked."
                        Else
                            CType(order1.FindControl("btnAdd"), Button).Visible = True
                            litMessage.Text = ""
                        End If

                        FillAssignToDropdown(ddlAssignTo)

                        Dim lngOrgAssignedTo As Long
                        objCommon = New CCommon
                        objCommon.DomainID = Session("DomainID")
                        objCommon.Mode = 14
                        objCommon.Str = radCmbCompany.SelectedValue
                        lngOrgAssignedTo = objCommon.GetSingleFieldValue()

                        If ddlAssignTo.Items.FindByValue(lngOrgAssignedTo) IsNot Nothing Then
                            ddlAssignTo.ClearSelection()
                            ddlAssignTo.Items.FindByValue(lngOrgAssignedTo).Selected = True
                        End If
                    Catch ex As Exception

                    End Try

                    'If ddlSalesTemplate.SelectedValue = "0" Then
                    BindSalesTemplate()
                    'End If
                    CCommon.UpdateItemRadComboValues("1", radCmbCompany.SelectedValue)
                    order1.UpdateDetails()
                    CreateClone()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Function FillContact(ByVal ddlCombo As DropDownList)
            Try
                Dim fillCombo As New COpportunities
                With fillCombo
                    .DivisionID = radCmbCompany.SelectedValue
                    ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                    ddlCombo.DataTextField = "Name"
                    ddlCombo.DataValueField = "numcontactId"
                    ddlCombo.DataBind()
                End With
                ddlCombo.Items.Insert(0, New ListItem("---Select One---", "0"))
                If ddlCombo.Items.Count = 2 Then
                    ddlCombo.Items(1).Selected = True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Private Sub FillAssignToDropdown(ByRef ddlCombo As DropDownList)
            Try
                If Session("PopulateUserCriteria") = 1 Then
                    objCommon.sb_FillConEmpFromTerritories(ddlCombo, Session("DomainID"), 0, 0, objCommon.TerittoryID)
                ElseIf Session("PopulateUserCriteria") = 2 Then
                    objCommon.sb_FillConEmpFromDBUTeam(ddlCombo, Session("DomainID"), Session("UserContactID"))
                Else : objCommon.sb_FillConEmpFromDBSel(ddlCombo, Session("DomainID"), 0, 0)
                End If

                Dim objUser As New UserAccess
                objUser.DomainID = Session("DomainID")
                objUser.byteMode = 1

                Dim dt As DataTable = objUser.GetCommissionsContacts

                Dim item As ListItem
                Dim dr As DataRow
                For Each dr In dt.Rows
                    item = New ListItem()
                    item.Text = CCommon.ToString(dr("vcUserName"))
                    item.Value = CCommon.ToString(dr("numContactID"))
                    ddlCombo.Items.Add(item)
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub radNewCustomer_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radNewCustomer.CheckedChanged
            Try
                If radNewCustomer.Checked = True Then

                    objCommon.sb_FillComboFromDBwithSel(ddlBillCountry, 40, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlShipCountry, 40, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))

                    If Not ddlBillCountry.Items.FindByValue(Session("DefCountry")) Is Nothing Then
                        ddlBillCountry.Items.FindByValue(Session("DefCountry")).Selected = True
                        If ddlBillCountry.SelectedIndex > 0 Then FillState(ddlBillState, ddlBillCountry.SelectedItem.Value, Session("DomainID"))
                    End If
                    If Not ddlShipCountry.Items.FindByValue(Session("DefCountry")) Is Nothing Then
                        ddlShipCountry.Items.FindByValue(Session("DefCountry")).Selected = True
                        If ddlShipCountry.SelectedIndex > 0 Then FillState(ddlShipState, ddlShipCountry.SelectedItem.Value, Session("DomainID"))
                    End If


                    If Not ddlRelationship.Items.FindByValue("46") Is Nothing Then
                        ddlRelationship.ClearSelection()
                        ddlRelationship.Items.FindByValue("46").Selected = True
                        ddlRelationship_SelectedIndexChanged()
                    End If

                    FillAssignToDropdown(ddlNewAssignTo)

                    CType(order1.FindControl("btnAdd"), Button).Visible = True
                    litMessage.Text = ""

                    If chkTaxItems.Items.FindByValue("0") IsNot Nothing Then
                        chkTaxItems.ClearSelection()
                        chkTaxItems.Items.FindByValue("0").Selected = True
                    End If
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub

        Private Sub ddlBillCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBillCountry.SelectedIndexChanged
            Try
                If ddlBillCountry.SelectedIndex > 0 Then FillState(ddlBillState, ddlBillCountry.SelectedItem.Value, Session("DomainID"))
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub

        Private Sub ddlShipCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShipCountry.SelectedIndexChanged
            Try
                If ddlShipCountry.SelectedIndex > 0 Then FillState(ddlShipState, ddlShipCountry.SelectedItem.Value, Session("DomainID"))
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub

        Private Sub ddlRelationship_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRelationship.SelectedIndexChanged
            Try
                ddlRelationship_SelectedIndexChanged()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub
        Private Sub ddlRelationship_SelectedIndexChanged()
            Try
                order1.UpdateDetails()
                LoadProfile()
                Dim objOpp As New OppBizDocs
                If objOpp.ValidateARAP(0, ddlRelationship.SelectedValue, Session("DomainID")) = False Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting->Accounting/RelationShip"" To Save' );", True)
                    ddlRelationship.SelectedIndex = 0
                    Exit Sub
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub LoadProfile()
            Try
                Dim objUserAccess As New UserAccess
                objUserAccess.RelID = ddlRelationship.SelectedItem.Value
                objUserAccess.DomainID = Session("DomainID")
                ddlProfile.DataSource = objUserAccess.GetRelProfileD
                ddlProfile.DataTextField = "ProName"
                ddlProfile.DataValueField = "numProfileID"
                ddlProfile.DataBind()
                ddlProfile.Items.Insert(0, New ListItem("---Select One---", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlSalesTemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSalesTemplate.SelectedIndexChanged
            Try
                If ddlSalesTemplate.SelectedIndex = 0 Then Exit Sub
                Dim objOpp As New COpportunities
                Dim dtTemplate As DataTable
                objOpp.DomainID = Session("DomainID")
                objOpp.SalesTemplateID = ddlSalesTemplate.SelectedValue.Split(",")(0)
                objOpp.byteMode = 2
                dtTemplate = objOpp.GetSalesTemplate()
                If dtTemplate.Rows.Count > 0 Then
                    Dim dtItems As DataTable
                    Dim objOpportunity As New MOpportunity
                    'Fetch items from Opportunity
                    If CCommon.ToLong(dtTemplate.Rows(0)("numOppId")) > 0 Then
                        objOpportunity.OpportunityId = CCommon.ToLong(dtTemplate.Rows(0)("numOppId"))
                        objOpportunity.DomainID = Session("DomainID")
                        objOpportunity.Mode = 2
                        dtItems = objOpportunity.GetOrderItems().Tables(0)
                    Else 'Fetch item from sales template items 
                        dtItems = objOpp.GetSalesTemplateItems()
                    End If

                    Dim objItems As New CItems
                    Dim ds As New DataSet
                    Dim drow As DataRow
                    Dim i As Integer = 1
                    Dim k As Integer
                    Dim dtItemTax As DataTable
                    objCommon = New CCommon
                    ds = ViewState("SOItems")
                    ds.Tables(0).Clear()
                    ds.Tables(1).Clear()
                    ds.Tables(2).Clear()
                    For Each dr As DataRow In dtItems.Rows
                        drow = ds.Tables(0).NewRow()
                        drow("numoppitemtCode") = dr("numoppitemtCode")
                        drow("numItemCode") = dr("numItemCode")
                        drow("numUnitHour") = dr("numUnitHour")
                        drow("monPrice") = String.Format("{0:0.00}", dr("monPrice"))
                        drow("monTotAmount") = String.Format("{0:0.00}", dr("monTotAmount"))
                        drow("numSourceID") = dr("numSourceID")
                        drow("vcItemDesc") = dr("vcItemDesc")
                        drow("numWarehouseID") = dr("numWarehouseID")
                        drow("vcItemName") = dr("vcItemName")
                        drow("Warehouse") = dr("Warehouse")
                        drow("numWarehouseItmsID") = dr("numWarehouseItmsID")
                        drow("ItemType") = dr("vcType")
                        drow("Attributes") = dr("vcAttributes")
                        drow("Op_Flag") = 1 '1 for insertion
                        drow("DropShip") = dr("bitDropShip")
                        drow("bitDiscountType") = dr("bitDiscountType")
                        drow("fltDiscount") = String.Format("{0:0.00}", dr("fltDiscount"))
                        drow("monTotAmtBefDiscount") = String.Format("{0:0.00}", dr("monTotAmtBefDiscount"))
                        drow("numUOM") = dr("numUOMId")
                        drow("vcUOMName") = dr("vcUOMName")
                        drow("UOMConversionFactor") = dr("UOMConversionFactor")
                        drow("bitWorkOrder") = dr("bitWorkOrder")
                        drow("charItemType") = dr("charItemType")

                        drow("numVendorWareHouse") = 0
                        drow("numShipmentMethod") = 0
                        drow("numSOVendorId") = dr("numSOVendorId")
                        drow("numProjectID") = 0
                        drow("numProjectStageID") = 0
                        drow("vcBaseUOMName") = dr("vcBaseUOMName")
                        drow("bitIsAuthBizDoc") = False

                        'Calculate Tax 
                        objItems.DomainID = Session("DomainID")
                        objItems.ItemCode = dr("numItemCode")
                        dtItemTax = objItems.ItemTax()
                        Dim strApplicable1 As String = ""
                        For Each dr1 As DataRow In dtItemTax.Rows
                            strApplicable1 = strApplicable1 & dr1("bitApplicable") & ","
                        Next
                        strApplicable1 = strApplicable1 & dr("bitTaxable")
                        CType(order1.FindControl("Taxable"), HtmlInputHidden).Value = strApplicable1

                        Dim strApplicable(), strTax() As String
                        strApplicable = CType(order1.FindControl("Taxable"), HtmlInputHidden).Value.Split(",")
                        strTax = CType(order1.FindControl("txtTax"), TextBox).Text.Split(",")
                        For k = 0 To chkTaxItems.Items.Count - 1
                            If strApplicable(k) = True Then
                                drow("Tax" & chkTaxItems.Items(k).Value) = String.Format("{0:0.00}", strTax(k) * dr("monTotAmount") / 100)
                                drow("bitTaxable" & chkTaxItems.Items(k).Value) = True
                            Else
                                drow("Tax" & chkTaxItems.Items(k).Value) = 0
                                drow("bitTaxable" & chkTaxItems.Items(k).Value) = False
                            End If
                        Next
                        i += 1
                        ds.Tables(0).Rows.Add(drow)
                    Next
                    ds.Tables(0).AcceptChanges()
                    ViewState("SOItems") = ds

                    order1.UpdateDetails()
                    'CType(order1.FindControl("dgItems"), DataGrid).DataSource = ds
                    'CType(order1.FindControl("dgItems"), DataGrid).DataBind()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub
        Private Sub BindSalesTemplate()
            Try
                If radCmbCompany.SelectedValue.Length > 0 Then
                    Dim objOpp As New COpportunities
                    Dim dtTemplate As DataTable
                    objOpp.DomainID = Session("DomainID")
                    objOpp.byteMode = 1
                    objOpp.DivisionID = IIf(radCmbCompany.SelectedValue.Length > 0, radCmbCompany.SelectedValue, 0)
                    dtTemplate = objOpp.GetSalesTemplate()
                    ddlSalesTemplate.DataTextField = "vcTemplateName"
                    ddlSalesTemplate.DataValueField = "numSalesTemplateID1"
                    ddlSalesTemplate.DataSource = dtTemplate
                    ddlSalesTemplate.DataBind()
                    ddlSalesTemplate.Items.Insert(0, "--Select One--")
                    ddlSalesTemplate.Items.FindByText("--Select One--").Value = "0"
                Else
                    ddlSalesTemplate.Items.Insert(0, "--Select One--")
                    ddlSalesTemplate.Items.FindByText("--Select One--").Value = "0"
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub BindMultiCurrency()
            Try
                If Session("MultiCurrency") = True Then
                    pnlCurrency.Visible = True
                    Dim objCurrency As New CurrencyRates
                    objCurrency.DomainID = Session("DomainID")
                    objCurrency.GetAll = 0
                    ddlCurrency.DataSource = objCurrency.GetCurrencyWithRates()
                    ddlCurrency.DataTextField = "vcCurrencyDesc"
                    ddlCurrency.DataValueField = "numCurrencyID"
                    ddlCurrency.DataBind()
                    ddlCurrency.Items.Insert(0, "--Select One--")
                    ddlCurrency.Items.FindByText("--Select One--").Value = "0"
                    If Not ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")) Is Nothing Then
                        ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")).Selected = True
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub btnTemp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTemp.Click
            Try
                If (CCommon.ToLong(CType(order1.FindControl("hdnBillAddressID"), HiddenField).Value) > 0) Then
                    BindAddressByAddressID(CCommon.ToLong(CType(order1.FindControl("hdnBillAddressID"), HiddenField).Value), "Bill")
                End If
                If (CCommon.ToLong(CType(order1.FindControl("hdnShipAddressID"), HiddenField).Value) > 0) Then
                    BindAddressByAddressID(CCommon.ToLong(CType(order1.FindControl("hdnShipAddressID"), HiddenField).Value), "Ship")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub
        Private Sub BindAddressByAddressID(ByVal lngAddressID As Long, ByVal AddType As String)
            Try
                Dim objContact As New CContacts
                objContact.DomainID = Session("DomainID")
                objContact.AddressID = lngAddressID
                objContact.byteMode = 1
                Dim dtTable As DataTable = objContact.GetAddressDetail
                If dtTable.Rows.Count > 0 Then
                    If AddType = "Bill" Then
                        lblBillTo1.Text = CCommon.ToString(dtTable.Rows(0)("vcFullAddress"))
                        lblBillTo2.Text = ""
                    Else
                        lblShipTo1.Text = CCommon.ToString(dtTable.Rows(0)("vcFullAddress"))
                        lblShipTo2.Text = ""
                    End If
                Else
                    lblBillTo1.Text = ""
                    lblBillTo2.Text = ""
                    lblShipTo1.Text = ""
                    lblShipTo2.Text = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Private Sub lnkClick_Click(sender As Object, e As System.EventArgs) Handles lnkClick.Click
            Try
                tblMultipleOrderMessage.Visible = False
                ViewState("SOItems") = Nothing
                Response.Redirect(Request.Url.ToString())
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            lblException.Text = ex
            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ScrollTo", "ScrollToError();", True)
        End Sub
    End Class
End Namespace