<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmChangeDefaultAdd.aspx.vb"
    Inherits=".frmChangeDefaultAdd" MasterPageFile="~/common/PopupBootstrap.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Use following Address</title>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="pull-right">
        <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save &amp; Close"></asp:Button>
        <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" Text="Close" OnClientClick="return Close();" />
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Use Following Address
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="form-inline">
                <div class="radio col-xs-4">
                    <label>
                        <asp:RadioButton ID="rbCompany" runat="server" GroupName="Address" AutoPostBack="true" Checked="true" />
                        <b>Use Company Address</b>
                    </label>
                </div>
                <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="200" DropDownWidth="500px"
                    OnClientItemsRequested="OnClientItemsRequestedOrganization"
                    ClientIDMode="Static"
                    ShowMoreResultsBox="true"
                    Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True">
                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                </telerik:RadComboBox>
                <div class="form-group">
                    <label>&nbsp;&nbsp;Address</label>
                    <asp:DropDownList runat="server" ID="ddlAddressName" CssClass="form-control" AutoPostBack="true" Width="150">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="form-inline">
                <div class="radio col-xs-4">
                    <label>
                        <asp:RadioButton ID="rbContact" runat="server" GroupName="Address" AutoPostBack="true" />
                        <b>Use Contact Address</b>
                    </label>
                </div>
                <asp:DropDownList ID="ddlContact" CssClass="form-control" Width="200" runat="server" AutoPostBack="true">
                </asp:DropDownList>
                <div class="form-group">
                    <label>&nbsp;&nbsp;Address</label>
                    <asp:DropDownList ID="ddlAddressType" CssClass="form-control" runat="server" AutoPostBack="true" Width="150">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="form-inline">
                <div class="radio col-xs-4">
                    <label>
                        <asp:RadioButton ID="rbProject" runat="server" GroupName="Address" AutoPostBack="true" />
                        <b>Use Project Address</b>
                    </label>
                </div>
                <asp:DropDownList ID="ddlProject" CssClass="form-control" Width="200" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlProject_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="row padbottom10" id="divWarehouse" runat="server">
        <div class="col-xs-12">
            <div class="form-inline">
                <div class="radio col-xs-4">
                    <label>
                        <asp:RadioButton ID="rbWarehouse" runat="server" GroupName="Address" AutoPostBack="true" />
                        <b>Use Warehouse Address</b>
                    </label>
                </div>
                <asp:DropDownList ID="ddlWarehouse" CssClass="form-control" Width="200" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlWarehouse_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group" style="padding-left:20px;">
                <label>Address:</label>
                <div>
                <asp:Label ID="lblFullAddress" runat="server" CssClass="text" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
