﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAddEditOrder.aspx.vb"
    Inherits=".frmAddEditOrder" MasterPageFile="~/common/SalesOrder.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="order.ascx" TagName="order" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Add/Edit Order</title>
    <style type="text/css">
        .column {
            float: left;
        }

        html > body .RadComboBoxDropDown_Vista .rcbItem, html > body .RadComboBoxDropDown_Vista .rcbHovered, html > body .RadComboBoxDropDown_Vista .rcbDisabled, html > body .RadComboBoxDropDown_Vista .rcbLoading {
            display: inline-block;
        }
    </style>
    
    <script language="javascript" type="text/javascript">

        function Save(a) {
        }

        function Add(a) {

            if ($find('radWareHouse') != null) {
                if ($find('radWareHouse').get_value() == '') {
                    alert("Select Warehouse")
                    return false;
                }
            }
            if ($ControlFind('txtUnits').value == "") {
                alert("Enter Units")
                $ControlFind('txtUnits').focus();
                return false;
            }
            if (parseFloat($ControlFind('txtUnits').value) <= 0) {
                alert("Units must greater than 0.")
                $ControlFind('txtUnits').focus();
                return false;
            }
            if ($ControlFind('txtprice').value == "") {
                alert("Enter Price")
                $ControlFind('txtprice').focus();
                return false;
            }


            var table = $ControlFind('order1_dgItems');
            var WareHouesID;
            //            alert($('radWareHouse'));
            if ($('radWareHouse') == null) {
                WareHouesID = '';
            }
            else {
                WareHouesID = $find('radWareHouse').get_value();
            }

            if (a == 1) {
                if ($ControlFind('chkDropShip').checked == true && $ControlFind('hdnPrimaryVendorID').value == "0") {
                    alert("You must add a primary vendor to this item first")
                    return false;
                }
            }

            var dropship;
            if ($('chkDropShip').checked == true) {
                dropship = 'True';
                WareHouesID = '';
            }
            else {
                dropship = 'False';
            }
        }

        function SetWareHouseId(id) {
            var combo = $find('radWareHouse')
            var node = combo.findItemByValue(id);
            node.select();
        }

        function parseCurrency(num) {
            return parseFloat(num.replace(/,/g, ''));
        }

        function validateQty(txtUnitsID, OppType, oppItemCode) {
            //console.log(parseFloat(document.getElementById(txtUnitsID).value));
            //console.log(parseFloat(document.getElementById(txtUnitsID.replace('txtUnits', 'txtOldUnits')).value));
            //console.log(parseFloat(document.getElementById(txtUnitsID.replace('txtUnits', 'txtUOMConversionFactor')).value));
            //console.log(parseFloat(document.getElementById(txtUnitsID.replace('txtUnits', 'txtnumUnitHourReceived')).value));
            //console.log(parseFloat(document.getElementById(txtUnitsID.replace('txtUnits', 'txtnumQtyShipped')).value));

            var Units = isNaN(parseCurrency(document.getElementById(txtUnitsID).value)) ? 0 : parseCurrency(document.getElementById(txtUnitsID).value);
            var OldUnits = isNaN(parseCurrency(document.getElementById(txtUnitsID.replace('txtUnits', 'txtOldUnits')).value)) ? 0 : parseCurrency(document.getElementById(txtUnitsID.replace('txtUnits', 'txtOldUnits')).value);
            var UOMConversionFactor = isNaN(parseCurrency(document.getElementById(txtUnitsID.replace('txtUnits', 'txtUOMConversionFactor')).value)) ? 0 : parseCurrency(document.getElementById(txtUnitsID.replace('txtUnits', 'txtUOMConversionFactor')).value);
            var numUnitHourReceived = isNaN(parseCurrency(document.getElementById(txtUnitsID.replace('txtUnits', 'txtnumUnitHourReceived')).value)) ? 0 : parseCurrency(document.getElementById(txtUnitsID.replace('txtUnits', 'txtnumUnitHourReceived')).value);
            var numQtyShipped = isNaN(parseCurrency(document.getElementById(txtUnitsID.replace('txtUnits', 'txtnumQtyShipped')).value)) ? 0 : parseCurrency(document.getElementById(txtUnitsID.replace('txtUnits', 'txtnumQtyShipped')).value);

            //console.log(Units);
            //console.log(OldUnits);
            //console.log(UOMConversionFactor);
            //console.log(numUnitHourReceived);
            //console.log(numQtyShipped);

            if (OppType == 1) {
                if (parseFloat(numQtyShipped) > parseFloat(Units * UOMConversionFactor)) {
                    document.getElementById(txtUnitsID).value = OldUnits;

                    //console.log('From OppType 1');
                    CalculateTotal();

                    alert('Given input is not valid, As ' + numQtyShipped + ' units of selected item is already received.');
                    return false;
                }
            }
            else if (OppType == 2) {
                if (parseFloat(numUnitHourReceived) > parseFloat(Units * UOMConversionFactor)) {
                    document.getElementById(txtUnitsID).value = OldUnits;

                    //console.log('From OppType 2');
                    CalculateTotal();

                    alert('Given input is not valid, As ' + numUnitHourReceived + ' units of selected item is already shipped.');
                    return false;
                }
            }

            CalculateTotal();

            if (OppType == 1) {
                $("#hdnOppItemCodeToUpdatePrice").val(oppItemCode);
                $("#hdnItemQuantity").val(Units);
            }

            return true;
        }

        function CalculateTotal() {
            var Total, subTotal, TaxAmount, ShipAmt, IndTaxAmt, discount;;
            var index, findex;
            Total = 0;
            TaxAmount = 0;
            subTotal = 0;
            IndTaxAmt = 0;
            ShipAmt = 0;

            jQuery('#ctl00_Content_order1_dgItems > tbody > tr').not(':first,:last').each(function () {
                if (parseFloat(jQuery(this).find("input[id*='txtUnits']").val()) <= 0) {
                    alert("Units must greater than 0.")
                    jQuery(this).find("input[id*='txtUnits']").focus();
                    return false;
                }

                var txtUnits, txtUOMConversionFactor, txtUnitPrice, txtTotalDiscount;
                txtUnits = isNaN(parseCurrency(jQuery(this).find("input[id*='txtUnits']").val())) ? 0 : parseCurrency(jQuery(this).find("input[id*='txtUnits']").val());
                txtUOMConversionFactor = isNaN(parseCurrency(jQuery(this).find("input[id*='txtUOMConversionFactor']").val())) ? 1 : parseCurrency(jQuery(this).find("input[id*='txtUOMConversionFactor']").val());
                txtUnitPrice = isNaN(parseCurrency(jQuery(this).find("input[id*='txtUnitPrice']").val())) ? 0 : parseCurrency(jQuery(this).find("input[id*='txtUnitPrice']").val());
                txtTotalDiscount = isNaN(parseCurrency(jQuery(this).find("input[id*='txtTotalDiscount']").val())) ? 0 : parseCurrency(jQuery(this).find("input[id*='txtTotalDiscount']").val());
                var hdnMarkupDiscountVal = jQuery(this).find("input[id*='hdnMarkupDiscount']").val();

                //WHEN Item Level UOM is Enabled we are displying total price in unitprice textbox
                //So for exammple, if 1 6Packs = 1 Can and order uom is 6 Packs then unit price is multiplied by 6.
                //to get unit price of single unit we have to divide by conversion factor
                if ($("#hdnEnabledItemLevelUOM").val() == "True") {
                    txtUnitPrice = parseCurrency(txtUnitPrice / txtUOMConversionFactor);
                }

                jQuery(this).find("input[id*='hdnUnitPrice']").val(txtUnitPrice);

                txtUnitPrice = parseCurrency(jQuery(this).find("input[id*='hdnUnitPrice']").val());
                subTotal = (txtUnits * txtUOMConversionFactor) * txtUnitPrice;

                if (jQuery(this).find("input[id*='txtDiscountType']").val() != undefined) {

                    if (jQuery(this).find("input[id*='txtDiscountType']").val() == 'True') {

                        if (hdnMarkupDiscountVal != undefined) {
                            if (hdnMarkupDiscountVal == "1") {
                                subTotal = subTotal + txtTotalDiscount;
                            }
                            else {
                                subTotal = subTotal - txtTotalDiscount;
                            }
                        }
                        else {
                            subTotal = subTotal - txtTotalDiscount;
                        }
                    }
                    else {
                        discount = (subTotal * txtTotalDiscount) / 100;
                        if (hdnMarkupDiscountVal != undefined) {
                            if (hdnMarkupDiscountVal == "1") {
                                subTotal = subTotal + discount;
                            }
                            else {
                                subTotal = subTotal - discount;
                            }
                        }
                        else {
                            subTotal = subTotal - discount;
                        }
                        jQuery(this).find("[id*='lblDiscount']").text(CommaFormatted(roundNumber(discount, 2)) + ' (' + txtTotalDiscount + '%)');
                    }
                }

                jQuery(this).find("[id*='lblTotal']").text(CommaFormatted(roundNumber(subTotal, 2)));

                Total = parseFloat(Total) + parseFloat(subTotal);
            });

            jQuery('#ctl00_Content_order1_dgItems > tbody > tr:last').find("[id*='lblFTotal']").text(CommaFormatted(roundNumber((Total), 2)));

            jQuery('#lblTotal').text(CommaFormatted(roundNumber((Total) + parseFloat(TaxAmount) + parseFloat(ShipAmt), 2)));

        }

        function openItem(a, b, c, d, e) {

            if (document.getElementById(b).value == "") {
                b = 0
            }
            else {
                b = document.getElementById(b).value
            }

            var CalPrice = 0;
            if ($ControlFind('hdKit').value == 'True' && ($ControlFind('dgKitItem') != null)) {
                jQuery('ctl00_Content_order1_dgKitItem tr').not(':first,:last').each(function () {
                    var str;
                    str = jQuery(this).find("[id*='lblUnitPrice']").text();
                    CalPrice = CalPrice + (parseCurrency(str.split('/')[0]) * jQuery(this).find("input[id*='txtQuantity']").val());
                });
            }

            //            var CalPrice = 0;
            //            if (document.getElementById('hdKit').value == 'True' && (document.getElementById('dgKitItems') != null)) {

            //                var index;
            //                for (i = 1; i < document.getElementById('dgKitItems').rows.length; i++) {
            //                    if (i <= 8) {
            //                        index = '0' + (i + 1)
            //                    }
            //                    else {
            //                        index = (i + 1);
            //                    }
            //                    var str;
            //                    if (document.all) {
            //                        str = document.getElementById('dgKitItems_ctl' + index + '_lblUnitPrice').innerText;
            //                    } else {
            //                        str = document.getElementById('dgKitItems_ctl' + index + '_lblUnitPrice').textContent;
            //                    }
            //                    CalPrice = CalPrice + (parseFloat(str.split('/')[0]) * document.getElementById('dgKitItems_ctl' + index + '_txtQuantity').value)

            //                }
            //            }
            var WareHouseItemID = 0;
            if ($find('radWareHouse') != null) {
                if ($find('radWareHouse').get_value() == "") {
                    WareHouseItemID = 0
                }
                else {
                    WareHouseItemID = $find('radWareHouse').get_value()
                }
            }
            if (a != 0 && b != 0) {
                window.open('../opportunity/frmItemPriceRecommd.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=' + a + '&Unit=' + b + '&DivID=' + c + '&OppType=' + e + '&CalPrice=' + CalPrice + '&WarehouseItemID=' + WareHouseItemID, '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')

            }
        }

        function ValidateShipperAccount() {
            if (document.getElementById('hdnShipperAccountNo') != null) {
                if (document.getElementById('hdnShipperAccountNo').value.toString() != '') {
                    return true;
                }
                else {
                    if ($("[id$=hdnCmbDivisionID]").val() != "") {
                        var myWidth = 750;
                        var myHeight = 700;
                        var left = (screen.width - myWidth) / 2;
                        var top = (screen.height - myHeight) / 4;
                        window.open('../account/frmConfigParcelShippingAccount.aspx?IsFromNewOrder=1&DivID=' + $("[id$=hdnCmbDivisionID]").val(), '', 'toolbar=no,titlebar=no,left=' + left + ',top=' + top + ',width=' + myWidth + ',height=' + myHeight + ',scrollbars=yes,resizable=yes');
                        document.getElementById('chkUseCustomerShippingAccount').checked = false;
                        return false;
                    }
                }
            }
        }

        function UpdateShipperAccountNo(shipperAccountNo) {
            if (document.getElementById('hdnShipperAccountNo') != null) {
                document.getElementById('hdnShipperAccountNo').value = shipperAccountNo
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanelException" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Label ID="lblException" runat="server" Style="color: red;"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Add/Edit Order
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="100%" runat="server" id="tblMultipleOrderMessage" visible="false" style="background-color: #FFFFFF; color: red">
        <tr>
            <td class="normal4" align="center">You can not create multiple orders simultaneously,
                <asp:LinkButton Text="Click here" runat="server" ID="lnkClick" />
                to clear other order and proceed.
            </td>
        </tr>
    </table>
    <div class="row">
        <div class="col-xs-12 text-center">
            <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
        </div>
    </div>
    <div class="container customer-detail">
        <div class="grid grid-pad">
            <uc1:order ID="order1" runat="server" />
        </div>
    </div>
    <asp:HiddenField ID="hdnCmbDivisionID" runat="server" />
    <asp:HiddenField ID="hdnCmbOppType" runat="server" />
    <asp:HiddenField ID="hdnCurrencyID" runat="server" />
</asp:Content>
