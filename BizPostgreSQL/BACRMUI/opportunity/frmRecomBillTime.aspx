<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmRecomBillTime.aspx.vb"
    Inherits=".opportunity_frmRecomBillTime" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Recommended Rate/Hour</title>

    <script language="javascript">
        function Close() {

            if (document.all) {
                window.opener.FillBox(document.getElementById('lblRate').innerText, document.form1.ddlItem.value)
            } else {
                window.opener.FillBox(document.getElementById('lblRate').textContent, document.form1.ddlItem.value)
            }
            window.close();
            return false;
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <br />
    <br />
    <asp:Table ID="Table1" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" BorderColor="black" GridLines="None" Height="160">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <table align="center">
                    <tr>
                        <td class="normal1">
                            Item
                        </td>
                        <td>
                            <asp:TextBox ID="txtItem" runat="server" Width="100" CssClass="signup"></asp:TextBox>
                            &nbsp;
                            <asp:Button ID="btnGo" runat="server" Text="Go" CssClass="button"></asp:Button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:DropDownList ID="ddlItem" CssClass="signup" runat="server" AutoPostBack="True"
                                Width="200">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="normal1">
                            Recommended Rate/Hour :
                            <asp:Label ID="lblRate" runat="server"></asp:Label>
                            &nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <asp:Button ID="btnAdd" runat="server" Width="50" CssClass="button" Text="Add" />
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </form>
</body>
</html>
