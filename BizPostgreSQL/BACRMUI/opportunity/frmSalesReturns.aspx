﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSalesReturns.aspx.vb"
    Inherits="BACRM.UserInterface.Opportunities.frmSalesReturns" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="menu1" TagName="Menu" Src="../include/webmenu.ascx" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="../common/frmBizSorting.ascx" TagName="frmBizSorting" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title runat="server" id="PageTitle"></title>
    <script type="text/javascript">
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function pageLoaded() {
            //OVERRIDE EXISTING FUNCTION WHICH IS IN MASTERPAGE : CODE UTILITY USED.
            window.OpenBizInvoice = function (a, b, c) {
                window.open('../opportunity/frmMirrorBizdoc.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&RefID=' + a + '&RefType=' + b + '&Print=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
                return false;
            };
        }

        function OpenOpp(a, b) {

            var str;
            str = "../opportunity/frmReturnDetail.aspx?type=" + b + "&ReturnID=" + a;
            document.location.href = str;
        }
        function SubmitForPayment(a, b, c) {
            var str;
            str = "../opportunity/frmSubmitReturnForPayment.aspx?OpID=" + a + "&ReturnID=" + b + "&type=" + c;
            window.open(str, '', 'toolbar=no,titlebar=no,left=300,top=200,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenItem(a) {
            window.location.href = '../Items/frmKitDetails.aspx?ItemCode=' + a;
            return false;
        }

        function DeleteRecord() {
            var i = 0;
            $("[id$=gvSalesReturns] INPUT[type='checkbox']").each(function () {
                if (this.checked) {
                    if (this.id.indexOf("chkSelectAll") == -1) {
                        i += 1;
                    }
                }
            });

            if (i == 0) {
                alert('Please select atleast one record!!');
                return false;
            }
            return true;
        }
        function OpenHelp() {
            window.open('../Help/Sales_Returns.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
               
            </div>
            <div class="pull-right">
                <button id="btnAddNewReturn" class="btn btn-primary" runat="server" ><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New</button>
                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="alert alert-warning" id="divAlert" runat="server" visible="false">
        <h4><i class="icon fa fa-warning"></i>Alert!</h4>
        <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblReturns" runat="server"></asp:Label>&nbsp;<a href="#" onclick="return OpenHelpPopUp('opportunity/frmsalesreturns.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ContentPlaceHolderID="GridSettingPopup" runat="server" ClientIDMode="Static">
    <asp:LinkButton ID="lnkClearFilter" runat="server" ToolTip="Clear All Filters" OnClick="lnkClearFilter_Click"><i class="fa fa-lg fa-filter"></i></asp:LinkButton>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
        ShowPageIndexBox="Never"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
     <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="table-responsive">
        <asp:GridView ID="gvSalesReturns" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-striped" Width="100%" AllowSorting="True" DataKeyNames="numReturnHeaderID,numOppId" ShowHeaderWhenEmpty="true">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:LinkButton ID="lbCreatedDate" runat="server" Font-Bold="true" Text="Date Created" CommandName="Sort" CommandArgument="dtCreateDate"></asp:LinkButton>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("dtCreateDate")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:LinkButton ID="lbCustomer" runat="server" Font-Bold="true" Text="Customer" CommandName="Sort" CommandArgument="vcCompanyName"></asp:LinkButton>
                        <br />
                        <asp:TextBox ID="txtCustomer" runat="server" Width="180" OnTextChanged="txtCustomer_TextChanged" AutoPostBack="true"></asp:TextBox>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("vcCompanyName")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:LinkButton ID="lbRMA" runat="server" Font-Bold="true" Text="RMA #" CommandName="Sort" CommandArgument="vcRMA"></asp:LinkButton>
                        <br />
                        <asp:TextBox ID="txtRMA" runat="server" Width="180" OnTextChanged="txtRMA_TextChanged" AutoPostBack="true"></asp:TextBox>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <a href="javascript:OpenOpp('<%# Eval("numReturnHeaderID") %>','<%# Eval("tintReturnType") %>')">
                            <%# Eval("vcRMA") %></a>
                        <asp:LinkButton ID="lblSalesCreditMemo" runat="server" Visible="false"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:LinkButton ID="lbCreditFromAccount" runat="server" Font-Bold="true" Text="Credit From Account" CommandName="Sort" CommandArgument="vcCreditFromAccount"></asp:LinkButton>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("vcCreditFromAccount")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:LinkButton ID="lbReturn" runat="server" Font-Bold="true" Text="Reason for Return" CommandName="Sort" CommandArgument="ReasonforReturn"></asp:LinkButton>
                        <br />
                        <asp:DropDownList ID="ddlReason" runat="server" Width="120" OnSelectedIndexChanged="ddlReason_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <%# Eval("ReasonforReturn")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:LinkButton ID="lbStatus" runat="server" Font-Bold="true" Text="Status" CommandName="Sort" CommandArgument="Status"></asp:LinkButton>
                        <br />
                        <asp:DropDownList ID="ddlStatus" runat="server" Width="120" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        <asp:CheckBox ID="chkAll" runat="server" CssClass="chkAll" onclick="SelectAll('chkAll','chkSelect')" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox ID="chk" runat="server" CssClass="chkSelect" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                No data available
            </EmptyDataTemplate>
        </asp:GridView>
    </div>

    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:ValidationSummary ID="valSummary" runat="server" DisplayMode="BulletList" ShowMessageBox="true"
        ShowSummary="false" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:HiddenField ID="hdnOrgSearch" runat="server" />
    <asp:HiddenField ID="hdnRMASearch" runat="server" />
    <asp:HiddenField ID="hdnStatus" runat="server" />
    <asp:HiddenField ID="hdnReason" runat="server" />
    <asp:HiddenField ID="hdnSortColumn" runat="server" />
    <asp:HiddenField ID="hdnSortDirection" runat="server" />
</asp:Content>

