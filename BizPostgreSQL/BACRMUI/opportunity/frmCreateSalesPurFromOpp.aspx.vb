Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Workflow
Imports Telerik.Web.UI

Partial Public Class frmCreateSalesPurFromOpp
    Inherits BACRMPage

    Dim dsTemp As DataSet
    Dim lngOppID As Long
    Dim dtOppBiDocItems As DataTable
    Dim dtOppBiDocDtl As DataTable
    Dim dtVendors As DataTable
    Dim OppBizDocID As Long
    Dim ddlVendor As DropDownList
    Dim objItems As New CItems
    Public OppType As Short
    Dim objProspects As CProspects
    Dim dtUnit As DataTable
    Dim dtWareHouse As DataTable
    Dim dsSOShipAddress As DataSet

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbBillOther)
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbShipOther)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'HIDE ERROR DIV
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            'HIDE ALERT DIV
            divAlert.Visible = False
            litMessage.Text = ""

            divSuccess.Visible = False
            litSuccessMessage.Text = ""

            OppType = CCommon.ToShort(GetQueryStringVal("OppType"))
            If Not IsPostBack Then
                hdnOppID.Value = CCommon.ToLong(GetQueryStringVal("OppID"))
                hdnMSCurrencyID.Value = CCommon.ToString(Session("Currency"))

                Dim objOpportunity As New OppotunitiesIP
                objOpportunity.OpportunityId = GetQueryStringVal("OppID")
                objOpportunity.DomainID = Session("DomainID")

                Dim dtDetails As DataTable = objOpportunity.OpportunityDTL.Tables(0)
                If dtDetails.Rows.Count > 0 Then
                    hdnDivID.Value = CCommon.ToLong(dtDetails.Rows(0).Item("numDivisionID"))
                    hdnCustomerName.Value = CCommon.ToString(dtDetails.Rows(0).Item("vcCompanyName"))
                End If

                LoadItems()
                LoadBizDocs()

                radCmbBillOther.Visible = False
                radCmbBillOtherAddress.Visible = False

                radCmbShipOther.Visible = False
                radCmbShipOtherAddress.Visible = False

                If OppType = 1 Then
                    btnDisassociate.Visible = True
                    btnMatchPO.Visible = True
                    divSingleShipAddress.Visible = True
                    divShippingAddress.Style.Add("display", "none")
                    Page.Title = "Create & Associate Purchase Orders from items in Sales Order"
                    lblHeading.Text = "Create & Associate Purchase Orders from items in Sales Order"
                    aHelp.Attributes.Add("onclick", "return OpenHelpPopUp('opportunity/frmcreatesalespurfromopp.aspx?opptype=1')")
                    btnSave.Text = "Create Purchase Order"
                    chkShowOnlyBOItems.Visible = True
                    btnUseReplenishmentFormula.Visible = True
                    divPreferredVendor.Visible = True
                    divVendor1.Style.Add("display", "none")
                    divVendor2.Style.Add("display", "none")
                    divVendor3.Style.Add("display", "none")
                    cbPreferredVendor.Text = "Use this vendor instead"
                    lblCompany.Text = "Vendor"
                    radBillEmployer.Text = " Use " & CCommon.ToString(Session("CompanyName"))
                    radShipEmployer.Text = " Use " & CCommon.ToString(Session("CompanyName"))

                    radCmbCompany.EmptyMessage = "Select Vendor"

                    radBillCustomer.Visible = False
                    radCmbBillCustomerAddress.Visible = False
                    radShipCustomer.Visible = False
                    radCmbShipCustomerAddress.Visible = False

                    If Session("DefaultBillToForPO") = 1 Then
                        radBillEmployer.Checked = True
                        radBillEmployer_CheckedChanged(Nothing, Nothing)
                    End If
                    If Session("DefaultBillToForPO") = 2 Then
                        radBillSOCustomer.Checked = True
                        radBillSOCustomer_CheckedChanged(Nothing, Nothing)
                    End If
                    If Session("DefaultShipToForPO") = 1 Then
                        radShipEmployer.Checked = True
                        radShipEmployer_CheckedChanged(Nothing, Nothing)
                    End If
                    If Session("DefaultShipToForPO") = 2 Then
                        radShipSO.Checked = True
                        radShipSO_CheckedChanged(Nothing, Nothing)
                    End If

                Else
                    radBillSOCustomer.Visible = False
                    radShipSO.Visible = False
                    radBillCustomer.Checked = True
                    radShipCustomer.Checked = True
                    radBillCustomer_CheckedChanged(Nothing, Nothing)
                    radShipCustomer_CheckedChanged(Nothing, Nothing)

                    Page.Title = "Create Sales Order From Purchase Order"
                    lblHeading.Text = "Create Sales Order From Purchase Order"
                    aHelp.Attributes.Add("onclick", "return OpenHelpPopUp('opportunity/frmcreatesalespurfromopp.aspx?opptype=2')")
                    btnSave.Text = "Create Sales Order"
                    radBillCustomer.Text = "Customer"
                    cbPreferredVendor.Visible = False
                    cbPreferredVendor.Text = "Use this customer instead"
                    lblCompany.Text = "Customer"
                End If

                btnClose.Attributes.Add("onclick", "return Close()")
            End If
            If GetQueryStringVal("Created") = "True" Then
                Dim strScript As String
                strScript = "<script>OpenOpp('" & GetQueryStringVal("NewOppID") & "');</script>"
                If (Not ClientScript.IsStartupScriptRegistered("OpenOpp")) Then
                    Page.ClientScript.RegisterStartupScript(Me.GetType(), "OpenOpp", strScript)
                End If
                DisplaySuccess("Deal Is Created")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Sub LoadBizDocs()
        Try
            Dim objBizDoc As New OppBizDocs
            Dim ds As DataSet
            objBizDoc.OppId = GetQueryStringVal("OppID")
            objBizDoc.IsAuthoritativeBizDoc = 1
            ds = objBizDoc.GetBizDocsInOpp()
            If ds.Tables.Count = 1 Then
                If (ds.Tables(0).Rows.Count > 0) Then
                    ddlInvoices.DataSource = ds.Tables(0).DefaultView
                    ddlInvoices.DataTextField = "vcBizDocID"
                    ddlInvoices.DataValueField = "numOppBizDocsId"
                    ddlInvoices.DataBind()
                    ddlInvoices.Items.Insert(0, New ListItem("---Select One---", "0"))
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub LoadItems()
        Try
            If chkFromExisting.Checked And Not (ddlInvoices.SelectedIndex = 0) Then
                Dim objBizDoc As New OppBizDocs
                Dim ds As DataSet
                objBizDoc.OppId = GetQueryStringVal("OppID")
                objBizDoc.DomainID = Session("DomainID")
                objBizDoc.OppBizDocId = ddlInvoices.SelectedValue
                ds = objBizDoc.GetOppInItems()
                'ds.Tables(0).Columns("ItemCode").ColumnName = "numItemCode"
                ds.Tables(0).Columns("charItemType").ColumnName = "vcType"
                ds.Tables(0).Columns("ItemType").ColumnName = "charItemType"
                ds.Tables(0).Columns("DropShip").ColumnName = "bitDropShip"
                ds.Tables(0).Columns("VendorCost").ColumnName = "monVendorCost"
                ds.Tables(0).Columns("txtItemDesc").ColumnName = "vcItemDesc"
                'ds.Tables(0).Columns("txtNotes").ColumnName = "vcNotes"
                Dim dtItems As DataTable = ds.Tables(0)
                dgItem.DataSource = dtItems
                dgItem.DataBind()

                'Set Ship to address to Customer's address with contact name
                radShipOther.Checked = True
                radCmbShipOther.Visible = True
                radCmbShipOtherAddress.Visible = True
            Else
                Dim objOpportunity As New MOpportunity
                objOpportunity.OpportunityId = GetQueryStringVal("OppID")
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.Mode = 3

                Dim ds As DataSet
                ds = objOpportunity.GetOrderItems()

                Dim dvSearchableTable As DataView
                dvSearchableTable = ds.Tables(0).DefaultView
                If OppType = 1 Then
                    'dvSearchableTable.RowFilter = "monPrice>0"
                End If
                dgItem.DataSource = dvSearchableTable
                dgItem.DataBind()
            End If

            'Hide vendor column when creating SO from PO
            If OppType = 2 Then
                dgItem.Columns(12).Visible = False
                dgItem.Columns(13).Visible = False
                dgItem.Columns(14).Visible = False
                dgItem.Columns(15).Visible = False
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgItem_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgItem.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Header Then
                If OppType = 1 Then
                    Dim chk As CheckBox = e.Item.FindControl("chkSelectAll")

                    Dim objOpp As New OppotunitiesIP
                    objOpp.DomainID = CCommon.ToLong(Session("DomainID"))
                    objOpp.OpportunityId = CCommon.ToLong(GetQueryStringVal("OppID"))
                    dsSOShipAddress = objOpp.GetOpportunityAddress(2)

                    Dim objItem As New CItems
                    objItem.DomainID = Session("DomainID")
                    dtWareHouse = objItem.GetWareHouses()
                End If

                If objCommon Is Nothing Then
                    objCommon = New CCommon
                End If

                objCommon.DomainID = Session("DomainID")
                objCommon.UOMAll = True
                dtUnit = objCommon.GetItemUOM()
            End If
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

                Dim chk As CheckBox = DirectCast(e.Item.FindControl("chkSelect"), CheckBox)
                chk.Checked = True

                Dim txtPrice As TextBox = DirectCast(e.Item.FindControl("txtPrice"), TextBox)
                txtPrice.Text = String.Format("{0:#,##0.00###}", Math.Abs(CCommon.ToDecimal(txtPrice.Text)))
                txtPrice.Attributes.Add("onkeydown", "CheckNumber(1,event)")

                Dim ddlUOM As DropDownList = DirectCast(e.Item.FindControl("ddlUOM"), DropDownList)

                If DataBinder.Eval(e.Item.DataItem, "charItemType") = "S" Then
                    CType(e.Item.FindControl("txtUnits"), TextBox).Attributes.Add("onkeydown", "CheckNumber(1,event)")
                Else
                    CType(e.Item.FindControl("txtUnits"), TextBox).Attributes.Add("onkeydown", "CheckNumber(2,event)")
                End If

                'Only create vendor dropdown when creating PO from SO
                If OppType = 1 Then
                    Dim chkDropship As CheckBox = DirectCast(e.Item.FindControl("chkDropship"), CheckBox)
                    chkDropship.Attributes.Add("onchange", "return DropshipChanged(this)")

                    Dim lblShipToAddress As Label = DirectCast(e.Item.FindControl("lblShipToAddress"), Label)
                    Dim ddlShipToWarehouse As DropDownList = DirectCast(e.Item.FindControl("ddlShipToWarehouse"), DropDownList)

                    If Not dsSOShipAddress Is Nothing AndAlso dsSOShipAddress.Tables.Count > 1 AndAlso dsSOShipAddress.Tables(1).Rows.Count > 0 Then
                        lblShipToAddress.Text = CCommon.ToString(dsSOShipAddress.Tables(1).Rows(0)("ShippingAdderss")).Replace("<pre>", "").Replace("</pre>", ",<br/>")
                        DirectCast(e.Item.FindControl("hdnShipToAddress"), HiddenField).Value = lblShipToAddress.Text.Replace("&", "&amp;").Replace("<", "&lt;").Replace(">", "&gt;")
                    End If

                    If Not dtWareHouse Is Nothing AndAlso dtWareHouse.Rows.Count > 0 Then
                        Dim listItemWarehouse As ListItem
                        For Each drWarehouse As DataRow In dtWareHouse.Rows
                            listItemWarehouse = New ListItem
                            listItemWarehouse.Text = CCommon.ToString(drWarehouse("vcWareHouse"))
                            listItemWarehouse.Value = CCommon.ToLong(drWarehouse("numWareHouseID")) & "~" & CCommon.ToLong(drWarehouse("numAddressID"))

                            If CCommon.ToLong(Session("DefaultWarehouse")) = CCommon.ToLong(drWarehouse("numWareHouseID")) Then
                                listItemWarehouse.Selected = True
                            End If

                            ddlShipToWarehouse.Items.Add(listItemWarehouse)
                        Next
                    End If

                    If CCommon.ToBool(drv("bitDropShip")) Then
                        lblShipToAddress.Style.Remove("display")
                        ddlShipToWarehouse.Style.Add("display", "none")
                    Else
                        lblShipToAddress.Style.Add("display", "none")
                        ddlShipToWarehouse.Style.Remove("display")
                    End If

                    ddlVendor = e.Item.FindControl("ddlVendor")

                    objItems.DomainID = Session("DomainID")
                    objItems.ItemCode = CCommon.ToLong(drv("numItemCode"))
                    dtVendors = objItems.GetVendors

                    For Each drVendor As DataRow In dtVendors.Select()
                        ddlVendor.Items.Add(New ListItem(drVendor.Item("Vendor").ToString(), drVendor.Item("numVendorID").ToString() + "~" + drVendor.Item("numContactId").ToString() + "~" + String.Format("{0:#,##0.00}", drVendor.Item("monCost"))))

                        If CCommon.ToLong(CType(e.Item.FindControl("hfSOVendorId"), HiddenField).Value) > 0 Then
                            If CType(e.Item.FindControl("hfSOVendorId"), HiddenField).Value = drVendor.Item("numVendorID").ToString() Then
                                ddlVendor.SelectedIndex = ddlVendor.Items.Count - 1
                            End If
                        Else
                            If CType(e.Item.FindControl("hfVendorId"), HiddenField).Value = drVendor.Item("numVendorID").ToString() Then
                                ddlVendor.SelectedIndex = ddlVendor.Items.Count - 1
                            End If
                        End If
                        ddlVendor.Attributes.Add("onclick", "ChangeVendorCost('" & txtPrice.ClientID & "','" & ddlVendor.ClientID & "') ")
                    Next

                    'When item doesn't have primary vendor set price to Cost of selected vendor
                    If (Convert.ToInt32(Session("numCost")) = 2) Then
                    Else
                        If (CCommon.ToLong(CType(e.Item.FindControl("hfSOVendorId"), HiddenField).Value) > 0 Or CCommon.ToLong(CType(e.Item.FindControl("hfVendorId"), HiddenField).Value) = 0) And ddlVendor.SelectedIndex >= 0 Then
                            CType(e.Item.FindControl("txtPrice"), TextBox).Text = String.Format("{0:#,##0.00}", Math.Abs(CCommon.ToDecimal(ddlVendor.SelectedValue.Split("~")(2).ToString)))
                        End If
                    End If


                    DirectCast(e.Item.FindControl("hdnPOID"), HiddenField).Value = CCommon.ToLong(drv("numPOID"))
                    DirectCast(e.Item.FindControl("hdnPOItemID"), HiddenField).Value = CCommon.ToLong(drv("numPOItemID"))
                    If CCommon.ToLong(drv("numPOID")) > 0 Then
                        Dim poLink As New HtmlGenericControl("a")
                        poLink.Attributes.Add("href", "../opportunity/frmOpportunities.aspx?frm=deallist&amp;OpID=" & CCommon.ToLong(drv("numPOID")))
                        poLink.InnerHtml = CCommon.ToString(drv("vcPOName"))
                        DirectCast(e.Item.FindControl("divPO"), HtmlGenericControl).Controls.Add(poLink)

                        DirectCast(e.Item.FindControl("hdnPOName"), HiddenField).Value = CCommon.ToString(drv("vcPOName"))
                    End If

                    If CCommon.ToLong(drv("numOrigUnitHour")) > CCommon.ToLong(drv("numOnHand")) Then
                        e.Item.Style.Add("background-color", "#FFE1E1")
                    End If

                    If Not e.Item.FindControl("rdpItemReqDate") Is Nothing Then
                        Dim requiredDate As DateTime
                        If DateTime.TryParse(CCommon.ToString(drv("ItemRequiredDate")), requiredDate) Then
                            DirectCast(e.Item.FindControl("rdpItemReqDate"), RadDatePicker).SelectedDate = requiredDate
                        End If
                    End If
                End If

                If Not ddlUOM Is Nothing AndAlso Not dtUnit Is Nothing AndAlso dtUnit.Rows.Count > 0 Then
                    For Each drUOM In dtUnit.Rows
                        objCommon.DomainID = Session("DomainID")
                        objCommon.ItemCode = objItems.ItemCode
                        objCommon.UnitId = CCommon.ToLong(drUOM("numUOMId"))
                        Dim dtUOMFactor As DataTable = objCommon.GetItemUOMConversion()

                        If Not dtUOMFactor Is Nothing AndAlso dtUOMFactor.Rows.Count > 0 Then
                            ddlUOM.Items.Add(New ListItem(CCommon.ToString(drUOM("vcUnitName")), CCommon.ToString(drUOM("numUOMId")) & "~" & CCommon.ToDouble(dtUOMFactor.Rows(0)("BaseUOMConversion"))))
                        Else
                            ddlUOM.Items.Add(New ListItem(CCommon.ToString(drUOM("vcUnitName")), CCommon.ToString(drUOM("numUOMId")) & "~1"))
                        End If
                    Next

                    If Not ddlUOM.Items.FindByValue(CCommon.ToLong(drv("numUOM"))) Is Nothing Then
                        ddlUOM.Items.FindByValue(CCommon.ToLong(drv("numUOM"))).Selected = True

                        If Not ddlUOM.SelectedValue.EndsWith("~1") Then
                            DirectCast(e.Item.FindControl("lblUOMUnits"), Label).Text = (CCommon.ToDouble(CType(e.Item.FindControl("txtUnits"), TextBox).Text) * CCommon.ToDouble(ddlUOM.SelectedValue.Split("~")(1))) & " Units"
                        End If
                    End If
                Else
                    ddlUOM.Items.Add(New ListItem("Units", "0~1"))
                End If

                CalculateUnitPrice(e.Item)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Public Sub FillContact(ByVal ddlCombo As DropDownList)
        Try
            Dim fillCombo As New COpportunities
            With fillCombo
                .DivisionID = radCmbCompany.SelectedValue
                ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                ddlCombo.DataTextField = "Name"
                ddlCombo.DataValueField = "numcontactId"
                ddlCombo.DataBind()
            End With
            ddlCombo.Items.Insert(0, New ListItem("---Select One---", "0"))
            If ddlCombo.Items.Count = 2 Then
                ddlCombo.Items(1).Selected = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub CreateOrder(ByVal DivisionID As Long, _
                            ByVal ContactID As Long, _
                            ByVal CompanyName As String, _
                            ByVal BillCompanyName As String, _
                            ByVal BillStreet As String, _
                            ByVal BillCity As String, _
                            ByVal BillState As Long, _
                            ByVal BillPostal As String, _
                            ByVal BillCountry As Long, _
                            ByVal ShipCompanyName As String, _
                            ByVal ShipStreet As String, _
                            ByVal ShipCity As String, _
                            ByVal ShipState As Long, _
                            ByVal ShipPostal As String, _
                            ByVal ShipCountry As Long, _
                            ByVal Dropship? As Boolean, _
                            ByVal numShipToAddressID As Long, _
                            ByVal numWarehouseID As Long)
        Try
            Dim isItemExists As Boolean = False
            Dim dg As DataGridItem
            For i = 0 To dgItem.Items.Count - 1
                dg = dgItem.Items(i)
                Dim chk As CheckBox
                chk = dg.FindControl("chkSelect")
                If chk.Checked = True Then
                    If Not Dropship Is Nothing Then
                        If CType(dg.FindControl("chkDropship"), CheckBox).Checked <> Dropship Then
                            Continue For
                        End If

                        If numShipToAddressID <> -1 Then
                            If numShipToAddressID <> CCommon.ToLong(CType(dg.FindControl("hdnShipToAddressID"), HiddenField).Value) Then
                                Continue For
                            End If
                        End If

                        If numWarehouseID <> -1 Then
                            If numWarehouseID <> CCommon.ToLong(CType(dg.FindControl("ddlShipToWarehouse"), DropDownList).SelectedValue.Split("~")(0)) Then
                                Continue For
                            End If
                        End If
                    End If

                    isItemExists = True
                End If
            Next

            If isItemExists Then
                Dim i As Integer
                Dim objOpportunity As New MOpportunity
                objOpportunity.OppType = IIf(OppType = 1, 2, 1)
                If ddlOpportunity.SelectedIndex <= 0 Then
                    objOpportunity.OpportunityId = 0
                    objOpportunity.OppType = IIf(OppType = 1, 2, 1)
                    objOpportunity.OpportunityName = CompanyName & "-" & IIf(objOpportunity.OppType = 1, "SO", "PO") & "-" & Format(Now(), "MMMM")
                    objOpportunity.ContactID = ContactID
                    objOpportunity.DivisionID = DivisionID
                    objOpportunity.UserCntID = Session("UserContactID")
                    objOpportunity.EstimatedCloseDate = Now
                    objOpportunity.DomainID = Session("DomainId")
                    objOpportunity.Active = 1
                    objOpportunity.DealStatus = objOpportunity.EnmDealStatus.DealWon
                    objOpportunity.SourceType = 1

                    Dim objAdmin As New CAdmin
                    Dim dtBillingTerms As DataTable
                    objAdmin.DivisionID = DivisionID
                    dtBillingTerms = objAdmin.GetBillingTerms()

                    objOpportunity.boolBillingTerms = IIf(dtBillingTerms.Rows(0).Item("tintBillingTerms") = 1, True, False)
                    objOpportunity.BillingDays = dtBillingTerms.Rows(0).Item("numBillingDays")
                    objOpportunity.boolInterestType = IIf(dtBillingTerms.Rows(0).Item("tintInterestType") = 1, True, False)
                    objOpportunity.Interest = dtBillingTerms.Rows(0).Item("fltInterest")

                    'objOpportunity.strItems = dsTemp.GetXml
                    objOpportunity.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                    lngOppID = objOpportunity.Save()(0)
                ElseIf ddlOpportunity.SelectedIndex > 0 Then
                    lngOppID = ddlOpportunity.SelectedValue
                End If
                objOpportunity.OpportunityId = lngOppID

                ''Added By Sachin Sadhu||Date:29thApril12014
                ''Purpose :To Added Opportunity data in work Flow queue based on created Rules
                ''          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = lngOppID
                objWfA.SaveWFOrderQueue()
                'end of code

                For i = 0 To dgItem.Items.Count - 1
                    dg = dgItem.Items(i)
                    Dim chk As CheckBox
                    chk = dg.FindControl("chkSelect")
                    If chk.Checked = True Then
                        If Not Dropship Is Nothing Then
                            If CType(dg.FindControl("chkDropship"), CheckBox).Checked <> Dropship Then
                                Continue For
                            End If

                            If numShipToAddressID <> -1 Then
                                If numShipToAddressID <> CCommon.ToLong(CType(dg.FindControl("hdnShipToAddressID"), HiddenField).Value) Then
                                    Continue For
                                End If
                            End If

                            If numWarehouseID <> -1 Then
                                If numWarehouseID <> CCommon.ToLong(CType(dg.FindControl("ddlShipToWarehouse"), DropDownList).SelectedValue.Split("~")(0)) Then
                                    Continue For
                                End If
                            End If
                        End If

                        objOpportunity.ItemCode = CCommon.ToLong(CType(dg.FindControl("hdnItemCode"), HiddenField).Value)
                        objOpportunity.Units = Math.Abs(CCommon.ToDecimal(CType(dg.FindControl("txtUnits"), TextBox).Text)) * CCommon.ToDecimal(CType(dg.FindControl("ddlUOM"), DropDownList).SelectedValue.Split("~")(1))
                        objOpportunity.UnitPrice = Math.Abs(CCommon.ToDecimal(CType(dg.FindControl("txtPrice"), TextBox).Text))
                        objOpportunity.Desc = HttpUtility.HtmlDecode(dg.Cells(0).Text).Trim()
                        objOpportunity.ItemName = CCommon.ToString(CType(dg.FindControl("lblItemName"), Label).Text)
                        objOpportunity.Notes = HttpUtility.HtmlDecode(dg.Cells(6).Text).Trim()
                        objOpportunity.ModelName = HttpUtility.HtmlDecode(dg.Cells(2).Text).Trim
                        objOpportunity.ItemThumbImage = HttpUtility.HtmlDecode(dg.Cells(1).Text).Trim
                        objOpportunity.ItemType = HttpUtility.HtmlDecode(dg.Cells(5).Text).Trim()
                        objOpportunity.Dropship = CType(dg.FindControl("chkDropship"), CheckBox).Checked
                        If CCommon.ToLong(numWarehouseID) > 0 AndAlso objOpportunity.ItemType = "P" Then
                            If objCommon Is Nothing Then
                                objCommon = New CCommon
                            End If
                            Dim dtWareHouses As DataTable = objCommon.GetWarehousesForSelectedItem(objOpportunity.ItemCode, numWarehouseID, 0, 0, 1, 0, 0, "")
                            If Not dtWareHouses Is Nothing AndAlso dtWareHouses.Rows.Count > 0 Then
                                objOpportunity.WarehouseItemID = CCommon.ToLong(dtWareHouses.Rows(0)("numWareHouseItemId"))
                            Else
                                Throw New Exception("Selected warehouse not available for item: " & CCommon.ToString(CType(dg.FindControl("lblItemName"), Label).Text))
                            End If
                        Else
                            objOpportunity.WarehouseItemID = CCommon.ToLong(CType(dg.FindControl("lblWarehouseItemId"), Label).Text)
                        End If

                        If CType(dg.FindControl("chkDropship"), CheckBox).Checked Then
                            objOpportunity.WarehouseItemID = 0
                        End If

                        objOpportunity.numUOM = CCommon.ToLong(CType(dg.FindControl("ddlUOM"), DropDownList).SelectedValue.Split("~")(0))

                        objOpportunity.ProjectID = CCommon.ToLong(CType(dg.FindControl("hfnumProjectID"), HiddenField).Value)
                        objOpportunity.ClassID = CCommon.ToLong(CType(dg.FindControl("hfnumClassID"), HiddenField).Value)

                        objOpportunity.ItemRequiredDate = CType(dg.FindControl("rdpItemReqDate"), RadDatePicker).SelectedDate  'Item Required Date
                        objOpportunity.Attributes = CCommon.ToString(CType(dg.FindControl("hdnAttributes"), HiddenField).Value)
                        objOpportunity.AttributeIDs = CCommon.ToString(CType(dg.FindControl("hdnAttrValues"), HiddenField).Value)

                        Dim oppItemID As Long = 0

                        If cbPreferredVendor.Checked = False And OppType = 1 Then
                            Dim ddlVendor As DropDownList
                            ddlVendor = dg.FindControl("ddlVendor")

                            Dim strValue As String() = ddlVendor.SelectedValue.Split(CChar("~"))

                            If CCommon.ToLong(strValue(0)) = DivisionID Then
                                oppItemID = objOpportunity.UpdateOpportunityItems()
                            End If
                        Else
                            oppItemID = objOpportunity.UpdateOpportunityItems()
                        End If

                        'This is commnented because we are grouping same item from order in one line item and it may affect logic when same item is available in muliple kit items
                        'If oppItemID > 0 Then
                        '    Dim objSOLIPL As New SalesOrderLineItemsPOLinking
                        '    objSOLIPL.DomainID = CCommon.ToLong(Session("DomainID"))
                        '    objSOLIPL.SalesOrderID = CCommon.ToLong(GetQueryStringVal("OppID"))
                        '    objSOLIPL.SalesOrderItemID = CCommon.ToLong(CType(dg.FindControl("hdnOppItemID"), HiddenField).Value)
                        '    objSOLIPL.SalesOrderOppChildItemID = CCommon.ToLong(CType(dg.FindControl("hdnOppChildItemID"), HiddenField).Value)
                        '    objSOLIPL.SalesOrderOppKitChildItemID = CCommon.ToLong(CType(dg.FindControl("hdnOppKitChildItemID"), HiddenField).Value)
                        '    objSOLIPL.PurchaseOrderID = objOpportunity.OpportunityId
                        '    objSOLIPL.PurchaseOrderItemID = oppItemID
                        '    objSOLIPL.Save()
                        'End If
                    End If
                Next
                'Fix of Bug id 638 -by chintan, Update inventory counts
                Dim objCustomReport As New BACRM.BusinessLogic.Reports.CustomReports
                objCustomReport.DynamicQuery = "PERFORM USP_UpdatingInventoryonCloseDeal(" & objOpportunity.OpportunityId & ",0," & Session("UserContactID") & "0);"
                objCustomReport.ExecuteDynamicSql()



                objOpportunity.BillToType = 2
                objOpportunity.BillCompanyName = BillCompanyName
                objOpportunity.BillStreet = BillStreet
                objOpportunity.BillCity = BillCity
                objOpportunity.BillState = BillState
                objOpportunity.BillPostal = BillPostal
                objOpportunity.BillCountry = BillCountry

                objOpportunity.ShipToType = 2
                objOpportunity.ShipCompanyName = ShipCompanyName
                objOpportunity.ShipStreet = ShipStreet
                objOpportunity.ShipCity = ShipCity
                objOpportunity.ShipState = ShipState
                objOpportunity.ShipPostal = ShipPostal
                objOpportunity.ShipCountry = ShipCountry

                objOpportunity.ParentOppID = GetQueryStringVal("OppID")
                objOpportunity.ParentBizDocID = IIf(chkFromExisting.Checked, ddlInvoices.SelectedValue, 0)

                If rbBillingContactAlt.Checked Then
                    objOpportunity.BillingContact = 0
                    objOpportunity.IsAltBillingContact = True
                    objOpportunity.AltBillingContact = CCommon.ToString(txtBillingContactAlt.Text)
                Else
                    objOpportunity.BillingContact = CCommon.ToLong(ddlBillingContact.SelectedValue)
                    objOpportunity.IsAltBillingContact = False
                    objOpportunity.AltBillingContact = ""
                End If

                If rbShippingContactAlt.Checked Then
                    objOpportunity.ShippingContact = 0
                    objOpportunity.IsAltShippingContact = True
                    objOpportunity.AltShippingContact = CCommon.ToString(txtShippingContactAlt.Text)
                Else
                    objOpportunity.ShippingContact = CCommon.ToLong(ddlShippingContact.SelectedValue)
                    objOpportunity.IsAltShippingContact = False
                    objOpportunity.AltShippingContact = ""
                End If

                objOpportunity.UpdateOpportunityLinkingDtls()

                If CCommon.ToLong(Session("PODropShipBizDoc")) AndAlso objOpportunity.OppType = 2 Then
                    Dim objOppBizDocs As New OppBizDocs
                    objOppBizDocs.OppId = lngOppID
                    objOppBizDocs.OppType = objOpportunity.OppType

                    objOppBizDocs.UserCntID = Session("UserContactID")
                    objOppBizDocs.DomainID = Session("DomainID")

                    objOppBizDocs.BizDocId = CCommon.ToLong(Session("PODropShipBizDoc"))
                    objOppBizDocs.BizDocTemplateID = CCommon.ToLong(Session("PODropShipBizDocTemplate"))

                    objOppBizDocs.OppBizDocId = 0

                    Dim dtOppItems As DataTable = objOppBizDocs.GetOppItemsForBizDocs()
                    Dim dsBizDocItems As New DataSet
                    Dim dtBizDocItems As New DataTable
                    If dtOppItems IsNot Nothing AndAlso dtOppItems.Rows.Count > 0 Then
                        dtBizDocItems.TableName = "BizDocItems"
                        dtBizDocItems.Columns.Add("OppItemID")
                        dtBizDocItems.Columns.Add("Quantity")
                        dtBizDocItems.Columns.Add("Notes")
                        dtBizDocItems.Columns.Add("monPrice")
                        dtBizDocItems.AcceptChanges()

                        Dim drNew As DataRow
                        For Each drItem As DataRow In dtOppItems.Rows
                            If (CCommon.ToBool(drItem("bitDropShip"))) Then
                                drNew = dtBizDocItems.NewRow
                                drNew("OppItemID") = CCommon.ToDouble(drItem("numoppitemtCode"))
                                drNew("Quantity") = CCommon.ToDouble(drItem("QtyOrdered"))
                                drNew("Notes") = CCommon.ToString(drItem("vcNotes"))
                                drNew("monPrice") = CCommon.ToDouble(drItem("monOppItemPrice"))

                                dtBizDocItems.Rows.Add(drNew)
                                dtBizDocItems.AcceptChanges()
                            End If
                        Next
                        If dtBizDocItems IsNot Nothing AndAlso dtBizDocItems.Rows.Count > 0 Then

                            dsBizDocItems.Tables.Add(dtBizDocItems)
                            dsBizDocItems.Tables(0).TableName = "BizDocItems"

                            objOppBizDocs.strBizDocItems = dsBizDocItems.GetXml()

                            objOppBizDocs.vcPONo = "" 'txtPO.Text
                            objOppBizDocs.vcComments = "" 'txtComments.Text

                            objOppBizDocs.FromDate = DateTime.UtcNow
                            objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                            objCommon = New CCommon
                            objCommon.DomainID = Session("DomainID")
                            objCommon.Mode = 33
                            objCommon.Str = objOppBizDocs.BizDocId
                            objOppBizDocs.SequenceId = objCommon.GetSingleFieldValue()

                            objCommon = New CCommon
                            objCommon.DomainID = Session("DomainID")
                            objCommon.Mode = 34
                            objCommon.Str = lngOppID
                            objOppBizDocs.RefOrderNo = objCommon.GetSingleFieldValue()

                            objOppBizDocs.bitPartialShipment = True

                            'Transaction initiated from save click event
                            OppBizDocID = objOppBizDocs.SaveBizDoc()

                            Dim lintAuthorizativeBizDocsId As Long
                            objOppBizDocs.DomainID = Session("DomainID")
                            objOppBizDocs.OppId = lngOppID
                            lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()

                            'Create Journals only for authoritative bizdocs
                            '-------------------------------------------------
                            If lintAuthorizativeBizDocsId = objOppBizDocs.BizDocId Then
                                Dim ds As New DataSet
                                Dim dtOppBiDocItems As DataTable

                                objOppBizDocs.OppBizDocId = OppBizDocID
                                ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                                dtOppBiDocItems = ds.Tables(0)

                                Dim objCalculateDealAmount As New CalculateDealAmount

                                objCalculateDealAmount.CalculateDealAmount(lngOppID, OppBizDocID, OppType, Session("DomainID"), dtOppBiDocItems)

                                ''---------------------------------------------------------------------------------
                                Dim JournalId As Long = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, objOppBizDocs.FromDate, Description:=ds.Tables(1).Rows(0).Item("vcBizDocID"))
                                Dim objJournalEntries As New JournalEntry

                                If CCommon.ToBool(ds.Tables(1).Rows(0).Item("bitPPVariance")) Then
                                    objJournalEntries.SaveJournalEntriesPurchaseVariance(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, DivisionID, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), ds.Tables(1).Rows(0).Item("fltExchangeRateBizDoc"), vcBaseCurrency:=ds.Tables(1).Rows(0).Item("vcBaseCurrency"), vcForeignCurrency:=ds.Tables(1).Rows(0).Item("vcForeignCurrency"))
                                Else
                                    objJournalEntries.SaveJournalEntriesPurchase(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, DivisionID, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"))
                                End If
                            End If

                            ''Added By Sachin Sadhu||Date:1stMay2014
                            ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                            ''          Using Change tracking
                            objWfA = New Workflow()
                            objWfA.DomainID = Session("DomainID")
                            objWfA.UserCntID = Session("UserContactID")
                            objWfA.RecordID = OppBizDocID
                            objWfA.SaveWFBizDocQueue()

                            '------ Send BizDoc Alerts - Notify record owners, their supervisors, and your trading partners when a BizDoc is created, modified, or approved.
                            Dim objAlert As New CAlerts
                            objAlert.SendBizDocAlerts(lngOppID, OppBizDocID, objOppBizDocs.BizDocId, Session("DomainID"), CAlerts.enmBizDoc.IsCreated)

                            Dim objAutomatonRule As New AutomatonRule
                            objAutomatonRule.ExecuteAutomationRule(49, OppBizDocID, 1)
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message = "NOT_ALLOWED" Then
                Exit Sub
            Else
                Throw ex
            End If
        End Try
    End Sub
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim i As Integer
            Dim objItem As New CItems
            Dim objOpp As New OppBizDocs

            'Accounting validation->Do not allow to save PO untill Default COGs account is mapped 
            If OppType = 1 Then '1 means opposite (2)- create po
                Dim objJournal As New JournalEntry
                If ChartOfAccounting.GetDefaultAccount("CG", Session("DomainID")) = 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please Set Default COGs account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                    Exit Sub
                End If

                If ChartOfAccounting.GetDefaultAccount("PC", Session("DomainID")) = 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please Set Default Purchase Clearing account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                    Exit Sub
                End If

                If ChartOfAccounting.GetDefaultAccount("PV", Session("DomainID")) = 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please Set Default Purchase Price Variance account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                    Exit Sub
                End If
            End If

            'validate item income,asset , cogs account are mapped based on item type
            For Each dgGridItem As DataGridItem In dgItem.Items
                If CType(dgGridItem.FindControl("chkSelect"), CheckBox).Checked Then

                    If Not objItem.ValidateItemAccount(CCommon.ToLong(CType(dgGridItem.FindControl("hdnItemCode"), HiddenField).Value), Session("DomainID"), IIf(OppType = 1, 2, 1)) = True Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "AccountValidation", "alert('Please Set Income,Asset,COGs(Expense) Account for " & CCommon.ToString(CType(dgGridItem.FindControl("lblItemName"), Label).Text) & " from Administration->Inventory->Item Details')", True)
                        Exit Sub
                    End If

                    If cbPreferredVendor.Checked = False And OppType = 1 Then
                        Dim ddlVendor As DropDownList
                        ddlVendor = dgGridItem.FindControl("ddlVendor")

                        Dim strValue As String() = ddlVendor.SelectedValue.Split(CChar("~"))

                        If objOpp.ValidateARAP(CCommon.ToLong(strValue(0)), 0, Session("DomainID")) = False Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting->Accounts for RelationShip"" To Save' );", True)
                            Exit Sub
                        End If
                    End If
                End If
            Next

            If OppType = 2 AndAlso radCmbCompany.SelectedValue = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Select customer.' );", True)
                Exit Sub
            ElseIf OppType = 2 AndAlso (ddlContact.SelectedIndex = "-1" Or ddlContact.SelectedValue = "0") Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Select Contact.' );", True)
                Exit Sub
            ElseIf OppType = 1 AndAlso chkUseSingleShipAddress.Checked AndAlso radBillOther.Checked AndAlso radCmbBillOther.SelectedValue = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Select bill other location customer.' );", True)
                Exit Sub
            ElseIf OppType = 1 AndAlso chkUseSingleShipAddress.Checked AndAlso radShipOther.Checked AndAlso radCmbShipOther.SelectedValue = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Select ship other location customer.' );", True)
                Exit Sub
            ElseIf OppType = 2 AndAlso radBillOther.Checked AndAlso radCmbBillOther.SelectedValue = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Select bill other location customer.' );", True)
                Exit Sub
            ElseIf OppType = 2 AndAlso radShipOther.Checked AndAlso radCmbShipOther.SelectedValue = "" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Select ship other location customer.' );", True)
                Exit Sub
            End If

            Dim BillCompanyName As String
            Dim BillStreet As String
            Dim BillCity As String
            Dim BillState As Long
            Dim BillPostal As String
            Dim BillCountry As Long
            Dim ShipCompanyName As String
            Dim ShipStreet As String
            Dim ShipCity As String
            Dim ShipState As Long
            Dim ShipPostal As String
            Dim ShipCountry As Long

            'Update Order Billing /Shipping Address
            If radBillEmployer.Checked = True AndAlso CCommon.ToLong(radCmbBillEmployerAddress.SelectedValue) Then
                Dim objContact As New CContacts
                objContact.DomainID = CCommon.ToLong(Session("DomainID"))
                objContact.AddressID = CCommon.ToLong(radCmbBillEmployerAddress.SelectedValue)
                objContact.byteMode = 1
                Dim dtAddress As DataTable = objContact.GetAddressDetail()

                If Not dtAddress Is Nothing AndAlso dtAddress.Rows.Count > 0 Then
                    BillCompanyName = CCommon.ToString(dtAddress.Rows(0)("vcCompanyName"))
                    BillStreet = CCommon.ToString(dtAddress.Rows(0)("vcStreet"))
                    BillCity = CCommon.ToString(dtAddress.Rows(0)("vcCity"))
                    BillState = CCommon.ToLong(dtAddress.Rows(0)("numState"))
                    BillPostal = CCommon.ToString(dtAddress.Rows(0)("vcPostalCode"))
                    BillCountry = CCommon.ToLong(dtAddress.Rows(0)("numCountry"))
                End If
            ElseIf radBillSOCustomer.Checked = 1 Then
                Dim objContact As New CContacts
                objContact.DomainID = CCommon.ToLong(Session("DomainID"))
                objContact.AddressType = CContacts.enmAddressType.BillTo
                objContact.RecordID = CCommon.ToLong(hdnDivID.Value)
                objContact.AddresOf = CContacts.enmAddressOf.Organization
                objContact.byteMode = 2
                Dim dtAddress As DataTable = objContact.GetAddressDetail()

                If Not dtAddress Is Nothing AndAlso dtAddress.Rows.Count > 0 Then
                    BillCompanyName = CCommon.ToString(dtAddress.Rows(0)("vcCompanyName"))
                    BillStreet = CCommon.ToString(dtAddress.Rows(0)("vcStreet"))
                    BillCity = CCommon.ToString(dtAddress.Rows(0)("vcCity"))
                    BillState = CCommon.ToLong(dtAddress.Rows(0)("numState"))
                    BillPostal = CCommon.ToString(dtAddress.Rows(0)("vcPostalCode"))
                    BillCountry = CCommon.ToLong(dtAddress.Rows(0)("numCountry"))
                End If
            ElseIf radBillCustomer.Checked = True Then
                Dim objContact As New CContacts
                objContact.DomainID = CCommon.ToLong(Session("DomainID"))
                objContact.AddressID = CCommon.ToLong(radCmbBillCustomerAddress.SelectedValue)
                objContact.byteMode = 1
                Dim dtAddress As DataTable = objContact.GetAddressDetail()

                If Not dtAddress Is Nothing AndAlso dtAddress.Rows.Count > 0 Then
                    BillCompanyName = CCommon.ToString(dtAddress.Rows(0)("vcCompanyName"))
                    BillStreet = CCommon.ToString(dtAddress.Rows(0)("vcStreet"))
                    BillCity = CCommon.ToString(dtAddress.Rows(0)("vcCity"))
                    BillState = CCommon.ToLong(dtAddress.Rows(0)("numState"))
                    BillPostal = CCommon.ToString(dtAddress.Rows(0)("vcPostalCode"))
                    BillCountry = CCommon.ToLong(dtAddress.Rows(0)("numCountry"))
                End If
            ElseIf radBillOther.Checked Then
                Dim objContact As New CContacts
                objContact.DomainID = CCommon.ToLong(Session("DomainID"))
                objContact.AddressID = CCommon.ToLong(radCmbBillOtherAddress.SelectedValue)
                objContact.byteMode = 1
                Dim dtAddress As DataTable = objContact.GetAddressDetail()

                If Not dtAddress Is Nothing AndAlso dtAddress.Rows.Count > 0 Then
                    BillCompanyName = CCommon.ToString(dtAddress.Rows(0)("vcCompanyName"))
                    BillStreet = CCommon.ToString(dtAddress.Rows(0)("vcStreet"))
                    BillCity = CCommon.ToString(dtAddress.Rows(0)("vcCity"))
                    BillState = CCommon.ToLong(dtAddress.Rows(0)("numState"))
                    BillPostal = CCommon.ToString(dtAddress.Rows(0)("vcPostalCode"))
                    BillCountry = CCommon.ToLong(dtAddress.Rows(0)("numCountry"))
                End If
            End If

            If radShipEmployer.Checked = True Then
                Dim objContact As New CContacts
                objContact.DomainID = CCommon.ToLong(Session("DomainID"))
                objContact.AddressID = CCommon.ToLong(radCmbShipEmployerAddress.SelectedValue)
                objContact.byteMode = 1
                Dim dtAddress As DataTable = objContact.GetAddressDetail()

                If Not dtAddress Is Nothing AndAlso dtAddress.Rows.Count > 0 Then
                    ShipCompanyName = CCommon.ToString(dtAddress.Rows(0)("vcCompanyName"))
                    ShipStreet = CCommon.ToString(dtAddress.Rows(0)("vcStreet"))
                    ShipCity = CCommon.ToString(dtAddress.Rows(0)("vcCity"))
                    ShipState = CCommon.ToLong(dtAddress.Rows(0)("numState"))
                    ShipPostal = CCommon.ToString(dtAddress.Rows(0)("vcPostalCode"))
                    ShipCountry = CCommon.ToLong(dtAddress.Rows(0)("numCountry"))
                End If
            ElseIf radShipSO.Checked Then
                Dim objOppAddress As New MOpportunity
                objOppAddress.DomainID = CCommon.ToLong(Session("DomainID"))
                objOppAddress.OpportunityId = CCommon.ToLong(GetQueryStringVal("OppID"))
                objOppAddress.Mode = 1
                Dim dtAddress As DataTable = objOppAddress.GetOpportunityAddress()

                If Not dtAddress Is Nothing AndAlso dtAddress.Rows.Count > 0 Then
                    ShipCompanyName = CCommon.ToString(dtAddress.Rows(0)("vcShipCompanyName"))
                    ShipStreet = CCommon.ToString(dtAddress.Rows(0)("Street"))
                    ShipCity = CCommon.ToString(dtAddress.Rows(0)("City"))
                    ShipState = CCommon.ToLong(dtAddress.Rows(0)("State"))
                    ShipPostal = CCommon.ToString(dtAddress.Rows(0)("PostCode"))
                    ShipCountry = CCommon.ToLong(dtAddress.Rows(0)("Country"))
                End If
            ElseIf radShipCustomer.Checked = True Then
                Dim objContact As New CContacts
                objContact.DomainID = CCommon.ToLong(Session("DomainID"))
                objContact.AddressID = CCommon.ToLong(radCmbShipCustomerAddress.SelectedValue)
                objContact.byteMode = 1
                Dim dtAddress As DataTable = objContact.GetAddressDetail()

                If Not dtAddress Is Nothing AndAlso dtAddress.Rows.Count > 0 Then
                    ShipCompanyName = CCommon.ToString(dtAddress.Rows(0)("vcCompanyName"))
                    ShipStreet = CCommon.ToString(dtAddress.Rows(0)("vcStreet"))
                    ShipCity = CCommon.ToString(dtAddress.Rows(0)("vcCity"))
                    ShipState = CCommon.ToLong(dtAddress.Rows(0)("numState"))
                    ShipPostal = CCommon.ToString(dtAddress.Rows(0)("vcPostalCode"))
                    ShipCountry = CCommon.ToLong(dtAddress.Rows(0)("numCountry"))
                End If
            ElseIf radShipOther.Checked = True Then
                Dim objContact As New CContacts
                objContact.DomainID = CCommon.ToLong(Session("DomainID"))
                objContact.AddressID = CCommon.ToLong(radCmbShipOtherAddress.SelectedValue)
                objContact.byteMode = 1
                Dim dtAddress As DataTable = objContact.GetAddressDetail()

                If Not dtAddress Is Nothing AndAlso dtAddress.Rows.Count > 0 Then
                    ShipCompanyName = CCommon.ToString(dtAddress.Rows(0)("vcCompanyName"))
                    ShipStreet = CCommon.ToString(dtAddress.Rows(0)("vcStreet"))
                    ShipCity = CCommon.ToString(dtAddress.Rows(0)("vcCity"))
                    ShipState = CCommon.ToLong(dtAddress.Rows(0)("numState"))
                    ShipPostal = CCommon.ToString(dtAddress.Rows(0)("vcPostalCode"))
                    ShipCountry = CCommon.ToLong(dtAddress.Rows(0)("numCountry"))
                End If
            End If

            Dim errorMessage As String = ""

            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                'Create PO against Preffered vendor from grid
                If OppType = 1 Then
                    Dim dr As DataRow
                    Dim gridRow As DataGridItem

                    'Create table to group item by selected vendor
                    Dim dtItem As New DataTable

                    dtItem.Columns.Add("ItemCode")
                    dtItem.Columns.Add("Units")
                    dtItem.Columns.Add("UnitPrice")
                    dtItem.Columns.Add("Desc")
                    dtItem.Columns.Add("ItemName")
                    dtItem.Columns.Add("ModelName")
                    dtItem.Columns.Add("ItemThumbImage")
                    dtItem.Columns.Add("ItemType")
                    dtItem.Columns.Add("Dropship")
                    dtItem.Columns.Add("WarehouseItemID")
                    dtItem.Columns.Add("numVendorID")
                    dtItem.Columns.Add("numVendorContactId")
                    dtItem.Columns.Add("VendorCompanyName")
                    dtItem.Columns.Add("numUOM")
                    dtItem.Columns.Add("txtNotes")
                    dtItem.Columns.Add("numPOID")
                    dtItem.Columns.Add("numPOItemID")
                    dtItem.Columns.Add("numShipToAddressID")
                    dtItem.Columns.Add("numWarehouseID")
                    dtItem.Columns.Add("numWarehouseAdddressID")

                    For i = 0 To dgItem.Items.Count - 1
                        gridRow = dgItem.Items(i)
                        Dim chk As CheckBox
                        chk = gridRow.FindControl("chkSelect")
                        If chk.Checked = True Then

                            Dim ddlVendor As DropDownList
                            ddlVendor = gridRow.FindControl("ddlVendor")

                            If ddlVendor.Items.Count > 0 Then
                                dr = dtItem.NewRow

                                dr("ItemCode") = CCommon.ToLong(CType(gridRow.FindControl("hdnItemCode"), HiddenField).Value)
                                dr("Units") = CCommon.ToDouble(CType(gridRow.FindControl("txtUnits"), TextBox).Text) * CCommon.ToDecimal(CType(gridRow.FindControl("ddlUOM"), DropDownList).SelectedValue.Split("~")(1))
                                dr("UnitPrice") = CCommon.ToDouble(CType(gridRow.FindControl("txtPrice"), TextBox).Text)
                                dr("Desc") = HttpUtility.HtmlDecode(gridRow.Cells(0).Text).Trim()
                                dr("ItemName") = CCommon.ToString(CType(gridRow.FindControl("lblItemName"), Label).Text)
                                dr("ModelName") = gridRow.Cells(2).Text.Trim
                                dr("txtNotes") = HttpUtility.HtmlDecode(gridRow.Cells(6).Text).Trim()
                                dr("ItemThumbImage") = gridRow.Cells(1).Text.Trim
                                dr("ItemType") = HttpUtility.HtmlDecode(gridRow.Cells(5).Text).Trim()
                                dr("Dropship") = CType(gridRow.FindControl("chkDropship"), CheckBox).Checked
                                dr("WarehouseItemID") = CCommon.ToLong(CType(gridRow.FindControl("lblWarehouseItemId"), Label).Text)
                                dr("numUOM") = CCommon.ToLong(CType(gridRow.FindControl("ddlUOM"), DropDownList).SelectedValue.Split("~")(0))

                                If chkUseSingleShipAddress.Checked Then
                                    dr("numShipToAddressID") = -1
                                    dr("numWarehouseID") = -1
                                    dr("numWarehouseAdddressID") = -1
                                Else
                                    dr("numShipToAddressID") = CCommon.ToLong(CType(gridRow.FindControl("hdnShipToAddressID"), HiddenField).Value)
                                    If Not CType(gridRow.FindControl("chkDropship"), CheckBox).Checked Then
                                        dr("numWarehouseID") = CCommon.ToLong(CType(gridRow.FindControl("ddlShipToWarehouse"), DropDownList).SelectedValue.Split("~")(0))
                                        dr("numWarehouseAdddressID") = CCommon.ToLong(CType(gridRow.FindControl("ddlShipToWarehouse"), DropDownList).SelectedValue.Split("~")(1))
                                    Else
                                        dr("numWarehouseID") = -1
                                        dr("numWarehouseAdddressID") = -1
                                    End If
                                End If

                                Dim strValue As String() = ddlVendor.SelectedValue.Split(CChar("~"))

                                dr("numVendorID") = CCommon.ToLong(strValue(0))
                                dr("numVendorContactId") = CCommon.ToLong(strValue(1))
                                dr("VendorCompanyName") = ddlVendor.SelectedItem.Text

                                dtItem.Rows.Add(dr)
                            Else
                                If String.IsNullOrEmpty(errorMessage) AndAlso Not cbPreferredVendor.Checked Then
                                    errorMessage = "PO can not be created, Your option is to select a vender for following item(s):"
                                Else
                                    errorMessage += "<br/>=>" & CCommon.ToString(CType(gridRow.FindControl("lblItemName"), Label).Text)
                                End If
                            End If
                        End If
                    Next

                    If Not String.IsNullOrEmpty(errorMessage) Then
                        DisplayAlert(errorMessage)
                    End If


                    Dim dtVendor As DataTable
                    If cbPreferredVendor.Checked Then
                        For Each drRow As DataRow In dtItem.Rows
                            drRow.Item("numVendorID") = CCommon.ToLong(radCmbCompany.SelectedValue)
                        Next
                        dtItem.AcceptChanges()
                    End If

                    dtVendor = dtItem.DefaultView.ToTable(True, "numVendorID", "Dropship", "numShipToAddressID", "numWarehouseID", "numWarehouseAdddressID")

                    For Each drRow As DataRow In dtVendor.Select()
                        Dim drItemColl() As DataRow
                        'Create single order for all item which are referenced to same vendor
                        drItemColl = dtItem.Select("numVendorID = " & CCommon.ToLong(drRow.Item("numVendorID")) & " AND Dropship=" & CCommon.ToBool(drRow.Item("Dropship")) & " AND numShipToAddressID=" & CCommon.ToLong(drRow.Item("numShipToAddressID")) & " AND numWarehouseID=" & CCommon.ToLong(drRow.Item("numWarehouseID")))

                        If Not chkUseSingleShipAddress.Checked Then
                            If CCommon.ToBool(drRow.Item("Dropship")) Then
                                If CCommon.ToLong(drRow.Item("numShipToAddressID")) > 0 Then
                                    Dim objContact As New CContacts
                                    objContact.DomainID = CCommon.ToLong(Session("DomainID"))
                                    objContact.AddressID = CCommon.ToLong(radCmbShipOtherAddress.SelectedValue)
                                    objContact.byteMode = 1
                                    Dim dtAddress As DataTable = objContact.GetAddressDetail()

                                    If Not dtAddress Is Nothing AndAlso dtAddress.Rows.Count > 0 Then
                                        ShipCompanyName = CCommon.ToString(dtAddress.Rows(0)("vcCompanyName"))
                                        ShipStreet = CCommon.ToString(dtAddress.Rows(0)("vcStreet"))
                                        ShipCity = CCommon.ToString(dtAddress.Rows(0)("vcCity"))
                                        ShipState = CCommon.ToLong(dtAddress.Rows(0)("numState"))
                                        ShipPostal = CCommon.ToString(dtAddress.Rows(0)("vcPostalCode"))
                                        ShipCountry = CCommon.ToLong(dtAddress.Rows(0)("numCountry"))
                                    Else
                                        ShipCompanyName = ""
                                        ShipStreet = ""
                                        ShipCity = ""
                                        ShipState = 0
                                        ShipPostal = ""
                                        ShipCountry = 0
                                    End If
                                Else
                                    Dim objOppAddress As New MOpportunity
                                    objOppAddress.DomainID = CCommon.ToLong(Session("DomainID"))
                                    objOppAddress.OpportunityId = CCommon.ToLong(GetQueryStringVal("OppID"))
                                    objOppAddress.Mode = 1
                                    Dim dtAddress As DataTable = objOppAddress.GetOpportunityAddress()

                                    If Not dtAddress Is Nothing AndAlso dtAddress.Rows.Count > 0 Then
                                        ShipCompanyName = CCommon.ToString(dtAddress.Rows(0)("vcShipCompanyName"))
                                        ShipStreet = CCommon.ToString(dtAddress.Rows(0)("Street"))
                                        ShipCity = CCommon.ToString(dtAddress.Rows(0)("City"))
                                        ShipState = CCommon.ToLong(dtAddress.Rows(0)("State"))
                                        ShipPostal = CCommon.ToString(dtAddress.Rows(0)("PostCode"))
                                        ShipCountry = CCommon.ToLong(dtAddress.Rows(0)("Country"))
                                    Else
                                        ShipCompanyName = ""
                                        ShipStreet = ""
                                        ShipCity = ""
                                        ShipState = 0
                                        ShipPostal = ""
                                        ShipCountry = 0
                                    End If
                                End If
                            ElseIf CCommon.ToLong(drRow.Item("numWarehouseID")) > 0 Then
                                Dim objContact As New CContacts
                                objContact.DomainID = CCommon.ToLong(Session("DomainID"))
                                objContact.AddressID = CCommon.ToLong(drRow.Item("numWarehouseAdddressID"))
                                objContact.byteMode = 1
                                Dim dtAddress As DataTable = objContact.GetAddressDetail()

                                If Not dtAddress Is Nothing AndAlso dtAddress.Rows.Count > 0 Then
                                    ShipCompanyName = CCommon.ToString(dtAddress.Rows(0)("vcCompanyName"))
                                    ShipStreet = CCommon.ToString(dtAddress.Rows(0)("vcStreet"))
                                    ShipCity = CCommon.ToString(dtAddress.Rows(0)("vcCity"))
                                    ShipState = CCommon.ToLong(dtAddress.Rows(0)("numState"))
                                    ShipPostal = CCommon.ToString(dtAddress.Rows(0)("vcPostalCode"))
                                    ShipCountry = CCommon.ToLong(dtAddress.Rows(0)("numCountry"))
                                Else
                                    ShipCompanyName = ""
                                    ShipStreet = ""
                                    ShipCity = ""
                                    ShipState = 0
                                    ShipPostal = ""
                                    ShipCountry = 0
                                End If
                            End If
                        End If

                        If cbPreferredVendor.Checked Then
                            CreateOrder(radCmbCompany.SelectedValue, ddlContact.SelectedValue, radCmbCompany.Text, BillCompanyName, BillStreet, BillCity, BillState, BillPostal, BillCountry, ShipCompanyName, ShipStreet, ShipCity, ShipState, ShipPostal, ShipCountry, CCommon.ToBool(drRow.Item("Dropship")), CCommon.ToLong(drRow.Item("numShipToAddressID")), CCommon.ToLong(drRow.Item("numWarehouseID")))
                        Else
                            CreateOrder(CCommon.ToLong(drItemColl(0).Item("numVendorID")), CCommon.ToLong(drItemColl(0).Item("numVendorContactId")), CCommon.ToString(drItemColl(0).Item("VendorCompanyName")), BillCompanyName, BillStreet, BillCity, BillState, BillPostal, BillCountry, ShipCompanyName, ShipStreet, ShipCity, ShipState, ShipPostal, ShipCountry, CCommon.ToBool(drRow.Item("Dropship")), CCommon.ToLong(drRow.Item("numShipToAddressID")), CCommon.ToLong(drRow.Item("numWarehouseID")))
                        End If
                    Next
                Else
                    CreateOrder(radCmbCompany.SelectedValue, ddlContact.SelectedValue, radCmbCompany.Text, BillCompanyName, BillStreet, BillCity, BillState, BillPostal, BillCountry, ShipCompanyName, ShipStreet, ShipCity, ShipState, ShipPostal, ShipCountry, Nothing, -1, -1)
                End If

                objTransactionScope.Complete()
            End Using

            If String.IsNullOrEmpty(errorMessage) Then
                If lngOppID > 0 Then
                    Response.Redirect("../opportunity/frmCreateSalesPurFromOpp.aspx?OppType=" & OppType & "&OppID=" & GetQueryStringVal("OppID") & "&Created=True&NewOppID=" & lngOppID)
                Else
                    Response.Redirect("../opportunity/frmCreateSalesPurFromOpp.aspx?OppType=" & OppType & "&OppID=" & GetQueryStringVal("OppID"))
                End If
            End If
        Catch ex As Exception
            If ex.Message = "FY_CLOSED" Then
                DisplayAlert("This transaction can not be posted,Because transactions date belongs to closed financial year.")
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub

    Sub FillState(ByVal ddl As DropDownList, ByVal lngCountry As Long, ByVal lngDomainID As Long)
        Try
            Dim dtTable As DataTable
            Dim objUserAccess As New UserAccess
            objUserAccess.DomainID = lngDomainID
            objUserAccess.Country = lngCountry
            dtTable = objUserAccess.SelState
            ddl.DataSource = dtTable
            ddl.DataTextField = "vcState"
            ddl.DataValueField = "numStateID"
            ddl.DataBind()
            ddl.Items.Insert(0, "--Select One--")
            ddl.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
        Try
            If CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                Dim objOpp As New OppBizDocs
                If objOpp.ValidateARAP(radCmbCompany.SelectedValue, 0, Session("DomainID")) = False Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting->Accounts for RelationShip"" To Continue' );", True)
                    radCmbCompany.SelectedValue = ""
                    radCmbCompany.Text = ""
                    Exit Sub
                End If
                Dim objProject As New Project
                objProject.DomainID = Session("DomainId")
                objProject.DivisionID = radCmbCompany.SelectedValue
                objProject.bytemode = 3
                objProject.OppType = IIf(OppType = 1, 2, 1)
                ddlOpportunity.DataSource = objProject.GetOpportunities
                ddlOpportunity.DataTextField = "vcPOppName"
                ddlOpportunity.DataValueField = "numOppId"
                ddlOpportunity.DataBind()
                ddlOpportunity.Items.Insert(0, "Create New")
                ddlOpportunity.Items.FindByText("Create New").Value = 0
                FillContact(ddlContact)
                Dim objOpportunity As New MOpportunity
                For Each dgGridItem As DataGridItem In dgItem.Items
                    If CType(dgGridItem.FindControl("chkSelect"), CheckBox).Checked = True Then
                        objOpportunity.ItemCode = CCommon.ToLong(CType(dgGridItem.FindControl("hdnItemCode"), HiddenField).Value)
                        objOpportunity.DivisionID = radCmbCompany.SelectedValue
                        objOpportunity.GetPartNo()
                        dgGridItem.Cells(4).Text = objOpportunity.PartNo
                        If (Convert.ToInt32(Session("numCost")) = 2) Then
                        Else
                            If objOpportunity.VendorCost > 0 Then
                                CType(dgGridItem.FindControl("txtPrice"), TextBox).Text = String.Format("{0:#,##0.00}", objOpportunity.VendorCost)
                            End If
                        End If

                    End If

                    CalculateUnitPrice(dgGridItem)
                Next

                If radBillCustomer.Checked Then
                    BindCustomerAddress(1, radCmbBillCustomerAddress)
                End If

                If radShipCustomer.Checked Then
                    BindCustomerAddress(2, radCmbShipCustomerAddress)
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub


    Private Sub ddlInvoices_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlInvoices.SelectedIndexChanged
        Try

            LoadItems()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub ddlVendor_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim ddllist As DropDownList = CType(sender, DropDownList)

            If Not ddllist.SelectedItem Is Nothing Then
                Dim cell As TableCell = CType(ddllist.Parent, TableCell)
                Dim dg As DataGridItem = CType(cell.Parent, DataGridItem)

                CalculateUnitPrice(dg)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Protected Sub txtunits_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim txtunits As TextBox = CType(sender, TextBox)
            Dim cell As TableCell = CType(txtunits.Parent, TableCell)
            Dim dg As DataGridItem = CType(cell.Parent, DataGridItem)

            CalculateUnitPrice(dg)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Private Sub CalculateUnitPrice(ByVal dg As DataGridItem)
        Try
            Dim objItems As New CItems

            objItems.ItemCode = CCommon.ToLong(CType(dg.FindControl("hdnItemCode"), HiddenField).Value)
            If Not CType(dg.FindControl("ddlUOM"), DropDownList).SelectedItem Is Nothing Then
                objItems.NoofUnits = CCommon.ToDouble(CType(dg.FindControl("txtUnits"), TextBox).Text) * CCommon.ToDouble(CType(dg.FindControl("ddlUOM"), DropDownList).SelectedValue.Split("~")(1))

                If Not CType(dg.FindControl("ddlUOM"), DropDownList).SelectedItem.Value.EndsWith("~1") Then
                    CType(dg.FindControl("lblUOMUnits"), Label).Text = String.Format("{0:#,##0.#####}", objItems.NoofUnits)
                End If
            Else
                objItems.NoofUnits = Math.Abs(CCommon.ToDecimal(CType(dg.FindControl("txtUnits"), TextBox).Text))
            End If

            objItems.OppId = 0
            objItems.DomainID = Session("DomainID")
            objItems.Amount = 0
            objItems.WareHouseItemID = CCommon.ToLong(CType(dg.FindControl("lblWarehouseItemId"), Label).Text)

            If cbPreferredVendor.Checked = False And OppType = 1 Then
                Dim ddlVendor As DropDownList
                ddlVendor = dg.FindControl("ddlVendor")

                Dim strValue As String() = ddlVendor.SelectedValue.Split(CChar("~"))
                If ddlVendor.Items.Count > 0 Then
                    objItems.DivisionID = CCommon.ToLong(strValue(0))
                End If
            Else
                objItems.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
            End If

            objItems.byteMode = IIf(OppType = 1, 2, 1)

            Dim ds As DataSet
            Dim dtItemPricemgmt As DataTable

            ds = objItems.GetPriceManagementList1
            dtItemPricemgmt = ds.Tables(0)

            If dtItemPricemgmt.Rows.Count = 0 Then
                dtItemPricemgmt = ds.Tables(2)
            End If

            dtItemPricemgmt.Merge(ds.Tables(1))

            Dim dtUnit As DataTable
            objCommon.DomainID = Session("DomainID")
            objCommon.ItemCode = CCommon.ToLong(CType(dg.FindControl("hdnItemCode"), HiddenField).Value)
            objCommon.UnitId = 0
            dtUnit = objCommon.GetItemUOMConversion()
            If (Convert.ToInt32(Session("numCost")) = 2) Then
            Else
                CType(dg.FindControl("txtPrice"), TextBox).Text = String.Format("{0:#,##0.00}", CCommon.ToDecimal(dtItemPricemgmt.Rows(0)("ListPrice")) * IIf(OppType = 1, dtUnit(0)("PurchaseUOMConversion"), 1))
            End If


        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BindContact(ByVal numDivisionID As Long, ByRef ddlContact As DropDownList)
        Try
            ddlContact.Items.Clear()
            Dim objOpp As New BACRM.BusinessLogic.Opportunities.COpportunities
            objOpp.DivisionID = numDivisionID
            Dim ds As DataSet = objOpp.ListContact()
            Dim listItem As ListItem
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In ds.Tables(0).Rows
                    listItem = New ListItem()
                    listItem.Text = CCommon.ToString(dr("Name"))
                    listItem.Value = CCommon.ToString(dr("numcontactId"))
                    If CCommon.ToBool(dr("bitPrimaryContact")) Then
                        listItem.Selected = True
                    End If
                    ddlContact.Items.Add(listItem)
                Next
            End If

            ddlContact.Items.Insert(0, New ListItem("---Select One---", "0"))
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BindEmployerAddress(ByVal type As Int32, ByRef radcmbAddresses As RadComboBox)
        Try
            radcmbAddresses.Items.Clear()
            'type: 1 = Bill, 2 = ship
            Dim objContact As New CContacts
            If type = 1 Then
                objContact.AddressType = CContacts.enmAddressType.BillTo
                BindContact(CCommon.ToLong(Session("UserDivisionID")), ddlBillingContact)
            Else
                objContact.AddressType = CContacts.enmAddressType.ShipTo
                BindContact(CCommon.ToLong(Session("UserDivisionID")), ddlShippingContact)
            End If
            objContact.DomainID = CCommon.ToLong(Session("DomainID"))
            objContact.RecordID = CCommon.ToLong(Session("UserDivisionID"))
            objContact.AddresOf = CContacts.enmAddressOf.Organization
            objContact.byteMode = 2
            Dim dtAddress As DataTable = objContact.GetAddressDetail()
            Dim listItem As RadComboBoxItem
            For Each dr As DataRow In dtAddress.Rows
                listItem = New RadComboBoxItem
                listItem.Text = CCommon.ToString(dr("vcAddressName"))
                listItem.Value = CCommon.ToString(dr("numAddressID"))
                listItem.Attributes.Add("Address", CCommon.ToString(dr("vcFullAddress")).Replace("<pre>", "").Replace("</pre>", "<br/>"))
                listItem.Attributes.Add("numContact", dr("numContact"))
                listItem.Attributes.Add("bitAltContact", dr("bitAltContact"))
                listItem.Attributes.Add("vcAltContact", dr("vcAltContact"))
                radcmbAddresses.Items.Add(listItem)
            Next

            If type = 1 AndAlso radBillEmployer.Checked AndAlso Not radcmbAddresses.SelectedItem Is Nothing Then
                litBillAddress.Text = radcmbAddresses.SelectedItem.Attributes("Address").ToString().Replace("<pre>", "").Replace("</pre>", ",<br/>")

                If CCommon.ToBool(radcmbAddresses.SelectedItem.Attributes("bitAltContact")) Then
                    rbBillingContactAlt.Checked = True
                    rbBillingContact.Checked = False
                    ddlBillingContact.SelectedValue = 0
                    txtBillingContactAlt.Text = CCommon.ToString(radcmbAddresses.SelectedItem.Attributes("vcAltContact"))
                ElseIf Not ddlBillingContact.Items.FindByValue(CCommon.ToLong(radcmbAddresses.SelectedItem.Attributes("numContact"))) Is Nothing Then
                    rbBillingContact.Checked = True
                    rbBillingContactAlt.Checked = False
                    txtBillingContactAlt.Text = ""
                    ddlBillingContact.Items.FindByValue(CCommon.ToLong(radcmbAddresses.SelectedItem.Attributes("numContact"))).Selected = True
                End If
            ElseIf type = 2 AndAlso radShipEmployer.Checked AndAlso Not radcmbAddresses.SelectedItem Is Nothing Then
                litShipAddress.Text = radcmbAddresses.SelectedItem.Attributes("Address").ToString().Replace("<pre>", "").Replace("</pre>", ",<br/>")

                If CCommon.ToBool(radcmbAddresses.SelectedItem.Attributes("bitAltContact")) Then
                    rbShippingContactAlt.Checked = True
                    rbShippingContact.Checked = False
                    ddlShippingContact.SelectedValue = 0
                    txtShippingContactAlt.Text = CCommon.ToString(radcmbAddresses.SelectedItem.Attributes("vcAltContact"))
                ElseIf Not ddlShippingContact.Items.FindByValue(CCommon.ToLong(radcmbAddresses.SelectedItem.Attributes("numContact"))) Is Nothing Then
                    rbShippingContact.Checked = True
                    rbShippingContactAlt.Checked = False
                    txtShippingContactAlt.Text = ""
                    ddlShippingContact.Items.FindByValue(CCommon.ToLong(radcmbAddresses.SelectedItem.Attributes("numContact"))).Selected = True
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BindCustomerAddress(ByVal type As Int32, ByRef radcmbAddresses As RadComboBox, Optional ByVal bitOtherAddress As Boolean = False)
        Try
            radcmbAddresses.Items.Clear()
            'type: 1 = Bill, 2 = ship
            Dim objContact As New CContacts

            If bitOtherAddress Then
                If type = 1 AndAlso String.IsNullOrEmpty(radCmbBillOther.SelectedValue) Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please select other bill address customer.' );", True)
                    Exit Sub
                ElseIf type = 2 AndAlso String.IsNullOrEmpty(radCmbShipOther.SelectedValue) Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please select other ship address customer.' );", True)
                    Exit Sub
                End If

                If type = 1 Then
                    objContact.AddressType = CContacts.enmAddressType.BillTo
                Else
                    objContact.AddressType = CContacts.enmAddressType.ShipTo
                End If
                objContact.DomainID = CCommon.ToLong(Session("DomainID"))

                If type = 1 Then
                    objContact.RecordID = CCommon.ToLong(radCmbBillOther.SelectedValue)
                    BindContact(objContact.RecordID, ddlBillingContact)
                ElseIf type = 2 Then
                    objContact.RecordID = CCommon.ToLong(radCmbShipOther.SelectedValue)
                    BindContact(objContact.RecordID, ddlShippingContact)
                End If

                objContact.AddresOf = CContacts.enmAddressOf.Organization
                objContact.byteMode = 2
                Dim dtAddress As DataTable = objContact.GetAddressDetail()
                Dim listItem As RadComboBoxItem

                If type = 2 AndAlso OppType = 1 Then
                    listItem = New RadComboBoxItem
                    listItem.Text = CCommon.ToString("SO Ship-to")
                    listItem.Value = "-1"

                    Dim objOpp As New OppotunitiesIP
                    objOpp.DomainID = CCommon.ToLong(Session("DomainID"))
                    objOpp.OpportunityId = CCommon.ToLong(GetQueryStringVal("OppID"))
                    Dim dsAddress As DataSet = objOpp.GetOpportunityAddress(2)

                    If Not dsAddress Is Nothing AndAlso dsAddress.Tables.Count > 1 AndAlso dsAddress.Tables(1).Rows.Count > 0 Then
                        listItem.Attributes.Add("Address", CCommon.ToString(dsAddress.Tables(1).Rows(0)("ShippingAdderss")))
                        listItem.Attributes.Add("numContact", dsAddress.Tables(1).Rows(0)("numContact"))
                        listItem.Attributes.Add("bitAltContact", dsAddress.Tables(1).Rows(0)("bitAltContact"))
                        listItem.Attributes.Add("vcAltContact", dsAddress.Tables(1).Rows(0)("vcAltContact"))
                    Else
                        listItem.Attributes.Add("Address", "")
                        listItem.Attributes.Add("numContact", "")
                        listItem.Attributes.Add("bitAltContact", 0)
                        listItem.Attributes.Add("vcAltContact", "")
                    End If

                    radcmbAddresses.Items.Add(listItem)
                End If

                For Each dr As DataRow In dtAddress.Rows
                    listItem = New RadComboBoxItem
                    listItem.Text = CCommon.ToString(dr("vcAddressName"))
                    listItem.Value = CCommon.ToString(dr("numAddressID"))
                    listItem.Attributes.Add("Address", CCommon.ToString(dr("vcFullAddress")).Replace("<pre>", "").Replace("</pre>", "<br/>"))
                    listItem.Attributes.Add("numContact", dr("numContact"))
                    listItem.Attributes.Add("bitAltContact", dr("bitAltContact"))
                    listItem.Attributes.Add("vcAltContact", dr("vcAltContact"))
                    radcmbAddresses.Items.Add(listItem)
                Next

                If type = 1 Then
                    litBillAddress.Text = radcmbAddresses.SelectedItem.Attributes("Address").ToString().Replace("<pre>", "").Replace("</pre>", "<br/>")

                    If CCommon.ToBool(radcmbAddresses.SelectedItem.Attributes("bitAltContact")) Then
                        rbBillingContactAlt.Checked = True
                        rbBillingContact.Checked = False
                        ddlBillingContact.SelectedValue = 0
                        txtBillingContactAlt.Text = CCommon.ToString(radcmbAddresses.SelectedItem.Attributes("vcAltContact"))
                    ElseIf Not ddlBillingContact.Items.FindByValue(CCommon.ToLong(radcmbAddresses.SelectedItem.Attributes("numContact"))) Is Nothing Then
                        rbBillingContact.Checked = True
                        rbBillingContactAlt.Checked = False
                        txtBillingContactAlt.Text = ""
                        ddlBillingContact.Items.FindByValue(CCommon.ToLong(radcmbAddresses.SelectedItem.Attributes("numContact"))).Selected = True
                    End If
                ElseIf type = 2 Then
                    litShipAddress.Text = radcmbAddresses.SelectedItem.Attributes("Address").ToString().Replace("<pre>", "").Replace("</pre>", "<br/>")

                    If CCommon.ToBool(radcmbAddresses.SelectedItem.Attributes("bitAltContact")) Then
                        rbShippingContactAlt.Checked = True
                        rbShippingContact.Checked = False
                        ddlShippingContact.SelectedValue = 0
                        txtShippingContactAlt.Text = CCommon.ToString(radcmbAddresses.SelectedItem.Attributes("vcAltContact"))
                    ElseIf Not ddlShippingContact.Items.FindByValue(CCommon.ToLong(radcmbAddresses.SelectedItem.Attributes("numContact"))) Is Nothing Then
                        rbShippingContact.Checked = True
                        rbShippingContactAlt.Checked = False
                        txtShippingContactAlt.Text = ""
                        ddlShippingContact.Items.FindByValue(CCommon.ToLong(radcmbAddresses.SelectedItem.Attributes("numContact"))).Selected = True
                    End If
                End If
            Else
                If OppType = 2 AndAlso Not String.IsNullOrEmpty(radCmbCompany.SelectedValue) Then
                    If type = 1 Then
                        objContact.AddressType = CContacts.enmAddressType.BillTo
                    Else
                        objContact.AddressType = CContacts.enmAddressType.ShipTo
                    End If

                    objContact.DomainID = CCommon.ToLong(Session("DomainID"))
                    objContact.RecordID = CCommon.ToLong(radCmbCompany.SelectedValue)
                    objContact.AddresOf = CContacts.enmAddressOf.Organization
                    objContact.byteMode = 2
                    Dim dtAddress As DataTable = objContact.GetAddressDetail()
                    Dim listItem As RadComboBoxItem
                    For Each dr As DataRow In dtAddress.Rows
                        listItem = New RadComboBoxItem
                        listItem.Text = CCommon.ToString(dr("vcAddressName"))
                        listItem.Value = CCommon.ToString(dr("numAddressID"))
                        listItem.Attributes.Add("Address", CCommon.ToString(dr("vcFullAddress")).Replace("<pre>", "").Replace("</pre>", "<br/>"))
                        listItem.Attributes.Add("numContact", dr("numContact"))
                        listItem.Attributes.Add("bitAltContact", dr("bitAltContact"))
                        listItem.Attributes.Add("vcAltContact", dr("vcAltContact"))
                        radcmbAddresses.Items.Add(listItem)
                    Next

                    If type = 1 AndAlso radBillCustomer.Checked Then
                        litBillAddress.Text = radcmbAddresses.SelectedItem.Attributes("Address").ToString().Replace("<pre>", "").Replace("</pre>", "<br/>")

                        If CCommon.ToBool(radcmbAddresses.SelectedItem.Attributes("bitAltContact")) Then
                            rbBillingContactAlt.Checked = True
                            rbBillingContact.Checked = False
                            ddlBillingContact.SelectedValue = 0
                            txtBillingContactAlt.Text = CCommon.ToString(radcmbAddresses.SelectedItem.Attributes("vcAltContact"))
                        ElseIf Not ddlBillingContact.Items.FindByValue(CCommon.ToLong(radcmbAddresses.SelectedItem.Attributes("numContact"))) Is Nothing Then
                            rbBillingContact.Checked = True
                            rbBillingContactAlt.Checked = False
                            txtBillingContactAlt.Text = ""
                            ddlBillingContact.Items.FindByValue(CCommon.ToLong(radcmbAddresses.SelectedItem.Attributes("numContact"))).Selected = True
                        End If
                    ElseIf type = 2 AndAlso radShipCustomer.Checked Then
                        litShipAddress.Text = radcmbAddresses.SelectedItem.Attributes("Address").ToString().Replace("<pre>", "").Replace("</pre>", "<br/>")

                        If CCommon.ToBool(radcmbAddresses.SelectedItem.Attributes("bitAltContact")) Then
                            rbShippingContactAlt.Checked = True
                            rbShippingContact.Checked = False
                            ddlShippingContact.SelectedValue = 0
                            txtShippingContactAlt.Text = CCommon.ToString(radcmbAddresses.SelectedItem.Attributes("vcAltContact"))
                        ElseIf Not ddlShippingContact.Items.FindByValue(CCommon.ToLong(radcmbAddresses.SelectedItem.Attributes("numContact"))) Is Nothing Then
                            rbShippingContact.Checked = True
                            rbShippingContactAlt.Checked = False
                            txtShippingContactAlt.Text = ""
                            ddlShippingContact.Items.FindByValue(CCommon.ToLong(radcmbAddresses.SelectedItem.Attributes("numContact"))).Selected = True
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub radCmbBillEmployerAddress_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Try
            If radBillEmployer.Checked Then
                litBillAddress.Text = radCmbBillEmployerAddress.SelectedItem.Attributes("Address").ToString().Replace("<pre>", "").Replace("</pre>", "<br/>")

                If CCommon.ToBool(radCmbBillEmployerAddress.SelectedItem.Attributes("bitAltContact")) Then
                    rbBillingContactAlt.Checked = True
                    rbBillingContact.Checked = False
                    ddlBillingContact.SelectedValue = 0
                    txtBillingContactAlt.Text = CCommon.ToString(radCmbBillEmployerAddress.SelectedItem.Attributes("vcAltContact"))
                ElseIf Not ddlBillingContact.Items.FindByValue(CCommon.ToLong(radCmbBillEmployerAddress.SelectedItem.Attributes("numContact"))) Is Nothing Then
                    rbBillingContact.Checked = True
                    rbBillingContactAlt.Checked = False
                    txtBillingContactAlt.Text = ""
                    ddlBillingContact.Items.FindByValue(CCommon.ToLong(radCmbBillEmployerAddress.SelectedItem.Attributes("numContact"))).Selected = True
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub radCmbShipEmployerAddress_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Try
            If radShipEmployer.Checked Then
                litShipAddress.Text = radCmbShipEmployerAddress.SelectedItem.Attributes("Address").ToString().Replace("<pre>", "").Replace("</pre>", "<br/>")

                If CCommon.ToBool(radCmbShipEmployerAddress.SelectedItem.Attributes("bitAltContact")) Then
                    rbShippingContactAlt.Checked = True
                    rbShippingContact.Checked = False
                    ddlShippingContact.SelectedValue = 0
                    txtShippingContactAlt.Text = CCommon.ToString(radCmbShipEmployerAddress.SelectedItem.Attributes("vcAltContact"))
                ElseIf Not ddlShippingContact.Items.FindByValue(CCommon.ToLong(radCmbShipEmployerAddress.SelectedItem.Attributes("numContact"))) Is Nothing Then
                    rbShippingContact.Checked = True
                    rbShippingContactAlt.Checked = False
                    txtShippingContactAlt.Text = ""
                    ddlShippingContact.Items.FindByValue(CCommon.ToLong(radCmbShipEmployerAddress.SelectedItem.Attributes("numContact"))).Selected = True
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub radCmbBillOther_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbBillOther.SelectedIndexChanged
        Try
            BindCustomerAddress(1, radCmbBillOtherAddress, True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub radCmbShipOther_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbShipOther.SelectedIndexChanged
        Try
            BindCustomerAddress(2, radCmbShipOtherAddress, True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub radCmbBillOtherAddress_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbBillOtherAddress.SelectedIndexChanged
        Try
            If radBillOther.Checked Then
                litBillAddress.Text = radCmbBillOtherAddress.SelectedItem.Attributes("Address").ToString().Replace("<pre>", "").Replace("</pre>", "<br/>")

                If CCommon.ToBool(radCmbBillOtherAddress.SelectedItem.Attributes("bitAltContact")) Then
                    rbBillingContactAlt.Checked = True
                    rbBillingContact.Checked = False
                    ddlBillingContact.SelectedValue = 0
                    txtBillingContactAlt.Text = CCommon.ToString(radCmbBillOtherAddress.SelectedItem.Attributes("vcAltContact"))
                ElseIf Not ddlBillingContact.Items.FindByValue(CCommon.ToLong(radCmbBillOtherAddress.SelectedItem.Attributes("numContact"))) Is Nothing Then
                    rbBillingContact.Checked = True
                    rbBillingContactAlt.Checked = False
                    txtBillingContactAlt.Text = ""
                    ddlBillingContact.Items.FindByValue(CCommon.ToLong(radCmbBillOtherAddress.SelectedItem.Attributes("numContact"))).Selected = True
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub radCmbShipOtherAddress_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbShipOtherAddress.SelectedIndexChanged
        Try
            If radShipOther.Checked Then
                litShipAddress.Text = radCmbShipOtherAddress.SelectedItem.Attributes("Address").ToString().Replace("<pre>", "").Replace("</pre>", "<br/>")

                If CCommon.ToBool(radCmbShipOtherAddress.SelectedItem.Attributes("bitAltContact")) Then
                    rbShippingContactAlt.Checked = True
                    rbShippingContact.Checked = False
                    ddlShippingContact.SelectedValue = 0
                    txtShippingContactAlt.Text = CCommon.ToString(radCmbShipOtherAddress.SelectedItem.Attributes("vcAltContact"))
                ElseIf Not ddlShippingContact.Items.FindByValue(CCommon.ToLong(radCmbShipOtherAddress.SelectedItem.Attributes("numContact"))) Is Nothing Then
                    rbShippingContact.Checked = True
                    rbShippingContactAlt.Checked = False
                    txtShippingContactAlt.Text = ""
                    ddlShippingContact.Items.FindByValue(CCommon.ToLong(radCmbShipOtherAddress.SelectedItem.Attributes("numContact"))).Selected = True
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub radCmbShipCustomerAddress_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbShipCustomerAddress.SelectedIndexChanged
        Try
            If radShipCustomer.Checked AndAlso radCmbShipCustomerAddress.Visible Then
                litShipAddress.Text = radCmbShipCustomerAddress.SelectedItem.Attributes("Address").ToString().Replace("<pre>", "").Replace("</pre>", "<br/>")

                If CCommon.ToBool(radCmbShipCustomerAddress.SelectedItem.Attributes("bitAltContact")) Then
                    rbShippingContactAlt.Checked = True
                    rbShippingContact.Checked = False
                    ddlShippingContact.SelectedValue = 0
                    txtShippingContactAlt.Text = CCommon.ToString(radCmbShipCustomerAddress.SelectedItem.Attributes("vcAltContact"))
                ElseIf Not ddlShippingContact.Items.FindByValue(CCommon.ToLong(radCmbShipCustomerAddress.SelectedItem.Attributes("numContact"))) Is Nothing Then
                    rbShippingContact.Checked = True
                    rbShippingContactAlt.Checked = False
                    txtShippingContactAlt.Text = ""
                    ddlShippingContact.Items.FindByValue(CCommon.ToLong(radCmbShipCustomerAddress.SelectedItem.Attributes("numContact"))).Selected = True
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub radCmbBillCustomerAddress_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbBillCustomerAddress.SelectedIndexChanged
        Try
            If radBillCustomer.Checked AndAlso radCmbBillCustomerAddress.Visible Then
                litBillAddress.Text = radCmbBillCustomerAddress.SelectedItem.Attributes("Address").ToString()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub radBillEmployer_CheckedChanged(sender As Object, e As EventArgs)
        Try
            radCmbBillOther.Visible = False
            radCmbBillOtherAddress.Visible = False
            BindEmployerAddress(1, radCmbBillEmployerAddress)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub radBillCustomer_CheckedChanged(sender As Object, e As EventArgs) Handles radBillCustomer.CheckedChanged
        Try
            radCmbBillOther.Visible = False
            radCmbBillOtherAddress.Visible = False

            If OppType = 1 Then
                rbBillingContact.Checked = False
                rbBillingContactAlt.Checked = False
                txtBillingContactAlt.Text = ""
                ddlBillingContact.SelectedValue = 0
                litBillAddress.Text = "Default bill address of each item vendor selected."
            Else
                BindCustomerAddress(1, radCmbBillCustomerAddress)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub radShipCustomer_CheckedChanged(sender As Object, e As EventArgs)
        Try
            radCmbShipOther.Visible = False
            radCmbShipOtherAddress.Visible = False

            If OppType = 1 Then
                rbShippingContact.Checked = False
                rbShippingContactAlt.Checked = False
                txtShippingContactAlt.Text = ""
                ddlShippingContact.SelectedValue = 0
                litShipAddress.Text = "Default ship address of each item vendor selected."
            Else
                BindCustomerAddress(2, radCmbShipCustomerAddress)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub radShipEmployer_CheckedChanged(sender As Object, e As EventArgs)
        Try
            radCmbShipOther.Visible = False
            radCmbShipOtherAddress.Visible = False
            BindEmployerAddress(2, radCmbShipEmployerAddress)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub radBillOther_CheckedChanged(sender As Object, e As EventArgs)
        Try
            radCmbBillOther.SelectedValue = ""
            radCmbBillOther.Text = ""
            radCmbBillOtherAddress.Items.Clear()
            radCmbBillOther.Visible = True
            radCmbBillOtherAddress.Visible = True
            litBillAddress.Text = ""
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub radShipOther_CheckedChanged(sender As Object, e As EventArgs)
        Try
            radCmbShipOther.SelectedValue = ""
            radCmbShipOther.Text = ""
            radCmbShipOtherAddress.Items.Clear()
            radCmbShipOther.Visible = True
            radCmbShipOtherAddress.Visible = True
            litShipAddress.Text = ""
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub DisplayAlert(ByVal message As String)
        Try
            litMessage.Text = message
            divAlert.Visible = True
            divAlert.Focus()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplaySuccess(ByVal message As String)
        Try
            litSuccessMessage.Text = message
            divSuccess.Visible = True
            divSuccess.Focus()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub radBillSOCustomer_CheckedChanged(sender As Object, e As EventArgs)
        Try
            If radBillSOCustomer.Checked Then
                Dim objContact As New CContacts
                objContact.DomainID = CCommon.ToLong(Session("DomainID"))
                objContact.AddressType = CContacts.enmAddressType.BillTo
                objContact.RecordID = CCommon.ToLong(hdnDivID.Value)
                objContact.AddresOf = CContacts.enmAddressOf.Organization
                objContact.byteMode = 2
                Dim dtAddress As DataTable = objContact.GetAddressDetail()

                If Not dtAddress Is Nothing AndAlso dtAddress.Rows.Count > 0 Then
                    litBillAddress.Text = CCommon.ToString(dtAddress.Rows(0)("vcFullAddress")).Replace("<pre>", "").Replace("</pre>", "<br/>")
                End If

                BindContact(CCommon.ToLong(hdnDivID.Value), ddlBillingContact)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub radShipSO_CheckedChanged(sender As Object, e As EventArgs)
        Try
            If radShipSO.Checked Then
                Dim objOpp As New OppotunitiesIP
                objOpp.DomainID = CCommon.ToLong(Session("DomainID"))
                objOpp.OpportunityId = CCommon.ToLong(GetQueryStringVal("OppID"))
                dsSOShipAddress = objOpp.GetOpportunityAddress(2)

                If Not dsSOShipAddress Is Nothing AndAlso dsSOShipAddress.Tables.Count > 1 AndAlso dsSOShipAddress.Tables(1).Rows.Count > 0 Then
                    litShipAddress.Text = CCommon.ToString(dsSOShipAddress.Tables(1).Rows(0)("ShippingAdderss")).Replace("<pre>", "").Replace("</pre>", ",<br/>")
                End If

                BindContact(CCommon.ToLong(hdnDivID.Value), ddlShippingContact)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
End Class