<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSelWhouseOpenDls.aspx.vb" Inherits=".frmSelWhouseOpenDls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <script language="javascript" type="text/javascript" >
        function SelectItems(a,b,c)
        {
           if (document.getElementById(a).value==0)
           {
                alert("Please Select Warehouse")
                document.getElementById(a).focus()
                return false;
           }
            
            window.open('../opportunity/frmSelectSrlItemFrmInv.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&WItemID='+document.getElementById(a).value+'&Attr='+b+'&ItemCode='+c)
            return false;
            
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <br />
    <table width="100%">
        <tr>
            <td align="right" >
                <asp:Button  ID="btnSaveClose" runat="server" Text="Save & Close" CssClass="button"/>
                <asp:Button  ID="btnClose" runat="server" Text="Close" CssClass="button"/>
            </td>
        </tr>
         <tr>
            <td>
            <br />
            <br />
                <asp:datagrid id="dgItem" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False" BorderColor="white">
		            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
		            <ItemStyle CssClass="is"></ItemStyle>
		            <HeaderStyle CssClass="hs"></HeaderStyle>
		                <Columns>
		                    <asp:BoundColumn DataField="numoppitemtCode"  Visible="false" ></asp:BoundColumn>
		                    <asp:BoundColumn DataField="numItemCode"  Visible="false" ></asp:BoundColumn>
		                    <asp:BoundColumn DataField="numWarehouseItmsID" Visible="false"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="bitSerialized" Visible="false"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="ItemType" Visible="false"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="vcAttributes" Visible=false></asp:BoundColumn>
		                    <asp:BoundColumn DataField="vcItemName" HeaderText="Item"></asp:BoundColumn>
		                    <asp:TemplateColumn>
		                        <ItemTemplate>
		                            <asp:DropDownList ID="ddlWarehouse" runat="server" CssClass="signup" Width="180"></asp:DropDownList>
		                        </ItemTemplate>
		                    </asp:TemplateColumn>
		                    <asp:BoundColumn DataField="monPrice" HeaderText="Unit Price"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="numUnitHour" HeaderText="Units"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="monTotAmount" HeaderText="Total Amount"></asp:BoundColumn>
		                    <asp:BoundColumn DataField="Attributes" HeaderText="Attributes"></asp:BoundColumn>
		                    
		                     <asp:TemplateColumn>
		                        <ItemTemplate>
		                           <asp:Button ID="btnSelSer" runat="server" CssClass="button"  Text="Select Serialaized Items"/>
		                        </ItemTemplate>
		                    </asp:TemplateColumn>
            		    
		                </Columns>
	            </asp:datagrid>
            </td>
        </tr>
    </table>
    
    </form>
</body>
</html>
