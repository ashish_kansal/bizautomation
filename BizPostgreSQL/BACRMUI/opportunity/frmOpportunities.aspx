<%@ Page Language="vb" AutoEventWireup="false" ValidateRequest="false" EnableEventValidation="false"
    CodeBehind="frmOpportunities.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmOpportunities" MasterPageFile="~/common/DetailPage.Master" %>

<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="../projects/MileStone.ascx" TagName="MileStone" TagPrefix="uc2" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Src="../common/frmAssociatedContacts.ascx" TagName="frmAssociatedContacts"
    TagPrefix="uc1" %>
<%@ Register Src="../common/frmCollaborationUserControl.ascx" TagName="frmCollaborationUserControl" TagPrefix="uc1" %>
<%@ Register Src="../common/frmCorrespondence.ascx" TagName="frmCorrespondence" TagPrefix="uc3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script type="text/javascript" src='<%# ResolveUrl("~/JavaScript/colResizable-1.6.min.js")%>'></script>
    <script src="../JavaScript/scripts/jquery.responsivetable.min.js" type="text/javascript"></script>
    <title>Opportunities</title>
    <style type="text/css">
        #lblCPackagingSlipQty{
		    line-height:34px;
	    }	

        #tblMain tr td {
            border-bottom: 1px solid #e1e1e1 !important;
        }

        #tblMain tbody tr:first-child td {
            border-top: 1px solid #fff !important;
            border-left: 1px solid #fff !important;
            border-right: 1px solid #fff !important;
        }

        .tableGroupHeader {
            background-color: #f5f5f5 !important;
            font-weight: bold;
        }

        .lipps, .psscol1, .psscol2, .psscol3, .psscol4, .col1liwidth, .col2liwidth, .col3liwidth {
            padding-left: 5px !important;
            padding-right: 0px !important;
        }

       .psscol1 {
            vertical-align:top;
            padding-left:10px !important;
        }

        .psscol2 {
            vertical-align:top;
            padding-left:10px !important;
        }

        .psscol3 {
            vertical-align:top;
            padding-left:10px !important;
        }

        li.col1liwidth:last-child, li.col2liwidth:last-child, li.col3liwidth:last-child {
            width: 120px;
            text-align: center;
        }

        .col1liwidth {
            width: 70px;
            text-align: center;
        }

        .col2liwidth, .col3liwidth {
            width: 60px;
            text-align: center;
        }

        .innertable > tbody > tr > td {
            border: 0px;
        }

        .innertable tr td {
            border: 0px;
        }

        .innertabletextwidth {
            width: 50px;
            vertical-align: middle;
        }

        .innertabletextboxwidth {
            width: 100px;
        }

        #gvProduct input[type="text"], textarea {
            min-width: 150px;
        }

        .innertabletextboxwidth input[type="text"] {
            min-width: 80px !important;
        }

        .innertabletextwidth #lblCPackagingSlipQty {
            margin-right: 50px !important;
        }

        #txtvcItemDesc {
            min-width: 200px !important;
        }

        .column {
            float: left;
        }

        html > body .RadComboBoxDropDown_Vista .rcbItem, html > body .RadComboBoxDropDown_Vista .rcbHovered, html > body .RadComboBoxDropDown_Vista .rcbDisabled, html > body .RadComboBoxDropDown_Vista .rcbLoading {
            display: inline-block;
        }

        .tblOrderSettingstr {
            height: 30px;
        }

        .tblRecurDetail {
            margin: 0px;
            padding: 0px;
            border-spacing: 0px;
            border-collapse: collapse;
        }

            .tblRecurDetail th {
                background: #e5e5e5;
                border-bottom: 1px solid #cbcbcb;
                border-top: 1px solid #cbcbcb;
                border-right: 1px solid #cbcbcb;
                border-left: 1px solid #cbcbcb;
                font-weight: bold;
                height: 25px;
            }

            .tblRecurDetail td {
                border: 1px solid #cbcbcb;
                text-align: center;
            }

        .tblOrderSettings td {
            width: auto;
            white-space: nowrap;
        }

            .tblOrderSettings td:nth-child(1) {
                font-weight: bold;
                width: 23.5%;
            }

            .tblOrderSettings td:nth-child(3) {
                font-weight: bold;
                width: 14%;
            }

            .tblOrderSettings td:nth-child(2) {
                width: 30%;
            }

            .tblOrderSettings td:nth-child(4) {
                width: 35%;
            }

        #tblMain pre {
            font-family: INHERIT !IMPORTANT;
            PADDING: 0PX !important;
            margin: 0px !important;
            background-color: transparent !important;
            border: none !important;
            font-size: inherit !important;
        }

        .multipleRowsColumns .rcbItem, .multipleRowsColumns .rcbHovered {
            float: left;
            margin: 0 1px;
            min-height: 13px;
            overflow: hidden;
            padding: 2px 19px 2px 6px;
        }

        .col1, .col2, .col3, .col4, .col5, .col6 {
            float: left;
            width: 80px;
            margin: 0;
            padding: 0 5px 0 0;
            line-height: 14px;
            display: block; /*border:0px solid red;*/
        }

        .col1 {
            width: 240px;
        }

        .col2 {
            width: 200px;
        }

        .col3 {
            width: 80px;
        }

        .col4 {
            width: 80px;
        }

        .col5 {
            width: 85px;
        }

        .col6 {
            width: 80px;
        }

        .RadComboBoxDropDown_Default .rcbHeader {
            background-image: none !important;
        }

        img.resizeme {
            width: 100px;
        }

        #divLinkUpload:before {
            content: "";
            background-color: #dcdcdc;
            position: absolute;
            width: 1px;
            height: 100%;
            top: 10px;
            left: 0%;
            display: block;
        }
    </style>
    <script type="text/javascript">
        function reDirectPage(url) {
            window.location.href = url
        }

        function reDirect(a) {
            document.location.href = a;
        }
        function OpenShippingStatus() {
            var companyId = $('[id$=hdnShippingCompanyId]').val();
            var trackingno = $('[id$=hdnTrackingNo]').val();
            if (trackingno != "0") {
                window.open('frmShippingStatus.aspx?companyId=' + companyId + '&trackingno=' + trackingno + '', '', 'toolbar=no,titlebar=no,top=10,left=100,width=550,height=400,scrollbars=yes,resizable=yes')
            }
            return false;
        }

        function ToggleRecurringVisibility(chk) {
            if (chk.checked) {
                $("[id$=trRecurring2]").show();
                $("[id$=trRecurring3]").show();
                ScrollToLastRow();
            } else {
                $("[id$=trRecurring2]").hide();
                $("[id$=trRecurring3]").hide();
            }

            return false;
        }
        function OpenEmail(a, b) {

            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpemEmail(b) {

            var a = $("#imgEmailPopupId").attr("email")
            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenContact(a, b) {
            var str;
            str = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactlist&ft6ty=oiuy&CntId=" + a;
            document.location.href = str;
        }
        function OpenBizInvoice(a, b, c) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b + '&Print=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=850,height=700,scrollbars=yes,resizable=yes');
            return false;
        }

        function ScrollToLastRow() {
            $('html, body').animate({
                scrollTop: $("[id$=trRecurring3]").offset().top
            }, 500);
        }

        function SubTabSectionMsg(ddl) {
            if ($('[id$=ddlProcessList] option:selected').val() == 0) {
                return true;
            }
            else {
                radalert('You can only use this field when a selling process is not selected within the ""Milestones & Stages"" sub-tab section', 450, 135, "");
                return false;
            }
        }

        function openEmpAvailability(OppID, DivID) {
            var OrgName;
            var BProcessName;

            OrgName = $('[id$=hplCustomer]').text();
            if ($('[id$=hfSlpId]').val() == '0') {
                BProcessName = '';
            }
            else {
                BProcessName = $('[id$=ddlProcessList] option:selected').text();
            }

            // Uncomment Below To Test Validation
            //$('#ddlProcessList option:selected').val() == '0';
            //$('#hdnAddress').val() == '';

            if ($('[id$=hfSlpId]').val() == '0' || $('[id$=hdnShipToAddress]').val() == '') {
                if (confirm('No Business Process or Ship-to address found, do you want to continue? ')) {
                    window.open("../ActionItems/frmEmpAvailability.aspx?frm=Opportunities&OrgName=" + OrgName + "&BProcessName=" + BProcessName + "&OppID=" + OppID, '', 'toolbar=no,titlebar=no,top=0,left=100,width=1000,height=500,scrollbars=yes,resizable=yes')
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                window.open("../ActionItems/frmEmpAvailability.aspx?frm=Opportunities&OrgName=" + OrgName + "&BProcessName=" + BProcessName + "&OppID=" + OppID, '', 'toolbar=no,titlebar=no,top=0,left=100,width=1000,height=500,scrollbars=yes,resizable=yes')
                return false;
            }
        }
        function MappingContact() {
            var contact = 0;
            $('select[attr="numPartenerContact"]').each(function () {
                contact = parseInt($("option:selected", this).val());
            });
            console.log(contact);
            $.ajax({
                type: "POST",
                url: '../opportunity/frmOpportunities.aspx/MappingContact',
                data: JSON.stringify({ "ContactID": contact }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success:
                    function (result) {
                        console.log("Success");

                    },
                error:
                    function (XMLHttpRequest, textStatus, errorThrown) {
                        console.log("Error");
                    },

            });
        }
        function BindPartnerContact(domain, oppid, dropId) {
            //var dropId = $(this).attr("id");
            console.log(dropId);
            var DivisionID = 0;
            $('select[attr="numPartner"]').each(function () {

                DivisionID = $("option:selected", this).val();
            });
            oppid = 0;
            $.ajax({
                type: "POST",
                url: '../opportunity/frmOpportunities.aspx/bindContact',
                data: JSON.stringify({ "domain": domain, "oppid": oppid, "DivisionID": DivisionID }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success:
                    function (result) {
                        var i = 0;
                        $('select[attr="numPartenerContact"]').each(function () {
                            if (i == 0) {
                                $(this).empty();
                                $(this).append("<option value='0'>-- Select One --</option>");
                                var id = $(this);
                                $.each(JSON.parse(result.d), function (idx, obj) {
                                    id.append("<option value='" + obj.numContactId + "'>" + obj.vcGivenName + "</option>");
                                });
                            }
                            i = 1;
                        });

                    },
                error:
                    function (XMLHttpRequest, textStatus, errorThrown) {
                        console.log("Error");
                    },

            });
        }
        function IsExists(pagePath, dataString) {
            $.ajax({
                type: "POST",
                url: pagePath,
                data: dataString,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                error:
                    function (XMLHttpRequest, textStatus, errorThrown) {
                        console.log("Error");
                    },
                success:
                    function (result) {
                        console.log("success");
                    }
            });
        }

        function OnClientLoad(sender) {
            ManageTabs($find("<%= radOppTab.ClientID%>").get_selectedTab());
        }

        function OnClientTabSelected(sender, eventArgs) {
            var tab = eventArgs.get_tab();
            PersistTab(tab.get_value());
            //Added By:sachin Sadhu ||Date:29thJuly2014
            //Puprose : to make product/service grid responsive
            //setupResponsiveTables();
            //end of code
        }

        function PersistTab(index) {
            console.log(window.location.pathname);
            console.log(index);
            console.log($("[id$=lblOpportunity]").text());
            var key;
            var value;
            var formName;

            key = "index";
            value = index;

            var pagePath = window.location.pathname + "/PersistTab";
            var dataString = "{strKey:'" + key + "',strValue:'" + value + "',PageType:'" + document.getElementById('OppType').value + "',PageStatus:'" + document.getElementById('OppStatus').value + "'}";
            IsExists(pagePath, dataString);
        }

        function beforeSelectEvent(sender, eventArgs) {
            var tab = eventArgs.get_tab();
            ManageTabs(tab);
        }

        function ManageTabs(tab) {
            if (tab.get_value() == 'BizDocs') {
                var iframe = document.getElementById('IframeBiz')

                if (iframe.src == '') {
                    iframe.src = '../opportunity/frmBizDocs.aspx?OpID=' + document.getElementById('OppID').value + '&OppType=' + document.getElementById('OppType').value
                }

                return false;
            }
        }
        function fn_GoToURL(varURL) {

            if ((varURL != '') && (varURL.substr(0, 7) == 'http://') && (varURL.length > 7)) {
                var LoWindow = window.open(varURL, "", "");
                LoWindow.focus();
            }
            return false;
        }
        function ShowlinkedProjects(a) {
            window.open("../opportunity/frmLinkedProjects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&opId=" + a, '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function DeleteRecord(button) {
            function DeleteOrderConfirmFn(arg) {
                if (arg) {
                    __doPostBack("<%= btnActdelete.UniqueID%>", "");
                }
            }

            radconfirm("Are you sure, you want to delete the selected record?", DeleteOrderConfirmFn, 450, 100, null, "Delete Order");
        }

        function DeletMsg() {
            var bln = confirm("You�re about to remove the Stage from this Process, all stage data will be deleted")
            if (bln == true) {
                return true;
            }
            else {
                return false;
            }
        }
        /*Commented by Neelam - Obsolete function*/
        function OpenDocuments(a) {
            window.open("../Documents/frmSpecDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=O&yunWE=" + a, '', 'toolbar=no,titlebar=no,top=10,width=1000,height=450,left=10,scrollbars=yes,resizable=yes')
            return false;
        }

        function CannotShip() {

            radalert("You can't ship at this time because you don't have enough quantity on hand to support your shipment. Your options are to modify your order, or replenish inventory (to check inventory click on the edit link within the line item, then the value in the 'Products/Services' column):", 450, 180, "Shipping Qty")
            return false;
        }
        function AlertMsg() {
            if (ValidateUpdateSettings() == false) {
                return false;
            } else {
                return true;
            }
        }

        function ShowWindow(Page, q, att) {

            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                return false;

            }
        }
        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }
        function Save() {
            return validateCustomFields();
        }
        function OpenBiz(a, b) {
            window.open('../opportunity/frmBizDocs.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppType=' + b, '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenExpense(a, b, c) {
            window.open('../opportunity/frmOppExpense.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OPPStageID=' + a + '&Opid=' + b + '&DivId=' + c, '', 'toolbar=no,titlebar=no,left=500, top=300,width=650,height=300,scrollbars=yes,resizable=yes');
            return false;
        }
        function ValidateCheckBox(cint) {
            if (cint == 1) {
                if (document.getElementById('chkDClosed').checked == true) {
                    if (document.getElementById('chkDlost').checked == true) {
                        radalert("The Deal is already Lost !", 450, 100, "Deal Lost")
                        document.getElementById('chkDClosed').checked = false
                        return false;
                    }

                }
            }
            if (cint == 2) {
                if (document.getElementById('chkDlost').checked == true) {
                    if (document.getElementById('chkDClosed').checked == true) {
                        radalert("The Deal is already Closed !", 450, 100, "Deal Closed")
                        document.getElementById('chkDlost').checked = false
                        return false;
                    }
                    document.getElementById('chkActive').checked = false;
                }
            }

        }
        function DeleteMessage() {
            radalert("You Are not Authorized to Delete the Selected Record !", 450, 100, "Authorization");
            return false;
        }
        function OpenCreateOpp(a, b) {
            window.location.href = '../opportunity/frmCreateSalesPurFromOpp.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID=' + a + '&OppType=' + b + '&Created=false';
            //window.open('../opportunity/frmCreateSalesPurFromOpp.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID=' + a + '&OppType=' + b + '&Created=false', '', 'toolbar=no,titlebar=no,left=100,top=100,width=1000,height=700,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenConfSerItem(a, b, c) {
            window.open('../opportunity/frmAddSerializedItem.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID=' + a + '&ItemCode=' + b + '&OppType=' + c, '', 'toolbar=no,titlebar=no,left=100,top=100,width=530,height=400,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenTransfer(url) {
            window.open(url, '', "width=340,height=150,status=no,top=100,left=150");
            return false;
        }
        function OpenSalesTemplate(a) {
            window.open('../opportunity/frmSalesTemplate.aspx?OppId=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=1000,height=500,scrollbars=yes,resizable=yes');
            return false;
        }
        function ShowLayout(a, b, d) {
            window.open("../pagelayout/frmCustomisePageLayout.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Ctype=" + a + "&PType=3&FormId=" + d, '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
            return false;
        }

        /*Modified by Neelam Kapila || 10/07/2017 - Modified the way to show child record window */
        function openChild(a) {
            //window.open("../opportunity/frmChildOpportunity.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=" + a, '', 'toolbar=no,titlebar=no,width=1000,height=400,top=200,scrollbars=yes,resizable=yes');
            $find("<%= RadWinChildRecords.ClientID%>").show();
            return false;
        }
        function OpenRecurringDetailPage(oppId, Shipped) {
            window.open('../Accounting/frmRecurringDetails.aspx?oppId=' + oppId + '&Shipped=' + Shipped, '', 'toolbar=no,titlebar=no,left=300,top=300,width=700,height=350,scrollbars=yes,resizable=no')
            return false;
        }
        function OpenSetting(formID) {
            window.open('../Items/frmConfItemList.aspx?FormID=' + formID, '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        function EditImage(a, b) {
            window.open('../Items/frmOrderItemImage.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=' + a + '&OppItemCode=' + b, '', 'toolbar=no,titlebar=no,left=300, top=250,width=600,height=500,scrollbars=yes,resizable=yes');
            return false;
        }
        function EditItem(a) {
            document.getElementById('txtHidEditOppItem').value = a;
            document.getElementById('btnEditSelected').click();
            return false;
        }
        function Export() {
            document.getElementById('txtGridHTML').value = $('#gvProduct').html()//document.getElementById('gvProduct').innerHTML;
        }
        function SetWareHouseId(id) {
            var combo = $find('radWareHouse')
            var node = combo.findItemByValue(id);
            node.select();
        }
        function AddEditOrder(a, b, c) {
            if ($("[id$=hdnRecurrenceType]").val() != "") {
                radalert("You can't edit products & services when order or bizdoc within order is recurring.", 450, 100, "Recurring Order/BizDoc");
            }
            else {
                var isStockTransfer = parseInt(document.getElementById('hdnStockTransfer').value) || 0;
                window.open('../opportunity/frmAddEditOrder.aspx?opid=' + a + '&OppType=' + b + '&DivId=' + c + '&bitStock=' + isStockTransfer, '', 'toolbar=no,titlebar=no,left=300, top=250,width=1300,height=600,scrollbars=yes,resizable=yes');
            }
            return false;
        }
        function openGrossProfitEstimate(a) {
            window.open('../opportunity/frmGrossProfitEstimate.aspx?oppid=' + a, '', 'toolbar=no,titlebar=no,left=100,top=100,width=1000,height=600,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenProjectStageDetail(a, b) {
            if (a > 0 && b > 0) {
                var str;
                str = "../projects/frmProjects.aspx?ProId=" + a + "&StageId=" + b + "&SelectedIndex=1";
                window.location.href = str;
            }
            return false;
        }
        function OpenConfSerItem(a, b, c, d, e, f) {
            window.open('../opportunity/frmAddSerializedItem.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID=' + a + '&ItemCode=' + b + '&OppType=' + c + '&OppBizDocId=' + d + '&Qty=' + document.getElementById(e).value + '&LotId=' + f, '', 'toolbar=no,titlebar=no,left=100,top=100,width=530,height=400,scrollbars=yes,resizable=yes')
            return false;
        }


        $(window).on('load', function () {
            var pickPackShip1Width = getMaxWidthPickPackShip(".psscol1");
            var pickPackShip2Width = getMaxWidthPickPackShip(".psscol2");
            var pickPackShip3Width = getMaxWidthPickPackShip(".psscol3");
            if (pickPackShip1Width > 0) {
                $(".psscol1").width(pickPackShip1Width);
            }
            if (pickPackShip2Width > 0) {
                $(".psscol2").width(pickPackShip2Width);
            }
            if (pickPackShip3Width > 0) {
                $(".psscol3").width(pickPackShip3Width);
            }
        });

        function pageLoaded() {
            $('input[type=radio][name$=OrgGroup]').click(function () {
                $('input[type=radio][name$=OrgGroup]').each(function () {
                    $(this).prop('checked', false);
                });

                $(this).prop('checked', true);
            });

            $('[id$=divOrderSettings]').click(function () {
                $('[id$=pnlOrderSettings]').toggle();

                if ($('[id$=pnlOrderSettings]').css('display') == 'none') {
                    $('[id$=imgShowOrderSettings]').attr('src', '../images/Down-arrow-inv1.png');
                }
                else {
                    $('[id$=imgShowOrderSettings]').attr('src', '../images/Up-arrow-inv.png');
                }
            });

            var chkAllUnreceive = $("#chkAllUnReceive");
            chkAllUnreceive.click(function () {
                $("[id$=gvProduct] INPUT[id='chkUnReceive']:enabled").prop('checked', chkAllUnreceive.is(':checked'));
            });

            var chkAllPickPackSlip = $("#chkAllPickPackSlip");
            chkAllPickPackSlip.click(function () {
                $("[id$=gvProduct] INPUT[id='chkPickPackShip']:enabled").prop('checked', chkAllPickPackSlip.is(':checked'));
            });

            var chkBox = $("input[id$='chkSelectAll']");
            chkBox.click(function () {
                $("[id$=gvProduct] INPUT[id='chkSelect']:enabled").prop('checked', chkBox.is(':checked'));
            });

            var chkBoxPrint = $("input[id$='chkSelectPrintAll']");
            chkBoxPrint.click(function () {
                $("[id$=gvProduct] INPUT[id='chkSelectPrint']").prop('checked', chkBoxPrint.is(':checked'));
            });

            // To deselect CheckAll when a GridView CheckBox        // is unchecked
            $("[id$=gvProduct] INPUT[id='chkSelect']").click(
                function (e) {
                    if (!$(this)[0].checked) {
                        chkBox.prop("checked", false);
                    }
                });

            $("[id$=hplCreateOpp]").click(function (event) {
                if (event.target != null && event.target.tagName != "SPAN") {
                    window.location.href = '../opportunity/frmCreateSalesPurFromOpp.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID=' + $("[id$=OppID]").val() + '&OppType=' + $("[id$=OppType]").val() + '&Created=false';
                }
            });
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });


        function ReOpenDeal(button) {
            function ReopenOrderConfirmFn(arg) {
                if (arg) {
                    __doPostBack("<%= btnReOpenDeal.UniqueID %>", "");
            }
        }

        radconfirm("Are you sure, you want to Re-Open Deal? It will reverse Inventory of Item.", ReopenOrderConfirmFn, 450, 135, null, "Reopen Deal");
    }
    function OpenSalesOrderClone(OppId, OppType, c) {
        if (OppType == 1)
            window.open('../opportunity/frmNewOrder.aspx?OppType=1&OppStatus=' + document.getElementById('OppStatus').value + '&OppId=' + OppId + '&IsClone=true&rtyWR=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=1000,height=600,scrollbars=yes,resizable=yes');
        else
            window.open('../opportunity/frmNewPurchaseOrder.aspx?OppType=2&OppStatus=' + document.getElementById('OppStatus').value + '&OppId=' + OppId + '&IsClone=true&rtyWR=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=1000,height=600,scrollbars=yes,resizable=yes');

        return false;
    }
    function SubmitForPayment(a, b, c) {
        var str;
        str = "../opportunity/frmSubmitReturnForPayment.aspx?OpID=" + a + "&ReturnID=" + b + "&type=" + c;
        window.open(str, '', 'toolbar=no,titlebar=no,left=300,top=200,scrollbars=yes,resizable=yes');
    }
    function OpenShareRecord(a) {
        window.open("../common/frmShareRecord.aspx?RecordID=" + a + "&ModuleID=3", '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
        return false;
    }
    function OpenOppKitItems(a, b) {
        window.open('../opportunity/frmOppKitItems.aspx?OppID=' + a + '&OppItemCode=' + b, '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
    }
    function OpenTransactionDetails(a) {
        window.open("../opportunity/frmTransactionDetails.aspx?OppID=" + a, '', 'toolbar=no,titlebar=no,left=200, top=300,width=1024,height=350,scrollbars=no,resizable=yes')
        return false;
    }
    function OpenAutomationExecutionLog(a) {
        window.open("../opportunity/frmOpportunityAutomationQueueExecutionLog.aspx?OppID=" + a, '', 'toolbar=no,titlebar=no,left=200, top=300,width=1024,height=350,scrollbars=yes,resizable=yes')
        return false;
    }
    //$(document).ready(function () {

    //});
    function CheckBoxClicked(operation) {
        if (operation == 'Add') {
            document.getElementById('chkAddSalesTax').checked = true;
            document.getElementById('chkRemoveTax').checked = false;
        }

        if (operation == 'Remove') {
            document.getElementById('chkAddSalesTax').checked = false;
            document.getElementById('chkRemoveTax').checked = true;
        }
    }
    function openAddress(a, b) {
        window.open('../opportunity/frmEditOppAddress.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&AddType=' + a + '&OppID=' + b, '', 'toolbar=no,titlebar=no,left=100,top=350,width=500,height=350,scrollbars=yes,resizable=yes')
        return false;
    }
    function RefereshParentPage() {
        window.opener.location.href = window.opener.location.href;
    }
    function OpenMirrorBizDoc(a, b, c) {
        window.open('../opportunity/frmMirrorBizDoc.aspx?RefID=' + a + '&RefType=' + b + '&Print=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=1000,height=600,scrollbars=yes,resizable=yes');
        return false;
    }
    function FulfillOrderConfirm() {
        if (confirm('All items within this order will now be Invoiced.  Inventory items (if any) will be placed in a Packing-Slip and cleared from allocation. Want to proceed?')) {
            return true;
        }
        else {
            return false;
        }
    }
    function OpenGetRates(a) {
        //window.open('../opportunity/frmShippingBox.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=' + a + '&OppBizDocId=0', '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=1000,height=350,scrollbars=yes,resizable=yes');
        //return false;
    }

    function openReportList(a, b, c) {
        var h = screen.height;
        var w = screen.width;

        if (c > 0) {
            window.open('../opportunity/frmShippingReport.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppBizDocId=' + b + '&ShipReportId=' + c + '&OppId=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }
        else {
            window.open('../opportunity/frmShippingReportList.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=' + a + '&OppBizDocId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=800,height=500,scrollbars=yes,resizable=yes');
            return false;
        }
    }

    function openLabelList(a) {
        window.open('../opportunity/frmShippingLabelImages.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&BoxID=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=900,height=600,scrollbars=yes,resizable=yes');
    }

    function OpenOrderLog(a) {
        window.open('../opportunity/frmOrderLog.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=680,height=250,scrollbars=yes,resizable=yes');
        return false;
    }
    function ConfirmApplyCoupon() {
        if (confirm('Coupon Discount will be applied to the whole order. Do you wish to proceed ?')) {
            return true;
        }
        else {
            return false;
        }
    }
    function ConfirmShippingItem() {
        if (confirm('This will add selected shipping charges to the �Products & Services� section�.Do you want to proceed ?')) {
            return true;
        }
        else {
            return false;
        }
    }
    function ValidateUpdateSettings() {
        if (document.getElementById('OppType').value == 2) {
            if ($("[id$=calItemReceivedDate_txtDate]").val() == 0) {
                radalert("Enter Item Received Date", 450, 100, "Received Date")
                $("[id$=calItemReceivedDate_txtDate]").focus();
                return false;
            }
        }
    }
    function OpenItem(a) {
        str = "../Items/frmKitDetails.aspx?ItemCode=" + a;
        var win = window.open(str, '_blank');
        if (win) {
            //Browser has allowed it to be opened
            win.focus();
        } else {
            //Browser has blocked it
            alert('Please allow popups for this website');
        }
        //window.location.href = str;
    }
    function dropDownChange(id, className) {
        ddlValue = document.getElementById(id).value;

        $("." + className).each(function () {
            $(this).val(ddlValue);
        });
    }
    function ValidateShipperAccount() {
        if (document.getElementById('hdnShipperAccountNo') != null && document.getElementById('hdnShipperAccountNo').value.toString() != '') {
            return true;
        }
        else {
            radalert('Customer shipping account information not present. Go to the accounting sub-tab of this customer and enter the proper shipping account information for the shipping method selected.', 450, 135, "Shipping account");
            document.getElementById('chkUseCustomerShippingAccount').checked = false;
            return false;
        }
    }

    function AddUpdate() {
        return validateFixedFields();
        if (confirm('You are about to change the shipping rate for this order, are you sure you want to do this ? yes or no ?') == true) {
            return true;
        }
        else {
            return false;
        }
    }



    function openShippingLabel(a, b, c) {
        if (c > 0) {
            window.open('../opportunity/frmShippingBox.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=' + a + '&OppBizDocId=' + b + '&ShipCompID=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=1000,height=350,scrollbars=yes,resizable=yes');
        }
        else {
            window.open('../opportunity/frmShippingBox.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=' + a + '&OppBizDocId=' + b + '&ShipCompID=0', '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=1000,height=350,scrollbars=yes,resizable=yes');
        }
        return false;
    }

    function openLanedCost(a) {
        if ($("[id$=hftintshipped]").val() == "1") {
            window.open("../opportunity/frmLandedCostBill.aspx?OppId=" + a, '', 'toolbar=no,titlebar=no,left=300,width=800,height=500,scrollbars=yes,resizable=yes')
        } else {
            radalert("You need to close the PO before you can apply landed cost.", 450, 100, "Landed Cost");
        }
        return false;
    }


    function validate(i) {
        if (i < 10) {
            str = '0' + i
        }
        else {
            str = i
        }

        var OldQtyReceived = document.getElementById('lblnumUnitHour').innerHTML;
        var NewQtyReceived = document.getElementById('txtDYRentalIN').value;
        var NewQtyDYRentalOut = document.getElementById('txtDYRentalOut').value;
        var NewQtyRentalLost = document.getElementById('txtDYRentalLost').value;


        var OldRentalIN = document.getElementById('hfRentalIN').value;
        var OldRentalOUT = document.getElementById('hfRentalOut').value;
        var OldRentalLOST = document.getElementById('hfRentalLost').value;


        if (parseInt(NewQtyReceived) > 0) { //IN 
            if ((parseInt(NewQtyReceived)) > ((parseInt(OldRentalOUT) - (parseInt(OldRentalLOST) + parseInt(OldRentalIN))))) {
                radalert('You can not enter IN Qty more than Ordered', 450, 100, "IN Qty");
                return false;
            }
        }
        if (parseInt(NewQtyDYRentalOut) > 0) {//OUT
            if ((parseInt(NewQtyDYRentalOut)) > ((parseInt(OldQtyReceived)) - (parseInt(OldRentalOUT) + parseInt(OldRentalIN) + parseInt(OldRentalLOST)))) {
                radalert('You can not enter OUT Qty more than Ordered', 450, 100, "OUT Qty");
                return false;
            }
        }

        if (parseInt(NewQtyRentalLost) > 0) {//Lost
            if ((parseInt(NewQtyRentalLost)) > ((parseInt(OldRentalOUT) - (parseInt(OldRentalIN) + parseInt(OldRentalLOST))))) {
                radalert('You can not enter Lost Qty more than Ordered', 450, 100, "Lost Qty");
                return false;
            }
        }
        return true;
    }

    function CheckAllSameOpp() {

        for (var i = 2; i <= document.getElementById('gvProduct').rows.length; i++) {
            var str;
            if (i < 10) {
                str = '0' + i;

            }
            else {
                str = i;
            }
            return validate(i);
        }
    }

    function OpenRecurrenceDetail() {
        window.open('../opportunity/frmRecurrenceDetail.aspx?RecurConfigID=' + $("#hdnRecConfigID").val(), '', 'toolbar=no,titlebar=no,left=300,top=100,width=700,height=350,scrollbars=yes,resizable=no')
        return false;
    }

    function OpenOpp(a) {
        var str;
        str = "../opportunity/frmOpportunities.aspx?frm=deallist&OpID=" + a;
        document.location.href = str;
    }

    function OpenSertailLot(oppID, oppItemID, itemID, warehouseItemID, itemType, qty) {
        window.open('../opportunity/frmAddSerialLotNumbers.aspx?oppID=' + oppID + '&oppItemID=' + oppItemID + '&itemID=' + itemID + '&warehouseItemID=' + warehouseItemID + "&itemType=" + itemType + '&qty=' + qty, '', 'toolbar=no,titlebar=no,left=300,top=100,width=700,height=350,scrollbars=yes,resizable=no')
        return false;
    }

    function InvoiceAndShipItems() {
        function InvoiceAndShipItemsConfirmFn(arg) {
            if (arg) {
                __doPostBack('InvoiceAndShipItems', "true");
            }
        }

        Sys.Application.add_load(function () {
            radconfirm("To close this sales order, all items must be invoiced, and all inventory must have fulfilment orders. <br/><br/> Want BizAutomation to create remaining Invoice(s) & Fulfillment Order(s) ?", InvoiceAndShipItemsConfirmFn, 450, 180, null, "Invoice and Ship Items");
        });
    }

    function CreateBill() {
        function InvoiceAndShipItemsConfirmFn(arg) {
            if (arg) {
                __doPostBack('CreateBill', "true");
            }
        }

        Sys.Application.add_load(function () {
            radconfirm("To close this purchase order, all items must be billed. <br/><br/> Want BizAutomation to create an Bill for any item(s) and/or qty not already billed?", InvoiceAndShipItemsConfirmFn, 450, 135, null, "Create Bill");
        });
    }

    function BarcodeRecord() {
        var ItemListId = '';

        $('[id$=gvProduct] tr').each(function () {
            var checkBox = $(this).children().find('#chkSelectPrint');
            var hfItemCode = $(this).children().find('#hfItemCode');

            if (checkBox.length > 0 && hfItemCode.length > 0 && checkBox.is(":checked")) {
                ItemListId = (ItemListId == "" ? hfItemCode.val() : ItemListId + "," + hfItemCode.val());
            }
        });

        if (ItemListId == '') {
            radalert("Please select atleast one item to generate Barcode.", 450, 100, "Select Item");
            return false;
        }
        else {
            window.open('../Items/frmItemBarCode.aspx?ItemList=' + ItemListId, '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false;
        }
    }

    function FulfillmentPending(oppID) {
        function FulfillmentConfirmFn(arg) {
            if (arg) {
                document.location.href = "../opportunity/frmSalesFulfillmentOrder.aspx?fromOppID=" + oppID;
            }
        }

        Sys.Application.add_load(function () {
            radconfirm("Order contains fulfillment bizdoc(s) which is not fulfilled. Please fulfill bizdoc(s) first. Press ok to go to sales fulfillment screen.", FulfillmentConfirmFn, 450, 135, null, "Fulfillment bizdoc(s)");
        });
    }

    function InvoiceAgainstDeferredBizDocPending() {
        Sys.Application.add_load(function () {
            radalert('Order contains diferred income bizdoc(s) against which invoice(s) are not generated. Please generate invoice(s) first.', 450, 135, "Invoice Against Deferred Income BizDoc");
        });
    }


    function PurchaseClearingAccountNotExists() {
        Sys.Application.add_load(function () {
            radalert('Please Set Default Purchase Clearing account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save.', 450, 100, "Bill Created");
        });
    }

    function PurchaseVarianceAccountNotExists() {
        Sys.Application.add_load(function () {
            radalert('Please Set Default Purchase Price Variance account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save.', 450, 100, "Bill Created");
        });
    }

    function OpenHistory(a) {
        window.open('../common/frmRecordHistory.aspx?numMouduleID=3&numRecordID=' + a, '', 'toolbar=no,titlebar=no,left=300,top=100,width=980,height=600,scrollbars=yes,resizable=no')
        return false;
    }

    function OpenItemReleaseDates(oppID, oppItemID) {
        window.open('../opportunity/frmItemReleaseDates.aspx?numOppID=' + oppID + '&numOppItemID=' + oppItemID, '', 'toolbar=no,titlebar=no,left=300,top=100,width=768,height=600,scrollbars=yes,resizable=no')
        return false;
    }

    function ConfirmDelete() {
        var isRowSelected = false

        $("[id$=gvProduct]").children("tbody").children('tr').not(':first').each(function () {
            if ($(this).find('input[id$=chkSelect]').is(':checked')) {
                isRowSelected = true;
            }
        });

        if (!isRowSelected) {
            alert("Select atleast one item.");
            return false;
        } else {
            if (confirm('Selected item will be deleted. Do you want to proceed?')) {
                return true;
            } else {
                return false;
            }
        }
    }

    /*Function added by Neelam on 10/07/2017 - Added functionality to open Child Record window in modal pop up*/
    function OpenDocumentFiles(divID) {
        $find("<%= RadWinDocumentFiles.ClientID%>").show();
    }

    /*Function added by Neelam on 02/14/2018 - Added functionality to close document modal pop up*/
    function CloseDocumentFiles(mode) {
        var radwindow = $find('<%=RadWinDocumentFiles.ClientID %>');
        radwindow.close();

        if (mode == 'C') { return false; }
        else if (mode == 'L') {
            __doPostBack("<%= btnLinkClose.UniqueID%>", "");
        }
}

function DeleteDocumentOrLink(fileId) {
    if (confirm('Are you sure, you want to delete the selected item?')) {
        $('#hdnFileId').val(fileId);
        __doPostBack("<%= btnDeleteDocOrLink.UniqueID%>", "");
    }
    else {
        return false;
    }
}

$(document).ready(function ($) {
    requestStart = function (target, arguments) {
        if (arguments.get_eventTarget().indexOf("btnAttach") > -1) {
            arguments.set_enableAjax(false);
        }
        if (arguments.get_eventTarget().indexOf("btnLinkClose") > -1) {
            arguments.set_enableAjax(false);
        }
    }

    var prm = Sys.WebForms.PageRequestManager.getInstance();
    prm.add_pageLoaded(pageLoadedMaster);

});

function pageLoadedMaster() {
    BindInlineEdit();
    //BindJquery();

    $("table[id$=gvProduct]").colResizable({
        //fixed: false,
        liveDrag: true,
        gripInnerHtml: "<div class='grip2'></div>",
        draggingClass: "dragging",
        minWidth: 30,
        resizeMode: "overflow",
        onResize: onGridColumnResized
    });
}

// Grid Column Resize
//$(document).ready(function () {
//    var prm = Sys.WebForms.PageRequestManager.getInstance();
//    prm.add_pageLoaded(pageLoadedMaster);
//});

var onGridColumnResized = function (e) {
    var columns = $(e.currentTarget).find("th");
    var msg = "";
    columns.each(function () {
        //console.log($(this));

        var thId = $(this).attr("id");
        var thWidth = $(this).width();
        msg += (thId || "anchor") + ':' + thWidth + ';';
    }
    )
    console.log(msg);
    $.ajax({
        type: "POST",
        url: "../common/Common.asmx/SaveGridColumnWidth",
        data: "{str:'" + msg + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json"
    });
};

function OpenWorkOrder(woID) {
    window.open('../items/frmWorkOrder.aspx?WOID=' + woID, '_blank');
    return false;
}

function UpdateParentKit(ddlParentKit) {
    try {
        $("#UpdateProgress").show();
        var oppID = parseInt($("#OppID").val());
        var oppItemID = parseInt($(ddlParentKit).closest("tr").find("#hfOppItemtCode").val());

        $.ajax({
            type: "POST",
            url: '../common/Common.asmx/UpdateParentKit',
            data: JSON.stringify({ "OppID": oppID, "OppItemID": oppItemID, "ParentKitOppItemID": parseInt($(ddlParentKit).val()) }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                alert("Kit updated successfully.");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("Error occurred while updating kit.");
            }
        });
    } catch (e) {
        alert("Unknown error occurred while updating kit.");
    } finally {
        $("#UpdateProgress").hide();
    }


    return false;
}

function SetAsWinningBid(btnWinningBid) {
    if (confirm('Selected kit will be set as winning bid. Do you want to proceed?')) {
        try {
            $("#UpdateProgress").show();
            var oppID = parseInt($("#OppID").val());
            var oppItemID = parseInt($(btnWinningBid).closest("tr").find("#hfOppItemtCode").val());

            $.ajax({
                type: "POST",
                url: '../common/Common.asmx/SetAsWinningBid',
                data: JSON.stringify({ "OppID": oppID, "OppItemID": oppItemID }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    $("#gvProduct tr").not(":first").each(function (index, tr) {
                        if (parseInt($(tr).find("#hfOppItemtCode").val()) === oppItemID) {
                            $(tr).find("#btnWinningBid").hide();
                        } else {
                            $(tr).find("#btnWinningBid").show();
                        }
                    });

                    alert("Winning bid is set successfully.");
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert("Error occurred while setting winning bid.");
                },
                complete: function () {
                    $("#UpdateProgress").hide();
                }
            });
        } catch (e) {
            alert("Unknown error occurred while setting winning bid.");
        } finally {
            $("#UpdateProgress").hide();
        }
    }

    return false;
}

function CloneOrderLineItem(img) {
    try {
        $("#UpdateProgress").show();
        var oppID = parseInt($("#OppID").val());
        var oppItemID = parseInt($(img).closest("tr").find("#hfOppItemtCode").val());

        $.ajax({
            type: "POST",
            url: '../common/Common.asmx/CloneOrderLineItem',
            data: JSON.stringify({ "OppID": oppID, "OppItemID": oppItemID }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (result) {
                alert("Item is cloned successfully.");
                document.location.href = document.location.href;
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                if (IsJsonString(XMLHttpRequest.responseText)) {
                    if ($.parseJSON(XMLHttpRequest.responseText).d === "CLOSED_ORDER") {
                        alert("Can't clone item in closed opp/order.");
                    } else {
                        alert("Error occurred while cloning item.");
                    }
                } else {
                    alert("Error occurred while cloning item.");
                }
            },
            complete: function () {
                $("#UpdateProgress").hide();
            }
        });
    } catch (e) {
        alert("Unknown error occurred while cloning item.");
    } finally {
        $("#UpdateProgress").hide();
    }

    return false;
}

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(window).on('load', function () {
                //console.log("window on load");
                //setupResponsiveTables();
            });
        });

        //Added By:sachin Sadhu ||Date:29thJuly2014
        //Puprose : to make product/service grid responsive
        function setupResponsiveTables() {
            $('[id$=gvProduct]').responsiveTable();

            var $table = $('[id$=gvProduct]');
            var $fixedColumn = $('.responsiveTableStatic');

            $fixedColumn.find('th:not(:first-child),td:not(:first-child)').remove();

            $fixedColumn.children("tbody").children('tr').each(function (i, elem) {
                $(this).height($table.children("tbody").children('tr:eq(' + i + ')').height() < 25 ? 25 : $table.children("tbody").children('tr:eq(' + i + ')').height());
            });
        }
        function OpenHelpforBizDocs() {
            window.open('../Help/Opportunities_Orders_BizDocs_.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        function OpenHelpforCorrespondence() {
            window.open('../Help/Opportunities_Orders_Correspondence_.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        function OpenHelpforDetailPage() {
            window.open('../Help/Opportunities_Orders_Detail_Page_.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        function OpenHelpforMilestonesStages() {
            window.open('../Help/Opportunities_Orders_Milestones_Stages_.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        function OpenHelpforProductsServices() {
            window.open('../Help/Opportunities_Orders_Products_Services_.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        function OpenHelpforSalesFulfillment() {
            window.open('../Help/Opportunities_Orders_Sales_Fulfillment_.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        function OpenHelpforShippingPackaging() {
            window.open('../Help/Opportunities_Orders_Shipping_Packaging_.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        function OpenCreateDopshipPOWindow(oppID) {
            var h = screen.height;
            var w = screen.width;

            window.open('../opportunity/frmCreateDropshipPO.aspx?numOppID=' + oppID, '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenCreateBackOrderPOWindow(oppID) {
            var h = screen.height;
            var w = screen.width;

            window.open('../opportunity/frmCreateBackOrderPO.aspx?numOppID=' + oppID, '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenCustomerChangeWindow() {
            var h = screen.height;
            var w = screen.width;

            window.open('../common/frmChangeOrganization.aspx?numOppID=' + $("[id$=OppID]").val(), '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }

        function Open3PLEDIHistory(oppID) {
            var h = screen.height;
            var w = screen.width;

            if (w > 600) {
                w = w - 500;
            }


            window.open('../opportunity/frm3PLEDIHistory.aspx?numOppID=' + oppID, 'EDIHistory', 'toolbar=no,titlebar=no,top=50,left=50,width=' + w + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }

        function OnItemReleaseDateSelected(sender, e) {
            if (e.get_newDate() != null) {
                var id = sender.get_element().id + "_wrapper";
                var oppItemID = $get(id).getAttribute("OppItemID");
                var oppID = $get(id).getAttribute("OppID");

                var month = e.get_newDate().getMonth() + 1;
                var day = e.get_newDate().getDate();
                var year = e.get_newDate().getFullYear();
                var releaseDate = (month.toString().length == 1 ? "0" : "") + month + "/" + (day.toString().length == 1 ? "0" : "") + day + "/" + year;

                $.ajax({
                    type: "POST",
                    url: "../common/Common.asmx/UpdateOppItemReleaseDate",
                    data: "{oppID:" + oppID + ",oppItemID:" + oppItemID + ",releaseDate:'" + releaseDate + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        if (msg.d != null && msg.d === "Invalid Date") {
                            alert("Select valid release date");
                        } else if (msg.d != null && msg.d.indexOf("Error") >= 0) {
                            alert(msg.d);
                        } else {
                            var lblReleaseDateID = "lblItemReleaseDate" + oppItemID
                            if ($("#" + lblReleaseDateID) != null) {
                                $("#" + lblReleaseDateID).html(releaseDate);
                            }
                        }

                    }
                });
                //logEvent("OnDateSelected: " + e.get_newDate().toDateString() + " selected in " + sender.get_id() + "<br />");
            }
        }

        function parseCurrency(num) {
            return parseFloat(num.replace(/,/g, ''));
        }

        function validateQty(txtUnitsID, OppType, oppID, oppItemCode) {
            var Units = isNaN(parseCurrency(document.getElementById(txtUnitsID).value)) ? 0 : parseCurrency(document.getElementById(txtUnitsID).value);
            var OldUnits = isNaN(parseCurrency($("#" + txtUnitsID).parent().parent().find("[id$=hfUnitHour]").val())) ? 0 : parseCurrency($("#" + txtUnitsID).parent().parent().find("[id$=hfUnitHour]").val());
            var UOMConversionFactor = isNaN(parseCurrency($("#" + txtUnitsID).parent().parent().find("[id$=hfUOMConversionFactor]").val())) ? 0 : parseCurrency($("#" + txtUnitsID).parent().parent().find("[id$=hfUOMConversionFactor]").val());
            var numUnitHourReceived = isNaN(parseCurrency($("#" + txtUnitsID).parent().parent().find("[id$=hfUnitHourReceived]").val())) ? 0 : parseCurrency($("#" + txtUnitsID).parent().parent().find("[id$=hfUnitHourReceived]").val());
            var numQtyShipped = isNaN(parseCurrency($("#" + txtUnitsID).parent().parent().find("[id$=hfQtyShipped]").val())) ? 0 : parseCurrency($("#" + txtUnitsID).parent().parent().find("[id$=hfQtyShipped]").val());

            if (OppType == 1) {
                if (parseFloat(numQtyShipped) > parseFloat(Units * UOMConversionFactor)) {
                    document.getElementById(txtUnitsID).value = OldUnits / UOMConversionFactor;

                    alert('Given input is not valid, As ' + numQtyShipped + ' units of selected item is already received.');
                    return false;
                }
            }
            else if (OppType == 2) {
                if (parseFloat(numUnitHourReceived) > parseFloat(Units * UOMConversionFactor)) {
                    document.getElementById(txtUnitsID).value = OldUnits / UOMConversionFactor;

                    alert('Given input is not valid, As ' + numUnitHourReceived + ' units of selected item is already shipped.');
                    return false;
                }
            }

            try {
                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/Orders/UpdateOrderedItemQuantity',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "mode":1
                        ,"oppID": oppID
                        , "oppItemID": oppItemCode
                        , "units": parseFloat(Units * UOMConversionFactor)
                        , "price": 0
                        ,"notes": ""
                    }),
                    success: function (data) {
                        //alert("Quantity updated successfully.");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert($.parseJSON(jqXHR.responseText));
                    }
                });
            } catch (err) {
                alert("Unknown error occurred while updating quantity.");
            }

            return false;
        }

        function UpdateOrderItemPrice(txtmonPriceUOM, OppType, oppID, oppItemCode) {
            try {
                var price = isNaN(parseCurrency(document.getElementById(txtmonPriceUOM).value)) ? 0 : parseCurrency(document.getElementById(txtmonPriceUOM).value);
                var UOMConversionFactor = isNaN(parseCurrency($("#" + txtmonPriceUOM).parent().parent().find("[id$=hfUOMConversionFactor]").val())) ? 0 : parseCurrency($("#" + txtmonPriceUOM).parent().parent().find("[id$=hfUOMConversionFactor]").val());

                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/Orders/UpdateOrderedItemQuantity',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "mode": 2
                        , "oppID": oppID
                        , "oppItemID": oppItemCode
                        , "units": 0
                        , "price": parseFloat(price * UOMConversionFactor)
                        , "notes": ""
                    }),
                    success: function (data) {
                        //alert("Price updated successfully.");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert($.parseJSON(jqXHR.responseText));
                    }
                });
            } catch (err) {
                alert("Unknown error occurred while updating price.");
            }

            return false;
        }

        function UpdateOrderItemNotes(txtvcNotes, OppType, oppID, oppItemCode) {
            try {
                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/Orders/UpdateOrderedItemQuantity',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "mode": 3
                        , "oppID": oppID
                        , "oppItemID": oppItemCode
                        , "units": 0
                        , "price": 0
                        , "notes": document.getElementById(txtvcNotes).value
                    }),
                    success: function (data) {
                        //alert("Notes updated successfully.");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert($.parseJSON(jqXHR.responseText));
                    }
                });
            } catch (err) {
                alert("Unknown error occurred while updating notes.");
            }

            return false;
        }

        function UpdateOrderItemDescription(txtvcItemDesc, OppType, oppID, oppItemCode) {
            try {
                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/Orders/UpdateOrderedItemQuantity',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "mode": 4
                        , "oppID": oppID
                        , "oppItemID": oppItemCode
                        , "units": 0
                        , "price": 0
                        , "notes": document.getElementById(txtvcItemDesc).value
                    }),
                    success: function (data) {
                        //alert("Notes updated successfully.");
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert($.parseJSON(jqXHR.responseText));
                    }
                });
            } catch (err) {
                alert("Unknown error occurred while updating notes.");
            }

            return false;
        }

        function ConfirmUnreceive() {
            if (confirm("Received quantity will be reverted. Do you want to proceed?")) {
                return true;
            }

            return false;
        }

        function HideEDIPopup(divId) {

            document.getElementById(divId).style.display = "none";
        }

        function ValidateEDITextboxes() {

            var rem = document.querySelectorAll("[id*='txtEDI']"), i = 0;

            for (var i = 0; i < rem.length; i++) {

                if (rem[i].value === "") {

                    alert("form could not be sent one input text field is empty");
                    return false;
                }
            }
            return true;
        }

        function OnItemRequiredDateSelected(sender, e) {

            if (e.get_newDate() != null) {
                var id = sender.get_element().id + "_wrapper";
                var oppItemID = $get(id).getAttribute("OppItemID");
                var oppID = $get(id).getAttribute("OppID");

                var month = e.get_newDate().getMonth() + 1;
                var day = e.get_newDate().getDate();
                var year = e.get_newDate().getFullYear();
                var requiredDate = (month.toString().length == 1 ? "0" : "") + month + "/" + (day.toString().length == 1 ? "0" : "") + day + "/" + year;

                var lblRequiredDateID = "txtItemRequiredDate" + oppItemID

                $.ajax({
                    type: 'POST',
                    url: "../Opportunity/frmOpportunities.aspx/UpdateItemRequiredDate",
                    data: "{oppID:" + oppID + ",oppItemID:" + oppItemID + ",ItemRequiredDate:'" + requiredDate + "'}",
                    contentType: 'application/json; charset=utf-8',
                    dataType: "json",
                    success: function (msg) {

                        if (msg.d != null && msg.d === "Invalid Date") {
                            alert("Select valid release date");
                        } else if (msg.d != null && msg.d.indexOf("Error") >= 0) {
                            alert(msg.d);
                        } else {
                            document.getElementById(lblRequiredDateID).value = requiredDate;
                        }
                    }
                });
            }
        }
        function DealStatusChanged() {
            $("#openCloseDealDiv").css("display", "inline");
            $("#modalConfirmDealStatus").modal("hide");
            location.reload();
        }
        function CreateOrMapItem(oppID, oppItemID) {
            window.open('../Items/frmAddItem.aspx?numOppID=' + oppID + '&numOppItemID=' + oppItemID, '', 'toolbar=no,titlebar=no,left=300,top=100,width=768,height=600,scrollbars=yes,resizable=no')
            return false;
        }
        function OpenVendorCostTable(numVendorTCode, numVendorID, numItemCode) {
            var w = screen.width;
            var h = 700;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            window.open("../common/frmManageVendorCostTable.aspx?IsUpdateDefaultVendorCostRange=0&VendorID=" + numVendorID + "&ItemID=" + numItemCode + "&VendorTCode=" + numVendorTCode, '', 'toolbar=no,titlebar=no,width=' + w + ',height=' + h + ',top=' + top + ',left=' + left + ',scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenInTransit(selectedItemCode, selectedWarehouseItemID) {
            var w = screen.width - 200;
            var h = screen.height - 100;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            window.open('../opportunity/frmOrderTransit.aspx?ItemCode=' + selectedItemCode + '&WarehouseItemID=' + selectedWarehouseItemID, '', 'toolbar=no,titlebar=no,width=' + w + ',height=' + h + ',top=' + top + ',left=' + left + ',scrollbars=yes,resizable=yes')
            return false;
        }
        function getMaxWidthPickPackShip(className) {
            max = 0;
            $(className).each(function () {
                c_width = parseInt($(this).width());
                if (c_width > max) {
                    max = c_width;
                }
            });
            return max;
        }
    </script>
    <style>
        .tblNoBorder td, .tblNoBorder th {
            border: 0px solid #fff !important;
        }

        @media (max-width:40em) {
            .tblNoBorder table, .tblNoBorder thead, .tblNoBorder tbody, .tblNoBorder tfoot, .tblNoBorder th, .tblNoBorder td, .tblNoBorder tr {
                display: block;
            }

            .tblNoBorder .editable_select {
                min-height: 30px;
            }

            .tblNoBorder td, .tblNoBorder th {
                text-align: left;
            }
        }

        /*div.RadUpload .ruFakeInput {
            visibility: hidden;
            width: 0;
            padding: 0;
        }

        div.RadUpload .ruFileInput {
            width: 1;
        }*/

        .hidden, .HideDateInput {
            display: none;
        }

        .ItemReleaseDate .rcInputCell {
            width: 0px !important;
        }

        .ItemReleaseDate input {
            min-width: 0px !important;
        }

        .VerticalAligned {
            vertical-align: top;
        }

        img.btn-icon-template {
            height: 23px;
        }

        .box-header > .box-tools {
            top: 3px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    <asp:Label ID="lblTitle" runat="server">Order</asp:Label>&nbsp;&nbsp;<asp:Label ID="lblTitleDetails" runat="server" ForeColor="Gray" />
    <a href="#" id="aHelp" runat="server"><label class="badge bg-yellow">?</label></a>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div class="rwDialogButtons">
                    <a class="rwPopupButton" onclick="$find('{0}').close(true); return false;"><span class="rwOuterSpan"><span class="rwInnerSpan">Yes</span></span></a>
                    <a class="rwPopupButton" onclick="$find('{0}').close(false); return false;"><span class="rwOuterSpan"><span class="rwInnerSpan">No</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
    <asp:Button ID="btnDeleteDocOrLink" OnClick="btnDeleteDocOrLink_Click" runat="server" CssClass="hidden"></asp:Button>
    <asp:HiddenField runat="server" ID="hdnFileId" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
    <div class="row" id="divPageError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>
                <p>
                    <asp:Label ID="lblPageError" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-md-3">
            <div class="form-inline">
                <div class="pull-left" style="width: 90%">
                    <div class="callout calloutGroup bg-theme">
                        <asp:Label ID="lblCustomerType" CssClass="customerType" runat="server"></asp:Label>
                        <span>
                            <u>
                                <asp:HyperLink ID="hplCustomer" runat="server" CssClass="hyperlink" Style="font-size: 16px !important; font-weight: bold !important; font-style: italic !important">
                                </asp:HyperLink></u>

                            <asp:Label ID="lblRelationCustomerType" CssClass="customerType" runat="server"></asp:Label>
                            <div>
                                <asp:Label ID="lblDealCompletedDate" runat="server" CssClass="text" Visible="false"></asp:Label>
                                <asp:HyperLink ID="hplOppLinked" runat="server" CssClass="hyperlink">
                                </asp:HyperLink>
                            </div>

                        </span>
                    </div>
                    <div class="record-small-box record-small-group-box">
                        <strong>
                            <asp:Label ID="lblContactName" runat="server" CssClass="text-color-theme" Text=""></asp:Label></strong>
                        <span class="contact">
                            <asp:Label ID="lblPhone" runat="server" Text=""></asp:Label></span>
                        <a id="btnSendEmail" runat="server" href="#">
                            <img src="../images/msg_unread_small.gif" />
                        </a>
                    </div>
                </div>
                <div class="pull-left">
                    <asp:ImageButton runat="server" ID="ibChangeCustomer" OnClientClick="return OpenCustomerChangeWindow();" ImageUrl="~/images/edit28.png" Style="height: 20px; margin-top: 13px;" />
                </div>
            </div>
        </div>
        <div class="col-md-4" style="padding: 0px; width: 36%;">
            <div class="record-small-box record-small-group-box createdBySection">

                <a href="#">
                    <label>Created</label>
                </a>
                <span class="innerCreated">
                    <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
                </span>
                <asp:HyperLink ID="hplTransfer" runat="server" Visible="true" Text="Record Owner:"
                    ToolTip="Transfer Ownership">
                     Owner
                </asp:HyperLink>
                <span class="innerCreated">
                    <asp:Label ID="lblRecordOwner" runat="server"></asp:Label>
                </span>
                <a id="hplHistory" runat="server" href="#">Modified</a>
                <span class="innerCreated">
                    <asp:Label ID="lblLastModifiedBy" runat="server"></asp:Label>
                </span>
            </div>
        </div>
        <div class="col-md-5" style="padding-left: 0px; width: 39%;">
            <div class="pull-right createdBySection">
                <%--<asp:Button ID="btnFulfillOrder" Text="Fulfill Order" runat="server" CssClass="button"
                        Visible="false"></asp:Button>--%>
                <%--<asp:LinkButton ID="lkbDemoteToOpportunity" runat="server" Visible="false" CssClass="btn btn-danger">&nbsp;&nbsp;Demote to Opportunity</asp:LinkButton>--%>
                <asp:LinkButton ID="btnReceivedOrShipped" runat="server" Visible="false" CssClass="btn btn-success"><i class="fa fa-times"></i>&nbsp;&nbsp;Close Order</asp:LinkButton>
                <asp:LinkButton ID="btnReOpenDeal" CssClass="btn btn-primary" Visible="false" runat="server"><i class="fa fa-folder-open-o"></i>&nbsp;&nbsp;Re-Open Deal</asp:LinkButton>
                <asp:LinkButton ID="btnResourceScheduling" runat="server" CssClass="btn btn-primary" UseSubmitBehavior="false"><i class="fa fa-calendar"></i>&nbsp;&nbsp;Schedule Resources</asp:LinkButton>

                <asp:LinkButton ID="btnEdit" runat="server" CssClass="btn btn-primary"><i class="fa fa-pencil-square-o"></i>&nbsp;&nbsp;Edit</asp:LinkButton>
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save &amp; Close</asp:LinkButton>
                <asp:LinkButton ID="btnActdelete" runat="server" CssClass="btn btn-danger" Text=""><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
                <asp:LinkButton ID="btnCancel" runat="server" Text="" CssClass="btn btn-primary"><i class="fa fa-times"></i>&nbsp;&nbsp;Close</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    <asp:HyperLink ID="hplGrossProfitEstimate" runat="server" CssClass="btn btn-default" Visible="false" Font-Bold="true" BackColor="Transparent"><i class="fa fa-usd" aria-hidden="true" style="color:#00B050"></i>&nbsp;&nbsp;Gross Profit</asp:HyperLink>
    <asp:HyperLink ID="hpClone" runat="server" CssClass="btn btn-default" Visible="true" Font-Bold="true" BackColor="Transparent"><i class="fa fa-clone" aria-hidden="true" style="color:blue"></i>&nbsp;&nbsp;Clone Order</asp:HyperLink>
    <asp:HyperLink ID="hplSalesTemplate" runat="server" CssClass="btn btn-default" Style="padding: 4px 8px;" Font-Bold="true" BackColor="Transparent">Item Templates</asp:HyperLink>
    <asp:HyperLink ID="hplCreateOpp" runat="server" Visible="false" CssClass="btn btn-default btn-sm" Style="padding: 4px 8px;" Font-Bold="true" BackColor="Transparent"></asp:HyperLink>
    <asp:HyperLink ID="hrfTransactionDetail" runat="server" CssClass="btn btn-default" Style="padding: 4px 8px; height: 33px"><img src="../images/creditcards.png" > CreditCard Transactions</asp:HyperLink>
    <asp:HyperLink ID="hrfAutomationExecutionLog" CssClass="btn btn-default" runat="server" Style="padding: 4px 8px; height: 33px"><img src="../images/GLReport.png" border="0" /> Automation Execution Log</asp:HyperLink>
    <div class="btn btn-default" runat="server" id="btnLayout" style="padding: 4px 8px; height: 33px">
        <span><i class="fa fa-columns"></i>&nbsp;&nbsp;Layout</span>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:HiddenField ID="hdnTrackingNo" Value="0" runat="server" />
    <asp:HiddenField ID="hdnShippingCompanyId" Value="0" runat="server" />
    <telerik:RadTabStrip ID="radOppTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
        Skin="Default" ClickSelectedTab="True" MultiPageID="radMultiPage_OppTab" SelectedIndex="0"
        OnClientTabSelecting="beforeSelectEvent" AutoPostBack="false" OnClientTabSelected="OnClientTabSelected"
        OnClientLoad="OnClientLoad">
        <Tabs>
            <telerik:RadTab Text="Opportunity Details" Value="Details" PageViewID="radPageView_Details">
            </telerik:RadTab>
            <telerik:RadTab Text="Milestones & Stages" Value="Milestone" PageViewID="radPageView_Milestone">
            </telerik:RadTab>
            <telerik:RadTab Text="Associated Contacts" Value="AssociatedContacts" PageViewID="radPageView_AssociatedContacts">
            </telerik:RadTab>
            <telerik:RadTab Text="Products & Services" Value="ProductsServices" PageViewID="radPageView_ProductsServices" PostBack="true" runat="server">
            </telerik:RadTab>
            <telerik:RadTab Text="BizDocs" Value="BizDocs" PageViewID="radPageView_BizDocs">
            </telerik:RadTab>
            <telerik:RadTab Text="Correspondence" Value="Correspondence" PageViewID="radPageView_Correspondence">
            </telerik:RadTab>
            <telerik:RadTab Text="Shipping & Packaging" Value="ShippingAndPackaging" PageViewID="radPageView_ShippingAndPackaging">
            </telerik:RadTab>
            <telerik:RadTab Text="Collaboration" Value="Collaboration" PageViewID="radPageView_Collaboration">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage_OppTab" runat="server" CssClass="pageView">
        <telerik:RadPageView ID="radPageView_Details" runat="server">
            <div class="table-responsive">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="pull-left">
                            <asp:ImageButton ID="imgOrder" runat="server" ImageUrl="~/images/icons/cart_large.png" />
                        </div>
                        <div class="pull-right">
                            <table border="0" style="width: 100%; padding-right: 5px;">
                                <tr>
                                    <td style="text-align: left;">
                                        <table id="tblDocuments" runat="server">
                                        </table>
                                    </td>
                                    <td>
                                        <asp:ImageButton ID="imgOpenDocument" runat="server" ImageUrl="~/images/icons/drawer.png" Style="height: 30px;" />
                                    </td>
                                    <td style="text-align: left;">
                                        <asp:HyperLink ID="hplOpenDocument" runat="server" Font-Bold="true" Font-Underline="true" Text="Attach or Link" Style="display: inline-block; vertical-align: top; line-height: normal;"></asp:HyperLink>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkEDIOrder" OnClick="lnkEDIOrder_Click" Visible="false" Text="EDI Fields" Font-Underline="true" runat="server"></asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <asp:Table ID="tblMain" CellPadding="3" CellSpacing="3" align="center" Width="100%" GridLines="none" border="0" runat="server" CssClass="table table-responsive tblNoBorder">
                </asp:Table>
                <asp:Table ID="tblProspects" CellPadding="0" CellSpacing="0" Height="300" runat="server"
                    Width="100%" GridLines="None">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <table width="100%">

                                <tr>
                                    <td id="tdOrderSetting" runat="server" colspan="2" valign="top">
                                        <fieldset>

                                            <div class="box box-primary box-solid">
                                                <div class="box-header with-border">
                                                    <h3 class="box-title">Order Settings</h3>
                                                    <div class="box-tools pull-right">
                                                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                                            <i class="fa fa-minus"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="box-body" style="display: block;">
                                                    <asp:Panel ID="pnlOrderSettings" runat="server">
                                                        <table width="100%" class="tblOrderSettings table table-responsive tblNoBorder">
                                                            <tr class="tblOrderSettingstr">
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td align="right">
                                                                    <asp:Button ID="btnUpdate" runat="server" CssClass="btn btn-primary" Text="Update"></asp:Button>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="trShipping" class="tblOrderSettingstr">
                                                                <td align="right">&nbsp;Terms :&nbsp;
                                                                </td>
                                                                <td>
                                                                    <asp:DropDownList ID="ddlNetDays" runat="server" CssClass="form-control">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td class="normal1" align="right">Add/Remove Sales Tax :</td>
                                                                <td>
                                                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlSalesTax">
                                                                        <asp:ListItem Text="Default" Value="0"></asp:ListItem>
                                                                        <asp:ListItem Text="Add Sales Tax" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Text="Remove Sales Tax" Value="2"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4" valign="top">
                                                                    <table width="100%" border="0" align="left" runat="server" class="tblOrderSettings" id="tblCurrency" cellpadding="0" cellspacing="0">
                                                                        <tr>
                                                                            <td align="right">&nbsp;Exchange Rate :
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label runat="server" ID="lblForeignCurr"></asp:Label>&nbsp;=&nbsp;
                                                                <asp:TextBox ID="txtExchangeRate" runat="server" Width="40px"></asp:TextBox>&nbsp;&nbsp;<asp:Label
                                                                    runat="server" ID="lblBaseCurr" Text="USD"></asp:Label>
                                                                            </td>
                                                                            <td></td>
                                                                            <td></td>
                                                                        </tr>
                                                                    </table>
                                                                    <table width="100%" align="left" border="0" runat="server" class="tblOrderSettings" id="tblItemReceivedDate" visible="false">
                                                                        <tr>
                                                                            <td class="normal1" align="right">Item Received Date :
                                                                            </td>
                                                                            <td>
                                                                                <BizCalendar:Calendar ID="calItemReceivedDate" runat="server" ClientIDMode="AutoID" />
                                                                            </td>
                                                                            <td></td>
                                                                            <td></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" id="trLandedCost" visible="false" class="tblOrderSettingstr">
                                                                <td align="right">
                                                                    <asp:HyperLink ID="hplLandedCost" runat="server" Text="Landed Cost :"></asp:HyperLink>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="lblLanedCost" runat="server"></asp:Label>
                                                                </td>
                                                                <td>&nbsp;</td>
                                                                <td></td>
                                                            </tr>
                                                            <tr id="trRecur" runat="server">
                                                                <td colspan="4">
                                                                    <table id="tblRecurInsert" width="100%" runat="server" class="tblOrderSettings table table-responsive tblNoBorder">
                                                                        <tr id="trRecurring1" class="tblOrderSettingstr">
                                                                            <td align="right">Recur :
                                                                            </td>
                                                                            <td>
                                                                                <asp:CheckBox ID="chkEnableRecurring" onClick="ToggleRecurringVisibility(this);" runat="server" Style="margin-left: -3px;" />
                                                                                <asp:Label ID="Label1" Text="[?]" CssClass="tip" runat="server" ToolTip="Will create a clone of the SO based on the recurring 
                                                                                                                                                        pattern selected. The cloned SO will have updated dates on the SO & the Invoice 
                                                                                                                                                        (Invoice Date & Due Date)." />
                                                                            </td>
                                                                            <td align="right"></td>
                                                                            <td></td>
                                                                        </tr>
                                                                        <tr id="trRecurring2" runat="server" style="display: none;" class="tblOrderSettingstr">
                                                                            <td align="right">Frequency :</td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddlFrequency" runat="server" CssClass="form-control">
                                                                                    <asp:ListItem Text="Daily" Value="1"></asp:ListItem>
                                                                                    <asp:ListItem Text="Weekly" Value="2"></asp:ListItem>
                                                                                    <asp:ListItem Text="Monthly" Value="3"></asp:ListItem>
                                                                                    <asp:ListItem Text="Quarterly" Value="4"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td align="right"></td>
                                                                            <td></td>
                                                                        </tr>
                                                                        <tr id="trRecurring3" runat="server" style="display: none;" class="tblOrderSettingstr">
                                                                            <td align="right">Start Date :</td>
                                                                            <td>
                                                                                <BizCalendar:Calendar ID="calRecurStart" runat="server" ClientIDMode="AutoID" />
                                                                            </td>
                                                                            <td align="right">End Date :</td>
                                                                            <td>
                                                                                <BizCalendar:Calendar ID="calRecurEnd" runat="server" ClientIDMode="AutoID" />
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table id="tblRecurDetail" width="100%" runat="server" visible="false" class="tblRecurDetail">
                                                                        <tr>
                                                                            <th colspan="5">
                                                                                <a href="javascript:OpenRecurrenceDetail()">Recurrence Detail</a>
                                                                            </th>
                                                                        </tr>
                                                                        <tr>
                                                                            <th>Frequency</th>
                                                                            <th>Start Date</th>
                                                                            <th>End Date</th>
                                                                            <th>Status</th>
                                                                            <th></th>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <asp:Label ID="lblFrequency" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblStartDate" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:Label ID="lblEndDate" runat="server"></asp:Label>
                                                                            </td>
                                                                            <td>
                                                                                <asp:DropDownList ID="ddlRecurStatus" runat="server" CssClass="form-control">
                                                                                    <asp:ListItem Text="Enabled" Value="1" />
                                                                                    <asp:ListItem Text="Disabled" Value="2" />
                                                                                </asp:DropDownList>
                                                                                <asp:Label ID="lblRecurStatus" Text="[?]" CssClass="tip" runat="server" ToolTip="You have to delete this recurrence and create new one to enable recurrence again." Visible="false" />

                                                                            </td>
                                                                            <td>
                                                                                <asp:Button ID="btnDeleteRecurrence" runat="server" CssClass="button Delete" Text="X"></asp:Button>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr id="trParentOrder" runat="server" class="tblOrderSettingstr" visible="false">
                                                                <td style="text-align: right">Parent Order:
                                                                </td>
                                                                <td style="color: red" colspan="3">
                                                                    <asp:HyperLink ID="hplParentOrder" runat="server" ForeColor="#6600CC" Font-Bold="true"></asp:HyperLink>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                            <%--<div id="divOrderSettings">
                                                <strong>Order Settings</strong>&nbsp;<asp:Image ID="imgShowOrderSettings" runat="server"
                                                    ImageUrl="~/images/Down-arrow-inv1.png" ToolTip="Show Order Settings" />
                                            </div>--%>
                                        </fieldset>
                                    </td>
                                </tr>
                            </table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Milestone" runat="server">
            <div>
                <div>
                    &nbsp;<a href="#" onclick="return OpenHelpforMilestonesStages()"><label class="badge bg-yellow">?</label></a>
                </div>
                <uc2:MileStone ID="MileStone1" runat="server" />
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_AssociatedContacts" runat="server">
            <div class="table-responsive">

                <uc1:frmAssociatedContacts ID="frmAssociatedContacts1" runat="server" PageType="OppotunitiesIP" />
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_ProductsServices" runat="server">

            <div class="pull-right">
                &nbsp;<a href="#" onclick="return OpenHelpforProductsServices()"><label class="badge bg-yellow">?</label></a> &nbsp;
                <img alt="" id="imgGridConfiguration" runat="server" src="../images/settings30x30.png" style="vertical-align: middle; margin-bottom: 6px;" title="Settings" />
            </div>
            <div class="row" id="tblProducts" runat="server" visible="false">
                <div class="col-xs-12">
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <div class="pull-left">
                                Items Added To Order 
                            </div>
                            <div class="pull-right">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" style="display: inline">
                                    <ContentTemplate>
                                        <asp:Button ID="btnUnreceive" runat="server" CssClass="btn btn-primary" Text="Unreceive" ToolTip="Unreceive" Visible="false" Style="margin-right: 20px" OnClick="btnUnreceive_Click" OnClientClick="return ConfirmUnreceive();" />
                                        <asp:Button ID="btnRecalculatePrice" runat="server" CssClass="btn btn-primary" Text="Recalculate Price" ToolTip="Recalculate Price" Visible="false" Style="margin-right: 20px" />
                                        <asp:ImageButton ID="btnBarcode" runat="server" CssClass="imageAlign" Height="20" Style="vertical-align: middle;" ImageUrl="~/images/Barcode.png" AlternateText="Barcode" ToolTip="Barcode" Visible="false" OnClientClick="return BarcodeRecord()"></asp:ImageButton>
                                        <asp:Button ID="btnUpdateLabel" runat="server" CssClass="btn btn-primary" Text="Update Changes" />&nbsp;                                    
                                        <asp:Button ID="btnAddEditOrder" runat="server" CssClass="btn btn-primary" Text="Add/Edit" ToolTip="Add/Edit" Visible="false" />
                                        <asp:LinkButton ID="lkbDelete" runat="server" class="btn btn-danger" Visible="false" OnClientClick="return ConfirmDelete();"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="lkbDelete" />
                                        <asp:PostBackTrigger ControlID="btnUpdateLabel" />
                                        <asp:PostBackTrigger ControlID="btnRecalculatePrice" />
                                        <asp:PostBackTrigger ControlID="btnUnreceive" />
                                    </Triggers>
                                </asp:UpdatePanel>
                                <asp:Button ID="btnApproveUnitPrice" runat="server" CssClass="btn btn-primary" Text="Approve Unit Price(s)" Visible="false" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <asp:HiddenField ID="hftintshipped" runat="server" />
                            <div class="table-responsive">
                                <asp:GridView ID="gvProduct" runat="server" EnableViewState="true" AutoGenerateColumns="false"
                                    CssClass="table table-bordered table-striped" DataKeyNames="numoppitemtcode,bitDiscountType" Width="100%" ShowHeaderWhenEmpty="true" UseAccessibleHeader="true" Style="margin-bottom: 0px;">
                                    <Columns>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                            <asp:HiddenField ID="hdnEditUnitPriceRight" runat="server" />
                            <br />
                            <asp:Label ID="lblReturnLegend" runat="server" CssClass="normal1"
                                Text="<font color='red'>Units (Quantity To Return)</font></b> e.g <b>3(1)</b>"
                                Visible="false"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>

        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_BizDocs" runat="server" Height="100%">
            <div class="table-responsive">
                <div id="divBizDocsDtl" runat="server">
                    <div class="pull-right">
                        &nbsp;<a href="#" onclick="return OpenHelpforBizDocs()"><label class="badge bg-yellow">?</label></a>
                    </div>
                    <iframe id="IframeBiz" frameborder="0" width="100%" scrolling="auto"
                        clientidmode="Static" height="600" runat="server"></iframe>
                </div>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Collaboration" runat="server">
            <uc1:frmCollaborationUserControl ID="frmCollaborationUserControl" runat="server" />
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Correspondence" runat="server">
            <div class="table-responsive">
                <div class="pull-right">
                    &nbsp;<a href="#" onclick="return OpenHelpforCorrespondence()"><label class="badge bg-yellow">?</label></a>
                </div>
                <uc3:frmCorrespondence ID="Correspondence1" runat="server" />
            </div>
        </telerik:RadPageView>

        <telerik:RadPageView ID="radPageView_ShippingAndPackaging" runat="server">
            <div class="table-responsive">
                <div style="display: none">
                    <div class="pull-right">
                        &nbsp;<a href="#" onclick="return OpenHelpforShippingPackaging()"><label class="badge bg-yellow">?</label></a>
                    </div>
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <div class="form-inline">
                                <div class="form-group">
                                    <label>Selected Shipping Service:</label>
                                    <asp:DropDownList runat="server" ID="ddlShippingService" CssClass="form-control"></asp:DropDownList>
                                </div>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                        <div class="form-group">
                            <label>Total Order item weight:</label>
                            <asp:Label runat="server" ID="lblTotalWeight" Text="0"></asp:Label>&nbsp;lbs
                        </div>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                        <div class="form-group">
                            <label>
                                <asp:Image runat="server" ID="imgShipping" ImageUrl="~/images/TruckGreen.png" Width="25px" />&nbsp;Current Shipping Charge:</label>
                            <asp:Label runat="server" ID="lblCurrentShippingCharge" Text="0"></asp:Label>
                        </div>
                                <div class="form-group pull-right">
                                    <label>Manually enter shipping rate:</label>

                                    <asp:TextBox runat="server" ID="txtShippingRate" CssClass="form-control" Text="0.00">
                                    </asp:TextBox>
                                    <asp:Button runat="server" CssClass="btn btn-primary" ID="btnAddUpdate1" Text="Add/Update" OnClientClick="return AddUpdate();" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <div class="form-inline">
                                <div class="form-group">
                                    <label>Selected Rate Type:</label>
                                    <asp:DropDownList runat="server" ID="ddlRateType" CssClass="form-control">
                                        <asp:ListItem Text="Negotiated Rates" Value="1" Enabled="true"></asp:ListItem>
                                        <asp:ListItem Text="Retail/List Rates" Value="2"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                        <div class="form-group">
                            <label>
                                <asp:CheckBox runat="server" ID="chkMarkupShippingCharges" />&nbsp;Mark up shipping charges by:</label>
                            <asp:TextBox runat="server" ID="txtMarkupShippingCharge" CssClass="form-control" Text="0.00"></asp:TextBox>&nbsp;%
                        </div>
                                &nbsp;&nbsp;&nbsp;&nbsp;
                        <div class="form-group">
                            <label>Use Customer's shipping account:</label>
                            <asp:CheckBox runat="server" ID="chkUseCustomerShippingAccount" onchange="ValidateShipperAccount()" />
                            <asp:HiddenField runat="server" ID="hdnShipperAccountNo" />
                        </div>
                                <div class="form-group pull-right">
                                    <asp:Button runat="server" CssClass="btn btn-primary" ID="btnCalculateShippingRate" Text="Calculate shipping rate" />
                                    &nbsp;
                            <asp:Label runat="server" ID="lblCalculatedShippingRate" Text="0"></asp:Label>
                                    <asp:HiddenField ID="hdnCalculatedShippingRate" runat="server" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button runat="server" CssClass="btn btn-primary" ID="btnAddUpdate2" Text="Add/Update" OnClientClick="return AddUpdate();" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row padbottom10">
                    <div class="col-xs-12">
                        <div class="form-inline">
                            <div class="form-group">
                                <img src="../images/delivery.png" />
                            </div>
                            <div class="form-group">
                                <h1>Delivery</h1>
                                <asp:Label ID="lblDeliveryDate" runat="server" Text="8:30 A.M. Wednesday, April 19, 2017"></asp:Label>&nbsp;&nbsp;
                                <asp:Label ID="lblDayinTransit" runat="server" Text="Days in Transit : 1"></asp:Label>&nbsp;&nbsp;
                                <a href="javascript:void(0)" onclick="OpenShippingStatus()" class="btn-sm btn-warning">Track</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <asp:GridView ID="gvShipping" runat="server" EnableViewState="true" AutoGenerateColumns="false"
                                CssClass="table table-bordered table-striped" Width="100%" ShowHeaderWhenEmpty="true" UseAccessibleHeader="true">
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>BizDoc</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hplBizdcoc" runat="server" NavigateUrl="#" Text='<%# DataBinder.Eval(Container.DataItem, "BizDoc") %>'>
                                            </asp:HyperLink>
                                            <asp:Label ID="lblBizDocID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numOppBizDocsId") %>'>
                                            </asp:Label>
                                            (<%#Eval("BizDocType").ToString()%>) /
                                                    <asp:Label ID="lblBizDocName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vcBizDocID") %>'>
                                                    </asp:Label>
                                            <asp:HiddenField runat="server" ID="hdnShipCompany" Value='<%# DataBinder.Eval(Container.DataItem, "numShippingCompany")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>Boxes</HeaderTemplate>
                                        <ItemTemplate>
                                            <center>
                                                        <a href="javascript:void(0);" onclick="openShippingLabel('<%# Eval("numOppId") %>','<%# Eval("numoppbizdocsid")%>','<%# Eval("numShippingReportId") %>');">
                                                            <%# Eval("numBoxCount")%>
                                                        </a>
                                                    </center>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>Box Weight</HeaderTemplate>
                                        <ItemTemplate>
                                            <%#Eval("BoxWeight")%>&nbsp;<%--Lbs--%>
                                            <asp:Label ID="lblBoxTip" Text="[?]" CssClass="tip" runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>Package Shipping Label</HeaderTemplate>
                                        <ItemTemplate>
                                            <a href="javascript:void(0);" onclick="openLabelList('<%# Eval("numBoxID")%>');">Shipping Label
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>Shipping Report</HeaderTemplate>
                                        <ItemTemplate>
                                            <a href="javascript:void(0);" onclick="openReportList('<%# Eval("numOppId") %>','<%# Eval("numoppbizdocsid")%>','<%# Eval("numShippingLabelReportId") %>');">Open
                                            </a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>Package Tracking #s</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:HyperLink Target="_blank" ID="hplTrackingNums" runat="server" Text='<%#Eval("vcTrackingNumber")%>'></asp:HyperLink>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>Cloned BizDocs</HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="lblClonedBizdocs" runat="server" Text='<%#Eval("vcBizDocs")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    No records Found.
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>

    </telerik:RadMultiPage>
    <asp:HiddenField ID="OppType" runat="server" />
    <asp:HiddenField ID="OppStatus" runat="server" />
    <asp:HiddenField ID="DivID" runat="server" />
    <asp:HiddenField ID="OppID" runat="server" />
    <asp:TextBox ID="txtrecOwner" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtHidValue" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:TextBox ID="txtHidEditOppItem" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:TextBox ID="txtHidOptValue" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:TextBox ID="txtSerialize" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:TextBox ID="txtTemplateId" Text="0" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:TextBox ID="txtProcessId" Text="0" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:TextBox ID="txtGridHTML" Text="" runat="server" Style="display: none">
    </asp:TextBox>
    <input id="hdKit" runat="server" type="hidden" />
    <input id="hdOptKit" runat="server" type="hidden" />
    <asp:TextBox ID="txtModelID" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:DropDownList ID="ddlOpttype" runat="server" CssClass="signup" Width="200" Style="display: none"
        Enabled="False">
        <asp:ListItem Value="P">Product</asp:ListItem>
        <asp:ListItem Value="S">Service</asp:ListItem>
        <asp:ListItem Value="N">Non-Inventory Item</asp:ListItem>
    </asp:DropDownList>
    <%--<asp:Button ID="btnRequestReturn1" runat="server" Text="" Style="display: none" />--%>
    <asp:HiddenField ID="hdnCurrencyID" runat="server" />
    <asp:HiddenField ID="hdnCmbDivisionID" runat="server" />
    <asp:HiddenField ID="hdnCmbOppType" runat="server" />
    <asp:HiddenField ID="hdnCRMType" runat="server" />
    <asp:HiddenField ID="hdnRecordOwner" runat="server" />
    <asp:HiddenField ID="hdnTerrID" runat="server" />
    <asp:HiddenField ID="hdnStockTransfer" runat="server" />
    <asp:HiddenField ID="hdnContactID" runat="server" />
    <asp:HiddenField ID="hdnContactEmail" runat="server" />
    <asp:HiddenField ID="hdnShipToAddress" runat="server" />
    <asp:HiddenField ID="hdnCountry" runat="server" Value="0" />
    <asp:HiddenField ID="hdnState" runat="server" Value="0" />
    <asp:HiddenField ID="hdnPostal" runat="server" Value="0" />
    <asp:HiddenField ID="hdnPPVariance" runat="server" Value="0" />
    <asp:HiddenField ID="hfPoppName" runat="server" Value="0" />
    <asp:HiddenField ID="hdnRecConfigID" runat="server" />
    <asp:HiddenField ID="hdnRecurrenceType" runat="server" />
    <asp:HiddenField ID="hdnRecurred" runat="server" />
    <asp:HiddenField ID="hdnInventoryItems" runat="server" Value="0" />
    <asp:HiddenField ID="hdnOppname" runat="server" />
    <asp:Button ID="btnOpenInvoice" runat="server" Text="Button" Visible="false" />
    <asp:HiddenField ID="hdnFulfillentInterationValue" runat="server" />
    <asp:HiddenField ID="hdnInvoiceInterationValue" runat="server" />
    <asp:Button ID="btnItemReqDate" runat="server" OnClick="btnItemReqDate_Click" Style="display: none;" />
    <div id="dialMoreFO" class="modal fade" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Select Source</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-danger" id="divPickPackShipMoreException" runat="server">
                        <h4><i class="icon fa fa-ban"></i>Alert!</h4>
                        <<asp:Label ID="lblPickPackShipMoreException" runat="server" Text=""></asp:Label>
                    </div>
                    <p>
                        <asp:RadioButtonList ID="rdnFOSelection" runat="server"></asp:RadioButtonList>
                        <asp:HiddenField ID="hdnSelectedType" runat="server" />
                        <asp:HiddenField ID="hdnShippingBizDoc" Value="0" runat="server" />
                        <asp:HiddenField ID="hdnSelectedTempId" runat="server" />
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <asp:Button ID="btnMoreFoSelection" CssClass="btn btn-primary" runat="server" Text="Save & Close" OnClientClick="$('#dialMoreFO').modal('hide'); return true;" />
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div id="modalConfirmDealStatus" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">How do you want to conclude this opportunity ?</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12" id="openCloseDealDiv">
                        <asp:RadioButton runat="server" ID="rbDealWon" GroupName="DealWonLost" Checked="true" /><label>Close won</label>
                        (This option will convert the opportunity into an order)
                           <div style="margin-top: 10px">
                               <asp:RadioButton runat="server" ID="rbDealLost" GroupName="DealWonLost" /><label>Close lost</label>
                               (This option will remove the opportunity from the open list)
                           </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 10px; padding-left: 0px;">
                        <label class="col-md-4"><span class="text-danger">*</span>Conclusion Reason</label>
                        <div class="col-md-8">
                            <asp:DropDownList ID="ddlConclusionReason" CssClass="form-control" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <h4>Change to:</h4>
                    </div>
                    <div class="col-md-12">
                        <asp:UpdatePanel ID="uplChangeOrg" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <ul class="list-inline">
                                    <li>
                                        <asp:RadioButton runat="server" ID="rbCustomer" GroupName="OrgGroup" Checked="true" />
                                    </li>
                                    <li>
                                        <telerik:RadComboBox AccessKey="C" Width="290" ID="radCmbCompany" DropDownWidth="600px"
                                            Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" ShowMoreResultsBox="true" EnableLoadOnDemand="True"
                                            OnClientItemsRequested="OnClientItemsRequestedOrganization" EmptyMessage="Select Organization"
                                            ClientIDMode="AutoID" TabIndex="1" OnSelectedIndexChanged="radCmbCompany_SelectedIndexChanged">
                                            <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                        </telerik:RadComboBox>
                                    </li>
                                    <li>
                                        <telerik:RadComboBox ID="radcmbContact" EmptyMessage="Select Contact" Width="200" runat="server" AutoPostBack="true" TabIndex="2"></telerik:RadComboBox>
                                    </li>
                                </ul>
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="radCmbCompany" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <asp:Repeater runat="server" ID="rptAssociatedContacts">
                        <ItemTemplate>
                            <div class="col-md-12">
                                <ul class="list-inline">
                                    <li>
                                        <asp:RadioButton runat="server" ID="rbAssociatedContact" GroupName="OrgGroup" /></li>
                                    <li>
                                        <%# Eval("TargetCompany")%> (<%# Eval("ContactName")%>) <%# Eval("vcData")%>
                                        <asp:HiddenField ID="hdnDivisionID" runat="server" Value='<%# Eval("numDivisionID") %>' />
                                        <asp:HiddenField ID="hdnContactID" runat="server" Value='<%# Eval("numContactID") %>' />
                                    </li>
                                </ul>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <asp:Button runat="server" CssClass="btn btn-primary" ID="btnSaveConclusion" Text="Save & Close" OnClick="btnSaveConclusion_Click" />
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <%--Added by Neelam on 10/07/2017 - Added RadWindow to open Child Record window in modal pop up--%>
    <telerik:RadWindow RenderMode="Lightweight" runat="server" ID="RadWinChildRecords" Title="Child Records" RestrictionZoneID="ContentTemplateZone" Modal="true" Behaviors="Resize,Close,Move" Width="800" Height="400">
        <ContentTemplate>
            <asp:UpdatePanel ID="Updatepanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <asp:GridView ID="gvChildOpps" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-striped" Width="100%" ShowHeaderWhenEmpty="true" UseAccessibleHeader="true">
                            <Columns>
                                <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Image ID="imgType" runat="Server" Width="20px" Height="20px" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Record ID" SortExpression="vcPOppName" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <a href="javascript:OpenOpp('<%# Eval("numOppId") %>')">
                                            <%# Eval("vcPOppName") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%-- <asp:BoundField HeaderText="Record Status" DataField="vcStatus" ReadOnly="true" HeaderStyle-HorizontalAlign="Center" />--%>
                                <asp:BoundField DataField="RecordOrganization" HeaderText="Record Organization" HeaderStyle-HorizontalAlign="Center"></asp:BoundField>
                                <asp:BoundField DataField="RecordContact" HeaderText="Record Contact" HeaderStyle-HorizontalAlign="Center"></asp:BoundField>
                                <asp:BoundField DataField="Comments" HeaderText="Comments" HeaderStyle-HorizontalAlign="Center"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </telerik:RadWindow>
    <telerik:RadWindow RenderMode="Lightweight" runat="server" ID="RadWinDocumentFiles" Title="Documents / Files" RestrictionZoneID="ContentTemplateZone" Modal="true" Behaviors="Resize,Close,Move" Width="800" Height="530">
        <ContentTemplate>
            <asp:UpdatePanel ID="Updatepanel3" runat="server" UpdateMode="Conditional" class="row" style="margin-right: 0px; margin-left: 0px">
                <ContentTemplate>
                    <br />
                    <%--<div class="row">--%>
                    <div class="col-xs-12 padbottom10">
                        <div class="pull-right">
                            <input type="button" id="btnCloseDocumentFiles" onclick="return CloseDocumentFiles('C');" class="btn btn-primary" style="color: white" value="Close" />
                        </div>
                    </div>
                    <%-- </div>
                    <div class="row">--%>
                    <div class="col-sm-6">
                        <h4>
                            <img src="../images/icons/Attach.png" height="24">
                            Upload a Document from your Desktop
                        </h4>
                        <hr style="color: #dcdcdc" />
                        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel1" ClientEvents-OnRequestStart="requestStart">
                            <div class="form-group">
                                <div class="custom-file">
                                    <input class="custom-file-input" id="fileupload" type="file" name="fileupload" runat="server">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Section</label>
                                <asp:DropDownList ID="ddlSectionFile" runat="server" CssClass="form-control"></asp:DropDownList>
                            </div>
                            <asp:Button ID="btnAttach" OnClick="btnAttach_Click" runat="server" Text="Attach & Close" CssClass="btn btn-primary"></asp:Button>
                            <asp:Button ID="btnLinkClose" OnClick="btnLinkClose_Click" runat="server" CssClass="hidden"></asp:Button>
                        </telerik:RadAjaxPanel>
                        <telerik:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server" InitialDelayTime="0" Skin="Default">
                        </telerik:RadAjaxLoadingPanel>
                    </div>

                    <div class="col-sm-6" id="divLinkUpload">
                        <h4>
                            <img src="../images/icons/Link.png">
                            Link to a Document</h4>
                        <hr style="color: #dcdcdc" />
                        <div class="form-group">
                            <label>Link Name</label>
                            <asp:TextBox runat="server" ID="txtLinkName" EmptyMessage="Link Name" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Link URL</label>
                            <asp:TextBox runat="server" ID="txtLinkURL" EmptyMessage="Paste link here" TextMode="MultiLine" CssClass="form-control" Rows="3"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Section</label>
                            <asp:DropDownList ID="ddlSectionLink" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                        <asp:Button ID="btnLink" runat="server" OnClientClick="return CloseDocumentFiles('L');" Text="Link & Close" CssClass="btn btn-primary"></asp:Button>
                        <asp:Label ID="Label2" Text="[?]" CssClass="tip" runat="server" ToolTip="Linking to a file on Google Drive or any other file repository is a good way to preserve your BizAutomation storage space. To avoid login credentials when clicking the  link, just make it public (e.g. On Google Drive you have to make sure that �Link Sharing� is set to �Public on the web�)." />
                    </div>
                    <%--</div>--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </telerik:RadWindow>

    <div class="overlay" id="divEDIOrder" runat="server" style="display: none">
        <div class="overlayContent" style="color: #000; background-color: #FFF; width: 350px; border: 1px solid #ddd;">
            <table border="0" class="dialog-body">
                <tr>
                    <td style="text-align: center;">
                        <asp:Button ID="btnCancelEdiFields" OnClientClick="javascript: HideEDIPopup('divEDIOrder'); return false;" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="Cancel" />
                        <asp:Button ID="btnSaveEdiFields" runat="server" OnClick="btnSaveEdiFields_Click" CssClass="btn btn-primary" ForeColor="White" Text="Save & Close" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                           <asp:Table ID="tblEDIOrder" CellPadding="3" CellSpacing="3" runat="server" CssClass="table table-responsive tblNoBorder" Style="width: 100%">
                           </asp:Table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
    </div>
    <div class="overlay" id="divEDIOppItem" runat="server" style="display: none">
        <div class="overlayContent" style="color: #000; background-color: #FFF; width: 350px; border: 1px solid #ddd;">
            <table border="0" class="dialog-body">
                <tr>
                    <td style="text-align: center;">
                        <asp:Button ID="btnCancelEdiOppItem" OnClientClick="javascript: HideEDIPopup('divEDIOppItem'); return false;" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="Cancel" />
                        <asp:Button ID="btnSaveEdiOppItem" OnClick="btnSaveEdiOppItem_Click" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="Save & Close" />
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                           <asp:Table ID="tblEDIOppItem" CellPadding="3" CellSpacing="3" runat="server" CssClass="table table-responsive tblNoBorder" Style="width: 100%">
                           </asp:Table>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
