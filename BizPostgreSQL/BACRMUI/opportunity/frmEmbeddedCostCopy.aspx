﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmEmbeddedCostCopy.aspx.vb"
    Inherits=".frmEmbeddedCostCopy" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Copy values from seleted BizDoc</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save & Close" />&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Copy values from seleted BizDoc
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="700px">
        <tr>
            <td class="normal4" align="center" colspan="2">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Select BizDoc&nbsp;
            </td>
            <td align="left" class="normal1">
                <asp:DropDownList runat="server" ID="ddlBizDoc" CssClass="signup">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="pnlInfo" CssClass="info">
        above wizard will delete all embedded cost for current bizdoc and copy all values
        from selected bizdoc
    </asp:Panel>
</asp:Content>
