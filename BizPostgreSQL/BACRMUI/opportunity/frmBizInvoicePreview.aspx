﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmBizInvoicePreview.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmBizInvoicePreview" ValidateRequest="false" %>

<%@ Register Src="../Accounting/PaymentHistory.ascx" TagName="PaymentHistory" TagPrefix="uc1" %>
<%--disabled validate request to submit html form client side when clicked on send email--%>
<%--<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="rad" %>--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <script src="../JavaScript/jquery.min.js" type="text/javascript"></script>
    <title>BizDocs</title>
    <script src="../JavaScript/jquery.min.js" type="text/javascript"></script>
    <script src="../JavaScript/jquery.Tooltip.js" type="text/javascript"></script>

    <script language="javascript" type="text/javascript">
        function PrintIt() {
            //if (confirm("To print bizdoc with background color and border you need to check 'Print Background(colors & images)' from page setup options of your browser") == true) {
            document.getElementById('tblButtons').style.display = 'none';
            document.getElementById('tblApproval').style.display = 'none';
            window.print();
            // }
            return false;
        }

        function Close1(lngOppID) {
            //opener.location='dreamweaver.htm';self.close()">
            // window.open('../opportunity/frmQBStatusReport.aspx?BizDocID='+lngOppID,'','toolbar=no,titlebar=no,top=300,width=800,height=500,scrollbars=yes,resizable=yes')
            window.opener.reDirect('../opportunity/frmOpportunities.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylist&OpID=' + lngOppID);
            self.close();
            return false;
        }

        function RefereshParentPage() {
            document.Form1.btnTemp_DontDelete.click()
            return false;
        }
        function OpenAmtPaid(a, b, c) {
            //var BalanceAmt;
            //BalanceAmt = 0;
            //if (document.getElementById('lblBalance') != null) {
            //    BalanceAmt = document.getElementById('lblBalance').innerHTML;
            //    BalanceAmt = BalanceAmt.replace(/,/g, "");
            //}
            //if (BalanceAmt == "")
            //    BalanceAmt = 0;

            if (document.getElementById("hdnOppType").value == "1") {
                window.open('../opportunity/frmAmtPaid.aspx?Popup=1&OppBizId=' + a + '&OppId=' + b + '&DivId=' + c, '', 'toolbar=no,titlebar=no,top=300,width=700,height=400,scrollbars=no,resizable=no');
            }
            return false;
        }

        function OpenRecievedDate(OppID) {
            window.open('../opportunity/frmRecievedDate.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID=' + OppID, '', 'toolbar=no,titlebar=no,top=300,width=400,height=100,scrollbars=yes,resizable=yes');
            return false;
        }
        function ShowWindow(Page, q, att) {

            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                return false;

            }
        }


        function OpenLogoPage() {
            window.open('frmLogoUpload.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=BizDocs', '', 'toolbar=no,titlebar=no,top=300,width=400,height=200,scrollbars=yes,resizable=yes');
            return false;
        }

        function Close() {
            window.close();
        }

        function CheckNumber(cint) {
            if (cint == 1) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode == 44 || window.event.keyCode == 46)) {
                    window.event.keyCode = 0;
                }
            }
            if (cint == 2) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                    window.event.keyCode = 0;
                }
            }

        }
        function openApp(a, b, c) {

            if (document.all) {
                window.open('../Documents/frmDocApprovers.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DocID=' + a + '&DocType=' + b + '&OpID=' + c + "&DocName=" + document.getElementById('lblBizDocIDValue').innerText, '', 'toolbar=no,titlebar=no,left=300,top=450,width=900,height=600,scrollbars=yes,resizable=yes')
            } else {
                window.open('../Documents/frmDocApprovers.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DocID=' + a + '&DocType=' + b + '&OpID=' + c + "&DocName=" + document.getElementById('lblBizDocIDValue').textContent, '', 'toolbar=no,titlebar=no,left=300,top=450,width=900,height=600,scrollbars=yes,resizable=yes')
            }

            return false;
        }
        function SendEmail(a, b) {
            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenAtch(a, b, c, d) {
            window.open("../opportunity/frmBizDocAttachments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&BizDocID=" + document.Form1.txtBizDoc.value + "&TempID=" + document.Form1.ddlBizDocTemplate.value + "&E=2&OpID=" + a + "&OppBizId=" + b + "&DomainID=" + c + "&ConID=" + c, "", "width=800,height=400,status=no,scrollbars=yes,left=155,top=160");
            return false;
        }
        function openeditAddress(a, b, ReturnID) {
            window.open('../opportunity/frmEditOppAddress.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ReturnID=' + ReturnID + '&AddType=' + a + '&OppID=' + b, '', 'toolbar=no,titlebar=no,left=100,top=350,width=500,height=350,scrollbars=yes,resizable=yes')
            return false;
        }
        function Openfooter(a) {
            window.open('../opportunity/frmFooterUpload.aspx?OppType=' + a, '', 'toolbar=no,titlebar=no,top=300,width=400,height=100,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenExport(a, b) {
            window.open('../opportunity/frmBizDocExport.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppBizId=' + a + '&OpID=' + b, '', 'toolbar=no,titlebar=no,top=300,width=400,height=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenEdit(a, b, c) {
            var strURL = '../opportunity/frmAddBizDocs.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b + '&OppType=' + c;
            if (opener.location.href.toLowerCase().indexOf('frmmenu') > 0) {
                opener.top.frames['mainframe'].location.href = strURL
            } else {
                opener.location.href = strURL
            }
            window.close();
            return false;
        }
        function ChangeDate(a, b, c) {
            window.open('../opportunity/frmChangeDueDate.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppBizId=' + a + '&date=' + b + '&OppID=' + c, '', 'toolbar=no,titlebar=no,top=300,width=240,height=250,scrollbars=yes,resizable=yes');
            return false;
        }
        function GrabHTML() {
            document.getElementById('hdnBizDocHTML').value = document.getElementById("pnlBizDoc").innerHTML.replace(/https/gi, "http"); //case in-sensetive replace
            return true;
        }

    </script>
    <style type="text/css">
     
                               #tdr
{
border:1px solid black;
}
    </style>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <br />
       
        <table width="100%" id="#tdr">
            <tr>

                <td>
                    <asp:Literal ID="ltrDynamicHtml" runat="server"></asp:Literal>
                </td>
            </tr>
        
        </table>
    </form>
    <script type="text/javascript">
        windowResize();

        function windowResize() {
            var width = document.getElementById("resizeDiv").offsetWidth;
            var height = document.getElementById("resizeDiv").offsetHeight;
            window.resizeTo(width + 45, height + 150);
        }

        $("#Form1").find("span.tip").tooltip({

            // place tooltip on the right edge
            position: "center right",

            // a little tweaking of the position
            offset: [-2, 10],

            // use the built-in fadeIn/fadeOut effect
            effect: "fade",

            // custom opacity setting
            opacity: 0.7

        });
    </script>
</body>
</html>