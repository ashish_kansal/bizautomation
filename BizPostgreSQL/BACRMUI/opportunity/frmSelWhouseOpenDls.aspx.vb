Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common

Partial Public Class frmSelWhouseOpenDls
    Inherits System.Web.UI.Page

    Dim dsTemp As DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then LoadItems()
            Session("Help") = Request.Url.Segments(Request.Url.Segments.Length - 1)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadItems()
        Try
            Dim objOpportunity As New MOpportunity
            objOpportunity.OpportunityId = GetQueryStringVal(Request.QueryString("enc"), "Opid")
            dsTemp = objOpportunity.ItemsByOppId
            dgItem.DataSource = dsTemp.Tables(0)
            dgItem.DataBind()
            Session("Data") = dsTemp
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgItem_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgItem.ItemDataBound
        Try
            Dim objCommon As New CCommon
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btn As Button
                If e.Item.Cells(3).Text = "False" Then
                    btn = e.Item.FindControl("btnSelSer")
                    btn.Visible = False
                End If
                If Not e.Item.Cells(4).Text = "Service" Then
                    Dim ddl As DropDownList
                    ddl = e.Item.FindControl("ddlWarehouse")
                    ddl.ID = "ddl" & e.Item.Cells(0).Text
                    ddl.DataSource = objCommon.GetWarehouseOnAttrSel(e.Item.Cells(1).Text, IIf(e.Item.Cells(5).Text = "&nbsp;", "", e.Item.Cells(5).Text))
                    ddl.DataTextField = "vcWareHouse"
                    ddl.DataValueField = "numWareHouseItemId"
                    ddl.DataBind()
                    ddl.Items.Insert(0, "--Select One--")
                    ddl.Items.FindByText("--Select One--").Value = "0"
                    If e.Item.Cells(3).Text = "True" Then
                        btn = e.Item.FindControl("btnSelSer")
                        btn.Attributes.Add("onclick", "return SelectItems('" & ddl.ClientID & "','" & e.Item.Cells(5).Text & "','" & e.Item.Cells(1).Text & "')")
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            dsTemp = Session("Data")
            Dim dtTable As DataTable
            dtTable = dsTemp.Tables(0)
            dtTable.PrimaryKey = New DataColumn() {dsTemp.Tables(0).Columns("numoppitemtCode")}
            Dim dr As DataRow
            Dim DemoGridItem As DataGridItem
            For Each DemoGridItem In dgItem.Items
                If Not DemoGridItem.Cells(4).Text = "Service" Then
                    dr = dtTable.Rows.Find(DemoGridItem.Cells(0).Text)
                    Dim ddl As DropDownList = DemoGridItem.Cells(7).Controls(1)
                    dr("numWarehouseItmsID") = ddl.SelectedValue
                End If
            Next
            Dim objOpportunity As New MOpportunity
            objOpportunity.OpportunityId = GetQueryStringVal(Request.QueryString("enc"), "Opid")
            objOpportunity.strItems = dsTemp.GetXml
            objOpportunity.UpdateOppAfterWarehouseSel()
            Response.Write("<script>opener.location.href='../opportunity/frmOpportunities.aspx?Opid=" & GetQueryStringVal(Request.QueryString("enc"), "Opid") & "'; self.close();</script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Write("<script>self.close();</script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class