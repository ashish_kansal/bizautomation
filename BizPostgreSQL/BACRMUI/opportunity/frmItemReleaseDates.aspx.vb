﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities

Public Class frmItemReleaseDates
    Inherits BACRMPage
#Region "Member Variables"
    Private objOpportunityItemsReleaseDates As OpportunityItemsReleaseDates
#End Region

#Region "Constructor"
    Sub New()
        objOpportunityItemsReleaseDates = New OpportunityItemsReleaseDates
    End Sub
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblException.Text = ""

            If Not Page.IsPostBack Then
                hdnOppID.Value = GetQueryStringVal("numOppID")
                hdnOppItemID.Value = GetQueryStringVal("numOppItemID")

                If CCommon.ToLong(hdnOppID.Value) > 0 AndAlso CCommon.ToLong(hdnOppItemID.Value) > 0 Then
                    BindData()
                Else
                    pnlData.Visible = False
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

#Region "Private Methods"
    Private Sub Clear()
        Try
            btnAdd.Text = "Add"
            hdnID.Value = ""
            txtQuantity.Text = ""
            ddlReleaseStatus.SelectedValue = "1"
            radReleaseDate.Clear()
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub BindData()
        Try
            objOpportunityItemsReleaseDates.DomainID = Session("DomainID")
            objOpportunityItemsReleaseDates.OppID = hdnOppID.Value
            objOpportunityItemsReleaseDates.OppItemID = hdnOppItemID.Value
            gvReleaseDates.DataSource = objOpportunityItemsReleaseDates.GetByOppItemID()
            gvReleaseDates.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region

#Region "Event Handlers"
    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Try
            Dim qty As Long
            Dim releaseDate As Date

            If String.IsNullOrEmpty(radReleaseDate.SelectedDate) Then
                lblException.Text = "Select release date."
                Exit Sub
            ElseIf Not Date.TryParse(radReleaseDate.SelectedDate, releaseDate) Then
                lblException.Text = "Select valid release date date."
                Exit Sub
            ElseIf String.IsNullOrEmpty(txtQuantity.Text) Then
                lblException.Text = "Select quantity."
                Exit Sub
            ElseIf Not Int32.TryParse(txtQuantity.Text, qty) Then
                lblException.Text = "Quantity must be numeric."
                Exit Sub
            ElseIf qty <= 0 Then
                lblException.Text = "Quantity must be greater than 0."
                Exit Sub
            End If

            objOpportunityItemsReleaseDates.DomainID = Session("DomainID")
            objOpportunityItemsReleaseDates.OppID = hdnOppID.Value
            objOpportunityItemsReleaseDates.OppItemID = hdnOppItemID.Value
            objOpportunityItemsReleaseDates.ID = IIf(CCommon.ToLong(hdnID.Value) > 0, CCommon.ToLong(hdnID.Value), -1)
            objOpportunityItemsReleaseDates.ReleaseDate = radReleaseDate.SelectedDate
            objOpportunityItemsReleaseDates.Quantity = qty
            objOpportunityItemsReleaseDates.ReleaseStatus = ddlReleaseStatus.SelectedValue
            objOpportunityItemsReleaseDates.Save()

            Clear()
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Try
            Clear()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub gvReleaseDates_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvReleaseDates.RowCommand
        Try
            If e.CommandName = "EditItem" Then
                hdnID.Value = DirectCast(DirectCast(e.CommandSource, ImageButton).Parent.FindControl("hdnID"), HiddenField).Value
                Dim hdnQuantity As HiddenField = DirectCast(DirectCast(e.CommandSource, ImageButton).Parent.FindControl("hdnQuantity"), HiddenField)
                Dim hdnReleaseDate As HiddenField = DirectCast(DirectCast(e.CommandSource, ImageButton).Parent.FindControl("hdnReleaseDate"), HiddenField)
                Dim hdnReleaseStatus As HiddenField = DirectCast(DirectCast(e.CommandSource, ImageButton).Parent.FindControl("hdnReleaseStatus"), HiddenField)

                radReleaseDate.SelectedDate = hdnReleaseDate.Value
                txtQuantity.Text = hdnQuantity.Value
                ddlReleaseStatus.SelectedValue = hdnReleaseStatus.Value
                btnAdd.Text = "Update"
            ElseIf e.CommandName = "DeleteItem" Then
                Dim hdnID As HiddenField = DirectCast(DirectCast(e.CommandSource, ImageButton).Parent.FindControl("hdnID"), HiddenField)
                objOpportunityItemsReleaseDates.ID = hdnID.Value
                objOpportunityItemsReleaseDates.Delete()

                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region
End Class