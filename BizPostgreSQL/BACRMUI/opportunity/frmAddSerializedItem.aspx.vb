Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports Infragistics.Web.UI.GridControls
Imports Telerik.Web.UI

Partial Public Class frmAddSerializedItem
    Inherits BACRMPage

    Dim ds As DataSet
    Dim dtCusTable As New DataTable
    Dim lngOppID, lngItemCode, lngOppBizDocId As Long
    Dim iOppType As Short
    Private Sub frmAddSerializedItem_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Try

            lngOppID = CCommon.ToLong(GetQueryStringVal("OppID"))
            lngItemCode = CCommon.ToLong(GetQueryStringVal("ItemCode"))
            lngOppBizDocId = CCommon.ToLong(GetQueryStringVal("OppBizDocId"))
            iOppType = CCommon.ToLong(GetQueryStringVal("OppType"))

            populateDataSet()

            Dim i As Integer
            Dim bField As GridBoundColumn
            Dim radColumn As Telerik.Web.UI.GridColumn

            For Each dr As DataRow In ds.Tables(2).Rows
                radColumn = gvSerialLot.Columns.FindByUniqueNameSafe(dr("Fld_label"))

                If radColumn IsNot Nothing Then
                    gvSerialLot.Columns.Remove(radColumn)
                End If

                bField = New GridBoundColumn

                bField.HeaderText = dr("Fld_label")
                bField.DataField = dr("Fld_label") & "Value"
                bField.UniqueName = dr("Fld_label")

                gvSerialLot.Columns.Add(bField)
            Next

            If hfbitLotNo.Value = True Then
                gvSerialLot.Columns.FindByUniqueNameSafe("vcSerialNo").HeaderText = "LOT #"
            Else
                gvSerialLot.Columns.FindByUniqueNameSafe("vcSerialNo").HeaderText = "Serial #"
                gvSerialLot.Columns.FindByUniqueNameSafe("TotalQty").Visible = False
                gvSerialLot.Columns.FindByUniqueNameSafe("UsedQty").Visible = False
            End If

            gvSerialLot.MasterTableView.EnableColumnsViewState = False
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                LoadDropdown()
                BindInventory()
            End If
            'btnClose.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub populateDataSet()
        Try
            Dim objOpp As New COpportunities
            objOpp.OppItemCode = lngItemCode

            objOpp.OpportID = lngOppID
            objOpp.OppType = iOppType
            objOpp.BizDocId = lngOppBizDocId

            ds = objOpp.GetSerializedIndOppItems_BizDoc

            Dim dtTable, dtTable1 As DataTable
            dtTable = ds.Tables(2)
            dtTable1 = ds.Tables(1)
            Dim dr, dr1 As DataRow
            For Each dr In dtTable.Rows
                If dr("fld_type") = "TextBox" Or dr("fld_type") = "TextArea" Then
                    For Each dr1 In dtTable1.Rows
                        If dr1(dr("Fld_label")) = "0" Then dr1(dr("Fld_label")) = ""
                    Next
                End If
            Next
            ds.AcceptChanges()
            If ds.Relations.Count < 1 Then
                ds.Tables(0).TableName = "WareHouse"
                ds.Tables(1).TableName = "SerializedItems"
                ds.Tables(0).PrimaryKey = New DataColumn() {ds.Tables(0).Columns("numWareHouseItemID")}
                ds.Tables(1).PrimaryKey = New DataColumn() {ds.Tables(1).Columns("numWareHouseItmsDTLID")}
                ds.Relations.Add("Customers", ds.Tables(0).Columns("numWareHouseItemID"), ds.Tables(1).Columns("numWareHouseItemID"))
            End If
            'lblUnits.Text = ds.Tables(0).Rows(0).Item("numUnitHour")
            hfbitSerialized.Value = ds.Tables(0).Rows(0).Item("bitSerialize")
            hfbitLotNo.Value = ds.Tables(0).Rows(0).Item("bitLotNo")

            lblUnits.Text = String.Format("{0:N0}", (GetQueryStringVal("Qty")))
            lblUnitsName.Text = ds.Tables(0).Rows(0).Item("vcBaseUOMName")

            hdWareHouseItemID.Value = ds.Tables(0).Rows(0).Item("numWareHouseItemID")

            If iOppType = 1 Then dtTable1.Merge(ds.Tables(3))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindInventory()
        Try
            gvSerialLot.DataSource = ds.Tables(1)
            gvSerialLot.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadDropdown()
        Try
            Dim objOpp As New COpportunities
            objOpp.OpportID = GetQueryStringVal("OppID")
            ddlItems.DataSource = objOpp.GetSerializedOppItems()
            ddlItems.DataTextField = "vcItemName"
            ddlItems.DataValueField = "numOppItemTCode"
            ddlItems.DataBind()
            ddlItems.Items.Insert(0, "--Select One--")
            ddlItems.Items.FindByText("--Select One--").Value = "0"

            If (GetQueryStringVal("ItemCode")) Then
                If (ddlItems.Items.FindByValue(GetQueryStringVal("ItemCode")) IsNot Nothing) Then
                    ddlItems.ClearSelection()
                    ddlItems.Items.FindByValue(GetQueryStringVal("ItemCode")).Selected = True

                    ddlItems.Enabled = False
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlItems_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlItems.SelectedIndexChanged
        Try
            If ddlItems.SelectedIndex > 0 Then populateDataSet()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim intUnits As Decimal = 0
            Dim txtReturn As String = ""

            If iOppType = 1 Then
                For Each dataGridItems As GridDataItem In gvSerialLot.Items
                    If CType(dataGridItems.FindControl("chkInclude"), CheckBox).Checked = True Then
                        Dim TotalQty As Integer = dataGridItems("TotalQty").Text
                        Dim UsedQty As Integer = CType(dataGridItems.FindControl("txtUsedQty"), TextBox).Text

                        If hfbitLotNo.Value = True Then
                            If UsedQty > TotalQty Then
                                litMessage.Text = "Used qty " & UsedQty & " can't be more than Total qty " & TotalQty & "!"
                                Exit Sub
                            Else
                                intUnits = intUnits + UsedQty
                            End If
                            txtReturn += dataGridItems("vcSerialNo").Text & " (" & UsedQty & "),"
                        Else
                            UsedQty = 1
                            intUnits = intUnits + 1

                            txtReturn += dataGridItems("vcSerialNo").Text & ","
                        End If
                    End If
                Next

                If intUnits > CCommon.ToDecimal(lblUnits.Text) Then
                    litMessage.Text = "No of Serialized Items Selected " & intUnits & " is not equal to No of Serailized Items " & lblUnits.Text & " Required!"
                    Exit Sub
                End If
            End If

            Response.Write("<script>if(window.opener != null){window.opener.document.getElementById('" & GetQueryStringVal("LotId") & "').value='" & txtReturn.Trim(",") & "' ;window.opener.document.getElementById('" & GetQueryStringVal("LotId") & "').focus();}window.close();</script>")
            Exit Sub

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class