<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmLinkedProjects.aspx.vb"
    Inherits="BACRM.UserInterface.Opportunities.frmLinkedProjects" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Linked Items</title>
    <script language="javascript" type="text/javascript">
        function OpenActionItem(a) {
            window.opener.reDirect("../admin/ActionItemDetailsOld.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&CommId=" + a);
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnProOK" runat="server" CssClass="button" Text="Close" OnClientClick="javascript:window.close()"></asp:Button>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Linked Items
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="100%" class="aspTable">
        <tr>
            <td class="rowHeader" align="left">Projects
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gvLinkProjects" runat="server" EnableViewState="true" AutoGenerateColumns="false"
                    CssClass="tbl" Width="100%" ShowHeaderWhenEmpty="true">
                    <Columns>
                        <asp:BoundField DataField="vcProjectName" HeaderText="Item Name" />
                        <asp:BoundField DataField="Type" HeaderText="Type" />
                    </Columns>
                    <EmptyDataTemplate>
                        No records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="rowHeader" align="left">
                <br />
                Action Items
            </td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gvCommunication" runat="server" EnableViewState="true" AutoGenerateColumns="false"
                    CssClass="tbl" Width="100%" ShowHeaderWhenEmpty="true" DataKeyNames="numCommId">
                    <Columns>
                        <asp:TemplateField HeaderText="Company Name - Contact">
                            <ItemTemplate>
                                <%#Eval("vcDivisionName")%> - <%#Eval("vcContactName")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="vcAssignTo" HeaderText="Assigned To" />
                        <asp:TemplateField HeaderText="Type">
                            <ItemTemplate>
                                <a onclick="return OpenActionItem('<%# Eval("numCommId")%>');" href="#">
                                    <%# Eval("vcTask")%></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="vcActivity" HeaderText="Activity" />
                        <asp:BoundField DataField="vcStatus" HeaderText="Priority" />
                        <asp:TemplateField HeaderText="Start Date - End Date">
                            <ItemTemplate>
                                <%#Eval("dtStartTime")%> - <%#Eval("dtEndTime")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
