﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmItemReleaseDates.aspx.vb" Inherits=".frmItemReleaseDates" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("#btnAdd").click(function () {
                var datePicker = $find("<%=radReleaseDate.ClientID%>");

                if (datePicker.get_dateInput().get_selectedDate() == null) {
                    alert("Select release date.");
                    return false;
                } else if (isNaN(Date.parse(datePicker.get_dateInput().get_selectedDate()))) {
                    alert("Select valid release date.");
                    return false;
                } else if ($("#txtQuantity").val() == "") {
                    alert("Select quantity.");
                    return false;
                } else if (!parseInt($("#txtQuantity").val()) || parseInt($("#txtQuantity").val()) <= 0) {
                    alert("Quantity must be greater than 0.");
                    return false;
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <asp:Label ID="lblException" runat="server" ForeColor="Red"></asp:Label>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Item Release Dates
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:Panel ID="pnlData" runat="server">
        <table width="100%">
            <tr>
                <td style="text-align:right; font-weight:bold">Release Date:</td>
                <td>
                    <telerik:RadDatePicker ID="radReleaseDate" runat="server"></telerik:RadDatePicker>
                </td>
                <td style="text-align:right; font-weight:bold">Quantity:</td>
                <td>
                    <asp:TextBox ID="txtQuantity" runat="server" Width="80"></asp:TextBox></td>
                <td style="text-align:right; font-weight:bold">Release Status:</td>
                <td><asp:DropDownList ID="ddlReleaseStatus" runat="server">
                    <asp:ListItem Text="Open" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Purchased" Value="2"></asp:ListItem>
                </asp:DropDownList></td>
                <td>
                    <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-primary" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn" />
                </td>
            </tr>
            <tr>
                <td colspan="7">
                    <asp:GridView ID="gvReleaseDates" runat="server" Width="100%" UseAccessibleHeader="true" ShowHeaderWhenEmpty="true" CssClass="dg" AutoGenerateColumns="false">
                        <AlternatingRowStyle CssClass="ais"></AlternatingRowStyle>
                        <RowStyle CssClass="is"></RowStyle>
                        <HeaderStyle CssClass="hs" Height="30" Font-Size="11"></HeaderStyle>
                        <Columns>
                            <asp:BoundField HeaderText="Release Dates" DataField="vcDesctiption" />
                            <asp:TemplateField ItemStyle-Width="50" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnID" runat="server" Value='<%# Eval("numID")%>' />
                                    <asp:HiddenField ID="hdnQuantity" runat="server" Value='<%# Eval("numQty")%>' />
                                    <asp:HiddenField ID="hdnReleaseDate" runat="server" Value='<%# Eval("dtReleaseDate")%>' />
                                    <asp:HiddenField ID="hdnReleaseStatus" runat="server" Value='<%# Eval("tintStatus")%>' />
                                    <asp:ImageButton ID="ibtnEdit" runat="server" ImageUrl="~/images/pencil-24.gif" ToolTip="Edit" Height="16" CommandName="EditItem" />
                                    <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/images/delete2.gif" ToolTip="DeleteItem" OnClientClick="return Confirm('Record will be deleted. Do you want to proceed?')" CommandName="Delete" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:HiddenField ID="hdnID" runat="server" />
    <asp:HiddenField ID="hdnOppID" runat="server" />
    <asp:HiddenField ID="hdnOppItemID" runat="server" />
</asp:Content>
