<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPurchaseFulfillment.aspx.vb"
    Inherits="BACRM.UserInterface.Opportunities.frmPurchaseFulfillment" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="menu1" TagName="Menu" Src="../include/webmenu.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="../common/frmBizSorting.ascx" TagName="frmBizSorting" TagPrefix="uc1" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Purchase Fulfillment</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <style type="text/css">
        .listObjectShow {
            color: green;
            vertical-align: middle;
        }

        .listObjectShowCheck {
            margin: 0px !important;
        }

        .table {
            border: 1px solid #e1e1e1;
        }

        .RadComboBoxDropDown_Default .rcbHeader {
            background-image: none !important;
            font-weight: bold;
        }

        /** Columns */
        .rcbHeader ul,
        .rcbFooter ul,
        .rcbItem ul,
        .rcbHovered ul,
        .rcbDisabled ul {
            margin: 0;
            padding: 0;
            width: 100%;
            display: inline-block;
            list-style-type: none;
        }

        .rcbHeader {
            padding: 5px 27px 4px 7px;
        }

        .rcbSlide {
            left: 18%;
        }

        /** Multiple rows and columns */
        .multipleRowsColumns .rcbItem,
        .multipleRowsColumns .rcbHovered {
            margin: 0 1px;
            min-height: 13px;
            overflow: hidden;
            padding: 2px 19px 2px 6px;
        }

        .col0 {
            width: 0px;
        }

        .col1 {
            width: 200px;
        }

        .col2 {
            width: 200px;
        }

        .col3, .col4, .col5, .col6 {
            width: 90px;
        }

        .col1,
        .col2,
        .col3,
        .col4, 
        .col5,
        .col6 {
            margin: 0;
            padding: 0 5px 0 0;
            line-height: 14px;
            float: left;
        }
    </style>
    <script type="text/javascript">
        function OpenOpp(a) {
            var str;
            str = "../opportunity/frmOpportunities.aspx?frm=PurchaseFulfillment&OpID=" + a;
            document.location.href = str;
        }
        function CheckAllSameOpp(OppId, chk) {
            if ($("#" + chk).is(":checked")) {
                OppId = document.getElementById($("#" + chk).attr("id").replace("_chkSelect", "_lblOppID")).innerHTML
                $('#ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch tr').each(function () {
                    if ($(this).find("input[id*='chkSelect']").length > 0 && document.getElementById($(this).find("input[id*='chkSelect']").attr("id").replace("_chkSelect", "_lblOppID")).innerHTML != OppId) {
                        $(this).find("input[id*='chkSelect']").prop("checked", false);
                    }
                });

                //for (var i = 2; i <= document.getElementById('ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch').rows.length; i++) {
                //    var str;
                //    if (i < 10) {
                //        str = '0' + i;
                //    }
                //    else {
                //        str = i;
                //    }
                //    if (document.getElementById(chk).checked == true) {
                //        if (document.getElementById('ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch_ctl' + str + '_lblOppID').innerHTML == OppId) {
                //            document.getElementById('ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch_ctl' + str + '_chkSelect').checked = validate(i);
                //        }
                //    }
                //}
            }

            SwapQty();
        }
        function SwapQty() {
            var strName = '';
            $('#ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch tr').each(function () {
                if ($(this).find("input[id*='chkSelect']").is(':checked')) {
                    if ($(this).find("input[id*='txtQtyFulfilled']").length > 0) {
                        $(this).find("input[id*='txtQtyFulfilled']").val(parseFloat($(this).find("[id*='lblQtyOrdered']").text()) - parseFloat($(this).find("[id*='lblQtyReceived']").text()));
                    }
                }
                else {
                    if ($(this).find("input[id*='txtQtyFulfilled']").length > 0) {
                        $(this).find("input[id*='txtQtyFulfilled']").val("0");
                    }
                    strName = '';
                    $('#hdfTest').val("");
                }
            });
        }

        function pageLoaded() {
            $('#ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch tr').each(function () {
                $(this).find("input[id*='txtQtyFulfilled']").click(function () {
                    $(this).parents('#ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch tr').find("input[id*='chkSelect']").prop('checked', true);
                });
            });

            $("#btnSaveMain").css({ "display": "none" });
            $("#btnUpdateOrderStatus").css({ "display": "none" });
            $("#lblCharType").css({ "color": "red" });

            $('#btnSave').click(function (e) {
                if ($("[id$=calfrom_txtDate]").value == "") {
                    alert("Enter Received Date")


                    $("[id$=calfrom_txtDate]").focus();
                    e.preventDefault();
                }
                else {
                    $("#lblPurchaseOrder").text("");
                    $("#lblVendorConfirm").text("");
                    $("#lblSelectedItems").html("");

                    var selectedPO = 0;
                    var isItemSelected = false;
                    $('#ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch tr :not(:first)').each(function () {
                        if ($(this).find("input[id*='chkSelect']").is(':checked')) {
                            isItemSelected = true;

                            if (selectedPO == 0) {
                                selectedPO = parseInt($(this).find("[id*='lblOppID']").text());
                                $("#lblPurchaseOrder").text($(this).find("[id$=lblPOppName]").text());
                                $("#lblVendorConfirm").text($(this).find("[id$=lblVendor]").text());
                                if ($(this).find("[id$=txtQtyFulfilled]").length > 0) {
                                    $("#lblSelectedItems").html($(this).find("[id$=lblItemName]").text() + " (" + $(this).find("[id$=txtQtyFulfilled]").val() + ")");
                                } else {
                                    $("#lblSelectedItems").html($(this).find("[id$=lblItemName]").text() + " (" + $(this).find("[id$=lblRemaining]").text() + ")");
                                }
                            } else if (selectedPO != parseInt($(this).find("[id*='lblOppID']").text())) {
                                alert("Yor can receive item(s) from single purchase order only.");
                                return false;
                            } else {
                                if ($(this).find("[id$=txtQtyFulfilled]").length > 0) {
                                    $("#lblSelectedItems").html($("#lblSelectedItems").html() + "<br/>" + $(this).find("[id$='lblItemName']").text() + " (" + $(this).find("[id$=txtQtyFulfilled]").val() + ")");
                                } else {
                                    $("#lblSelectedItems").html($("#lblSelectedItems").html() + "<br/>" + $(this).find("[id$='lblItemName']").text() + " (" + $(this).find("[id$=lblRemaining]").text() + ")");
                                }
                            }
                        }
                    });

                    if (!isItemSelected) {
                        alert("Please select atleast one item.");
                        return false;
                    }

                    $("#btnReceive").removeClass("disabled");
                    $("#dialog-confirm").modal();

                    e.preventDefault();
                }
            });

            $("#btnReceive").click(function () {
                if ($(this).hasClass("disabled"))
                    return;

                $(this).addClass("disabled");

                $("#dialog-confirm").modal("hide");
                $("#btnSaveMain").click();
            });
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function ReceivePO() {
            var CheckedValue = 1;

            $('#ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch tr').each(function () {

                if ($(this).find("input[id*='chkSelect']").is(':checked')) {
                    CheckedValue = 0;
                    return true;
                }
            });

            if (CheckedValue == 0) {
                return true;
            }
            else {
                CheckedValue = 1;
                alert("Please Select Any Order");

                return false;
            }

            return true
        }
        function validate(i) {
            if (i < 10) {
                str = '0' + i
            }
            else {
                str = i
            }

            var txtQuantityToReceive = $("input[id*='_ctl'" + str + "'_txtQtyFulfilled']");

            if (txtQuantityToReceive != null) {
                var OldQtyReceived = $("[id*='_ctl'" + str + "'_lblQtyReceived']").innerHTML;
                var NewQtyReceived = txtQuantityToReceive.value;
                var QtyOrdered = $("[id*='_ctl'" + str + "'_lblQtyOrdered']").innerHTML;

                console.log('ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch_ctl' + str + '_chkSelect');


                if (parseInt(NewQtyReceived) > (parseInt(QtyOrdered) - parseInt(OldQtyReceived))) {
                    alert('You can not un-receive an Item');
                    txtQuantityToReceive.value = txtQuantityToReceive.defaultValue;
                    return false;
                }

                if (parseInt(NewQtyReceived) > parseInt(QtyOrdered)) {
                    alert('You can not receive more than Qty Ordered');
                    txtQuantityToReceive.value = txtQuantityToReceive.defaultValue;
                    return false;
                }
            }

            return true;
        }

        function keyPressed(TB, e) {
            var tblGrid = document.getElementById("ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch");

            var rowcount = tblGrid.rows.length;
            var TBID = document.getElementById(TB);

            var key;
            if (window.event) { e = window.event; }
            key = e.keyCode;
            if (key == 37 || key == 38 || key == 39 || key == 40) {
                for (Index = 1; Index < rowcount; Index++) {

                    for (childIndex = 0; childIndex <
              tblGrid.rows[Index].cells.length; childIndex++) {

                        if (tblGrid.rows[Index].cells[childIndex].children[0] != null) {
                            if (tblGrid.rows[Index].cells[
                 childIndex].children[0].id == TBID.id) {

                                if (key == 40) {
                                    if (Index + 1 < rowcount) {
                                        if (tblGrid.rows[Index + 1].cells[
                     childIndex].children[0] != null) {
                                            if (tblGrid.rows[Index + 1].cells[
                       childIndex].children[0].type == 'text') {

                                                //downvalue

                                                tblGrid.rows[Index + 1].cells[
                             childIndex].children[0].focus();
                                                return false;
                                            }
                                        }
                                    }
                                }
                                if (key == 38) {
                                    if (tblGrid.rows[Index - 1].cells[
                     childIndex].children[0] != null) {
                                        if (tblGrid.rows[Index - 1].cells[
                       childIndex].children[0].type == 'text') {
                                            //upvalue

                                            tblGrid.rows[Index - 1].cells[
                             childIndex].children[0].focus();
                                            return false;
                                        }
                                    }
                                }

                                if (key == 37 && (childIndex != 0)) {

                                    if ((tblGrid.rows[Index].cells[
                      childIndex - 1].children[0]) != null) {

                                        if (tblGrid.rows[Index].cells[
                       childIndex - 1].children[0].type == 'text') {
                                            //left

                                            if (tblGrid.rows[Index].cells[
                         childIndex - 1].children[0].value != '') {
                                                var cPos =
                           getCaretPos(tblGrid.rows[Index].cells[
                                       childIndex - 1].children[0], 'left');
                                                if (cPos) {
                                                    tblGrid.rows[Index].cells[
                                 childIndex - 1].children[0].focus();
                                                    return false;
                                                }
                                                else {
                                                    return false;
                                                }
                                            }
                                            tblGrid.rows[Index].cells[childIndex - 1].children[0].focus();
                                            return false;
                                        }
                                    }
                                }

                                if (key == 39) {
                                    if (tblGrid.rows[Index].cells[childIndex + 1].children[0] != null) {
                                        if (tblGrid.rows[Index].cells[
                       childIndex + 1].children[0].type == 'text') {
                                            //right

                                            if (tblGrid.rows[Index].cells[
                         childIndex + 1].children[0].value != '') {
                                                var cPosR =
                           getCaretPos(tblGrid.rows[Index].cells[
                                       childIndex + 1].children[0], 'right');
                                                if (cPosR) {
                                                    tblGrid.rows[Index].cells[
                                 childIndex + 1].children[0].focus();
                                                    return false;
                                                }
                                                else {
                                                    return false;
                                                }
                                            }
                                            tblGrid.rows[Index].cells[childIndex + 1].children[0].focus();
                                            return false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        function OpenHelp() {
            window.open('../Help/Purchase_Fulfillment.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function SelectAllPurchaseFulfillment(headerCheckBox, ItemCheckboxClass) {
            if ($('[id$=' + headerCheckBox + ']').is(':checked')) {
                var selectedPO = 0;

                $('#ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch tr :not(:first)').each(function () {
                    if ($(this).find("input[id*='chkSelect']").is(":checked")) {
                        selectedPO = parseInt($(this).find("[id*='lblOppID']").text());
                    }
                });

                $('#ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch tr :not(:first)').each(function () {
                    if (selectedPO > 0) {
                        if (parseInt($(this).find("[id*='lblOppID']").text()) == selectedPO) {
                            $(this).find("input[id*='chkSelect']").prop("checked", true);
                        }
                    } else {
                        selectedPO = parseInt($(this).find("[id*='lblOppID']").text());
                        $(this).find("input[id*='chkSelect']").prop("checked", true);
                    }
                });

            } else {
                $('#ctl00_ctl00_MainContent_GridPlaceHolder_gvSearch tr :not(:first)').each(function () {
                    $(this).find("input[id*='chkSelect']").prop("checked", false);
                });
            }

            SwapQty();
        }
    </script>
    <script type="text/javascript">
        var onFulfillmentGridColumnResized = function (e) {
            var columns = $(e.currentTarget).find("th");
            var msg = "";
            columns.each(function () {
                var thId = $(this).attr("id");
                var thWidth = $(this).width();
                msg += (thId || "anchor") + ':' + thWidth + ';';
            }
            )

            $.ajax({
                type: "POST",
                url: "../common/Common.asmx/SaveGridColumnWidth",
                data: "{str:'" + msg + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json"
            });
        };
        function AddTHEAD(tableName) {
            var table = document.getElementById(tableName);
            if (table != null) {
                var head = document.createElement("THEAD");
                head.style.display = "table-header-group";
                head.appendChild(table.rows[0]);
                table.insertBefore(head, table.childNodes[0]);
            }
        }

        function OpenSetting() {
            window.open('../Items/frmConfItemList.aspx?FormID=135', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function OnClientItemsRequesting(sender, eventArgs) {
            var context = eventArgs.get_context();
            context["searchType"] = $("[id$=ddlSearch]").val();
        }

        function HandleEnterKeyPress(sender, eventArgs) {
            //if (eventArgs.get_domEvent().keyCode == 13) {
            //    sender.raise_Page_Load();
            //}
        }

        function FocusSearch() {
            var comboBox = $find("<%=radcmbSearch.ClientID%>");
            comboBox.get_inputDomElement().focus();
            comboBox.selectText(comboBox.get_text().length, comboBox.get_text().length);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <asp:HiddenField ID="hdfTest" runat="server" Value="0" />
                    <label>Search For:</label>
                    <telerik:RadComboBox runat="server" ID="radcmbSearch" OnClientKeyPressing="HandleEnterKeyPress" Width="200" AutoPostBack="true"
                        DropDownWidth="300px" OnItemsRequested="radcmbSearch_ItemsRequested" EnableVirtualScrolling="true" ShowMoreResultsBox="true"
                        EnableLoadOnDemand="true" AllowCustomText="True" OnClientItemsRequesting="OnClientItemsRequesting"
                        OnSelectedIndexChanged="radcmbSearch_SelectedIndexChanged">
                    </telerik:RadComboBox>
                    &nbsp;
                        <asp:DropDownList ID="ddlSearch" runat="server" CssClass="form-control">
                            <asp:ListItem Text="Item" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Warehouse" Value="2"></asp:ListItem>
                            <asp:ListItem Text="Purchase Order" Value="3"></asp:ListItem>
                            <asp:ListItem Text="Vendor" Value="4"></asp:ListItem>
                            <asp:ListItem Text="BizDoc" Value="5"></asp:ListItem>
                            <asp:ListItem Text="SKU" Value="6"></asp:ListItem>
                        </asp:DropDownList>
                    <div class="form-group" id="divVendorInvoice" runat="server" visible="false">
                        <label>Filter by vendor invoice:</label>
                        <asp:DropDownList ID="ddlVendorInvoice" runat="server" CssClass="form-control" AutoPostBack="true">
                            <asp:ListItem Text="-- Select One --" Value="0"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <div class="form-inline">
                    <label>Received Date:</label>
                    <BizCalendar:Calendar ID="calfrom" runat="server" ClientIDMode="AutoID" />
                    <asp:HiddenField ID="hdfValue" runat="server" Value="0" />
                    <asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" Text="Save Quantity Received" OnClientClick="return ReceivePO();"></asp:Button>
                    <asp:Button ID="btnSaveMain" CssClass="btn btn-primary" runat="server" Style="display: none;" Text="Save Quantity Received"></asp:Button>
                    <asp:Button ID="btnUpdateOrderStatus" runat="server" Style="display: none;" CssClass="btn btn-primary" Text="btn" />
                </div>
            </div>
        </div>
    </div>
    <div id="dialog-confirm" class="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">�</span></button>
                    <h4 class="modal-title">Fulfillment action required.</h4>
                </div>
                <div class="modal-body">
                    <p>
                        You�re about to receive inventory for: <b>
                            <label id="lblPurchaseOrder"></label>
                        </b>from vendor: <b>
                            <label id="lblVendorConfirm"></label>
                        </b>
                    </p>
                    <b>Item name (Qty to receive)</b>
                    <br />
                    <span id="lblSelectedItems"></span>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnReceive" class="btn btn-primary">Yep let�s update inventory</button>
                    <button type="button" id="btnCancel" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Purchase Fulfillment&nbsp;<a href="#" onclick="return OpenHelp()"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ContentPlaceHolderID="GridSettingPopup" runat="server">
    <asp:LinkButton ID="lkbClearFilter" runat="server" OnClick="lkbClearFilter_Click" class="btn-box-tool" ToolTip="Clear All Filters"><i class="fa fa-2x fa-filter"></i></asp:LinkButton>
    <a id="tdGridConfiguration" runat="server" href="#" onclick="return OpenSetting()" title="Show Grid Settings." class="btn-box-tool"><i class="fa fa-2x fa-cog"></i></a>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server" ClientIDMode="Static">
    <uc1:frmBizSorting ID="frmBizSorting2" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
        <asp:GridView ID="gvSearch" runat="server" Width="100%" CssClass="table table-responsive" AllowSorting="True"
            AutoGenerateColumns="False" ClientIDMode="AutoID" UseAccessibleHeader="true" ShowHeader="true">
            <AlternatingRowStyle CssClass="ais"></AlternatingRowStyle>
            <RowStyle CssClass="is"></RowStyle>
            <HeaderStyle CssClass="hs"></HeaderStyle>
            <Columns>
            </Columns>
        </asp:GridView>
    </div>
    <asp:TextBox ID="txtDelOppId" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPagePOFulFillment" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecordsPOFulFillment" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtOpptype" runat="server" Text="0" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:HiddenField ID="hdnMode" runat="server" />
    <asp:HiddenField ID="hdfOppID" runat="server" Value="0" />
    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:HiddenField ID="hdnSelectedOppID" runat="server" />


</asp:Content>
