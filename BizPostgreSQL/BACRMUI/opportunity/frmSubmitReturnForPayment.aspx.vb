﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Item

Namespace BACRM.UserInterface.Opportunities
    Partial Public Class frmSubmitReturnForPayment
        Inherits BACRMPage
        Dim objOpportunity As MOpportunity
        Dim dtItems As DataTable
        Dim lngCustomerAPAccount As Long
        Dim iOppType As Integer

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                iOppType = CCommon.ToInteger(GetQueryStringVal("type"))

                If Not IsPostBack Then
                    BindGrid()


                    calReturnDate.SelectedDate = DateTime.Now.Date

                    If iOppType = 2 Then
                        ddlPaymentAction.Items.Remove(ddlPaymentAction.Items.FindByValue("3"))
                        lblAmountPaidTitle.Text = "Amount Paid to Vendor"
                        btnCreditMemo.Visible = False
                    Else
                        lblAmountPaidTitle.Text = "Amount Customer Paid"
                    End If
                End If
                btnClose.Attributes.Add("onclick", "return Close('" & iOppType & "')")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Sub BindGrid()
            Try

                objCommon.sb_FillComboFromDBwithSel(ddlPaymentMethod, 31, Session("DomainID"))

                If objOpportunity Is Nothing Then objOpportunity = New MOpportunity
                With objOpportunity
                    .DomainID = Session("DomainID")
                    .ReturnStatus = 0
                    .ReasonForReturn = 0
                    .ReturnID = GetQueryStringVal("ReturnID")
                    .SortCharacter = "0"
                    .SearchText = ""
                    .CurrentPage = 1
                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0
                    .OppType = iOppType
                End With

                objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                Dim dt As DataTable = objOpportunity.GetReturns

                If dt.Rows.Count > 0 Then

                    lblQtytoReturn.Text = dt(0)("QuantityToReturn")
                    lblQtyReturned.Text = dt(0)("QuantityReturned")

                    txtAmountCreditCmp.Text = dt(0)("montotamount")
                    txtQReturned.Text = dt(0)("QuantityToReturn") - dt(0)("QuantityReturned")
                    txtQtyReturnCmp.Text = dt(0)("QuantityToReturn") - dt(0)("QuantityReturned")

                    If dt(0)("QuantityToReturn") - dt(0)("QuantityReturned") = 0 Then
                        btnSubmit.Visible = False
                        lblMessage.Text = "No quantity available to return.All quantity Returned."
                    End If

                    hdnItemCode.Value = dt(0)("numItemCode")
                    hdnDivisionID.Value = dt(0)("numDivisionID")
                    lblPrice.Text = String.Format("{0:0.##}", dt(0)("monprice"))
                    lblAmountPaid.Text = String.Format("{0:0.##}", dt(0)("montotamount"))
                    lblItemCreditBalance.Text = String.Format("{0:0.##}", dt(0)("monCreditBalance"))

                    hfItemPrice.Value = dt(0)("monprice")
                    hfAmountPaid.Value = dt(0)("montotamount")
                    hfItemCreditBalance.Value = dt(0)("monCreditBalance")

                    btnCreditMemo.Attributes.Add("onclick", "return OpenRMA('" & dt(0)("numOppId") & "','Credit Memo');")

                    If iOppType = 1 Then
                        hfDivisionCreditBalance.Value = dt(0)("monSCreditBalance")
                        lblOrganizationCreditBalance.Text = String.Format("{0:0.##}", dt(0)("monSCreditBalance"))
                    Else
                        hfDivisionCreditBalance.Value = dt(0)("monPCreditBalance")
                        lblOrganizationCreditBalance.Text = String.Format("{0:0.##}", dt(0)("monPCreditBalance"))
                    End If
                End If
                'gvSubmitForPayment.DataSource = objOpportunity.GetReturns
                'gvSubmitForPayment.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
            Try

                If ddlPaymentAction.SelectedValue = 2 Then
                    If CCommon.ToDecimal(txtAmountCredit.Text) > CCommon.ToDecimal(hfDivisionCreditBalance.Value) Then
                        lblMessage.Text = "There is not enough Organization Credit Balance."
                        Exit Sub
                    End If
                End If

                If ddlPaymentAction.SelectedValue = 1 Then
                    If CCommon.ToDecimal(txtAmountCredit.Text) > (CCommon.ToDecimal(hfAmountPaid.Value) - CCommon.ToDecimal(hfItemCreditBalance.Value)) Then
                        lblMessage.Text = "Amount Credit is greater than Amount Customer Paid and Item Credit Balance."
                        Exit Sub
                    End If
                End If

                If ddlPaymentAction.SelectedValue = 1 Or ddlPaymentAction.SelectedValue = 2 Then
                    Dim objOppBizDocs As New OppBizDocs
                    lngCustomerAPAccount = objOppBizDocs.ValidateCustomerAR_APAccounts("AP", Session("DomainID"), hdnDivisionID.Value)
                    If lngCustomerAPAccount = 0 Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip""  To Save Amount' );", True)
                        Exit Sub
                    End If

                    Dim objItem As New CItems
                    If Not objItem.ValidateItemAccount(hdnItemCode.Value, Session("DomainID"), 1) = True Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "AccountValidation", "alert('Please Set Income,Asset,COGs(Expense) Account for selected Item from Administration->Inventory->Item Details')", True)
                        Return
                    End If
                End If




                If objOpportunity Is Nothing Then objOpportunity = New MOpportunity
                'If Not CType(gvSubmitForPayment.Rows(0).FindControl("calReturnDate_txtDate"), TextBox).Text = "" Then
                With objOpportunity
                    .DomainID = Session("DomainID")
                    .PaymentType = 3
                    .Reference = "" 'txtReference.Text
                    .Memo = txtMemo.Text
                    .UserCntID = Session("UserContactID")

                    .ReturnDate = DateTime.Parse(calReturnDate.SelectedDate).ToShortDateString & " 23:59:59"
                    .ReturnID = GetQueryStringVal("ReturnID")
                    .UnitsReturned = txtQReturned.Text
                    .OppType = iOppType
                    '.ReturnStatus = CType(gvSubmitForPayment.Rows(0).FindControl("ddlStatus"), DropDownList).SelectedValue
                End With

                If ddlPaymentAction.SelectedValue = 1 Or ddlPaymentAction.SelectedValue = 2 Then 'Issue credit (adds to the credit balance) --or-- Reverse issued credit  (subtracts from the credit balance)   
                    objOpportunity.Amount = CCommon.ToDecimal(txtAmountCredit.Text.Trim())
                    Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                        If objOpportunity.ManageCreditBalanceHistory(ddlPaymentAction.SelectedValue) = True Then
                            'Make journal entry bug id 1462
                            Dim jounralID As Long = SaveDataToHeader(objOpportunity.Amount, 0)
                            SaveDataToGeneralJournalDetails(jounralID, objOpportunity.Amount)
                        End If

                        objTransactionScope.Complete()
                    End Using
                ElseIf ddlPaymentAction.SelectedValue = 3 Then 'Create a sales return in money-out 
                    objOpportunity.PaymentMethod = ddlPaymentMethod.SelectedValue
                    objOpportunity.Amount = CDbl(hfItemPrice.Value) * CCommon.ToInteger(txtQReturned.Text)

                    objOpportunity.AddSubmitForPayment(ddlPaymentAction.SelectedValue)
                End If

                Page.RegisterClientScriptBlock("close", "<script>Close('" & iOppType & "');</script>")

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub


        Private Sub ddlPaymentAction_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPaymentAction.SelectedIndexChanged
            Try
                If ddlPaymentAction.SelectedValue = 1 Or ddlPaymentAction.SelectedValue = 2 Then 'Issue credit --or-- Reverse issued credit 
                    trPaymentMethod.Visible = False
                    trAmountCredit.Visible = True

                    txtMemo.Text = "Credit balance issued"
                ElseIf ddlPaymentAction.SelectedValue = 3 Then 'Create a sales return in money-out 
                    trPaymentMethod.Visible = True
                    trAmountCredit.Visible = False

                    txtMemo.Text = "Sales return added to money-out"
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Private Function SaveDataToHeader(ByVal p_Amount As Decimal, ByVal p_OppId As Integer) As Integer
            Try
                Dim lngJournalId As Integer = 0

                Dim objJEHeader As New JournalEntryHeader
                With objJEHeader
                    .JournalId = 0
                    .RecurringId = 0
                    .EntryDate = Date.UtcNow.ToShortDateString & " 12:00:00" 'Now.UtcNow
                    .Description = txtMemo.Text + " " + txtAmountCredit.Text
                    .Amount = CCommon.ToDecimal(p_Amount)
                    .CheckId = 0
                    .CashCreditCardId = 0
                    .ChartAcntId = 0
                    .OppId = p_OppId
                    .OppBizDocsId = 0 'p_OppBizDocId
                    .DepositId = 0
                    .BizDocsPaymentDetId = 0
                    .IsOpeningBalance = 0
                    .LastRecurringDate = Date.Now
                    .NoTransactions = 0
                    .CategoryHDRID = 0
                    .ReturnID = CCommon.ToLong(GetQueryStringVal("ReturnID"))
                    .CheckHeaderID = 0
                    .BillID = 0
                    .BillPaymentID = 0
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")
                End With
                lngJournalId = objJEHeader.Save()
                Return lngJournalId

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Private Sub SaveDataToGeneralJournalDetails(ByVal lngJournalId As Long, ByVal Amount As Decimal)

            Try
                Dim objItem As New CItems
                Dim dtItemDetails As DataTable

                objItem.ItemCode = hdnItemCode.Value
                objItem.ClientZoneOffsetTime = Session("ClientMachineUTCTimeOffset")
                dtItemDetails = objItem.ItemDetails

                If dtItemDetails.Rows.Count > 0 Then
                    If CCommon.ToLong(dtItemDetails.Rows(0)("numIncomeChartAcntId")) > 0 Then

                        Dim decDebitAmtItem As Decimal
                        Dim decCreditAmtItem As Decimal
                        Dim decDebitAmtCustomer As Decimal
                        Dim decCreditAmtCustomer As Decimal

                        If ddlPaymentAction.SelectedValue = 1 Then '- issue credit 
                            decDebitAmtItem = Amount
                            decCreditAmtItem = 0
                            decDebitAmtCustomer = 0
                            decCreditAmtCustomer = Amount
                        ElseIf ddlPaymentAction.SelectedValue = 2 Then
                            decDebitAmtItem = 0
                            decCreditAmtItem = Amount
                            decDebitAmtCustomer = Amount
                            decCreditAmtCustomer = 0
                        End If

                        Dim objJEList As New JournalEntryCollection
                        Dim objJE As New JournalEntryNew()

                        objJE = New JournalEntryNew()

                        objJE.TransactionId = 0
                        objJE.DebitAmt = decDebitAmtItem
                        objJE.CreditAmt = decCreditAmtItem
                        objJE.ChartAcntId = CCommon.ToLong(dtItemDetails.Rows(0)("numIncomeChartAcntId")) 'Income account of item
                        objJE.Description = ""
                        objJE.CustomerId = CCommon.ToLong(hdnDivisionID.Value)
                        objJE.MainDeposit = 0
                        objJE.MainCheck = 0
                        objJE.MainCashCredit = 0
                        objJE.OppitemtCode = 0
                        objJE.BizDocItems = ""
                        objJE.Reference = ""
                        objJE.PaymentMethod = 0
                        objJE.Reconcile = False
                        objJE.CurrencyID = 0
                        objJE.FltExchangeRate = 0
                        objJE.TaxItemID = 0
                        objJE.BizDocsPaymentDetailsId = 0
                        objJE.ContactID = 0
                        objJE.ItemID = hdnItemCode.Value
                        objJE.ProjectID = 0
                        objJE.ClassID = 0
                        objJE.CommissionID = 0
                        objJE.ReconcileID = 0
                        objJE.Cleared = 0
                        objJE.ReferenceType = enmReferenceType.RMA 'SalesReturn
                        objJE.ReferenceID = CCommon.ToLong(GetQueryStringVal("ReturnID"))

                        objJEList.Add(objJE)


                        objJE = New JournalEntryNew()

                        objJE.TransactionId = 0
                        objJE.DebitAmt = decDebitAmtCustomer
                        objJE.CreditAmt = decCreditAmtCustomer
                        objJE.ChartAcntId = lngCustomerAPAccount 'Customer's Account Payable
                        objJE.Description = ""
                        objJE.CustomerId = CCommon.ToLong(hdnDivisionID.Value)
                        objJE.MainDeposit = 0
                        objJE.MainCheck = 0
                        objJE.MainCashCredit = 0
                        objJE.OppitemtCode = 0
                        objJE.BizDocItems = ""
                        objJE.Reference = ""
                        objJE.PaymentMethod = 0
                        objJE.Reconcile = False
                        objJE.CurrencyID = 0
                        objJE.FltExchangeRate = 0
                        objJE.TaxItemID = 0
                        objJE.BizDocsPaymentDetailsId = 0
                        objJE.ContactID = 0
                        objJE.ItemID = hdnItemCode.Value
                        objJE.ProjectID = 0
                        objJE.ClassID = 0
                        objJE.CommissionID = 0
                        objJE.ReconcileID = 0
                        objJE.Cleared = 0
                        objJE.ReferenceType = enmReferenceType.RMA 'SalesReturn
                        objJE.ReferenceID = CCommon.ToLong(GetQueryStringVal("ReturnID"))

                        objJEList.Add(objJE)

                        objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, Session("DomainID"))
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
    End Class
End Namespace
