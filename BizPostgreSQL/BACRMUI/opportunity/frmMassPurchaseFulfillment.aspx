﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/BizMaster.Master" CodeBehind="frmMassPurchaseFulfillment.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmMassPurchaseFulfillment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Purchase Fulfillment</title>
    <link rel="stylesheet" href="../CSS/select2.css" />
    <link rel="stylesheet" href="../CSS/animate.min.css" />
    <script type="text/javascript" src="../JavaScript/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../JavaScript/jquery.twbsPagination.min.js"></script>
    <script type="text/javascript" src="../JavaScript/select2.js"></script>
    <script type="text/javascript" src="../JavaScript/MassPurchaseFulfillment.js"></script>
    <script type="text/javascript" src="../JavaScript/colResizable-1.6.min.js"></script>
    <style type="text/css">
        #chkshowrecords input {
            width: 15px;
            height: 15px;
        }

        .bg-putaway, .bg-putaway:hover, .bg-putaway:focus {
            background-color: #0073b7;
            color: #fff;
        }

        .bg-close, .bg-close:hover, .bg-close:focus {
            background-color: #c00000;
            color: #fff;
        }

        .receive-items, .bill-orders, .close-orders {
            font-size: 22px;
        }


        .ms-sidebar > .row {
            margin: 0px;
            padding: 0px;
        }

        .ms-sidebar > .row {
            margin: 0px;
            padding: 0px;
        }

        #ms-sidebar {
            min-height: 85vh;
            vertical-align: top;
            background-color: #f9f9f9;
            border-right: 5px solid #00a65a;
            -webkit-transition: -webkit-transform .3s ease-in-out, width .3s ease-in-out;
            -moz-transition: -moz-transform .3s ease-in-out, width .3s ease-in-out;
            -o-transition: -o-transform .3s ease-in-out, width .3s ease-in-out;
            transition: transform .3s ease-in-out, width .3s ease-in-out;
        }

        div.ms-sidebar-collapsed #ms-sidebar {
            -webkit-transform: translate(0, 0);
            -ms-transform: translate(0, 0);
            -o-transform: translate(0, 0);
            transform: translate(0, 0);
            width: 0px !important;
            padding-right: 0px;
            padding-left: 0px;
            border-right: 0px !important;
        }

            div.ms-sidebar-collapsed #ms-sidebar .row {
                display: none;
            }

        #ms-main-grid {
            min-height: 85vh;
            vertical-align: top;
            border-right: 5px solid #d0d0d0;
            -webkit-transition: -webkit-transform .3s ease-in-out, width .3s ease-in-out;
            -moz-transition: -moz-transform .3s ease-in-out, width .3s ease-in-out;
            -o-transition: -o-transform .3s ease-in-out, width .3s ease-in-out;
            transition: transform .3s ease-in-out, width .3s ease-in-out;
        }

        #min-grid-controls {
            margin: 0px;
        }

            #min-grid-controls ul.pagination {
                margin: 0px;
            }

            #min-grid-controls > li {
                padding: 0px;
                vertical-align: top;
            }

        #liRecordDisplay {
            line-height: 34px;
        }

        div.ms-maingrid-collapsed #ms-main-grid {
            -webkit-transform: translate(0, 0);
            -ms-transform: translate(0, 0);
            -o-transform: translate(0, 0);
            transform: translate(0, 0);
            width: 0px !important;
            padding-right: 0px;
            padding-left: 0px;
            border-right: 0px !important;
        }

            div.ms-maingrid-collapsed #ms-main-grid .row {
                display: none;
            }

        #ms-selected-grid {
            min-height: 85vh;
            vertical-align: top;
            -webkit-transition: -webkit-transform .3s ease-in-out, width .3s ease-in-out;
            -moz-transition: -moz-transform .3s ease-in-out, width .3s ease-in-out;
            -o-transition: -o-transform .3s ease-in-out, width .3s ease-in-out;
            transition: transform .3s ease-in-out, width .3s ease-in-out;
        }

            #ms-selected-grid .select2-container {
                margin-top: 0px;
                margin-left: 0px;
            }

        div.ms-selectedgrid-collapsed #ms-selected-grid {
            -webkit-transform: translate(0, 0);
            -ms-transform: translate(0, 0);
            -o-transform: translate(0, 0);
            transform: translate(0, 0);
            width: 0px !important;
            padding-right: 0px;
            padding-left: 0px;
        }

            div.ms-selectedgrid-collapsed #ms-selected-grid .row {
                display: none;
            }

        div.ms-selectedgrid-collapsed #ms-main-grid {
            border-right: 0px !important;
        }

        #ulFilfillmentSteps {
            padding-top: 20px;
        }

            #ulFilfillmentSteps input {
                border: 0px;
                text-align: left;
            }

            #ulFilfillmentSteps li {
                padding-right: 15px;
                -webkit-transition: -webkit-transform .3s ease-in-out, padding-right .3s ease-in-out;
                -moz-transition: -moz-transform .3s ease-in-out, padding-right .3s ease-in-out;
                -o-transition: -o-transform .3s ease-in-out, padding-right .3s ease-in-out;
                transition: transform .3s ease-in-out, padding-right .3s ease-in-out;
            }

                #ulFilfillmentSteps li.active {
                    padding-right: 0px;
                }

        #divOrderStatus div.checkbox, #divFilterValue div.checkbox {
            margin-top: 0px;
            margin-bottom: 0px;
        }

            #divOrderStatus div.checkbox input, #divFilterValue div.checkbox input {
                margin-left: 0px;
            }

            #divOrderStatus div.checkbox label {
                font-weight: 600;
            }

        #divMainGrid {
            height: 72vh;
            overflow-y: auto;
            overflow-x: auto;
        }

            #divMainGrid > table > tbody > tr > th {
                position: sticky;
                top: -1px;
                white-space: nowrap;
            }

            #divMainGrid > table > tbody > tr > td.nowrap {
                white-space: nowrap;
            }

        #divMainGridTopFilters div.checkbox {
            margin-top: 0px;
            margin-bottom: 0px;
        }

            #divMainGridTopFilters div.checkbox input {
                margin-left: 0px;
            }

            #divMainGridTopFilters div.checkbox label {
                font-weight: 600;
            }

        .focusedRow {
            transform: scale(1);
            -webkit-transform: scale(1);
            -moz-transform: scale(1);
            box-shadow: 0px 0px 5px #06a65a;
            -webkit-box-shadow: 0px 0px 5px #06a65a;
            -moz-box-shadow: 0px 0px 5px #06a65a;
        }

        .putAwayItem {
            background-color: #DBF9DC !important;
        }

        #divMobilePutAway ul.nav {
            position: relative;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: -moz-flex;
            display: -ms-flex;
            display: flex;
            border-bottom: 0px;
        }

            #divMobilePutAway ul.nav li {
                border: 1px solid #0073b7;
                -webkit-flex: 1;
                -moz-flex: 1;
                -ms-flex: 1;
                flex: 1;
                text-align: center;
            }

                #divMobilePutAway ul.nav li:not(:last-child) {
                    border-right: none;
                }

                #divMobilePutAway ul.nav li.active {
                    border-top-color: #0073b7;
                    border-bottom: none;
                    background-color: #0073b7;
                }

                #divMobilePutAway ul.nav li a, #divMobilePutAway ul.nav li a:active, #divMobilePutAway ul.nav li a:focus, #divMobilePutAway ul.nav li a:hover {
                    border: none;
                    color: #000;
                    border-radius: 0px;
                    margin: 0px;
                }

                #divMobilePutAway ul.nav li.active a {
                    background: none;
                    color: #FFF;
                    font-weight: bold;
                }

        #divMobilePutAway .receive-items, .bill-orders, .close-orders {
            font-size: 22px;
        }

        #divMobilePutAway {
            display: none;
        }

        /* Custom, iPhone Retina */
        @media only screen and (max-width : 320px) {
            .container-fluid {
                padding-left: 15px;
                padding-right: 15px;
            }

            #ms-main-grid .row {
                margin-left: 0px;
                margin-right: 0px;
            }

            .main-header {
                display: none;
            }

            .content-wrapper {
                padding-top: 0px !important;
            }

            #divMobilePutAway {
                display: block;
            }

            #ms-sidebar {
                display: none;
            }

            #ms-main-grid {
                display: initial;
            }

            #ms-selected-grid {
                display: none;
            }

            #btncollapseselectedgrid {
                display: none;
            }
        }

        /* Extra Small Devices, Phones */
        @media only screen and (max-width : 480px) {
            .container-fluid {
                padding-left: 15px;
                padding-right: 15px;
            }

            #ms-main-grid .row {
                margin-left: 0px;
                margin-right: 0px;
            }

            .main-header {
                display: none;
            }

            .content-wrapper {
                padding-top: 0px !important;
            }

            #divMobilePutAway {
                display: block;
            }

            #ms-sidebar {
                display: none;
            }

            #ms-main-grid {
                display: initial;
            }

            #ms-selected-grid {
                display: none;
            }

            #btncollapseselectedgrid {
                display: none;
            }
        }

        /* Small Devices, Tablets */
        @media only screen and (max-width : 768px) {
            .container-fluid {
                padding-left: 15px;
                padding-right: 15px;
            }

            #ms-main-grid .row {
                margin-left: 0px;
                margin-right: 0px;
            }

            .main-header {
                display: none;
            }

            .content-wrapper {
                padding-top: 0px !important;
            }

            #divMobilePutAway {
                display: block;
            }

            #ms-sidebar {
                display: none;
            }

            #ms-main-grid {
                display: initial;
            }

            #ms-selected-grid {
                display: none;
            }

            #btncollapseselectedgrid {
                display: none;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="overlay" id="divLoader" style="z-index: 10000; display: none">
        <div class="overlayContent" style="color: #000; background-color: #fff; text-align: center; width: 250px; padding: 20px">
            <i class="fa fa-2x fa-refresh fa-spin"></i>
            <h3>Please wait...</h3>
        </div>
    </div>
    <div class="container-fluid">
        <div id="ms-container" class="row ms-sidebar-expanded ms-maingrid-expanded ms-selectedgrid-expanded" style="background-color: white; border-radius: 5px;">
            <div class="row padbottom10" id="divMobilePutAway">
                <div class="col-xs-12">
                    <ul class="nav nav-tabs">
                        <li id="liPutAwayView1" class="active"><a href="javascript:PutAwayViewChanged(1)">Select Item(s)</a></li>
                        <li id="liPutAwayView2"><a href="javascript:PutAwayViewChanged(2)">Put-Away Item(s)</a></li>
                    </ul>
                </div>
            </div>
            <div id="ms-sidebar" class="col-sm-12 col-md-2">
                <div class="row" style="margin-top: 5px">
                    <div class="col-xs-12 padbottom10">
                        <ul class="list-inline">
                            <li>
                                <input type="radio" id="rbInclude" name="filterType" value="1" checked="checked" />
                                Include</li>
                            <li>
                                <input type="radio" id="rbExclude" name="filterType" value="2" />
                                Exclude</li>
                            <li>
                                <a href="#" onclick="return OpenHelpPopUp('opportunity/frmmasspurchasefulfillment.aspx')"><label class="badge bg-yellow">?</label></a>
                            </li>
                        </ul>
                        <ul class="list-unstyled" id="divOrderStatus">
                        </ul>
                    </div>
                    <div class="col-xs-12 padbottom10">
                        <div class="form-group">
                            <select id="ddlFilterType" class="form-control">
                                <option value="5">BizDoc</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Search Text" />
                        </div>
                        <input type="button" value="Apply Filter(s)" id="btnApplyFilters" class="btn btn-primary btn-block" />
                        <br />
                        <br />
                    </div>
                    <div class="col-xs-12" style="padding-right: 0px;">
                        <ul class="list-unstyled" id="ulFilfillmentSteps">
                            <li style="padding-bottom: 10px" class="liReceive active">
                                <input type="button" id="btnReceive" value="Ready to Receive" class="btn btn-flat bg-green btn-block" />
                            </li>
                            <li style="padding-bottom: 10px" class="liPutAway">
                                <input type="button" id="btnPutAway" value="Ready to Put-Away" class="btn btn-flat bg-putaway btn-block" />
                            </li>
                            <li style="padding-bottom: 10px" class="liBill">
                                <input type="button" id="btnBill" value="Ready to Bill" class="btn btn-flat bg-orange btn-block" />
                            </li>
                            <li class="liClose">
                                <input type="button" id="btnClose" value="Pending Close" class="btn btn-flat bg-close btn-block" />
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
            <div id="ms-main-grid" class="col-sm-12 col-md-7">
                <div class="row padbottom10" style="padding: 5px;" id="ms-main-grid-top">
                    <div class="pull-left">
                        <div class="btn-group" role="group">
                            <button id="btnmssidebar" type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i></button>
                            <div class="inline ShowRecords" id="divshowrecords" style="margin-left: 60px; font-size: 15px;" runat="server">

                                <input type="checkbox" id="chkshowrecords" name="chkshowrecords" runat="server" clientidmode="Static">
                                <label for="chkshowrecords">Show records that have been billed</label>

                            </div>
                        </div>

                    </div>
                    <div class="pull-right">
                        <ul class="list-inline" id="min-grid-controls">
                            <li id="liRecordDisplay"></li>
                            <li>
                                <ul id="ms-pagination-left"></ul>
                            </li>
                            <li>
                                <button id="btnClearFilters" type="button" class="btn btn-default" title="Clear Filters"><i class="fa fa-filter"></i></button>
                                <button id="btncolumnconfig" type="button" class="btn btn-default" title="Configure Columns"><i class="fa fa-cog"></i></button>
                                <div class="btn-group" role="group" id="divExpandCollpaseGroup">
                                    <button id="btncollapsemaingrid" type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i></button>
                                    <button id="btnexpandbothgrid" type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i><i class="fa fa-chevron-right"></i></button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row padbottom10">
                    <div class="pull-left" style="padding-left: 5px;">
                        <button id="btnReceiveOnly" type="button" class="btn btn-flat btn-success">Receive Only</button>
                        <button id="btnAddBill" type="button" class="btn btn-flat btn-primary" style="display: none">Add bill to POs</button>
                        <button id="btnCloseOrders" type="button" class="btn btn-flat btn-primary" style="display: none">Close Orders</button>
                    </div>
                    <div class="pull-right" id="divMainGridTopFilters">
                        <ul class="list-inline">
                            <li id="liGroupBy">
                                <div class="checkbox">
                                    <input type="checkbox" id="chkGroupBy" /><label>Group by Order</label>
                                </div>
                            </li>
                            <li>
                                <select id="ddlWarehouse" class="form-control">
                                </select>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12" style="padding: 0px;">
                        <div class="table-responsive" id="divMainGrid">
                        </div>
                    </div>
                </div>
            </div>
            <div id="ms-selected-grid" class="col-sm-12 col-md-3">
                <div class="row padbottom10" style="padding: 5px;">
                    <div class="pull-left">
                        <div class="btn-group" role="group">
                            <button id="btncollapseselectedgrid" type="button" class="btn btn-default"><i class="fa fa-chevron-right"></i></button>
                        </div>

                    </div>
                    <div class="pull-right">
                        <input type="text" id="txtScan" class="form-control" placeholder="Scan Item" onkeydown='return ItemPutAway(this);' />
                    </div>
                </div>
                <div class="row padbottom10" style="padding: 5px; margin-bottom: 15px;">
                    <div class="pull-left">
                        <div class="form-inline">
                            <div class="form-group">
                                <label>Location</label>
                                <input class="location-dropdown" style="width: 120px" />
                            </div>
                            <input type="button" id="btnAddLocation" value="Add" class="btn btn-flat btn-primary" />
                        </div>
                    </div>
                    <div class="pull-right">
                        <div class="form-inline">
                            <div class="form-group">
                                <label>Received Date: </label>
                                <input type="text" style="width: 105px" class="form-control" id="dtPutAwayDate" />
                            </div>
                            <button id="btnPutAwayItems" type="button" class="btn btn-flat btn-primary">Put-Away</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12" style="padding: 0px;">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped" id="tblRight">
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hdnMSWarehouseID" runat="server" />
    <asp:HiddenField ID="hdnMSImagePath" runat="server" />
    <asp:HiddenField ID="hdnMSCurrencyID" runat="server" />
    <asp:HiddenField ID="hdnDecimalPoints" runat="server" />
    <input type="hidden" id="hdnGroupByOrderReceive" />
    <input type="hidden" id="hdnGroupByOrderPutAway" />
    <input type="hidden" id="hdnGroupByOrderBill" />
    <input type="hidden" id="hdnGroupByOrderClose" />
    <input type="hidden" id="hdnViewID" value="1" runat="server" clientidmode="Static" />
    <input type="hidden" id="hdnFlag" value="" runat="server" clientidmode="Static" />
    <input type="hidden" id="hdnSortColumn" value="OpportunityMaster.bintCreatedDate" />
    <input type="hidden" id="hdnSortOrder" value="DESC" />
    <input type="hidden" id="hdnHeaderSearch" />
    <input type="hidden" id="hdnSelectedRecordsForPutAway" />
    <input type="hidden" id="hdnScanField" />
    <input type="hidden" id="hdnReceiveBillOnClose" />

</asp:Content>
