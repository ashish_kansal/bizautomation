Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Opportunities
    Public Class frmItemPriceRecommd
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents dgItemPricemgmt As System.Web.UI.WebControls.DataGrid
        Protected WithEvents btnClose As System.Web.UI.WebControls.Button

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                    
                    Dim objItems As New CItems
                    'Dim objPbook As New PriceBookRule
                    'objPbook.ItemID = GetQueryStringVal( "Itemcode")
                    'objPbook.QntyofItems = IIf(GetQueryStringVal( "Unit") = "", 0, GetQueryStringVal( "Unit"))
                    'objPbook.DomainID = Session("DomainID")
                    'objPbook.DivisionID = GetQueryStringVal( "DivID")
                    objItems.ItemCode = CCommon.ToLong(GetQueryStringVal("Itemcode"))
                    objItems.NoofUnits = CCommon.ToLong(GetQueryStringVal("Unit"))
                    objItems.OppId = CCommon.ToLong(GetQueryStringVal("OppID"))
                    objItems.DivisionID = CCommon.ToLong(GetQueryStringVal("DivID"))
                    objItems.DomainID = Session("DomainID")
                    objItems.byteMode = CCommon.ToInteger(GetQueryStringVal("OppType"))
                    objItems.Amount = CCommon.ToDecimal(GetQueryStringVal("CalPrice"))
                    objItems.WareHouseItemID = CCommon.ToLong(GetQueryStringVal("WarehouseItemID"))
                    objItems.mode = True
                    Dim dtItemPricemgmt As DataTable
                    'Dim dtCalPrice As DataTable
                    'dtCalPrice = objPbook.GetPriceBasedonPriceBook

                    'If dtCalPrice.Rows.Count > 0 And dtItemPricemgmt.Rows.Count > 0 Then
                    '    dtItemPricemgmt.Rows(0).Item("ListPrice") = dtCalPrice.Rows(0).Item("DiscItemPrice")
                    'End If

                    Dim ds As DataSet
                    ds = objItems.GetPriceManagementList1
                    dtItemPricemgmt = ds.Tables(0)
                    If dtItemPricemgmt.Rows.Count = 0 Then
                        dtItemPricemgmt = ds.Tables(2)
                    End If

                    dtItemPricemgmt.Merge(ds.Tables(1))

                    If objItems.byteMode = 1 Then

                    End If
                    dgItemPricemgmt.DataSource = dtItemPricemgmt
                    dgItemPricemgmt.DataBind()
                End If
                btnClose.Attributes.Add("onclick", "return Close();")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Function ReturnName(ByVal SDate) As String
            Try
                Dim strDate As String = ""
                If Not IsDBNull(SDate) Then
                    strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                    If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                        strDate = "<font color=red>" & strDate & "</font>"
                    ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                        strDate = "<font color=orange>" & strDate & "</font>"
                    End If
                End If
                Return strDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub dgItemPricemgmt_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgItemPricemgmt.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim lbl As Label
                    lbl = e.Item.FindControl("lblLstPrice")
                    Dim btnAdd As Button
                    btnAdd = e.Item.FindControl("btnAdd")
                    If GetQueryStringVal( "Type") = "Edit" Then
                        btnAdd.Attributes.Add("onclick", "return AccessParent('" & String.Format("{0:#,###.00}", CCommon.ToLong(lbl.Text)) & "','2','" & IIf(CCommon.ToInteger(GetQueryStringVal("OptItem")) = 1, "txtOptPrice", "txtPrice") & "')")
                    Else : btnAdd.Attributes.Add("onclick", "return AccessParent('" & String.Format("{0:#,###.00}", CCommon.ToLong(lbl.Text)) & "','1','" & IIf(CCommon.ToInteger(GetQueryStringVal("OptItem")) = 1, "txtOptPrice", "txtprice") & "')")
                    End If

                    If DataBinder.Eval(e.Item.DataItem, "numPricRuleId") > 0 AndAlso DataBinder.Eval(e.Item.DataItem, "tintPricingMethod") = 1 Then
                        e.Item.Cells(2).Text = DataBinder.Eval(e.Item.DataItem, "OppStatus") & " ( <a href='#' onclick='return PriceTable(" & DataBinder.Eval(e.Item.DataItem, "numPricRuleId") & ")'>Price Table</a>)"
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace
