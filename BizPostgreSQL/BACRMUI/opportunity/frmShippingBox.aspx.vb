﻿Imports BACRM.BusinessLogic.Common
Imports Microsoft.Web.UI.WebControls
Imports BACRM.BusinessLogic.Opportunities
Imports System.Text
Imports BACRM.BusinessLogic.ShioppingCart
Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Contacts
Imports System.IO

Partial Public Class frmShippingBox
    Inherits BACRMPage

#Region "Object Declaration"

    Dim objShipping As New Shipping
    Dim objShippingRule As New ShippingRule
    Dim objOppBizDocs As New OppBizDocs

#End Region

#Region "Global Declaration"

    Dim lngOppBizDocID As Long
    Dim lngOppID As Long
    Dim lngShippingCompany As Long
    Dim lngShippingService As Long
    Dim str As New StringBuilder
    Dim blnMode As Boolean = True
    Dim IsDomestic As Boolean = True
    Dim lngShippingReportId As Long = 0

#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DomainID = Session("DomainID")
            UserCntID = Session("UserContactID")

            lngOppID = CCommon.ToLong(GetQueryStringVal("OppId"))
            lngOppBizDocID = CCommon.ToLong(GetQueryStringVal("OppBizDocId"))

            lngShippingCompany = If(If(CCommon.ToLong(GetQueryStringVal("ShipCompID")) = 0, Session("DefaultShippingCompany"), CCommon.ToLong(GetQueryStringVal("ShipCompID"))) = 0, 91, If(CCommon.ToLong(GetQueryStringVal("ShipCompID")) = 0, Session("DefaultShippingCompany"), CCommon.ToLong(GetQueryStringVal("ShipCompID"))))
            hdnShippingCompanyID.Value = lngShippingCompany

            lngShippingReportId = CCommon.ToLong(GetQueryStringVal("ShipReportId"))
            hdnShippingReportID.Value = lngShippingReportId

            lngShippingService = CCommon.ToLong(GetQueryStringVal("ShipServiceID"))

            If Not IsPostBack Then
                BindPackageType()
                BindData()

                If lngShippingReportId > 0 Then
                    LoadBoxesWithItems()

                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

#Region "Methods"

    Private Sub BindData(Optional ByVal dtBox As DataTable = Nothing)
        Try
            If lngShippingReportId = 0 Then
                Dim dtAvailableItems As DataTable
                If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                Dim ds As DataSet
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.OppBizDocId = lngOppBizDocID
                objOppBizDocs.OppId = lngOppID
                ds = objOppBizDocs.GetBizDocInventoryItems
                dtAvailableItems = ds.Tables(0)
                Dim ContainerStr As StringBuilder
                If Not dtBox Is Nothing Then
                    'ds.Tables.Add(dtBox)

                Else
                    If dtAvailableItems.Rows.Count = 0 Then
                        litMessage.Text = "No inventory items found in selected bizdoc."
                        btnSave.Visible = False
                        Exit Sub
                    End If

                    'Generate Array of items with its dimension
                    Dim sb As New System.Text.StringBuilder
                    Dim iRowIndex As Integer = 0
                    sb.Append(vbCrLf & "var arrItemDim = new Array();" & vbCrLf)     'Create a array to hold the field information in javascript
                    For Each dr As DataRow In dtAvailableItems.Rows
                        sb.Append("arrItemDim[" & iRowIndex & "] = new ItemDimension('" & dr("numOppBizDocItemID").ToString & "','" & dr("fltHeight").ToString & "', '" & dr("fltWidth").ToString() & "','" & dr("fltlength").ToString() & "','" & dr("fltweight").ToString() & "','" & CCommon.ToInteger(dr("numUnitHour")).ToString() & "');" & vbCrLf)
                        iRowIndex += 1
                    Next
                    If Not ClientScript.IsStartupScriptRegistered("ItemDim") Then ClientScript.RegisterStartupScript(Me.GetType, "ItemDim", sb.ToString(), True)

                    ContainerStr = New StringBuilder()
                    Dim strItemIntoContainer As New StringBuilder()
                    Dim arrContainers As DataRow() = dtAvailableItems.DefaultView.ToTable(True, "numContainer", "numNoItemIntoContainer").Select("numContainer > 0")

                    If Not arrContainers Is Nothing AndAlso arrContainers.Length > 0 Then
                        For Each drContainer In arrContainers
                            Dim containerQty As Double = CCommon.ToDouble(drContainer("numNoItemIntoContainer"))
                            Dim drItemsWithContainer As DataRow() = dtAvailableItems.Select("bitContainer=0 AND numContainer=" & drContainer("numContainer") & " AND numNoItemIntoContainer=" & drContainer("numNoItemIntoContainer"))

                            If Not drItemsWithContainer Is Nothing AndAlso drItemsWithContainer.Length > 0 Then
                                For Each drItem In drItemsWithContainer
                                    Dim drContainerItem As DataRow() = dtAvailableItems.Select("bitContainer=1 AND numItemCode=" & drContainer("numContainer") & " AND numUnitHour > 0")

                                    If Not drContainerItem Is Nothing AndAlso drContainerItem.Length > 0 Then
                                        Do While (CCommon.ToDouble(drItem("numUnitHour")) > 0 AndAlso Not drContainerItem Is Nothing AndAlso drContainerItem.Length > 0)
                                            If containerQty = CCommon.ToDouble(drContainer("numNoItemIntoContainer")) Then
                                                hdnNoOfBox.Value = Convert.ToInt32(hdnNoOfBox.Value) + 1
                                                strItemIntoContainer.Append("<div class='box' id='Box" & hdnNoOfBox.Value & "'><div class='boxheader'>Box" & hdnNoOfBox.Value & ":" & CCommon.ToString(drContainerItem(0)("vcItemName")) & " <div class='delete'></div> <div class='dimension'><span class='BoxWeight'>" & Convert.ToString(drContainerItem(0)("fltWeight")) & "</span> L<input type='text' id='length' value=" & Convert.ToString(drContainerItem(0)("fltLength")) & " class='positive' style='width:30px'> x W<input type='text' id='width' value=" & Convert.ToString(drContainerItem(0)("fltWidth")) & " class='positive' style='width:30px'> x H<input type='text' id='height' value=" & Convert.ToString(drContainerItem(0)("fltHeight")) & " class='positive' style='width:30px'>  </div> </div> <ul class='ui-sortable boxitems'>")
                                                
                                                If CCommon.ToDouble(drItem("numUnitHour")) >= containerQty Then
                                                    strItemIntoContainer.Append("<li itemid='" & Convert.ToString(drItem("numOppBizDocItemID")) & "' class='ui-draggable' style='display: list-item;'>" & drItem("vcItemName") & "<div class='deleteItem'></div><div class='AddedItemQty'>" & Convert.ToString(containerQty) & "</div></li>")
                                                    drItem("numUnitHour") = CCommon.ToDouble(drItem("numUnitHour")) - containerQty
                                                    containerQty = CCommon.ToDouble(drContainer("numNoItemIntoContainer"))
                                                Else
                                                    strItemIntoContainer.Append("<li itemid='" & Convert.ToString(drItem("numOppBizDocItemID")) & "' class='ui-draggable' style='display: list-item;'>" & drItem("vcItemName") & "<div class='deleteItem'></div><div class='AddedItemQty'>" & Convert.ToString(drItem("numUnitHour")) & "</div></li>")
                                                    containerQty = containerQty - Convert.ToDouble(drItem("numUnitHour"))
                                                    drItem("numUnitHour") = 0
                                                End If
                                            Else
                                                If CCommon.ToDouble(drItem("numUnitHour")) >= containerQty Then
                                                    strItemIntoContainer.Append("<li itemid='" & Convert.ToString(drItem("numOppBizDocItemID")) & "' class='ui-draggable' style='display: list-item;'>" & drItem("vcItemName") & "<div class='deleteItem'></div><div class='AddedItemQty'>" & Convert.ToString(containerQty) & "</div></li>")
                                                    drItem("numUnitHour") = CCommon.ToDouble(drItem("numUnitHour")) - containerQty
                                                    containerQty = CCommon.ToDouble(drContainer("numNoItemIntoContainer"))
                                                Else
                                                    strItemIntoContainer.Append("<li itemid='" & Convert.ToString(drItem("numOppBizDocItemID")) & "' class='ui-draggable' style='display: list-item;'>" & drItem("vcItemName") & "<div class='deleteItem'></div><div class='AddedItemQty'>" & Convert.ToString(drItem("numUnitHour")) & "</div></li>")
                                                    containerQty = containerQty - Convert.ToDouble(drItem("numUnitHour"))
                                                    drItem("numUnitHour") = 0
                                                End If
                                            End If

                                            If containerQty = CCommon.ToDouble(drContainer("numNoItemIntoContainer")) Then
                                                strItemIntoContainer.Append("</ul></div>")
                                                drContainerItem(0)("numUnitHour") = CCommon.ToDouble(drContainerItem(0)("numUnitHour")) - 1
                                            End If

                                            drContainerItem = dtAvailableItems.Select("bitContainer=1 AND numItemCode=" & drContainer("numContainer") & " AND numUnitHour > 0")
                                        Loop
                                    End If
                                Next
                            End If

                            If Not strItemIntoContainer.ToString().EndsWith("</ul></div>") Then
                                strItemIntoContainer = strItemIntoContainer.Append("</ul></div>")
                            End If
                        Next

                        If Not strItemIntoContainer.ToString().EndsWith("</ul></div>") Then
                            strItemIntoContainer = strItemIntoContainer.Append("</ul></div>")
                        End If

                        BoxHolder.InnerHtml += Convert.ToString(strItemIntoContainer)
                        ClientScript.RegisterClientScriptBlock(Me.GetType, "Box", "CreateBox(0)", True)
                    End If

                    str.Append("<ul id='AvailItems'>")
                    For Each dr As DataRow In dtAvailableItems.Rows
                        str.Append("<li itemid='" + dr("numOppBizDocItemID").ToString + "' >" + dr("vcItemName").ToString + "<input class='itemQty' type='text' value='" + CCommon.ToInteger(dr("numUnitHour")).ToString() + "' ></input> </li>")
                    Next
                    str.Append("</ul>")
                    litContent.Text = str.ToString()
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetOrderItemShippingRules(ByVal OppId As Long, _
                                                   ByVal OppBizDocID As Long, _
                                                   ByVal intDomainId As Integer) As DataTable
        Dim dtResult As DataTable = Nothing
        Try
            Dim dsItems As New DataSet
            Dim dtOrderDetail As New DataTable
            objOppBizDocs = New OppBizDocs

            objOppBizDocs.OppId = OppId
            objOppBizDocs.OppBizDocId = OppBizDocID
            objOppBizDocs.DomainID = DomainID

            dsItems = objOppBizDocs.GetOppInItems()
            'Tracing Part
            '------------------------------------------------------------------------------------------------------
            Trace.Write("Start Tracing Items detail.")
            Trace.Write("Is Item Dataset is initialized or not ? : " & IIf(dsItems Is Nothing, "NO", "YES"))
            Trace.Write("Is Item Dataset has tables or not? : " & IIf(dsItems.Tables.Count > 0, dsItems.Tables.Count, 0))
            Trace.Write("End Tracing Items detail.")

            '------------------------------------------------------------------------------------------------------

            If dsItems IsNot Nothing AndAlso dsItems.Tables.Count > 0 AndAlso dsItems.Tables(0).Rows.Count > 0 Then
                Dim dvItems As DataView = dsItems.Tables(0).Copy.DefaultView
                dvItems.RowFilter = "vcItemType = 'Inventory'"
                dsItems.Tables.Clear()
                dsItems.Tables.Add(dvItems.ToTable())
            End If

            If dsItems IsNot Nothing AndAlso dsItems.Tables.Count > 0 AndAlso dsItems.Tables(0).Rows.Count > 0 Then

                dtOrderDetail.Columns.Add("OppId", GetType(Long))
                dtOrderDetail.Columns.Add("OppBizDocItemID", GetType(Long))
                dtOrderDetail.Columns.Add("ItemCode", GetType(Long))
                dtOrderDetail.Columns.Add("ShippingRuleID", GetType(Long))
                dtOrderDetail.Columns.Add("numShippingCompanyID1", GetType(Long))
                dtOrderDetail.Columns.Add("numShippingCompanyID2", GetType(Long))
                dtOrderDetail.Columns.Add("DomesticServiceID", GetType(Integer))
                dtOrderDetail.Columns.Add("InternationalServiceID", GetType(Integer))
                dtOrderDetail.Columns.Add("PackageTypeID", GetType(Integer))
                dtOrderDetail.Columns.Add("numShipClass", GetType(Long))
                dtOrderDetail.Columns.Add("numUnitHour", GetType(Long))
                dtOrderDetail.Columns.Add("fltWeight", GetType(Double))
                dtOrderDetail.Columns.Add("fltWidth", GetType(Double))
                dtOrderDetail.Columns.Add("fltHeight", GetType(Double))
                dtOrderDetail.Columns.Add("fltLength", GetType(Double))

                Trace.Write("Start Tracing Rules detail.")
                ' GET SHIPPING RULES AS PER ITEM'S NEEDS
                Dim dtShipInfo As New DataTable
                For Each drItem As DataRow In dsItems.Tables(0).Rows
                    objOppBizDocs.OppId = lngOppID
                    objOppBizDocs.OppBizDocId = OppBizDocID
                    objOppBizDocs.BizDocItemId = CCommon.ToLong(drItem("OppBizDocItemID"))
                    objOppBizDocs.ShipClassID = CCommon.ToLong(drItem("numShipClass"))
                    objOppBizDocs.UnitHour = CCommon.ToLong(drItem("numUnitHour"))
                    dtShipInfo = objOppBizDocs.GetShippingRuleInfo()

                    Trace.Write("Shipping Rule dataset available or not: " & If(dtShipInfo Is Nothing, "NO", "YES, Then its row count is :" & dtShipInfo.Rows.Count))

                    If dtShipInfo IsNot Nothing AndAlso dtShipInfo.Rows.Count > 0 Then
                        Dim drOrderDetail As DataRow
                        drOrderDetail = dtOrderDetail.NewRow

                        drOrderDetail("OppId") = lngOppID
                        drOrderDetail("OppBizDocItemID") = CCommon.ToLong(drItem("OppBizDocItemID"))
                        drOrderDetail("ItemCode") = CCommon.ToLong(drItem("ItemCode"))
                        drOrderDetail("ShippingRuleID") = CCommon.ToLong(dtShipInfo.Rows(0)("numShippingRuleID"))
                        drOrderDetail("numShippingCompanyID1") = CCommon.ToInteger(dtShipInfo.Rows(0)("numShippingCompanyID1"))
                        drOrderDetail("numShippingCompanyID2") = CCommon.ToInteger(dtShipInfo.Rows(0)("numShippingCompanyID2"))

                        drOrderDetail("DomesticServiceID") = CCommon.ToInteger(dtShipInfo.Rows(0)("numDomesticShipID"))
                        drOrderDetail("InternationalServiceID") = CCommon.ToInteger(dtShipInfo.Rows(0)("numInternationalShipID"))
                        drOrderDetail("PackageTypeID") = CCommon.ToInteger(dtShipInfo.Rows(0)("numPackageTypeID"))
                        drOrderDetail("numShipClass") = CCommon.ToLong(drItem("numShipClass"))
                        drOrderDetail("numUnitHour") = CCommon.ToLong(drItem("numUnitHour"))

                        drOrderDetail("fltWeight") = CCommon.ToDouble(drItem("fltWeight"))
                        drOrderDetail("fltWidth") = CCommon.ToDouble(drItem("fltWidth"))
                        drOrderDetail("fltHeight") = CCommon.ToDouble(drItem("fltHeight"))
                        drOrderDetail("fltLength") = CCommon.ToDouble(drItem("fltLength"))

                        dtOrderDetail.Rows.Add(drOrderDetail)
                        dtOrderDetail.AcceptChanges()
                    Else
                        dtOrderDetail = Nothing
                    End If

                Next

            Else
                dtResult = Nothing
            End If

            Trace.Write("End Tracing Rules Detail.")

            If dtOrderDetail IsNot Nothing Then
                Dim dtView As DataView = dtOrderDetail.DefaultView
                dtView.Sort = "numShipClass"
                dtResult = dtView.ToTable()
            Else
                dtResult = Nothing
            End If

        Catch ex As Exception
            dtResult = Nothing
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            'Response.Write(ex)
        End Try

        Return dtResult
    End Function

    Private Sub BindPackageType()
        Try
            Dim dsPackages As DataSet = Nothing
            dsPackages = OppBizDocs.LoadAllPackages(Session("DomainID"))
            If dsPackages IsNot Nothing AndAlso dsPackages.Tables.Count > 0 AndAlso dsPackages.Tables(0).Rows.Count > 0 Then

                ddlPackageType.DataSource = dsPackages
                ddlPackageType.DataValueField = "ValueDimension"
                ddlPackageType.DataTextField = "vcPackageName"
                ddlPackageType.DataBind()

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub Save()
        Try
            If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
            Dim ds As New DataSet
            'Create Blank shipping report and use Shipreportid in reference of box
            If lngOppBizDocID > 0 And lngOppID > 0 And lngShippingCompany > 0 Then
                With objOppBizDocs
                    .OppId = lngOppID
                    .DomainID = Session("DomainID")
                    .OppBizDocId = lngOppBizDocID
                    .ShipCompany = lngShippingCompany
                    .UserCntID = Session("UserContactID")
                    ds.Tables.Add(New DataTable())
                    .strText = ds.GetXml()
                    .ShippingReportId = .ManageShippingReport
                    hdnShippingReportID.Value = .ShippingReportId
                End With

            End If

            ds = New DataSet
            Dim dtBox As New DataTable("Box")
            Dim dtItem As New DataTable("Item")
            dtBox.Columns.Add("vcBoxName")
            dtBox.Columns.Add("fltTotalWeight")
            dtBox.Columns.Add("fltHeight")
            dtBox.Columns.Add("fltWidth")
            dtBox.Columns.Add("fltLength")
            dtBox.Columns.Add("fltDimensionalWeight")
            dtBox.Columns.Add("numPackageTypeID")
            dtBox.Columns.Add("numShipCompany")
            dtBox.Columns.Add("numServiceTypeID")
            dtItem.Columns.Add("vcBoxName")
            dtItem.Columns.Add("numOppBizDocItemID")
            dtItem.Columns.Add("intBoxQty")

            Dim strBox() As String = hdnValues.Value.Split(New String() {","}, System.StringSplitOptions.RemoveEmptyEntries)

            Dim dr As DataRow
            For i As Integer = 0 To strBox.Length - 1

                Dim strItem() As String = strBox(i).Split(New String() {":"}, StringSplitOptions.None)
                If strItem.Length >= 7 Then
                    dr = dtBox.NewRow
                    dr("vcBoxName") = strItem(0) '"Box" & (i + 1).ToString
                    dr("fltTotalWeight") = CCommon.ToDecimal(0)
                    dr("fltLength") = CCommon.ToDecimal(strItem(1))
                    dr("fltWidth") = CCommon.ToDecimal(strItem(2))
                    dr("fltHeight") = CCommon.ToDecimal(strItem(3))
                    If CCommon.ToDecimal(strItem(5)) = 0 Then
                        dr("fltDimensionalWeight") = (If(CCommon.ToDecimal(strItem(1)) = 0, 1, CCommon.ToDecimal(strItem(1))) * _
                        If(CCommon.ToDecimal(strItem(2)) = 0, 1, CCommon.ToDecimal(strItem(2))) * _
                        If(CCommon.ToDecimal(strItem(3)) = 0, 1, CCommon.ToDecimal(strItem(3)))) / 166
                    Else
                        dr("fltDimensionalWeight") = CCommon.ToDecimal(strItem(5))
                    End If


                    dr("numPackageTypeID") = CCommon.ToInteger(strItem(4))
                    dr("numShipCompany") = lngShippingCompany
                    dr("numServiceTypeID") = lngShippingService

                    dtBox.Rows.Add(dr)

                    'Add Items 
                    For j As Integer = 6 To strItem.Length - 1
                        dr = dtItem.NewRow
                        dr("vcBoxName") = strItem(0)
                        If strItem(j).Split("~").Length > 0 AndAlso strItem(j) <> "" Then
                            dr("intBoxQty") = IIf(strItem(j).Split("~").Length > 1, strItem(j).Split("~")(0), 0)
                            dr("numOppBizDocItemID") = IIf(strItem(j).Split("~").Length > 1, strItem(j).Split("~")(1), 0)

                            dtItem.Rows.Add(dr)
                        End If
                    Next
                End If
            Next

            ds.Tables.Add(dtBox)
            ds.Tables.Add(dtItem)

            With objOppBizDocs
                .ShippingReportId = CCommon.ToLong(hdnShippingReportID.Value)
                .strText = ds.GetXml()
                .UserCntID = Session("UserContactID")
                .byteMode = 0
                .ManageShippingBox()
            End With

            If dtBox.Select("numShipCompany=" & 91).Count > 0 Then
                If dtBox.Select("numShipCompany=" & 91).Count = dtBox.Rows.Count Then
                    blnMode = False
                End If
            End If

            If dtBox.Select("numShipCompany=" & 88).Count > 0 Then
                If dtBox.Select("numShipCompany=" & 88).Count = dtBox.Rows.Count Then
                    blnMode = False
                End If
            End If


        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadBoxesWithItems()
        Try
            Dim dsBoxes As New DataSet
            Dim lngTotalQty As Long = 0
            If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
            objOppBizDocs.ShippingReportId = lngShippingReportId
            dsBoxes = objOppBizDocs.GetShippingBoxes()

            If dsBoxes IsNot Nothing AndAlso dsBoxes.Tables.Count > 0 Then
                Dim dtBox As DataTable = dsBoxes.Tables(0)
                Dim dtBoxItems As DataTable = dsBoxes.Tables(1)

                dtBox.TableName = "Box"
                dtBoxItems.TableName = "BoxItems"
                lngTotalQty = CCommon.ToLong(dtBoxItems.Compute("SUM(intBoxQty)", "1=1"))

                Dim strBoxHTML As String
                strBoxHTML = Sites.ReadFile(Server.MapPath("~/Opportunity/ShippingTemplates/ShippingBox.htm"))

                Dim strBoxItemsHTML As String
                strBoxItemsHTML = " <li itemid='{0}' class='ui-draggable ui-droppable' style='display: list-item;'>{1}<div class='deleteItem'></div><div class='AddedItemQty'>{2}</div></li>"
                'Sites.ReadFile(Server.MapPath("~/Opportunity/ShippingTemplates/ShippingBoxItems.htm"))

                Dim strMain As String = ""
                Dim strTempItems As String = ""
                Dim strAppendItem As String
                For Each drBox As DataRow In dtBox.Rows

                    Dim drBoxItems As DataRow() = dtBoxItems.Select("numBoxID = " & CCommon.ToLong(drBox("numBoxID")))
                    If drBoxItems IsNot Nothing AndAlso drBoxItems.Count > 0 Then

                        strAppendItem = ""
                        strAppendItem = "<ul class='ui-sortable boxitems'>"
                        strTempItems = strBoxItemsHTML

                        For Each dr In drBoxItems
                            'Format Fields Item Template
                            '{0}- $$ItemID$$
                            '{1}- $$ItemName$$
                            '{2}- $$ItemQuantity$$

                            strAppendItem = strAppendItem & String.Format(strTempItems, _
                                                         CCommon.ToLong(dr("numOppBizDocItemID")), _
                                                         CCommon.ToString(dr("vcItemName")), _
                                                         CCommon.ToInteger(dr("intBoxQty")))
                        Next
                        strAppendItem = strAppendItem + "</ul>"
                    End If

                    'Format Fields : Box Template
                    '{0}- $$BoxID$$ - same as box name, '{1}- $$BoxName$$, '{2}- $$BoxWeight$$, '{3}- $$length$$, '{4}- $$width$$, '{5}- $$height$$, '{6}- $$Items$$

                    strMain = strMain & String.Format(strBoxHTML, _
                                                      CCommon.ToString(drBox("vcBoxName")), _
                                                      CCommon.ToString(drBox("vcBoxName")), _
                                                      CCommon.ToDecimal(drBox("fltTotalWeight")), _
                                                      CCommon.ToDecimal(drBox("fltLength")), _
                                                      CCommon.ToDecimal(drBox("fltWidth")), _
                                                      CCommon.ToDecimal(drBox("fltHeight")), _
                                                      strAppendItem)

                Next
                BoxHolder.InnerHtml = ""
                BoxHolder.InnerHtml = strMain
                'ClientScript.RegisterStartupScript(Me.GetType, "Box", "$('.BoxHolder').html('" & strMain & "')", True)

                Dim dtAvailableItems As DataTable
                If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                Dim ds As DataSet
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.OppBizDocId = lngOppBizDocID
                objOppBizDocs.OppId = lngOppID
                ds = objOppBizDocs.GetBizDocInventoryItems
                dtAvailableItems = ds.Tables(0)

                If dtAvailableItems.Rows.Count = 0 Then
                    litMessage.Text = "No inventory items found in selected bizdoc."
                    btnSave.Visible = False
                    Exit Sub
                End If

                str = New StringBuilder()
                str.Append("<ul id='AvailItems'>")
                For Each dr As DataRow In dtAvailableItems.Rows
                    If dr("bitContainer") = True Then
                        dr("numUnitHour") = 0
                        lngTotalQty = 0
                    Else
                        lngTotalQty = CCommon.ToLong(dtBoxItems.Compute("SUM(intBoxQty)", "numItemCode=" + Convert.ToString(dr("numItemCode")) + ""))
                    End If
                    str.Append("<li itemid='" + dr("numOppBizDocItemID").ToString + "' >" + dr("vcItemName").ToString + _
                               "<input class='itemQty' type='text' value='" + (CCommon.ToLong(dr("numUnitHour")) - lngTotalQty).ToString() + "' ></input> </li>")
                Next
                str.Append("</ul>")
                litContent.Text = str.ToString()

                'Generate Array of items with its dimension
                Dim sb As New System.Text.StringBuilder
                Dim iRowIndex As Integer = 0
                sb.Append(vbCrLf & "var arrItemDim = new Array();" & vbCrLf)     'Create a array to hold the field information in javascript
                For Each dr As DataRow In dtAvailableItems.Rows
                    sb.Append("arrItemDim[" & iRowIndex & "] = new ItemDimension('" & dr("numOppBizDocItemID").ToString & "','" & dr("fltHeight").ToString & "', '" & dr("fltWidth").ToString() & "','" & dr("fltlength").ToString() & "','" & dr("fltweight").ToString() & "','" & CCommon.ToLong(dr("numUnitHour")).ToString() & "');" & vbCrLf)
                    iRowIndex += 1
                Next

                If Not ClientScript.IsStartupScriptRegistered("ItemDim") Then ClientScript.RegisterStartupScript(Me.GetType, "ItemDim", sb.ToString(), True)

            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
#End Region

#Region "Button Events"

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Save()
            If blnMode = False Then
                Response.Redirect("frmGenerateShippingLabel.aspx?mode=0&ShipReportId=" & hdnShippingReportID.Value & "&OppId=" & lngOppID.ToString & "&OppBizDocId=" & lngOppBizDocID.ToString & "&ShipCompID=" & lngShippingCompany.ToString & "&ShipCompName=" & GetQueryStringVal("ShipCompName"), False)
            Else
                Response.Redirect("frmGenerateShippingLabel.aspx?mode=1&ShipReportId=" & hdnShippingReportID.Value & "&OppId=" & lngOppID.ToString & "&OppBizDocId=" & lngOppBizDocID.ToString & "&ShipCompID=" & lngShippingCompany.ToString & "&ShipCompName=" & GetQueryStringVal("ShipCompName"), False)
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(sender As Object, e As EventArgs) Handles btnSaveClose.Click
        Try
            Save()
            ClientScript.RegisterClientScriptBlock(Me.GetType, "CloseWindow", "this.close();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnAutoSelect_Click(sender As Object, e As EventArgs) Handles btnAutoSelect.Click
        Try
            'Dim objShipping As New Shipping
            'objShipping.GenerateShippingLabel(53501, 61782, 913, 1, 1)
            GenerateShippingBoxes(IsGenerateShippingLabel:=False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnGenerateShippingLabel_Click(sender As Object, e As EventArgs) Handles btnGenerateShippingLabel.Click
        Try
            GenerateShippingBoxes(IsGenerateShippingLabel:=True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

#Region "Shipping Label Generation Method"

    Dim lngBizDocItemId As Long = 0
    Dim specialService As Integer = 0

    Public Sub GenerateShippingBoxes(Optional ByVal IsGenerateShippingLabel As Boolean = False)
        Dim strErrorOppID As String = ""
        Dim strSuccessOppID As String = ""

        litMessage.Text = ""
        Try
            Dim dblTotalWeight As Double = 0
            Dim lngShippingReportID As Long = 0
            'lngOppBizDocID = lngOppBizDocID
            lngBizDocItemId = 0

            objOppBizDocs.OppId = lngOppID
            objOppBizDocs.BizDocId = lngOppBizDocID
            objOppBizDocs.BizDocItemId = lngBizDocItemId
            objOppBizDocs.ItemClassID = 0
            objOppBizDocs.DomainID = DomainID

            If objShipping Is Nothing Then objShipping = New Shipping()
            Dim ds As New DataSet
            Dim dsFinal As New DataSet
            Dim intBoxCounter As Integer = 0
            Dim blnSameCountry As Boolean = False

            Dim dtBox As New DataTable("Box")
            dtBox.Columns.Add("vcBoxName")
            dtBox.Columns.Add("fltTotalWeight")
            dtBox.Columns.Add("fltHeight")
            dtBox.Columns.Add("fltWidth")
            dtBox.Columns.Add("fltLength")
            dtBox.Columns.Add("numPackageTypeID")
            dtBox.Columns.Add("numServiceTypeID")
            dtBox.Columns.Add("fltDimensionalWeight")
            dtBox.Columns.Add("numShipCompany")

            Dim dtItem As New DataTable("Item")
            dtItem.Columns.Add("vcBoxName")
            dtItem.Columns.Add("numOppBizDocItemID")
            dtItem.Columns.Add("intBoxQty")
            dtItem.Columns.Add("UnitWeight")

            Dim dsItems As New DataSet
            Dim dtItems As New DataTable
            Dim dtShipDetail As DataTable = GetOrderItemShippingRules(lngOppID, lngOppBizDocID, DomainID)
            If dtShipDetail Is Nothing Then
                If strErrorOppID.Contains(lngOppID) = False Then strErrorOppID = lngOppID & "," & strErrorOppID
                litMessage.Text = litMessage.Text & "Order ID " & lngOppID & " : " & vbCrLf & " Shipping Rule has not been found." & "<br/>"
                Exit Sub
                'Continue For
            End If

            dsItems.Tables.Add(dtShipDetail.Copy)
            If dsItems IsNot Nothing AndAlso dsItems.Tables.Count > 0 AndAlso dsItems.Tables(0).Rows.Count > 0 Then

                If lngOppID > 0 AndAlso lngOppBizDocID > 0 Then
                    dtItems = dsItems.Tables(0).Clone
                    Dim objOpportunity As New MOpportunity
                    'Dim dtDistinct As DataTable = dsItems.Tables(0).DefaultView.ToTable(True, "numShipClass")
                    For i As Integer = 0 To dsItems.Tables(0).Rows.Count - 1

                        objOpportunity.OpportunityId = 0
                        objOpportunity.DomainID = Session("DomainID")
                        objOpportunity.ItemCode = CCommon.ToLong(dsItems.Tables(0).Rows(i)("ItemCode"))
                        If objOpportunity.CheckServiceItemInOrder() = True Then
                            Continue For
                        End If
                        Dim objOpp As New MOpportunity()
                        Dim objCommon As New CCommon()
                        objOpp.OpportunityId = lngOppID
                        objOpp.Mode = 1
                        Dim dtTable As DataTable = Nothing
                        dtTable = objOpp.GetOpportunityAddress()

                        Dim dtShipFrom As DataTable = Nothing
                        Dim objContacts As New CContacts()
                        objContacts.ContactID = UserCntID
                        objContacts.DomainID = DomainID
                        dtShipFrom = objContacts.GetBillOrgorContAdd()
                        If objCommon Is Nothing Then
                            objCommon = New CCommon()
                        End If

                        ' GET SHIPPING COMPANY INFORMATION
                        Dim dtShipCompany As DataTable = Nothing
                        objOppBizDocs.OppId = lngOppID
                        objOppBizDocs.ShipClassID = CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShipClass"))
                        objOppBizDocs.BizDocItemId = CCommon.ToLong(dsItems.Tables(0).Rows(i)("OppBizDocItemID"))

                        'dtShipCompany = objOppBizDocs.GetShippingRuleInfo()
                        'If dtShipCompany IsNot Nothing AndAlso dtShipCompany.Rows.Count > 0 Then

                        'IF Shipping Detail is not available continue process with another order detail
                        If dtTable.Rows.Count = 0 OrElse dtShipFrom.Rows.Count = 0 Then
                            Continue For
                        End If

                        If CCommon.ToLong(dtTable.Rows(0)("Country")) = CCommon.ToLong(dtShipFrom.Rows(0)("vcShipCountry")) Then
                            objOppBizDocs.ShipCompany = CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShippingCompanyID1"))
                            'objOppBizDocs.Value2 = CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID")) ' CCommon.ToString(dtShipCompany.Rows(0)("numDomesticShipID"))
                            objOppBizDocs.BoxServiceTypeID = CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID"))
                        Else
                            objOppBizDocs.ShipCompany = CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShippingCompanyID2"))
                            'objOppBizDocs.Value2 = CCommon.ToString(dsItems.Tables(0).Rows(i)("InternationalServiceID"))
                            objOppBizDocs.BoxServiceTypeID = CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID"))
                        End If
                        'End If

                        'Get Order Items and Process Shipping Label Generating Logic
                        If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs

                        objOppBizDocs.ShippingReportId = 0
                        objOppBizDocs.DomainID = DomainID
                        objOppBizDocs.OppBizDocId = lngOppBizDocID

                        ' FROM DETAIL
                        objOppBizDocs.FromName = CCommon.ToString(dtShipFrom.Rows(0)("vcFirstname")) & " " & CCommon.ToString(dtShipFrom.Rows(0)("vcLastname"))
                        objOppBizDocs.FromCompany = CCommon.ToString(dtShipFrom.Rows(0)("vcCompanyName"))
                        objOppBizDocs.FromPhone = CCommon.ToString(dtShipFrom.Rows(0)("vcPhone"))
                        If dtShipFrom.Rows(0)("vcShipStreet").ToString().Contains(Environment.NewLine) Then
                            Dim strAddressLines As String() = dtShipFrom.Rows(0)("vcShipStreet").ToString().Split(System.Environment.NewLine) ' dtShipFrom.Rows(0)("vcShipStreet").ToString().Split(New Char() {Convert.ToChar(Environment.NewLine)}, StringSplitOptions.RemoveEmptyEntries)
                            objOppBizDocs.FromAddressLine1 = (If(strAddressLines.Length >= 0, strAddressLines(0).ToString(), "")).ToString()
                            objOppBizDocs.FromAddressLine2 = (If(strAddressLines.Length >= 1, strAddressLines(1).ToString(), "")).ToString()
                        Else
                            objOppBizDocs.FromAddressLine1 = CCommon.ToString(dtShipFrom.Rows(0)("vcShipStreet"))
                            objOppBizDocs.FromAddressLine2 = ""
                        End If

                        'objOppBizDocs.FromAddressLine1 = dtShipFrom.Rows[0]["vcShipStreet"].ToString().Substring(dtShipFrom.Rows[0]["vcShipStreet"].ToString().IndexOf(Environment.NewLine)).Replace(Environment.NewLine, " ");
                        'objOppBizDocs.FromAddressLine2 = dtShipFrom.Rows[0]["vcShipStreet"].ToString().Substring(0, dtShipFrom.Rows[0]["vcShipStreet"].ToString().IndexOf(Environment.NewLine));
                        objOppBizDocs.FromCountry = CCommon.ToString(dtShipFrom.Rows(0)("vcShipCountry"))
                        objOppBizDocs.FromState = CCommon.ToString(dtShipFrom.Rows(0)("vcShipState"))
                        objOppBizDocs.FromCity = CCommon.ToString(dtShipFrom.Rows(0)("vcShipCity"))
                        objOppBizDocs.FromZip = CCommon.ToString(dtShipFrom.Rows(0)("vcShipPostCode"))
                        objOppBizDocs.IsFromResidential = False

                        objOppBizDocs.UserCntID = UserCntID
                        ' Order Record Owner
                        objOppBizDocs.strText = Nothing

                        'TO DETAILS
                        objOppBizDocs.ToName = CCommon.ToString(dtTable.Rows(0)("Name"))
                        objOppBizDocs.ToCompany = CCommon.ToString(dtTable.Rows(0)("Company"))
                        objOppBizDocs.ToPhone = CCommon.ToString(dtTable.Rows(0)("Phone"))

                        If dtTable.Rows(0)("Street").ToString().Contains(Environment.NewLine) Then
                            Dim strAddressLines As String() = dtTable.Rows(0)("Street").ToString().Split(System.Environment.NewLine) 'dtTable.Rows(0)("Street").ToString().Split(New Char() {Convert.ToChar(Environment.NewLine)}, StringSplitOptions.RemoveEmptyEntries)
                            objOppBizDocs.ToAddressLine1 = (If(strAddressLines.Length >= 0, strAddressLines(0).ToString(), "")).ToString()
                            objOppBizDocs.ToAddressLine2 = (If(strAddressLines.Length >= 1, strAddressLines(1).ToString(), "")).ToString()
                        Else
                            objOppBizDocs.ToAddressLine1 = CCommon.ToString(dtTable.Rows(0)("Street"))
                            objOppBizDocs.ToAddressLine2 = ""
                        End If

                        objOppBizDocs.ToCity = CCommon.ToString(dtTable.Rows(0)("City"))
                        objOppBizDocs.ToState = CCommon.ToString(dtTable.Rows(0)("State"))
                        objOppBizDocs.ToCountry = CCommon.ToString(dtTable.Rows(0)("Country"))
                        objOppBizDocs.ToZip = CCommon.ToString(dtTable.Rows(0)("PostCode"))
                        objOppBizDocs.IsToResidential = False

                        ' SPECIAL SERVICES
                        objOppBizDocs.IsCOD = False
                        objOppBizDocs.IsDryIce = False
                        objOppBizDocs.IsHoldSaturday = False
                        objOppBizDocs.IsHomeDelivery = False
                        objOppBizDocs.IsInsideDelivery = False
                        objOppBizDocs.IsInsidePickup = False
                        objOppBizDocs.IsReturnShipment = False
                        objOppBizDocs.IsSaturdayDelivery = False
                        objOppBizDocs.IsSaturdayPickup = False
                        objOppBizDocs.CODAmount = 0
                        objOppBizDocs.CODType = ""
                        objOppBizDocs.IsAdditionalHandling = False
                        objOppBizDocs.LargePackage = False
                        objOppBizDocs.DeliveryConfirmation = ""
                        objOppBizDocs.Description = ""
                        objOppBizDocs.TotalInsuredValue = CCommon.ToDecimal(Session("TotalInsuredValue"))
                        objOppBizDocs.TotalCustomsValue = CCommon.ToDecimal(Session("TotalCustomsValue"))

                        'Setup IsDomestic as per given address
                        If CCommon.ToInteger(objOppBizDocs.FromCountry) = CCommon.ToInteger(objOppBizDocs.ToCountry) Then
                            IsDomestic = True
                        Else
                            IsDomestic = False
                        End If

                        Try
                            If lngShippingReportID <= 0 Then
                                lngShippingReportID = objOppBizDocs.ManageShippingReport()
                            End If

                        Catch ex As Exception
                            strErrorOppID = objOppBizDocs.OppId & "," & strErrorOppID
                            Continue For
                        End Try

                        'Update Shipping report address values and create shipping report items 
                        objShipping.DomainID = DomainID
                        objShipping.UserCntID = UserCntID
                        objShipping.ShippingReportId = lngShippingReportID
                        objShipping.OpportunityId = lngOppID
                        objShipping.OppBizDocId = objOppBizDocs.OppBizDocId
                        objShipping.InvoiceNo = CCommon.ToString(objOppBizDocs.OppBizDocId)

                        'If dtShipCompany IsNot Nothing AndAlso dtShipCompany.Rows.Count > 0 Then
                        objShipping.Provider = CCommon.ToInteger(dsItems.Tables(0).Rows(i)("numShippingCompanyID1"))
                        objShipping.PackagingType = CCommon.ToShort(dsItems.Tables(0).Rows(i)("PackageTypeID"))

                        If CCommon.ToLong(dtTable.Rows(0)("Country")) = CCommon.ToLong(dtShipFrom.Rows(0)("vcShipCountry")) Then
                            objShipping.ServiceType = CCommon.ToShort(dsItems.Tables(0).Rows(i)("DomesticServiceID"))
                            blnSameCountry = True
                        Else
                            objShipping.ServiceType = CCommon.ToShort(dsItems.Tables(0).Rows(i)("InternationalServiceID"))
                        End If
                        'End If

                        'FOR FOLLOWING SERVICE TYPES,IN FEDEX THERE IS ONLY ONE "YOUR PACKAGING" IS AVAILABLE
                        If objShipping.Provider = 91 AndAlso _
                           (objShipping.ServiceType = 15 OrElse objShipping.ServiceType = 16 OrElse _
                            objShipping.ServiceType = 17 OrElse objShipping.ServiceType = 18 OrElse _
                            objShipping.ServiceType = 19) Then

                            objShipping.PackagingType = 21

                        End If
                        '------------------------------------------------------------------------------------

                        objShipping.ImageWidth = CCommon.ToInteger(Session("ShippingImageWidth"))
                        objShipping.ImageHeight = CCommon.ToInteger(Session("ShippingImageHeight"))
                        objShipping.TotalInsuredValue = CCommon.ToDecimal(Session("TotalInsuredValue"))
                        objShipping.TotalCustomsValue = CCommon.ToDecimal(Session("TotalCustomsValue"))

                        Dim dtFields As New DataTable
                        If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                        'dtFields.Columns.Add("numItemCode")
                        dtFields.Columns.Add("numBoxID")
                        dtFields.Columns.Add("tintServiceType")
                        dtFields.Columns.Add("dtDeliveryDate")
                        dtFields.Columns.Add("monShippingRate")
                        dtFields.Columns.Add("fltTotalWeight")
                        dtFields.Columns.Add("intNoOfBox")
                        dtFields.Columns.Add("fltHeight")
                        dtFields.Columns.Add("fltWidth")
                        dtFields.Columns.Add("fltLength")
                        dtFields.Columns.Add("fltDimensionalWeight")
                        'dtFields.Columns.Add("numOppBizDocItemID")
                        dtFields.TableName = "Box"

                        Dim lngBoxID As Long = 0
                        Dim strBoxID As String = 0
                        Dim lngTotalQty As Long = 0
                        Dim dblWidth As Double = 0
                        Dim dblHeight As Double = 0
                        Dim dblLength As Double = 0
                        Dim dblWeight As Double = 0

                        Dim lngPackageTypeID As Long = 0
                        Dim lngServiceTypeID As Long = 0

                        If dtItems.Select("numShipClass = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShipClass")) & _
                                          " AND PackageTypeID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("PackageTypeID")) & _
                                          " AND DomesticServiceID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID")) & _
                                          " AND InternationalServiceID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("InternationalServiceID"))).Length > 0 Then
                            Continue For
                        Else
                            dtItems.Rows.Clear()
                        End If

                        lngTotalQty = 0

                        Dim drClass() As DataRow = dsItems.Tables(0).Select("numShipClass = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShipClass")) & _
                                                                            " AND PackageTypeID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("PackageTypeID")) & _
                                                                            " AND DomesticServiceID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID")) & _
                                                                            " AND ItemCode <> " & CCommon.ToLong(Session("ShippingServiceItem")) & _
                                                                            " AND InternationalServiceID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("InternationalServiceID")))
                        For Each dr As DataRow In drClass
                            dtItems.ImportRow(dr)
                            lngTotalQty = lngTotalQty + CCommon.ToLong(dr("numUnitHour"))
                        Next

                        Dim dblBoxQty As Double
                        Dim dblItemQty As Double
                        Dim dblCarryFwdQty As Double = 0
                        Dim dblQtyToBox As Double = 0
                        Dim dblWeightToBox As Double = 0
                        Dim dblDimensionalWeight As Double = 0

                        For iItem As Integer = 0 To dtItems.Rows.Count - 1

                            If objOpportunity Is Nothing Then objOpportunity = New MOpportunity
                            objOpportunity.OpportunityId = 0
                            objOpportunity.DomainID = Session("DomainID")
                            objOpportunity.ItemCode = CCommon.ToLong(dsItems.Tables(0).Rows(i)("ItemCode"))
                            If objOpportunity.CheckServiceItemInOrder() = True Then
                                Continue For
                            End If
                            'If dtItems.Rows.Count > 1 Then
                            '    intCarryFwdQty = 0
                            'End If

                            objOppBizDocs.OppId = lngOppID
                            objOppBizDocs.ShipClassID = CCommon.ToLong(dtItems.Rows(iItem)("numShipClass"))
                            objOppBizDocs.ItemCode = CCommon.ToLong(dtItems.Rows(iItem)("ItemCode"))
                            objOppBizDocs.UnitHour = IIf(lngTotalQty = 0, CCommon.ToDouble(dtItems.Rows(iItem)("numUnitHour")), lngTotalQty)
                            objOppBizDocs.PackageTypeID = CCommon.ToLong(dtItems.Rows(iItem)("PackageTypeID"))
                            objOppBizDocs.DomainID = DomainID
                            lngPackageTypeID = objOppBizDocs.PackageTypeID
                            lngServiceTypeID = objShipping.ServiceType

                            Dim dtPackages As DataTable = Nothing
                            Dim intBoxCount As Integer = 0
                            If objShipping.PackagingType <> 21 Then
                                'ADD ITEMS INTO PACKAGE AS PER PACKAGE RULES
                                dtPackages = objOppBizDocs.GetPackageInfo()
                                'If package rule not found then set default 
                                If dtPackages IsNot Nothing AndAlso dtPackages.Rows.Count > 0 Then
                                    intBoxCount = Math.Ceiling(objOppBizDocs.UnitHour / If(dtPackages.Rows(0)("numFromQty") = 0, 1, CCommon.ToDouble(dtPackages.Rows(0)("numFromQty"))))

                                    dblWeight = CCommon.ToDouble(dtPackages.Rows(0)("fltTotalWeight"))
                                    dblWidth = CCommon.ToDouble(dtPackages.Rows(0)("fltWidth"))
                                    dblHeight = CCommon.ToDouble(dtPackages.Rows(0)("fltHeight"))
                                    dblLength = CCommon.ToDouble(dtPackages.Rows(0)("fltLength"))
                                    dblDimensionalWeight = (If(dblWidth = 0, 1, dblWidth) * If(dblHeight = 0, 1, dblHeight) * If(dblLength = 0, 1, dblLength)) / 166

                                Else
                                    If strErrorOppID.Contains(objOppBizDocs.OppId) = False Then strErrorOppID = objOppBizDocs.OppId & "," & strErrorOppID
                                    litMessage.Text = litMessage.Text & "Order ID " & objOppBizDocs.OppId & " : " & vbCrLf & " Package Rule has not been found." & "<br/>"
                                    Continue For

                                End If

                            Else
                                intBoxCount = 1
                                dblWeight = 1
                                dblWidth = 1
                                dblHeight = 1
                                dblLength = 1
                                dblDimensionalWeight = (If(dblWidth = 0, 1, dblWidth) * If(dblHeight = 0, 1, dblHeight) * If(dblLength = 0, 1, dblLength)) / 166

                            End If

                            If objOppBizDocs.UnitHour <= 0 Then
                                If strErrorOppID.Contains(objOppBizDocs.OppId) = False Then strErrorOppID = objOppBizDocs.OppId & "," & strErrorOppID
                                litMessage.Text = litMessage.Text & "Order ID " & objOppBizDocs.OppId & " : " & "Quantity can not be zero." & "<br/>"
                                Continue For
                            End If

                            ''Create Blank shipping report and use Shipreportid in reference of box
                            If lngOppBizDocID > 0 And lngOppID > 0 And objOppBizDocs.ShipCompany > 0 AndAlso lngShippingReportID <= 0 Then
                                With objOppBizDocs
                                    .DomainID = DomainID
                                    .OppBizDocId = lngOppBizDocID
                                    .ShipCompany = objOppBizDocs.ShipCompany
                                    .UserCntID = UserCntID
                                    ds.Tables.Add(New DataTable())
                                    .strText = ds.GetXml()

                                    Try
                                        .ShippingReportId = .ManageShippingReport
                                    Catch ex As Exception
                                        Continue For
                                    End Try


                                    lngShippingReportID = .ShippingReportId
                                End With

                            End If

                            '--------------------------------------------------------------------------------

                            'For iCnt As Integer = 0 To dtItems.Rows.Count - 1
                            'Next
                            'dblWeight = CCommon.ToInteger(dtItems.Rows(iItem)("fltWeight"))
                            'dblWidth = CCommon.ToInteger(dtItems.Rows(iItem)("fltWidth"))
                            'dblHeight = CCommon.ToInteger(dtItems.Rows(iItem)("fltHeight"))
                            'dblLength = CCommon.ToInteger(dtItems.Rows(iItem)("fltLength"))

                            dblBoxQty = CCommon.ToDouble(dtPackages.Rows(0)("numFromQty"))
                            dblItemQty = CCommon.ToDouble(dtItems.Rows(iItem)("numUnitHour"))

                            If lngTotalQty <> CCommon.ToDouble(dtItems.Rows(iItem)("numUnitHour")) Then
                                dblItemQty = dblCarryFwdQty + CCommon.ToDouble(dtItems.Rows(iItem)("numUnitHour"))
                            End If

                            If intBoxCounter = intBoxCount - 1 AndAlso intBoxCounter > 1 Then
                                Exit For
                            Else
                                For intCount As Integer = 0 To intBoxCount - 1

                                    Dim dr As DataRow
                                    If dblItemQty <= 0 Then
                                        If dblItemQty = 0 Then dblCarryFwdQty = 0
                                        intBoxCounter = intBoxCounter - 1
                                        Exit For

                                    ElseIf dblItemQty < dblBoxQty Then
                                        intBoxCounter = intBoxCounter + 1
                                        dblCarryFwdQty = dblItemQty

                                        If dblCarryFwdQty > 0 AndAlso dtItems.Rows.Count <> iItem + 1 Then
                                            dr = dtItem.NewRow
                                            dr("vcBoxName") = "Box" & intBoxCounter ' (intCount + i + 1).ToString()
                                            dr("numOppBizDocItemID") = CCommon.ToLong(dtItems.Rows(iItem)("OppBizDocItemID"))
                                            dr("intBoxQty") = dblCarryFwdQty
                                            dr("UnitWeight") = CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")) ' dblWeight

                                            dtItem.Rows.Add(dr)
                                            dblTotalWeight = dblTotalWeight + CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")) 'dblWeight

                                            dblQtyToBox = dblCarryFwdQty
                                        Else
                                            ''Check for other Shipping Rule / Package Rule available for Remaining Quantity or not.

                                            Dim dtShipInfoNew As DataTable
                                            objOppBizDocs.OppId = lngOppID
                                            objOppBizDocs.OppBizDocId = lngOppBizDocID
                                            objOppBizDocs.BizDocItemId = lngBizDocItemId
                                            objOppBizDocs.ShipClassID = CCommon.ToLong(dtItems.Rows(iItem)("numShipClass"))
                                            objOppBizDocs.UnitHour = dblCarryFwdQty
                                            dtShipInfoNew = objOppBizDocs.GetShippingRuleInfo()

                                            ''If available then use that package otherwise same package will be used.
                                            If dtShipInfoNew IsNot Nothing AndAlso dtShipInfoNew.Rows.Count > 0 Then
                                                objOppBizDocs.OppId = lngOppID
                                                objOppBizDocs.ShipClassID = CCommon.ToLong(dtItems.Rows(iItem)("numShipClass"))
                                                objOppBizDocs.ItemCode = CCommon.ToLong(dtItems.Rows(iItem)("ItemCode"))
                                                objOppBizDocs.UnitHour = dblCarryFwdQty 'IIf(lngTotalQty = 0, CCommon.ToLong(dtItems.Rows(iItem)("numUnitHour")), lngTotalQty)
                                                objOppBizDocs.PackageTypeID = CCommon.ToLong(dtShipInfoNew.Rows(0)("numPackageTypeID")) 'objShipping.PackagingType
                                                objOppBizDocs.DomainID = DomainID

                                                'ADD ITEMS INTO PACKAGE AS PER PACKAGE RULES
                                                Dim dtCarryFPackage As DataTable = Nothing
                                                dtCarryFPackage = objOppBizDocs.GetPackageInfo()
                                                'If package rule not found then set default 
                                                If dtPackages IsNot Nothing AndAlso dtCarryFPackage.Rows.Count > 0 Then
                                                    lngPackageTypeID = CCommon.ToLong(dtCarryFPackage.Rows(0)("numCustomPackageID"))
                                                    lngServiceTypeID = If(blnSameCountry = True, CCommon.ToShort(dtShipInfoNew.Rows(0)("numDomesticShipID")), CCommon.ToShort(dtShipInfoNew.Rows(0)("numInternationalShipID")))

                                                    'Else
                                                    '    dr = dtItem.NewRow
                                                    '    dr("vcBoxName") = "Box" & intBoxCounter ' (intCount + i + 1).ToString()
                                                    '    dr("numOppBizDocItemID") = CCommon.ToLong(dtItems.Rows(iCnt)("OppBizDocItemID"))
                                                    '    dr("intBoxQty") = intCarryFwdQty
                                                    '    dr("UnitWeight") = dblWeight
                                                    '    dtItem.Rows.Add(dr)
                                                    '    dblTotalWeight = dblTotalWeight + dblWeight
                                                    '    intQtyToBox = intCarryFwdQty
                                                End If
                                            End If


                                            dr = dtItem.NewRow
                                            dr("vcBoxName") = "Box" & intBoxCounter ' (intCount + i + 1).ToString()
                                            dr("numOppBizDocItemID") = CCommon.ToLong(dtItems.Rows(iItem)("OppBizDocItemID"))

                                            If dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'").Length > 0 Then
                                                'Dim drQty As DataRow() = dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'")
                                                Dim intExistBoxQty As Integer
                                                For Each drQty As DataRow In dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'")
                                                    intExistBoxQty = intExistBoxQty + CCommon.ToDouble(drQty("intBoxQty"))
                                                Next
                                                If dblBoxQty <= intExistBoxQty Then
                                                    Continue For
                                                Else
                                                    dblCarryFwdQty = dblBoxQty - intExistBoxQty
                                                End If
                                            End If

                                            dr("intBoxQty") = dblCarryFwdQty
                                            dr("UnitWeight") = CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")) 'dblWeight
                                            dtItem.Rows.Add(dr)

                                            'dblWeight = CCommon.ToInteger(dtItems.Rows(iItem)("fltWeight"))
                                            'dblWidth = CCommon.ToInteger(dtItems.Rows(iItem)("fltWidth"))
                                            'dblHeight = CCommon.ToInteger(dtItems.Rows(iItem)("fltHeight"))
                                            'dblLength = CCommon.ToInteger(dtItems.Rows(iItem)("fltLength"))

                                            dblTotalWeight = dblTotalWeight + CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight"))
                                            'dblTotalWeight = dblTotalWeight + dblWeight
                                            dblQtyToBox = dblCarryFwdQty
                                        End If

                                        dblWeightToBox = dblQtyToBox * CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")) 'dblWeight
                                    Else

                                        intBoxCounter = intBoxCounter + 1
                                        If intBoxCounter = intBoxCount - 1 AndAlso iItem = dtItems.Rows.Count - 1 Then
                                            'intCarryFwdQty = 0
                                        End If

                                        dr = dtItem.NewRow
                                        dr("vcBoxName") = "Box" & intBoxCounter '(intCount + i + 1).ToString()
                                        dr("numOppBizDocItemID") = CCommon.ToLong(dtItems.Rows(iItem)("OppBizDocItemID"))

                                        Dim dblExistBoxQty As Double
                                        If dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'").Length > 0 Then

                                            For Each drQty As DataRow In dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'")
                                                dblExistBoxQty = dblExistBoxQty + CCommon.ToDouble(drQty("intBoxQty"))
                                            Next
                                            If dblBoxQty <= dblExistBoxQty Then
                                                Continue For
                                            Else
                                                dblExistBoxQty = dblBoxQty - dblExistBoxQty
                                            End If
                                        Else
                                            dblExistBoxQty = dblBoxQty - dblCarryFwdQty
                                        End If

                                        dr("intBoxQty") = dblExistBoxQty ' intBoxQty - intCarryFwdQty ' If(intItemQty > intBoxQty AndAlso intCarryFwdQty = 0, intBoxQty - intCarryFwdQty, intItemQty - intBoxQty)
                                        dr("UnitWeight") = CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")) 'dblWeight

                                        dtItem.Rows.Add(dr)
                                        dblTotalWeight = dblTotalWeight + CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")) 'dblWeight

                                        dblQtyToBox = dblBoxQty - dblCarryFwdQty

                                        If dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'").Length > 1 Then
                                            For Each drI As DataRow In dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'")
                                                dblWeightToBox = dblWeightToBox + (drI("intBoxQty") * drI("UnitWeight"))
                                            Next
                                        Else
                                            dblWeightToBox = (dblQtyToBox * CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")))
                                            'intWeightToBox = (intQtyToBox * dblWeight)
                                        End If

                                    End If

                                    ''Creating Box Entry
                                    'Dim dblFinalWeight As Double
                                    'dblFinalWeight = dblTotalWeight * intQtyToBox 'IIf(intItemQty > intBoxQty, intBoxQty, intItemQty)

                                    dr = dtBox.NewRow
                                    dr("vcBoxName") = "Box" & intBoxCounter '(intCount + i + 1).ToString()
                                    dr("fltTotalWeight") = dblWeightToBox 'CCommon.ToLong(dsItems.Tables(0).Rows(i)("fltWeight"))
                                    dr("fltLength") = dblLength  'CCommon.ToLong(dsItems.Tables(0).Rows(i)("fltLength"))
                                    dr("fltWidth") = dblWidth 'CCommon.ToLong(dsItems.Tables(0).Rows(i)("fltWidth"))
                                    dr("fltHeight") = dblHeight 'CCommon.ToLong(dsItems.Tables(0).Rows(i)("fltHeight"))
                                    dr("numPackageTypeID") = lngPackageTypeID
                                    dr("numServiceTypeID") = lngServiceTypeID
                                    dr("fltDimensionalWeight") = dblDimensionalWeight
                                    If CCommon.ToString(dtPackages.Rows(0)("vcPackageName")).ToLower.Contains("fedex") Then
                                        dr("numShipCompany") = 91
                                    ElseIf CCommon.ToString(dtPackages.Rows(0)("vcPackageName")).ToLower.Contains("ups") Then
                                        dr("numShipCompany") = 88
                                    ElseIf CCommon.ToString(dtPackages.Rows(0)("vcPackageName")).ToLower.Contains("usps") Then
                                        dr("numShipCompany") = 90
                                    Else
                                        dr("numShipCompany") = 0
                                    End If

                                    If dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'").Length > 1 Then
                                        dtBox.Rows.Remove(dtBox.Select("vcBoxName='" & "Box" & intBoxCounter & "'")(0))
                                    End If
                                    dtBox.Rows.Add(dr)

                                    dblWeightToBox = 0
                                    dblTotalWeight = 0
                                    dblItemQty = dblItemQty - dblQtyToBox 'intBoxQty
                                Next
                            End If
                        Next
                    Next

                    ds.Tables.Add(dtBox)
                    ds.Tables.Add(dtItem)

                    With objOppBizDocs
                        .ShippingReportId = lngShippingReportID
                        .strText = ds.GetXml()
                        .UserCntID = UserCntID
                        .byteMode = 0
                        .PageMode = 1
                        Try
                            .ManageShippingBox()
                        Catch ex As Exception
                            If strErrorOppID.Contains(objOppBizDocs.OppId) = False Then strErrorOppID = objOppBizDocs.OppId & "," & strErrorOppID
                            litMessage.Text = litMessage.Text & "Order ID " & objOppBizDocs.OppId & " : " & objShipping.ErrorMsg & "<br/>"
                        End Try

                    End With

                    'GET BOX DETAILS AS PER SHIPPING REPORT ID
                    dtBox = Nothing
                    dtItem = Nothing
                    If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                    objOppBizDocs.ShippingReportId = lngShippingReportID
                    dtBox = objOppBizDocs.GetShippingBoxes().Tables(0)
                    dtItem = objOppBizDocs.GetShippingBoxes().Tables(1)

                    If IsGenerateShippingLabel = False Then
                        BindData(dtBox)
                        ClientScript.RegisterClientScriptBlock(Me.GetType, "Box", "OpenShippingReport(" & lngOppBizDocID & ", " & lngShippingReportID & ", " & lngOppID & ", " & 1 & ");", True)

                    Else
                        For i As Integer = 0 To dtBox.Rows.Count - 1

                            ' Add condition by which we can specify whether we use dimensions or not
                            If objShipping.PackagingType <> 21 Then
                                If objOppBizDocs.ShipCompany = 91 Then
                                    objShipping.UseDimentions = True
                                Else
                                    objShipping.UseDimentions = False
                                End If
                            Else
                                objShipping.UseDimentions = True
                            End If

                            objShipping.Provider = objOppBizDocs.ShipCompany
                            objShipping.Height = CCommon.ToDouble(dtBox.Rows(i)("fltHeight"))
                            objShipping.Length = CCommon.ToDouble(dtBox.Rows(i)("fltLength"))
                            objShipping.Width = CCommon.ToDouble(dtBox.Rows(i)("fltWidth"))
                            objShipping.WeightInLbs = CCommon.ToDouble(dtBox.Rows(i)("fltTotalWeight"))
                            If objShipping.WeightInOunce <= 0 Then
                                objShipping.WeightInOunce = objShipping.WeightInLbs * 16
                            End If
                            'objShipping.WeightInOunce = 0 'CCommon.ToLong(dsItems.Tables(0).Rows(i)("fltWeight"))
                            objShipping.BoxID = CCommon.ToDouble(dtBox.Rows(i)("numBoxID"))

                            If objShipping.WeightInLbs = 0 Then
                                'litMessage.Text = "Please specify weight of box.";
                                If strErrorOppID.Contains(objOppBizDocs.OppId) = False Then strErrorOppID = objOppBizDocs.OppId & "," & strErrorOppID
                                litMessage.Text = litMessage.Text & "Order ID " & objOppBizDocs.OppId & " : " & "Please specify weight of box." & "<br/>"
                                Continue For
                            End If
                            objShipping.ReferenceNo = ""
                            objShipping.AddPackage()
                            objShipping.ShipperSpecialServices = specialService

                            If dtItem IsNot Nothing AndAlso dtItem.Rows.Count > 0 Then
                                Dim drCommodity As DataRow() = dtItem.Select("numBoxID=" & objShipping.BoxID)
                                If drCommodity.Length > 0 Then
                                    objShipping.CompoDescription = CCommon.ToString(drCommodity(0)("vcItemName")) & CCommon.ToString(drCommodity(0)("vcItemDesc"))
                                    objShipping.Quantity = CCommon.ToInteger(drCommodity(0)("intBoxQty"))
                                    objShipping.UnitName = CCommon.ToString(drCommodity(0)("vcUnitName"))
                                    objShipping.UnitPrice = CCommon.ToDouble(drCommodity(0)("monUnitPrice"))
                                Else
                                    litMessage.Text = litMessage.Text & " Commodity information is required for Box :" & objShipping.BoxID & " to ship it internationally."
                                End If
                            Else
                                litMessage.Text = litMessage.Text & " Commodity information is required for Box :" & objShipping.BoxID & " to ship it internationally."
                            End If

                            If objShipping.CompoDescription = "" Then
                                litMessage.Text = litMessage.Text & " Commodity information (Description) is required for Box :" & objShipping.BoxID & " to ship it internationally."
                                Exit Sub

                            ElseIf objShipping.Quantity <= 0 Then
                                litMessage.Text = litMessage.Text & " Commodity information (Qauntity) is required for Box :" & objShipping.BoxID & " to ship it internationally."
                                Exit Sub

                            ElseIf objShipping.UnitName = "" Then
                                litMessage.Text = litMessage.Text & " Commodity information (Unit Name) is required for Box :" & objShipping.BoxID & " to ship it internationally."
                                Exit Sub

                            ElseIf objShipping.UnitPrice <= 0 Then
                                litMessage.Text = litMessage.Text & " Commodity information (Unit Price) is required for Box :" & objShipping.BoxID & " to ship it internationally."
                                Exit Sub
                            End If

                            Try
                                objShipping.GetShippingLabel(0, IsDomestic, objShipping.Provider)
                                strSuccessOppID = objOppBizDocs.OppId & "," & strSuccessOppID
                            Catch ex As Exception
                                If strErrorOppID.Contains(objOppBizDocs.OppId) = False Then strErrorOppID = objOppBizDocs.OppId & "," & strErrorOppID
                                Continue For
                            End Try

                            dblTotalWeight = 0
                            If Not String.IsNullOrEmpty(objShipping.ErrorMsg) Then
                                If strErrorOppID.Contains(objOppBizDocs.OppId) = False Then strErrorOppID = objOppBizDocs.OppId & "," & strErrorOppID

                                If (objShipping.ErrorMsg.Contains("6532")) Then
                                    litMessage.Text = litMessage.Text & "Order ID " & objOppBizDocs.OppId & " : " & "Shipper's phone number is required, your option is to update phone number of primary contact." & "<br/>"
                                ElseIf (objShipping.ErrorMsg.Contains("6541")) Then
                                    litMessage.Text = litMessage.Text & "Order ID " & objOppBizDocs.OppId & " : " & objShipping.ErrorMsg & "<br/>"
                                Else
                                    litMessage.Text = litMessage.Text & "Order ID " & objOppBizDocs.OppId & " : " & objShipping.ErrorMsg & "<br/>"
                                End If
                            Else
                                ClientScript.RegisterClientScriptBlock(Me.GetType, "Box", "OpenShippingReport(" & lngOppBizDocID & ", " & lngShippingReportID & ", " & lngOppID & ", " & 1 & ");", True)
                            End If
                        Next

                    End If
                End If
            End If

            'Try
            '    objOppBizDocs.UpdateBizDocsShippingFields()
            'Catch ex As Exception
            '    Throw ex
            'End Try

            'lngShippingReportID = 0

            ''IF SUCCESS/SHIPPING LABEL IS GENERATED, THEN ORDER WILL BE SET STATUS AS PER CONFIGURED IN SHIPPING LABEL CONFIGURATION 
            'If Not String.IsNullOrEmpty(strSuccessOppID) AndAlso strSuccessOppID <> "" Then

            '    'Check for Shipping Label Batch Process
            '    Dim dtConfig As New DataTable
            '    If objShippingRule Is Nothing Then objShippingRule = New ShippingRule
            '    With objShippingRule
            '        .DomainID = DomainID
            '        dtConfig = .GetShippingLabelConfiguration()
            '    End With

            '    'Change Opp Status
            '    objCommon = New CCommon
            '    objCommon.Mode = 24
            '    objCommon.Comments = String.Join(",", strSuccessOppID)
            '    objCommon.UpdateValueID = CCommon.ToLong(dtConfig.Rows(0)("numSuccessStatus"))
            '    objCommon.UpdateSingleFieldValue()

            'End If

            ''IF ERROR/SHIPPING LABEL IS NOT GENERATED, THEN ORDER WILL BE SET STATUS AS PER CONFIGURED IN SHIPPING LABEL CONFIGURATION 
            'If Not String.IsNullOrEmpty(strErrorOppID) AndAlso strErrorOppID <> "" Then

            '    'Check for Shipping Label Batch Process
            '    Dim dtConfig As New DataTable
            '    If objShippingRule Is Nothing Then objShippingRule = New ShippingRule
            '    With objShippingRule
            '        .DomainID = DomainID
            '        dtConfig = .GetShippingLabelConfiguration()
            '    End With

            '    'Change Opp Status
            '    objCommon = New CCommon
            '    objCommon.Mode = 24
            '    objCommon.Comments = String.Join(",", strErrorOppID)
            '    objCommon.UpdateValueID = CCommon.ToLong(dtConfig.Rows(0)("numErrorStatus"))
            '    objCommon.UpdateSingleFieldValue()

            'End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

End Class