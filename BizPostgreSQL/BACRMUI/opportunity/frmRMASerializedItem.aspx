﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmRMASerializedItem.aspx.vb" Inherits=".frmRMASerializedItem" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Serial / Lot #s</title>
    <script language="javascript" type="text/javascript">
        function Save() {

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" />&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Return Serial / Lot #s
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <table width="800px">
        <tr>
            <td class="normal4" align="center" colspan="2">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td align="left" class="normal1">Item :
                            <asp:Label ID="lblItemName" runat="server" CssClass="signup"></asp:Label>
                &nbsp; Units :
                            <asp:Label ID="lblUnits" runat="server" CssClass="signup"></asp:Label>&nbsp;<asp:Label
                                ID="lblUnitsName" runat="server" CssClass="signup"></asp:Label>
              From WareHouse : <asp:Label
                                ID="lblWareHouse" runat="server" CssClass="signup"></asp:Label>
            </td>
        </tr>
        <tr>
            <td>
                <telerik:RadGrid ID="gvSerialLot" runat="server" AutoGenerateColumns="false" AlternatingItemStyle-CssClass="ais"
                    ItemStyle-CssClass="is" HeaderStyle-CssClass="hs" Skin="windows" EnableEmbeddedSkins="false"
                    CssClass="dg" EnableLinqExpressions="false" ItemStyle-HorizontalAlign="Center"
                    AlternatingItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                    <MasterTableView DataKeyNames="numWareHouseItmsDTLID,numWareHouseItemID" Width="100%">
                        <Columns>
                            <telerik:GridTemplateColumn HeaderText="" UniqueName="CheckBox">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkInclude" runat="server" />
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="vcSerialNo" HeaderText="Serial/LOT(#)" UniqueName="vcSerialNo" />
                            <telerik:GridBoundColumn DataField="TotalQty" HeaderText="Available Qty" UniqueName="TotalQty" />
                            <telerik:GridTemplateColumn HeaderText="Used Qty" UniqueName="UsedQty">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtUsedQty" runat="server" Width="80" CssClass="signup" Text='<%#DataBinder.Eval(Container.DataItem, "UsedQty")%>'
                                        onkeypress="CheckNumber(2,event)"></asp:TextBox>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                        <NoRecordsTemplate>
                        </NoRecordsTemplate>
                    </MasterTableView>
                    <ClientSettings AllowExpandCollapse="true" />
                </telerik:RadGrid>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hfbitSerialized" runat="server" />
    <asp:HiddenField ID="hfbitLotNo" runat="server" />
    <input id="hdWareHouseItemID" runat="server" type="hidden" />
</asp:Content>
