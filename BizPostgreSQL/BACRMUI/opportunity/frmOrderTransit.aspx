﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmOrderTransit.aspx.vb"
    Inherits=".frmOrderTransit" MasterPageFile="~/common/PopupBootstrap.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>In Transit</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="pull-right">
<asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" OnClientClick="window.close()" Text="Close" />
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    On Order
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:GridView ID="gvOrderTransit" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-striped" UseAccessibleHeader="true">
        <Columns>
            <asp:TemplateField HeaderText="Units">
                <ItemTemplate>
                    <%# String.Format("{0:#,##0.#####}", Eval("numUnitHour")) & " " & Eval("vcUOMName") %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Ordered On">
                <ItemTemplate>
                    <%# Eval("bintCreatedDate") & " (" & Eval("vcpOppName") & ")" %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="vcShipFrom" HeaderText="From"></asp:BoundField>
            <asp:BoundField DataField="vcShipTo" HeaderText="To"></asp:BoundField>
            <asp:BoundField DataField="vcShipmentMethod" HeaderText="Ship-Via"></asp:BoundField>
            <asp:BoundField DataField="vcShipmentService" HeaderText="Shipping Carrier Service"></asp:BoundField>
            <asp:TemplateField HeaderText="Expected (Updated)">
                <ItemTemplate>
                    <%#  Eval("bintExpectedDelivery") %>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
