﻿
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Admin
Imports System.IO
Imports System.Net
Imports Moneris

Partial Public Class frmChangeDefaultAdd : Inherits BACRMPage

    Protected sAddressType As String = String.Empty
    Protected lngDivID As Long = 0

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngDivID = CCommon.ToLong(GetQueryStringVal("CompanyId"))
            sAddressType = GetQueryStringVal("AddressType")
            Dim sCompanyName As String = CCommon.ToString(GetQueryStringVal("CompanyName"))
            If Not IsPostBack Then
                If lngDivID = 0 Then
                    radCmbCompany.Text = ""
                    radCmbCompany.SelectedValue = ""
                    ddlContact = Nothing
                ElseIf GetQueryStringVal("IsDropshipClicked") <> "1" Then
                    radCmbCompany.Text = sCompanyName
                    radCmbCompany.SelectedValue = lngDivID
                    Me.radCmbCompany_SelectedIndexChanged(radCmbCompany, Nothing)
                End If

                BindProjects()
                BindWarehouses()
                FillSelectedAddress()

                If GetQueryStringVal("IsDropshipClicked") = "1" Then
                    divWarehouse.Visible = False
                    rbCompany.Checked = True
                    rbContact.Checked = False
                    rbProject.Checked = False
                    rbWarehouse.Checked = False
                    radCmbCompany.Text = ""
                    radCmbCompany.SelectedValue = ""
                    Me.radCmbCompany_SelectedIndexChanged(radCmbCompany, Nothing)
                    radCmbCompany.Focus()
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub FillSelectedAddress()
        Try
            If CCommon.ToLong(GetQueryStringVal("AddressID")) > 0 Then
                Dim isWarehouseAddress As Boolean = False
                Dim addressID As Long = CCommon.ToLong(GetQueryStringVal("AddressID"))
                Dim objContact As New CContacts
                objContact.DomainID = Session("DomainID")
                objContact.AddressID = addressID
                objContact.byteMode = 1
                Dim dtAddress As DataTable = objContact.GetAddressDetail
                If Not dtAddress Is Nothing AndAlso dtAddress.Rows.Count > 0 Then
                    lblFullAddress.Text = CCommon.ToString(dtAddress.Rows(0)("vcFullAddress"))
                Else
                    lblFullAddress.Text = "NA"
                End If

                For Each objItem As ListItem In ddlWarehouse.Items
                    If CCommon.ToLong(objItem.Value.Split("~")(1)) = addressID Then
                        ddlWarehouse.Items.FindByValue(objItem.Value).Selected = True
                        BindAddressByAddressID(ddlWarehouse.SelectedValue.Split("~")(1))
                        rbCompany.Checked = False
                        rbContact.Checked = False
                        rbProject.Checked = False
                        rbWarehouse.Checked = True
                        isWarehouseAddress = True
                    End If
                Next

                If Not isWarehouseAddress Then
                    If Not dtAddress Is Nothing AndAlso dtAddress.Rows.Count > 0 Then
                        If CCommon.ToShort(dtAddress.Rows(0)("tintAddressOf")) = 4 Then 'Project
                            If Not ddlProject.Items.FindByValue(CCommon.ToLong(dtAddress.Rows(0)("numRecordID"))) Is Nothing Then
                                ddlProject.Items.FindByValue(CCommon.ToLong(dtAddress.Rows(0)("numRecordID"))).Selected = True
                                rbCompany.Checked = False
                                rbContact.Checked = False
                                rbProject.Checked = True
                                rbWarehouse.Checked = False
                            End If
                        ElseIf CCommon.ToShort(dtAddress.Rows(0)("tintAddressOf")) = 1 Then 'Contact
                            radCmbCompany.Text = CCommon.ToString(dtAddress.Rows(0)("vcCompanyName"))
                            radCmbCompany.SelectedValue = CCommon.ToLong(dtAddress.Rows(0)("numDivisionID"))
                            Me.radCmbCompany_SelectedIndexChanged(radCmbCompany, Nothing)

                            If Not ddlContact.Items.FindByValue(CCommon.ToLong(dtAddress.Rows(0)("numRecordID"))) Is Nothing Then
                                ddlContact.Items.FindByValue(CCommon.ToLong(dtAddress.Rows(0)("numRecordID"))).Selected = True
                                ddlContact_SelectedIndexChanged(ddlContact, Nothing)
                                If Not ddlAddressType.Items.FindByValue(addressID) Is Nothing Then
                                    ddlAddressType.Items.FindByValue(addressID).Selected = True
                                    rbCompany.Checked = False
                                    rbContact.Checked = True
                                    rbProject.Checked = False
                                    rbWarehouse.Checked = False
                                End If
                            End If
                        ElseIf CCommon.ToShort(dtAddress.Rows(0)("tintAddressOf")) = 2 Then 'Organization
                            radCmbCompany.Text = CCommon.ToString(dtAddress.Rows(0)("vcCompanyName"))
                            radCmbCompany.SelectedValue = CCommon.ToLong(dtAddress.Rows(0)("numRecordID"))
                            Me.radCmbCompany_SelectedIndexChanged(radCmbCompany, Nothing)

                            If Not ddlAddressName.Items.FindByValue(addressID) Is Nothing Then
                                ddlAddressName.Items.FindByValue(addressID).Selected = True
                                rbCompany.Checked = True
                                rbContact.Checked = False
                                rbProject.Checked = False
                                rbWarehouse.Checked = False
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BindWarehouses()
        Try
            ddlWarehouse.Items.Insert(0, New ListItem("-- Select One --", "0~0"))

            Dim objItem As New CItems
            objItem.DomainID = Session("DomainID")
            Dim dt As DataTable = objItem.GetWareHouses

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    ddlWarehouse.Items.Add(New ListItem(dr("vcWareHouse"), CCommon.ToLong(dr("numWareHouseID")) & "~" & CCommon.ToLong(dr("numAddressID"))))
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
        Try
            If CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                Dim iDivisionId As Integer = radCmbCompany.SelectedValue
                Dim oCOpportunities As New COpportunities
                oCOpportunities.DomainID = CType((Session("DomainID")), Long)
                ddlContact.DataSource = oCOpportunities.GetContactDetails(CInt(Session("UserID")), CInt(iDivisionId))
                ddlContact.DataTextField = "contactName"
                ddlContact.DataValueField = "numContactID"
                ddlContact.DataBind()

                BindAddressName()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Try
            If rbCompany.Checked Then
                If CCommon.ToLong(radCmbCompany.SelectedValue) > 0 And CCommon.ToLong(ddlAddressName.SelectedValue) > 0 Then
                    Response.Write("<script>opener.FillModifiedAddress('" & sAddressType & "','" & ddlAddressName.SelectedValue & "','" & lblFullAddress.Text & "',-1,'" & GetQueryStringVal("IsDropshipClicked") & "'); self.close();</script>")
                Else
                    litMessage.Text = "Please select a company address"
                End If
            End If
            If rbContact.Checked Then
                If CCommon.ToLong(ddlAddressType.SelectedValue) > 0 Then
                    Response.Write("<script>opener.FillModifiedAddress('" & sAddressType & "','" & ddlAddressType.SelectedValue & "','" & lblFullAddress.Text & "',-1,'" & GetQueryStringVal("IsDropshipClicked") & "'); self.close();</script>")
                Else
                    litMessage.Text = "Please select a contact address"
                End If
            End If
            If rbProject.Checked Then
                Dim objProject As New BACRM.BusinessLogic.Projects.Project
                objProject.DomainID = CCommon.ToLong(Session("DomainID"))
                objProject.ProjectID = CCommon.ToLong(ddlProject.SelectedValue)
                Dim projectAddressID As Long = CCommon.ToLong(objProject.GetFieldValue("numAddressID"))

                If projectAddressID > 0 Then
                    Response.Write("<script>opener.FillModifiedAddress('" & sAddressType & "','" & projectAddressID & "','" & lblFullAddress.Text & "',-1,'" & GetQueryStringVal("IsDropshipClicked") & "'); self.close();</script>")
                Else
                    litMessage.Text = "Please select a project address"
                End If
            End If
            If rbWarehouse.Checked Then
                If CCommon.ToLong(ddlWarehouse.SelectedValue.Split("~")(1)) > 0 Then
                    Response.Write("<script>opener.FillModifiedAddress('" & sAddressType & "','" & ddlWarehouse.SelectedValue.Split("~")(1) & "','" & lblFullAddress.Text & "'," & ddlWarehouse.SelectedValue.Split("~")(0) & ",'0'); self.close();</script>")
                Else
                    litMessage.Text = "Please select a warehouse address"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlAddressName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAddressName.SelectedIndexChanged
        Try
            If ddlAddressName.SelectedValue > 0 Then
                BindAddressByAddressID(ddlAddressName.SelectedValue)
            Else
                lblFullAddress.Text = "NA"
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlAddressType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAddressType.SelectedIndexChanged
        Try
            If ddlAddressType.SelectedValue > 0 Then
                BindAddressByAddressID(ddlAddressType.SelectedValue)
            Else
                lblFullAddress.Text = "NA"
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlContact_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContact.SelectedIndexChanged
        Try
            If ddlContact.SelectedValue > 0 Then
                Dim objContact As New CContacts
                objContact.DomainID = Session("DomainID")
                objContact.RecordID = ddlContact.SelectedValue
                objContact.AddresOf = CContacts.enmAddressOf.Contact
                objContact.AddressType = CContacts.enmAddressType.None
                objContact.byteMode = 2
                ddlAddressType.DataValueField = "numAddressID"
                ddlAddressType.DataTextField = "vcAddressName"
                ddlAddressType.DataSource = objContact.GetAddressDetail()
                ddlAddressType.DataBind()
                ddlAddressType.Items.Insert(0, New ListItem("--Select One--", "0"))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub BindAddressName()
        Try
            Dim objContact As New CContacts
            If GetQueryStringVal("AddType") = "Bill" Then
                objContact.AddressType = CContacts.enmAddressType.BillTo
            Else
                objContact.AddressType = CContacts.enmAddressType.ShipTo
            End If
            objContact.DomainID = Session("DomainID")
            objContact.RecordID = radCmbCompany.SelectedValue
            objContact.AddresOf = CContacts.enmAddressOf.Organization
            objContact.byteMode = 2
            ddlAddressName.DataValueField = "numAddressID"
            ddlAddressName.DataTextField = "vcAddressName"
            ddlAddressName.DataSource = objContact.GetAddressDetail()
            ddlAddressName.DataBind()
            ddlAddressName.Items.Insert(0, New ListItem("--Select One--", "0"))

            If ddlContact.SelectedValue > 0 Then
                objContact.DomainID = Session("DomainID")
                objContact.RecordID = ddlContact.SelectedValue
                objContact.AddresOf = CContacts.enmAddressOf.Contact
                objContact.AddressType = CContacts.enmAddressType.None
                objContact.byteMode = 2
                ddlAddressType.DataValueField = "numAddressID"
                ddlAddressType.DataTextField = "vcAddressName"
                ddlAddressType.DataSource = objContact.GetAddressDetail()
                ddlAddressType.DataBind()
                ddlAddressType.Items.Insert(0, New ListItem("--Select One--", "0"))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindProjects()
        Try
            Dim objProject As New BACRM.BusinessLogic.Projects.Project
            objProject.DomainID = Session("DomainID")
            Dim dtProject As DataTable = objProject.GetOpenProject()
            ddlProject.DataTextField = "vcProjectName"
            ddlProject.DataValueField = "numProId"
            ddlProject.DataSource = dtProject
            ddlProject.DataBind()
            ddlProject.Items.Insert(0, "--Select One--")
            ddlProject.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindAddressByAddressID(ByVal lngAddressID As Long)
        Try
            Dim objContact As New CContacts
            objContact.DomainID = Session("DomainID")
            objContact.AddressID = lngAddressID
            objContact.byteMode = 1
            Dim dtTable As DataTable = objContact.GetAddressDetail
            If dtTable.Rows.Count > 0 Then
                lblFullAddress.Text = CCommon.ToString(dtTable.Rows(0)("vcFullAddress"))
            Else
                lblFullAddress.Text = "NA"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub ddlProject_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            If CCommon.ToLong(ddlProject.SelectedValue) > 0 Then
                Dim projectAddressID As Long = 0

                Dim objProject As New BACRM.BusinessLogic.Projects.Project
                objProject.DomainID = CCommon.ToLong(Session("DomainID"))
                objProject.ProjectID = CCommon.ToLong(ddlProject.SelectedValue)
                projectAddressID = CCommon.ToLong(objProject.GetFieldValue("numAddressID"))
                BindAddressByAddressID(projectAddressID)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub ddlWarehouse_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            If rbWarehouse.Checked Then
                If CCommon.ToLong(ddlWarehouse.SelectedValue.Split("~")(1)) > 0 Then
                    BindAddressByAddressID(ddlWarehouse.SelectedValue.Split("~")(1))
                Else
                    lblFullAddress.Text = "NA"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class