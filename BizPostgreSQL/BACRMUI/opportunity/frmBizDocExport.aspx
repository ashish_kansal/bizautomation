﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmBizDocExport.aspx.vb"
    Inherits="BACRM.UserInterface.Opportunities.frmBizDocExport" EnableViewState="false"
    ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <script type="text/javascript">
        function Close() {
            self.close();
            return false;
        }
    </script>

</head>
<body runat="server" id="body">
    <form id="form1" runat="server">
    <input type="button" value="Close" onclick="Close();" />
    <div style="display: none">
        <asp:Panel runat="server" ID="pnlExport">
            <table class="style1" border="1">
                <tr>
                    <td colspan="2">
                        <asp:Label ID="lblCompName" runat="server" Style="font-size: x-large"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblBizDoc1" runat="server"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        Date:
                    </td>
                    <td>
                        <asp:Label ID="lblDueDate1" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        STORE
                    </td>
                    <td>
                        <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td>
                        NAME:
                    </td>
                    <td colspan="2">
                        <asp:Label ID="lblCustName" runat="server" ForeColor="Red"></asp:Label>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        ADDRESS1
                    </td>
                    <td>
                        <asp:Label ID="lblAddress1" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td>
                        SUITE:
                    </td>
                    <td colspan="2">
                        <asp:Label ID="lblSuite" runat="server" ForeColor="Red"></asp:Label>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        ADDRESS2
                    </td>
                    <td>
                        <asp:Label ID="lblAddress2" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td>
                        ZIP:
                    </td>
                    <td colspan="2">
                        <asp:Label ID="lblZip" runat="server" ForeColor="Red"></asp:Label>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        TELEPHONE
                    </td>
                    <td>
                        <asp:Label ID="lblTelephone" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td>
                        FAX:
                    </td>
                    <td colspan="2">
                        <asp:Label ID="lblFax" runat="server" ForeColor="Red"></asp:Label>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        EMAIL
                    </td>
                    <td>
                        <asp:Label ID="lblEmail" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td>
                        CELL:
                    </td>
                    <td colspan="2">
                        <asp:Label ID="lblCell" runat="server" ForeColor="Red"></asp:Label>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        ORDER NO
                    </td>
                    <td>
                        <asp:Label ID="lblOrderNo" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td>
                        PO:
                    </td>
                    <td colspan="2">
                        <asp:Label ID="lblPO" runat="server" ForeColor="Red"></asp:Label>
                        &nbsp;
                    </td>
                </tr>
                <%-- <tr>
                <td>
                    CODE
                </td>
                <td>
                    UPC
                </td>
                <td>
                    DESCRIPTION
                </td>
                <td>
                    WHOLESALE
                </td>
                <td>
                    ORDER
                </td>
                <td>
                    TOTAL
                </td>
                <td>
                    OH
                </td>
            </tr>--%>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td colspan="5">
                        <asp:DataGrid ID="dgBizDocs" runat="server" ForeColor="Transparent" Font-Size="10px"
                            Width="100%" BorderStyle="None"  BackColor="White" GridLines="Vertical"
                            HorizontalAlign="Center" AutoGenerateColumns="False">
                            <AlternatingItemStyle Font-Size="8pt" Font-Names="tahoma,verdana,arial,helvetica"
                                BackColor="White"></AlternatingItemStyle>
                            <ItemStyle Font-Size="8pt" Font-Names="tahoma,verdana,arial,helvetica" HorizontalAlign="Center"
                                BorderStyle="None"  VerticalAlign="Middle" BackColor="#e0dfe3">
                            </ItemStyle>
                            <HeaderStyle Font-Size="8pt" Font-Names="tahoma,verdana,arial,helvetica" Wrap="False"
                                HorizontalAlign="Center" Height="20px" ForeColor="White" BorderColor="Black"
                                VerticalAlign="Middle" BackColor="#9d9da1"></HeaderStyle>
                            <Columns>
                            </Columns>
                        </asp:DataGrid>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td valign="top">
                        <b>COMMENTS</b>&nbsp;
                    </td>
                    <td>
                        <asp:Label ID="lblComment" runat="server"></asp:Label>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                    <td colspan="2">
                        <table id="tblBizDocSumm" runat="server" border="0">
                        </table>
                    </td>
                </tr>
                <%-- <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    <b>TOTAL</b>
                </td>
                <td>
                    <asp:Label ID="lblTotal" runat="server"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;
                </td>
                <td>
                    SALES TAX
                </td>
                <td>
                    <asp:Label ID="lblSalesTax" runat="server"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    SHIPPING
                </td>
                <td>
                    <asp:Label ID="lblShipping" runat="server"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    TOTAL
                </td>
                <td>
                    <asp:Label ID="lblGTotal" runat="server"></asp:Label>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>--%>
            </table>
        </asp:Panel>
        <asp:Button ID="btnExport" runat="server" />
    </div>
    </form>
    <input id="hdSubTotal" runat="server" type="hidden" />
    <input id="hdShipAmt" runat="server" type="hidden" />
    <input id="hdTaxAmt" runat="server" type="hidden" />
    <input id="hdCRVTaxAmt" runat="server" type="hidden" />
    <input id="hdDisc" runat="server" type="hidden" />
    <input id="hdLateCharge" runat="server" type="hidden" />
    <input id="hdGrandTotal" runat="server" type="hidden" />
    <input id="hdnCreditAmount" runat="server" type="hidden" />
</body>
</html>
