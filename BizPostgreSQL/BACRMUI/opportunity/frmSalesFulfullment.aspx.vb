﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI
Imports System.Text

''Created By Anoop Jayaraj
Namespace BACRM.UserInterface.Opportunities
    Partial Public Class frmSalesFulfullment
        Inherits BACRMPage

        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim strColumn As String
        Dim m_aryRightsForPage1() As Integer


        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                GetUserRightsForPage(10, 15)

                If m_aryRightsForPage(RIGHTSTYPE.EXPORT) = 0 Then
                    'btnCreatePickList.Visible = False
                    If Not ddlBatchAction.Items.FindByValue("1") Is Nothing Then
                        ddlBatchAction.Items.Remove(ddlBatchAction.Items.FindByValue("1"))
                    End If
                    If Not ddlBatchAction.Items.FindByValue("2") Is Nothing Then
                        ddlBatchAction.Items.Remove(ddlBatchAction.Items.FindByValue("2"))
                    End If

                End If

                If GetQueryStringVal("frm") <> "" Then
                    frm = ""
                    frm = GetQueryStringVal("frm")
                Else : frm = ""
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = ""
                    frm1 = GetQueryStringVal("frm1")
                Else : frm1 = ""
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    frm2 = ""
                    frm2 = GetQueryStringVal("frm2")
                Else : frm2 = ""
                End If


                m_aryRightsForPage1 = GetUserRightsForPage_Other(10, 10)
                litMessage.Text = ""
                If Not IsPostBack Then
                    'BindPickListCombo()
                    ' = "Opportunity"
                    objCommon.sb_FillComboFromDBwithSel(ddlOrderStatus, 176, Session("DomainID"))
                    ' ddlSortDeals.SelectedIndex = Session("SDeals")

                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                        txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                        txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                        txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))

                        txtCustomer.Text = CCommon.ToString(PersistTable(PersistKey.OrgName))

                        If Not ddlSortSalesFulFillment.Items.FindByValue(CCommon.ToString(PersistTable(PersistKey.FilterBy))) Is Nothing Then
                            ddlSortSalesFulFillment.ClearSelection()
                            ddlSortSalesFulFillment.Items.FindByValue(CCommon.ToString(PersistTable(PersistKey.FilterBy))).Selected = True
                            'Else
                            '    SetDefaultFilter()
                        End If

                        If Not ddlOrderStatus.Items.FindByValue(PersistTable(ddlOrderStatus.ID)) Is Nothing Then
                            ddlOrderStatus.ClearSelection()
                            ddlOrderStatus.Items.FindByValue(PersistTable(ddlOrderStatus.ID)).Selected = True
                        End If

                        If Not ddlBatchAction.Items.FindByValue(PersistTable(ddlBatchAction.ID)) Is Nothing Then
                            ddlBatchAction.ClearSelection()
                            ddlBatchAction.Items.FindByValue(PersistTable(ddlBatchAction.ID)).Selected = True
                        End If

                        'If PersistTable(radPickList.ID) IsNot Nothing Then
                        '    Dim strPickList As String() = PersistTable(radPickList.ID).Split(CChar("~"))

                        '    For i As Integer = 0 To strPickList.Length - 1
                        '        If radPickList.Items.FindItemByValue(strPickList(i)) IsNot Nothing Then
                        '            radPickList.Items.FindItemByValue(strPickList(i)).Checked = True
                        '        End If
                        '    Next
                        'End If
                    End If

                    BindDatagrid()
                End If

                'If ddlSortSalesFulFillment.SelectedValue = "1" Or ddlSortSalesFulFillment.SelectedValue = "2" Then
                If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                    If Not ddlBatchAction.Items.FindByValue("4") Is Nothing Then
                        ddlBatchAction.Items.Remove(ddlBatchAction.Items.FindByValue("4"))
                    End If
                    'btnSaveQtyShipped.Visible = False
                End If

                'Else
                'btnSaveQtyShipped.Visible = False
                'btnCreatePackingSlip.Visible = False
                'End If

                'btnCreatePickList.Attributes.Add("onclick", "return GetRecords('picklist')")
                'btnCreatePackingSlip.Attributes.Add("onclick", "return GetRecords('packingslip')")
                btnWareHouses.Attributes.Add("onclick", "return OpenPickWarehouse()")
                ClientScript.RegisterClientScriptBlock(Me.GetType, "SetTab", "if (parent.parent.frames.length > 0) { parent.parent.SelectTabByValue('1');}else{ parent.SelectTabByValue('1'); } ", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub SavePersistTable()
            Try
                PersistTable.Clear()
                PersistTable.Add(PersistKey.CurrentPage, txtCurrrentPage.Text)
                PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())
                PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
                PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text.Trim())
                PersistTable.Add(PersistKey.OrgName, txtCustomer.Text.Trim())
                PersistTable.Add(PersistKey.FilterBy, ddlSortSalesFulFillment.SelectedValue)
                PersistTable.Add(ddlOrderStatus.ID, ddlOrderStatus.SelectedValue)
                PersistTable.Add(ddlBatchAction.ID, ddlBatchAction.SelectedValue)

                'If radPickList.CheckedItems.Count > 0 Then
                '    Dim strPickList As String() = New [String](radPickList.CheckedItems.Count - 1) {}
                '    Dim j As Integer
                '    For Each item As RadComboBoxItem In radPickList.CheckedItems
                '        strPickList(j) = item.Value
                '        j = j + 1
                '    Next
                '    If strPickList.Length > 0 Then
                '        PersistTable.Add(radPickList.ID, String.Join("~", strPickList))
                '    End If
                'End If

                PersistTable.Save()
            Catch ex As Exception

            End Try
        End Sub
        Sub BindDatagrid(Optional ByVal CreateCol As Boolean = True)
            Try
                Dim dtOpportunity As DataTable
                Dim objOpportunity As New COpportunities
                objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                'Set default filter 

                With objOpportunity
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")
                    .SortOrder = ddlSortSalesFulFillment.SelectedItem.Value
                    .endDate = Now()
                    .SortCharacter = txtSortChar.Text.Trim()
                    .CustName = txtCustomer.Text
                    .OrderStatus = ddlOrderStatus.SelectedValue

                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()

                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0

                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If
                End With

                If txtSortColumn.Text <> "" Then
                    objOpportunity.columnName = txtSortColumn.Text
                Else : objOpportunity.columnName = "opp.bintCreatedDate"
                End If

                objOpportunity.SalesDealsUserRights = m_aryRightsForPage1(RIGHTSTYPE.VIEW)
                dtOpportunity = objOpportunity.SalesFulfillment()

                bizPager.PageSize = Session("PagingRows")
                bizPager.CurrentPageIndex = txtCurrrentPage.Text
                bizPager.RecordCount = objOpportunity.TotalRecords

                SavePersistTable()

                'Session("ListDetails") = SortChar & ",,," & txtCustomer.Text & "," & txtCurrrentPageDeals.Text & "," & txtSortColumn.Text & "," & Session("Asc")

                'If objOpportunity.TotalRecords = 0 Then
                '    tblPaging.Visible = False
                '    lblRecordsDeals.Text = 0
                'Else
                '    tblPaging.Visible = True
                '    lblRecordsDeals.Text = String.Format("{0:#,###}", objOpportunity.TotalRecords)
                '    Dim strTotalPage As String()
                '    Dim decTotalPage As Decimal
                '    decTotalPage = lblRecordsDeals.Text / Session("PagingRows")
                '    decTotalPage = Math.Round(decTotalPage, 2)
                '    strTotalPage = CStr(decTotalPage).Split(".")
                '    If (lblRecordsDeals.Text Mod Session("PagingRows")) = 0 Then
                '        lblTotalDeals.Text = strTotalPage(0)
                '        txtTotalPageDeals.Text = strTotalPage(0)
                '    Else
                '        lblTotalDeals.Text = strTotalPage(0) + 1
                '        txtTotalPageDeals.Text = strTotalPage(0) + 1
                '    End If
                '    txtTotalRecordsDeals.Text = lblRecordsDeals.Text
                'End If

                '   dgDeals.DataSource = dtOpportunity
                ' dgDeals.DataBind()


                Dim i As Integer

                For i = 0 To dtOpportunity.Columns.Count - 1
                    dtOpportunity.Columns(i).ColumnName = dtOpportunity.Columns(i).ColumnName.Replace(".", "").Replace("(", "").Replace(")", "")
                Next
                If CreateCol = True Then
                    Dim bField As BoundField
                    Dim Tfield As TemplateField
                    gvSearch.Columns.Clear()
                    For i = 0 To dtOpportunity.Columns.Count - 1
                        'If dtOpportunity.Columns(i).ColumnName = "Contact Email" Or dtOpportunity.Columns(i).ColumnName = "Organization Name" Or dtOpportunity.Columns(i).ColumnName = "First Name" Or dtOpportunity.Columns(i).ColumnName = "Last Name" Or dtOpportunity.Columns(i).ColumnName = "Opp Name" Or dtOpportunity.Columns(i).ColumnName = "Qty Shipped" Then
                        '    Tfield = New TemplateField
                        '    Dim str As String()
                        '    str = dtOpportunity.Columns(i).ColumnName.Split("~")

                        '    Tfield.HeaderTemplate = New MyTemp1(ListItemType.Header, str(0), str(1), dtOpportunity.Columns(0).ColumnName, dtOpportunity.Columns(1).ColumnName, dtOpportunity.Columns(4).ColumnName, dtOpportunity.Columns(5).ColumnName, dtOpportunity.Columns(2).ColumnName, dtOpportunity.Columns(6).ColumnName, dtOpportunity.Columns(7).ColumnName, dtOpportunity.Columns(8).ColumnName, dtOpportunity.Columns(9).ColumnName, dtOpportunity.Columns(10).ColumnName, dtOpportunity.Columns(11).ColumnName)
                        '    Tfield.ItemTemplate = New MyTemp1(ListItemType.Item, str(0), str(1), dtOpportunity.Columns(0).ColumnName, dtOpportunity.Columns(1).ColumnName, dtOpportunity.Columns(4).ColumnName, dtOpportunity.Columns(5).ColumnName, dtOpportunity.Columns(2).ColumnName, dtOpportunity.Columns(6).ColumnName, dtOpportunity.Columns(7).ColumnName, dtOpportunity.Columns(8).ColumnName, dtOpportunity.Columns(9).ColumnName, dtOpportunity.Columns(10).ColumnName, dtOpportunity.Columns(11).ColumnName)
                        '    gvSearch.Columns.Add(Tfield)
                        'Else
                        If i < 15 Then
                            bField = New BoundField
                            bField.DataField = dtOpportunity.Columns(i).ColumnName
                            bField.Visible = False
                            gvSearch.Columns.Add(bField)
                        Else
                            Tfield = New TemplateField
                            Dim str As String()
                            str = dtOpportunity.Columns(i).ColumnName.Split("~")
                            Tfield.HeaderTemplate = New MyTemp1(ListItemType.Header, str(0), str(1), dtOpportunity.Columns(0).ColumnName, dtOpportunity.Columns(1).ColumnName, dtOpportunity.Columns(4).ColumnName, dtOpportunity.Columns(5).ColumnName, dtOpportunity.Columns(2).ColumnName, dtOpportunity.Columns(6).ColumnName, dtOpportunity.Columns(7).ColumnName, dtOpportunity.Columns(8).ColumnName, dtOpportunity.Columns(9).ColumnName, dtOpportunity.Columns(10).ColumnName, dtOpportunity.Columns(11).ColumnName, str(2), dtOpportunity.Columns(12).ColumnName)

                            Tfield.ItemTemplate = New MyTemp1(ListItemType.Item, str(0), str(1), dtOpportunity.Columns(0).ColumnName, dtOpportunity.Columns(1).ColumnName, dtOpportunity.Columns(4).ColumnName, dtOpportunity.Columns(5).ColumnName, dtOpportunity.Columns(2).ColumnName, dtOpportunity.Columns(6).ColumnName, dtOpportunity.Columns(7).ColumnName, dtOpportunity.Columns(8).ColumnName, dtOpportunity.Columns(9).ColumnName, dtOpportunity.Columns(10).ColumnName, dtOpportunity.Columns(11).ColumnName, str(2), dtOpportunity.Columns(12).ColumnName)
                            gvSearch.Columns.Add(Tfield)
                        End If
                        'End If
                    Next

                    'Tfield = New TemplateField

                    'Tfield.HeaderTemplate = New MyTemp1(ListItemType.Header, "Lot/Serial #s", "Lot/Serial #s", dtOpportunity.Columns(0).ColumnName, dtOpportunity.Columns(1).ColumnName, dtOpportunity.Columns(4).ColumnName, dtOpportunity.Columns(5).ColumnName, dtOpportunity.Columns(2).ColumnName, dtOpportunity.Columns(6).ColumnName, dtOpportunity.Columns(7).ColumnName, dtOpportunity.Columns(8).ColumnName, dtOpportunity.Columns(9).ColumnName, dtOpportunity.Columns(10).ColumnName, dtOpportunity.Columns(11).ColumnName)
                    'Tfield.ItemTemplate = New MyTemp1(ListItemType.Item, "Lot/Serial #s", "Lot/Serial #s", dtOpportunity.Columns(0).ColumnName, dtOpportunity.Columns(1).ColumnName, dtOpportunity.Columns(4).ColumnName, dtOpportunity.Columns(5).ColumnName, dtOpportunity.Columns(2).ColumnName, dtOpportunity.Columns(6).ColumnName, dtOpportunity.Columns(7).ColumnName, dtOpportunity.Columns(8).ColumnName, dtOpportunity.Columns(9).ColumnName, dtOpportunity.Columns(10).ColumnName, dtOpportunity.Columns(11).ColumnName)
                    'gvSearch.Columns.Add(Tfield)

                    Tfield = New TemplateField
                    Tfield.HeaderTemplate = New MyTemp1(ListItemType.Header, "CheckBox", "", dtOpportunity.Columns(0).ColumnName, dtOpportunity.Columns(1).ColumnName, dtOpportunity.Columns(4).ColumnName, dtOpportunity.Columns(5).ColumnName, dtOpportunity.Columns(2).ColumnName, dtOpportunity.Columns(6).ColumnName, dtOpportunity.Columns(7).ColumnName, dtOpportunity.Columns(8).ColumnName, dtOpportunity.Columns(9).ColumnName, dtOpportunity.Columns(10).ColumnName, dtOpportunity.Columns(11).ColumnName, "0", dtOpportunity.Columns(12).ColumnName)
                    Tfield.ItemTemplate = New MyTemp1(ListItemType.Item, "CheckBox", "", dtOpportunity.Columns(0).ColumnName, dtOpportunity.Columns(1).ColumnName, dtOpportunity.Columns(4).ColumnName, dtOpportunity.Columns(5).ColumnName, dtOpportunity.Columns(2).ColumnName, dtOpportunity.Columns(6).ColumnName, dtOpportunity.Columns(7).ColumnName, dtOpportunity.Columns(8).ColumnName, dtOpportunity.Columns(9).ColumnName, dtOpportunity.Columns(10).ColumnName, dtOpportunity.Columns(11).ColumnName, "0", dtOpportunity.Columns(12).ColumnName)
                    gvSearch.Columns.Add(Tfield)
                End If
                gvSearch.DataSource = dtOpportunity
                gvSearch.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub



        Private Sub ddlSortDeals_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSortSalesFulFillment.SelectedIndexChanged
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        'Private Sub dgDeals_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgDeals.SortCommand
        '    strColumn = e.SortExpression.ToString()
        '    If Viewstate("Column") <> strColumn Then
        '        Viewstate("Column") = strColumn
        '        Session("Asc") = 0
        '    Else
        '        If Session("Asc") = 0 Then
        '            Session("Asc") = 1
        '        Else
        '            Session("Asc") = 0
        '        End If
        '    End If
        '    BindDatagrid()
        'End Sub

        Function ReturnDateTime(ByVal CloseDate) As String
            Try
                Dim strTargetResolveDate As String = ""
                If Not IsDBNull(CloseDate) Then strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
                Return strTargetResolveDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ReturnType(ByVal Type)
            Try
                Dim strType As String
                If Type = 1 Then
                    strType = "Sales"
                ElseIf Type = 2 Then
                    strType = "Purchase"
                ElseIf Type = 3 Then
                    strType = "Sales Deals from Portal"
                End If
                Return strType
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ReturnMoney(ByVal Money)
            Try
                If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Protected Overrides Sub Finalize()
            MyBase.Finalize()
        End Sub

        Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub PrintPickList()
            'Try
            If (txtSelOppId.Text <> "") Then
                SavePersistTable()

                Dim objOpp As New COpportunities
                Dim ds As DataSet
                objOpp.DomainID = Session("DomainID")
                'objOpp.bitAll = chkSelectAll.Checked

                objOpp.strItemDtls = txtSelOppId.Text
                objOpp.BizDocId = enmBizDocTemplate_BizDocID.Packing_Slip_Template
                ds = objOpp.GetSFItemsforImport

                Dim dt As DataTable = ds.Tables(0)
                Dim strArr() As String = txtSelOppAndValues.Text.Split(",")
                Dim ht As New Hashtable
                For i As Integer = 0 To strArr.Length - 1
                    If strArr(i).Length > 0 Then
                        'WarehouseItemID~Qty to pick
                        If ht.ContainsKey(strArr(i).Split("~")(0)) Then
                            ht(strArr(i).Split("~")(0)) = CCommon.ToInteger(ht(strArr(i).Split("~")(0))) + CCommon.ToInteger(strArr(i).Split("~")(1))
                        Else
                            ht.Add(strArr(i).Split("~")(0), strArr(i).Split("~")(1))
                        End If
                    End If
                Next

                For Each dr As DataRow In dt.Rows
                    dr("Qty To Pick") = CCommon.ToInteger(dr("Qty To Pick")) + IIf(ht(dr("numWareHouseItemID").ToString) Is Nothing, 0, ht(dr("numWareHouseItemID").ToString))
                Next

                'To fix the bug : 1795
                If Not dt.Columns.Contains("SrNo") Then
                    Dim column As DataColumn = New DataColumn("SrNo")
                    column.DataType = System.Type.GetType("System.Int32")
                    With column
                        .AutoIncrement = True
                        .AutoIncrementSeed = 1
                        .AutoIncrementStep = 1
                    End With
                    dt.Columns.Add(column)
                    dt.Columns("SrNo").SetOrdinal(0)
                End If
                dt.AcceptChanges()

                Dim dr1 As DataRow
                Dim intCnt As Integer = 0
                For index As Integer = 0 To dt.Rows.Count - 1
                    dr1 = dt.Rows(index)
                    If Not CCommon.ToInteger(dr1("Qty To Pick")) > 0 Then
                        dr1.Delete()
                    Else
                        intCnt = intCnt + 1
                        dr1("SrNo") = intCnt
                    End If
                Next
                dt.Columns.Remove("numWareHouseItemID")
                dt.AcceptChanges()

                'ModifyExcelDataTable(dt)
                dg.DataSource = dt
                dg.DataBind()
                'dg.Columns(dg.Columns.Count - 1).Visible = False
                'ds = New DataSet
                'ds.Tables.Add(dt.Copy)
                'CSVExport.DataSetToExcel(ds, "PickList_" & Now.Day & Now.Month & Now.Year & Now.Hour & Now.Minute & Now.Second & ".xls", Session("DomainID"))
                ExportToExcel.DataGridToExcel(dg, Response)
            Else
                BindDatagrid()
            End If
            'Catch ex As Exception
            '    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            '    Response.Write(ex)
            'End Try
        End Sub

        'Private Sub btnShipSelected_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShipSelected.Click
        '    Try

        '        BindDatagrid()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub
        'Private Sub SetDefaultFilter()
        '    Try
        '        PersistTable.Load()
        '        If PersistTable.Count > 0 Then
        '            If Not ddlSortSalesFulFillment.Items.FindByValue(PersistTable(PersistKey.FilterBy)) Is Nothing Then
        '                ddlSortSalesFulFillment.ClearSelection()
        '                ddlSortSalesFulFillment.Items.FindByValue(PersistTable(PersistKey.FilterBy)).Selected = True
        '            End If
        '        End If
        '        'Dim objContact As New BusinessLogic.Contacts.CContacts
        '        'Dim dtTable As DataTable
        '        'objContact.DomainID = Session("DomainId")
        '        'objContact.UserCntID = Session("UsercontactId")
        '        'objContact.FormId = 23
        '        'objContact.ContactType = 0
        '        'dtTable = objContact.GetDefaultfilter()
        '        'If dtTable.Rows.Count = 1 Then
        '        '    If Not IsDBNull(dtTable.Rows(0).Item("numfilterId")) Then
        '        '        If Not ddlSortSalesFulFillment.Items.FindByValue(dtTable.Rows(0).Item("numfilterId")) Is Nothing Then
        '        '            ddlSortSalesFulFillment.ClearSelection()
        '        '            ddlSortSalesFulFillment.Items.FindByValue(dtTable.Rows(0).Item("numfilterId")).Selected = True
        '        '        End If
        '        '    End If
        '        'End If
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        Function ValidateSerialLot(ByVal OpportunityId As Integer, ByVal OppItemCode As Integer, ByVal UnitHour As Integer) As Integer
            Dim objOpportunity As New MOpportunity
            Dim dtTable As DataTable
            objOpportunity.OppItemCode = OppItemCode
            objOpportunity.OpportunityId = OpportunityId

            dtTable = objOpportunity.GetSalesOppSerializLot()

            If dtTable.Rows.Count > 0 Then
                If dtTable.Rows(0)("bitSerialized") = True Or dtTable.Rows(0)("bitLotNo") = True Then
                    If dtTable.Rows(0)("numQty") <> UnitHour Then
                        litError.Text += "<br/>" + dtTable.Rows(0)("vcPOppName") + " " + dtTable.Rows(0)("vcItemName")
                        litError.Visible = True
                    Else
                        Return 1
                    End If
                Else
                    Return 1
                End If
            End If
        End Function

        Private Sub SaveQtyShipped()
            Try
                'Dim sb As New System.Text.StringBuilder
                Dim objOpp As New OppBizDocs
                Dim objOpportunity As New MOpportunity
                If txtSelOppId.Text <> "" Then

                    Dim arrRec() As String = txtSelOppId.Text.Split(",")
                    Dim arrItem() As String
                    Dim arrOpp() As String
                    Dim strMessage As String
                    Dim strOppItemID As String
                    Dim intUnitHour As Double

                    litError.Visible = False
                    'litError.Text = "Following items could not be saved your option is to provide respective no of Lot/Serial #s :"

                    For i As Integer = 0 To arrRec.Length - 1
                        arrItem = arrRec(i).Split("~")
                        arrOpp = arrRec(i).Split("!")
                        strOppItemID = arrItem(0).Split("!")(1)
                        intUnitHour = CCommon.ToDouble(arrItem(1))
                        If arrItem.Length = 2 Then
                            If CCommon.ToInteger(strOppItemID) > 0 And intUnitHour > 0 Then

                                'If ValidateSerialLot(CType(arrOpp(0).ToString(), Long), strOppItemID, intUnitHour) = 1 Then
                                objOpp.UnitHour = intUnitHour
                                objOpp.OppItemID = strOppItemID
                                objOpp.UserCntID = Session("UserContactID")

                                strMessage = objOpp.UpdateQtyShipped()
                                If strMessage.Length > 0 Then
                                    litMessage.Text = strMessage
                                    BindDatagrid()
                                    Exit Sub
                                End If
                                If arrOpp(0).Length > 0 Then
                                    objOpportunity.OpportunityId = CType(arrOpp(0).ToString(), Long)
                                    objOpportunity.bitCheck = True
                                    Dim dtItemList As DataTable
                                    dtItemList = objOpportunity.CheckCanbeShipped()

                                    If dtItemList.Rows.Count = 0 Then
                                        '    litMessage.Text = "You can't ship at this time because you don't have enough quantity on allocation to support your shipment. Your options are to modify your order, or replenish inventory (to check inventory click on the edit link within the line item, then the value in the 'Products/Services' column):"
                                        'Else

                                        Dim dtTable As DataTable
                                        dtTable = objOpportunity.ValidateOppSerializLot()

                                        Dim dr As DataRow
                                        litError.Text = "Following items could not be saved (your option is to provide respective no of Lot/Serial #s):"

                                        For Each dr In dtTable.Rows
                                            If dr("Error") = 1 Then
                                                litError.Text += "<br/>" + dr("vcItemName")
                                                BindDatagrid()
                                                Exit Sub
                                            End If
                                        Next

                                        objOpportunity.UserCntID = Session("UserContactID")
                                        objOpportunity.ShippingOrReceived()
                                        litMessage.Text = "Selected Deals are now Closed"
                                    End If
                                End If
                                'End If
                            End If
                        End If
                    Next

                End If
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub PrintPackingSlip()
            'Try
            If (txtSelOppId.Text <> "") Then
                Dim objOpp As New COpportunities
                Dim ds As DataSet
                objOpp.DomainID = Session("DomainID")
                objOpp.BizDocId = enmBizDocTemplate_BizDocID.Packing_Slip_Template
                objOpp.strItemDtls = txtSelOppId.Text
                ds = objOpp.GetSFItemsforImport

                Dim dt As DataTable = ds.Tables(1)
                Dim strArr() As String = txtSelOppAndValues.Text.Split(",")
                Dim ht As New Hashtable
                For i As Integer = 0 To strArr.Length - 1
                    If strArr(i).Length > 0 Then
                        If ht.ContainsKey(strArr(i).Split("~")(0)) Then
                            ht(strArr(i).Split("~")(0)) = strArr(i).Split("~")(1)
                        Else
                            ht.Add(strArr(i).Split("~")(0), strArr(i).Split("~")(1))
                        End If
                    End If
                Next

                For Each dr As DataRow In dt.Rows
                    dr("numUnitHour") = IIf(ht(dr("numoppitemtCode").ToString) Is Nothing, 0, ht(dr("numoppitemtCode").ToString))
                Next
                dt.AcceptChanges()
                If dt.Rows.Count > 0 Then
                    Dim objPDF As New HTMLToPDF

                    Dim intResult As Integer = objPDF.CreatePackingSlipExportPdf(dt)
                    If intResult = 101 Then
                        litMessage.Text = "Please enable ""Packing Slip"" BizDoc from ""Administration->Global Settings->Order Management(subtab)->BizDoc Template"", as well Set One template as default template."
                    ElseIf intResult = 102 Then
                        litMessage.Text = "Please configure Packing Slip line items from ""Administration->BizForm Wizard->BizDoc(Sales)""."
                    ElseIf intResult = 103 Then
                        litMessage.Text = " Please contact support by creating bug or feedback with following error : ""Invalid page break in template."""
                    End If

                    BindDatagrid()
                End If
            Else
                BindDatagrid()
            End If
            'Catch ex As Exception
            '    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            '    Response.Write(ex)
            'End Try
        End Sub

        Private Sub ddlOrderStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlOrderStatus.SelectedIndexChanged
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

#Region "TELERIK COMBO SUBROUTINES/FUNCTIONS"

        'Private Sub ModifyExcelDataTable(ByRef dtModified As DataTable)

        '    Dim dtFinal As New DataTable
        '    For Each item As RadComboBoxItem In radPickList.CheckedItems
        '        If Not dtFinal.Columns.Contains("SrNo") Then dtFinal.Columns.Add("SrNo")
        '        If dtModified.Columns.Contains(item.Text) Then
        '            If item.Text.ToLower = "numWareHouseItemID".ToLower Then Continue For
        '            dtFinal.Columns.Add(item.Text)
        '        End If
        '    Next

        '    If radPickList.CheckedItems.Count > 0 Then
        '        For Each drRow As DataRow In dtModified.Rows
        '            dtFinal.ImportRow(drRow)
        '        Next
        '        dtFinal.AcceptChanges()
        '        dtModified = dtFinal
        '    End If
        'End Sub

        'Private Sub BindPickListCombo()
        '    Try
        '        Dim objOpp As New COpportunities
        '        Dim ds As DataSet
        '        objOpp.DomainID = Session("DomainID")
        '        objOpp.BizDocId = enmBizDocTemplate_BizDocID.Packing_Slip_Template
        '        objOpp.strItemDtls = txtSelOppId.Text
        '        ds = objOpp.GetSFItemsforImport

        '        Dim dt As DataTable = ds.Tables(0)
        '        Dim dtColums As New DataTable
        '        dtColums.Columns.Add("KeyField")
        '        dtColums.AcceptChanges()

        '        Dim drRow As DataRow
        '        Dim intCnt As Integer = 0
        '        For Each dtCol As DataColumn In dt.Columns
        '            If dtCol.ColumnName.ToLower = "numWareHouseItemID".ToLower Then Continue For
        '            drRow = dtColums.NewRow
        '            drRow("KeyField") = dtCol.ColumnName
        '            dtColums.Rows.Add(drRow)
        '        Next
        '        dtColums.AcceptChanges()

        '        If dtColums IsNot Nothing AndAlso dtColums.Rows.Count > 0 Then
        '            radPickList.DataSource = dtColums
        '            radPickList.DataTextField = "KeyField"
        '            radPickList.DataValueField = "KeyField"
        '            radPickList.ShowDropDownOnTextboxClick = True
        '            radPickList.CheckBoxes = True
        '            radPickList.EmptyMessage = "-- Select Pick List Columns --"
        '            radPickList.Width = 175
        '            radPickList.EnableTextSelection = True
        '            ' radPickList.EnableCheckAllItemsCheckBox = True
        '            radPickList.DataBind()
        '        End If
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub
#End Region

        Private Sub btnGoBatch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGoBatch.Click
            'Try

            Select Case ddlBatchAction.SelectedValue
                Case 1 'picklist
                    PrintPickList()
                Case 2 'packing slip
                    PrintPackingSlip()
                Case 3 'Shipping label

                Case 4 'Save Qty Shipped
                    SaveQtyShipped()
                Case Else

            End Select

            'Catch ex As Exception
            '    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            '    Response.Write(ex)
            'End Try
        End Sub
    End Class


    Public Class MyTemp1
        Implements ITemplate

        Dim TemplateType As ListItemType
        Dim Field1, Field2, Field3, Field4, Field5, Field6, Field7, Field8, Field9, Field10, Field11, Field12, Field13, Field14, Field15 As String
        Sub New(ByVal type As ListItemType, ByVal fld1 As String, ByVal fld2 As String, ByVal fld3 As String, ByVal fld4 As String, ByVal fld5 As String, ByVal fld6 As String, ByVal fld7 As String, ByVal fld8 As String, ByVal fld9 As String, ByVal fld10 As String, ByVal fld11 As String, ByVal fld12 As String, ByVal fld13 As String, ByVal fld14 As String, ByVal fld15 As String)
            Try
                TemplateType = type
                Field1 = fld1
                Field2 = fld2
                Field3 = fld3
                Field4 = fld4
                Field5 = fld5
                Field6 = fld6
                Field7 = fld7
                Field8 = fld8
                Field9 = fld9
                Field10 = fld10
                Field11 = fld11

                Field12 = fld12
                Field13 = fld13
                Field14 = fld14
                Field15 = fld15

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub InstantiateIn(ByVal Container As Control) Implements ITemplate.InstantiateIn
            Try
                Dim lbl1 As Label = New Label()
                Dim lbl2 As Label = New Label()
                Dim lbl3 As Label = New Label()
                Dim lblQtyFulfilled As Label = New Label()
                Dim lblQtyShipped As Label = New Label()
                Dim lblQtyOrdered As Label = New Label()
                Dim lblWarehouseItemID As Label = New Label()
                Dim lnkButton As New LinkButton
                Dim lnk As New HyperLink

                Select Case TemplateType
                    Case ListItemType.Header
                        If Field1 <> "CheckBox" Then
                            '   lnk.Attributes.Add("onclick", "return FilterWithinRecords('" & Field1 & "','" & Field2 & "','divColHeader')")
                            '   lnk.CssClass = "LinkArrow1"
                            '   lnk.Text = "8"
                            '   Container.Controls.Add(lnk)

                            Field1 = IIf(Field1 = "Opp Name Status", "Sales Order Name (Status)", Field1)

                            If Field14 = "0" Then
                                lbl1.ID = Field2
                                lbl1.Text = Field1
                                Container.Controls.Add(lbl1)
                            ElseIf Field14 = "1" Then
                                lnkButton.ID = Field2
                                lnkButton.Text = Field1
                                lnkButton.Attributes.Add("onclick", "return SortColumn('" & Field2 & "')")
                                Container.Controls.Add(lnkButton)
                            End If
                        Else
                            Dim chk As New CheckBox
                            chk.ID = "chkSelectAll"
                            'chk.Attributes.Add("onclick", "return SelectAll('chkAll')")
                            Container.Controls.Add(chk)
                        End If
                    Case ListItemType.Item
                        If Field1 <> "CheckBox" Then
                            If Field2 = "numQtyShipped" Then
                                Dim txtQtyShipped As New TextBox
                                AddHandler txtQtyShipped.DataBinding, AddressOf BindQtyShipped
                                Container.Controls.Add(txtQtyShipped)
                            Else
                                AddHandler lbl1.DataBinding, AddressOf BindStringColumn
                                Container.Controls.Add(lbl1)
                            End If
                        Else
                            Dim chk As New CheckBox
                            chk.ID = "chkSelect"
                            'chk.CssClass = "chkSelect"

                            AddHandler lbl1.DataBinding, AddressOf Bindvalue
                            AddHandler lbl2.DataBinding, AddressOf BindvalueTerr
                            AddHandler lbl3.DataBinding, AddressOf BindvalueOppItemID
                            AddHandler lblQtyFulfilled.DataBinding, AddressOf BindvalueQtyFulFilled
                            AddHandler lblQtyShipped.DataBinding, AddressOf BindvalueQtyShipped
                            AddHandler lblWarehouseItemID.DataBinding, AddressOf BindvalueWarehouseItemID
                            AddHandler lblQtyOrdered.DataBinding, AddressOf BindvalueQtyOrdered

                            chk.Attributes.Add("onclick", "SwapQty();")
                            Container.Controls.Add(chk)
                            Container.Controls.Add(lbl1)
                            Container.Controls.Add(lbl2)
                            Container.Controls.Add(lbl3)
                            Container.Controls.Add(lblQtyFulfilled)
                            Container.Controls.Add(lblQtyShipped)
                            Container.Controls.Add(lblWarehouseItemID)
                            Container.Controls.Add(lblQtyOrdered)

                        End If
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub Bindvalue(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
                lbl1.ID = "lbl1"
                lbl1.Text = DataBinder.Eval(Container.DataItem, Field6)
                lbl1.Attributes.Add("style", "display:none")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindvalueTerr(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl2 As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lbl2.NamingContainer, GridViewRow)
                lbl2.ID = "lbl2"
                lbl2.Text = DataBinder.Eval(Container.DataItem, Field7)
                lbl2.Attributes.Add("style", "display:none")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindvalueOppItemID(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl3 As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lbl3.NamingContainer, GridViewRow)
                lbl3.ID = "lbl3"
                lbl3.Text = DataBinder.Eval(Container.DataItem, Field8)
                lbl3.Attributes.Add("style", "display:none")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub BindvalueQtyFulFilled(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lblQtyFulfilled As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lblQtyFulfilled.NamingContainer, GridViewRow)
                lblQtyFulfilled.ID = "lblQtyFulfilled"
                lblQtyFulfilled.Text = DataBinder.Eval(Container.DataItem, Field10)
                lblQtyFulfilled.Attributes.Add("style", "display:none")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub BindvalueQtyShipped(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lblQtyShipped As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lblQtyShipped.NamingContainer, GridViewRow)
                lblQtyShipped.ID = "lblQtyShipped"
                lblQtyShipped.Text = DataBinder.Eval(Container.DataItem, Field9)
                lblQtyShipped.Attributes.Add("style", "display:none")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub BindvalueWarehouseItemID(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lblWarehouseItemID As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lblWarehouseItemID.NamingContainer, GridViewRow)
                lblWarehouseItemID.ID = "lblWarehouseItemID"
                lblWarehouseItemID.Text = DataBinder.Eval(Container.DataItem, Field11)
                lblWarehouseItemID.Attributes.Add("style", "display:none")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub BindvalueQtyOrdered(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lblQtyOrdered As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lblQtyOrdered.NamingContainer, GridViewRow)
                lblQtyOrdered.ID = "lblQtyOrdered"
                lblQtyOrdered.Text = DataBinder.Eval(Container.DataItem, "QtyOrdered")
                lblQtyOrdered.Attributes.Add("style", "display:none")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub BindStringColumn(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
                If Field2 = "vcCompanyName" Or Field2 = "vcFirstName" Or Field2 = "vcLastName" Or Field2 = "vcPoppName" Or Field2 = "numInTransit" Then
                    lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2 & "~" & Field14)), "", DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2 & "~" & Field14))
                    Dim intermediatory As Integer
                    intermediatory = IIf(System.Web.HttpContext.Current.Session("EnableIntMedPage") = 1, 1, 0)
                    If Field2 = "vcCompanyName" Then
                        If DataBinder.Eval(Container.DataItem, Field5) = 0 Then
                            lbl1.Text = "<a  href='javascript:OpenWindow(" & DataBinder.Eval(Container.DataItem, Field4) & ",0," & intermediatory & ")'>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                        ElseIf DataBinder.Eval(Container.DataItem, Field5) = 1 Then
                            lbl1.Text = "<a  href='javascript:OpenWindow(" & DataBinder.Eval(Container.DataItem, Field4) & ",1," & intermediatory & ")'>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                        ElseIf DataBinder.Eval(Container.DataItem, Field5) = 2 Then
                            lbl1.Text = "<a  href='javascript:OpenWindow(" & DataBinder.Eval(Container.DataItem, Field4) & ",2," & intermediatory & ")'>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                        End If
                    ElseIf Field2 = "vcFirstName" Or Field2 = "vcLastName" Then
                        lbl1.Text = "<a  href='javascript:OpenContact(" & DataBinder.Eval(Container.DataItem, Field3) & "," & intermediatory & ")'>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                        'ElseIf Field1 = "Contact Email" Then
                        '    lbl1.Attributes.Add("onclick", "return OpemEmail('" & lbl1.Text.ToString & "'," & DataBinder.Eval(Container.DataItem, Field3) & ")")
                        '    lbl1.Text = "<a  href=#>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"
                    ElseIf Field2 = "vcPoppName" Then
                        lbl1.Text = "<a  href='javascript:OpenAddBizDoc(" & DataBinder.Eval(Container.DataItem, Field6) & ")'>" & lbl1.Text & "</a> &nbsp;&nbsp;&nbsp;"

                        If DataBinder.Eval(Container.DataItem, "bitKitParent") = True Then
                            lbl1.Text += "<a  href='javascript:OpenOppKitItems(" & DataBinder.Eval(Container.DataItem, "numOppId") & "," & DataBinder.Eval(Container.DataItem, "numOppItemtCode") & ")'>(Kit Items)</a>"
                        End If
                    ElseIf Field2 = "numInTransit" Then
                        If CCommon.ToLong(lbl1.Text) > 0 Then
                            lbl1.Text = "<a  href='javascript:OpenInTransit(" & DataBinder.Eval(Container.DataItem, Field15) & ")'>" & lbl1.Text & "</a>"
                        Else
                            lbl1.Text = ""
                        End If
                    End If
                ElseIf Field1 = "Lot/Serial #s" Then
                    If DataBinder.Eval(Container.DataItem, Field12) = True Or DataBinder.Eval(Container.DataItem, Field13) = True Then
                        lbl1.Text = "<a  href='#' onclick='javascript:OpenConfSerItem(" & DataBinder.Eval(Container.DataItem, Field6) & "," & DataBinder.Eval(Container.DataItem, Field8) & ")'>Serialize</a> &nbsp;&nbsp;&nbsp;"
                    End If
                Else : lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2 & "~" & Field14)), "", DataBinder.Eval(Container.DataItem, Field1 & "~" & Field2 & "~" & Field14))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindStringColumn1(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Dim i As Integer = 2
        Sub BindQtyShipped(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim txtBox As TextBox = CType(Sender, TextBox)
                Dim Container As GridViewRow = CType(txtBox.NamingContainer, GridViewRow)
                txtBox.ID = "txtQtyShipped"
                txtBox.Width = Unit.Pixel(50)
                txtBox.CssClass = "signup"
                txtBox.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, Field9)), 0, DataBinder.Eval(Container.DataItem, Field9))
                txtBox.Attributes.Add("onkeyup", "return validate('" & i.ToString & "');")
                txtBox.Attributes.Add("onkeypress", "return CheckNumber(2,event)")
                i = i + 1
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
    End Class


End Namespace