﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSalesFulfullment.aspx.vb"
    Inherits="BACRM.UserInterface.Opportunities.frmSalesFulfullment" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register TagPrefix="menu1" TagName="Menu" Src="../include/webmenu.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Src="../common/frmBizSorting.ascx" TagName="frmBizSorting" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Sales Fulfillment</title>
    <script language="javascript" type="text/javascript">
        function PopupCheck() {
            document.getElementById("btnGo1").click();
        }
        function GetRecords(flag) {
            var OppId = '';
            var OppIdAndValue = '';
            var QtyDiff = 0;
            for (var i = 2; i <= document.getElementById('ctl00_GridPlaceHolder_gvSearch').rows.length; i++) {
                if (i < 10) {
                    str = '0' + i
                }
                else {
                    str = i
                }
                if (document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_chkSelect').checked == true) {

                    QtyDiff = parseInt(document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_txtQtyShipped').value) - parseInt(document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_lblQtyShipped').innerHTML)

                    if (OppId == '') {
                        OppId = document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_lbl3').innerHTML;
                        if (flag == 'picklist') {
                            OppIdAndValue = parseInt(document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_lblWarehouseItemID').innerHTML) + '~' + QtyDiff;
                        }
                        else {
                            OppIdAndValue = document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_lbl3').innerHTML + '~' + QtyDiff;
                        }
                    }
                    else {
                        OppId = OppId + ',' + document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_lbl3').innerHTML;
                        if (flag == 'picklist') {
                            OppIdAndValue = OppIdAndValue + ',' + parseInt(document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_lblWarehouseItemID').innerHTML) + '~' + QtyDiff;
                        } else {
                            OppIdAndValue = OppIdAndValue + ',' + document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_lbl3').innerHTML + '~' + QtyDiff;
                        }
                    }
                }
            }
            document.getElementById('txtSelOppId').value = OppId;
            document.getElementById('txtSelOppAndValues').value = OppIdAndValue;
            //            alert(OppIdAndValue);
            return true;
        }
        function GetRecordsOpp() {
            if (confirm("This Deal will now be removed from the 'Open Deals' section, and will reside only in the 'Closed Deals' section within the Organization the deal is for. Except for BizDocs or any Projects that depend on BizDocs - Modifications to Deal Details, Milestones & Stages, Associated Contacts, and Products / Services, will no longer be allowed.")) {
                var OppId = '';
                var ItemCode = '';
                var Qty;
                var selected = '';
                for (var i = 2; i <= document.getElementById('ctl00_GridPlaceHolder_gvSearch').rows.length; i++) {
                    if (i < 10) {
                        str = '0' + i
                    }
                    else {
                        str = i
                    }
                    if (document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_chkSelect').checked == true) {
                        Qty = document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_txtQtyShipped').value;
                        ItemCode = document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_lbl3').innerHTML;
                        if (OppId == '') {
                            OppId = document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_lbl1').innerHTML + '!' + ItemCode + '~' + Qty;
                        }
                        else {
                            OppId = OppId + ',' + document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_lbl1').innerHTML + '!' + ItemCode + '~' + Qty;
                        }
                    }
                }
                document.getElementById('txtSelOppId').value = OppId
                //                alert(OppId);
                return true;
            }
            else {
                return false;
            }
        }
        function SwapQty() {
            //            for (var i = 2; i <= document.getElementById('ctl00_GridPlaceHolder_gvSearch').rows.length; i++) {
            //                if (i < 10) {
            //                    str = '0' + i
            //                }
            //                else {
            //                    str = i
            //                }
            //                if (document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_chkSelect').checked == true) {

            //                    document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_txtQtyShipped').value = document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_lblQtyOrdered').innerHTML
            //                }
            //                else {
            //                    document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_txtQtyShipped').value = document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_lblQtyShipped').innerHTML
            //                }
            //            }
            $('#ctl00_GridPlaceHolder_gvSearch tr').each(function () {
                if ($(this).find("input[id*='chkSelect']").is(':checked')) {
                    $(this).find("input[id*='txtQtyShipped']").val($(this).find("[id*='lblQtyOrdered']").text());
                }
                else {
                    $(this).find("input[id*='txtQtyShipped']").val($(this).find("[id*='lblQtyShipped']").text());
                }
            });
        }
        //        function SelectAll(a) {
        //            var str;

        //            if (typeof (a) == 'string') {
        //                a = document.getElementById(a)
        //            }
        //            if (a.checked == true) {
        //                for (var i = 1; i <= document.getElementById('ctl00_GridPlaceHolder_gvSearch').rows.length; i++) {
        //                    if (i < 10) {
        //                        str = '0' + i
        //                    }
        //                    else {
        //                        str = i
        //                    }
        //                    document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_chkSelect').checked = true;
        //                    if (i > 1) { validate(i); }
        //                }
        //            }
        //            else if (a.checked == false) {
        //                for (var i = 1; i <= document.getElementById('ctl00_GridPlaceHolder_gvSearch').rows.length; i++) {
        //                    if (i < 10) {
        //                        str = '0' + i
        //                    }
        //                    else {
        //                        str = i
        //                    }
        //                    document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_chkSelect').checked = false;
        //                }
        //            }

        //            SwapQty();
        //        }

        $(document).ready(function () {
            var chkBox = $("input[id$='chkSelectAll']");
            chkBox.click(
          function () {
              $("#ctl00_GridPlaceHolder_gvSearch INPUT[type='checkbox']").prop('checked', chkBox.is(':checked'));
              SwapQty();
          });

            $("#btnGoBatch").click(function () {

                var selection = $("#ddlBatchAction").val()
                if (selection == 1) {
                    return GetRecords('picklist');

                } else if (selection == 2)
                { return GetRecords('packingslip'); }
                else if (selection == 4)
                { return GetRecordsOpp(); }
                //                console.log(document.getElementById('txtSelOppId').value);
                //                console.log(document.getElementById('txtSelOppAndValues').value);

            })

        });

        function OpenWindow(a, b, c) {
            var str;
            if (b == 0) {

                str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&DivID=" + a;

            }
            else if (b == 1) {

                str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&DivID=" + a;

            }
            else if (b == 2) {

                str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&klds+7kldf=fjk-las&DivId=" + a;

            }

            document.location.href = str;

        }
        function OpenContact(a, b) {
            var str;
            str = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&ft6ty=oiuy&CntId=" + a;
            document.location.href = str;
        }
        function SelWarOpenDeals(a) {
            window.open("../opportunity/frmSelWhouseOpenDls.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Opid=" + a)
            return false;
        }
        function OpenSetting() {

            window.open('../Items/frmConfItemList.aspx?FormID=23', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        function OpenAddBizDoc(a) {

            var str;
            str = "../opportunity/frmAddBizDocs.aspx?frm=SalesFulfillment&OppType=1&OpID=" + a;
            document.location.href = str;
        }
        function OpenPickWarehouse() {

            window.open('../opportunity/frmPickWarehouse.aspx?FormID=23', '', 'toolbar=no,titlebar=no,top=200,left=200,width=600,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function validate(i) {
            if (i < 10) {
                str = '0' + i
            }
            else {
                str = i
            }
            var OldQtyShipped = document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_lblQtyShipped').innerHTML;
            var NewQtyShipped = document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_txtQtyShipped').value;
            var QtyFulfilled = document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_lblQtyFulfilled').innerHTML;
            var QtyOrdered = document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_lblQtyOrdered').innerHTML;

            //            if (NewQtyShipped == '') {
            //                document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_txtQtyShipped').value='0';
            //                return false;
            //            }
            if (parseInt(NewQtyShipped) < parseInt(OldQtyShipped)) {
                //document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_txtQtyShipped').value = document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_lblQtyShipped').innerHTML;
                document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_txtQtyShipped').value = OldQtyShipped;
                alert('You can not Unship Item');
                return false;
            }
            //            alert(QtyFulfilled);
            //            alert(NewQtyShipped);
            //            if (parseInt(QtyFulfilled) < parseInt(NewQtyShipped)) {
            //                document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_txtQtyShipped').value = document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_lblQtyShipped').innerHTML;
            //                alert('You can not ship more than Qty Fulfilled');
            //                return false;
            //            }
            if (parseInt(QtyOrdered) < parseInt(NewQtyShipped)) {
                document.getElementById('ctl00_GridPlaceHolder_gvSearch_ctl' + str + '_txtQtyShipped').value = OldQtyShipped;
                alert('You can not ship more than Qty Ordered');
                return false;
            }
            return true;
        }
        function OpenConfSerItem(a, b) {
            window.open('../opportunity/frmAddSerializedItem.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID=' + a + '&ItemCode=' + b + '&OppType=1', '', 'toolbar=no,titlebar=no,left=100,top=100,width=530,height=400,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenInTransit(a) {
            window.open('../opportunity/frmOrderTransit.aspx?ItemCode=' + a, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=yes,resizable=yes')
        }

        function OpenOppKitItems(a, b) {
            window.open('../opportunity/frmOppKitItems.aspx?OppID=' + a + '&OppItemCode=' + b, '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table align="right">
                <tr>
                    <td>
                        <%--
Fully Invoiced Items (Un-shipped/Completed)
   Definition – All items added in completely to an invoice (no item is in a partially fulfilled state).
Partially Invoiced Items (Un-shipped/Completed)
   Definition – Items added to an invoice where there is still a balance to fulfill
Non-invoiced items (All)
   Definition – All items in open S.O.s  not yet partially or fully fulfilled /  invoiced.--%>
                        <asp:DropDownList ID="ddlSortSalesFulFillment" runat="server" CssClass="signup" AutoPostBack="True">
                            <%--<asp:ListItem Value="1">Shippable Inventory Items (w/o partials)</asp:ListItem>
                    <asp:ListItem Value="2">Shippable Inventory Items (w partials)</asp:ListItem>
                    <asp:ListItem Value="3">Only Partially Shippable Inventory Items</asp:ListItem>
                    <asp:ListItem Value="4">Shippable Invoiced Items</asp:ListItem>--%>
                            <asp:ListItem Value="1">Fully Invoiced Inventory Items</asp:ListItem>
                            <asp:ListItem Value="2">Partially Invoiced Inventory Items</asp:ListItem>
                            <asp:ListItem Value="3">Un-shipped non-invoiced inventory items</asp:ListItem>
                            <asp:ListItem Value="4">Shipped items that need to be invoiced</asp:ListItem>
                            <asp:ListItem Value="0">All Open Inventory Items</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td>
                        <label>
                            Order Status</label>
                    </td>
                    <td align="left">
                        <asp:DropDownList ID="ddlOrderStatus" runat="server" CssClass="signup" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                    <td class="normal1">
                        <label>
                            Organization</label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtCustomer" runat="server" Width="55px" CssClass="signup"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Button ID="btnGo" CssClass="button" runat="server" Text="Go"></asp:Button>
                    </td>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table align="right">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litError" runat="server" Visible="false"></asp:Literal><br />
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
            <td align="right">
                <%--<telerik:RadComboBox ID="radPickList" runat="server" CheckBoxes="true">
                </telerik:RadComboBox>--%>
            </td>
            <td align="right">
                <%--<asp:Button ID="btnSaveQtyShipped" CssClass="button" runat="server" Text="Save Qty Shipped / Close Sales Order"
                    OnClientClick="return GetRecordsOpp()"></asp:Button>--%>
            </td>
            <td align="right">
                <%--<asp:Button ID="btnShipSelected" runat="server" CssClass="button" Text="Ship Selected Orders" />--%>
                <asp:Button ID="btnWareHouses" runat="server" CssClass="button" Text="Warehouse(s)" />
            </td>
            <td align="right">
                Actions:
                <asp:DropDownList runat="server" ID="ddlBatchAction">
                    <%--Do not change Value of this items as permission is coded based on list value--%>
                    <asp:ListItem Text="Print Pick List" Value="1" />
                    <asp:ListItem Text="Print Packing Slip" Value="2" />
                    <%--<asp:ListItem Text="Print Shipping Label" Value="3" />--%>
                    <asp:ListItem Text="Save Qty Shipped / Close Sales Order" Value="4" />
                </asp:DropDownList>
                <asp:Button ID="btnGoBatch" CssClass="button" runat="server" Text="Go"></asp:Button>
                <%--<asp:Button ID="btnCreatePickList" runat="server" CssClass="button" Text="Create Pick List" Visible="false" />--%>
            </td>
            <td align="right">
                <%--<asp:Button ID="btnCreatePackingSlip" runat="server" CssClass="button" Text="Create Packing Slip" Visible="false" />--%>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridSettingPopup" runat="server"
    ClientIDMode="Static">
    <a href="#" onclick="return OpenSetting()" title="Show Grid Settings.">
        <img src="../images/NewUiImages/setting-icon.gif" alt="" /></a>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Sales Fulfillment
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridBizSorting" runat="server" ClientIDMode="Static">
    <uc1:frmBizSorting ID="frmBizSorting2" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server" Direction="RightToLeft" HorizontalAlign="Right"
        LayoutType="div" UrlPaging="false" CssClass="pagn" ShowMoreButtons="true" ShowPageIndexBox="Never"
        Width="" AlwaysShow="true" ShowCustomInfoSection="Left" CustomInfoHTML="Showing records %startrecordindex% to %endrecordindex% of %recordcount% "
        CustomInfoSectionWidth="300px" CustomInfoStyle="line-height:20px;margin-right:3px;text-align:right !important;">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:GridView ID="gvSearch" runat="server" EnableViewState="true" AutoGenerateColumns="false"
        CssClass="tbl" Width="100%" ClientIDMode="AutoID">
        <Columns>
        </Columns>
    </asp:GridView>
    <asp:DataGrid ID="dg" runat="server" Style="display: none">
    </asp:DataGrid>
    <asp:TextBox ID="txtSelOppId" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSelOppAndValues" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPageDeals" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecordsDeals" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
