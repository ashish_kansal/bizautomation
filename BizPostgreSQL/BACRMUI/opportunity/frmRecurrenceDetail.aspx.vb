﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Workflow
Imports System.IO

Public Class frmRecurrenceDetail
    Inherits BACRMPage

#Region "Member Variables"

    Private objBizRecurrence As BizRecurrence

#End Region

#Region "Constructor"

    Sub New()
        objBizRecurrence = New BizRecurrence()
    End Sub

#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblMessage.Text = ""

            If Not Page.IsPostBack Then
                If CCommon.ToLong(GetQueryStringVal("RecurConfigID")) > 0 Then
                    hdnRecurConfigID.Value = GetQueryStringVal("RecurConfigID")
                    BindData()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub BindData()
        Try
            Dim ds As DataSet = objBizRecurrence.GetRecurrenceDetail(CCommon.ToLong(hdnRecurConfigID.Value))

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                'Recurrence Detail
                lblRecurType.Text = CCommon.ToString(ds.Tables(0).Rows(0)("vcType"))
                lblFrequency.Text = CCommon.ToString(ds.Tables(0).Rows(0)("vcFrequency"))
                lblStartDate.Text = CCommon.ToString(ds.Tables(0).Rows(0)("dtStartDate"))

                If CCommon.ToString(ds.Tables(0).Rows(0)("numType")) = "1" Then
                    hdnRecurrenceType.Value = "1"
                    lblRecurTitle.Text = "Recurred Orders:"
                    lblEndDate.Text = CCommon.ToString(ds.Tables(0).Rows(0)("dtEndDate"))
                Else
                    hdnRecurrenceType.Value = "2"
                    lblRecurTitle.Text = "Recurred BizDocs:"
                    lblEndDateTitle.Visible = False
                    lblEndDate.Visible = False
                End If

                lblNextRecurrenceDate.Text = CCommon.ToString(ds.Tables(0).Rows(0)("dtNextRecurrenceDate"))

                If CCommon.ToBool(ds.Tables(0).Rows(0)("bitCompleted")) Then
                    lblCompleted.Text = "Completed"
                ElseIf CCommon.ToBool(ds.Tables(0).Rows(0)("bitDisabled")) Then
                    lblCompleted.Text = "Disabled"

                    For Each dcfColumn As DataControlField In gvRecurrence.Columns
                        If dcfColumn.AccessibleHeaderText = "Delete" Then
                            dcfColumn.Visible = True
                        End If
                    Next
                Else
                    lblCompleted.Text = "Recurring"
                End If

                lblTransaction.Text = CCommon.ToString(ds.Tables(0).Rows(0)("numTransaction"))

                'Recurred order or bizdoc detail
                If Not ds.Tables(1) Is Nothing Then
                    gvRecurrence.DataSource = ds.Tables(1)
                    gvRecurrence.DataBind()
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DeleteOrder(ByVal oppID As Long)
        Try
            Try
                Dim objOpport As New COpportunities
                With objOpport
                    .OpportID = oppID
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                End With
                Dim lintCount As Integer
                lintCount = objOpport.GetAuthoritativeOpportunityCount()
                If lintCount = 0 Then
                    Dim objAdmin As New CAdmin

                    objAdmin.DomainID = Session("DomainId")
                    objAdmin.ModeType = MileStone1.ModuleType.Opportunity
                    objAdmin.ProjectID = oppID

                    objAdmin.RemoveStagePercentageDetails()

                    objOpport.DelOpp()
                    Dim strFileName As String = ""
                    strFileName = CCommon.GetDocumentPhysicalPath(Session("DomainId")) & "OrderShippingDetail_" & oppID & "_" & CCommon.ToString(Session("DomainId")) & ".xml"
                    If File.Exists(strFileName) Then
                        File.Delete(strFileName)
                    End If
                Else : lblMessage.Text = "You cannot delete Order for which Authoritative BizDocs is present"
                End If
            Catch ex As Exception
                If ex.Message = "DEPENDANT" Then
                    'Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "alert", "alert('Can not remove record, Your option is to remove Time and Expense associated with all stages from ""milestone & stages"" sub-tab and try again.')", True)
                    lblMessage.Text = "Can not remove record, Your option is to remove Time and Expense associated with all stages from ""milestone & stages"" sub-tab and try again."
                ElseIf ex.Message = "RETURN_EXIST" Then
                    lblMessage.Text = "Return exists against order.record can not be deleted."
                ElseIf ex.Message = "CASE DEPENDANT" Then
                    lblMessage.Text = "Dependent case exists. Cannot be Deleted."
                ElseIf ex.Message = "CreditBalance DEPENDANT" Then
                    lblMessage.Text = "Credit balance of current order is being used. Your option is used to credit balance of Organization from Accunting sub tab."
                ElseIf ex.Message = "OppItems DEPENDANT" Then
                    lblMessage.Text = "Items already used. Cannot be Deleted."
                ElseIf ex.Message = "FY_CLOSED" Then
                    lblMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
                ElseIf ex.Message = "RECURRING ORDER OR BIZDOC" Then
                    lblMessage.Text = "Can not remove record because recurrence is set on order or on bizdoc within order."
                ElseIf ex.Message = "RECURRED ORDER" Then
                    lblMessage.Text = "Can not remove record because it is recurred from another order"
                ElseIf ex.Message.Contains("SERIAL/LOT#_USED") Then
                    lblMessage.Text = "Can not delete record because some serial/lot# is used in any sales order."
                Else
                    Throw ex
                End If
            End Try
        Catch ex As Exception
            lblMessage.Text = "Dependent record exists. Cannot be Deleted."
            Throw
        End Try
    End Sub

    Private Sub DeleteBizDoc(ByVal oppID As Long, ByVal oppBizDocID As Long)
        Try
            Dim lintCount As Integer
            Dim lintJournalId As Integer
            Dim objBizDocs As New OppBizDocs
            Dim objOppBizDocs As New OppBizDocs
            Dim lobjJournalEntry As New JournalEntry
            Dim lintAuthorizativeBizDocsId As Integer

            objBizDocs.OppBizDocId = oppBizDocID
            objBizDocs.OppId = oppID
            objBizDocs.DomainID = Session("DomainID")
            objBizDocs.UserCntID = Session("UserContactID")

            objOppBizDocs.OppId = oppID
            objOppBizDocs.UserCntID = Session("UserContactID")
            objOppBizDocs.DomainID = Session("DomainID")
            lintCount = objBizDocs.GetOpportunityBizDocsCount()
            objOppBizDocs.OppType = 1
            lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()
            objOppBizDocs.OppBizDocId = oppBizDocID
            lintJournalId = objOppBizDocs.GetJournalIdForAuthorizativeBizDocs
            If lintCount = 0 Then
                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    ''To Delete Journal Entry
                    If lintJournalId <> 0 Then
                        lobjJournalEntry.JournalId = lintJournalId
                        lobjJournalEntry.DomainID = Session("DomainId")
                        lobjJournalEntry.DeleteJournalEntryDetails()
                    End If
                    objBizDocs.DeleteComissionJournal()

                    Dim objAutomatonRule As New AutomatonRule
                    objAutomatonRule.ExecuteAutomationRule(49, objOppBizDocs.OppBizDocId, 4)

                    objBizDocs.DeleteBizDoc()
                    objTransactionScope.Complete()
                End Using

                ''Added By Sachin Sadhu||Date:8thMay2014
                ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                ''          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = CCommon.ToLong(oppBizDocID)
                objWfA.SaveWFBizDocQueue()
            Else
                lblMessage.Text = "You cannot delete BizDocs for which amount is deposited"
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

#End Region

#Region "Event Handlers"

    Private Sub gvRecurrence_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gvRecurrence.RowDeleting

    End Sub

    Private Sub gvRecurrence_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvRecurrence.RowCommand
        Try
            If e.CommandName = "Delete" Then
                Dim btnDelete As Button = DirectCast(e.CommandSource, Button)
                Dim index As Integer = CCommon.ToInteger(btnDelete.CommandArgument)
                Dim row As GridViewRow = gvRecurrence.Rows(index)

                If hdnRecurrenceType.Value = "1" Then
                    Dim hdnOppID As HiddenField = DirectCast(row.FindControl("hdnOppID"), HiddenField)

                    If Not String.IsNullOrEmpty(hdnOppID.Value) Then
                        DeleteOrder(CCommon.ToLong(hdnOppID.Value))
                    End If
                ElseIf hdnRecurrenceType.Value = "2" Then
                    Dim hdnOppID As HiddenField = DirectCast(row.FindControl("hdnOppID"), HiddenField)
                    Dim hdnOppBizDocID As HiddenField = DirectCast(row.FindControl("hdnOppBizDocID"), HiddenField)

                    If Not String.IsNullOrEmpty(hdnOppID.Value) AndAlso Not String.IsNullOrEmpty(hdnOppBizDocID.Value) Then
                        DeleteBizDoc(CCommon.ToLong(hdnOppID.Value), CCommon.ToLong(hdnOppBizDocID.Value))
                    End If
                End If

                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub gvRecurrence_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvRecurrence.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                Dim lblNameHeader As Label = DirectCast(e.Row.FindControl("lblNameHeader"), Label)
                If hdnRecurrenceType.Value = "1" Then
                    lblNameHeader.Text = "Order"
                Else
                    lblNameHeader.Text = "BizDoc"
                End If
            End If

            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim hplName As HyperLink = DirectCast(e.Row.FindControl("hplName"), HyperLink)
                Dim lblDate As Label = DirectCast(e.Row.FindControl("lblDate"), Label)
                Dim lblError As Label = DirectCast(e.Row.FindControl("lblError"), Label)
                Dim hdnOppID As HiddenField = DirectCast(e.Row.FindControl("hdnOppID"), HiddenField)
                Dim hdnOppBizDocID As HiddenField = DirectCast(e.Row.FindControl("hdnOppBizDocID"), HiddenField)
                Dim btnDelete As Button = DirectCast(e.Row.FindControl("btnDelete"), Button)

                If Not hplName Is Nothing Then
                    If hdnRecurrenceType.Value = "1" Then
                        If Not e.Row.DataItem("numOppId") Is DBNull.Value Then
                            hplName.Attributes.Add("onclick", "return OpenOpp(" & CCommon.ToLong(e.Row.DataItem("numOppId")) & ")")
                            hplName.Text = e.Row.DataItem("vcPOppName")
                            hdnOppID.Value = CCommon.ToString(e.Row.DataItem("numOppId"))
                            btnDelete.Visible = True
                            btnDelete.CommandArgument = CCommon.ToString(e.Row.RowIndex)

                            If Not lblError Is Nothing Then
                                If CCommon.ToBool(e.Row.DataItem("bitErrorOccured")) Then
                                    lblError.Text = "Completed With Error"
                                    lblError.ForeColor = Color.Red
                                Else
                                    lblError.Text = "Completed"
                                    lblError.ForeColor = Color.Green
                                End If
                            End If
                        Else
                            btnDelete.Visible = False
                            If Not lblError Is Nothing Then
                                lblError.Text = "Failed"
                                lblError.ForeColor = Color.Red
                            End If
                        End If
                    Else
                        If Not e.Row.DataItem("numOppId") Is DBNull.Value AndAlso Not e.Row.DataItem("numOppBizDocsId") Is DBNull.Value Then
                            hplName.Attributes.Add("onclick", "return OpenBizInvoice('" & CCommon.ToLong(e.Row.DataItem("numOppId")) & "','" & CCommon.ToLong(e.Row.DataItem("numOppBizDocsId")) & "',0)")
                            hplName.Text = e.Row.DataItem("vcBizDocName")
                            hdnOppID.Value = CCommon.ToString(e.Row.DataItem("numOppId"))
                            hdnOppBizDocID.Value = CCommon.ToString(e.Row.DataItem("numOppBizDocsId"))
                            btnDelete.Visible = True
                            btnDelete.CommandArgument = CCommon.ToString(e.Row.RowIndex)

                            If Not lblError Is Nothing Then
                                If CCommon.ToBool(e.Row.DataItem("bitErrorOccured")) Then
                                    lblError.Text = "Completed With Error"
                                    lblError.ForeColor = Color.Red
                                Else
                                    lblError.Text = "Completed"
                                    lblError.ForeColor = Color.Green
                                End If
                            End If
                        Else
                            btnDelete.Visible = False
                            If Not lblError Is Nothing Then
                                lblError.Text = "Failed"
                                lblError.ForeColor = Color.Red
                            End If
                        End If
                    End If
                End If

                If Not lblDate Is Nothing Then
                    lblDate.Text = CCommon.ToString(e.Row.DataItem("dtCreatedDate"))
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

   
End Class