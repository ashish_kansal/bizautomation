'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Alerts
Imports System.IO
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Workflow

Namespace BACRM.UserInterface.Opportunities
    Public Class frmMirrorBizdoc
        Inherits BACRMPage

        Dim dtOppBiDocItems As New DataTable
        Dim lintJournalId As Integer
        Dim sintRefType, lngReferenceID, lngDivID As Long
        Dim m_aryRightsForAuthoritativ(), m_aryRightsForNonAuthoritativ() As Integer

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub




        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lngReferenceID = CCommon.ToLong(GetQueryStringVal("RefID"))
                sintRefType = CCommon.ToLong(GetQueryStringVal("RefType"))
                'SetWebControlAttributes()

                If Not IsPostBack Then

                    getBizDocDetails()

                    If CCommon.ToInteger(GetQueryStringVal("Print")) = 1 Then
                        tblButtons.Visible = False
                        tblApproval.Visible = False

                        Page.ClientScript.RegisterStartupScript(Me.GetType, "PrintInvoice", " window.print();", True)
                    End If

                    If CCommon.ToInteger(GetQueryStringVal("DocGenerate")) = 1 Then
                        tblButtons.Visible = False
                        tblApproval.Visible = False
                    End If

                    If sintRefType = enmReferenceTypeForMirrorBizDocs.SalesOrder OrElse sintRefType = enmReferenceTypeForMirrorBizDocs.PurchaseOrder Then
                        PaymentHistory1.OppID = CCommon.ToLong(lngReferenceID)
                        PaymentHistory1.OppBizDocID = 0
                        PaymentHistory1.BindGrid()
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub getBizDocDetails()
            Try
                Dim dtOppBiDocDtl As DataTable
                Dim objOppBizDocs As New OppBizDocs
                objOppBizDocs.ReferenceType = sintRefType
                objOppBizDocs.ReferenceID = lngReferenceID
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.UserCntID = Session("UserContactID")
                objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                Dim ds1 As DataSet = objOppBizDocs.GetMirrorBizDocDtls
                dtOppBiDocDtl = ds1.Tables(0)

                If dtOppBiDocDtl.Rows.Count = 0 Then Exit Sub

                hplOppID.Text = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OppName"))
                lblBizDoc.Text = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("BizDcocName"))
                lblBizDocIDLabel.Text = lblBizDoc.Text & "#"
                txtBizDoc.Text = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("BizDoc"))
                txtOppOwner.Text = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("Owner"))
                txtCompName.Text = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("CompName"))
                txtConID.Text = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("numContactID"))
                txtBizDocRecOwner.Text = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("BizDocOwner"))
                txtConEmail.Text = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactEmail"))

                If dtOppBiDocDtl.Rows(0).Item("bitAuthoritativeBizDocs") = 1 Then
                    m_aryRightsForAuthoritativ = GetUserRightsForPage_Other(10, 28)

                    If m_aryRightsForAuthoritativ(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    End If
                Else
                    m_aryRightsForNonAuthoritativ = GetUserRightsForPage_Other(10, 29)

                    If m_aryRightsForNonAuthoritativ(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    End If

                End If

                'lblRefOrderNo.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcRefOrderNo")), "", dtOppBiDocDtl.Rows(0).Item("vcRefOrderNo"))
                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("monAmountPaid")) Then
                    lblAmountPaid.Text = String.Format("{0:#,##0.00}", CCommon.ToDouble(dtOppBiDocDtl.Rows(0).Item("monAmountPaid")))
                Else : lblAmountPaid.Text = "0.00"
                End If

                If sintRefType = 1 Or sintRefType = 2 Or sintRefType = 3 Or sintRefType = 4 Then
                    hplBillto.Attributes.Add("onclick", "return openeditAddress('Bill'," & lngReferenceID & ",0)")
                    hplShipTo.Attributes.Add("onclick", "return openeditAddress('Ship'," & lngReferenceID & ",0)")
                Else
                    hplBillto.Attributes.Add("onclick", "return openeditAddress('Bill',0," & lngReferenceID & ")")
                    hplShipTo.Attributes.Add("onclick", "return openeditAddress('Ship',0," & lngReferenceID & ")")
                End If

                If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                    If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("BizdocFooter")) Then
                        imgFooter.Visible = True
                        imgFooter.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtOppBiDocDtl.Rows(0).Item("BizdocFooter")
                    Else : imgFooter.Visible = False
                    End If
                    lblPONo.Text = "P.O"

                Else
                    If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("PurBizdocFooter")) Then
                        imgFooter.Visible = True
                        imgFooter.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtOppBiDocDtl.Rows(0).Item("PurBizdocFooter")
                    Else : imgFooter.Visible = False
                    End If
                    lblPONo.Text = "Invoice"

                End If

                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcBizDocImagePath")) Then
                    imgLogo.Visible = True
                    imgLogo.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtOppBiDocDtl.Rows(0).Item("vcBizDocImagePath")
                Else : imgLogo.Visible = False
                End If

                lblBizDocIDValue.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcBizDocID")), "", dtOppBiDocDtl.Rows(0).Item("vcBizDocID"))
                lblcreated.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("numCreatedby")), "", dtOppBiDocDtl.Rows(0).Item("numCreatedby")) & "  &nbsp;" & IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtCreatedDate")), "", dtOppBiDocDtl.Rows(0).Item("dtCreatedDate"))
                lblModifiedby.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("numModifiedBy")), "", dtOppBiDocDtl.Rows(0).Item("numModifiedBy")) & "  &nbsp;" & IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtModifiedDate")), "", dtOppBiDocDtl.Rows(0).Item("dtModifiedDate"))
                lblDate.Text = FormattedDateFromDate(dtOppBiDocDtl.Rows(0).Item("dtCreatedDate"), Session("DateFormat"))
                lblOrganizationName.Text = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrganizationName"))
                lblOrganizationContactName.Text = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactName"))
                hdnDivID.Value = CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numDivisionID"))
                SetWebControlAttributes()
                Dim strDate As Date

                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtFromDate")) Then
                    strDate = DateAdd(DateInterval.Day, CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("numBillingDaysName")), dtOppBiDocDtl.Rows(0).Item("dtFromDate"))
                    lblDuedate.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
                End If
                If dtOppBiDocDtl.Rows(0).Item("tintBillingTerms") = True Then
                    lblBillingTerms.Text = "Net " & dtOppBiDocDtl.Rows(0).Item("numBillingDaysName") & " , " & IIf(dtOppBiDocDtl.Rows(0).Item("tintInterestType") = False, "-", "+") & dtOppBiDocDtl.Rows(0).Item("fltInterest") & " %"
                    lblBillingTermsName.Text = dtOppBiDocDtl.Rows(0).Item("vcBillingTermsName") & " , " & IIf(dtOppBiDocDtl.Rows(0).Item("tintInterestType") = False, "-", "+") & dtOppBiDocDtl.Rows(0).Item("fltInterest") & " %"

                Else
                    lblBillingTerms.Text = "-"
                    lblBillingTermsName.Text = "-"

                End If
                'If Not strDate = Nothing Then
                '    hplDueDate.Attributes.Add("onclick", "return ChangeDate('" & sintRefType & "','" & strDate.ToString("yyyyMMdd") & "')")
                'End If
                If dtOppBiDocDtl.Rows(0).Item("fltDiscount") > 0 Then
                    If dtOppBiDocDtl.Rows(0).Item("bitDiscountType") = False Then

                        lblDiscount.Text = dtOppBiDocDtl.Rows(0).Item("fltDiscount") & " %"
                    Else
                        lblDiscount.Text = dtOppBiDocDtl.Rows(0).Item("fltDiscount")
                    End If
                End If

                lblStatus.Text = dtOppBiDocDtl.Rows(0).Item("BizDocStatus")
                lblComments.Text = "<pre class='normal1'>" & dtOppBiDocDtl.Rows(0).Item("vcComments") & "</pre>"
                lblNo.Text = dtOppBiDocDtl.Rows(0).Item("vcOppRefOrderNo")
                'txtTrackingURL.Text = dtOppBiDocDtl.Rows(0).Item("vcTrackingURL")
                hdShipAmt.Value = CCommon.GetDecimalFormat(dtOppBiDocDtl.Rows(0).Item("monShipCost"))

                Dim strTrackingNo As String = ""

                If dtOppBiDocDtl.Rows(0).Item("vcTrackingNo").ToString().Trim.Length > 0 Then
                    Dim strTrackingNoList() As String = dtOppBiDocDtl.Rows(0).Item("vcTrackingNo").Split(New String() {"$^$"}, StringSplitOptions.None)
                    Dim strIDValue() As String

                    If strTrackingNoList.Length > 0 Then
                        For k As Integer = 0 To strTrackingNoList.Length - 1
                            strIDValue = strTrackingNoList(k).Split(New String() {"#^#"}, StringSplitOptions.None)

                            If strIDValue(1).Length > 0 Then
                                strTrackingNo += String.Format("<a href='{0}' target='_blank'>{1}</a>&nbsp;&nbsp;", IIf(strIDValue(0).Length = 0, "javascript:void(0)", String.Format(strIDValue(0), strIDValue(1))), strIDValue(1))
                            End If
                        Next
                    End If
                End If
                lblTrackingNumbers.Text = "<div style='width: 95%;word-break: break-all;'>" & strTrackingNo & "</div>"

                'Show capture amount button
                'If dtOppBiDocDtl.Rows(0).Item("bitAuthoritativeBizDocs") = 1 Then btnCaptureAmount.Visible = IsCardAuthorized()

                Dim ds As New DataSet
                Dim dtOppBiDocItems As DataTable
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.ReferenceType = sintRefType
                objOppBizDocs.ReferenceID = lngReferenceID
                ds = objOppBizDocs.GetMirrorBizDocItems
                dtOppBiDocItems = ds.Tables(0)

                Dim objCalculateDealAmount As New CalculateDealAmount
                'objCalculateDealAmount.CalculateDealAmount(lngReferenceID, sintRefType, CShort(dtOppBiDocDtl.Rows(0).Item("tintOppType")), Session("DomainID"), dtOppBiDocItems, FromBizInvoice:=True)
                objCalculateDealAmount.CalculateDealAmountForMirrorBizDoc(lngReferenceID, sintRefType, CShort(dtOppBiDocDtl.Rows(0).Item("tintOppType")), Session("DomainID"), dtOppBiDocItems, dtOppBiDocDtl, FromBizInvoice:=True)

                hdLateCharge.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalLateCharges)


                If CCommon.ToBool(Session("DiscountOnUnitPrice")) Then
                    hdDisc.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalDiscount)
                    hdSubTotal.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalAmount)
                Else
                    hdDisc.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalDiscount + IIf(dtOppBiDocItems.Rows.Count > 0, dtOppBiDocItems.Compute("SUM(DiscAmt)", ""), 0))
                    hdSubTotal.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalAmount + objCalculateDealAmount.TotalDiscount + IIf(dtOppBiDocItems.Rows.Count > 0, dtOppBiDocItems.Compute("SUM(DiscAmt)", ""), 0))
                End If

                hdGrandTotal.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.GrandTotal - objCalculateDealAmount.CreditAmount)

                hdTaxAmt.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalTaxAmount)

                hdCRVTxtAmt.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalCRVTaxAmount)

                hdnCreditAmount.Value = CCommon.GetDecimalFormat(IIf(objCalculateDealAmount.CreditAmount > 0, objCalculateDealAmount.CreditAmount * -1, 0))

                Session("Itemslist") = dtOppBiDocItems

                '''Add columns to datagrid
                Dim i As Integer
                Dim dtdgColumns As DataTable
                dtdgColumns = ds.Tables(1)
                Dim bColumn As BoundColumn
                For i = 0 To dtdgColumns.Rows.Count - 1
                    bColumn = New BoundColumn
                    bColumn.HeaderText = dtdgColumns.Rows(i).Item("vcFormFieldName")
                    bColumn.DataField = dtdgColumns.Rows(i).Item("vcDbColumnName")
                    If dtdgColumns.Rows(i).Item("vcDbColumnName") = "SerialLotNo" OrElse dtdgColumns.Rows(i).Item("vcDbColumnName") = "txtItemDesc" Then
                        bColumn.ItemStyle.CssClass = "WordWrapSerialNo"
                    End If
                    'Added By   :Sachin Sadhu||Date:4thJune2014
                    'Description:To resolve Design issue 
                    If dtdgColumns.Rows(i).Item("vcDbColumnName") = "txtItemDesc" And dtdgColumns.Rows.Count = 2 Then
                        bColumn.ItemStyle.CssClass = "TwoColumns"
                    ElseIf dtdgColumns.Rows(i).Item("vcDbColumnName") = "txtItemDesc" And dtdgColumns.Rows.Count = 1 Then
                        bColumn.ItemStyle.CssClass = "OneColumns"
                    ElseIf dtdgColumns.Rows(i).Item("vcDbColumnName") = "txtItemDesc" And dtdgColumns.Rows.Count = 3 Then
                        bColumn.ItemStyle.CssClass = "ThreeColumns"
                    End If
                    'End of Code
                    If dtdgColumns.Rows(i).Item("vcFieldDataType") = "M" Then bColumn.DataFormatString = "{0:#,##0.00}"

                    dgBizDocs.Columns.Add(bColumn)
                Next
                BindItems()
                Dim objConfigWizard As New FormConfigWizard
                If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                    ibtnFooter.Attributes.Add("onclick", "return Openfooter(1)")
                    objConfigWizard.FormID = 7
                Else
                    objConfigWizard.FormID = 8
                    ibtnFooter.Attributes.Add("onclick", "return Openfooter(2)")
                End If
                hdnOppType.Value = dtOppBiDocDtl.Rows(0).Item("tintOppType")
                hdnBizDocId.Value = dtOppBiDocDtl.Rows(0).Item("numBizDocId")

                BindBizDocsTemplate()

                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("numBizDocTempID")) Then
                    If Not ddlBizDocTemplate.Items.FindByValue(dtOppBiDocDtl.Rows(0).Item("numBizDocTempID")) Is Nothing Then
                        ddlBizDocTemplate.ClearSelection()
                        ddlBizDocTemplate.Items.FindByValue(dtOppBiDocDtl.Rows(0).Item("numBizDocTempID")).Selected = True
                    End If
                End If
                If CCommon.ToLong(ddlBizDocTemplate.SelectedValue) = 0 And ddlBizDocTemplate.Items.Count > 0 Then
                    ddlBizDocTemplate.ClearSelection()
                    ddlBizDocTemplate.SelectedIndex = 1

                    ddlBizDocTemplate_SelectedIndexChanged()
                End If

                objConfigWizard.DomainID = Session("DomainID")
                objConfigWizard.BizDocID = dtOppBiDocDtl.Rows(0).Item("numBizDocId")
                objConfigWizard.BizDocTemplateID = dtOppBiDocDtl.Rows(0).Item("numBizDocTempID")

                objOppBizDocs.BizDocId = dtOppBiDocDtl.Rows(0).Item("numBizDocId")
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.BizDocTemplateID = CCommon.ToLong(ddlBizDocTemplate.SelectedValue)

                Dim dsNew As DataSet
                Dim dtTable As DataTable
                dsNew = objConfigWizard.GetFieldFormListForBizDocsSumm
                dtTable = dsNew.Tables(1)

                Dim tblrow As HtmlTableRow
                Dim tblCell As HtmlTableCell
                Dim strLabelCellID As String


                For Each dr As DataRow In dtTable.Rows
                    tblrow = New HtmlTableRow
                    tblCell = New HtmlTableCell
                    tblCell.Attributes.Add("class", "normal1")
                    tblCell.InnerText = dr("vcFormFieldName") & ": "
                    tblrow.Cells.Add(tblCell)

                    tblCell = New HtmlTableCell
                    tblCell.Attributes.Add("class", "normal1")
                    If dr("vcDbColumnName") = "SubTotal" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", CCommon.ToDouble(hdSubTotal.Value))
                    ElseIf dr("vcDbColumnName") = "ShippingAmount" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", CCommon.ToDouble(hdShipAmt.Value))
                    ElseIf dr("vcDbColumnName") = "TotalSalesTax" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", CCommon.ToDouble(hdTaxAmt.Value) - CCommon.ToDouble(hdCRVTxtAmt.Value))
                    ElseIf dr("vcDbColumnName") = "TotalCRVTax" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", CCommon.ToDouble(hdCRVTxtAmt.Value))
                    ElseIf dr("vcDbColumnName") = "LateCharge" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", CCommon.ToDouble(hdLateCharge.Value))
                    ElseIf dr("vcDbColumnName") = "Discount" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", CCommon.ToDouble(hdDisc.Value))
                    ElseIf dr("vcDbColumnName") = "CreditApplied" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", CCommon.ToDouble(hdnCreditAmount.Value))
                    ElseIf dr("vcDbColumnName") = "GrandTotal" Then
                        tblCell.ID = "tdGradTotal"
                        strLabelCellID = tblCell.ID
                        'tblCell.InnerText = hdGrandTotal.Value
                    ElseIf dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                        Dim taxAmt As Decimal
                        taxAmt = IIf(IsDBNull(dtOppBiDocItems.Compute("SUM([" & dr("vcFormFieldName") & "])", "[" & dr("vcFormFieldName") & "]>0")), 0, dtOppBiDocItems.Compute("SUM([" & dr("vcFormFieldName") & "])", "[" & dr("vcFormFieldName") & "]>0"))
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", CCommon.ToDouble(taxAmt))
                    End If
                    tblrow.Cells.Add(tblCell)
                    tblBizDocSumm.Rows.Add(tblrow)
                Next
                If strLabelCellID <> "" Then
                    CType(tblBizDocSumm.FindControl(strLabelCellID), HtmlTableCell).InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", CCommon.ToDouble(hdGrandTotal.Value))
                End If

                lblBalance.Text = String.Format("{0:#,##0.00}", CCommon.ToDouble(hdGrandTotal.Value - lblAmountPaid.Text))
                hdnBalance.Value = lblBalance.Text
                lblAmountPaidCurrency.Text = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString()
                lblBalanceDueCurrency.Text = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString()

                If dtOppBiDocDtl.Rows(0)("bitEnabled") = True And dtOppBiDocDtl.Rows(0)("txtBizDocTemplate").ToString().Length > 0 Then
                    'BizDoc UI modification 
                    Dim strCss As String = "<style>" & Server.HtmlDecode(dtOppBiDocDtl.Rows(0)("txtCSS").ToString()) & "</style>"
                    Dim strBizDocUI As String = strCss & HttpUtility.HtmlDecode(dtOppBiDocDtl.Rows(0)("txtBizDocTemplate").ToString())
                    If strBizDocUI.Length > 0 Then
                        strBizDocUI = strBizDocUI.Replace("#Logo#", CCommon.RenderControl(imgLogo))
                        'strBizDocUI = strBizDocUI.Replace("#Signature#", CCommon.RenderControl(imgSignature))

                        strBizDocUI = strBizDocUI.Replace("#FooterImage#", CCommon.RenderControl(imgFooter))
                        strBizDocUI = strBizDocUI.Replace("#BizDocType#", CCommon.RenderControl(lblBizDoc))

                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        'Customer/Vendor Information
                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrganizationName")))
                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationPhone#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrganizationPhone")))

                        If ds1.Tables(1).Rows.Count = 0 Then
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddressName#", "")
                            strBizDocUI = strBizDocUI.Replace("#BillingContact#", "")
                        Else
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCompanyName#", CCommon.ToString(ds1.Tables(1).Rows(0)("vcCompanyName")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddress#", CCommon.ToString(ds1.Tables(1).Rows(0)("vcFullAddress")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToStreet#", CCommon.ToString(ds1.Tables(1).Rows(0)("vcStreet")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCity#", CCommon.ToString(ds1.Tables(1).Rows(0)("vcCity")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToPostal#", CCommon.ToString(ds1.Tables(1).Rows(0)("vcPostalCode")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToState#", CCommon.ToString(ds1.Tables(1).Rows(0)("vcState")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCountry#", CCommon.ToString(ds1.Tables(1).Rows(0)("vcCountry")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddressName#", CCommon.ToString(ds1.Tables(1).Rows(0)("vcAddressName")))
                            strBizDocUI = strBizDocUI.Replace("#BillingContact#", CCommon.ToString(ds1.Tables(1).Rows(0)("vcContact")))
                        End If

                        If Not ds1 Is Nothing AndAlso ds1.Tables.Count > 5 AndAlso ds1.Tables(5).Rows.Count > 0 Then
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCompanyName#", CCommon.ToString(ds1.Tables(5).Rows(0)("vcCompanyName")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddress#", CCommon.ToString(ds1.Tables(5).Rows(0)("vcFullAddress")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToStreet#", CCommon.ToString(ds1.Tables(5).Rows(0)("vcStreet")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCity#", CCommon.ToString(ds1.Tables(5).Rows(0)("vcCity")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToPostal#", CCommon.ToString(ds1.Tables(5).Rows(0)("vcPostalCode")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToState#", CCommon.ToString(ds1.Tables(5).Rows(0)("vcState")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCountry#", CCommon.ToString(ds1.Tables(5).Rows(0)("vcCountry")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddressName#", CCommon.ToString(ds1.Tables(5).Rows(0)("vcAddressName")))
                        Else
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddressName#", "")
                        End If

                        If ds1.Tables(2).Rows.Count = 0 Then
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddressName#", "")
                            strBizDocUI = strBizDocUI.Replace("#ShippingContact#", "")
                        Else
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCompanyName#", CCommon.ToString(ds1.Tables(2).Rows(0)("vcCompanyName")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddress#", CCommon.ToString(ds1.Tables(2).Rows(0)("vcFullAddress")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToStreet#", CCommon.ToString(ds1.Tables(2).Rows(0)("vcStreet")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCity#", CCommon.ToString(ds1.Tables(2).Rows(0)("vcCity")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToPostal#", CCommon.ToString(ds1.Tables(2).Rows(0)("vcPostalCode")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToState#", CCommon.ToString(ds1.Tables(2).Rows(0)("vcState")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCountry#", CCommon.ToString(ds1.Tables(2).Rows(0)("vcCountry")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddressName#", CCommon.ToString(ds1.Tables(2).Rows(0)("vcAddressName")))
                            strBizDocUI = strBizDocUI.Replace("#ShippingContact#", CCommon.ToString(ds1.Tables(2).Rows(0)("vcContact")))
                        End If

                        If Not ds1 Is Nothing AndAlso ds1.Tables.Count > 6 AndAlso ds1.Tables(6).Rows.Count > 0 Then
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCompanyName#", CCommon.ToString(ds1.Tables(6).Rows(0)("vcCompanyName")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddress#", CCommon.ToString(ds1.Tables(6).Rows(0)("vcFullAddress")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToStreet#", CCommon.ToString(ds1.Tables(6).Rows(0)("vcStreet")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCity#", CCommon.ToString(ds1.Tables(6).Rows(0)("vcCity")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToPostal#", CCommon.ToString(ds1.Tables(6).Rows(0)("vcPostalCode")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToState#", CCommon.ToString(ds1.Tables(6).Rows(0)("vcState")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCountry#", CCommon.ToString(ds1.Tables(6).Rows(0)("vcCountry")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddressName#", CCommon.ToString(ds1.Tables(6).Rows(0)("vcAddressName")))
                        Else
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddressName#", "")
                        End If

                        If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                            If strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") + "#OppOrderChangeBillToHeader(".Length, strBizDocUI.LastIndexOf(")#") - strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") - "#OppOrderChangeBillToHeader(".Length)
                                hplBillto.InnerHtml = strLink '"<font color=""white"">" + strLink + "</font>"
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeBillToHeader(" & strLink & ")#", CCommon.RenderControl(hplBillto))
                            Else
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeBillToHeader#", CCommon.RenderControl(hplBillto))
                            End If

                            If strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") + "#OppOrderChangeShipToHeader(".Length, strBizDocUI.IndexOf(")#") - strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") - "#OppOrderChangeShipToHeader(".Length)
                                hplShipTo.InnerHtml = strLink '"<font color=""white"">" + strLink + "</font>"
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeShipToHeader(" & strLink & ")#", CCommon.RenderControl(hplShipTo))
                            Else
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeShipToHeader#", CCommon.RenderControl(hplShipTo))
                            End If
                        Else
                            If strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") + "#OppOrderChangeBillToHeader(".Length, strBizDocUI.LastIndexOf(")#") - strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") - "#OppOrderChangeBillToHeader(".Length)
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeBillToHeader(" & strLink & ")#", strLink)
                            Else
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeBillToHeader#", "Bill To")
                            End If

                            If strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") + "#OppOrderChangeShipToHeader(".Length, strBizDocUI.IndexOf(")#") - strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") - "#OppOrderChangeShipToHeader(".Length)
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeShipToHeader(" & strLink & ")#", strLink)
                            Else
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeShipToHeader#", "Ship To")
                            End If
                        End If

                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationComments#", dtOppBiDocDtl.Rows(0)("vcOrganizationComments").ToString())

                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationContactName#", CCommon.RenderControl(lblOrganizationContactName))
                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationContactEmail#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactEmail")))
                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationContactPhone#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactPhone")))
                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                        'Employer Information
                        strBizDocUI = strBizDocUI.Replace("#EmployerOrganizationName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("EmployerOrganizationName")))
                        strBizDocUI = strBizDocUI.Replace("#EmployerOrganizationPhone#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("EmployerOrganizationPhone")))

                        If ds1.Tables(3).Rows.Count = 0 Then
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddressName#", "")
                        Else
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCompanyName#", ds1.Tables(3).Rows(0)("vcCompanyName"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddress#", ds1.Tables(3).Rows(0)("vcFullAddress"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToStreet#", ds1.Tables(3).Rows(0)("vcStreet"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCity#", ds1.Tables(3).Rows(0)("vcCity"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToPostal#", ds1.Tables(3).Rows(0)("vcPostalCode"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToState#", ds1.Tables(3).Rows(0)("vcState"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCountry#", ds1.Tables(3).Rows(0)("vcCountry"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddressName#", ds1.Tables(3).Rows(0)("vcAddressName"))
                        End If
                       
                        If ds1.Tables(4).Rows.Count = 0 Then
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddressName#", "")
                        Else
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCompanyName#", ds1.Tables(4).Rows(0)("vcCompanyName"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddress#", ds1.Tables(4).Rows(0)("vcFullAddress"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToStreet#", ds1.Tables(4).Rows(0)("vcStreet"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCity#", ds1.Tables(4).Rows(0)("vcCity"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToPostal#", ds1.Tables(4).Rows(0)("vcPostalCode"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToState#", ds1.Tables(4).Rows(0)("vcState"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCountry#", ds1.Tables(4).Rows(0)("vcCountry"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddressName#", ds1.Tables(4).Rows(0)("vcAddressName"))
                        End If

                        If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 2 Then
                            If strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") + "#EmployerChangeBillToHeader(".Length, strBizDocUI.LastIndexOf(")#") - strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") - "#EmployerChangeBillToHeader(".Length)
                                hplBillto.InnerHtml = strLink '"<font color=""white"">" + strLink + "</font>"
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeBillToHeader(" & strLink & ")#", CCommon.RenderControl(hplBillto))
                            Else
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeBillToHeader#", CCommon.RenderControl(hplBillto))
                            End If

                            If strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") + "#EmployerChangeShipToHeader(".Length, strBizDocUI.IndexOf(")#") - strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") - "#EmployerChangeShipToHeader(".Length)
                                hplShipTo.InnerHtml = strLink '"<font color=""white"">" + strLink + "</font>"
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeShipToHeader(" & strLink & ")#", CCommon.RenderControl(hplShipTo))
                            Else
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeShipToHeader#", CCommon.RenderControl(hplShipTo))
                            End If
                        Else
                            If strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") + "#EmployerChangeBillToHeader(".Length, strBizDocUI.LastIndexOf(")#") - strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") - "#EmployerChangeBillToHeader(".Length)
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeBillToHeader(" & strLink & ")#", strBizDocUI)
                            Else
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeBillToHeader#", "Bill To")
                            End If

                            If strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") + "#EmployerChangeShipToHeader(".Length, strBizDocUI.IndexOf(")#") - strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") - "#EmployerChangeShipToHeader(".Length)
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeShipToHeader(" & strLink & ")#", strLink)
                            Else
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeShipToHeader#", "Ship To")
                            End If
                        End If

                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                        strBizDocUI = strBizDocUI.Replace("#BizDocStatus#", lblStatus.Text)
                        strBizDocUI = strBizDocUI.Replace("#Currency#", lblBalanceDueCurrency.Text)
                        strBizDocUI = strBizDocUI.Replace("#AmountPaid#", lblAmountPaid.Text)
                        strBizDocUI = strBizDocUI.Replace("#BalanceDue#", CCommon.RenderControl(lblBalance)) 'used by amt paid js
                        strBizDocUI = strBizDocUI.Replace("#Discount#", lblDiscount.Text)
                        strBizDocUI = strBizDocUI.Replace("#BillingTerms#", lblBillingTerms.Text)
                        If CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("dtFromDate")).Length > 3 Then
                            strBizDocUI = strBizDocUI.Replace("#BillingTermFromDate#", FormattedDateFromDate(dtOppBiDocDtl.Rows(0).Item("dtFromDate"), Session("DateFormat")))
                        End If
                        strBizDocUI = strBizDocUI.Replace("#BillingTermsName#", lblBillingTermsName.Text)

                        strBizDocUI = strBizDocUI.Replace("#DueDate#", lblDuedate.Text)
                        strBizDocUI = strBizDocUI.Replace("#BizDocID#", CCommon.RenderControl(lblBizDocIDValue))
                        strBizDocUI = strBizDocUI.Replace("#P.O.NO#", lblNo.Text)
                        strBizDocUI = strBizDocUI.Replace("#OrderID#", CCommon.RenderControl(hplOppID))
                        strBizDocUI = strBizDocUI.Replace("#Comments#", lblComments.Text)
                        strBizDocUI = strBizDocUI.Replace("#Products#", CCommon.RenderControl(dgBizDocs))
                        strBizDocUI = strBizDocUI.Replace("#BizDocSummary#", CCommon.RenderControl(tblBizDocSumm))
                        strBizDocUI = strBizDocUI.Replace("#BizDocCreatedDate#", lblDate.Text)

                        strBizDocUI = strBizDocUI.Replace("#AmountPaidPopUp#", CCommon.RenderControl(lblAmountPaidTitle))
                        strBizDocUI = strBizDocUI.Replace("#ChangeDueDate#", CCommon.RenderControl(hplDueDate))
                        strBizDocUI = strBizDocUI.Replace("#AssigneeName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("AssigneeName")))
                        strBizDocUI = strBizDocUI.Replace("#PartnerSource#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcPartner")))
                        strBizDocUI = strBizDocUI.Replace("#DropShip#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcDropShip")))
                        strBizDocUI = strBizDocUI.Replace("#ReleaseDate#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcReleaseDate")))
                        strBizDocUI = strBizDocUI.Replace("#AssigneeEmail#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("AssigneeEmail")))
                        strBizDocUI = strBizDocUI.Replace("#AssigneePhoneNo#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("AssigneePhone")))
                        strBizDocUI = strBizDocUI.Replace("#CustomerPO##", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcCustomerPO#")))
                        strBizDocUI = strBizDocUI.Replace("#RequiredDate#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcRequiredDate")))
                        strBizDocUI = strBizDocUI.Replace("#InventoryStatus#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcInventoryStatus")))
                        'strBizDocUI = strBizDocUI.Replace("#OrganizationContactName#", CCommon.RenderControl(lblOrganizationContactName))
                        'strBizDocUI = strBizDocUI.Replace("#OrganizationContactPhone#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactPhone")))
                        'strBizDocUI = strBizDocUI.Replace("#OrganizationContactEmail#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactEmail")))
                        'strBizDocUI = strBizDocUI.Replace("#OrganizationBillingAddress#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("CompanyBillingAddress")))
                        'strBizDocUI = strBizDocUI.Replace("#OrganizationPhone#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrganizationPhone")))
                        strBizDocUI = strBizDocUI.Replace("#OrderRecOwner#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OnlyOrderRecOwner")))

                        'strBizDocUI = strBizDocUI.Replace("#BillToAddressName#", lblBillToAddressName.Text)
                        'strBizDocUI = strBizDocUI.Replace("#ShipToAddressName#", lblShipToAddressName.Text)

                        'strBizDocUI = strBizDocUI.Replace("#BillToCompanyName#", CCommon.RenderControl(lblBillToCompanyName))
                        'strBizDocUI = strBizDocUI.Replace("#ShipToCompanyName#", CCommon.RenderControl(lblShipToCompanyName))

                        strBizDocUI = strBizDocUI.Replace("#ShippingCompany#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("ShipVia")))
                        strBizDocUI = strBizDocUI.Replace("#ShippingService#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcShippingService")))
                        strBizDocUI = strBizDocUI.Replace("#PackingSlipID#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcPackingSlip")))
                        strBizDocUI = strBizDocUI.Replace("#BizDocTemplateName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcTemplateName")))
                        If dtOppBiDocItems IsNot Nothing AndAlso dtOppBiDocItems.Rows.Count > 0 Then
                            If dtOppBiDocItems.Select("numItemCode <>" & CCommon.ToDouble(Session("DiscountServiceItem")) & " AND numItemCode <> " & CCommon.ToDouble(Session("ShippingServiceItem"))).Length > 0 Then
                                strBizDocUI = strBizDocUI.Replace("#TotalQuantity#", String.Format("{0:#,##0.##}", CCommon.ToDecimal(dtOppBiDocItems.Select("numItemCode <>" & CCommon.ToDouble(Session("DiscountServiceItem")) & " AND numItemCode <> " & CCommon.ToDouble(Session("ShippingServiceItem"))).CopyToDataTable().Compute("sum(numUnitHour)", ""))))
                            Else
                                strBizDocUI = strBizDocUI.Replace("#TotalQuantity#", String.Format("{0:#,##0.##}", 0))
                            End If
                        Else
                            strBizDocUI = strBizDocUI.Replace("#TotalQuantity#", String.Format("{0:#,##0.##}", 0))
                        End If
                        strBizDocUI = strBizDocUI.Replace("#TrackingNo#", CCommon.RenderControl(lblTrackingNumbers))
                        strBizDocUI = strBizDocUI.Replace("#SOComments#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcSOComments")))
                        strBizDocUI = strBizDocUI.Replace("#ParcelShippingAccount#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcShippersAccountNo")))
                        strBizDocUI = strBizDocUI.Replace("#OrderCreatedDate#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrderCreatedDate")))

                        strBizDocUI = strBizDocUI.Replace("#VendorInvoice#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcVendorInvoice")))

                        Dim lNumericToWord As New NumericToWord

                        strBizDocUI = strBizDocUI.Replace("#GrandTotalinWords#", lNumericToWord.SpellNumber(Replace(hdGrandTotal.Value, ",", "")))

                        If dtOppBiDocDtl.Columns.Contains("vcTotalQtybyUOM") Then
                            strBizDocUI = strBizDocUI.Replace("#TotalQytbyUOM#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcTotalQtybyUOM")))
                        Else
                            strBizDocUI = strBizDocUI.Replace("#TotalQytbyUOM#", "")
                        End If

                        'Replace custom field values
                        Dim objCustomFields As New CustomFields
                        If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                            objCustomFields.locId = 2
                        Else
                            objCustomFields.locId = 6
                        End If
                        objCustomFields.RelId = 0
                        objCustomFields.DomainID = Session("DomainID")
                        objCustomFields.RecordId = lngReferenceID
                        ds = objCustomFields.GetCustFlds
                        For Each drow As DataRow In ds.Tables(0).Rows
                            If CCommon.ToLong(drow("numListID")) > 0 Then
                                strBizDocUI = strBizDocUI.Replace("#" & drow("fld_label").ToString.Trim.Replace(" ", "") & "#", objCommon.GetListItemValue(CCommon.ToString(drow("Value")), Session("DomainID")))
                            Else
                                strBizDocUI = strBizDocUI.Replace("#" & drow("fld_label").ToString.Trim.Replace(" ", "") & "#", drow("Value"))
                            End If
                        Next
                        litBizDocTemplate.Text = strBizDocUI
                        tblOriginalBizDoc.Visible = False
                        tblFormattedBizDoc.Visible = True
                    End If
                Else
                    tblOriginalBizDoc.Visible = True
                    tblFormattedBizDoc.Visible = False
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub BindItems()
            Try
                Dim dtItems As New DataTable
                dtItems = Session("Itemslist")
                dgBizDocs.DataSource = dtItems
                dgBizDocs.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub Save()
            Try

                Dim JournalId As Integer
                Dim dtOppBiDocDtl As New DataTable
                Dim lintAuthorizativeBizDocsId As Integer
                Dim objOppBizDocs As New OppBizDocs
                Dim lintOppTypeId As Integer
                Dim i, j As Integer
                Dim lintBizDocId As Integer

                objOppBizDocs.vcComments = ""
                objOppBizDocs.UserCntID = Session("UserContactID")
                objOppBizDocs.ReferenceType = sintRefType
                objOppBizDocs.BizDocStatus = 0
                objOppBizDocs.SaveInvoiceDtl()
                ''Added By Sachin Sadhu||Date:1stMay2014
                ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                ''          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                ' objWfA.RecordID = lngOppBizDocID
                ' objWfA.SaveWFBizDocQueue()
                'end of code
                Dim objAlerts As New CAlerts
                Dim dtAlerts As DataTable
                objAlerts.BizDocs = txtBizDoc.Text
                objAlerts.DomainID = Session("DomainID")
                dtAlerts = objAlerts.BizDocAlertDTLs
                If dtAlerts.Rows.Count > 0 Then
                    j = 0
                    For i = 0 To dtAlerts.Rows.Count - 1
                        If dtAlerts.Rows(i).Item("bitModified") = True Then
                            j = 1
                            Exit For
                        End If
                    Next
                    If j = 1 Then
                        Dim dtEmailTemplate As DataTable
                        Dim objDocuments As New DocumentList
                        objDocuments.GenDocID = dtAlerts.Rows(i).Item("numEmailTemplate")
                        objDocuments.DomainID = Session("DomainID")
                        dtEmailTemplate = objDocuments.GetDocByGenDocID
                        If dtEmailTemplate.Rows.Count > 0 Then
                            Dim objSendMail As New Email
                            Dim dtMergeFields As New DataTable
                            Dim drNew As DataRow
                            dtMergeFields.Columns.Add("BizDocID")
                            dtMergeFields.Columns.Add("DealAmount")
                            dtMergeFields.Columns.Add("vcCompanyName")
                            drNew = dtMergeFields.NewRow
                            drNew("BizDocID") = lblBizDoc.Text
                            drNew("DealAmount") = hdGrandTotal.Value
                            drNew("vcCompanyName") = txtCompName.Text

                            dtMergeFields.Rows.Add(drNew)
                            objCommon.byteMode = 1
                            objCommon.ContactID = txtOppOwner.Text
                            Dim strTo As String
                            strTo = IIf(dtAlerts.Rows(i).Item("bitOppOwner") = 1, objCommon.GetAccountHoldersEmail, "")
                            If strTo <> "" Then
                                strTo = strTo & ";" & IIf(dtAlerts.Rows(i).Item("bitPriConatct") = 1, txtConEmail.Text, "")
                                strTo = strTo.TrimEnd(";")
                            Else : strTo = IIf(dtAlerts.Rows(i).Item("bitPriConatct") = 1, txtConEmail.Text, "")
                            End If
                            objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtAlerts.Rows(i).Item("bitCCManager") = 1, objCommon.GetManagerEmail, ""), Session("UserEmail"), strTo, dtMergeFields)
                        End If

                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function ReturnMoney(ByVal Money)
            Try
                Return String.Format("{0:#,###.##}", Math.Round(Money, 2))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnTemp_DontDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTemp_DontDelete.Click
            Try
                getBizDocDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ibtnSendEmail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnSendEmail.Click
            Try
                hdnBizDocHTML.Value = "<body><link rel='stylesheet' href='" & Server.MapPath("~/CSS/master.css") & "' type='text/css' />" + hdnBizDocHTML.Value + "</body>"

                Session("Attachements") = Nothing 'reset to nothing to avoid duplicate attachments
                Dim objBizDocs As New OppBizDocs
                Dim dt As DataTable
                objBizDocs.BizDocId = txtBizDoc.Text
                objBizDocs.DomainID = Session("DomainID")
                objBizDocs.BizDocTemplateID = CCommon.ToLong(ddlBizDocTemplate.SelectedValue)
                dt = objBizDocs.GetBizDocAttachments
                Dim i As Integer
                Dim strFileName As String = ""
                Dim strFilePhysicalLocation As String = ""

                Dim objHTMLToPDF As New HTMLToPDF
                strFileName = objHTMLToPDF.ConvertHTML2PDF(hdnBizDocHTML.Value, CCommon.ToLong(Session("DomainID")))

                strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
                objCommon.AddAttchmentToSession(IIf(lblBizDocIDValue.Text.Trim.Length > 0, lblBizDocIDValue.Text.Trim.Replace(" ", "_") + ".pdf", strFileName), CCommon.GetDocumentPath(Session("DomainID")) & strFileName, strFilePhysicalLocation)

                Dim str As String
                str = "<script language='javascript'>window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&PickAtch=1&isAttachmentRequired=1&Lsemail=" & HttpUtility.JavaScriptStringEncode(txtConEmail.Text) & "&pqwRT=" & txtConID.Text & "&OppID=" & lngReferenceID.ToString() & "&RefType=" & sintRefType & "&CaseBizDoc=" & "MirrorBizDoc" & "&OppType=" & hdnOppType.Value & "','ComposeWindow','toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')</script>"
                'Response.Write(str)
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Email", str, False)
                getBizDocDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Private Sub SetWebControlAttributes()
            Try
                ibtnLogo.Attributes.Add("onclick", "return OpenLogoPage();")
                'hplAmountPaid.Attributes.Add("onclick", "return OpenAmtPaid(" & sintRefType & "," & lngReferenceID.ToString & " ," & hdnDivID.Value & ");")
                ibtnPrint.Attributes.Add("onclick", "return PrintIt();")
                ibtnExport.Attributes.Add("onclick", "OpenExport(" & sintRefType.ToString & "," & lngReferenceID.ToString & "); return false;")
                hplOppID.Attributes.Add("onclick", "return Close1(" & lngReferenceID.ToString & ")")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub ibtnExportPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnExportPDF.Click
            Try
                hdnBizDocHTML.Value = "<body><link rel='stylesheet' href='" & Server.MapPath("~/CSS/master.css") & "' type='text/css' />" + hdnBizDocHTML.Value + "</body>"

                Dim objBizDocs As New OppBizDocs
                Dim strFileName As String = ""
                Dim strFilePhysicalLocation As String = ""

                Dim objHTMLToPDF As New HTMLToPDF
                strFileName = objHTMLToPDF.ConvertHTML2PDF(hdnBizDocHTML.Value, CCommon.ToLong(Session("DomainID")))
                strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName

                Response.Clear()
                Response.ClearContent()
                Response.ContentType = "application/pdf"
                Response.AddHeader("content-disposition", "attachment; filename=" + IIf(lblBizDocIDValue.Text.Trim.Length > 0, lblBizDocIDValue.Text.Trim.Replace(" ", "_") + ".pdf", strFileName))
                Response.WriteFile(strFilePhysicalLocation)
                Response.End()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub SendEmail(ByVal boolApproved As Boolean)
            Try
                Dim objSendEmail As New Email

                objSendEmail.DomainID = Session("DomainID")
                objSendEmail.TemplateCode = "#SYS#DOC_APPROVAL/DECLINED_NOTIFICATION"
                objSendEmail.ModuleID = 1
                objSendEmail.RecordIds = txtBizDocRecOwner.Text
                objSendEmail.AdditionalMergeFields.Add("DocumentApprovalStatus", IIf(boolApproved, "Approved", "Declined"))
                objSendEmail.AdditionalMergeFields.Add("DocumentApprovalComment", "")
                objSendEmail.AdditionalMergeFields.Add("DocumentName", lblBizDocIDValue.Text.Trim)
                objSendEmail.AdditionalMergeFields.Add("LoggedInUser", Session("ContactName"))
                objSendEmail.FromEmail = Session("UserEmail")
                objSendEmail.ToEmail = "##ContactFirstName## <##ContactEmail##>"
                objSendEmail.SendEmailTemplate()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindBizDocsTemplate()
            Try
                Dim objOppBizDoc As New OppBizDocs
                objOppBizDoc.DomainID = Session("DomainID")
                objOppBizDoc.BizDocId = hdnBizDocId.Value
                objOppBizDoc.OppType = hdnOppType.Value

                Dim dtBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()

                ddlBizDocTemplate.DataSource = dtBizDocTemplate
                ddlBizDocTemplate.DataTextField = "vcTemplateName"
                ddlBizDocTemplate.DataValueField = "numBizDocTempID"
                ddlBizDocTemplate.DataBind()


                'ddlBizDocTemplate.Items.Insert(0, New ListItem("--Select--", 0))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlBizDocTemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBizDocTemplate.SelectedIndexChanged
            Try
                If CCommon.ToLong(ddlBizDocTemplate.SelectedValue) > 0 Then
                    ddlBizDocTemplate_SelectedIndexChanged()
                End If

                getBizDocDetails()
                'End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlBizDocTemplate_SelectedIndexChanged()
            Try
                objCommon = New CCommon
                If sintRefType = enmReferenceTypeForMirrorBizDocs.SalesReturns OrElse sintRefType = enmReferenceTypeForMirrorBizDocs.PurchaseReturns Then
                    objCommon.Mode = 29
                ElseIf sintRefType = enmReferenceTypeForMirrorBizDocs.SalesCreditMemo OrElse sintRefType = enmReferenceTypeForMirrorBizDocs.CreditMemo Then
                    objCommon.Mode = 30
                ElseIf sintRefType = enmReferenceTypeForMirrorBizDocs.SalesOrder OrElse sintRefType = enmReferenceTypeForMirrorBizDocs.PurchaseOrder _
                    OrElse sintRefType = enmReferenceTypeForMirrorBizDocs.SalesOpportuinity OrElse sintRefType = enmReferenceTypeForMirrorBizDocs.PurchaseOpportunity Then
                    objCommon.Mode = 31
                ElseIf sintRefType = enmReferenceTypeForMirrorBizDocs.CreditMemo OrElse sintRefType = enmReferenceTypeForMirrorBizDocs.RefundReceipt Then
                    objCommon.Mode = 33
                End If

                objCommon.UpdateRecordID = lngReferenceID
                objCommon.UpdateValueID = CCommon.ToLong(ddlBizDocTemplate.SelectedValue)
                objCommon.UpdateSingleFieldValue()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
    End Class
End Namespace
