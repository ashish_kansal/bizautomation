﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmTransactionDetails.aspx.vb"
    Inherits=".frmOrderPaymentDetails" MasterPageFile="~/common/Popup.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Transaction Details</title>
    <script language="JavaScript" type="text/javascript" src="../javascript/date-picker.js"></script>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script language="javascript" type="text/javascript">
        function OpenAmtPaid(a, b, c, d) {
            RPPopup = window.open('../opportunity/frmAmtPaid.aspx?d=' + a + '&a=' + b + '&b=' + c + '&c=' + d, 'ReceivePayment', 'toolbar=no,titlebar=no,top=300,width=600,height=270,scrollbars=no,resizable=no');
            console.log(RPPopup);
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function OpenOpp(a, b) {
            var str;
            str = "../opportunity/frmOpportunities.aspx?frm=OpenBizDoc&OpID=" + a + "&OppType=" + b;

            document.location.href = str;
        }
        function OpenBizDoc(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }

        function DeleteMessage() {
            alert("You cannot delete Payment Details for which amount is deposited !");
            return false;
        }

        $(document).ready(function () {
            InitializeValidation();
        });

    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClientClick="return Close();">
            </asp:Button>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblHeader" runat="server" Text="Transaction Details"></asp:Label>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="1000px">
        <tr valign="top">
            <td valign="top">
                <asp:DataGrid ID="dgPaymentDetails" runat="server" CssClass="dgNHover" Width="100%"
                    AutoGenerateColumns="False" CellPadding="2" CellSpacing="2">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs" HorizontalAlign="Center"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Transaction#" ItemStyle-Wrap="false">
                            <ItemTemplate>
                                <asp:Label ID="lblTransactionID" runat="server" Text='<%# Eval("vcTransactionID") & If(Not String.IsNullOrEmpty(Eval("vcResponseCode")), " (" & Eval("vcResponseCode") & ")", "")%>'></asp:Label>
                                <asp:HiddenField runat="server" ID="hdnTransHistoryID" Value='<%# Eval("numTransHistoryID") %>' />
                                <asp:HiddenField runat="server" ID="hdnTransactionID" Value='<%# Eval("vcTransactionID") %>' />
                                <asp:HiddenField runat="server" ID="hdnDivisionID" Value='<%# Eval("numDivisionID") %>' />
                                <asp:HiddenField runat="server" ID="hdnContactID" Value='<%# Eval("numContactID") %>' />
                                <asp:HiddenField runat="server" ID="hdnOppID" Value='<%# Eval("numOppID") %>' />
                                <asp:HiddenField runat="server" ID="hdnOppBizDocID" Value='<%# Eval("numOppBizDocsID") %>' />
                                <asp:HiddenField runat="server" ID="hdnCardNo" Value='<%# Eval("vcCreditCardNo") %>' />
                                <asp:HiddenField runat="server" ID="hdnCVVNo" Value='<%# Eval("vcCVV2") %>' />
                                <asp:HiddenField runat="server" ID="hdnMonth" Value='<%# Eval("tintValidMonth") %>' />
                                <asp:HiddenField runat="server" ID="hdnYear" Value='<%# Eval("intValidYear") %>' />
                                <asp:HiddenField runat="server" ID="hdnPaymentGateway" Value='<%# Eval("intPaymentGateWay") %>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Status" HeaderStyle-Wrap="false">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblStatus" Text='<%# Eval("vcTransactionStatus") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Authorize Amt" HeaderStyle-HorizontalAlign="Center"  HeaderStyle-Wrap="false">
                            <ItemStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblAuthorizeAmt" Text='<%# String.Format("{0:#,##0.00}", Eval("monAuthorizedAmt")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Capture Amt" HeaderStyle-HorizontalAlign="Center"  HeaderStyle-Wrap="false">
                            <ItemStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblCapturedAmt" Text='<%# String.Format("{0:#,##0.00}", Eval("monCapturedAmt")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Refund Amt" HeaderStyle-HorizontalAlign="Center"  HeaderStyle-Wrap="false">
                            <ItemStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblRefundAmt" Text='<%# String.Format("{0:#,##0.00}", Eval("monRefundAmt")) %>'></asp:Label>&nbsp;
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Date" ItemStyle-Wrap="false">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblDate" Text='<%# ReturnName(DataBinder.Eval(Container.DataItem, "dtCreatedDate"))%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="BizDoc ID / Order ID"  HeaderStyle-Wrap="false">
                            <ItemTemplate>
                                <asp:HyperLink ID="hplBizDoc" runat="server">
                                    <asp:Label runat="server" ID="lblBizOrderID" Text='<%# Eval("vcBizOrderID") %>'></asp:Label></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Card No" HeaderStyle-Wrap="false">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblCardNo"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Card Type" HeaderStyle-Wrap="false">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblCardType" Text='<%# Eval("vcCardType") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>

                        <asp:TemplateColumn HeaderText="Amount">
                            <ItemTemplate>
                                <table width="100%" cellpadding="2" cellspacing="2" border="1" style="border: 1px dotted gray"
                                    id="tblActions" runat="server">
                                    <tr>
                                        <td style="text-align: right">
                                            <asp:TextBox ID="txtAmount" runat="server" Width="60"></asp:TextBox>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnCredit" runat="server" Text="Refund" CssClass="ybutton" CommandName="Credit" />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
</asp:Content>
