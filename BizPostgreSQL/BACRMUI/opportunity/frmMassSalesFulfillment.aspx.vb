﻿Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Opportunities
    Public Class frmMassSalesFulfillment
        Inherits BACRMPage

#Region "Page Events"
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not Page.IsPostBack Then
                    hdnMSWarehouseID.Value = CCommon.ToLong(Session("DefaultWarehouse"))
                    hdnMSImagePath.Value = CCommon.GetDocumentPath(Session("DomainID"))
                    hdnMSCurrencyID.Value = CCommon.ToString(Session("Currency"))
                    hdnDecimalPoints.Value = CCommon.ToShort(Session("DecimalPoints"))
                    hdnDefaultShippingBizDoc.Value = CCommon.ToLong(Session("numDefaultSalesShippingDoc"))

                    If GetQueryStringVal("View") <> "" Then
                        hdnViewID.Value = GetQueryStringVal("View")
                    End If
                    PersistTable.Load(boolOnlyURL:=True)
                    Dim queryViewId As Int16
                    queryViewId = CCommon.ToInteger(GetQueryStringVal("ViewId"))
                    If queryViewId > 0 Then
                        hdnLastViewMode.Value = queryViewId
                    Else
                        If PersistTable.Count > 0 Then
                            hdnLastViewMode.Value = CCommon.ToInteger(PersistTable("viewID"))
                            If hdnLastViewMode.Value = "2" Then
                                hdnPackLastSubViewMode.Value = CCommon.ToInteger(PersistTable("subViewID"))
                            ElseIf hdnLastViewMode.Value = "6" Then
                                hdnPrintLastSubViewMode.Value = CCommon.ToInteger(PersistTable("subViewID"))
                            End If
                        End If
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DirectCast(Page.Master, BizMaster).DisplayError(ex.Message)
            End Try
        End Sub
#End Region



    End Class
End Namespace