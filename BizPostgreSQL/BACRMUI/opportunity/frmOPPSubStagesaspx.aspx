<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmOPPSubStagesaspx.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmOPPSubStagesaspx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Sub Stages</title>
		<script language="javascript">
			function Close()
			{
				window.close()
			}
		</script>
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
			<table width="100%" align="center">
				<tr valign="top">
					<td align="right" colSpan="2"><asp:button id="btnSave" Text="Save" CssClass="button" Runat="server"></asp:button>
					<asp:button id="btnSaveClose" Text="Save &amp; Close" CssClass="button" Runat="server"></asp:button>
					<asp:button id="btnCancel" Text="Close" CssClass="button" Runat="server"></asp:button>
					</td>
				</tr>
				<tr>
				</tr>
				<tr>
					<td class="normal1" align="right">Choose Sub Stage :
					</td>
					<td><asp:dropdownlist id="ddlSubStage" CssClass="signup" Runat="server" Width="150" AutoPostBack="true"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td colSpan="2"><asp:table id="tblSubStage" Runat="server" Width="100%"></asp:table></td>
				</tr>
			</table>
		</form>
	</body>
</HTML>
