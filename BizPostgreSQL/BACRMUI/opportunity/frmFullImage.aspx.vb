Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Partial Class frmFullImage
    Inherits BACRMPage

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub

    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim lngItemCode As Long
            lngItemCode = GetQueryStringVal( "ItemCode")
            If Not IsPostBack Then
                
                Dim objItems As New CItems
                Dim dtItemDetails As DataTable
                objItems.ItemCode = lngItemCode
                dtItemDetails = objItems.ItemDetails
                If dtItemDetails.Rows.Count > 0 Then
                    If Not IsDBNull(dtItemDetails.Rows(0).Item("vcPathForImage")) Then
                        If dtItemDetails.Rows(0).Item("vcPathForImage") <> "" Then
                            imgItem.Visible = True
                            imgItem.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtItemDetails.Rows(0).Item("vcPathForImage")
                        Else : imgItem.Visible = False
                        End If
                    Else : imgItem.Visible = False
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class
