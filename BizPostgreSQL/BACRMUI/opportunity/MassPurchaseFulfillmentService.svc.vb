﻿Imports System.ServiceModel
Imports System.ServiceModel.Activation
Imports System.ServiceModel.Web
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports Newtonsoft.Json
Imports BACRM.BusinessLogic.Accounting
Imports System.Text.RegularExpressions

<ServiceContract(Namespace:="MassPurchaseFulfillmentService")>
<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)>
Public Class MassPurchaseFulfillmentService

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="GET", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetMassPurchaseFulfillmentConfiguration")>
    Public Function GetMassPurchaseFulfillmentConfiguration() As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objMassPurchaseFulfillment As New MassPurchaseFulfillment
                objMassPurchaseFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objMassPurchaseFulfillment.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                Dim dt As DataTable = objMassPurchaseFulfillment.GetConfiguration()

                Return JsonConvert.SerializeObject(dt, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetRecords")>
    Public Function GetRecords(ByVal viewID As Short, ByVal warehouseID As Long, ByVal isGroupByOrder As Boolean, ByVal isIncludeSearch As Boolean, ByVal selectedOrderStatus As String, ByVal customSearchValue As String, ByVal receiveType As Short, ByVal pageIndex As Integer, ByVal sortColumn As String, ByVal sortOrder As String, ByVal tintFlag As Short) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objMassPurchaseFulfillment As New MassPurchaseFulfillment
                objMassPurchaseFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objMassPurchaseFulfillment.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objMassPurchaseFulfillment.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                objMassPurchaseFulfillment.ViewID = viewID
                objMassPurchaseFulfillment.WarehouseID = warehouseID
                objMassPurchaseFulfillment.IsGroupByOrder = isGroupByOrder
                objMassPurchaseFulfillment.tintFlag = tintFlag
                objMassPurchaseFulfillment.IsIncludeSearch = isIncludeSearch
                objMassPurchaseFulfillment.SelectedOrderStaus = selectedOrderStatus
                objMassPurchaseFulfillment.CustomSearchValue = customSearchValue
                objMassPurchaseFulfillment.ReceiveType = receiveType
                objMassPurchaseFulfillment.PageIndex = pageIndex
                objMassPurchaseFulfillment.SortColumn = sortColumn
                objMassPurchaseFulfillment.SortOrder = sortOrder


                Dim ds As DataSet = objMassPurchaseFulfillment.GetRecords()

                Return JsonConvert.SerializeObject(New With {Key .Records = JsonConvert.SerializeObject(ds.Tables(0), Formatting.None), Key .LeftPaneFields = JsonConvert.SerializeObject(ds.Tables(1), Formatting.None), Key .TotalRecords = objMassPurchaseFulfillment.TotalRecords, Key .SortColumn = objMassPurchaseFulfillment.SortColumn, Key .SortOrder = objMassPurchaseFulfillment.SortOrder}, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED") Then
                Dim objMSException As New MSFException
                objMSException.ErrorMessage = "DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED"
                objMSException.ErrorCode = 500

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Dim objMSException As New MSFException
                objMSException.ErrorMessage = ex.Message
                objMSException.ErrorCode = 500

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            End If
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/ReceiveItems")>
    Public Function ReceiveItems(ByVal selectedRecords As String)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim ds As New DataSet
                Dim dt As DataTable = JsonConvert.DeserializeObject(Of DataTable)(selectedRecords)
                ds.Tables.Add(dt)
                ds.AcceptChanges()

                Dim objMassPurchaseFulfillment As New MassPurchaseFulfillment
                objMassPurchaseFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objMassPurchaseFulfillment.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objMassPurchaseFulfillment.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                objMassPurchaseFulfillment.SelectedRecords = ds.GetXml()
                Dim dtResult As DataTable = objMassPurchaseFulfillment.ReceiveOnlyItems()
                Return JsonConvert.SerializeObject(dtResult, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("MISSING_PC_ACCOUNT") Then
                Dim objMSException As New MSFException
                objMSException.ErrorMessage = "Please Set Default Purchase Clearing account from ""Administration -> Global Settings -> Accounting -> Default Accounts"" To Save"
                objMSException.ErrorCode = 500

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            ElseIf ex.Message.Contains("MISSING_PV_ACCOUNT") Then
                Dim objMSException As New MSFException
                objMSException.ErrorMessage = "Please Set Default Purchase Price Variance account from ""Administration -> Global Settings -> Accounting -> Default Accounts"" To Save"
                objMSException.ErrorCode = 500

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Dim objMSException As New MSFException
                objMSException.ErrorMessage = ex.Message
                objMSException.ErrorCode = 500

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            End If
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/PutAwayItems")>
    Public Function PutAwayItems(ByVal mode As Short, ByVal receivedDate As DateTime, ByVal selectedRecords As String)
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                If ChartOfAccounting.GetDefaultAccount("PC", HttpContext.Current.Session("DomainID")) = 0 Then
                    Throw New Exception("MISSING_PC_ACCOUNT")
                End If

                If ChartOfAccounting.GetDefaultAccount("PV", HttpContext.Current.Session("DomainID")) = 0 Then
                    Throw New Exception("MISSING_PV_ACCOUNT")
                End If

                receivedDate = DateAdd(DateInterval.Minute, CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset")) * -1, receivedDate)

                Dim dt As DataTable = JsonConvert.DeserializeObject(Of DataTable)(selectedRecords)

                For Each dr As DataRow In dt.Rows
                    Try
                        ReceiveAndPutAwayItem(dr, receivedDate, mode)

                        dr("IsSuccess") = True
                        dr("ErrorMessage") = ""
                    Catch ex As Exception
                        dr("IsSuccess") = False
                        dr("ErrorMessage") = ex.Message
                    End Try
                Next

                dt.AcceptChanges()

                Return JsonConvert.SerializeObject(dt, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("MISSING_PC_ACCOUNT") Then
                Dim objMSException As New MSFException
                objMSException.ErrorMessage = "Please Set Default Purchase Clearing account from ""Administration -> Global Settings -> Accounting -> Default Accounts"" To Save"
                objMSException.ErrorCode = 500

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            ElseIf ex.Message.Contains("MISSING_PV_ACCOUNT") Then
                Dim objMSException As New MSFException
                objMSException.ErrorMessage = "Please Set Default Purchase Price Variance account from ""Administration -> Global Settings -> Accounting -> Default Accounts"" To Save"
                objMSException.ErrorCode = 500

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Dim objMSException As New MSFException
                objMSException.ErrorMessage = ex.Message
                objMSException.ErrorCode = 500

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            End If
        End Try
    End Function

    Private Sub ReceiveAndPutAwayItem(ByVal dr As DataRow, ByVal receivedDate As DateTime, ByVal mode As Short)
        Try
            Dim oppID As Long
            Dim woID As Long
            Dim isStockTransfer As Boolean
            Dim isSerilizedItem As Boolean
            Dim isLotItem As Boolean
            Dim intQtyReceived As Double
            Dim itemName As String
            Dim serialLots As String

            Using transScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                If CCommon.ToLong(dr("WOID")) > 0 Then
                    Try
                        woID = CCommon.ToLong(dr("WOID"))
                        isStockTransfer = CCommon.ToBool(dr("IsStockTransfer"))
                        isSerilizedItem = CCommon.ToBool(dr("IsSerial"))
                        isLotItem = CCommon.ToBool(dr("IsLot"))
                        intQtyReceived = CCommon.ToDouble(dr("QtyToReceive"))

                        If intQtyReceived > 0 Then
                            Dim objWorkOrder As New WorkOrder
                            objWorkOrder.DomainID = HttpContext.Current.Session("DomainID")
                            objWorkOrder.UserCntID = HttpContext.Current.Session("UserContactID")
                            objWorkOrder.WorkOrderID = CCommon.ToLong(dr("WOID"))
                            objWorkOrder.Quantity = CCommon.ToDouble(dr("QtyToReceive"))
                            objWorkOrder.PutAway(mode, CDate(receivedDate.ToShortDateString() & " 12:00:00"), CCommon.ToString(dr("WarehouseItemIDs")))

                            Dim assetChartAcntId As Long
                            Dim avgCost As Decimal
                            Dim objItems As New BACRM.BusinessLogic.Item.CItems
                            objItems.numWOId = woID
                            objItems.UserCntID = HttpContext.Current.Session("UserContactID")
                            objItems.DomainID = HttpContext.Current.Session("DomainID")
                            objItems.ItemCode = CCommon.ToLong(dr("ItemCode"))
                            objItems.ClientZoneOffsetTime = HttpContext.Current.Session("ClientMachineUTCTimeOffset")
                            Dim dt As DataTable = objItems.ItemDetails
                            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                                assetChartAcntId = CCommon.ToLong(dt.Rows(0)("numAssetChartAcntId"))
                                avgCost = CCommon.ToDecimal(dt.Rows(0)("monAverageCost"))
                            End If


                            Dim lngWIPAccount As Long = BACRM.BusinessLogic.Accounting.ChartOfAccounting.GetDefaultAccount("WP", HttpContext.Current.Session("DomainID"))

                            If assetChartAcntId > 0 And lngWIPAccount > 0 Then
                                Dim journalID As Long = objItems.SaveJournalHeader(intQtyReceived)
                                objItems.SaveDataToGeneralJournalDetails(journalID, assetChartAcntId, lngWIPAccount, intQtyReceived, avgCost, 1, CCommon.ToLong(dr("ItemCode")))
                            End If
                        End If
                    Catch ex As Exception
                        If ex.Message.Contains("RECEIVED_QTY_MUST_BE_LESS_THEN_OR_EQUAL_TO_ORDERED_QTY") Then
                            Throw New Exception("Received quantity can not be greater then ordered quantity")
                        ElseIf ex.Message.Contains("QTY_TO_RECEIVE_NOT_PROVIDED") Then
                            Throw New Exception("Received quantity must be greater then 0")
                        ElseIf ex.Message.Contains("LOCATION_NOT_SELECTED") Then
                            Throw New Exception("Warehouse location(s) to put-away not provided")
                        ElseIf ex.Message.Contains("INALID_QTY_RECEIVED") Then
                            Throw New Exception("Received quantity is not same as whats added in warehouse location(s)")
                        ElseIf ex.Message.Contains("SERIALLOT_REQUIRED") Then
                            Throw New Exception("Serial/Lot# are required")
                        ElseIf ex.Message.Contains("INALID_SERIALLOT") Then
                            Throw New Exception("Serial/Lot# provided are not same as Received quantity")
                        ElseIf ex.Message.Contains("INVALID_WAREHOUSE_LOCATION") Then
                            Throw New Exception("Warehouse internal location does not exists")
                        ElseIf ex.Message.Contains("INVALID_WAREHOUSE") Then
                            Throw New Exception("Warehouse does not exists")
                        ElseIf ex.Message.Contains("INVALID_SERIAL_NO") Then
                            Throw New Exception("Searial/Lot number are not in proper format.")
                        ElseIf ex.Message.Contains("INVALID_LOT_QUANTITY") Then
                            Throw New Exception("Selected Lot number does not have required quantity in biz")
                        ElseIf ex.Message.Contains("INVALID_SERIAL_QUANTITY") Then
                            Throw New Exception("Selected Serial number is not available")
                        ElseIf ex.Message.Contains("DUPLICATE_SERIAL_NO") Then
                            Throw New Exception("Duplicate serial/lot number")
                        Else
                            Throw New Exception("Unknown error occured: " & ex.Message)
                        End If
                    End Try
                Else
                    Try
                        oppID = CCommon.ToLong(dr("OppID"))
                        isStockTransfer = CCommon.ToBool(dr("IsStockTransfer"))
                        isSerilizedItem = CCommon.ToBool(dr("IsSerial"))
                        isLotItem = CCommon.ToBool(dr("IsLot"))
                        intQtyReceived = CCommon.ToDouble(dr("QtyToReceive"))
                        itemName = CCommon.ToString(dr("ItemName"))

                        If intQtyReceived > 0 Then
                            Dim objOpp As New COpportunities
                            objOpp.DomainID = HttpContext.Current.Session("DomainID")
                            objOpp.UserCntID = HttpContext.Current.Session("UserContactID")
                            objOpp.byteMode = mode
                            objOpp.OpportID = CCommon.ToLong(dr("OppID"))
                            objOpp.OppItemCode = CCommon.ToLong(dr("OppItemID"))
                            objOpp.UnitHour = CCommon.ToDouble(dr("QtyToReceive"))
                            objOpp.dtItemReceivedDate = CDate(receivedDate.ToShortDateString() & " 12:00:00")
                            objOpp.BizDocId = 0
                            objOpp.PutAway(CCommon.ToString(dr("WarehouseItemIDs")))
                        End If

                        If CCommon.ToBool(dr("IsPPVariance")) Then
                            Dim dtMain As New DataTable
                            CCommon.AddColumnsToDataTable(dtMain, "numDivisionID,vcPOppName,numoppitemtCode,numItemCode,vcItemName,monPrice,numUnitReceived,numCurrencyID,fltExchangeRate,charItemType,ItemType,DropShip,numProjectID,numClassID,ItemInventoryAsset,ItemCoGs,itemIncomeAccount")
                            Dim drAcc As DataRow
                            drAcc = dtMain.NewRow

                            drAcc("numDivisionID") = CCommon.ToLong(dr("DivisionID"))
                            drAcc("vcPOppName") = CCommon.ToString(dr("OppName"))
                            drAcc("numoppitemtCode") = CCommon.ToLong(dr("OppItemID"))
                            drAcc("numItemCode") = CCommon.ToLong(dr("ItemCode"))
                            drAcc("vcItemName") = CCommon.ToLong(dr("ItemName"))
                            drAcc("monPrice") = CCommon.ToDecimal(dr("Price"))
                            drAcc("numUnitReceived") = intQtyReceived
                            drAcc("numCurrencyID") = CCommon.ToLong(dr("CurrencyID"))
                            drAcc("fltExchangeRate") = CCommon.ToDecimal(dr("ExchangeRate"))
                            drAcc("charItemType") = CCommon.ToString(dr("CharItemType"))
                            drAcc("ItemType") = CCommon.ToString(dr("ItemType"))
                            drAcc("DropShip") = CCommon.ToBool(dr("DropShip"))
                            drAcc("numProjectID") = CCommon.ToLong(dr("ProjectID"))
                            drAcc("numClassID") = CCommon.ToLong(dr("ClassID"))
                            drAcc("ItemInventoryAsset") = CCommon.ToLong(dr("AssetChartAcntId"))
                            drAcc("ItemCoGs") = CCommon.ToLong(dr("COGsChartAcntId"))
                            drAcc("itemIncomeAccount") = CCommon.ToLong(dr("IncomeChartAcntId"))

                            dtMain.Rows.Add(drAcc)

                            Dim JournalId As Long = 0

                            Dim objOppBizDocs As New OppBizDocs
                            objOppBizDocs.OppId = oppID
                            objOppBizDocs.DomainID = HttpContext.Current.Session("DomainID")
                            objOppBizDocs.UserCntID = HttpContext.Current.Session("UserContactID")

                            JournalId = objOppBizDocs.SaveDataToHeader(drAcc("numUnitReceived") * drAcc("monPrice") * dtMain(0)("fltExchangeRate"), CDate(receivedDate.ToShortDateString() & " 12:00:00"), Description:=CCommon.ToString(dr("OppName")))

                            Dim objJournalEntries As New JournalEntry
                            objJournalEntries.SaveJournalEntriesPurchaseClearing(oppID, HttpContext.Current.Session("DomainID"), dtMain, JournalId)
                            'End If
                        End If
                    Catch ex As Exception
                        If ex.Message.Contains("INVALID_VENDOR_INVOICE_RECEIVE_QTY") Then
                            Throw New Exception("Invalid vendor invoice received quantity")
                        ElseIf ex.Message.Contains("RECEIVED_QTY_MUST_BE_LESS_THEN_OR_EQUAL_TO_ORDERED_QTY") Then
                            Throw New Exception("Received quantity can not be greater than ordered quantity")
                        ElseIf ex.Message.Contains("QTY_TO_RECEIVE_NOT_PROVIDED") Then
                            Throw New Exception("Received quantity must be greater then 0")
                        ElseIf ex.Message.Contains("LOCATION_NOT_SELECTED") Then
                            Throw New Exception("Warehouse location(s) to put-away not provided")
                        ElseIf ex.Message.Contains("INALID_QTY_RECEIVED") Then
                            Throw New Exception("Received quantity is not same as whats added in warehouse location(s)")
                        ElseIf ex.Message.Contains("SERIALLOT_REQUIRED") Then
                            Throw New Exception("Serial/Lot# are required")
                        ElseIf ex.Message.Contains("INALID_SERIALLOT") Then
                            Throw New Exception("Serial/Lot# provided are not same as Received quantity")
                        ElseIf ex.Message.Contains("INVALID_WAREHOUSE_LOCATION") Then
                            Throw New Exception("Warehouse internal location does not exists")
                        ElseIf ex.Message.Contains("INVALID_WAREHOUSE") Then
                            Throw New Exception("Warehouse does not exists")
                        ElseIf ex.Message.Contains("INVALID_SERIAL_NO") Then
                            Throw New Exception("Searial/Lot number are not in proper format.")
                        ElseIf ex.Message.Contains("INVALID_LOT_QUANTITY") Then
                            Throw New Exception("Selected Lot number does not have required quantity in biz")
                        ElseIf ex.Message.Contains("INVALID_SERIAL_QUANTITY") Then
                            Throw New Exception("Selected Serial number is not available")
                        ElseIf ex.Message.Contains("DUPLICATE_SERIAL_NO") Then
                            Throw New Exception("Duplicate serial/lot number")
                        Else
                            Throw New Exception("Unknown error occured: " & ex.Message)
                        End If
                    End Try
                End If
                
                transScope.Complete()
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/BillOrders")>
    Public Function BillOrders(ByVal orders As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim dt As DataTable = JsonConvert.DeserializeObject(Of DataTable)(orders)

                Dim objOpportunity As COpportunities
                Dim objMassPurchaseFulfillment As New MassPurchaseFulfillment
                objMassPurchaseFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objMassPurchaseFulfillment.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))

                For Each dr As DataRow In dt.Rows
                    Try
                        objOpportunity = New COpportunities
                        objOpportunity.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                        objOpportunity.OpportID = CCommon.ToLong(dr("OppID"))
                        Dim dtOrder As DataTable = objOpportunity.GetOrderDetailForBizDoc()

                        If Not dtOrder Is Nothing AndAlso dtOrder.Rows.Count > 0 Then
                            objMassPurchaseFulfillment.OppID = CCommon.ToLong(dr("OppID"))
                            objMassPurchaseFulfillment.SelectedRecords = CCommon.ToString(dr("OppItemIDs"))
                            Dim dtOrderItems As DataTable = objMassPurchaseFulfillment.GetOrderItems(1)

                            If Not dtOrderItems Is Nothing AndAlso dtOrderItems.Rows.Count > 0 Then
                                Dim objOpportunityBizDocs As New OpportunityBizDocs
                                objOpportunityBizDocs.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                                objOpportunityBizDocs.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                                objOpportunityBizDocs.ClientMachineUTCTimeOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                                objOpportunityBizDocs.DivisionID = CCommon.ToLong(dtOrder.Rows(0)("numDivisionID"))
                                objOpportunityBizDocs.CompanyName = CCommon.ToString(dtOrder.Rows(0)("vcCompanyName"))
                                objOpportunityBizDocs.OppID = CCommon.ToLong(dr("OppID"))
                                objOpportunityBizDocs.OppType = CCommon.ToShort(dtOrder.Rows(0)("tintOppType"))
                                objOpportunityBizDocs.ShipVia = CCommon.ToLong(dtOrder.Rows(0)("intUsedShippingCompany"))
                                objOpportunityBizDocs.BizDocType = 644
                                objOpportunityBizDocs.IsDeferred = False
                                objOpportunityBizDocs.BizDocItems = GetBizDocItems(dtOrderItems)
                                objOpportunityBizDocs.IsEnableDeferredIncome = CCommon.ToBool(HttpContext.Current.Session("IsEnableDeferredIncome"))
                                objOpportunityBizDocs.IsAutolinkUnappliedPayment = CCommon.ToBool(HttpContext.Current.Session("AutolinkUnappliedPayment"))
                                objOpportunityBizDocs.ExchangeRate = CCommon.ToDouble(HttpContext.Current.Session("fltExchangeRate"))
                                objOpportunityBizDocs.RefOrderNo = CCommon.ToString(dr("InvoiceNo"))
                                If Not String.IsNullOrEmpty(CCommon.ToString(dr("BillingDate"))) Then
                                    objOpportunityBizDocs.FromDate = Convert.ToDateTime(dr("BillingDate"))
                                End If

                                objOpportunityBizDocs.CreateBizDoc(False)

                                dr("IsSucess") = True
                                dr("ErrorMessage") = ""
                            Else
                                dr("IsSucess") = False
                                dr("ErrorMessage") = "No item(s) left to bill"
                            End If
                        Else
                            dr("IsSucess") = False
                            dr("ErrorMessage") = "Order doesn't exists."
                        End If
                    Catch ex As Exception
                        dr("IsSucess") = False

                        If ex.Message.Contains("WORK_ORDER_NOT_COMPLETED") Then
                            dr("ErrorMessage") = "Not able to add invoice because some work orders are still not completed."
                        ElseIf ex.Message.Contains("QTY_MISMATCH") Then
                            dr("ErrorMessage") = "Not able to add bill because qty is not same as required qty."
                        ElseIf ex.Message.Contains("REQUIRED_SERIALS_NOT_PROVIDED") Then
                            dr("ErrorMessage") = "Not able to add bill because required number of serials are not provided."
                        ElseIf ex.Message.Contains("INVALID_SERIAL_NUMBERS") Then
                            dr("ErrorMessage") = "Not able to add bill because invalid serials are provided."
                        ElseIf ex.Message.Contains("REQUIRED_LOTNO_NOT_PROVIDED") Then
                            dr("ErrorMessage") = "Not able to add bill because required number of Lots are not provided."
                        ElseIf ex.Message.Contains("INVALID_LOT_NUMBERS") Then
                            dr("ErrorMessage") = "Not able to add bill because invalid Lots are provided."
                        ElseIf ex.Message.Contains("SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY") Then
                            dr("ErrorMessage") = "Not able to add bill because Lot number do not have enough qty to fulfill."
                        ElseIf ex.Message.Contains("NOTSUFFICIENTQTY_ALLOCATION") Then
                            dr("ErrorMessage") = "Not able to add bill because warehouse allocation is invlid."
                        ElseIf ex.Message.Contains("NOTSUFFICIENTQTY_ONHAND") Then
                            dr("ErrorMessage") = "Not able to add bill because warehouse does not have enought on hand quantity."
                        ElseIf ex.Message.Contains("BIZDOC_QTY_MORE_THEN_ORDERED_QTY") Then
                            dr("ErrorMessage") = "Not able to add bill because bizdoc qty is more than ordered quantity for some item(s)."
                        Else
                            dr("ErrorMessage") = ex.Message
                        End If
                    End Try
                Next

                dt.AcceptChanges()

                Return JsonConvert.SerializeObject(dt, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)

            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/CloseOrders")>
    Public Function CloseOrders(ByVal orders As String, ByVal isReceiveBillOnClose As Boolean) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                If ChartOfAccounting.GetDefaultAccount("PC", HttpContext.Current.Session("DomainID")) = 0 Then
                    Throw New Exception("MISSING_PC_ACCOUNT")
                End If

                If ChartOfAccounting.GetDefaultAccount("PV", HttpContext.Current.Session("DomainID")) = 0 Then
                    Throw New Exception("MISSING_PV_ACCOUNT")
                End If

                Dim receivedDate As DateTime = DateAdd(DateInterval.Minute, CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset")) * -1, DateTime.UtcNow)

                Dim objOpportunity As COpportunities
                Dim objMassPurchaseFulfillment As New MassPurchaseFulfillment
                objMassPurchaseFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objMassPurchaseFulfillment.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))

                Dim dt As DataTable = JsonConvert.DeserializeObject(Of DataTable)(orders)

                If isReceiveBillOnClose Then
                    For Each dr As DataRow In dt.Rows
                        Try
                            Using transScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                '1: Mark item(s) as received
                                objMassPurchaseFulfillment.OppID = CCommon.ToLong(dr("OppID"))
                                objMassPurchaseFulfillment.SelectedRecords = ""
                                Dim dtItems As DataTable = objMassPurchaseFulfillment.GetOrderItems(2)

                                If Not dtItems Is Nothing AndAlso dtItems.Rows.Count > 0 Then
                                    For Each drItem As DataRow In dtItems.Rows
                                        ReceiveAndPutAwayItem(drItem, receivedDate, 3)
                                    Next
                                End If

                                '2: Create bill for remaining quantity
                                objOpportunity = New COpportunities
                                objOpportunity.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                                objOpportunity.OpportID = CCommon.ToLong(dr("OppID"))
                                Dim dtOrder As DataTable = objOpportunity.GetOrderDetailForBizDoc()

                                If Not dtOrder Is Nothing AndAlso dtOrder.Rows.Count > 0 Then
                                    objMassPurchaseFulfillment.OppID = CCommon.ToLong(dr("OppID"))
                                    objMassPurchaseFulfillment.SelectedRecords = ""
                                    Dim dtOrderItems As DataTable = objMassPurchaseFulfillment.GetOrderItems(1)

                                    If Not dtOrderItems Is Nothing AndAlso dtOrderItems.Rows.Count > 0 Then
                                        Dim objOpportunityBizDocs As New OpportunityBizDocs
                                        objOpportunityBizDocs.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                                        objOpportunityBizDocs.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                                        objOpportunityBizDocs.ClientMachineUTCTimeOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                                        objOpportunityBizDocs.DivisionID = CCommon.ToLong(dtOrder.Rows(0)("numDivisionID"))
                                        objOpportunityBizDocs.CompanyName = CCommon.ToString(dtOrder.Rows(0)("vcCompanyName"))
                                        objOpportunityBizDocs.OppID = CCommon.ToLong(dr("OppID"))
                                        objOpportunityBizDocs.OppType = CCommon.ToShort(dtOrder.Rows(0)("tintOppType"))
                                        objOpportunityBizDocs.ShipVia = CCommon.ToLong(dtOrder.Rows(0)("intUsedShippingCompany"))
                                        objOpportunityBizDocs.BizDocType = 644
                                        objOpportunityBizDocs.IsDeferred = False
                                        objOpportunityBizDocs.BizDocItems = GetBizDocItems(dtOrderItems)
                                        objOpportunityBizDocs.IsEnableDeferredIncome = CCommon.ToBool(HttpContext.Current.Session("IsEnableDeferredIncome"))
                                        objOpportunityBizDocs.IsAutolinkUnappliedPayment = CCommon.ToBool(HttpContext.Current.Session("AutolinkUnappliedPayment"))
                                        objOpportunityBizDocs.ExchangeRate = CCommon.ToDouble(HttpContext.Current.Session("fltExchangeRate"))
                                        objOpportunityBizDocs.CreateBizDoc(False)
                                    End If
                                Else
                                    dr("IsSucess") = False
                                    dr("ErrorMessage") = "Order doesn't exists."

                                    Continue For
                                End If

                                Dim objOpportunityAutomation As New OpportunityAutomation
                                objOpportunityAutomation.CloseOrder(CCommon.ToLong(HttpContext.Current.Session("DomainID")), CCommon.ToLong(HttpContext.Current.Session("UserContactID")), CCommon.ToLong(dr("OppID")), 2, 1)

                                transScope.Complete()
                            End Using

                            dr("IsSucess") = True
                            dr("ErrorMessage") = ""
                        Catch ex As Exception
                            dr("IsSucess") = False
                            dr("ErrorMessage") = ex.Message
                        End Try

                    Next
                Else
                    For Each dr As DataRow In dt.Rows
                        Try
                            Dim objOpportunityAutomation As New OpportunityAutomation
                            objOpportunityAutomation.CloseOrder(CCommon.ToLong(HttpContext.Current.Session("DomainID")), CCommon.ToLong(HttpContext.Current.Session("UserContactID")), CCommon.ToLong(dr("OppID")), 2, 1)

                            dr("IsSucess") = True
                            dr("ErrorMessage") = ""
                        Catch ex As Exception
                            dr("IsSucess") = False
                            dr("ErrorMessage") = ex.Message
                        End Try
                    Next
                End If

                dt.AcceptChanges()
                Return JsonConvert.SerializeObject(dt, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)

            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetRecordsForPutAway")>
    Public Function GetRecordsForPutAway(ByVal mode As Short, ByVal selectedRecords As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim dtSource As DataTable = JsonConvert.DeserializeObject(Of DataTable)(selectedRecords)
                Dim dsSource As New DataSet
                dsSource.Tables.Add(dtSource)

                Dim objMassPurchaseFulfillment As New MassPurchaseFulfillment
                objMassPurchaseFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objMassPurchaseFulfillment.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objMassPurchaseFulfillment.SelectedRecords = dsSource.GetXml()

                Dim ds As DataSet = objMassPurchaseFulfillment.GetRecordsForPutAway(mode)

                Return JsonConvert.SerializeObject(New With {Key .Records = JsonConvert.SerializeObject(ds.Tables(0), Formatting.None), Key .RightPaneFields = JsonConvert.SerializeObject(ds.Tables(1), Formatting.None)}, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    Private Function GetBizDocItems(ByVal dtOrderedItems As DataTable) As String
        Try
            Dim dtBizDocItems As New DataTable
            Dim strOppBizDocItems As String

            dtBizDocItems.TableName = "BizDocItems"
            dtBizDocItems.Columns.Add("OppItemID")
            dtBizDocItems.Columns.Add("Quantity")
            dtBizDocItems.Columns.Add("Notes")
            dtBizDocItems.Columns.Add("monPrice")

            Dim dr As DataRow
            'Dim BizCalendar As UserControl
            Dim strDeliveryDate As String

            Dim CalRentalStartDate As UserControl
            Dim CalRentalReturnDate As UserControl

            Dim RentalStartDate, RentalReturnDate As DateTime

            For Each drItem As DataRow In dtOrderedItems.Rows
                dr = dtBizDocItems.NewRow
                dr("OppItemID") = CCommon.ToLong(drItem("numoppitemtCode"))
                dr("Notes") = ""
                dr("Quantity") = CCommon.ToDouble(drItem("numUnitHour"))
                dr("monPrice") = CCommon.ToDecimal(drItem("monPrice"))
                dtBizDocItems.Rows.Add(dr)
            Next

            Dim dsNew As New DataSet
            dsNew.Tables.Add(dtBizDocItems)
            strOppBizDocItems = dsNew.GetXml
            dsNew.Tables.Remove(dsNew.Tables(0))
            Return strOppBizDocItems
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class
