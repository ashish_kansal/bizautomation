<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmItemPriceRecommd.aspx.vb"
    Inherits="BACRM.UserInterface.Opportunities.frmItemPriceRecommd" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Price Recommdation</title>
    <script language="javascript">
        function Close() {
            window.close()
            return false;
        }
        function AccessParent(a, b, c) {
            window.opener.AddPrice(a, b, c)
            window.close();
            return false;
        }

        function PriceTable(a) {
            window.open("../Items/frmPricingTable.aspx?View=1&RuleID=" + a, '', 'toolbar=no,titlebar=no,top=200,width=400,height=600,left=200,scrollbars=yes,resizable=yes')
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="button"></asp:Button>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Item Price Recommendations &amp; Quotes from History
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:DataGrid ID="dgItemPricemgmt" runat="server" Width="600px" CssClass="dg" AutoGenerateColumns="False"
        >
        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:TemplateColumn HeaderText="Date Previously Quoted">
                <ItemTemplate>
                    <%# ReturnName(DataBinder.Eval(Container.DataItem, "bintCreatedDate")) %>
                    <asp:Label ID="lblLstPrice" runat="server" Visible="false" Text='<%# DataBinder.Eval(Container.DataItem, "ListPrice") %>'>
                    </asp:Label>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:BoundColumn DataField="numUnitHour" HeaderText="Units"></asp:BoundColumn>
            <asp:BoundColumn DataField="OppStatus" HeaderText="Type of Quotation"></asp:BoundColumn>
            <asp:BoundColumn DataField="vcPOppName" HeaderText="Opportunity/Deal Name"></asp:BoundColumn>
            <asp:BoundColumn DataField="convrsPrice" HeaderText="Unit Price" DataFormatString="{0:#,##0.00}">
            </asp:BoundColumn>
            <asp:TemplateColumn>
                <ItemTemplate>
                    <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add" Width="50"></asp:Button>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
</asp:Content>
