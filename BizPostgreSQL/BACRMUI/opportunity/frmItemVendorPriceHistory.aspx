﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmItemVendorPriceHistory.aspx.vb" Inherits=".frmItemVendorPriceHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../CSS/bootstrap.min.css" rel="Stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="row">
        <div class="col-xs-12 pull-right">
            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-primary pull-right" OnClientClick="Close();" />
        </div>
        <div class="col-xs-12">
            <asp:Label ID="lblException" runat="server" Text="" ForeColor="Red"></asp:Label>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Vendor Price History
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:GridView ID="gvVendorPrice" Width="1000" UseAccessibleHeader="true" HeaderStyle-BackColor="#ebebeb" runat="server" HeaderStyle-HorizontalAlign="Center" AutoGenerateColumns="false" CssClass="table table-bordered table-striped">
        <Columns>
            <asp:BoundField DataField="vcCreatedDate" HeaderText="DateCreated" />
            <asp:BoundField DataField="vcPOppName" HeaderText="Source" />
            <asp:BoundField DataField="numUnitHour" HeaderText="Units" />
            <asp:BoundField DataField="vcUOMName" HeaderText="UOM" />
            <asp:BoundField DataField="monPrice" HeaderText="Price" />
        </Columns>
    </asp:GridView>
</asp:Content>
