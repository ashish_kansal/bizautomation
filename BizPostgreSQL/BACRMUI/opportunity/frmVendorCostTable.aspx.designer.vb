﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace BACRM.UserInterface.VendorCost

    Partial Public Class frmVendorCostTable

        '''<summary>
        '''btnUpdate control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnUpdate As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnClose control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnClose As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''gvVendorCost control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents gvVendorCost As Global.System.Web.UI.WebControls.GridView
    End Class
End Namespace
