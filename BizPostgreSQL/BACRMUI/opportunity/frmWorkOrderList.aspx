﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmWorkOrderList.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmWorkOrder2" MasterPageFile="~/common/GridMaster.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script type="text/javascript" src="../JavaScript/dateFormat.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function pageLoaded() {
            var workOrderIds = "";
            var shortDateFormat = '<%= Session("DateFormat")%> HH:mm:ss';

            $("#gvSearch tr").not(":first").each(function (index, tr) {
                workOrderIds += ((workOrderIds.length > 0 ? "," : "") + $(this).find("#lbl1").text());

                if ($(tr).find("label.lblProjectedFinish").length > 0) {
                    $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/GetProjectedFinish',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "workOrderID": parseInt($(this).find("#lbl1").text())
                        }),
                        success: function (data) {
                            var obj = $.parseJSON(data.GetProjectedFinishResult);

                            if ($(tr).find("label.lblForecastDetail").length > 0) {
                                if ($(tr).find("#hfRequiredFinishDate").val() != "") {
                                    if (Date.parse($(tr).find("#hfRequiredFinishDate").val())) {
                                        var diff = new Date(new Date($(tr).find("#hfRequiredFinishDate").val()) - new Date(obj));

                                        // get days
                                        var days = diff / 1000 / 60 / 60 / 24;
                                        var html = "";

                                        if (days > 0) {
                                            html += '<lable style="color: #00a65a;border: 2px solid #00a65a;padding: 4px;font-weight: 600;">' + parseInt(days) + (parseInt(days) > 1 ? ' Days' : ' Day') + ' Early</lable>';
                                        } else {
                                            html += '<lable style="color: #dd4b39;border: 2px solid #dd4b39;padding: 4px;font-weight: 600;">' + (parseInt(days) * -1) + ((parseInt(days) * -1) > 1 ? ' Days' : ' Day') + ' Late</lable>';
                                        }

                                        $(tr).find("label.lblForecastDetail").closest("td").html(html);
                                    } else {
                                        $(tr).find("label.lblForecastDetail").closest("td").html("<i class='fa fa-exclamation-triangle'></i>");
                                    }
                                } else {
                                    $(tr).find("label.lblForecastDetail").closest("td").html("Requested Finish not set");
                                }
                            }

                            $(tr).find("label.lblProjectedFinish").closest("td").html(formatDate(new Date(obj), shortDateFormat.replace("DD", "dd").replace("YYYY", "yyyy")));
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $(tr).find("label.lblProjectedFinish").closest("td").html("<i class='fa fa-exclamation-triangle'></i>");
                            $(tr).find("label.lblForecastDetail").closest("td").html("<i class='fa fa-exclamation-triangle'></i>");
                        }
                    });
                }

                //if ($(this).find("label.lblForecastDetail").length > 0) {
                //    $(this).find("label.lblForecastDetail").text("test");
                //}

                if ($(this).find("label.lblCapacityLoad").length > 0) {
                    $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/GetCapacityLoad',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "workOrderID": parseInt($(this).find("#lbl1").text())
                        }),
                        success: function (data) {
                            var obj = $.parseJSON(data.GetCapacityLoadResult);
                            var html = "";
                            if (parseInt(obj) > 100) {
                                html += '<lable style="background-color:#ffd1d1;border:2px solid #ffd1d1;padding:4px;font-weight:600;">';
                            } else if (parseInt(obj) > 76 && parseInt(obj) <= 100) {
                                html += '<lable style="background-color:#ffdc7d;border:2px solid #ffdc7d;padding:4px;font-weight:600;">';
                            } else if (parseInt(obj) > 51 && parseInt(obj) <= 75) {
                                html += '<lable style="background-color:#ffff00;border:2px solid #ffff00;padding:4px;font-weight:600;">';
                            } else {
                                html += '<lable style="background-color:#ccfcd9;border:2px solid #ccfcd9;padding:4px;font-weight:600;">';
                            }

                            html += parseInt(obj);
                            html += '%</label>';

                            $(tr).find("label.lblCapacityLoad").closest("td").html(html);
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $(tr).find("label.lblCapacityLoad").closest("td").html("<i class='fa fa-exclamation-triangle'></i>");
                        }
                    });
                }
            });


            $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/GetWorkOrderStatus',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "workOrderIDs": workOrderIds
                }),
                success: function (data) {
                    var obj = $.parseJSON(data.GetWorkOrderStatusResult);

                    $("#gvSearch > tbody > tr").not(":first").each(function (index, tr) {
                        var result = $.grep(obj, function (e) { return e.numWOID == $(tr).find("#lbl1").text(); });

                        if (result.length === 0) {
                            $(tr).find("label.lblWorkOrderStatus").closest("td").html("<i class='fa fa-exclamation-triangle'></i>");
                        } else {
                            $(tr).find("label.lblWorkOrderStatus").closest("td").html(result[0].vcWorkOrderStatus);
                        } 
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(tr).find("label.lblWorkOrderStatus").closest("td").html("<i class='fa fa-exclamation-triangle'></i>");
                }
            });
        }

        function DeleteRecord() {
            var RecordIDs = GetCheckedRowValues()
            document.getElementById('txtDelWoId').value = RecordIDs;
            if (RecordIDs.length > 0)
                return true
            else {
                alert('Please select atleast one record!!');
                return false
            }
        }

        function PickList() {
            var selectedIds = "";

            $("[id$=gvSearch] tr").each(function () {
                if ($(this).find("#chkSelect").is(':checked')) {
                    selectedIds += ((selectedIds.length > 0 ? "," : "") + $(this).find("#lbl1").text());
                }
            });

            if (selectedIds.length > 0) {
                var h = screen.height;
                var w = screen.width;

                window.open('../opportunity/frmWorkOrderPickList.aspx?IDs=' + selectedIds, '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            }
            else {
                alert('Please select atleast one record!!');
            }

            return false;
        }

        function PickSelected() {
            var arrSelectedRows = [];

            $("[id$=gvSearch] tr").each(function () {
                if ($(this).find("#chkSelect").is(':checked')) {
                    var objWorkOrder = {};
                    objWorkOrder["ID"] = parseInt($(this).find("#lbl1").text());
                    objWorkOrder["Name"] = $(this).find("[id$=hdnWorkOrderName]").val();
                    arrSelectedRows.push(objWorkOrder);
                }
            });

            if (arrSelectedRows.length > 0) {
                document.getElementById('txtDelWoId').value = JSON.stringify(arrSelectedRows);
                return true;
            }
            else {
                alert('Please select atleast one record!!');
                return false;
            }
        }

        function OpenSetting() {
            window.open('../Prospects/frmConfCompanyList.aspx?FormId=145', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=400,scrollbars=yes,resizable=yes')
            return false
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="pull-left">
        <asp:RadioButton ID="radOpen" GroupName="rad" AutoPostBack="true" runat="server" Text="In Process" />
        <asp:RadioButton ID="radClosed" GroupName="rad" AutoPostBack="true" runat="server" Text="Finished" />
    </div>
    <div class="pull-right">
        <div class="form-inline" style="margin-bottom: 10px;">
            <asp:Button ID="btnPick" runat="server" CssClass="btn btn-primary" Text="Pick List" OnClientClick="return PickList();" />
            <asp:Button ID="btnPickSelected" runat="server" CssClass="btn btn-primary" Text="Pick Selected" OnClientClick="return PickSelected();" OnClick="btnPickSelected_Click" />
            <asp:LinkButton ID="btnFinishWorkOrder" OnClientClick="return DeleteRecord()" CssClass="btn btn-success" runat="server"><img style="height:13px;" src="../images/FinishWorkOrder.png" />&nbsp;&nbsp;Finish Work Order</asp:LinkButton>
            <asp:LinkButton ID="btnRemove" OnClientClick="return DeleteRecord()" runat="server" CssClass="btn  btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Remove</asp:LinkButton>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litError" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblDeal" runat="server"></asp:Label>&nbsp;<a href="#" onclick="return OpenHelpPopUp('opportunity/frmworkorderlist.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        ShowPageIndexBox="Never"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>


    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvSearch" runat="server" CssClass="table table-bordered table-striped" DataKeyNames="numWOID" EnableViewState="true" AutoGenerateColumns="false" Width="100%" ShowHeaderWhenEmpty="true" OnRowDataBound="gvSearch_RowDataBound">
                    <Columns>
                    </Columns>
                    <EmptyDataTemplate>
                        No records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </div>
    </div>


    <asp:TextBox ID="txtDelWoId" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPageDeals" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecordsDeals" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
