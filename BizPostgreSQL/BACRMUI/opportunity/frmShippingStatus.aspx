﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmShippingStatus.aspx.vb" Inherits=".frmShippingStatus" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
       <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script>
        function CloseWindow() {
            this.close();
        }
    </script>
    <style>
        .table{
            border:0px !important;
        }
        .table tr{
            border:0px !important;
        }
        .table tr td{
            border:0px !important;
        }
        .table td:nth-child(2){
            font-weight:bold;
        }
        .greenStatus {
    color: #45a008;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Tracking Information
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
 <div class="pull-right"><asp:Button runat="server" ID="btnClose" Text="Close" OnClientClick="CloseWindow()" CssClass="button btn btn-primary" /></div>
    <div style="clear:both"></div>
    <div class="container" style="margin-top:10px">
        
  <table class="table table-responsive" style="border:0px;">
            <tr>
                <td>STATUS</td>
                <td>
                    <asp:label runat="server" ID="lblStatus" CssClass="greenStatus" text="Delivered"></asp:label>
                </td>
            </tr>
            <tr>
                <td>LOCATION</td>
                <td>
                    <asp:label runat="server" ID="lblLocation" CssClass="greenStatus" text="Delivered"></asp:label>
                </td>
            </tr>
            <tr>
                <td>SHIP CARRIER</td>
                <td>
                    <asp:label runat="server" ID="lblShipCarrier" text="Delivered"></asp:label>
                </td>
            </tr>
            <tr>
                <td>TRACKING ID</td>
                <td>
                    <asp:label runat="server" ID="lblTrackingId" text="Delivered"></asp:label>
                </td>
            </tr>
            <tr>
                <td>LATEST EVENT</td>
                <td>
                    <asp:label runat="server" ID="lblLatestEvent" CssClass="greenStatus" text="Delivered"></asp:label>
                </td>
            </tr>
            <tr>
                <td>SHIPMENT UPDATES VIA TEXT</td>
                <td>
                    <asp:label runat="server" ID="lblShipmentText" text="Delivered"></asp:label>
                </td>
            </tr>
            
        </table>
</div>
</asp:Content>
