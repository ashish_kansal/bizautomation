'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports System.Text.RegularExpressions
Imports System.Web.Script.Serialization
Imports ConsumedByCode.SignatureToImage
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Workflow
Namespace BACRM.UserInterface.Opportunities
    Public Class frmCloneBizDoc
        Inherits BACRMPage

        Dim lngOppBizDocID, lngOppID, lngDivID, lngBizDocID As Long
        Dim OppType As Integer
        Dim lintAuthorizativeBizDocsId As Long
        Dim objOppBizDocs As New OppBizDocs

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                lngDivID = CCommon.ToLong(GetQueryStringVal("DivId"))
                lngOppBizDocID = CCommon.ToLong(GetQueryStringVal("OppBizDocId"))
                lngOppID = CCommon.ToLong(GetQueryStringVal("OppId"))
                lngBizDocID = CCommon.ToLong(GetQueryStringVal("BizDocType"))
                OppType = CCommon.ToInteger(GetQueryStringVal("OppType"))

                GetUserRightsForPage(35, 112)

                If Not IsPostBack Then
                    BindBizdocs()
                    'btnClone.Attributes.Add("onclick", "javascript:opener.location.reload(true);window.close();return false;")

                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub BindBizdocs()
            Try

                Dim objCommon As New CCommon
                Dim dtMasterItem As DataTable
                dtMasterItem = objCommon.GetMasterListItemsForBizDocs(27, Session("DomainID"), lngOppID)

                If lngBizDocID = enmBizDocTemplate_BizDocID.Invoice_1 Then
                    If dtMasterItem.Select("numListItemID = " & lngBizDocID) IsNot Nothing AndAlso dtMasterItem.Select("numListItemID = " & lngBizDocID).Length > 0 Then
                        dtMasterItem.Rows.Remove(dtMasterItem.Select("numListItemID = " & lngBizDocID)(0))
                    End If
                End If

                'If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                'objOppBizDocs.DomainID = Session("DomainID")
                'objOppBizDocs.OppId = lngOppID
                'lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()
                'If dtMasterItem.Select("numListItemID = " & lintAuthorizativeBizDocsId) IsNot Nothing AndAlso dtMasterItem.Select("numListItemID = " & lintAuthorizativeBizDocsId).Length > 0 Then
                '    dtMasterItem.Rows.Remove(dtMasterItem.Select("numListItemID = " & lintAuthorizativeBizDocsId)(0))
                'End If

                ddlBizDocs.DataSource = dtMasterItem
                ddlBizDocs.DataTextField = "vcData"
                ddlBizDocs.DataValueField = "numListItemID"
                ddlBizDocs.DataBind()
                ddlBizDocs.Items.Insert(0, New ListItem("--Select One--", "0"))

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnClone_Click(sender As Object, e As EventArgs) Handles btnClone.Click
            Try

                Dim strResult As String = CloneBizDoc()
                If String.IsNullOrEmpty(strResult) Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "ReturnMessage", "ReturnAndClose();", True)

                Else
                    Session("CloneResult") = strResult
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "ReturnMessage", "ReturnAndClose();", True)

                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Function CloneBizDoc() As String
            Dim strResult As String = ""
            Try
                If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                Dim OppBizDocID, lngDivId, JournalId As Long
                Dim lngCntID As Long = 0
                objOppBizDocs.OppId = lngOppID
                objOppBizDocs.OppType = OppType

                objOppBizDocs.UserCntID = Session("UserContactID")
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.vcPONo = "" 'txtPO.Text
                objOppBizDocs.vcComments = "" 'txtComments.Text
                'objOppBizDocs.ShipCost = 0 'IIf(IsNumeric(txtShipCost.Text), txtShipCost.Text, 0)
                'Add Billing Term from selected SalesTemplate(Sales Order)'s first Autoritative bizdoc's Billing term

                Dim dtDetails, dtBillingTerms As DataTable
                Dim objPageLayout As New CPageLayout
                objPageLayout.OpportunityId = lngOppID
                objPageLayout.DomainID = Session("DomainID")
                dtDetails = objPageLayout.OpportunityDetails.Tables(0)
                lngDivId = dtDetails.Rows(0).Item("numDivisionID")
                lngCntID = dtDetails.Rows(0).Item("numContactID")
                Dim objAdmin As New CAdmin
                objAdmin.DivisionID = lngDivId

                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.OppId = lngOppID
                lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()

                Dim objJournal As New JournalEntry
                If lintAuthorizativeBizDocsId = ddlBizDocs.SelectedItem.Value Then 'save bizdoc only if selected company's AR and AP account is mapped
                    If objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), lngDivId) = 0 Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip"" To Save BizDoc' );", True)
                        Exit Function
                    End If
                End If

                If OppType = 2 Then
                    'Accounting validation->Do not allow to save PO untill Default COGs account is mapped
                    If ChartOfAccounting.GetDefaultAccount("CG", Session("DomainID")) = 0 Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default COGs account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                        Exit Function
                    End If
                End If

                If OppType = 2 Then
                    If ChartOfAccounting.GetDefaultAccount("PC", Session("DomainID")) = 0 Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Purchase Clearing account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                        Exit Function
                    End If

                    If ChartOfAccounting.GetDefaultAccount("PV", Session("DomainID")) = 0 Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Purchase Price Variance account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                        Exit Function
                    End If
                End If

                objOppBizDocs.FromDate = DateTime.UtcNow
                objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                objOppBizDocs.BizDocId = ddlBizDocs.SelectedValue
                objOppBizDocs.BizDocTemplateID = ddlBizDocs.SelectedValue

                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")
                objCommon.Mode = 33
                objCommon.Str = ddlBizDocs.SelectedValue
                objOppBizDocs.SequenceId = objCommon.GetSingleFieldValue()

                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")
                objCommon.Mode = 34
                objCommon.Str = lngOppID
                objOppBizDocs.RefOrderNo = objCommon.GetSingleFieldValue()

                objOppBizDocs.bitPartialShipment = True

                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    OppBizDocID = objOppBizDocs.SaveBizDoc()

                    If OppBizDocID = 0 Then
                        strResult = "A BizDoc by the same name is already created. Please select another BizDoc !"
                        Return strResult
                        'Exit Function
                    End If

                    'If OppBizDocID > 0 Then
                    '    GetWorkFlowAutomation(OppBizDocID, lngCntID, lngDivId)

                    'End If

                    'Create Journals only for authoritative bizdocs
                    '-------------------------------------------------
                    If lintAuthorizativeBizDocsId = ddlBizDocs.SelectedValue Or ddlBizDocs.SelectedValue = 304 Then
                        Dim ds As New DataSet
                        Dim dtOppBiDocItems As DataTable

                        objOppBizDocs.OppBizDocId = OppBizDocID
                        ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                        dtOppBiDocItems = ds.Tables(0)

                        Dim objCalculateDealAmount As New CalculateDealAmount

                        objCalculateDealAmount.CalculateDealAmount(lngOppID, OppBizDocID, OppType, Session("DomainID"), dtOppBiDocItems)

                        ''---------------------------------------------------------------------------------
                        JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, objOppBizDocs.FromDate, Description:=ds.Tables(1).Rows(0).Item("vcBizDocID"))
                        Dim objJournalEntries As New JournalEntry

                        If OppType = 2 Then
                            If CCommon.ToBool(ds.Tables(1).Rows(0).Item("bitPPVariance")) Then
                                objJournalEntries.SaveJournalEntriesPurchaseVariance(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), ds.Tables(1).Rows(0).Item("fltExchangeRateBizDoc"), vcBaseCurrency:=ds.Tables(1).Rows(0).Item("vcBaseCurrency"), vcForeignCurrency:=ds.Tables(1).Rows(0).Item("vcForeignCurrency"))
                            Else
                                objJournalEntries.SaveJournalEntriesPurchase(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"))
                            End If
                        ElseIf OppType = 1 Then
                            If ddlBizDocs.SelectedValue = 304 AndAlso CCommon.ToBool(Session("IsEnableDeferredIncome")) Then 'Deferred Revenue
                                objJournalEntries.SaveJournalEntriesDeferredIncome(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), objCalculateDealAmount.GrandTotal)
                            Else
                                If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                                    objJournalEntries.SaveJournalEntriesSales(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), 0)
                                Else
                                    objJournalEntries.SaveJournalEntriesSalesNew(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), 0)
                                End If

                                If Session("AutolinkUnappliedPayment") Then
                                    Dim objSalesFulfillment As New SalesFulfillmentWorkflow
                                    objSalesFulfillment.DomainID = Session("DomainID")
                                    objSalesFulfillment.UserCntID = Session("UserContactID")
                                    objSalesFulfillment.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                                    objSalesFulfillment.OppId = lngOppID
                                    objSalesFulfillment.OppBizDocId = OppBizDocID

                                    objSalesFulfillment.AutoLinkUnappliedPayment()
                                End If
                            End If
                        End If
                    ElseIf ddlBizDocs.SelectedValue = 296 Then 'Fulfillment BizDoc
                        'AUTO FULFILLS bizdoc AND MAKED QTY AS SHIPPED AND RELEASES IT FROM ALLOCATION
                        Dim objOppBizDoc As New OppBizDocs
                        objOppBizDoc.DomainID = CCommon.ToLong(Session("DomainID"))
                        objOppBizDoc.UserCntID = CCommon.ToLong(Session("UserContactID"))
                        objOppBizDoc.OppId = lngOppID
                        objOppBizDoc.OppBizDocId = OppBizDocID
                        objOppBizDoc.AutoFulfillBizDoc()


                        'IMPORTANT: WE ARE MAKING ACCOUNTING CHANGES BECAUSE WE ARE CREATING FULFILLMENT ORDER IN BACKGROUP AND MARKIGN IT AS SHIPPED
                        '           OTHERWISE ACCOUNTING IS ONLY IMPACTED WHEN USER SHIPPED IT FROM SALES FULFILLMENT PAGE
                        'CREATE SALES CLEARING ENTERIES
                        'CREATE JOURNALS ONLY FOR FILFILLMENT BIZDOCS
                        '-------------------------------------------------
                        Dim ds As New DataSet
                        Dim dtOppBiDocItems As DataTable

                        objOppBizDocs.OppBizDocId = OppBizDocID
                        ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                        dtOppBiDocItems = ds.Tables(0)

                        Dim objCalculateDealAmount As New CalculateDealAmount
                        objCalculateDealAmount.CalculateDealAmount(lngOppID, OppBizDocID, 1, CCommon.ToLong(Session("DomainID")), dtOppBiDocItems)

                        'WHEN FROM COA SOMEONE DELETED JOURNALS THEN WE NEED TO CREATE NEW JOURNAL HEADER ENTRY
                        JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, Entry_Date:=Date.UtcNow.Date, Description:=CStr(ds.Tables(1).Rows(0).Item("vcBizDocID")))

                        Dim objJournalEntries As New BACRM.BusinessLogic.Accounting.JournalEntry
                        If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                            objJournalEntries.SaveJournalEnteriesSalesClearing(lngOppID, CCommon.ToLong(Session("DomainID")), dtOppBiDocItems, JournalId, ViewState("DivisionID"), OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                        Else
                            objJournalEntries.SaveJournalEnteriesSalesClearingNew(lngOppID, CCommon.ToLong(Session("DomainID")), dtOppBiDocItems, JournalId, ViewState("DivisionID"), OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                        End If
                        '---------------------------------------------------------------------------------
                    End If

                    objTransactionScope.Complete()
                End Using

                ''Added By Sachin Sadhu||Date:1stMay2014
                ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                ''          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = OppBizDocID
                objWfA.SaveWFBizDocQueue()
                'end of code

                '------ Send BizDoc Alerts - Notify record owners, their supervisors, and your trading partners when a BizDoc is created, modified, or approved.
                'Dim objAlert As New CAlerts
                'objAlert.SendBizDocAlerts(lngOppID, OppBizDocID, ddlBizDocs.SelectedValue, Session("DomainID"), CAlerts.enmBizDoc.IsCreated)

                Dim objAutomatonRule As New AutomatonRule
                objAutomatonRule.ExecuteAutomationRule(49, OppBizDocID, 1)

            Catch ex As Exception
                If ex.Message = "NOT_ALLOWED" Then
                    strResult = "To split order items multiple bizdocs you must create all bizdocs with ""partial fulfilment"" checked!"

                ElseIf ex.Message = "NOT_ALLOWED_FulfillmentOrder" Then
                    strResult = "Only a Sales Order can create a Fulfillment Order"

                ElseIf ex.Message = "FY_CLOSED" Then
                    strResult = "This transaction can not be posted,Because transactions date belongs to closed financial year."

                ElseIf ex.Message = "AlreadyInvoice" Then
                    strResult = "You can�t create a Invoice against a Fulfillment Order that already contains a Invoice"

                ElseIf ex.Message = "AlreadyPackingSlip" Then
                    strResult = "You can�t create a Packing-Slip against a Fulfillment Order that already contains a Packing-Slip"

                ElseIf ex.Message = "AlreadyInvoice_NOT_ALLOWED_FulfillmentOrder" Then
                    strResult = "You can�t create a fulfillment order against a sales order that already contains an Invoice or Packing-Slip"

                ElseIf ex.Message = "AlreadyFulfillmentOrder" Then
                    strResult = "Manually adding Invoices or Packing Slips to a Sales Order is not allowed after a Fulfillment Order (FO) is added. Your options are to edit the FO status to trigger creation of Invoices and/or Packing Slips, Delete the Fulfillment Order, or Create Invoices and/or Packing Slips from the Sales Fulfillment section"

                Else
                    Throw ex
                End If
            End Try

            Return strResult
        End Function
    End Class

End Namespace
