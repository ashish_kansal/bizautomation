<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSelectSrlItemFrmInv.aspx.vb" Inherits=".frmSelectSrlItemFrmInv" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
     <br />
    <table width="100%">
        <tr>
            <td align="right" >
                <asp:Button  ID="btnSaveClose" runat="server" Text="Save & Close" CssClass="button"/>
                <asp:Button  ID="btnClose" runat="server" Text="Close" CssClass="button"/>
            </td>
        </tr>
         <tr>
            <td>
            <br />
            <br />
               <igtbl:ultrawebgrid id="uwItemSel" Width="100%" DisplayLayout-AllowRowNumberingDefault="ByDataIsland"  runat="server" Browser="Xml"   Height="100%">
					                                    <DisplayLayout AutoGenerateColumns="true"  RowHeightDefault="18"  AllowAddNewDefault="Yes" Version="3.00" SelectTypeRowDefault="Single"
					                                    ViewType="Hierarchical" TableLayout="Auto" SelectTypeCellDefault="Extended" BorderCollapseDefault="Separate" AllowColSizingDefault="Free" 
					                                    Name="uwItemSel" EnableClientSideRenumbering="true" SelectTypeColDefault="Extended" AllowUpdateDefault="Yes">
					                                     <HeaderStyleDefault VerticalAlign="Middle" Font-Size="8pt" Font-Names="Arial" BorderStyle="Solid" HorizontalAlign="Left" ForeColor="white" BackColor="#52658C">
                                                            <Padding Left="2px" Right="2px"></Padding>
                                                            <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                        </HeaderStyleDefault>
                                                        <RowSelectorStyleDefault BackColor="White"></RowSelectorStyleDefault>
                                                        <FrameStyle Width="100%" Cursor="Default" BorderWidth="3px" Font-Size="8pt" Font-Names="Arial" BorderStyle="Double"></FrameStyle>
                                                        <FooterStyleDefault BorderWidth="1px" BorderStyle="Solid" BackColor="LightGray">
                                                            <BorderDetails ColorTop="White" WidthLeft="1px" WidthTop="1px" ColorLeft="White"></BorderDetails>
                                                        </FooterStyleDefault>
                                                        <EditCellStyleDefault BorderWidth="0px" BorderStyle="None"></EditCellStyleDefault>
                                                        <SelectedRowStyleDefault ForeColor="White" BackColor="#666666"></SelectedRowStyleDefault>
                                                        <RowStyleDefault BorderWidth="1px" Font-Size="8pt" Font-Names="Arial" BorderColor="Gray" BorderStyle="Solid" BackColor="White">
                                                            <Padding Left="5px" Right="5px"></Padding>
                                                            <BorderDetails WidthLeft="0px" WidthTop="0px"></BorderDetails>
                                                        </RowStyleDefault>
                                                        <RowExpAreaStyleDefault BackColor="LightSteelBlue"></RowExpAreaStyleDefault>
				                                    </DisplayLayout>
											                    <Bands>
												                    <igtbl:UltraGridBand  AllowDelete="No" AllowAdd="Yes" AllowUpdate="No"    AddButtonCaption="Serialized Items" BaseTableName="SerializedItems" Key="SerializedItems">
													                    <Columns>
												                            <igtbl:UltraGridColumn AllowUpdate="Yes"  Type="CheckBox" Key="Select">
												                            </igtbl:UltraGridColumn>
													                    </Columns>
												                    </igtbl:UltraGridBand>
											                    </Bands>
										                    </igtbl:ultrawebgrid>
          </td> 
        </tr> 
     </table> 
  
    </form>
</body>
</html>
