<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmDealList.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmDealList"
    MasterPageFile="~/common/GridMaster.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>
        <asp:Literal ID="litTitle" runat="server"></asp:Literal>
    </title>
    <link href="../CSS/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="//ajax.aspnetcdn.com/ajax/4.0/1/MicrosoftAjax.js"></script>
    <script type="text/javascript">

        function OpenContact(a, b) {

            //var ContactType = document.getElementById('ddlContactType').value;
            var str;

            str = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactlist&ft6ty=oiuy&CntId=" + a + '&ContactType=1';


            document.location.href = str;

        }
        function BindPartnerContact(domain, oppid, dropId) {
            //var dropId = $(this).attr("id");
            console.log(dropId);
            var DivisionID = 0;
            $('select[attr="numPartner"]').each(function () {

                DivisionID = $("option:selected", this).val();
            });
            oppid = 0;
            $.ajax({
                type: "POST",
                url: '../opportunity/frmOpportunities.aspx/bindContact',
                data: JSON.stringify({ "domain": domain, "oppid": oppid, "DivisionID": DivisionID }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success:
                function (result) {
                    var i = 0;
                    $('select[attr="numPartenerContact"]').each(function () {
                        if (i == 0) {
                            $(this).empty();
                            $(this).append("<option value='0'>-- Select One --</option>");
                            var id = $(this);
                            $.each(JSON.parse(result.d), function (idx, obj) {
                                id.append("<option value='" + obj.numContactId + "'>" + obj.vcGivenName + "</option>");
                            });
                        }
                        i = 1;
                    });

                },
                error:
                function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log("Error");
                },

            });
        }
        function OpenWindow(a, b, c) {
            var str;
            if (b == 0) {

                str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&DivID=" + a;

            }
            else if (b == 1) {

                str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&DivID=" + a;

            }
            else if (b == 2) {

                str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&klds+7kldf=fjk-las&DivId=" + a;

            }

            document.location.href = str;

        }
        function openShippingLabel(a, b, c, d) {
            //var index = document.getElementById('ddlShipCompany').selectedIndex;
            if (c > 0) {
                window.open('../opportunity/frmShippingReport.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppBizDocId=' + b + '&ShipReportId=' + c + '&OppId=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=50,width=1024,height=800,scrollbars=yes,resizable=yes');
                return false;
            }
            else {
                window.open('../opportunity/frmShippingBox.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=' + a + '&OppBizDocId=' + b + '&ShipCompID=' + d, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=1024,height=600,scrollbars=yes,resizable=yes');
                return false;
            }
        }
        function openReportList(a, b, c) {
            var h = screen.height;
            var w = screen.width;

            if (c > 0) {
                window.open('../opportunity/frmShippingReport.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppBizDocId=' + b + '&ShipReportId=' + c + '&OppId=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=50,top=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
                return false;
            }
            else {
                window.open('../opportunity/frmShippingReportList.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=' + a + '&OppBizDocId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=980,height=250,scrollbars=yes,resizable=yes');
                return false;
            }
        }
        function OpenPartner(a, b, c) {
            var str;
            if (b == 0) {

                str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylistDivID=" + a;

            }
            else if (b == 1) {

                str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylist&DivID=" + a;

            }
            else if (b == 2) {

                str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylist&klds+7kldf=fjk-las&DivId=" + a;

            }

            document.location.href = str;

        }

        function OpenItemListForInvoice(a) {
            $(".imgUnItem").attr("alt", "");
            $(".imgUnItem").attr("title", "");
            $.ajax({
                type: "POST",
                url: window.location.pathname + "/FillInvoiceItem",
                data: JSON.stringify({ "OppId": a }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                error:
                function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log("Error");
                },
                success: function (result) {
                    $(".imgUnItem").attr("alt", "The following items are not yet invoiced : " + result.d);
                    $(".imgUnItem").attr("title", "The following items are not yet invoiced : " + result.d);
                }
            });
        }

        //function OpenSentEmailForInvoice(numOppId, numDivisionID, vcEmail) {
        /*function OpenSentEmailForInvoice(numOppId) {
            debugger;
            $(".imgUnItem").attr("alt", "");
            $(".imgUnItem").attr("title", "");
            $.ajax({
                type: "POST",
                url: window.location.pathname + "/CreateHTMLBizDocs",
                data: JSON.stringify({ "Oppid": numOppId}),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                error:
                function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log("Error");
                },
                success: function (result) {
                    $(".imgUnItem").attr("alt", "Email : " + result.d);
                    $(".imgUnItem").attr("title", "Email : " + result.d);
                }
            });
        }*/


        function OpenItemListForPendingApproval(a) {
            $(".imgAUnItem").attr("alt", "");
            $(".imgAUnItem").attr("title", "");
            $.ajax({
                type: "POST",
                url: window.location.pathname + "/FillPendingApproval",
                data: JSON.stringify({ "OppId": '<%#Session("DomainID")%>' }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                error:
                function (XMLHttpRequest, textStatus, errorThrown) {
                    console.log("Error");
                },
                success: function (result) {
                    $(".imgAUnItem").attr("alt", "Pending Price Approval from : " + result.d);
                    $(".imgAUnItem").attr("title", "Pending Price Approval from : " + result.d);
                }
            });
        }

        function OpenContact(a, b) {

            var str;

            str = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&ft6ty=oiuy&CntId=" + a;


            document.location.href = str;

        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function SelWarOpenDeals(a) {
            window.open("../opportunity/frmSelWhouseOpenDls.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Opid=" + a)
            return false;
        }
        function OpenSetting() {
            window.open('../Opportunity/frmConfOppList.aspx?FormId=' + document.getElementById("txtFormID").value, '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=400,scrollbars=yes,resizable=yes')
            return false
        }
        function OpenOpp(a, b) {

            var str;

            str = "../opportunity/frmOpportunities.aspx?frm=deallist&OpID=" + a;

            document.location.href = str;
        }
        //var prg_width = 60;
        function progress(ProgInPercent, Container, progress, prg_width) {
            //Set Total Width
            var OuterDiv = document.getElementById(Container);
            OuterDiv.style.width = prg_width + 'px';
            OuterDiv.style.height = '5px';

            if (ProgInPercent > 100) {
                ProgInPercent = 100;
            }
            //Set Progress Percentage
            var node = document.getElementById(progress);
            node.style.width = parseInt(ProgInPercent) * prg_width / 100 + 'px';
        }

        function DeleteRecord() {
            var RecordIDs = GetCheckedRowValues()
            document.getElementById('txtDelOppId').value = RecordIDs;
            if (RecordIDs.length > 0)
                return true
            else {
                alert('Please select atleast one record!!');
                return false
            }
        }

        function OpenBizInvoice(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
        }

        function OpenMirrorBizDoc(a, b) {
            window.open('../opportunity/frmMirrorBizDoc.aspx?RefID=' + a + '&RefType=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=1000,height=600,scrollbars=yes,resizable=yes');
        }

        function OpenEmailSent(a, b) {
            window.open('../opportunity/frmMirrorBizDoc.aspx?RefID=' + a + '&RefType=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=1000,height=600,scrollbars=yes,resizable=yes');
        }

        function GetPOAvailableForMerge() {

        }

        function OpenPOMergeWindow() {
            window.open('../opportunity/frmMergePO.aspx', '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }

        function OrderSearchClientSelectedIndexChanged(sender, eventArgs) {
            $("[id$=btnGotoRecord]").click();
        }
        function OpenHelp() {
            window.open('../Help/Initial_Grid_Opportunities_Orders_.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        function OpenCreateDopshipPOWindow(oppID) {
            var h = screen.height;
            var w = screen.width;

            window.open('../opportunity/frmCreateDropshipPO.aspx?numOppID=' + oppID, '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenCreateBackOrderPOWindow(oppID) {
            var h = screen.height;
            var w = screen.width;

            window.open('../opportunity/frmCreateBackOrderPO.aspx?numOppID=' + oppID, '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }

        /*Function added by Neelam on 10/07/2017 - Added functionality to open Child Record window in modal pop up*/
        function openChild(event, oppID) {
            $find("<%= RadWinChildRecords.ClientID%>").show();
            document.getElementById("<%= hdnBindChild.ClientID %>").value = oppID;
            $('#' + '<%= btnBindChild.ClientID %>').trigger("click");
            event.preventDefault();
        }

        $(document).ready(function () {
            if ($("[id$=hdnDealType]").val() == "2") {
                try {
                    var request = $.ajax({
                        type: "POST",
                        url: "../common/Common.asmx/GetPOAvailableForMerge",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            if (response.d == "1") {
                                $("#trPoMerge").show();

                                setTimeout(function () {
                                    $("#trPoMerge").hide();
                                }, 5000);
                            } else {
                                $("#trPoMerge").hide();
                            }
                        },
                        failure: function (response) {
                            alert("fail");
                        },
                        error: function (XMLHttpRequest, textStatus, errorThrown) {
                            alert("fail");
                        }
                    });

                } catch (ex) {

                }
            }
        });
    </script>
    <style type="text/css">

        .ProgressBar {
            height: 5px;
            width: 0px;
            background-color: Green;
        }
        .tableGrid{
            width:100%;
        }
        .tableGrid tr td{
            /*padding:5px !important;
            padding-bottom:8px !important;*/
        }
        .ProgressContainer {
            border: 1px solid black;
            height: 5px;
            font-size: 1px;
            background-color: White;
            line-height: 0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
           <%-- <div class="pull-left">
                <div class="form-inline">
                    <label>
                        View</label>
                    <asp:DropDownList ID="ddlFilterBy" runat="server" CssClass="form-control" AutoPostBack="True" Style="max-width: 150px">
                        <asp:ListItem Value="0">--Select One--</asp:ListItem>
                        <asp:ListItem Value="1">Partially Fulfilled Orders</asp:ListItem>
                        <asp:ListItem Value="2">Fulfilled Orders</asp:ListItem>
                        <asp:ListItem Value="3">Orders without Authoritative-BizDocs</asp:ListItem>
                        <asp:ListItem Value="4">Orders with items yet to be added to Invoices</asp:ListItem>
                        <asp:ListItem Value="5">Sort Alphabetically</asp:ListItem>
                        <asp:ListItem Value="6">Orders with deferred Income/Invoices</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>--%>
            <div class="pull-right">
                <div class="form-inline">
                    <asp:LinkButton runat="server" ID="btnExport" Text="Export"  Visible="False"><img alt="" src="../images/Excel.png" width="30px" height="30px"/></asp:LinkButton>
                    <asp:RadioButton ID="radOpen" GroupName="rad" AutoPostBack="true"
                        runat="server" Text="Open" />
                    <asp:RadioButton ID="radClosed" GroupName="rad" AutoPostBack="true" runat="server"
                        Text="Closed" />
                    <asp:RadioButton ID="radAll" GroupName="rad" AutoPostBack="true" runat="server"
                        Text="All" />
                    &nbsp;&nbsp;
                    <asp:DropDownList Visible="false" ID="ddlSortDeals" runat="server" Style="max-width: 100px" CssClass="form-control" AutoPostBack="True">
                        <asp:ListItem Value="1">Sales Deals</asp:ListItem>
                        <asp:ListItem Value="2">Purchase Deals</asp:ListItem>
                    </asp:DropDownList>

                    <telerik:RadComboBox ID="radCmbSearch" runat="server" DropDownWidth="600px" Width="200"
                        Skin="Default" AllowCustomText="false" ShowMoreResultsBox="true" EnableLoadOnDemand="true"
                        EnableScreenBoundaryDetection="true" ItemsPerRequest="10" EnableVirtualScrolling="true"
                        OnItemsRequested="radCmbSearch_ItemsRequested" HighlightTemplatedItems="true" ClientIDMode="Static"
                        TabIndex="503" MaxHeight="230" OnClientSelectedIndexChanged="OrderSearchClientSelectedIndexChanged" Style="display: none">
                    </telerik:RadComboBox>
                    <asp:LinkButton ID="btnSendAnnounceMent" runat="server" CssClass="btn btn-primary" Visible="false"><i class="fa fa-envelope"></i>&nbsp;&nbsp;Send Announcement</asp:LinkButton>
                    <asp:Button ID="btnGo" CssClass="btn btn-primary" runat="server" Text="Go" Style="display: none"></asp:Button>
                    <button id="btnAddNewOrder" class="btn btn-primary" runat="server" ><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New</button>
                    <asp:LinkButton ID="btnDelete" OnClientClick="return DeleteRecord()" CssClass="btn btn-danger" runat="server"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
                    <asp:Button ID="btnGotoRecord" runat="server" Style="display: none;" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%" border="0">
        <tr id="trPoMerge" style="display: none;">
            <td colspan="2" style="text-align: center;">
                <label style="color: red; font-size: 14px; vertical-align: middle">BizAutomation has detected multiple open POs for the same vendor.</label>&nbsp;<asp:Button ID="btnMerge" runat="server" Text="Click Here" CssClass="btn btn-primary" Style="padding-left: 5px !important;" OnClientClick="return OpenPOMergeWindow();" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblDeal" runat="server"></asp:Label>&nbsp;<a href="#" id="aHelp" runat="server"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        ShowPageIndexBox="Never"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <%--Added by Neelam on 10/07/2017 - Added RadWindow to open Child Record window in modal pop up--%>
    <telerik:RadWindow RenderMode="Lightweight" runat="server" ID="RadWinChildRecords" Title="Child Records" RestrictionZoneID="ContentTemplateZone" Modal="true" Behaviors="Resize,Close,Move" Width="800" Height="400">
        <ContentTemplate>
            <asp:UpdatePanel ID="Updatepanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <asp:GridView ID="gvChildOpps" runat="server" AutoGenerateColumns="False" CssClass="table table-bordered table-striped" Width="100%" ShowHeaderWhenEmpty="true" UseAccessibleHeader="true">
                            <Columns>
                                <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:Image ID="imgType" runat="Server" Width="20px" Height="20px" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Record ID" SortExpression="vcPOppName" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <a href="javascript:OpenOpp('<%# Eval("numOppId") %>')">
                                            <%# Eval("vcPOppName") %>
                                        </a>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Record Status" DataField="vcStatus" ReadOnly="true" HeaderStyle-HorizontalAlign="Center" />
                                <asp:BoundField DataField="RecordOrganization" HeaderText="Record Organization" HeaderStyle-HorizontalAlign="Center"></asp:BoundField>
                                <asp:BoundField DataField="RecordContact" HeaderText="Record Contact" HeaderStyle-HorizontalAlign="Center"></asp:BoundField>
                                <asp:BoundField DataField="Comments" HeaderText="Comments" HeaderStyle-HorizontalAlign="Center"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </div>
                    <asp:Button ID="btnBindChild" runat="server" Text="Button" Style="visibility: hidden;" />
                </ContentTemplate>

            </asp:UpdatePanel>
        </ContentTemplate>
    </telerik:RadWindow>

    <asp:HiddenField runat="server" ID="hdnBindChild" />

    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvSearch" runat="server" CssClass="table table-bordered table-striped" DataKeyNames="numOppID" EnableViewState="true" AutoGenerateColumns="false" Width="100%" ShowHeaderWhenEmpty="true">
                    <Columns>
                    </Columns>
                    <EmptyDataTemplate>
                        No records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </div>
    </div>

    <asp:TextBox ID="txtDelOppId" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPageDeals" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecordsDeals" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtFormID" runat="server" Style="display: none"></asp:TextBox>
    <asp:HiddenField ID="hdnDealType" runat="server" />
</asp:Content>
