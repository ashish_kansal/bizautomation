<%@ Page Language="vb" AutoEventWireup="false" AspCompat="true" CodeBehind="frmBizDocAddAttchmnts.aspx.vb"
    Inherits=".frmBizDocAddAttchmnts" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>BizDoc Attachments</title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <script language="javascript">
        function AddAttachmnts() {
            if (document.getElementById("txtDocName").value == '') {
                alert("Enter Document Name")
                document.getElementById("txtDocName").focus();
                return false;
            }
            if (document.getElementById("ddlBizDoc").value == 0) {
                alert("Select BizDoc")
                document.getElementById("ddlBizDoc").focus();
                return false;
            }
            if (document.getElementById("txtMagFile").value == '') {
                alert("Browse File")
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save &amp; Close">
            </asp:Button>
            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button><br>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    BizDoc Attachments
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="600px">
        <tr>
            <td class="normal1" align="right">
                Document Name
            </td>
            <td>
                <asp:TextBox ID="txtDocName" runat="server" CssClass="signup"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                BizDoc
            </td>
            <td>
                <asp:DropDownList ID="ddlBizDoc" runat="server" CssClass="signup" AutoPostBack="true">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Template
            </td>
            <td>
                <asp:DropDownList ID="ddlBizDocTemplate" runat="server" CssClass="signup">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Select File
            </td>
            <td>
                <input id="txtMagFile" type="file" name="txtMagFile" runat="server" style="width: 200px">
            </td>
        </tr>
        <%--<tr>
					<td class="normal1" align="right">Covert to PDF
					</td>
					<td><asp:CheckBox ID="chkConvert" runat="server"  /></td>
				</tr>--%>
    </table>
    <asp:TextBox ID="txtURL" Style="display: none" runat="server" CssClass="signup"></asp:TextBox>
</asp:Content>
