﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmWorkOrderPickList.aspx.vb" Inherits=".frmWorkOrderPickList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="../JavaScript/jquery.twbsPagination.min.js"></script>
    <script type="text/javascript">
        var recordsPerPage = 10;
        var updatedRecords = [];

        $(document).ajaxStart(function () {
            $("#divLoader").show();
        }).ajaxStop(function () {
            $("#divLoader").hide();
        });

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function pageLoaded() {
            if ($("[id$=hdnSelectedRecords]").val() != "") {
                BindData();
            } else {
                $("#tblBOM tbody").append("<tr><td colspan='9'>No records</td></tr>");
            }

            $("#ms-pagination").pagination({
                items: 0,
                itemsOnPage: recordsPerPage,
                displayedPages: 3,
                hrefTextPrefix: "#"
            });

            $("#txtSearchItem").on('input propertychange paste', function () {
                var searchText = $(this).val().trim();

                $("#tblBOM > tbody > tr").each(function (index, tr) {
                    if ($(tr).find("td.itemName").text().indexOf(searchText) > 0 || searchText === "") {
                        $(tr).show();
                    } else {
                        $(tr).hide();
                    }
                });
            });
        }

        function BindData() {
            try {
                $("#tblBOM > tbody > tr").remove();   

                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/GetWorkOrderBOMForPick',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "workOrderIDs": $("[id$=hdnSelectedRecords]").val()
                    }),
                    success: function (data) {
                        try {
                            var obj = $.parseJSON(data.GetWorkOrderBOMForPickResult);

                            if (obj != null && obj.length > 0) {
                                $.each(obj, function (i, n) {
                                    var html = "<tr id='" + n.numWODetailId + "'>";
                                    html += "<td><ul class='list-inline'><li>" + replaceNull(n.vcWorkOrderName) + "</li>";
                                    var objPickError = $.grep(updatedRecords, function (obj) {
                                        return obj.numWODetailID === (n.numWODetailId || 0) && !obj.bitSuccess;
                                    });
                                    if (objPickError.length > 0) {
                                        html += "<li><i class='fa fa-exclamation-triangle ship-orders' style='color:red' data-toggle='tooltip' title='" + replaceNull(objPickError[0].vcErrorMessage) + "' aria-hidden='true'></i></li>";
                                    }
                                    html +="</ul></td>";
                                    html += "<td>" + replaceNull(n.vcAssembly) + "</td>";
                                    html += "<td>" + replaceNull(n.vcRequestedDate) + "</td>";
                                    html += "<td class='itemName'>" + replaceNull(n.vcBOM) + "</td>";
                                    html += "<td>" + replaceNull(n.vcWarehouse) + "</td>";
                                    html += "<td>" + (n.numAllocation || 0) + "</td>";
                                    html += "<td style='white-space:nowrap; min-width:200px'>";
                                    html += "<table class='tblPickLocation' style='width: 100%;white-space:nowrap;'>";
                                    JSON.parse(n.vcWarehouseLocations.toString()).forEach(function (objLocation) {
                                        html += "<tr>";
                                        html += "<td>" + replaceNull(objLocation.Location) + " (" + (objLocation.Available || 0) + ") " + "<input type='hidden' class='hdnWarehouseItemID' value='" + objLocation.WarehouseItemID + "'/><input type='hidden' class='hdnAvailable' value='" + (objLocation.Available || 0) + "'/></td>";
                                        html += "<td><input type='text' class='txtQtyToPick form-control' value='0' style='padding:1px;width:80px;' /></td>";
                                        html += "</tr>";
                                    });
                                    html += "</table>";
                                    html += "<input type='hidden' class='hdnWorkOrderDetailID' value='" + n.numWODetailId + "'/>";
                                    html += "<input type='hidden' class='hdnItemCode' value='" + n.numItemCode + "'/>";
                                    html += "<input type='hidden' class='hdnWOWarehouseItemID' value='" + n.numWareHouseItemID + "'/>";
                                    html += "<input type='hidden' class='hdnRemining' value='" + ((n.numRequiredQty || 0) - (n.numPickedQty || 0)) + "' />";                                    
                                    html += "</td>";
                                    html += "<td>" + replaceNull(n.numPickedQty) + "</td>";
                                    html += "<td>" + ((n.numRequiredQty || 0) - (n.numPickedQty || 0)) + "</td>";
                                    html += "</tr>";
                                    $("#tblBOM > tbody").append(html);
                                });

                                $('.txtQtyToPick').on('input propertychange paste', function () {                                   
                                    var remainingQty = parseFloat($(this).closest("tr").closest("table.tblPickLocation").closest("tr").find(".hdnRemining").val());

                                    var pickedQty = 0;
                                    $(this).closest(".tblPickLocation").find("tr").each(function () {
                                        if (parseFloat($(this).find(".txtQtyToPick").val()) > 0) {
                                            pickedQty += parseFloat($(this).find(".txtQtyToPick").val());
                                        }
                                    });

                                    if (pickedQty > remainingQty) {
                                        alert("Picked quantity can not be greater then quantity left to be picked.");
                                        $(this).val("0");
                                    }
                                });

                                updatedRecords = [];
                            }
                        } catch (e) {
                            alert("Unknown error occurred while diplaying data.");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (IsJsonString(jqXHR.responseText)) {
                            var objError = $.parseJSON(jqXHR.responseText)
                            if (objError.Message != null) {
                                alert("Error occurred: " + objError.Message);
                            } else {
                                alert(objError);
                            }
                        } else {
                            alert("Unknown error ocurred while fetching data");
                        }
                    }
                });
            } catch (e) {
                alert("Unknown error occurred while fetching data.");
            }
        }

        function UpdatePickedQty() {
            var arrPickedItems = [];
            try {
                $("#tblBOM > tbody > tr").each(function (index, tr) {
                    $(this).find(".tblPickLocation").find("tr").each(function () {
                        if (parseFloat($(this).find(".txtQtyToPick").val()) > 0) {
                            var objItem = {};
                            objItem.numWODetailID = parseInt($(tr).find(".hdnWorkOrderDetailID").val());
                            objItem.numItemCode = parseInt($(tr).find(".hdnItemCode").val());
                            objItem.numWarehouseItemID = parseInt($(tr).find(".hdnWOWarehouseItemID").val());
                            objItem.numPickedQty = parseFloat($(this).find(".txtQtyToPick").val());;
                            objItem.numFromWarehouseItemID = parseInt($(this).find(".hdnWarehouseItemID").val());
                            arrPickedItems.push(objItem);                           
                        }
                    });
                });

                if (arrPickedItems.length > 0) {
                    $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/UpdateWOPickedQuantity',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "pickedItems": JSON.stringify(arrPickedItems)
                        }),
                        success: function (data) {
                            updatedRecords = $.parseJSON(data.UpdateWOPickedQuantityResult);

                            var foundRecordsWithError = $.map(updatedRecords, function (e) {
                                if (!e.bitSuccess)
                                    return e;
                            });

                            if (foundRecordsWithError.length > 0) {
                                BindData();
                            }
                            else {
                                if (window.opener != null) {
                                    window.opener.location.href = window.opener.location.href;
                                }

                                window.close();
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (IsJsonString(jqXHR.responseText)) {
                                var objError = $.parseJSON(jqXHR.responseText)
                                if (objError.Message != null) {
                                    alert("Error occurred: " + objError.Message);
                                } else {
                                    alert(objError);
                                }
                            } else {
                                alert("Unknown error ocurred while updating picked quantity");
                            }
                        }
                    });
                } else {
                    alert("Please enter pick quantity for at least one row.");
                }
            } catch (e) {
                alert("Unknown error occurred while updating picked quantity.");
            }

            return false;
        }

        function replaceNull(value) {
            return String(value) === "null" || String(value) === "undefined" ? "" : value.toString().replace(/'/g, "&#39;");
        }

        function IsJsonString(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }
    </script>
    <style type="text/css">
        #min-grid-controls {
            margin: 0px;
        }

            #min-grid-controls ul.pagination {
                margin: 0px;
            }

            #min-grid-controls > li {
                padding: 0px;
                vertical-align: top;
            }

        #liRecordDisplay {
            line-height: 34px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:Button runat="server" ID="btnSaveClose" CssClass="btn btn-primary" Text="Save & Close" OnClientClick="return UpdatePickedQty();" />
                <asp:Button runat="server" ID="btnClose" CssClass="btn btn-primary" Text="Close" OnClientClick="return Close();" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <div class="overlay" id="divLoader" style="z-index: 10000">
        <div class="overlayContent" style="color: #000; background-color: #fff; text-align: center; width: 250px; padding: 20px">
            <i class="fa fa-2x fa-refresh fa-spin"></i>
            <h3>Please wait...</h3>
        </div>
    </div>
    <div class="row padbottom10" id="divError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>
                <p>
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <table id="tblBOM" class="table table-bordered table-primary">
                    <thead>
                        <tr>
                            <th colspan="3"></th>
                            <th><input type="text" id="txtSearchItem" class="form-control" /></th>
                            <th colspan="5"></th>
                        </tr>
                        <tr>
                            <th>Work Order</th>
                            <th>Assembly (Qty) </th>
                            <th style="width: 120px">Requested Date</th>
                            <th>BOM To Pick (Qty)</th>
                            <th style="width: 155px">Warehouse</th>
                            <th style="width: 100px">On Allocation</th>
                            <th style="width: 100px">Pick</th>
                            <th style="width: 100px">Picked</th>
                            <th style="width: 100px">Remaining</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    <asp:HiddenField ID="hdnSelectedRecords" runat="server" />
</asp:Content>
