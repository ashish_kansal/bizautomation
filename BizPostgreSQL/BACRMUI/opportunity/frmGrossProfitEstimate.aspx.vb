﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common

Public Class frmGrossProfitEstimate
    Inherits BACRMPage

    Dim lnglngOppId As Long
    Dim numerator As Decimal
    Dim denominator As Decimal


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lnglngOppId = GetQueryStringVal("oppid")

            If Not IsPostBack Then
                If lnglngOppId > 0 Then
                    bindGrid()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub bindGrid()
        Dim objOpportunity As New MOpportunity
        objOpportunity.OpportunityId = lnglngOppId
        objOpportunity.DomainID = Session("DomainID")

        Dim dtTableInfo As DataTable
        dtTableInfo = objOpportunity.GetGrossProfitEstimate()

        If Not dtTableInfo Is Nothing AndAlso dtTableInfo.Rows.Count > 0 Then
            lblShippingProfit.Text = Format("{0:#,##0.00;(#,##0.00);-}", CCommon.ToDouble(dtTableInfo.Rows(0)("monShippingProfit")))

            If CCommon.ToDouble(dtTableInfo.Rows(0)("monShippingProfit")) >= 0 Then
                lblShippingProfit.CssClass = "text-green"
            Else
                lblShippingProfit.CssClass = "text-red"
            End If
        Else
            lblShippingProfit.Text = Format("{0:#,##0.00;(#,##0.00);-}", 0)
            lblShippingProfit.CssClass = "text-green"
        End If

        gvGrossProfitEstimate.DataSource = dtTableInfo
        gvGrossProfitEstimate.DataBind()
    End Sub

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then If Money <> 0 Then Return String.Format("{0:#,###.00}", Money)
            Return "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub gvGrossProfitEstimate_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvGrossProfitEstimate.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                numerator = 0
                denominator = 0
            ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                Dim hdnBitApproval As HiddenField
                Dim lblItem As Label
                hdnBitApproval = e.Row.FindControl("hdnBitApproval")
                lblItem = e.Row.FindControl("lblItem")
                If Convert.ToBoolean(hdnBitApproval.Value) = True Then
                    lblItem.Text = lblItem.Text + "<img src='../images/icons/error.png'>"
                End If
                Dim drv As DataRowView = DirectCast(e.Row.DataItem, DataRowView)
                numerator = numerator + CCommon.ToDecimal(drv("Profit"))
                denominator = denominator + (CCommon.ToDecimal(drv("numUnitHour")) * CCommon.ToDecimal(drv("monPrice")))
            ElseIf e.Row.RowType = DataControlRowType.Footer Then
                lblProfitPercent.Text = FormatPercent((numerator / IIf(denominator = 0, 1, denominator)), 0)
                lblProfit.Text = String.Format("{0:" & CCommon.ToString(Session("Currency")) & " #,##0.00;(" & CCommon.ToString(Session("Currency")) & " #,##0.00);-}", numerator)

                If numerator >= 0 Then
                    lblProfit.CssClass = "text-green"
                Else
                    lblProfit.CssClass = "text-red"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class