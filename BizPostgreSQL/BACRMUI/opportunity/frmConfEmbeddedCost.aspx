﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmConfEmbeddedCost.aspx.vb"
    Inherits=".frmConfEmbeddedCost" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Configure Embedded Cost</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClientClick="return Close();" />&nbsp;&nbsp;
        </div>
    </div>
    <table width="100%" cellpadding="2" cellspacing="2">
        <tr>
            <td class="normal4" align="center" colspan="2" valign="top">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Embedded Cost
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <table width="600px" height="300px">
        <tr>
            <td valign="top">
                Embedded Cost Category
            </td>
            <td align="left" valign="top" class="normal1">
                <asp:DropDownList runat="server" ID="ddlCostCategory" CssClass="signup" AutoPostBack="true">
                </asp:DropDownList>
            </td>
        </tr>
        <tr valign="top">
            <td colspan="2">
                <asp:GridView ID="gvReturns" runat="server" AutoGenerateColumns="false" CssClass="tbl"
                    Width="100%" DataKeyNames="numEmbeddedCostID">
                    <%--<AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />--%>
                    <Columns>
                        <asp:BoundField HeaderText="numEmbeddedCostID" DataField="numEmbeddedCostID" Visible="false" />
                        <asp:BoundField HeaderText="numCostCatID" DataField="numCostCatID" Visible="false" />
                        <asp:TemplateField HeaderText="Cost Center Name">
                            <ItemTemplate>
                                <asp:DropDownList runat="server" ID="ddlCostCenter" CssClass="signup">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Expense Account">
                            <ItemTemplate>
                                <asp:DropDownList runat="server" ID="ddlAccount" CssClass="signup">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vendor">
                            <ItemTemplate>
                                <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="200px"
                                    Skin="Default" runat="server" AllowCustomText="True" EnableLoadOnDemand="True">
                                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                </telerik:RadComboBox>
                                <%-- <rad:RadComboBox AccessKey="C" ID="radCmbCompany" ExternalCallBackPage="../include/LoadCompany.aspx"
                                                Width="195px" DropDownWidth="200px" Skin="WindowsXP" runat="server" AutoPostBack="True"
                                                AllowCustomText="True" EnableLoadOnDemand="True">
                                            </rad:RadComboBox>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Payment Method">
                            <ItemTemplate>
                                <asp:DropDownList runat="server" ID="ddlPaymentMethod" CssClass="signup">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <%-- <table cellpadding="0" cellspacing="0" width="100%">
        <tr valign="bottom">
            <td>
                <table class="TabStyle">
                    <tbody>
                        <tr>
                            <td>
                                <span>Embed cost on following item classification</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td>
                <asp:Button ID="btnSaveClass" runat="server" CssClass="button" Text="Save" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Table ID="table1" Width="100%" runat="server" Height="100" GridLines="None"
                    BorderColor="black" CssClass="aspTable" BorderWidth="1" CellSpacing="0" CellPadding="0">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:CheckBoxList runat="server" ID="chkClassification" CssClass="signup" RepeatDirection="Vertical"
                                RepeatColumns="5">
                            </asp:CheckBoxList>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
    </table>--%>
</asp:Content>
