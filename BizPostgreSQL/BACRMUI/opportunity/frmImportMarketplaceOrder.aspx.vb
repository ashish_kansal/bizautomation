﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.WebAPI
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Accounting

Imports System.Data


Public Class frmImportMarketplaceOrder
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then
                BindDropDowns()
                btnGetOrders.Attributes.Add("onclick", "javascript:return ValidateGetOrderList();")
                btnImportOrder.Attributes.Add("onclick", "javascript:return ValidateReImportSelected();")

            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindDropDowns()
        Try
            Dim objWebApi As New WebAPI
            objWebApi.Mode = 4
            objWebApi.DomainID = CCommon.ToLong(Session("DomainID"))
            Dim dtWebAPI As DataTable = objWebApi.GetWebApi()
            ddlWebApi.DataValueField = "WebApiId"
            ddlWebApi.DataTextField = "vcProviderName"
            ddlWebApi.DataSource = dtWebAPI
            ddlWebApi.DataBind()
            ddlWebApi.Items.Insert(0, "--Select One--")
            ddlWebApi.Items.FindByText("--Select One--").Value = 0

        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub btnImportOrder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnImportOrder.Click
        Try
            Dim objWebApi As New WebAPI

            For Each gvRow As GridViewRow In gvMatchingOrders.Rows
                Dim chkSelect As CheckBox = CType(gvRow.FindControl("chkSelect"), CheckBox)
                Dim lblOppID As Label = CType(gvRow.FindControl("lblOppID"), Label)
                Dim lblMarketplaceOrderId As Label = CType(gvRow.FindControl("lblMarketplaceOrderId"), Label)
                ' Dim lblOrderPosted As Label = CType(gvRow.FindControl("lblOrderPosted"), Label)
              
                If chkSelect.Checked = True Then
                    objWebApi.ImportApiOrderReqId = 0
                    objWebApi.DomainID = CCommon.ToLong(Session("DomainID"))
                    objWebApi.WebApiId = CCommon.ToInteger(ddlWebApi.SelectedValue)
                    'If (txtApiOrderId.Text.Trim = "") Then
                    '    objWebApi.API_OrderId = "0"
                    'Else
                    '    objWebApi.API_OrderId = txtApiOrderId.Text.Trim
                    'End If
                    objWebApi.API_OrderId = lblMarketplaceOrderId.Text.Trim
                    objWebApi.Status = CCommon.ToBool(1)
                    objWebApi.OrderImportReqType = 1
                    objWebApi.OrdersFromDate = DateTime.Now
                    objWebApi.OrdersToDate = DateTime.Now
                    objWebApi.OppId = CCommon.ToLong(lblOppID.Text)
                    'If Not calOrderFromDate.SelectedDate Is Nothing Then
                    '    objWebApi.OrdersFromDate = calOrderFromDate.SelectedDate
                    'Else
                    '    objWebApi.OrdersFromDate = DateTime.Now
                    'End If

                    'If Not calOrderTodate.SelectedDate Is Nothing Then
                    '    objWebApi.OrdersToDate = calOrderTodate.SelectedDate
                    'Else
                    '    objWebApi.OrdersToDate = DateTime.Now
                    'End If

                    objWebApi.ManageAPIImportOrderRequest()
                End If

            Next

            GetMatchingOrdersToReImport()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub

    Private Sub btnGetOrders_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGetOrders.Click
        Try
            GetMatchingOrdersToReImport()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub

    Private Sub GetMatchingOrdersToReImport()
        Try
            Dim dtOrders As New DataTable
            Dim objWebApi As New WebAPI
            objWebApi.Mode = 0
            objWebApi.DomainID = CCommon.ToLong(Session("DomainID"))
            objWebApi.WebApiId = CCommon.ToInteger(ddlWebApi.SelectedValue)
            objWebApi.API_OrderId = "0"

            If Not calOrderFromDate.SelectedDate Is Nothing Then
                objWebApi.OrdersFromDate = calOrderFromDate.SelectedDate
            Else
                objWebApi.OrdersFromDate = DateTime.Now
            End If

            If Not calOrderTodate.SelectedDate Is Nothing Then
                objWebApi.OrdersToDate = calOrderTodate.SelectedDate
            Else
                objWebApi.OrdersToDate = DateTime.Now
            End If

            dtOrders = objWebApi.GetApiOpportunities()
            gvMatchingOrders.DataSource = dtOrders
            gvMatchingOrders.DataBind()
        Catch ex As Exception
            Throw ex

        End Try
    End Sub
End Class