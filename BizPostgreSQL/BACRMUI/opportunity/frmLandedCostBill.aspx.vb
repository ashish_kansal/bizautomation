﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Opportunities

Public Class frmLandedCostBill
    Inherits BACRMPage

    Dim dtChartAcntDetails As DataTable
    Dim dtVendors As DataTable
    Dim dtItems As New DataTable
    Dim lngOppId As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngOppId = CCommon.ToLong(GetQueryStringVal("OppId"))
            DomainID = Session("DomainId")
            UserCntID = Session("UserContactID")

            If Not IsPostBack Then
                Dim objOpp As New MOpportunity
                objOpp.DomainID = DomainID
                objOpp.OpportunityId = lngOppId
                Dim canApplyLandedCost As Boolean = objOpp.CanApplyLandedCost()

                If Not canApplyLandedCost Then
                    tblLandedCost.Visible = False
                    lblError.Visible = True
                Else
                    lblError.Visible = False
                    tblLandedCost.Visible = True
                    If rblLandedCost.Items.FindByValue(Session("LanedCostDefault")) IsNot Nothing Then
                        rblLandedCost.ClearSelection()
                        rblLandedCost.Items.FindByValue(Session("LanedCostDefault")).Selected = True
                    End If

                    FillNetDays()

                    calBillDate.SelectedDate = Date.Now

                    BindGrid(boolPostback:=True)

                    PaymentHistory1.LandedCostOppId = lngOppId
                    PaymentHistory1.BindGrid()
                End If

              
            End If

            calBillDate.Attributes.Add("onchange", "return duedateChage('" & CCommon.GetValidationDateFormat() & "');")
            calBillDate.Attributes.Add("onclick", "return duedateChage('" & CCommon.GetValidationDateFormat() & "');")
            ddlTerms.Attributes.Add("onchange", "return duedateChage('" & CCommon.GetValidationDateFormat() & "');")
            hfDateFormat.Value = CCommon.GetValidationDateFormat()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub FillNetDays()
        Try
            Dim objOpp As New OppotunitiesIP
            Dim dtTerms As New DataTable
            With objOpp
                .DomainID = Session("DomainID")
                .TermsID = 0
                dtTerms = .GetTerms()
            End With

            If dtTerms IsNot Nothing Then
                ddlTerms.DataSource = dtTerms
                ddlTerms.DataTextField = "vcTerms"
                ddlTerms.DataValueField = "vcValue"
                ddlTerms.DataBind()
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindGrid(Optional deleteRowIndex As Integer = -1, Optional boolAddNew As Boolean = False, Optional boolPostback As Boolean = False)
        Dim j As Integer
        Dim drRow As DataRow
        Try
            dtItems.Rows.Clear()

            Dim objLC As New LandedCost
            objLC.DomainID = Session("DomainID")
            objLC.OppID = lngOppId
            dtItems = objLC.GetLandedCostBillDetails

            If boolPostback AndAlso dtItems.Rows.Count > 0 Then
                calBillDate.SelectedDate = dtItems.Rows(0)("dtBillDate")

                If ddlTerms.Items.FindByValue(CCommon.ToString(dtItems.Rows(0)("vcTermName"))) IsNot Nothing Then
                    ddlTerms.ClearSelection()
                    ddlTerms.Items.FindByValue(CCommon.ToString(dtItems.Rows(0)("vcTermName"))).Selected = True
                End If
                calDueDate.SelectedDate = dtItems.Rows(0)("dtDueDate")

                If rblLandedCost.Items.FindByValue(CCommon.ToString(dtItems.Rows(0)("vcLanedCost"))) IsNot Nothing Then
                    rblLandedCost.ClearSelection()
                    rblLandedCost.Items.FindByValue(CCommon.ToString(dtItems.Rows(0)("vcLanedCost"))).Selected = True
                End If
            End If

            If boolPostback = False Then
                dtItems.Rows.Clear()

                Dim gvRow As GridViewRow
                Dim dtrow As DataRow

                For i As Integer = 0 To gvLandedExpense.Rows.Count - 1
                    If i <> deleteRowIndex Then
                        dtrow = dtItems.NewRow
                        gvRow = gvLandedExpense.Rows(i)

                        dtrow.Item("numBillID") = CCommon.ToLong(CType(gvRow.FindControl("hdnBillID"), HiddenField).Value)
                        dtrow.Item("numBillDetailID") = CCommon.ToLong(CType(gvRow.FindControl("hdnBillDetailID"), HiddenField).Value)
                        dtrow.Item("numTransactionId_Bill") = CCommon.ToLong(CType(gvRow.FindControl("hdnTransactionId_Bill"), HiddenField).Value)
                        dtrow.Item("numTransactionId_BillDetail") = CCommon.ToLong(CType(gvRow.FindControl("hdnTransactionId_BillDetail"), HiddenField).Value)
                        dtrow.Item("monAmtPaid") = CCommon.ToDecimal(CType(gvRow.FindControl("hdnAmtPaid"), HiddenField).Value)

                        dtrow.Item("numDivisionID") = CCommon.ToLong(CType(gvRow.FindControl("ddlVendor"), DropDownList).SelectedValue)
                        dtrow.Item("numExpenseAccountID") = CCommon.ToLong(CType(gvRow.FindControl("ddlExpenseAccount"), DropDownList).SelectedValue.Split("~")(1))
                        dtrow.Item("monAmountDue") = CCommon.ToDecimal(CType(gvRow.FindControl("txtAmount"), TextBox).Text)

                        If dtrow.Item("numDivisionID") > 0 AndAlso
                            dtrow.Item("numExpenseAccountID") > 0 AndAlso
                            dtrow.Item("monAmountDue") > 0 Then
                            dtItems.Rows.Add(dtrow)
                        End If
                    End If
                Next
            End If

            If boolAddNew Or dtItems.Rows.Count = 0 Then
                drRow = dtItems.NewRow

                dtItems.Rows.Add(drRow)
            End If

            gvLandedExpense.DataSource = dtItems
            gvLandedExpense.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function ReturnMoney(ByVal Money As Object) As String
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:###0.00}", Money)

            Return ""
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub gvLandedExpense_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvLandedExpense.RowCommand
        Try
            If e.CommandName = "DeleteRow" Then
                Dim row As GridViewRow = CType(CType(e.CommandSource, Control).NamingContainer, GridViewRow)

                Dim BillID As Long = CCommon.ToLong(CType(row.FindControl("hdnBillID"), HiddenField).Value)
                If BillID > 0 Then
                    Dim objJournalEntry As New JournalEntry
                    objJournalEntry.JournalId = 0
                    objJournalEntry.BillPaymentID = 0
                    objJournalEntry.DepositId = 0
                    objJournalEntry.CheckHeaderID = 0
                    objJournalEntry.BillID = BillID
                    objJournalEntry.CategoryHDRID = 0
                    objJournalEntry.ReturnID = 0
                    objJournalEntry.DomainID = Session("DomainId")
                    objJournalEntry.UserCntID = Session("UserContactID")

                    Try
                        Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                            objJournalEntry.DeleteJournalEntryDetails()

                            Dim objLC As New LandedCost
                            objLC.DomainID = Session("DomainID")
                            objLC.OppID = lngOppId
                            objLC.LandedCostOpportunityUpdate(rblLandedCost.SelectedValue)

                            objTransactionScope.Complete()
                        End Using
                    Catch ex As Exception
                        If ex.Message = "BILL_PAID" Then
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('This transaction has been paid. If you want to change or delete it, you must edit the bill payment it appears on and remove it first.');", True)
                            Exit Sub
                        Else
                            Throw ex
                        End If
                    End Try
                End If

                BindGrid(deleteRowIndex:=row.DataItemIndex)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub gvLandedExpense_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvLandedExpense.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim ddlExpenseAccount As DropDownList = DirectCast(e.Row.FindControl("ddlExpenseAccount"), DropDownList)
                Dim ddlVendor As DropDownList = DirectCast(e.Row.FindControl("ddlVendor"), DropDownList)

                If dtChartAcntDetails Is Nothing Then
                    Dim objLC As New LandedCost
                    objLC.DomainID = Session("DomainID")
                    dtChartAcntDetails = objLC.GetLandedCostExpenseAccount
                End If

                If dtVendors Is Nothing Then
                    Dim objLC As New LandedCost
                    objLC.DomainID = Session("DomainID")
                    dtVendors = objLC.GetLandedCostVendors
                End If

                Dim item As ListItem
                For Each dr As DataRow In dtChartAcntDetails.Rows
                    item = New ListItem()
                    item.Text = dr("vcDescription")
                    item.Value = String.Format("{0}~{1}~{2}", dr("numLCExpenseId"), dr("numAccountId"), dr("vcAccountName"))

                    If CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numExpenseAccountID")) > 0 Then
                        If dr("numAccountId") = CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numExpenseAccountID")) Then
                            item.Selected = True
                            DirectCast(e.Row.FindControl("lblExpenseAccount"), Label).Text = dr("vcAccountName")
                        End If
                    End If

                    ddlExpenseAccount.Items.Add(item)
                Next
                ddlExpenseAccount.Items.Insert(0, New ListItem("--Select One --", "0~0~0"))

                For Each dr As DataRow In dtVendors.Rows
                    item = New ListItem()
                    item.Text = dr("vcCompanyName")
                    item.Value = dr("numVendorId")
                    ddlVendor.Items.Add(item)
                Next
                ddlVendor.Items.Insert(0, New ListItem("--Select One --", "0"))

                If CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numDivisionID")) > 0 Then
                    If Not ddlVendor.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numDivisionID"))) Is Nothing Then
                        ddlVendor.Items.FindByValue(CCommon.ToLong(CType(e.Row.DataItem, DataRowView).Row("numDivisionID"))).Selected = True
                    End If
                End If

                If CCommon.ToDecimal(CType(e.Row.DataItem, DataRowView).Row("monAmtPaid")) > 0 Then
                    ddlExpenseAccount.Enabled = False
                    ddlVendor.Enabled = False
                    DirectCast(e.Row.FindControl("txtAmount"), TextBox).Enabled = False
                    DirectCast(e.Row.FindControl("btnDeleteRow"), Button).Visible = False

                    Dim lnkDelete As LinkButton = CType(e.Row.FindControl("lnkDelete"), LinkButton)
                    lnkDelete.Attributes.Add("onclick", "return NotAllowMessage(1)")
                    lnkDelete.Visible = True
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Try
            BindGrid(boolAddNew:=True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            If SaveBill() Then
                Dim objLC As New LandedCost
                objLC.DomainID = Session("DomainID")
                objLC.OppID = lngOppId
                objLC.LandedCostOpportunityUpdate(rblLandedCost.SelectedValue)

                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Close", "<script> if (window.opener != null) {window.opener.location.reload(true);Close();}</script>")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    'Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
    '    Try

    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub


    Function SaveBill() As Boolean
        Try

            Dim objOppInvoice As New OppInvoice
            Dim dtBillDetails As New DataTable

            Dim gvRow As GridViewRow
            Dim dtrow As DataRow

            CCommon.AddColumnsToDataTable(dtBillDetails, "numBillDetailID,numExpenseAccountID,monAmount,vcDescription,numProjectID,numClassID,numCampaignID")

            For i As Integer = 0 To gvLandedExpense.Rows.Count - 1
                dtBillDetails.Rows.Clear()

                dtrow = dtBillDetails.NewRow
                gvRow = gvLandedExpense.Rows(i)

                dtrow.Item("numBillDetailID") = CCommon.ToLong(CType(gvRow.FindControl("hdnBillDetailID"), HiddenField).Value)
                dtrow.Item("numExpenseAccountID") = CCommon.ToLong(CType(gvRow.FindControl("ddlExpenseAccount"), DropDownList).SelectedValue.Split("~")(1))
                dtrow.Item("monAmount") = CCommon.ToDecimal(CType(gvRow.FindControl("txtAmount"), TextBox).Text)
                dtrow.Item("vcDescription") = CCommon.ToLong(CType(gvRow.FindControl("ddlExpenseAccount"), DropDownList).SelectedItem.Text)
                dtrow.Item("numProjectID") = 0
                dtrow.Item("numClassID") = 0
                dtrow.Item("numCampaignID") = 0

                If CCommon.ToLong(CType(gvRow.FindControl("ddlVendor"), DropDownList).SelectedValue) > 0 AndAlso
                           dtrow.Item("numExpenseAccountID") > 0 AndAlso
                           dtrow.Item("monAmount") > 0 Then

                    dtBillDetails.Rows.Add(dtrow)

                    With objOppInvoice
                        .BillID = CCommon.ToLong(CType(gvRow.FindControl("hdnBillID"), HiddenField).Value)
                        .DivisionID = CCommon.ToLong(CType(gvRow.FindControl("ddlVendor"), DropDownList).SelectedValue)
                        .OppId = lngOppId
                        .IsLandedCost = True

                        .Reference = "Landed Cost"
                        .BillDate = calBillDate.SelectedDate
                        .AmountDue = CCommon.ToDecimal(CType(gvRow.FindControl("txtAmount"), TextBox).Text)
                        .Terms = CCommon.ToString(ddlTerms.SelectedValue).Split("~")(0)
                        '.TotalBillAmount = CCommon.ToDouble(hdnAmtPaid.Value)
                        .DueDate = calDueDate.SelectedDate
                        .Memo = "Landed Cost"
                        .DomainID = DomainID
                        .UserCntID = UserCntID

                        Dim dsBill As New DataSet
                        dsBill.Tables.Add(dtBillDetails.Copy())
                        .StrItems = dsBill.GetXml()


                        Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                            Dim lngBillID As Long = .ManageBillHeader()

                            If lngBillID > 0 Then
                                Dim lngJournalId As Long
                                lngJournalId = SaveDataToHeader(CCommon.ToLong(CType(gvRow.FindControl("hdnTransactionId_Bill"), HiddenField).Value), lngBillID, objOppInvoice.AmountDue)
                                SaveDataToGeneralJournalDetails(lngJournalId, lngBillID)
                            End If

                            objTransactionScope.Complete()
                        End Using
                    End With
                End If
            Next

            litMessage.Visible = True
            litMessage.Attributes.Add("class", "successInfo")
            litMessage.Text = "Bill is Created."

            Return True
        Catch ex As Exception
            If ex.Message = "FY_CLOSED" Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
                Return False
            Else
                Throw ex
            End If
        End Try
    End Function


    ''To Save Header details in General_Journal_Header table
    Private Function SaveDataToHeader(ByVal lngJournalId As Long, ByVal lngBillID As Long, ByVal Amount As Decimal) As Integer
        Try
            Dim objJEHeader As New JournalEntryHeader
            With objJEHeader
                .JournalId = lngJournalId
                .RecurringId = 0
                .EntryDate = CDate(calBillDate.SelectedDate & " 12:00:00")
                .Description = "Bill:Landed Cost"
                .Amount = Amount
                .CheckId = 0
                .CashCreditCardId = 0
                .ChartAcntId = 0
                .OppId = 0
                .OppBizDocsId = 0
                .DepositId = 0
                .BizDocsPaymentDetId = 0
                .IsOpeningBalance = 0
                .LastRecurringDate = Date.Now
                .NoTransactions = 0
                .CategoryHDRID = 0
                .ReturnID = 0
                .CheckHeaderID = 0
                .BillID = lngBillID
                .BillPaymentID = 0
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
            End With
            lngJournalId = objJEHeader.Save()
            Return lngJournalId
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub SaveDataToGeneralJournalDetails(ByVal lngJournalId As Long, ByVal lngBillID As Long)
        Try
            Dim objOppInvoice As New OppInvoice

            Dim dsBillDetails As New DataSet

            objOppInvoice.DomainID = DomainID
            objOppInvoice.BillID = CCommon.ToLong(lngBillID)
            dsBillDetails = objOppInvoice.FetchBillDetails()

            Dim dtBillHeader As New DataTable
            dtBillHeader = dsBillDetails.Tables(0)


            Dim objJEList As New JournalEntryCollection

            Dim objJE As New JournalEntryNew()

            objJE.TransactionId = dtBillHeader(0)("numTransactionId")
            objJE.DebitAmt = 0
            objJE.CreditAmt = Math.Abs(dtBillHeader(0)("monAmountDue"))

            If CCommon.ToLong(dtBillHeader(0)("numDivisionID")) = 0 And CCommon.ToLong(dtBillHeader(0)("numAccountID")) > 0 Then
                objJE.ChartAcntId = CCommon.ToLong(dtBillHeader(0)("numAccountID"))
                If objJE.ChartAcntId = 0 Then
                    objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("AP", Session("DomainID"))
                End If
                objJE.CustomerId = 0
            ElseIf CCommon.ToLong(dtBillHeader(0)("numDivisionID")) > 0 Then
                objJE.ChartAcntId = GetVendorAP_AccountID(CCommon.ToLong(dtBillHeader(0)("numDivisionID")))
                objJE.CustomerId = dtBillHeader(0)("numDivisionID")
            End If

            objJE.Description = "Bill"
            objJE.MainDeposit = 0
            objJE.MainCheck = 0
            objJE.MainCashCredit = 0
            objJE.OppitemtCode = 0
            objJE.BizDocItems = "AP"
            objJE.Reference = "Landed Cost"
            objJE.PaymentMethod = 0
            objJE.Reconcile = False
            objJE.CurrencyID = 0
            objJE.FltExchangeRate = 0
            objJE.TaxItemID = 0
            objJE.BizDocsPaymentDetailsId = 0
            objJE.ContactID = 0
            objJE.ItemID = 0
            objJE.ProjectID = 0
            objJE.ClassID = 0
            objJE.CommissionID = 0
            objJE.ReconcileID = 0
            objJE.Cleared = 0
            objJE.ReferenceType = enmReferenceType.BillHeader
            objJE.ReferenceID = lngBillID

            objJEList.Add(objJE)


            Dim dtDetails As New DataTable
            dtDetails = dsBillDetails.Tables(1)

            For Each dr As DataRow In dtDetails.Rows
                objJE = New JournalEntryNew()

                objJE.TransactionId = dr("numTransactionId")
                objJE.DebitAmt = Math.Abs(CCommon.ToDecimal(dr("monAmount")))
                objJE.CreditAmt = 0
                objJE.ChartAcntId = dr("numExpenseAccountID")
                objJE.Description = dr("vcDescription")
                objJE.CustomerId = CCommon.ToLong(dtBillHeader(0)("numDivisionID"))
                objJE.MainDeposit = 0
                objJE.MainCheck = 0
                objJE.MainCashCredit = 0
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "S"
                objJE.Reference = "Landed Cost"
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 0
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = dr("numProjectID")
                objJE.ClassID = dr("numClassID")
                objJE.CampaignID = dr("numCampaignID")
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = 0
                objJE.ReferenceType = enmReferenceType.BillDetail
                objJE.ReferenceID = CCommon.ToLong(dr("numBillDetailID"))

                objJEList.Add(objJE)
            Next

            objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, Session("DomainID"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Function GetVendorAP_AccountID(ByVal lngDivisionID As Long) As Long
        Try
            Dim AccountID As Long
            Dim objCOA As New ChartOfAccounting
            Dim ds As DataSet
            With objCOA
                .COARelationshipID = 0
                .DivisionID = lngDivisionID
                .DomainID = Session("DomainID")
                ds = .GetCOARelationship()
            End With
            If ds.Tables(0).Rows.Count > 0 Then
                AccountID = CCommon.ToLong(ds.Tables(0).Rows(0).Item("numAPAccountId"))
            End If

            If AccountID = 0 Then
                '' For Account Payable to be Debited
                AccountID = ChartOfAccounting.GetDefaultAccount("AP", Session("DomainID"))
            End If
            Return AccountID
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class