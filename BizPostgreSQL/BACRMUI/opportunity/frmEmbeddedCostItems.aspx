﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmEmbeddedCostItems.aspx.vb"
    Inherits=".frmEmbeddedCostItems" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/javascript">
        function keyPressed(TB, e) {
            var tblGrid = document.getElementById("dgItems");

            var rowcount = tblGrid.rows.length;
            var TBID = document.getElementById(TB);

            var key;
            if (window.event) { e = window.event; }
            key = e.keyCode;
            if (key == 37 || key == 38 || key == 39 || key == 40) {
                for (Index = 1; Index < rowcount; Index++) {

                    for (childIndex = 0; childIndex <
              tblGrid.rows[Index].cells.length; childIndex++) {

                        if (tblGrid.rows[Index].cells[childIndex].children[0] != null) {
                            if (tblGrid.rows[Index].cells[
                 childIndex].children[0].id == TBID.id) {

                                if (key == 40) {
                                    if (Index + 1 < rowcount) {
                                        if (tblGrid.rows[Index + 1].cells[
                     childIndex].children[0] != null) {
                                            if (tblGrid.rows[Index + 1].cells[
                       childIndex].children[0].type == 'text') {

                                                //downvalue

                                                tblGrid.rows[Index + 1].cells[
                             childIndex].children[0].focus();
                                                return false;
                                            }
                                        }
                                    }
                                }
                                if (key == 38) {
                                    if (tblGrid.rows[Index - 1].cells[
                     childIndex].children[0] != null) {
                                        if (tblGrid.rows[Index - 1].cells[
                       childIndex].children[0].type == 'text') {
                                            //upvalue

                                            tblGrid.rows[Index - 1].cells[
                             childIndex].children[0].focus();
                                            return false;
                                        }
                                    }
                                }

                                if (key == 37 && (childIndex != 0)) {

                                    if ((tblGrid.rows[Index].cells[
                      childIndex - 1].children[0]) != null) {

                                        if (tblGrid.rows[Index].cells[
                       childIndex - 1].children[0].type == 'text') {
                                            //left

                                            if (tblGrid.rows[Index].cells[
                         childIndex - 1].children[0].value != '') {
                                                var cPos =
                           getCaretPos(tblGrid.rows[Index].cells[
                                       childIndex - 1].children[0], 'left');
                                                if (cPos) {
                                                    tblGrid.rows[Index].cells[
                                 childIndex - 1].children[0].focus();
                                                    return false;
                                                }
                                                else {
                                                    return false;
                                                }
                                            }
                                            tblGrid.rows[Index].cells[childIndex - 1].children[0].focus();
                                            return false;
                                        }
                                    }
                                }

                                if (key == 39) {
                                    if (tblGrid.rows[Index].cells[childIndex + 1].children[0] != null) {
                                        if (tblGrid.rows[Index].cells[
                       childIndex + 1].children[0].type == 'text') {
                                            //right

                                            if (tblGrid.rows[Index].cells[
                         childIndex + 1].children[0].value != '') {
                                                var cPosR =
                           getCaretPos(tblGrid.rows[Index].cells[
                                       childIndex + 1].children[0], 'right');
                                                if (cPosR) {
                                                    tblGrid.rows[Index].cells[
                                 childIndex + 1].children[0].focus();
                                                    return false;
                                                }
                                                else {
                                                    return false;
                                                }
                                            }
                                            tblGrid.rows[Index].cells[childIndex + 1].children[0].focus();
                                            return false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        function getCaretPos(control, way) {
            var movement;
            if (way == 'left') {
                movement = -1;
            }
            else {
                movement = 1;
            }
            if (control.createTextRange) {
                control.caretPos = document.selection.createRange().duplicate();
                if (control.caretPos.move("character", movement) != '') {
                    return false;
                }
                else {
                    return true;
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table width="100%" cellpadding="2" cellspacing="2">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save &amp; Close" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <table align="center" width="100%">
        <tr>
            <td align="center" class="normal4">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Embedded Cost Items
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="tbl" CssClass="aspTable" CellPadding="0" CellSpacing="0" Height="300"
        runat="server" Width="500px" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgItems" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is" HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="numOppBizDocItemID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="vcItemName" HeaderText="Item" ItemStyle-VerticalAlign="Top"
                            ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:BoundColumn DataField="vcModelID" HeaderText="Model #"></asp:BoundColumn>
                        <asp:BoundColumn DataField="fltWeight" HeaderText="Weight"></asp:BoundColumn>
                        <asp:BoundColumn DataField="fltLength" HeaderText="Length"></asp:BoundColumn>
                        <asp:BoundColumn DataField="fltWidth" HeaderText="Width"></asp:BoundColumn>
                        <asp:BoundColumn DataField="fltHeight" HeaderText="Height"></asp:BoundColumn>
                        <asp:BoundColumn DataField="numUnitHour" HeaderText="Unit(s)"></asp:BoundColumn>
                        <asp:BoundColumn DataField="vcUnitofMeasure" HeaderText="UOM"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Amount">
                            <ItemTemplate>
                                <asp:TextBox ID="txtAmount" runat="server" CssClass="signup" Text='<%# ReturnMoney(Eval("monAmount")) %>'
                                    Width="50" onkeyup="keyPressed(this.id,event)" AUTOCOMPLETE="OFF"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
