Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Projects

Partial Public Class frmEditOppAddress : Inherits BACRMPage

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblError.Text = ""
            If Not IsPostBack Then

                Dim objOpp As New MOpportunity
                objOpp.OpportunityId = CCommon.ToLong(GetQueryStringVal("OppID"))

                If CCommon.ToLong(GetQueryStringVal("OppID")) > 0 Then
                    trProject.Visible = True
                    BindProjects()
                End If

                Dim objOrgOppAddressModificationHistory As New OrgOppAddressModificationHistory
                objOrgOppAddressModificationHistory.DomainID = CCommon.ToLong(Session("DomainID"))
                objOrgOppAddressModificationHistory.RecordID = objOpp.OpportunityId

                If GetQueryStringVal("AddType") = "Bill" Then
                    objOpp.Mode = 0
                    lblAddressType.Text = "Billing Name"
                    If objOpp.OpportunityId > 0 Then
                        objOrgOppAddressModificationHistory.AddressOfRecord = 2
                        objOrgOppAddressModificationHistory.AddressType = 1
                        Dim dtModificationHistory As DataTable = objOrgOppAddressModificationHistory.GetModificationDetail()

                        If Not dtModificationHistory Is Nothing AndAlso dtModificationHistory.Rows.Count > 0 Then
                            lblHistory.Text = "<b>Last Modified By:</b> " & CCommon.ToString(dtModificationHistory.Rows(0)("vcModifiedBy")) & ", " & CCommon.ToString(dtModificationHistory.Rows(0)("dtModifiedDate"))
                            lblHistory.Visible = True
                        End If
                    End If
                Else
                    objOpp.Mode = 1
                    tblDropShipAddress.Visible = True
                    lblAddressType.Text = "Shipping Name"

                    If objOpp.OpportunityId > 0 Then
                        objOrgOppAddressModificationHistory.AddressOfRecord = 2
                        objOrgOppAddressModificationHistory.AddressType = 2
                        Dim dtModificationHistory As DataTable = objOrgOppAddressModificationHistory.GetModificationDetail()

                        If Not dtModificationHistory Is Nothing AndAlso dtModificationHistory.Rows.Count > 0 Then
                            lblHistory.Text = "<b>Last Modified By:</b> " & CCommon.ToString(dtModificationHistory.Rows(0)("vcModifiedBy")) & ", " & CCommon.ToString(dtModificationHistory.Rows(0)("dtModifiedDate"))
                            lblHistory.Visible = True
                        End If
                    End If
                End If

                If CCommon.ToLong(GetQueryStringVal("ReturnID")) > 0 Then
                    objOpp.Mode = 2
                    objOpp.ReturnHeaderID = CCommon.ToLong(GetQueryStringVal("ReturnID"))
                End If

                If CCommon.ToLong(GetQueryStringVal("ProID")) > 0 Then
                    objOpp.Mode = 3
                    objOpp.OpportunityId = CCommon.ToLong(GetQueryStringVal("ProID"))
                    tblDropShipAddress.Visible = False
                    chkTogether.Visible = False
                End If

                Dim dtTable As DataTable
                dtTable = objOpp.GetOpportunityAddress

                objCommon.sb_FillComboFromDBwithSel(ddlBillCountry, 40, Session("DomainID"))
                If dtTable.Rows.Count > 0 Then
                    txtName.Text = CCommon.ToString(dtTable.Rows(0)("Company"))

                    txtStreet.Text = CCommon.ToString(dtTable.Rows(0)("Street"))
                    txtCity.Text = CCommon.ToString(dtTable.Rows(0)("City"))
                    txtPostal.Text = CCommon.ToString(dtTable.Rows(0)("PostCode"))
                    ddlBillCountry.SelectedValue = CCommon.ToString(dtTable.Rows(0)("Country"))
                    ddlBillCountry_SelectedIndexChanged(Nothing, Nothing)
                    ddlBillState.SelectedValue = CCommon.ToString(dtTable.Rows(0)("State"))

                    If CCommon.ToBool(dtTable.Rows(0)("bitAltContact")) Then
                        rbAddressContact.Checked = False
                        rbAddressContactAlt.Checked = True
                        txtAddressContactAlt.Text = CCommon.ToString(dtTable.Rows(0)("vcAltContact"))
                    Else
                        rbAddressContact.Checked = True
                        rbAddressContactAlt.Checked = False
                        ddlAddressContact.Items.Add(New ListItem(CCommon.ToString(dtTable.Rows(0)("vcContact")), CCommon.ToString(dtTable.Rows(0)("numContact"))))
                        ddlAddressContact.SelectedValue = CCommon.ToString(dtTable.Rows(0)("numContact"))
                    End If

                    radCmbCompany.Text = CCommon.ToString(dtTable.Rows(0)("Company"))
                    radCmbCompany.SelectedValue = CCommon.ToString(dtTable.Rows(0)("DivisionID"))

                    OrganizationChanged(False)

                    If CCommon.ToLong(dtTable.Rows(0)("numAddressID")) > 0 Then
                        Dim isWarehouseAddress As Boolean = False
                        Dim addressID As Long = CCommon.ToLong(dtTable.Rows(0)("numAddressID"))
                        Dim objContact As New CContacts
                        objContact.DomainID = Session("DomainID")
                        objContact.AddressID = addressID
                        objContact.byteMode = 1
                        Dim dtAddress As DataTable = objContact.GetAddressDetail

                        If Not dtAddress Is Nothing AndAlso dtAddress.Rows.Count > 0 Then
                            If CCommon.ToShort(dtAddress.Rows(0)("tintAddressOf")) = 4 Then 'Project
                                If Not ddlProject.Items.FindByValue(CCommon.ToLong(dtAddress.Rows(0)("numRecordID"))) Is Nothing Then
                                    ddlProject.Items.FindByValue(CCommon.ToLong(dtAddress.Rows(0)("numRecordID"))).Selected = True
                                    rbCompany.Checked = False
                                    rbContact.Checked = False
                                    rbProject.Checked = True
                                End If
                            ElseIf CCommon.ToShort(dtAddress.Rows(0)("tintAddressOf")) = 1 Then 'Contact
                                radCmbCompany.Text = CCommon.ToString(dtAddress.Rows(0)("vcCompanyName"))
                                radCmbCompany.SelectedValue = CCommon.ToLong(dtAddress.Rows(0)("numDivisionID"))
                                OrganizationChanged(False)

                                If Not ddlContact.Items.FindByValue(CCommon.ToLong(dtAddress.Rows(0)("numRecordID"))) Is Nothing Then
                                    ddlContact.Items.FindByValue(CCommon.ToLong(dtAddress.Rows(0)("numRecordID"))).Selected = True
                                    BindAddressName()
                                    If Not ddlAddressType.Items.FindByValue(addressID) Is Nothing Then
                                        ddlAddressType.Items.FindByValue(addressID).Selected = True
                                        rbCompany.Checked = False
                                        rbContact.Checked = True
                                        rbProject.Checked = False
                                    End If
                                End If
                            ElseIf CCommon.ToShort(dtAddress.Rows(0)("tintAddressOf")) = 2 Then 'Organization
                                radCmbCompany.Text = CCommon.ToString(dtAddress.Rows(0)("vcCompanyName"))
                                radCmbCompany.SelectedValue = CCommon.ToLong(dtAddress.Rows(0)("numRecordID"))
                                OrganizationChanged(False)

                                If Not ddlAddressName.Items.FindByValue(addressID) Is Nothing Then
                                    ddlAddressName.Items.FindByValue(addressID).Selected = True
                                    rbCompany.Checked = True
                                    rbContact.Checked = False
                                    rbProject.Checked = False
                                End If
                            End If
                        End If

                    End If
                End If
            End If

            btnClose.Attributes.Add("onclick", "return Close()")

            If Not String.IsNullOrEmpty(hdnGoogleState.Value) AndAlso
                Not ddlBillState.SelectedItem Is Nothing AndAlso ddlBillState.SelectedItem.Text <> hdnGoogleState.Value Then
                If Not ddlBillState.Items.FindByText(hdnGoogleState.Value) Is Nothing Then
                    ddlBillState.ClearSelection()
                    ddlBillState.Items.FindByText(hdnGoogleState.Value).Selected = True
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub BindProjects()
        Try
            Dim objProject As New Project
            objProject.DomainID = Session("DomainID")
            Dim dtProject As DataTable = objProject.GetOpenProject()
            ddlProject.DataTextField = "vcProjectName"
            ddlProject.DataValueField = "numProId"
            ddlProject.DataSource = dtProject
            ddlProject.DataBind()
            ddlProject.Items.Insert(0, "--Select One--")
            ddlProject.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objOpp As New MOpportunity

            If CCommon.ToLong(GetQueryStringVal("OppID")) > 0 Then
                objOpp.OpportunityId = CCommon.ToLong(GetQueryStringVal("OppID"))

                If GetQueryStringVal("AddType") = "Bill" Then
                    objOpp.Mode = 0
                Else
                    objOpp.Mode = 1
                End If

            ElseIf CCommon.ToLong(GetQueryStringVal("ProID")) > 0 Then
                objOpp.OpportunityId = CCommon.ToLong(GetQueryStringVal("ProID"))
                objOpp.Mode = 3
            End If

            If radCmbCompany.SelectedValue <> "" Then
                objOpp.CompanyID = radCmbCompany.SelectedValue ' This line is added by Ajit Singh on 01/10/2008
                objOpp.BillCompanyName = txtName.Text ' This line is added by Ajit Singh on 01/10/2008
            End If
            'End If

            If rbCompany.Checked Then
                objOpp.BillToAddressID = ddlAddressName.SelectedValue
            ElseIf rbContact.Checked Then
                objOpp.BillToAddressID = ddlAddressType.SelectedValue
            ElseIf rbProject.Checked Then
                Dim objProject As New BACRM.BusinessLogic.Projects.Project
                objProject.DomainID = CCommon.ToLong(Session("DomainID"))
                objProject.ProjectID = CCommon.ToLong(ddlProject.SelectedValue)
                Dim projectAddressID As Long = CCommon.ToLong(objProject.GetFieldValue("numAddressID"))

                objOpp.BillToAddressID = projectAddressID
            End If

            objOpp.BillStreet = txtStreet.Text
            objOpp.BillCity = txtCity.Text
            objOpp.BillPostal = txtPostal.Text
            If ddlBillState.SelectedIndex > 0 Then objOpp.BillState = ddlBillState.SelectedValue
            objOpp.BillCountry = ddlBillCountry.SelectedValue
            objOpp.DomainID = Session("DomainID")
            objOpp.UserCntID = Session("UserContactID")
            objOpp.AddressName = txtAddressName.Text.Trim
            objOpp.BillCompanyName = txtName.Text.Trim()

            If rbAddressContactAlt.Checked Then
                objOpp.ContactID = 0
                objOpp.IsAltContact = True
                objOpp.AltContact = txtAddressContactAlt.Text.Trim()
            Else
                objOpp.ContactID = CCommon.ToLong(ddlAddressContact.SelectedValue)
                objOpp.IsAltContact = False
            End If

            objOpp.UpdateOpportunityAddress()

            OppJournal(objOpp.OpportunityId)
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Save", "Save();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlBillCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBillCountry.SelectedIndexChanged
        Try
            FillState(ddlBillState, ddlBillCountry.SelectedItem.Value, Session("DomainID"))

            If Not String.IsNullOrEmpty(hdnGoogleState.Value) Then
                If Not ddlBillState.Items.FindByText(hdnGoogleState.Value) Is Nothing Then
                    ddlBillState.Items.FindByText(hdnGoogleState.Value).Selected = True
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
        Try
            OrganizationChanged(True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlContact_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContact.SelectedIndexChanged
        Try
            BindAddressName()
            Me._fillAddress()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub _fillAddress()
        Try
            Dim oCOpportunities As New COpportunities
            Dim sAddressType As String = GetQueryStringVal("AddType")
            Dim sPrimaryContactCheck As String = "No"

            If CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                Dim dtAddress As DataTable
                If (rbCompany.Checked And CCommon.ToLong(ddlAddressName.SelectedValue) = 0) Or (rbContact.Checked And CCommon.ToLong(ddlAddressType.SelectedValue) = 0) Then
                    'Load Existing Address
                    oCOpportunities.DomainID = CType((Session("DomainID")), Long)
                    dtAddress = oCOpportunities.GetBizDocExistingAddress(CInt(Session("UserID")), CCommon.ToInteger(radCmbCompany.SelectedValue), sAddressType, sPrimaryContactCheck)
                    If dtAddress.Rows.Count = 0 Then Exit Sub

                    BindName()

                    txtStreet.Text = IIf(IsDBNull(dtAddress.Rows(0).Item("Street")), "", dtAddress.Rows(0).Item("Street"))
                    txtCity.Text = IIf(IsDBNull(dtAddress.Rows(0).Item("City")), "", dtAddress.Rows(0).Item("City"))
                    txtAddressName.Text = IIf(IsDBNull(dtAddress.Rows(0).Item("vcAddressName")), "", dtAddress.Rows(0).Item("vcAddressName"))

                    If Not IsDBNull(dtAddress.Rows(0).Item("Country")) Then
                        If Not ddlBillCountry.Items.FindByValue(dtAddress.Rows(0).Item("Country")) Is Nothing Then
                            ddlBillCountry.ClearSelection()
                            ddlBillCountry.Items.FindByValue(dtAddress.Rows(0).Item("Country")).Selected = True
                        End If
                    End If
                    If ddlBillCountry.SelectedIndex > 0 Then
                        FillState(ddlBillState, ddlBillCountry.SelectedItem.Value, Session("DomainID"))
                        If Not IsDBNull(dtAddress.Rows(0).Item("State")) Then
                            If Not ddlBillState.Items.FindByValue(dtAddress.Rows(0).Item("State")) Is Nothing Then
                                ddlBillState.ClearSelection()
                                ddlBillState.Items.FindByValue(dtAddress.Rows(0).Item("State")).Selected = True
                            End If
                        End If
                    End If
                    txtPostal.Text = IIf(IsDBNull(dtAddress.Rows(0).Item("PostCode")), "", dtAddress.Rows(0).Item("PostCode"))

                ElseIf rbCompany.Checked And ddlAddressName.SelectedValue > 0 Then 'Company Address
                    BindAddressByAddressID(ddlAddressName.SelectedValue)
                ElseIf rbContact.Checked And ddlAddressType.SelectedValue > 0 Then 'Contact Address
                    BindAddressByAddressID(ddlAddressType.SelectedValue)
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            If chkDropShip.Checked = True Then
                Dim objOpp As New MOpportunity
                objOpp.OpportunityId = CCommon.ToLong(GetQueryStringVal("OppID"))
                objOpp.Mode = 2 'added new mode which will update tintShiptoType = 3 
                objOpp.UserCntID = Session("UserContactID")
                objOpp.UpdateOpportunityAddress()

                OppJournal(objOpp.OpportunityId)
                Response.Write("<script>opener.RefereshParentPage(); self.close();</script>")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub rbCompany_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbCompany.CheckedChanged
        Try
            Me._fillAddress()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub rbContact_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbContact.CheckedChanged
        Try
            Me._fillAddress()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub chkTogether_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkTogether.CheckedChanged
        Try
            BindName()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub BindName()
        Try
            If chkTogether.Checked Then
                txtName.Text = ddlContact.SelectedItem.Text + " (" + radCmbCompany.Text + ")"
            Else
                txtName.Text = radCmbCompany.Text
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindAddressName()
        Try
            Dim objContact As New CContacts
            If GetQueryStringVal("AddType") = "Bill" Then
                objContact.AddressType = CContacts.enmAddressType.BillTo
            Else
                objContact.AddressType = CContacts.enmAddressType.ShipTo
            End If
            objContact.DomainID = Session("DomainID")
            objContact.RecordID = radCmbCompany.SelectedValue
            objContact.AddresOf = CContacts.enmAddressOf.Organization
            objContact.byteMode = 2
            ddlAddressName.DataValueField = "numAddressID"
            ddlAddressName.DataTextField = "vcAddressName"
            ddlAddressName.DataSource = objContact.GetAddressDetail()
            ddlAddressName.DataBind()
            'If ddlAddressName.Items.Count > 0 Then
            '    ddlAddressName.SelectedIndex = 1
            'End If
            ddlAddressName.Items.Insert(0, New ListItem("--Select One--", "0"))
            If CCommon.ToInteger(ddlContact.SelectedValue) > 0 Then
                objContact.DomainID = Session("DomainID")
                objContact.RecordID = ddlContact.SelectedValue
                objContact.AddresOf = CContacts.enmAddressOf.Contact
                objContact.AddressType = CContacts.enmAddressType.None
                objContact.byteMode = 2
                ddlAddressType.DataValueField = "numAddressID"
                ddlAddressType.DataTextField = "vcAddressName"
                ddlAddressType.DataSource = objContact.GetAddressDetail()
                ddlAddressType.DataBind()
                ddlAddressType.Items.Insert(0, New ListItem("--Select One--", "0"))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlAddressType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAddressType.SelectedIndexChanged
        Try
            If rbContact.Checked Then
                Me._fillAddress()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlAddressName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAddressName.SelectedIndexChanged
        Try
            If rbCompany.Checked Then
                Me._fillAddress()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub BindAddressByAddressID(ByVal lngAddressID As Long)
        Try
            Dim objContact As New CContacts
            objContact.DomainID = Session("DomainID")
            objContact.AddressID = lngAddressID
            objContact.byteMode = 1
            Dim dtTable As DataTable = objContact.GetAddressDetail

            If dtTable.Rows.Count > 0 Then
                txtStreet.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcStreet")), "", dtTable.Rows(0).Item("vcStreet"))
                txtCity.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcCity")), "", dtTable.Rows(0).Item("vcCity"))
                txtAddressName.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcAddressName")), "", dtTable.Rows(0).Item("vcAddressName"))

                If Not IsDBNull(dtTable.Rows(0).Item("numCountry")) Then
                    If Not ddlBillCountry.Items.FindByValue(dtTable.Rows(0).Item("numCountry")) Is Nothing Then
                        ddlBillCountry.SelectedIndex = -1 ' reset selected index
                        ddlBillCountry.Items.FindByValue(dtTable.Rows(0).Item("numCountry")).Selected = True
                    End If
                End If
                If ddlBillCountry.SelectedIndex > 0 Then
                    FillState(ddlBillState, ddlBillCountry.SelectedItem.Value, Session("DomainID"))
                End If
                If Not IsDBNull(dtTable.Rows(0).Item("numState")) Then
                    If Not ddlBillState.Items.FindByValue(dtTable.Rows(0).Item("numState")) Is Nothing Then
                        ddlBillState.SelectedIndex = -1 ' reset selected index
                        ddlBillState.Items.FindByValue(dtTable.Rows(0).Item("numState")).Selected = True
                    End If
                End If
                txtPostal.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcPostalCode")), "", dtTable.Rows(0).Item("vcPostalCode"))

                If CCommon.ToBool(dtTable.Rows(0).Item("bitAltContact")) Then
                    rbAddressContactAlt.Checked = True
                    txtAddressContactAlt.Text = CCommon.ToString(dtTable.Rows(0).Item("vcAltContact"))
                ElseIf Not ddlAddressContact.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0).Item("numContact"))) Is Nothing Then
                    ddlAddressContact.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0).Item("numContact"))).Selected = True
                End If
            End If

            BindName()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub OppJournal(ByVal lngOppID As Long)
        Try
            Dim objOppBizDocs As New OppBizDocs
            Dim lintAuthorizativeBizDocsId As Integer
            Dim lintOpportunityType As Integer

            objOppBizDocs.OppId = lngOppID
            objOppBizDocs.DomainID = Session("DomainID")
            lintOpportunityType = objOppBizDocs.GetOpportunityType()

            If lintOpportunityType = 1 Then
                lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()

                Dim dtTable As DataTable
                Dim objBizDocs As New OppBizDocs
                objBizDocs.OppId = lngOppID
                objBizDocs.DomainID = Session("DomainID")
                objBizDocs.UserCntID = Session("UserContactID")
                objBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                Dim dsBizDocs As DataSet = objBizDocs.GetBizDocsByOOpId

                If Not dsBizDocs Is Nothing AndAlso dsBizDocs.Tables.Count > 1 Then
                    dtTable = dsBizDocs.Tables(1)
                End If

                If dtTable.Rows.Count > 0 Then
                    For Each dr As DataRow In dtTable.Rows
                        If dr("bitAuthoritativeBizDocs") = 1 AndAlso dr("tintDeferred") = 0 Then
                            Dim JournalId As Long
                            Dim OppBizDocID As Long = dr("numOppBizDocsId")
                            Dim BizDocId As Long = dr("numBizDocId")

                            Dim dtOppBiDocItems As DataTable

                            If lintAuthorizativeBizDocsId = BizDocId Then
                                '' Authorizative Accounting BizDocs
                                ''--------------------------------------------------------------------------''
                                Dim ds As New DataSet
                                objOppBizDocs.OppBizDocId = OppBizDocID
                                objOppBizDocs.UserCntID = Session("UserContactID")

                                ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                                dtOppBiDocItems = ds.Tables(0)

                                Dim objCalculateDealAmount As New CalculateDealAmount
                                objCalculateDealAmount.CalculateDealAmount(lngOppID, OppBizDocID, lintOpportunityType, Session("DomainID"), dtOppBiDocItems)

                                'Get existing journal id and delete old and insert new journal entries
                                JournalId = objOppBizDocs.GetJournalIdForAuthorizativeBizDocs
                                'When From COA someone deleted journals then we need to create new journal header entry
                                'If JournalId = 0 Then
                                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                    JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, CDate(dr("dtFromDate")), JournalId, Description:=ds.Tables(1).Rows(0).Item("vcBizDocID"))
                                    'End If

                                    Dim objJournalEntries As New JournalEntry
                                    If lintOpportunityType = 1 Then
                                        If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                                            objJournalEntries.SaveJournalEntriesSales(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, dr("numDivisionId"), ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), DiscAcntType:=0, lngShippingMethod:=dr("numShipVia"))
                                        Else
                                            objJournalEntries.SaveJournalEntriesSalesNew(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, dr("numDivisionId"), ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), DiscAcntType:=0, lngShippingMethod:=dr("numShipVia"))
                                        End If
                                    Else
                                        If CCommon.ToBool(ds.Tables(1).Rows(0).Item("bitPPVariance")) Then
                                            objJournalEntries.SaveJournalEntriesPurchaseVariance(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, dr("numDivisionId"), OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), ds.Tables(1).Rows(0).Item("fltExchangeRateBizDoc"), DiscAcntType:=0, lngShippingMethod:=dr("numShipVia"), vcBaseCurrency:=ds.Tables(1).Rows(0).Item("vcBaseCurrency"), vcForeignCurrency:=ds.Tables(1).Rows(0).Item("vcForeignCurrency"))
                                        Else
                                            objJournalEntries.SaveJournalEntriesPurchase(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, dr("numDivisionId"), OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), DiscAcntType:=0, lngShippingMethod:=dr("numShipVia"))
                                        End If
                                    End If

                                    objTransactionScope.Complete()
                                End Using
                            End If
                        End If
                    Next
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub OrganizationChanged(ByVal isUpdateAddress As Boolean)
        Try
            If CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                Dim iDivisionId As Integer = radCmbCompany.SelectedValue
                Dim oCOpportunities As New COpportunities
                oCOpportunities.DomainID = CType((Session("DomainID")), Long)
                Dim dt As DataTable = oCOpportunities.GetContactDetails(CInt(Session("UserID")), CInt(iDivisionId))

                ddlContact.Items.Clear()
                ddlContact.DataSource = dt
                ddlContact.DataTextField = "contactName"
                ddlContact.DataValueField = "numContactId"
                ddlContact.DataBind()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    Dim listItem As ListItem
                    For Each dr As DataRow In dt.Rows
                        listItem = New ListItem()
                        listItem.Text = CCommon.ToString(dr("contactName"))
                        listItem.Value = CCommon.ToString(dr("numContactId"))
                        If CCommon.ToBool(dr("bitPrimaryContact")) Then
                            listItem.Selected = True
                        End If
                        ddlAddressContact.Items.Add(listItem)
                    Next
                End If

                BindAddressName()

                If isUpdateAddress Then
                    Me._fillAddress()
                End If
            End If
        Catch ex As Exception
            Throw
        End Try

    End Sub

    Protected Sub ddlProject_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            If CCommon.ToLong(ddlProject.SelectedValue) > 0 Then
                Dim projectAddressID As Long = 0

                Dim objProject As New Project
                objProject.DomainID = CCommon.ToLong(Session("DomainID"))
                objProject.ProjectID = CCommon.ToLong(ddlProject.SelectedValue)

                Dim iDivisionId As Integer = CCommon.ToLong(objProject.GetFieldValue("numDivisionID"))
                txtName.Text = CCommon.ToString(objProject.GetFieldValue("vcCompanyName"))


                Dim oCOpportunities As New COpportunities
                oCOpportunities.DomainID = CType((Session("DomainID")), Long)
                Dim dt As DataTable = oCOpportunities.GetContactDetails(CInt(Session("UserID")), CInt(iDivisionId))

                ddlContact.Items.Clear()
                ddlContact.DataSource = dt
                ddlContact.DataTextField = "contactName"
                ddlContact.DataValueField = "numContactId"
                ddlContact.DataBind()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    Dim listItem As ListItem
                    For Each dr As DataRow In dt.Rows
                        listItem = New ListItem()
                        listItem.Text = CCommon.ToString(dr("contactName"))
                        listItem.Value = CCommon.ToString(dr("numContactId"))
                        If CCommon.ToBool(dr("bitPrimaryContact")) Then
                            listItem.Selected = True
                        End If
                        ddlAddressContact.Items.Add(listItem)
                    Next
                End If

                If Not ddlContact.Items.FindByValue(objProject.GetFieldValue("numIntPrjMgr")) Is Nothing Then
                    ddlContact.Items.FindByValue(objProject.GetFieldValue("numIntPrjMgr")).Selected = True
                End If


                projectAddressID = CCommon.ToLong(objProject.GetFieldValue("numAddressID"))
                BindAddressByAddressID(projectAddressID)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class