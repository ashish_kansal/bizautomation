'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports System.Text.RegularExpressions
Imports System.Web.Script.Serialization
Imports ConsumedByCode.SignatureToImage
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Workflow
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Contacts

Namespace BACRM.UserInterface.Opportunities
    Public Class frmAmtPaid
        Inherits BACRMPage

        Dim objOppBizDocs As OppBizDocs
        Dim ResponseMessage As String

        Dim lngOppBizDocID, lngOppID, lngDivID, lngPaymentId, IsNewOrder, IsPOS, lngRecentlyCreatedBizDocId As Long
        Dim fltExchangeRateCreatedBizDoc As Double
        Dim lngDepositeID, lngTransactionHistoryID As Long
        Dim ReturnTransactionID As String = ""
        Dim strBizDocIds As String = ""
        Dim decNewOrderIncludeTaxAmount As Decimal = 0
        Dim PaymentDate As String = ""
        Dim PaymentTime As String = ""
        Dim PaymentReceiver As String = ""
        Private listTempSelectedInvoices As System.Collections.Generic.List(Of SelectedInvoice)

        Public Property UserSelectedInvoices As String
            Get
                Return CCommon.ToString(hdnSelectedInvoices.Value)
            End Get
            Set(value As String)
                hdnSelectedInvoices.Value = value
            End Set
        End Property

        Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            Try
                CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                lngDivID = CCommon.ToLong(GetQueryStringVal("DivId"))
                lngOppBizDocID = CCommon.ToLong(GetQueryStringVal("OppBizId"))
                lngOppID = CCommon.ToLong(GetQueryStringVal("OppId"))
                lngDepositeID = CCommon.ToLong(GetQueryStringVal("DepositeID"))
                lngPaymentId = CCommon.ToLong(GetQueryStringVal("PaymentId"))
                IsNewOrder = CCommon.ToLong(GetQueryStringVal("IsNewOrder"))
                IsPOS = CCommon.ToLong(GetQueryStringVal("IsPOS"))
                lblAuthorizeCharge.Text = Convert.ToString(Session("numAuthorizePercentage"))
                decNewOrderIncludeTaxAmount = CCommon.ToDecimal(GetQueryStringVal("totalAmount"))
                hdnOppType.Value = 1 'This page will be only used for Sales Invoice payment, not for purchase bizdocs
                'Following two parameters will be avail from frmOpenBizDocs.aspx
                'CurrentPageIndex = CCommon.ToInteger(GetQueryStringVal("Page"))
                'shrtOppType = CCommon.ToInteger(GetQueryStringVal("OppType"))
                'strBizDocID = GetQueryStringVal("BizDocs")

                PaymentDate = CCommon.ToString(GetQueryStringVal("PaymentDate"))
                PaymentTime = CCommon.ToString(GetQueryStringVal("PaymentTime"))
                PaymentReceiver = CCommon.ToString(GetQueryStringVal("PaymentReciver"))
                lblreceivedonby.Text = PaymentDate + ", " + PaymentTime + ", " + PaymentReceiver

                If CCommon.ToLong(Session("DomainID")) = 0 Then
                    Response.Redirect("../LogOff.htm")
                End If

                'btnFClose.Visible = IIf(CCommon.ToShort(GetQueryStringVal("Popup")) = 1, True, False)

                GetUserRightsForPage(35, 112)

                If Not IsPostBack Then
                    txtCurrrentPage.Text = 1

                    If lngDivID = 0 And (GetQueryStringVal("CntID") <> "" Or GetQueryStringVal("DivID") <> "" Or GetQueryStringVal("ProID") <> "" Or GetQueryStringVal("OpID") <> "" Or GetQueryStringVal("CaseID") <> "") Then
                        If objCommon Is Nothing Then objCommon = New CCommon
                        objCommon.UserCntID = Session("UserContactID")
                        If GetQueryStringVal("CntID") <> "" Then
                            objCommon.ContactID = CCommon.ToLong(GetQueryStringVal("CntID"))
                            objCommon.charModule = "C"
                        ElseIf GetQueryStringVal("DivID") <> "" Then
                            objCommon.DivisionID = CCommon.ToLong(GetQueryStringVal("DivID"))
                            objCommon.charModule = "D"
                        ElseIf GetQueryStringVal("ProID") <> "" Then
                            objCommon.ProID = CCommon.ToLong(GetQueryStringVal("ProID"))
                            objCommon.charModule = "P"
                        ElseIf GetQueryStringVal("OpID") <> "" Then
                            objCommon.OppID = CCommon.ToLong(GetQueryStringVal("OpID"))
                            objCommon.charModule = "O"
                        ElseIf GetQueryStringVal("CaseID") <> "" Then
                            objCommon.CaseID = CCommon.ToLong(GetQueryStringVal("CaseID"))
                            objCommon.charModule = "S"
                        End If

                        objCommon.GetCompanySpecificValues1()
                        lngDivID = objCommon.DivisionID
                    End If

                    ''  txtAmount.Text = GetQueryStringVal("Amt")
                    '' LoadDepositDropDown()
                    objCommon.sb_FillComboFromDBwithSel(ddlPayment, 31, Session("DomainID"))

                    FillDepositTocombo()
                    BindCardYear()
                    BindMultiCurrency()
                    'LoadBizDocDetail()
                    'intialize default values
                    calDate.SelectedDate = Date.Now
                    ddlPayment.SelectedValue = 5 'checks

                    If lngDivID > 0 Then
                        radCmbCompany.SelectedValue = lngDivID
                        objCommon.DivisionID = lngDivID
                        radCmbCompany.Text = objCommon.GetCompanyName()
                        FillContact(ddlContact)
                        BindCreditgrid()

                        BindDivisionInformation()
                    End If


                    'trContractAmount.Visible = False 'Deffered income Code removed by chintan, reason: we donot need deffered incomme whic is replaced by new bizdoc breakdown

                    If lngDepositeID > 0 Then
                        LoadSavedDetails()
                        BindCreditgrid()
                        btnSaveClose.Visible = False
                        btnSaveCloseWindow.Visible = False
                        btnCreditCard.Visible = False
                        ddlPayment.Enabled = False
                    Else
                        btnDelete.Visible = False
                    End If



                    ValidateBizDocApproval()
                    'lblAmountLabel.Text = IIf(OppType.Value = "1", "Amount Received", "Amount Paid")

                    If CCommon.ToInteger(GetQueryStringVal("swipe")) = 1 Then
                        ddlPayment.ClearSelection()
                        ddlPayment.Items.FindByValue("1").Selected = True

                        BindCardType()

                        pnlCreditCardForm.Visible = True

                        txtSwipe.Focus()
                        trSignature.Visible = True

                        btnSaveClose.Text = "Pay & Save Order"
                        btnSaveCloseWindow.Visible = False
                        btnSaveClose.CssClass = "Button_POS"
                        'btnCancel.Visible = False
                    End If

                    If objCommon Is Nothing Then objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    Dim dtReceivePaymentTo As DataTable = objCommon.GetDomainSettingValue("tintReceivePaymentTo")

                    If Not dtReceivePaymentTo Is Nothing AndAlso dtReceivePaymentTo.Rows.Count > 0 Then
                        If CCommon.ToShort(dtReceivePaymentTo.Rows(0)("tintReceivePaymentTo")) = 1 Then
                            radDepositeTo.Checked = True
                            radUnderpositedFund.Checked = False

                            Dim dtReceivePaymentBankAccount As DataTable = objCommon.GetDomainSettingValue("numReceivePaymentBankAccount")

                            If Not dtReceivePaymentBankAccount Is Nothing AndAlso dtReceivePaymentBankAccount.Rows.Count > 0 Then
                                If ddlDepositTo.Items.FindByValue(CCommon.ToLong(dtReceivePaymentBankAccount.Rows(0)("numReceivePaymentBankAccount"))) IsNot Nothing Then
                                    ddlDepositTo.ClearSelection()
                                    ddlDepositTo.Items.FindByValue(CCommon.ToLong(dtReceivePaymentBankAccount.Rows(0)("numReceivePaymentBankAccount"))).Selected = True
                                    ddlDepositTo_SelectedIndexChanged(sender, e)
                                End If
                            Else
                                Throw New Exception("Not able to fetch receive payment default bank account global settings value.")
                                Exit Sub
                            End If
                        Else
                            radDepositeTo.Checked = False
                            radUnderpositedFund.Checked = True
                        End If
                    Else
                        Throw New Exception("Not able to fetch receive payment global settings value.")
                        Exit Sub
                    End If

                    PersistTable.Load(boolOnlyURL:=True)
                    If PersistTable.Count > 0 And lngDepositeID = 0 Then

                        If ddlDepositTo.Items.FindByValue(CCommon.ToString(PersistTable(ddlDepositTo.ID))) IsNot Nothing Then
                            ddlDepositTo.ClearSelection()
                            ddlDepositTo.Items.FindByValue(CCommon.ToString(PersistTable(ddlDepositTo.ID))).Selected = True
                            ddlDepositTo_SelectedIndexChanged(sender, e)
                        End If
                        radDepositeTo.Checked = PersistTable(radDepositeTo.ID)
                        radUnderpositedFund.Checked = PersistTable(radUnderpositedFund.ID)

                    End If

                    If CCommon.ToInteger(GetQueryStringVal("flag")) = 1 Then litMessage.Text = "<span style='color:#00b017;font-weight:bold'>Payment Received Sucessfully</span>"
                    If CCommon.ToInteger(GetQueryStringVal("flag")) = 2 Then litMessage.Text = "<span style='color:#00b017;font-weight:bold'>Credit card charged successfully</span>"
                    If CCommon.ToInteger(GetQueryStringVal("flag")) = 3 Then litMessage.Text = "<span style='color:#00b017;font-weight:bold'>Payment deleted Sucessfully</span>"
                    If CCommon.ToInteger(GetQueryStringVal("flag")) = 4 Then litMessage.Text = "<span style='color:#00b017;font-weight:bold'>Credit card authorized successfully</span>"
                    Dim objOpp As New OppotunitiesIP()
                    objOpp.OpportunityId = lngOppID 'lngOppID
                    objOpp.DomainID = Session("DomainID")
                    objOpp.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    objOpp.PublicFlag = 0
                    objOpp.UserCntID = Session("UserContactID")
                    objOpp.OpportunityDetails()
                    lblSalesOrderName.Text = objOpp.OpportunityName
                    If (decNewOrderIncludeTaxAmount > 0) Then
                        txtOrderAmount.Text = decNewOrderIncludeTaxAmount
                        hdnOrderAmount.Value = decNewOrderIncludeTaxAmount
                    Else
                        txtOrderAmount.Text = objOpp.Amount
                        hdnOrderAmount.Value = objOpp.Amount
                    End If

                    txtAmount.Text = objOpp.Amount
                    BindSalesTemplate(objOpp.OppType)
                    hdnNewOrderOpptype.Value = objOpp.OppType
                    BindDatagrid()
                    If (IsNewOrder = 1) Then

                        btnSaveClose.Visible = True
                        If (lngPaymentId <> 1) Then
                            pnlCreditCardForm.Visible = False
                        End If
                    End If

                    Dim m_aryRightsForUndepositedFund() As Integer = GetUserRightsForPage_Other(35, 150)
                    Dim m_aryRightsForBankDeposit() As Integer = GetUserRightsForPage_Other(35, 151)

                    If m_aryRightsForUndepositedFund(RIGHTSTYPE.VIEW) = 0 Then
                        radUnderpositedFund.Checked = False
                        trDeposite1.Visible = False
                    End If

                    If m_aryRightsForBankDeposit(RIGHTSTYPE.VIEW) = 0 Then
                        radDepositeTo.Checked = False
                        trDeposite.Visible = False
                    End If

                    Dim objPaymentGateway As New PaymentGateway
                    objPaymentGateway.DomainID = CCommon.ToLong(Session("DomainID"))
                    objPaymentGateway.SiteID = 0
                    Dim defaultPaymentateway As Integer = objPaymentGateway.GetDefaultGateway()

                    If defaultPaymentateway = PaymentGateway.PGateWay.CardConnect1 Or defaultPaymentateway = PaymentGateway.PGateWay.CardConnect2 Then
                        Dim dtPaymentGateway As DataTable = objPaymentGateway.GetCardConnectGateway()

                        If Not dtPaymentGateway Is Nothing AndAlso dtPaymentGateway.Rows.Count > 1 Then
                            ddlPaymentGateWay.DataSource = dtPaymentGateway
                            ddlPaymentGateWay.DataValueField = "intPaymentGateWay"
                            ddlPaymentGateWay.DataValueField = "vcThirdFldValue"
                            ddlPaymentGateWay.DataBind()

                            trPaymentGateway.Visible = True
                        Else
                            trPaymentGateway.Visible = False
                        End If
                    End If
                End If

                btnSaveClose.Attributes.Add("onclick", "return Save(" & IIf(GetQueryStringVal("d") = "", 0, GetQueryStringVal("d")) & ")")
                txtAmount.Attributes.Add("onkeypress", "CheckNumber(1,event)")
                txtCCNo.Attributes.Add("onkeypress", "CheckNumber(2,event)")
                txtCVV2.Attributes.Add("onkeypress", "CheckNumber(2,event)")
                txtSwipe.Attributes.Add("onchange", "swipedCreditCard()")

                If (IsNewOrder = 1) Then
                    If lngPaymentId = 1 Then
                        btnSaveClose.Visible = False
                    Else
                        btnSaveClose.Visible = True
                    End If
                    NewOrderArea.Visible = True
                    tbl2.Visible = True
                    chkCreateInvoice.Checked = True
                    If (IsPOS = 1) Then
                        chkItemShipped.Checked = True
                        chkThermalReceipt.Checked = True
                    End If

                    If lngOppBizDocID > 0 Then
                        chkCreateInvoice.Checked = False
                        txtOrderAmount.Text = 0
                        chkCreateInvoice.Enabled = False
                        txtOrderAmount.Enabled = False
                    End If
                Else
                    NewOrderArea.Visible = False
                    chkCreateInvoice.Checked = False
                    txtOrderAmount.Text = 0
                End If

                If CCommon.ToLong(GetQueryStringVal("IsNewOrderPayment")) = 1 AndAlso CCommon.ToLong(GetQueryStringVal("NewOrderId")) > 0 Then
                    btnCloseAndOpenOrder.Visible = True
                    btnCloseAndOpenOrder.Attributes.Add("onclick", "return CloseAndOpenOrder(" & CCommon.ToLong(GetQueryStringVal("NewOrderId")) & ");")
                End If
            Catch sqlEx As SqlClient.SqlException
                Dim errorMessages As New System.Text.StringBuilder

                Dim i As Integer = 0
                Do While (i < sqlEx.Errors.Count)
                    errorMessages.Append(("dgOpportunity_ItemDataBound: Index #" _
                                    + (i + ("" & vbLf + ("Message: " _
                                    + (sqlEx.Errors(i).Message + ("" & vbLf + ("LineNumber: " _
                                    + (sqlEx.Errors(i).LineNumber + ("" & vbLf + ("Source: " _
                                    + (sqlEx.Errors(i).Source + ("" & vbLf + ("Procedure: " _
                                    + (sqlEx.Errors(i).Procedure + "" & vbLf)))))))))))))))
                    i = (i + 1)
                Loop

                ExceptionModule.ExceptionPublish(errorMessages.ToString(), Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(sqlEx)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Function CreateBizDoc(ByVal OppType As Integer, ByVal BizDocTypeId As Integer) As Boolean
            Try
                If CCommon.ToBool(Session("IsMinUnitPriceRule")) Then
                    If CheckIfApprovalRequired() Then
                        Throw New Exception("You can not add BizDoc because approval of unit price(s) is required.")
                    End If
                End If

                Dim objOppBizDocs As New OppBizDocs
                Dim OppBizDocID, lngDivId, JournalId As Long
                Dim lngCntID As Long = 0
                objOppBizDocs.OppId = lngOppID
                objOppBizDocs.OppType = OppType

                objOppBizDocs.UserCntID = Session("UserContactID")
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.vcPONo = "" 'txtPO.Text
                objOppBizDocs.vcComments = "" 'txtComments.Text
                'objOppBizDocs.ShipCost = 0 'IIf(IsNumeric(txtShipCost.Text), txtShipCost.Text, 0)
                'Add Billing Term from selected SalesTemplate(Sales Order)'s first Autoritative bizdoc's Billing term


                Dim dtDetails, dtBillingTerms As DataTable
                Dim objPageLayout As New CPageLayout
                objPageLayout.OpportunityId = lngOppID
                objPageLayout.DomainID = Session("DomainID")
                dtDetails = objPageLayout.OpportunityDetails.Tables(0)
                lngDivId = dtDetails.Rows(0).Item("numDivisionID")
                lngCntID = dtDetails.Rows(0).Item("numContactID")
                Dim objAdmin As New CAdmin
                objAdmin.DivisionID = lngDivId

                Dim lintAuthorizativeBizDocsId As Long
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.OppId = lngOppID
                objOppBizDocs.ShipCompany = CCommon.ToLong(dtDetails.Rows(0).Item("intUsedShippingCompany"))
                lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()

                Dim objJournal As New JournalEntry
                If lintAuthorizativeBizDocsId = BizDocTypeId Then 'save bizdoc only if selected company's AR and AP account is mapped

                    If objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), lngDivId) = 0 Then
                        Throw New Exception("Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip"" To Save BizDoc")
                    End If
                End If
                If OppType = 2 Then
                    'Accounting validation->Do not allow to save PO untill Default COGs account is mapped
                    If ChartOfAccounting.GetDefaultAccount("CG", Session("DomainID")) = 0 Then
                        Throw New Exception("Please Set Default COGs account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save")
                    End If
                End If

                If OppType = 2 Then
                    If ChartOfAccounting.GetDefaultAccount("PC", Session("DomainID")) = 0 Then
                        Throw New Exception("Please Set Default Purchase Clearing account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save")
                    End If

                    If ChartOfAccounting.GetDefaultAccount("PV", Session("DomainID")) = 0 Then
                        Throw New Exception("Please Set Default Purchase Price Variance account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save")
                    End If
                End If

                objOppBizDocs.FromDate = DateTime.UtcNow
                objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                objOppBizDocs.BizDocId = BizDocTypeId
                objOppBizDocs.BizDocTemplateID = ddlOrderTemplates.SelectedValue

                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")
                objCommon.Mode = 33
                objCommon.Str = BizDocTypeId
                objOppBizDocs.SequenceId = objCommon.GetSingleFieldValue()

                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")
                objCommon.Mode = 34
                objCommon.Str = lngOppID
                objOppBizDocs.RefOrderNo = objCommon.GetSingleFieldValue()

                objOppBizDocs.bitPartialShipment = True

                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    If IsNewOrder = 1 AndAlso chkCreateInvoice.Checked Then
                        objOppBizDocs.DomainID = CCommon.ToLong(Session("DomainID"))
                        objOppBizDocs.OppId = lngOppID
                        Dim dtInvoices As DataTable = objOppBizDocs.GetUnPaidInvoices()

                        If Not dtInvoices Is Nothing AndAlso dtInvoices.Rows.Count > 0 Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "InvoiceAlreadyExists", "InvoiceAlreadyExists(" & lngOppID & "," & CCommon.ToLong(dtInvoices.Rows(0)("numOppBizDocsId")) & "," & CCommon.ToLong(dtInvoices.Rows(0)("numDivisionId")) & ")", True)
                            Return False
                            Exit Function
                        End If
                    End If

                    OppBizDocID = objOppBizDocs.SaveBizDoc()
                    If BizDocTypeId = 287 Then
                        lngRecentlyCreatedBizDocId = OppBizDocID
                        fltExchangeRateCreatedBizDoc = CCommon.ToDouble(dtDetails.Rows(0).Item("fltExchangeRate"))
                    End If

                    lngOppBizDocID = OppBizDocID

                    If OppBizDocID = 0 Then
                        Throw New Exception("A BizDoc by the same name is already created. Please select another BizDoc !")
                    End If

                    'Create Journals only for authoritative bizdocs
                    '-------------------------------------------------
                    If lintAuthorizativeBizDocsId = BizDocTypeId Or BizDocTypeId = 304 Then ' 304 - Deferred Income
                        Dim ds As New DataSet
                        Dim dtOppBiDocItems As DataTable

                        objOppBizDocs.OppBizDocId = OppBizDocID
                        ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                        dtOppBiDocItems = ds.Tables(0)

                        Dim objCalculateDealAmount As New CalculateDealAmount

                        objCalculateDealAmount.CalculateDealAmount(lngOppID, OppBizDocID, OppType, Session("DomainID"), dtOppBiDocItems)

                        ''---------------------------------------------------------------------------------
                        JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, objOppBizDocs.FromDate, Description:=ds.Tables(1).Rows(0).Item("vcBizDocID"))
                        Dim objJournalEntries As New JournalEntry

                        If OppType = 2 Then
                            If CCommon.ToBool(ds.Tables(1).Rows(0).Item("bitPPVariance")) Then
                                objJournalEntries.SaveJournalEntriesPurchaseVariance(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), ds.Tables(1).Rows(0).Item("fltExchangeRateBizDoc"), vcBaseCurrency:=ds.Tables(1).Rows(0).Item("vcBaseCurrency"), vcForeignCurrency:=ds.Tables(1).Rows(0).Item("vcForeignCurrency"))
                            Else
                                objJournalEntries.SaveJournalEntriesPurchase(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"))
                            End If
                        ElseIf OppType = 1 Then
                            If BizDocTypeId = 304 AndAlso CCommon.ToBool(Session("IsEnableDeferredIncome")) Then 'Deferred Revenue
                                objJournalEntries.SaveJournalEntriesDeferredIncome(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), objCalculateDealAmount.GrandTotal)
                            Else
                                If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                                    objJournalEntries.SaveJournalEntriesSales(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), 0, bitInvoiceAgainstDeferredIncomeBizDoc:=objOppBizDocs.bitInvoiceForDeferred)
                                Else
                                    objJournalEntries.SaveJournalEntriesSalesNew(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), 0, bitInvoiceAgainstDeferredIncomeBizDoc:=objOppBizDocs.bitInvoiceForDeferred)
                                End If

                                If Session("AutolinkUnappliedPayment") Then
                                    Dim objSalesFulfillment As New SalesFulfillmentWorkflow
                                    objSalesFulfillment.DomainID = Session("DomainID")
                                    objSalesFulfillment.UserCntID = Session("UserContactID")
                                    objSalesFulfillment.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                                    objSalesFulfillment.OppId = lngOppID
                                    objSalesFulfillment.OppBizDocId = OppBizDocID

                                    objSalesFulfillment.AutoLinkUnappliedPayment()
                                End If
                            End If
                        End If
                    End If



                    objTransactionScope.Complete()
                End Using

                ''Added By Sachin Sadhu||Date:1stMay2014
                ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                ''          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = OppBizDocID
                objWfA.SaveWFBizDocQueue()
                'end of code

                '------ Send BizDoc Alerts - Notify record owners, their supervisors, and your trading partners when a BizDoc is created, modified, or approved.
                Dim objAlert As New CAlerts
                objAlert.SendBizDocAlerts(lngOppID, OppBizDocID, BizDocTypeId, Session("DomainID"), CAlerts.enmBizDoc.IsCreated)

                Dim objAutomatonRule As New AutomatonRule
                objAutomatonRule.ExecuteAutomationRule(49, OppBizDocID, 1)


                Return True
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Function CheckIfApprovalRequired() As Boolean
            Try
                Dim objOpportunity As New MOpportunity
                objOpportunity.OpportunityId = lngOppID
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.UserCntID = Session("UserContactID")
                objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                Dim dsTemp As DataSet = objOpportunity.ItemsByOppId

                If Not dsTemp Is Nothing AndAlso dsTemp.Tables.Count > 0 AndAlso dsTemp.Tables(0).Rows.Count > 0 Then
                    Dim results = From myRow In dsTemp.Tables(0).AsEnumerable()
                                  Where CCommon.ToBool(myRow.Field(Of Boolean)("bitItemPriceApprovalRequired")) = True
                                  Select myRow
                    If results.Count > 0 Then
                        Return True
                    Else
                        Return False
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub BindSalesTemplate(ByVal OppType As Integer)
            Try
                Dim objOppBizDoc As New OppBizDocs
                objOppBizDoc.DomainID = Session("DomainID")
                objOppBizDoc.BizDocId = 287
                objOppBizDoc.OppType = OppType
                objOppBizDoc.byteMode = 0

                Dim dtBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()

                ddlOrderTemplates.DataSource = dtBizDocTemplate
                ddlOrderTemplates.DataTextField = "vcTemplateName"
                ddlOrderTemplates.DataValueField = "numBizDocTempID"
                ddlOrderTemplates.DataBind()

                'ddlOrderTemplates.Items.Insert(0, New ListItem("--Select--", 0))

                objOppBizDoc.byteMode = 1


            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'When bizdoc approval is pending or declined then don't allow user to make payments
        Sub ValidateBizDocApproval()
            Try
                Dim dtOppBiDocDtl As DataTable
                If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs


                objOppBizDocs.OppBizDocId = lngOppBizDocID
                objOppBizDocs.OppId = lngOppID
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.UserCntID = Session("UserContactID")
                objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl
                If dtOppBiDocDtl.Rows.Count = 0 Then Exit Sub
                If CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("Pending")) > 0 Or CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("Declined")) > 0 Then
                    litMessage.Text = "You can not pay this BizDoc because approval has either been declined or is still pending: "
                    hplApproval.Visible = True
                    hplApproval.Attributes.Add("onclick", "return openApp(" & lngOppBizDocID & ",'B'," & lngOppID & ",'" & IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcBizDocID")), "", dtOppBiDocDtl.Rows(0).Item("vcBizDocID")) & "');")
                    btnSaveClose.Visible = False
                    btnSaveCloseWindow.Visible = False
                End If

                If dtOppBiDocDtl.Rows(0).Item("tintDeferred") = 1 Then
                    litMessage.Text = "You can not pay to Deferred BizDoc"
                    btnSaveClose.Visible = False
                    btnSaveCloseWindow.Visible = False
                End If

                lblBalanceAmt.Text = ReturnMoney(CCommon.GetDecimalFormat(dtOppBiDocDtl.Rows(0).Item("monPAmount")) - CCommon.GetDecimalFormat(dtOppBiDocDtl.Rows(0).Item("monAmountPaid")))

                txtAmount.Text = CCommon.GetDecimalFormat(dtOppBiDocDtl.Rows(0).Item("monPAmount")) - CCommon.GetDecimalFormat(dtOppBiDocDtl.Rows(0).Item("monAmountPaid"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub BindMultiCurrency()
            Try
                If Session("MultiCurrency") = True Then
                    trMultiCurrency.Visible = True
                    Dim objCurrency As New CurrencyRates
                    objCurrency.DomainID = Session("DomainID")
                    objCurrency.GetAll = 0
                    ddlCurrency.DataSource = objCurrency.GetCurrencyWithRates()
                    ddlCurrency.DataTextField = "vcCurrencyDesc"
                    ddlCurrency.DataValueField = "numCurrencyID"
                    ddlCurrency.DataBind()
                    'ddlCurrency.Items.Insert(0, "--Select One--")
                    'ddlCurrency.Items.FindByText("--Select One--").Value = "0"
                    If Not ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")) Is Nothing Then
                        ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")).Selected = True
                        ddlCurrency_SelectedIndexChanged()
                    End If
                Else
                    trMultiCurrency.Visible = False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Sub LoadContractsInfo()
        '    Try
        '        If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs

        '        objOppBizDocs.UserCntID = Session("UserContactId")
        '        objOppBizDocs.DomainID = Session("DomainId")
        '        objOppBizDocs.OppId = lngOppID
        '        ddlContracts.DataSource = objOppBizDocs.GetContractDdlList()
        '        ddlContracts.DataTextField = "vcContractName"
        '        ddlContracts.DataValueField = "numcontractId"
        '        ddlContracts.DataBind()
        '        ddlContracts.Items.Insert(0, "--Select One--")
        '        ddlContracts.Items.FindByText("--Select One--").Value = "0"
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        Private Sub BindCardYear()
            Try
                For i As Integer = Date.Now.Year To Date.Now.Year + 10
                    ddlYear.Items.Add(New ListItem(i.ToString("00"), i.ToString.Substring(2, 2)))
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub LoadSavedCreditCardInfo()
            Try
                Dim objOppInvoice As New OppInvoice
                Dim dsCCInfo As DataSet
                objOppInvoice.DomainID = Session("DomainID")
                objOppInvoice.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                'objOppInvoice.UserCntID =
                objOppInvoice.numCCInfoID = ddlCards.SelectedValue
                objOppInvoice.IsDefault = 1
                objOppInvoice.bitflag = 1
                dsCCInfo = objOppInvoice.GetCustomerCreditCardInfo()


                If dsCCInfo.Tables(0).Rows.Count > 0 Then
                    Dim objEncryption As New QueryStringValues
                    txtCHName.Text = objEncryption.Decrypt(dsCCInfo.Tables(0).Rows(0)("vcCardHolder").ToString())
                    Dim strCCNo As String
                    lblAuthStatus.Text = ""
                    lblAuthRequestDate.Text = ""
                    lblStatusTimeRequest.Text = ""
                    lblAmount.Text = ""
                    lblAuthStatus.Text = ""
                    lblAuthCode.Text = ""

                    strCCNo = objEncryption.Decrypt(dsCCInfo.Tables(0).Rows(0)("vcCreditCardNo").ToString())

                    Session("CCNO") = strCCNo

                    If strCCNo.Length > 4 Then
                        strCCNo = "************" & strCCNo.Substring(strCCNo.Length - 4, 4)
                    Else
                        strCCNo = ""
                    End If
                    txtCCNo.Text = strCCNo

                    GetAuthorizedTrasactionDetails()

                    txtCVV2.Text = objEncryption.Decrypt(dsCCInfo.Tables(0).Rows(0)("vcCVV2").ToString())
                    ddlCardType.SelectedIndex = -1
                    ddlMonth.SelectedIndex = -1
                    ddlYear.SelectedIndex = -1
                    If Not ddlCardType.Items.FindByValue(dsCCInfo.Tables(0).Rows(0)("numCardTypeID")) Is Nothing Then
                        ddlCardType.Items.FindByValue(dsCCInfo.Tables(0).Rows(0)("numCardTypeID").ToString()).Selected = True
                    End If
                    If ddlCardType.SelectedValue > 0 Then
                        ddlCardType_SelectedIndexChanged(New Object, Nothing)
                    End If
                    If Not ddlMonth.Items.FindByValue(CInt(dsCCInfo.Tables(0).Rows(0)("tintValidMonth")).ToString("00")) Is Nothing Then
                        ddlMonth.Items.FindByValue(CInt(dsCCInfo.Tables(0).Rows(0)("tintValidMonth")).ToString("00")).Selected = True
                    End If
                    If Not ddlYear.Items.FindByValue(dsCCInfo.Tables(0).Rows(0)("intValidYear").ToString()) Is Nothing Then
                        ddlYear.Items.FindByValue(dsCCInfo.Tables(0).Rows(0)("intValidYear").ToString()).Selected = True
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub GetAuthorizedTrasactionDetails()
            Dim objTransactionHistory As New TransactionHistory
            objTransactionHistory.TransHistoryID = 0
            objTransactionHistory.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
            objTransactionHistory.ContactID = CCommon.ToLong(ddlContact.SelectedValue)
            objTransactionHistory.DomainID = Session("DomainID")
            Dim dt As DataTable = objTransactionHistory.GetAuthrizedTrasaction(0, 2)

            If (Not dt Is Nothing AndAlso dt.Rows.Count > 0) Then
                lblAuthRequestDate.Text = FormattedDateFromDate(dt.Rows(0)("dtCreatedDate"), Session("DateFormat"))
                Dim totalDays As Long
                totalDays = Microsoft.VisualBasic.DateDiff(Microsoft.VisualBasic.DateInterval.Day, Convert.ToDateTime(dt.Rows(0)("dtCreatedDate")), System.DateTime.Now)
                If (totalDays > 7) Then
                    lblAuthStatus.Text = "Expired"
                    lblAuthStatus.ForeColor = Color.Red
                Else
                    lblAuthStatus.Text = "Approved"
                    lblAuthStatus.ForeColor = Color.Green
                End If
                lblStatusTimeRequest.Text = "Approved"
                lblStatusTimeRequest.ForeColor = Color.Green
                lblAuthCode.Text = dt.Rows(0)("vcTransactionID")
                lblAmount.Text = dt.Rows(0)("monAuthorizedAmt")
            Else
                lblAuthStatus.Text = ""
                lblAuthRequestDate.Text = ""
                lblStatusTimeRequest.Text = ""
                lblAmount.Text = ""
                lblAuthStatus.Text = ""
                lblAuthCode.Text = ""
            End If
        End Sub

        Private Sub LoadSavedDetails()
            Try
                Dim dsDeposite As DataSet
                Dim objMakeDeposit As New MakeDeposit
                objMakeDeposit.DepositId = lngDepositeID
                objMakeDeposit.DomainID = Session("DomainID")
                dsDeposite = objMakeDeposit.GetDepositDetails()

                hdnDepositeID.Value = lngDepositeID
                If dsDeposite.Tables(0).Rows.Count > 0 Then
                    If CCommon.ToLong(dsDeposite.Tables(0).Rows(0).Item("numDivisionID")) > 0 Then
                        radCmbCompany.SelectedValue = CCommon.ToLong(dsDeposite.Tables(0).Rows(0).Item("numDivisionID"))
                        radCmbCompany.Text = CCommon.ToString(dsDeposite.Tables(0).Rows(0).Item("vcCompanyName"))
                    End If

                    hdnDepositePage.Value = CCommon.ToShort(dsDeposite.Tables(0).Rows(0).Item("tintDepositePage"))

                    If CCommon.ToShort(dsDeposite.Tables(0).Rows(0).Item("tintDepositePage")) <> 3 Then 'If not Credit Memo

                        txtAmount.Text = String.Format("{0:###0.00}", CCommon.ToDecimal(dsDeposite.Tables(0).Rows(0).Item("monDepositAmount")))
                        txtReference.Text = CCommon.ToString(dsDeposite.Tables(0).Rows(0).Item("vcReference"))
                        txtMemo.Text = CCommon.ToString(dsDeposite.Tables(0).Rows(0).Item("vcMemo"))

                        If Not IsDBNull(dsDeposite.Tables(0).Rows(0).Item("dtDepositDate")) Then
                            calDate.SelectedDate = dsDeposite.Tables(0).Rows(0).Item("dtOrigDepositDate")
                        End If


                        If CCommon.ToShort(dsDeposite.Tables(0).Rows(0).Item("tintDepositeToType")) = 1 Then
                            radDepositeTo.Checked = True
                            radUnderpositedFund.Checked = False
                            If Not IsDBNull(dsDeposite.Tables(0).Rows(0).Item("numChartAcntId")) Then
                                If Not ddlDepositTo.Items.FindByValue(dsDeposite.Tables(0).Rows(0).Item("numChartAcntId")) Is Nothing Then
                                    ddlDepositTo.ClearSelection()
                                    ddlDepositTo.Items.FindByValue(dsDeposite.Tables(0).Rows(0).Item("numChartAcntId")).Selected = True
                                    ddlDepositTo_SelectedIndexChanged()
                                End If
                            End If
                        ElseIf CCommon.ToShort(dsDeposite.Tables(0).Rows(0).Item("tintDepositeToType")) = 2 Then
                            radUnderpositedFund.Checked = True
                            radDepositeTo.Checked = False
                        End If

                        If Not IsDBNull(dsDeposite.Tables(0).Rows(0).Item("numPaymentMethod")) Then
                            If Not ddlPayment.Items.FindByValue(dsDeposite.Tables(0).Rows(0).Item("numPaymentMethod")) Is Nothing Then
                                ddlPayment.ClearSelection()
                                ddlPayment.Items.FindByValue(dsDeposite.Tables(0).Rows(0).Item("numPaymentMethod")).Selected = True
                            End If
                        End If
                        If CCommon.ToShort(dsDeposite.Tables(0).Rows(0).Item("tintDepositeToType")) = 2 Then
                            hdnIsDepositedToAccount.Value = CCommon.ToBool(dsDeposite.Tables(0).Rows(0).Item("bitDepositedToAcnt"))
                        End If

                        If Session("MultiCurrency") = True Then
                            If Not IsDBNull(dsDeposite.Tables(0).Rows(0).Item("numCurrencyID")) Then
                                If Not ddlCurrency.Items.FindByValue(dsDeposite.Tables(0).Rows(0).Item("numCurrencyID")) Is Nothing Then
                                    ddlCurrency.ClearSelection()
                                    ddlCurrency.Items.FindByValue(dsDeposite.Tables(0).Rows(0).Item("numCurrencyID")).Selected = True
                                    ddlCurrency_SelectedIndexChanged()
                                End If
                            End If
                            txtExchangeRate.Text = CCommon.ToDouble(dsDeposite.Tables(0).Rows(0).Item("fltExchangeRateReceivedPayment"))
                            lblBaseCurr.Text = CCommon.ToString(dsDeposite.Tables(0).Rows(0).Item("BaseCurrencySymbol")).Trim()
                            lblForeignCurr.Text = CCommon.ToString(dsDeposite.Tables(0).Rows(0).Item("varCurrSymbol")).Trim()

                        End If
                    End If

                    hdnRefundAmount.Value = CCommon.ToDecimal(dsDeposite.Tables(0).Rows(0).Item("monRefundAmount"))
                End If

                'Dim objOppInvoice As New OppInvoice
                'Dim dtOppBizDocsDetails As DataTable
                'Dim lintCount As Integer
                'Dim lobjReceivePayment As New ReceivePayment


                'If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                'objOppBizDocs.OppId = lngOppID
                'objOppBizDocs.DomainID = Session("DomainId")
                'objOppBizDocs.OppType = hdnOppType.Value
                ''lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()
                'objOppBizDocs.OppBizDocId = lngOppBizDocID

                ''lobjReceivePayment.BizDocsPaymentDetId = CCommon.ToLong(GetQueryStringVal("d"))
                ''lintCount = lobjReceivePayment.GetOpportunityBizDocsIntegratedToAcnt()

                'objOppInvoice.OppBizDocId = lngOppBizDocID
                'objOppInvoice.DomainID = Session("DomainId")
                'objOppInvoice.BizDocsPaymentDetId = CCommon.ToLong(GetQueryStringVal("d"))
                'dtOppBizDocsDetails = objOppInvoice.GetOpportunityBizDocsDetails()
                'If dtOppBizDocsDetails.Rows.Count > 0 Then
                '    If Not IsDBNull(dtOppBizDocsDetails.Rows(0).Item("numPaymentMethod")) Then
                '        If Not ddlPayment.Items.FindByValue(dtOppBizDocsDetails.Rows(0).Item("numPaymentMethod")) Is Nothing Then
                '            ddlPayment.Items.FindByValue(dtOppBizDocsDetails.Rows(0).Item("numPaymentMethod")).Selected = True
                '        End If
                '    End If
                '    If Not IsDBNull(dtOppBizDocsDetails.Rows(0).Item("monAmount")) Then
                '        txtAmount.Text = String.Format("{0:#,##0.00}", dtOppBizDocsDetails.Rows(0).Item("monAmount"))
                '        hdnAmountPaid.Value = dtOppBizDocsDetails.Rows(0).Item("monAmount")
                '        lblBalanceAmt.Text = CCommon.ToDecimal(txtAmount.Text) - CCommon.ToDecimal(hdnAmountPaid.Value)
                '    End If
                '    If Not IsDBNull(dtOppBizDocsDetails.Rows(0).Item("vcReference")) Then
                '        txtReference.Text = dtOppBizDocsDetails.Rows(0).Item("vcReference")
                '    End If
                '    If Not IsDBNull(dtOppBizDocsDetails.Rows(0).Item("vcMemo")) Then
                '        txtMemo.Text = dtOppBizDocsDetails.Rows(0).Item("vcMemo")
                '    End If

                '    'If Not IsDBNull(dtOppBizDocsDetails.Rows(0).Item("bitDeferredIncome")) Then
                '    '    If dtOppBizDocsDetails.Rows(0).Item("bitDeferredIncome") = True Then
                '    '        chkDeferredIncome.Checked = True
                '    '        If Not ddlDeferredIncomePeriod.Items.FindByValue(dtOppBizDocsDetails.Rows(0).Item("sintDeferredIncomePeriod")) Is Nothing Then
                '    '            ddlDeferredIncomePeriod.Items.FindByValue(dtOppBizDocsDetails.Rows(0).Item("sintDeferredIncomePeriod")).Selected = True
                '    '        End If
                '    '        If Not IsDBNull(dtOppBizDocsDetails.Rows(0).Item("dtDeferredIncomeStartDate")) Then
                '    '            calFrom.SelectedDate = dtOppBizDocsDetails.Rows(0).Item("dtDeferredIncomeStartDate")
                '    '        End If
                '    '    Else
                '    '        If Not IsDBNull(dtOppBizDocsDetails.Rows(0).Item("numContractId")) Then
                '    '            If Not ddlContracts.Items.FindByValue(dtOppBizDocsDetails.Rows(0).Item("numContractId")) Is Nothing Then
                '    '                ddlContracts.Items.FindByValue(dtOppBizDocsDetails.Rows(0).Item("numContractId")).Selected = True
                '    '                chkContractAmt.Checked = True
                '    '            End If
                '    '        End If
                '    '    End If
                '    'End If
                '    'If lintCount = 0 Then
                '    '    txtAmount.Enabled = True
                '    '    trDeferredIncome.Disabled = False
                '    '    trContractAmount.Disabled = False
                '    'Else
                '    '    txtAmount.Enabled = False
                '    '    trDeferredIncome.Disabled = True
                '    '    trContractAmount.Disabled = True
                '    'End If
                'End If
                ''  End If
                'Get Saved Credit Card info As per Permition

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Dim TransactionCharge As Double
        Dim TransactionChargeAccountID As Long
        Dim TransactionChargeBareBy As Short = 0 ' default by employer
        Dim UndepositedFundAccountID As Long
        Dim lngJournalID As Long
        Dim lngCustomerARAccount As Long
        Dim lngEmpPayRollAccount As Long
        Dim lngDefaultAPAccount As Long
        Dim AmountToPay As Decimal
        Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                Dim isInvoiceCreated As Boolean = True

                If ValidFormInput() Then
                    If (chkCreateInvoice.Checked = True) Then
                        isInvoiceCreated = CreateBizDoc(hdnNewOrderOpptype.Value, 287)
                    End If
                    If (chkItemShipped.Checked = True) Then
                        Dim objOpp As New COpportunities
                        objOpp.CreateFulfillmentBizDoc(Session("DomainID"), Session("UserContactID"), lngOppID)
                    End If

                    If isInvoiceCreated Then
                        SaveNewWithTransaction(False)
                    End If
                End If
            Catch ex As Exception
                If ex.Message = "NOT_ALLOWED" Then
                    litMessage.Text = "To split order items multiple bizdocs you must create all bizdocs with ""partial fulfilment"" checked!"
                ElseIf ex.Message = "NOT_ALLOWED_FulfillmentOrder" Then
                    litMessage.Text = "Only a Sales Order can create a Fulfillment Order"
                ElseIf ex.Message = "FY_CLOSED" Then
                    litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
                ElseIf ex.Message = "AlreadyInvoice" Then
                    litMessage.Text = "You can�t create a Invoice against a Fulfillment Order that already contains a Invoice"
                ElseIf ex.Message = "AlreadyPackingSlip" Then
                    litMessage.Text = "You can�t create a Packing-Slip against a Fulfillment Order that already contains a Packing-Slip"
                ElseIf ex.Message = "AlreadyInvoice_NOT_ALLOWED_FulfillmentOrder" Then
                    litMessage.Text = "You can�t create a fulfillment order against a sales order that already contains an Invoice or Packing-Slip"
                ElseIf ex.Message = "AlreadyFulfillmentOrder" Then
                    litMessage.Text = "Manually adding Invoices or Packing Slips to a Sales Order is not allowed after a Fulfillment Order (FO) is added. Your options are to edit the FO status to trigger creation of Invoices and/or Packing Slips, Delete the Fulfillment Order, or Create Invoices and/or Packing Slips from the Sales Fulfillment section"
                ElseIf ex.Message = "IM_BALANCE" Then
                    litMessage.Text = "The Credit and Debit amounts you�re entering must be of the same amount"
                ElseIf ex.Message = "FY_CLOSED" Then
                    litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
                ElseIf CCommon.ToString(ex.Message).ToLower().Contains("timeout expired") Then
                    litMessage.Text = "Server request timeout."
                ElseIf ex.Message.Contains("You can not add BizDoc because approval of unit price(s) is required.") Then
                    litMessage.Text = "You can not receive payment for order because approval of unit price(s) is required."
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Response.Write(ex)
                End If
            End Try
        End Sub

        Private Sub btnSaveCloseWindow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveCloseWindow.Click
            Try
                Dim isInvoiceCreated As Boolean = True

                If ValidFormInput() Then
                    If (chkCreateInvoice.Checked = True) Then
                        isInvoiceCreated = CreateBizDoc(hdnNewOrderOpptype.Value, 287)
                    End If

                    If (chkItemShipped.Checked = True) Then
                        Dim objOpp As New COpportunities
                        objOpp.CreateFulfillmentBizDoc(Session("DomainID"), Session("UserContactID"), lngOppID)
                    End If

                    If isInvoiceCreated Then
                        SaveNewWithTransaction(True)
                    End If
                End If
            Catch ex As Exception
                If ex.Message = "NOT_ALLOWED" Then
                    litMessage.Text = "To split order items multiple bizdocs you must create all bizdocs with ""partial fulfilment"" checked!"
                ElseIf ex.Message = "NOT_ALLOWED_FulfillmentOrder" Then
                    litMessage.Text = "Only a Sales Order can create a Fulfillment Order"
                ElseIf ex.Message = "FY_CLOSED" Then
                    litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
                ElseIf ex.Message = "AlreadyInvoice" Then
                    litMessage.Text = "You can�t create a Invoice against a Fulfillment Order that already contains a Invoice"
                ElseIf ex.Message = "AlreadyPackingSlip" Then
                    litMessage.Text = "You can�t create a Packing-Slip against a Fulfillment Order that already contains a Packing-Slip"
                ElseIf ex.Message = "AlreadyInvoice_NOT_ALLOWED_FulfillmentOrder" Then
                    litMessage.Text = "You can�t create a fulfillment order against a sales order that already contains an Invoice or Packing-Slip"
                ElseIf ex.Message = "AlreadyFulfillmentOrder" Then
                    litMessage.Text = "Manually adding Invoices or Packing Slips to a Sales Order is not allowed after a Fulfillment Order (FO) is added. Your options are to edit the FO status to trigger creation of Invoices and/or Packing Slips, Delete the Fulfillment Order, or Create Invoices and/or Packing Slips from the Sales Fulfillment section"
                ElseIf ex.Message = "IM_BALANCE" Then
                    litMessage.Text = "The Credit and Debit amounts you�re entering must be of the same amount"
                ElseIf ex.Message = "FY_CLOSED" Then
                    litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
                ElseIf CCommon.ToString(ex.Message).ToLower().Contains("timeout expired") Then
                    litMessage.Text = "Server request timeout."
                ElseIf ex.Message.Contains("You can not add BizDoc because approval of unit price(s) is required.") Then
                    litMessage.Text = "You can not receive payment for order because approval of unit price(s) is required."
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Response.Write(ex)
                End If
            End Try
        End Sub

        Private Sub btnDeleteCardDetails_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeleteCardDetails.Click
            Try
                If ddlCards.SelectedValue > 0 Then
                    Dim objOppInvoice As New OppInvoice
                    objOppInvoice.DomainID = Session("DomainID")
                    objOppInvoice.numCCInfoID = ddlCards.SelectedValue
                    objOppInvoice.DeleteCustomerCreditCardInfo()
                    LoadSavedCreditCardInfo()
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)

            End Try
        End Sub

        Private Function SaveNewWithTransaction(ByVal isFromCloseWindow As Boolean)
            Dim objJEHeader As New JournalEntryHeader
            Dim isPaymentProcessed As Boolean = False

            'Dim TransName As String = ""
            Try
                'Check if all default accounts are set before making journal entries
                If PerformValidations() Then
                    If Not (ddlPayment.SelectedValue = 1 AndAlso rdbAuthorizeOnly.Checked) Then
                        Using objTran As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                            Dim lngDepositeID1 As Integer
                            Dim dtInvoices As DataTable
                            dtInvoices = GetItems()
                            If CCommon.ToDecimal(txtAmount.Text) <> 0 Then
                                If strBizDocIds.Length > 0 Then
                                    'ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "ConfirmPayment", "return confirm('Some of bizdocs you have selected are already paid, if you still try to pay those bizdocs it will be created as Unapplied Credit/Amount. Are you sure you want to pay these bizdocs ?');", True)
                                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "ConfirmPaymentBox", "CallConfirmPayment();", True)
                                End If

                                Dim ds As New DataSet
                                Dim drArray() As DataRow
                                Dim dv As New DataView(dtInvoices)
                                Dim dvAR As New DataView(dtInvoices)
                                Dim dtDistictDivisions As DataTable = dv.ToTable(True, "numdivisionId")
                                Dim dtRecordsBasedOnARAccount As DataTable = dvAR.ToTable(True, "numdivisionId", "numARAccountID")

                                Dim totalAmountPaid As Decimal = CCommon.ToDecimal(txtAmount.Text)
                                If dtDistictDivisions.Rows.Count = 0 Or dtDistictDivisions.Select("numdivisionId='" & CCommon.ToLong(radCmbCompany.SelectedValue) & "'", Nothing).Length = 0 Then
                                    Dim dr As DataRow = dtDistictDivisions.NewRow()
                                    dr("numdivisionId") = CCommon.ToLong(radCmbCompany.SelectedValue)
                                    dtDistictDivisions.Rows.Add(dr)
                                End If

                                totalAmountPaid = CCommon.ToDecimal(txtAmount.Text) - Math.Abs(CCommon.ToDecimal(dtInvoices.Compute("sum(monAmountPaid)", "numdivisionId<>" & CCommon.ToLong(radCmbCompany.SelectedValue) & "")))

                                For i As Integer = 0 To dtDistictDivisions.Rows.Count - 1
                                    drArray = dtInvoices.Select("numdivisionId='" & dtDistictDivisions.Rows(i)("numdivisionId").ToString & "'", Nothing)
                                    'Create Single Deposite Entry against multiple invoice payments And Store in DB
                                    'TransName = "ReceivePayment"

                                    Dim decAmount As Decimal = 0
                                    Dim str As String = ""
                                    ds.Tables.Clear()
                                    If drArray.Length > 0 Then
                                        ds.Tables.Add(drArray.CopyToDataTable())
                                        ds.Tables(0).TableName = "Item"
                                        str = ds.GetXml()

                                        decAmount = Math.Abs(CCommon.ToDecimal(ds.Tables(0).Compute("sum(monAmountPaid)", "")))
                                    End If

                                    If CCommon.ToLong(dtDistictDivisions.Rows(i)("numdivisionId")) = CCommon.ToLong(radCmbCompany.SelectedValue) Then
                                        decAmount = totalAmountPaid
                                    End If

                                    If decAmount > 0 Then
                                        objCommon = New CCommon
                                        objCommon.DomainID = Session("DomainId")
                                        objCommon.UserCntID = Session("UserContactID")
                                        objCommon.DivisionID = CCommon.ToLong(dtDistictDivisions.Rows(i)("numdivisionId").ToString)
                                        Dim lngAccountClass As Long = objCommon.GetAccountingClass()

                                        Dim objMakeDeposit As New MakeDeposit
                                        With objMakeDeposit
                                            .DivisionId = CCommon.ToLong(dtDistictDivisions.Rows(i)("numdivisionId").ToString)
                                            .Entry_Date = calDate.SelectedDate
                                            If ddlPayment.SelectedValue = 1 Then
                                                .TransactionHistoryID = lngTransactionHistoryID
                                            End If
                                            .Reference = txtReference.Text.Trim()
                                            .Memo = txtMemo.Text.Trim()
                                            .PaymentMethod = ddlPayment.SelectedValue
                                            .DepositeToType = IIf(radDepositeTo.Checked, 1, 2)
                                            .numAmount = decAmount
                                            If radDepositeTo.Checked Then
                                                .AccountId = ddlDepositTo.SelectedValue
                                            ElseIf radUnderpositedFund.Checked Then
                                                .AccountId = UndepositedFundAccountID
                                            End If
                                            .RecurringId = 0
                                            .DepositId = IIf(CCommon.ToShort(hdnDepositePage.Value) = 3, 0, lngDepositeID)
                                            .UserCntID = Session("UserContactID")
                                            .DomainID = Session("DomainId")
                                            .StrItems = str
                                            .Mode = 0
                                            .DepositePage = 2
                                            .CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                                            .ExchangeRate = CCommon.ToDouble(txtExchangeRate.Text)
                                            .AccountClass = lngAccountClass
                                            lngDepositeID1 = .SaveDataToMakeDepositDetails(Nothing)
                                        End With

                                        If lngDepositeID1 > 0 Then

                                            'Save General Header Details
                                            If objJEHeader Is Nothing Then objJEHeader = New JournalEntryHeader
                                            Dim lngJournalID As Long
                                            With objJEHeader
                                                .JournalId = TransactionInfo1.JournalID
                                                .RecurringId = 0
                                                .EntryDate = calDate.SelectedDate & " 12:00:00" 'Now.UtcNow
                                                .Description = "Payment received:" & txtMemo.Text
                                                .Amount = decAmount * IIf(CCommon.ToDouble(txtExchangeRate.Text) = 0.0, 1, CCommon.ToDouble(txtExchangeRate.Text))
                                                .CheckId = 0
                                                .CashCreditCardId = 0
                                                .ChartAcntId = 0
                                                .OppId = 0
                                                .OppBizDocsId = 0
                                                .DepositId = lngDepositeID1
                                                .BizDocsPaymentDetId = 0
                                                .IsOpeningBalance = 0
                                                .LastRecurringDate = Date.Now
                                                .NoTransactions = 0
                                                .CategoryHDRID = 0
                                                .ReturnID = 0
                                                .CheckHeaderID = 0
                                                .BillID = 0
                                                .BillPaymentID = 0
                                                .UserCntID = Session("UserContactID")
                                                .DomainID = Session("DomainID")
                                            End With
                                            lngJournalID = objJEHeader.Save()

                                            If lngJournalID > 0 Then
                                                'Save General Ledger Details

                                                Dim objJEList As New JournalEntryCollection
                                                Dim objJE As New JournalEntryNew()
                                                Dim FltExchangeRate As Double

                                                'Determine Exchange Rate
                                                If CCommon.ToLong(ddlCurrency.SelectedValue) = 0 Then
                                                    FltExchangeRate = 1
                                                ElseIf CCommon.ToLong(ddlCurrency.SelectedValue) <> CCommon.ToLong(Session("BaseCurrencyID")) Then
                                                    FltExchangeRate = CCommon.ToDouble(txtExchangeRate.Text)
                                                Else
                                                    FltExchangeRate = 1
                                                End If

                                                'Debit : Undeposit Fund (Amount)
                                                objJE = New JournalEntryNew()

                                                objJE.TransactionId = CCommon.ToLong(TransactionInfo1.HeaderTransactionID)
                                                objJE.DebitAmt = decAmount * FltExchangeRate 'total amount at current exchange rate
                                                objJE.CreditAmt = 0
                                                objJE.ChartAcntId = IIf(radDepositeTo.Checked, ddlDepositTo.SelectedValue, UndepositedFundAccountID)
                                                objJE.Description = IIf(radDepositeTo.Checked, "Amount Deposited to Bank A/c", "Payment received to Undeposited Fund") 'IIf(radDepositeTo.Checked, "Amount Paid (" & p_Amount.ToString() & ")  To " & ddlDepositTo.SelectedItem.Text, "Amount Paid (" & p_Amount.ToString() & ")  To Undeposited Fund")
                                                objJE.CustomerId = CCommon.ToLong(dtDistictDivisions.Rows(i)("numdivisionId").ToString)
                                                objJE.MainDeposit = 1
                                                objJE.MainCheck = 0
                                                objJE.MainCashCredit = 0
                                                objJE.OppitemtCode = 0
                                                objJE.BizDocItems = ""
                                                objJE.Reference = txtReference.Text
                                                objJE.PaymentMethod = ddlPayment.SelectedValue
                                                objJE.Reconcile = False
                                                objJE.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                                                objJE.FltExchangeRate = FltExchangeRate
                                                objJE.TaxItemID = 0
                                                objJE.BizDocsPaymentDetailsId = 0
                                                objJE.ContactID = 0
                                                objJE.ItemID = 0
                                                objJE.ProjectID = 0
                                                objJE.ClassID = 0
                                                objJE.CommissionID = 0
                                                objJE.ReconcileID = 0
                                                objJE.Cleared = 0
                                                objJE.ReferenceType = enmReferenceType.DepositHeader
                                                objJE.ReferenceID = lngDepositeID1

                                                objJEList.Add(objJE)

                                                Dim TempDecAmount As Decimal = decAmount
                                                Dim TempTotalAmountAtExchangeRateOfOrder As Decimal = 0
                                                Dim TempTotalAmountVariationDueToCurrencyExRate As Decimal = 0

                                                'i.e foreign currency payment
                                                If FltExchangeRate <> 1 Then
                                                    TempTotalAmountAtExchangeRateOfOrder = 0
                                                    For Each dr As DataRow In dtInvoices.Rows
                                                        TempTotalAmountAtExchangeRateOfOrder = TempTotalAmountAtExchangeRateOfOrder + CCommon.ToDecimal(dr("monAmountPaid")) * CCommon.ToDouble(dr("numExchangeRateOfOrder"))
                                                    Next
                                                    If TempTotalAmountAtExchangeRateOfOrder > 0 Then
                                                        TempTotalAmountVariationDueToCurrencyExRate = TempTotalAmountAtExchangeRateOfOrder - (decAmount * FltExchangeRate)
                                                        If Math.Abs(TempTotalAmountVariationDueToCurrencyExRate) < 0.0 Then
                                                            TempTotalAmountVariationDueToCurrencyExRate = 0
                                                        End If
                                                    End If
                                                End If


                                                Dim TotalAmountAtExchangeRateOfOrder As Decimal
                                                Dim TotalAmountVariationDueToCurrencyExRate As Decimal
                                                Dim invoiceAmount As Decimal

                                                If Not dtRecordsBasedOnARAccount Is Nothing AndAlso dtRecordsBasedOnARAccount.Rows.Count > 0 AndAlso dtRecordsBasedOnARAccount.Select("numdivisionId=" & CCommon.ToLong(dtDistictDivisions.Rows(i)("numdivisionId"))).Length > 0 Then
                                                    For Each drAR As DataRow In dtRecordsBasedOnARAccount.Select("numdivisionId=" & CCommon.ToLong(dtDistictDivisions.Rows(i)("numdivisionId")))
                                                        'For Each dr As DataRow In dtItems.Rows
                                                        'Credit: Customer A/R With (Amount)
                                                        objJE = New JournalEntryNew()

                                                        'i.e foreign currency payment
                                                        TotalAmountAtExchangeRateOfOrder = 0
                                                        TotalAmountVariationDueToCurrencyExRate = 0
                                                        invoiceAmount = 0

                                                        For Each dr As DataRow In dtInvoices.Select("numdivisionId=" & CCommon.ToLong(dtDistictDivisions.Rows(i)("numdivisionId")) & " AND ISNULL(numARAccountID,0) = " & CCommon.ToLong(drAR("numARAccountID")))
                                                            invoiceAmount = invoiceAmount + CCommon.ToDecimal(dr("monAmountPaid"))
                                                        Next

                                                        If FltExchangeRate <> 1 Then
                                                            For Each dr As DataRow In dtInvoices.Select("numdivisionId=" & CCommon.ToLong(dtDistictDivisions.Rows(i)("numdivisionId")) & " AND ISNULL(numARAccountID,0) = " & CCommon.ToLong(drAR("numARAccountID")))
                                                                TotalAmountAtExchangeRateOfOrder = TotalAmountAtExchangeRateOfOrder + CCommon.ToDecimal(dr("monAmountPaid")) * CCommon.ToDouble(dr("numExchangeRateOfOrder"))
                                                            Next
                                                            If TotalAmountAtExchangeRateOfOrder > 0 Then
                                                                TotalAmountVariationDueToCurrencyExRate = TotalAmountAtExchangeRateOfOrder - (invoiceAmount * FltExchangeRate)
                                                                If Math.Abs(TotalAmountVariationDueToCurrencyExRate) < 0.0 Then
                                                                    TotalAmountVariationDueToCurrencyExRate = 0
                                                                End If
                                                            End If
                                                        End If

                                                        TempDecAmount = TempDecAmount - invoiceAmount
                                                        TempTotalAmountAtExchangeRateOfOrder = TempTotalAmountAtExchangeRateOfOrder - TotalAmountAtExchangeRateOfOrder
                                                        TempTotalAmountVariationDueToCurrencyExRate = TempTotalAmountVariationDueToCurrencyExRate - TotalAmountVariationDueToCurrencyExRate

                                                        objJE.TransactionId = 0
                                                        objJE.DebitAmt = 0
                                                        objJE.CreditAmt = IIf(TotalAmountAtExchangeRateOfOrder > 0, TotalAmountAtExchangeRateOfOrder, invoiceAmount * FltExchangeRate)
                                                        If CCommon.ToLong(drAR("numARAccountID")) > 0 Then
                                                            objJE.ChartAcntId = CCommon.ToLong(drAR("numARAccountID"))
                                                        Else
                                                            objJE.ChartAcntId = objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), CCommon.ToLong(dtDistictDivisions.Rows(i)("numdivisionId")))
                                                        End If
                                                        objJE.Description = "Payment received-Credited to Customer A/R" '"Credit Customer's AR account"
                                                        objJE.CustomerId = CCommon.ToLong(dtDistictDivisions.Rows(i)("numdivisionId").ToString)
                                                        objJE.MainDeposit = 0
                                                        objJE.MainCheck = 0
                                                        objJE.MainCashCredit = 0
                                                        objJE.OppitemtCode = 0
                                                        objJE.BizDocItems = ""
                                                        objJE.Reference = txtReference.Text
                                                        objJE.PaymentMethod = ddlPayment.SelectedValue
                                                        objJE.Reconcile = False
                                                        objJE.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                                                        objJE.FltExchangeRate = FltExchangeRate
                                                        objJE.TaxItemID = 0
                                                        objJE.BizDocsPaymentDetailsId = 0
                                                        objJE.ContactID = 0
                                                        objJE.ItemID = 0
                                                        objJE.ProjectID = 0
                                                        objJE.ClassID = 0 'FIELD GETS UPDATED IN STORE PROCEDURE WITH THE CLASS ID OF GENERAL_JOURNAL_HEADER CLASS ID SO NO NEED TO PASS IT
                                                        objJE.CommissionID = 0
                                                        objJE.ReconcileID = 0
                                                        objJE.Cleared = 0
                                                        objJE.ReferenceType = enmReferenceType.DepositDetail
                                                        objJE.ReferenceID = 0

                                                        objJEList.Add(objJE)

                                                        If CCommon.ToLong(ddlCurrency.SelectedValue) > 0 And Math.Abs(TotalAmountVariationDueToCurrencyExRate) > 0 Then
                                                            If CCommon.ToLong(ddlCurrency.SelectedValue) <> CCommon.ToLong(Session("BaseCurrencyID")) And CCommon.ToDouble(txtExchangeRate.Text) > 0.0 Then 'Foreign Currency Transaction

                                                                Dim lngDefaultForeignExchangeAccountID As Long = ChartOfAccounting.GetDefaultAccount("FE", Session("DomainID"))

                                                                'Credit: Customer A/R With (Amount)
                                                                objJE = New JournalEntryNew()
                                                                objJE.TransactionId = 0

                                                                ' difference in amount due to exchange rate variation between order placed exchange rate with Entered exchange rate
                                                                If TotalAmountVariationDueToCurrencyExRate < 0 Then
                                                                    objJE.DebitAmt = 0
                                                                    objJE.CreditAmt = Math.Abs(TotalAmountVariationDueToCurrencyExRate)
                                                                ElseIf TotalAmountVariationDueToCurrencyExRate > 0 Then
                                                                    objJE.DebitAmt = Math.Abs(TotalAmountVariationDueToCurrencyExRate)
                                                                    objJE.CreditAmt = 0
                                                                End If

                                                                objJE.ChartAcntId = lngDefaultForeignExchangeAccountID
                                                                objJE.Description = "Exchange Gain/Loss"
                                                                objJE.CustomerId = CCommon.ToLong(dtDistictDivisions.Rows(i)("numdivisionId").ToString)
                                                                objJE.MainDeposit = 0
                                                                objJE.MainCheck = 0
                                                                objJE.MainCashCredit = 0
                                                                objJE.OppitemtCode = 0
                                                                objJE.BizDocItems = ""
                                                                objJE.Reference = txtReference.Text
                                                                objJE.PaymentMethod = ddlPayment.SelectedValue
                                                                objJE.Reconcile = False
                                                                objJE.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                                                                objJE.FltExchangeRate = FltExchangeRate
                                                                objJE.TaxItemID = 0
                                                                objJE.BizDocsPaymentDetailsId = 0
                                                                objJE.ContactID = 0
                                                                objJE.ItemID = 0
                                                                objJE.ProjectID = 0
                                                                objJE.ClassID = 0 'FIELD GETS UPDATED IN STORE PROCEDURE WITH THE CLASS ID OF GENERAL_JOURNAL_HEADER CLASS ID SO NO NEED TO PASS IT
                                                                objJE.CommissionID = 0
                                                                objJE.ReconcileID = 0
                                                                objJE.Cleared = 0
                                                                objJE.ReferenceType = enmReferenceType.DepositDetail
                                                                objJE.ReferenceID = 0

                                                                objJEList.Add(objJE)
                                                            End If
                                                        End If
                                                    Next
                                                End If

                                                If TempTotalAmountAtExchangeRateOfOrder > 0 Or TempTotalAmountVariationDueToCurrencyExRate > 0 Or TempDecAmount > 0 Then
                                                    'For Each dr As DataRow In dtItems.Rows
                                                    'Credit: Customer A/R With (Amount)
                                                    objJE = New JournalEntryNew()

                                                    objJE.TransactionId = 0
                                                    objJE.DebitAmt = 0
                                                    objJE.CreditAmt = IIf(TempTotalAmountAtExchangeRateOfOrder > 0, TempTotalAmountAtExchangeRateOfOrder, TempDecAmount * FltExchangeRate)
                                                    objJE.ChartAcntId = lngCustomerARAccount
                                                    objJE.Description = "Payment received-Credited to Customer A/R" '"Credit Customer's AR account"
                                                    objJE.CustomerId = CCommon.ToLong(dtDistictDivisions.Rows(i)("numdivisionId").ToString)
                                                    objJE.MainDeposit = 0
                                                    objJE.MainCheck = 0
                                                    objJE.MainCashCredit = 0
                                                    objJE.OppitemtCode = 0
                                                    objJE.BizDocItems = ""
                                                    objJE.Reference = txtReference.Text
                                                    objJE.PaymentMethod = ddlPayment.SelectedValue
                                                    objJE.Reconcile = False
                                                    objJE.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                                                    objJE.FltExchangeRate = FltExchangeRate
                                                    objJE.TaxItemID = 0
                                                    objJE.BizDocsPaymentDetailsId = 0
                                                    objJE.ContactID = 0
                                                    objJE.ItemID = 0
                                                    objJE.ProjectID = 0
                                                    objJE.ClassID = 0 'FIELD GETS UPDATED IN STORE PROCEDURE WITH THE CLASS ID OF GENERAL_JOURNAL_HEADER CLASS ID SO NO NEED TO PASS IT
                                                    objJE.CommissionID = 0
                                                    objJE.ReconcileID = 0
                                                    objJE.Cleared = 0
                                                    objJE.ReferenceType = enmReferenceType.DepositDetail
                                                    objJE.ReferenceID = 0

                                                    objJEList.Add(objJE)

                                                    If CCommon.ToLong(ddlCurrency.SelectedValue) > 0 And Math.Abs(TempTotalAmountVariationDueToCurrencyExRate) > 0 Then
                                                        If CCommon.ToLong(ddlCurrency.SelectedValue) <> CCommon.ToLong(Session("BaseCurrencyID")) And CCommon.ToDouble(txtExchangeRate.Text) > 0.0 Then 'Foreign Currency Transaction

                                                            Dim lngDefaultForeignExchangeAccountID As Long = ChartOfAccounting.GetDefaultAccount("FE", Session("DomainID"))

                                                            'Credit: Customer A/R With (Amount)
                                                            objJE = New JournalEntryNew()
                                                            objJE.TransactionId = 0

                                                            ' difference in amount due to exchange rate variation between order placed exchange rate with Entered exchange rate
                                                            If TempTotalAmountVariationDueToCurrencyExRate < 0 Then
                                                                objJE.DebitAmt = 0
                                                                objJE.CreditAmt = Math.Abs(TempTotalAmountVariationDueToCurrencyExRate)
                                                            ElseIf TempTotalAmountVariationDueToCurrencyExRate > 0 Then
                                                                objJE.DebitAmt = Math.Abs(TempTotalAmountVariationDueToCurrencyExRate)
                                                                objJE.CreditAmt = 0
                                                            End If

                                                            objJE.ChartAcntId = lngDefaultForeignExchangeAccountID
                                                            objJE.Description = "Exchange Gain/Loss"
                                                            objJE.CustomerId = CCommon.ToLong(dtDistictDivisions.Rows(i)("numdivisionId").ToString)
                                                            objJE.MainDeposit = 0
                                                            objJE.MainCheck = 0
                                                            objJE.MainCashCredit = 0
                                                            objJE.OppitemtCode = 0
                                                            objJE.BizDocItems = ""
                                                            objJE.Reference = txtReference.Text
                                                            objJE.PaymentMethod = ddlPayment.SelectedValue
                                                            objJE.Reconcile = False
                                                            objJE.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                                                            objJE.FltExchangeRate = FltExchangeRate
                                                            objJE.TaxItemID = 0
                                                            objJE.BizDocsPaymentDetailsId = 0
                                                            objJE.ContactID = 0
                                                            objJE.ItemID = 0
                                                            objJE.ProjectID = 0
                                                            objJE.ClassID = 0 'FIELD GETS UPDATED IN STORE PROCEDURE WITH THE CLASS ID OF GENERAL_JOURNAL_HEADER CLASS ID SO NO NEED TO PASS IT
                                                            objJE.CommissionID = 0
                                                            objJE.ReconcileID = 0
                                                            objJE.Cleared = 0
                                                            objJE.ReferenceType = enmReferenceType.DepositDetail
                                                            objJE.ReferenceID = 0

                                                            objJEList.Add(objJE)
                                                        End If
                                                    End If
                                                End If

                                                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalID, Session("DomainID"), Nothing)
                                            Else
                                                Throw New Exception("ERROR:2 Error occurred while processing payment.")
                                            End If

                                            'Order Automation Execution  
                                            For Each dr As DataRow In drArray
                                                Dim objRule As New OrderAutoRules
                                                objRule.GenerateAutoPO(CCommon.ToLong(dr("numOppBizDocsID")), Nothing)
                                            Next
                                        Else
                                            Throw New Exception("ERROR:1 Error occurred while processing payment.")
                                        End If
                                    End If
                                Next
                            End If

                            'Process Payment When Payment method is Credit card
                            isPaymentProcessed = ProcessPayment()
                            If isPaymentProcessed Then
                                Dim objMakeDeposit As New MakeDeposit
                                objMakeDeposit.DepositId = lngDepositeID1
                                objMakeDeposit.TransactionHistoryID = lngTransactionHistoryID
                                objMakeDeposit.UpdateDepositeTransactionID()

                                objTran.Complete()
                            Else
                                objTran.Dispose()
                            End If
                        End Using
                    End If

                    If isPaymentProcessed Then
                        If isFromCloseWindow Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "receivepaymentandclose", "receivepaymentandclose();", True)
                        Else
                            PageRedirect()
                        End If
                    End If
                End If
                strBizDocIds = ""
            Catch ex As Exception
                If ex.Message.StartsWith("ERROR:") Then
                    litMessage.Text = ex.Message
                Else
                    Throw
                End If
            End Try
        End Function

        Private Sub PageRedirect()
            Try
                If GetQueryStringVal("frm") = "BankRegister" Then
                    Response.Redirect("../Accounting/frmJournalEntry.aspx?ChartAcntId=" & CCommon.ToLong(GetQueryStringVal("ChartAcntId")), False)
                ElseIf GetQueryStringVal("frm") = "RecurringTransaction" Then
                    Response.Redirect("../Accounting/frmRecurringTransaction.aspx", False)
                ElseIf GetQueryStringVal("frm") = "GeneralLedger" Then
                    Response.Redirect("../Accounting/frmGeneralLedger.aspx", False)
                ElseIf GetQueryStringVal("frm") = "Transaction" Then
                    Response.Redirect("../Accounting/frmTransactions.aspx?ChartAcntId=" & CCommon.ToLong(GetQueryStringVal("ChartAcntId")), False)
                ElseIf GetQueryStringVal("frm") = "BankRecon" Then
                    Response.Redirect("../Accounting/frmBankReconciliation.aspx?ReconcileID=" & CCommon.ToLong(GetQueryStringVal("ReconcileID")), False)
                ElseIf GetQueryStringVal("frm") = "Deposit" Then
                    Response.Redirect("../Accounting/frmMakeDeposit.aspx?DepositId=" & CCommon.ToLong(GetQueryStringVal("MakeDepositId")), False)
                ElseIf GetQueryStringVal("frm") = "AR" Or GetQueryStringVal("frm") = "AP" Then
                    Dim strScript As String = ""
                    strScript += String.Format("opener.top.frames['mainframe'].receivePayment('{1:c} from {0} Received Successfully!'); self.close();window.opener.reDirect('../Accounting/frmAccountsReceivable.aspx');", radCmbCompany.Text, CCommon.ToDecimal(txtAmount.Text))
                    If (Not Page.IsStartupScriptRegistered("ARCloseScript")) Then
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "clientScript", strScript, True)
                    End If
                ElseIf ddlPayment.SelectedValue = 1 Then
                    If rdbAuthorizeOnly.Checked Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "RedirectPaymentSuccess", "callfromparent('" & ("?flag=4&NewOrderId=" & lngOppID.ToString & "&IsNewOrderPayment=" & IsNewOrder) & "');", True)
                    Else
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "RedirectPaymentSuccess", "callfromparent('" & ("?flag=2&NewOrderId=" & lngOppID.ToString & "&IsNewOrderPayment=" & IsNewOrder) & "');", True)
                    End If
                ElseIf GetQueryStringVal("frm") = "topTab" Then
                    txtCurrrentPage.Text = 1
                    litMessage.Text = String.Format("<span style='color: green; font-weight: bold;'>{1:c} from {0} Received Successfully!</span>", radCmbCompany.Text, CCommon.ToDecimal(txtAmount.Text))
                    BindCreditgrid()
                    BindDatagrid()
                    txtMemo.Text = ""
                    txtReference.Text = ""
                    txtAmount.Text = "0"
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "RedirectPaymentSuccess", "callfromparent('" & ("?flag=1&NewOrderId=" & lngOppID.ToString & "&IsNewOrderPayment=" & IsNewOrder) & "');", True)
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Function GetItems() As DataTable
            Try
                Dim objMakeDeposit As New MakeDeposit
                Dim dtMain As New DataTable
                Dim dtCredits As New DataTable
                Dim dtDeposit As New DataTable

                CCommon.AddColumnsToDataTable(dtMain, "numDepositeDetailID,numOppBizDocsID,numOppID,numdivisionId,bitChild,numExchangeRateOfOrder,numARAccountID")
                CCommon.AddColumnsToDataTable(dtCredits, "numDepositeDetailID,numOppBizDocsID,numOppID")
                CCommon.AddColumnsToDataTable(dtDeposit, "numDepositeDetailID,numOppBizDocsID,numOppID,numdivisionId,bitChild,numExchangeRateOfOrder,numARAccountID")

                Dim dcAmountPaid = New DataColumn("monAmountPaid", System.Type.GetType("System.Decimal"))
                dtMain.Columns.Add(dcAmountPaid)

                dcAmountPaid = New DataColumn("monAmountPaid", System.Type.GetType("System.Decimal"))
                dtCredits.Columns.Add(dcAmountPaid)

                dcAmountPaid = New DataColumn("monAmountPaid", System.Type.GetType("System.Decimal"))
                dtDeposit.Columns.Add(dcAmountPaid)

                Dim listSelectedInvoices As System.Collections.Generic.List(Of SelectedInvoice)

                If Not String.IsNullOrEmpty(UserSelectedInvoices.Trim()) Then
                    listSelectedInvoices = Newtonsoft.Json.JsonConvert.DeserializeObject(Of System.Collections.Generic.List(Of SelectedInvoice))(UserSelectedInvoices.Trim())
                End If

                Dim dr As DataRow
                Dim dsCheckedDeposits As DataSet
                Dim AmountToBeDeposited As Double = 0
                For Each item As DataGridItem In dgOpportunity.Items
                    If CType(item.FindControl("chk"), CheckBox).Checked Then
                        If Not listSelectedInvoices Is Nothing AndAlso listSelectedInvoices.Where(Function(x) x.numOppBizDocsID = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value)).Count() > 0 Then
                            listSelectedInvoices.RemoveAll(Function(x) x.numOppBizDocsID = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value))
                        End If

                        objMakeDeposit.DivisionId = CCommon.ToLong(CType(item.FindControl("hdnDivisionId"), HiddenField).Value)
                        objMakeDeposit.OppBizDocsId = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value)
                        objMakeDeposit.numAmount = CCommon.ToDecimal(CType(item.FindControl("txtAmountToPay"), TextBox).Text)
                        objMakeDeposit.DomainID = Session("DomainId")
                        dsCheckedDeposits = objMakeDeposit.CheckBizDocAmountsForReceivePayment()

                        If dsCheckedDeposits IsNot Nothing AndAlso dsCheckedDeposits.Tables.Count > 0 AndAlso dsCheckedDeposits.Tables(0).Rows.Count > 0 Then
                            For Each drCheckRow As DataRow In dsCheckedDeposits.Tables(0).Rows

                                'If Invoice is not paid in full, check whether amount is making over-payment or not. 
                                'If it is making over-payment, deduct increased amount from Invoiced amount & create Unapplied Amount/Credit for that particular company

                                If CCommon.ToShort(drCheckRow("IsPaidInFull")) = 1 Then
                                    strBizDocIds = strBizDocIds & "," & CCommon.ToString(CType(item.FindControl("hdnBizDocID"), HiddenField).Value)
                                Else
                                    dr = dtMain.NewRow
                                    dr("numDepositeDetailID") = CCommon.ToLong(CType(item.FindControl("hdnDepositeDetailID"), HiddenField).Value)
                                    AmountToBeDeposited = CCommon.ToDecimal(CType(item.FindControl("txtAmountToPay"), TextBox).Text)
                                    dr("numOppBizDocsID") = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value)
                                    dr("numOppID") = CCommon.ToLong(CType(item.FindControl("hdnOppID"), HiddenField).Value)
                                    dr("monAmountPaid") = AmountToBeDeposited 'CCommon.ToDecimal(CType(item.FindControl("txtAmountToPay"), TextBox).Text)
                                    dr("numdivisionId") = CCommon.ToLong(CType(item.FindControl("hdnDivisionId"), HiddenField).Value)
                                    dr("bitChild") = CCommon.ToBool(CType(item.FindControl("hdnChild"), HiddenField).Value)
                                    dr("numExchangeRateOfOrder") = CCommon.ToDouble(CType(item.FindControl("hdnExchangeRateOfOrder"), HiddenField).Value)
                                    dr("numARAccountID") = CCommon.ToLong(CType(item.FindControl("hdnARAccountID"), HiddenField).Value)
                                    dtMain.Rows.Add(dr)
                                End If
                            Next

                        Else
                            dr = dtMain.NewRow
                            dr("numDepositeDetailID") = CCommon.ToLong(CType(item.FindControl("hdnDepositeDetailID"), HiddenField).Value)
                            AmountToBeDeposited = CCommon.ToDecimal(CType(item.FindControl("txtAmountToPay"), TextBox).Text)
                            dr("numOppBizDocsID") = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value)
                            dr("numOppID") = CCommon.ToLong(CType(item.FindControl("hdnOppID"), HiddenField).Value)
                            dr("monAmountPaid") = AmountToBeDeposited 'CCommon.ToDecimal(CType(item.FindControl("txtAmountToPay"), TextBox).Text)
                            dr("numdivisionId") = CCommon.ToLong(CType(item.FindControl("hdnDivisionId"), HiddenField).Value)
                            dr("bitChild") = CCommon.ToBool(CType(item.FindControl("hdnChild"), HiddenField).Value)
                            dr("numExchangeRateOfOrder") = CCommon.ToDouble(CType(item.FindControl("hdnExchangeRateOfOrder"), HiddenField).Value)
                            dr("numARAccountID") = CCommon.ToLong(CType(item.FindControl("hdnARAccountID"), HiddenField).Value)
                            dtMain.Rows.Add(dr)

                        End If
                    Else
                        'If item in unchecked in grid and exists in list of selected invoices than remove it
                        If Not listSelectedInvoices Is Nothing AndAlso listSelectedInvoices.Where(Function(x) x.numOppBizDocsID = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value)).Count() > 0 Then
                            listSelectedInvoices.RemoveAll(Function(x) x.numOppBizDocsID = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value))
                        End If
                    End If
                Next

                If Not listSelectedInvoices Is Nothing AndAlso listSelectedInvoices.Count > 0 Then
                    For Each objSelectedInvoice In listSelectedInvoices
                        objMakeDeposit.DivisionId = objSelectedInvoice.numdivisionId
                        objMakeDeposit.OppBizDocsId = objSelectedInvoice.numOppBizDocsID
                        objMakeDeposit.numAmount = objSelectedInvoice.monAmountPaid
                        objMakeDeposit.DomainID = Session("DomainId")
                        dsCheckedDeposits = objMakeDeposit.CheckBizDocAmountsForReceivePayment()

                        If dsCheckedDeposits IsNot Nothing AndAlso dsCheckedDeposits.Tables.Count > 0 AndAlso dsCheckedDeposits.Tables(0).Rows.Count > 0 Then
                            For Each drCheckRow As DataRow In dsCheckedDeposits.Tables(0).Rows

                                'If Invoice is not paid in full, check whether amount is making over-payment or not. 
                                'If it is making over-payment, deduct increased amount from Invoiced amount & create Unapplied Amount/Credit for that particular company

                                If CCommon.ToShort(drCheckRow("IsPaidInFull")) = 1 Then
                                    strBizDocIds = strBizDocIds & "," & CCommon.ToString(objSelectedInvoice.numBizDocID)
                                Else
                                    dr = dtMain.NewRow
                                    dr("numDepositeDetailID") = objSelectedInvoice.numDepositeDetailID
                                    dr("numOppBizDocsID") = objSelectedInvoice.numOppBizDocsID
                                    dr("numOppID") = objSelectedInvoice.numOppID
                                    dr("monAmountPaid") = objSelectedInvoice.monAmountPaid
                                    dr("numdivisionId") = objSelectedInvoice.numdivisionId
                                    dr("numARAccountID") = objSelectedInvoice.numARAccountID
                                    dr("bitChild") = objSelectedInvoice.bitChild
                                    dr("numExchangeRateOfOrder") = objSelectedInvoice.numExchangeRateOfOrder
                                    dtMain.Rows.Add(dr)
                                End If
                            Next
                        Else
                            dr = dtMain.NewRow
                            dr("numDepositeDetailID") = objSelectedInvoice.numDepositeDetailID
                            dr("numOppBizDocsID") = objSelectedInvoice.numOppBizDocsID
                            dr("numOppID") = objSelectedInvoice.numOppID
                            dr("monAmountPaid") = objSelectedInvoice.monAmountPaid
                            dr("numdivisionId") = objSelectedInvoice.numdivisionId
                            dr("numARAccountID") = objSelectedInvoice.numARAccountID
                            dr("bitChild") = objSelectedInvoice.bitChild
                            dr("numExchangeRateOfOrder") = objSelectedInvoice.numExchangeRateOfOrder
                            dtMain.Rows.Add(dr)
                        End If
                    Next
                End If

                If (chkCreateInvoice.Checked = True AndAlso lngRecentlyCreatedBizDocId > 0 AndAlso CCommon.ToDecimal(txtOrderAmount.Text) > 0) Then
                    dr = dtMain.NewRow
                    dr("numDepositeDetailID") = 0
                    AmountToBeDeposited = txtOrderAmount.Text
                    dr("numOppBizDocsID") = lngRecentlyCreatedBizDocId
                    dr("numOppID") = lngOppID
                    dr("monAmountPaid") = AmountToBeDeposited 'CCommon.ToDecimal(CType(item.FindControl("txtAmountToPay"), TextBox).Text)
                    dr("numdivisionId") = radCmbCompany.SelectedValue
                    dr("bitChild") = False
                    dr("numARAccountID") = 0
                    dr("numExchangeRateOfOrder") = fltExchangeRateCreatedBizDoc
                    dtMain.Rows.Add(dr)
                End If

                Dim decCredit As Decimal = 0

                For Each gvr As GridViewRow In gvCredits.Rows
                    If CCommon.ToLong(hdnDepositePage.Value) = 3 AndAlso CCommon.ToLong(CType(gvr.FindControl("lblReferenceID"), Label).Text) = CCommon.ToLong(hdnDepositeID.Value) Then

                        If objMakeDeposit Is Nothing Then objMakeDeposit = New MakeDeposit
                        With objMakeDeposit
                            .DepositId = CCommon.ToLong(CType(gvr.FindControl("lblReferenceID"), Label).Text)
                            .UserCntID = Session("UserContactID")
                            .DomainID = Session("DomainId")

                            .StrItems = ""
                            .Mode = 2
                            .DepositePage = 3
                            .SaveDataToMakeDepositDetails()
                        End With
                    End If

                    If CType(gvr.FindControl("chk"), CheckBox).Checked Then
                        Dim decCreditAmount As Decimal = CCommon.ToDecimal(CType(gvr.FindControl("txtCreditAmount"), TextBox).Text)

                        dtCredits.Rows.Clear()

                        If decCreditAmount > 0 Then
                            decCredit += decCreditAmount

                            For Each drMain As DataRow In dtMain.Rows
                                If CCommon.ToBool(drMain("bitChild")) = False Then
                                    If decCreditAmount > 0 Then
                                        Dim monAmountPaid As Decimal = CCommon.ToDecimal(drMain("monAmountPaid"))

                                        If monAmountPaid > 0 Then
                                            dr = dtCredits.NewRow

                                            dr("numDepositeDetailID") = 0
                                            dr("numOppBizDocsID") = drMain("numOppBizDocsID")
                                            dr("numOppID") = drMain("numOppID")

                                            If decCreditAmount > monAmountPaid Then
                                                dr("monAmountPaid") = monAmountPaid
                                                decCreditAmount = decCreditAmount - monAmountPaid
                                            Else
                                                dr("monAmountPaid") = decCreditAmount
                                                decCreditAmount = 0
                                            End If

                                            drMain("monAmountPaid") = monAmountPaid - dr("monAmountPaid")
                                            dtCredits.Rows.Add(dr)
                                        End If
                                    Else
                                        Exit For
                                    End If
                                End If
                            Next

                            dtMain.AcceptChanges()

                            If dtCredits.Rows.Count > 0 Then
                                If objMakeDeposit Is Nothing Then objMakeDeposit = New MakeDeposit

                                With objMakeDeposit
                                    .DepositId = CCommon.ToLong(CType(gvr.FindControl("lblReferenceID"), Label).Text)
                                    .UserCntID = Session("UserContactID")
                                    .DomainID = Session("DomainId")

                                    Dim ds As New DataSet
                                    Dim str As String = ""

                                    If dtCredits.Rows.Count > 0 Then
                                        ds.Tables.Add(dtCredits.Copy)
                                        ds.Tables(0).TableName = "Item"
                                        str = ds.GetXml()
                                    End If

                                    .StrItems = str
                                    .Mode = 2
                                    .DepositePage = 2
                                    .SaveDataToMakeDepositDetails()
                                End With
                            End If
                        End If
                    End If
                Next

                dtMain.AcceptChanges()

                Dim decAmount As Decimal = CCommon.ToDecimal(txtAmount.Text) + decCredit

                For Each drMain As DataRow In dtMain.Rows
                    If decAmount > 0 Then
                        Dim monAmountPaid As Decimal = CCommon.ToDecimal(drMain("monAmountPaid"))

                        If monAmountPaid > 0 Then
                            dr = dtDeposit.NewRow

                            dr("numDepositeDetailID") = IIf(CCommon.ToLong(hdnDepositePage.Value) = 3, 0, drMain("numDepositeDetailID"))
                            dr("numOppBizDocsID") = drMain("numOppBizDocsID")
                            dr("numOppID") = drMain("numOppID")
                            dr("numdivisionId") = drMain("numdivisionId")
                            dr("numARAccountID") = drMain("numARAccountID")
                            dr("bitChild") = drMain("bitChild")
                            dr("numExchangeRateOfOrder") = drMain("numExchangeRateOfOrder")

                            If decAmount > monAmountPaid Then
                                dr("monAmountPaid") = monAmountPaid
                                decAmount = decAmount - monAmountPaid
                            Else
                                dr("monAmountPaid") = decAmount
                                decAmount = 0
                            End If

                            drMain("monAmountPaid") = monAmountPaid - dr("monAmountPaid")
                            dtDeposit.Rows.Add(dr)
                        End If
                    Else
                        Exit For
                    End If
                Next

                Return dtDeposit

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function PerformValidations() As Boolean
            Try
                'validate account mapping
                UndepositedFundAccountID = ChartOfAccounting.GetDefaultAccount("UF", Session("DomainID")) 'Undeposited Fund
                If Not UndepositedFundAccountID > 0 Then
                    litMessage.Text = "Please Map Default Undeposited Funds Account for Your Company from ""Administration->Global Settings->Accounting->Default Accounts""."
                    Return False
                End If


                lngDefaultAPAccount = ChartOfAccounting.GetDefaultAccount("PL", Session("DomainID"))

                If Not lngDefaultAPAccount > 0 Then
                    litMessage.Text = "Please Map Default Payroll Liablity Account for Your Company from ""Administration->Global Settings->Accounting->Default Accounts""."
                    Return False
                End If

                lngEmpPayRollAccount = ChartOfAccounting.GetDefaultAccount("EP", Session("DomainID"))

                If Not lngEmpPayRollAccount > 0 Then
                    litMessage.Text = "Please Map Default Employee Payroll Expense Account for Your Company from ""Administration->Global Settings->Accounting->Default Accounts"" ."
                    Return False
                End If



                If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                If dgOpportunity.Items.Count > 0 Then 'Multiple Invoice payment
                    For Each item As DataGridItem In dgOpportunity.Items
                        lngCustomerARAccount = objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), CCommon.ToLong(CType(item.FindControl("hdnDivisionId"), HiddenField).Value))
                        If lngCustomerARAccount = 0 Then
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip""  To Save Amount' );", True)
                            Return False
                        End If
                    Next

                    Dim listSelectedInvoices As System.Collections.Generic.List(Of SelectedInvoice)
                    If Not String.IsNullOrEmpty(UserSelectedInvoices.Trim()) Then
                        listSelectedInvoices = Newtonsoft.Json.JsonConvert.DeserializeObject(Of System.Collections.Generic.List(Of SelectedInvoice))(UserSelectedInvoices.Trim())
                    End If

                    If Not listSelectedInvoices Is Nothing AndAlso listSelectedInvoices.Count > 0 Then
                        For Each objSelectedInvoice In listSelectedInvoices
                            lngCustomerARAccount = objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), objSelectedInvoice.numdivisionId)
                            If lngCustomerARAccount = 0 Then
                                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip""  To Save Amount' );", True)
                                Return False
                            End If
                        Next
                    End If
                Else 'single invoice paymentZ
                    lngCustomerARAccount = objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), CCommon.ToLong(radCmbCompany.SelectedValue))
                    If lngCustomerARAccount = 0 Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting->Accounts for RelationShip"" To Save Amount' );", True)
                        Return False
                    End If
                End If

                ' Validation of linked deposit transaction

                If CCommon.ToBool(hdnIsDepositedToAccount.Value) Then
                    litMessage.Text = "This transaction has been deposited. If you want to change or delete it, you must edit the deposit it appears on and remove it first."
                    Return False
                End If

                If Session("MultiCurrency") = True Then
                    Dim lngDefaultForeignExchangeAccountID As Long = ChartOfAccounting.GetDefaultAccount("FE", Session("DomainID"))

                    If lngDefaultForeignExchangeAccountID = 0 Then
                        litMessage.Text = "Please Map Default Foreign Exchange Gain/Loss Account for Your Company from ""Administration->Global Settings->Accounting->Default Accounts"" ."
                        Return False
                    End If
                End If
                If CCommon.ToLong(radCmbCompany.SelectedValue) > 0 And CCommon.ToLong(ddlPayment.SelectedValue) = 1 And CCommon.ToLong(ddlContact.SelectedValue) = 0 Then
                    litMessage.Text = "Please select contact to proceed with credit card payment, its required to process credit card."
                    Return False
                End If

                Return True
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Function ReturnMoney(ByVal Money)
            Try
                If Not IsDBNull(Money) Then Return String.Format("{0:###0.00}", Money)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub ddlPayment_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPayment.SelectedIndexChanged
            Try
                BindCardType()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindCardType()
            Try
                If (lngPaymentId > 0) Then
                    ddlPayment.SelectedValue = lngPaymentId
                End If
                If ddlPayment.SelectedValue = 1 Then
                    btnSaveClose.Visible = False
                    btnSaveCloseWindow.Visible = False
                    btnCreditCard.Visible = True
                    txtSwipe.Focus()
                    objCommon.sb_FillComboFromDBwithSel(ddlCardType, 120, Session("DomainID"))

                    Dim objOppInvoice As New OppInvoice
                    Dim dsCCInfo As DataSet
                    objOppInvoice.DomainID = Session("DomainID")
                    objOppInvoice.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                    objOppInvoice.numCCInfoID = 0
                    objOppInvoice.IsDefault = 1
                    objOppInvoice.bitflag = 1
                    dsCCInfo = objOppInvoice.GetCustomerCreditCardInfo()
                    Dim strCard As String = ""
                    Dim objEncryption As New QueryStringValues
                    For i As Integer = 0 To dsCCInfo.Tables(0).Rows.Count - 1
                        strCard = objEncryption.Decrypt(dsCCInfo.Tables(0).Rows(i)("vcCreditCardNo").ToString())
                        If strCard.Length > 4 Then
                            dsCCInfo.Tables(0).Rows(i)("vcCreditCardNo") = "************" & strCard.Substring(strCard.Length - 4, 4)
                        Else
                            dsCCInfo.Tables(0).Rows(i)("vcCreditCardNo") = "NA"
                        End If
                    Next
                    ddlCards.DataTextField = "vcCreditCardNo"
                    ddlCards.DataValueField = "numCCInfoID"
                    ddlCards.DataSource = dsCCInfo
                    ddlCards.DataBind()
                    ddlCards.Items.Insert(0, "--Select One--")
                    ddlCards.Items.FindByText("--Select One--").Value = "0"

                    If CCommon.ToLong(hdnDefaultCreditCard.Value) > 0 AndAlso ddlPayment.Items.FindByValue(CCommon.ToLong(hdnDefaultCreditCard.Value)) Is Nothing Then
                        ddlCards.ClearSelection()
                        ddlCards.Items.FindByValue(CCommon.ToLong(hdnDefaultCreditCard.Value)).Selected = True

                        LoadSavedCreditCardInfo()
                    ElseIf dsCCInfo.Tables(0).Rows.Count > 0 Then
                        ddlCards.SelectedIndex = 1
                        LoadSavedCreditCardInfo()
                    End If

                    pnlCreditCardForm.Visible = True

                    Me.ClientScript.RegisterStartupScript(Me.GetType(), "ResizeWindow", "windowResize();", True) 'Bug id 2076
                    btnSaveCloseWindow.Visible = False
                Else
                    pnlCreditCardForm.Visible = False
                    btnCreditCard.Visible = False
                    btnSaveClose.Visible = True
                    btnSaveCloseWindow.Visible = True
                    btnSaveClose.Text = "Receive Payment"
                End If

                Page.ClientScript.RegisterStartupScript(Me.GetType, "UpdateTotalNew", "UpdateTotalNew();", True)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindDatagrid()
            Try
                If CCommon.ToLong(radCmbCompany.SelectedValue) = 0 Then Exit Sub

                Dim dtBizDocs As DataTable
                Dim objMakeDeposit As New MakeDeposit
                objMakeDeposit.DomainID = Session("DomainID")
                objMakeDeposit.DivisionId = CCommon.ToLong(radCmbCompany.SelectedValue)
                objMakeDeposit.DepositId = lngDepositeID
                objMakeDeposit.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                objMakeDeposit.CurrentPage = CCommon.ToLong(txtCurrrentPage.Text)
                Dim ds As DataSet = objMakeDeposit.GetBizDocsForReceivePayment()

                dtBizDocs = ds.Tables(0)
                dgOpportunity.DataSource = dtBizDocs
                dgOpportunity.DataBind()

                bizPager.RecordCount = CCommon.ToInteger(ds.Tables(1).Rows(0)("TotalRecords"))
                bizPager.PageSize = CCommon.ToInteger(ds.Tables(1).Rows(0)("PageSize"))
                bizPager.CurrentPageIndex = txtCurrrentPage.Text

                For Each gridRow As DataGridItem In dgOpportunity.Items
                    If lngOppBizDocID > 0 Then ' Automatically check selected bizdoc to receive payment
                        If CType(gridRow.FindControl("hdnOppBizDocID"), HiddenField).Value = lngOppBizDocID Then
                            CType(gridRow.FindControl("chk"), CheckBox).Checked = True
                            CType(gridRow.FindControl("txtAmountToPay"), TextBox).Text = String.Format("{0:n}", CCommon.ToDecimal(Replace(CType(gridRow.FindControl("lblBalDue"), Label).Text, ",", "")))
                        End If
                    End If
                Next

                lblAmountToPay.Text = String.Format("{0:###0.00}", CCommon.ToDecimal(txtAmount.Text))
                lblBalanceAmt.Text = String.Format("{0:n}", CCommon.ToDecimal(txtAmount.Text))
                tbl2.Visible = True

                TransactionInfo1.ReferenceType = enmReferenceType.DepositHeader
                TransactionInfo1.ReferenceID = lngDepositeID
                TransactionInfo1.getTransactionInfo()
            Catch ex As Exception
                Throw
            End Try
        End Sub

        'Sub LoadBizDocDetail()
        '    Try
        '        Dim dsBizDocDetail As DataSet
        '        If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
        '        objOppBizDocs.DomainID = Session("DomainID")
        '        objOppBizDocs.OppBizDocId = lngOppBizDocID
        '        dsBizDocDetail = objOppBizDocs.GetBizDocsDetails()
        '        If dsBizDocDetail.Tables(0).Rows.Count > 0 Then
        '            ddlContact.SelectedValue = dsBizDocDetail.Tables(0).Rows(0)("numContactId")
        '            hdnDivisionID.Value = dsBizDocDetail.Tables(0).Rows(0)("numDivisionId")
        '            hdnOppType.Value = dsBizDocDetail.Tables(0).Rows(0)("tintOppType")
        '            hdnAmountPaid.Value = dsBizDocDetail.Tables(0).Rows(0)("monAmountPaid")
        '            hdnBizDocAmount.Value = dsBizDocDetail.Tables(0).Rows(0)("monDealAmount")

        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub
        'Private Sub ManageComission(ByVal numOppBizDocID As Integer)
        '    Try
        '        If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
        '        objOppBizDocs.OppBizDocId = lngOppBizDocID
        '        'Only create journal entries when auth bizdoc's amount is paid
        '        If objOppBizDocs.GetBizDocId() = CCommon.ToLong(Session("AuthoritativeSalesBizDoc")) Then 'Bug fix id 1377
        '            'Load latest Amount paid and BizDoc amount
        '            LoadBizDocDetail()
        '            If CCommon.ToDecimal(hdnAmountPaid.Value) >= CCommon.ToDecimal(hdnBizDocAmount.Value) Then
        '                Dim dtComission As DataTable
        '                Dim objPayRoll As PayrollExpenses
        '                Dim lngComJnID As Long
        '                Dim dblComTotal As Double

        '                objPayRoll = New PayrollExpenses
        '                objPayRoll.DomainId = Session("DomainId")
        '                dtComission = objPayRoll.GetComissionDetails(numOppBizDocID)
        '                If dtComission.Rows.Count = 0 Then Exit Sub

        '                'dblComTotal = CCommon.ToDouble(dtComission.Compute("sum(numComission)", ""))

        '                For Each drComission As DataRow In dtComission.Rows
        '                    dblComTotal = CCommon.ToDouble(drComission("numComission"))
        '                    If dblComTotal > 0 Then
        '                        'Add bill 
        '                        Dim objOppInvoice As New OppInvoice
        '                        objOppInvoice.AmtPaid = dblComTotal
        '                        objOppInvoice.UserCntID = Session("UserContactID")
        '                        objOppInvoice.PaymentMethod = 4 'cash
        '                        objOppInvoice.Reference = "Commission Expenses (" & CCommon.ToString(drComission("vcItemName")) & ") - " & CCommon.ToString(drComission("vcContactName"))
        '                        'objOppInvoice.Memo = txtMemo.Text
        '                        objOppInvoice.DomainId = Session("DomainId")
        '                        'objOppInvoice.ExpenseAcount = ddlExpenseAccount.SelectedValue
        '                        objOppInvoice.DivisionID = hdnDivisionID.Value
        '                        objOppInvoice.LiabilityAccount = 0
        '                        objOppInvoice.DueDate = Now
        '                        objOppInvoice.CurrencyID = Session("BaseCurrencyID")
        '                        objOppInvoice.PaymentType = OppInvoice.BillPaymentType.CommissionExpense
        '                        objOppInvoice.CommissionID = drComission("numComissionID")
        '                        objOppInvoice.OppBizDocId = numOppBizDocID
        '                        objOppInvoice.ContactID = drComission("numcontactid")
        '                        objOppInvoice.AddBillDetails()
        '                    End If
        '                Next
        '            End If
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Private Function UpdateAmountPaidForCash(ByVal objOppInvoice As OppInvoice) As Boolean
        '    Try
        '        objOppInvoice.AmtPaid = AmountToPay
        '        objOppInvoice.UserCntID = Session("UserContactID")
        '        objOppInvoice.OppBizDocId = lngOppBizDocID
        '        objOppInvoice.OppId = lngOppID
        '        objOppInvoice.PaymentMethod = ddlPayment.SelectedValue
        '        objOppInvoice.BizDocsPaymentDetId = IIf(GetQueryStringVal("d") = "", 0, GetQueryStringVal("d"))
        '        If ddlPayment.SelectedValue = 1 Then
        '            objOppInvoice.Reference = ResponseMessage
        '        Else : objOppInvoice.Reference = txtReference.Text
        '        End If
        '        objOppInvoice.IntegratedToAcnt = False
        '        objOppInvoice.Memo = txtMemo.Text
        '        objOppInvoice.DomainID = Session("DomainId")
        '        'If chkDeferredIncome.Checked = True Then
        '        '    If OppType.Value = 1 Then objOppInvoice.bitDeferredIncomeSales = 1
        '        '    objOppInvoice.DeferredIncome = True
        '        '    objOppInvoice.DeferredIncomePeriod = ddlDeferredIncomePeriod.SelectedItem.Value
        '        '    objOppInvoice.DeferredIncomeStartDate = calFrom.SelectedDate
        '        'ElseIf chkContractAmt.Checked = True Then
        '        '    If OppType.Value = 1 Then objOppInvoice.bitDeferredIncomeSales = 1
        '        '    objOppInvoice.DeferredIncome = False
        '        '    objOppInvoice.ContractId = ddlContracts.SelectedItem.Value
        '        'Else
        '        Dim strNow As Date
        '        strNow = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
        '        objOppInvoice.DeferredIncomeStartDate = strNow.Date
        '        'End If
        '        'If trDeferredIncome.Disabled = True And trContractAmount.Disabled = True Then
        '        '    objOppInvoice.bitDisable = True
        '        'Else : objOppInvoice.bitDisable = False
        '        'End If
        '        If objOppInvoice.UpdateAmountPaid() Then
        '            If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
        '            objOppBizDocs.OppBizDocId = lngOppBizDocID
        '            'Only create journal entries when auth bizdoc's amount is paid
        '            If objOppBizDocs.GetBizDocId() = CCommon.ToLong(Session("AuthoritativeSalesBizDoc")) Then 'Bug fix id 1377
        '                'Debit: Un-Deposit Funds
        '                'Credit: Customer A/R account
        '                If hdnOppType.Value = 1 Then 'For SO
        '                    lngJournalID = SaveDataToHeader(AmountToPay, objOppInvoice.BizDocsPaymentDetId, lngOppBizDocID, lngOppID)
        '                    SaveDataToGeneralJournalDetailsForCashAndCheCks(objOppInvoice.BizDocsPaymentDetId)
        '                End If
        '            End If
        '        Else
        '            litMessage.Text = ResponseMessage
        '            Exit Function
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

        'Private Sub btnProcessCard_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnProcessCard.Click
        '    Try
        '        If Save(True) Then
        '            Response.Write("<script>opener.location.reload(true); self.close();</script>")
        '        End If
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub

        Private Sub ddlCards_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCards.SelectedIndexChanged
            Try
                LoadSavedCreditCardInfo()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlCardType_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles ddlCardType.SelectedIndexChanged
            Try
                'validate Card Type Mapping 
                'Get transaction charge for selected credit card type
                If ddlCardType.SelectedValue > 0 Then
                    'SetCreditCardTransValues()
                    If TransactionCharge > 0 And TransactionChargeBareBy = enmTransChargeBareBy.Customer Then
                        TransactionCharge = (TransactionCharge * CCommon.ToDecimal(Replace(lblBalanceAmt.Text, ",", "")) / 100)

                        lblAmountToPay.Text = CCommon.ToDecimal(Replace(lblBalanceAmt.Text, ",", "")) + TransactionCharge
                        txtAmount.Text = CCommon.ToDecimal(Replace(lblBalanceAmt.Text, ",", "")) + TransactionCharge
                        lblTransCharge.Text = CCommon.ToDecimal(TransactionCharge)
                        lblTransChargeLabel.Visible = True
                        lblTransCharge.Visible = True
                    Else
                        'txtAmount.Text = CCommon.ToDecimal(Replace(lblBalanceAmt.Text, ",", ""))
                        lblTransCharge.Text = 0
                        lblTransChargeLabel.Visible = False
                        lblTransCharge.Visible = False
                    End If
                    'If TransactionChargeAccountID = 0 Then litMessage.Text = "Please Set AP account for " & ddlCardType.SelectedItem.Text & " from ""Administration->Domain Details->Accounting->Accounts for Credit Card""."
                Else
                    litMessage.Text = "Please Select a Card Type"
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        'Private Sub SetCreditCardTransValues()
        '    Try
        '        Dim objCOA As New ChartOfAccounting
        '        Dim ds As DataSet
        '        With objCOA
        '            .TransChargeID = 0
        '            .CreditCardTypeId = ddlCardType.SelectedValue
        '            .DomainID = Session("DomainID")
        '            ds = .GetCOACreditCardCharge()
        '            If ds.Tables(0).Rows.Count > 0 Then
        '                TransactionChargeBareBy = CCommon.ToInteger(ds.Tables(0).Rows(0).Item("tintBareBy"))
        '                TransactionChargeAccountID = CCommon.ToLong(ds.Tables(0).Rows(0).Item("numAccountId"))
        '                If TransactionChargeAccountID > 0 Then
        '                    TransactionCharge = CCommon.ToDouble(ds.Tables(0).Rows(0).Item("fltTransactionCharge"))
        '                Else
        '                    TransactionCharge = 0
        '                End If
        '            End If
        '        End With
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        Sub FillDepositTocombo()
            Try
                Dim dtChartAcntDetails As DataTable
                Dim objCOA As New ChartOfAccounting
                objCOA.DomainID = Session("DomainId")
                objCOA.AccountCode = "010101"
                dtChartAcntDetails = objCOA.GetParentCategory()

                'Exclude AR As per discussion with jeff. since it created AR aging summary report balance off.
                Dim drArray() As DataRow = dtChartAcntDetails.Select("vcAccountTypeCode <> '01010105'")
                If drArray.Length > 0 Then
                    dtChartAcntDetails = drArray.CopyToDataTable()
                End If


                Dim item As ListItem
                For Each dr As DataRow In dtChartAcntDetails.Rows
                    item = New ListItem()
                    item.Text = dr("vcAccountName")
                    item.Value = dr("numAccountID")
                    item.Attributes("OptionGroup") = dr("vcAccountType")
                    ddlDepositTo.Items.Add(item)
                Next
                ddlDepositTo.Items.Insert(0, New ListItem("--Select One --", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlDepositTo_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDepositTo.SelectedIndexChanged
            Try
                ddlDepositTo_SelectedIndexChanged()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub ddlDepositTo_SelectedIndexChanged()
            Try
                Dim lobjCashCreditCard As New CashCreditCard
                Dim lstrColor As String
                Dim ldecOpeningBalance As Decimal
                If ddlDepositTo.SelectedItem.Value > 0 Then
                    lobjCashCreditCard.AccountId = ddlDepositTo.SelectedItem.Value
                    lobjCashCreditCard.DomainID = Session("DomainId")
                    ldecOpeningBalance = lobjCashCreditCard.GetCheckOpeningBalanceForCashCredit()
                    lblOpeningBalance.Text = CCommon.ToString(Session("Currency")) + " " + ReturnMoney(CCommon.GetDecimalFormat(ldecOpeningBalance))
                Else
                    lblOpeningBalance.Text = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
            Try
                PersistTable.Clear()
                PersistTable.Add(ddlDepositTo.ID, ddlDepositTo.SelectedValue)
                PersistTable.Add(radDepositeTo.ID, radDepositeTo.Checked)
                PersistTable.Add(radUnderpositedFund.ID, radUnderpositedFund.Checked)
                'PersistTable.Add(ddlCCAction.ID, ddlCCAction.SelectedValue)
                'PersistTable.Add(ddlCardType.ID, ddlCardType.SelectedValue)
                PersistTable.Save(boolOnlyURL:=True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub radCmbCompany_SelectedIndexChanged(sender As Object, e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
            Try
                txtCurrrentPage.Text = 1
                FillContact(ddlContact)
                BindDivisionInformation()
                BindDatagrid()

                BindCreditgrid()
            Catch sqlEx As SqlClient.SqlException
                Dim errorMessages As New System.Text.StringBuilder

                Dim i As Integer = 0
                Do While (i < sqlEx.Errors.Count)
                    errorMessages.Append(("dgOpportunity_ItemDataBound: Index #" _
                                    + (i + ("" & vbLf + ("Message: " _
                                    + (sqlEx.Errors(i).Message + ("" & vbLf + ("LineNumber: " _
                                    + (sqlEx.Errors(i).LineNumber + ("" & vbLf + ("Source: " _
                                    + (sqlEx.Errors(i).Source + ("" & vbLf + ("Procedure: " _
                                    + (sqlEx.Errors(i).Procedure + "" & vbLf)))))))))))))))
                    i = (i + 1)
                Loop

                ExceptionModule.ExceptionPublish(errorMessages.ToString(), Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(sqlEx)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindDivisionInformation()
            Try
                Dim objProspects As New CProspects
                Dim dtComInfo As DataTable
                objProspects.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                objProspects.DomainID = Session("DomainID")
                objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtComInfo = objProspects.GetCompanyInfoForEdit

                If dtComInfo.Rows.Count > 0 Then
                    If Not IsDBNull(dtComInfo.Rows(0).Item("numDefaultPaymentMethod")) AndAlso CCommon.ToLong(dtComInfo.Rows(0).Item("numDefaultPaymentMethod")) > 0 Then
                        If Not ddlPayment.Items.FindByValue(dtComInfo.Rows(0).Item("numDefaultPaymentMethod")) Is Nothing Then
                            ddlPayment.ClearSelection()
                            ddlPayment.Items.FindByValue(dtComInfo.Rows(0).Item("numDefaultPaymentMethod")).Selected = True
                        End If
                    End If
                    chkOnCreditHold.Checked = CCommon.ToBool(dtComInfo.Rows(0).Item("bitOnCreditHold"))
                    hdnDefaultCreditCard.Value = CCommon.ToLong(dtComInfo.Rows(0).Item("numDefaultCreditCard"))
                    BindCardType()
                    'chkOnCreditHold.Checked = CCommon.ToBool(dtComInfo.Rows(0).Item("bitOnCreditHold"))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function FillContact(ByVal ddlCombo As DropDownList)
            Try
                Dim fillCombo As New COpportunities
                With fillCombo
                    .DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                    ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                    ddlCombo.DataTextField = "Name"
                    ddlCombo.DataValueField = "numcontactId"
                    ddlCombo.DataBind()
                End With
                ddlCombo.Items.Insert(0, New ListItem("---Select One---", "0"))
                If ddlCombo.Items.Count >= 2 Then
                    ddlCombo.Items(1).Selected = True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Private Sub CreateJournalEntryForDeposite()
        '    Try
        '        'Select Case ddlPayment.SelectedValue
        '        'Case 5 Or 4 'check or cash
        '        AmountToPay = CCommon.ToDecimal(txtAmount.Text)

        '        If (AmountToPay > 0) Then
        '            lngJournalID = SaveDataToHeader(AmountToPay, 0, 0)
        '            SaveDataToGeneralJournalDetailsForCashAndCheCks()
        '        End If
        '        'Case 1 'credit card
        '        '    AmountToPay = CCommon.ToDecimal(txtAmount.Text)
        '        '    AmountToPay = AmountToPay + (AmountToPay * TransactionCharge / 100)
        '        '    lngJournalID = SaveDataToHeader(CCommon.ToDecimal(txtAmount.Text), 0, 0)
        '        'SaveDataToGeneralJournalDetailsForCreditCard()
        '        'Case Else

        '        'End Select

        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        Private Function ProcessPayment() As Boolean
            Try
                Dim objOppInvoice As New OppInvoice

                If ddlPayment.SelectedValue = 1 And (rdbAuthorizeAndCharge.Checked Or rdbAuthorizeOnly.Checked) Then ' Credit card

                    'If ProcessCard = True Then
                    Dim paymentGateway As Integer = 0
                    Dim dtTable As DataTable
                    objCommon.DomainID = Session("DomainID")
                    If trPaymentGateway.Visible AndAlso ddlPaymentGateWay.Items.Count > 0 Then
                        objCommon.PaymentGWId = CCommon.ToLong(ddlPaymentGateWay.SelectedValue)
                        paymentGateway = CCommon.ToInteger(ddlPaymentGateWay.SelectedValue)
                    End If
                    dtTable = objCommon.GetPaymentGatewayDetails
                    If dtTable.Rows.Count = 0 Then
                        litMessage.Text = "Please Configure Payment Gateway"
                        Return False
                    Else
                        Dim responseCode As String = ""
                        Dim objPaymentGateway As New PaymentGateway
                        objPaymentGateway.DomainID = Session("DomainID")
                        Dim isPaymentProcessed As Boolean = False
                        Dim isCaptureAuthorizeAmountFailed As Boolean = True
                        Dim dtTransaction As DataTable

                        If rdbAuthorizeAndCharge.Checked Then
                            'CHECK IF PREVIOUSLY AUTHORIZED AMOUNT >= AMOUNT TO RECEIVE IS AVAILABLE OR NOT
                            Dim objTrans As New TransactionHistory
                            objTrans.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                            objTrans.DomainID = Session("DomainID")
                            dtTransaction = objTrans.GetAuthrizedTrasaction(CCommon.ToDecimal(txtAmount.Text), 1)

                            If Not dtTransaction Is Nothing AndAlso dtTransaction.Rows.Count > 0 Then
                                isPaymentProcessed = objPaymentGateway.CaptureOrCreditAmount(CCommon.ToString(dtTransaction.Rows(0)("vcTransactionID")), txtAmount.Text, True, ResponseMessage, paymentGateway:=PaymentGateway)

                                If isPaymentProcessed Then
                                    ReturnTransactionID = CCommon.ToString(dtTransaction.Rows(0)("vcTransactionID"))
                                    isCaptureAuthorizeAmountFailed = False

                                    With objTrans
                                        .TransHistoryID = CCommon.ToLong(dtTransaction.Rows(0)("numTransHistoryID"))
                                        .CapturedAmt = CCommon.ToDecimal(txtAmount.Text)
                                        .TransactionStatus = 2 'Captured
                                        .DomainID = Session("DomainID")
                                        .UserCntID = Session("UserContactID")
                                        .PGResponse = ResponseMessage
                                        .PaymentGatewayID = paymentGateway
                                        .ManageTransactionHistory()
                                    End With
                                Else
                                    With objTrans
                                        .TransHistoryID = CCommon.ToLong(dtTransaction.Rows(0)("numTransHistoryID"))
                                        .TransactionStatus = 4 'Failed
                                        .DomainID = Session("DomainID")
                                        .UserCntID = Session("UserContactID")
                                        .PGResponse = ResponseMessage
                                        .PaymentGatewayID = paymentGateway
                                        .ManageTransactionHistory()
                                    End With
                                End If
                            End If
                        End If

                        If isCaptureAuthorizeAmountFailed Then
                            isPaymentProcessed = objPaymentGateway.GatewayTransaction(IIf(IsNumeric(txtAmount.Text), txtAmount.Text, 0), txtCHName.Text, IIf(txtCCNo.Text.Trim().Contains("*"), Session("CCNO"), txtCCNo.Text.Trim()), txtCVV2.Text, ddlMonth.SelectedValue, ddlYear.SelectedValue, CCommon.ToLong(ddlContact.SelectedValue), ResponseMessage, ReturnTransactionID, responseCode, CreditCardAuthOnly:=IIf(rdbAuthorizeOnly.Checked, True, False), CreditCardTrack1:=hdnSwipeTrack1.Value, paymentGateway:=paymentGateway)
                        End If

                        If isPaymentProcessed Then
                            If rdbAuthorizeOnly.Checked Then
                                litMessage.Text = "<span style='color:#00b017;font-weight:bold'>Credit card authorized successfully</span>"
                            Else
                                litMessage.Text = "<span style='color:#00b017;font-weight:bold'>Credit card charged successfully</span>"
                            End If


                            'Insert Transaction details into TransactionHistory table
                            Dim objTransactionHistory As New TransactionHistory
                            objTransactionHistory.UserCntID = Session("UserContactID")
                            objTransactionHistory.DomainID = Session("DomainID")
                            objTransactionHistory.TransHistoryID = 0
                            objTransactionHistory.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                            objTransactionHistory.ContactID = CCommon.ToLong(ddlContact.SelectedValue)
                            objTransactionHistory.OppID = If(rdbAuthorizeOnly.Checked, 0, lngOppID)
                            objTransactionHistory.OppBizDocsID = If(rdbAuthorizeOnly.Checked, 0, lngOppBizDocID)
                            objTransactionHistory.TransactionID = CCommon.ToString(ReturnTransactionID)
                            objTransactionHistory.TransactionStatus = IIf(rdbAuthorizeOnly.Checked, 1, 2)
                            objTransactionHistory.PGResponse = ResponseMessage
                            objTransactionHistory.Type = 2 '1= bizcart & 2 =Internal
                            objTransactionHistory.PaymentGatewayID = paymentGateway
                            If rdbAuthorizeOnly.Checked Then
                                objTransactionHistory.AuthorizedAmt = CCommon.ToDecimal(txtAmount.Text)
                            ElseIf rdbAuthorizeAndCharge.Checked Then
                                objTransactionHistory.CapturedAmt = CCommon.ToDecimal(txtAmount.Text)
                            End If
                            objTransactionHistory.RefundAmt = 0

                            If Not isCaptureAuthorizeAmountFailed AndAlso Not dtTransaction Is Nothing AndAlso dtTransaction.Rows.Count > 0 Then
                                objTransactionHistory.CardHolder = CCommon.ToString(dtTransaction.Rows(0)("vcCardHolder"))
                                objTransactionHistory.CreditCardNo = CCommon.ToString(dtTransaction.Rows(0)("vcCreditCardNo"))
                                objTransactionHistory.Cvv2 = CCommon.ToString(dtTransaction.Rows(0)("vcCVV2"))
                                objTransactionHistory.ValidMonth = CCommon.ToShort(dtTransaction.Rows(0)("tintValidMonth"))
                                objTransactionHistory.ValidYear = CCommon.ToInteger(dtTransaction.Rows(0)("intValidYear"))
                                objTransactionHistory.CardType = CCommon.ToLong(dtTransaction.Rows(0)("numCardType"))
                                objTransactionHistory.SignatureFile = CCommon.ToString(dtTransaction.Rows(0)("vcSignatureFile"))

                            Else
                                objTransactionHistory.CardHolder = objCommon.Encrypt(txtCHName.Text)
                                objTransactionHistory.CreditCardNo = objCommon.Encrypt(IIf(txtCCNo.Text.Trim().Contains("*"), Session("CCNO"), txtCCNo.Text.Trim()))
                                objTransactionHistory.Cvv2 = objCommon.Encrypt(txtCVV2.Text)
                                objTransactionHistory.ValidMonth = ddlMonth.SelectedValue
                                objTransactionHistory.ValidYear = ddlYear.SelectedValue
                                objTransactionHistory.CardType = ddlCardType.SelectedValue
                                objTransactionHistory.SignatureFile = SaveSignetureImage()
                            End If

                            objTransactionHistory.ResponseCode = responseCode
                            lngTransactionHistoryID = objTransactionHistory.ManageTransactionHistory()

                            'If True Then
                            If dtTable.Rows(0)("bitSaveCreditCardInfo") = True And CCommon.ToLong(ddlContact.SelectedValue) > 0 Then
                                'Save Credit Card Details
                                Dim objEncryption As New QueryStringValues
                                With objOppInvoice
                                    .ContactID = CCommon.ToLong(ddlContact.SelectedValue)
                                    .CardHolder = objEncryption.Encrypt(txtCHName.Text.Trim())
                                    .CardTypeID = ddlCardType.SelectedValue
                                    .CreditCardNumber = objEncryption.Encrypt(IIf(txtCCNo.Text.Trim().Contains("*"), Session("CCNO"), txtCCNo.Text.Trim()))
                                    .CVV2 = objEncryption.Encrypt(txtCVV2.Text.Trim())
                                    .ValidMonth = ddlMonth.SelectedValue
                                    .ValidYear = ddlYear.SelectedValue
                                    .UserCntID = Session("UserContactID")
                                    .AddCustomerCreditCardInfo()
                                End With
                            End If

                            litMessage.Text = ResponseMessage

                        Else
                            If rdbAuthorizeOnly.Checked Then
                                litMessage.Text = "Error occurred while charging previously authorized amount:" & ResponseMessage
                            Else
                                litMessage.Text = "Error occurred while processing credit card:" & ResponseMessage
                            End If

                            Return False
                        End If
                    End If
                End If

                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Function SaveSignetureImage() As String
            Try
                If CCommon.ToInteger(GetQueryStringVal("swipe")) = 1 Then

                    Dim sigToImg As SignatureToImage = New SignatureToImage()
                    sigToImg.CanvasHeight = "110"
                    sigToImg.CanvasWidth = "680"
                    Dim strSignatureFile As String = ""
                    strSignatureFile = lngOppBizDocID & "_" & Day(DateTime.Now) & Month(DateTime.Now) & Year(DateTime.Now) & Hour(DateTime.Now) & Minute(DateTime.Now) & Second(DateTime.Now) & ".jpg"

                    Dim signatureImage As Bitmap = sigToImg.SigJsonToImage(hdnSignatureOutput.Value)
                    signatureImage.Save(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strSignatureFile, System.Drawing.Imaging.ImageFormat.Jpeg)

                    Return strSignatureFile
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Sub BindCreditgrid()
            Try
                If CCommon.ToLong(radCmbCompany.SelectedValue) = 0 Then Exit Sub

                Dim objReturns As New Returns
                With objReturns
                    .DivisionId = CCommon.ToLong(radCmbCompany.SelectedValue)
                    .DomainID = Session("DomainID")
                    .ReferenceID = lngDepositeID
                    .Mode = 1
                    .CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                    gvCredits.DataSource = .GetCustomerCredits
                    gvCredits.DataBind()
                End With
                'End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlCurrency_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlCurrency.SelectedIndexChanged
            Try
                txtCurrrentPage.Text = 1
                ddlCurrency_SelectedIndexChanged()
                BindDatagrid()

                BindCreditgrid()
            Catch sqlEx As SqlClient.SqlException
                Dim errorMessages As New System.Text.StringBuilder

                Dim i As Integer = 0
                Do While (i < sqlEx.Errors.Count)
                    errorMessages.Append(("dgOpportunity_ItemDataBound: Index #" _
                                    + (i + ("" & vbLf + ("Message: " _
                                    + (sqlEx.Errors(i).Message + ("" & vbLf + ("LineNumber: " _
                                    + (sqlEx.Errors(i).LineNumber + ("" & vbLf + ("Source: " _
                                    + (sqlEx.Errors(i).Source + ("" & vbLf + ("Procedure: " _
                                    + (sqlEx.Errors(i).Procedure + "" & vbLf)))))))))))))))
                    i = (i + 1)
                Loop

                ExceptionModule.ExceptionPublish(errorMessages.ToString(), Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(sqlEx)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlCurrency_SelectedIndexChanged()
            Try
                Dim objCurrency As New CurrencyRates
                objCurrency.DomainID = Session("DomainID")
                objCurrency.CurrencyID = CCommon.ToLong(ddlCurrency.SelectedValue)
                objCurrency.GetAll = 0
                Dim dt As DataTable = objCurrency.GetCurrencyWithRates()
                If dt.Rows.Count > 0 Then
                    lblBaseCurr.Text = CCommon.ToString(Session("Currency")).Trim()
                    lblForeignCurr.Text = CCommon.ToString(dt.Rows(0)("varCurrSymbol")).Trim()
                    txtExchangeRate.Text = CCommon.ToString(dt.Rows(0)("fltExchangeRate"))


                Else
                    txtExchangeRate.Text = "1"
                    lblForeignCurr.Text = CCommon.ToString(Session("Currency")).Trim()
                End If

                If Session("BaseCurrencyID") = CCommon.ToString(ddlCurrency.SelectedValue) Then
                    txtExchangeRate.Text = 1
                    txtExchangeRate.Enabled = False
                Else
                    txtExchangeRate.Enabled = True
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
            Try
                If lngDepositeID > 0 Then
                    Dim objJournalEntry As New JournalEntry
                    objJournalEntry.JournalId = 0
                    objJournalEntry.BillPaymentID = 0
                    objJournalEntry.DepositId = lngDepositeID
                    objJournalEntry.CheckHeaderID = 0
                    objJournalEntry.BillID = 0
                    objJournalEntry.CategoryHDRID = 0
                    objJournalEntry.ReturnID = 0
                    objJournalEntry.DomainID = Session("DomainId")
                    objJournalEntry.UserCntID = Session("UserContactID")

                    Try
                        objJournalEntry.DeleteJournalEntryDetails()
                        Response.Redirect("../Opportunity/frmAmtPaid.aspx?flag=3", False)
                    Catch ex As Exception
                        If ex.Message = "Undeposited_Account" Then
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('This transaction has been deposited. If you want to change or delete it, you must edit the deposit it appears on and remove it first.');", True)
                            Exit Sub
                        ElseIf ex.Message = "Refund_Payment" Then
                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('This transaction has been used in Refund. If you want to change or delete it, you must edit the Refund and remove it first.');", True)
                            Exit Sub
                        Else
                            Throw ex
                        End If
                    End Try
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgOpportunity_ItemCommand(source As Object, e As DataGridCommandEventArgs) Handles dgOpportunity.ItemCommand
            Try
                If e.CommandName = "ApplyDiscount" Then

                    If CCommon.ToDouble(Session("DiscountServiceItem")) = 0 Then
                        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "AccountValidation", "alert('Please Set Default Shipping Item from Administration->Global Settings->Accounting Tab->Shipping Service Item')", True)
                        litMessage.Text = "Please first set Default Shipping Item from Administration->Global Settings->Accounting Tab->Discount Service Item.."
                        Exit Sub
                    End If

                    Dim dtOrderDetails As DataTable
                    Dim objOpp As New OppotunitiesIP

                    For Each dgItem As DataGridItem In dgOpportunity.Items
                        Dim chkApply As CheckBox
                        chkApply = DirectCast(dgItem.FindControl("chkApply"), CheckBox)
                        If chkApply IsNot Nothing AndAlso chkApply.Checked = True Then

                            Dim hdnOppBizDocID As New HiddenField
                            hdnOppBizDocID = DirectCast(dgItem.FindControl("hdnOppBizDocID"), HiddenField)
                            Dim OppBizDocID As Long = 0
                            If hdnOppBizDocID IsNot Nothing Then
                                OppBizDocID = CCommon.ToLong(hdnOppBizDocID.Value)
                            End If

                            Dim hdnOppID As New HiddenField
                            hdnOppID = DirectCast(dgItem.FindControl("hdnOppID"), HiddenField)
                            Dim OppID As Long = 0 ' CCommon.ToLong(e.CommandArgument)
                            If hdnOppID IsNot Nothing Then
                                OppID = CCommon.ToLong(hdnOppID.Value)
                            End If

                            objOpp.OpportunityId = OppID 'lngOppID
                            objOpp.DomainID = Session("DomainID")
                            objOpp.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                            objOpp.PublicFlag = 0
                            objOpp.UserCntID = Session("UserContactID")
                            objOpp.OpportunityDetails()

                            Dim dtDetails As DataTable
                            Dim objPageLayout As New BACRM.BusinessLogic.Contacts.CPageLayout
                            objPageLayout.OpportunityId = OppID 'lngOppID
                            objPageLayout.DomainID = Session("DomainID")
                            dtDetails = objPageLayout.OpportunityDetails.Tables(0)

                            Dim JournalId As Long
                            Dim lintAuthorizativeBizDocsId As Integer
                            If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                            Dim lintOpportunityType As Integer
                            Dim objJournal As New JournalEntry
                            Dim dtOppBiDocDtl As DataTable
                            objOppBizDocs.OppBizDocId = OppBizDocID 'lngOppBizDocID
                            objOppBizDocs.OppId = OppID 'lngOppID
                            objOppBizDocs.DomainID = Session("DomainID")
                            objOppBizDocs.UserCntID = Session("UserContactID")
                            objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                            dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl
                            lintOpportunityType = objOppBizDocs.GetOpportunityType()
                            lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()

                            If dtOppBiDocDtl.Rows.Count <> 0 Then

                                'bAddBizDoc = IIf(OppBizDocID = 0, True, False)

                                objOppBizDocs.OppBizDocId = OppBizDocID
                                objOppBizDocs.BizDocId = CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numBizDocId"))
                                objOppBizDocs.UserCntID = Session("UserContactID")
                                objOppBizDocs.OppType = objOppBizDocs.GetOpportunityType()
                                objOppBizDocs.bitPartialShipment = CCommon.ToBool(dtOppBiDocDtl.Rows(0).Item("bitPartialFulfilment"))
                                objOppBizDocs.ShipCompany = CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("numShipVia"))
                                objOppBizDocs.FromDate = CCommon.ToSqlDate(dtOppBiDocDtl.Rows(0).Item("dtFromDate"))
                                objOppBizDocs.SequenceId = CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numSequenceId"))
                                objOppBizDocs.vcComments = Replace(dtOppBiDocDtl.Rows(0).Item("vcComments").ToString, "'", "''")
                                objOppBizDocs.vcPONo = "" 'hdnPONumber.Value
                                objOppBizDocs.Deferred = IIf(CCommon.ToBool(dtOppBiDocDtl.Rows(0).Item("tintDeferred")) = True, 1, 0)
                                objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                                objOppBizDocs.boolRentalBizDoc = CCommon.ToBool(dtOppBiDocDtl.Rows(0).Item("bitRentalBizDoc"))
                                objOppBizDocs.BizDocTemplateID = CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numBizDocTempID"))
                                objOppBizDocs.TrackingNo = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcTrackingNo"))
                                objOppBizDocs.RefOrderNo = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcRefOrderNo"))
                                objOppBizDocs.fltExchangeRateBizDoc = CCommon.ToDecimal(dtOppBiDocDtl.Rows(0).Item("fltExchangeRateBizDoc"))

                                Dim dtItems As DataTable
                                dsTemp = objOpp.ItemsByOppId
                                dtItems = dsTemp.Tables(0)

                                Dim dtTable, dtItemTax As DataTable
                                Dim objItems As New CItems
                                Dim strUnit As String
                                Dim objOpportunity As New MOpportunity
                                Dim dblDiscount As Double

                                objOpportunity.OppItemCode = 0
                                objOpportunity.OpportunityId = CCommon.ToLong(GetQueryStringVal("opid"))
                                objOpportunity.DomainID = Session("DomainID")
                                objOpportunity.Mode = 2
                                objItems.ItemCode = CCommon.ToDouble(Session("DiscountServiceItem"))
                                dtTable = objItems.ItemDetails
                                If dtTable.Rows.Count > 0 Then
                                    dblDiscount = CCommon.ToDouble(DirectCast(dgItem.FindControl("hdnDiscount"), HiddenField).Value)
                                    dblDiscount = ((CCommon.ToDouble(DirectCast(dgItem.FindControl("txtAmountToPay"), TextBox).Text) - CCommon.ToDouble(DirectCast(dgItem.FindControl("hdnShippingCharge"), HiddenField).Value)) * dblDiscount) / 100
                                    AddDiscountItem(objItems, dtTable, dblDiscount, "Term Discount-Ref:" & If(DirectCast(dgItem.FindControl("hdnBizDocID"), HiddenField) IsNot Nothing, DirectCast(dgItem.FindControl("hdnBizDocID"), HiddenField).Value, ""))
                                    DirectCast(dgItem.FindControl("txtAmountToPay"), TextBox).Text = CCommon.ToDouble(DirectCast(dgItem.FindControl("txtAmountToPay"), TextBox).Text) - dblDiscount
                                End If

                                objOpp.OppType = lintOpportunityType
                                If Not Session("NewItems") Is Nothing Then
                                    dsTemp = Session("NewItems")
                                    dsTemp.Tables(0).TableName = "Item"
                                    objOpp.strItems = "<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & dsTemp.GetXml
                                    If objOpp.strItems.Contains("<numWarehouseItmsID>0</numWarehouseItmsID>") OrElse _
                                       objOpp.strItems.Contains("<numWarehouseItmsID></numWarehouseItmsID>") Then
                                        objOpp.strItems = objOpp.strItems.Replace("<numWarehouseItmsID>0</numWarehouseItmsID>", "")
                                        objOpp.strItems = objOpp.strItems.Replace("<numWarehouseItmsID></numWarehouseItmsID>", "")
                                    End If
                                End If
                                objOpp.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                                Dim arrOutPut() As String = New String(2) {}
                                arrOutPut = objOpp.Save()
                                objOpp.OpportunityId = arrOutPut(0)

                                If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                                objOppBizDocs.DomainID = Session("DomainID")
                                objOppBizDocs.OppId = OppID 'lngOppID
                                objOppBizDocs.BizDocId = lintAuthorizativeBizDocsId 'objOppBizDocs.GetAuthorizativeOpportuntiy()
                                objOppBizDocs.OppBizDocId = OppBizDocID 'OppBizDocID

                                Dim dtOppItems As DataTable = objOppBizDocs.GetOppItemsForBizDocs()
                                Dim dsBizItems As DataSet = objOppBizDocs.GetBizDocItems()

                                'If CCommon.ToBool(Session("bitRentalItem")) = True AndAlso CCommon.ToLong(Session("RentalItemClass")) > 0 Then
                                '    Dim view As DataView = New DataView
                                '    view.Table = dtOppItems
                                '    view.RowFilter = "numItemClassification<>" & CCommon.ToLong(Session("RentalItemClass"))
                                'End If

                                Dim dsBizDocItems As New DataSet
                                Dim dtBizDocItems As New DataTable
                                If dtOppItems IsNot Nothing AndAlso dtOppItems.Rows.Count > 0 Then

                                    If dsBizItems IsNot Nothing AndAlso dsBizItems.Tables.Count > 0 AndAlso dsBizItems.Tables(0).Rows.Count > 0 Then
                                        If dsBizItems.Tables(0).Select("numItemCode=" & CCommon.ToDouble(Session("DiscountServiceItem")) & " AND vcItemDesc ilike '%Term Discount%'").Length > 0 Then
                                            litMessage.Text = "Term Discount is already applied to this bizdoc."
                                            Continue For
                                        End If
                                    End If

                                    dtBizDocItems.TableName = "BizDocItems"
                                    dtBizDocItems.Columns.Add("numItemCode")
                                    dtBizDocItems.Columns.Add("OppItemID")
                                    dtBizDocItems.Columns.Add("Quantity")
                                    dtBizDocItems.Columns.Add("Notes")
                                    dtBizDocItems.Columns.Add("monPrice")
                                    dtBizDocItems.Columns.Add("vcItemDesc")
                                    dtBizDocItems.AcceptChanges()

                                    Dim drNew As DataRow
                                    For Each drItem As DataRow In dtOppItems.Rows
                                        drNew = dtBizDocItems.NewRow
                                        drNew("numItemCode") = CCommon.ToDouble(drItem("numItemCode"))
                                        drNew("OppItemID") = CCommon.ToDouble(drItem("numoppitemtCode"))
                                        drNew("Quantity") = CCommon.ToDouble(drItem("QtyOrdered"))
                                        drNew("Notes") = CCommon.ToString(drItem("vcNotes"))
                                        drNew("monPrice") = CCommon.ToDouble(drItem("monOppItemPrice"))
                                        If CCommon.ToDouble(Session("DiscountServiceItem")) = CCommon.ToDouble(drItem("numItemCode")) Then
                                            drNew("vcItemDesc") = "Term Discount-Ref:" & If(DirectCast(dgItem.FindControl("hdnBizDocID"), HiddenField) IsNot Nothing, DirectCast(dgItem.FindControl("hdnBizDocID"), HiddenField).Value, "")
                                        Else
                                            drNew("vcItemDesc") = CCommon.ToString(drItem("vcNotes"))
                                        End If

                                        dtBizDocItems.Rows.Add(drNew)
                                        dtBizDocItems.AcceptChanges()

                                    Next
                                    dsBizDocItems.Tables.Add(dtBizDocItems)
                                    dsBizDocItems.Tables(0).TableName = "BizDocItems"
                                    If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                                    objOppBizDocs.DomainID = Session("DomainID")
                                    objOppBizDocs.OppId = OppID 'lngOppID
                                    objOppBizDocs.BizDocId = lintAuthorizativeBizDocsId 'objOppBizDocs.GetAuthorizativeOpportuntiy()
                                    objOppBizDocs.OppBizDocId = OppBizDocID 'OppBizDocID

                                    objOppBizDocs.strBizDocItems = dsBizDocItems.GetXml()

                                    Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                        Try
                                            OppBizDocID = objOppBizDocs.SaveBizDoc()

                                            If lintOpportunityType = 1 Then

                                                Dim ds As New DataSet
                                                Dim dtOppBiDocItems As New DataTable
                                                objOppBizDocs.OppBizDocId = OppID 'lngOppID
                                                objOppBizDocs.OppBizDocId = OppBizDocID
                                                objOppBizDocs.UserCntID = Session("UserContactID")
                                                objOppBizDocs.DomainID = Session("DomainID")
                                                ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                                                dtOppBiDocItems = ds.Tables(0)

                                                Dim objCalculateDealAmount As New CalculateDealAmount
                                                objCalculateDealAmount.CalculateDealAmount(OppID, OppBizDocID, lintOpportunityType, Session("DomainID"), dtOppBiDocItems)

                                                'Get existing journal id and delete old and insert new journal entries
                                                JournalId = objOppBizDocs.GetJournalIdForAuthorizativeBizDocs
                                                'When From COA someone deleted journals then we need to create new journal header entry
                                                If JournalId = 0 Then
                                                    JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, _
                                                                                               Date.UtcNow, _
                                                                                               JournalId,
                                                                                               If(DirectCast(dgItem.FindControl("hdnBizDocID"), HiddenField) IsNot Nothing, DirectCast(dgItem.FindControl("hdnBizDocID"), HiddenField).Value, ""))
                                                Else
                                                    JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, _
                                                                                               Date.UtcNow, _
                                                                                               JournalId, _
                                                                                               If(DirectCast(dgItem.FindControl("hdnBizDocID"), HiddenField) IsNot Nothing, DirectCast(dgItem.FindControl("hdnBizDocID"), HiddenField).Value, ""))
                                                End If

                                                Dim objJournalEntries As New JournalEntry
                                                If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                                                    objJournalEntries.SaveJournalEntriesSales(OppID, Session("DomainID"), dtOppBiDocItems, JournalId, OppBizDocID, _
                                                                                          objCalculateDealAmount.GrandTotal, _
                                                                                          objCalculateDealAmount.TotalDiscount, _
                                                                                          objCalculateDealAmount.ShippingAmount, _
                                                                                          objCalculateDealAmount.TotalLateCharges, _
                                                                                          CCommon.ToLong(dtDetails.Rows(0).Item("numDivisionID")), _
                                                                                          CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numCurrencyID")), _
                                                                                          CCommon.ToDecimal(dtOppBiDocDtl.Rows(0).Item("fltExchangeRateBizDoc")), _
                                                                                          CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numDiscountAcntType").ToString), _
                                                                                          CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("numShipVia")))
                                                Else
                                                    objJournalEntries.SaveJournalEntriesSalesNew(OppID, Session("DomainID"), dtOppBiDocItems, JournalId, OppBizDocID, _
                                                                                          objCalculateDealAmount.GrandTotal, _
                                                                                          objCalculateDealAmount.TotalDiscount, _
                                                                                          objCalculateDealAmount.ShippingAmount, _
                                                                                          objCalculateDealAmount.TotalLateCharges, _
                                                                                          CCommon.ToLong(dtDetails.Rows(0).Item("numDivisionID")), _
                                                                                          CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numCurrencyID")), _
                                                                                          CCommon.ToDecimal(dtOppBiDocDtl.Rows(0).Item("fltExchangeRateBizDoc")), _
                                                                                          CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numDiscountAcntType").ToString), _
                                                                                          CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("numShipVia")))
                                                End If


                                            End If

                                            objTransactionScope.Complete()
                                        Catch ex As Exception
                                            If ex.Message = "NOT_ALLOWED" Then
                                                litMessage.Text = "To split order items multiple bizdocs you must create all bizdocs with ""partial"" checked!"
                                                ' isExitSub = True : Exit Sub
                                            ElseIf ex.Message = "NOT_ALLOWED_FulfillmentOrder" Then
                                                litMessage.Text = "Only a Sales Order can create a Fulfillment Order"
                                                ' isExitSub = True : Exit Sub
                                            ElseIf ex.Message = "AlreadyInvoice" Then
                                                litMessage.Text = "You can�t create a Invoice against a Fulfillment Order that already contains a Invoice"
                                                'isExitSub = True : Exit Sub
                                            ElseIf ex.Message = "AlreadyPackingSlip" Then
                                                litMessage.Text = "You can�t create a Packing-Slip against a Fulfillment Order that already contains a Packing-Slip"
                                                'isExitSub = True : Exit Sub
                                            ElseIf ex.Message = "AlreadyInvoice_NOT_ALLOWED_FulfillmentOrder" Then
                                                litMessage.Text = "You can�t create a fulfillment order against a sales order that already contains an Invoice or Packing-Slip"
                                                'isExitSub = True : Exit Sub
                                            ElseIf ex.Message = "AlreadyFulfillmentOrder" Then
                                                litMessage.Text = "Manually adding Invoices or Packing Slips to a Sales Order is not allowed after a Fulfillment Order (FO) is added. Your options are to edit the FO status to trigger creation of Invoices and/or Packing Slips, Delete the Fulfillment Order, or Create Invoices and/or Packing Slips from the Sales Fulfillment section"
                                                'isExitSub = True : Exit Sub
                                            Else
                                                Throw ex
                                            End If
                                        End Try
                                    End Using

                                    ''Added By Sachin Sadhu||Date:1stMay2014
                                    ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                                    ''          Using Change tracking
                                    Dim objWfA As New Workflow()
                                    objWfA.DomainID = Session("DomainID")
                                    objWfA.UserCntID = Session("UserContactID")
                                    objWfA.RecordID = OppBizDocID
                                    objWfA.SaveWFBizDocQueue()
                                    'end of code
                                End If

                            End If

                        End If

                    Next

                End If

                BindDatagrid()
            Catch sqlEx As SqlClient.SqlException
                Dim errorMessages As New System.Text.StringBuilder

                Dim i As Integer = 0
                Do While (i < sqlEx.Errors.Count)
                    errorMessages.Append(("dgOpportunity_ItemDataBound: Index #" _
                                    + (i + ("" & vbLf + ("Message: " _
                                    + (sqlEx.Errors(i).Message + ("" & vbLf + ("LineNumber: " _
                                    + (sqlEx.Errors(i).LineNumber + ("" & vbLf + ("Source: " _
                                    + (sqlEx.Errors(i).Source + ("" & vbLf + ("Procedure: " _
                                    + (sqlEx.Errors(i).Procedure + "" & vbLf)))))))))))))))
                    i = (i + 1)
                Loop

                ExceptionModule.ExceptionPublish(errorMessages.ToString(), Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(sqlEx)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgOpportunity_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles dgOpportunity.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Header Then
                    If Not String.IsNullOrEmpty(UserSelectedInvoices.Trim()) Then
                        txtAmount.Text = "0"
                        listTempSelectedInvoices = Newtonsoft.Json.JsonConvert.DeserializeObject(Of System.Collections.Generic.List(Of SelectedInvoice))(UserSelectedInvoices.Trim())
                    Else
                        listTempSelectedInvoices = Nothing
                    End If
                ElseIf e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                    'Dim lblBillingDate As Label
                    'lblBillingDate = DirectCast(e.Item.FindControl("lblBillingDate"), Label)

                    If CCommon.ToBool(DataBinder.Eval(e.Item.DataItem, "IsDiscountItemAdded")) = True OrElse CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numDiscount")) = 0 Then
                        DirectCast(e.Item.FindControl("chkApply"), CheckBox).Visible = False
                        DirectCast(e.Item.FindControl("lblApply"), Label).Visible = False
                    Else
                        DirectCast(e.Item.FindControl("chkApply"), CheckBox).Visible = True
                        DirectCast(e.Item.FindControl("lblApply"), Label).Visible = True
                    End If

                    Dim dateToCheck As Date
                    dateToCheck = DateAdd(DateInterval.Day, CCommon.ToDouble(DataBinder.Eval(e.Item.DataItem, "numDiscountPaidInDays")), CCommon.ToSqlDate(DataBinder.Eval(e.Item.DataItem, "dtOrigFromDate")))
                    If CCommon.ToSqlDate(DataBinder.Eval(e.Item.DataItem, "dtOrigFromDate")) <> Nothing Then

                        If Date.Compare(dateToCheck, CCommon.ToSqlDate(calDate.SelectedDate)) > 0 Then
                            'calDate.SelectedDate is earlier than dateToCheck. Apply Discount 
                            DirectCast(e.Item.FindControl("lblApply"), Label).Text = CCommon.ToDouble(DataBinder.Eval(e.Item.DataItem, "numDiscount")) & "% Discount"

                        ElseIf Date.Compare(dateToCheck, CCommon.ToSqlDate(calDate.SelectedDate)) = 0 Then
                            'calDate.SelectedDate t1 is the same as dateToCheck.  Apply Discount
                            DirectCast(e.Item.FindControl("lblApply"), Label).Text = CCommon.ToDouble(DataBinder.Eval(e.Item.DataItem, "numDiscount")) & "% Discount"

                        ElseIf Date.Compare(dateToCheck, CCommon.ToSqlDate(calDate.SelectedDate)) < 0 Then
                            'Greater than zero calDate.SelectedDate is later than dateToCheck.  Do not Apply Discount
                            DirectCast(e.Item.FindControl("lblApply"), Label).Text = "Not Applicable"
                            DirectCast(e.Item.FindControl("chkApply"), CheckBox).Visible = False

                        End If
                    End If
                    Dim hdnOppBizDocID As New HiddenField
                    hdnOppBizDocID = DirectCast(e.Item.FindControl("hdnOppBizDocID"), HiddenField)
                    Dim OppBizDocID As Long = 0
                    If hdnOppBizDocID IsNot Nothing Then
                        OppBizDocID = CCommon.ToLong(hdnOppBizDocID.Value)
                    End If

                    Dim hdnOppID As New HiddenField
                    hdnOppID = DirectCast(e.Item.FindControl("hdnOppID"), HiddenField)
                    Dim OppID As Long = 0 ' CCommon.ToLong(e.CommandArgument)
                    If hdnOppID IsNot Nothing Then
                        OppID = CCommon.ToLong(hdnOppID.Value)
                    End If
                    Dim lblBizDocStatus As New Label
                    lblBizDocStatus = DirectCast(e.Item.FindControl("lblBizDocStatus"), Label)

                    'Added by: Sachin Sadhu||Date:21stApril2014
                    'Purpose :To Display bizDoc status i.e. approved ,rejected,or pending
                    'Assigned By :Carl



                    Dim dtOppBiDocDtl As DataTable
                    If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs


                    objOppBizDocs.OppBizDocId = OppBizDocID
                    objOppBizDocs.OppId = OppID
                    objOppBizDocs.DomainID = Session("DomainID")
                    objOppBizDocs.UserCntID = Session("UserContactID")
                    objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl
                    If dtOppBiDocDtl.Rows.Count = 0 Then Exit Sub
                    If CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("Pending")) > 0 Then
                        lblBizDocStatus.Text = "Pending"
                        DirectCast(e.Item.FindControl("chk"), CheckBox).Visible = False
                        DirectCast(e.Item.FindControl("lblCheck"), Label).Visible = True
                        DirectCast(e.Item.FindControl("lblBizDocStatus"), Label).ForeColor = Color.Red
                        DirectCast(e.Item.FindControl("lblBizDocStatus"), Label).Font.Bold = True
                    ElseIf CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("Declined")) > 0 Then
                        lblBizDocStatus.Text = "Declined"
                        DirectCast(e.Item.FindControl("chk"), CheckBox).Visible = False
                        DirectCast(e.Item.FindControl("lblCheck"), Label).Visible = True
                        DirectCast(e.Item.FindControl("lblBizDocStatus"), Label).ForeColor = Color.Red
                        DirectCast(e.Item.FindControl("lblBizDocStatus"), Label).Font.Bold = True
                    ElseIf CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("Approved")) > 0 Then
                        lblBizDocStatus.Text = "Approved by All"
                        DirectCast(e.Item.FindControl("chk"), CheckBox).Enabled = True
                        DirectCast(e.Item.FindControl("lblCheck"), Label).Visible = False
                        DirectCast(e.Item.FindControl("lblBizDocStatus"), Label).ForeColor = Color.Green
                        DirectCast(e.Item.FindControl("lblBizDocStatus"), Label).Font.Bold = True
                    ElseIf CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("AppReq")) > 0 Then
                        lblBizDocStatus.Text = "Requested for Approval"
                        DirectCast(e.Item.FindControl("chk"), CheckBox).Enabled = False
                        DirectCast(e.Item.FindControl("lblCheck"), Label).Visible = True
                        DirectCast(e.Item.FindControl("lblBizDocStatus"), Label).ForeColor = Color.Blue
                        DirectCast(e.Item.FindControl("lblBizDocStatus"), Label).Font.Bold = True
                        ' AppReq
                    Else
                        lblBizDocStatus.Text = "-"
                        DirectCast(e.Item.FindControl("chk"), CheckBox).Enabled = True
                        DirectCast(e.Item.FindControl("lblCheck"), Label).Visible = False
                    End If
                    'end of Code

                    If Not listTempSelectedInvoices Is Nothing Then
                        If DirectCast(e.Item.FindControl("chk"), CheckBox).Visible AndAlso DirectCast(e.Item.FindControl("chk"), CheckBox).Enabled Then
                            If listTempSelectedInvoices.Where(Function(x) x.numOppBizDocsID = CCommon.ToDouble(DataBinder.Eval(e.Item.DataItem, "numOppBizDocsID"))).Count > 0 Then
                                DirectCast(e.Item.FindControl("chk"), CheckBox).Checked = True
                                DirectCast(e.Item.FindControl("txtAmountToPay"), TextBox).Text = listTempSelectedInvoices.First(Function(x) x.numOppBizDocsID = CCommon.ToDouble(DataBinder.Eval(e.Item.DataItem, "numOppBizDocsID"))).monAmountPaid
                            End If
                        End If
                    End If
                End If
            Catch sqlEx As SqlClient.SqlException
                Dim errorMessages As New System.Text.StringBuilder

                Dim i As Integer = 0
                Do While (i < sqlEx.Errors.Count)
                    errorMessages.Append(("dgOpportunity_ItemDataBound: Index #" _
                                    + (i + ("" & vbLf + ("Message: " _
                                    + (sqlEx.Errors(i).Message + ("" & vbLf + ("LineNumber: " _
                                    + (sqlEx.Errors(i).LineNumber + ("" & vbLf + ("Source: " _
                                    + (sqlEx.Errors(i).Source + ("" & vbLf + ("Procedure: " _
                                    + (sqlEx.Errors(i).Procedure + "" & vbLf)))))))))))))))
                    i = (i + 1)
                Loop

                ExceptionModule.ExceptionPublish(errorMessages.ToString(), Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(sqlEx)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnGo_Click(sender As Object, e As EventArgs) Handles btnGo.Click
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch sqlEx As SqlClient.SqlException
                Dim errorMessages As New System.Text.StringBuilder

                Dim i As Integer = 0
                Do While (i < sqlEx.Errors.Count)
                    errorMessages.Append(("dgOpportunity_ItemDataBound: Index #" _
                                    + (i + ("" & vbLf + ("Message: " _
                                    + (sqlEx.Errors(i).Message + ("" & vbLf + ("LineNumber: " _
                                    + (sqlEx.Errors(i).LineNumber + ("" & vbLf + ("Source: " _
                                    + (sqlEx.Errors(i).Source + ("" & vbLf + ("Procedure: " _
                                    + (sqlEx.Errors(i).Procedure + "" & vbLf)))))))))))))))
                    i = (i + 1)
                Loop

                ExceptionModule.ExceptionPublish(errorMessages.ToString(), Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(sqlEx)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Dim dsTemp As DataSet
        Private Sub AddDiscountItem(ByVal objItems As CItems, ByVal dtTable As DataTable, ByVal dblDiscount As Decimal, ByVal strDescForServiceItem As String)
            Try
                If dsTemp Is Nothing Then
                    dsTemp = Session("NewItems")
                End If

                objCommon = New CCommon
                Dim dtUnit As DataTable
                objCommon.DomainID = Session("DomainID")
                objCommon.ItemCode = CCommon.ToDouble(Session("DiscountServiceItem"))
                objCommon.UOMAll = True
                dtUnit = objCommon.GetItemUOM()

                Dim dtItemTax As DataTable
                'Dim objItems As New CItems
                Dim strUnit As String
                'Dim objOpportunity As New MOpportunity
                'objOpportunity.OppItemCode = 0
                'objOpportunity.OpportunityId = CCommon.ToLong(GetQueryStringVal("opid"))
                'objOpportunity.DomainID = Session("DomainID")
                'objOpportunity.Mode = 2
                'objItems.ItemCode = CCommon.ToDouble(Session("DiscountServiceItem"))
                'dtTable = objItems.ItemDetails
                strUnit = "numSaleUnit"

                Dim strApplicable As String
                objItems.DomainID = Session("DomainID")
                dtItemTax = objItems.ItemTax()
                For Each dr As DataRow In dtItemTax.Rows
                    strApplicable = strApplicable & dr("bitApplicable") & ","
                Next

                If dtTable.Rows.Count > 0 Then

                    Dim PageType As Integer

                    PageType = 1
                    strApplicable = strApplicable & dtTable.Rows(0).Item("bitTaxable")
                    objItems.ItemName = CCommon.ToString(dtTable.Rows(0).Item("vcItemName"))
                    objItems.type = CCommon.ToInteger(dtTable.Rows(0).Item("tintStandardProductIDType"))
                    objItems.ItemDesc = strDescForServiceItem
                    objItems.Description = strDescForServiceItem
                    objItems.ModelID = CCommon.ToString(dtTable.Rows(0).Item("vcModelID"))
                    objItems.PathForImage = CCommon.ToString(dtTable.Rows(0).Item("vcPathForImage"))
                    objItems.ItemType = dtTable.Rows(0).Item("ItemType")

                    objItems.AllowDropShip = CCommon.ToBool(dtTable.Rows(0).Item("bitAllowDropShip"))
                    objItems.VendorID = CCommon.ToLong(dtTable.Rows(0).Item("numVendorID"))
                    objItems.IsPrimaryVendor = True

                    objItems.Weight = CCommon.ToDouble(dtTable.Rows(0).Item("fltWeight"))
                    objItems.FreeShipping = CCommon.ToBool(dtTable.Rows(0).Item("bitFreeShipping"))
                    objItems.Height = CCommon.ToDouble(dtTable.Rows(0).Item("fltHeight"))
                    objItems.Width = CCommon.ToDouble(dtTable.Rows(0).Item("fltWidth"))
                    objItems.Length = CCommon.ToDouble(dtTable.Rows(0).Item("fltLength"))

                    'Dim dtUnit As DataTable
                    If strUnit.Length > 0 Then
                        objItems.UnitofMeasure = dtTable.Rows(0).Item("vcUnitofMeasure")

                        objItems.BaseUnit = dtTable.Rows(0).Item("numBaseUnit")
                        objItems.PurchaseUnit = dtTable.Rows(0).Item("numPurchaseUnit")
                        objItems.SaleUnit = dtTable.Rows(0).Item("numSaleUnit")

                        objCommon.DomainID = Session("DomainID")
                        objCommon.ItemCode = objItems.ItemCode
                        objCommon.UnitId = dtTable.Rows(0).Item("numSaleUnit")
                        dtUnit = objCommon.GetItemUOMConversion()
                        'hdvUOMConversionFactor.Value = dtUnit(0)("BaseUOMConversion")
                        'hdvPurchaseUOMConversionFactor.Value = dtUnit(0)("PurchaseUOMConversion")
                        'hdvSalesUOMConversionFactor.Value = dtUnit(0)("SaleUOMConversion")

                    End If

                    Dim dtTaxTypes As DataTable
                    Dim ObjTaxItems As New TaxDetails
                    ObjTaxItems.DomainID = Session("DomainID")
                    dtTaxTypes = ObjTaxItems.GetTaxItems

                    For Each drTax As DataRow In dtTaxTypes.Rows
                        dtTable.Columns.Add("Tax" & drTax("numTaxItemID"), GetType(Decimal))
                        dtTable.Columns.Add("bitTaxable" & drTax("numTaxItemID"))
                    Next

                    Dim dr As DataRow
                    dr = dtTaxTypes.NewRow
                    dr("numTaxItemID") = 0
                    dr("vcTaxName") = "Sales Tax(Default)"
                    dtTaxTypes.Rows.Add(dr)

                    Dim chkTaxItemsForService As New CheckBoxList
                    chkTaxItemsForService.DataTextField = "vcTaxName"
                    chkTaxItemsForService.DataValueField = "numTaxItemID"
                    chkTaxItemsForService.DataSource = dtTaxTypes
                    chkTaxItemsForService.DataBind()

                    If Not dsTemp.Tables(0).Columns.Contains("Op_Flag") Then dsTemp.Tables(0).Columns.Add("Op_Flag")
                    If Not dsTemp.Tables(0).Columns.Contains("bitIsAuthBizDoc") Then dsTemp.Tables(0).Columns.Add("bitIsAuthBizDoc")
                    If Not dsTemp.Tables(0).Columns.Contains("numUnitHourReceived") Then dsTemp.Tables(0).Columns.Add("numUnitHourReceived")
                    If Not dsTemp.Tables(0).Columns.Contains("numQtyShipped") Then dsTemp.Tables(0).Columns.Add("numQtyShipped")
                    If Not dsTemp.Tables(0).Columns.Contains("numUOM") Then dsTemp.Tables(0).Columns.Add("numUOM")
                    If Not dsTemp.Tables(0).Columns.Contains("vcUOMName") Then dsTemp.Tables(0).Columns.Add("vcUOMName")
                    If Not dsTemp.Tables(0).Columns.Contains("UOMConversionFactor") Then dsTemp.Tables(0).Columns.Add("UOMConversionFactor")
                    If Not dsTemp.Tables(0).Columns.Contains("monTotAmtBefDiscount") Then dsTemp.Tables(0).Columns.Add("monTotAmtBefDiscount")
                    If Not dsTemp.Tables(0).Columns.Contains("vcBaseUOMName") Then dsTemp.Tables(0).Columns.Add("vcBaseUOMName")
                    If Not dsTemp.Tables(0).Columns.Contains("bitDiscountType") Then dsTemp.Tables(0).Columns.Add("bitDiscountType")
                    If Not dsTemp.Tables(0).Columns.Contains("numVendorWareHouse") Then dsTemp.Tables(0).Columns.Add("numVendorWareHouse")
                    If Not dsTemp.Tables(0).Columns.Contains("numShipmentMethod") Then dsTemp.Tables(0).Columns.Add("numShipmentMethod")
                    If Not dsTemp.Tables(0).Columns.Contains("numSOVendorId") Then dsTemp.Tables(0).Columns.Add("numSOVendorId")
                    If Not dsTemp.Tables(0).Columns.Contains("numWarehouseID") Then dsTemp.Tables(0).Columns.Add("numWarehouseID")
                    If Not dsTemp.Tables(0).Columns.Contains("numWarehouseItmsID") Then dsTemp.Tables(0).Columns.Add("numWarehouseItmsID")
                    If Not dsTemp.Tables(0).Columns.Contains("vcSKU") Then dsTemp.Tables(0).Columns.Add("vcSKU")

                    Session("NewItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon, dsTemp, True, objItems.ItemType, objItems.ItemType, objItems.AllowDropShip,
                                                                                         objItems.KitParent, CCommon.ToDouble(Session("DiscountServiceItem")), 1, -dblDiscount,
                                                                                         objItems.ItemDesc, 0, "Term Discount", "", 0, CCommon.ToString(dtTable.Rows(0).Item("vcModelID")).Trim, 0, "", 1,
                                                                                         PageType.ToString(), False, 0, strApplicable, objItems.Tax, "", chkTaxItemsForService, False, "", Nothing,
                                                                                         0, 0, 0, 0, 0, 0, True, 0, 0, 0, 0, 0, 0, 0)


                    Dim drRow As DataRow
                    Dim dsItems As DataSet = DirectCast(Session("NewItems"), DataSet)
                    For Each drFlag As DataRow In dsItems.Tables(0).Rows
                        drFlag("Op_Flag") = 2
                        dsItems.Tables(0).AcceptChanges()
                    Next

                    drRow = dsItems.Tables(0).Select("numItemCode = " & CCommon.ToDouble(Session("DiscountServiceItem")))(0)
                    If drRow IsNot Nothing Then
                        drRow("tintOppType") = 1
                        drRow("numOppId") = lngOppID
                        drRow("numReturnID") = 0
                        drRow("Op_Flag") = 1
                        drRow("numOriginalUnitHour") = 1
                        drRow("Source") = ""
                        drRow("numSourceID") = 0
                        drRow("bitKitParent") = 0
                        drRow("numShipClass") = 0
                        'drRow("vcPathForImage") = ""
                        drRow("vcItemName") = "Term Discount"
                        drRow("vcModelId") = objItems.ModelID
                        drRow("vcWarehouse") = ""
                        drRow("numOnHand") = 1
                        drRow("vcLocation") = ""
                        drRow("vcItemDesc") = objItems.ItemDesc
                        drRow("vcAttributes") = ""
                        drRow("DropShip") = False
                        drRow("bitDropShip") = False
                        drRow("vcSKU") = ""
                        drRow("vcManufacturer") = ""
                        drRow("numItemClassification") = objItems.ItemClassification
                        drRow("numItemGroup") = objItems.ItemGroupID
                        drRow("fltWeight") = 0
                        drRow("numBarCodeId") = 0
                        drRow("vcWorkOrderStatus") = ""
                        drRow("DiscAmt") = 0
                        drRow("bitItemAddedToAuthBizDoc") = True
                        drRow("numClassID") = 0 'WE ARE NOW NOT SUPPORTING ITEM LEVEL CLASS TRACKING
                        drRow("vcProjectStageName") = ""
                        drRow("numQty") = 1
                        drRow("bitSerialized") = False
                        drRow("bitLotNo") = False
                        drRow("numUOMId") = 0
                        drRow("vcUOMName") = ""
                        dsItems.Tables(0).AcceptChanges()

                        dsItems.Tables(0).Columns.Remove("numWarehouseId")
                        Session("NewItems") = dsItems
                    End If

                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnCreditCard_Click(sender As Object, e As EventArgs) Handles btnCreditCard.Click
            Try
                Dim isInvoiceCreated As Boolean = True

                If ValidFormInput() Then
                    rdbAuthorizeOnly.Checked = False
                    rdbAuthorizeAndCharge.Checked = True
                    If (chkCreateInvoice.Checked = True) Then
                        isInvoiceCreated = CreateBizDoc(hdnNewOrderOpptype.Value, 287)
                    End If
                    If (chkItemShipped.Checked = True) Then
                        Dim objOpp As New COpportunities
                        objOpp.CreateFulfillmentBizDoc(Session("DomainID"), Session("UserContactID"), lngOppID)
                    End If

                    If isInvoiceCreated Then
                        SaveNewWithTransaction(False)
                    End If
                End If
            Catch ex As Exception
                If ex.Message = "NOT_ALLOWED" Then
                    litMessage.Text = "To split order items multiple bizdocs you must create all bizdocs with ""partial fulfilment"" checked!"
                ElseIf ex.Message = "NOT_ALLOWED_FulfillmentOrder" Then
                    litMessage.Text = "Only a Sales Order can create a Fulfillment Order"
                ElseIf ex.Message = "FY_CLOSED" Then
                    litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
                ElseIf ex.Message = "AlreadyInvoice" Then
                    litMessage.Text = "You can�t create a Invoice against a Fulfillment Order that already contains a Invoice"
                ElseIf ex.Message = "AlreadyPackingSlip" Then
                    litMessage.Text = "You can�t create a Packing-Slip against a Fulfillment Order that already contains a Packing-Slip"
                ElseIf ex.Message = "AlreadyInvoice_NOT_ALLOWED_FulfillmentOrder" Then
                    litMessage.Text = "You can�t create a fulfillment order against a sales order that already contains an Invoice or Packing-Slip"
                ElseIf ex.Message = "AlreadyFulfillmentOrder" Then
                    litMessage.Text = "Manually adding Invoices or Packing Slips to a Sales Order is not allowed after a Fulfillment Order (FO) is added. Your options are to edit the FO status to trigger creation of Invoices and/or Packing Slips, Delete the Fulfillment Order, or Create Invoices and/or Packing Slips from the Sales Fulfillment section"
                ElseIf ex.Message = "IM_BALANCE" Then
                    litMessage.Text = "The Credit and Debit amounts you�re entering must be of the same amount"
                ElseIf ex.Message = "FY_CLOSED" Then
                    litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
                ElseIf ex.Message.Contains("You can not add BizDoc because approval of unit price(s) is required.") Then
                    litMessage.Text = "You can not receive payment for order because approval of unit price(s) is required."
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Response.Write(ex)
                End If
            End Try
        End Sub

        Private Sub btnAuthrizeOnly_Click(sender As Object, e As EventArgs) Handles btnAuthrizeOnly.Click
            Try
                If ValidFormInput() Then
                    rdbAuthorizeOnly.Checked = True
                    rdbAuthorizeAndCharge.Checked = False
                    SaveNewWithTransaction(False)
                End If
            Catch ex As Exception
                If ex.Message = "IM_BALANCE" Then
                    litMessage.Text = "The Credit and Debit amounts you�re entering must be of the same amount"
                ElseIf ex.Message = "FY_CLOSED" Then
                    litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Response.Write(ex)
                End If
            End Try
        End Sub

        Private Function ValidFormInput() As Boolean
            Try
                If radDepositeTo.Checked And ddlDepositTo.SelectedValue = "0" Then
                    litMessage.Text = "Select Deposit to account."
                    ddlDepositTo.Focus()
                    Return False
                End If

                If Not radDepositeTo.Checked AndAlso Not radUnderpositedFund.Checked Then
                    litMessage.Text = "Select Deposit to account or choose undeposited fund option."
                    Return False
                End If

                If IsNewOrder = 1 AndAlso chkCreateInvoice.Checked Then
                    Dim objOppBizDocs As New OppBizDocs
                    objOppBizDocs.DomainID = CCommon.ToLong(Session("DomainID"))
                    objOppBizDocs.OppId = lngOppID
                    Dim dtInvoices As DataTable = objOppBizDocs.GetUnPaidInvoices()

                    If Not dtInvoices Is Nothing AndAlso dtInvoices.Rows.Count > 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "InvoiceAlreadyExists", "InvoiceAlreadyExists(" & lngOppID & "," & CCommon.ToLong(dtInvoices.Rows(0)("numOppBizDocsId")) & "," & CCommon.ToLong(dtInvoices.Rows(0)("numDivisionId")) & ")", True)
                        Return False
                    End If
                End If

                Dim amountToReceive As Decimal

                ' First check amount is valid
                If Decimal.TryParse(txtAmount.Text, amountToReceive) Then
                    If amountToReceive < 0 Then
                        litMessage.Text = "Amount to Receive must be greater than or equal to 0."
                        txtAmount.Focus()
                        Return False
                    End If
                Else
                    litMessage.Text = "Invalid Amount to Receive."
                    txtAmount.Focus()
                    Return False
                End If

                'Verify invoices and credit selection
                Dim isInvoicesSelected As Boolean
                Dim selectedInvoiceAmount As Decimal
                Dim selectedCreditAmount As Decimal

                Dim listSelectedInvoices As System.Collections.Generic.List(Of SelectedInvoice)

                If Not String.IsNullOrEmpty(UserSelectedInvoices.Trim()) Then
                    listSelectedInvoices = Newtonsoft.Json.JsonConvert.DeserializeObject(Of System.Collections.Generic.List(Of SelectedInvoice))(UserSelectedInvoices.Trim())
                End If

                For Each row As DataGridItem In dgOpportunity.Items
                    If DirectCast(row.FindControl("chk"), CheckBox).Checked Then
                        If Not listSelectedInvoices Is Nothing AndAlso listSelectedInvoices.Where(Function(x) x.numOppBizDocsID = CCommon.ToLong(CType(row.FindControl("hdnOppBizDocID"), HiddenField).Value)).Count() > 0 Then
                            Continue For
                        Else
                            isInvoicesSelected = True
                            selectedInvoiceAmount += CCommon.ToDecimal(DirectCast(row.FindControl("txtAmountToPay"), TextBox).Text)
                        End If
                    End If
                Next

                If Not listSelectedInvoices Is Nothing AndAlso listSelectedInvoices.Count > 0 Then
                    For Each objSelectedInvoice In listSelectedInvoices
                        isInvoicesSelected = True
                        selectedInvoiceAmount += objSelectedInvoice.monAmountPaid
                    Next
                End If

                For Each row As GridViewRow In gvCredits.Rows
                    If DirectCast(row.FindControl("chk"), CheckBox).Checked Then
                        selectedCreditAmount += CCommon.ToDecimal(DirectCast(row.FindControl("txtCreditAmount"), TextBox).Text)
                    End If
                Next

                If IsNewOrder = 1 AndAlso chkCreateInvoice.Checked Then
                    selectedInvoiceAmount += CCommon.ToDecimal(txtOrderAmount.Text)
                End If

                If IsNewOrder = 1 AndAlso chkCreateInvoice.Checked AndAlso CCommon.ToDouble(txtOrderAmount.Text) = 0 Then
                    litMessage.Text = "Select amount to receive for order."
                    txtOrderAmount.Focus()
                    Return False
                End If

                If selectedCreditAmount > selectedInvoiceAmount Then
                    litMessage.Text = "Total of credit amount to apply is greater than total of amount to receive. Either decrease applied credit amount or select more invoice(s)."
                    Return False
                End If

                If amountToReceive < (selectedInvoiceAmount - selectedCreditAmount) Then
                    litMessage.Text = "Invalid amount to receive. Please check total amount of selected invoices minus applied credit anount if any."
                    Return False
                End If

                Return True
            Catch ex As Exception
                Throw
            End Try
        End Function

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                Dim listSelectedInvoices As System.Collections.Generic.List(Of SelectedInvoice)

                If Not String.IsNullOrEmpty(UserSelectedInvoices.Trim()) Then
                    listSelectedInvoices = Newtonsoft.Json.JsonConvert.DeserializeObject(Of System.Collections.Generic.List(Of SelectedInvoice))(UserSelectedInvoices.Trim())
                Else
                    listSelectedInvoices = New Generic.List(Of SelectedInvoice)
                End If

                Dim objSelectedInvoice As SelectedInvoice
                For Each item As DataGridItem In dgOpportunity.Items
                    If CType(item.FindControl("chk"), CheckBox).Checked Then
                        If listSelectedInvoices.Where(Function(x) x.numOppBizDocsID = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value)).Count() > 0 Then
                            objSelectedInvoice = listSelectedInvoices.First(Function(x) x.numOppBizDocsID = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value))
                            objSelectedInvoice.monAmountPaid = CCommon.ToDecimal(CType(item.FindControl("txtAmountToPay"), TextBox).Text)
                        Else
                            objSelectedInvoice = New SelectedInvoice
                            objSelectedInvoice.numDepositeDetailID = CCommon.ToLong(CType(item.FindControl("hdnDepositeDetailID"), HiddenField).Value)
                            objSelectedInvoice.numOppBizDocsID = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value)
                            objSelectedInvoice.numBizDocID = CCommon.ToLong(CType(item.FindControl("hdnBizDocID"), HiddenField).Value)
                            objSelectedInvoice.numOppID = CCommon.ToLong(CType(item.FindControl("hdnOppID"), HiddenField).Value)
                            objSelectedInvoice.numdivisionId = CCommon.ToLong(CType(item.FindControl("hdnDivisionId"), HiddenField).Value)
                            objSelectedInvoice.numARAccountID = CCommon.ToLong(CType(item.FindControl("hdnARAccountID"), HiddenField).Value)
                            objSelectedInvoice.bitChild = CCommon.ToBool(CType(item.FindControl("hdnChild"), HiddenField).Value)
                            objSelectedInvoice.monAmountPaid = CCommon.ToDecimal(CType(item.FindControl("txtAmountToPay"), TextBox).Text)
                            objSelectedInvoice.numExchangeRateOfOrder = CCommon.ToDecimal(CType(item.FindControl("hdnExchangeRateOfOrder"), HiddenField).Value)
                            listSelectedInvoices.Add(objSelectedInvoice)
                        End If
                    ElseIf listSelectedInvoices.Where(Function(x) x.numOppBizDocsID = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value)).Count > 0 Then
                        listSelectedInvoices.Remove(listSelectedInvoices.First(Function(x) x.numOppBizDocsID = CCommon.ToLong(CType(item.FindControl("hdnOppBizDocID"), HiddenField).Value)))
                    End If
                Next

                If listSelectedInvoices.Count > 0 Then
                    UserSelectedInvoices = Newtonsoft.Json.JsonConvert.SerializeObject(listSelectedInvoices)
                Else
                    UserSelectedInvoices = ""
                End If

                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                BindCreditgrid()
                BindDatagrid()
            Catch sqlEx As SqlClient.SqlException
                Dim errorMessages As New System.Text.StringBuilder

                Dim i As Integer = 0
                Do While (i < sqlEx.Errors.Count)
                    errorMessages.Append(("dgOpportunity_ItemDataBound: Index #" _
                                    + (i + ("" & vbLf + ("Message: " _
                                    + (sqlEx.Errors(i).Message + ("" & vbLf + ("LineNumber: " _
                                    + (sqlEx.Errors(i).LineNumber + ("" & vbLf + ("Source: " _
                                    + (sqlEx.Errors(i).Source + ("" & vbLf + ("Procedure: " _
                                    + (sqlEx.Errors(i).Procedure + "" & vbLf)))))))))))))))
                    i = (i + 1)
                Loop

                ExceptionModule.ExceptionPublish(errorMessages.ToString(), Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(sqlEx)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Class SelectedInvoice
            Public Property numDepositeDetailID As Long
            Public Property numOppBizDocsID As Long
            Public Property numBizDocID As Long
            Public Property numOppID As Long
            Public Property numdivisionId As Long
            Public Property numARAccountID As Long
            Public Property bitChild As Boolean
            Public Property monAmountPaid As Decimal
            Public Property numExchangeRateOfOrder As Decimal
        End Class

    End Class
End Namespace
