﻿'Created By Sachin Sadhu
'Created Date 
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Alerts
Imports System.IO

Imports BACRM.BusinessLogic.Accounting

Namespace BACRM.UserInterface.Opportunities
    Public Class frmBizInvoicePreview
        Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

            BuildPreview()
            'ltrDynamicCss.Text = Session("editorPreviewCss")
        End Sub

        Private Sub BuildPreview()
            Try
                Dim iTemplateID As Int32
                iTemplateID = Convert.ToInt32(Server.UrlDecode(CCommon.ToString(GetQueryStringVal("TemplateID"))))
                Dim iTemplateType As Int32
                iTemplateType = Convert.ToInt32(Server.UrlDecode(CCommon.ToString(GetQueryStringVal("TemplateType"))))
                Dim strTemplateName As String
                strTemplateName = Convert.ToString(Server.UrlDecode(CCommon.ToString(GetQueryStringVal("TemplateName"))))
                Dim objOppBizDoc As New OppBizDocs
                objOppBizDoc.DomainID = Session("DomainID")
                objOppBizDoc.TemplateType = iTemplateType
                objOppBizDoc.BizDocTemplateID = iTemplateID
                Dim dt As DataTable = objOppBizDoc.GetBizDocTemplate()
                Dim strLogoUrl As String = ""
                Dim strFooterUrl As String = ""
                If dt.Rows.Count > 0 Then

                    If Not IsDBNull(dt.Rows(0).Item("vcBizDocImagePath")) Then
                        strLogoUrl = CCommon.GetDocumentPath(Session("DomainID")) & dt.Rows(0).Item("vcBizDocImagePath")
                    End If
                    If Not IsDBNull(dt.Rows(0).Item("vcBizDocFooter")) Then
                        strFooterUrl = CCommon.GetDocumentPath(Session("DomainID")) & dt.Rows(0).Item("vcBizDocFooter")
                    End If
                End If



                Dim strTag As String = Session("editorPreview")

                If strTag.Contains("#BizDocType#") Then
                    strTag = strTag.Replace("#BizDocType#", strTemplateName)
                End If

                If strTag.Contains("#Logo#") Then

                    strTag = strTag.Replace("#Logo#", "<img id='imgLogo' src='" + strLogoUrl + "' style='border-width:0px;'>")
                End If
                If strTag.Contains("#FooterImage#") Then
                    strTag = strTag.Replace("#FooterImage#", "<img id='imgLogofooter' src='" + strFooterUrl + "' style='border-width:0px;'>")
                End If

                ltrDynamicHtml.Text = strTag

                Dim classStyle As Style = New Style()
                Page.Header.StyleSheet.CreateStyleRule(classStyle, Nothing, Session("editorPreviewCss"))

            Catch ex As Exception

            End Try
        End Sub

    End Class
End Namespace