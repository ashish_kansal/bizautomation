﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmCreateBackOrderPO.aspx.vb" Inherits=".frmCreateBackOrderPO" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function ChangeVendorCost(numDefaultCostType, a, b, uomConversionFactorPurchase, numBackOrderQty, txtUnits) {
            if (numDefaultCostType == 3) {
                //VendorCost
                document.getElementById(a).value = (document.getElementById(b).value.split("~")[2] / uomConversionFactorPurchase);
            }

            if ($("[id$=hdnUnitsRecommendationForAutoPOBackOrder]").val() == "2") {
                if (document.getElementById(b).value.split("~")[3] > numBackOrderQty) {
                    document.getElementById(txtUnits).value = document.getElementById(b).value.split("~")[3];
                } else {
                    document.getElementById(txtUnits).value = numBackOrderQty;
                }
            } else {
                document.getElementById(txtUnits).value = parseInt(document.getElementById(b).value.split("~")[3]) + numBackOrderQty;
            }
        }

        function SelectAll(chkSelectAll) {
            $("[id$=gvBackOrderItems] tr").not(":first").each(function () {
                var chkSelect = $(this).find("[id$=chkSelect]");
                if (chkSelect != null) {
                    chkSelect.prop("checked", $(chkSelectAll).is(":checked"));
                }
            });
        }

        function ValidateDetail() {
            var i = 0;

            $('[id$=gvBackOrderItems] tr').not(":first").each(function () {
                if ($(this).find("input[id*='chkSelect']").is(':checked')) {
                    if ($(this).find("input[id*='txtUnitCost']").val() == "") {
                        alert("Enter Price")
                        $(this).find("input[id*='txtUnitCost']").focus();
                        return false;
                    }

                    if ($(this).find("input[id*='txtUnits']").val() == "" || parseInt($(this).find("input[id*='txtUnits']").val()) == 0) {
                        alert("Enter Units")
                        $(this).find("input[id*='txtUnits']").focus();
                        return false;
                    }

                    var ddlVendor = $(this).find("[id$=ddlVendor]");
                    if ($(ddlVendor).length > 0) {
                        if ($(ddlVendor).val() == null || $(ddlVendor).val() == "") {
                            alert("Select Vendor")
                            $(ddlVendor).focus();
                            return false;
                        }
                    }

                    i += 1;
                }
            });

            if (i == 0) {
                alert('Please Select At least one item.');
                return false;
            }

            var radcmbEmployerShipToAddresses = $find('radcmbEmployerShipToAddresses');
            if (radcmbEmployerShipToAddresses != null && !parseInt(radcmbEmployerShipToAddresses.get_value()) > 0) {
                alert("Select Ship-To Location.");
                radcmbEmployerShipToAddresses.focus();
                return false;
            }

            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="pull-right">
        <asp:Button ID="btnCreateBackOrderPO" runat="server" CssClass="btn btn-primary" Text="Create Back-Order P.O.(s)" OnClientClick="return ValidateDetail();" />
        <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary" Text="Cancel" OnClientClick="Close();" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Create Purchase Orders from Back-Orders
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="row padbottom10" id="divError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>
                <p>
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanel2" class="row padbottom10" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="col-xs-12">
                <div class="pull-left">
                    <div class="form-inline">
                        <div class="form-group">
                            <label>Ship-To Location:</label>
                            <telerik:RadComboBox ID="radcmbEmployerShipToAddresses" runat="server" AutoPostBack="true" Width="150" />
                        </div>
                        <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <div class="row">
        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" ChildrenAsTriggers="true" runat="server" class="col-xs-12">
            <ContentTemplate>
                <div class="table-responsive">
                    <asp:GridView ID="gvBackOrderItems" Width="100%" runat="server" AutoGenerateColumns="false" UseAccessibleHeader="true" ShowHeaderWhenEmpty="true" CssClass="table table-bordered table-striped">
                        <Columns>
                            <asp:TemplateField HeaderStyle-Width="25">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkSelectAll" runat="server" onchange="SelectAll(this)" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="75">
                                <ItemTemplate>
                                    <asp:Image ID="imgThumb" CssClass="img-responsive" runat="server" />
                                    <asp:HiddenField ID="hdnWarehouseItemID" runat="server" Value='<%# Eval("numWareHouseItemID")%>' />
                                    <asp:HiddenField ID="hdnCharItemType" runat="server" Value='<%# Eval("charItemType")%>' />
                                    <asp:HiddenField ID="hdnBaseUnit" runat="server" Value='<%# Eval("numUOMID")%>' />
                                    <asp:HiddenField ID="hdnUOMConversionFactor" runat="server" Value='<%# Eval("fltUOMConversionFactor")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Item" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="300">
                                <ItemTemplate>
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="white-space: nowrap; text-align: right; font-weight: bold">Item Code:</td>
                                            <td style="width: 100%; padding-left: 5px;">
                                                <asp:Label ID="lblItemCode" runat="server" Text='<%# Eval("numItemCode")%>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="white-space: nowrap; text-align: right; font-weight: bold">Item:</td>
                                            <td style="width: 100%; padding-left: 5px;">
                                                <asp:Label ID="lblItemName" runat="server" Text='<%# Eval("vcItemName")%>'></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="white-space: nowrap; text-align: right; font-weight: bold">Model ID:</td>
                                            <td style="width: 100%; padding-left: 5px;">
                                                <asp:Label ID="lblModelID" runat="server" Text='<%# Eval("vcModelID")%>'></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="white-space: nowrap; text-align: right; font-weight: bold">SKU:</td>
                                            <td style="width: 100%; padding-left: 5px;"><%# Eval("vcSKU")%></td>
                                        </tr>
                                        <tr>
                                            <td style="white-space: nowrap; text-align: right; font-weight: bold">B.O. Qty:</td>
                                            <td style="width: 100%; padding-left: 5px; color: red">
                                                <%# Eval("numBackOrder")%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Vendor" HeaderStyle-Width="250">
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlVendor" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged"></asp:DropDownList>
                                    <br />
                                    <b>Min Order Qty:</b>
                                    <asp:Label ID="lblMinOrderQty" runat="server" Text='<%# Eval("intMinQty") %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Vendor Warehouse" HeaderStyle-Width="200">
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlVendorWarehouses" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlVendorWarehouses_SelectedIndexChanged"></asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Ship Method" HeaderStyle-Width="200">
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlVendorShipmentMethod" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlVendorShipmentMethod_SelectedIndexChanged"></asp:DropDownList>
                                    <br />
                                    <b>Lead Time:</b>
                                    <asp:Label ID="lblLeadTimeDays" runat="server" Text='-'></asp:Label>
                                    Days
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Notes" HeaderStyle-Width="300" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtNotes" runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Units" ItemStyle-Wrap="false" HeaderStyle-Width="80">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtUnits" runat="server" CssClass="form-control"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cost per unit" HeaderStyle-Width="120">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtUnitCost" runat="server" CssClass="form-control"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:HiddenField ID="hdnOppID" runat="server" />
        <asp:HiddenField ID="hfnReOrderPointOrderStatus" runat="server" />
        <asp:HiddenField ID="hdnOppStautsForAutoPOBackOrder" runat="server" />
        <asp:HiddenField ID="hdnUnitsRecommendationForAutoPOBackOrder" runat="server" />
    </div>
</asp:Content>
