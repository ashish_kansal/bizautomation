﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmOrderTransitforSO.aspx.vb" Inherits=".frmOrderTransitforSO" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Lead-times for items that need POs</title>
    <style>
        i.text-success {
    color: #09a92b;
}
        i.text-warning {
    color: #c56707;
}
        .tabHeader{
            text-align: left;
    padding: 10px;
    font-size: 15px;
        }
        .tabBodyCompany{

        }
        .tabBody{
            font-size: 13px;
        }
        .text-primary{
         color:#048ecb
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="window.close()"
                Text="Close" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Lead-times for items that need POs
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table class="tbl" style="width:850px;">
        <tbody>
            <asp:Repeater ID="rptItems" runat="server" OnItemDataBound="rptItems_ItemDataBound">
                <ItemTemplate>
                    <tr>
                        <th class="tabHeader">
                            <%# Eval("ItemName")%>
                            <asp:HiddenField ID="hdnItemNameData" runat="server" Value='<%# Eval("ItemName")%>' />
                        </th>
                        
                    </tr>
                     <asp:Repeater ID="rptVendorDetails" runat="server">
                <ItemTemplate>
                    <tr><td class="tabBodyCompany tabBody"><%# Eval("vcCompanyName")%></td></tr>
                    <tr><td class="tabBody">
                        <asp:Label ID="Label1" style="    padding-left: 25px;" runat="server" Text='<%# Server.HtmlDecode(Eval("vendorShipmentDays").ToString()) %>'></asp:Label></td></tr>
                </ItemTemplate>
            </asp:Repeater>
                </ItemTemplate>
            </asp:Repeater>
           
        </tbody>
    </table>
</asp:Content>
