<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAddSerializedItem.aspx.vb"
    Inherits=".frmAddSerializedItem" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Serial / Lot #s</title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <script language="javascript" type="text/javascript">
        function Close() {
            window.close()
            return false;
        }
        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server" style="left: 10px; top: 10px; position: relative;">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <table cellspacing="0" cellpadding="0" width="500">
        <tr>
            <td>
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;&nbsp;&nbsp;Serial / Lot #s&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td align="right">
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save &amp; Close" />
            </td>
        </tr>
    </table>
    <asp:Table ID="tblSErialLot" runat="server" Width="500" GridLines="None" CssClass="aspTable"
        BorderColor="black" BorderWidth="1">
        <asp:TableRow>
            <asp:TableCell>
                <table>
                    <tr>
                        <td align="center" class="normal4">
                            <asp:HiddenField ID="hfbitSerialized" runat="server" />
                            <asp:HiddenField ID="hfbitLotNo" runat="server" />
                            <input id="hdWareHouseItemID" runat="server" type="hidden" />
                            <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="normal1">
                            Select Item :
                            <asp:DropDownList ID="ddlItems" CssClass="signup" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                            &nbsp; Units :
                            <asp:Label ID="lblUnits" runat="server" CssClass="signup"></asp:Label>&nbsp;<asp:Label
                                ID="lblUnitsName" runat="server" CssClass="signup"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <telerik:RadGrid ID="gvSerialLot" runat="server" AutoGenerateColumns="false" AlternatingItemStyle-CssClass="ais"
                                ItemStyle-CssClass="is" HeaderStyle-CssClass="hs" Skin="windows" EnableEmbeddedSkins="false"
                                CssClass="dg" EnableLinqExpressions="false" ItemStyle-HorizontalAlign="Center"
                                AlternatingItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                <MasterTableView DataKeyNames="numWareHouseItmsDTLID,numWareHouseItemID" Width="100%">
                                    <Columns>
                                        <telerik:GridTemplateColumn HeaderText="" UniqueName="CheckBox">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkInclude" runat="server" />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn DataField="vcSerialNo" HeaderText="Serial/LOT(#)" UniqueName="vcSerialNo" />
                                        <telerik:GridBoundColumn DataField="TotalQty" HeaderText="Available Qty" UniqueName="TotalQty" />
                                        <telerik:GridTemplateColumn HeaderText="Used Qty" UniqueName="UsedQty">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtUsedQty" runat="server" Width="80" CssClass="signup" Text='<%#DataBinder.Eval(Container.DataItem, "UsedQty")%>'
                                                    onkeypress="CheckNumber(2,event)"></asp:TextBox>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                    <NoRecordsTemplate>
                                    </NoRecordsTemplate>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="true" />
                            </telerik:RadGrid>
                            <%--      <ig:WebDataGrid ID="gvInventory" runat="server" AutoGenerateColumns="false" AutoGenerateBands="false"
                                DataKeyFields="numWareHouseItmsDTLID" InitialExpandDepth="0" InitialDataBindDepth="0"
                                Width="100%">
                                <Columns>
                                    <ig:TemplateDataField Key="bitAdded" Width="30px">
                                        <Header Text=" " />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cbSelect" runat="server" />
                                        </ItemTemplate>
                                    </ig:TemplateDataField>
                                </Columns>
                                <Behaviors>
                                    <ig:EditingCore AutoCRUD="false" EnableInheritance="true" BatchUpdating="true">
                                        <Behaviors>
                                            <ig:RowAdding Alignment="Top" EditModeActions-EnableOnActive="true" Enabled="true"
                                                EditModeActions-MouseClick="Single">
                                                <ColumnSettings>
                                                </ColumnSettings>
                                            </ig:RowAdding>
                                            <ig:CellEditing Enabled="true" EditModeActions-EnableF2="true">
                                                <ColumnSettings>
                                                </ColumnSettings>
                                            </ig:CellEditing>
                                        </Behaviors>
                                    </ig:EditingCore>
                                    <ig:RowSelectors RowNumbering="true">
                                    </ig:RowSelectors>
                                    <ig:Selection RowSelectType="Single ">
                                    </ig:Selection>
                                </Behaviors>
                            </ig:WebDataGrid>--%>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    </form>
</body>
</html>
