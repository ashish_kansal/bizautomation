﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Contacts

Public Class frmItemVendors
    Inherits BACRMPage

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                hdnItemCode.Value = CCommon.ToLong(GetQueryStringVal("numItemCode"))
                hdnShipPostCode.Value = CCommon.ToString(GetQueryStringVal("numShipAddressID"))


                If CCommon.ToLong(hdnItemCode.Value) > 0 Then
                    If CCommon.ToBool(GetQueryStringVal("isHideDistance")) Then
                        For Each column As DataControlField In gvVendor.Columns
                            If column.HeaderText = "" Then
                                column.Visible = False
                            End If
                        Next
                    End If

                    BindGrid()

                    If CCommon.ToLong(GetQueryStringVal("numShipAddressID")) = 0 Then
                        Dim oCOpportunities As New BACRM.BusinessLogic.Opportunities.COpportunities
                        oCOpportunities.DomainID = CType((Session("DomainID")), Long)
                        Dim dtAddress As DataTable = oCOpportunities.GetExistingAddress(CInt(Session("UserID")), CInt(Session("UserDivisionID")))

                        If Not dtAddress Is Nothing AndAlso dtAddress.Rows.Count > 0 Then
                            If CCommon.ToString(dtAddress.Rows(0)("vcShipToPostal")) <> "" Then
                                hdnShipPostCode.Value = CCommon.ToString(dtAddress.Rows(0)("vcShipToPostal"))
                            Else
                                lblException.Text = "Ship to address or postal code is not available."
                            End If
                        Else
                            lblException.Text = "Ship to address is not selected."
                        End If
                    Else
                        Dim objContact As New CContacts
                        objContact.AddressType = CContacts.enmAddressType.ShipTo
                        objContact.DomainID = Session("DomainID")
                        objContact.AddressID = CCommon.ToLong(GetQueryStringVal("numShipAddressID"))
                        objContact.byteMode = 1
                        Dim dt As DataTable = objContact.GetAddressDetail()

                        If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                            If CCommon.ToString(dt.Rows(0)("vcPostalCode")) <> "" Then
                                hdnShipPostCode.Value = CCommon.ToString(dt.Rows(0)("vcPostalCode"))
                            Else
                                lblException.Text = "Ship to address or postal code is not available."
                            End If
                        Else
                            lblException.Text = "Ship to address or postal code is not available."
                        End If
                    End If
                Else
                    lblException.Text = "Item code is not available"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(CCommon.ToString(ex))
        End Try
    End Sub

#End Region
    
#Region "Private Methods"

    Private Sub BindGrid()
        Try
            'TODO: SET STOCK TRANSFER FALSE
            Dim objItem As New CItems
            objItem.DomainID = CCommon.ToLong(Session("DomainID"))
            objItem.ItemCode = CCommon.ToLong(hdnItemCode.Value)
            objItem.ClientZoneOffsetTime = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            gvVendor.DataSource = objItem.GetItemVendors()
            gvVendor.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

#End Region
    
#Region "Event Handlers"



#End Region

    Private Sub gvVendor_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvVendor.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then


                Dim ddlShipAddress As DropDownList = DirectCast(e.Row.FindControl("ddlShipAddress"), DropDownList)

                If Not ddlShipAddress Is Nothing Then
                    Dim row As DataRowView = DirectCast(e.Row.DataItem, DataRowView)
                    Dim plhDistance As PlaceHolder = DirectCast(e.Row.FindControl("plhDistance"), PlaceHolder)

                    Dim objContact As New CContacts
                    objContact.AddressType = CContacts.enmAddressType.ShipTo
                    objContact.DomainID = Session("DomainID")
                    objContact.RecordID = CCommon.ToLong(row("numVendorID"))
                    objContact.AddresOf = CContacts.enmAddressOf.Organization
                    objContact.byteMode = 2
                    Dim dt As DataTable = objContact.GetAddressDetail()

                    Dim listItem As ListItem
                    If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                        For Each dr As DataRow In dt.Rows
                            listItem = New ListItem()
                            listItem.Text = dr("vcAddressName")
                            listItem.Value = dr("numAddressID") & "#" & dr("vcPostalCode")
                            ddlShipAddress.Items.Add(listItem)
                        Next
                    End If

                    If ddlShipAddress.Items.Count = 0 Or hdnShipPostCode.Value = "" Then
                        Dim label As New HtmlGenericControl("label")
                        label.InnerText = "NA"
                        plhDistance.Controls.Add(label)
                    Else
                        Dim div As New HtmlGenericControl("div")
                        div.Attributes.Add("id", "divLoading")
                        div.InnerHtml = "<i class=""fa fa-2x fa-refresh fa-spin""></i>"
                        plhDistance.Controls.Add(div)
                    End If

                    Dim imgSelectVendor As System.Web.UI.WebControls.Image = DirectCast(e.Row.FindControl("imgSelectVendor"), System.Web.UI.WebControls.Image)
                    imgSelectVendor.Attributes.Add("onclick", "SelectVendor(" & CCommon.ToLong(row("numVendorID")) & "," & CCommon.ToDouble(row("monVendorCost")) & ");")
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(CCommon.ToString(ex))
        End Try
    End Sub
End Class