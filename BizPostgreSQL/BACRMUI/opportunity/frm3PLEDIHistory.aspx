﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frm3PLEDIHistory.aspx.vb" Inherits=".frm3PLEDIHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .timeline {
            list-style: none;
            padding: 20px 0 20px;
            position: relative;
        }

            .timeline:before {
                top: 0;
                bottom: 0;
                position: absolute;
                content: " ";
                width: 3px;
                background-color: #eeeeee;
                left: 2%;
                margin-left: -1.5px;
            }

            .timeline > li {
                margin-bottom: 20px;
                position: relative;
            }

                .timeline > li:before,
                .timeline > li:after {
                    content: " ";
                    display: table;
                }

                .timeline > li:after {
                    clear: both;
                }

                .timeline > li:before,
                .timeline > li:after {
                    content: " ";
                    display: table;
                }

                .timeline > li:after {
                    clear: both;
                }

                .timeline > li > .timeline-panel {
                    width: 94%;
                    float: left;
                    border: 1px solid #d4d4d4;
                    border-radius: 2px;
                    padding: 20px;
                    position: relative;
                    -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
                    box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
                }

                    .timeline > li > .timeline-panel:before {
                        position: absolute;
                        top: 26px;
                        right: -15px;
                        display: inline-block;
                        border-top: 15px solid transparent;
                        border-left: 15px solid #ccc;
                        border-right: 0 solid #ccc;
                        border-bottom: 15px solid transparent;
                        content: " ";
                    }

                    .timeline > li > .timeline-panel:after {
                        position: absolute;
                        top: 27px;
                        right: -14px;
                        display: inline-block;
                        border-top: 14px solid transparent;
                        border-left: 14px solid #fff;
                        border-right: 0 solid #fff;
                        border-bottom: 14px solid transparent;
                        content: " ";
                    }

                .timeline > li > .timeline-badge {
                    color: #fff;
                    width: 50px;
                    height: 50px;
                    line-height: 50px;
                    font-size: 1.4em;
                    text-align: center;
                    position: absolute;
                    top: 16px;
                    left: 2%;
                    margin-left: -25px;
                    background-color: #999999;
                    z-index: 100;
                    border-top-right-radius: 50%;
                    border-top-left-radius: 50%;
                    border-bottom-right-radius: 50%;
                    border-bottom-left-radius: 50%;
                }

                .timeline > li.timeline-inverted > .timeline-panel {
                    float: right;
                }

                    .timeline > li.timeline-inverted > .timeline-panel:before {
                        border-left-width: 0;
                        border-right-width: 15px;
                        left: -15px;
                        right: auto;
                    }

                    .timeline > li.timeline-inverted > .timeline-panel:after {
                        border-left-width: 0;
                        border-right-width: 14px;
                        left: -14px;
                        right: auto;
                    }


        .timeline-badge.success {
            background-color: #3f903f !important;
        }

        .timeline-badge.danger {
            background-color: #d9534f !important;
        }

        .timeline-title {
            margin-top: 0;
            color: inherit;
        }

        .timeline-body > p,
        .timeline-body > ul {
            margin-bottom: 0;
        }

            .timeline-body > p + p {
                margin-top: 5px;
            }

        /*Extra small devices (portrait phones, less than 576px)*/
        @media (max-width: 575px) {
            .timeline:before {
                left: 5%;
                margin-left: -1.5px;
            }

            .timeline > li > .timeline-badge {
                left: 5%;
                margin-left: -25px;
            }

            .timeline > li > .timeline-panel {
                width: 80%;
            }
        }

        /*Small devices (landscape phones, less than 768px)*/
        @media (max-width: 767px) {
            .timeline:before {
                left: 5%;
                margin-left: -1.5px;
            }

            .timeline > li > .timeline-badge {
                left: 5%;
                margin-left: -25px;
            }

            .timeline > li > .timeline-panel {
                width: 85%;
            }
        }

        /*Medium devices (tablets, less than 992px)*/
        @media (max-width: 991px) {
            .timeline:before {
                left: 5%;
                margin-left: -1.5px;
            }

            .timeline > li > .timeline-badge {
                left: 5%;
                margin-left: -25px;
            }

            .timeline > li > .timeline-panel {
                width: 88%;
            }
        }

        /*Large devices (desktops, less than 1200px)*/
        @media (max-width: 1199px) {
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    3PL-EDI History
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
     <div class="row padbottom10" id="divError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>
                <p>
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
    <div class="row padbottom10" id="divNoData" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-info text-center">
               No Data Available
            </div>
        </div>
    </div>
    <asp:Repeater runat="server" ID="rptEDIHistory">
        <HeaderTemplate>
            <ul class="timeline" style="height:auto;position:relative;">
        </HeaderTemplate>
        <ItemTemplate>
            <li class="timeline-inverted">
                <div class='<%# If(DataBinder.Eval(Container, "DataItem.bitSuccess") = "True", "timeline-badge success", "timeline-badge danger")%>'><i class='<%# If(DataBinder.Eval(Container, "DataItem.bitSuccess") = "True", "glyphicon glyphicon-thumbs-up", "glyphicon glyphicon-thumbs-down")%>'></i></div>
                <div class="timeline-panel">
                    <div class="timeline-heading">
                        <h4 class="timeline-title"><%# DataBinder.Eval(Container, "DataItem.vcDate")%></h4>
                    </div>
                    <div class="timeline-body">
                        <p>
                           <%# DataBinder.Eval(Container, "DataItem.vcLog")%>
                        </p>
                    </div>
                </div>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul>
        </FooterTemplate>
    </asp:Repeater>
    <asp:HiddenField ID="hdnOppID" runat="server" />
</asp:Content>
