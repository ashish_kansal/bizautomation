﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmGetShippingRates.aspx.vb"
    Inherits=".frmGetShippingRates" MasterPageFile="~/common/Popup.Master" %>

<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/javascript">
        function getSelectedValues() {

            var i;
            var strSelected = ''
            for (i = 0; i < document.getElementById('form1').elements.length; i++) {
                var elm = document.getElementById('form1').elements[i];
                if (elm.type == "radio" && elm.name.indexOf("G") != -1) {
                    if (elm.checked) {
                        strSelected = strSelected + elm.value + ','
                    }
                }
            }
            document.getElementById('hdnSelectedIDs').value = strSelected;
        }
        function Save() {
            if (document.form1.ddlFromState.selectedIndex == 0) {
                alert("Please Select From State");
                document.form1.ddlFromState.focus();
                return false;
            }
            if (document.form1.ddlToState.selectedIndex == 0) {
                alert("Please Select To State");
                document.form1.ddlToState.focus();
                return false;
            }
            if (document.getElementById('txtFromZip').value.length < 5) {
                alert("Please enter valid From Zip");
                document.getElementById('txtFromZip').focus();
                return false;
            }
            if (document.getElementById('txtToZip').value.length < 5) {
                alert("Please enter valid To Zip");
                document.getElementById('txtToZip').focus();
                return false;
            }
            if (document.form1.ddlFromCountry.selectedIndex == 0) {
                alert("Please Select From Country");
                document.form1.ddlFromCountry.focus();
                return false;
            }
            if (document.form1.ddlToCountry.selectedIndex == 0) {
                alert("Please Select To Country");
                document.form1.ddlToCountry.focus();
                return false;
            }

            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table width="100%">
                <tr>
                    <td class="normal1" align="right">
                        <asp:CheckBox ID="chkUseDimensions" runat="server" Text="Use Dimensions" CssClass="signup" />&nbsp;<asp:Button
                            ID="btnGetRates" runat="server" CssClass="button" Text="Get Rates"></asp:Button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <table align="center" width="100%">
        <tr>
            <td align="center" class="normal4">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal1" valign="top">
                <asp:Table ID="tblItems" BorderWidth="1" CssClass="aspTable" CellPadding="0" CellSpacing="0"
                    Height="" runat="server" Width="500px" BorderColor="black" GridLines="None">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:DataGrid ID="dgBizDocItems" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False">
                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                <ItemStyle CssClass="is"></ItemStyle>
                                <HeaderStyle CssClass="hs"></HeaderStyle>
                                <Columns>
                                    <asp:BoundColumn DataField="numItemCode" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="vcItemName" HeaderText="Item"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="vcModelID" HeaderText="Model ID"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Description">
                                        <ItemTemplate>
                                            <%# Eval("vcItemDesc").ToString().TrimLength(50) %>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="fltWeight" HeaderText="Weight per Unit"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Total Weight">
                                        <ItemTemplate>
                                            <asp:TextBox CssClass="signup" ID="txtTotalWeight" runat="server" Text='<%# Eval("TotalWeight") %>'
                                                Width="50px"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <%--<asp:TemplateColumn HeaderText="Boxes/Packages">
                                        <ItemTemplate>
                                            <asp:TextBox CssClass="signup" ID="txtPackages" runat="server" Text="1" Width="50px"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>--%>
                                    <asp:TemplateColumn HeaderText="Dimensions = Height Length Width" ItemStyle-HorizontalAlign="Right"
                                        HeaderStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:TextBox CssClass="signup" ID="txtHeight" runat="server" Text='<%# Eval("fltHeight") %>'
                                                Width="25px"></asp:TextBox>&nbsp;&nbsp;&nbsp;&nbsp;
                                            <asp:TextBox CssClass="signup" ID="txtLength" runat="server" Text='<%# Eval("fltLength") %>'
                                                Width="25px"></asp:TextBox>&nbsp;&nbsp;&nbsp;
                                            <asp:TextBox CssClass="signup" ID="txtWidth" runat="server" Text='<%# Eval("fltWidth") %>'
                                                Width="25px"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <br />
                            <table width="100%" id="tblConfig" runat="server">
                                <tr>
                                    <td class="normal1" align="right" valign="bottom">
                                        Service type
                                    </td>
                                    <td class="normal1" align="left" valign="bottom">
                                        <asp:DropDownList CssClass="signup" ID="ddlServiceType" runat="server">
                                            <asp:ListItem Selected="True" Value="0">Unspecified</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td class="normal1" align="right" valign="bottom">
                                        Shipping Provider
                                    </td>
                                    <td class="normal1" align="left" valign="bottom">
                                        <asp:Label runat="server" ID="lblShippingProvider" Font-Bold="true"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right" valign="bottom">
                                        Packaging Type
                                    </td>
                                    <td class="normal1" align="left">
                                        <asp:DropDownList CssClass="signup" ID="ddlPackagingType" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right" valign="bottom">
                                        From State
                                    </td>
                                    <td class="normal1" align="left">
                                        <asp:DropDownList CssClass="signup" ID="ddlFromState" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="normal1" align="right" valign="bottom">
                                        To State
                                    </td>
                                    <td class="normal1" align="left">
                                        <asp:DropDownList CssClass="signup" ID="ddlToState" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right" valign="bottom">
                                        From Zip
                                    </td>
                                    <td class="normal1" align="left">
                                        <asp:TextBox CssClass="signup" ID="txtFromZip" runat="server" Width="58px"></asp:TextBox>
                                    </td>
                                    <td class="normal1" align="right" valign="bottom">
                                        To Zip
                                    </td>
                                    <td class="normal1" align="left">
                                        <asp:TextBox CssClass="signup" ID="txtToZip" runat="server" Width="49px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="normal1" align="right" valign="bottom">
                                        From Country
                                    </td>
                                    <td class="normal1" align="left">
                                        <asp:DropDownList CssClass="signup" ID="ddlFromCountry" AutoPostBack="True" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td class="normal1" align="right" valign="bottom">
                                        To Country
                                    </td>
                                    <td class="normal1" align="left">
                                        <asp:DropDownList CssClass="signup" ID="ddlToCountry" AutoPostBack="True" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
    </table>
    <asp:Table ID="tblShipping" Visible="false" CssClass="aspTable" CellPadding="0" CellSpacing="0"
        Height="" runat="server" Width="100%" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgItems" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is" HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="numItemCode" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="fltWeight" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="fltHeight" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="fltWidth" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="fltLength" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="numOppBizDocItemID" Visible="false"></asp:BoundColumn>
                        <%--<asp:BoundColumn DataField="intNoOfBox" Visible="false"></asp:BoundColumn>--%>
                        <asp:BoundColumn DataField="vcItemName" HeaderText="Item" ItemStyle-VerticalAlign="Top"
                            ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="" ItemStyle-HorizontalAlign="Right" ItemStyle-VerticalAlign="Top">
                            <ItemTemplate>
                                <asp:DataGrid ID="gridRates" runat="server" CssClass="dg" AutoGenerateColumns="False">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is" HorizontalAlign="Left"></ItemStyle>
                                    <HeaderStyle CssClass="hs"></HeaderStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Select" ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <input type="radio" value="<%# Eval("ID") %>" name="<%# Eval("GroupName") %>" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="Date" HeaderText="Delivery Day/Date"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="ServiceType" HeaderText="Service Type"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Rate" HeaderText="Rate (US$)" DataFormatString="{0:c}">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <div style="float: right">
        <asp:Button ID="btnAddToShippingReport" runat="server" Text="Save selection" Visible="false"
            CssClass="button" />
        &nbsp; &nbsp;
    </div>
    <asp:HiddenField ID="hdnSelectedIDs" runat="server" />
</asp:Content>
