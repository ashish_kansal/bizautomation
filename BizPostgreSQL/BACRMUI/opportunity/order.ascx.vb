﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Text.RegularExpressions
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.ShioppingCart
Imports BACRM.BusinessLogic.TimeAndExpense
Imports BACRM.BusinessLogic.Workflow
Imports Telerik.Web.UI

Public Class order
    Inherits BACRMUserControl

    Dim objCommon As CCommon
    Dim objItems As CItems
    Dim strValues As String
    Dim dtOppAtributes As DataTable
    Dim dsTemp As DataSet
    Dim lngDivId, lngCntID, lngOppId, OppBizDocID, JournalId As Long
    Dim dtOppBiDocItems As DataTable
    Dim strMessage As String
    Dim ResponseMessage As String
    Dim boolPurchased As Boolean = False
    Dim IsFromCreateBizDoc As Boolean = False
    Dim IsFromSaveAndOpenOrderDetails As Boolean = False
    Dim IsFromSaveAndNew As Boolean = False
    Dim boolFlag As Boolean = True
    Dim m_aryRightsForItems(), m_aryRightsForAuthoritativ(), m_aryRightsForNonAuthoritativ(), m_aryRightsForEditUnitPrice() As Integer
    Dim IsPOS As Short
    Dim isShippingMethodNeeded As Boolean = False
    Dim lngShippingItemCode As Double = 0
    Private objOppBizDocs As New OppBizDocs
    Private strDocumentPath As String
    Shared dtMultiSelectShipFromAddr As DataTable = Nothing
    Shared dtMultiSelectShipToAddr As DataTable = Nothing
    Private dtMultiSelect As DataTable
    Private _OppType As Integer
    Private m_aryRightsForAddEditCustomerPartNo() As Integer
    Private lngDiscountServiceItemForOrder As Double = 0

    Public Property OppType() As Integer
        Get
            Return _OppType
        End Get
        Set(ByVal value As Integer)
            _OppType = value
        End Set
    End Property
    Public Property OppStatus As Short
    Public Property OrderSource As Long
    Public Property SourceType As Short
    Public _PageType As PageType
    Enum PageType As Integer
        Sales
        Purchase
        AddOppertunity
        AddEditOrder
    End Enum

    Private _IsStockTransfer As Boolean
    Public Property IsStockTransfer() As Boolean
        Get
            Return _IsStockTransfer
        End Get
        Set(ByVal value As Boolean)
            _IsStockTransfer = value
        End Set
    End Property


    Public Property SOItems() As DataSet
        Get
            Return ViewState("SOItems")
        End Get
        Set(ByVal value As DataSet)
            ViewState("SOItems") = value
        End Set
    End Property

#Region "Private Property"
    Private Property SelectedWarehouseItemIDs() As ArrayList
        Get
            If ViewState("SelectedWarehouseItemIDs") Is Nothing Then
                ViewState("SelectedWarehouseItemIDs") = New ArrayList
            End If

            Return CType(ViewState("SelectedWarehouseItemIDs"), ArrayList)
        End Get
        Set(ByVal Value As ArrayList)
            ViewState("SelectedWarehouseItemIDs") = Value
        End Set
    End Property
#End Region
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            If CCommon.ToDouble(Session("DiscountServiceItem")) > 0 Then
                lngDiscountServiceItemForOrder = CCommon.ToDouble(Session("DiscountServiceItem"))
            Else
                lngDiscountServiceItemForOrder = 0
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            txtUnits.Attributes.Add("onkeyup", "return AddItemToOrder(event);")
            txtprice.Attributes.Add("onkeyup", "return AddItemToOrder(event);")
            txtItemDiscount.Attributes.Add("onkeyup", "return AddItemToOrder(event);")

            If CCommon.ToDouble(Session("ShippingServiceItem")) > 0 Then
                lngShippingItemCode = CCommon.ToDouble(Session("ShippingServiceItem"))
            Else
                lngShippingItemCode = 0
            End If

            DomainID = Session("DomainID")
            UserCntID = Session("UserContactID")
            IsPOS = 0
            GetUserRightsForPage(10, 1)

            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS", False)
            If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnSave.Visible = False
                btnSaveNew.Visible = False
                btnSaveOpenOrder.Visible = False
            End If

            If Not IsPostBack Then
                hdnPortalURL.Value = CCommon.GetDocumentPath(Session("DomainID"))

                If CCommon.ToLong(Session("UserContactID")) > 0 AndAlso CCommon.ToLong(Session("UserID")) = 0 Then
                    txtprice.ReadOnly = True
                    txtItemDiscount.ReadOnly = True
                End If

                hdnEnabledItemLevelUOM.Value = CCommon.ToBool(Session("EnableItemLevelUOM"))
                hdnDefaultUserExternalLocation.Value = CCommon.ToLong(Session("DefaultWarehouse"))

                hdnPOS.Value = IsPOS
                hdnOppType.Value = _OppType

                If (_PageType <> PageType.AddEditOrder) Then
                    createSet()
                    BindGrid()
                    divItemGridColumnSetting.Visible = False
                Else
                    divItemGridColumnSetting.Visible = True
                End If
                objItems = New CItems
                objItems.DivisionID = CCommon.ToLong(GetQueryStringVal("DivId"))
                Dim dtTable As DataTable
                dtTable = objItems.GetAmountDue
                chkAutoCheckCustomerPart.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitAutoCheckCustomerPart"))
                chkSearchOrderCustomerHistory.Visible = IIf(Session("SearchOrderCustomerHistory") = True, True, False)

                If CCommon.ToBool(dtTable.Rows(0)("bitUseShippersAccountNo")) Then
                    chkUseCustomerShippingAccount.Checked = True
                End If

                txtDivID.Text = 0

                objCommon = New CCommon
                objCommon.UserCntID = Session("UserContactID")

                BindButtons()
                ' CreateClone()
                'Hide unit price approvar button if user does not have right to approve unit price
                Dim userID As String = CCommon.ToString(Session("UserID"))
                Dim unitPriceApprover() As String = CCommon.ToString(Session("UnitPriceApprover")).Split(",")

                If CCommon.ToBool(Session("IsMinUnitPriceRule")) AndAlso Not unitPriceApprover Is Nothing AndAlso unitPriceApprover.Contains(userID) Then
                    btnApproveUnitPrice.Visible = True
                End If

                If _PageType = PageType.Sales Or ((_PageType = PageType.AddEditOrder Or _PageType = PageType.AddOppertunity) AndAlso OppType = 1) Then
                    'Check if user has right to edit unit price
                    m_aryRightsForEditUnitPrice = GetUserRightsForPage_Other(10, 31)
                    If m_aryRightsForEditUnitPrice(RIGHTSTYPE.UPDATE) = 0 Then
                        hdnEditUnitPriceRight.Value = "False"
                        txtprice.Enabled = False
                    End If

                    m_aryRightsForAddEditCustomerPartNo = GetUserRightsForPage_Other(10, 136)
                    If m_aryRightsForAddEditCustomerPartNo(RIGHTSTYPE.ADD) = 0 Or m_aryRightsForAddEditCustomerPartNo(RIGHTSTYPE.UPDATE) = 0 Then
                        txtCustomerPartNo.Enabled = False
                    Else
                        txtCustomerPartNo.Enabled = True
                    End If

                    BindShippingCompanyAndMethods(radShipVia, radCmbShippingMethod)

                    If Not radShipVia.FindItemByValue(dtTable.Rows(0).Item("intShippingCompany")) Is Nothing Then
                        radShipVia.FindItemByValue(dtTable.Rows(0).Item("intShippingCompany")).Selected = True
                        hdnShipVia.Value = CCommon.ToLong(dtTable.Rows(0).Item("intShippingCompany"))
                    End If

                    radShipVia_SelectedIndexChanged(Nothing, Nothing)

                    If Not radCmbShippingMethod.FindItemByValue(dtTable.Rows(0).Item("numDefaultShippingServiceID")) Is Nothing Then
                        radCmbShippingMethod.FindItemByValue(dtTable.Rows(0).Item("numDefaultShippingServiceID")).Selected = True
                        hdnShippingService.Value = CCommon.ToLong(dtTable.Rows(0).Item("numDefaultShippingServiceID"))
                    End If

                    Dim objOppBizDocs As New OppBizDocs
                    objOppBizDocs.OppId = CCommon.ToLong(GetQueryStringVal("opid"))
                    objOppBizDocs.DomainID = CCommon.ToLong(Session("DomainID"))
                    Dim dsAddress As DataSet = objOppBizDocs.GetOPPGetOppAddressDetails


                    If Not ViewState("SOItems") Is Nothing Then
                        Dim warehouseID As Long = 0

                        If DirectCast(ViewState("SOItems"), DataSet).Tables.Count > 0 AndAlso DirectCast(ViewState("SOItems"), DataSet).Tables(0).Rows.Count > 0 Then
                            warehouseID = CCommon.ToLong(DirectCast(ViewState("SOItems"), DataSet).Tables(0).Select("numWarehouseItmsID > 0").FirstOrDefault())
                        End If

                        If warehouseID = 0 AndAlso Not Session("DefaultWarehouse") Is Nothing Then
                            warehouseID = CCommon.ToLong(Session("DefaultWarehouse"))
                        End If

                        If warehouseID > 0 Then
                            Dim objItem As New CItems
                            objItem.WarehouseID = warehouseID
                            objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                            Dim dtWarehouseAddress As DataTable = objItem.GetWareHouses
                            If Not dtWarehouseAddress Is Nothing AndAlso dtWarehouseAddress.Rows.Count > 0 Then
                                lblShipFrom.Text = CCommon.ToString(dtWarehouseAddress.Rows(0)("vcAddress")).Replace("<pre>", "").Replace("</pre>", ", ")
                                hdnShipFromState.Value = CCommon.ToLong(dtWarehouseAddress.Rows(0)("numWState"))
                                hdnShipFromCountry.Value = CCommon.ToLong(dtWarehouseAddress.Rows(0)("numWCountry"))
                                hdnShipFromPostalCode.Value = CCommon.ToString(dtWarehouseAddress.Rows(0)("vcWPinCode"))
                            End If
                        Else
                            If Not dsAddress Is Nothing AndAlso dsAddress.Tables.Count > 0 Then
                                lblShipFrom.Text = CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcFullAddress")).Replace("<pre>", "").Replace("</pre>", ", ")
                                hdnShipFromState.Value = CCommon.ToLong(dsAddress.Tables(0).Rows(0)("numState"))
                                hdnShipFromCountry.Value = CCommon.ToLong(dsAddress.Tables(0).Rows(0)("numCountry"))
                                hdnShipFromPostalCode.Value = CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcPostalCode"))
                            End If
                        End If
                    End If

                    If Not dsAddress Is Nothing AndAlso dsAddress.Tables.Count > 1 Then
                        lblShipTo.Text = CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcFullAddress")).Replace("<pre>", "").Replace("</pre>", ", ")
                        hdnShipToState.Value = CCommon.ToLong(dsAddress.Tables(1).Rows(0)("numState"))
                        hdnShipToCountry.Value = CCommon.ToLong(dsAddress.Tables(1).Rows(0)("numCountry"))
                        hdnShipToPostalCode.Value = CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcPostalCode"))
                    End If


                    divShipping.Visible = True
                End If

                If OppType = 1 AndAlso OppStatus = 1 Then
                    Dim objMassSalesFulfillmentConfiguration As New MassSalesFulfillmentConfiguration
                    objMassSalesFulfillmentConfiguration.DomainID = CCommon.ToLong(Session("DomainID"))
                    hdnIsAutoWarehouseSelection.Value = CCommon.ToBool(objMassSalesFulfillmentConfiguration.GetColumnValue("bitAutoWarehouseSelection"))

                    If CCommon.ToBool(hdnIsAutoWarehouseSelection.Value) Then
                        Dim vcWarehousePriority As String = ""

                        Dim objMassSalesFulfillmentWM As New MassSalesFulfillmentWarehouseMapping
                        objMassSalesFulfillmentWM.DomainID = CCommon.ToLong(Session("DomainID"))
                        objMassSalesFulfillmentWM.OrderSource = OrderSource
                        objMassSalesFulfillmentWM.SourceType = SourceType
                        objMassSalesFulfillmentWM.ShipToCountry = CCommon.ToLong(hdnShipToCountry.Value)
                        objMassSalesFulfillmentWM.ShipToState = CCommon.ToLong(hdnShipToState.Value)
                        Dim dt As DataTable = objMassSalesFulfillmentWM.GetWarehousePriority()

                        If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                            For Each dr As DataRow In dt.Rows
                                vcWarehousePriority = vcWarehousePriority & IIf(vcWarehousePriority.Length > 0, ",", "") & CCommon.ToLong(dr("numWarehouseID"))
                            Next

                            hdnWarehousePriority.Value = CCommon.ToString(dt.Rows(0)("vcWarehousePriorities"))
                        Else
                            hdnWarehousePriority.Value = "-1"
                        End If
                    Else
                        hdnWarehousePriority.Value = "-1"
                    End If
                Else
                    hdnIsAutoWarehouseSelection.Value = False
                    hdnWarehousePriority.Value = "-1"
                End If

                objCommon = New CCommon
                Dim dtUnit As DataTable
                objCommon.DomainID = Session("DomainID")
                objCommon.UOMAll = True
                dtUnit = objCommon.GetItemUOM()

                With radcmbUOM
                    .DataSource = dtUnit
                    .DataTextField = "vcUnitName"
                    .DataValueField = "numUOMId"
                    .DataBind()
                    .Items.Insert(0, New RadComboBoxItem("--Select One--", "0"))
                End With

                Dim objOpportunity As New MOpportunity
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.OpportunityId = CCommon.ToLong(GetQueryStringVal("opid"))

                ViewState("UsedPromotion") = objOpportunity.GetUsedPromotions()

                If OppType = 2 Then
                    lblDateType.Text = "Item Required Date"
                    divVendorDetail.Visible = True
                    divCustomerPartNo.Style.Add("display", "none")
                    Dim objCommon As New CCommon
                    objCommon.DomainID = CCommon.ToLong(Session("DomainID"))

                    Dim dt As DataTable = objCommon.GetDomainSettingValue("vcPOHiddenColumns")

                    If Not dt Is Nothing AndAlso dt.Rows.Count > 0 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)(0))) Then
                        Dim vcPOHiddenColumns As String = "," & CCommon.ToString(dt.Rows(0)(0)) & ","

                        If vcPOHiddenColumns.Contains(",ItemID,") Then
                            dgItems.Columns(1).Visible = False
                        End If

                        If vcPOHiddenColumns.Contains(",Item,") Then
                            dgItems.Columns(2).Visible = False
                        End If

                        If vcPOHiddenColumns.Contains(",Description,") Then
                            dgItems.Columns(3).Visible = False
                        End If

                        If vcPOHiddenColumns.Contains(",SKU,") Then
                            dgItems.Columns(4).Visible = False
                        End If

                        If vcPOHiddenColumns.Contains(",Warehouse,") Then
                            dgItems.Columns(5).Visible = False
                        End If
                    End If
                End If

                ' Markup, Discount display logic from Global settings
                If OppType = 1 Then
                    ddlMarkupDiscountOption.Visible = True
                    lblDiscount.Visible = False
                    LoadMarkupDiscount()
                Else
                    ddlMarkupDiscountOption.Visible = False
                    lblDiscount.Visible = True
                End If

            End If

            Dim eventTarget As String = If((Me.Request("__EVENTTARGET") Is Nothing), String.Empty, Me.Request("__EVENTTARGET"))

            If eventTarget = "btnVendorCostChange" Then
                CalculateProfit()

            ElseIf eventTarget = "btnBindOrderGrid" AndAlso hdnType.Value = "add" Then
                dsTemp = ViewState("SOItems")
                hdnType.Value = ""
                UpdateDetails()
                clearControls()
                objCommon = Nothing
            Else
                If ViewState("SOItems") Is Nothing Then
                    createSet()
                End If
                'UpdateDetails()
                UpdateDataTable()


                If ddltype.SelectedValue = "S" Then
                    txtUnits.Attributes.Add("onkeypress", "CheckNumber(1,event);TempHideButton()")
                Else
                    txtUnits.Attributes.Add("onkeypress", "CheckNumber(2,event);TempHideButton()")
                End If

                txtprice.Attributes.Add("onkeypress", "CheckNumber(3,event)")
                txtItemDiscount.Attributes.Add("onkeypress", "CheckNumber(1,event)")
                radcmbUOM.Attributes.Add("OnChange", "TempHideButton()")

                If (_PageType = PageType.Purchase) Then
                    dgItems.Columns(8).Visible = False
                    radPer.Checked = False
                End If


                If (_PageType = PageType.AddOppertunity) Then
                    dgItems.Columns(8).Visible = False
                    btnSave.Text = "Save & Open"
                End If

                If (_PageType = PageType.Sales) Then
                    btnAddToCart.Visible = True
                End If

                If (_PageType = PageType.AddEditOrder) Then
                    btnSave.Text = "Save & Close"
                    btnSave.Width = New Unit(132)
                    If OppType = 2 Then
                        radPer.Checked = False
                    End If
                End If

                If (OppType = 2) Then
                    dgItems.Columns(9).Visible = False
                End If


                m_aryRightsForItems = GetUserRightsForPage_Other(10, 5)

                If m_aryRightsForItems(RIGHTSTYPE.ADD) = 0 Then btnUpdate.Visible = False
                If m_aryRightsForItems(RIGHTSTYPE.UPDATE) = 0 Then dgItems.Columns(11).Visible = False
                If m_aryRightsForItems(RIGHTSTYPE.DELETE) = 0 Then dgItems.Columns(12).Visible = False

                If _PageType = PageType.AddEditOrder Or _PageType = PageType.AddOppertunity Then
                    btnSave.Visible = True
                Else
                    btnSave.Visible = False
                End If
            End If

            If eventTarget = "DeleteAllItemsAndSave" Then
                btnSave_Click(Nothing, Nothing)
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

    Private Sub BindShippingCompanyAndMethods(ByRef radShipVia As RadComboBox, ByRef radCmbShippingMethod As RadComboBox)
        Try
            If objCommon Is Nothing Then objCommon = New CCommon
            radShipVia.DataSource = objCommon.GetMasterListItems(82, Session("DomainID"))
            radShipVia.DataTextField = "vcData"
            radShipVia.DataValueField = "numListItemID"
            radShipVia.DataBind()

            Dim listItem As Telerik.Web.UI.RadComboBoxItem
            If radShipVia.Items.FindItemByText("Fedex") IsNot Nothing Then
                radShipVia.Items.Remove(radShipVia.Items.FindItemByText("Fedex"))
            End If
            If radShipVia.Items.FindItemByText("UPS") IsNot Nothing Then
                radShipVia.Items.Remove(radShipVia.Items.FindItemByText("UPS"))
            End If
            If radShipVia.Items.FindItemByText("USPS") IsNot Nothing Then
                radShipVia.Items.Remove(radShipVia.Items.FindItemByText("USPS"))
            End If

            listItem = New Telerik.Web.UI.RadComboBoxItem
            listItem.Text = "-- Select One --"
            listItem.Value = "0"
            radShipVia.Items.Insert(0, listItem)

            listItem = New Telerik.Web.UI.RadComboBoxItem
            listItem.Text = "Fedex"
            listItem.Value = "91"
            listItem.ImageUrl = "../images/FedEx.png"
            radShipVia.Items.Insert(1, listItem)

            listItem = New Telerik.Web.UI.RadComboBoxItem
            listItem.Text = "UPS"
            listItem.Value = "88"
            listItem.ImageUrl = "../images/UPS.png"
            radShipVia.Items.Insert(2, listItem)

            listItem = New Telerik.Web.UI.RadComboBoxItem
            listItem.Text = "USPS"
            listItem.Value = "90"
            listItem.ImageUrl = "../images/USPS.png"
            radShipVia.Items.Insert(3, listItem)

            radCmbShippingMethod.Items.Clear()
            radCmbShippingMethod.Items.Add(New RadComboBoxItem("-- Select One --", "0"))
            OppBizDocs.LoadServiceTypes(radShipVia.SelectedValue, radCmbShippingMethod)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadMarkupDiscount()
        Try
            Dim objCommon As New CCommon
            Dim MarkupDiscountOption As DataTable
            objCommon.DomainID = CCommon.ToLong(Session("DomainID"))

            MarkupDiscountOption = objCommon.GetDomainSettingValue("tintMarkupDiscountOption")

            If (MarkupDiscountOption.Rows.Count > 0) Then
                ddlMarkupDiscountOption.SelectedValue = MarkupDiscountOption.Rows(0)("tintMarkupDiscountOption").ToString()
            End If

            MarkupDiscountOption = objCommon.GetDomainSettingValue("tintMarkupDiscountValue")

            If (MarkupDiscountOption.Rows.Count > 0) Then
                If (MarkupDiscountOption.Rows(0)("tintMarkupDiscountValue").ToString() = "1") Then
                    radPer.Checked = True
                Else
                    radAmt.Checked = True
                End If
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub CalculateProfit()
        Try
            If OppType = 1 Then

                Dim m_aryRightsForSalesProfit() As Integer
                m_aryRightsForSalesProfit = GetUserRightsForPage_Other(10, 21)

                If m_aryRightsForSalesProfit(RIGHTSTYPE.VIEW) <> 0 Then
                    Dim objItems As New CItems

                    tdSalesProfit.Visible = True

                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")

                    Dim decAverageCost, decProfit, decSellPrice As Decimal

                    decSellPrice = CCommon.ToDecimal(hdnUnitCost.Value)
                    objItems.ItemCode = CCommon.ToLong(hdnCurrentSelectedItem.Value)
                    objItems.DomainID = Session("DomainID")
                    objItems.GetItemDetails()


                    If btnUpdate.Text = "Update" Then
                        decAverageCost = CCommon.ToDouble(txtPUnitCost.Text)
                    Else
                        objItems.ItemCode = CCommon.ToLong(hdnCurrentSelectedItem.Value)
                        objItems.DomainID = Session("DomainID")
                        objItems.GetItemDetails()

                        If CCommon.ToShort(Session("numCost")) = 2 Then 'Vendor Cost
                            decAverageCost = CCommon.ToDecimal(IIf(IsDBNull(objItems.AverageCost), 0, objItems.monCost) * IIf(OppType = 1, hdvPurchaseUOMConversionFactor.Value, 1))
                        ElseIf CCommon.ToShort(Session("numCost")) = 3 Then 'Product & services cost
                            decAverageCost = CCommon.ToDecimal(IIf(IsDBNull(objItems.AverageCost), 0, objItems.monCost) * IIf(OppType = 1, hdvPurchaseUOMConversionFactor.Value, 1))
                        Else
                            decAverageCost = CCommon.ToDecimal(IIf(IsDBNull(objItems.AverageCost), 0, objItems.AverageCost) * IIf(OppType = 1, hdvPurchaseUOMConversionFactor.Value, 1))
                        End If
                        CCommon.SetValueInDecimalFormat(decAverageCost, txtPUnitCost)
                    End If

                    Dim dtUnit As DataTable
                    objCommon.DomainID = Session("DomainID")
                    objCommon.ItemCode = CCommon.ToLong(hdnCurrentSelectedItem.Value)
                    objCommon.UnitId = CCommon.ToLong(radcmbUOM.SelectedValue)
                    dtUnit = objCommon.GetItemUOMConversion()
                    hdvPurchaseUOMConversionFactor.Value = CCommon.ToDouble(dtUnit(0)("PurchaseUOMConversion"))
                    lblBaseUOMName.Text = CCommon.ToString(dtUnit(0)("vcBaseUOMName"))
                    CCommon.SetValueInDecimalFormat(decAverageCost, txtPUnitCost)

                    decProfit = (decSellPrice - decAverageCost)

                    'lblProfit.Text = String.Format("{0:#,##0.00} / {1:#,##0.00} ({2})", decProfit, decVendorCost, FormatPercent(decProfit / IIf(decVendorCost = 0, 1, decVendorCost), 2))
                    lblProfit.Text = String.Format("{0:#,##0.00} / {1:#,##0.00} ({2})", decProfit, decSellPrice, FormatPercent(decProfit / IIf(decSellPrice = 0, 1, decSellPrice), 2))
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadItemDetails(itemCode As String)
        Try
            If itemCode <> "" Then
                Dim dtTable, dtItemTax As DataTable
                If objItems Is Nothing Then objItems = New CItems

                Dim strUnit As String = String.Empty

                If CCommon.ToLong(txtHidEditOppItem.Text) > 0 And CCommon.ToLong(GetQueryStringVal("opid")) > 0 Then
                    Dim objOpportunity As New MOpportunity
                    objOpportunity.OppItemCode = CCommon.ToLong(txtHidEditOppItem.Text)
                    objOpportunity.OpportunityId = CCommon.ToLong(GetQueryStringVal("opid"))
                    objOpportunity.DomainID = Session("DomainID")
                    objOpportunity.Mode = 2
                    Dim ds As DataSet = objOpportunity.GetOrderItems()
                    If ds.Tables(0).Rows.Count > 0 Then
                        dtTable = ds.Tables(0)
                        dtTable.Columns("monPrice").ColumnName = "monListPrice"
                        dtTable.Columns("vcPathForTImage").ColumnName = "vcPathForImage"

                        If Not dtTable.Columns.Contains("txtItemDesc") Then
                            dtTable.Columns("vcItemDesc").ColumnName = "txtItemDesc"
                        End If

                        dtTable.AcceptChanges()
                        strUnit = "numUOM"
                    Else
                        objItems.ItemCode = CCommon.ToLong(itemCode)
                        dtTable = objItems.ItemDetails

                        If (OppType = 2) Then
                            strUnit = "numPurchaseUnit"
                        ElseIf (OppType = 1) Then
                            strUnit = "numSaleUnit"
                        End If
                    End If
                Else
                    objItems.ItemCode = CCommon.ToLong(itemCode)
                    dtTable = objItems.ItemDetails

                    If (OppType = 2) Then
                        strUnit = "numPurchaseUnit"
                    ElseIf (OppType = 1) Then
                        strUnit = "numSaleUnit"
                    End If
                End If

                If (_PageType = PageType.Purchase Or _PageType = PageType.Sales Or _PageType = PageType.AddOppertunity) Then
                    If IsStockTransfer Then
                        hplPrice.Attributes.Add("onclick", "return openItem('" & itemCode & "','" & txtUnits.ClientID & "','" & CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).ClientID & "','" & txtSerialize.ClientID & "',' " & OppType & "')")
                    Else
                        hplPrice.Attributes.Add("onclick", "return openItem('" & itemCode & "','" & txtUnits.ClientID & "','" & CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).ClientID & "','" & txtSerialize.ClientID & "',' " & OppType & "')")
                    End If
                ElseIf (_PageType = PageType.AddEditOrder) Then
                    hplPrice.Attributes.Add("onclick", "return openItem('" & itemCode & "','" & txtUnits.ClientID & "','" & GetQueryStringVal("DivId") & "','" & txtSerialize.ClientID & "','" & OppType & "')")
                End If

                Dim lngTempDivId As Long = 0
                If Not _PageType = PageType.AddEditOrder Then
                    If IsStockTransfer Then
                        lngTempDivId = IIf(CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue)
                    Else
                        lngTempDivId = IIf(CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue)
                    End If
                Else
                    lngTempDivId = CCommon.ToLong(GetQueryStringVal("DivId"))
                End If

                If (OppType = 2) Then
                    hplUnits.Attributes.Add("onclick", "return openVendorCostTable('2'," & itemCode & "," & lngTempDivId & ")")
                    hplPrice.Text = "Unit Cost"
                ElseIf (OppType = 1) Then
                    hplUnitCost.Attributes.Add("onclick", "return openVendorCostTable('1'," + itemCode & "," & lngTempDivId & ")")
                    hplPrice.Text = "Unit Price"
                End If

                Dim strApplicable As String = String.Empty

                If (_PageType = PageType.Sales) Then
                    objItems.DomainID = Session("DomainID")
                    dtItemTax = objItems.ItemTax()

                    For Each dr As DataRow In dtItemTax.Rows
                        strApplicable = strApplicable & dr("bitApplicable") & ","
                    Next
                End If

                If dtTable.Rows.Count > 0 Then

                    If (_PageType = PageType.Sales) Then
                        strApplicable = strApplicable & dtTable.Rows(0).Item("bitTaxable")
                        Taxable.Value = strApplicable
                    End If

                    If dtTable.Rows(0).Item("bitKitParent") = True And dtTable.Rows(0).Item("bitAssembly") = False Then
                        hdKit.Value = dtTable.Rows(0).Item("bitKitParent")
                    Else : hdKit.Value = ""
                    End If

                    If OppType = 1 And dtTable.Rows(0).Item("bitAssembly") = True Then
                        divWorkOrder.Visible = True
                    Else
                        divWorkOrder.Visible = False
                    End If

                    'lblUnits.Text = dtTable.Rows(0).Item("vcUnitofMeasure")
                    ddltype.SelectedItem.Selected = False
                    If ddltype.Items.FindByValue(dtTable.Rows(0).Item("charItemType")) IsNot Nothing Then
                        ddltype.Items.FindByValue(dtTable.Rows(0).Item("charItemType")).Selected = True
                        lblItemType.Text = ddltype.SelectedItem.Text
                    Else
                        lblItemType.Text = ""
                    End If

                    txtdesc.Text = CCommon.ToString(dtTable.Rows(0).Item("txtItemDesc"))
                    txtModelID.Text = CCommon.ToString(dtTable.Rows(0).Item("vcModelID"))
                    txtCustomerPartNo.Text = CCommon.ToString(dtTable.Rows(0).Item("CustomerPartNo"))

                    If dtTable.Rows(0).Item("charItemType") = "P" Then
                        divWarehouse.Visible = True
                        If dtTable.Rows(0).Item("bitSerialized") = True Then
                            txtSerialize.Text = 1
                        Else : txtSerialize.Text = 0
                        End If
                        SetDropshipCheckbox(chkDropShip.Checked, True)
                    Else
                        SetDropshipCheckbox(False, False)
                        If dtTable.Rows(0).Item("bitSerialized") = True Then
                            txtSerialize.Text = 1
                        Else : txtSerialize.Text = 0
                        End If
                        txtHidValue.Text = ""
                        divWarehouse.Visible = False
                        radWareHouse.SelectedValue = ""
                    End If

                    SetDropshipCheckbox(CCommon.ToBool(dtTable.Rows(0).Item("bitAllowDropShip")), chkDropShip.Visible)
                    hdnPrimaryVendorID.Value = CCommon.ToLong(dtTable.Rows(0).Item("numVendorID"))

                    hdnItemWeight.Value = CCommon.ToDouble(dtTable.Rows(0).Item("fltWeight"))
                    hdnFreeShipping.Value = CCommon.ToBool(dtTable.Rows(0).Item("bitFreeShipping"))
                    hdnHeight.Value = CCommon.ToDouble(dtTable.Rows(0).Item("fltHeight"))
                    hdnWidth.Value = CCommon.ToDouble(dtTable.Rows(0).Item("fltWidth"))
                    hdnLength.Value = CCommon.ToDouble(dtTable.Rows(0).Item("fltLength"))
                    hdnSKU.Value = CCommon.ToString(dtTable.Rows(0).Item("vcSKU"))
                    'If CCommon.ToLong(txtHidEditOppItem.Text) > 0 And CCommon.ToLong(GetQueryStringVal( "opid")) > 0 Then
                    '    strUnit = "numUOM"
                    'Else
                    '    If (OppType = 2) Then
                    '        strUnit = "numPurchaseUnit"
                    '    ElseIf (OppType = 1) Then
                    '        strUnit = "numSaleUnit"
                    '    End If
                    'End If
                    If strUnit.Length > 0 Then
                        If radcmbUOM.Items.FindItemByValue(dtTable.Rows(0).Item(strUnit)) IsNot Nothing Then
                            radcmbUOM.ClearSelection()
                            radcmbUOM.Items.FindItemByValue(dtTable.Rows(0).Item(strUnit)).Selected = True
                        End If
                    End If

                End If


            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub SetDropshipCheckbox(ByVal isChecked As Boolean, ByVal isVisible As Boolean)
        Try
            If OppType = 1 Then
                chkDropShip.Checked = isChecked
                chkDropShip.Visible = isVisible
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function IsDuplicate(ByVal ItemCode As String, ByVal WareHouseItemID As String, ByVal DropShip As Boolean) As Boolean
        'validate duplicate entry on refresh / when js is disabled
        If Not CCommon.ToBool(Session("IsAllowDuplicateLineItems")) AndAlso (dsTemp.Tables(0).Select(" numItemCode='" & ItemCode & "'" & IIf(WareHouseItemID <> "", " and numWarehouseItmsID = '" & WareHouseItemID & "'", "") & " and DropShip ='" & DropShip & "'").Length > 0) Then
            UpdateDetails()
            Return True
        End If
        Return False
    End Function

    Private Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            If CCommon.ToDouble(txtUnits.Text) = 0 Then
                ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "UnitsValidation", "alert(""Enter Units"");", True)
                txtUnits.Focus()
                Return
            End If
            If OppType = 2 Then
                Dim objVendorCostTable As New VendorCostTable
                objVendorCostTable.DomainID = CCommon.ToLong(Session("DomainID"))
                objVendorCostTable.UserCntID = CCommon.ToLong(Session("UserContactID"))
                If Not _PageType = PageType.AddEditOrder Then
                    If IsStockTransfer Then
                        objVendorCostTable.VendorID = IIf(CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue)
                    Else
                        objVendorCostTable.VendorID = IIf(CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue)
                    End If
                Else
                    objVendorCostTable.VendorID = CCommon.ToLong(GetQueryStringVal("DivId"))
                End If
                If CCommon.ToLong(txtHidEditOppItem.Text) > 0 Then
                    objVendorCostTable.ItemCode = CCommon.ToLong(txtHidEditOppItem.Text)
                ElseIf hdnSelectedItems.Value <> "" Then
                    Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer()
                    Dim objSelectedItem As SelectedItems = serializer.Deserialize(Of SelectedItems)(hdnSelectedItems.Value)
                    objVendorCostTable.ItemCode = objSelectedItem.numItemCode
                End If
                If (_PageType = PageType.AddEditOrder) Then
                    objVendorCostTable.CurrencyID = CCommon.ToLong(CType(Me.Parent.FindControl("hdnCurrencyID"), HiddenField).Value)
                Else
                    objVendorCostTable.CurrencyID = CCommon.ToLong(CType(Me.Parent.FindControl("ddlCurrency"), DropDownList).SelectedValue)
                End If

                If objVendorCostTable.ItemCode > 0 AndAlso Not objVendorCostTable.IsRangeExist(CCommon.ToDecimal(txtUnits.Text * txtUOMConversionFactor.Text)) Then
                    ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "OpenVendorCostTable", "openVendorCostTable(2," & objVendorCostTable.ItemCode & "," & objVendorCostTable.VendorID & ")", True)
                    Exit Sub
                End If
            End If
            If txtprice.Text.Trim() = "" Then
                CCommon.SetValueInDecimalFormat(0, txtprice)
            End If
            If txtItemDiscount.Text.Trim() = "" Then
                CCommon.SetValueInDecimalFormat(0, txtItemDiscount)
            End If

            ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "HidePOVendor", "$('#imgPODistance').hide(); $('#imgPODistance').removeAttr('onclick');", True)

            If OppType = 1 Then
                'IF Promotion discount is more than line item total than set discount value to the total of line item
                If CCommon.ToLong(hdnPromotionID.Value) > 0 AndAlso Not CCommon.ToBool(hdnPromotionTriggered.Value) Then
                    Dim drPromotion As DataRow = DirectCast(ViewState("UsedPromotion"), DataTable).Select("numProId=" & hdnPromotionID.Value)(0)

                    If drPromotion("tintDiscountType") = "2" AndAlso CCommon.ToDecimal(txtItemDiscount.Text) > CCommon.ToDecimal(txtUnits.Text * txtUOMConversionFactor.Text * hdnListPrice.Value) Then
                        CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(txtUnits.Text * txtUOMConversionFactor.Text * hdnListPrice.Value), txtItemDiscount)
                    End If
                End If
            End If

            If CCommon.ToLong(txtHidEditOppItem.Text) > 0 Then
                If OppType = 1 AndAlso chkWorkOrder.Checked AndAlso CCommon.ToLong(txtWOQty.Text) > CCommon.ToLong(lblWOQty.Text) Then
                    ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "WOQtyValidation", "alert('You’ve exceeded the qty of finished product that can be assembled. Child items may be placed in backorder.')", True)
                End If

                litMessage.Text = ""
                dsTemp = ViewState("SOItems")

                If chkWorkOrder.Checked Then
                    If ChartOfAccounting.GetDefaultAccount("WP", Session("DomainID")) = 0 Then
                        ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "Validation", "alert('Can not save record, your option is to set default Work in Progress Account from Administration->Global Settings->Accounting tab->Default Accounts')", True)
                        Exit Sub
                    End If
                End If

                AddItemToSession()
                UpdateDataTable()
            Else
                litMessage.Text = ""
                hdnApprovalActionTaken.Value = ""
                hdnClickedButton.Value = ""
                hdnIsUnitPriceApprovalRequired.Value = ""

                dsTemp = ViewState("SOItems")
                Dim dtitem As DataTable
                Dim MaxRowOrder As Long
                MaxRowOrder = dsTemp.Tables(0).Rows.Count
                dtitem = dsTemp.Tables(2)
                dtitem.Rows.Clear()

                Dim alert As String
                Dim alertMessage As String
                Dim account As String
                Dim itemWithoutWarehouse As String

                If hdnSelectedItems.Value <> "" Then
                    Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer()
                    Dim objSelectedItem As SelectedItems = serializer.Deserialize(Of SelectedItems)(hdnSelectedItems.Value)

                    If objSelectedItem.bitHasKitAsChild AndAlso String.IsNullOrEmpty(hdnKitChildItems.Value) Then
                        alert = "alert(""Child kit items are not configured. please try to add kit item again"")"
                        ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "AccountValidation", alert, True)
                        Return
                    End If

                    Dim objItem As New CItems
                    objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                    If Not _PageType = PageType.AddEditOrder Then
                        If IsStockTransfer Then
                            objItem.DivisionID = IIf(CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue)
                        Else
                            objItem.DivisionID = IIf(CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue)
                        End If
                    Else
                        objItem.DivisionID = CCommon.ToLong(GetQueryStringVal("DivId"))
                    End If
                    objItem.byteMode = CCommon.ToShort(OppType)
                    objItem.ItemCode = objSelectedItem.numItemCode
                    objItem.WareHouseItemID = CCommon.ToLong(radWareHouse.SelectedValue)
                    objItem.GetItemAndWarehouseDetails()
                    objItem.ItemDesc = txtdesc.Text
                    objItem.CustomerPartNo = txtCustomerPartNo.Text
                    objItem.bitHasKitAsChild = objSelectedItem.bitHasKitAsChild
                    objItem.vcKitChildItems = hdnKitChildItems.Value
                    objItem.SelectedUOMID = radcmbUOM.SelectedValue
                    objItem.SelectedUOM = IIf(radcmbUOM.SelectedValue > 0, radcmbUOM.Text, "")
                    objItem.AttributeIDs = objSelectedItem.vcAttributeIDs
                    objItem.Attributes = objSelectedItem.vcAttributes
                    MaxRowOrder = MaxRowOrder + 1
                    objItem.numSortOrder = MaxRowOrder

                    If objItem.ItemType = "P" AndAlso Not chkDropShip.Checked AndAlso CCommon.ToLong(objItem.WareHouseItemID) = 0 Then
                        ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "NoWarehouse", "alert(""Please select warehouse/location."")", True)
                        Return
                    ElseIf objItem.ItemType = "P" AndAlso IsDuplicate(objItem.ItemCode, objItem.WareHouseItemID, chkDropShip.Checked) Then
                        ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "DuplicateItemWarehouse", "alert(""Inventory item with same warehouse location is already added. Please select another location."")", True)
                        Return
                    End If

                    If (OppType = 1) Then
                        If (objItem.numContainer > 0) Then
                            Dim containerqty As Long = Math.Ceiling((CCommon.ToDouble(txtUnits.Text) * If(CCommon.ToDouble(txtUOMConversionFactor.Text) > 0, CCommon.ToDouble(txtUOMConversionFactor.Text), 1.0)) / objItem.numNoItemIntoContainer)

                            Dim objContainerItem As New CItems
                            objContainerItem.DomainID = CCommon.ToLong(Session("DomainID"))
                            objContainerItem.WarehouseID = objItem.WarehouseID
                            objContainerItem.ItemCode = objItem.numContainer
                            Dim dtContainer As DataTable = objContainerItem.GetItemWarehouseLocations()

                            If Not dtContainer Is Nothing AndAlso dtContainer.Rows.Count > 0 Then
                                Dim objItem1 As New CItems
                                objItem1.byteMode = CCommon.ToShort(OppType)
                                objItem1.ItemCode = objItem.numContainer
                                objItem1.WareHouseItemID = CCommon.ToLong(dtContainer.Rows(0)("numWarehouseItemID"))
                                objItem1.GetItemAndWarehouseDetails()

                                objItem1.bitHasKitAsChild = False
                                objItem1.vcKitChildItems = ""
                                MaxRowOrder = MaxRowOrder + 1
                                objItem1.numSortOrder = MaxRowOrder
                                objItem1.SelectedUOMID = objItem1.BaseUnit
                                objItem1.SelectedQuantity = containerqty
                                objItem1.AttributeIDs = ""
                                objItem1.Attributes = ""
                                objItem1.numNoItemIntoContainer = -1

                                If objItem.ItemType = "P" AndAlso IsDuplicate(objItem1.ItemCode, objItem1.WareHouseItemID, chkDropShip.Checked) Then
                                    If objItem1.numNoItemIntoContainer = -1 Then
                                        Dim dsContainer As DataSet
                                        dsContainer = ViewState("SOItems")

                                        Dim itemQtyInContainer As Double = 0
                                        Dim containerMaxQty As Double = 0


                                        Dim arrContainers As DataRow() = dsContainer.Tables(0).Select("numItemCode=" & objItem1.ItemCode & " AND numWarehouseItmsID=" & objItem1.WareHouseItemID)
                                        If Not arrContainers Is Nothing AndAlso arrContainers.Length > 0 Then
                                            For Each drSameContainer As DataRow In arrContainers
                                                containerMaxQty = containerMaxQty + CCommon.ToDouble(drSameContainer("numUnitHour") * objItem.numNoItemIntoContainer)
                                            Next
                                        End If

                                        'Check if there are items exists which uses same container and has same qty which can be added to container and container qty is not fully used
                                        Dim arrItems As DataRow() = dsContainer.Tables(0).Select("numContainer=" & objItem1.ItemCode & " AND numWarehouseID=" & objItem1.WarehouseID & " AND numContainerQty=" & objItem.numNoItemIntoContainer)
                                        If Not arrItems Is Nothing AndAlso arrItems.Length > 0 Then
                                            For Each drSameContainer As DataRow In arrItems
                                                itemQtyInContainer = itemQtyInContainer + CCommon.ToDouble(drSameContainer("numUnitHour") * drSameContainer("UOMConversionFactor"))
                                            Next
                                        End If

                                        If containerMaxQty > 0 AndAlso itemQtyInContainer > 0 Then
                                            Dim itemCanbeAddedToContainer As Double = containerMaxQty - itemQtyInContainer

                                            If itemCanbeAddedToContainer < CCommon.ToDouble(txtUnits.Text) * If(CCommon.ToDouble(txtUOMConversionFactor.Text) > 0, CCommon.ToDouble(txtUOMConversionFactor.Text), 1.0) Then
                                                containerqty = containerqty - (CCommon.ToDouble(txtUnits.Text) * If(CCommon.ToDouble(txtUOMConversionFactor.Text) > 0, CCommon.ToDouble(txtUOMConversionFactor.Text), 1.0) - itemCanbeAddedToContainer)
                                                objItem1.SelectedQuantity = containerqty
                                                Dim drContainer As DataRow() = dsContainer.Tables(0).Select("numItemCode=" & objItem1.ItemCode & " AND numWarehouseItmsID=" & objItem1.WareHouseItemID)
                                                For Each drowContainer As DataRow In drContainer
                                                    drowContainer("numUnitHour") = (drowContainer("numUnitHour")) + objItem1.SelectedQuantity
                                                Next
                                            End If
                                        Else
                                            Dim drContainer As DataRow() = dsContainer.Tables(0).Select("numItemCode=" & objItem1.ItemCode & " AND numWarehouseItmsID=" & objItem1.WareHouseItemID)
                                            For Each drowContainer As DataRow In drContainer
                                                drowContainer("numUnitHour") = (drowContainer("numUnitHour")) + objItem1.SelectedQuantity
                                            Next
                                        End If

                                        AddItemToSession(objItem)
                                        ViewState("SOItems") = dsContainer
                                        dsTemp = dsContainer
                                    End If
                                Else
                                    AddItemToSession(objItem)
                                    AddItemToSession(objItem1)
                                End If
                            Else
                                ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "DuplicateItemWarehouse", "alert(""Item associated container item does not contain warehouse. Please add warehouse to container first."")", True)
                                Return
                            End If
                        Else
                            AddItemToSession(objItem)
                        End If
                    Else
                        AddItemToSession(objItem)
                    End If

                    If OppType = 1 Then
                        AddSimilarRequiredRow()
                    End If
                    UpdateDetails()

                    ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "ClearSelectedItems", "$('#txtItem').select2('data', null);", True)
                    hdnSelectedItems.Value = ""
                End If

                If OppType = 1 AndAlso CCommon.ToLong(hdnPromotionID.Value) > 0 AndAlso CCommon.ToBool(hdnPromotionTriggered.Value) Then
                    Dim drPromotion As DataRow = DirectCast(ViewState("UsedPromotion"), DataTable).Select("numProId=" & hdnPromotionID.Value)(0)

                    If drPromotion("tintDiscoutBaseOn") = "3" AndAlso ((drPromotion("tintOfferBasedOn") = 1 AndAlso CCommon.ToString(drPromotion("vcPromotionItems")).Split(",").Contains(hdnCurrentSelectedItem.Value)) Or (drPromotion("tintOfferBasedOn") = 2 AndAlso CCommon.ToString(drPromotion("vcPromotionItems")).Split(",").Contains(hdnItemClassification.Value))) Then
                        lblRelatedItemPromotion.Text = lblPromotionDescription.Text
                        hdnRelatedItemPromotionID.Value = hdnPromotionID.Value
                        plhPromotion.Visible = True
                        hplRelatedItems_Click(Nothing, Nothing)
                    End If
                End If
                UpdateDataTable()
            End If

            btnUpdate.Text = "Add"
            btnUpdate.Style.Add("display", "none")
            btnEditCancel.Style.Add("display", "none")
            lkbEditKitSelection.Visible = False
            clearControls()
            ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "FocusItem", "FocuonItem();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            objCommon = Nothing
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Sub AddItemToSession()
        Try
            Dim objItem As New CItems
            objItem.byteMode = CCommon.ToShort(OppType)
            objItem.ItemCode = hdnCurrentSelectedItem.Value
            objItem.WareHouseItemID = CCommon.ToLong(radWareHouse.SelectedValue)
            If OppType = 1 Then
                objItem.vcKitChildItems = hdnKitChildItems.Value
            End If
            objItem.GetItemAndWarehouseDetails()

            Dim chkTaxItems As CheckBoxList
            If OppType = 1 Then
                chkTaxItems = CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList)
            End If

            objCommon = New CCommon
            objCommon.DomainID = CCommon.ToLong(Session("DomainID"))

            If OppType = 1 Then
                Dim dr As DataRow = dsTemp.Tables(0).Rows.Find(CCommon.ToLong(txtHidEditOppItem.Text))

                If Not dr Is Nothing Then
                    If CCommon.ToLong(dr("numPromotionID")) > 0 AndAlso Not chkUsePromotion.Checked Then
                        dr("numPromotionID") = 0
                        dr("bitPromotionTriggered") = False
                        dr("vcPromotionDescription") = ""
                        dr("bitDisablePromotion") = True
                    End If
                End If
            End If

            ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon, dsTemp, False, ddltype.SelectedValue,
                                                                               ddltype.SelectedItem.Text, chkDropShip.Checked, hdKit.Value, hdnCurrentSelectedItem.Value,
                                                                                Math.Abs(CCommon.ToDecimal(txtUnits.Text)),
                                                                                CCommon.ToDecimal(txtprice.Text),
                                                                               txtdesc.Text,
                                                                               CCommon.ToLong(radWareHouse.SelectedValue), "", radWareHouse.Text,
                                                                               CCommon.ToLong(txtHidEditOppItem.Text), txtModelID.Text.Trim,
                                                                               CCommon.ToLong(radcmbUOM.SelectedValue),
                                                                               If(CCommon.ToLong(radcmbUOM.SelectedValue) > 0, radcmbUOM.SelectedItem.Text, ""),
                                                                               If(CCommon.ToDouble(txtUOMConversionFactor.Text) = 0, 1, CCommon.ToDouble(txtUOMConversionFactor.Text)),
                                                                               _PageType.ToString(), IIf(radPer.Checked, True, False),
                                                                               IIf(IsNumeric(txtItemDiscount.Text), txtItemDiscount.Text, 0), CCommon.ToString(Taxable.Value), txtTax.Text, txtCustomerPartNo.Text, chkTaxItems,
                                                                               IIf(chkWorkOrder.Checked, 1, 0), txtInstruction.Text, CCommon.ToString(radCalCompliationDate.SelectedDate),
                                                                               0,
                                                                               0,
                                                                               CCommon.ToLong(GetQueryStringVal("Source")), CCommon.ToLong(GetQueryStringVal("StageID")),
                                                                               objItem:=objItem,
                                                                               vcBaseUOMName:=lblBaseUOMName.Text,
                                                                               ToWarehouseItemID:=CCommon.ToLong(radWarehouseTo.SelectedValue),
                                                                               numPrimaryVendorID:=CCommon.ToLong(hdnPrimaryVendorID.Value),
                                                                               fltItemWeight:=CCommon.ToLong(hdnItemWeight.Value),
                                                                               IsFreeShipping:=CCommon.ToBool(hdnFreeShipping.Value),
                                                                               fltHeight:=CCommon.ToDouble(hdnHeight.Value),
                                                                               fltWidth:=CCommon.ToDouble(hdnWidth.Value),
                                                                               fltLength:=CCommon.ToDouble(hdnLength.Value),
                                                                               strSKU:=CCommon.ToString(hdnSKU.Value),
                                                                               numMaxWOQty:=CCommon.ToInteger(lblWOQty.Text),
                                                                               numItemClassification:=CCommon.ToLong(hdnItemClassification.Value),
                                                                               numPromotionID:=CCommon.ToLong(hdnPromotionID.Value),
                                                                               IsPromotionTriggered:=CCommon.ToBool(hdnPromotionTriggered.Value),
                                                                               vcPromotionDetail:=hdnPromotionDescription.Value,
                                                                               numSortOrder:=objItem.numSortOrder,
                                                                               numCost:=CCommon.ToDouble(txtPUnitCost.Text),
                                                                               vcVendorNotes:=CCommon.ToString(txtNotes.Text),
                                                                               bitMarkupDiscount:=ddlMarkupDiscountOption.SelectedValue,
                                                                               bitAlwaysCreateWO:=CCommon.ToBool(hdnIsCreateWO.Value),
                                                                               numWOQty:=CCommon.ToDouble(txtWOQty.Text),
                                                                               dtPlannedStart:=CCommon.ToString(rdpPlannedStart.SelectedDate),
                                                                               itemReleaseDate:=If(OppType = 1, Convert.ToDateTime(rdpItemDate.SelectedDate), Nothing),
                                                                               itemRequiredDate:=If(OppType = 2, Convert.ToDateTime(rdpItemDate.SelectedDate), Nothing)
            )

            'BindGrid()
            If OppType = 1 Then
                RecalculateContainerQty(dsTemp.Tables(0))
            End If
            UpdateDetails()

            clearControls()
            objCommon = Nothing

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Public Sub clearControls()
        hdnCurrentSelectedItem.Value = ""
        hdnIsMatrix.Value = ""
        hdnKitChildItems.Value = ""
        hdnHasKitAsChild.Value = ""
        hdnIsSKUMatch.Value = ""
        txtUnits.Text = "1"
        txtprice.Text = ""
        lblBaseUOMName.Text = ""
        txtdesc.Text = ""
        txtNotes.Text = ""
        lblMinOrderQty.Text = "-"
        divMinOrderQty.Visible = False
        ddltype.SelectedIndex = 0
        txtItemDiscount.Text = ""
        chkUsePromotion.Checked = False
        radPer.Checked = True
        txtUnits.Enabled = True
        If hdnEditUnitPriceRight.Value <> "False" Then
            txtprice.Enabled = True
        End If
        txtItemDiscount.Enabled = True
        radcmbUOM.Enabled = True
        hdnItemClassification.Value = ""
        radPer.Enabled = True
        radAmt.Enabled = True

        hplRelatedItems.Visible = False
        hplLastPrices.Visible = False

        radWareHouse.Items.Clear()
        radWareHouse.Text = ""
        radWareHouse.DataSource = Nothing
        radWareHouse.DataBind()
        lblOnHand.Text = ""
        lblOnAllocation.Text = ""
        hplOnOrder.Text = ""
        lblOnBackOrder.Text = ""
        rdpItemDate.SelectedDate = Nothing
        If OppType = 1 Or (OppType = 2 AndAlso Not chkDropShip.Checked) Then
            radWareHouse.Enabled = True
            divWarehouse.Visible = True
        End If

        ddlPriceLevel.DataSource = Nothing
        ddlPriceLevel.DataBind()
        ddlPriceLevel.Enabled = True
        spnPriceLevel.Visible = False
        divWarehouse.Style.Add("display", "")
        SetDropshipCheckbox(False, False)
        chkDropShip.Enabled = True

        divWorkOrder.Visible = False
        chkWorkOrder.Checked = False
        hdnIsCreateWO.Value = "0"
        txtWOQty.Text = "0"
        txtWOQty.Enabled = True
        divUnitCost.Visible = False
        divProfit.Visible = False

        txtUOMConversionFactor.Text = ""
        lblBaseUOMName.Text = "-"
        btnOptionsNAccessories.Visible = False
        txtModelID.Text = ""

        txtHidEditOppItem.Text = ""
        hdnUnitCost.Value = "0"
        hdnVendorId.Value = "0"
        lblProfit.Text = ""

        radcmbUOM.SelectedValue = "0"
        radcmbUOM.Enabled = True
        hdnPrimaryVendorID.Value = "0"
        divPricingOption.Visible = False
        lkbPriceLevel.Visible = True
        lkbPriceRule.Visible = True
        lkbLastPrice.Visible = True
        lkbListPrice.Visible = True
        lkbEditKitSelection.Visible = False
        lkbPriceLevel.Text = "Use Price Level"
        lkbPriceRule.Text = "Use Price Rule"
        lkbLastPrice.Text = "Use Last Price"
        lkbListPrice.Text = "Use List Price"
        lkbAddPromotion.Text = "Use Promotion"
        lkbPriceLevel.Visible = True
        lkbPriceRule.Visible = True
        lkbLastPrice.Visible = True
        lkbListPrice.Visible = True
        hdnListPrice.Value = ""
        hdnLastPrice.Value = ""
        hdnPricingBasedOn.Value = ""
        hdnBaseUOMName.Value = ""
        hdnVendorCost.Value = ""
        hplRelatedItems.Visible = False
        hplLastPrices.Visible = False

        hdnPromotionID.Value = ""
        hdnPromotionTriggered.Value = False
        hdnPromotionDescription.Value = ""
        divPromotionDetail.Visible = False
        lblPromotionName.Text = ""
        lblPromotionDescription.Text = ""
        lblExpiration.Text = ""
        lblPromotionDescription.Text = ""
        divCouponCode.Visible = False
        txtCouponCode.Text = ""
        divPromotionShippingDetail.Visible = False
        lblShippingDetail.Text = ""
        lkbAddPromotion.Visible = False
        txtCustomerPartNo.Text = ""

        gvMultiSelectItems.DataSource = Nothing
        gvMultiSelectItems.DataBind()
        divMultiSelect.Visible = False

        lkbEditKitSelection.Visible = False

        ViewState("DataTableTempPromotion") = Nothing
        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "ClearSelectedItems", "$('#divItem').show(); $('#txtItem').select2('data', null);", True)
    End Sub

    Private Sub BindGrid()
        If Not dsTemp Is Nothing AndAlso dsTemp.Tables.Count > 0 Then

            If Not CCommon.ToBool(Session("IsMinUnitPriceRule")) Then
                For Each dr As DataRow In dsTemp.Tables(0).Rows
                    dr("bitItemPriceApprovalRequired") = False
                Next
            End If

            dgItems.DataSource = dsTemp.Tables(0)
            dgItems.DataBind()
        End If
    End Sub

    Sub createSet()
        Try

            If Not IsPostBack And Not ViewState("SOItems") Is Nothing Then
                Dim ds1 As DataSet = ViewState("SOItems")
                If ds1.Tables(0).Rows.Count > 0 Then
                    dsTemp = ViewState("SOItems")
                    Exit Sub
                End If
            End If
            dsTemp = New DataSet
            Dim dtItem As New DataTable
            Dim dtSerItem As New DataTable
            Dim dtChildItems As New DataTable
            dtItem.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
            dtItem.Columns.Add("numItemCode")
            dtItem.Columns.Add("numItemClassification")
            dtItem.Columns.Add("numUnitHour", GetType(Decimal))

            dtItem.Columns.Add("monPrice", GetType(Decimal))
            dtItem.Columns.Add("numUOM")
            dtItem.Columns.Add("vcUOMName")
            dtItem.Columns.Add("UOMConversionFactor")

            dtItem.Columns.Add("monTotAmount", GetType(Decimal))
            dtItem.Columns.Add("numSourceID")
            dtItem.Columns.Add("vcItemDesc")
            dtItem.Columns.Add("vcModelID")
            dtItem.Columns.Add("numWarehouseID")
            dtItem.Columns.Add("vcItemName")
            dtItem.Columns.Add("Warehouse")
            dtItem.Columns.Add("numWarehouseItmsID")
            dtItem.Columns.Add("ItemType")
            dtItem.Columns.Add("Attributes")
            dtItem.Columns.Add("AttributeIDs")
            dtItem.Columns.Add("Op_Flag")
            dtItem.Columns.Add("bitWorkOrder")
            dtItem.Columns.Add("numMaxWorkOrderQty")
            dtItem.Columns.Add("vcInstruction")
            dtItem.Columns.Add("bintCompliationDate")
            dtItem.Columns.Add("dtPlannedStart")
            dtItem.Columns.Add("numWOAssignedTo")
            dtItem.Columns.Add("bitAlwaysCreateWO")
            dtItem.Columns.Add("numWOQty")

            dtItem.Columns.Add("DropShip", GetType(Boolean))
            dtItem.Columns.Add("bitDiscountType", GetType(Boolean))
            dtItem.Columns.Add("fltDiscount", GetType(Decimal))
            dtItem.Columns.Add("monTotAmtBefDiscount", GetType(Decimal))

            dtItem.Columns.Add("bitTaxable0")
            dtItem.Columns.Add("Tax0", GetType(Decimal))
            dtItem.Columns.Add("TotalTax", GetType(Decimal))

            dtItem.Columns.Add("numVendorWareHouse")
            dtItem.Columns.Add("numShipmentMethod")
            dtItem.Columns.Add("numSOVendorId")

            dtItem.Columns.Add("numProjectID")
            dtItem.Columns.Add("numProjectStageID")
            dtItem.Columns.Add("charItemType")
            dtItem.Columns.Add("numToWarehouseItemID") 'added by chintan for stock transfer
            dtItem.Columns.Add("bitIsAuthBizDoc", GetType(Boolean))
            dtItem.Columns.Add("numUnitHourReceived")
            dtItem.Columns.Add("numQtyShipped")
            dtItem.Columns.Add("vcBaseUOMName")
            dtItem.Columns.Add("numVendorID", System.Type.GetType("System.Int32"))
            dtItem.Columns.Add("fltItemWeight")
            dtItem.Columns.Add("IsFreeShipping")
            dtItem.Columns.Add("fltHeight")
            dtItem.Columns.Add("fltWidth")
            dtItem.Columns.Add("fltLength")
            dtItem.Columns.Add("vcSKU")
            dtItem.Columns.Add("bitItemPriceApprovalRequired")
            dtItem.Columns.Add("bitAddedFulFillmentBizDoc", GetType(Boolean))
            dtItem.Columns.Add("bitHasKitAsChild", GetType(Boolean))
            dtItem.Columns.Add("KitChildItems", GetType(String))
            dtItem.Columns.Add("numPromotionID")
            dtItem.Columns.Add("bitPromotionTriggered")
            dtItem.Columns.Add("vcPromotionDetail")
            dtItem.Columns.Add("numSortOrder")
            dtItem.Columns.Add("numCost", GetType(Decimal))
            dtItem.Columns.Add("vcVendorNotes")
            dtItem.Columns.Add("CustomerPartNo")
            dtItem.Columns.Add("bitMarkupDiscount")
            dtItem.Columns.Add("ItemReleaseDate")
            dtItem.Columns.Add("ItemRequiredDate")

            If (_PageType = PageType.Sales) Then
                '''Loading Other Taxes
                Dim dtTaxTypes As DataTable
                Dim ObjTaxItems As New TaxDetails
                ObjTaxItems.DomainID = Session("DomainID")
                dtTaxTypes = ObjTaxItems.GetTaxItems

                For Each drTax As DataRow In dtTaxTypes.Rows
                    dtItem.Columns.Add("Tax" & drTax("numTaxItemID"), GetType(Decimal))
                    dtItem.Columns.Add("bitTaxable" & drTax("numTaxItemID"))
                Next

                Dim dr As DataRow
                dr = dtTaxTypes.NewRow
                dr("numTaxItemID") = 0
                dr("vcTaxName") = "Sales Tax(Default)"
                dtTaxTypes.Rows.Add(dr)

                Dim chkTaxItems As CheckBoxList = CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList)

                chkTaxItems.DataTextField = "vcTaxName"
                chkTaxItems.DataValueField = "numTaxItemID"
                chkTaxItems.DataSource = dtTaxTypes
                chkTaxItems.DataBind()
            End If

            dtSerItem.Columns.Add("numWarehouseItmsDTLID")
            dtSerItem.Columns.Add("numWItmsID")
            dtSerItem.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
            dtSerItem.Columns.Add("vcSerialNo")
            dtSerItem.Columns.Add("Comments")
            dtSerItem.Columns.Add("Attributes")

            dtChildItems.Columns.Add("numOppChildItemID", System.Type.GetType("System.Int32"))
            dtChildItems.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
            dtChildItems.Columns.Add("numItemCode")
            dtChildItems.Columns.Add("numQtyItemsReq")
            dtChildItems.Columns.Add("vcItemName")
            dtChildItems.Columns.Add("monListPrice")
            dtChildItems.Columns.Add("UnitPrice")
            dtChildItems.Columns.Add("charItemType")
            dtChildItems.Columns.Add("txtItemDesc")
            dtChildItems.Columns.Add("numWarehouseItmsID")
            dtChildItems.Columns.Add("Op_Flag")
            dtChildItems.Columns.Add("ItemType")
            dtChildItems.Columns.Add("Attr")
            dtChildItems.Columns.Add("vcWareHouse")

            dtItem.TableName = "Item"
            dtSerItem.TableName = "SerialNo"
            dtChildItems.TableName = "ChildItems"

            dsTemp.Tables.Add(dtItem)
            dsTemp.Tables.Add(dtSerItem)
            dsTemp.Tables.Add(dtChildItems)
            dtItem.PrimaryKey = New DataColumn() {dsTemp.Tables(0).Columns("numoppitemtCode")}
            dtChildItems.PrimaryKey = New DataColumn() {dsTemp.Tables(2).Columns("numOppChildItemID")}
            ViewState("SOItems") = dsTemp
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function CheckIfUnitPriceApprovalRequired() As Boolean
        Try
            Dim isApprovalRequired As Boolean = False

            Dim objOpportunity As New MOpportunity
            objOpportunity.DomainID = CCommon.ToLong(Session("DomainID"))

            If (_PageType = PageType.AddEditOrder) Then
                objOpportunity.CurrencyID = CCommon.ToLong(CType(Me.Parent.FindControl("hdnCurrencyID"), HiddenField).Value)
            Else
                objOpportunity.CurrencyID = CCommon.ToLong(CType(Me.Parent.FindControl("ddlCurrency"), DropDownList).SelectedValue)
            End If

            If (_PageType = PageType.Sales Or _PageType = PageType.Purchase Or _PageType = PageType.AddOppertunity) Then
                If IsStockTransfer Then
                    objOpportunity.DivisionID = IIf(CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue)
                Else
                    objOpportunity.DivisionID = IIf(CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue)
                End If
            Else
                objOpportunity.DivisionID = CCommon.ToLong(GetQueryStringVal("DivId"))
            End If
            'Checks whether unit price approval is required for any item added in order
            isApprovalRequired = objOpportunity.CheckIfUnitPriceApprovalRequired(dsTemp)
            hdnIsUnitPriceApprovalRequired.Value = isApprovalRequired

            UpdateDetails()
            Return isApprovalRequired
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        Try
            hdnApprovalActionTaken.Value = "Clear"

            If Not dsTemp Is Nothing AndAlso dsTemp.Tables.Count > 0 AndAlso dsTemp.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In dsTemp.Tables(0).Rows
                    dr("bitItemPriceApprovalRequired") = False
                Next

                UpdateDetails()
            End If

            dialogUnitPriceApproval.Visible = False
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Private Sub btnSaveAndProceed_Click(sender As Object, e As EventArgs) Handles btnSaveAndProceed.Click
        Try
            hdnApprovalActionTaken.Value = "Save&Proceed"

            Select Case hdnClickedButton.Value
                Case "btnSave"
                    btnSave_Click(Nothing, Nothing)
            End Select

            dialogUnitPriceApproval.Visible = False
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Private Sub btnApproveUnitPrice_Click(sender As Object, e As EventArgs) Handles btnApproveUnitPrice.Click
        Try
            hdnApprovalActionTaken.Value = "Approved"

            For Each dr As DataRow In dsTemp.Tables(0).Rows
                dr("bitItemPriceApprovalRequired") = False
            Next

            Select Case hdnClickedButton.Value
                Case "btnSave"
                    btnSave_Click(Nothing, Nothing)
            End Select

            dialogUnitPriceApproval.Visible = False
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            If _PageType = PageType.AddEditOrder AndAlso If((Me.Request("__EVENTTARGET") Is Nothing), String.Empty, Me.Request("__EVENTTARGET")) <> "DeleteAllItemsAndSave" AndAlso dsTemp.Tables(0).Rows.Count = 0 Then
                ScriptManager.RegisterStartupScript(upnl, upnl.GetType(), "Reload", "DeleteAllConfirm();", True)
                Exit Sub
            End If

            Dim sequences As List(Of String) = dgItems.Items.Cast(Of DataGridItem)().[Select](Function(r) TryCast(r.FindControl("txtRowOrder"), TextBox).Text.Trim()).ToList()
            'Try
            '    Dim userSequence As Integer() = sequences.[Select](Function(x) Integer.Parse(x)).OrderBy(Function(x) x).ToArray()
            '    Dim isInSequence As Boolean = userSequence.SequenceEqual(Enumerable.Range(1, userSequence.Count()))
            '    If isInSequence = False Then
            '        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ValidationItemDelete", "alert('Sequential row order is required, make corrections and try again.')", True)
            '        Exit Sub
            '    Else

            '    End If
            'Catch generatedExceptionName As Exception

            'End Try


            'Check tf minimum unit price approval rule is set for domin 
            If CCommon.ToBool(Session("IsMinUnitPriceRule")) AndAlso (_PageType = PageType.Sales Or _PageType = PageType.AddEditOrder Or _PageType = PageType.AddOppertunity) AndAlso OppType = 1 Then
                If Not hdnApprovalActionTaken.Value = "Approved" And Not hdnApprovalActionTaken.Value = "Save&Proceed" Then
                    If CheckIfUnitPriceApprovalRequired() Then
                        dialogUnitPriceApproval.Visible = True
                        hdnClickedButton.Value = "btnSave"
                        Exit Sub
                    End If
                End If
            End If

            save()

            ViewState("SOItems") = Nothing
            Session("CouponCode") = Nothing
            Session("TotalDiscount") = Nothing

            If (_PageType = PageType.Purchase) Then
                If CCommon.ToLong(GetQueryStringVal("StageID")) > 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Reload", "window.opener.location.href=window.opener.location.href;Close();self.close();", True)
                    Exit Sub
                ElseIf GetQueryStringVal("Source") <> "" Then
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Open", "OpenOpp(" & lngOppId.ToString & ")", True)
                    Exit Sub
                End If
                If boolFlag Then
                    If OppBizDocID > 0 Then
                        Response.Redirect("../opportunity/frmBizInvoice.aspx?OpID=" & lngOppId & "&OppBizId=" & OppBizDocID, False)
                    ElseIf CCommon.ToLong(GetQueryStringVal("StageID")) > 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Reload", "window.opener.location.href=window.opener.location.href;Close();self.close();", True)
                    Else
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Open", "window.opener.reDirectPage('../opportunity/frmOpportunities.aspx?opId=" & lngOppId.ToString() & "'); self.close();", True)
                    End If
                End If
            ElseIf (_PageType = PageType.Sales) Then
                Dim objRule As New OrderAutoRules
                objRule.GenerateAutoPO(OppBizDocID)
                ' GetWorkFlowAutomation()
                If boolFlag Then
                    'Dim Str As String
                    'Str = "<script language='javascript'>window.open('../opportunity/frmBizInvoice.aspx?OpID=" & lngOppId & "&OppBizId=" & OppBizDocID &
                    '  "','','toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes'); this.parent.close();</script>"


                    'Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "BizInvoice", Str, False)

                    Response.Redirect("../opportunity/frmBizInvoice.aspx?OpID=" & lngOppId & "&OppBizId=" & OppBizDocID, False)
                End If
            End If
        Catch ex As Exception
            If ex.Message = "FY_CLOSED" Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            ElseIf ex.Message.Contains("EDITED_ITEMS_QTY_CAN_NOT_BE_GREATER_THEN_COMPLETED_WORK_ORDER") Then
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Alert", "alert('Once work order is completed, You can\'t have qty more than completed work order qty.');", True)
            ElseIf ex.Message.Contains("CAN_NOT_CREATE_MULTIPLE_PO_FOR_ASSET_ITEMS") Then
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Alert", "alert('You can\'t have multiple purchase orders for asset item(s).');", True)
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End If
        End Try
    End Sub

    Private Sub btnAddToCart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddToCart.Click
        Try
            Session("CouponCode") = Nothing
            Session("TotalDiscount") = Nothing

            'Add customer or use existing
            If (_PageType = PageType.Purchase) Or (_PageType = PageType.Sales) Then
                If CType(Me.Parent.FindControl("trNewCust"), HtmlGenericControl).Visible Then
                    InsertProspect()
                Else
                    If IsStockTransfer Then
                        lngDivId = CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue
                        lngCntID = CType(Me.Parent.FindControl("ddlFromContact"), DropDownList).SelectedValue
                    Else
                        lngDivId = CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue
                        lngCntID = CType(Me.Parent.FindControl("ddlContact"), DropDownList).SelectedValue
                    End If
                End If
            Else
                If IsStockTransfer Then
                    lngDivId = CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue
                    lngCntID = CType(Me.Parent.FindControl("ddlFromContact"), DropDownList).SelectedValue
                Else
                    lngDivId = CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue
                    lngCntID = CType(Me.Parent.FindControl("ddlContact"), DropDownList).SelectedValue
                End If
            End If


            'Step1:Get items added to grid and insert it into cartitems table
            Dim objUserAccess As New UserAccess()
            objUserAccess.DomainID = Sites.ToLong(Session("DomainID"))
            Dim dtData As DataTable = objUserAccess.GetECommerceDetails()

            Dim lngSiteID As Long
            Dim lngWarehouseID As Long
            If dtData.Rows.Count = 1 Then
                lngSiteID = CCommon.ToString(dtData.Rows(0)("numSiteID"))
                lngWarehouseID = CCommon.ToString(dtData.Rows(0)("numDefaultWareHouseID"))
            End If

            If lngSiteID = 0 Then
                litMessage.Text = "Please set Default site from e-Commerce Settings to continue."
                Return
            End If
            If lngWarehouseID = 0 Then
                litMessage.Text = "Please set Default warehouse from e-Commerce Settings to continue."
                Return
            End If
            Dim ds As DataSet = Nothing
            Dim dtItemDetails As DataTable = Nothing
            Dim objItems As New CItems()
            Dim dr As DataRow = Nothing
            Dim objcart As BACRM.BusinessLogic.ShioppingCart.Cart
            For Each dgGridItem As DataGridItem In dgItems.Items
                Dim numItemCode As Long = CCommon.ToLong(dgItems.DataKeys(dgGridItem.ItemIndex))
                objItems.ItemCode = Sites.ToInteger(numItemCode.ToString())
                objItems.WarehouseID = lngWarehouseID
                objItems.DomainID = Sites.ToLong(Session("DomainID"))
                objItems.SiteID = lngSiteID
                objItems.DivisionID = lngDivId
                'ds = objItems.ItemDetailsForEcomm()
                'dtItemDetails = ds.Tables(0)

                ''' REMOVE COMMENT FROM THIS BLOCK & COMMENT ABOVE 2 LINES FOR GETTING DISCOUNT INTO ITEMS & CHANGE CODE IN Cart.ascx.cs at line # 711
                If ViewState("SOItems") Is Nothing Then
                    ds = objItems.ItemDetailsForEcomm()
                Else
                    ds = ViewState("SOItems")
                    If Not ds.Tables(0).Columns.Contains("txtItemDesc") Then ds.Tables(0).Columns.Add("txtItemDesc")
                    If Not ds.Tables(0).Columns.Contains("numWareHouseItemID") Then ds.Tables(0).Columns.Add("numWareHouseItemID")
                    If Not ds.Tables(0).Columns.Contains("bitFreeShipping") Then ds.Tables(0).Columns.Add("bitFreeShipping")
                    If Not ds.Tables(0).Columns.Contains("vcCategoryName") Then ds.Tables(0).Columns.Add("vcCategoryName")
                    If Not ds.Tables(0).Columns.Contains("fltWeight") Then ds.Tables(0).Columns.Add("fltWeight")
                    If Not ds.Tables(0).Columns.Contains("bitDiscountType") Then ds.Tables(0).Columns.Add("bitDiscountType")
                    If Not ds.Tables(0).Columns.Contains("fltDiscount") Then ds.Tables(0).Columns.Add("fltDiscount")

                    Dim dsItemDetailsForEcomm As New DataSet
                    dsItemDetailsForEcomm = objItems.ItemDetailsForEcomm()
                    Dim drValue As DataRow = dsItemDetailsForEcomm.Tables(0).Select("numItemCode = " & numItemCode)(0)

                    Dim drItem As DataRow = ds.Tables(0).Select("numItemCode = " & numItemCode)(0)
                    drItem("txtItemDesc") = CCommon.ToString(drValue("txtItemDesc"))
                    drItem("numWareHouseItemID") = CCommon.ToLong(drValue("numWareHouseItemID"))
                    drItem("bitFreeShipping") = CCommon.ToBool(drValue("bitFreeShipping"))
                    drItem("vcCategoryName") = CCommon.ToString(drValue("vcCategoryName"))
                    drItem("fltWeight") = CCommon.ToDouble(drValue("fltWeight"))
                    drItem("bitDiscountType") = CCommon.ToBool(CType(dgGridItem.FindControl("txtDiscountType"), TextBox).Text)
                    drItem("fltDiscount") = CCommon.ToDouble(CType(dgGridItem.FindControl("txttotalDiscount"), TextBox).Text)

                    drItem.AcceptChanges()
                    ds.Tables(0).AcceptChanges()

                End If

                dtItemDetails = ds.Tables(0)

                If dtItemDetails.Rows.Count > 0 Then
                    dr = dtItemDetails.Rows(0)

                    objcart = New BACRM.BusinessLogic.ShioppingCart.Cart()
                    objcart.ContactId = lngCntID
                    objcart.DomainID = CCommon.ToLong(Session("DomainID"))
                    objcart.CookieId = ""
                    objcart.OppItemCode = dgGridItem.ItemIndex 'CCommon.ToLong(dr("numOppItemCode"))
                    objcart.ItemCode = numItemCode
                    objcart.UnitHour = Math.Abs(CCommon.ToDecimal(CType(dgGridItem.FindControl("txtUnits"), TextBox).Text))
                    objcart.Price = CCommon.ToDecimal(CType(dgGridItem.FindControl("hdnUnitPrice"), HiddenField).Value)
                    objcart.SourceId = 0
                    objcart.ItemDesc = CCommon.ToString(dr("txtItemDesc"))
                    objcart.WarehouseId = lngWarehouseID 'CCommon.ToLong(dr("numWarehouseID"))
                    objcart.ItemName = CCommon.ToString(dr("vcItemName"))
                    objcart.Warehouse = ""
                    objcart.WarehouseItemsId = CCommon.ToLong(dr("numWareHouseItemID"))
                    If CCommon.ToString(dr("charItemType")) = "P" Then
                        objcart.ItemType = "Inventory Item"
                    ElseIf CCommon.ToString(dr("charItemType")) = "S" Then
                        objcart.ItemType = "Service Item"
                    ElseIf CCommon.ToString(dr("charItemType")) = "N" Then
                        objcart.ItemType = "Non-Inventory Item"
                    ElseIf CCommon.ToString(dr("charItemType")) = "A" Then
                        objcart.ItemType = "Accessory"
                    End If

                    objcart.Attributes = CCommon.ToString(dr("Attributes"))
                    'objcart.AttributesValue = CCommon.ToString(dr("vcAttrValues"))' no need for storing value,since its not going to utlized anywhere.
                    objcart.FreeShipping = CCommon.ToBool(dr("bitFreeShipping"))
                    objcart.OPFlag = 1 'CShort(CCommon.ToInteger(dr("tintOpFlag")))
                    objcart.DiscountType = CCommon.ToBool(dr("bitDiscountType")) ''' REMOVE COMMENT TO ADD DiscountType
                    objcart.Discount = CCommon.ToDouble(dr("fltDiscount")) ''' REMOVE COMMENT TO ADD Discount
                    objcart.TotAmtBeforeDisc = objcart.UnitHour * objcart.Price 'CCommon.ToDecimal(dr("monTotAmtBefDiscount"))
                    objcart.ItemURL = GetProductURL(CCommon.ToString(dr("vcCategoryName")), objcart.ItemName, numItemCode.ToString)
                    objcart.UOM = CCommon.ToLong(dr("numUOM"))
                    objcart.UOMName = If(CCommon.ToString(dr("vcUOMName")) = "", "Units", Sites.ToString(dr("vcUOMName"))) 'CCommon.ToString(dr("vcUOMName"))
                    objcart.UOMConversionFactor = CCommon.ToDecimal(dr("UOMConversionFactor"))
                    objcart.Weight = CCommon.ToLong(dr("fltWeight"))
                    objcart.Height = CCommon.ToLong(dr("fltHeight"))
                    objcart.Length = CCommon.ToLong(dr("fltLength"))
                    objcart.Width = CCommon.ToLong(dr("fltWidth"))
                    objcart.ShippingMethod = ""
                    objcart.ServiceTypeId = 0
                    objcart.ShippingCompany = 0
                    objcart.ServiceType = 0
                    objcart.DeliveryDate = DateTime.Now.[Date]
                    objcart.TotAmount = objcart.UnitHour * objcart.Price
                    objcart.SiteID = -1
                    objcart.InsertCartItems()
                End If
            Next

            'Step2:Create Extranet account for selected contact if not exist
            Dim objUserGroups As New UserGroups
            objUserGroups.ContactID = lngCntID
            objUserGroups.DomainID = Session("DomainID")
            If objCommon Is Nothing Then
                objCommon = New CCommon
            End If
            objUserGroups.Password = objCommon.Encrypt(PasswordGenerator.Generate(10, 10))
            objUserGroups.AccessAllowed = 1
            Dim lngExtranetID As Long = objUserGroups.CreateExtranetAccount()

            objUserGroups.ExtranetID = lngExtranetID
            dsTemp = objUserGroups.GetExtranetDetails()
            dtData = dsTemp.Tables(1)
            If dtData.Rows.Count > 0 Then
                'Add SMTPServerIntegration = true because it checks while sending Email .
                'System.Web.HttpContext.Current.Session("SMTPServerIntegration") = True
                Dim dr1() As DataRow = dtData.Select("numContactID = '" + lngCntID.ToString + "'")
                If dr1.Length > 0 Then
                    objUserGroups.Password = CCommon.ToString(dr1(0)("Password"))
                End If
                'Step3:Send email template with username and password to  complete shopping
                Dim objSendEmail As New Email
                objSendEmail.DomainID = Sites.ToLong(Session("DomainID"))
                objSendEmail.TemplateCode = "#SYS#ECOMMERCE_SIGNUP"
                objSendEmail.ModuleID = 1
                objSendEmail.RecordIds = Sites.ToString(lngCntID)
                objSendEmail.AdditionalMergeFields.Add("PortalPassword", objUserGroups.Password)
                objSendEmail.FromEmail = Session("UserEmail")
                objSendEmail.ToEmail = "##ContactFirstName## <##ContactEmail##>"
                objSendEmail.SendEmailTemplate()
            Else
                litMessage.Text = "unable to create external access to selected customer, please contact administrator."
                Return
            End If



            If boolFlag Then
                Response.Redirect("../opportunity/frmNewOrder.aspx?I=1", False)
            End If

        Catch ex As Exception
            If ex.Message = "FY_CLOSED" Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            ElseIf ex.Message.Contains("CAN_NOT_CREATE_MULTIPLE_PO_FOR_ASSET_ITEMS") Then
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Alert", "alert('You can\'t have multiple purchase orders for asset item(s).');", True)
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End If
        End Try
    End Sub

    Sub save()
        Try
            If (_PageType = PageType.AddEditOrder) Then
                'If (True) Then
                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")

                objCommon.Mode = 27
                objCommon.Str = CCommon.ToLong(GetQueryStringVal("opid"))
                If objCommon.GetSingleFieldValue() = 1 Then
                    boolFlag = False
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "ValidationClosedOrder", "alert('You are not allowed to edit closed order.')", True)
                    Exit Sub
                End If

                Dim dtItem As DataTable

                dtItem = dsTemp.Tables(0)
                If dtItem.Columns.Contains("numContainer") Then
                    dtItem.Columns.Remove("numContainer")
                End If
                If dtItem.Columns.Contains("numContainerQty") Then
                    dtItem.Columns.Remove("numContainerQty")
                End If
                If dtItem.Rows.Count > 0 Then
                    For Each dr As DataRow In dtItem.Rows
                        Dim decUnits As Decimal = Math.Abs(CCommon.ToDecimal(dr("numUnitHour"))) * Math.Abs(CCommon.ToDecimal(dr("UOMConversionFactor")))
                        Dim decUnitHourReceived As Decimal = Math.Abs(CCommon.ToDecimal(dr("numUnitHourReceived")))
                        Dim decQtyShipped As Decimal = Math.Abs(CCommon.ToDecimal(dr("numQtyShipped")))

                        If OppType = 1 Then
                            If decQtyShipped > decUnits Then
                                boolFlag = False
                                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "ValidationItemDelete", "alert('You are not allowed to edit this item,since item has shipped qty more than order qty.')", True)
                                Exit Sub
                            End If
                        ElseIf OppType = 2 Then
                            If decUnitHourReceived > decUnits Then
                                boolFlag = False
                                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "ValidationItemDelete", "alert('You are not allowed to edit this item,since item has received qty more than order qty.')", True)
                                Exit Sub
                            End If
                        End If
                    Next
                End If

                SaveOpportunity()
                lngOppId = CCommon.ToLong(GetQueryStringVal("opid"))
            Else
                If (_PageType = PageType.Purchase) Then
                    If GetQueryStringVal("Source") <> "" AndAlso GetQueryStringVal("StageID") <> "" Then

                        Dim dtItem As DataTable

                        dtItem = dsTemp.Tables(0)

                        If dtItem.Rows.Count > 0 Then
                            Dim TotalAmount As Decimal
                            TotalAmount = CType(dsTemp.Tables(0).Compute("Sum(monTotAmount)", ""), Decimal)

                            Dim objTimeExp As New TimeExpenseLeave
                            Dim dtTimeDetails As DataTable

                            objTimeExp.DomainID = Session("DomainID")
                            objTimeExp.ProID = CCommon.ToLong(GetQueryStringVal("Source"))
                            objTimeExp.StageId = CCommon.ToLong(GetQueryStringVal("StageID"))

                            dtTimeDetails = objTimeExp.GetTimeAndExpense_BudgetTotal()

                            If dtTimeDetails.Rows.Count <> 0 Then
                                If dtTimeDetails.Rows(0).Item("bitExpenseBudget") = True Then
                                    If (TotalAmount > dtTimeDetails.Rows(0).Item("TotalExpense")) Then
                                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Alert", "alert('You’ve exceeded the amount budgeted for this stage.' );", True)
                                        boolFlag = False
                                        Exit Sub
                                    End If
                                    'Else
                                    '    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please set Expense Budget for this stage.' );", True)
                                    '    boolFlag = False
                                    '    Exit Sub
                                End If
                            End If
                        End If
                    End If
                End If

                If OppType = 2 Then
                    If _PageType = PageType.Purchase Then
                        'Accounting validation->Do not allow to save PO untill Default COGs account is mapped 
                        Dim objJournal As New JournalEntry
                        If ChartOfAccounting.GetDefaultAccount("CG", Session("DomainID")) = 0 Then
                            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Alert", "alert('Please Set Default COGs account from Administration->Global Settings->Accounting->Default Accounts Mapping To Save' );", True)
                            boolFlag = False
                            Exit Sub
                        End If
                    End If

                    If ChartOfAccounting.GetDefaultAccount("PC", Session("DomainID")) = 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Alert", "alert('Please Set Default Purchase Clearing account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                        boolFlag = False
                        Exit Sub
                    End If

                    If ChartOfAccounting.GetDefaultAccount("PV", Session("DomainID")) = 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Alert", "alert('Please Set Default Purchase Price Variance account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                        boolFlag = False
                        Exit Sub
                    End If
                End If

                If (_PageType = PageType.Purchase) Or (_PageType = PageType.Sales) Then
                    If CType(Me.Parent.FindControl("trNewCust"), HtmlGenericControl).Visible Then
                        InsertProspect()
                    Else
                        If IsStockTransfer Then
                            lngDivId = CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue
                            lngCntID = CType(Me.Parent.FindControl("ddlFromContact"), DropDownList).SelectedValue
                        Else
                            lngDivId = CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue
                            lngCntID = CType(Me.Parent.FindControl("ddlContact"), DropDownList).SelectedValue
                        End If
                    End If
                Else
                    If IsStockTransfer Then
                        lngDivId = CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue
                        lngCntID = CType(Me.Parent.FindControl("ddlFromContact"), DropDownList).SelectedValue
                    Else
                        lngDivId = CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue
                        lngCntID = CType(Me.Parent.FindControl("ddlContact"), DropDownList).SelectedValue
                    End If
                End If

                Opportunity()


                'Create Purchase Order for DropShip Item against Primary Vendor

                AddToRecentlyViewed(RecetlyViewdRecordType.Opportunity, lngOppId)
            End If

            ''Added By Sachin Sadhu||Date:29thApril12014
            ''Purpose :To Added Opportunity data in work Flow queue based on created Rules
            ''          Using Change tracking
            Dim objWfA As New Workflow()
            objWfA.DomainID = Session("DomainID")
            objWfA.UserCntID = Session("UserContactID")
            objWfA.RecordID = lngOppId
            objWfA.SaveWFOrderQueue()
            'end of code
            If _PageType = PageType.AddEditOrder Then
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Reload", "window.opener.location.href=window.opener.location.href;self.close();", True)
            End If

        Catch ex As Exception
            boolFlag = False

            If ex.Message.Contains("USAGE_OF_COUPON_EXCEED") Then
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Alert", "alert('Usage of coupon(s) exceed.' );", True)
            ElseIf ex.Message.Contains("SERIAL/LOT#_ASSIGNED_TO_ITEM_DELETED") Then
                ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "Alert", "alert('You are not allowed to delete item(s), since serial/lot# assigned to item!.' );", True)
            ElseIf ex.Message.Contains("ITEM_USED_IN_BIZDOC") Then
                ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "Alert", "alert('Deleted item(s) are used in bizdoc(s).');", True)
            ElseIf ex.Message.Contains("ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY") Then
                ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "Alert", "alert('Ordered qty can no be less than packing slip quantity when customer default settings is ""Allocate inventory from Packing Slip"".');", True)
            ElseIf ex.Message.Contains("CAN_NOT_CHANGE_ASSEMBLY_WAREHOUSE_WORK_ORDER_EXISTS") Then
                ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "Alert", "alert('You can't change warehouse of assembly item(s) once Work order are created. You have to add item again after deleting work order(s) and existing item.');", True)
            Else
                Throw
            End If
        End Try
    End Sub

    Sub SaveOpportunity()
        Try
            Dim arrOutPut() As String
            Dim objOpportunity As New OppotunitiesIP
            objOpportunity.OpportunityId = CCommon.ToLong(GetQueryStringVal("opid"))
            objOpportunity.DomainID = Session("DomainID")
            objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            objOpportunity.OpportunityDetails()

            objOpportunity.OpportunityId = CCommon.ToLong(GetQueryStringVal("opid"))
            objOpportunity.PublicFlag = 0
            objOpportunity.UserCntID = Session("UserContactID")
            objOpportunity.DomainID = Session("DomainId")
            objOpportunity.OppType = OppType
            'objOpportunity.SourceType = 1
            objOpportunity.CouponCode = ""
            objOpportunity.Discount = 0
            objOpportunity.boolDiscType = False

            If CCommon.ToLong(hdnShipVia.Value) > 0 Then
                objOpportunity.intUsedShippingCompany = CCommon.ToLong(hdnShipVia.Value)
            ElseIf Not Me.Parent.FindControl("rdcmbShipVia") Is Nothing Then
                objOpportunity.intUsedShippingCompany = CCommon.ToLong(CType(Me.Parent.FindControl("rdcmbShipVia"), RadComboBox).SelectedValue)
                objOpportunity.ShipmentMethod = CCommon.ToLong(CType(Me.Parent.FindControl("rdcmbShipVia"), RadComboBox).SelectedValue)
            End If

            If CCommon.ToLong(hdnShippingService.Value) > 0 Then
                objOpportunity.ShippingService = CCommon.ToLong(hdnShippingService.Value)
            ElseIf Not Me.Parent.FindControl("rdcmbShippingService") Is Nothing Then
                objOpportunity.ShippingService = CCommon.ToLong(CType(Me.Parent.FindControl("rdcmbShippingService"), RadComboBox).SelectedValue)
            End If

            objOpportunity.Discount = 0
            objOpportunity.boolDiscType = False

            dsTemp.Tables(2).Rows.Clear()
            dsTemp.AcceptChanges()

            If Not ViewState("SOItems") Is Nothing Then
                dsTemp = ViewState("SOItems")
                objOpportunity.strItems = "<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & dsTemp.GetXml
            End If
            If Convert.ToBoolean(objOpportunity.bitIsInitalSalesOrder) = True Then
                If hdnApprovalActionTaken.Value = "Save&Proceed" And Convert.ToBoolean(Session("bitMarginPriceViolated")) = True Then
                    objOpportunity.DealStatus = 0
                End If
                If hdnApprovalActionTaken.Value = "Approved" Then
                    objOpportunity.DealStatus = 1
                End If
            End If
            objOpportunity.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            arrOutPut = objOpportunity.Save()
            objOpportunity.OpportunityId = arrOutPut(0)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Public Function GetProductURL(CategoryName As String, ProductName As String, ItemCode As String) As String
        Try
            Dim strURL As String = Sites.ToString(Session("SitePath")) + "/Product/" + Sites.GenerateSEOFriendlyURL(CategoryName) + "/" + Sites.GenerateSEOFriendlyURL(ProductName) + "/" + ItemCode
            Return strURL
        Catch ex As Exception
            Throw ex
        End Try
    End Function



    Private Sub btnSaveNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNew.Click
        Try
            IsFromSaveAndNew = True
            'If ProcessPayment() Then
            save()
            Dim objRule As New OrderAutoRules
            objRule.GenerateAutoPO(OppBizDocID)

            ViewState("SOItems") = Nothing
            Session("CouponCode") = Nothing
            Session("TotalDiscount") = Nothing

            If boolFlag Then
                If IsPOS = 1 Then
                    Response.Redirect("../opportunity/frmNewOrder.aspx?I=1&IsPOS=1", False)
                Else
                    Response.Redirect("../opportunity/frmNewOrder.aspx?I=1", False)
                End If
            End If
            'Else
            'Exit Sub
            'End If
        Catch ex As Exception
            If ex.Message = "FY_CLOSED" Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            ElseIf ex.Message.Contains("CAN_NOT_CREATE_MULTIPLE_PO_FOR_ASSET_ITEMS") Then
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Alert", "alert('You can\'t have multiple purchase orders for asset item(s).');", True)
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End If
        End Try
    End Sub

    Private Function InsertProspect() As Boolean
        Try

            Dim objLeads As New CLeads
            With objLeads
                Dim txtCompany As TextBox = CType(Me.Parent.FindControl("txtCompany"), TextBox)
                Dim txtFirstName As TextBox = CType(Me.Parent.FindControl("txtFirstName"), TextBox)
                Dim txtLastName As TextBox = CType(Me.Parent.FindControl("txtLastName"), TextBox)

                Dim txtPhone As TextBox = CType(Me.Parent.FindControl("txtPhone"), TextBox)
                Dim txtExt As TextBox = CType(Me.Parent.FindControl("txtExt"), TextBox)
                Dim txtEmail As TextBox = CType(Me.Parent.FindControl("txtEmail"), TextBox)

                If Len(txtCompany.Text) < 1 Then txtCompany.Text = txtLastName.Text & "," & txtFirstName.Text
                .CompanyName = txtCompany.Text.Trim
                CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).Text = txtCompany.Text
                .CustName = txtCompany.Text.Trim
                .CRMType = 1
                .DivisionName = "-"
                .LeadBoxFlg = 1
                .UserCntID = Session("UserContactId")
                .Country = Session("DefCountry")
                .SCountry = Session("DefCountry")
                .FirstName = txtFirstName.Text
                .LastName = txtLastName.Text
                .ContactPhone = txtPhone.Text
                .PhoneExt = txtExt.Text
                .Email = txtEmail.Text
                .DomainID = Session("DomainID")
                .ContactType = 0 'commented by chintan, contact type 70 is obsolete. default contact type is 0
                .PrimaryContact = True

                .UpdateDefaultTax = False
                If CType(Me.Parent.FindControl("ddlRelationship"), DropDownList).SelectedIndex > 0 Then .CompanyType = CType(Me.Parent.FindControl("ddlRelationship"), DropDownList).SelectedValue
                If CType(Me.Parent.FindControl("ddlProfile"), DropDownList).SelectedIndex > 0 Then .Profile = CType(Me.Parent.FindControl("ddlProfile"), DropDownList).SelectedValue

                If (_PageType = PageType.Sales) Then
                    .NoTax = IIf(CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList).Items.FindByValue(0).Selected = True, False, True)
                End If
            End With
            objLeads.CompanyID = objLeads.CreateRecordCompanyInfo
            lngDivId = objLeads.CreateRecordDivisionsInfo
            objLeads.DivisionID = lngDivId
            lngCntID = objLeads.CreateRecordAddContactInfo()
            'Added By Sachin Sadhu||Date:22ndMay12014
            'Purpose :To Added Organization data in work Flow queue based on created Rules
            '          Using Change tracking
            Dim objWfA As New Workflow()
            objWfA.DomainID = Session("DomainID")
            objWfA.UserCntID = Session("UserContactID")
            objWfA.RecordID = lngDivId
            objWfA.SaveWFOrganizationQueue()
            'end of code

            'Added By Sachin Sadhu||Date:24thJuly2014
            'Purpose :To Added Contact data in work Flow queue based on created Rules
            '         Using Change tracking
            Dim objWF As New Workflow()
            objWF.DomainID = Session("DomainID")
            objWF.UserCntID = Session("UserContactID")
            objWF.RecordID = lngCntID
            objWF.SaveWFContactQueue()
            ' ss//end of code


            If (_PageType = PageType.Sales) Then
                SaveTaxTypes()
            End If

            Dim txtStreet As TextBox = CType(Me.Parent.FindControl("txtBillStreet"), TextBox)
            Dim txtCity As TextBox = CType(Me.Parent.FindControl("txtBillCity"), TextBox)
            Dim txtPostal As TextBox = CType(Me.Parent.FindControl("txtBillPostal"), TextBox)
            Dim txtShipStreet As TextBox = CType(Me.Parent.FindControl("txtShipStreet"), TextBox)
            Dim txtShipPostal As TextBox = CType(Me.Parent.FindControl("txtShipPostal"), TextBox)
            Dim txtShipCity As TextBox = CType(Me.Parent.FindControl("txtShipCity"), TextBox)

            Dim ddlBillCountry As DropDownList = CType(Me.Parent.FindControl("ddlBillCountry"), DropDownList)
            Dim ddlBillState As DropDownList = CType(Me.Parent.FindControl("ddlBillState"), DropDownList)
            Dim ddlShipState As DropDownList = CType(Me.Parent.FindControl("ddlShipState"), DropDownList)
            Dim ddlShipCountry As DropDownList = CType(Me.Parent.FindControl("ddlShipCountry"), DropDownList)

            Dim chkShipDifferent As CheckBox = CType(Me.Parent.FindControl("chkShipDifferent"), CheckBox)

            Dim objContacts As New CContacts
            objContacts.BillStreet = txtStreet.Text
            objContacts.BillCity = txtCity.Text
            objContacts.BillCountry = ddlBillCountry.SelectedItem.Value
            objContacts.BillPostal = txtPostal.Text
            objContacts.BillState = ddlBillState.SelectedItem.Value
            objContacts.BillingAddress = IIf(chkShipDifferent.Checked = True, 0, 1)
            objContacts.ShipStreet = IIf(chkShipDifferent.Checked = True, txtShipStreet.Text, txtStreet.Text)
            objContacts.ShipState = IIf(chkShipDifferent.Checked = True, ddlShipState.SelectedItem.Value, ddlBillState.SelectedItem.Value)
            objContacts.ShipPostal = IIf(chkShipDifferent.Checked = True, txtShipPostal.Text, txtPostal.Text)
            objContacts.ShipCountry = IIf(chkShipDifferent.Checked = True, ddlShipCountry.SelectedItem.Value, ddlBillCountry.SelectedItem.Value)
            objContacts.ShipCity = IIf(chkShipDifferent.Checked = True, txtShipCity.Text, txtCity.Text)
            objContacts.DivisionID = lngDivId
            objContacts.UpdateCompanyAddress()
        Catch ex As Exception
            boolFlag = False
            Throw ex
        End Try
    End Function

    Sub SaveTaxTypes()
        Try
            Dim dtCompanyTaxTypes As New DataTable
            dtCompanyTaxTypes.Columns.Add("numTaxItemID")
            dtCompanyTaxTypes.Columns.Add("bitApplicable")
            Dim dr As DataRow
            Dim i As Integer
            For i = 0 To CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList).Items.Count - 1
                If CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList).Items(i).Selected = True Then
                    dr = dtCompanyTaxTypes.NewRow
                    dr("numTaxItemID") = CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList).Items(i).Value
                    dr("bitApplicable") = 1
                    dtCompanyTaxTypes.Rows.Add(dr)
                End If
            Next
            Dim ds As New DataSet
            Dim strdetails As String
            dtCompanyTaxTypes.TableName = "Table"
            ds.Tables.Add(dtCompanyTaxTypes)
            strdetails = ds.GetXml
            ds.Tables.Remove(ds.Tables(0))
            Dim objProspects As New CProspects
            objProspects.DivisionID = lngDivId
            objProspects.strCompanyTaxTypes = strdetails
            objProspects.ManageCompanyTaxTypes()
            ds.Dispose()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub Opportunity()
        Try
            Dim arrOutPut() As String
            Dim objOpportunity As New MOpportunity
            objOpportunity.OpportunityId = 0
            objOpportunity.ContactID = lngCntID
            objOpportunity.DivisionID = lngDivId
            If Not Me.Parent.FindControl("txtComments") Is Nothing Then
                objOpportunity.OppComments = DirectCast(Me.Parent.FindControl("txtComments"), TextBox).Text.Trim()
            End If

            If (_PageType = PageType.Purchase) Then
                If IsStockTransfer Then
                    objOpportunity.OpportunityName = CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).Text & "-PO-" & Format(Now(), "MMMM")
                Else
                    objOpportunity.OpportunityName = CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).Text & "-PO-" & Format(Now(), "MMMM")
                End If

                objOpportunity.IsStockTransfer = _IsStockTransfer
            ElseIf (_PageType = PageType.Sales) Then
                objOpportunity.OpportunityName = CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).Text & "-SO-" & Format(Now(), "MMMM")
            ElseIf (_PageType = PageType.AddOppertunity) Then
                objOpportunity.OpportunityName = CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).Text & "-" & Format(Now(), "MMMM")
            End If

            objOpportunity.UserCntID = Session("UserContactID")
            objOpportunity.EstimatedCloseDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
            objOpportunity.PublicFlag = 0
            objOpportunity.DomainID = Session("DomainId")

            If Not CType(Me.Parent.FindControl("ddlClass"), DropDownList) Is Nothing Then
                objOpportunity.ClassID = CCommon.ToLong(CType(Me.Parent.FindControl("ddlClass"), DropDownList).SelectedValue)
            End If

            'when address value is >0 then it will Copy addressid's address to Opportunity address
            objOpportunity.BillToAddressID = CCommon.ToLong(hdnBillAddressID.Value)
            objOpportunity.ShipToAddressID = CCommon.ToLong(hdnShipAddressID.Value)

            objOpportunity.CouponCode = "" 'CCommon.ToString(Session("CouponCode"))
            'If CCommon.ToDouble(Session("TotalDiscount")) > 0 Then
            '    objOpportunity.Discount = CCommon.ToDouble(Session("TotalDiscount"))
            '    objOpportunity.boolDiscType = True
            'Else
            objOpportunity.Discount = 0
            objOpportunity.boolDiscType = False
            'End If

            If Session("MultiCurrency") = True Then
                objOpportunity.CurrencyID = CType(Me.Parent.FindControl("ddlCurrency"), DropDownList).SelectedValue
            Else
                objOpportunity.CurrencyID = Session("BaseCurrencyID")
            End If

            If (_PageType = PageType.Purchase) Or (_PageType = PageType.Sales) Then
                If CType(Me.Parent.FindControl("trNewCust"), HtmlGenericControl).Visible Then
                    If CType(Me.Parent.FindControl("ddlNewAssignTo"), DropDownList).SelectedIndex >= 0 Then objOpportunity.AssignedTo = CType(Me.Parent.FindControl("ddlNewAssignTo"), DropDownList).SelectedValue
                Else
                    If CType(Me.Parent.FindControl("ddlAssignTo"), DropDownList).SelectedIndex >= 0 Then objOpportunity.AssignedTo = CType(Me.Parent.FindControl("ddlAssignTo"), DropDownList).SelectedValue
                    'If ddlCampaign.SelectedIndex > 0 Then objOpportunity.CampaignID = ddlCampaign.SelectedValue
                End If
            End If
            If (_PageType = PageType.AddOppertunity) Then
                If Not Me.Parent.FindControl("ddlAssignTo") Is Nothing Then
                    If CType(Me.Parent.FindControl("ddlAssignTo"), DropDownList).SelectedIndex >= 0 Then objOpportunity.AssignedTo = CType(Me.Parent.FindControl("ddlAssignTo"), DropDownList).SelectedValue

                End If
            End If
            objOpportunity.OppType = OppType

            If (_PageType = PageType.Purchase Or _PageType = PageType.Sales) Then
                objOpportunity.DealStatus = 1
                objOpportunity.SourceType = 1

                If hdnApprovalActionTaken.Value = "Save&Proceed" And Convert.ToBoolean(Session("bitMarginPriceViolated")) = True Then
                    objOpportunity.DealStatus = 0
                End If
            ElseIf (_PageType = PageType.AddOppertunity) Then
                objOpportunity.Source = CType(Me.Parent.FindControl("ddlSource"), DropDownList).SelectedItem.Value
                objOpportunity.SourceType = 1
            End If

            dsTemp.Tables(2).Rows.Clear()
            dsTemp.AcceptChanges()
            Dim TotalAmount As Decimal = 0
            If Not ViewState("SOItems") Is Nothing Then
                dsTemp = ViewState("SOItems")
                If dsTemp.Tables(0).Rows.Count > 0 Then
                    TotalAmount = CType(dsTemp.Tables(0).Compute("Sum(monTotAmount)", ""), Decimal)
                    objOpportunity.strItems = "<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & dsTemp.GetXml
                End If
            End If

            Dim objAdmin As New CAdmin
            Dim dtBillingTerms As DataTable
            objAdmin.DivisionID = lngDivId
            dtBillingTerms = objAdmin.GetBillingTerms()

            objOpportunity.boolBillingTerms = IIf(dtBillingTerms.Rows(0).Item("tintBillingTerms") = 1, True, False)
            objOpportunity.BillingDays = dtBillingTerms.Rows(0).Item("numBillingDays")
            objOpportunity.boolInterestType = IIf(dtBillingTerms.Rows(0).Item("tintInterestType") = 1, True, False)
            objOpportunity.Interest = dtBillingTerms.Rows(0).Item("fltInterest")

            If CCommon.ToLong(hdnShipVia.Value) > 0 Then
                objOpportunity.intUsedShippingCompany = CCommon.ToLong(hdnShipVia.Value)
            ElseIf Not Me.Parent.FindControl("rdcmbShipVia") Is Nothing Then
                objOpportunity.intUsedShippingCompany = CCommon.ToLong(CType(Me.Parent.FindControl("rdcmbShipVia"), RadComboBox).SelectedValue)
                objOpportunity.ShipmentMethod = CCommon.ToLong(CType(Me.Parent.FindControl("rdcmbShipVia"), RadComboBox).SelectedValue)
            End If

            If CCommon.ToLong(hdnShippingService.Value) > 0 Then
                objOpportunity.ShippingService = CCommon.ToLong(hdnShippingService.Value)
            ElseIf Not Me.Parent.FindControl("rdcmbShippingService") Is Nothing Then
                objOpportunity.ShippingService = CCommon.ToLong(CType(Me.Parent.FindControl("rdcmbShippingService"), RadComboBox).SelectedValue)
            End If

            If Not CType(Me.Parent.FindControl("ddlVendorAddresses"), DropDownList) Is Nothing Then
                If CType(Me.Parent.FindControl("ddlVendorAddresses"), DropDownList).SelectedValue = "0" Then
                    objOpportunity.VendorAddressID = 0
                Else
                    objOpportunity.VendorAddressID = CCommon.ToLong(CType(Me.Parent.FindControl("ddlVendorAddresses"), DropDownList).SelectedValue.Split("~")(0))
                End If
            End If

            Dim objOpp As New MOpportunity
            objOpp.DivisionID = lngDivId
            objOpp.DomainID = objOpportunity.DomainID
            objOpportunity.CampaignID = objOpp.GetCampaignForOrganization()
            objOpportunity.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            arrOutPut = objOpportunity.Save
            lngOppId = arrOutPut(0)


            '''AddingBizDocs
            Dim objOppBizDocs As New OppBizDocs

            If (_PageType = PageType.Purchase) Then

                'Set Bill To and Ship To based on domain details settings
                If objOpportunity.BillToAddressID = 0 And objOpportunity.ShipToAddressID = 0 Then
                    objOpportunity.OpportunityId = lngOppId
                    objOpportunity.BillToType = IIf(Session("DefaultBillToForPO") = 1, 0, 0)
                    objOpportunity.ShipToType = IIf(Session("DefaultShipToForPO") = 1, 0, 0)
                    objOpportunity.UpdateOpportunityLinkingDtls()
                End If

                'IMPORTANT - CREATE BIZDOC METHOD IS COMMENTED BECAUSE USER CAN NOW USE WORKFLOW WORKLFLOW AUTOMATION AUTOMATION RULES
                'CreateOppBizDoc(objOppBizDocs, False)

                'Link PO with Project if Source Exist
                'If objOpportunity.Source > 0 Then
                If GetQueryStringVal("Source") <> "" AndAlso GetQueryStringVal("StageID") <> "" Then
                    objOpportunity.OpportunityId = lngOppId
                    objOpportunity.ProjectID = CCommon.ToLong(GetQueryStringVal("Source"))
                    objOpportunity.ManageOppLinking()
                    'Link BizDoc with Project
                    Dim objProject As New Project
                    objProject.ProjectID = CCommon.ToLong(GetQueryStringVal("Source"))
                    objProject.OpportunityId = lngOppId
                    'objProject.OppBizDocID = OppBizDocID
                    objProject.DomainID = Session("DomainID")
                    objProject.StageID = CCommon.ToLong(GetQueryStringVal("StageID"))
                    objProject.SaveProjectOpportunities()
                End If

                'stock transfer,update order name 
                If _IsStockTransfer Then
                    If objCommon Is Nothing Then objCommon = New CCommon()
                    objCommon.Mode = 19
                    objCommon.UpdateRecordID = lngOppId
                    objCommon.Comments = "Stock Transfer-" & lngOppId.ToString()
                    objCommon.UpdateSingleFieldValue()

                    'Create Journal entries.
                End If

                If CCommon.ToBool(GetQueryStringVal("isDF")) AndAlso Not Session("ReleaseItems") Is Nothing Then
                    Dim list As List(Of OpportunityItemsReleaseDates) = Session("ReleaseItems")

                    If Not list Is Nothing AndAlso list.Count > 0 Then
                        Dim listInserted As New List(Of OpportunityItemsReleaseDates)


                        Dim objOpportunityItemsReleaseDates As New OpportunityItemsReleaseDates
                        objOpportunityItemsReleaseDates.DomainID = DomainID

                        For Each objReleaseItem As OpportunityItemsReleaseDates In list
                            objOpportunityItemsReleaseDates.ID = If(CCommon.ToLong(objReleaseItem.ID) = 0, -1, objReleaseItem.ID)
                            objOpportunityItemsReleaseDates.OppID = objReleaseItem.OppID
                            objOpportunityItemsReleaseDates.OppItemID = objReleaseItem.OppItemID
                            objOpportunityItemsReleaseDates.ReleaseDate = DateTime.Now
                            objOpportunityItemsReleaseDates.Quantity = CCommon.ToLong(objReleaseItem.OriginalQuantity)
                            objOpportunityItemsReleaseDates.ReleaseStatus = 2

                            If objReleaseItem.ID = 0 Then
                                If listInserted.Where(Function(x) x.ID = 0 AndAlso x.OppID = objOpportunityItemsReleaseDates.OppID AndAlso x.OppItemID = objOpportunityItemsReleaseDates.OppItemID).Count() = 0 Then
                                    'IN CASE OF KIT OR MATRIX ITEM CHILD ITEMS HAVE SAME OPPID AND OPPITEMID SO NO NEED TO INSERT MULTIPLE RECORDS
                                    objOpportunityItemsReleaseDates.UpdateItemReleaseStatus(lngOppId)
                                    listInserted.Add(New OpportunityItemsReleaseDates With {.ID = 0, .OppID = objOpportunityItemsReleaseDates.OppID, .OppItemID = objOpportunityItemsReleaseDates.OppItemID})
                                End If
                            Else
                                objOpportunityItemsReleaseDates.UpdateItemReleaseStatus(lngOppId)
                            End If
                        Next

                        objOpportunityItemsReleaseDates.UpdateOrderReleaseStatus()
                    End If
                End If
            ElseIf (_PageType = PageType.AddOppertunity) Then
                Try
                    Dim objAlerts As New CAlerts
                    If OppType <> 1 Then
                        'When a Purchase Opportunity is created by a “non” employee, & when a Purchase Order is created by anyone.
                        Dim dtDetails As DataTable
                        objAlerts.AlertDTLID = 22 'Alert DTL ID for sending alerts in opportunities
                        objAlerts.DomainID = Session("DomainID")
                        dtDetails = objAlerts.GetIndAlertDTL
                        If dtDetails.Rows.Count > 0 Then
                            If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
                                Dim dtEmailTemplate As DataTable
                                Dim objDocuments As New DocumentList
                                objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
                                objDocuments.DomainID = Session("DomainID")
                                dtEmailTemplate = objDocuments.GetDocByGenDocID
                                If dtEmailTemplate.Rows.Count > 0 Then
                                    Dim objSendMail As New Email
                                    Dim dtMergeFields As New DataTable
                                    Dim drNew As DataRow
                                    dtMergeFields.Columns.Add("OppID")
                                    dtMergeFields.Columns.Add("DealAmount")
                                    dtMergeFields.Columns.Add("Organization")
                                    dtMergeFields.Columns.Add("ContactName")
                                    drNew = dtMergeFields.NewRow
                                    drNew("OppID") = arrOutPut(1)
                                    drNew("DealAmount") = TotalAmount
                                    drNew("ContactName") = CType(Me.Parent.FindControl("ddlContacts"), DropDownList).SelectedItem.Text
                                    drNew("Organization") = CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).Text
                                    dtMergeFields.Rows.Add(drNew)

                                    Dim dtEmail As DataTable
                                    objAlerts.AlertDTLID = 22
                                    objAlerts.DomainID = Session("DomainId")
                                    dtEmail = objAlerts.GetAlertEmails
                                    Dim strCC As String = ""
                                    Dim p As Integer
                                    For p = 0 To dtEmail.Rows.Count - 1
                                        strCC = strCC & dtEmail.Rows(p).Item("vcEmailID") & ";"
                                    Next
                                    strCC = strCC.TrimEnd(";")
                                    objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), "", Session("UserEmail"), strCC, dtMergeFields)
                                End If
                            End If
                        End If
                    End If
                    objAlerts.AlertDTLID = 19
                    objAlerts.ContactID = CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue ''pass CompanyID eventhough declared as contact
                    If objAlerts.GetCountCompany > 0 Then
                        ' Sending Mail for Key Organization Opportunity
                        Dim dtDetails As DataTable
                        objAlerts.AlertDTLID = 19 'Alert DTL ID for sending alerts in opportunities
                        objAlerts.DomainID = Session("DomainID")
                        dtDetails = objAlerts.GetIndAlertDTL
                        If dtDetails.Rows.Count > 0 Then
                            If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
                                Dim dtEmailTemplate As DataTable
                                Dim objDocuments As New DocumentList
                                objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
                                objDocuments.DomainID = Session("DomainID")
                                dtEmailTemplate = objDocuments.GetDocByGenDocID
                                If dtEmailTemplate.Rows.Count > 0 Then
                                    Dim objSendMail As New Email
                                    Dim dtMergeFields As New DataTable
                                    Dim drNew As DataRow
                                    dtMergeFields.Columns.Add("OppID")
                                    dtMergeFields.Columns.Add("DealAmount")
                                    dtMergeFields.Columns.Add("Organization")
                                    drNew = dtMergeFields.NewRow
                                    drNew("OppID") = arrOutPut(1)
                                    drNew("DealAmount") = TotalAmount
                                    drNew("Organization") = CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).Text

                                    Dim dtEmail As DataTable
                                    objAlerts.AlertDTLID = 19
                                    objAlerts.DomainID = Session("DomainId")
                                    dtEmail = objAlerts.GetAlertEmails
                                    Dim strCC As String = ""
                                    Dim p As Integer
                                    For p = 0 To dtEmail.Rows.Count - 1
                                        strCC = strCC & dtEmail.Rows(p).Item("vcEmailID") & ";"
                                    Next
                                    strCC = strCC.TrimEnd(";")
                                    dtMergeFields.Rows.Add(drNew)
                                    If objCommon Is Nothing Then objCommon = New CCommon
                                    objCommon.byteMode = 1
                                    objCommon.ContactID = Session("UserContactID")
                                    objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtDetails.Rows(0).Item("tintCCManager") = 1, objCommon.GetManagerEmail, ""), Session("UserEmail"), strCC, dtMergeFields)
                                End If
                            End If
                        End If
                    End If
                Catch ex As Exception
                End Try

                Dim strScript As String = ""
                strScript += "window.opener.reDirectPage('../opportunity/frmOpportunities.aspx?opId=" & arrOutPut(0) & "'); self.close();"
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "OpenOpportunity", strScript, True)
            End If

        Catch ex As Exception
            boolFlag = False
            Throw ex
        End Try
    End Sub

    Private Sub dgItems_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgItems.ItemCommand
        Try
            dsTemp = ViewState("SOItems")
            hdnApprovalActionTaken.Value = ""
            hdnClickedButton.Value = ""
            hdnIsUnitPriceApprovalRequired.Value = ""

            If e.CommandName = "Delete" Then

                If OppType = 1 Then
                    Dim decQtyShipped As Decimal = Math.Abs(CCommon.ToDecimal(CType(e.Item.FindControl("txtnumQtyShipped"), TextBox).Text))

                    If decQtyShipped > 0 Then
                        ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "ValidationDelete", "alert('You are not allowed to delete this item,since item has shipped qty.')", True)
                        Exit Sub
                    End If
                ElseIf OppType = 2 Then
                    Dim decUnitHourReceived As Decimal = Math.Abs(CCommon.ToDecimal(CType(e.Item.FindControl("txtnumUnitHourReceived"), TextBox).Text))

                    If decUnitHourReceived > 0 Then
                        ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "ValidationDelete", "alert('You are not allowed to delete this item,since item has received qty.')", True)
                        Exit Sub
                    End If
                End If

                Dim dtItem As DataTable
                dtItem = dsTemp.Tables(0)
                dtItem.Rows.Find(CType(e.Item.FindControl("lblOppItemCode"), Label).Text).Delete()
                If OppType = 1 Then
                    RecalculateContainerQty(dtItem)
                End If
                dtItem.AcceptChanges()

                dtItem = dsTemp.Tables(0)
                dtItem = dtItem.DefaultView.ToTable()
                dtItem.AcceptChanges()

                Dim i As Long = 1
                For Each dr As DataRow In dtItem.Rows
                    dr("numSortOrder") = i
                    i = i + 1
                    dr.AcceptChanges()
                Next
                'dsTemp.Tables(0).Clear()
                'dsTemp.Tables(0).Merge(dtItem)
                Dim perserveChanges As Boolean = True
                Dim msAction As System.Data.MissingSchemaAction = System.Data.MissingSchemaAction.Ignore

                Dim changes As System.Data.DataTable = dsTemp.Tables(0).GetChanges()
                If changes IsNot Nothing Then
                    changes.Merge(dtItem, perserveChanges, msAction)
                    dsTemp.Tables(0).Merge(changes, False, msAction)
                Else
                    dsTemp.Tables(0).Merge(dtItem, False, msAction)
                End If
                litMessage.Text = ""
                UpdateDetails()
                clearControls()
            ElseIf e.CommandName = "Edit" Then

                btnUpdate.Text = "Update"
                btnUpdate.Style.Remove("display")
                btnEditCancel.Style.Remove("display")

                txtHidEditOppItem.Text = CType(e.Item.FindControl("lblOppItemCode"), Label).Text

                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "HideItemSearch", "$(""#divItem"").hide();", True)

                If Not dtOppAtributes Is Nothing Then dtOppAtributes.Rows.Clear()
                Dim dr As DataRow
                Dim dtItem As DataTable
                Dim dtSerItem As DataTable
                'Dim dtChildItems As DataTable
                dtItem = dsTemp.Tables(0)
                dtSerItem = dsTemp.Tables(1)
                'dtChildItems = dsTemp.Tables(2)

                Dim dRows As DataRow() = dtItem.Select("numoppitemtCode = " & CCommon.ToLong(txtHidEditOppItem.Text).ToString())
                If dRows.Length > 0 Then
                    dr = dRows(0)

                    hdnCurrentSelectedItem.Value = CCommon.ToString(dr("numItemCode"))
                    If OppType = 1 Then
                        hdnKitChildItems.Value = CCommon.ToString(dr("KitChildItems"))
                        If Not dr("ItemReleaseDate") Is Nothing AndAlso Not dr("ItemReleaseDate") Is DBNull.Value Then
                            rdpItemDate.SelectedDate = Convert.ToDateTime(dr("ItemReleaseDate"))
                        End If
                    Else
                        If Not dr("ItemRequiredDate") Is Nothing AndAlso Not dr("ItemRequiredDate") Is DBNull.Value Then
                            rdpItemDate.SelectedDate = Convert.ToDateTime(dr("ItemRequiredDate"))
                        End If
                    End If

                    If Not IsDBNull(dr("ItemType")) Then
                        If Not ddltype.Items.FindByText(dr("ItemType")) Is Nothing Then
                            ddltype.ClearSelection()
                            ddltype.Items.FindByText(dr("ItemType")).Selected = True
                        End If
                    End If

                    LoadItemDetails(hdnCurrentSelectedItem.Value)

                    txtdesc.Text = CCommon.ToString(dr("vcItemDesc"))

                    If radcmbUOM.Items.FindItemByValue(dr("numUOM")) IsNot Nothing Then
                        radcmbUOM.ClearSelection()
                        radcmbUOM.Items.FindItemByValue(dr("numUOM")).Selected = True
                    End If

                    hdnVendorId.Value = CCommon.ToLong(dr("numSOVendorId"))

                    If ddltype.SelectedValue = "P" Then
                        If objCommon Is Nothing Then objCommon = New CCommon
                        Dim dtWarehouses As DataTable = objCommon.GetWarehousesForSelectedItem(CCommon.ToLong(hdnCurrentSelectedItem.Value), 0, CCommon.ToDouble(dr("numUnitHour")), OrderSource, SourceType, CCommon.ToLong(hdnShipToCountry.Value), CCommon.ToLong(hdnShipToState.Value), hdnKitChildItems.Value)

                        If _IsStockTransfer Then
                            radWareHouse.DataSource = dtWarehouses
                            radWareHouse.DataTextField = "vcWareHouse"
                            radWareHouse.DataValueField = "numWareHouseItemId"
                            radWareHouse.DataBind()

                            radWarehouseTo.DataSource = dtWarehouses
                            radWarehouseTo.DataBind()
                        Else
                            radWareHouse.DataSource = dtWarehouses
                            radWareHouse.DataTextField = "vcWareHouse"
                            radWareHouse.DataValueField = "numWareHouseItemId"
                            radWareHouse.DataBind()

                            If Not radWareHouse.FindItemByValue(CCommon.ToString(dr("numWarehouseItmsID"))) Is Nothing Then
                                radWareHouse.FindItemByValue(CCommon.ToString(dr("numWarehouseItmsID"))).Selected = True
                                UpdateInventoryGrid()
                            End If
                        End If


                        divWarehouse.Visible = True
                        SetDropshipCheckbox(CCommon.ToBool(dr("DropShip")), True)

                        If CCommon.ToBool(dr("DropShip")) Then
                            divWarehouse.Style.Add("display", "none")
                        End If
                    Else
                        SetDropshipCheckbox(False, False)
                        divWarehouse.Visible = False
                        radWareHouse.SelectedValue = ""
                    End If

                    SetDropshipCheckbox(CCommon.ToBool(dr("DropShip")), chkDropShip.Visible)
                    If chkDropShip.Checked Then
                        divWarehouse.Style.Add("display", "none")
                    End If

                    hdnIsCreateWO.Value = If(CCommon.ToBool(dr("bitAlwaysCreateWO")), "1", "0")
                    txtWOQty.Text = CCommon.ToDouble(dr("numWOQty"))
                    If CCommon.ToString(dr("Op_Flag")) = 2 Or CCommon.ToString(dr("Op_Flag")) = 0 Then
                        txtWOQty.Enabled = False
                    Else
                        txtWOQty.Enabled = True
                    End If
                    chkWorkOrder.Checked = dr("bitWorkOrder")
                    lblWOQty.Text = CCommon.ToString(dr("numMaxWorkOrderQty"))
                    CCommon.SetValueInDecimalFormat(CCommon.ToDouble(dr("numCost")), txtPUnitCost)
                    lblMinOrderQty.Text = "-"
                    divMinOrderQty.Visible = False
                    txtNotes.Text = CCommon.ToString(dr("vcVendorNotes"))

                    If chkWorkOrder.Checked Then
                        txtInstruction.Text = CCommon.ToString(dr("vcInstruction"))
                        If Not String.IsNullOrEmpty(CCommon.ToString(dr("dtPlannedStart"))) Then
                            rdpPlannedStart.SelectedDate = CCommon.ToSqlDate(dr("dtPlannedStart"))
                        End If
                        If Not String.IsNullOrEmpty(CCommon.ToString(dr("bintCompliationDate"))) Then
                            radCalCompliationDate.SelectedDate = CCommon.ToSqlDate(dr("bintCompliationDate"))
                        End If
                    End If

                    Dim i, k As Integer
                    'If hdKit.Value = "True" Then
                    '    Dim dv As New DataView(dtChildItems)
                    '    dv.RowFilter = "numoppitemtCode=" & CCommon.ToString(dr("numoppitemtCode"))
                    '    'dgKitItems.DataSource = dv
                    '    'dgKitItems.DataBind()
                    'End If
                    CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(dr("monPrice")), txtprice)
                    hdnUnitCost.Value = CCommon.ToDecimal(dr("monPrice"))

                    If ddltype.SelectedValue = "S" Then
                        txtUnits.Text = CCommon.ToDecimal(dr("numUnitHour"))
                        txtUnits.Attributes.Add("onkeypress", "CheckNumber(1,event);TempHideButton()")
                    Else
                        txtUnits.Text = CCommon.ToDecimal(dr("numUnitHour"))
                        txtUnits.Attributes.Add("onkeypress", "CheckNumber(2,event);TempHideButton()")
                    End If

                    If _PageType = PageType.AddEditOrder Then
                        txtUnits.Attributes.Add("onkeyup", "return validateQty('" & txtUnits.ClientID & "'," & OppType & ");")

                        radcmbUOM.Enabled = False
                    End If

                    txtOldUnits.Text = txtUnits.Text
                    txtnumUnitHourReceived.Text = CCommon.ToInteger(dr("numUnitHourReceived"))
                    txtnumQtyShipped.Text = CCommon.ToInteger(dr("numQtyShipped"))
                    txtUOMConversionFactor.Text = dr("UOMConversionFactor")

                    lblBaseUOMName.Text = dr("vcBaseUOMName")

                    If OppType = 1 Then
                        CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(dr("fltDiscount")), txtItemDiscount)

                        If CCommon.ToBool(dr("bitDisablePromotion")) Then
                            chkUsePromotion.Checked = False
                        Else
                            If CCommon.ToLong(dr("numPromotionID")) > 0 Then
                                chkUsePromotion.Checked = True
                            End If
                        End If

                        If dr("bitDiscountType") = True Then
                            radAmt.Checked = True
                        Else
                            radPer.Checked = True
                        End If

                        ddlMarkupDiscountOption.SelectedValue = dr("bitMarkupDiscount").ToString()
                    End If

                    If radcmbUOM.Items.FindItemByValue(dr("numUOM")) IsNot Nothing Then
                        radcmbUOM.ClearSelection()
                        radcmbUOM.Items.FindItemByValue(dr("numUOM")).Selected = True
                    End If

                    If (radWareHouse.Items.Count > 0) Then
                        If Not IsDBNull(dr("numWarehouseItmsID")) Then
                            If Not radWareHouse.FindItemByValue(dr("numWarehouseItmsID")) Is Nothing Then
                                radWareHouse.FindItemByValue(dr("numWarehouseItmsID")).Selected = True
                                UpdateInventoryGrid()
                            End If
                        End If
                    End If
                    If _IsStockTransfer Then
                        If (radWarehouseTo.Items.Count > 0) Then
                            If Not IsDBNull(dr("numWarehouseItmsID")) Then
                                If Not radWarehouseTo.FindItemByValue(dr("numToWarehouseItemID")) Is Nothing Then
                                    radWarehouseTo.FindItemByValue(dr("numToWarehouseItemID")).Selected = True
                                End If
                            End If
                        End If
                    End If

                    hdnItemClassification.Value = dr("numItemClassification")
                    hdnPromotionID.Value = CCommon.ToLong(dr("numPromotionID"))
                    hdnPromotionTriggered.Value = CCommon.ToBool(dr("bitPromotionTriggered"))
                    hdnPromotionDescription.Value = CCommon.ToString(dr("vcPromotionDetail"))

                    divUnitCost.Visible = True
                    divProfit.Visible = True

                    'User can't change quanity and amount once promotion is triggres
                    If OppType = 1 AndAlso CCommon.ToLong(hdnPromotionID.Value) > 0 Then
                        txtUnits.Enabled = False
                        radWareHouse.Enabled = False
                        radcmbUOM.Enabled = False
                        ddlPriceLevel.Enabled = False
                        txtprice.Enabled = False
                        txtItemDiscount.Enabled = False
                        radPer.Enabled = False
                        radAmt.Enabled = False
                    End If

                    BindPriceLevel(hdnCurrentSelectedItem.Value)

                    If CCommon.ToLong(hdnCurrentSelectedItem.Value) > 0 Then
                        ItemFieldsChanged(hdnCurrentSelectedItem.Value, True)
                    End If

                    CalculateProfit()

                    If OppType = 1 Then
                        Dim objItem As New CItems
                        objItem.ItemCode = CCommon.ToLong(hdnCurrentSelectedItem.Value)
                        objItem.ChildItemID = 0
                        Dim dsItem As DataSet = objItem.GetSelectedChildItemDetails(CCommon.ToString(dr("KitChildItems")), True)

                        If Not dsItem Is Nothing And dsItem.Tables.Count > 1 Then
                            If CCommon.ToBool(dsItem.Tables(0).Rows(0)("bitKit")) AndAlso Not CCommon.ToBool(dsItem.Tables(0).Rows(0)("bitHasChildKits")) Then
                                divPricingOption.Visible = True
                                lkbPriceLevel.Visible = False
                                lkbPriceRule.Visible = False
                                lkbLastPrice.Visible = False
                                lkbListPrice.Visible = False

                                gvChildItems.DataSource = Nothing
                                gvChildItems.DataBind()

                                UpdatePanel3.Update()

                                lkbEditKitSelection.Visible = True

                                If CCommon.ToBool(dsItem.Tables(0).Rows(0)("bitCalAmtBasedonDepItems")) Then
                                    lblKitPrice.Text = CCommon.ToDouble(dsItem.Tables(0).Rows(0)("monPrice"))
                                    hdnKitPricing.Value = CCommon.ToShort(dsItem.Tables(0).Rows(0)("tintKitAssemblyPriceBasedOn"))
                                    hdnKitListPrice.Value = CCommon.ToDouble(dsItem.Tables(0).Rows(0)("monListPrice"))

                                    divKitPrice.Visible = True
                                    If CCommon.ToShort(dsItem.Tables(0).Rows(0)("tintKitAssemblyPriceBasedOn")) = 4 Then
                                        lblKitPriceType.Text = "Kit/Assembly list price plus sum total of child item's list price"
                                    ElseIf CCommon.ToShort(dsItem.Tables(0).Rows(0)("tintKitAssemblyPriceBasedOn")) = 3 Then
                                        lblKitPriceType.Text = "Sum total of child item's Primary Vendor Cost"
                                    ElseIf CCommon.ToShort(dsItem.Tables(0).Rows(0)("tintKitAssemblyPriceBasedOn")) = 2 Then
                                        lblKitPriceType.Text = "Sum total of child item's Average Cost"
                                    Else
                                        lblKitPriceType.Text = "Sum total of child item's List Price"
                                    End If
                                Else
                                    divKitPrice.Visible = False
                                    lblKitPrice.Text = ""
                                    hdnKitPricing.Value = ""
                                    hdnKitListPrice.Value = ""
                                End If

                                gvChildItems.DataSource = dsItem.Tables(1)
                                gvChildItems.DataBind()

                                UpdatePanel3.Update()
                            Else
                                lkbEditKitSelection.Visible = False
                                divKitPrice.Visible = False
                                lblKitPrice.Text = ""
                                hdnKitPricing.Value = ""
                                hdnKitListPrice.Value = ""
                            End If
                        End If
                    End If
                End If
                UpdateDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

    Private Sub UpdateInventoryGrid()
        Try
            If Not radWareHouse.SelectedItem Is Nothing Then
                lblOnHand.Text = String.Format("{0:#,##0.#####}", CCommon.ToDouble(radWareHouse.SelectedItem.Attributes("numOnHand"))) & " (" & String.Format("{0:#,##0.#####}", CCommon.ToDouble(radWareHouse.SelectedItem.Attributes("numOnHand")) + CCommon.ToDouble(radWareHouse.SelectedItem.Attributes("numAllocation"))) & ")"
                lblOnAllocation.Text = String.Format("{0:#,##0.#####}", CCommon.ToDouble(radWareHouse.SelectedItem.Attributes("numAllocation")))
                hplOnOrder.Text = String.Format("{0:#,##0.#####}", CCommon.ToDouble(radWareHouse.SelectedItem.Attributes("numOnOrder")))
                hplOnOrder.Attributes.Add("onclick", "OpenInTransit(" & CCommon.ToLong(hdnCurrentSelectedItem.Value) & "," & radWareHouse.SelectedItem.Value & ")")
                lblOnBackOrder.Text = String.Format("{0:#,##0.#####}", CCommon.ToDouble(radWareHouse.SelectedItem.Attributes("numBackOrder")))
            Else
                lblOnHand.Text = ""
                lblOnAllocation.Text = ""
                hplOnOrder.Text = ""
                hplOnOrder.Attributes.Remove("onclick")
                lblOnBackOrder.Text = ""
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub RecalculateContainerQty(ByRef dtItem As DataTable)
        Try
            'FIRST MAKE ALL CONTAINER QTY 0
            Dim arrContainer As DataRow() = dtItem.Select("numContainer > 0")
            For Each drRowC In arrContainer
                Dim arrContainers As DataRow() = dtItem.Select("numItemCode=" & drRowC("numContainer"))

                If Not arrContainers Is Nothing AndAlso arrContainers.Length > 0 Then
                    For Each drCont As DataRow In arrContainers
                        drCont("numUnitHour") = 0
                    Next
                End If
            Next

            'Now calculate container qty again because item qty can be change from grid
            arrContainer = dtItem.Select("numContainer > 0")
            If Not arrContainer Is Nothing AndAlso arrContainer.Length > 0 Then
                Dim dtTempContainer As New DataTable
                dtTempContainer.Columns.Add("numContainer")
                dtTempContainer.Columns.Add("numContainerQty")
                dtTempContainer.Columns.Add("numWarehouseID")

                For Each dr As DataRow In arrContainer
                    If dtTempContainer.Select("numContainer =" & dr("numContainer") & " AND numWarehouseID=" & CCommon.ToLong(dr("numWarehouseID")) & " AND numContainerQty=" & dr("numContainerQty")).Length = 0 Then
                        Dim drTemp As DataRow = dtTempContainer.NewRow
                        drTemp("numContainer") = dr("numContainer")
                        drTemp("numContainerQty") = dr("numContainerQty")
                        drTemp("numWarehouseID") = CCommon.ToLong(dr("numWarehouseID"))
                        dtTempContainer.Rows.Add(drTemp)
                    End If
                Next

                For Each drContainer As DataRow In dtTempContainer.Rows
                    'Check if there are items exists which uses same container and has same qty which can be added to container and container qty is not fully used
                    Dim containerQty As Double = 0
                    Dim arrItems As DataRow() = dtItem.Select(" Dropship = 0 AND numContainer=" & drContainer("numContainer") & " AND numWarehouseID=" & drContainer("numWarehouseID") & " AND numContainerQty=" & drContainer("numContainerQty"))
                    If Not arrItems Is Nothing AndAlso arrItems.Length > 0 Then
                        Dim qty As Double = arrItems.Sum(Function(x) CCommon.ToDouble(x("numUnitHour") * x("UOMConversionFactor")))
                        If qty > CCommon.ToDouble(drContainer("numContainerQty")) Then
                            While qty >= CCommon.ToDouble(drContainer("numContainerQty"))
                                containerQty = containerQty + 1
                                qty = qty - CCommon.ToDouble(drContainer("numContainerQty"))
                            End While

                            If (qty > 0) Then
                                containerQty = containerQty + 1
                            End If
                        Else
                            containerQty = containerQty + 1
                        End If
                    End If

                    Dim drMain As DataRow = dtItem.Select("numItemCode=" & drContainer("numContainer") & " AND numWarehouseID=" & drContainer("numWarehouseID")).FirstOrDefault()

                    If Not drMain Is Nothing Then
                        drMain("numUnitHour") = drMain("numUnitHour") + containerQty
                    End If
                Next
            End If


            'Remove containers with qty 0
            Dim arrMainContainer As DataRow() = dtItem.Select("numContainer > 0")
            If Not arrMainContainer Is Nothing AndAlso arrMainContainer.Length > 0 Then
                For Each dr As DataRow In arrMainContainer
                    Dim arrItems As DataRow() = dtItem.Select("numContainer=" & dr("numContainer") & " AND numUnitHour=0")

                    If arrItems.Length > 0 Then
                        For Each drContainer As DataRow In arrItems
                            dtItem.Rows.Remove(drContainer)
                        Next
                    End If
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Sub UpdateDataTable()
        Try
            If Not ViewState("SOItems") Is Nothing Then
                dsTemp = ViewState("SOItems")
                Dim dtItem As DataTable
                dtItem = dsTemp.Tables(0)
                If dtItem.Rows.Count > 0 Then
                    Dim i, k As Integer
                    i = 0

                    For Each dgGridItem As DataGridItem In dgItems.Items
                        dtItem.Rows(i).Item("numUnitHour") = Math.Abs(CCommon.ToDouble(CType(dgGridItem.FindControl("txtUnits"), TextBox).Text))
                        dtItem.Rows(i).Item("monPrice") = Convert.ToDecimal(CType(dgGridItem.FindControl("hdnUnitPrice"), HiddenField).Value)
                        dtItem.Rows(i).Item("monTotAmtBefDiscount") = Convert.ToDecimal(dtItem.Rows(i).Item("numUnitHour") * dtItem.Rows(i).Item("UOMConversionFactor")) * Convert.ToDouble(dtItem.Rows(i).Item("monPrice"))
                        If dtItem.Rows(i).Item("bitDiscountType") = 0 Then
                            If (OppType = 1) Then
                                If (CCommon.ToString(CType(dgGridItem.FindControl("hdnMarkupDiscount"), HiddenField).Value) = "1") Then
                                    dtItem.Rows(i).Item("monTotAmount") = dtItem.Rows(i).Item("monTotAmtBefDiscount") + (CCommon.ToDecimal(dtItem.Rows(i).Item("monTotAmtBefDiscount")) * Convert.ToDecimal(dtItem.Rows(i).Item("fltDiscount")) / 100)
                                Else
                                    dtItem.Rows(i).Item("monTotAmount") = dtItem.Rows(i).Item("monTotAmtBefDiscount") - (CCommon.ToDecimal(dtItem.Rows(i).Item("monTotAmtBefDiscount")) * Convert.ToDecimal(dtItem.Rows(i).Item("fltDiscount")) / 100)
                                End If
                            Else
                                dtItem.Rows(i).Item("monTotAmount") = dtItem.Rows(i).Item("monTotAmtBefDiscount") - (CCommon.ToDecimal(dtItem.Rows(i).Item("monTotAmtBefDiscount")) * Convert.ToDecimal(dtItem.Rows(i).Item("fltDiscount")) / 100)
                            End If
                        Else
                            If (OppType = 1) Then
                                If (CCommon.ToString(CType(dgGridItem.FindControl("hdnMarkupDiscount"), HiddenField).Value) = "1") Then
                                    dtItem.Rows(i).Item("monTotAmount") = CCommon.ToDecimal(dtItem.Rows(i).Item("monTotAmtBefDiscount")) + CCommon.ToDecimal(dtItem.Rows(i).Item("fltDiscount"))
                                Else
                                    dtItem.Rows(i).Item("monTotAmount") = CCommon.ToDecimal(dtItem.Rows(i).Item("monTotAmtBefDiscount")) - CCommon.ToDecimal(dtItem.Rows(i).Item("fltDiscount"))
                                End If
                            Else
                                dtItem.Rows(i).Item("monTotAmount") = CCommon.ToDecimal(dtItem.Rows(i).Item("monTotAmtBefDiscount")) - CCommon.ToDecimal(dtItem.Rows(i).Item("fltDiscount"))
                            End If
                        End If

                        If dtItem.Rows(i).Item("Op_Flag") = 0 Then
                            dtItem.Rows(i).Item("Op_Flag") = 2
                        End If

                        i = i + 1
                    Next

                    If OppType = 1 Then
                        RecalculateContainerQty(dtItem)
                    End If
                End If

                dsTemp.AcceptChanges()

                ViewState("SOItems") = dsTemp
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub AddSimilarRequiredRow()
        Dim dt As New DataTable()
        Dim objItem As New CItems
        objItem.DomainID = CCommon.ToLong(Session("DomainID"))
        objItem.ParentItemCode = hdnCurrentSelectedItem.Value
        'objItem.WarehouseID = CCommon.ToLong(DirectCast(Me.Parent.FindControl("hdnShipToWarehouseID"), HiddenField).Value)
        If Not DirectCast(Me.Parent.FindControl("hdnShipToWarehouseID"), HiddenField) Is Nothing AndAlso CCommon.ToLong(DirectCast(Me.Parent.FindControl("hdnShipToWarehouseID"), HiddenField).Value) > 0 Then
            objItem.WarehouseID = CCommon.ToLong(DirectCast(Me.Parent.FindControl("hdnShipToWarehouseID"), HiddenField).Value)
        Else
            If OppType = 1 And CCommon.ToLong(hdnShipFromLocation.Value) > 0 Then
                objItem.WarehouseID = CCommon.ToLong(hdnShipFromLocation.Value)
            ElseIf CCommon.ToLong(hdnDefaultUserExternalLocation.Value) > 0 Then
                objItem.WarehouseID = CCommon.ToLong(hdnDefaultUserExternalLocation.Value)
            Else
                objItem.WarehouseID = CCommon.ToLong(radWareHouse.SelectedValue)
            End If
        End If

        dt = objItem.GetSimilarRequiredItem()

        Dim MaxRowOrder As Integer
        MaxRowOrder = dsTemp.Tables(0).Rows.Count

        If dt.Rows.Count > 0 Then
            For Each dr As DataRow In dt.Rows

                objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                If Not _PageType = PageType.AddEditOrder Then
                    If IsStockTransfer Then
                        objItem.DivisionID = IIf(CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue)
                    Else
                        objItem.DivisionID = IIf(CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue)
                    End If
                Else
                    objItem.DivisionID = CCommon.ToLong(GetQueryStringVal("DivId"))
                End If
                objItem.byteMode = CCommon.ToShort(OppType)
                objItem.ItemCode = dr("numItemCode")
                objItem.WareHouseItemID = dr("numWareHouseItemID")
                objItem.GetItemAndWarehouseDetails()
                objItem.ItemDesc = dr("txtItemDesc")
                objItem.bitHasKitAsChild = 0
                objItem.vcKitChildItems = ""
                objItem.SelectedUOMID = CCommon.ToLong(dr("numSaleUnit"))
                objItem.SelectedUOM = ""
                objItem.AttributeIDs = ""
                objItem.Attributes = ""
                MaxRowOrder = MaxRowOrder + 1
                objItem.numSortOrder = MaxRowOrder
                objItem.numContainer = dr("numContainer")

                If objItem.ItemType = "P" AndAlso IsDuplicate(objItem.ItemCode, objItem.WareHouseItemID, dr("bitAllowDropShip")) Then
                    Continue For
                End If

                If (objItem.numContainer > 0) Then
                    'Dim containerqty As Long = Math.Ceiling((CCommon.ToDouble(txtUnits.Text) * If(CCommon.ToDouble(txtUOMConversionFactor.Text) > 0, CCommon.ToDouble(txtUOMConversionFactor.Text), 1.0)) / objItem.numNoItemIntoContainer)
                    Dim containerqty As Long = Math.Ceiling((1 * If(CCommon.ToDouble(dr("fltUOMConversionFactor")) > 0, CCommon.ToDouble(dr("fltUOMConversionFactor")), 1.0)) / objItem.numNoItemIntoContainer)

                    Dim objContainerItem As New CItems
                    objContainerItem.DomainID = CCommon.ToLong(Session("DomainID"))
                    objContainerItem.WarehouseID = objItem.WarehouseID
                    objContainerItem.ItemCode = objItem.numContainer
                    Dim dtContainer As DataTable = objContainerItem.GetItemWarehouseLocations()

                    If Not dtContainer Is Nothing AndAlso dtContainer.Rows.Count > 0 Then
                        Dim objItem1 As New CItems
                        objItem1.byteMode = CCommon.ToShort(OppType)
                        objItem1.ItemCode = objItem.numContainer
                        objItem1.WareHouseItemID = CCommon.ToLong(dtContainer.Rows(0)("numWarehouseItemID"))
                        objItem1.GetItemAndWarehouseDetails()

                        objItem1.bitHasKitAsChild = False
                        objItem1.vcKitChildItems = ""
                        MaxRowOrder = MaxRowOrder + 1
                        objItem1.numSortOrder = MaxRowOrder
                        objItem1.SelectedUOMID = objItem1.BaseUnit
                        objItem1.SelectedQuantity = containerqty
                        objItem1.AttributeIDs = ""
                        objItem1.Attributes = ""
                        objItem1.numNoItemIntoContainer = -1

                        If objItem.ItemType = "P" AndAlso IsDuplicate(objItem1.ItemCode, objItem1.WareHouseItemID, dr("bitAllowDropShip")) Then
                            If objItem1.numNoItemIntoContainer = -1 Then
                                Dim dsContainer As DataSet
                                dsContainer = ViewState("SOItems")

                                Dim itemQtyInContainer As Double = 0
                                Dim containerMaxQty As Double = 0


                                Dim arrContainers As DataRow() = dsContainer.Tables(0).Select("numItemCode=" & objItem1.ItemCode & " AND numWarehouseItmsID=" & objItem1.WareHouseItemID)
                                If Not arrContainers Is Nothing AndAlso arrContainers.Length > 0 Then
                                    For Each drSameContainer As DataRow In arrContainers
                                        containerMaxQty = containerMaxQty + CCommon.ToDouble(drSameContainer("numUnitHour") * objItem.numNoItemIntoContainer)
                                    Next
                                End If

                                'Check if there are items exists which uses same container and has same qty which can be added to container and container qty is not fully used
                                Dim arrItems As DataRow() = dsContainer.Tables(0).Select("numContainer=" & objItem1.ItemCode & " AND numWarehouseID=" & objItem1.WarehouseID & " AND numContainerQty=" & objItem.numNoItemIntoContainer)
                                If Not arrItems Is Nothing AndAlso arrItems.Length > 0 Then
                                    For Each drSameContainer As DataRow In arrItems
                                        itemQtyInContainer = itemQtyInContainer + CCommon.ToDouble(drSameContainer("numUnitHour") * drSameContainer("UOMConversionFactor"))
                                    Next
                                End If

                                If containerMaxQty > 0 AndAlso itemQtyInContainer > 0 Then
                                    Dim itemCanbeAddedToContainer As Double = containerMaxQty - itemQtyInContainer

                                    If itemCanbeAddedToContainer < CCommon.ToDouble(txtUnits.Text) * If(CCommon.ToDouble(txtUOMConversionFactor.Text) > 0, CCommon.ToDouble(txtUOMConversionFactor.Text), 1.0) Then
                                        containerqty = containerqty - (CCommon.ToDouble(txtUnits.Text) * If(CCommon.ToDouble(txtUOMConversionFactor.Text) > 0, CCommon.ToDouble(txtUOMConversionFactor.Text), 1.0) - itemCanbeAddedToContainer)
                                        objItem1.SelectedQuantity = containerqty
                                        Dim drContainer As DataRow() = dsContainer.Tables(0).Select("numItemCode=" & objItem1.ItemCode & " AND numWarehouseItmsID=" & objItem1.WareHouseItemID)
                                        For Each drowContainer As DataRow In drContainer
                                            drowContainer("numUnitHour") = (drowContainer("numUnitHour")) + objItem1.SelectedQuantity
                                        Next
                                    End If
                                Else
                                    Dim drContainer As DataRow() = dsContainer.Tables(0).Select("numItemCode=" & objItem1.ItemCode & " AND numWarehouseItmsID=" & objItem1.WareHouseItemID)
                                    For Each drowContainer As DataRow In drContainer
                                        drowContainer("numUnitHour") = (drowContainer("numUnitHour")) + objItem1.SelectedQuantity
                                    Next
                                End If

                                AddRelatedItemToSession(objItem, True, dr("fltUOMConversionFactor"))
                                ViewState("SOItems") = dsContainer
                                dsTemp = dsContainer
                            End If
                        Else
                            AddRelatedItemToSession(objItem, True, dr("fltUOMConversionFactor"))
                            AddRelatedItemToSession(objItem1, True, dr("fltUOMConversionFactor"))
                        End If

                    Else
                        'ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "DuplicateItemWarehouse", "alert(""Item associated container item does not contain warehouse. Please add warehouse to container first."")", True)
                        'Return
                    End If

                Else
                    AddRelatedItemToSession(objItem, True, dr("fltUOMConversionFactor"))
                End If
            Next
        End If
    End Sub

    Private Sub AddRelatedItemToSession(objItem As CItems, ByVal bitFromAddClick As Boolean, ByVal UOMConversionFactor As Double)
        Try
            objCommon = New CCommon
            objCommon.DomainID = CCommon.ToLong(Session("DomainID"))

            Dim chkTaxItems As CheckBoxList
            If OppType = 1 Then
                chkTaxItems = CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList)
            End If

            Dim units As Double
            Dim price As Decimal
            Dim salePrice As Decimal
            Dim discount As Decimal = 0

            Dim isDiscountInPer As Boolean = False

            If objItem.numNoItemIntoContainer = -1 Then
                units = CCommon.ToDouble(objItem.SelectedQuantity)
                price = CCommon.ToDecimal(objItem.Price)
                salePrice = CCommon.ToDecimal(0)
            Else
                units = 1 'CCommon.ToDouble(txtUnits.Text)
                price = CCommon.ToDecimal(objItem.ListPrice)
                salePrice = CCommon.ToDecimal(objItem.ListPrice)
                discount = 0
                isDiscountInPer = radPer.Checked
                If lngShippingItemCode = objItem.ItemCode Then
                    salePrice = price
                    discount = 0
                    isDiscountInPer = False
                    objItem.Price = price
                ElseIf objItem.ItemCode = CCommon.ToLong(Session("DiscountServiceItem")) Then
                    salePrice = price
                    discount = 0
                    isDiscountInPer = False
                    objItem.Price = price
                    objItem.ItemDesc = txtdesc.Text
                End If
            End If

            'Start - Get Tax Detail
            Dim strApplicable As String
            Dim dtItemTax As DataTable

            If (OppType = 1) Then
                dtItemTax = objItem.ItemTax()

                For Each dr As DataRow In dtItemTax.Rows
                    strApplicable = strApplicable & dr("bitApplicable") & ","
                Next

                strApplicable = strApplicable & objItem.Taxable
            End If
            If objItem.numNoItemIntoContainer = -1 Then
                ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon,
                                                                               dsTemp,
                                                                               True,
                                                                               objItem.ItemType,
                                                                               "",
                                                                               False,
                                                                               objItem.KitParent,
                                                                               objItem.ItemCode,
                                                                               units,
                                                                               price,
                                                                               objItem.ItemDesc, objItem.WareHouseItemID, objItem.ItemName, objItem.Warehouse, 0, objItem.ModelID, CCommon.ToLong(objItem.SelectedUOMID), If(CCommon.ToLong(objItem.SelectedUOMID) > 0, objItem.SelectedUOM, "-"),
                                                                               1,
                                                                               _PageType.ToString(), isDiscountInPer, discount, strApplicable, txtTax.Text, "", chkTaxItems,
                                                                               0, "", CCommon.ToString(""),
                                                                               CCommon.ToLong(0),
                                                                               CCommon.ToLong(0),
                                                                               numProjectID:=CCommon.ToLong(GetQueryStringVal("Source")),
                                                                               numProjectStageID:=CCommon.ToLong(GetQueryStringVal("StageID")),
                                                                               vcBaseUOMName:=0,
                                                                               numSOVendorId:=CCommon.ToLong(0),
                                                                                strSKU:=objItem.SKU, objItem:=objItem, primaryVendorCost:=0, salePrice:=salePrice, numMaxWOQty:=objItem.numMaxWOQty, vcAttributes:=objItem.Attributes, vcAttributeIDs:=objItem.AttributeIDs, numContainer:=objItem.numContainer, numContainerQty:=objItem.numNoItemIntoContainer,
                                                                               numItemClassification:=CCommon.ToLong(0),
                                                                               numPromotionID:=CCommon.ToLong(0),
                                                                               IsPromotionTriggered:=CCommon.ToBool(0),
                                                                               vcPromotionDetail:=CCommon.ToString(""), numSortOrder:=objItem.numSortOrder)
            Else
                ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon,
                                                                               dsTemp,
                                                                               True,
                                                                               objItem.ItemType,
                                                                               "",
                                                                               objItem.AllowDropShip,
                                                                               objItem.KitParent,
                                                                               objItem.ItemCode,
                                                                               units,
                                                                               price,
                                                                               objItem.ItemDesc, objItem.WareHouseItemID, objItem.ItemName, objItem.Warehouse, 0, objItem.ModelID, objItem.SaleUnit, "",
                                                                              UOMConversionFactor,
                                                                               _PageType.ToString(), isDiscountInPer, discount, strApplicable, txtTax.Text, "", chkTaxItems,
                                                                               0, "", "",
                                                                              0,
                                                                               0,
                                                                               numProjectID:=CCommon.ToLong(GetQueryStringVal("Source")),
                                                                               numProjectStageID:=CCommon.ToLong(GetQueryStringVal("StageID")),
                                                                               vcBaseUOMName:=hdnBaseUOMName.Value,
                                                                               numPrimaryVendorID:=CCommon.ToLong(objItem.VendorID),
                                                                               numSOVendorId:=CCommon.ToLong(hdnPrimaryVendorID.Value),
                                                                                strSKU:=objItem.SKU, objItem:=objItem, primaryVendorCost:=objItem.monCost, salePrice:=salePrice, numMaxWOQty:=objItem.numMaxWOQty, vcAttributes:=objItem.Attributes, vcAttributeIDs:=objItem.AttributeIDs, numContainer:=objItem.numContainer, numContainerQty:=objItem.numNoItemIntoContainer,
                                                                               numItemClassification:=CCommon.ToLong(objItem.ItemClassification),
                                                                               numPromotionID:=0,
                                                                               IsPromotionTriggered:=0,
                                                                               vcPromotionDetail:="", numSortOrder:=objItem.numSortOrder)
            End If


            objCommon = Nothing

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub RelatedItems(IsPreCheckout As Boolean)
        Dim objItems As New CItems
        objItems.DomainID = CCommon.ToLong(Session("DomainID"))
        objItems.ParentItemCode = hdnCurrentSelectedItem.Value
        If IsPreCheckout = False Then
            objItems.byteMode = 2
        ElseIf IsPreCheckout = True Then
            objItems.byteMode = 8
        End If

        objItems.SiteID = CCommon.ToLong(Session("SiteId"))
        Dim dt As DataTable
        dt = objItems.GetSimilarItem
        If dt.Rows.Count > 0 Then
            gvRelatedItems.DataSource = dt
            gvRelatedItems.DataBind()

            divRelatedItems.Style.Add("display", "")
            UpdatePanelActionWindow.Update()
            RelatedItemsPromotion(dt.Rows(0)("numItemCode"), dt.Rows(0)("numWareHouseItemID"))

            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideItemSearch", "$('.itemsArea').css('visibility', 'visible');$('.itemsArea').css('display', 'flow-root');", True)
        End If


        'If IsPreCheckout = False Then
        '    dt = objItems.GetSimilarItem
        '    If dt.Rows.Count > 0 Then
        '        gvRelatedItems.DataSource = dt
        '        gvRelatedItems.DataBind()

        '        divRelatedItems.Style.Add("display", "")
        '        UpdatePanelActionWindow.Update()

        '        RelatedItemsPromotion(dt.Rows(0)("numItemCode"), dt.Rows(0)("numWareHouseItemID"))

        '        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideItemSearch", "$('.itemsArea').css('visibility', 'visible');$('.itemsArea').css('display', 'flow-root');", True)
        '    End If
        'ElseIf IsPreCheckout = True Then
        '    objItems.IsPreCheckout = IsPreCheckout
        '    objItems.WarehouseID = CCommon.ToLong(radcmbLocation.SelectedValue)
        '    dt = objItems.GetSimilarItemWithPreCheckout
        '    If dt.Rows.Count > 0 Then
        '        gvRelatedItems.DataSource = dt
        '        gvRelatedItems.DataBind()

        '        divRelatedItems.Style.Add("display", "")
        '        UpdatePanelActionWindow.Update()

        '        RelatedItemsPromotion(dt.Rows(0)("numItemCode"), dt.Rows(0)("numWareHouseItemID"))

        '        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideItemSearch", "$('.itemsArea').css('visibility', 'visible');$('.itemsArea').css('display', 'flow-root');", True)
        '    End If
        'End If
    End Sub

    Private Sub RelatedItemsPromotion(numItemCode As Integer, numWarehouseItemID As Long)
        Dim objItems As New CItems
        objItems.ItemCode = numItemCode
        objItems.DomainID = CCommon.ToLong(Session("DomainID"))
        objItems.WareHouseItemID = numWarehouseItemID
        objItems.numSiteId = CCommon.ToLong(HttpContext.Current.Session("SiteID"))
        Dim dt As DataTable = objItems.GetPromotionsForSimilarItems()
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            divPromotionDetailRI.Visible = True
            lblPromotionNameRI.Text = CCommon.ToString(dt.Rows(0)("vcProName"))
            lblPromotionDescriptionRI.Text = CCommon.ToString(dt.Rows(0)("vcPromotionDescription"))

            If Not CCommon.ToBool(dt.Rows(0)("bitNeverExpires")) Then
                lblExpirationRI.Text = " (Expires on" & dt.Rows(0)("dtExpire") & ")"
            Else
                lblExpirationRI.Text = ""
            End If

            If CCommon.ToBool(dt.Rows(0)("bitRequireCouponCode") AndAlso hdnCouponNotRequire.Value <> "1") Then
                txtCouponCodeRI.Visible = True
                txtCouponCodeRI.Text = dt.Rows(0)("txtCouponCode")
                lblCouponText.Visible = True
            ElseIf hdnCouponNotRequire.Value = "1" Then
                lblPromotionDescriptionRI.Text = hdnPromotionDescription.Value
                lblPromotionNameRI.Text = hdnPromotionName.Value
                txtCouponCodeRI.Visible = False
                lblCouponText.Visible = False
            End If
        Else
            divPromotionDetailRI.Visible = False
        End If
    End Sub

    Private Function CheckIfSalesTaxColumnExist(ByVal columnName As String) As Boolean
        Dim isColumnExist As Boolean = False

        For Each column As DataGridColumn In dgItems.Columns
            If column.HeaderText = columnName Then
                isColumnExist = True
                Exit For
            End If
        Next

        Return isColumnExist
    End Function

    Private Sub dgItems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgItems.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                If CCommon.ToLong(Session("UserContactID")) > 0 AndAlso CCommon.ToLong(Session("UserID")) = 0 Then
                    CType(e.Item.FindControl("txtUnitPrice"), TextBox).ReadOnly = True
                End If

                Dim dataRowView As DataRowView = DirectCast(e.Item.DataItem, DataRowView)
                CCommon.SetCommaSeparatedInDecimalFormat(CCommon.ToDecimal(dataRowView("monPrice")), CType(e.Item.FindControl("txtUnitPrice"), TextBox))
                CCommon.SetCommaSeparatedInDecimalFormat(CCommon.ToDecimal(dataRowView("TotalTax")), CType(e.Item.FindControl("lblTaxAmt0"), Label))
                CCommon.SetCommaSeparatedInDecimalFormat(CCommon.ToDecimal(dataRowView("fltDiscount")), CType(e.Item.FindControl("txtTotalDiscount"), TextBox))

                If CCommon.ToBool(hdnEnabledItemLevelUOM.Value) Then
                    CCommon.SetCommaSeparatedInDecimalFormat(CCommon.ToDecimal(dataRowView("monPrice") * dataRowView("UOMConversionFactor")), CType(e.Item.FindControl("txtUnitPrice"), TextBox))
                    CType(e.Item.FindControl("lblUOMName"), Label).Visible = False
                    CType(e.Item.FindControl("lblItemUOM"), Label).Visible = True
                Else
                    CType(e.Item.FindControl("lblUOMName"), Label).Visible = True
                    CType(e.Item.FindControl("lblItemUOM"), Label).Visible = False
                End If

                If hdnEditUnitPriceRight.Value = "False" Then
                    CType(e.Item.FindControl("txtUnitPrice"), TextBox).Enabled = False
                End If

                If DataBinder.Eval(e.Item.DataItem, "charItemType") = "S" Then
                    'Do not allow edit/delete for service item with project
                    If CCommon.ToLong(CType(e.Item.FindControl("lblnumProjectID"), Label).Text) > 0 AndAlso _PageType = PageType.AddEditOrder AndAlso OppType = 1 Then
                        CType(e.Item.FindControl("txtUnits"), TextBox).Enabled = False
                        CType(e.Item.FindControl("txtUnitPrice"), TextBox).Enabled = False

                        CType(e.Item.FindControl("lnkEdit"), LinkButton).Attributes.Add("onclick", "return NotAllowMessage(2)")
                        CType(e.Item.FindControl("lnkDelete"), LinkButton).Attributes.Add("onclick", "return NotAllowMessage(2)")
                    End If

                    CType(e.Item.FindControl("txtUnits"), TextBox).Attributes.Add("onkeypress", "CheckNumber(1,event)")
                Else
                    CType(e.Item.FindControl("txtUnits"), TextBox).Attributes.Add("onkeypress", "CheckNumber(2,event)")
                End If

                If _PageType = PageType.AddEditOrder Then
                    CType(e.Item.FindControl("txtUnits"), TextBox).Attributes.Add("onchange", "return validateQty('" & CType(e.Item.FindControl("txtUnits"), TextBox).ClientID & "'," & OppType & "," & CCommon.ToLong(CType(e.Item.FindControl("lblOppItemCode"), Label).Text) & ");")
                Else
                    CType(e.Item.FindControl("txtUnits"), TextBox).Attributes.Add("onkeyup", "return CalculateTotal()")
                End If

                CType(e.Item.FindControl("txtUnitPrice"), TextBox).Attributes.Add("onkeyup", "return CalculateTotal()")
                CType(e.Item.FindControl("txtUnitPrice"), TextBox).Attributes.Add("onkeypress", "CheckNumber(3,event)")

                If (OppType = 1) Then
                    If (_PageType = PageType.AddEditOrder) Then
                        If (Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "bitWorkOrder")) = True) AndAlso CCommon.ToDouble(DataBinder.Eval(e.Item.DataItem, "numWOQty")) > 0 Then
                            CType(e.Item.FindControl("lnkDelete"), LinkButton).Attributes.Add("onclick", "return NotAllowMessage(3)")
                        End If

                        If DirectCast(e.Item.DataItem, System.Data.DataRowView).Row.Table.Columns.Contains("numSerialNoAssigned") Then
                            If (CCommon.ToBool(DataBinder.Eval(e.Item.DataItem, "bitSerialized")) = True Or CCommon.ToBool(DataBinder.Eval(e.Item.DataItem, "bitLotNo")) = True) AndAlso CCommon.ToInteger(DataBinder.Eval(e.Item.DataItem, "numSerialNoAssigned")) > 0 Then
                                CType(e.Item.FindControl("lnkEdit"), LinkButton).Attributes.Add("onclick", "return NotAllowMessage(4)")
                                CType(e.Item.FindControl("lnkDelete"), LinkButton).Attributes.Add("onclick", "return NotAllowMessage(4)")
                                CType(e.Item.FindControl("txtUnits"), TextBox).Enabled = False
                            End If
                        End If
                    End If

                    If (Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "bitDiscountType")) = True) Then
                        CCommon.SetCommaSeparatedInDecimalFormat(CCommon.ToDecimal(DataBinder.Eval(e.Item.DataItem, "fltDiscount")), CType(e.Item.FindControl("lblDiscount"), Label))
                    Else
                        CType(e.Item.FindControl("lblDiscount"), Label).Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(DataBinder.Eval(e.Item.DataItem, "monTotAmtBefDiscount")) - CCommon.ToDecimal(DataBinder.Eval(e.Item.DataItem, "monTotAmount"))) & " (" & CCommon.GetDecimalFormat(CCommon.ToDecimal(DataBinder.Eval(e.Item.DataItem, "fltDiscount"))) & "%)"
                    End If
                    If (CType(e.Item.FindControl("hdnDBMarkupDiscount"), HiddenField).Value <> "") Then
                        CType(e.Item.FindControl("hdnMarkupDiscount"), HiddenField).Value = CType(e.Item.FindControl("hdnDBMarkupDiscount"), HiddenField).Value
                    Else
                        CType(e.Item.FindControl("hdnMarkupDiscount"), HiddenField).Value = ddlMarkupDiscountOption.SelectedValue
                    End If

                End If
                If (OppType = 2) Then
                    If (_PageType = PageType.AddEditOrder) Then
                        If DirectCast(e.Item.DataItem, System.Data.DataRowView).Row.Table.Columns.Contains("numSerialNoAssigned") Then
                            If (CCommon.ToBool(DataBinder.Eval(e.Item.DataItem, "bitSerialized")) = True Or CCommon.ToBool(DataBinder.Eval(e.Item.DataItem, "bitLotNo")) = True) AndAlso CCommon.ToInteger(DataBinder.Eval(e.Item.DataItem, "numSerialNoAssigned")) > 0 Then
                                CType(e.Item.FindControl("lnkEdit"), LinkButton).Attributes.Add("onclick", "return NotAllowMessage(4)")
                                CType(e.Item.FindControl("lnkDelete"), LinkButton).Attributes.Add("onclick", "return NotAllowMessage(4)")

                                CType(e.Item.FindControl("txtUnits"), TextBox).Enabled = False
                                CType(e.Item.FindControl("txtUnitPrice"), TextBox).Enabled = False
                            ElseIf CCommon.ToDouble(DataBinder.Eval(e.Item.DataItem, "numUnitHourReceived")) > 0 Then
                                'CType(e.Item.FindControl("lnkEdit"), LinkButton).Attributes.Add("onclick", "return NotAllowMessage(6)")
                                CType(e.Item.FindControl("lnkDelete"), LinkButton).Attributes.Add("onclick", "return NotAllowMessage(6)")

                                'CType(e.Item.FindControl("txtUnits"), TextBox).Enabled = False
                                CType(e.Item.FindControl("txtUnitPrice"), TextBox).Enabled = False
                            End If
                        ElseIf CCommon.ToDouble(DataBinder.Eval(e.Item.DataItem, "numUnitHourReceived")) > 0 Then
                            'CType(e.Item.FindControl("lnkEdit"), LinkButton).Attributes.Add("onclick", "return NotAllowMessage(6)")
                            CType(e.Item.FindControl("lnkDelete"), LinkButton).Attributes.Add("onclick", "return NotAllowMessage(6)")

                            'CType(e.Item.FindControl("txtUnits"), TextBox).Enabled = False
                            CType(e.Item.FindControl("txtUnitPrice"), TextBox).Enabled = False
                        End If
                    End If
                End If

                'If Auth BizDocs associated with item then do not allow edit or delete
                If (Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "bitIsAuthBizDoc")) = True) Then
                    CType(e.Item.FindControl("txtUnitPrice"), TextBox).Enabled = False
                    CType(e.Item.FindControl("lnkDelete"), LinkButton).Attributes.Add("onclick", "return NotAllowMessage(1)")
                End If

                'If FulFillment Order BizDoc is added to order then do not allow edit or delete
                If (Convert.ToBoolean(CCommon.ToInteger(DataBinder.Eval(e.Item.DataItem, "bitAddedFulFillmentBizDoc"))) = True) Then
                    'CType(e.Item.FindControl("txtUnits"), TextBox).Enabled = False
                    CType(e.Item.FindControl("txtUnitPrice"), TextBox).Enabled = False
                    CType(e.Item.FindControl("lnkDelete"), LinkButton).Attributes.Add("onclick", "return NotAllowMessage(5)")
                End If

                If OppType = 1 AndAlso CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numPromotionID")) > 0 Then
                    CType(e.Item.FindControl("txtUnitPrice"), TextBox).Enabled = False
                    CType(e.Item.FindControl("txtUnits"), TextBox).Enabled = False

                    If CCommon.ToBool(DataBinder.Eval(e.Item.DataItem, "bitPromotionTriggered")) Then
                        CType(e.Item.FindControl("lnkDelete"), LinkButton).Attributes.Add("onclick", "return DeleteItem(" & DataBinder.Eval(e.Item.DataItem, "numoppitemtCode") & ");")
                    End If

                    If (CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numItemCode") = lngDiscountServiceItemForOrder)) Then
                        CType(e.Item.FindControl("lnkEdit"), LinkButton).Visible = False
                    End If
                End If

                If Not e.Item.FindControl("lblPromDetail") Is Nothing Then
                    CType(e.Item.FindControl("lblPromDetail"), Label).Text = CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "vcPromotionDetail"))
                End If

            ElseIf e.Item.ItemType = ListItemType.Header Then
                CType(e.Item.FindControl("lblUnitPriceCaption"), Label).Text = IIf(OppType = 1, "Unit Price", "Unit Cost")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub radKitWareHouse_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemEventArgs)
        Try
            e.Item.Text = CType(e.Item.DataItem, DataRowView)("vcWareHouse").ToString() & " ---- " & CType(e.Item.DataItem, DataRowView)("Attr").ToString().TrimEnd(",")
            e.Item.Value = CType(e.Item.DataItem, DataRowView)("numWareHouseItemId").ToString()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub radWareHouse_ItemDataBound(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemEventArgs) Handles radWareHouse.ItemDataBound
        Try
            e.Item.Attributes.Add("numOnHand", CCommon.ToDouble(CType(e.Item.DataItem, DataRowView)("numOnHand")))
            e.Item.Attributes.Add("numOnOrder", CCommon.ToDouble(CType(e.Item.DataItem, DataRowView)("numOnOrder")))
            e.Item.Attributes.Add("numAllocation", CCommon.ToDouble(CType(e.Item.DataItem, DataRowView)("numAllocation")))
            e.Item.Attributes.Add("numBackOrder", CCommon.ToDouble(CType(e.Item.DataItem, DataRowView)("numBackOrder")))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub radWareHouseTo_ItemDataBound(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemEventArgs) Handles radWarehouseTo.ItemDataBound
        Try
            e.Item.Text = CType(e.Item.DataItem, DataRowView)("vcWareHouse").ToString() & " (" & CType(e.Item.DataItem, DataRowView)("numOnHand").ToString() & ") " & " ---- " & CType(e.Item.DataItem, DataRowView)("Attr").ToString().TrimEnd(",")
            e.Item.Value = CType(e.Item.DataItem, DataRowView)("numWareHouseItemId").ToString()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub UpdateDetails()
        Try
            If dsTemp Is Nothing AndAlso ViewState("SOItems") IsNot Nothing Then
                dsTemp = ViewState("SOItems")
            ElseIf dsTemp Is Nothing AndAlso ViewState("SOItems") Is Nothing Then
                Exit Sub
            End If

            Dim dtAllItems As DataTable
            dtAllItems = dsTemp.Tables(0)

            If Not CCommon.ToBool(Session("IsMinUnitPriceRule")) Then
                For Each dr As DataRow In dtAllItems.Rows
                    dr("bitItemPriceApprovalRequired") = False
                Next
            End If

            Dim strTax As String = ""
            Dim strTaxIds As String = ""
            Dim chkTaxItems As CheckBoxList

            If (_PageType = PageType.Sales) Then
                chkTaxItems = CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList)

                If CType(Me.Parent.FindControl("trNewCust"), HtmlGenericControl).Visible Then
                    objCommon = New CCommon
                    For Each Item As ListItem In chkTaxItems.Items
                        If Item.Selected = True Then

                            If Session("BaseTaxCalcOn") = 1 Then 'Base tax calculation on Shipping Address(2) or Billing Address(1) 
                                strTax = strTax & objCommon.TaxPercentage(0, CType(Me.Parent.FindControl("ddlBillCountry"), DropDownList).SelectedValue, CType(Me.Parent.FindControl("ddlBillState"), DropDownList).SelectedValue, Session("DomainID"), Item.Value, CType(Me.Parent.FindControl("txtCity"), TextBox).Text, CType(Me.Parent.FindControl("txtPostal"), TextBox).Text, Session("BaseTaxOnArea"), Session("BaseTaxCalcOn")) & ","
                            Else
                                strTax = strTax & objCommon.TaxPercentage(0, CType(Me.Parent.FindControl("ddlShipCountry"), DropDownList).SelectedValue, CType(Me.Parent.FindControl("ddlShipState"), DropDownList).SelectedValue, Session("DomainID"), Item.Value, CType(Me.Parent.FindControl("txtShipCity"), TextBox).Text, CType(Me.Parent.FindControl("txtShipPostal"), TextBox).Text, Session("BaseTaxOnArea"), Session("BaseTaxCalcOn")) & ","
                            End If

                        Else
                            strTax = strTax & "0#1,"
                        End If
                        strTaxIds = strTaxIds & Item.Value & ","
                    Next
                    strTax = strTax.TrimEnd(",")
                    strTaxIds = strTaxIds.TrimEnd(",")
                    txtTax.Text = strTax
                    TaxItemsId.Value = strTaxIds

                Else
                    If CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue <> "" Then
                        objCommon = New CCommon
                        Dim dtTaxTypes As DataTable
                        Dim ObjTaxItems As New TaxDetails
                        ObjTaxItems.DomainID = Session("DomainID")
                        dtTaxTypes = ObjTaxItems.GetTaxItems
                        For Each dr As DataRow In dtTaxTypes.Rows

                            'Base tax calculation on Shipping Address(2) or Billing Address(1) 
                            strTax = strTax & objCommon.TaxPercentage(CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue, 0, 0, Session("DomainID"), dr("numTaxItemID"), "", "", Session("BaseTaxOnArea"), Session("BaseTaxCalcOn")) & ","
                            strTaxIds = strTaxIds & dr("numTaxItemID") & ","

                        Next
                        strTax = strTax & objCommon.TaxPercentage(CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue, 0, 0, Session("DomainID"), 0, "", "", Session("BaseTaxOnArea"), Session("BaseTaxCalcOn"))

                        strTaxIds = strTaxIds & "0"
                        txtTax.Text = strTax
                        TaxItemsId.Value = strTaxIds
                    Else
                        For k As Integer = 0 To chkTaxItems.Items.Count - 1
                            strTax = strTax & "0" & ","
                        Next
                        txtTax.Text = strTax
                    End If
                End If

                Dim strTaxes(), strTaxApplicable() As String
                strTaxes = txtTax.Text.Split(",")
                strTaxApplicable = Taxable.Value.Split(",")

                If Not dtAllItems Is Nothing AndAlso dtAllItems.Rows.Count > 0 Then
                    For Each drRow As DataRow In dtAllItems.Rows
                        drRow("TotalTax") = 0

                        For k = 0 To chkTaxItems.Items.Count - 1
                            If dtAllItems.Columns.Contains("bitTaxable" & chkTaxItems.Items(k).Value) Then
                                If IIf(IsDBNull(drRow("bitTaxable" & chkTaxItems.Items(k).Value)), False, drRow("bitTaxable" & chkTaxItems.Items(k).Value)) = True Then
                                    Dim decTaxValue As Decimal = CCommon.ToDecimal(strTaxes(k).Split("#")(0))
                                    Dim tintTaxType As Short = CCommon.ToShort(strTaxes(k).Split("#")(1))

                                    If tintTaxType = 2 Then 'FLAT AMOUNT
                                        drRow("Tax" & chkTaxItems.Items(k).Value) = decTaxValue * (drRow("numUnitHour") * drRow("UOMConversionFactor"))
                                    Else 'PERCENTAGE
                                        drRow("Tax" & chkTaxItems.Items(k).Value) = IIf(IsDBNull(drRow("monTotAmount")), 0, drRow("monTotAmount")) * decTaxValue / 100
                                    End If

                                    drRow("TotalTax") = CCommon.ToDecimal(drRow("TotalTax")) + CCommon.ToDecimal(drRow("Tax" & chkTaxItems.Items(k).Value))
                                End If
                            End If
                        Next
                    Next
                End If
            End If

            dgItems.DataSource = dtAllItems
            dgItems.DataBind()

            If dtAllItems.Rows.Count > 0 Then
                Dim decGrandTotal As Decimal
                decGrandTotal = IIf(IsDBNull(dtAllItems.Compute("sum(monTotAmount)", "")), 0, dtAllItems.Compute("sum(monTotAmount)", ""))
                CType(dgItems.Controls(0).Controls(dtAllItems.Rows.Count + 1).FindControl("lblFTotal"), Label).Text = CCommon.GetDecimalFormat(Math.Truncate(10000 * decGrandTotal) / 10000)

                If (_PageType = PageType.Sales) Then
                    For Each Item As ListItem In CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList).Items
                        Dim decTax As Decimal = IIf(IsDBNull(dtAllItems.Compute("sum(Tax" & Item.Value & " )", "")), 0, dtAllItems.Compute("sum(Tax" & Item.Value & " )", ""))
                        decGrandTotal = decGrandTotal + decTax
                        CType(dgItems.Controls(0).Controls(dtAllItems.Rows.Count + 1).FindControl("lblFTaxAmt" & Item.Value), Label).Text = CCommon.GetDecimalFormat(Math.Truncate(10000 * decTax) / 10000)
                    Next
                End If

                lblShippingCost.Text = Math.Truncate(10000 * GetShippingCharge()) / 10000
                lblTotal.Text = Math.Truncate(10000 * (CCommon.ToDecimal(decGrandTotal) - CCommon.ToDecimal(Session("TotalDiscount")))) / 10000
            Else
                lblTotal.Text = "0.00"
                lblShippingCost.Text = "0.00"
            End If
            Dim strBaseCurrency As String = ""
            If (Session("MultiCurrency") = True) And (_PageType <> PageType.AddEditOrder) Then
                lblCurrencyTotal.Text = "<b>Grand Total:</b> " + IIf(CType(Me.Parent.FindControl("ddlCurrency"), DropDownList).SelectedValue > 0, CType(Me.Parent.FindControl("ddlCurrency"), DropDownList).SelectedItem.Text.Substring(0, 3), "")
                lblShippingCharges.Text = "<b>Shipping Charges:</b> " + IIf(CType(Me.Parent.FindControl("ddlCurrency"), DropDownList).SelectedValue > 0, CType(Me.Parent.FindControl("ddlCurrency"), DropDownList).SelectedItem.Text.Substring(0, 3), "")
            Else
                lblCurrencyTotal.Text = "<b>Grand Total:</b> "
                lblShippingCharges.Text = "<b>Shipping Charges:</b> "
            End If

            Dim totalWeight As Double = 0
            For Each dr As DataRow In dtAllItems.Rows
                totalWeight = totalWeight + (CCommon.ToDouble(dr("fltItemWeight")) * CCommon.ToDouble(dr("numUnitHour")))
            Next

            txtTotalWeight.Text = totalWeight
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function CheckIfColumnExist(ByVal columnName As String) As Boolean
        Try
            Dim isColumnExist As Boolean = False

            For Each column As DataGridColumn In dgItems.Columns
                If column.HeaderText = columnName Then
                    isColumnExist = True
                    Exit For
                End If
            Next

            Return isColumnExist
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub radWareHouse_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radWareHouse.SelectedIndexChanged
        Try
            If CCommon.ToLong(hdnCurrentSelectedItem.Value) > 0 Then
                ItemFieldsChanged(hdnCurrentSelectedItem.Value, If(CCommon.ToLong(txtHidEditOppItem.Text) > 0, True, False))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

    Private Sub txtunits_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUnits.TextChanged
        Try
            If CCommon.ToLong(hdnCurrentSelectedItem.Value) > 0 Then
                ItemFieldsChanged(hdnCurrentSelectedItem.Value, If(CCommon.ToLong(txtHidEditOppItem.Text) > 0, True, False))
            End If

            If CCommon.ToLong(txtHidEditOppItem.Text) = 0 AndAlso CCommon.ToBool(hdnIsAutoWarehouseSelection.Value) Then
                If objCommon Is Nothing Then objCommon = New CCommon
                Dim dtWareHouses As DataTable = objCommon.GetWarehousesForSelectedItem(hdnCurrentSelectedItem.Value, 0, 1, OrderSource, SourceType, CCommon.ToLong(hdnShipToCountry.Value), CCommon.ToLong(hdnShipToState.Value), hdnKitChildItems.Value)

                radWareHouse.DataSource = dtWareHouses
                radWareHouse.DataTextField = "vcWareHouse"
                radWareHouse.DataValueField = "numWareHouseItemId"
                radWareHouse.DataBind()
            End If

            If hdnIsCreateWO.Value = "1" Then
                txtWOQty.Text = txtUnits.Text
            ElseIf radWareHouse.Items.Count > 0 AndAlso divWorkOrder.Visible AndAlso hdnIsCreateWO.Value <> "1" Then
                If CCommon.ToDouble(txtUnits.Text) > CCommon.ToDouble(radWareHouse.Items(0).Attributes("numOnHand")) Then
                    chkWorkOrder.Checked = True
                    txtWOQty.Text = CCommon.ToDouble(txtUnits.Text) - CCommon.ToDouble(radWareHouse.Items(0).Attributes("numOnHand"))
                Else
                    chkWorkOrder.Checked = False
                    txtWOQty.Text = 0
                End If
            End If

            ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "FocusPrice", "FocusPrice();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

    Private Sub txtprice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtprice.TextChanged
        Try
            If OppType = 1 Then
                hdnUnitCost.Value = CCommon.ToDecimal(txtprice.Text)
                CalculateProfit()
            End If
            ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "FocusDiscount", "FocusDiscount();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

    Protected Sub txtPUnitCost_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPUnitCost.TextChanged
        Try
            Dim decVendorCost, decProfit, decSellPrice As Decimal

            decSellPrice = CCommon.ToDecimal(hdnUnitCost.Value)
            decVendorCost = CCommon.ToDouble(txtPUnitCost.Text)
            decProfit = (decSellPrice - decVendorCost)

            lblProfit.Text = String.Format("{0:#,##0.00} / {1:#,##0.00}", decProfit, decSellPrice)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnSaveOpenOrder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveOpenOrder.Click
        Try
            IsFromSaveAndOpenOrderDetails = True
            'If ProcessPayment() Then
            save()

            If (_PageType = PageType.Sales) Then
                Dim objRule As New OrderAutoRules
                objRule.GenerateAutoPO(OppBizDocID)
            End If

            ViewState("SOItems") = Nothing
            Session("CouponCode") = Nothing
            Session("TotalDiscount") = Nothing

            If boolFlag Then
                If IsPOS = 1 Then
                    Response.Redirect("../opportunity/frmOpportunities.aspx?opId=" & lngOppId.ToString(), False)
                Else
                    Dim strScript As String = "" '"<script language=JavaScript>"
                    If GetQueryStringVal("IsClone") = "" Then
                        strScript += "window.opener.reDirectPage('../opportunity/frmOpportunities.aspx?opId=" & lngOppId.ToString() & "'); self.close();"
                    ElseIf CCommon.ToBool(GetQueryStringVal("isDF")) Then
                        strScript += "alert('Order created.'); self.close();"
                    Else
                        strScript += "window.opener.top.frames[0].location.href=('../opportunity/frmOpportunities.aspx?opId=" & lngOppId.ToString() & "'); self.close();"
                    End If

                    'strScript += "</script>"
                    If (Not Page.IsStartupScriptRegistered("clientScript")) Then ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "clientScript", strScript, True)
                End If
            End If
        Catch ex As Exception
            If ex.Message = "FY_CLOSED" Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            ElseIf ex.Message.Contains("CAN_NOT_CREATE_MULTIPLE_PO_FOR_ASSET_ITEMS") Then
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Alert", "alert('You can\'t have multiple purchase orders for asset item(s).');", True)
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End If
        End Try
    End Sub

    Private Sub BindButtons()
        Try
            If (_PageType = PageType.Purchase Or _PageType = PageType.AddOppertunity Or _PageType = PageType.AddEditOrder) Then
                btnSaveOpenOrder.Visible = IIf(_PageType = PageType.Purchase, True, False)
                btnSaveNew.Visible = False
            End If

            'btnClose.Attributes.Add("onclick", "return Close()")

            If (_PageType = PageType.Purchase Or _PageType = PageType.Sales) Then
                CType(Me.Parent.FindControl("lnkBillTo"), LinkButton).Attributes.Add("onclick", "return OpenAddressWindow('BillTo',0)")
                CType(Me.Parent.FindControl("lnkShipTo"), LinkButton).Attributes.Add("onclick", "return OpenAddressWindow('ShipTo',0)")
            End If


            chkDropShip.Attributes.Add("onclick", "return DropShip()")
            btnSave.Attributes.Add("onclick", "return Save(" & OppType & ")")

            btnSaveNew.Attributes.Add("onclick", "return Save(" & OppType & ")")
            btnSaveOpenOrder.Attributes.Add("onclick", "return Save(" & OppType & ")")

            'Get Default BizDoc Name Set in Domain details 
            Dim objRpt As New BACRM.BusinessLogic.Reports.PredefinedReports
            Dim dtBizDoc As DataTable
            objRpt.BizDocID = CCommon.ToLong(Session("AuthoritativeSalesBizDoc"))
            dtBizDoc = objRpt.GetBizDocType()
            If dtBizDoc.Rows.Count > 0 Then
                btnSave.Text = "Save & Create " + CCommon.ToString(dtBizDoc.Rows(0)("vcData")) & " "
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnEditCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditCancel.Click
        Try
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "HidePOVendor", "$('#imgPODistance').hide(); $('#imgPODistance').removeAttr('onclick');", True)

            If OppType = 1 AndAlso CCommon.ToLong(txtHidEditOppItem.Text) = 0 AndAlso lkbAddPromotion.Visible AndAlso lkbAddPromotion.Text.Contains("Remove") Then
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "RemovePromotion", "alert('First remove applied promotion.');", True)
                Exit Sub
            End If

            btnUpdate.Text = "Add"
            divItem.Style.Add("display", "")
            lkbEditKitSelection.Visible = False
            clearControls()

            hdnCurrentSelectedItem.Value = ""
            ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "FocusItem", "FocuonItem();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

    Private Sub btnBindOrderGrid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBindOrderGrid.Click
        Try
            Dim eventTarget As String = If((Me.Request("__EVENTTARGET") Is Nothing), String.Empty, Me.Request("__EVENTTARGET"))
            If eventTarget = "btnBindOrderGrid" AndAlso hdnType.Value = "add" Then
                dsTemp = ViewState("SOItems")
                hdnType.Value = ""
            End If

            UpdateDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

    Private Sub chkSearchOrderCustomerHistory_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSearchOrderCustomerHistory.CheckedChanged
        Try
            hdnSearchOrderCustomerHistory.Value = IIf(chkSearchOrderCustomerHistory.Checked, 1, 0)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

    Private Sub btnClose_Click(sender As Object, e As System.EventArgs) Handles btnClose.Click
        Try
            ViewState("SOItems") = Nothing
            Session("CouponCode") = Nothing
            Session("TotalDiscount") = Nothing

            If (Not Page.IsStartupScriptRegistered("clientScript")) Then ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "clientScript", "self.close();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

    Private Sub lkbItemSelected_Click(sender As Object, e As EventArgs) Handles lkbItemSelected.Click
        Try
            If hdnHasKitAsChild.Value = "1" Then
                ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "LoadChildKits", "LoadChildKits(" & CCommon.ToLong(hdnCurrentSelectedItem.Value) & ")", True)
                hdnHasKitAsChild.Value = "0"
            Else
                LoadSelectedItemDetail(hdnCurrentSelectedItem.Value)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub lkbItemRemoved_Click(sender As Object, e As EventArgs) Handles lkbItemRemoved.Click
        Try
            clearControls()
            UpdateDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub AddItemToSession(objItem As CItems, Optional ByVal Quantity As Double = 1, Optional ByVal MultiSelectItemPrice As Decimal = 0, Optional ByVal itemReleaseDate As Date? = Nothing)
        Try
            Dim units As Double
            Dim price As Decimal
            Dim discount As Decimal = 0
            Dim isDiscountInPer As Boolean = False

            Dim chkTaxItems As CheckBoxList
            If OppType = 1 Then
                chkTaxItems = CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList)
            End If

            If objItem.numNoItemIntoContainer = -1 Then
                units = CCommon.ToDouble(objItem.SelectedQuantity)
                price = CCommon.ToDecimal(objItem.Price)
                discount = CCommon.ToDecimal(0)
            Else
                units = CCommon.ToDouble(txtUnits.Text)
                price = CCommon.ToDecimal(txtprice.Text)
                discount = CCommon.ToDecimal(txtItemDiscount.Text)
                isDiscountInPer = radPer.Checked
            End If

            'Start - Get Tax Detail
            Dim strApplicable As String
            Dim dtItemTax As DataTable

            If (OppType = 1) Then
                dtItemTax = objItem.ItemTax()

                For Each dr As DataRow In dtItemTax.Rows
                    strApplicable = strApplicable & dr("bitApplicable") & ","
                Next

                strApplicable = strApplicable & objItem.Taxable
            End If
            'End - Get Tax Detail

            If objItem.ItemCode = lngShippingItemCode Or objItem.ItemCode = CCommon.ToLong(Session("DiscountServiceItem")) Then
                discount = 0
                isDiscountInPer = False
                If objItem.numNoItemIntoContainer > -1 Then
                    objItem.Price = price
                End If
            End If

            objCommon = New CCommon
            objCommon.DomainID = CCommon.ToLong(Session("DomainID"))

            If (Quantity > 1) Then
                units = Quantity
            End If

            If (MultiSelectItemPrice > 0) Then
                price = MultiSelectItemPrice
            End If

            If CCommon.ToString(itemReleaseDate) = "" AndAlso Not String.IsNullOrEmpty(hdnReleaseDate.Value) Then
                itemReleaseDate = Convert.ToDateTime(hdnReleaseDate.Value)
            End If

            If objItem.numNoItemIntoContainer = -1 Then
                ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon,
                                                                               dsTemp,
                                                                               True,
                                                                               objItem.ItemType,
                                                                               "",
                                                                               objItem.AllowDropShip,
                                                                               objItem.KitParent,
                                                                               objItem.ItemCode,
                                                                               units,
                                                                               price,
                                                                               objItem.ItemDesc, objItem.WareHouseItemID, objItem.ItemName, objItem.Warehouse, 0, objItem.ModelID, CCommon.ToLong(objItem.SelectedUOMID), If(CCommon.ToLong(objItem.SelectedUOMID) > 0, objItem.SelectedUOM, "-"),
                                                                               If(CCommon.ToDecimal(0) = 0, 1, CCommon.ToDecimal(0)),
                                                                               _PageType.ToString(), isDiscountInPer, discount, strApplicable, "", txtCustomerPartNo.Text, chkTaxItems,
                                                                               0, txtInstruction.Text, CCommon.ToString(""),
                                                                               CCommon.ToLong(0),
                                                                               0,
                                                                               numShipmentMethod:=0,
                                                                               numProjectID:=CCommon.ToLong(GetQueryStringVal("Source")),
                                                                               numProjectStageID:=CCommon.ToLong(GetQueryStringVal("StageID")),
                                                                               ToWarehouseItemID:=CCommon.ToLong(objItem.StockTransferToWarehouseItemID),
                                                                               vcBaseUOMName:=objItem.SelectedUOM,
                                                                               numSOVendorId:=CCommon.ToLong(0), strSKU:=objItem.SKU,
                                                                               objItem:=objItem, vcAttributes:=objItem.Attributes, vcAttributeIDs:=objItem.AttributeIDs, numContainer:=objItem.numContainer, numContainerQty:=objItem.numNoItemIntoContainer,
                                                                               numItemClassification:=CCommon.ToLong(0),
                                                                               numPromotionID:=CCommon.ToLong(0),
                                                                               IsPromotionTriggered:=CCommon.ToBool(0),
                                                                               vcPromotionDetail:=CCommon.ToString(0),
                                                                               numSortOrder:=objItem.numSortOrder,
                                                                               itemReleaseDate:=If(OppType = 1, If(itemReleaseDate Is Nothing, Convert.ToDateTime(rdpItemDate.SelectedDate), itemReleaseDate), Nothing),
                                                                               bitMarkupDiscount:=ddlMarkupDiscountOption.SelectedValue,
                                                                               itemRequiredDate:=If(OppType = 2, Convert.ToDateTime(rdpItemDate.SelectedDate), Nothing))

            Else
                ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon,
                                                                               dsTemp,
                                                                               True,
                                                                               objItem.ItemType,
                                                                               "",
                                                                               chkDropShip.Checked,
                                                                               objItem.KitParent,
                                                                               objItem.ItemCode,
                                                                               units,
                                                                               price,
                                                                               objItem.ItemDesc, objItem.WareHouseItemID, objItem.ItemName, objItem.Warehouse, 0, objItem.ModelID, CCommon.ToLong(radcmbUOM.SelectedValue), If(CCommon.ToLong(radcmbUOM.SelectedValue) > 0, radcmbUOM.SelectedItem.Text, "-"),
                                                                               If(CCommon.ToDecimal(txtUOMConversionFactor.Text) = 0, 1, CCommon.ToDecimal(txtUOMConversionFactor.Text)),
                                                                               _PageType.ToString(), isDiscountInPer, discount, strApplicable, txtTax.Text, txtCustomerPartNo.Text, chkTaxItems,
                                                                               IIf(chkWorkOrder.Checked, 1, 0), txtInstruction.Text, CCommon.ToString(radCalCompliationDate.SelectedDate),
                                                                               0,
                                                                               0,
                                                                               numShipmentMethod:=0,
                                                                               numProjectID:=CCommon.ToLong(GetQueryStringVal("Source")),
                                                                               numProjectStageID:=CCommon.ToLong(GetQueryStringVal("StageID")),
                                                                               ToWarehouseItemID:=CCommon.ToLong(radWarehouseTo.SelectedValue),
                                                                               vcBaseUOMName:=hdnBaseUOMName.Value,
                                                                               numSOVendorId:=CCommon.ToLong(hdnPrimaryVendorID.Value), strSKU:=objItem.SKU,
                                                                               objItem:=objItem, vcAttributes:=objItem.Attributes, vcAttributeIDs:=objItem.AttributeIDs, numContainer:=objItem.numContainer, numContainerQty:=objItem.numNoItemIntoContainer,
                                                                               numItemClassification:=CCommon.ToLong(hdnItemClassification.Value),
                                                                               numPromotionID:=CCommon.ToLong(hdnPromotionID.Value),
                                                                               IsPromotionTriggered:=CCommon.ToBool(hdnPromotionTriggered.Value),
                                                                               vcPromotionDetail:=CCommon.ToString(hdnPromotionDescription.Value), numSortOrder:=objItem.numSortOrder, numCost:=CCommon.ToDouble(txtPUnitCost.Text), vcVendorNotes:=CCommon.ToString(txtNotes.Text),
                                                                                itemReleaseDate:=If(OppType = 1, If(itemReleaseDate Is Nothing, Convert.ToDateTime(rdpItemDate.SelectedDate), itemReleaseDate), Nothing),
                                                                                bitMarkupDiscount:=ddlMarkupDiscountOption.SelectedValue,
                                                                                bitAlwaysCreateWO:=CCommon.ToBool(hdnIsCreateWO.Value),
                                                                                numWOQty:=CCommon.ToDouble(txtWOQty.Text),
                                                                                dtPlannedStart:=CCommon.ToString(rdpPlannedStart.SelectedDate),
                                                                               itemRequiredDate:=If(OppType = 2, Convert.ToDateTime(rdpItemDate.SelectedDate), Nothing))

            End If


            objCommon = Nothing

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetShippingCharge() As String
        Try
            Return "0.00"
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub ddlPriceLevel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlPriceLevel.SelectedIndexChanged
        Try
            If ddlPriceLevel.SelectedIndex > 0 Then
                CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(ddlPriceLevel.SelectedValue), txtprice)
                hdnUnitCost.Value = CCommon.ToDecimal(txtprice.Text)
                txtItemDiscount.Text = "0"

                lkbPriceLevel.Text = "<i class=""fa fa-check""></i>&nbsp;Used Price Level"
                lkbLastPrice.Text = lkbLastPrice.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "").Replace("Used", "Add")
                lkbPriceRule.Text = lkbPriceRule.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "").Replace("Used", "Add")
                lkbListPrice.Text = lkbListPrice.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "").Replace("Used", "Add")

                CalculateProfit()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

    Sub BindPriceLevel(ByVal itemCode As Long)
        Try
            If OppType = 1 Then
                Dim dtItemsPriceLevel As New DataTable
                Dim dtItemsPriceRule As New DataTable

                objItems = New CItems
                objItems.ItemCode = itemCode
                objItems.DomainID = Session("DomainID")

                If Not radWareHouse.SelectedValue = "" Then
                    objItems.WareHouseItemID = radWareHouse.SelectedValue
                Else
                    objItems.WareHouseItemID = Nothing
                End If

                If (_PageType = PageType.Sales Or _PageType = PageType.Purchase Or _PageType = PageType.AddOppertunity) Then
                    If IsStockTransfer Then
                        objItems.DivisionID = IIf(CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue)
                    Else
                        objItems.DivisionID = IIf(CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue)
                    End If
                Else
                    objItems.DivisionID = CCommon.ToLong(GetQueryStringVal("DivId"))
                End If

                Dim dsPriceLevel As DataSet = objItems.GetPriceLevelItemPrice()
                dtItemsPriceLevel = dsPriceLevel.Tables(0)

                ddlPriceLevel.Items.Clear()
                ddlPriceLevel.Items.Insert(0, New ListItem("--Select One--", "0"))

                spnPriceLevel.Visible = False

                Dim Litem As ListItem

                If dtItemsPriceLevel.Rows.Count > 0 Then
                    For Each dr As DataRow In dtItemsPriceLevel.Rows
                        Litem = New ListItem()

                        If CCommon.ToInteger(dr("intFromQty")) = 0 AndAlso CCommon.ToInteger(dr("intToQty")) = 0 AndAlso Not String.IsNullOrEmpty(dr("vcName")) Then
                            Litem.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(dr("decDiscount"))) & " - " & CCommon.ToString(dr("vcName"))
                        Else
                            Litem.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(dr("decDiscount"))) & " (" & CCommon.ToLong(dr("intFromQty")) & "-" & CCommon.ToLong(dr("intToQty")) & ")"
                        End If
                        Litem.Value = dr("decDiscount")
                        Litem.Attributes("OptionGroup") = "Price Table"

                        If hdnEditUnitPriceRight.Value = "False" Then
                            If CCommon.ToLong(txtUnits.Text) >= CCommon.ToLong(dr("intFromQty")) AndAlso CCommon.ToLong(txtUnits.Text) <= CCommon.ToLong(dr("intToQty")) Then
                                Litem.Enabled = True
                            Else
                                Litem.Enabled = False
                            End If
                        End If

                        ddlPriceLevel.Items.Add(Litem)
                    Next
                    spnPriceLevel.Visible = True
                End If

                dtItemsPriceRule = dsPriceLevel.Tables(1)

                If dtItemsPriceRule.Rows.Count > 0 Then
                    For Each dr As DataRow In dtItemsPriceRule.Rows
                        Litem = New ListItem()
                        Litem.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(dr("decDiscount"))) & " (" & CCommon.ToLong(dr("intFromQty")) & "-" & CCommon.ToLong(dr("intToQty")) & ")"
                        Litem.Value = dr("decDiscount")
                        Litem.Attributes("OptionGroup") = "Price Rule"

                        If hdnEditUnitPriceRight.Value = "False" Then
                            If CCommon.ToLong(txtUnits.Text) >= CCommon.ToLong(dr("intFromQty")) AndAlso CCommon.ToLong(txtUnits.Text) <= CCommon.ToLong(dr("intToQty")) Then
                                Litem.Enabled = True
                            Else
                                Litem.Enabled = False
                            End If
                        End If

                        ddlPriceLevel.Items.Add(Litem)
                    Next
                    spnPriceLevel.Visible = True
                End If

                ddlPriceLevel.ClearSelection()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As String)
        Dim lbl As Label = Me.Page.Master.FindControl("FiltersAndViews1").FindControl("lblException")

        If Not lbl Is Nothing Then
            lbl.Text = ex
            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ScrollTo", "ScrollToError();", True)
        End If
    End Sub

    Protected Sub txtUnitsGrid_TextChanged(sender As Object, e As EventArgs)
        Try
            Dim dr As DataRow
            Dim dtItem As DataTable
            Dim dtSerItem As DataTable
            Dim txtUnits As TextBox = CType(sender, TextBox)

            dtItem = dsTemp.Tables(0)
            dtSerItem = dsTemp.Tables(1)

            Dim dRows As DataRow() = dtItem.Select("numoppitemtCode = " & CCommon.ToLong(hdnOppItemCodeToUpdatePrice.Value).ToString())
            If dRows.Length > 0 Then
                dr = dRows(0)

                Dim price As Decimal = CCommon.ToDecimal(dr("monPrice"))
                Dim discount As Decimal = CCommon.ToDecimal(dr("fltDiscount"))
                Dim salePrice As Decimal = price


                dr("monTotAmtBefDiscount") = ((CCommon.ToInteger(txtUnits.Text) * CCommon.ToDecimal(dr("UOMConversionFactor"))) * dr("monPrice"))

                If CCommon.ToString(dr("bitDiscountType")) = "1" Then
                    salePrice = price - (price * (discount / 100))
                Else
                    If CCommon.ToInteger(txtUnits.Text) > 0 Then
                        salePrice = ((price * (CCommon.ToInteger(txtUnits.Text) * dr("UOMConversionFactor"))) - discount) / (CCommon.ToInteger(txtUnits.Text) * dr("UOMConversionFactor"))
                    Else
                        salePrice = 0
                    End If
                End If

                dr("monTotAmount") = (dr("numUnitHour") * dr("UOMConversionFactor")) * salePrice

                If dtItem.Columns.Contains("TotalDiscountAmount") Then
                    If Not CCommon.ToString(dr("bitDiscountType")) = "1" Then
                        dr("TotalDiscountAmount") = dr("fltDiscount")
                    Else
                        If dr("monTotAmount") > dr("monTotAmtBefDiscount") Then
                            dr("TotalDiscountAmount") = 0
                        Else
                            dr("TotalDiscountAmount") = dr("monTotAmtBefDiscount") - dr("monTotAmount")
                        End If
                    End If
                End If


                dr.AcceptChanges()
                dsTemp.AcceptChanges()
                UpdateDetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        Finally
            hdnOppItemCodeToUpdatePrice.Value = ""
            hdnItemQuantity.Value = ""
        End Try
    End Sub

    Protected Sub txtRowOrder_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim dr As DataRow
            Dim dtItem As DataTable
            Dim dtSerItem As DataTable
            Dim txtRowOrder As TextBox = CType(sender, TextBox)
            Dim row As DataGridItem = TryCast(sender.NamingContainer, DataGridItem)
            dtItem = dsTemp.Tables(0)
            dtSerItem = dsTemp.Tables(1)
            For Each dgGridItem As DataGridItem In dgItems.Items
                Dim dRows As DataRow() = dtItem.Select("numoppitemtCode = " & CCommon.ToLong(CType(dgGridItem.FindControl("lblOppItemCode"), Label).Text).ToString())
                txtRowOrder = CType(dgGridItem.FindControl("txtRowOrder"), TextBox)
                If dRows.Length > 0 Then
                    dr = dRows(0)
                    dr("numSortOrder") = txtRowOrder.Text
                    dr.AcceptChanges()
                    dsTemp.AcceptChanges()

                End If
            Next
            UpdateDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnSaveCloseKit_Click(sender As Object, e As EventArgs) Handles btnSaveCloseKit.Click
        Try
            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "EnableScroll", "Scroll(true); $('[id$=divKitChild]').hide();", True)

            LoadSelectedItemDetail(hdnCurrentSelectedItem.Value)
            If OppType = 1 Then
                RelatedItems(True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub LoadSelectedItemDetail(ByVal itemCode As Long, Optional ByVal LoadAttributes As Boolean = True)
        Try
            If OppType = 2 Then
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "ShowPODistance", "$('#imgPODistance').show(); $('#imgPODistance').attr(""onclick"",""OpenItemVendors(" & hdnCurrentSelectedItem.Value & "," & CCommon.ToLong(hdnShipAddressID.Value) & ")"")", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "ShowPODistance", "$('#imgPODistance').hide();", True)
                LoadMarkupDiscount()   'Markup N Discount logic from Global settings
            End If

            Dim oppId As Long = CCommon.ToLong(GetQueryStringVal("opid"))
            divPromotionDesription.Visible = True
            If IsStockTransfer Then
                If oppId = 0 AndAlso (CCommon.ToLong(DirectCast(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue) = 0 Or CCommon.ToLong(DirectCast(Me.Parent.FindControl("radcmbSTTo"), RadComboBox).SelectedValue) = 0) Then
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "CustomerValidation", "$('#txtItem').select2(""data"", """"); alert(""Select From and To customer."")", True)
                    Return
                ElseIf oppId = 0 AndAlso (CCommon.ToLong(DirectCast(Me.Parent.FindControl("ddlFromContact"), DropDownList).SelectedValue) = 0 Or CCommon.ToLong(DirectCast(Me.Parent.FindControl("ddlToContact"), DropDownList).SelectedValue) = 0) Then
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "CustomerValidation", "$('#txtItem').select2(""data"", """"); alert(""Select From and To contact."")", True)
                    Return
                End If
            Else
                If oppId = 0 AndAlso Me.Parent.FindControl("trNewCust") Is Nothing Then
                    If CCommon.ToLong(DirectCast(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue) = 0 AndAlso OppType <> 2 Then
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "CustomerValidation", "$('#txtItem').select2(""data"", """"); alert(""Select customer."")", True)
                        Return
                    ElseIf CCommon.ToLong(DirectCast(Me.Parent.FindControl("ddlContact"), DropDownList).SelectedValue) = 0 AndAlso OppType <> 2 Then
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "CustomerValidation", "$('#txtItem').select2(""data"", """"); alert(""Select contact."")", True)
                        Return
                    End If
                Else
                    If oppId = 0 AndAlso Not CType(Me.Parent.FindControl("trNewCust"), HtmlGenericControl).Visible AndAlso CCommon.ToLong(DirectCast(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue) = 0 AndAlso OppType <> 2 Then '
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "CustomerValidation", "$('#txtItem').select2(""data"", """"); alert(""Select customer."")", True)
                        Return
                    ElseIf oppId = 0 AndAlso Not CType(Me.Parent.FindControl("trNewCust"), HtmlGenericControl).Visible AndAlso CCommon.ToLong(DirectCast(Me.Parent.FindControl("ddlContact"), DropDownList).SelectedValue) = 0 AndAlso OppType <> 2 Then '
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "CustomerValidation", "$('#txtItem').select2(""data"", """"); alert(""Select contact."")", True)
                        Return
                    End If
                End If
            End If



            Dim objItem As New CItems
            If Not objItem.ValidateItemAccount(CCommon.ToLong(itemCode), Session("DomainID"), 1) Then
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "CustomerValidation", "$('#txtItem').select2(""data"", """"); alert(""Item Income,Asset,COGs(Expense) Account are not set. To set Income,Asset,COGs(Expense) Account go to Administration->Inventory->Item Details."")", True)
                Return
            End If

            If String.IsNullOrEmpty(txtUnits.Text) Then
                txtUnits.Text = "1"
            End If

            Dim objOpportunity As New COpportunities
            objOpportunity.DomainID = Session("DomainID")
            If Not _PageType = PageType.AddEditOrder Then
                If IsStockTransfer Then
                    objOpportunity.DivisionID = IIf(CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue)
                Else
                    objOpportunity.DivisionID = IIf(CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue)
                End If
            Else
                objOpportunity.DivisionID = CCommon.ToLong(GetQueryStringVal("DivId"))
            End If
            objOpportunity.OppType = OppType
            objOpportunity.ItemCode = itemCode
            objOpportunity.UnitHour = txtUnits.Text

            If Not DirectCast(Me.Parent.FindControl("hdnShipToWarehouseID"), HiddenField) Is Nothing AndAlso CCommon.ToLong(DirectCast(Me.Parent.FindControl("hdnShipToWarehouseID"), HiddenField).Value) > 0 Then
                objOpportunity.WarehouseID = CCommon.ToLong(DirectCast(Me.Parent.FindControl("hdnShipToWarehouseID"), HiddenField).Value)
            Else
                If OppType = 1 And CCommon.ToLong(hdnShipFromLocation.Value) > 0 Then
                    objOpportunity.WarehouseID = CCommon.ToLong(hdnShipFromLocation.Value)
                ElseIf CCommon.ToLong(hdnDefaultUserExternalLocation.Value) > 0 Then
                    objOpportunity.WarehouseID = CCommon.ToLong(hdnDefaultUserExternalLocation.Value)
                Else
                    objOpportunity.WarehouseItmsID = CCommon.ToLong(radWareHouse.SelectedValue)
                End If
            End If
            If (_PageType = PageType.AddEditOrder) Then
                objOpportunity.CurrencyID = CCommon.ToLong(CType(Me.Parent.FindControl("hdnCurrencyID"), HiddenField).Value)
            Else
                objOpportunity.CurrencyID = CCommon.ToLong(CType(Me.Parent.FindControl("ddlCurrency"), DropDownList).SelectedValue)
            End If
            objOpportunity.vcCoupon = txtCouponCode.Text

            Dim ds As DataSet = objOpportunity.GetOrderItemDetails(CCommon.ToLong(radcmbUOM.SelectedValue), hdnKitChildItems.Value, CCommon.ToLong(hdnCountry.Value))

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                If (OppType = 2) Then
                    hplPrice.Text = "Unit Cost"
                    hplPrice.Attributes.Add("onclick", "return openVendorCostTable('2'," & itemCode & "," & objOpportunity.DivisionID & ")")
                ElseIf (OppType = 1) Then
                    hplUnitCost.Attributes.Add("onclick", "return openVendorCostTable('1'," & itemCode & "," & objOpportunity.DivisionID & ")")
                    hplPrice.Text = "Unit Price"
                End If

                If CCommon.ToString(ds.Tables(0).Rows(0)("dtDynamicCostModifiedDate")) <> "" Then
                    lblDynamicCostUpdatedDate.Text = "Updated: " & FormattedDateFromDate(Convert.ToDateTime(ds.Tables(0).Rows(0)("dtDynamicCostModifiedDate")).AddMinutes(CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")) * -1).ToShortDateString(), Session("DateFormat"))
                End If

                If CCommon.ToLong(txtHidEditOppItem.Text) = 0 Then
                    ddltype.SelectedValue = CCommon.ToString(ds.Tables(0).Rows(0)("ItemType"))
                    If Not ddltype.SelectedItem Is Nothing Then
                        lblItemType.Text = ddltype.SelectedItem.Text
                    End If

                    If ddltype.SelectedValue = "P" Then
                        If objCommon Is Nothing Then objCommon = New CCommon
                        Dim dtWarehouse As DataTable = objCommon.GetWarehousesForSelectedItem(itemCode, 0, 1, OrderSource, SourceType, CCommon.ToLong(hdnShipToCountry.Value), CCommon.ToLong(hdnShipToState.Value), hdnKitChildItems.Value)

                        If _IsStockTransfer Then
                            radWareHouse.DataSource = dtWarehouse
                            radWareHouse.DataTextField = "vcWareHouse"
                            radWareHouse.DataValueField = "numWareHouseItemId"
                            radWareHouse.DataBind()

                            radWarehouseTo.DataSource = dtWarehouse
                            radWarehouseTo.DataBind()
                        Else
                            If CCommon.ToBool(hdnIsAutoWarehouseSelection.Value) AndAlso hdnWarehousePriority.Value <> "-1" Then
                                radWareHouse.DataSource = dtWarehouse
                                radWareHouse.DataTextField = "vcWareHouse"
                                radWareHouse.DataValueField = "numWareHouseItemId"
                                radWareHouse.DataBind()
                            Else
                                If Not DirectCast(Me.Parent.FindControl("hdnShipToWarehouseID"), HiddenField) Is Nothing AndAlso CCommon.ToLong(DirectCast(Me.Parent.FindControl("hdnShipToWarehouseID"), HiddenField).Value) > 0 Then
                                    If Not dtWarehouse Is Nothing AndAlso dtWarehouse.Select("numWarehouseID=" & CCommon.ToLong(DirectCast(Me.Parent.FindControl("hdnShipToWarehouseID"), HiddenField).Value)).Length > 0 Then
                                        radWareHouse.DataSource = dtWarehouse.Select("numWarehouseID=" & CCommon.ToLong(DirectCast(Me.Parent.FindControl("hdnShipToWarehouseID"), HiddenField).Value)).CopyToDataTable()
                                        radWareHouse.DataTextField = "vcWareHouse"
                                        radWareHouse.DataValueField = "numWareHouseItemId"
                                        radWareHouse.DataBind()
                                    Else
                                        radWareHouse.DataSource = dtWarehouse
                                        radWareHouse.DataTextField = "vcWareHouse"
                                        radWareHouse.DataValueField = "numWareHouseItemId"
                                        radWareHouse.DataBind()
                                    End If
                                Else
                                    radWareHouse.DataSource = dtWarehouse
                                    radWareHouse.DataTextField = "vcWareHouse"
                                    radWareHouse.DataValueField = "numWareHouseItemId"
                                    radWareHouse.DataBind()
                                End If
                            End If
                        End If

                        If CCommon.ToBool(hdnIsAutoWarehouseSelection.Value) AndAlso hdnWarehousePriority.Value <> "-1" Then
                            If radWareHouse.Items.Count > 0 Then
                                radWareHouse.Items(0).Selected = True
                                UpdateInventoryGrid()
                            End If
                        Else
                            If Not radWareHouse.FindItemByValue(CCommon.ToString(ds.Tables(0).Rows(0)("numWarehouseItemID"))) Is Nothing Then
                                radWareHouse.FindItemByValue(CCommon.ToString(ds.Tables(0).Rows(0)("numWarehouseItemID"))).Selected = True
                                UpdateInventoryGrid()
                            End If
                        End If



                        divWarehouse.Visible = True
                        SetDropshipCheckbox(CCommon.ToBool(ds.Tables(0).Rows(0)("bitDropship")), True)

                        If chkDropShip.Checked Then
                            divWarehouse.Style.Add("display", "none")
                        End If
                    Else
                        SetDropshipCheckbox(False, False)
                        divWarehouse.Visible = False
                        radWareHouse.SelectedValue = ""
                    End If

                    divUnitCost.Visible = True
                    divProfit.Visible = True

                    txtdesc.Text = CCommon.ToString(ds.Tables(0).Rows(0)("ItemDescription"))
                    If OppType = 2 Then
                        lblMinOrderQty.Text = CCommon.ToString(ds.Tables(0).Rows(0).Item("vcMinOrderQty"))
                        If CCommon.ToDecimal(ds.Tables(0).Rows(0).Item("vcMinOrderQty")) > 0 Then
                            divMinOrderQty.Visible = True
                        Else
                            divMinOrderQty.Visible = False
                        End If
                        txtNotes.Text = CCommon.ToString(ds.Tables(0).Rows(0).Item("vcVendorNotes"))
                    End If
                    hdnListPrice.Value = CCommon.ToDecimal(ds.Tables(0).Rows(0)("monListPrice"))
                    hdnLastPrice.Value = CCommon.ToDecimal(ds.Tables(0).Rows(0)("monLastPrice"))
                    hdnVendorCost.Value = CCommon.ToDecimal(ds.Tables(0).Rows(0)("VendorCost"))
                    hdnPrimaryVendorID.Value = CCommon.ToLong(ds.Tables(0).Rows(0)("numVendorID"))
                    lblBaseUOMName.Text = CCommon.ToString(ds.Tables(0).Rows(0)("vcBaseUOMName"))
                    hdnBaseUOMName.Value = CCommon.ToString(ds.Tables(0).Rows(0)("vcBaseUOMName"))
                    hdnItemClassification.Value = CCommon.ToLong(ds.Tables(0).Rows(0)("ItemClassification"))
                    hdnIsMatrix.Value = CCommon.ToBool(ds.Tables(0).Rows(0)("bitMatrix"))

                    If CCommon.ToBool(hdnIsSKUMatch.Value) AndAlso CCommon.ToBool(hdnIsMatrix.Value) Then
                        LoadAttributes = False
                    End If
                    txtUOMConversionFactor.Text = CCommon.ToString(ds.Tables(0).Rows(0)("UOMConversionFactor"))
                    If CCommon.ToBool(ds.Tables(0).Rows(0)("bitAssembly")) Then
                        If CCommon.ToBool(ds.Tables(0).Rows(0).Item("bitSOWorkOrder")) Then
                            chkWorkOrder.Checked = True
                            hdnIsCreateWO.Value = "1"
                        Else
                            hdnIsCreateWO.Value = "0"
                        End If

                        divWorkOrder.Visible = True
                        lblWOQty.Text = CCommon.ToString(ds.Tables(0).Rows(0)("MaxWorkOrderQty"))
                    End If

                    If OppType = 1 AndAlso Not radcmbUOM.Items.FindItemByValue(CCommon.ToString(ds.Tables(0).Rows(0)("numSaleUnit"))) Is Nothing Then
                        radcmbUOM.Items.FindItemByValue(CCommon.ToString(ds.Tables(0).Rows(0)("numSaleUnit"))).Selected = True
                    ElseIf OppType = 2 AndAlso Not radcmbUOM.Items.FindItemByValue(CCommon.ToString(ds.Tables(0).Rows(0)("numPurchaseUnit"))) Is Nothing Then
                        radcmbUOM.Items.FindItemByValue(CCommon.ToString(ds.Tables(0).Rows(0)("numPurchaseUnit"))).Selected = True
                    End If

                    BindPriceLevel(itemCode)

                    If OppType = 1 AndAlso Not DirectCast(ViewState("UsedPromotion"), DataTable) Is Nothing AndAlso
                        Not DirectCast(ViewState("UsedPromotion"), DataTable).AsEnumerable().OrderBy(Function(x) x("tintPriority")).FirstOrDefault(Function(x) CCommon.ToString(x("vcItems")).Split(",").Contains(itemCode) Or CCommon.ToString(x("vcItems")).Split(",").Contains(CCommon.ToLong(hdnItemClassification.Value))) Is Nothing Then

                        Dim drPromotion As DataRow = DirectCast(ViewState("UsedPromotion"), DataTable).AsEnumerable().OrderBy(Function(x) x("tintPriority")).FirstOrDefault(Function(x) CCommon.ToString(x("vcItems")).Split(",").Contains(itemCode) Or CCommon.ToString(x("vcItems")).Split(",").Contains(CCommon.ToLong(hdnItemClassification.Value)))

                        Dim canApplyPromotion As Boolean = True

                        Dim remainingQty As Int32 = 0
                        Dim remainingAmt As Decimal = 0
                        If drPromotion("tintDiscountType") = "3" Then 'Free Quantity
                            Dim usedQuantity As Int32 = 0
                            Dim foundRows As DataRow() = dsTemp.Tables(0).Select("numPromotionID=" & drPromotion("numProID"))

                            For Each dr As DataRow In foundRows
                                If CCommon.ToInteger(dr("TotalDiscountAmount")) > 0 Then
                                    usedQuantity += CCommon.ToInteger(dr("TotalDiscountAmount") / dr("monPrice"))
                                End If
                            Next

                            If usedQuantity >= drPromotion("fltDiscountValue") Then
                                canApplyPromotion = False
                            Else
                                remainingQty = CCommon.ToInteger(drPromotion("fltDiscountValue")) - usedQuantity
                            End If
                        ElseIf drPromotion("tintDiscountType") = 2 Then 'Flat Amount
                            Dim usedDiscountAmt As Int32 = 0
                            usedDiscountAmt = dsTemp.Tables(0).Select("numPromotionID=" & drPromotion("numProID")).Sum(Function(x) x("TotalDiscountAmount"))
                            remainingAmt = CCommon.ToInteger(drPromotion("fltDiscountValue")) - usedDiscountAmt

                            If usedDiscountAmt >= CCommon.ToInteger(drPromotion("fltDiscountValue")) Then
                                canApplyPromotion = False
                            End If
                        End If

                        If canApplyPromotion Then
                            CCommon.SetValueInDecimalFormat(hdnListPrice.Value, txtprice)
                            hdnUnitCost.Value = CCommon.ToDecimal(txtprice.Text)

                            If drPromotion("tintDiscountType") = "1" Then
                                CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(drPromotion("fltDiscountValue")), txtItemDiscount)
                                radPer.Checked = True
                                radAmt.Checked = False
                            ElseIf drPromotion("tintDiscountType") = "2" Then
                                CCommon.SetValueInDecimalFormat(remainingAmt, txtItemDiscount)
                                radAmt.Checked = True
                                radPer.Checked = False
                            ElseIf drPromotion("tintDiscountType") = "3" Then
                                txtUnits.Text = remainingQty
                                If Not radcmbUOM.Items.FindItemByValue(CCommon.ToString(ds.Tables(0).Rows(0)("numBaseUnit"))) Is Nothing Then
                                    radcmbUOM.Items.FindItemByValue(CCommon.ToString(ds.Tables(0).Rows(0)("numBaseUnit"))).Selected = True
                                End If
                                CCommon.SetValueInDecimalFormat(remainingQty * hdnListPrice.Value, txtItemDiscount)
                                radAmt.Checked = True
                                radPer.Checked = False
                            End If

                            hdnPromotionID.Value = drPromotion("numProId")
                            hdnPromotionTriggered.Value = False
                            hdnPromotionDescription.Value = drPromotion("vcPromotionDescription")
                            hdnPromotionName.Value = drPromotion("vcProName")
                            hdnCouponNotRequire.Value = "1"
                            lkbAddPromotion.Text = "Remove Applied Promotion"
                            lkbAddPromotion.Visible = True

                            lkbPriceLevel.Visible = False
                            lkbPriceRule.Visible = False
                            lkbLastPrice.Visible = False
                            lkbListPrice.Visible = False

                            ddlPriceLevel.Enabled = False
                            txtprice.Enabled = False
                            txtItemDiscount.Enabled = False
                            radPer.Enabled = False
                            radAmt.Enabled = False
                        Else
                            CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(ds.Tables(0).Rows(0)("monPrice")), txtprice)
                            hdnUnitCost.Value = CCommon.ToDecimal(txtprice.Text)
                            CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(ds.Tables(0).Rows(0)("Discount")), txtItemDiscount)

                            If CCommon.ToString(ds.Tables(0).Rows(0)("DiscountType")) = "1" Then
                                radPer.Checked = True
                                radAmt.Checked = False
                            Else
                                radAmt.Checked = True
                                radPer.Checked = False
                            End If
                            SetPricingOption(ds.Tables(0).Rows(0))
                        End If
                    Else
                        CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(ds.Tables(0).Rows(0)("monPrice")), txtprice)
                        hdnUnitCost.Value = CCommon.ToDecimal(txtprice.Text)
                        CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(ds.Tables(0).Rows(0)("Discount")), txtItemDiscount)

                        If CCommon.ToDouble(ds.Tables(0).Rows(0)("Discount")) <> 0 Then
                            If CCommon.ToString(ds.Tables(0).Rows(0)("DiscountType")) = "1" Then
                                radPer.Checked = True
                                radAmt.Checked = False
                            Else
                                radAmt.Checked = True
                                radPer.Checked = False
                            End If
                        End If

                        If CCommon.ToBool(ds.Tables(0).Rows(0)("bitMarkupDiscount")) Then
                            ddlMarkupDiscountOption.SelectedValue = "1"
                        Else
                            ddlMarkupDiscountOption.SelectedValue = "0"
                        End If

                        If OppType = 1 Then
                            SetPricingOption(ds.Tables(0).Rows(0))
                        End If
                    End If

                    CalculateProfit()


                    If LoadAttributes And ((ds.Tables(0).Rows(0)("ItemType") <> "P" AndAlso ds.Tables(0).Rows(0)("ItemGroup") > 0) Or (ds.Tables(0).Rows(0)("ItemGroup") > 0 And CCommon.ToBool(ds.Tables(0).Rows(0)("bitMatrix")))) Then
                        LoadItemAttributes(itemCode)
                    End If

                    If OppType = 1 Then
                        If CCommon.ToBool(ds.Tables(0).Rows(0)("bitKitParent")) And Not CCommon.ToBool(ds.Tables(0).Rows(0)("bitHasChildKits")) Then
                            lkbEditKitSelection.Visible = True

                            Dim objTempItem As New CItems
                            objTempItem.ItemCode = itemCode
                            objTempItem.ChildItemID = 0
                            Dim dsItem As DataSet = objTempItem.GetSelectedChildItemDetails("", False)

                            If Not dsItem Is Nothing And dsItem.Tables.Count > 1 Then
                                gvChildItems.DataSource = dsItem.Tables(1)
                                gvChildItems.DataBind()
                            Else
                                gvChildItems.DataSource = Nothing
                                gvChildItems.DataBind()
                            End If

                            UpdatePanel3.Update()

                            If CCommon.ToBool(ds.Tables(0).Rows(0)("bitCalAmtBasedonDepItems")) Then
                                divKitPrice.Visible = True
                                hdnKitPricing.Value = CCommon.ToShort(ds.Tables(0).Rows(0)("tintKitAssemblyPriceBasedOn"))
                                hdnKitListPrice.Value = CCommon.ToDouble(ds.Tables(0).Rows(0)("monListPrice"))

                                If CCommon.ToShort(ds.Tables(0).Rows(0)("tintKitAssemblyPriceBasedOn")) = 4 Then
                                    lblKitPriceType.Text = "Kit/Assembly list price plus sum total of child item's list price"
                                ElseIf CCommon.ToShort(ds.Tables(0).Rows(0)("tintKitAssemblyPriceBasedOn")) = 3 Then
                                    lblKitPriceType.Text = "Sum total of child item's Primary Vendor Cost"
                                ElseIf CCommon.ToShort(ds.Tables(0).Rows(0)("tintKitAssemblyPriceBasedOn")) = 2 Then
                                    lblKitPriceType.Text = "Sum total of child item's Average Cost"
                                Else
                                    lblKitPriceType.Text = "Sum total of child item's List Price"
                                End If
                            End If
                        Else
                            lkbEditKitSelection.Visible = False
                        End If
                    End If
                Else
                    If OppType = 1 Then
                        SetPricingOption(ds.Tables(0).Rows(0))
                    End If
                End If

                If OppType = 1 AndAlso ds.Tables.Count > 1 AndAlso dsTemp.Tables(0).Select("numItemCode=" & hdnCurrentSelectedItem.Value & " AND bitPromotionTriggered=TRUE AND numPromotionID=" & ds.Tables(1).Rows(0)("numProId")).Length = 0 Then
                    ViewState("DataTableTempPromotion") = ds.Tables(1)

                    lkbAddPromotion.Visible = True
                    divPromotionDetail.Visible = True
                    lblPromotionName.Text = CCommon.ToString(ds.Tables(1).Rows(0)("vcProName"))
                    lblPromotionDescription.Text = CCommon.ToString(ds.Tables(1).Rows(0)("vcPromotionDescription"))

                    If Not CCommon.ToBool(ds.Tables(1).Rows(0)("bitNeverExpires")) Then
                        lblExpiration.Text = " (Expires on" & ds.Tables(1).Rows(0)("dtExpire") & ")"
                    Else
                        lblExpiration.Text = ""
                    End If

                    If CCommon.ToBool(ds.Tables(1).Rows(0)("bitRequireCouponCode") AndAlso hdnCouponNotRequire.Value <> "1") Then
                        If (Convert.ToString(txtCouponCode.Text) <> ds.Tables(1).Rows(0)("txtCouponCode")) Then
                            divPromotionDesription.Visible = False
                        End If
                        divCouponCode.Visible = True
                    ElseIf hdnCouponNotRequire.Value = "1" Then
                        lblPromotionDescription.Text = hdnPromotionDescription.Value
                        lblPromotionName.Text = hdnPromotionName.Value
                    End If

                    hdnCouponNotRequire.Value = "0"
                    If Not ViewState("UsedPromotion") Is Nothing AndAlso Not DirectCast(ViewState("UsedPromotion"), DataTable).Select("vcShippingDescription <> ''").FirstOrDefault() Is Nothing Then
                        Dim dr As DataRow

                        For Each drPromotion As DataRow In DirectCast(ViewState("UsedPromotion"), DataTable).Rows
                            If Not String.IsNullOrEmpty(drPromotion("vcShippingDescription")) Then
                                dr = drPromotion
                                Exit For
                            End If
                        Next

                        SetShippingDetail(dr)
                    ElseIf Not String.IsNullOrEmpty(ds.Tables(1).Rows(0)("vcShippingDescription")) Then
                        Dim dr As DataRow = ds.Tables(1).Rows(0)
                        SetShippingDetail(dr)
                    End If
                Else
                    If Not ViewState("UsedPromotion") Is Nothing AndAlso Not DirectCast(ViewState("UsedPromotion"), DataTable).Select("vcShippingDescription <> ''").FirstOrDefault() Is Nothing Then
                        Dim dr As DataRow

                        For Each drPromotion As DataRow In DirectCast(ViewState("UsedPromotion"), DataTable).Rows
                            If Not String.IsNullOrEmpty(drPromotion("vcShippingDescription")) Then
                                dr = drPromotion
                                Exit For
                            End If
                        Next

                        SetShippingDetail(dr)
                    End If
                End If

                If OppType = 1 Then
                    divPricingOption.Visible = True
                End If

                If OppType = 1 Then
                    hplRelatedItems.Text = "Related Items(" & CCommon.ToLong(ds.Tables(0).Rows(0)("RelatedItemsCount")) & ")"
                    hplRelatedItems.Visible = True
                End If

                txtUnits.Text = ""
                txtprice.Text = ""
                txtItemDiscount.Text = ""
            End If

            ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "FocusAddButton", "FocusUnits();", True)
        Catch ex As Exception
            If ex.Message.Contains("WAREHOUSE_DOES_NOT_EXISTS") Then
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "CustomerValidation", "$('#txtItem').select2(""data"", """"); alert(""No warehouse available for selected item."")", True)
            Else
                Throw
            End If
        End Try
    End Sub

    Private Sub SetShippingDetail(ByVal dr As DataRow)
        Try
            Dim subTotal As Long = CCommon.ToLong(dsTemp.Tables(0).Compute("SUM(monTotAmtBefDiscount)", ""))
            lblShippingDetail.Text = ""
            divPromotionShippingDetail.Visible = True

            If CCommon.ToBool(dr("bitFreeShiping")) AndAlso CCommon.ToLong(dr("numFreeShippingCountry")) = hdnCountry.Value AndAlso subTotal >= CCommon.ToDecimal(dr("monFreeShippingOrderAmount")) Then
                lblShippingDetail.Text = "You’ve qualified for FREE shipping."
            Else
                Dim remainingAmount As Long

                If CCommon.ToLong(dr("numFreeShippingCountry")) = hdnCountry.Value Then
                    remainingAmount = dr("monFreeShippingOrderAmount") - subTotal
                    lblShippingDetail.Text = "Spend $" & remainingAmount & If(subTotal > 0, " more", "") & " and get shipping FREE!"
                End If

                If CCommon.ToBool(dr("bitFixShipping2")) AndAlso subTotal >= CCommon.ToDecimal(dr("monFixShipping2OrderAmount")) Then
                    lblShippingDetail.Text = "You’ve qualified for $" & CCommon.ToInteger(dr("monFixShipping2Charge")) & " shipping. " & lblShippingDetail.Text
                Else
                    remainingAmount = dr("monFixShipping2OrderAmount") - subTotal
                    lblShippingDetail.Text = "Spend $" & remainingAmount & If(subTotal > 0, " more", "") & " and get $" & CCommon.ToInteger(dr("monFixShipping2Charge")) & " shipping. " & lblShippingDetail.Text

                    If CCommon.ToBool(dr("bitFixShipping1")) AndAlso subTotal >= CCommon.ToDecimal(dr("monFixShipping1OrderAmount")) Then
                        lblShippingDetail.Text = "You’ve qualified for $" & CCommon.ToInteger(dr("monFixShipping1Charge")) & " shipping. " & lblShippingDetail.Text
                    Else
                        remainingAmount = dr("monFixShipping1OrderAmount") - subTotal
                        lblShippingDetail.Text = "Spend $" & remainingAmount & If(subTotal > 0, " more", "") & " and get $" & CCommon.ToInteger(dr("monFixShipping1Charge")) & " shipping. " & lblShippingDetail.Text
                    End If
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub SetPricingOption(ByVal dr As DataRow)
        Try
            hdnLastPrice.Value = CCommon.ToDecimal(dr("monLastPrice"))
            lkbListPrice.Text = "Use List Price (" & CCommon.ToString(dr("monListPrice")) & ")"
            lkbLastPrice.Text = "Use Last Price (" & CCommon.ToString(dr("monLastPrice")) & "," & CCommon.ToString(dr("LastOrderDate")) & ")"
            hdnLastOrderDate.Value = CCommon.ToString(dr("LastOrderDate"))
            hplLastPrices.Visible = True

            If dr("tintPriceBaseOn") = "1" Then 'Price Level
                hdnPricingBasedOn.Value = "1"
                lkbPriceLevel.Text = "<i class=""fa fa-check""></i>&nbsp; Used Price Level"

                If dr("tintRuleType") = 3 Then
                    If ddlPriceLevel.Items.Count >= CCommon.ToLong(dr("tintPriceLevel")) Then
                        ddlPriceLevel.Items(CCommon.ToLong(dr("tintPriceLevel"))).Selected = True
                    End If
                End If
            ElseIf dr("tintPriceBaseOn") = "2" Then 'Price Rule
                hdnPricingBasedOn.Value = "2"
                lkbPriceRule.Text = "<i class=""fa fa-check""></i>&nbsp; Used Price Rule"
            ElseIf dr("tintPriceBaseOn") = "3" Then 'Last Price
                hdnPricingBasedOn.Value = "3"
                lkbListPrice.Text = "<i class=""fa fa-check""></i> &nbsp; Used List Price (" & CCommon.ToString(dr("monLastPrice")) & "," & CCommon.ToString(dr("LastOrderDate")) & ")"
            ElseIf dr("tintPriceBaseOn") = "4" Then 'Calculated Based On Dependent Item
                hdnPricingBasedOn.Value = "4"
            Else
                lkbListPrice.Text = "<i class=""fa fa-check""></i> &nbsp; Used List Price (" & CCommon.ToString(dr("monListPrice")) & ")"
            End If

            If CCommon.ToLong(hdnPromotionID.Value) > 0 Then
                lkbAddPromotion.Text = "Remove Promotion"
                lkbAddPromotion.Visible = True

                divPricingOption.Visible = True
                hplLastPrices.Visible = False
                lkbPriceLevel.Visible = False
                lkbPriceRule.Visible = False
                lkbLastPrice.Visible = False
                lkbListPrice.Visible = False
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    'Change Price
    Private Sub ItemFieldsChanged(ByVal itemCOde As Long, Optional ByVal isEditClicked As Boolean = False)
        Try
            Dim divisionID As Long = 0

            If Not _PageType = PageType.AddEditOrder Then
                If IsStockTransfer Then
                    divisionID = IIf(CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue)
                Else
                    divisionID = IIf(CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue)
                End If
            Else
                divisionID = CCommon.ToLong(GetQueryStringVal("DivId"))
            End If

            Dim objOpportunity As New COpportunities
            objOpportunity.DomainID = Session("DomainID")
            objOpportunity.DivisionID = CCommon.ToLong(divisionID)
            objOpportunity.OppType = OppType
            objOpportunity.ItemCode = itemCOde
            objOpportunity.UnitHour = txtUnits.Text
            objOpportunity.WarehouseItmsID = CCommon.ToLong(radWareHouse.SelectedValue)
            If (_PageType = PageType.AddEditOrder) Then
                objOpportunity.CurrencyID = CCommon.ToLong(CType(Me.Parent.FindControl("hdnCurrencyID"), HiddenField).Value)
            Else
                objOpportunity.CurrencyID = CCommon.ToLong(CType(Me.Parent.FindControl("ddlCurrency"), DropDownList).SelectedValue)
            End If
            Dim ds As DataSet = objOpportunity.GetOrderItemDetails(divisionID, hdnKitChildItems.Value, CCommon.ToLong(hdnCountry.Value))

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                hdnListPrice.Value = CCommon.ToDecimal(ds.Tables(0).Rows(0)("monListPrice"))
                hdnVendorCost.Value = CCommon.ToDecimal(ds.Tables(0).Rows(0)("VendorCost"))

                If OppType = 1 Then
                    hplRelatedItems.Text = "Related Items(" & CCommon.ToLong(ds.Tables(0).Rows(0)("RelatedItemsCount")) & ")"
                    hplRelatedItems.Visible = True

                    SetPricingOption(ds.Tables(0).Rows(0))
                End If

                If CCommon.ToString(ds.Tables(0).Rows(0)("dtDynamicCostModifiedDate")) <> "" Then
                    lblDynamicCostUpdatedDate.Text = "Updated: " & FormattedDateFromDate(Convert.ToDateTime(ds.Tables(0).Rows(0)("dtDynamicCostModifiedDate")).AddMinutes(CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")) * -1).ToShortDateString(), Session("DateFormat"))
                End If

                If (OppType = 2 Or CCommon.ToLong(hdnPromotionID.Value) = 0) AndAlso Not isEditClicked Then
                    txtUOMConversionFactor.Text = CCommon.ToString(ds.Tables(0).Rows(0)("UOMConversionFactor"))
                    CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(ds.Tables(0).Rows(0)("monPrice")), txtprice)
                    CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(ds.Tables(0).Rows(0)("Discount")), txtItemDiscount)

                    If CCommon.ToString(ds.Tables(0).Rows(0)("DiscountType")) = "1" Then
                        radPer.Checked = True
                        radAmt.Checked = False
                    Else
                        radAmt.Checked = True
                        radPer.Checked = False
                    End If

                    If CCommon.ToBool(ds.Tables(0).Rows(0)("bitMarkupDiscount")) Then
                        ddlMarkupDiscountOption.SelectedValue = "1"
                    Else
                        ddlMarkupDiscountOption.SelectedValue = "0"
                    End If
                ElseIf OppType = 1 AndAlso CCommon.ToLong(hdnPromotionID.Value) > 0 AndAlso CCommon.ToLong(txtHidEditOppItem.Text) = 0 Then
                    CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(ds.Tables(0).Rows(0)("monListPrice")), txtprice)
                    txtUOMConversionFactor.Text = CCommon.ToString(ds.Tables(0).Rows(0)("UOMConversionFactor"))

                    Dim drPromotion As DataRow = DirectCast(ViewState("UsedPromotion"), DataTable).Select("numProID=" & hdnPromotionID.Value)(0)

                    If drPromotion("tintDiscountType") = "3" Then 'Free Quantity
                        Dim freeQuantityAdded As Int32 = 0

                        Dim arrItems As DataRow() = dsTemp.Tables(0).Select("numPromotionID=" & drPromotion("numProID"))

                        For Each drItem As DataRow In arrItems
                            If CCommon.ToInteger(drItem("fltDiscount")) > 0 Then
                                freeQuantityAdded += CCommon.ToInteger(drItem("fltDiscount") / drItem("monPrice"))
                            End If
                        Next

                        Dim remainingQty As Int32 = CCommon.ToInteger(drPromotion("fltDiscountValue")) - freeQuantityAdded

                        If CCommon.ToInteger(txtUnits.Text * txtUOMConversionFactor.Text) >= remainingQty Then
                            CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(remainingQty * hdnListPrice.Value), txtItemDiscount)
                        Else
                            CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(txtUnits.Text * txtUOMConversionFactor.Text * hdnListPrice.Value), txtItemDiscount)
                        End If
                    End If
                End If

                hdnUnitCost.Value = CCommon.ToDecimal(txtprice.Text)
                CalculateProfit()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub RegisterPostBackControl()
        For Each row As GridViewRow In gvRelatedItems.Rows
            Dim lnkAdd As LinkButton = TryCast(row.FindControl("lnkAdd"), LinkButton)
            ScriptManager.GetCurrent(Me.Page).RegisterAsyncPostBackControl(lnkAdd)
        Next
    End Sub

    Public Class SelectedItems
        Public Property numItemCode As Long
        Public Property numWarehouseItemID As String
        Public Property bitHasKitAsChild As Boolean
        Public Property kitChildItems As String
        Public Property numSelectedUOM As Long
        Public Property vcSelectedUOM As String
        Public Property numQuantity As Long
        Public Property numToWarehouseItemID As String
        Public Property vcAttributes As String
        Public Property vcAttributeIDs As String
    End Class

    Private Sub LoadItemAttributes(ByVal itemCode As Long)
        Try
            objItems = New CItems
            objItems.ItemCode = itemCode
            objItems.WareHouseItemID = 0
            Dim ds As DataSet = objItems.GetItemWareHouses()

            If Not ds Is Nothing AndAlso ds.Tables.Count >= 3 AndAlso ds.Tables(2).Rows.Count > 0 Then
                rptAttributes.DataSource = ds.Tables(2)
                rptAttributes.DataBind()

                divItemAttributes.Style.Add("display", "")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSaveCloseAttributes_Click(sender As Object, e As EventArgs) Handles btnSaveCloseAttributes.Click
        Try
            Dim vcAttributes As String
            Dim vcAttributeIDs As String

            For Each item As RepeaterItem In rptAttributes.Items
                Dim lblAttributeName As Label = DirectCast(item.FindControl("lblAttributeName"), Label)
                Dim ddlAttributes As DropDownList = DirectCast(item.FindControl("ddlAttributes"), DropDownList)
                Dim hdnFldID As HiddenField = DirectCast(item.FindControl("hdnFldID"), HiddenField)

                If CCommon.ToLong(ddlAttributes.SelectedValue) = 0 Then
                    lblAttributeError.Text = "No value is selected for attribute <b>" & lblAttributeName.Text & "</b>."
                    ddlAttributes.Focus()
                    Exit Sub
                Else
                    vcAttributes = IIf(String.IsNullOrEmpty(vcAttributes), "", vcAttributes + ",") & lblAttributeName.Text & ":" & ddlAttributes.SelectedItem.Text
                    vcAttributeIDs = IIf(String.IsNullOrEmpty(vcAttributeIDs), "", vcAttributeIDs + ",") & hdnFldID.Value & ":" & ddlAttributes.SelectedItem.Value
                End If
            Next

            Dim warehouseItemID As Long = 0
            If CCommon.ToBool(hdnIsMatrix.Value) Then
                objItems = New CItems
                objItems.DomainID = CCommon.ToLong(Session("DomainID"))
                objItems.ItemCode = CCommon.ToLong(hdnCurrentSelectedItem.Value)
                objItems.AttributeIDs = vcAttributeIDs
                Dim dt As DataTable = objItems.GetMatrixItemFromAttributes()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    If hdnCurrentSelectedItem.Value <> CCommon.ToLong(dt.Rows(0)("numItemCode")) Then
                        hdnCurrentSelectedItem.Value = CCommon.ToLong(dt.Rows(0)("numItemCode"))
                        warehouseItemID = CCommon.ToLong(dt.Rows(0)("numWarehouseItemID"))
                        LoadSelectedItemDetail(CCommon.ToLong(dt.Rows(0)("numItemCode")), LoadAttributes:=False)

                        If OppType = 1 Then
                            RelatedItems(True)
                        End If
                    End If
                Else
                    lblAttributeError.Text = "No item available with selected attributes."
                    Exit Sub
                End If
            End If

            ScriptManager.RegisterClientScriptBlock(upnl, GetType(Page), "SetItemAttributes", "SetItemAttributes(" + hdnCurrentSelectedItem.Value + "," + warehouseItemID.ToString() + ",'" + vcAttributes + "','" + vcAttributeIDs + "');", True)
            ScriptManager.RegisterStartupScript(upnl, GetType(Page), "EnableScroll", "Scroll(true);", True)
            divItemAttributes.Style.Add("display", "none")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub rptAttributes_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptAttributes.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim dr As DataRowView = DirectCast(e.Item.DataItem, System.Data.DataRowView)
                Dim ddlAttributes As DropDownList = DirectCast(e.Item.FindControl("ddlAttributes"), DropDownList)
                Dim hdnFldID As HiddenField = DirectCast(e.Item.FindControl("hdnFldID"), HiddenField)
                Dim hdnListID As HiddenField = DirectCast(e.Item.FindControl("hdnListID"), HiddenField)

                If e.Item.ItemIndex = 0 Then
                    ddlAttributes.Focus()
                End If

                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")
                objCommon.sb_FillAttibuesForEcommFromDB(ddlAttributes, hdnListID.Value, hdnCurrentSelectedItem.Value, "", 0)

                If Not dr("Fld_Value") Is Nothing AndAlso Not ddlAttributes.Items.FindByValue(dr("Fld_Value")) Is Nothing Then
                    ddlAttributes.Items.FindByValue(dr("Fld_Value")).Selected = True
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub lkbLastPrice_Click(sender As Object, e As EventArgs) Handles lkbLastPrice.Click
        Try
            CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(hdnLastPrice.Value), txtprice)
            hdnUnitCost.Value = CCommon.ToDecimal(txtprice.Text)
            CCommon.SetValueInDecimalFormat(0, txtItemDiscount)
            radPer.Checked = True
            hdnPricingBasedOn.Value = "3"

            lkbLastPrice.Text = "<i class=""fa fa-check""></i>&nbsp;Used Last Price (" & hdnLastPrice.Value & "," & hdnLastOrderDate.Value & ")"
            lkbPriceLevel.Text = lkbPriceLevel.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "").Replace("Used", "Add")
            lkbPriceRule.Text = lkbPriceRule.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "").Replace("Used", "Add")
            lkbListPrice.Text = lkbListPrice.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "").Replace("Used", "Add")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub lkbListPrice_Click(sender As Object, e As EventArgs) Handles lkbListPrice.Click
        Try
            CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(hdnListPrice.Value), txtprice)
            hdnUnitCost.Value = CCommon.ToDecimal(txtprice.Text)
            CCommon.SetValueInDecimalFormat(0, txtItemDiscount)
            radPer.Checked = True
            hdnPricingBasedOn.Value = ""

            lkbListPrice.Text = "<i class=""fa fa-check""></i>&nbsp;Used List Price (" & hdnListPrice.Value & ")"
            lkbPriceLevel.Text = lkbPriceLevel.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "").Replace("Used", "Add")
            lkbPriceRule.Text = lkbPriceRule.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "").Replace("Used", "Add")
            lkbLastPrice.Text = lkbLastPrice.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "").Replace("Used", "Add")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub lkbPriceLevel_Click(sender As Object, e As EventArgs) Handles lkbPriceLevel.Click
        Try
            Dim objOpportuniy As New COpportunities
            objOpportuniy.DomainID = CCommon.ToLong(Session("DomainID"))
            If Not _PageType = PageType.AddEditOrder Then
                If IsStockTransfer Then
                    objOpportuniy.DivisionID = IIf(CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue)
                Else
                    objOpportuniy.DivisionID = IIf(CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue)
                End If
            Else
                objOpportuniy.DivisionID = CCommon.ToLong(GetQueryStringVal("DivId"))
            End If
            If (_PageType = PageType.AddEditOrder) Then
                objOpportuniy.CurrencyID = CCommon.ToLong(CType(Me.Parent.FindControl("hdnCurrencyID"), HiddenField).Value)
            Else
                objOpportuniy.CurrencyID = CCommon.ToLong(CType(Me.Parent.FindControl("ddlCurrency"), DropDownList).SelectedValue)
            End If
            objOpportuniy.OppType = OppType 'Price Level
            objOpportuniy.ItemCode = hdnCurrentSelectedItem.Value
            objOpportuniy.UnitHour = txtUnits.Text
            Dim dt As DataTable = objOpportuniy.GetItemPriceBasedOnPricingOption(1, hdnListPrice.Value, hdnVendorCost.Value, txtUOMConversionFactor.Text)

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                hdnPricingBasedOn.Value = "1"
                CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(dt.Rows(0)("monPrice")), txtprice)
                hdnUnitCost.Value = CCommon.ToDecimal(txtprice.Text)

                If CCommon.ToBool(dt.Rows(0)("tintDisountType")) Then
                    radPer.Checked = True
                    radAmt.Checked = False
                Else
                    radAmt.Checked = True
                    radPer.Checked = False
                End If
                CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(dt.Rows(0)("decDiscount")), txtItemDiscount)
                lkbPriceLevel.Text = "<i class=""fa fa-check""></i>&nbsp;Used Price Level"
                lkbLastPrice.Text = lkbLastPrice.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "").Replace("Used", "Add")
                lkbPriceRule.Text = lkbPriceRule.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "").Replace("Used", "Add")
                lkbListPrice.Text = lkbListPrice.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "").Replace("Used", "Add")
            End If


        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub lkbPriceRule_Click(sender As Object, e As EventArgs) Handles lkbPriceRule.Click
        Try
            Dim objOpportuniy As New COpportunities
            objOpportuniy.DomainID = CCommon.ToLong(Session("DomainID"))
            If Not _PageType = PageType.AddEditOrder Then
                If IsStockTransfer Then
                    objOpportuniy.DivisionID = IIf(CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue)
                Else
                    objOpportuniy.DivisionID = IIf(CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue)
                End If
            Else
                objOpportuniy.DivisionID = CCommon.ToLong(GetQueryStringVal("DivId"))
            End If
            If (_PageType = PageType.AddEditOrder) Then
                objOpportuniy.CurrencyID = CCommon.ToLong(CType(Me.Parent.FindControl("hdnCurrencyID"), HiddenField).Value)
            Else
                objOpportuniy.CurrencyID = CCommon.ToLong(CType(Me.Parent.FindControl("ddlCurrency"), DropDownList).SelectedValue)
            End If
            objOpportuniy.OppType = OppType 'Price Rule
            objOpportuniy.ItemCode = hdnCurrentSelectedItem.Value
            objOpportuniy.UnitHour = txtUnits.Text
            Dim dt As DataTable = objOpportuniy.GetItemPriceBasedOnPricingOption(2, hdnListPrice.Value, hdnVendorCost.Value, txtUOMConversionFactor.Text)

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                hdnPricingBasedOn.Value = "2"
                CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(dt.Rows(0)("monPrice")), txtprice)
                hdnUnitCost.Value = CCommon.ToDecimal(txtprice.Text)

                If CCommon.ToBool(dt.Rows(0)("tintDisountType")) Then
                    radPer.Checked = True
                Else
                    radAmt.Checked = True
                End If

                CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(dt.Rows(0)("decDiscount")), txtItemDiscount)
                lkbPriceRule.Text = "<i class=""fa fa-check""></i>&nbsp;Used Price Rule"
                lkbLastPrice.Text = lkbLastPrice.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "").Replace("Used", "Add")
                lkbPriceLevel.Text = lkbPriceLevel.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "").Replace("Used", "Add")
                lkbListPrice.Text = lkbListPrice.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "").Replace("Used", "Add")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub lkbAddPromotion_Click(sender As Object, e As EventArgs) Handles lkbAddPromotion.Click
        Try
            If lkbAddPromotion.Text.Contains("Remove") Then
                If CCommon.ToBool(hdnPromotionTriggered.Value) Then
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "RemovePromotion", "RemovePromotion();", True)
                Else
                    btnRemovePromotion_Click(Nothing, Nothing)
                End If
            Else
                Dim dt As DataTable = DirectCast(ViewState("DataTableTempPromotion"), DataTable)

                If Not dt Is Nothing And dt.Rows.Count > 0 Then
                    Dim dr As DataRow = dt.Rows(0)

                    'CHECK COUPONCODE IF REQUIRED FOR PROMOTION
                    If CCommon.ToBool(dr("bitRequireCouponCode")) AndAlso String.IsNullOrEmpty(txtCouponCode.Text) Then
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "RequireCoupnCode", "alert('Coupon code is required.');", True)
                        Exit Sub
                    ElseIf CCommon.ToBool(dr("bitRequireCouponCode")) AndAlso txtCouponCode.Text.Trim() <> dr("txtCouponCode") Then
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "InvalidCoupnCode", "alert('Coupon code does not existis.');", True)
                        Exit Sub
                    End If

                    Dim totalQuantity As Int32
                    Dim totalAmount As Decimal
                    If dr("tintOfferTriggerValueType") = 1 Then 'Quantity
                        If dr("tintOfferBasedOn") = "2" Then
                            totalQuantity += dsTemp.Tables(0).Select("numItemClassification=" & hdnItemClassification.Value).Sum(Function(x) x("numUnitHour") * x("UOMConversionFactor"))
                        Else
                            totalQuantity += dsTemp.Tables(0).Select("numItemCode=" & hdnCurrentSelectedItem.Value).Sum(Function(x) x("numUnitHour") * x("UOMConversionFactor"))
                        End If

                        totalQuantity += CCommon.ToInteger(txtUnits.Text * txtUOMConversionFactor.Text)

                        If totalQuantity < CCommon.ToInteger(dr("fltOfferTriggerValue")) AndAlso CCommon.ToInteger(txtUnits.Text * txtUOMConversionFactor.Text) < CCommon.ToInteger(dr("fltOfferTriggerValue")) Then
                            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "PromotionQty", "alert('Buy " & CCommon.ToInteger(dr("fltOfferTriggerValue")) & " to avail promotion offer.');", True)
                            Exit Sub
                        Else

                            Dim remainingQtyToFulfill As Int32 = CCommon.ToInteger(dr("fltOfferTriggerValue")) - CCommon.ToInteger(txtUnits.Text * txtUOMConversionFactor.Text)

                            If remainingQtyToFulfill > 0 Then
                                Dim foundRows As DataRow()

                                If dr("tintOfferBasedOn") = "2" Then
                                    foundRows = dsTemp.Tables(0).Select("numItemClassification=" & hdnItemClassification.Value & " AND bitDisablePromotion=false")
                                Else
                                    foundRows = dsTemp.Tables(0).Select("numItemCode=" & hdnCurrentSelectedItem.Value & " ADN bitDisablePromotion=false")
                                End If

                                For Each drItem As DataRow In foundRows

                                    Dim objItem As New CItems
                                    objItem.byteMode = CCommon.ToShort(OppType)
                                    objItem.ItemCode = CCommon.ToLong(drItem("numItemCode"))
                                    objItem.WareHouseItemID = CCommon.ToLong(drItem("numWarehouseItmsID"))
                                    objItem.GetItemAndWarehouseDetails()

                                    Dim price As Decimal = objItem.ListPrice
                                    Dim salePrice As Decimal = price

                                    'Start - Get Tax Detail
                                    Dim strApplicable As String
                                    Dim dtItemTax As DataTable
                                    Dim chkTaxItems As CheckBoxList

                                    If OppType = 1 Then
                                        chkTaxItems = CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList)

                                        objItem.DomainID = Session("DomainID")
                                        dtItemTax = objItems.ItemTax()

                                        For Each drTax As DataRow In dtItemTax.Rows
                                            strApplicable = strApplicable & drTax("bitApplicable") & ","
                                        Next

                                        strApplicable = strApplicable & objItems.Taxable
                                    End If

                                    objCommon = New CCommon
                                    objCommon.DomainID = CCommon.ToLong(Session("DomainID"))



                                    ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon, dsTemp, False, drItem("charItemType"),
                                                                                   drItem("ItemType"), drItem("DropShip"), "", drItem("numItemCode"),
                                                                                   drItem("numUnitHour"),
                                                                                   price,
                                                                                   CCommon.ToString(drItem("vcItemDesc")),
                                                                                   drItem("numWarehouseItmsID"), "", drItem("Warehouse"),
                                                                                   drItem("numoppitemtCode"), CCommon.ToString(drItem("vcModelID")),
                                                                                   drItem("numUOM"), drItem("vcUOMName"), drItem("UOMConversionFactor"),
                                                                                   _PageType.ToString(), True,
                                                                                   0, strApplicable, txtTax.Text, txtCustomerPartNo.Text, chkTaxItems,
                                                                                   drItem("bitWorkOrder"), CCommon.ToString(drItem("vcInstruction")), CCommon.ToString(drItem("bintCompliationDate")),
                                                                                   CCommon.ToLong(drItem("numWOAssignedTo")),
                                                                                   drItem("numSOVendorId"),
                                                                                   drItem("numProjectID"), drItem("numProjectStageID"),
                                                                                   objItem:=objItem,
                                                                                   vcBaseUOMName:=drItem("vcBaseUOMName"),
                                                                                   numPrimaryVendorID:=drItem("numVendorID"),
                                                                                   fltItemWeight:=CCommon.ToLong(hdnItemWeight.Value), IsFreeShipping:=CCommon.ToBool(hdnFreeShipping.Value),
                                                                                   fltHeight:=CCommon.ToDouble(hdnHeight.Value), fltWidth:=CCommon.ToDouble(hdnWidth.Value),
                                                                                   fltLength:=CCommon.ToDouble(hdnLength.Value), strSKU:=CCommon.ToString(hdnSKU.Value), salePrice:=salePrice, numMaxWOQty:=drItem("numMaxWorkOrderQty"), numPromotionID:=dr("numProId"), IsPromotionTriggered:=True, vcPromotionDetail:=dr("vcPromotionDescription"))

                                    remainingQtyToFulfill -= CCommon.ToInteger(drItem("numUnitHour") * drItem("UOMConversionFactor"))

                                    If remainingQtyToFulfill <= 0 Then
                                        Exit For
                                    End If
                                Next
                            End If
                        End If
                    ElseIf dr("tintOfferTriggerValueType") = 2 AndAlso CCommon.ToInteger(txtUnits.Text * txtUOMConversionFactor.Text * hdnListPrice.Value) < CCommon.ToInteger(dr("fltOfferTriggerValue")) Then 'Amount
                        If dr("tintOfferBasedOn") = "2" Then
                            totalAmount += dsTemp.Tables(0).Select("numItemClassification=" & hdnItemClassification.Value).Sum(Function(x) x("numUnitHour") * x("UOMConversionFactor") * x("monPrice"))
                        Else
                            totalAmount += dsTemp.Tables(0).Select("numItemCode=" & hdnCurrentSelectedItem.Value).Sum(Function(x) x("numUnitHour") * x("UOMConversionFactor") * x("monPrice"))
                        End If

                        totalAmount += CCommon.ToInteger(txtUnits.Text * txtUOMConversionFactor.Text) * hdnListPrice.Value

                        If totalAmount < CCommon.ToInteger(dr("fltOfferTriggerValue")) Then
                            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "PromotionAmount", "alert('Buy of $" & CCommon.ToInteger(dr("fltOfferTriggerValue")) & " to avail promotion offer.');", True)
                            Exit Sub
                        Else
                            Dim remainingAmountToFulfill As Int32 = CCommon.ToInteger(dr("fltOfferTriggerValue")) - CCommon.ToInteger(txtUnits.Text * txtUOMConversionFactor.Text * hdnListPrice.Value)

                            If remainingAmountToFulfill > 0 Then
                                Dim foundRows As DataRow()

                                If dr("tintOfferBasedOn") = "2" Then
                                    foundRows = dsTemp.Tables(0).Select("numItemClassification=" & hdnItemClassification.Value & "AND bitDisablePromotion=false")
                                Else
                                    foundRows = dsTemp.Tables(0).Select("numItemCode=" & hdnCurrentSelectedItem.Value & "AND bitDisablePromotion=false")
                                End If

                                For Each drItem As DataRow In foundRows

                                    Dim objItem As New CItems
                                    objItem.byteMode = CCommon.ToShort(OppType)
                                    objItem.ItemCode = CCommon.ToLong(drItem("numItemCode"))
                                    objItem.WareHouseItemID = CCommon.ToLong(drItem("numWarehouseItmsID"))
                                    objItem.GetItemAndWarehouseDetails()

                                    Dim price As Decimal = objItem.ListPrice
                                    Dim salePrice As Decimal = price

                                    'Start - Get Tax Detail
                                    Dim strApplicable As String
                                    Dim dtItemTax As DataTable
                                    Dim chkTaxItems As CheckBoxList

                                    If OppType = 1 Then
                                        chkTaxItems = CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList)

                                        objItem.DomainID = Session("DomainID")
                                        dtItemTax = objItems.ItemTax()

                                        For Each drTax As DataRow In dtItemTax.Rows
                                            strApplicable = strApplicable & drTax("bitApplicable") & ","
                                        Next

                                        strApplicable = strApplicable & objItems.Taxable
                                    End If

                                    objCommon = New CCommon
                                    objCommon.DomainID = CCommon.ToLong(Session("DomainID"))

                                    ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon, dsTemp, False, drItem("charItemType"),
                                                                                   drItem("ItemType"), drItem("DropShip"), "", drItem("numItemCode"),
                                                                                   drItem("numUnitHour"),
                                                                                   price,
                                                                                   CCommon.ToString(drItem("vcItemDesc")),
                                                                                   drItem("numWarehouseItmsID"), "", drItem("Warehouse"),
                                                                                   drItem("numoppitemtCode"), CCommon.ToString(drItem("vcModelID")),
                                                                                   drItem("numUOM"), drItem("vcUOMName"), drItem("UOMConversionFactor"),
                                                                                   _PageType.ToString(), True,
                                                                                   0, strApplicable, txtTax.Text, txtCustomerPartNo.Text, chkTaxItems,
                                                                                   drItem("bitWorkOrder"), CCommon.ToString(drItem("vcInstruction")), CCommon.ToString(drItem("bintCompliationDate")),
                                                                                   CCommon.ToLong(drItem("numWOAssignedTo")),
                                                                                   drItem("numSOVendorId"),
                                                                                   drItem("numProjectID"), drItem("numProjectStageID"),
                                                                                   objItem:=objItem,
                                                                                   vcBaseUOMName:=drItem("vcBaseUOMName"),
                                                                                   numPrimaryVendorID:=drItem("numVendorID"),
                                                                                   fltItemWeight:=CCommon.ToLong(hdnItemWeight.Value), IsFreeShipping:=CCommon.ToBool(hdnFreeShipping.Value),
                                                                                   fltHeight:=CCommon.ToDouble(hdnHeight.Value), fltWidth:=CCommon.ToDouble(hdnWidth.Value),
                                                                                   fltLength:=CCommon.ToDouble(hdnLength.Value), strSKU:=CCommon.ToString(hdnSKU.Value), salePrice:=salePrice, numMaxWOQty:=drItem("numMaxWorkOrderQty"), numPromotionID:=dr("numProId"), IsPromotionTriggered:=True, vcPromotionDetail:=dr("vcPromotionDescription"))

                                    remainingAmountToFulfill -= CCommon.ToInteger(drItem("numUnitHour") * drItem("UOMConversionFactor") * price)

                                    If remainingAmountToFulfill <= 0 Then
                                        Exit For
                                    End If
                                Next
                            End If
                        End If
                    End If

                    hdnPromotionID.Value = dr("numProId")
                    hdnPromotionTriggered.Value = True
                    hdnPromotionDescription.Value = dr("vcPromotionDescription")

                    If ((dr("tintDiscoutBaseOn") = 1 Or dr("tintDiscoutBaseOn") = 3) AndAlso CCommon.ToString(dr("vcItems")).Split(",").Contains(hdnCurrentSelectedItem.Value)) Or (dr("tintDiscoutBaseOn") = 2 AndAlso CCommon.ToString(dr("vcItems")).Split(",").Contains(hdnItemClassification.Value)) Then
                        If dr("tintDiscountType") = "1" Then
                            CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(dr("fltDiscountValue")), txtItemDiscount)
                            radAmt.Checked = False
                            radPer.Checked = True


                            Dim foundRows As New ArrayList

                            If dr("tintDiscoutBaseOn") = 1 Then
                                For Each drTemp As DataRow In dsTemp.Tables(0).Rows
                                    If drTemp("bitDisablePromotion") = False AndAlso (drTemp("numPromotionID") = 0 Or (drTemp("numPromotionID") = dr("numProID") AndAlso drTemp("fltDiscount") = 0)) AndAlso CCommon.ToString(dr("vcItems")).Split(",").Contains(drTemp("numItemCode")) Then
                                        foundRows.Add(drTemp)
                                    End If
                                Next
                            ElseIf dr("tintDiscoutBaseOn") = 2 Then
                                For Each drTemp As DataRow In dsTemp.Tables(0).Rows
                                    If drTemp("bitDisablePromotion") = False AndAlso (drTemp("numPromotionID") = 0 Or (drTemp("numPromotionID") = dr("numProID") AndAlso drTemp("fltDiscount") = 0)) AndAlso CCommon.ToString(dr("vcItems")).Split(",").Contains(drTemp("numItemClassification")) Then
                                        foundRows.Add(drTemp)
                                    End If
                                Next
                            End If

                            For Each drItem As DataRow In foundRows

                                Dim objItem As New CItems
                                objItem.byteMode = CCommon.ToShort(OppType)
                                objItem.ItemCode = CCommon.ToLong(drItem("numItemCode"))
                                objItem.WareHouseItemID = CCommon.ToLong(drItem("numWarehouseItmsID"))
                                objItem.GetItemAndWarehouseDetails()

                                Dim price As Decimal = objItem.ListPrice
                                Dim salePrice As Decimal = price

                                salePrice = price - (price * (CCommon.ToInteger(dr("fltDiscountValue")) / 100))

                                'Start - Get Tax Detail
                                Dim strApplicable As String
                                Dim dtItemTax As DataTable
                                Dim chkTaxItems As CheckBoxList

                                If OppType = 1 Then
                                    chkTaxItems = CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList)

                                    objItem.DomainID = Session("DomainID")
                                    dtItemTax = objItems.ItemTax()

                                    For Each drTax As DataRow In dtItemTax.Rows
                                        strApplicable = strApplicable & drTax("bitApplicable") & ","
                                    Next

                                    strApplicable = strApplicable & objItems.Taxable
                                End If

                                objCommon = New CCommon
                                objCommon.DomainID = CCommon.ToLong(Session("DomainID"))

                                ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon, dsTemp, False, drItem("charItemType"),
                                                                               drItem("ItemType"), drItem("DropShip"), "", drItem("numItemCode"),
                                                                               drItem("numUnitHour"),
                                                                               price,
                                                                               CCommon.ToString(drItem("vcItemDesc")),
                                                                               drItem("numWarehouseItmsID"), "", drItem("Warehouse"),
                                                                               drItem("numoppitemtCode"), CCommon.ToString(drItem("vcModelID")),
                                                                               drItem("numUOM"), drItem("vcUOMName"), drItem("UOMConversionFactor"),
                                                                               _PageType.ToString(), True,
                                                                               CCommon.ToInteger(dr("fltDiscountValue")), strApplicable, txtTax.Text, txtCustomerPartNo.Text, chkTaxItems,
                                                                               drItem("bitWorkOrder"), CCommon.ToString(drItem("vcInstruction")), CCommon.ToString(drItem("bintCompliationDate")),
                                                                               CCommon.ToLong(drItem("numWOAssignedTo")),
                                                                               drItem("numSOVendorId"),
                                                                               drItem("numProjectID"), drItem("numProjectStageID"),
                                                                               objItem:=objItem,
                                                                               vcBaseUOMName:=drItem("vcBaseUOMName"),
                                                                               numPrimaryVendorID:=drItem("numVendorID"),
                                                                               fltItemWeight:=CCommon.ToLong(hdnItemWeight.Value), IsFreeShipping:=CCommon.ToBool(hdnFreeShipping.Value),
                                                                               fltHeight:=CCommon.ToDouble(hdnHeight.Value), fltWidth:=CCommon.ToDouble(hdnWidth.Value),
                                                                               fltLength:=CCommon.ToDouble(hdnLength.Value), strSKU:=CCommon.ToString(hdnSKU.Value), salePrice:=salePrice, numMaxWOQty:=drItem("numMaxWorkOrderQty"), numPromotionID:=dr("numProId"), IsPromotionTriggered:=True, vcPromotionDetail:=dr("vcPromotionDescription"))
                            Next
                        ElseIf dr("tintDiscountType") = "2" Then
                            Dim remainingDiscountToApply As Decimal = CCommon.ToDecimal(dr("fltDiscountValue"))

                            If CCommon.ToDecimal(txtUnits.Text * txtUOMConversionFactor.Text) * hdnListPrice.Value >= CCommon.ToInteger(dr("fltDiscountValue")) Then
                                CCommon.SetValueInDecimalFormat(remainingDiscountToApply, txtItemDiscount)
                                radAmt.Checked = True
                                radPer.Checked = False
                            Else
                                Dim discount As Decimal = CCommon.ToDecimal(txtUnits.Text * txtUOMConversionFactor.Text * hdnListPrice.Value)
                                CCommon.SetValueInDecimalFormat(discount, txtItemDiscount)
                                radAmt.Checked = True
                                radPer.Checked = False
                                remainingDiscountToApply -= discount

                                If remainingDiscountToApply > 0 Then

                                    Dim foundRows As New ArrayList

                                    If dr("tintDiscoutBaseOn") = 1 Then
                                        For Each drTemp As DataRow In dsTemp.Tables(0).Rows
                                            If drTemp("bitDisablePromotion") = False AndAlso (drTemp("numPromotionID") = 0 Or (drTemp("numPromotionID") = dr("numProID") AndAlso drTemp("fltDiscount") = 0)) AndAlso CCommon.ToString(dr("vcItems")).Split(",").Contains(drTemp("numItemCode")) Then
                                                foundRows.Add(drTemp)
                                            End If
                                        Next
                                    ElseIf dr("tintDiscoutBaseOn") = 2 Then
                                        For Each drTemp As DataRow In dsTemp.Tables(0).Rows
                                            If drTemp("bitDisablePromotion") = False AndAlso (drTemp("numPromotionID") = 0 Or (drTemp("numPromotionID") = dr("numProID") AndAlso drTemp("fltDiscount") = 0)) AndAlso CCommon.ToString(dr("vcItems")).Split(",").Contains(drTemp("numItemClassification")) Then
                                                foundRows.Add(drTemp)
                                            End If
                                        Next
                                    End If

                                    For Each drItem As DataRow In foundRows

                                        Dim objItem As New CItems
                                        objItem.byteMode = CCommon.ToShort(OppType)
                                        objItem.ItemCode = CCommon.ToLong(drItem("numItemCode"))
                                        objItem.WareHouseItemID = CCommon.ToLong(drItem("numWarehouseItmsID"))
                                        objItem.GetItemAndWarehouseDetails()

                                        discount = CCommon.ToInteger(drItem("numUnitHour") * drItem("UOMConversionFactor") * objItem.ListPrice)

                                        If discount >= remainingDiscountToApply Then
                                            discount = remainingDiscountToApply
                                            remainingDiscountToApply = 0
                                        Else
                                            remainingDiscountToApply -= discount
                                        End If

                                        Dim price As Decimal = objItem.ListPrice
                                        Dim salePrice As Decimal = price

                                        salePrice = ((price * CCommon.ToDecimal(drItem("numUnitHour") * drItem("UOMConversionFactor"))) - discount) / CCommon.ToDecimal(drItem("numUnitHour") * drItem("UOMConversionFactor"))

                                        'Start - Get Tax Detail
                                        Dim strApplicable As String
                                        Dim dtItemTax As DataTable
                                        Dim chkTaxItems As CheckBoxList

                                        If OppType = 1 Then
                                            chkTaxItems = CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList)

                                            objItem.DomainID = Session("DomainID")
                                            dtItemTax = objItems.ItemTax()

                                            For Each drTax As DataRow In dtItemTax.Rows
                                                strApplicable = strApplicable & drTax("bitApplicable") & ","
                                            Next

                                            strApplicable = strApplicable & objItems.Taxable
                                        End If

                                        objCommon = New CCommon
                                        objCommon.DomainID = CCommon.ToLong(Session("DomainID"))

                                        ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon, dsTemp, False, drItem("charItemType"),
                                                                                       drItem("ItemType"), drItem("DropShip"), "", drItem("numItemCode"),
                                                                                       drItem("numUnitHour"),
                                                                                       price,
                                                                                       CCommon.ToString(drItem("vcItemDesc")),
                                                                                       drItem("numWarehouseItmsID"), "", drItem("Warehouse"),
                                                                                       drItem("numoppitemtCode"), CCommon.ToString(drItem("vcModelID")),
                                                                                       drItem("numUOM"), drItem("vcUOMName"), drItem("UOMConversionFactor"),
                                                                                       _PageType.ToString(), False,
                                                                                       discount, strApplicable, txtTax.Text, txtCustomerPartNo.Text, chkTaxItems,
                                                                                       drItem("bitWorkOrder"), CCommon.ToString(drItem("vcInstruction")), CCommon.ToString(drItem("bintCompliationDate")),
                                                                                       CCommon.ToLong(drItem("numWOAssignedTo")),
                                                                                       drItem("numSOVendorId"),
                                                                                       drItem("numProjectID"), drItem("numProjectStageID"),
                                                                                       objItem:=objItem,
                                                                                       vcBaseUOMName:=drItem("vcBaseUOMName"),
                                                                                       numPrimaryVendorID:=drItem("numVendorID"),
                                                                                       fltItemWeight:=CCommon.ToLong(hdnItemWeight.Value), IsFreeShipping:=CCommon.ToBool(hdnFreeShipping.Value),
                                                                                       fltHeight:=CCommon.ToDouble(hdnHeight.Value), fltWidth:=CCommon.ToDouble(hdnWidth.Value),
                                                                                       fltLength:=CCommon.ToDouble(hdnLength.Value), strSKU:=CCommon.ToString(hdnSKU.Value), salePrice:=salePrice, numMaxWOQty:=drItem("numMaxWorkOrderQty"), numPromotionID:=dr("numProId"), IsPromotionTriggered:=True, vcPromotionDetail:=dr("vcPromotionDescription"))

                                        If remainingDiscountToApply = 0 Then
                                            Exit For
                                        End If
                                    Next
                                End If
                            End If

                        ElseIf dr("tintDiscountType") = "3" Then
                            If (dr("tintOfferBasedOn") = 1 AndAlso CCommon.ToString(dr("vcPromotionItems")).Split(",").Contains(hdnCurrentSelectedItem.Value)) Or
                               (dr("tintOfferBasedOn") = 2 AndAlso CCommon.ToString(dr("vcPromotionItems")).Split(",").Contains(hdnItemClassification.Value)) Then

                                If dr("tintOfferTriggerValueType") = 1 Then 'Quantity
                                    Dim freeQuantity As Int32 = CCommon.ToInteger(dr("fltDiscountValue"))
                                    Dim requiredQty As Int32 = CCommon.ToInteger(dr("fltOfferTriggerValue"))
                                    Dim usedQty As Int32 = 0

                                    If dr("tintOfferBasedOn") = "2" Then
                                        usedQty += dsTemp.Tables(0).Select("bitPromotionTriggered=TRUE AND numPromotionID=" & dr("numProID") & " AND numItemClassification=" & hdnItemClassification.Value).Sum(Function(x) x("numUnitHour") * x("UOMConversionFactor"))
                                    Else
                                        usedQty += dsTemp.Tables(0).Select("bitPromotionTriggered=TRUE AND numPromotionID=" & dr("numProID") & " AND numItemCode=" & hdnCurrentSelectedItem.Value).Sum(Function(x) x("numUnitHour") * x("UOMConversionFactor"))
                                    End If
                                    requiredQty -= usedQty

                                    If CCommon.ToInteger(txtUnits.Text * txtUOMConversionFactor.Text) > requiredQty Then
                                        If CCommon.ToInteger(txtUnits.Text * txtUOMConversionFactor.Text) - requiredQty = 0 Then
                                            CCommon.SetValueInDecimalFormat(0, txtItemDiscount)
                                        Else
                                            If freeQuantity > (CCommon.ToInteger(txtUnits.Text * txtUOMConversionFactor.Text) - requiredQty) Then
                                                CCommon.SetValueInDecimalFormat((CCommon.ToDecimal(txtUnits.Text * txtUOMConversionFactor.Text) - requiredQty) * hdnListPrice.Value, txtItemDiscount)
                                            Else
                                                CCommon.SetValueInDecimalFormat(freeQuantity * hdnListPrice.Value, txtItemDiscount)
                                            End If
                                        End If

                                        radAmt.Checked = True
                                        radPer.Checked = False
                                    End If
                                Else 'Amount
                                    Dim freeQuantity As Int32 = CCommon.ToInteger(dr("fltDiscountValue"))
                                    Dim requiredAmnount As Int32 = CCommon.ToInteger(dr("fltOfferTriggerValue"))
                                    Dim usedAmount As Int32 = 0

                                    If dr("tintOfferBasedOn") = "2" Then
                                        usedAmount += dsTemp.Tables(0).Select("bitPromotionTriggered=TRUE AND numPromotionID=" & dr("numProID") & " AND numItemClassification=" & hdnItemClassification.Value).Sum(Function(x) x("numUnitHour") * x("UOMConversionFactor") * x("monPrice"))
                                    Else
                                        usedAmount += dsTemp.Tables(0).Select("bitPromotionTriggered=TRUE AND numPromotionID=" & dr("numProID") & " AND numItemCode=" & hdnCurrentSelectedItem.Value).Sum(Function(x) x("numUnitHour") * x("UOMConversionFactor") * x("monPrice"))
                                    End If

                                    requiredAmnount -= usedAmount

                                    If CCommon.ToInteger(txtUnits.Text * txtUOMConversionFactor.Text * hdnListPrice.Value) >= requiredAmnount Then
                                        If CCommon.ToInteger(txtUnits.Text * txtUOMConversionFactor.Text * hdnListPrice.Value) - requiredAmnount = 0 Then
                                            CCommon.SetValueInDecimalFormat(0, txtItemDiscount)
                                        Else
                                            Dim remainingAmount As Int32 = CCommon.ToInteger(txtUnits.Text * txtUOMConversionFactor.Text * hdnListPrice.Value) - requiredAmnount
                                            Dim discountedQty As Int32 = CCommon.ToInteger(Math.Truncate(remainingAmount / hdnListPrice.Value))
                                            CCommon.SetValueInDecimalFormat(discountedQty * hdnListPrice.Value, txtItemDiscount)
                                        End If

                                        radAmt.Checked = True
                                        radPer.Checked = False
                                    End If
                                End If
                            Else
                                CCommon.SetValueInDecimalFormat(0, txtItemDiscount)
                                radPer.Checked = True
                                radAmt.Checked = False
                            End If
                        End If
                    End If

                    CCommon.SetValueInDecimalFormat(hdnListPrice.Value, txtprice)
                    hdnUnitCost.Value = CCommon.ToDecimal(txtprice.Text)

                    lkbAddPromotion.Text = "Remove Promotion"

                    lkbPriceLevel.Visible = False
                    lkbPriceRule.Visible = False
                    lkbLastPrice.Visible = False
                    lkbListPrice.Visible = False

                    radWareHouse.Enabled = False
                    txtUnits.Enabled = False
                    radcmbUOM.Enabled = False
                    ddlPriceLevel.Enabled = False
                    txtprice.Enabled = False
                    txtItemDiscount.Enabled = False
                    radPer.Enabled = False
                    radAmt.Enabled = False

                    If Not ViewState("UsedPromotion") Is Nothing Then
                        Dim dtUsedPromotion As DataTable = DirectCast(ViewState("UsedPromotion"), DataTable)

                        For Each drPromotion As DataRow In DirectCast(ViewState("DataTableTempPromotion"), DataTable).Rows
                            dtUsedPromotion.ImportRow(drPromotion)
                        Next

                        ViewState("UsedPromotion") = dtUsedPromotion
                    Else
                        ViewState("UsedPromotion") = DirectCast(ViewState("DataTableTempPromotion"), DataTable)
                    End If

                    UpdateDetails()

                Else
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "RequireCoupnCode", "alert('Something went wrong.');", True)
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnRemovePromotion_Click(sender As Object, e As EventArgs) Handles btnRemovePromotion.Click
        Try
            If CCommon.ToBool(hdnPromotionTriggered.Value) Then
                'First remove all the items where promotion is applied.
                Dim rows As DataRow() = dsTemp.Tables(0).Select("numPromotionID=" & hdnPromotionID.Value & " AND bitPromotionTriggered = FALSE")

                For Each dr As DataRow In rows
                    dsTemp.Tables(0).Rows.Remove(dr)
                Next

                'Remove disocunt from items from which promotion is trigerred
                rows = dsTemp.Tables(0).Select("numPromotionID=" & hdnPromotionID.Value & " AND bitPromotionTriggered = TRUE")
                For Each drItem As DataRow In rows
                    Dim objItem As New CItems
                    objItem.byteMode = CCommon.ToShort(OppType)
                    objItem.ItemCode = CCommon.ToLong(drItem("numItemCode"))
                    objItem.WareHouseItemID = CCommon.ToLong(drItem("numWarehouseItmsID"))
                    objItem.GetItemAndWarehouseDetails()

                    'Start - Get Tax Detail
                    Dim strApplicable As String
                    Dim dtItemTax As DataTable
                    Dim chkTaxItems As CheckBoxList

                    If OppType = 1 Then
                        chkTaxItems = CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList)

                        'objItem.DomainID = Session("DomainID")
                        'dtItemTax = objItems.ItemTax()

                        'For Each drTax As DataRow In dtItemTax.Rows
                        '    strApplicable = strApplicable & drTax("bitApplicable") & ","
                        'Next

                        'strApplicable = strApplicable & objItems.Taxable
                    End If

                    objCommon = New CCommon
                    objCommon.DomainID = CCommon.ToLong(Session("DomainID"))

                    ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon, dsTemp, False, drItem("charItemType"),
                                                                   drItem("ItemType"), drItem("DropShip"), "", drItem("numItemCode"),
                                                                   drItem("numUnitHour"),
                                                                   objItem.ListPrice,
                                                                   CCommon.ToString(drItem("vcItemDesc")),
                                                                   CCommon.ToLong(drItem("numWarehouseItmsID")), "", CCommon.ToString(drItem("Warehouse")),
                                                                   drItem("numoppitemtCode"), CCommon.ToString(drItem("vcModelID")),
                                                                   drItem("numUOM"), drItem("vcUOMName"), drItem("UOMConversionFactor"),
                                                                   _PageType.ToString(), True,
                                                                   0, strApplicable, txtTax.Text, txtCustomerPartNo.Text, chkTaxItems,
                                                                   drItem("bitWorkOrder"), CCommon.ToString(drItem("vcInstruction")), CCommon.ToString(drItem("bintCompliationDate")),
                                                                   CCommon.ToLong(drItem("numWOAssignedTo")),
                                                                   drItem("numSOVendorId"),
                                                                   drItem("numProjectID"), drItem("numProjectStageID"),
                                                                   objItem:=objItem,
                                                                   vcBaseUOMName:=drItem("vcBaseUOMName"),
                                                                   numPrimaryVendorID:=CCommon.ToLong(drItem("numVendorID")),
                                                                   fltItemWeight:=CCommon.ToLong(hdnItemWeight.Value), IsFreeShipping:=CCommon.ToBool(hdnFreeShipping.Value),
                                                                   fltHeight:=CCommon.ToDouble(hdnHeight.Value), fltWidth:=CCommon.ToDouble(hdnWidth.Value),
                                                                   fltLength:=CCommon.ToDouble(hdnLength.Value), strSKU:=CCommon.ToString(hdnSKU.Value), salePrice:=objItem.ListPrice, numMaxWOQty:=drItem("numMaxWorkOrderQty"), numPromotionID:=0, IsPromotionTriggered:=False, vcPromotionDetail:="")
                Next

                UpdateDetails()
            ElseIf ViewState("DataTableTempPromotion") Is Nothing Then
                lkbAddPromotion.Visible = False
            End If

            CCommon.SetValueInDecimalFormat(hdnListPrice.Value, txtprice)
            CCommon.SetValueInDecimalFormat(0, txtItemDiscount)
            radPer.Checked = True
            hdnPromotionID.Value = ""
            hdnPromotionTriggered.Value = False
            hdnPromotionDescription.Value = ""

            lkbPriceLevel.Visible = True
            lkbPriceRule.Visible = True
            lkbLastPrice.Visible = True
            lkbListPrice.Visible = True

            lkbListPrice.Text = "<i class=""fa fa-check""></i>&nbsp;Used List Price (" & hdnListPrice.Value & ")"
            lkbPriceLevel.Text = lkbPriceLevel.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "").Replace("Used", "Add")
            lkbPriceRule.Text = lkbPriceRule.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "").Replace("Used", "Add")
            lkbLastPrice.Text = lkbLastPrice.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "").Replace("Used", "Add")
            lkbAddPromotion.Text = "Use Promotion"

            radWareHouse.Enabled = True
            txtUnits.Enabled = True
            radcmbUOM.Enabled = True
            ddlPriceLevel.Enabled = True
            If hdnEditUnitPriceRight.Value <> "False" Then
                txtprice.Enabled = True
            End If
            txtItemDiscount.Enabled = True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnDeleteItem_Click(sender As Object, e As EventArgs) Handles btnDeleteItem.Click
        Try
            Dim dtUsedPromotion As DataTable = DirectCast(ViewState("UsedPromotion"), DataTable)
            Dim row As DataRow = dsTemp.Tables(0).Select("numoppitemtCode=" & hdnDeleteItemID.Value)(0)

            If CCommon.ToBool(row("bitPromotionTriggered")) Then
                Dim rowPromotion As DataRow = dtUsedPromotion.Select("numProId=" & row("numPromotionID"))(0)
                dtUsedPromotion.Rows.Remove(rowPromotion)

                For Each dr As DataRow In dsTemp.Tables(0).Select("numPromotionID=" & row("numPromotionID"))
                    dsTemp.Tables(0).Rows.Remove(dr)
                Next
            End If

            dtUsedPromotion.AcceptChanges()
            dsTemp.AcceptChanges()
            ViewState("UsedPromotion") = dtUsedPromotion
            hdnDeleteItemID.Value = ""
            UpdateDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub hplRelatedItems_Click(sender As Object, e As EventArgs) Handles hplRelatedItems.Click
        Try
            RelatedItems(False)
            'Dim objItems As New CItems
            'objItems.DomainID = CCommon.ToLong(Session("DomainID"))
            'objItems.ParentItemCode = hdnCurrentSelectedItem.Value
            'objItems.byteMode = 2
            'gvRelatedItems.DataSource = objItems.GetSimilarItem
            'gvRelatedItems.DataBind()

            'divRelatedItems.Style.Add("display", "")
            'UpdatePanelActionWindow.Update()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnCloseRelatedItems_Click(sender As Object, e As EventArgs) Handles btnCloseRelatedItems.Click
        Try
            hdnRelatedItemPromotionID.Value = ""

            gvRelatedItems.DataSource = Nothing
            gvRelatedItems.DataBind()

            divRelatedItems.Style.Add("display", "none")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub gvRelatedItems_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvRelatedItems.RowCommand
        Try
            If e.CommandName = "Add" Then
                Dim confirmValue As String = Request.Form("confirm_value")
                ' event occures
                If confirmValue = "No" Then
                    Return
                End If
                Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)

                Dim dtTable As DataTable
                Dim numUOMId As Long = 0, vcUOMName As String
                Dim objItems As New CItems
                objItems.ItemCode = e.CommandArgument
                dtTable = objItems.ItemDetails


                If dtTable.Rows.Count > 0 Then
                    numUOMId = dtTable.Rows(0).Item("numSaleUnit")
                    vcUOMName = dtTable.Rows(0).Item("vcSaleUnit")

                    'UOM Conversion Factor
                    Dim dtUnit As DataTable
                    objCommon.DomainID = Session("DomainID")
                    objCommon.ItemCode = e.CommandArgument
                    objCommon.UnitId = numUOMId
                    dtUnit = objCommon.GetItemUOMConversion()
                    Dim decUOMConversionFactor = dtUnit(0)("BaseUOMConversion")

                    'Start - Get Tax Detail
                    Dim strApplicable As String
                    Dim dtItemTax As DataTable

                    If OppType = 1 Then
                        dtItemTax = objItems.ItemTax()

                        For Each dr As DataRow In dtItemTax.Rows
                            strApplicable = strApplicable & dr("bitApplicable") & ","
                        Next

                        strApplicable = strApplicable & objItems.Taxable
                    End If

                    Dim units As Integer = CCommon.ToInteger(DirectCast(row.FindControl("txtQty"), TextBox).Text)
                    Dim numWarehouseItemID As Long = CCommon.ToLong(DirectCast(row.FindControl("hdnWarehouseID"), HiddenField).Value)
                    Dim price As Decimal = CCommon.ToDecimal(DirectCast(row.FindControl("hdnPrice"), HiddenField).Value)
                    Dim decDiscount As Decimal = 0
                    Dim IsDiscountInPer As Boolean = True
                    Dim numPromotionID As Long = CCommon.ToLong(hdnRelatedItemPromotionID.Value)
                    Dim vcPromotionDetail As String = ""
                    Dim salePrice As Decimal = price

                    If numPromotionID > 0 Then
                        Dim drPromotion As DataRow = DirectCast(ViewState("UsedPromotion"), DataTable).Select("numProId=" & numPromotionID)(0)
                        decDiscount = CCommon.ToDecimal(drPromotion("fltDiscountValue"))
                        IsDiscountInPer = IIf(drPromotion("tintDiscountType") = "1", True, False)

                        If Not IsDiscountInPer AndAlso CCommon.ToInteger(decDiscount) > CCommon.ToInteger(units * decUOMConversionFactor * price) Then
                            decDiscount = CCommon.ToInteger(units * decUOMConversionFactor * price)
                        End If


                        vcPromotionDetail = CCommon.ToString(drPromotion("vcPromotionDescription"))

                        If IsDiscountInPer Then
                            salePrice = price - (price * (decDiscount / 100))
                        Else
                            If units > 0 Then
                                salePrice = ((price * units * decUOMConversionFactor) - decDiscount) / (units * decUOMConversionFactor)
                            Else
                                salePrice = 0
                            End If
                        End If
                    End If


                    objItems.DomainID = CCommon.ToLong(Session("DomainID"))
                    If Not _PageType = PageType.AddEditOrder Then
                        If IsStockTransfer Then
                            objItems.DivisionID = IIf(CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue)
                        Else
                            objItems.DivisionID = IIf(CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue)
                        End If
                    Else
                        objItems.DivisionID = CCommon.ToLong(GetQueryStringVal("DivId"))
                    End If
                    objItems.byteMode = CCommon.ToShort(OppType)
                    objItems.ItemCode = e.CommandArgument
                    objItems.WareHouseItemID = numWarehouseItemID
                    objItems.GetItemAndWarehouseDetails()

                    Dim chkTaxItems As CheckBoxList
                    If OppType = 1 Then
                        chkTaxItems = CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList)
                    End If

                    objCommon = New CCommon
                    objCommon.DomainID = CCommon.ToLong(Session("DomainID"))

                    ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon, dsTemp, True, objItems.ItemType, "", False, objItems.KitParent, objItems.ItemCode, units, price, objItems.ItemDesc, numWarehouseItemID,
                                                                                       objItems.ItemName, "", 0, objItems.ModelID, numUOMId, vcUOMName, decUOMConversionFactor, _PageType.ToString(), IsDiscountInPer, decDiscount, strApplicable, txtTax.Text, txtCustomerPartNo.Text, chkTaxItems,
                                                                                        numProjectID:=CCommon.ToLong(GetQueryStringVal("Source")),
                                                                                         numProjectStageID:=CCommon.ToLong(GetQueryStringVal("StageID")),
                                                                                        vcBaseUOMName:=dtTable.Rows(0).Item("vcBaseUnit"),
                                                                                        objItem:=objItems, primaryVendorCost:=0, salePrice:=salePrice, numPromotionID:=numPromotionID,
                                                                                        IsPromotionTriggered:=False,
                                                                                        vcPromotionDetail:=vcPromotionDetail)

                    UpdateDetails()

                    RegisterPostBackControl()
                    Dim btnAdd As Button
                    Dim index As Integer = Convert.ToInt32(e.CommandArgument)
                    Dim gvr As GridViewRow = CType(((CType(e.CommandSource, Button)).NamingContainer), GridViewRow)
                    Dim RowIndex As Integer = gvr.RowIndex
                    btnAdd = gvRelatedItems.Rows(RowIndex).FindControl("lnkAdd")
                    If (btnAdd.Text <> "Added") Then
                        btnAdd.Text = "Added"
                        btnAdd.Enabled = False
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub hplLastPrices_Click(sender As Object, e As EventArgs) Handles hplLastPrices.Click
        Try
            BindPriceHistory(False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub BindPriceHistory(ByVal forSelectedCustomer As Boolean)
        Try
            Dim objOpp As New COpportunities
            objOpp.DomainID = CCommon.ToLong(Session("DomainID"))
            objOpp.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            objOpp.ItemCode = hdnCurrentSelectedItem.Value
            objOpp.OppType = 1
            If forSelectedCustomer Then
                objOpp.DivisionID = CCommon.ToLong(CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue)
            End If
            gvPriceHistory.DataSource = objOpp.GetItemPriceHistory()
            gvPriceHistory.DataBind()

            divItemPriceHistory.Style.Add("display", "")
            UpdatePanelActionWindow.Update()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnClosePriceHistory_Click(sender As Object, e As EventArgs) Handles btnClosePriceHistory.Click
        Try
            gvPriceHistory.DataSource = Nothing
            gvPriceHistory.DataBind()
            divItemPriceHistory.Style.Add("display", "none")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub gvPriceHistory_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvPriceHistory.RowCommand
        Try
            If e.CommandName = "Add" Then
                Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)

                txtUnits.Text = CCommon.ToString(DirectCast(row.FindControl("hdnUnits"), HiddenField).Value)
                CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(DirectCast(row.FindControl("hdnPrice"), HiddenField).Value), txtprice)
                CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(DirectCast(row.FindControl("hdnDiscount"), HiddenField).Value), txtItemDiscount)
                If CCommon.ToString(DirectCast(row.FindControl("hdnDiscount"), HiddenField).Value) = "1" Then
                    radAmt.Checked = True
                Else
                    radPer.Checked = True
                End If
                txtUOMConversionFactor.Text = CCommon.ToString(DirectCast(row.FindControl("hdnUOMConversionFactor"), HiddenField).Value)
                If Not radcmbUOM.Items.FindItemByValue(DirectCast(row.FindControl("hdnUOMId"), HiddenField).Value) Is Nothing Then
                    radcmbUOM.Items.FindItemByValue(DirectCast(row.FindControl("hdnUOMId"), HiddenField).Value).Selected = True
                End If

                hdnPricingBasedOn.Value = "3"

                lkbLastPrice.Text = "<i class=""fa fa-check""></i>&nbsp;Used Last Price (" & txtprice.Text & "," & CCommon.ToString(DirectCast(row.FindControl("hdnDate"), HiddenField).Value) & ")"
                lkbPriceLevel.Text = lkbPriceLevel.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "").Replace("Used", "Add")
                lkbPriceRule.Text = lkbPriceRule.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "").Replace("Used", "Add")
                lkbListPrice.Text = lkbListPrice.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "").Replace("Used", "Add")

                divItemPriceHistory.Style.Add("display", "none")
                UpdatePanelActionWindow.Update()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub lnkBtnUpdateOrder_Click(sender As Object, e As EventArgs)
        Try

            Dim dtItem As DataTable
            Dim dtSerItem As DataTable
            dtItem = dsTemp.Tables(0)
            dtItem.DefaultView.Sort = "vcItemName ASC"
            dtItem = dtItem.DefaultView.ToTable()
            dtItem.AcceptChanges()

            Dim i As Long = 1
            For Each dr As DataRow In dtItem.Rows
                dr("numSortOrder") = i
                i = i + 1
                dr.AcceptChanges()
            Next
            Dim perserveChanges As Boolean = True
            Dim msAction As System.Data.MissingSchemaAction = System.Data.MissingSchemaAction.Ignore

            Dim changes As System.Data.DataTable = dsTemp.Tables(0).GetChanges()
            If changes IsNot Nothing Then
                changes.Merge(dtItem, perserveChanges, msAction)
                dsTemp.Tables(0).Merge(changes, False, msAction)
            Else
                dsTemp.Tables(0).Merge(dtItem, False, msAction)
            End If
            UpdateDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub txtCouponCode_TextChanged(sender As Object, e As EventArgs)
        LoadSelectedItemDetail(hdnCurrentSelectedItem.Value)
    End Sub



    Private Sub chkPriceHistoryByCustomer_CheckedChanged(sender As Object, e As EventArgs) Handles chkPriceHistoryByCustomer.CheckedChanged
        Try
            BindPriceHistory(chkPriceHistoryByCustomer.Checked)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub radcmbUOM_SelectedIndexChanged(ByVal sender As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs) Handles radcmbUOM.SelectedIndexChanged
        Try
            If CCommon.ToLong(hdnCurrentSelectedItem.Value) > 0 Then
                ItemFieldsChanged(hdnCurrentSelectedItem.Value, If(CCommon.ToLong(txtHidEditOppItem.Text) > 0, True, False))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub



    Private Sub gvRelatedItems_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvRelatedItems.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lnkAdd As Button = TryCast(e.Row.FindControl("lnkAdd"), Button)
                Dim lblOutOfStock As Label = TryCast(e.Row.FindControl("lblOutOfStock"), Label)

                Dim drv As DataRowView = TryCast(e.Row.DataItem, DataRowView)
                If drv IsNot Nothing Then
                    Dim txtQty As TextBox = DirectCast(e.Row.FindControl("txtQty"), TextBox)
                    Dim qty As Integer = CCommon.ToInteger(txtQty.Text)
                    Dim price As Decimal = CCommon.ToDecimal(drv.Row("monListPrice"))
                    Dim total As Decimal = CCommon.ToDecimal(qty * price)
                    Dim lblTotal As Label = DirectCast(e.Row.FindControl("lblTotal"), Label)
                    lblTotal.Text = CCommon.ToString(Session("Currency")) + " " + CCommon.ToString(Math.Round(total, 2))

                    Dim _numOnHand = CCommon.ToInteger(drv.Row("numOnHand"))
                    If (_numOnHand = 0) Then
                        lnkAdd.Visible = False
                        lblOutOfStock.Visible = True
                    End If

                    Dim ItemImagePath = "/Images/DefaultProduct.png"
                    If Not String.IsNullOrEmpty(drv.Row("vcPathForTImage").ToString()) Then
                        ItemImagePath = GetImagePath(ReplaceAllSpaces(drv.Row("vcPathForTImage").ToString()))
                    End If
                    drv.Row("vcPathForTImage") = ItemImagePath

                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub btnSaveEditKit_Click(sender As Object, e As EventArgs)
        Try
            Dim vcSelectedItems As String = ""
            For Each item As GridViewRow In gvChildItems.Rows
                vcSelectedItems = vcSelectedItems & If(vcSelectedItems.Length > 0, ",0-", "0-") & (DirectCast(item.FindControl("lblItemCode"), Label).Text & "-" & DirectCast(item.FindControl("txtUnits"), TextBox).Text & "-" & DirectCast(item.FindControl("txtSequence"), TextBox).Text)
            Next

            hdnKitChildItems.Value = vcSelectedItems

            ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "HideEditKitWindow", "$('#divEditKit').hide();", True)

            gvChildItems.DataSource = Nothing
            gvChildItems.DataBind()

            divKitPrice.Visible = False
            lblKitPrice.Text = ""
            lblKitPriceType.Text = ""
            hdnKitPricing.Value = ""
            hdnKitListPrice.Value = ""

            UpdatePanel3.Update()

            ItemFieldsChanged(hdnCurrentSelectedItem.Value, False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub btnAddChilItem_Click(sender As Object, e As EventArgs)
        Try
            If CCommon.ToLong(hdnSelectedChildItem.Value) > 0 Then
                Dim vcSelectedItems As String = ""
                For Each item As GridViewRow In gvChildItems.Rows
                    vcSelectedItems = vcSelectedItems & If(vcSelectedItems.Length > 0, ",0-", "0-") & (DirectCast(item.FindControl("lblItemCode"), Label).Text & "-" & DirectCast(item.FindControl("txtUnits"), TextBox).Text & "-" & DirectCast(item.FindControl("txtSequence"), TextBox).Text)
                Next

                Dim objItem As New CItems
                objItem.ItemCode = CCommon.ToLong(hdnCurrentSelectedItem.Value)
                objItem.ChildItemID = CCommon.ToLong(hdnSelectedChildItem.Value)
                Dim dsItem As DataSet = objItem.GetSelectedChildItemDetails(vcSelectedItems, False)

                If Not dsItem Is Nothing And dsItem.Tables.Count > 1 Then
                    If CCommon.ToBool(dsItem.Tables(0).Rows(0)("bitCalAmtBasedonDepItems")) Then
                        lblKitPrice.Text = CCommon.ToDouble(dsItem.Tables(0).Rows(0)("monPrice"))
                        hdnKitPricing.Value = CCommon.ToShort(dsItem.Tables(0).Rows(0)("tintKitAssemblyPriceBasedOn"))
                        hdnKitListPrice.Value = CCommon.ToDouble(dsItem.Tables(0).Rows(0)("monListPrice"))
                    Else
                        lblKitPrice.Text = ""
                        hdnKitPricing.Value = ""
                        hdnKitListPrice.Value = ""
                    End If

                    gvChildItems.DataSource = dsItem.Tables(1)
                    gvChildItems.DataBind()
                End If

                hdnSelectedChildItem.Value = ""
                ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "ClearSelectedChildItem", "$('#txtChildItem').select2('data', null);", True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub btnRemoveChilItem_Click(sender As Object, e As EventArgs)
        Try
            Dim vcSelectedItems As String = ""
            For Each item As GridViewRow In gvChildItems.Rows
                If Not DirectCast(item.FindControl("chkSelect"), CheckBox).Checked Then
                    vcSelectedItems = vcSelectedItems & If(vcSelectedItems.Length > 0, ",0-", "0-") & (DirectCast(item.FindControl("lblItemCode"), Label).Text & "-" & DirectCast(item.FindControl("txtUnits"), TextBox).Text & "-" & DirectCast(item.FindControl("txtSequence"), TextBox).Text)
                End If
            Next

            Dim objItem As New CItems
            objItem.ItemCode = CCommon.ToLong(hdnCurrentSelectedItem.Value)
            objItem.ChildItemID = CCommon.ToLong(hdnSelectedChildItem.Value)
            Dim dsItem As DataSet = objItem.GetSelectedChildItemDetails(vcSelectedItems, False)

            If Not dsItem Is Nothing And dsItem.Tables.Count > 1 Then
                If CCommon.ToBool(dsItem.Tables(0).Rows(0)("bitCalAmtBasedonDepItems")) Then
                    lblKitPrice.Text = CCommon.ToDouble(dsItem.Tables(0).Rows(0)("monPrice"))
                    hdnKitPricing.Value = CCommon.ToShort(dsItem.Tables(0).Rows(0)("tintKitAssemblyPriceBasedOn"))
                    hdnKitListPrice.Value = CCommon.ToDouble(dsItem.Tables(0).Rows(0)("monListPrice"))
                Else
                    lblKitPrice.Text = ""
                    hdnKitPricing.Value = ""
                    hdnKitListPrice.Value = ""
                End If

                gvChildItems.DataSource = dsItem.Tables(1)
                gvChildItems.DataBind()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

#Region "Multi-Select"
    Protected Sub btn2_Click(sender As Object, e As EventArgs)
        Try
            divMultiSelect.Visible = True
            If Not _PageType = PageType.AddEditOrder Then
                BindMultiSelectItemsToDatatable(CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue)
            Else
                BindMultiSelectItemsToDatatable(CType(Me.Parent.FindControl("hdnCmbDivisionID"), HiddenField).Value)
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Function CalculatePriceForMultiSelectItem(ByVal ItemCode As Long, ByVal Units As Double, ByVal UOMId As Long, ByVal KitChildItems As String) As DataSet
        Dim objOpportunity As New COpportunities
        objOpportunity.DomainID = Session("DomainID")

        If Not _PageType = PageType.AddEditOrder Then
            objOpportunity.DivisionID = CCommon.ToLong(CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue)
        Else
            objOpportunity.DivisionID = CCommon.ToLong(CType(Me.Parent.FindControl("hdnCmbDivisionID"), HiddenField).Value)
        End If

        objOpportunity.OppType = OppType
        objOpportunity.ItemCode = ItemCode
        objOpportunity.UnitHour = Units
        objOpportunity.WarehouseItmsID = 0
        If (_PageType = PageType.AddEditOrder) Then
            objOpportunity.CurrencyID = CCommon.ToLong(CType(Me.Parent.FindControl("hdnCurrencyID"), HiddenField).Value)
        Else
            objOpportunity.CurrencyID = CCommon.ToLong(CType(Me.Parent.FindControl("ddlCurrency"), DropDownList).SelectedValue)
        End If
        Dim ds As DataSet = objOpportunity.GetOrderItemDetails(UOMId, KitChildItems, CCommon.ToLong(hdnCountry.Value))
        Return ds
    End Function

    Private Sub BindMultiSelectItemsToDatatable(ByVal lngCustId As Long)
        Try
            If Not ViewState("dtMultiSelect") Is Nothing Then
                dtMultiSelect = DirectCast(ViewState("dtMultiSelect"), DataTable)
            Else
                dtMultiSelect = New DataTable()
                dtMultiSelect.Columns.Add("ID", GetType(String))
                dtMultiSelect.Columns.Add("numItemCode", GetType(String))
                dtMultiSelect.Columns.Add("ItemName", GetType(String))
                dtMultiSelect.Columns.Add("numUnitHour", GetType(String))
                dtMultiSelect.Columns.Add("monPrice", GetType(String))
                dtMultiSelect.Columns.Add("dtReleaseDate", GetType(String))
                dtMultiSelect.Columns.Add("numShipFrom", GetType(Long))
                dtMultiSelect.Columns.Add("vcWarehouse", GetType(String))
                dtMultiSelect.Columns.Add("numShipTo", GetType(Long))
                dtMultiSelect.Columns.Add("vcShipTo", GetType(String))
                dtMultiSelect.Columns.Add("Attr", GetType(String))
                dtMultiSelect.Columns.Add("numOnHand", GetType(String))
                dtMultiSelect.Columns.Add("numAllocation", GetType(String))
                dtMultiSelect.Columns.Add("numSaleUnit", GetType(String))
                dtMultiSelect.Columns.Add("UOMConversionFactor", GetType(Double))
                dtMultiSelect.Columns.Add("charItemType", GetType(String))
            End If

            Dim objContact As New CContacts

            objContact.AddressType = CContacts.enmAddressType.ShipTo
            objContact.DomainID = Session("DomainID")
            objContact.RecordID = lngCustId
            objContact.AddresOf = CContacts.enmAddressOf.Organization
            objContact.byteMode = 2
            dtMultiSelectShipToAddr = objContact.GetAddressDetail()

            Dim objItem As New CItems
            Dim dtfromDB As DataTable = objItem.GetMultiSelectItems(Session("DomainID"), Session("MultiSelectItemCodes"), OppType)

            If Not dtfromDB Is Nothing AndAlso dtfromDB.Rows.Count > 0 Then
                For i As Integer = 0 To dtfromDB.Rows.Count - 1
                    Dim drnew As DataRow = dtMultiSelect.NewRow()
                    drnew("ID") = Guid.NewGuid().ToString()
                    drnew("numItemCode") = dtfromDB.Rows(i)("numItemCode")
                    drnew("ItemName") = dtfromDB.Rows(i)("ItemName")
                    drnew("Attr") = dtfromDB.Rows(i)("Attr")
                    drnew("numOnHand") = dtfromDB.Rows(i)("numOnHand")
                    drnew("numAllocation") = dtfromDB.Rows(i)("numAllocation")
                    drnew("numSaleUnit") = dtfromDB.Rows(i)("numSaleUnit")
                    drnew("UOMConversionFactor") = CCommon.ToDouble(dtfromDB.Rows(i)("fltUOMConversionFactor"))
                    drnew("charItemType") = CCommon.ToString(dtfromDB.Rows(i)("charItemType"))
                    dtMultiSelect.Rows.Add(drnew)
                Next
            End If

            ViewState("dtMultiSelect") = dtMultiSelect
            Session("MultiSelectItemCodes") = ""
            Session.Remove("MultiSelectItemCodes")
            gvMultiSelectItems.DataSource = dtMultiSelect
            gvMultiSelectItems.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Function AddMultiSelectItemsToItemGrid(objSelectedItem As SelectedItems, MaxRowOrder As Long, WareHouseItemID As Long, Quantity As Double, itemReleaseDate As String, ShipFromLocation As String, numShipToAddressID As Long, ShipToFullAddress As String, OnHandAllocation As String, Optional ByVal MultiSelectItemPrice As Decimal = 0)
        Try
            Dim objItem As New CItems
            objItem.DomainID = CCommon.ToLong(Session("DomainID"))
            If Not _PageType = PageType.AddEditOrder Then
                If IsStockTransfer Then
                    objItem.DivisionID = IIf(CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radcmbSTFrom"), RadComboBox).SelectedValue)
                Else
                    objItem.DivisionID = IIf(CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue)
                End If
            Else
                objItem.DivisionID = CCommon.ToLong(GetQueryStringVal("DivId"))
            End If
            objItem.byteMode = CCommon.ToShort(OppType)
            objItem.ItemCode = objSelectedItem.numItemCode
            objItem.WareHouseItemID = WareHouseItemID 'CCommon.ToLong(radWareHouse.SelectedValue)
            objItem.GetItemAndWarehouseDetails()
            objItem.ItemDesc = txtdesc.Text
            objItem.CustomerPartNo = txtCustomerPartNo.Text
            objItem.bitHasKitAsChild = objSelectedItem.bitHasKitAsChild
            objItem.vcKitChildItems = hdnKitChildItems.Value
            objItem.SelectedUOMID = radcmbUOM.SelectedValue
            objItem.SelectedUOM = IIf(radcmbUOM.SelectedValue > 0, radcmbUOM.Text, "")
            objItem.AttributeIDs = objSelectedItem.vcAttributeIDs
            objItem.Attributes = objSelectedItem.vcAttributes
            MaxRowOrder = MaxRowOrder + 1
            objItem.numSortOrder = MaxRowOrder

            If (OppType = 1) Then
                If (objItem.numContainer > 0) Then
                    Dim containerqty As Long = Math.Ceiling((CCommon.ToDouble(txtUnits.Text) * If(CCommon.ToDouble(txtUOMConversionFactor.Text) > 0, CCommon.ToDouble(txtUOMConversionFactor.Text), 1.0)) / objItem.numNoItemIntoContainer)

                    Dim objContainerItem As New CItems
                    objContainerItem.DomainID = CCommon.ToLong(Session("DomainID"))
                    objContainerItem.WarehouseID = objItem.WarehouseID
                    objContainerItem.ItemCode = objItem.numContainer
                    Dim dtContainer As DataTable = objContainerItem.GetItemWarehouseLocations()

                    If Not dtContainer Is Nothing AndAlso dtContainer.Rows.Count > 0 Then
                        Dim objItem1 As New CItems
                        objItem1.byteMode = CCommon.ToShort(OppType)
                        objItem1.ItemCode = objItem.numContainer
                        objItem1.WareHouseItemID = CCommon.ToLong(dtContainer.Rows(0)("numWarehouseItemID"))
                        objItem1.GetItemAndWarehouseDetails()

                        objItem1.bitHasKitAsChild = False
                        objItem1.vcKitChildItems = ""
                        MaxRowOrder = MaxRowOrder + 1
                        objItem1.numSortOrder = MaxRowOrder
                        objItem1.SelectedUOMID = objItem1.BaseUnit
                        objItem1.SelectedQuantity = containerqty
                        objItem1.AttributeIDs = ""
                        objItem1.Attributes = ""
                        objItem1.numNoItemIntoContainer = -1

                        If objItem.ItemType = "P" AndAlso IsDuplicate(objItem1.ItemCode, objItem1.WareHouseItemID, chkDropShip.Checked) Then
                            If objItem1.numNoItemIntoContainer = -1 Then
                                Dim dsContainer As DataSet
                                dsContainer = ViewState("SOItems")

                                Dim itemQtyInContainer As Double = 0
                                Dim containerMaxQty As Double = 0


                                Dim arrContainers As DataRow() = dsContainer.Tables(0).Select("numItemCode=" & objItem1.ItemCode & " AND numWarehouseItmsID=" & objItem1.WareHouseItemID)
                                If Not arrContainers Is Nothing AndAlso arrContainers.Length > 0 Then
                                    For Each drSameContainer As DataRow In arrContainers
                                        containerMaxQty = containerMaxQty + CCommon.ToDouble(drSameContainer("numUnitHour") * objItem.numNoItemIntoContainer)
                                    Next
                                End If

                                'Check if there are items exists which uses same container and has same qty which can be added to container and container qty is not fully used
                                Dim arrItems As DataRow() = dsContainer.Tables(0).Select("numContainer=" & objItem1.ItemCode & " AND numWarehouseID=" & objItem1.WarehouseID & " AND numContainerQty=" & objItem.numNoItemIntoContainer)
                                If Not arrItems Is Nothing AndAlso arrItems.Length > 0 Then
                                    For Each drSameContainer As DataRow In arrItems
                                        itemQtyInContainer = itemQtyInContainer + CCommon.ToDouble(drSameContainer("numUnitHour") * drSameContainer("UOMConversionFactor"))
                                    Next
                                End If

                                If containerMaxQty > 0 AndAlso itemQtyInContainer > 0 Then
                                    Dim itemCanbeAddedToContainer As Double = containerMaxQty - itemQtyInContainer

                                    If itemCanbeAddedToContainer < CCommon.ToDouble(txtUnits.Text) * If(CCommon.ToDouble(txtUOMConversionFactor.Text) > 0, CCommon.ToDouble(txtUOMConversionFactor.Text), 1.0) Then
                                        containerqty = containerqty - (CCommon.ToDouble(txtUnits.Text) * If(CCommon.ToDouble(txtUOMConversionFactor.Text) > 0, CCommon.ToDouble(txtUOMConversionFactor.Text), 1.0) - itemCanbeAddedToContainer)
                                        objItem1.SelectedQuantity = containerqty
                                        Dim drContainer As DataRow() = dsContainer.Tables(0).Select("numItemCode=" & objItem1.ItemCode & " AND numWarehouseItmsID=" & objItem1.WareHouseItemID)
                                        For Each drowContainer As DataRow In drContainer
                                            drowContainer("numUnitHour") = (drowContainer("numUnitHour")) + objItem1.SelectedQuantity
                                        Next
                                    End If
                                Else
                                    Dim drContainer As DataRow() = dsContainer.Tables(0).Select("numItemCode=" & objItem1.ItemCode & " AND numWarehouseItmsID=" & objItem1.WareHouseItemID)
                                    For Each drowContainer As DataRow In drContainer
                                        drowContainer("numUnitHour") = (drowContainer("numUnitHour")) + objItem1.SelectedQuantity
                                    Next
                                End If

                                AddItemToSession(objItem, Quantity, MultiSelectItemPrice, itemReleaseDate)
                                ViewState("SOItems") = dsContainer
                                dsTemp = dsContainer
                            End If
                        Else
                            AddItemToSession(objItem, Quantity, MultiSelectItemPrice, itemReleaseDate)
                            AddItemToSession(objItem1, Quantity, MultiSelectItemPrice, itemReleaseDate)
                        End If
                    Else
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "DuplicateItemWarehouse", "alert(""Item associated container item does not contain warehouse. Please add warehouse to container first."")", True)
                        Return ""
                    End If
                Else
                    AddItemToSession(objItem, Quantity, MultiSelectItemPrice, itemReleaseDate)
                End If
            Else
                AddItemToSession(objItem, Quantity, MultiSelectItemPrice, itemReleaseDate)
            End If

            If OppType = 1 Then
                AddSimilarRequiredRow()
            End If
            UpdateDetails()

        Catch ex As Exception

        End Try
    End Function

    Protected Sub txtMultiSelectUnits_TextChanged(sender As Object, e As EventArgs)

        Dim dr As DataRow
        Dim dtMultiSelect As DataTable = ViewState("dtMultiSelect")

        Dim txtUnits As TextBox = CType(sender, TextBox)
        Dim gvr As GridViewRow = CType(txtUnits.Parent.Parent, GridViewRow)
        Dim rowindex As Integer = gvr.RowIndex
        Dim ItemCode As Long = CCommon.ToLong(CType(gvMultiSelectItems.Rows(rowindex).FindControl("hdnItemCode"), HiddenField).Value)
        Dim uniqueID As String = CCommon.ToString(CType(gvMultiSelectItems.Rows(rowindex).FindControl("hdnID"), HiddenField).Value)

        Dim dRows As DataRow() = dtMultiSelect.Select("ID = '" & uniqueID & "'")
        If dRows.Length > 0 Then
            dr = dRows(0)

            dr("numUnitHour") = CCommon.ToLong(txtUnits.Text)
            Dim numUOMId As Long = CCommon.ToLong(dr("numSaleUnit"))
            Dim dsPrice As DataSet = CalculatePriceForMultiSelectItem(ItemCode, CCommon.ToLong(txtUnits.Text), numUOMId, "")
            If (dsPrice.Tables IsNot Nothing) Then
                If (dsPrice.Tables(0).Rows.Count > 0) Then
                    CType(gvMultiSelectItems.Rows(rowindex).FindControl("txtMultiSelectUnitPrice"), TextBox).Text = CCommon.ToDecimal(dsPrice.Tables(0).Rows(0)("monPrice"))
                    dr("monPrice") = CCommon.ToDecimal(dsPrice.Tables(0).Rows(0)("monPrice"))
                End If
            End If

            dr.AcceptChanges()
            dtMultiSelect.AcceptChanges()
            ViewState("dtMultiSelect") = dtMultiSelect
        End If
    End Sub

    Protected Sub rdcmbMultiSelectShipFrom_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)

        Dim dr As DataRow
        Dim dtMultiSelect As DataTable = ViewState("dtMultiSelect")
        Dim rdcmbShipFrom As RadComboBox = CType(sender, RadComboBox)
        Dim gvr As GridViewRow = CType(rdcmbShipFrom.Parent.Parent, GridViewRow)
        Dim rowindex As Integer = gvr.RowIndex
        Dim ItemCode As Long = CCommon.ToLong(CType(gvMultiSelectItems.Rows(rowindex).FindControl("hdnItemCode"), HiddenField).Value)
        Dim uniqueID As String = CCommon.ToString(CType(gvMultiSelectItems.Rows(rowindex).FindControl("hdnID"), HiddenField).Value)

        Dim dRows As DataRow() = dtMultiSelect.Select("ID = '" & uniqueID & "'")
        If dRows.Length > 0 Then
            dr = dRows(0)
            dr("numShipFrom") = rdcmbShipFrom.SelectedValue
            If Not rdcmbShipFrom.SelectedItem Is Nothing Then
                dr("vcWarehouse") = DirectCast(rdcmbShipFrom.SelectedItem.FindControl("lblWarehouse"), Label).Text
            End If


            dr.AcceptChanges()
            dtMultiSelect.AcceptChanges()
            ViewState("dtMultiSelect") = dtMultiSelect
        End If
    End Sub

    Protected Sub rdcmbMultiSelectShipTo_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Dim dr As DataRow
        Dim dtMultiSelect As DataTable = ViewState("dtMultiSelect")
        Dim rdcmbShipTo As RadComboBox = CType(sender, RadComboBox)
        Dim gvr As GridViewRow = CType(rdcmbShipTo.Parent.Parent, GridViewRow)
        Dim rowindex As Integer = gvr.RowIndex
        Dim ItemCode As Long = CCommon.ToLong(CType(gvMultiSelectItems.Rows(rowindex).FindControl("hdnItemCode"), HiddenField).Value)
        Dim uniqueID As String = CCommon.ToString(CType(gvMultiSelectItems.Rows(rowindex).FindControl("hdnID"), HiddenField).Value)

        Dim dRows As DataRow() = dtMultiSelect.Select("ID = '" & uniqueID & "'")
        If dRows.Length > 0 Then
            dr = dRows(0)
            dr("numShipTo") = rdcmbShipTo.SelectedValue
            If Not rdcmbShipTo.SelectedItem Is Nothing Then
                dr("vcShipTo") = rdcmbShipTo.SelectedItem.Text
            End If
            dr.AcceptChanges()
            dtMultiSelect.AcceptChanges()
            ViewState("dtMultiSelect") = dtMultiSelect
        End If
    End Sub

    Protected Sub txtMultiSelectUnitPrice_TextChanged(sender As Object, e As EventArgs)

        Dim dr As DataRow
        Dim dtMultiSelect As DataTable = ViewState("dtMultiSelect")

        Dim txtUnitPrice As TextBox = CType(sender, TextBox)
        Dim gvr As GridViewRow = CType(txtUnitPrice.Parent.Parent, GridViewRow)
        Dim rowindex As Integer = gvr.RowIndex
        Dim ItemCode As Long = CCommon.ToLong(CType(gvMultiSelectItems.Rows(rowindex).FindControl("hdnItemCode"), HiddenField).Value)
        Dim uniqueID As String = CCommon.ToString(CType(gvMultiSelectItems.Rows(rowindex).FindControl("hdnID"), HiddenField).Value)

        Dim dRows As DataRow() = dtMultiSelect.Select("ID = '" & uniqueID & "'")
        If dRows.Length > 0 Then
            dr = dRows(0)
        End If

        dr("monPrice") = CCommon.ToLong(txtUnitPrice.Text)

        dr.AcceptChanges()
        dtMultiSelect.AcceptChanges()
        ViewState("dtMultiSelect") = dtMultiSelect
    End Sub

    Protected Sub rdMultiSelectItemReleaseDate_SelectedDateChanged(sender As Object, e As Calendar.SelectedDateChangedEventArgs)
        Try
            Dim dr As DataRow
            Dim dtMultiSelect As DataTable = ViewState("dtMultiSelect")

            Dim rdMultiSelectItemReleaseDate As RadDatePicker = CType(sender, RadDatePicker)
            If Not rdMultiSelectItemReleaseDate Is Nothing Then
                Dim gvr As GridViewRow = DirectCast(rdMultiSelectItemReleaseDate.NamingContainer, GridViewRow)

                If Not gvr.FindControl("hdnID") Is Nothing Then
                    Dim uniqueID As String = CCommon.ToString(CType(gvr.FindControl("hdnID"), HiddenField).Value)

                    Dim dRows As DataRow() = dtMultiSelect.Select("ID = '" & uniqueID & "'")
                    If dRows.Length > 0 Then
                        dr = dRows(0)

                        dr("dtReleaseDate") = rdMultiSelectItemReleaseDate.SelectedDate

                        dr.AcceptChanges()
                        dtMultiSelect.AcceptChanges()
                        ViewState("dtMultiSelect") = dtMultiSelect
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub gvMultiSelectItems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        Try
            gvMultiSelectItems.PageIndex = e.NewPageIndex
            dtMultiSelect = CType(ViewState("dtMultiSelect"), DataTable)

            gvMultiSelectItems.DataSource = dtMultiSelect
            gvMultiSelectItems.DataBind()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub rdcmbMultiSelectShipFrom_ItemDataBound(sender As Object, e As RadComboBoxItemEventArgs)
        Try
            e.Item.Text = CType(e.Item.DataItem, DataRowView)("vcWareHouse").ToString() & " (" & CType(e.Item.DataItem, DataRowView)("numOnHand").ToString() & ") " & " ---- " & CType(e.Item.DataItem, DataRowView)("Attr").ToString().TrimEnd(",")
            e.Item.Value = CType(e.Item.DataItem, DataRowView)("numWareHouseItemId").ToString()
            CType(e.Item.FindControl("lblOnHandAllocation"), Label).Text = CType(e.Item.DataItem, DataRowView)("numOnHand").ToString() + "/" + CType(e.Item.DataItem, DataRowView)("numAllocation").ToString()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub imgDuplicateLineItem_Click(sender As Object, e As ImageClickEventArgs)
        Try
            Dim dgGridItem As DataGridItem = TryCast(sender.NamingContainer, DataGridItem)

            Dim numItemCode As Long = CCommon.ToLong(dgItems.DataKeys(dgGridItem.ItemIndex))

            Dim dtSelectedItems As DataTable = dsTemp.Tables(0)
            Dim drnewitem As DataRow

            Dim dRows As DataRow() = dtSelectedItems.Select("numoppitemtCode = " & CCommon.ToLong(CType(dgGridItem.FindControl("lblOppItemCode"), Label).Text).ToString())

            drnewitem = dRows(0)
            Dim MaxRowOrder As Long = dgItems.Items.Count + 1

            Dim dgLastItem As DataGridItem = TryCast(dgItems.Items(dgItems.Items.Count - 1), DataGridItem)
            drnewitem("numoppitemtCode") = CCommon.ToLong(CType(dgLastItem.FindControl("lblOppItemCode"), Label).Text) + 1

            Dim txtUnits As TextBox = CType(dgGridItem.FindControl("txtUnits"), TextBox)
            drnewitem("numUnitHour") = txtUnits.Text

            Dim txtUnitPrice As TextBox = CType(dgGridItem.FindControl("txtUnitPrice"), TextBox)
            drnewitem("monPrice") = txtUnitPrice.Text

            Dim itemReleaseDate As String
            Dim ShipFrom As String = ""
            Dim WareHouseItemID As Long = 0
            Dim Quantity As Double = CCommon.ToDouble(CType(dgGridItem.FindControl("txtUnits"), TextBox).Text)
            Dim Price As Decimal = CCommon.ToDecimal(CType(dgGridItem.FindControl("txtUnitPrice"), TextBox).Text)
            Dim UOMConversionFactor As Decimal = CCommon.ToDecimal(drnewitem("UOMConversionFactor"))
            itemReleaseDate = "" 'CCommon.ToString(drnewitem("ItemReleaseDate"))
            ' If CCommon.ToString(drnewitem("charItemType")) = "P" Then
            '  ShipFrom = CCommon.ToString(drnewitem("Location"))
            WareHouseItemID = CCommon.ToLong(drnewitem("numWarehouseItmsID"))

            ' Dim numShipToAddressID As Long = CCommon.ToLong(drnewitem("numShipToAddressID"))
            '  Dim ShipToFullAddress As String = CCommon.ToString(drnewitem("ShipToFullAddress"))

            '  Dim OnHandAllocation As String = CCommon.ToString(drnewitem("OnHandAllocation"))

            Dim objItem As New CItems
            objItem.ItemCode = numItemCode
            objItem.WareHouseItemID = WareHouseItemID
            objItem.Attributes = ""
            objItem.AttributeIDs = ""

            hdnCurrentSelectedItem.Value = numItemCode

            'Start - Get Tax Detail
            Dim strApplicable As String
            Dim dtItemTax As DataTable

            If (OppType = 1) Then
                dtItemTax = objItem.ItemTax()

                For Each dr As DataRow In dtItemTax.Rows
                    strApplicable = strApplicable & dr("bitApplicable") & ","
                Next

                strApplicable = strApplicable & objItem.Taxable
            End If
            'End - Get Tax Detail
            Dim chkTaxItems As CheckBoxList
            If OppType = 1 Then
                chkTaxItems = CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList)
            End If

            objCommon = New CCommon
            objCommon.DomainID = CCommon.ToLong(Session("DomainID"))

            ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon,
                                                                               dsTemp,
                                                                               True,
                                                                               CCommon.ToString(drnewitem("charItemType")),
                                                                               "",
                                                                               CCommon.ToBool(drnewitem("DropShip")),
                                                                               "",
                                                                               numItemCode,
                                                                               Quantity,
                                                                               Price,
                                                                               CCommon.ToString(drnewitem("vcItemDesc")), WareHouseItemID, CCommon.ToString(drnewitem("vcItemName")), "", 0, CCommon.ToString(drnewitem("vcModelID")), CCommon.ToLong(drnewitem("numUOM")), CCommon.ToString(drnewitem("vcUOMName")),
                                                                               If(UOMConversionFactor = 0, 1, CCommon.ToDouble(UOMConversionFactor)),
                                                                               _PageType.ToString(), False, CCommon.ToDecimal(drnewitem("fltDiscount")), strApplicable, CCommon.ToString(drnewitem("Tax0")), CCommon.ToString(drnewitem("CustomerPartNo")), chkTaxItems,
                                                                               CCommon.ToBool(drnewitem("bitWorkOrder")), CCommon.ToString(drnewitem("vcInstruction")), CCommon.ToString(drnewitem("bintCompliationDate")),
                                                                               CCommon.ToLong(drnewitem("numWOAssignedTo")),
                                                                               CCommon.ToLong(drnewitem("numVendorWareHouse")),
                                                                               numProjectID:=CCommon.ToLong(drnewitem("numProjectID")),
                                                                               numProjectStageID:=CCommon.ToLong(drnewitem("numProjectStageID")),
                                                                               vcBaseUOMName:=CCommon.ToString(drnewitem("vcBaseUOMName")),
                                                                               numPrimaryVendorID:=CCommon.ToLong(drnewitem("numVendorID")),
                                                                               numSOVendorId:=CCommon.ToLong(drnewitem("numSOVendorId")),
                                                                               strSKU:=CCommon.ToString(drnewitem("vcSKU")), objItem:=objItem,
                                                                               numMaxWOQty:=CCommon.ToInteger(drnewitem("numMaxWorkOrderQty")), vcAttributes:=objItem.Attributes, vcAttributeIDs:=objItem.AttributeIDs, numContainer:=objItem.numContainer, numContainerQty:=objItem.numNoItemIntoContainer,
                                                                               numItemClassification:=CCommon.ToLong(drnewitem("numItemClassification")),
                                                                               numPromotionID:=CCommon.ToLong(drnewitem("numPromotionID")),
                                                                               IsPromotionTriggered:=CCommon.ToBool(drnewitem("bitPromotionTriggered")),
                                                                               vcPromotionDetail:=CCommon.ToString(drnewitem("vcPromotionDetail")), numSortOrder:=MaxRowOrder, numCost:=CCommon.ToDouble(txtPUnitCost.Text),
                                                                               itemReleaseDate:=itemReleaseDate, ShipFromLocation:=ShipFrom)
            ' ShipToFullAddress:=ShipToFullAddress, OnHandAllocation:=OnHandAllocation )

            If OppType = 1 Then
                AddSimilarRequiredRow()
            End If
            UpdateDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub btnAddtoOpp_Click(sender As Object, e As EventArgs)
        dtMultiSelect = CType(ViewState("dtMultiSelect"), DataTable)

        If Not dtMultiSelect Is Nothing AndAlso dtMultiSelect.Rows.Count > 0 Then
            For Each itemrow As DataRow In dtMultiSelect.Rows
                If CCommon.ToDouble(itemrow("numUnitHour")) = 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "UnitsValidation", "alert(""Enter Units"");", True)
                    txtUnits.Focus()
                    Return
                End If
            Next

            dsTemp = ViewState("SOItems")

            Dim MaxRowOrder As Long
            MaxRowOrder = dsTemp.Tables(0).Rows.Count

            Dim itemReleaseDate As String
            Dim ShipFrom As String = ""
            Dim WareHouseItemID As Long = 0

            For Each itemrow As DataRow In dtMultiSelect.Rows

                Dim numItemCode As Long = CCommon.ToLong(itemrow("numItemCode"))
                Dim Quantity As Double = CCommon.ToDouble(itemrow("numUnitHour"))
                Dim Price As Decimal = CCommon.ToDecimal(itemrow("monPrice"))
                Dim UOMConversionFactor As Decimal = CCommon.ToDecimal(itemrow("UOMConversionFactor"))
                itemReleaseDate = CCommon.ToString(itemrow("dtReleaseDate"))
                If CCommon.ToString(itemrow("charItemType")) = "P" Then
                    ShipFrom = CCommon.ToString(itemrow("vcWarehouse"))
                    WareHouseItemID = CCommon.ToLong(itemrow("numShipFrom"))
                Else
                    ShipFrom = ""
                    WareHouseItemID = 0
                End If

                Dim numShipToAddressID As Long = CCommon.ToLong(itemrow("numShipTo"))
                Dim ShipToFullAddress As String = CCommon.ToString(itemrow("vcShipTo"))

                Dim OnHandAllocation As String = CCommon.ToString(itemrow("numOnHand")) + "/" + CCommon.ToString(itemrow("numAllocation"))

                Dim objSelectedItem As New SelectedItems
                objSelectedItem.numItemCode = numItemCode
                objSelectedItem.numQuantity = Quantity
                objSelectedItem.numSelectedUOM = CCommon.ToLong(itemrow("numSaleUnit"))
                objSelectedItem.vcSelectedUOM = ""
                objSelectedItem.numWarehouseItemID = WareHouseItemID
                objSelectedItem.vcAttributeIDs = ""
                objSelectedItem.vcAttributes = ""

                hdnCurrentSelectedItem.Value = numItemCode

                ' CalculateItemSalePrice(Price, Quantity, 0, True, IIf(UOMConversionFactor = 0, 1, UOMConversionFactor))
                AddMultiSelectItemsToItemGrid(objSelectedItem, MaxRowOrder, WareHouseItemID, Quantity, itemReleaseDate, ShipFrom, numShipToAddressID, ShipToFullAddress, OnHandAllocation, Price)
                MaxRowOrder = MaxRowOrder + 1
            Next
        End If
        clearControls()
        ViewState("dtMultiSelect") = Nothing
        Session("MultiSelectItemCodes") = ""
        Session.Remove("MultiSelectItemCodes")
        ScriptManager.RegisterClientScriptBlock(upnl, upnl.GetType(), "FocusItem", "FocuonItem();", True)
    End Sub

    Protected Sub btnClearItems_Click(sender As Object, e As EventArgs)
        Try
            If (gvMultiSelectItems IsNot Nothing) Then
                gvMultiSelectItems.DataSource = Nothing
                gvMultiSelectItems.DataBind()
                divMultiSelect.Visible = False
                ViewState("dtMultiSelect") = Nothing
                Session("MultiSelectItemCodes") = ""
                Session.Remove("MultiSelectItemCodes")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub gvMultiSelectItems_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If (e.Row.RowType = DataControlRowType.DataRow) Then
            objCommon = New CCommon
            Dim dataRowView As DataRowView = DirectCast(e.Row.DataItem, DataRowView)
            dtMultiSelect = DirectCast(ViewState("dtMultiSelect"), DataTable)
            Dim dr As DataRow = dtMultiSelect.Select("ID='" & CCommon.ToString(dataRowView("ID")) & "'")(0)

            Dim numItemCode As Long = CCommon.ToLong(CType(e.Row.FindControl("hdnItemCode"), HiddenField).Value)
            Dim strItemName As String = CType(e.Row.FindControl("hdnItemName"), HiddenField).Value
            Dim strAttr As String = CType(e.Row.FindControl("hdnAttr"), HiddenField).Value
            Dim numUOMId As Long = CCommon.ToLong(dr("numSaleUnit"))

            Dim dtItemReleaseDate As RadDatePicker = CType(e.Row.FindControl("rdMultiSelectItemReleaseDate"), RadDatePicker)
            If Not String.IsNullOrEmpty(CCommon.ToString(dr("dtReleaseDate"))) Then
                dtItemReleaseDate.SelectedDate = Convert.ToDateTime(dr("dtReleaseDate"))
            Else
                dtItemReleaseDate.SelectedDate = Date.Today
                dr("dtReleaseDate") = dtItemReleaseDate.SelectedDate
            End If

            If CCommon.ToDouble(dr("numUnitHour")) > 0 Then
                CType(e.Row.FindControl("txtMultiSelectUnits"), TextBox).Text = dr("numUnitHour")
            Else
                CType(e.Row.FindControl("txtMultiSelectUnits"), TextBox).Text = 1
                dr("numUnitHour") = 1
            End If

            Dim Units As Double = CCommon.ToDouble(CType(e.Row.FindControl("txtMultiSelectUnits"), TextBox).Text)
            '''''' Bind ShipFrom Addressess''''''''''''''''
            dtMultiSelectShipFromAddr = objCommon.GetWarehouseAttrBasedItem(numItemCode)
            If Not dtMultiSelectShipFromAddr Is Nothing AndAlso dtMultiSelectShipFromAddr.Rows.Count > 0 Then
                Dim rdcmbShipFrom As RadComboBox = CType(e.Row.FindControl("rdcmbMultiSelectShipFrom"), RadComboBox)
                rdcmbShipFrom.DataSource = dtMultiSelectShipFromAddr
                rdcmbShipFrom.DataBind()

                If CCommon.ToLong(dataRowView("numShipFrom")) > 0 AndAlso Not rdcmbShipFrom.Items.FindItemByValue(dataRowView("numShipFrom")) Is Nothing Then
                    rdcmbShipFrom.Items.FindItemByValue(dataRowView("numShipFrom")).Selected = True
                Else
                    dr("numShipFrom") = rdcmbShipFrom.SelectedValue
                    dr("vcWareHouse") = CCommon.ToString(dtMultiSelectShipFromAddr.Select("numWareHouseItemId=" & rdcmbShipFrom.SelectedValue)(0)("vcWareHouse"))
                End If
            End If

            ''''''''''''''''''''''''''''
            '''''''''''' Bind ShipTo Addresses'''''''''''''
            Dim cmbShipTo As RadComboBox = CType(e.Row.FindControl("rdcmbMultiSelectShipTo"), RadComboBox)
            cmbShipTo.DataSource = dtMultiSelectShipToAddr
            cmbShipTo.DataTextField = "FullAddress"
            cmbShipTo.DataValueField = "numAddressID"
            cmbShipTo.DataBind()

            If CCommon.ToLong(dataRowView("numShipTo")) > 0 AndAlso Not cmbShipTo.Items.FindItemByValue(dataRowView("numShipTo")) Is Nothing Then
                cmbShipTo.Items.FindItemByValue(dataRowView("numShipTo")).Selected = True
            Else
                dr("numShipTo") = CCommon.ToLong(cmbShipTo.SelectedValue)
                If Not cmbShipTo.SelectedItem Is Nothing Then
                    dr("vcShipTo") = CCommon.ToString(cmbShipTo.SelectedItem.Text)
                End If
            End If
            '''''''''''''''''''''''''''''''''''''''''''''

            '''''''''''''Display Item Name with Attribute Info
            Dim lblItemNameAttr As Label = CType(e.Row.FindControl("lblMultiSelectItemNAttr"), Label)
            If (strAttr IsNot "") Then
                lblItemNameAttr.Text = strItemName + " ( " + strAttr + " )"
            Else
                lblItemNameAttr.Text = strItemName
            End If
            ''''''''''''''''''''''''''''''''''''''''''

            '''''''''''Calculate Unit Price for Every Item and Display in Unit Price textbox
            If Not String.IsNullOrEmpty(CCommon.ToString(dr("monPrice"))) Then
                CType(e.Row.FindControl("txtMultiSelectUnitPrice"), TextBox).Text = dr("monPrice")
            Else
                Dim dsPrice As DataSet = CalculatePriceForMultiSelectItem(numItemCode, Units, numUOMId, "")
                If (dsPrice.Tables IsNot Nothing) Then
                    If (dsPrice.Tables(0).Rows.Count > 0) Then
                        CType(e.Row.FindControl("txtMultiSelectUnitPrice"), TextBox).Text = CCommon.ToDecimal(dsPrice.Tables(0).Rows(0)("monPrice"))
                        dr("monPrice") = CCommon.ToDecimal(dsPrice.Tables(0).Rows(0)("monPrice"))
                    End If
                End If
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''
            dr.AcceptChanges()
            dtMultiSelect.AcceptChanges()
            ViewState("dtMultiSelect") = dtMultiSelect
        End If
    End Sub

    Protected Sub gvMultiSelectItems_RowDeleting(sender As Object, e As GridViewDeleteEventArgs)

    End Sub

    Protected Sub gvMultiSelectItems_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        If (e.CommandName = "Delete") Then
            Dim dr As DataRow
            dtMultiSelect = CType(ViewState("dtMultiSelect"), DataTable)
            Dim uniqueID As String = CCommon.ToString(CType(gvMultiSelectItems.Rows(CCommon.ToInteger(e.CommandArgument)).FindControl("hdnID"), HiddenField).Value)

            Dim dRows As DataRow() = dtMultiSelect.Select("ID = '" & uniqueID & "'")

            If dRows.Length > 0 Then
                dr = dRows(0)
                dr.Delete()

                dtMultiSelect.AcceptChanges()
                ViewState("dtMultiSelect") = dtMultiSelect
                gvMultiSelectItems.DataSource = dtMultiSelect
                gvMultiSelectItems.DataBind()

            End If
        End If
    End Sub
#End Region
    Public Function GetImagePath(ByVal ImageFileName As String) As String
        Try
            Dim path As String = Sites.ToString(Session("ItemImagePath")) & "/" & ImageFileName
            If Request.IsSecureConnection Then
                Return path.Replace("http", "https")
            Else
                Return path
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function ReplaceAllSpaces(ByVal str As String) As String
        Return Regex.Replace(str, "\s+", "%20")
    End Function

    Protected Sub radShipVia_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Try
            radCmbShippingMethod.Items.Clear()
            radCmbShippingMethod.Items.Add(New RadComboBoxItem("-- Select One --", "0"))
            OppBizDocs.LoadServiceTypes(radShipVia.SelectedValue, radCmbShippingMethod)

            If radShipVia.SelectedValue = "92" Then
                Dim warehosueID As Long = 0
                If CCommon.ToLong(hdnShipFromLocation.Value) > 0 Then
                    warehosueID = CCommon.ToLong(hdnShipFromLocation.Value)
                ElseIf dsTemp.Tables(0).Select("numWarehouseID > 0").Length > 0 Then
                    warehosueID = CCommon.ToLong(dsTemp.Tables(0).Select("numWarehouseID > 0")(0)("numWarehouseID"))
                End If

                Dim objItem As New CItems
                objItem.DomainID = Session("DomainID")
                objItem.WarehouseID = warehosueID
                Dim dtWarehouse As DataTable = objItem.GetWillCallWarehouseDetail()

                If Not dtWarehouse Is Nothing AndAlso dtWarehouse.Rows.Count > 0 Then
                    hdnWillCallWarehouseID.Value = CCommon.ToLong(dtWarehouse.Rows(0)("numWareHouseID"))
                    Dim address As String = CCommon.ToString(dtWarehouse.Rows(0)("vcFullAddress"))

                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "WillCallAddress", "$('[id$=lblShipTo1]').text('" & address & "');$('[id$=lblShipTo2]').text('');", True)
                End If
            Else
                hdnWillCallWarehouseID.Value = "0"
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub grdShippingInformation_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdShippingInformation.RowCommand
        Try
            If e.CommandName = "AddShippingRate" Then
                If lngShippingItemCode = 0 Then
                    litMessage.Text = "Please first set Default Shipping Item from Administration->Global Settings->Accounting Tab->Shipping Service Item."
                    Exit Sub
                End If

                Dim isAddItem As Boolean = True
                Dim oppItemID As Long = 0

                Dim dsItems As DataSet = If(dsTemp Is Nothing, ViewState("SOItems"), dsTemp)

                Dim MaxRowOrder As Long
                MaxRowOrder = dsTemp.Tables(0).Rows.Count
                hdnCurrentSelectedItem.Value = lngShippingItemCode
                Session("ShippingItem") = lngShippingItemCode

                Dim description As String
                Dim decShipppingAmount As Decimal
                Dim canShippingFromPromotionApplied As Boolean = False
                Dim numPromotionID As Long = 0
                Dim bitTriggerredPromotion As String = "0"
                Dim vcPromotionDetail As String = ""
                Dim itemReleaseDate As Date = Convert.ToDateTime(hdnReleaseDate.Value)

                If Not ViewState("UsedPromotion") Is Nothing AndAlso Not DirectCast(ViewState("UsedPromotion"), DataTable).Select("vcShippingDescription <> ''").FirstOrDefault() Is Nothing Then
                    Dim dr As DataRow

                    For Each drPromotion As DataRow In DirectCast(ViewState("UsedPromotion"), DataTable).Rows
                        If Not String.IsNullOrEmpty(drPromotion("vcShippingDescription")) Then
                            dr = drPromotion
                            Exit For
                        End If
                    Next

                    If CCommon.ToBool(dr("bitFreeShiping")) AndAlso CCommon.ToLong(dr("numFreeShippingCountry")) = hdnCountry.Value AndAlso CCommon.ToDecimal(lblTotal.Text) >= CCommon.ToDecimal(dr("monFreeShippingOrderAmount")) Then
                        canShippingFromPromotionApplied = True
                        decShipppingAmount = 0
                        numPromotionID = CCommon.ToLong(dr("numProId"))
                        vcPromotionDetail = CCommon.ToString(dr("vcShippingDescription"))
                        description = "Promotion Shipping"
                        hdnShipVia.Value = "0"
                        hdnShippingService.Value = "0"
                    ElseIf CCommon.ToBool(dr("bitFixShipping2")) AndAlso CCommon.ToDecimal(lblTotal.Text) >= CCommon.ToDecimal(dr("monFixShipping2OrderAmount")) Then
                        canShippingFromPromotionApplied = True
                        decShipppingAmount = CCommon.ToDecimal(dr("monFixShipping2Charge"))
                        numPromotionID = CCommon.ToLong(dr("numProId"))
                        vcPromotionDetail = CCommon.ToString(dr("vcShippingDescription"))
                        description = "Promotion Shipping"
                        hdnShipVia.Value = "0"
                        hdnShippingService.Value = "0"
                    ElseIf CCommon.ToBool(dr("bitFixShipping1")) AndAlso CCommon.ToDecimal(lblTotal.Text) >= CCommon.ToDecimal(dr("monFixShipping1OrderAmount")) Then
                        canShippingFromPromotionApplied = True
                        decShipppingAmount = CCommon.ToDecimal(dr("monFixShipping1Charge"))
                        numPromotionID = CCommon.ToLong(dr("numProId"))
                        vcPromotionDetail = CCommon.ToString(dr("vcShippingDescription"))
                        description = "Promotion Shipping"
                        hdnShipVia.Value = "0"
                        hdnShippingService.Value = "0"
                    End If
                End If

                If Not canShippingFromPromotionApplied Then
                    If chkUseCustomerShippingAccount.Checked = True Then
                        decShipppingAmount = 0
                        hdnShippingService.Value = "0"
                        description = "Shipping will be cost to account # " & CCommon.ToString(hdnShipperAccountNo.Value)
                    Else
                        Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, Control).NamingContainer, GridViewRow)
                        decShipppingAmount = CCommon.ToDouble(DirectCast(row.FindControl("hdnShipRate"), HiddenField).Value)
                        hdnShipVia.Value = CCommon.ToLong(DirectCast(row.FindControl("hdnShipViaID"), HiddenField).Value)
                        hdnShippingService.Value = CCommon.ToLong(DirectCast(row.FindControl("hdnShipServiceID"), HiddenField).Value)
                        description = CCommon.ToString(DirectCast(row.FindControl("hdnShipServiceName"), HiddenField).Value)
                        itemReleaseDate = CCommon.ToString(DirectCast(row.FindControl("hdnReleaseDate"), HiddenField).Value)
                    End If
                End If

                If dsItems.Tables.Count > 0 AndAlso dsItems.Tables(0).Rows.Count > 0 Then
                    If dsItems.Tables(0).Select("numItemCode=" & lngShippingItemCode & " AND ItemReleaseDate='" & itemReleaseDate & "'").Length > 0 Then
                        oppItemID = dsItems.Tables(0).Select("numItemCode=" & lngShippingItemCode & " AND ItemReleaseDate='" & itemReleaseDate & "'")(0)("numoppitemtCode")
                        isAddItem = False
                    End If
                End If

                Dim objItem As New CItems
                objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                objItem.DivisionID = CCommon.ToLong(lngDivId)
                objItem.byteMode = CCommon.ToShort(OppType)
                objItem.ItemCode = lngShippingItemCode
                objItem.WareHouseItemID = 0
                objItem.GetItemAndWarehouseDetails()
                MaxRowOrder = MaxRowOrder + 1
                objItem.numSortOrder = MaxRowOrder

                ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon,
                                                                               dsTemp,
                                                                               isAddItem,
                                                                               objItem.ItemType,
                                                                               "",
                                                                               False,
                                                                               objItem.KitParent,
                                                                               objItem.ItemCode,
                                                                               1,
                                                                               decShipppingAmount,
                                                                               description, objItem.WareHouseItemID, objItem.ItemName, objItem.Warehouse, oppItemID, objItem.ModelID, 0, "-",
                                                                               1,
                                                                               "AddEditOrder", True, 0, "", txtTax.Text, txtCustomerPartNo.Text, Nothing,
                                                                               numProjectID:=CCommon.ToLong(GetQueryStringVal("Source")),
                                                                               numProjectStageID:=CCommon.ToLong(GetQueryStringVal("StageID")),
                                                                               vcBaseUOMName:=hdnBaseUOMName.Value,
                                                                               objItem:=objItem, primaryVendorCost:=0, salePrice:=decShipppingAmount, numPromotionID:=numPromotionID,
                                                                               IsPromotionTriggered:=False,
                                                                               vcPromotionDetail:=vcPromotionDetail, numSortOrder:=objItem.numSortOrder)

                UpdateDetails()
            End If


        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Property SortDirection As String
        Get
            Return IIf(ViewState("SortDirection") IsNot Nothing, Convert.ToString(ViewState("SortDirection")), "ASC")
        End Get
        Set(value As String)
            ViewState("SortDirection") = value
        End Set
    End Property

    Private Sub BindShippingGrid(Optional ByVal sortExpression As String = Nothing)
        Try
            Dim dtShippingMethod As DataTable = ViewState("dtShippingMethod")
            If Not dtShippingMethod Is Nothing Then
                Dim dvShippingMethod As DataView
                dvShippingMethod = dtShippingMethod.DefaultView
                dvShippingMethod.Sort = "ReleaseDate ASC"
                grdShippingInformation.DataSource = dvShippingMethod
                grdShippingInformation.DataBind()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Protected Sub OnPageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        Try
            grdShippingInformation.PageIndex = e.NewPageIndex
            Me.BindShippingGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try

    End Sub
    Protected Sub OnSorting(ByVal sender As Object, ByVal e As GridViewSortEventArgs)
        Try
            Me.BindShippingGrid(e.SortExpression)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnCalculateShipping_Click(sender As Object, e As EventArgs) Handles btnCalculateShipping.Click
        Try
            litMessage.Text = ""
            Dim isItemToShipExists As Boolean
            ViewState("dtShippingMethod") = Nothing

            Dim IsAllCartItemsFreeShipping As Boolean = False
            Dim dtItems As DataTable
            If dsTemp Is Nothing Then
                dsTemp = ViewState("SOItems")
            End If

            dtItems = dsTemp.Tables(0)
            If dtItems IsNot Nothing AndAlso dtItems.Rows.Count > 0 Then

                Dim viewReleaseDates As New DataView(dtItems)
                Dim dtReleaseDates As DataTable = viewReleaseDates.ToTable(True, "ItemReleaseDate")

                If Not dtReleaseDates Is Nothing AndAlso dtReleaseDates.Rows.Count > 0 Then
                    isItemToShipExists = False

                    For Each drReleaseDate As DataRow In dtReleaseDates.Rows
                        Dim fltTotalWeight As Double = 0
                        Dim fltDimensionalWeight As Double = 0
                        Dim fltItemWeight As Double = 0
                        Dim fltItemHeight As Double = 0
                        Dim fltItemWidth As Double = 0
                        Dim fltItemLegnth As Double = 0
                        Dim dblTotalAmount As Double = 0

                        For Each drRow As DataRow In dtItems.Rows
                            If CCommon.ToString(drRow("charItemType")).ToLower() = "p" AndAlso Not CCommon.ToBool(drRow("DropShip")) Then
                                If drReleaseDate("ItemReleaseDate") = drRow("ItemReleaseDate") Then
                                    IsAllCartItemsFreeShipping = CCommon.ToBool(drRow("IsFreeShipping"))
                                    If IsAllCartItemsFreeShipping = False Then
                                        fltItemHeight = If(CCommon.ToDouble(drRow("fltHeight")) > 0, CCommon.ToDouble(drRow("fltHeight")), 1)
                                        fltItemWidth = If(CCommon.ToDouble(drRow("fltWidth")) > 0, CCommon.ToDouble(drRow("fltWidth")), 1)
                                        fltItemLegnth = If(CCommon.ToDouble(drRow("fltLength")) > 0, CCommon.ToDouble(drRow("fltLength")), 1)
                                        fltTotalWeight = fltTotalWeight + (CCommon.ToDouble(drRow("fltItemWeight")) * CCommon.ToDouble(drRow("numUnitHour")))
                                        fltDimensionalWeight = fltDimensionalWeight + ((fltItemHeight * fltItemWidth * fltItemLegnth) / 166)
                                        dblTotalAmount = dblTotalAmount + CCommon.ToDouble(drRow("monTotAmount"))

                                        isItemToShipExists = True
                                    End If
                                End If
                            End If
                        Next


                        If isItemToShipExists Then
                            Dim dblMarkupRate As Double = 1
                            If (chkMarkupShippingCharges.Checked = True) Then
                                dblMarkupRate = If(CCommon.ToDouble(txtMarkupShippingRate.Text) = 0, 1, CCommon.ToDouble(txtMarkupShippingRate.Text))
                            End If

                            If dtReleaseDates.Rows.Count = 1 Then
                                fltTotalWeight = CCommon.ToDouble(txtTotalWeight.Text)
                            End If

                            If fltTotalWeight > 0 OrElse fltDimensionalWeight > 0 Then
                                Dim intShippingCompany As Integer = CCommon.ToLong(radShipVia.SelectedValue)

                                If CCommon.ToInteger(radCmbShippingMethod.SelectedValue) >= 10 AndAlso CCommon.ToInteger(radCmbShippingMethod.SelectedValue) <= 28 Then
                                    intShippingCompany = 91
                                ElseIf CCommon.ToInteger(radCmbShippingMethod.SelectedValue) >= 40 AndAlso CCommon.ToInteger(radCmbShippingMethod.SelectedValue) <= 55 Then
                                    intShippingCompany = 88
                                ElseIf CCommon.ToInteger(radCmbShippingMethod.SelectedValue) >= 70 AndAlso CCommon.ToInteger(radCmbShippingMethod.SelectedValue) <= 76 Then
                                    intShippingCompany = 90
                                End If

                                Try
                                    GetShippingMethod(True, fltTotalWeight, dblTotalAmount, IsAllCartItemsFreeShipping, intShippingCompany, CCommon.ToInteger(radCmbShippingMethod.SelectedValue), drReleaseDate("ItemReleaseDate"), dblMarkupRate, isFlatMarkUp:=If(rblMarkupType.SelectedValue = 2, True, False))
                                Catch ex As Exception
                                    litMessage.Text = ex.Message
                                End Try
                            Else
                                litMessage.Text = "Item Weight is not available."
                            End If
                        End If
                    Next
                End If

                BindShippingGrid()
            End If

            UpdateDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub GetShippingMethod(ByVal ShowShippingAmount As Boolean,
                                  ByVal TotalWeight As Decimal,
                                  ByVal TotalAmount As Decimal,
                                  ByVal IsAllCartItemsFreeShipping As Boolean,
                                  ByVal shippingCompanyID As Long,
                                  ByVal shippingService As Long,
                                  ByVal itemReleaseDate As Date,
                                  Optional ByVal dcMarkupRates As Decimal = 1,
                                  Optional ByVal isFlatMarkUp As Boolean = False)
        Dim strErrLog As New StringBuilder

        Dim intCartItemCount As Integer = 0
        Dim dsItems As New DataSet()
        dsItems = If(dsTemp Is Nothing, ViewState("SOItems"), dsTemp)
        Dim dtShippingMethod As New DataTable()
        Dim strMessage As String = ""
        If Not IsAllCartItemsFreeShipping Then
            '#Region "Get Shipping Rule"
            Dim objRule As New ShippingRule()
            objRule.DomainID = CCommon.ToLong(Session("DomainID"))
            objRule.SiteID = -1
            objRule.DivisionID = CCommon.ToLong(txtDivID.Text)
            If CCommon.ToLong(hdnCountry.Value) <> 0 AndAlso
               CCommon.ToLong(hdnState.Value) <> 0 Then
                objRule.CountryID = CCommon.ToLong(hdnCountry.Value)
                objRule.StateID = CCommon.ToLong(hdnState.Value)

            End If

            dtShippingMethod = objRule.GetShippingMethodForItem1()

            '#Region "Get Shipping Method only when one Of Items in Cart having Free Shipping = false"
            If dtShippingMethod.Rows.Count > 0 Then
                If shippingCompanyID = 88 Or shippingCompanyID = 90 Or shippingCompanyID = 91 Then
                    If dtShippingMethod.Select("numShippingCompanyID=" & shippingCompanyID).Length > 0 Then
                        dtShippingMethod = dtShippingMethod.Select("numShippingCompanyID=" & shippingCompanyID).CopyToDataTable()
                    End If
                End If

                If shippingService > 0 AndAlso dtShippingMethod.Select("intNsoftEnum=" & shippingService).Length > 0 Then
                    dtShippingMethod = dtShippingMethod.Select("intNsoftEnum=" & shippingService).CopyToDataTable()
                End If

                'Default Column Add
                Dim newColumn As New System.Data.DataColumn("IsShippingRuleValid", GetType(System.Boolean))
                newColumn.DefaultValue = True
                dtShippingMethod.Columns.Add(newColumn)

                CCommon.AddColumnsToDataTable(dtShippingMethod, "vcServiceTypeID1")
                CCommon.AddColumnsToDataTable(dtShippingMethod, "TransitTime")
                CCommon.AddColumnsToDataTable(dtShippingMethod, "AnticipateDelivery")
                CCommon.AddColumnsToDataTable(dtShippingMethod, "ReleaseDate")
                CCommon.AddColumnsToDataTable(dtShippingMethod, "ExpectedDate")
                CCommon.AddColumnsToDataTable(dtShippingMethod, "ShippingAmount")
                CCommon.AddColumnsToDataTable(dtShippingMethod, "ShippingCarrierImage")

                ' dtShippingMethod = dtShippingMethod.Select("IsValid=TRUE").CopyToDataTable();
                '#Region "foreach start"

                For Each dr As DataRow In dtShippingMethod.Rows
                    Dim decShipingAmount As [Decimal] = 0

                    If Not IsAllCartItemsFreeShipping AndAlso TotalWeight > 0 Then
                        '#Region "Get Shipping Rates"
                        Dim objShipping As New Shipping()
                        objShipping.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                        objShipping.WeightInLbs = CCommon.ToDouble(TotalWeight)
                        objShipping.NoOfPackages = 1
                        objShipping.UseDimentions = If(CCommon.ToInteger(dr("numShippingCompanyID")) = 91, True, False)
                        'for fedex dimentions are must
                        objShipping.GroupCount = 1
                        If objShipping.UseDimentions Then
                            objShipping.Height = objRule.Height
                            objShipping.Width = objRule.Width
                            objShipping.Length = objRule.Length
                        End If

                        Dim objOppBizDocs As New OppBizDocs()
                        objOppBizDocs.ShipCompany = CCommon.ToInteger(dr("numShippingCompanyID"))
                        objOppBizDocs.ShipFromCountry = CCommon.ToLong(hdnShipFromCountry.Value) 'CCommon.ToLong(dtTable.Rows(0)("numWCountry"))
                        objOppBizDocs.ShipFromState = CCommon.ToLong(hdnShipFromState.Value) 'CCommon.ToLong(dtTable.Rows(0)("numWState"))
                        objOppBizDocs.ShipToCountry = CCommon.ToString(hdnShipToCountry.Value)
                        objOppBizDocs.ShipToState = CCommon.ToString(hdnShipToState.Value)
                        objOppBizDocs.strShipFromCountry = ""
                        objOppBizDocs.strShipFromState = ""
                        objOppBizDocs.strShipToCountry = ""
                        objOppBizDocs.strShipToState = ""
                        objOppBizDocs.GetShippingAbbreviation()

                        objShipping.SenderState = objOppBizDocs.strShipFromState
                        objShipping.SenderZipCode = hdnShipFromPostalCode.Value
                        objShipping.SenderCountryCode = objOppBizDocs.strShipFromCountry

                        objShipping.RecepientState = objOppBizDocs.strShipToState
                        objShipping.RecepientZipCode = hdnShipToPostalCode.Value
                        objShipping.RecepientCountryCode = objOppBizDocs.strShipToCountry

                        objShipping.ServiceType = Short.Parse(CCommon.ToString(dr("intNsoftEnum")))
                        objShipping.PackagingType = 31
                        'ptYourPackaging 
                        objShipping.ItemCode = objRule.ItemID
                        objShipping.OppBizDocItemID = 0
                        objShipping.ID = 9999
                        objShipping.Provider = CCommon.ToInteger(dr("numShippingCompanyID"))

                        Dim dtShipRates As DataTable = objShipping.GetRates()
                        If dtShipRates.Rows.Count > 0 Then
                            If CCommon.ToDecimal(dtShipRates.Rows(0)("Rate")) > 0 Then
                                'Currency conversion of shipping rate
                                decShipingAmount = CCommon.ToDecimal(dtShipRates.Rows(0)("Rate")) / If(CCommon.ToDecimal(CCommon.ToString(Session("ExchangeRate"))) = 0, 1, CCommon.ToDecimal(CCommon.ToString(Session("ExchangeRate"))))

                                If CCommon.ToBool(dr("bitMarkupType")) = True AndAlso CCommon.ToDecimal(dr("fltMarkup")) > 0 Then
                                    dr("fltMarkup") = decShipingAmount / CCommon.ToDecimal(dr("fltMarkup"))
                                End If
                                'Percentage
                                decShipingAmount = decShipingAmount + CCommon.ToDecimal(dr("fltMarkup"))
                                If dcMarkupRates > 1 Then
                                    If isFlatMarkUp Then
                                        decShipingAmount = decShipingAmount + dcMarkupRates
                                    Else
                                        decShipingAmount = decShipingAmount + ((decShipingAmount * dcMarkupRates) / 100)
                                    End If
                                End If
                                If (dr("numShippingCompanyID") = "91") Then
                                    dr("ShippingCarrierImage") = "../images/FedEx.png"
                                End If
                                If (dr("numShippingCompanyID") = "88") Then
                                    dr("ShippingCarrierImage") = "../images/UPS.png"
                                End If
                                If (dr("numShippingCompanyID") = "90") Then
                                    dr("ShippingCarrierImage") = "../images/USPS.png"
                                End If
                                dr("ShippingAmount") = decShipingAmount
                                dr("TransitTime") = dtShipRates.Rows(0)("TransitTIme")
                                Dim strExpectedDate As String = "-"
                                Dim expectedDate As Date
                                If Not String.IsNullOrEmpty(hdnExpectedDate.Value) AndAlso DateTime.TryParse(hdnExpectedDate.Value, expectedDate) Then
                                    strExpectedDate = FormattedDateFromDate(expectedDate, Session("DateFormat"))
                                End If

                                dr("ReleaseDate") = FormattedDateFromDate(itemReleaseDate, Session("DateFormat"))
                                dr("AnticipateDelivery") = FormattedDateFromDate(itemReleaseDate.AddDays(CCommon.ToInteger(dtShipRates.Rows(0)("TransitTIme"))), Session("DateFormat"))

                                dr("ExpectedDate") = strExpectedDate
                                dr("vcServiceTypeID1") = dr("numServiceTypeID").ToString() & "~" & CCommon.ToString(decShipingAmount) & "~" & CCommon.ToString(dr("numRuleID")) & "~" & CCommon.ToString(dr("numShippingCompanyID")) & "~" & CCommon.ToString(dr("vcServiceName")) & "~" & CCommon.ToString(dr("intNsoftEnum"))

                            End If
                        Else
                            dr("IsShippingRuleValid") = False
                        End If
                    Else
                        dr("IsShippingRuleValid") = False
                    End If
                Next
                '#End Region
                Dim drShippingMethod As DataRow() = dtShippingMethod.[Select]("IsShippingRuleValid=true")
                If drShippingMethod.Length > 0 Then
                    dtShippingMethod = dtShippingMethod.[Select]("IsShippingRuleValid=true").CopyToDataTable()
                Else
                    dtShippingMethod = New DataTable()
                End If
            End If
        End If

        '#Region "Bind Shipping Cost"
        If dtShippingMethod.Rows.Count > 0 Then
            dtShippingMethod.Columns.Add("monRate1", GetType(System.Decimal))
            For Each drService As DataRow In dtShippingMethod.Rows
                If CCommon.ToString(drService("vcServiceName")).Contains("-") AndAlso CCommon.ToString(drService("vcServiceName")).Split("-").Length > 0 Then
                    drService("monRate1") = CCommon.ToDecimal(CCommon.ToString(drService("vcServiceName")).Split("-")(1).Trim())
                Else
                    drService("monRate1") = 0
                End If
                dtShippingMethod.AcceptChanges()
            Next

            If ViewState("dtShippingMethod") Is Nothing Then
                ViewState("dtShippingMethod") = dtShippingMethod
            Else
                Dim dtTemp As DataTable = DirectCast(ViewState("dtShippingMethod"), DataTable)
                dtTemp.Merge(dtShippingMethod)
                ViewState("dtShippingMethod") = dtTemp
            End If
        End If

        If strMessage.Length > 0 Then
            Page.MaintainScrollPositionOnPostBack = False
        End If
    End Sub
End Class
Public Class CreateTempColn
    Implements ITemplate

    Dim TemplateType As ListItemType
    Dim Field1 As String

    Sub New(ByVal type As ListItemType, ByVal fld1 As String)
        Try
            TemplateType = type
            Field1 = fld1
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub InstantiateIn(ByVal Container As Control) Implements ITemplate.InstantiateIn
        Try
            Dim lbl1 As Label = New Label()
            Dim lbl2 As Label = New Label()
            Dim lbl3 As Label = New Label()
            'Dim lbl4 As Label = New Label()
            Select Case TemplateType
                Case ListItemType.Footer
                    lbl3.ID = "lblFTaxAmt" & Field1
                    'lbl3.EnableViewState = False
                    AddHandler lbl3.DataBinding, AddressOf BindStringColumn3
                    Container.Controls.Add(lbl3)
                Case ListItemType.Item
                    lbl1.ID = "lblTaxAmt" & Field1
                    'lbl1.EnableViewState = False
                    lbl2.ID = "lblTaxable" & Field1
                    'lbl2.EnableViewState = False
                    'lbl4.ID = "lblID" & Field1
                    AddHandler lbl1.DataBinding, AddressOf BindStringColumn1
                    AddHandler lbl2.DataBinding, AddressOf BindStringColumn2
                    'AddHandler lbl4.DataBinding, AddressOf BindStringColumn4
                    lbl2.Attributes.Add("style", "display:none")
                    Container.Controls.Add(lbl1)
                    Container.Controls.Add(lbl2)
                    'Container.Controls.Add(lbl4)

            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindStringColumn1(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl1 As Label = CType(Sender, Label)
            Dim Container As DataGridItem = CType(lbl1.NamingContainer, DataGridItem)
            lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "Tax" & Field1)), "", DataBinder.Eval(Container.DataItem, "Tax" & Field1))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindStringColumn2(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl1 As Label = CType(Sender, Label)
            Dim Container As DataGridItem = CType(lbl1.NamingContainer, DataGridItem)
            lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "bitTaxable" & Field1)), "", DataBinder.Eval(Container.DataItem, "bitTaxable" & Field1))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindStringColumn3(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl1 As Label = CType(Sender, Label)
            Dim Container As DataGridItem = CType(lbl1.NamingContainer, DataGridItem)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class