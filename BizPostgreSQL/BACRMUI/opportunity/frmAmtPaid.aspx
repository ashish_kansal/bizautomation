<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmAmtPaid.aspx.vb"
    Inherits="BACRM.UserInterface.Opportunities.frmAmtPaid" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Src="../Accounting/TransactionInfo.ascx" TagName="TransactionInfo" TagPrefix="uc1" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <link href="../JavaScript/Signature/jquery.signaturepad.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../JavaScript/Common.js"></script>
    <title>Receive Payment</title>
    <style type="text/css">
        .paddingTable tr td{
           padding: 3px 2px;
        }
        .Button_POS {
            background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #cfcdcf) );
            background: -moz-linear-gradient( center top, #ededed 5%, #cfcdcf 100% );
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#cfcdcf');
            background-color: #ededed;
            -moz-border-radius: 42px;
            -webkit-border-radius: 42px;
            border-radius: 42px;
            border: 1px solid #52658c;
            display: inline-block;
            color: #777777;
            font-family: arial;
            font-size: 15px;
            font-weight: bold;
            padding: 7px 15px;
            text-decoration: none;
        }

        .disabled {
            background: none !important;
        }

        .bizpagercustominfo {
            line-height: 70px;
            font-weight: bold;
        }
    </style>
    <script language="javascript" type="text/javascript">

        var bAmount = false;
        var bCreditApplied = false;
        var bInvoiceAmount = false;
        var bInvoiceChk = false;
        var flag = true;

       
        var submit = 0;
        function CheckDouble(button) {
            if (++submit > 1) {
                $(button).val("Please wait...");
                return false;
            }
        }

        function GetCardType(number) {
            debugger;
            // visa
            var re = new RegExp("^4");
            if (number.match(re) != null)
                return "Visa";

            // Mastercard
            re = new RegExp("^5[1-5]");
            if (number.match(re) != null)
                return "Mastercard";

            // AMEX
            re = new RegExp("^3[47]");
            if (number.match(re) != null)
                return "AMEX";

            // Discover
            re = new RegExp("^(6011|622(12[6-9]|1[3-9][0-9]|[2-8][0-9]{2}|9[0-1][0-9]|92[0-5]|64[4-9])|65)");
            if (number.match(re) != null)
                return "Discover";

            // Diners
            re = new RegExp("^36");
            if (number.match(re) != null)
                return "Diners";

            // Diners - Carte Blanche
            re = new RegExp("^30[0-5]");
            if (number.match(re) != null)
                return "Diners - Carte Blanche";

            // JCB
            re = new RegExp("^35(2[89]|[3-8][0-9])");
            if (number.match(re) != null)
                return "JCB";

            // Visa Electron
            re = new RegExp("^(4026|417500|4508|4844|491(3|7))");
            if (number.match(re) != null)
                return "Visa Electron";

            return "";
        }

        function closeScreen() {
            window.opener.reDirect('../Accounting/frmAccountsReceivable.aspx');
            self.close();
            return false;
        }

        function UpdateTotalNew() {
            var selectedInvoiceAmount = 0;
            var selectedCreditAmount = 0;
            var amountToCredit = 0;
            var objSelectedInvoices = jQuery.parseJSON($("[id$=hdnSelectedInvoices]").val());

            $(".dg tr").not(".hs,.fs").each(function () {
                chk = $(this).find("[id*='chk']");
                if ($(chk).is(':checked')) {
                    if (objSelectedInvoices != null) {
                        var numOppBizDocsID = $(this).find("#hdnOppBizDocID").val();
                        var obj = $.grep(objSelectedInvoices, function (obj) { return obj.numOppBizDocsID == numOppBizDocsID })[0];
                        if (obj == null) {
                            selectedInvoiceAmount += Number(currencyToDecimal($(this).find("#txtAmountToPay").val()));
                        }
                    } else {
                        selectedInvoiceAmount += Number(currencyToDecimal($(this).find("#txtAmountToPay").val()));
                    }
                } else {
                    if (objSelectedInvoices != null) {
                        var numOppBizDocsID = $(this).find("#hdnOppBizDocID").val();
                        objSelectedInvoices = $.grep(objSelectedInvoices, function (obj) {
                            return obj.numOppBizDocsID != numOppBizDocsID;
                        });
                    }
                }
            });

            if (objSelectedInvoices != null) {
                if (objSelectedInvoices.length > 0) {
                    $("[id$=hdnSelectedInvoices]").val(JSON.stringify(objSelectedInvoices));

                    $.each(objSelectedInvoices, function (index, obj) {
                        selectedInvoiceAmount += Number(obj.monAmountPaid);
                    });
                } else {
                    $("[id$=hdnSelectedInvoices]").val("");
                }
            }
            else {
                $("[id$=hdnSelectedInvoices]").val("");
            }

            $("#gvCredits tr").not(".hs,.fs").each(function () {
                txtCreditAmount = $(this).find("[id*='txtCreditAmount']");
                chk = $(this).find("[id*='chk']");
                if ($(chk).is(':checked')) {
                    selectedCreditAmount += Number($(txtCreditAmount).val());
                }
            });


            if (selectedInvoiceAmount - selectedCreditAmount < 0 && !$("#chkAutoApplyCredits").is(':checked'))
            {
                $(".dg tr").not(".hs,.fs").each(function () {
                    chk = $(this).find("[id*='chk']");
                    if (!$(chk).is(':checked') && (selectedInvoiceAmount - selectedCreditAmount) < 0) {
                        chk.prop("checked", true);
                        selectedInvoiceAmount += Number($(this).find("#lblBalDue").text());
                        $(this).find("#txtAmountToPay").val(formatCurrency($(this).find("#lblBalDue").text()));
                    }
                });
            }

            if ($("[id$=NewOrderArea]").length > 0 && $("[id$=txtOrderAmount]").length > 0 && $("[id$=chkCreateInvoice]").length > 0 && $("[id$=chkCreateInvoice]").is(":checked")) {
                selectedInvoiceAmount = selectedInvoiceAmount + Number($("[id$=txtOrderAmount]").val());
            }

            var amountToReceive = selectedInvoiceAmount - selectedCreditAmount;

            $('#lblAmountToPay').text(formatCurrency(amountToReceive));
            $('#txtAmount').val(formatCurrency(amountToReceive.toFixed(2)));
            $('#lblCreditAmountApplied').text(formatCurrency(selectedCreditAmount.toFixed(2)));
            $('#lblFooterTotalPayment').text(formatCurrency(selectedInvoiceAmount.toFixed(2)));

            var TotalAmount = parseFloat(currencyToDecimal($('#txtAmount').val()))

            var amountToCredit = TotalAmount - amountToReceive
            if (amountToCredit > 0)
                amountToCredit = parseFloat(amountToCredit).toFixed(2);
            else
                amountToCredit = 0;
            $('#lblAmountToCredit').text(formatCurrency(amountToCredit));
        }

        function CallConfirmPayment() {
            if (confirm('Some of bizdocs you have selected are already paid, if you still try to pay those bizdocs it will be created as Unapplied Credit/Amount. Are you sure you want to pay these bizdocs ?') == true) {
                return true;
            }
            else {
                return false;
            }
        }
        
        function InvoiceAlreadyExists(oppID, oppBizDocsID, divisionID) {
            alert("Invoice(s) already exists for order. You will redirected to receive payment screen to make payment against invoice.");
            window.location.href = '../opportunity/frmAmtPaid.aspx?Popup=1&OppBizId=' + oppBizDocsID + '&OppId=' + oppID + '&DivId=' + divisionID;
            return false;
        }

        $(document).ready(function () {
            InitializeValidation();

            $("#txtDate").blur(function () {
                $('#btnGo').click();
                //alert('tested');
            });

            //Show hide close button based on requirement
            if (window.opener != null) {
                //PopUp
                //console.log('Popup');
                $("#btnCancel").show();
            }
            else {
                //Not a Popup
                //console.log('Not Popup');
                $("#btnCancel").hide();
            }

            $("#chkAll").change(function () {
                $(".dg tr input[type = 'checkbox']").not("#chkAll").prop("checked", $("#chkAll").is(':checked'))
                flag = false;
                $(".dg tr input[type = 'checkbox']").not("#chkAll").change();
                flag = true;
                bInvoiceChk = true;
                UpdateTotalNew();
            });

            $("#chkAllCredits").change(function () {
                $("#gvCredits tr input[type = 'checkbox']").not("#chkAllCredits").prop("checked", $("#chkAllCredits").is(':checked'))
                flag = false;
                $("#gvCredits tr input[type = 'checkbox']").not("#chkAllCredits").change();
                flag = true;
                UpdateTotalNew();
            });

            $(".dg tr input[type = 'checkbox']").not("#chkAll").change(function () {

                //console.log($(CurrentRow).find('#txtAmountToPay').val());
                var CurrentRow = $(this).parents("tr").filter(':first')
                var lblBalDue = parseFloat(parseFloat(currencyToDecimal($(CurrentRow).find("#lblBalDue").text()))).toFixed(2)
                if ($(this).is(':checked')) {
                    $(CurrentRow).find("#txtAmountToPay").val(formatCurrency(lblBalDue))
                } else {
                    $(CurrentRow).find("#txtAmountToPay").val('')
                }

                bInvoiceChk = true;

                if (flag == true)
                    UpdateTotalNew();
            });

            $("#gvCredits tr input[type = 'checkbox']").not("#chkAllCredits").change(function () {
                var CurrentRow = $(this).parent().parent().parent();

                txtCreditAmount = $(CurrentRow).find("[id*='txtCreditAmount']");
                lblNonAppliedAmount = $(CurrentRow).find("[id*='lblNonAppliedAmount']");

                if ($(this).is(':checked')) {
                    $(txtCreditAmount).val(Number(parseFloat(currencyToDecimal($(lblNonAppliedAmount).text()))).toFixed(2));
                } else {
                    $(txtCreditAmount).val('');
                }

                bCreditApplied = true;
                if (flag == true)
                    UpdateTotalNew();
            });

            $("#gvCredits tr input[id='txtCreditAmount']").change(function () {
                var CurrentRow = $(this).parent().parent();

                $(this).val(Number($(this).val()).toFixed(2));

                txtCreditAmount = $(CurrentRow).find("[id*='txtCreditAmount']");
                lblNonAppliedAmount = $(CurrentRow).find("[id*='lblNonAppliedAmount']");
                chk = $(CurrentRow).find("[id*='chk']");

                if (Number($(txtCreditAmount).val()) > Number(parseFloat(currencyToDecimal($(lblNonAppliedAmount).text())))) {
                    $(txtCreditAmount).val(Number(parseFloat(currencyToDecimal($(lblNonAppliedAmount).text()))).toFixed(2));
                }

                if (Number($(txtCreditAmount).val()) > 0) {
                    $(chk).prop('checked', true);
                }
                else {
                    $(txtCreditAmount).val('');
                    $(chk).prop('checked', false);
                }

                bCreditApplied = true;
                UpdateTotalNew();
            });

            UpdateTotalNew();
            $("#btnChangeReturned").click(function () {
                $('#txtAmount').val(formatCurrency($("#hdnOrderAmount").val()));
                $('#txtAmount').change();
                return false;
            })
            $("#txtAmount").change(function () {
                $("#txtAmount").val(formatCurrency(Number(currencyToDecimal($("#txtAmount").val())).toFixed(2)));


                var selectedInvoiceAmount = 0;
                var selectedCreditAmount = 0;
                var amountToCredit = 0;
                var objSelectedInvoices = jQuery.parseJSON($("[id$=hdnSelectedInvoices]").val());


                $(".dg tr").not(".hs,.fs").each(function () {
                    chk = $(this).find("[id*='chk']");
                    if ($(chk).is(':checked')) {
                        if (objSelectedInvoices != null) {
                            var numOppBizDocsID = $(this).find("#hdnOppBizDocID").val();
                            var obj = $.grep(objSelectedInvoices, function (obj) { return obj.numOppBizDocsID == numOppBizDocsID })[0];
                            if (obj == null) {
                                selectedInvoiceAmount += Number(currencyToDecimal($(this).find("#txtAmountToPay").val()));
                            }
                        } else {
                            selectedInvoiceAmount += Number(currencyToDecimal($(this).find("#txtAmountToPay").val()));
                        }
                    } else {
                        if (objSelectedInvoices != null) {
                            var numOppBizDocsID = $(this).find("#hdnOppBizDocID").val();
                            objSelectedInvoices = $.grep(objSelectedInvoices, function (obj) {
                                return obj.numOppBizDocsID != numOppBizDocsID;
                            });
                        }
                    }
                });

                if (objSelectedInvoices != null) {
                    if (objSelectedInvoices.length > 0) {
                        $("[id$=hdnSelectedInvoices]").val(JSON.stringify(objSelectedInvoices));

                        $.each(objSelectedInvoices, function (index, obj) {
                            selectedInvoiceAmount += Number(obj.monAmountPaid);
                        });
                    } else {
                        $("[id$=hdnSelectedInvoices]").val("");
                    }
                }
                else {
                    $("[id$=hdnSelectedInvoices]").val("");
                }

                if ($("[id$=NewOrderArea]").length > 0 && $("[id$=txtOrderAmount]").length > 0 && $("[id$=chkCreateInvoice]").length > 0 && $("[id$=chkCreateInvoice]").is(":checked")) {
                    selectedInvoiceAmount = selectedInvoiceAmount + Number($("[id$=txtOrderAmount]").val());
                }

                $("#gvCredits tr").not(".hs,.fs").each(function () {
                    txtCreditAmount = $(this).find("[id*='txtCreditAmount']");
                    chk = $(this).find("[id*='chk']");
                    if ($(chk).is(':checked')) {
                        selectedCreditAmount += Number($(txtCreditAmount).val());
                    }
                });


                if (selectedInvoiceAmount - selectedCreditAmount < 0 && !$("#chkAutoApplyCredits").is(':checked')) {
                    $(".dg tr").not(".hs,.fs").each(function () {
                        chk = $(this).find("[id*='chk']");
                        if (!$(chk).is(':checked') && (selectedInvoiceAmount - selectedCreditAmount) < 0) {
                            chk.prop("checked", true);
                            selectedInvoiceAmount += Number($(this).find("#lblBalDue").text());
                            $(this).find("#txtAmountToPay").val(formatCurrency($(this).find("#lblBalDue").text()));
                        }
                    });
                }

                var amountToReceive = selectedInvoiceAmount - selectedCreditAmount;

                if ($(this).val() != $("#hdnOrderAmount").val() && $("#<%=ddlPayment.ClientID%> option:selected").val()==4) {
                    $("#btnChangeReturned").css("display", "initial");
                    var entryAmount = $(this).val()
                    var originalAmount = $("#hdnOrderAmount").val()

                    var difference = parseFloat(originalAmount) - parseFloat(entryAmount)
                    if (difference != 0) {
                        $("#lblChangeAmount").text('$' + difference + ' Balance Due');
                    } else {
                        $("#lblChangeAmount").text('');
                    }
                } else {
                    $("#btnChangeReturned").css("display", "none");
                }

                
                

                //$('#txtOrderAmount').text($('#txtAmount').val());
                $('#lblAmountToPay').text(formatCurrency($('#txtAmount').val()));
                $('#lblCreditAmountApplied').text(formatCurrency(selectedCreditAmount.toFixed(2)));
                $('#lblFooterTotalPayment').text(formatCurrency(selectedInvoiceAmount.toFixed(2)));
                $('#lblFooterTotalCreditAmount').text(selectedCreditAmount.toFixed(2));

                var TotalAmount = parseFloat(currencyToDecimal($('#txtAmount').val()))
                var amountToCredit = TotalAmount - amountToReceive
                if (amountToCredit > 0)
                    amountToCredit = parseFloat(amountToCredit).toFixed(2);
                else
                    amountToCredit = 0;
                $('#lblAmountToCredit').text(formatCurrency(amountToCredit));
            });

            $(".AmountToPay").change(function () {
                bInvoiceAmount = true;

                $(this).val(formatCurrency(Number(currencyToDecimal($(this).val())).toFixed(2)));

                var CurrentRow = $(this).parent().parent();
                txtAmountToPay = Number(currencyToDecimal($(CurrentRow).find("#txtAmountToPay").val()));
                chk = $(CurrentRow).find("[id*='chk']");

                if (txtAmountToPay > 0)
                    $(chk).prop("checked", true);
                else
                    $(chk).prop("checked", false);

                UpdateTotalNew();
            });

            $("[id$=chkCreateInvoice]").change(function () {
                UpdateTotalNew();
            });

            $("[id$=txtOrderAmount]").change(function () {
                $(this).val(Number($(this).val()).toFixed(2));
                UpdateTotalNew();
            });
                

            $("#rdbAuthorizeAndCharge").change(function () {
                if ($(this).is(':checked')) {
                    $("#btnCreditCard").val("Authorize & Charge Card");
                }
                else {
                    $("#btnCreditCard").val("Authorize Card");
                }
            });

            $("#rdbAuthorizeOnly").change(function () {
                if ($(this).is(':checked')) {
                    $("#btnCreditCard").val("Authorize Card");
                }
                else {
                    $("#btnCreditCard").val("Authorize & Charge Card");
                }
            });
        });

        function UpdateTotalAmountToCredit() {

            var TotalAmount = parseFloat(currencyToDecimal($('#txtAmount').val()))
            var TotalAmountToPay = parseFloat(currencyToDecimal($('#lblAmountToPay').text()))
            var TotalAmountToCredit = TotalAmount - TotalAmountToPay
            if (TotalAmountToCredit > 0)
                TotalAmountToCredit = parseFloat(TotalAmountToCredit).toFixed(2);
            else
                TotalAmountToCredit = 0;

            $('#lblAmountToCredit').text(formatCurrency(TotalAmountToCredit));
        }

        function DisableSaveClose() {
            $('#btnSaveClose').attr("disabled", true);
            __doPostBack('ctl00$Content$btnSaveClose', '');
        }

        function Save(d) {
            if (document.getElementById("ddlPayment").value == 0) {
                alert("Select Method of Payment");
                document.getElementById("ddlPayment").focus();

                return false;
            }
            if (document.getElementById("ddlCurrency") != null)
                if (document.getElementById("ddlCurrency").value > 0) {
                    if (parseFloat(document.getElementById("txtExchangeRate").value) <= 0.00) {
                        alert("Enter Exchange Rate");
                        document.getElementById("txtExchangeRate").focus();
                        return false;
                    }
                }


            if (parseFloat(document.getElementById("hdnRefundAmount").value) > parseFloat(document.getElementById("txtAmount").value)) {
                alert("This transaction has been used in Refund. If you want to change amount, you must edit the Refund and remove it first.");
                return false;
            }

            if (CheckIsReconciled()) {
                DisableSaveClose();
                return true;
            }
            else {
                return false;
            }

            var BalanceAmt;
            var TransactionCharge;
            BalanceAmt = 0;
            TransactionCharge = 0;
            //	             alert(d);
            //	             alert(window.document.getElementById('AmountPaid').value);
            TransactionCharge = parseFloat(window.document.getElementById('lblTransCharge').innerHTML.replace(/,/g, ""));

            if (d != 0) {
                BalanceAmt = parseFloat(currencyToDecimal($("#lblBalanceAmt").text()));// window.document.getElementById('lblBalanceAmt').innerHTML.replace(/,/g, "");
                BalanceAmt = parseFloat(BalanceAmt) + parseFloat(window.document.getElementById('hdnAmountPaid').value);
            }
            else {

                BalanceAmt = parseFloat(currencyToDecimal($("#lblBalanceAmt").text())); //window.document.getElementById('lblBalanceAmt').innerHTML.replace(/,/g, "");
            }

            if (parseFloat(document.getElementById("txtAmount").value) > parseFloat(BalanceAmt) + parseFloat(TransactionCharge)) {
                alert("Amount is greater than Balance Due");
                document.getElementById("txtAmount").focus();
                return false;
            }

            DisableSaveClose();
        }

        function WindowClose() {
            window.close();
            return false;
        }

        function Close() {
            window.close();
        }

        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;

            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16 || e.keyCode == 9)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16 || e.keyCode == 9)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }

        function OpenTransHistory(DivID, OppID) {
            window.open('../Opportunity/frmTransactionDetails.aspx?OppID=' + OppID + '&DivisionID=' + DivID, '', 'toolbar=0,titlebar=0,menubar=0,location=1,left=100,top=50,width=750,height=800,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenBizInvoice(a, b) {
            window.open('../Opportunity/frmBizInvoice.aspx?OpID=' + a + '&OppBizId=' + b, '', 'toolbar=0,titlebar=0,menubar=0,location=1,left=100,top=50,width=750,height=800,scrollbars=yes,resizable=yes');
            return false;
        }

        function IsNumeric(sText) {
            var ValidChars = "0123456789.";
            var IsNumber = true;
            var Char;
            if (sText == undefined) return true;
            for (i = 0; i < sText.length && IsNumber == true; i++) {
                Char = sText.charAt(i);
                if (ValidChars.indexOf(Char) == -1) {
                    IsNumber = false;
                }
            }
            return IsNumber;
        }

        function openApp(a, b, c, d) {

            window.open('../Documents/frmDocApprovers.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DocID=' + a + '&DocType=' + b + '&OpID=' + c + "&DocName=" + d, '', 'toolbar=no,titlebar=no,left=300,top=450,width=900,height=600,scrollbars=yes,resizable=yes')
            return false;
        }

        function swipedCreditCard() {
            txtSwipe = document.getElementById('txtSwipe');
            var cardType = GetCardType(txtSwipe.value);
            debugger;
            if (cardType != "") {
                $('#ddlCardType option').map(function () {
                    if ($(this).text() == cardType) return this;
                }).attr('selected', 'selected');
            } else {
                $('#ddlCardType').val(0);
            }
            txtCHName = document.getElementById('txtCHName');
            txtCCNo = document.getElementById('txtCCNo');
            ddlMonth = document.getElementById('ddlMonth');
            ddlYear = document.getElementById('ddlYear');
            txtCVV2 = document.getElementById('txtCVV2');

            //boolean values to determine which method to parse
            blnCarrotPresent = false;
            blnEqualPresent = false;
            blnBothPresent = false;

            strParse = txtSwipe.value;
            //console.log(txtSwipe.value);
            //console.log(strParse);
            if (strParse.length <= 10) {
                alert("Error Parsing Card.\r\n");
                txtSwipe.value = '';
                return false;
            }

            strCarrotPresent = strParse.indexOf("^");
            strEqualPresent = strParse.indexOf("=");

            if (strCarrotPresent > 0) {
                blnCarrotPresent = true;
            }

            if (strEqualPresent > 0) {
                blnEqualPresent = true;
            }

            if (blnCarrotPresent == true) {
                //contains carrot
                strParse = '' + strParse + ' ';
                arrSwipe = new Array(4);
                arrSwipe = strParse.split("^");

                if (arrSwipe.length > 2) {
                    account = stripAlpha(arrSwipe[0].substring(1, arrSwipe[0].length));
                    account_name = arrSwipe[1];
                    exp_month = arrSwipe[2].substring(2, 4);
                    exp_year = arrSwipe[2].substring(0, 2);
                    namepieces = account_name.split('/');
                }
                else {
                    alert("Error Parsing Card.\r\n");
                    txtSwipe.value = '';
                    return false;
                }
            }
            else if (blnEqualPresent == true) {
                //equal only delimiter
                sCardNumber = strParse.substring(1, strEqualPresent);
                account = stripAlpha(sCardNumber);
                exp_month = strParse.substr(strEqualPresent + 1, 2);
                exp_year = strParse.substr(strEqualPresent + 3, 2);
            }

            if (isValidCreditCard(account) == false) {
                alert("Error Parsing Card.\r\n Invalid credit card. \r\n");
                txtSwipe.value = '';
                return false;
            }

            selectOptionByValue(ddlMonth, exp_month);
            selectOptionByValue(ddlYear, exp_year);
            txtCCNo.value = account;
            selectOptionByValue(document.getElementById('ddlCardType'), getCreditCardType(account));

            if (blnCarrotPresent == true) {
                var l = namepieces[0];
                var f = namepieces[1];
                txtCHName.value = namepieces[0] + " " + namepieces[1];
            }

            trackData = strParse.split('?');
            document.getElementById('hdnSwipeTrack1').value = trackData[0] + '?';

            if (trackData.length = 2) {
                document.getElementById('hdnSwipeTrack2').value = trackData[1] + '?';
            }


            txtCVV2.style.visibility = "hidden";
            //txtCVV2.focus();
            return true;
            //            if (txtSwipe.value.length <= 0) {
            //                return true;
            //            }

            //            if (txtSwipe.value.substr(0, 1) == "B") {
            //                var containsCaret = txtSwipe.value.search('^');
            //                if (containsCaret < 0) {
            //                    expirField.focus();
            //                }
            //                else {
            //                    var pieces = txtSwipe.value.split('^');
            //                    if (pieces.length >= 3) {
            //                        var cardNumber = pieces[0];
            //                        var name = pieces[1];
            //                        var exp = pieces[2];

            //                        txtCCNo.value = cardNumber.substr(1, (cardNumber.length - 1));
            //                        expirField.value = exp.substr(2, 2) + exp.substr(0, 2);

            //                        namepieces = name.split('/');
            //                        var l = namepieces[0];
            //                        var f = namepieces[1];

            //                        txtCHName.value = namepieces[0] + " " + namepieces[1];
            //                        txtCVV2.focus();
            //                    }
            //                }
            //            }
            //            else {
            //                var containsEqual = txtSwipe.value.search('=');
            //                if (containsEqual < 0) {
            //                    expirField.focus();
            //                }
            //                else {
            //                    var pieces = txtSwipe.value.split('=');
            //                    if (pieces.length <= 2) {
            //                        var cardNumber = pieces[0];
            //                        var exp = pieces[1];

            //                        txtCCNo.value = cardNumber;
            //                        expirField.value = exp.substr(2, 2) + exp.substr(0, 2);

            //                        txtCVV2.focus();
            //                    }
            //                }
            //            }
            //            return true;
        }

        function stripAlpha(sInput) {
            if (sInput == null) return '';
            return sInput.replace(/[^0-9]/g, '');
        }

        function selectOptionByValue(selObj, val) {
            var A = selObj.options, L = A.length;
            while (L) {
                if (A[--L].value == val) {
                    selObj.selectedIndex = L;
                    L = 0;
                }
            }
        }

        function getCreditCardType(accountNumber) {

            //start without knowing the credit card type
            var result = "0";

            // Mastercard: length 16, prefix 51-55, dashes optional.
            if (/^5[1-5]\d{2}-?\d{4}-?\d{4}-?\d{4}$/.test(accountNumber)) {
                result = "14883";
            }

                // Visa: length 16, prefix 4, dashes optional.
            else if (/^4\d{3}-?\d{4}-?\d{4}-?\d{4}$/.test(accountNumber)) {
                result = "14882";
            }

                // Discover: length 16, prefix 6011, dashes optional.
            else if (/^6011-?\d{4}-?\d{4}-?\d{4}$/.test(accountNumber)) {
                result = "14885";
            }

                // American Express: length 15, prefix 34 or 37.
            else if (/^3[4,7]\d{13}$/.test(accountNumber)) {
                result = "14884";
            }

                // Diners: length 14, prefix 30, 36, or 38.
            else if (/^3[0,6,8]\d{12}$/.test(accountNumber)) {
                result = "14886";
            }
            return result;
        }

        function isValidCreditCard(ccnum) {
            var sum = 0, alt = false, i = ccnum.length - 1, num;
            //console.log('ccnum : ' + ccnum);
            //console.log('ccnum.length : ' + ccnum.length);

            if (ccnum.length < 13 || ccnum.length > 19) {
                return false;
            }

            // Remove all dashes for the checksum checks to eliminate negative numbers
            ccnum = ccnum.split("-").join("");
            //console.log('(2 - (ccnum.length % 2)) : ' + (2 - (ccnum.length % 2)))
            // Checksum ("Mod 10")
            // Add even digits in even length strings or odd digits in odd length strings.
            var checksum = 0;
            for (var i = (2 - (ccnum.length % 2)) ; i <= ccnum.length; i += 2) {
                checksum += parseInt(ccnum.charAt(i - 1));
            }
            //console.log('checksum1:' + checksum);
            // Analyze odd digits in even length strings or even digits in odd length strings.
            //console.log('(ccnum.length % 2) + 1 : ' + (ccnum.length % 2) + 1);
            for (var i = (ccnum.length % 2) + 1; i < ccnum.length; i += 2) {
                var digit = parseInt(ccnum.charAt(i - 1)) * 2;
                if (digit < 10) { checksum += digit; } else { checksum += (digit - 9); }
            }
            //console.log('checksum2 : ' + checksum);
            if ((checksum % 10) == 0) return true; else return false;
        }

        function OpenAmtPaid(a, b, c) {
            if (c == 3) {
                var str;
                str = "../opportunity/frmReturnDetail.aspx?ReturnID=" + b;
                document.location.href = str;
            }
            else {
                window.location.href = "../opportunity/frmAmtPaid.aspx?frm=GeneralLedger&DepositeID=" + a;
                //window.name = 'ReceivePayment';
            }
            return false;
        }

        function GetConfirmationToDeleteCardDetails() {
            if (confirm('Are you sure?, Do you want to delete the selected Credit card details permanently?') == true) {
                return true;
            }
            else {
                return false;
            }
        }

        function CloseAndCreateNewOrder() {
            window.open('../opportunity/frmNewOrder.aspx?OppStatus=1&OppType=1', "_blank", 'toolbar=no,titlebar=no,left=100,top=100,width=1200,height=645,scrollbars=yes,resizable=yes')
            window.close();
            return false;
        }

        function callfromparent(parameters) {
            if (window.opener != null) {
                window.opener.location.href = window.opener.location.href;
            }
            window.close();
            window.open('../Opportunity/frmAmtPaid.aspx' + parameters , "_blank", 'toolbar=no,titlebar=no,left=100,top=100,width=1200,height=645,scrollbars=yes,resizable=yes');
            return false;
        }

        function receivepaymentandclose() {
            if (window.opener != null) {
                var url = window.opener.location.href;    
                if (url.indexOf('?') > -1){
                   url += '&param=1'
                }else{
                   url += '?param=1'
                }
               // window.location.href = url;
                window.opener.location.href = url;               
            }
            window.close();
           // window.opener.location.reload();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <ul id="messagebox" class="errorInfo" style="display: none">
    </ul>
     <div class="input-part">
         <table width="100%" border="0">
             <tr>
                 <td style="padding-left:15px; padding-top:20px;">
                    <b> Received on, by:</b><asp:Label ID="lblreceivedonby" runat="server"></asp:Label>
                 </td>
                 <td style="float:right;">
                     <div class="right-input">
                        <asp:Button ID="btnCloseAndOpenOrder" runat="server" CssClass="button" Text="Close & Open Sales Order" Visible="false"></asp:Button>
                        <asp:Button ID="btnCloseAndCreateNewOrder" runat="server" CssClass="button" Text="Close & Create new Sales Order" OnClientClick="CloseAndCreateNewOrder();"></asp:Button>
                        <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClientClick="window.close();  window.opener.location.reload();"></asp:Button>&nbsp;&nbsp;
                    </div>
                </td>
             </tr>
         </table>
        
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Receive Payment
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager runat="server" ID="Sc1" />
    <table width="100%" border="0" style="min-width: 950px;">
        <tr>
            <td class="normal4" align="center" colspan="4">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                <asp:HyperLink ID="hplApproval" runat="server" Visible="false" NavigateUrl="#" Text="Check Approval Status">
                </asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td align="right">Customer
            </td>
            <td>
                <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                    OnClientItemsRequested="OnClientItemsRequestedOrganization"
                    ClientIDMode="Static"
                    ShowMoreResultsBox="true"
                    Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True">
                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                </telerik:RadComboBox>
                <asp:CheckBox ID="chkOnCreditHold" runat="server" Enabled="false" Text="On Credit Hold" />
            </td>
            <td align="right">Date
            </td>
            <td align="left">
                <BizCalendar:Calendar ID="calDate" runat="server" IsRequired="true" />
            </td>
        </tr>
        <tr>
            <td align="right">Memo
            </td>
            <td rowspan="3" valign="top">
                <asp:TextBox ID="txtMemo" runat="server" TextMode="MultiLine" Width="200px" Rows="4">
                </asp:TextBox>
            </td>
            <td align="right" style="color: #00b017; font-weight: bold">Amount to receive
            </td>
            <td align="left">
                <asp:TextBox ID="txtAmount" runat="server" AutoComplete="off" CssClass="required_decimal {required:false ,number:true, messages:{number:'provide valid value!'}}">
                </asp:TextBox>
                <input type="hidden" value="0" id="hdnOrginalAmountPaid" />
                <asp:Button ID="btnChangeReturned" style="display:none" CssClass="OrderButton" runat="server" Text="Change Returned" /><br />
                <asp:Label ID="lblChangeAmount" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        
        <tr>
            <td align="right"></td>
            <td align="right">Payment Method
            </td>
            <td>
                <asp:DropDownList ID="ddlPayment" runat="server" CssClass="signup" Visible="true"
                    AutoPostBack="True">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right"></td>
            <td align="right">Reference #
            </td>
            <td>
                <asp:TextBox ID="txtReference" runat="server" Visible="true">
                </asp:TextBox>
            </td>
        </tr>
        <tr id="trMultiCurrency" runat="server">
            <td align="right">Currency
            </td>
            <td align="left">
                <asp:DropDownList Width="200px" ID="ddlCurrency" runat="server" CssClass="signup"
                    AutoPostBack="true">
                </asp:DropDownList>
            </td>
            <td align="right">Exchange Rate
            </td>
            <td align="left">&nbsp;1&nbsp;<asp:Label runat="server" ID="lblForeignCurr"></asp:Label>&nbsp;=&nbsp;
                <asp:TextBox ID="txtExchangeRate" runat="server" Width="40px" onkeypress="CheckNumber(1,event);"></asp:TextBox>&nbsp;&nbsp;<asp:Label
                    runat="server" ID="lblBaseCurr" Text="USD"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="pnlCreditCardForm" Visible="false">

        <div style="width:98%;background:#efefef;padding: 10px;margin-top: 10px;margin-bottom: 10px;">
            <table>
                <tr>
                <td>
                    <table class="paddingTable" cellpadding="10" cellspacing="10">
                        <tr id="trPaymentGateway" runat="server" visible ="false">
                            <td align="right" style="width: 129px;">
                                Payment Gateway
                            </td>
                            <td align="left">
                                <asp:DropDownList runat="server" ID="ddlPaymentGateWay" CssClass="signup" Width="200">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="
    width: 129px;
">Saved Credit Card #s
                    </td>
                    <td align="left">
                                <asp:DropDownList runat="server" ID="ddlCards" CssClass="signup" Width="200" AutoPostBack="true">
                        </asp:DropDownList>
                        <small>(optional)</small>
                    </td>




                </tr>
                <tr>
                            <td align="right">Use Contact Name

                    </td>
                    <td>
                                <asp:DropDownList runat="server" ID="ddlContact" Width="200">
                        </asp:DropDownList>
                    </td>

                </tr>
                <tr>
                            <td align="right">Use this Name instead
                    </td>
                    <td>
                                <asp:TextBox ID="txtCHName" Width="200" runat="server" CssClass="signup" MaxLength="100">
                        </asp:TextBox>
                    </td>
                        </tr>
                        <tr>
                            <td align="right">Credit Card#
                    </td>
                    <td>
                                <asp:TextBox Width="200" ID="txtCCNo" CssClass="signup" runat="server" MaxLength="16">
                        </asp:TextBox>
                    </td>

                </tr>
                <tr>
                            <td colspan="2">
                                <table>
                                    <tr>
                                        <td align="right" style="width: 126px;">CVV
                    </td>
                    <td>
                                            <asp:TextBox ID="txtCVV2" CssClass="signup" runat="server" Width="60px" MaxLength="4">
                        </asp:TextBox>
                    </td>
                    <td align="right">Valid Up-to
                    </td>
                    <td>
                                            <table>
                                                <tr>
                                                    <td>
                        <asp:DropDownList CssClass="signup" ID="ddlMonth" runat="server">
                            <asp:ListItem Value="01">01</asp:ListItem>
                            <asp:ListItem Value="02">02</asp:ListItem>
                            <asp:ListItem Value="03">03</asp:ListItem>
                            <asp:ListItem Value="04">04</asp:ListItem>
                            <asp:ListItem Value="05">05</asp:ListItem>
                            <asp:ListItem Value="06">06</asp:ListItem>
                            <asp:ListItem Value="07">07</asp:ListItem>
                            <asp:ListItem Value="08">08</asp:ListItem>
                            <asp:ListItem Value="09">09</asp:ListItem>
                            <asp:ListItem Value="10">10</asp:ListItem>
                            <asp:ListItem Value="11">11</asp:ListItem>
                            <asp:ListItem Value="12">12</asp:ListItem>
                        </asp:DropDownList>
                    </td>

                                                    <td><asp:DropDownList CssClass="signup" ID="ddlYear" runat="server">
                                            </asp:DropDownList></td>
                                                </tr>
                                            </table>
                                            
                                            
                                        </td>
                                    </tr>
                                </table>
                            </td>
                </tr>
                <tr>
                    <td align="right">Card Type:
                    </td>
                    <td>
                                <asp:DropDownList runat="server" ID="ddlCardType" Width="200" CssClass="signup" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                </tr>
                        <tr>
                            <td align="right" id="tdSwipe1" runat="server">Swipe
                            </td>
                            <td id="tdSwipe2" runat="server">
                                <asp:TextBox ID="txtSwipe" Width="200" runat="server" CssClass="signup" MaxLength="250">
                                </asp:TextBox>
                            </td>
                        </tr>



                <tr id="trSignature" runat="server" visible="false">
                    <td class="sigPad" colspan="4">
                        <ul class="sigNav">
                            <li class="drawIt"><a href="#draw-it" class="current">Sign It</a></li>
                            <li class="clearButton"><a href="#clear">Clear</a></li>
                        </ul>
                        <div class="sig sigWrapper">
                            <div class="typed">
                            </div>
                            <canvas class="pad" width="650" height="100"></canvas>
                            <input type="hidden" name="output" class="output" id="hdnSignatureOutput" runat="server" />
                        </div>
                    </td>
                </tr>

                    </table>
                </td>
                <td style="display:none">
                    <asp:RadioButton ID="rdbAuthorizeAndCharge" Text="Authorize & Charge" runat="server" GroupName="PayByCreditCard" Checked="true" />
                    <asp:RadioButton ID="rdbAuthorizeOnly" Text="Authorize Only" runat="server" GroupName="PayByCreditCard" />
                </td>
                <td>
                    <table class="paddingTable">
                        <tr>
                            <td style="width:170px;">Authorization request date :</td>
                            <td>
                                <asp:Label ID="lblAuthRequestDate" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>Authorization status :</td>
                            <td><asp:Label ID="lblAuthStatus" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Status at time of request :</td>
                            <td><asp:Label ID="lblStatusTimeRequest" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Authorization code :</td>
                            <td><asp:Label ID="lblAuthCode" runat="server" Text=""></asp:Label></td>
                        </tr>
                        <tr>
                            <td>Amount :</td>
                            <td><asp:Label ID="lblAmount" runat="server" Text=""></asp:Label></td>
                        </tr>
                       <%-- <tr>
                            <td>AVS Code :</td>
                            <td><asp:Label ID="lblAvsCode" runat="server" Text=""></asp:Label></td>
                        </tr>--%>
                    </table>
                </td>
                <td>
                    <table>
                        <tr>
                            <td>
                                <asp:Button runat="server" ID="btnCreditCard" CssClass="OrderButton btnAuthorizeCharge" Text="Authorize & Charge Card" OnClientClick="return CheckDouble(this);"/></td>
                        </tr>
                         <tr>
                            <td>
                                <asp:Button runat="server" ID="btnAuthrizeOnly" CssClass="OrderButton btnAuthorize" Text="Authorize Only" OnClientClick="return CheckDouble(this);"  />
                                + <asp:Label ID="lblAuthorizeCharge" runat="server" Text="0"></asp:Label>%
                            </td>
                        </tr>
                <tr>
                    <td>
                                <asp:Button runat="server" ID="btnDeleteCardDetails" CssClass="OrderButton" Text="Delete Card Details" OnClientClick="return GetConfirmationToDeleteCardDetails();" /></td>
                        </tr>
            </table>
                </td>
                </tr>
            </table>
        </div>

    </asp:Panel>
    <table width="100%">
        <tr>
            <td>
                <strong>Credits</strong>
                <input id="chkAutoApplyCredits" type="checkbox" name="chkAutoApplyCredits" />
                <label for="chkAutoApplyCredits">Manually apply</label><br />
                <asp:GridView ID="gvCredits" runat="server" BorderWidth="0" CssClass="dgNHover" AutoGenerateColumns="False"
                    Style="margin-right: 0px" Width="100%" ShowFooter="true">
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" VerticalAlign="Middle" />
                    <FooterStyle CssClass="hs" />
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAllCredits" runat="server" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chk" CssClass="chkSelected" runat="server" Checked='<%# Eval("bitIsPaid") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Type">
                            <ItemTemplate>
                                <a href="#" onclick='javascript:OpenAmtPaid(<%#Eval("numReferenceID") %>,<%# Eval("numReturnHeaderID")%>,<%#Eval("tintRefType")%>)'>
                                    <%#Eval("vcReference")%></a> <span class='gray'>
                                        <%# Eval("vcDepositReference")%>
                                    </span>
                                <asp:Label ID="lblReferenceID" runat="server" Style="display: none" Text='<%# Eval("numReferenceID")%>'></asp:Label>
                                <asp:Label ID="lblRefType" Style="display: none" runat="server" Text='<%# Eval("tintRefType")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Original Amount" ItemStyle-HorizontalAlign="Right"
                            FooterStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <%#Eval("varCurrSymbol") + " " %><asp:Label ID="lblAmount" runat="server" Text='<%# Eval("monAmount","{0:N}")%>'></asp:Label>
                                <%#IIf(Eval("BaseCurrencySymbol") <> Eval("varCurrSymbol"), "<span class='gray'>(" + Eval("BaseCurrencySymbol") + " " + String.Format("{0:###00.00}", Eval("monAmount") * Eval("fltExchangeRateReceivedPayment")) + ")</span>", "")%>
                            </ItemTemplate>
                            <FooterTemplate>
                                Totals
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Open Balance" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <%#Eval("varCurrSymbol") + " " %><asp:Label ID="lblNonAppliedAmount" runat="server"
                                    Text='<%# Eval("monNonAppliedAmount","{0:N}")%>'></asp:Label>
                                <%#IIf(Eval("BaseCurrencySymbol") <> Eval("varCurrSymbol"), "<span class='gray'>(" + Eval("BaseCurrencySymbol") + " " + String.Format("{0:###00.00}", Eval("monNonAppliedAmount") * Eval("fltExchangeRateReceivedPayment")) + ")</span>", "")%>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label Text="" runat="server" ID="lblFooterTotalNonAppliedAmount" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <%#Eval("varCurrSymbol")%>
                                <asp:TextBox ID="txtCreditAmount" runat="server" AutoComplete="OFF" CssClass="money"
                                    onkeypress="CheckNumber(1,event);" Text='<%# ReturnMoney(Eval("monAmountPaid")) %>'></asp:TextBox>
                                <%#IIf(Eval("BaseCurrencySymbol") <> Eval("varCurrSymbol"), "<span class='gray'>(" + Eval("BaseCurrencySymbol") + String.Format("{0:###00.00}", Eval("monAmountPaid") * Eval("fltExchangeRateReceivedPayment"))+")</span>", "")%>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label Text="" runat="server" ID="lblFooterTotalCreditAmount" />
                            </FooterTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        No credits available for this Customer.
                    </EmptyDataTemplate>
                    <FooterStyle HorizontalAlign="Right" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>
                <br />
                <strong>Invoices and outstanding transactions</strong>
                <br />
                <webdiyer:AspNetPager ID="bizPager" runat="server"
                    PagingButtonSpacing="0"
                    CssClass="bizgridpager"
                    AlwaysShow="true"
                    CurrentPageButtonClass="active"
                    PagingButtonUlLayoutClass="pagination"
                    PagingButtonLayoutType="UnorderedList"
                    FirstPageText="<<"
                    LastPageText=">>"
                    NextPageText=">"
                    PrevPageText="<"
                    Width="100%"
                    UrlPaging="false"
                    NumericButtonCount="8"
                    ShowCustomInfoSection="Left"
                    OnPageChanged="bizPager_PageChanged"
                    ShowPageIndexBox="Never"
                    CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
                    CustomInfoClass="bizpagercustominfo">
                </webdiyer:AspNetPager>
                <br />
                <asp:DataGrid ID="dgOpportunity" runat="server" Width="1000px" CssClass="dg" AllowSorting="True"
                    AutoGenerateColumns="False" ClientIDMode="Static" ShowFooter="true">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <FooterStyle CssClass="fs"></FooterStyle>
                    <Columns>
                        <asp:TemplateColumn>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkAll" runat="server" onclick="SelectAll('chkAll','chkSelct');" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chk" CssClass="chkSelct" runat="server" Style="padding-left: 10px;"
                                    Checked='<%# Eval("numDepositeDetailID")%>' />
                                <asp:Label ID="lblCheck" runat="server" Text="*" Style="padding-left: 17px;"></asp:Label>
                                <asp:HiddenField runat="server" ID="hdnOppBizDocID" Value='<%# Eval("numOppBizDocsId") %>' />
                                <asp:HiddenField runat="server" ID="hdnOppID" Value='<%# Eval("numOppId") %>' />
                                <asp:HiddenField runat="server" ID="hdnDepositeID" Value='<%# Eval("numDepositID")%>' />
                                <asp:HiddenField runat="server" ID="hdnAmountPaidInDeposite" Value='<%# Eval("monAmountPaidInDeposite")%>' />
                                <asp:HiddenField runat="server" ID="hdnDepositeDetailID" Value='<%# Eval("numDepositeDetailID")%>' />
                                <asp:HiddenField runat="server" ID="hdnExchangeRateOfOrder" Value='<%# Eval("fltExchangeRateOfOrder")%>' />
                                <asp:HiddenField runat="server" ID="hdnExchangeRateCurrent" Value='<%# Eval("fltExchangeRateCurrent")%>' />
                                <asp:HiddenField runat="server" ID="hdnBizDocID" Value='<%# Eval("vcBizDocID")%>' />
                                <asp:Label Text='<%# Eval("numContactID")%>' runat="server" Style="display: none" ID="lblOrderContactID" />
                                <asp:HiddenField runat="server" ID="hdnChild" Value='<%# Eval("bitChild")%>' />
                                <asp:HiddenField runat="server" ID="hdnDivisionId" Value='<%# Eval("numdivisionId")%>' />
                                <asp:HiddenField runat="server" ID="hdnARAccountID" Value='<%# Eval("numARAccountID")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Organization (Ship-to City)" HeaderStyle-Wrap="false">
                            <ItemTemplate>
                                <%#eval("vcCompanyName")%> <%#IIf(Eval("vcCity") Is DBNull.Value, "", String.Format(" ({0})", Eval("vcCity")))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="dtFromDate" HeaderText="Billing Date" HeaderStyle-Wrap="false"></asp:BoundColumn>
                        <asp:BoundColumn DataField="dtDueDate" HeaderText="Due Date" HeaderStyle-Wrap="false"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderStyle-Wrap="false">
                            <HeaderTemplate>
                                BizDoc
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%-- <div style="display: block; height: 25px; width: 150px; overflow: hidden">--%>
                                <a href="#" onclick="return OpenBizInvoice('<%# Eval("numOppId") %>','<%# Eval("numOppBizDocsId") %>');"
                                    title='<%#Eval("vcBizDocID")%>'>
                                    <%#Eval("vcBizDocID")%></a>
                                <%--</div>--%>
                                <span class='gray'>
                                    <%# Eval("vcPOppName")%>
                                </span>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderStyle-Wrap="false">
                            <HeaderTemplate>
                                BizDoc Status
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblBizDocStatus" Text="NA" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="vcRefOrderNo" HeaderText="P.O #" HeaderStyle-Wrap="false"></asp:BoundColumn>
                        <%--<asp:TemplateColumn HeaderText="Total Commission" SortExpression="">
                            <ItemStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <a href="#">0.00</a>
                            </ItemTemplate>
                        </asp:TemplateColumn>--%>
                        <asp:TemplateColumn HeaderText="Original Amount" SortExpression="" HeaderStyle-Wrap="false">
                            <ItemStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%#Eval("varCurrSymbol") + " " + String.Format("{0:N}", Eval("monDealAmount"))%>
                                <%#IIf(Eval("BaseCurrencySymbol") <> Eval("varCurrSymbol"), "<span class='gray'>(" + Eval("BaseCurrencySymbol") + " " + String.Format("{0:###00.00}", Eval("monDealAmount") * Eval("fltExchangeRateOfOrder")) + ")</span>", "")%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%--<asp:TemplateColumn HeaderText="Credits" SortExpression="">
                            <ItemStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%# String.Format("{0:##,#00.00}", Eval("monCreditAmount"))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>--%>
                        <%--<asp:TemplateColumn HeaderText="Amount Paid" SortExpression="monAmountPaid">
                            <ItemStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%#Eval("varCurrSymbol") + " " + String.Format("{0:###00.00}", Eval("monAmountPaid"))%>
                            </ItemTemplate>
                            <FooterStyle HorizontalAlign="Right" />
                            <FooterTemplate>
                                Totals
                            </FooterTemplate>
                        </asp:TemplateColumn>--%>
                        <asp:TemplateColumn HeaderText="Open Balance" SortExpression="" HeaderStyle-Wrap="false">
                            <ItemStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%#Eval("varCurrSymbol")%>
                                <asp:Label ID="lblBalDue" Text='<%#  String.Format("{0:N}", Eval("baldue"))%>'
                                    runat="server" />
                                <%#IIf(Eval("BaseCurrencySymbol") <> Eval("varCurrSymbol"), "<span class='gray'>(" + Eval("BaseCurrencySymbol") + String.Format("{0:###00.00}", Eval("BalDue") * Eval("fltExchangeRateOfOrder"))+")</span>", "")%>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label Text="" runat="server" ID="lblFooterTotalBalanceDue" />
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Financial Terms" SortExpression="" HeaderStyle-Wrap="false">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkApply" runat="server" />
                                <asp:Label ID="lblApply" runat="server"></asp:Label>
                                <asp:HiddenField runat="server" Value='<%#Eval("numDiscount")%>' ID="hdnDiscount" />
                                <asp:HiddenField runat="server" Value='<%#Eval("numDiscountPaidInDays")%>' ID="hdnDiscountPaidInDays" />
                                <asp:HiddenField runat="server" Value='<%#Eval("numNetDueInDays")%>' ID="hdnNetDueInDays" />
                            </ItemTemplate>
                            <FooterTemplate>
                                <center>
                                    <asp:Button Text="Apply Discount" runat="server" CssClass="button" ID="btnApplyDiscount"
                                        CommandArgument='<%#Eval("numOppId")%>' CommandName="ApplyDiscount" /></center>
                            </FooterTemplate>
                        </asp:TemplateColumn>

                        <%--<asp:TemplateColumn HeaderText="BizDoc Comments">
                <ItemTemplate>
                    <asp:TextBox runat="server" ID="txtComments" CssClass="signup" Text="" Width="150"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateColumn>--%>
                        <%--  <asp:TemplateColumn>
                            <HeaderTemplate>
                                Currency Adjustment
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%-- <%# ((Eval("BalDue") * Eval("fltExchangeRateCurrent")) )%> -
                                 <%# ((Eval("BalDue") * Eval("fltExchangeRateOfOrder")))%>

                                <asp:TextBox runat="server" Width="60px" ID="txtCurrencyAdjustment" class=""
                                    Style="text-align: right;" onkeypress="CheckNumber(1,event);" AutoComplete="OFF"
                                    Text='<%# String.Format("{0:###00.00}", (Eval("BalDue") * Eval("fltExchangeRateCurrent")) - (Eval("BalDue") * Eval("fltExchangeRateOfOrder") ) )%>'></asp:TextBox>
                            </ItemTemplate>

                        </asp:TemplateColumn>--%>
                        <asp:TemplateColumn>
                            <HeaderTemplate>
                                Payment
                            </HeaderTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                            <FooterStyle HorizontalAlign="Right" />
                            <ItemTemplate>
                                <%#Eval("varCurrSymbol")%>
                                <asp:TextBox runat="server" Width="60px" ID="txtAmountToPay" class="AmountToPay"
                                    Style="text-align: right;" onkeypress="CheckNumber(1,event);" AutoComplete="OFF"
                                    Text='<%# String.Format("{0:N}", Eval("monAmountPaidInDeposite"))%>'></asp:TextBox>
                                <asp:Label ID="lblConversionDifference" Text='<%#IIf(Eval("BaseCurrencySymbol") <> Eval("varCurrSymbol"), Eval("BaseCurrencySymbol") + String.Format("{0:###00.00}", Eval("monAmountPaidInDeposite") * Eval("fltExchangeRateCurrent")) , "")%>'
                                    runat="server" />
                                <asp:HiddenField ID="hdnShippingCharge" runat="server" Value='<%# Eval("monShippingCharge")%>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label Text="" runat="server" ID="lblFooterTotalPayment" />
                            </FooterTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <table width="100%" id="NewOrderArea" runat="server" visible="false">
        <tr>
            <td>
                <asp:CheckBox ID="chkCreateInvoice" runat="server" />Apply to Invoice for Sales Order:
                <asp:Label ID="lblSalesOrderName" runat="server" Text="Label"></asp:Label>
                , using the following Invoice Template 
                <asp:DropDownList ID="ddlOrderTemplates" runat="server"></asp:DropDownList>
                in in the amount of
                <asp:TextBox ID="txtOrderAmount" runat="server" onkeypress="CheckNumber(1,event);"></asp:TextBox>
                <asp:HiddenField ID="hdnOrderAmount" runat="server" />
                <asp:HiddenField ID="hdnNewOrderOpptype" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox ID="chkItemShipped" runat="server" />Items Shipped / Received by customer
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox ID="chkThermalReceipt" runat="server" />Print Thermal Receipt

            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td>
                <table>
                    <tr runat="server" id="trDeposite">
                        <td align="left">
                            <asp:RadioButton Text="Deposit to:" runat="server" ID="radDepositeTo" GroupName="DepositeGroup"
                                Checked="true" />
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="ddlDepositTo" runat="server" CssClass="required_numeric {required:'#radDepositeTo:checked', messages:{required:'Please select at least one Deposit To Account!'}}"
                                AutoPostBack="true">
                            </asp:DropDownList>
                            &nbsp;&nbsp;Balance:&nbsp;
                <asp:Label ID="lblOpeningBalance" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr runat="server" id="trDeposite1">
                        <td align="left" colspan="4">
                            <asp:RadioButton Text="Group with other undeposited funds" runat="server" ID="radUnderpositedFund"
                                GroupName="DepositeGroup" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="left">
                            <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete" OnClientClick="return confirm('Are you sure, you want to delete payment received?')" /></td>
                    </tr>
                </table>
            </td>
            <td>
                <table visible="false" id="tbl2" runat="server" border="0" align="right">
                    <tr>
                        <td align="right">
                            <asp:Label ID="lblBalance" Text="Total A/R Balance Due (All Invoices)" Font-Bold="true" runat="server" Visible="true"></asp:Label>:
                        </td>
                        <td align="left">
                            <asp:Label ID="lblBalanceAmt" runat="server" Visible="true"></asp:Label>
                            <asp:Label ID="lblTransChargeLabel" Text=" + (Transaction charge)" CssClass="signup"
                                runat="server" Visible="false" />
                            <asp:Label ID="lblTransCharge" Text="" CssClass="signup" runat="server" Visible="false" />&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right"><b>Amount to Receive:</b>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblAmountToPay" runat="server" Text="0.00"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right"><b>Credit Amount Applied:</b>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblCreditAmountApplied" runat="server" Text="0.00"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right"><b>Total Amount To Credit:</b>
                        </td>
                        <td align="left">
                            <asp:Label ID="lblAmountToCredit" runat="server" Text="0.00"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnSaveClose" runat="server" UseSubmitBehavior="false" CssClass="OrderButton btnAuthorizeCharge" style="width: 171px;" Text="Receive Payment"></asp:Button>
                            <asp:Button ID="btnSaveCloseWindow" runat="server" UseSubmitBehavior="false" CssClass="OrderButton btnAuthorizeCharge" style="white-space: normal;text-align: left;width: 168px;padding-top: 3px !important;padding-bottom: 3px !important;padding-right: 0px !important;padding-left: 45px !important;" Text="Receive Payment & Close Window"></asp:Button>
                            <%--<asp:Button ID="btnFClose" runat="server" Text="Close Window"  CssClass="OrderButton btnPaymentClose" style="width: 140px;" OnClientClick="window.close();"/>--%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>


    <%--  </asp:TableCell>
        </asp:TableRow>
    </asp:Table>--%>
    <%--<asp:HiddenField ID="AmountPaid" runat="Server" />--%>
    <asp:HiddenField ID="hdnOppType" runat="server" />
    <%--<asp:HiddenField ID="hdnContactID" runat="server" />--%>
    <%--<asp:HiddenField ID="hdnDivisionID" runat="server" />--%>
    <asp:HiddenField ID="hdnAmountPaid" runat="server" />
    <asp:HiddenField ID="hdnBizDocAmount" runat="server" />
    <asp:HiddenField ID="hdnSwipeTrack1" runat="server" />
    <asp:HiddenField ID="hdnSwipeTrack2" runat="server" />
    <asp:HiddenField ID="hdnIsDepositedToAccount" runat="server" />
    <asp:HiddenField ID="hdnDepositePage" runat="server" />
    <asp:HiddenField ID="hdnDepositeID" runat="server" />
    <asp:HiddenField ID="hdnDefaultCreditCard" runat="server" />
    <asp:HiddenField ID="hdnRefundAmount" runat="server" Value="0" />
    <asp:HiddenField ID="hdnSelectedInvoices" runat="server" />
    <asp:Button ID="btnGo" runat="server" Style="visibility: hidden" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <script src="../JavaScript/Signature/jquery.signaturepad.min.js" type="text/javascript"></script>
    <script>
        $(document).ready(function () {
            $('.sigPad').signaturePad({ drawOnly: true });
        });
    </script>
    <script src="../JavaScript/Signature/json2.min.js" type="text/javascript"></script>
    <uc1:TransactionInfo ID="TransactionInfo1" runat="server" />
</asp:Content>
