﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Partial Public Class frmSalesTemplate
    Inherits BACRMPage
    Dim lngOppId As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If GetQueryStringVal("OppId") > 0 Then
                lngOppId = GetQueryStringVal("OppId")
            End If
            If Not IsPostBack Then
                BindTemplateGrids()
                LoadLocations()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            Dim objOpp As New COpportunities
            objOpp.DomainID = Session("DomainID")
            objOpp.DivisionID = 0 'Procedure takes care if its 0
            objOpp.TemplateName = txtTemplateName.Text
            objOpp.TemplateType = IIf(chkMakeGeneric.Checked, 1, 0)
            objOpp.OpportID = lngOppId
            objOpp.UserCntID = Session("UserContactID")
            objOpp.AppliesTo = ddlTemplateApplied.SelectedValue
            objOpp.ManageSalesTemplate()
            BindTemplateGrids()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgSpecificTemplates_ItemCommand(ByVal source As Object, ByVal e As DataGridCommandEventArgs) Handles dgSpecificTemplates.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim objOpp As New COpportunities
                objOpp.SalesTemplateID = e.Item.Cells(0).Text
                objOpp.DomainID = Session("DomainID")
                objOpp.DeleteSalesTemplate()
                BindTemplateGrids()
            ElseIf e.CommandName = "Edit" Then
                Dim hdnNumDivisionID As HiddenField
                hdnNumDivisionID = e.Item.FindControl("hdnNumDivisionID")
                hdnDivisionID.Value = hdnNumDivisionID.Value

                Dim hdnNumSalesTemplateID As HiddenField
                hdnNumSalesTemplateID = e.Item.FindControl("hdnNumSalesTemplateID")
                hdnTemplateId.Value = hdnNumSalesTemplateID.Value
                txtEditTemplateName.Text = e.Item.Cells(1).Text
                BindSTItemDetails()
                Dim script As String = "function f(){$find(""" + rwEditTemplate.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f); InitialiseItemTextBox();"
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, True)
                'ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "OpenEditWindow", "OpenEditWindow();", True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgSpecificTemplates_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSpecificTemplates.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As LinkButton
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                Dim _appliesTo = e.Item.Cells(4).Text
                If _appliesTo = 0 Then
                    e.Item.Cells(4).Text = "All Orders & Opportunities"
                ElseIf _appliesTo = 1 Then
                    e.Item.Cells(4).Text = "Sales Opportunities"
                ElseIf _appliesTo = 2 Then
                    e.Item.Cells(4).Text = "Sales Orders"
                ElseIf _appliesTo = 3 Then
                    e.Item.Cells(4).Text = "Sales Opportunities & Orders"
                ElseIf _appliesTo = 4 Then
                    e.Item.Cells(4).Text = "Purchase Opportunities"
                ElseIf _appliesTo = 5 Then
                    e.Item.Cells(4).Text = "Purchase Orders"
                ElseIf _appliesTo = 6 Then
                    e.Item.Cells(4).Text = "Purchase Opportunities & Orders"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgGenericTemplates_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgGenericTemplates.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim objOpp As New COpportunities
                objOpp.SalesTemplateID = e.Item.Cells(0).Text
                objOpp.DomainID = Session("DomainID")
                objOpp.DeleteSalesTemplate()
                'BindGeneric()
                BindTemplateGrids()
            ElseIf e.CommandName = "Edit" Then
                Dim hdnGTNumDivisionID As HiddenField
                hdnGTNumDivisionID = e.Item.FindControl("hdnGTNumDivisionID")
                hdnDivisionID.Value = hdnGTNumDivisionID.Value

                Dim hdnGTNumSalesTemplateID As HiddenField
                hdnGTNumSalesTemplateID = e.Item.FindControl("hdnGTNumSalesTemplateID")
                hdnTemplateId.Value = hdnGTNumSalesTemplateID.Value
                txtEditTemplateName.Text = e.Item.Cells(1).Text
                BindSTItemDetails()
                Dim script As String = "function f(){$find(""" + rwEditTemplate.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f); InitialiseItemTextBox();"
                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgGenericTemplates_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgGenericTemplates.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As LinkButton
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")

                Dim _appliesTo = e.Item.Cells(3).Text
                If _appliesTo = 0 Then
                    e.Item.Cells(3).Text = "All Orders & Opportunities"
                ElseIf _appliesTo = 1 Then
                    e.Item.Cells(3).Text = "Sales Opportunities"
                ElseIf _appliesTo = 2 Then
                    e.Item.Cells(3).Text = "Sales Orders"
                ElseIf _appliesTo = 3 Then
                    e.Item.Cells(3).Text = "Sales Opportunities & Orders"
                ElseIf _appliesTo = 4 Then
                    e.Item.Cells(3).Text = "Purchase Opportunities"
                ElseIf _appliesTo = 5 Then
                    e.Item.Cells(3).Text = "Purchase Orders"
                ElseIf _appliesTo = 6 Then
                    e.Item.Cells(3).Text = "Purchase Opportunities & Orders"
                End If

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgSalesTemplateItems_ItemCommand(ByVal source As Object, ByVal e As DataGridCommandEventArgs) Handles dgSalesTemplateItems.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim objOpp As New COpportunities
                Dim lblSalesTemplateItemID As Label = e.Item.FindControl("lblSalesTemplateItemID")
                objOpp.SalesTemplateItemID = lblSalesTemplateItemID.Text
                objOpp.DomainID = Session("DomainID")
                objOpp.DeleteSalesTemplateItem()
            End If
            BindSTItemDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub btnSave_Click(sender As Object, e As EventArgs)
        Dim objOpp As New COpportunities

        If hdnSelectedItems.Value <> "" Then
            Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer()
            Dim objSelectedItem As SelectedItems = serializer.Deserialize(Of SelectedItems)(hdnSelectedItems.Value)

            objOpp.SalesTemplateID = hdnTemplateId.Value
            objOpp.DomainID = Session("DomainID")
            objOpp.ItemCode = objSelectedItem.numItemCode
            objOpp.WarehouseItmsID = objSelectedItem.numWarehouseItmsID
            objOpp.numUOM = objSelectedItem.numUOM
            objOpp.vcUOMName = objSelectedItem.vcUOMName
            objOpp.vcAttributes = objSelectedItem.vcAttributes
            objOpp.vcAttrValues = objSelectedItem.vcAttrValues
            objOpp.ManageSalesSpecificGenericTemplateItems()
        End If
        txtItem.Text = String.Empty
        hdnCurrentSelectedItem.Value = String.Empty
        hdnHasKitAsChild.Value = String.Empty
        BindSTItemDetails()
    End Sub

    Protected Sub btnSaveClose_Click(sender As Object, e As EventArgs)
        dgSalesTemplateItems.DataSource = Nothing
        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "CloseEditWindow", "CloseEditWindow();", True)
    End Sub

    Private Sub BindTemplateGrids()
        Try
            Dim objOpp As New COpportunities
            Dim dtTemplate As DataTable
            objOpp.DomainID = Session("DomainID")
            objOpp.SalesTemplateID = 0
            objOpp.OpportID = lngOppId
            objOpp.byteMode = 0
            objOpp.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            dtTemplate = objOpp.GetSalesTemplate()
            If dtTemplate IsNot Nothing AndAlso dtTemplate.Rows.Count > 0 Then
                Dim dvST As DataView
                dvST = New DataView(dtTemplate)
                dvST.RowFilter = " tintType = 0"

                If dvST IsNot Nothing AndAlso dvST.Count > 0 Then
                    dgSpecificTemplates.DataSource = dvST
                    dgSpecificTemplates.DataBind()
                    trLabelSpecificTemplates.Visible = True
                    dgSpecificTemplates.Visible = True
                Else
                    trLabelSpecificTemplates.Visible = False
                    dgSpecificTemplates.Visible = False
                End If

                Dim dvGT As DataView
                dvGT = New DataView(dtTemplate)
                dvGT.RowFilter = " tintType = 1"
                If dvGT IsNot Nothing AndAlso dvGT.Count > 0 Then
                    dgGenericTemplates.DataSource = dvGT
                    dgGenericTemplates.DataBind()
                    trLabelGenericTemplates.Visible = True
                    dgGenericTemplates.Visible = True
                Else
                    trLabelGenericTemplates.Visible = False
                    dgGenericTemplates.Visible = False
                End If
            Else
                trLabelSpecificTemplates.Visible = False
                dgSpecificTemplates.Visible = False
                trLabelGenericTemplates.Visible = False
                dgGenericTemplates.Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindSTItemDetails()
        Dim objOpp As New COpportunities
        'If hdnSelectedItems.Value <> "" Then
        '    Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer()
        '    Dim objSelectedItem As SelectedItems = serializer.Deserialize(Of SelectedItems)(hdnSelectedItems.Value)

        objOpp.DomainID = Session("DomainID")
        objOpp.SalesTemplateID = hdnTemplateId.Value
        'objOpp.ItemCode = objSelectedItem.numItemCode
        Dim dt = objOpp.GetSalesTemplateItemDetail()
        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
            dgSalesTemplateItems.DataSource = dt
            dgSalesTemplateItems.DataBind()
            dgSalesTemplateItems.Visible = True
        Else
            dgSalesTemplateItems.DataSource = Nothing
            dgSalesTemplateItems.Visible = False
        End If
        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "InitialiseItemTextBox", "InitialiseItemTextBox();", True)

        'End If
    End Sub

    Private Sub LoadLocations()
        Try
            Dim objItem As New BACRM.BusinessLogic.Item.CItems
            objItem.DomainID = Session("DomainID")
            Dim dtWareHouse As DataTable
            dtWareHouse = objItem.GetWareHouses()
            radcmbLocation.DataSource = dtWareHouse
            radcmbLocation.DataTextField = "vcWareHouse"
            radcmbLocation.DataValueField = "numWareHouseID"
            radcmbLocation.DataBind()

            If Not Session("DefaultWarehouse") Is Nothing Then
                If Not radcmbLocation.Items.FindItemByValue(Session("DefaultWarehouse").ToString()) Is Nothing Then
                    radcmbLocation.Items.FindItemByValue(Session("DefaultWarehouse")).Selected = True
                End If
            ElseIf radcmbLocation.Items.Count > 1 Then
                radcmbLocation.Items(1).Selected = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Class SelectedItems
        Public Property numItemCode As Long
        Public Property numWarehouseItmsID As String
        Public Property numUOM As Long
        Public Property vcUOMName As String
        Public Property vcAttributes As String
        Public Property vcAttrValues As String
    End Class

    'Private Sub BindGeneric()
    '    Dim objOpp As New COpportunities
    '    Dim dtTemplate As DataTable
    '    objOpp.DomainID = Session("DomainID")
    '    'objOpp.byteMode = 3
    '    objOpp.byteMode = 0
    '    dtTemplate = objOpp.GetSalesTemplate()
    '    If dtTemplate IsNot Nothing AndAlso dtTemplate.Rows.Count > 0 Then
    '        Dim dvGT As DataView
    '        dvGT = New DataView(dtTemplate)
    '        dvGT.RowFilter = " tintType = 1"

    '        'dgGenericTemplates.DataSource = dtTemplate
    '        dgGenericTemplates.DataSource = dvGT
    '        dgGenericTemplates.DataBind()
    '        trLabelGenericTemplates.Visible = True
    '    Else
    '        trLabelGenericTemplates.Visible = False
    '        dgGenericTemplates.Visible = False
    '    End If
    'End Sub

    'Commented by Neelam - Mar 13,2017 - Obsolete
    'Private Sub btnShowGeneric_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowGeneric.Click
    '    Try
    '        BindGeneric()
    '        trGeneric.Visible = True
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub
End Class