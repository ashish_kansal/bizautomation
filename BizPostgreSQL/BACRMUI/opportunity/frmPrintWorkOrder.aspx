﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPrintWorkOrder.aspx.vb"
    Inherits=".frmPrintWorkOrder" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Work Order</title>
    <script language="javascript">
        function pageLoad(sender, args) {
            window.print(); window.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Work Order
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td>
                <telerik:RadTreeList ID="rtlWorkOrder" runat="server" Width="1000px"
                ParentDataKeyNames="numParentWOID" DataKeyNames="numWOID" 
                AutoGenerateColumns="false" 
                OnItemDataBound="rtlWorkOrder_ItemDataBound" AllowRecursiveSelection="True">
                <HeaderStyle Font-Bold="true" HorizontalAlign="Center" />
                <DetailTemplate>
                    <table style="padding: 0; margin: 0; width: 100%; border-collapse: collapse;">
                        <tr>
                            <td style="font-weight: bold; text-align: left; border: none !important;">Created By / On:
                            </td>
                            <td style="border: none !important;">
                                <asp:Label ID="Label4" runat="server" Text='<%# Eval("vcCreated") %>'></asp:Label>
                            </td>
                            <td style="border: none !important;"></td>
                            <td style="font-weight: bold; text-align: right; border: none !important;">Instruction:
                            </td>
                            <td style="border: none !important;">
                                <asp:Label ID="lblNotes" runat="server" Text='<%# Eval("vcInstruction") %>'></asp:Label>
                            </td>
                            <td style="border: none !important;"></td>
                            <td style="font-weight: bold; text-align: right; border: none !important;">Completion Date:
                            </td>
                            <td style="border: none !important; border: none !important;">
                                <asp:Label ID="Label1" runat="server" Text='<%# Eval("bintCompliationDate") %>'></asp:Label>
                            </td>
                            <td style="border: none !important;"></td>
                            <td style="font-weight: bold; text-align: right; border: none !important;">Assigned To:
                            </td>
                            <td style="border: none !important;">
                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("vcAssignedTo") %>'></asp:Label>
                            </td>
                    </table>
                </DetailTemplate>
                <Columns>
                    <telerik:TreeListBoundColumn DataField="vcItemName" UniqueName="vcItemName" HeaderStyle-Width="100px" HeaderText="Item"></telerik:TreeListBoundColumn>
                    <telerik:TreeListBoundColumn DataField="numQty" UniqueName="numQty"  HeaderText="Quantity" HeaderStyle-Width="50px" ItemStyle-HorizontalAlign="Left"></telerik:TreeListBoundColumn>
                    <telerik:TreeListBoundColumn DataField="vcWarehouse" UniqueName="vcWarehouse" HeaderStyle-Width="60px"  HeaderText="Warehouse"></telerik:TreeListBoundColumn>
                    <telerik:TreeListBoundColumn DataField="numOnHand" UniqueName="numOnHand" HeaderText="On Hand" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Left"></telerik:TreeListBoundColumn>
                    <telerik:TreeListBoundColumn DataField="numOnOrder" UniqueName="numOnOrder" HeaderText="On Order" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Left"></telerik:TreeListBoundColumn>
                    <telerik:TreeListBoundColumn DataField="numAllocation" UniqueName="numAllocation" HeaderText="Allocation" HeaderStyle-Width="60px" ItemStyle-HorizontalAlign="Left"></telerik:TreeListBoundColumn>
                    <telerik:TreeListBoundColumn DataField="numBackOrder" UniqueName="numBackOrder" HeaderText="Back Order" HeaderStyle-Width="70px" ItemStyle-HorizontalAlign="Left"></telerik:TreeListBoundColumn>
                    <%--<telerik:TreeListTemplateColumn HeaderText="Assembly Status" HeaderStyle-Width="157px" UniqueName="AssemblyStatus">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlWOAssemblyStatus" runat="server" CssClass="signup" onchange="ChangeAssemblyStatus(this);">
                            </asp:DropDownList>
                        </ItemTemplate>
                    </telerik:TreeListTemplateColumn>--%>
                    <telerik:TreeListTemplateColumn  HeaderText="Sales Order" HeaderStyle-Width="150px" UniqueName="Sales Order">
                        <ItemTemplate>
                            <a  onclick='OpenOpp(<%#Eval("numOppID")%>)' href="#">
                                <%#Eval("vcOppName")%></a>
                        </ItemTemplate>
                         <ItemStyle HorizontalAlign="Left" />
                    </telerik:TreeListTemplateColumn>
                    <telerik:TreeListTemplateColumn HeaderText="Purchase Order" HeaderStyle-Width="150px" UniqueName="Purchase Order">
                        <ItemTemplate>
                            <a onclick='OpenOpp(<%#Eval("numPOID")%>)' href="#">
                                <%#Eval("vcPOName")%></a>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                    </telerik:TreeListTemplateColumn>
              <%--      <telerik:TreeListTemplateColumn ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="25px">
                        <ItemStyle HorizontalAlign="Center" />
                        <ItemTemplate>
                            <asp:CheckBox ID="chkDelete" runat="server" />
                        </ItemTemplate>
                    </telerik:TreeListTemplateColumn>--%>
                    <%-- HIDDEN COLUMNS --%>
                    <telerik:TreeListBoundColumn DataField="numParentWOID" UniqueName="numParentWOID" HeaderText="Parent Work Order" Visible="false"></telerik:TreeListBoundColumn>
                    <telerik:TreeListBoundColumn DataField="numWOID" UniqueName="numWOID" HeaderText="Work Order" Visible="false"></telerik:TreeListBoundColumn>
                    <telerik:TreeListBoundColumn DataField="bitWorkOrder" UniqueName="bitWorkOrder" HeaderText="bitWorkOrder" Visible="false"></telerik:TreeListBoundColumn>
                    <telerik:TreeListBoundColumn DataField="numItemCode" UniqueName="numItemCode" HeaderText="Item Code" Visible="false"></telerik:TreeListBoundColumn>
                    <telerik:TreeListBoundColumn DataField="numWarehouseItemID" UniqueName="numWarehouseItemID" HeaderText="WarehuoseID" Visible="false"></telerik:TreeListBoundColumn>
                    <telerik:TreeListBoundColumn DataField="numWOStatus" UniqueName="numWOStatus" HeaderText="numWOStatus" Visible="false"></telerik:TreeListBoundColumn>
                    <telerik:TreeListBoundColumn DataField="numOppID" UniqueName="numOppID" HeaderText="numOppID" Visible="false"></telerik:TreeListBoundColumn>
                    <telerik:TreeListBoundColumn DataField="numPOID" UniqueName="numPOID" HeaderText="numPOID" Visible="false"></telerik:TreeListBoundColumn>
                    
                </Columns>
            </telerik:RadTreeList>
                <%--<telerik:RadGrid ID="RadGrid1" runat="server" OnColumnCreated="RadGrid1_ColumnCreated"
                    OnItemCreated="RadGrid1_ItemCreated" OnItemDataBound="RadGrid1_ItemDataBound"
                    AutoGenerateColumns="false" Skin="windows" EnableEmbeddedSkins="false" CssClass="tbl aspTable"
                    EnableLinqExpressions="false">
                    <MasterTableView HierarchyDefaultExpanded="true" HierarchyLoadMode="Client" DataKeyNames="numParentWOID,numWOId,numItemCode,ItemLevel"
                        Width="100%" GridLines="Both" AllowSorting="true" CssClass="dg">
                        <SelfHierarchySettings ParentKeyName="numParentWOID" KeyName="numWOId" />
                        <Columns>
                            <telerik:GridBoundColumn DataField="numItemCode" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="numWarehouseItmsID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ItemLevel" ItemStyle-Width="3%">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="vcCreatedBy" HeaderText="Created By, On" ItemStyle-Width="10%"
                                HeaderStyle-HorizontalAlign="Left" SortExpression="vcCreatedBy" UniqueName="Created By, On"
                                Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="vcItemName" HeaderText="Assembly Hierarchy" ItemStyle-Width="10%"
                                HeaderStyle-HorizontalAlign="Left" SortExpression="vcItemName" UniqueName="Assembly Hierarchy">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="vcWareHouse" HeaderText="Warehouse" ItemStyle-Width="10%"
                                ItemStyle-HorizontalAlign="Center" SortExpression="vcWareHouse" UniqueName="Warehouse">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="numQty" HeaderText="Qty." ItemStyle-Width="6%"
                                ItemStyle-HorizontalAlign="Center" SortExpression="numQtyItemsReq" UniqueName="Qty">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="vcItemDesc" HeaderText="Desc" ItemStyle-Width="17%"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" AllowSorting="false"
                                UniqueName="Description">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Sales Order Name" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center"
                                UniqueName="Sales Order Name" Visible="false">
                                <ItemTemplate>
                                    <a onclick='OpenOpp(<%#Eval("numOppId") %>)' href="#">
                                        <%#Eval("vcOppName") %></a>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Assembly Status" ItemStyle-Width="9%" ItemStyle-HorizontalAlign="Center"
                                Visible="false" UniqueName="Assembly Status">
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlWOAssemblyStatus" runat="server" CssClass="signup">
                                    </asp:DropDownList>
                                    <asp:Label ID="lblWOAssemblyStatus" runat="server" CssClass="signup"></asp:Label>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn DataField="vcWOAssignedTo" HeaderText="Assign To" ItemStyle-Width="16%"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" AllowSorting="false"
                                UniqueName="Assign To" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="vcInstruction" HeaderText="Instruction" ItemStyle-Width="16%"
                                ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" AllowSorting="false"
                                UniqueName="Instruction">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="bintCompliationDate" HeaderText="Compl. Date"
                                ItemStyle-Width="9%" ItemStyle-HorizontalAlign="Center" SortExpression="bintCompliationDate"
                                UniqueName="Completion date" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="SOComments" HeaderText="SO Comments" ItemStyle-Width="9%"
                                ItemStyle-HorizontalAlign="Center" SortExpression="SOComments" AllowSorting="false"
                                UniqueName="SOComments" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="UOM" HeaderText="UOM" ItemStyle-Width="9%" ItemStyle-HorizontalAlign="Center"
                                SortExpression="UOM" AllowSorting="false" UniqueName="UOM">
                            </telerik:GridBoundColumn>
                        </Columns>
                        <NoRecordsTemplate>
                        </NoRecordsTemplate>
                    </MasterTableView>
                    <ClientSettings AllowExpandCollapse="true" />
                </telerik:RadGrid>--%>
            </td>
        </tr>
    </table>
</asp:Content>
