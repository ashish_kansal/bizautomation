﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports Telerik.Web.UI
Imports System.Web.Services
Imports System
Imports BACRM.BusinessLogic.Item
Imports System.Collections.Generic

Namespace BACRM.UserInterface.Opportunities
    Public Class frmWorkOrder2
        Inherits BACRMPage
        Dim RegularSearch As String
        Dim CustomSearch As String
        Dim m_aryRightsOpenView() As Integer
        Dim m_aryRightsFinishedView() As Integer

#Region "Page Events"

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                m_aryRightsOpenView = GetUserRightsForPage_Other(16, 152)
                m_aryRightsFinishedView = GetUserRightsForPage_Other(16, 156)
                

                If Not IsPostBack Then
                    Dim m_aryRightsFinishWorkOrder() As Integer = GetUserRightsForPage_Other(16, 154)
                    Dim m_aryRightsDeleteWorkOrder() As Integer = GetUserRightsForPage_Other(16, 155)
                    Dim m_aryRightsPickWorkOrder() As Integer = GetUserRightsForPage_Other(16, 161)
                    Dim m_aryRightsForGridConfig() As Integer = GetUserRightsForPage_Other(16, 164)

                    If m_aryRightsFinishWorkOrder(RIGHTSTYPE.VIEW) = 0 Then
                        btnFinishWorkOrder.Visible = False
                    End If

                    If m_aryRightsDeleteWorkOrder(RIGHTSTYPE.VIEW) = 0 Then
                        btnRemove.Visible = False
                    End If

                    If m_aryRightsPickWorkOrder(RIGHTSTYPE.VIEW) = 0 Then
                        btnPick.Visible = False
                        btnPickSelected.Visible = False
                    End If

                    If m_aryRightsForGridConfig(RIGHTSTYPE.VIEW) = 0 Then
                        Dim tdGridConfiguration As HtmlAnchor = CType(CCommon.FindControlRecursive(Page.Master, "tdGridConfiguration"), HtmlAnchor)

                        If Not tdGridConfiguration Is Nothing Then
                            tdGridConfiguration.Visible = False
                        End If
                    End If

                    Dim strorderstatus As String
                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                        txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                        txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))
                        txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                        txtGridColumnFilter.Text = CCommon.ToString(PersistTable(PersistKey.GridColumnSearch))
                        strorderstatus = CCommon.ToString(PersistTable(PersistKey.OrderStatus))

                        If Not CCommon.ToString(strorderstatus) = "" Then
                            If strorderstatus = "0" Then
                                radOpen.Checked = True
                            ElseIf strorderstatus = "1" Then
                                radClosed.Checked = True
                            Else
                                radOpen.Checked = True
                            End If
                        Else
                            radOpen.Checked = True
                        End If
                    End If

                    If (CCommon.ToInteger(txtCurrrentPage.Text) = 0) Then
                        txtCurrrentPage.Text = "1"
                    End If

                    If m_aryRightsOpenView(RIGHTSTYPE.VIEW) = 0 AndAlso m_aryRightsFinishedView(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC&Module=Manufacturing&Permission=Work Orders Grid")
                    ElseIf m_aryRightsOpenView(RIGHTSTYPE.VIEW) = 0 Then
                        radOpen.Visible = False
                        radOpen.Checked = False
                        radClosed.Checked = True
                    ElseIf m_aryRightsFinishedView(RIGHTSTYPE.VIEW) = 0 Then
                        radClosed.Visible = False
                        radClosed.Checked = False
                        radOpen.Checked = True
                    End If

                    BindDatagrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

#End Region

#Region "Private Methods"

        Private Sub BindDatagrid()
            Try
                Dim dtOpportunity As DataTable
                Dim objOpportunity As New COpportunities
                objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                Dim SortChar As Char

                ' Set Default Filter 
                With objOpportunity
                    If radOpen.Checked Then
                        .UserRightType = m_aryRightsOpenView(RIGHTSTYPE.VIEW)
                    ElseIf radClosed.Checked Then
                        .UserRightType = m_aryRightsFinishedView(RIGHTSTYPE.VIEW)
                    Else
                        .UserRightType = 0
                    End If

                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")
                    .endDate = Now()
                    .SortCharacter = txtSortChar.Text.Trim()

                    If radOpen.Checked Then
                        .Shipped = 0
                    ElseIf radClosed.Checked Then
                        .Shipped = 1
                    Else
                        .Shipped = 0
                    End If
                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()

                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0

                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If
                    GridColumnSearchCriteria()

                    .RegularSearchCriteria = RegularSearch

                    .CustomSearchCriteria = ""
                End With
                Dim dsList As DataSet

                dsList = objOpportunity.GetWorkOrderList()
                dtOpportunity = dsList.Tables(0)

                Dim dtTableInfo As DataTable = dsList.Tables(1)
                
                Dim strorderstatus As String
                If radOpen.Checked Then
                    strorderstatus = 0
                ElseIf radClosed.Checked Then
                    strorderstatus = 1
                Else
                    strorderstatus = 0
                End If

                'Persist Form Settings
                PersistTable.Clear()
                PersistTable.Add(PersistKey.CurrentPage, IIf(dtOpportunity.Rows.Count > 0, txtCurrrentPage.Text, "1"))
                PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())
                PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
                PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text.Trim())
                PersistTable.Add(PersistKey.GridColumnSearch, txtGridColumnFilter.Text)
                PersistTable.Add(PersistKey.OrderStatus, strorderstatus)
                PersistTable.Save()

                If objOpportunity.TotalRecords = 0 Then
                    txtTotalRecordsDeals.Text = 0
                Else
                    Dim strTotalPage As String()
                    Dim decTotalPage As Decimal
                    decTotalPage = objOpportunity.TotalRecords / Session("PagingRows")
                    decTotalPage = Math.Round(decTotalPage, 2)
                    strTotalPage = CStr(decTotalPage).Split(".")
                    If (objOpportunity.TotalRecords Mod Session("PagingRows")) = 0 Then
                        txtTotalPageDeals.Text = strTotalPage(0)
                    Else
                        txtTotalPageDeals.Text = strTotalPage(0) + 1
                    End If
                    txtTotalRecordsDeals.Text = objOpportunity.TotalRecords
                End If
                Dim htGridColumnSearch As New Hashtable

                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")
                    Dim strIDValue() As String

                    For i = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")

                        htGridColumnSearch.Add(strIDValue(0), strIDValue(1))
                    Next
                End If

                Dim bField As BoundField
                Dim Tfield As TemplateField
                gvSearch.Columns.Clear()

                For Each drRow As DataRow In dtTableInfo.Rows
                    Tfield = New TemplateField

                    Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, drRow, 0, htGridColumnSearch, 145, objOpportunity.columnName, objOpportunity.columnSortOrder)

                    Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, drRow, 0, htGridColumnSearch, 145, objOpportunity.columnName, objOpportunity.columnSortOrder)
                    gvSearch.Columns.Add(Tfield)
                Next

                If radOpen.Checked = True Then
                    Dim dr As DataRow
                    dr = dtTableInfo.NewRow()
                    dr("vcAssociatedControlType") = "DeleteCheckBox"
                    dr("intColumnWidth") = "30"

                    Tfield = New TemplateField
                    Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, dr, 0, htGridColumnSearch, 145)
                    Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, dr, 0, htGridColumnSearch, 145)
                    gvSearch.Columns.Add(Tfield)
                End If

                bizPager.PageSize = Session("PagingRows")
                bizPager.RecordCount = objOpportunity.TotalRecords
                bizPager.CurrentPageIndex = txtCurrrentPage.Text
                gvSearch.DataSource = dtOpportunity
                gvSearch.DataBind()
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub GridColumnSearchCriteria()
            Try
                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")

                    Dim strIDValue(), strID(), strCustom As String
                    Dim strRegularCondition As New ArrayList
                    Dim strCustomCondition As New ArrayList

                    For i As Integer = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")
                        strID = strIDValue(0).Split("~")

                        If strID(0).Contains("CFW.Cust") Then

                            Select Case strID(3).Trim()
                                Case "TextBox", "TextArea"
                                    strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & " ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                Case "CheckBox"
                                    If strIDValue(1).ToLower() = "yes" Then
                                        strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and COALESCE(CFW.Fld_Value,'0') " & "='1')")
                                    ElseIf strIDValue(1).ToLower() = "no" Then
                                        strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND 1 = (CASE WHEN (SELECT COUNT(*) FROM CFW_Fld_Values_Opp CFWInner WHERE CFWInner.RecId=OpportunityMaster.numOppId AND CFWInner.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND CFWInner.Fld_Value='1') > 0 THEN 0 ELSE 1 END))")
                                    End If
                                Case "SelectBox"
                                    strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & "=" & strIDValue(1) & ")")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " >= '" & fromDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " <= '" & toDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    End If
                                Case "CheckBoxList"
                                    Dim items As String() = strIDValue(1).Split(",")
                                    Dim searchString As String = ""

                                    For Each item As String In items
                                        searchString = searchString & If(searchString.Length > 0, " OR ", "") & " GetCustFldValueOpp(" & strID(0).Replace("CFW.Cust", "") & ",OpportunityMaster.numOppID) " & " ilike '%" & item.Replace("'", "''") & "%'"
                                    Next

                                    strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND (" & searchString & "))")
                                Case Else
                                    strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & strCustom & ")")
                            End Select
                        Else
                            Select Case strID(3).Trim()
                                Case "Website", "Email", "TextBox", "Label"
                                    If strID(0) = "WO.vcWorkOrderNameWithQty" Then
                                        strRegularCondition.Add("(WO.vcWorkOrderName ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                    ElseIf strID(0) = "I.AssemblyItemSKU" Then
                                        strRegularCondition.Add("(I.vcItemName ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                    Else
                                        strRegularCondition.Add(strID(0) & " ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                    End If
                                Case "SelectBox"
                                    If strID(0) = "WO.MaxBuildQty" Then
                                        If strIDValue(1) = 1 Then
                                            strRegularCondition.Add("(select fn_CalculateMinBuildQtyByItem(WO.numItemCode,WHI.numWareHouseID)) >= WO.numQtyItemsReq")
                                        ElseIf strIDValue(1) = 2 Then
                                            strRegularCondition.Add("(select fn_CalculateMinBuildQtyByItem(WO.numItemCode,WHI.numWareHouseID)) < WO.numQtyItemsReq")
                                        End If
                                    ElseIf strID(0) = "SP.Slp_Name" Then
                                        strRegularCondition.Add("SP.Slp_id" & " IN (" & strIDValue(1) & ")")
                                    ElseIf strID(0) = "WO.vcAssignedTo" Then
                                        strRegularCondition.Add("WO.numAssignedTo" & " IN (" & strIDValue(1) & ")")
                                    Else
                                        strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                                    End If

                                Case "TextArea"
                                    strRegularCondition.Add(" Cast(" & strID(0) & " as varchar(5000)) ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                Case "TextBoxRangePercentage"

                                    If strID(4) = "RangeFrom" Then
                                        If CCommon.ToLong(strIDValue(1)) >= 0 Then
                                            strRegularCondition.Add(strID(0) & " >= '" & CCommon.ToLong(strIDValue(1)) & "'")
                                        End If
                                    ElseIf strID(4) = "RangeTo" Then
                                        If CCommon.ToLong(strIDValue(1)) >= 0 Then
                                            strRegularCondition.Add(strID(0) & " <= '" & CCommon.ToLong(strIDValue(1)) & "'")
                                        End If
                                    End If
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strRegularCondition.Add(strID(0) & " >= '" & fromDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strRegularCondition.Add(strID(0) & " <= '" & toDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    End If
                            End Select
                        End If
                    Next
                    RegularSearch = String.Join(" and ", strRegularCondition.ToArray())
                    CustomSearch = String.Join(" and ", strCustomCondition.ToArray())
                Else
                    RegularSearch = ""
                    CustomSearch = ""
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Private Shared Sub BinWorkOrderOpenColumns(dtTableInfo As DataTable)
            Try
                Dim dr As DataRow = dtTableInfo.NewRow()
                dr("vcFieldName") = "Work Order(Qty)"
                dr("vcDbColumnName") = "vcWorkOrderNameWithQty"
                dr("vcOrigDbColumnName") = "vcWorkOrderNameWithQty"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 1
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "TextBox"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "WorkOrder"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Max Build Qty"
                dr("vcDbColumnName") = "MaxBuildQty"
                dr("vcOrigDbColumnName") = "MaxBuildQty"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 2
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "SelectBox"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "WorkOrder"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Total Progress"
                dr("vcDbColumnName") = "TotalProgress"
                dr("vcOrigDbColumnName") = "TotalProgress"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 3
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "TextBoxRangePercentage"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "WorkOrder"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Item"
                dr("vcDbColumnName") = "AssemblyItemSKU"
                dr("vcOrigDbColumnName") = "AssemblyItemSKU"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 4
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "TextBox"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "Item"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Planned Start"
                dr("vcDbColumnName") = "dtmStartDate"
                dr("vcOrigDbColumnName") = "dtmStartDate"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 5
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "DateField"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "WorkOrder"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Actual Start"
                dr("vcDbColumnName") = "dtmActualStartDate"
                dr("vcOrigDbColumnName") = "dtmActualStartDate"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 0
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "DateField"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "WorkOrder"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Requested Finish"
                dr("vcDbColumnName") = "dtmEndDate"
                dr("vcOrigDbColumnName") = "dtmEndDate"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 0
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "DateField"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "WorkOrder"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Projected Finish"
                dr("vcDbColumnName") = "dtmProjectFinishDate"
                dr("vcOrigDbColumnName") = "dtmProjectFinishDate"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 0
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "DateField"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "WorkOrder"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Item Release"
                dr("vcDbColumnName") = "ItemReleaseDate"
                dr("vcOrigDbColumnName") = "ItemReleaseDate"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 0
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "DateField"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "OpportunityItems"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Forecast"
                dr("vcDbColumnName") = "ForecastDetails"
                dr("vcOrigDbColumnName") = "ForecastDetails"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 0
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "SelectBox"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "WorkOrder"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Build Process"
                dr("vcDbColumnName") = "Slp_Name"
                dr("vcOrigDbColumnName") = "Slp_Name"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 0
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "SelectBox"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "Sales_process_List_Master"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Build Manager"
                dr("vcDbColumnName") = "vcAssignedTo"
                dr("vcOrigDbColumnName") = "vcAssignedTo"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 0
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "SelectBox"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "WorkOrder"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Capacity Load"
                dr("vcDbColumnName") = "CapacityLoad"
                dr("vcOrigDbColumnName") = "CapacityLoad"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 0
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "TextBoxRangePercentage"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "WorkOrder"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Shared Sub BinWorkOrderClosedColumns(dtTableInfo As DataTable)
            Try
                Dim dr As DataRow = dtTableInfo.NewRow()
                dr("vcFieldName") = "Work Order"
                dr("vcDbColumnName") = "vcWorkOrderName"
                dr("vcOrigDbColumnName") = "vcWorkOrderName"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 0
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "TextBox"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "WorkOrder"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Assembly Item (SKU) "
                dr("vcDbColumnName") = "AssemblyItemSKU"
                dr("vcOrigDbColumnName") = "AssemblyItemSKU"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 0
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "TextBox"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "Item"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Quantity"
                dr("vcDbColumnName") = "numQtyItemsReq"
                dr("vcOrigDbColumnName") = "numQtyItemsReq"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 0
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "TextBox"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "WorkOrder"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Requested Finish"
                dr("vcDbColumnName") = "dtmEndDate"
                dr("vcOrigDbColumnName") = "dtmEndDate"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 0
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "DateField"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "WorkOrder"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Actual Finish"
                dr("vcDbColumnName") = "dtmProjectFinishDate"
                dr("vcOrigDbColumnName") = "dtmProjectFinishDate"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 0
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "DateField"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "WorkOrder"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Item Release"
                dr("vcDbColumnName") = "ItemReleaseDate"
                dr("vcOrigDbColumnName") = "ItemReleaseDate"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 0
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "DateField"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "OpportunityItems"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Schedule Performance "
                dr("vcDbColumnName") = "SchedulePerformance"
                dr("vcOrigDbColumnName") = "SchedulePerformance"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 0
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "SelectBox"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "WorkOrder"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Actual Time & $ vs Projected"
                dr("vcDbColumnName") = "ActualProjectDifference"
                dr("vcOrigDbColumnName") = "ActualProjectDifference"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 0
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "SelectBox"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "WorkOrder"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Build Process"
                dr("vcDbColumnName") = "Slp_Name"
                dr("vcOrigDbColumnName") = "Slp_Name"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 0
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "SelectBox"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "Sales_process_List_Master"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Build Manager"
                dr("vcDbColumnName") = "vcAssignedTo"
                dr("vcOrigDbColumnName") = "vcAssignedTo"
                dr("bitAllowSorting") = False
                dr("numFieldId") = 0
                dr("bitAllowEdit") = 0
                dr("bitCustomField") = 0
                dr("vcAssociatedControlType") = "SelectBox"
                dr("vcListItemType") = ""
                dr("numListID") = 0
                dr("ListRelID") = "0"
                dr("vcLookBackTableName") = "WorkOrder"
                dr("bitAllowFiltering") = True
                dr("vcFieldDataType") = "V"
                dtTableInfo.Rows.Add(dr)
            Catch ex As Exception
                Throw
            End Try
        End Sub

#End Region

#Region "Event Handlers"

        Private Sub radClosed_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radClosed.CheckedChanged
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub radOpen_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radOpen.CheckedChanged
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnRemove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRemove.Click
            Try
                If txtDelWoId.Text <> "" Then
                    Dim strWoId As String() = txtDelWoId.Text.Split(",")
                    Dim i As Int16 = 0
                    Dim lngWoID As Long = 0

                    litError.Text = ""
                    Dim bError As Boolean = False
                    Dim objItems As New CItems
                    Dim listWorkOrder As New List(Of WorkOrderDetail)
                    For i = 0 To strWoId.Length - 1
                        lngWoID = strWoId(i).Split("~")(0)
                        Dim objWorkOrderDetail As New WorkOrderDetail
                        objWorkOrderDetail.numWOID = lngWoID
                        listWorkOrder.Add(objWorkOrderDetail)
                    Next
                    Using objTransactionScope As New Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                        For Each item As WorkOrderDetail In listWorkOrder.OrderByDescending(Function(x) x.numLevel)
                            objItems.numWOId = item.numWOID
                            objItems.UserCntID = Session("UserContactID")
                            objItems.DomainID = Session("DomainID")
                            objItems.bitOrderDelete = False
                            objItems.DeleteWorkOrder()
                        Next

                        objTransactionScope.Complete()
                    End Using
                End If
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnFinishWorkOrder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFinishWorkOrder.Click
            Try
                If txtDelWoId.Text <> "" Then
                    Dim strWoId As String() = txtDelWoId.Text.Split(",")
                    Dim i As Int16 = 0
                    Dim lngWoID As Long
                    Dim objOpport As New COpportunities
                    Dim objCommon As New CCommon
                    objCommon.DomainID = Session("DomainID")
                    Dim dtCommitAllocation As DataTable = objCommon.GetDomainSettingValue("tintCommitAllocation")

                    If dtCommitAllocation Is Nothing Or dtCommitAllocation.Rows.Count = 0 Then
                        Throw New Exception("Not able to fetch commmit allocation global settings value.")
                        Exit Sub
                    End If

                    litError.Text = ""
                    Dim bError As Boolean = False
                    For i = 0 To strWoId.Length - 1
                        lngWoID = strWoId(i).Split("~")(0)

                        'Validate if there are any task which are pending to complete
                        Dim objWorkOrder As New WorkOrder
                        objWorkOrder.DomainID = CCommon.ToLong(Session("DomainID"))
                        objWorkOrder.WorkOrderID = lngWoID
                        If objWorkOrder.IsTaskPending() Then
                            litError.Text += "<br/>WO " & lngWoID & "Work order could not be completed until all tasks are finished."
                        Else
                            Dim objItems As New CItems

                            Try
                                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                    Dim itemCode As Long = CCommon.ToLong(objWorkOrder.GetFieldValue("numItemCode"))
                                    Dim quantityBuilt As Double = CCommon.ToLong(objWorkOrder.GetFieldValue("numQuantityBuilt"))

                                    'KEEP THIS BELOW ABOVE CALLES OTHERWISE QuantityBuild WILL BE WRONG VALUE
                                    objItems.numWOId = lngWoID
                                    objItems.numWOStatus = 23184
                                    objItems.UserCntID = Session("UserContactID")
                                    objItems.DomainID = Session("DomainID")
                                    objItems.ManageWorkOrderStatus()

                                    objTransactionScope.Complete()
                                End Using
                            Catch ex As Exception
                                If Not ex.Message.Contains("WORKORDER_IS_ALREADY_COMPLETED") Then
                                    bError = True
                                    litError.Text += "<br/>WO " & objItems.numWOId & ":" & " You must have to pick all BOM before work order can be finished."
                                End If
                            End Try
                        End If
                    Next
                    If bError = True Then
                        litError.Text = "Some work order could not be completed due to lack of required quantity to fullfill work order:" + litError.Text
                        Exit Sub
                    End If

                    BindDatagrid()
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                txtCurrrentPage_TextChanged(Nothing, Nothing)
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub txtCurrrentPage_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub gvSearch_RowDataBound(sender As Object, e As GridViewRowEventArgs)
            Try
                If e.Row.RowType = DataControlRowType.DataRow Then
                    Dim drv As DataRowView = DirectCast(e.Row.DataItem, DataRowView)

                    If Not e.Row.FindControl("hfRequiredFinishDate") Is Nothing Then
                        DirectCast(e.Row.FindControl("hfRequiredFinishDate"), HiddenField).Value = If(drv("dtmEndDate") Is DBNull.Value, DateTime.UtcNow.AddMinutes(-1 * Session("ClientMachineUTCTimeOffset")), drv("dtmEndDate"))
                    End If

                    If Not e.Row.FindControl("hdnWorkOrderName") Is Nothing Then
                        DirectCast(e.Row.FindControl("hdnWorkOrderName"), HiddenField).Value = CCommon.ToString(drv("vcWorkOrderName"))
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub btnPickSelected_Click(sender As Object, e As EventArgs)
            Try
                If txtDelWoId.Text <> "" Then
                    Dim definition = {New With {Key .Name = "", .ID = 0}}
                    Dim listWorkOrders As IEnumerable(Of Object) = Newtonsoft.Json.JsonConvert.DeserializeAnonymousType(txtDelWoId.Text, definition).ToList()

                    txtDelWoId.Text = ""

                    Dim vcErrorRecords As String = ""
                    Dim objWOrkOrder As New WorkOrder
                    objWOrkOrder.DomainID = CCommon.ToLong(Session("DomainID"))
                    objWOrkOrder.UserCntID = CCommon.ToLong(Session("UserContactID"))

                    For Each objItem As Object In listWorkOrders
                        Try
                            objWOrkOrder.WorkOrderID = objItem.ID
                            objWOrkOrder.PickItems()
                        Catch ex As Exception
                            vcErrorRecords = vcErrorRecords & If(vcErrorRecords.Length > 0, "<br />", "") & objItem.Name & ":"

                            If ex.Message.Contains("PICKED_QTY_GREATER_THAN_REQUIRED_QTY") Then
                                vcErrorRecords += " Qty picked can not be greater than quantity left to be picked."
                            ElseIf ex.Message.Contains("INVALID_PICKED_QTY_ALLOCATION") Then
                                vcErrorRecords += " Pick work order manually through pick list"
                            Else
                                vcErrorRecords += " Unknown error occurred."
                            End If
                        End Try
                    Next

                    If vcErrorRecords.Length > 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "ErrorRecords", "alert('Error occurred in following records: " & vcErrorRecords & "')", True)
                    End If

                    BindDatagrid()
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "SelectRecords", "alert('Select at least one record.')", True)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

#End Region

        Private Class WorkOrderDetail
            Private _numWOID As Long
            Private _numWOStatus As Long
            Private _numLevel As Integer

            Public Property numWOID() As Long
                Get
                    Return _numWOID
                End Get
                Set(ByVal value As Long)
                    _numWOID = value
                End Set
            End Property
            Public Property numWOStatus() As Long
                Get
                    Return _numWOStatus
                End Get
                Set(ByVal value As Long)
                    _numWOStatus = value
                End Set
            End Property
            Public Property numLevel() As Integer
                Get
                    Return _numLevel
                End Get
                Set(ByVal value As Integer)
                    _numLevel = value
                End Set
            End Property
        End Class

    End Class

End Namespace