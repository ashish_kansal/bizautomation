﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Partial Public Class frmEmbeddedCostCopy
    Inherits BACRMPage
    Dim lngOppID As Long
    Dim lngOppBizDocID As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngOppID = CCommon.ToLong(GetQueryStringVal( "OppID"))
            lngOppBizDocID = CCommon.ToLong(GetQueryStringVal( "OppBizDocId"))
            If Not IsPostBack Then
                BindBizDoc()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub BindBizDoc()
        Try
            Dim objOppBizDocs As New OppBizDocs
            objOppBizDocs.OppId = lngOppID
            objOppBizDocs.IsAuthoritativeBizDoc = 0
            objOppBizDocs.byteMode = 1
            objOppBizDocs.OppBizDocId = lngOppBizDocID
            Dim ds As DataSet = objOppBizDocs.GetBizDocsInOpp()
            ddlBizDoc.DataTextField = "vcBizDocName"
            ddlBizDoc.DataValueField = "numOppBizDocsId"
            ddlBizDoc.DataSource = ds
            ddlBizDoc.DataBind()
            ddlBizDoc.Items.Insert(0, New ListItem("--Select One--", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objOpp As New MOpportunity
            If ddlBizDoc.SelectedValue > 0 Then

                objOpp.DomainID = Session("DomainID")
                objOpp.CostCategoryID = 0
                objOpp.OppBizDocID = lngOppBizDocID
                Try
                    objOpp.DeleteEmbeddedCost()
                Catch ex As Exception
                    If ex.Message = "PAID" Then
                        lblMessage.Text = "Paid Bill Records can not be deleted,your option is to remove paid amount from respective Chart of Account and try again."
                        Exit Sub
                    Else
                        Throw ex
                    End If
                End Try

                If objOpp.CopyEmbeddedCost(ddlBizDoc.SelectedValue, lngOppBizDocID, Session("DomainID")) Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "close", "window.close();", True)
                Else
                    lblMessage.Text = "Can not copy, please contact administrator."
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class