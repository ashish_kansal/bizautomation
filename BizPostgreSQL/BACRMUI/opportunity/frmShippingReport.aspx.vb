﻿Imports BACRM.BusinessLogic.Common
Imports System.Data
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Item
Imports System.IO

Partial Public Class frmShippingReport
    Inherits BACRMPage

    Public lngShippingReportId As Long
    Public lngOppID As Long
    Dim lngShippingReportItemId As Long
    Public lngOppBizDocID As Long
    Dim ds As New DataSet()
    Dim ShipLabelImage As String
    Dim objOppBizDocs As OppBizDocs

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngOppID = GetQueryStringVal("OppId")
            lngOppBizDocID = GetQueryStringVal("OppBizDocId")
            lngShippingReportId = GetQueryStringVal("ShipReportId")
            litMessage.Text = ""
            btnAutoGenerateLabels.Visible = If(CCommon.ToBool(ConfigurationManager.AppSettings("IsDebugMode")) = True, True, False)

            If Not Page.IsPostBack Then
                BindShippingReport()
                BindWarehouse()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub BindWarehouse()
        Try
            Dim listItem As Telerik.Web.UI.RadComboBoxItem
            Dim objItem As New CItems
            objItem.DomainID = Session("DomainID")
            Dim dt As DataTable = objItem.GetWareHouses

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                For Each dr As DataRow In dt.Rows
                    listItem = New Telerik.Web.UI.RadComboBoxItem
                    listItem.Text = CCommon.ToString(dr("vcWareHouse"))
                    listItem.Value = CCommon.ToString(dr("numWareHouseID"))
                    listItem.Attributes.Add("PrintNodeAPIKey", CCommon.ToString(dr("vcPrintNodeAPIKey")).Trim())
                    listItem.Attributes.Add("PrintNodePrinterID", CCommon.ToString(dr("vcPrintNodePrinterID")).Trim())
                    radcmbWarehouses.Items.Add(listItem)
                Next
            End If

            listItem = New Telerik.Web.UI.RadComboBoxItem
            listItem.Text = "-- Select One --"
            listItem.Value = "0"

            radcmbWarehouses.Items.Insert(0, listItem)

            If CCommon.ToLong(hdnShipFromWarehouse.Value) > 0 AndAlso Not radcmbWarehouses.Items.FindItemByValue(hdnShipFromWarehouse.Value) Is Nothing Then
                radcmbWarehouses.Items.FindItemByValue(hdnShipFromWarehouse.Value).Selected = True
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub


    Private Sub BindShippingReport()
        Try
            If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
            Dim ds As New DataSet
            objOppBizDocs.DomainID = Session("DomainID")
            objOppBizDocs.OppBizDocId = lngOppBizDocID
            objOppBizDocs.ShippingReportId = lngShippingReportId
            ds = objOppBizDocs.GetShippingReport

            

            If ds.Tables(0).Rows.Count > 0 Then
                Dim regularWeight As Double = 0
                Dim dimentionalWeight As Double = 0

                For Each dr As DataRow In ds.Tables(0).Rows
                    regularWeight += CCommon.ToDouble(dr("fltTotalRegularWeight"))
                Next

                Dim dtBoxes As DataTable = ds.Tables(0).DefaultView.ToTable(True, "vcBoxName", "fltTotalDimensionalWeight")

                If Not dtBoxes Is Nothing AndAlso dtBoxes.Rows.Count > 0 Then
                    For Each dr As DataRow In dtBoxes.Rows
                        dimentionalWeight += CCommon.ToDouble(dr("fltTotalDimensionalWeight"))
                    Next
                End If

                lblAddedDetails.Text = CCommon.ToString(ds.Tables(0).Rows(0)("CreatedDetails"))
                lblRegularWeight.Text = regularWeight.ToString("F2")
                lblDimensionalWeight.Text = dimentionalWeight.ToString("F2")
                hdnShippingCompanyID.Value = CCommon.ToDouble(ds.Tables(0).Rows(0)("numShippingCompany"))
                hdnShippingCompanyName.Value = CCommon.ToString(ds.Tables(0).Rows(0)("vcShipCompany"))
                hdnShipFromWarehouse.Value = CCommon.ToLong(ds.Tables(0).Rows(0)("numShipFromWarehouse"))
                'Lead Tracking URL 
                Dim objItem As New CItems
                Dim dsShippingDtl As DataSet
                objItem.DomainID = Session("DomainID")
                objItem.ShippingCMPID = CCommon.ToLong(ds.Tables(0).Rows(0)("numShippingCompany"))
                dsShippingDtl = objItem.ShippingDtls()
                For Each drow As DataRow In dsShippingDtl.Tables(0).Rows
                    Select Case drow("vcFieldName").ToString()
                        Case "Tracking URL"
                            If (drow("vcShipFieldValue").ToString() = "") Then
                                ViewState("TrackingURL") = drow("vcShipFieldValue").ToString()
                            Else
                                ViewState("TrackingURL") = drow("vcShipFieldValue").ToString() + CCommon.ToString(ds.Tables(0).Rows(0)("vcTrackingNumber"))
                            End If
                    End Select
                Next
            Else
                radBoxes.DataSource = Nothing
                radBoxes.DataBind()
            End If

            If ds.Tables(1).Rows.Count > 0 Then
                lblBizDocName.Text = ds.Tables(1).Rows(0)("vcBizDocID").ToString()
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function TranslateServiceType(ByVal tintServiceType As Integer) As String
        Try
            Dim service As String = ""

            Select Case tintServiceType
                Case 0 : service = "Unspecified"

                Case 10 : service = "Priority Overnight"
                Case 11 : service = "Standard Overnight"
                Case 12 : service = "First Overnight"
                Case 13 : service = "FedEx 2nd Day"
                Case 14 : service = "FedEx Express Saver"
                Case 15 : service = "FedEx Ground"
                Case 16 : service = "FedEx Ground Home Delivery"
                Case 17 : service = "FedEx 1Day Freight"
                Case 18 : service = "FedEx 2Day Freight"
                Case 19 : service = "FedEx 3Day Freight"
                Case 20 : service = "International Priority"
                Case 21 : service = "International Priority Distribution"
                Case 22 : service = "International Economy"
                Case 23 : service = "International Economy Distribution"
                Case 24 : service = "International First"
                Case 25 : service = "International Priority Freight"
                Case 26 : service = "International Economy Freight"
                Case 27 : service = "International Distribution Freight"
                Case 28 : service = "Europe International Priority"

                Case 70 : service = "USPS Express"
                Case 71 : service = "USPS First Class"
                Case 72 : service = "USPS Priority"
                Case 73 : service = "USPS Parcel Post"
                Case 74 : service = "USPS Bound Printed Matter"
                Case 75 : service = "USPS Media"
                Case 76 : service = "USPS Library"

                Case 40 : service = "UPS Next Day Air"
                Case 42 : service = "UPS 2nd Day Air"
                Case 43 : service = "UPS Ground"
                Case 48 : service = "UPS 3Day Select"
                Case 49 : service = "UPS Next Day Air Saver"
                Case 50 : service = "UPS Saver"
                Case 51 : service = "UPS Next Day Air Early A.M."
                Case 55 : service = "UPS 2nd Day Air AM"
            End Select
            Return service
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub radBoxes_ItemCommand(sender As Object, e As Telerik.Web.UI.GridCommandEventArgs) Handles radBoxes.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim objOppBizDocs As New OppBizDocs
                objOppBizDocs.BoxID = CCommon.ToLong(e.CommandArgument)
                objOppBizDocs.DeleteShippingBox()
                'delete shipping label image file
                Dim ds As New DataSet
                objOppBizDocs.tintMode = 0
                ds = objOppBizDocs.GetShippingLabelList
                If ds.Tables(0).Rows.Count > 0 Then
                    If System.IO.File.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & CCommon.ToString(ds.Tables(0).Rows(0)("vcShippingLabelImage"))) Then
                        System.IO.File.Delete(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & CCommon.ToString(ds.Tables(0).Rows(0)("vcShippingLabelImage")))
                    End If
                End If
                BindShippingReport()
            ElseIf e.CommandName = "Download" Then
                    Dim filePath As String = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & "\" & e.CommandArgument
                    If File.Exists(filePath) Then
                        Response.Clear()
                        Response.ContentType = "application/octet-stream"
                        Response.AppendHeader("Content-Disposition", ("attachment; filename=" + Path.GetFileName(filePath)))
                        Response.WriteFile(filePath)
                        Response.End()
                    Else
                        Response.Write("Error:404, Requested file is not found.")
                End If
            ElseIf e.CommandName = "Print" Then
                If CCommon.ToString(e.CommandArgument).Length > 0 Then
                    hdnShippingLabelFile.Value = e.CommandArgument
                    If CCommon.ToLong(radcmbWarehouses.SelectedValue) > 0 Then
                        btnPrintLabels_Click(Nothing, Nothing)
                    Else
                        divWarehouse.Style.Add("display", "")
                    End If
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "NoShippingLabel", "alert('Shipping label is not available');", True)
                End If
            End If
        Catch ex As Threading.ThreadAbortException

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub radBoxes_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles radBoxes.NeedDataSource
        Try
            If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
            objOppBizDocs.ShippingReportId = lngShippingReportId
            ds = objOppBizDocs.GetShippingBoxes()
            ds.Tables(0).TableName = "Box"
            ds.Tables(1).TableName = "BoxItems"

            radBoxes.DataSource = ds

            If ds.Tables(0).Rows.Count > 0 Then
                lblTotalShipCost.Text = String.Format("{0:##,#00.00}", CCommon.ToDecimal(ds.Tables(0).Compute("sum(monShippingRate)", String.Empty)))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnGenerateShippingLabels_Click(sender As Object, e As EventArgs) Handles btnGenerateShippingLabels.Click
        Try
            Response.Redirect("frmGenerateShippingLabel.aspx?mode=1&ShipReportId=" & lngShippingReportId & _
                                                             "&OppId=" & lngOppID.ToString & _
                                                             "&OppBizDocId=" & lngOppBizDocID.ToString & _
                                                             "&ShipCompID=" & CCommon.ToString(hdnShippingCompanyID.Value) & _
                                                             "&ShipCompName=" & CCommon.ToString(hdnShippingCompanyName.Value), False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnAutoGenerateLabels_Click(sender As Object, e As EventArgs) Handles btnAutoGenerateLabels.Click
        Try
            Dim objShipping As New Shipping
            objShipping.GenerateShippingLabel(lngOppID, lngOppBizDocID, lngShippingReportId, Session("DomainID"), Session("UserContactID"))
            If Not String.IsNullOrEmpty(CCommon.ToString(objShipping.ErrorMsg)) Then
                litMessage.Text = CCommon.ToString(objShipping.ErrorMsg)
            Else
                BindShippingReport()
                radBoxes_NeedDataSource(radBoxes, Nothing)
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub DownloadFile(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim filePath As String = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & "Shipping\" & CCommon.ToString(CType(sender, ImageButton).CommandArgument) & ".xml"
            If File.Exists(filePath) Then
                Response.ContentType = ContentType
                Response.AppendHeader("Content-Disposition", ("attachment; filename=" + Path.GetFileName(filePath)))
                Response.WriteFile(filePath)
                Response.End()
            Else
                Response.Write("Error:404, Requested file is not found.")
            End If
            
        Catch ex As Exception
            Throw ex
        End Try

    End Sub


    Private Sub btnPrintLabels_Click(sender As Object, e As EventArgs) Handles btnPrintLabels.Click
        Try
            Dim isLabelSelected As Boolean = False

            For Each GridRow As Telerik.Web.UI.GridDataItem In radBoxes.Items
                Dim chkPrint As CheckBox = CType(GridRow.FindControl("chkSelect"), CheckBox)
                If Not (chkPrint Is Nothing) AndAlso chkPrint.Checked Then
                    isLabelSelected = True
                End If
            Next

            If isLabelSelected Then
                lkbPrint_Click(Nothing, Nothing)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "PrintRequestSend", "alert('Select label(s) to print.');", True)
            End If


        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub lkbPrint_Click(sender As Object, e As EventArgs)
        ' divWarehouse.Style.Add("display", "")
        If (CCommon.ToLong(radcmbWarehouses.SelectedValue) > 0) Then
            For Each GridRow As Telerik.Web.UI.GridDataItem In radBoxes.Items
                Dim hdnShippingLabelImage As HiddenField = CType(GridRow.FindControl("hdnShippingLabelImage"), HiddenField)
                Dim strBoxName As String = GridRow.Cells(3).Text
                Dim chkPrint As CheckBox = CType(GridRow.FindControl("chkSelect"), CheckBox)
                If Not (chkPrint Is Nothing) Then
                    If (chkPrint.Checked) Then
                        If hdnShippingLabelImage.Value <> "" Then
                            PrintLabels(hdnShippingLabelImage.Value)
                        Else
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "NoShippingLabel", "alert('Shipping label is not available for " + strBoxName + "');", True)
                        End If
                    End If
                End If

            Next
        Else
            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "SelectWarehouse", "alert('Select warehouse to print label');", True)
            divWarehouse.Style.Add("display", "")
        End If

    End Sub

    Private Sub PrintLabels(ByVal ShippingLabelFile As String)
        Try

            If String.IsNullOrEmpty(CCommon.ToString(radcmbWarehouses.SelectedItem.Attributes("PrintNodeAPIKey"))) Or String.IsNullOrEmpty(CCommon.ToString(radcmbWarehouses.SelectedItem.Attributes("PrintNodePrinterID"))) Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "PrintNode", "alert('PrintNode is not configured for selected warehouse');", True)
                divWarehouse.Style.Add("display", "")
            Else
                Dim service As New PrintNodeService("https://api.printnode.com/", radcmbWarehouses.SelectedItem.Attributes("PrintNodeAPIKey"))
                service.SubmitPrintJob(CCommon.GetDocumentPath(Session("DomainID")) & ShippingLabelFile, radcmbWarehouses.SelectedItem.Attributes("PrintNodePrinterID"))
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "PrintRequestSend", "alert('Print request is send to PrintNode.');", True)
                divWarehouse.Style.Add("display", "none")
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class