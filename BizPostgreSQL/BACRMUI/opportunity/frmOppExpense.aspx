<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmOppExpense.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmOppExpense" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />

    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>

    <script type="text/javascript" src="../JavaScript/en.js"></script>        
		<title>Billable Expense</title>
    <script language="javascript">
			function Close()
			{
				window.close();
			}
			function Save()
			{
				if (document.Form1.txtAmount.value=="")
				{
					alert("Enter Amount")
					document.Form1.txtAmount.focus();
					return false;
				}
			}
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="100%">
				<tr>
					 
					<td align="right" colSpan="2">
					    <asp:Button ID="btndelete" Runat="server" CssClass="button Delete" Text="X"></asp:Button>
					    <asp:Button ID="btnSave" Runat="server" CssClass="button" Text="Save &amp; Close"></asp:Button>
						<asp:Button ID="btnCancel" Runat="server" CssClass="button" Text="Close"></asp:Button>
						<br>
						<br>
					</td>
				</tr>
				<tr>
				    <td>
				    
				    </td>
				    <td align="left" valign="middle">
				        <asp:RadioButton ID="radBill" runat="server" CssClass="normal1" GroupName="Bill" Text="Billable"  Checked="true" />
				        <asp:RadioButton ID="radNonBill" runat="server" CssClass="normal1" GroupName="Bill" Text="Non-Billable" />
				    </td>
				  
				</tr>
				<tr>
					<td class="normal1" align="right">Amount
					</td>
					<td class="normal1"><asp:textbox id="txtAmount" Runat="server" CssClass=signup></asp:textbox>
					 <asp:CheckBox runat="server" Text="Reimbursable" ID="chkReimb" CssClass="signup" />
					</td>
					
				</tr>
				<tr>
		             <td class="normal1" align="right">
                               Created Date 
                     </td>
		            <td>		               
                              <BizCalendar:Calendar ID="CalCreated" runat="server" />                       
		            </td>
		        </tr>
				    <tr>
					    <td class="normal1" align="right">Contract
					    </td>
					    <td class="normal1"><asp:DropDownList ID="ddlContract" AutoPostBack="true" Width="180" runat="server"  CssClass="signup"></asp:DropDownList>&nbsp;(Apply  Expenses to Contract)</td>
				    </tr>
				    <tr>
				      <td align="right" class="normal1">
				        Contract Amount Balance : 
				     </td>
				      <td >
				          <asp:Label ID="lblRemAmount" runat="server" CssClass="normal1"></asp:Label>
				      </td>
				    </tr>
			
				<tr>
					<td class="normal1" align="right">Description
					</td>
					<td><asp:TextBox TextMode="MultiLine" Runat="server" Width="400" Height="50" id="txtDesc" CssClass=signup></asp:TextBox></td>
				</tr>
				 <tr>
				        <td align="center" colspan="2" class="normal4">
				          <asp:Literal runat="server" id="litMessage" EnableViewState="False"  ></asp:Literal> 				            
				        </td>
				    </tr>
			</table>
			<asp:textbox id="txtCategoryHDRID" Runat="server" Text="0" style="display:none" ></asp:textbox>
				<asp:textbox id="txtOppId" Runat="server" Text="0" style="display:none" ></asp:textbox>
		    <asp:textbox id="txtStageId" Runat="server" Text="0" style="display:none" ></asp:textbox>
		    <asp:textbox id="txtDivId" Runat="server" Text="0" style="display:none" ></asp:textbox>
		</form>
	</body>
</HTML>
