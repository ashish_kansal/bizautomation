<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmUnitdtlsForItem.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmUnitdtlsForItem"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"    runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Unit dtls For Item</title>
	
	</HEAD>
	<body >
		<form id="Form1" method="post" runat="server">
		<table width="100%"><td>
			<asp:datagrid id="dgUnits" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
				BorderColor="white">
				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
				<Columns>
					<asp:BoundColumn DataField="numOnHand" DataFormatString="{0:#,##0}" HeaderText="On Hand"></asp:BoundColumn>
					<asp:BoundColumn DataField="numOnOrder" DataFormatString="{0:#,##0}" HeaderText="On Order"></asp:BoundColumn>
					<asp:BoundColumn DataField="numAllocation" DataFormatString="{0:#,##0}"  HeaderText="Allocation"></asp:BoundColumn>
					<asp:BoundColumn DataField="numBackOrder" DataFormatString="{0:#,##0}" HeaderText="On Backorder"></asp:BoundColumn>
				</Columns>
			</asp:datagrid>
			</td></table>
		</form>
	</body>
</HTML>
