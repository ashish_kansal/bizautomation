﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports System.Reflection
Imports BACRM.BusinessLogic.Contacts
Imports System.IO
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Admin
Imports System.Text.RegularExpressions
Imports System.Text
Imports BACRM.BusinessLogic.ShioppingCart
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Workflow

Namespace BACRM.UserInterface.Opportunities
    Public Class frmSalesFulfillmentOrder
        Inherits BACRMPage

        Private objOpportunity As New MOpportunity
        Private objOppBizDocs As New OppBizDocs
        Private objShipping As New Shipping
        Private RegularSearch As String
        Private CustomSearch As String
        Private dtDynamicGridColumns As DataTable

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'HIDE ERROR DIV
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                'HIDE ALERT DIV
                divAlert.Visible = False
                lblException.Text = ""

                If Not IsPostBack Then
                    BindWarehouse()
                    hdnFromSalesOrderID.Value = CCommon.ToLong(GetQueryStringVal("fromOppID"))
                    objCommon.sb_FillComboFromDBwithSel(radShippingZone, 834, Session("DomainID"))
                    radDtShipped.SelectedDate = Date.Today.Date

                    radOrderStatus.DataSource = objCommon.GetMasterListItems(176, Session("DomainID"), 1, 2)
                    radOrderStatus.DataTextField = "vcData"
                    radOrderStatus.DataValueField = "numListItemID"
                    radOrderStatus.DataBind()
                    radOrderStatus.Items.Insert(0, New RadComboBoxItem("--None--", "0"))

                    BindOrderStatus()

                    If CCommon.ToLong(hdnFromSalesOrderID.Value) = 0 Then
                        PersistTable.Load()
                        If PersistTable.Count > 0 Then
                            txtSortChar.Text = PersistTable(PersistKey.SortCharacter)
                            txtSortColumn.Text = PersistTable(PersistKey.SortColumnName)
                            txtSortOrder.Text = PersistTable(PersistKey.SortOrder)
                            txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                            chkExpandGrid.Checked = CCommon.ToBool(PersistTable(chkExpandGrid.ID))
                            txtGridColumnFilter.Text = CCommon.ToString(PersistTable(PersistKey.GridColumnSearch))

                            'Order Status
                            Dim strOrderStatusValue() As String = CCommon.ToString(PersistTable(radOrderStatus.ID)).Split(",")
                            If strOrderStatusValue.Length > 0 Then
                                For i As Integer = 0 To strOrderStatusValue.Length - 1
                                    If radOrderStatus.Items.FindItemByValue(strOrderStatusValue(i)) IsNot Nothing Then
                                        radOrderStatus.Items.FindItemByValue(strOrderStatusValue(i)).Checked = True
                                    End If
                                Next
                            End If

                            If CCommon.ToLong(PersistTable(ddlFilterBy.ID)) > 0 Then
                                If Not ddlFilterBy.Items.FindByValue(CCommon.ToLong(PersistTable(ddlFilterBy.ID))) Is Nothing Then
                                    ddlFilterBy.Items.FindByValue(CCommon.ToLong(PersistTable(ddlFilterBy.ID))).Selected = True

                                    If CCommon.ToLong(PersistTable(ddlFilterBy.ID)) > 0 Then
                                        LoadFilterValueComboBox(CCommon.ToLong(PersistTable(ddlFilterBy.ID)))

                                        Dim strFiltersValue() As String = CCommon.ToString(PersistTable(radFilterValue.ID)).Split(",")
                                        If strFiltersValue.Length > 0 Then
                                            For i As Integer = 0 To strFiltersValue.Length - 1
                                                If radFilterValue.Items.FindItemByValue(strFiltersValue(i)) IsNot Nothing Then
                                                    radFilterValue.Items.FindItemByValue(strFiltersValue(i)).Checked = True
                                                End If
                                            Next
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    End If

                    BindDatagrid()
                    chkExpandGrid_CheckedChanged(Nothing, Nothing)
                End If

                If IsPostBack Then
                    BindDatagrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub LoadFilterValueComboBox(ByVal filterBy As Integer)
            Try
                radFilterValue.Items.Clear()

                If filterBy = 1 Then 'Order Status
                    objCommon.sb_FillComboFromDBwithSel(radFilterValue, 36, Session("DomainID"))
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub BindWarehouse()
            Try
                Dim listItem As Telerik.Web.UI.RadComboBoxItem
                Dim objItem As New CItems
                objItem.DomainID = Session("DomainID")
                Dim dt As DataTable = objItem.GetWareHouses

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    For Each dr As DataRow In dt.Rows
                        listItem = New Telerik.Web.UI.RadComboBoxItem
                        listItem.Text = CCommon.ToString(dr("vcWareHouse"))
                        listItem.Value = CCommon.ToString(dr("numWareHouseID"))
                        listItem.Attributes.Add("PrintNodeAPIKey", CCommon.ToString(dr("vcPrintNodeAPIKey")).Trim())
                        listItem.Attributes.Add("PrintNodePrinterID", CCommon.ToString(dr("vcPrintNodePrinterID")).Trim())
                        radcmbWarehouses.Items.Add(listItem)
                    Next
                End If

                listItem = New Telerik.Web.UI.RadComboBoxItem
                listItem.Text = "-- Select One --"
                listItem.Value = "0"

                radcmbWarehouses.Items.Insert(0, listItem)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub BindOrderStatus()
            Try
                'BizDoc Status
                Dim dtdata As New DataTable
                Dim dtdataCopy As New DataTable
                dtdata = objCommon.GetMasterListItems(176, Session("DomainID"), 1, 2)


                Dim objSalesFulfillmentConfiguration As New SalesFulfillmentConfiguration
                objSalesFulfillmentConfiguration.DomainID = CCommon.ToLong(Session("DomainID"))
                Dim dt As DataTable = objSalesFulfillmentConfiguration.GetByDomainID()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 AndAlso CCommon.ToBool(dt.Rows(0)("bitActive")) Then
                    For Each dr As DataRow In dtdata.Rows
                        If dr("numListItemID") = dt.Rows(0)("numRule2OrderStatus") Or _
                           dr("numListItemID") = dt.Rows(0)("numRule3OrderStatus") Or _
                           dr("numListItemID") = dt.Rows(0)("numRule4OrderStatus") Or _
                           (CCommon.ToBool(dt.Rows(0)("bitActive")) AndAlso CCommon.ToBool(dt.Rows(0)("bitRule1IsActive")) AndAlso CCommon.ToShort(dt.Rows(0)("tintRule1Type")) = 2 AndAlso dr("numListItemID") = dt.Rows(0)("numRule1OrderStatus")) Then
                            ddlBatchAction.Items.Add(New ListItem(dr("vcData"), "OrderStatus" & dr("numListItemID")))
                        End If
                    Next
                Else
                    For Each dr As DataRow In dtdata.Rows
                        ddlBatchAction.Items.Add(New ListItem(dr("vcData"), "OrderStatus" & dr("numListItemID")))
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub BindDatagrid(Optional ByVal CreateCol As Boolean = True, Optional ByVal isFromClearFilter As Boolean = False)
            Try
                Dim dtBizDocs As DataTable
                Dim objOpenBizDocs As New OpenBizDocs
                Dim stFilterValue As String = ""
                Dim strOrderStatusValue As String = ""
                Dim strShippingService As String = ""

                With objOpenBizDocs
                    .DomainID = Session("DomainID")

                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()
                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0
                    .UserCntID = Session("UserContactID")

                    If Not isFromClearFilter Then
                        If radOrderStatus.CheckedItems.Count > 0 Then
                            For Each item As RadComboBoxItem In radOrderStatus.CheckedItems
                                strOrderStatusValue = strOrderStatusValue & item.Value & ","
                            Next

                            .strOrderStatus = " AND COALESCE(Opp.numStatus,0) IN (" & strOrderStatusValue.TrimEnd(",") & ")"
                        End If

                        If CCommon.ToLong(ddlFilterBy.SelectedValue) > 0 Then
                            For Each item As RadComboBoxItem In radFilterValue.CheckedItems
                                stFilterValue = stFilterValue & item.Value & ","
                            Next

                            If Not String.IsNullOrEmpty(stFilterValue) Then
                                If ddlFilterBy.SelectedValue = "1" Then
                                    .strOrderStatus = .strOrderStatus & " AND (SELECT COUNT(*) FROM OpportunityItems OIInner INNER JOIN Item IInner ON OIInner.numItemCode = IInner.numItemCode WHERE OIInner.numOppID=Opp.numOppID AND COALESCE(IInner.numItemClassification,0) IN (" & stFilterValue.TrimEnd(",") & ")) > 0"
                                End If
                            End If
                        End If
                    End If

                    .vcShippingService = strShippingService
                    .numShippingZone = CCommon.ToLong(radShippingZone.SelectedValue)
                    .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    .OppID = CCommon.ToLong(hdnFromSalesOrderID.Value)
                    GridColumnSearchCriteria()
                    .RegularSearchCriteria = RegularSearch
                    .CustomSearchCriteria = CustomSearch
                End With

                If txtSortColumn.Text <> "" Then
                    objOpenBizDocs.SortCol = txtSortColumn.Text

                    If txtSortOrder.Text = "D" Then
                        objOpenBizDocs.SortDirection = "Desc"
                    Else
                        objOpenBizDocs.SortDirection = "Asc"
                    End If
                Else
                    objOpenBizDocs.SortCol = "OBD.dtCreatedDate"
                    objOpenBizDocs.SortDirection = "Desc"
                End If

                Dim ds As DataSet = objOpenBizDocs.GetSalesFulfillmentOrder(False)
                Dim dtTableInfo As DataTable = ds.Tables(2)
                dtDynamicGridColumns = dtTableInfo
                dtDynamicGridColumns.Columns.Add("ColumnUniqueName", GetType(System.String), "'23~'+numFieldId+'~'+bitCustomField")

                Dim htGridColumnSearch As New Hashtable

                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")
                    Dim strIDValue() As String

                    For i = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")

                        htGridColumnSearch.Add(strIDValue(0), strIDValue(1))
                    Next
                End If

                If CreateCol = True Then
                    Dim bField As GridBoundColumn
                    Dim Tfield As GridTemplateColumn
                    gvSearch.Columns.Clear()

                    For Each drRow As DataRow In dtTableInfo.Rows
                        Tfield = New GridTemplateColumn
                        Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, drRow, 3, htGridColumnSearch, 23, objOpenBizDocs.SortCol, objOpenBizDocs.SortDirection, isTelerik:=True)
                        Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, drRow, 3, htGridColumnSearch, 23, objOpenBizDocs.SortCol, objOpenBizDocs.SortDirection, isTelerik:=True)
                        Tfield.UniqueName = "23~" & drRow("numFieldId") & "~" & drRow("bitCustomField")
                        gvSearch.Columns.Add(Tfield)
                    Next

                    Dim dr As DataRow
                    dr = dtTableInfo.NewRow()
                    dr("vcAssociatedControlType") = "SalesFulfillmentLog"
                    dr("intColumnWidth") = "30"

                    Tfield = New GridTemplateColumn
                    Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, dr, 3, htGridColumnSearch, 23, isTelerik:=True)
                    Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, dr, 3, htGridColumnSearch, 23, isTelerik:=True)
                    Tfield.UniqueName = "SalesFulfillmentLog"
                    gvSearch.Columns.Add(Tfield)

                    dr = dtTableInfo.NewRow()
                    dr("vcAssociatedControlType") = "DeleteCheckBox"
                    dr("intColumnWidth") = "30"

                    Tfield = New GridTemplateColumn
                    Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, dr, 3, htGridColumnSearch, 23, isTelerik:=True)
                    Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, dr, 3, htGridColumnSearch, 23, isTelerik:=True)
                    Tfield.UniqueName = "DeleteCheckBox"
                    gvSearch.Columns.Add(Tfield)
                End If

                Session("SFBizDocsItems") = ds.Tables(1)

                dtBizDocs = ds.Tables(0)
                gvSearch.DataSource = dtBizDocs
                gvSearch.DataBind()

                bizPager.PageSize = Session("PagingRows")
                bizPager.RecordCount = objOpenBizDocs.TotalRecords
                bizPager.CurrentPageIndex = txtCurrrentPage.Text

                PersistTable.Clear()
                PersistTable.Add(PersistKey.CurrentPage, IIf(dtBizDocs.Rows.Count > 0, txtCurrrentPage.Text, "1"))
                PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim)
                PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text)
                PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text)
                PersistTable.Add(PersistKey.GridColumnSearch, txtGridColumnFilter.Text)
                PersistTable.Add(chkExpandGrid.ID, chkExpandGrid.Checked)
                PersistTable.Add(radShippingZone.ID, radShippingZone.SelectedValue)
                PersistTable.Add(radOrderStatus.ID, strOrderStatusValue)
                PersistTable.Add(ddlFilterBy.ID, ddlFilterBy.SelectedValue)
                PersistTable.Add(radFilterValue.ID, stFilterValue)

                If CCommon.ToLong(hdnFromSalesOrderID.Value) = 0 Then
                    PersistTable.Save()
                End If

                chkExpandGrid_CheckedChanged(Nothing, Nothing)

                Dim objPageControls As New PageControls
                Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtTableInfo)
                ClientScript.RegisterClientScriptBlock(Me.GetType, "InlineEditValidation", strValidation, True)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub gvSearch_DetailTableDataBind(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridDetailTableDataBindEventArgs) Handles gvSearch.DetailTableDataBind
            Try
                Dim parentItem As GridDataItem = CType(e.DetailTableView.ParentItem, GridDataItem)

                'Dim ds As New DataSet
                Dim dtOppBiDocItems As DataTable
                dtOppBiDocItems = Session("SFBizDocsItems")

                Dim dvConfig As DataView
                dvConfig = New DataView(dtOppBiDocItems)
                dvConfig.RowFilter = String.Format("numOppId={0}", Convert.ToInt32(parentItem.GetDataKeyValue("numOppID").ToString()))

                e.DetailTableView.DataSource = dvConfig
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub gvSearch_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles gvSearch.ItemCommand
            Try
                If e.CommandName = "OpenLog" Then
                    GetBoxLog(e.CommandArgument)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub gvSearch_ItemDataBound(sender As Object, e As GridItemEventArgs) Handles gvSearch.ItemDataBound
            Try
                If TypeOf e.Item Is GridDataItem Then
                    If e.Item.OwnerTableView Is gvSearch.MasterTableView Then
                        Dim txtoppID As HiddenField = DirectCast(e.Item.FindControl("txtoppID"), HiddenField)
                        If Not txtoppID Is Nothing Then
                            txtoppID.Value = CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numOppId"))
                        End If

                        Dim imgShippingReport As ImageButton = CType(e.Item.FindControl("imgShippingReport"), ImageButton)
                        If Not imgShippingReport Is Nothing Then
                            imgShippingReport.CommandArgument = CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numOppId"))

                            If CCommon.ToBool(DataBinder.Eval(e.Item.DataItem, "IsShippingServiceItemID")) = True Then
                                imgShippingReport.Visible = True
                                imgShippingReport.Attributes.Add("onclick", "return ConfirmShippingItem();")
                            Else
                                imgShippingReport.Visible = False
                            End If
                        End If

                        Dim chkSelect As CheckBox = CType(e.Item.FindControl("chkSelect"), CheckBox)
                        If CCommon.ToBool(CCommon.ToInteger(DataBinder.Eval(e.Item.DataItem, "bitPendingExecution"))) = True Then
                            chkSelect.Visible = False
                        End If

                        If Not chkSelect Is Nothing AndAlso chkSelect.Visible AndAlso CCommon.ToLong(hdnFromSalesOrderID.Value) > 0 Then
                            chkSelect.Checked = True
                        End If

                        'Dim litShipping As Literal = CType(e.Item.FindControl("litShipping"), Literal)
                        'If Not litShipping Is Nothing AndAlso Not String.IsNullOrEmpty(CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "vcShippingBox"))) Then
                        '    Dim arrShippingReports As String() = CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "vcShippingBox")).Split(",")
                        '    If arrShippingReports.Count > 0 AndAlso Not litShipping Is Nothing Then

                        '        For Each Str As String In arrShippingReports
                        '            Dim arrReport As String()
                        '            arrReport = Str.Split("~")
                        '            If arrReport.Count > 0 Then

                        '                If Convert.ToInt32(arrReport(3)) > 0 Then
                        '                    litShipping.Text = litShipping.Text & "&nbsp;<a style='text-decoration: none' href='javascript:void(0);' onclick='openShippingLabel(" & arrReport(0) & "," & arrReport(1) & "," & arrReport(2) & "," & 0 & ");'><img src='../images/box_generated_22x22.png'></a>"
                        '                Else
                        '                    litShipping.Text = litShipping.Text & "&nbsp;<a style='text-decoration: none' href='javascript:void(0);' onclick='openShippingLabel(" & arrReport(0) & "," & arrReport(1) & "," & arrReport(2) & "," & CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numShipVia")) & ");'><img src='../images/box_not_generated_22x22.png'></a>"
                        '                End If

                        '                litShipping.Text = litShipping.Text + "&nbsp;<a style='text-decoration: none' href='javascript:void(0);' onclick='openReportList(" & arrReport(0) & "," & arrReport(1) & "," & arrReport(2) & ");'>" & If(CCommon.ToInteger(arrReport(3)) = 1, "<img src='../images/barcode_generated_22x22.png'>", "<img src='../images/barcode_not_generated_22x22.png'>") & "</a>"
                        '            End If
                        '        Next
                        '    End If
                        'End If
                    Else
                        If Not String.IsNullOrEmpty(CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "vcImage"))) Then
                            DirectCast(e.Item.FindControl("imgThumb"), System.Web.UI.WebControls.Image).ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "vcImage"))
                        End If

                        If (DataBinder.Eval(e.Item.DataItem, "bitSerialized") = True Or DataBinder.Eval(e.Item.DataItem, "bitLotNo") = True) Then
                            e.Item.FindControl("hlSerialized").Visible = True
                            e.Item.FindControl("txLot").Visible = True
                            e.Item.FindControl("lblSelectedLot").Visible = True
                            e.Item.FindControl("imgClearSerial").Visible = True
                            CType(e.Item.FindControl("hlSerialized"), HyperLink).Attributes.Add("onclick", "return OpenConfSerItem('" & DataBinder.Eval(e.Item.DataItem, "numOppId") & "','" & DataBinder.Eval(e.Item.DataItem, "numoppitemtCode") & "','1','0' ,'" & DataBinder.Eval(e.Item.DataItem, "numUnitHourOrig") & "','" & e.Item.FindControl("txLot").ClientID & "')")
                        End If
                    End If
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub gvSearch_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles gvSearch.NeedDataSource
            Try
                BindDatagrid()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub gvSearch_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles gvSearch.PreRender
            Try
                HideExpandColumnRecursive(gvSearch.MasterTableView)

                If Not dtDynamicGridColumns Is Nothing Then
                    For Each column As GridColumn In gvSearch.MasterTableView.Columns
                        If dtDynamicGridColumns.Select("ColumnUniqueName='" & column.UniqueName & "'").Length > 0 Then
                            If CCommon.ToLong(dtDynamicGridColumns.Select("ColumnUniqueName='" & column.UniqueName & "'")(0)("intColumnWidth")) > 0 Then
                                column.HeaderStyle.Width = Unit.Pixel(CCommon.ToLong(dtDynamicGridColumns.Select("ColumnUniqueName='" & column.UniqueName & "'")(0)("intColumnWidth")))
                            End If
                        ElseIf column.UniqueName = "SalesFulfillmentLog" Or column.UniqueName = "DeleteCheckBox" Then
                            column.HeaderStyle.Width = Unit.Pixel(30)
                        End If
                    Next
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Public Sub HideExpandColumnRecursive(ByVal tableView As GridTableView)
            Dim nestedViewItems As GridItem() = tableView.GetItems(GridItemType.NestedView)
            For Each nestedViewItem As GridNestedViewItem In nestedViewItems
                For Each nestedView As GridTableView In nestedViewItem.NestedTableViews
                    If nestedView.HasDetailTables Then
                        HideExpandColumnRecursive(nestedView)
                    End If
                Next
            Next
        End Sub

        'Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
        '    Try
        '        BindDatagrid(True)
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(CCommon.ToString(ex))
        '    End Try
        'End Sub

        Private Sub btnGoBatch_Click(sender As Object, e As EventArgs) Handles btnGoBatch.Click
            Try
                'If not browser refresh
                If CCommon.ToString(Session("BizBrowserRefresh")) = CCommon.ToString(ViewState("BizBrowserRefresh")) Then
                    Session("BizBrowserRefresh") = Guid.NewGuid.ToString()

                    If ddlBatchAction.SelectedValue = "1" Then 'picklist
                        PrintPickList()
                    ElseIf ddlBatchAction.SelectedValue = "2" Then 'packing slip
                        PrintPackingSlip()
                    ElseIf ddlBatchAction.SelectedValue = "3" Then 'PrintShippingLabels
                        divWarehouse.Style.Add("display", "")
                        'PrintShippingLabels()
                    ElseIf ddlBatchAction.SelectedValue = "5" Then 'Group List
                        PrintGroupList()
                    ElseIf ddlBatchAction.SelectedValue = "6" Then 'Ship Orders (Add FOs)
                        AddFulfillmentOrder()
                    ElseIf ddlBatchAction.SelectedValue.StartsWith("OrderStatus") Then
                        Dim orderCount As Integer = 0
                        Dim objOpp As New MOpportunity
                        objOpp.DomainID = Context.Session("DomainID")
                        objOpp.UserCntID = Context.Session("UserContactID")

                        Dim objWfA As New Workflow()
                        objWfA.DomainID = Context.Session("DomainID")
                        objWfA.UserCntID = Context.Session("UserContactID")

                        For Each dataItem As GridDataItem In gvSearch.MasterTableView.Items
                            Dim chk As CheckBox = DirectCast(dataItem.FindControl("chkSelect"), CheckBox)

                            If Not chk Is Nothing AndAlso chk.Checked Then
                                Dim txtOppID As HiddenField = DirectCast(dataItem.FindControl("txtOppID"), HiddenField)

                                If Not txtOppID Is Nothing Then

                                    objOpp.OpportunityId = CCommon.ToLong(txtOppID.Value)
                                    objOpp.OrderStatus = CCommon.ToLong(ddlBatchAction.SelectedValue.Replace("OrderStatus", ""))
                                    objOpp.UpdateOrderStatus()

                                    ''Added By Sachin Sadhu||Date:5thMay2014
                                    ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                                    ''          Using Change tracking
                                    objWfA.RecordID = CCommon.ToLong(txtOppID.Value)
                                    objWfA.SaveWFOrderQueue()
                                    'end of code
                                End If

                                orderCount += 1
                            End If
                        Next

                        If orderCount > 0 Then
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "OrderStatusChange", "alert('Orders Status updated successfully!');", True)
                        Else
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "OrderStatusChange", "alert('Select atleast one record');", True)
                        End If

                    End If

                    BindDatagrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Public Sub PrintShippingLabels()
            Try
                If (txtSelOppId.Text <> "") Then
                    Dim objOppBizDocs As New OppBizDocs()
                    Dim dsShippingLabels As New DataSet

                    objOppBizDocs.BoxID = 0
                    objOppBizDocs.tintMode = 3
                    objOppBizDocs.strOrderID = txtSelOppId.Text.Trim(",")
                    objOppBizDocs.DomainID = Session("DomainID")
                    dsShippingLabels = objOppBizDocs.GetShippingLabelList()

                    Dim vcExcludedOrders As String = ""
                    Dim zplError As String = ""

                    If dsShippingLabels IsNot Nothing AndAlso dsShippingLabels.Tables.Count > 0 AndAlso dsShippingLabels.Tables(0).Rows.Count > 0 Then
                        For Each dr As DataRow In dsShippingLabels.Tables(0).Rows
                            If String.IsNullOrEmpty(dr("vcShippingLabelImage")) Then
                                vcExcludedOrders = vcExcludedOrders & dr("vcPOppName") & "<br />"
                                dsShippingLabels.Tables(0).Rows.Remove(dr)
                            End If
                        Next
                        dsShippingLabels.AcceptChanges()

                        Session("ShippingLabelImages") = dsShippingLabels


                        Dim dtTemp As DataTable = dsShippingLabels.Tables(0).DefaultView.ToTable(True, "numOppID")

                        If Not dtTemp Is Nothing AndAlso dtTemp.Rows.Count > 0 Then
                            For Each drTemp As DataRow In dtTemp.Rows
                                Dim objSalesFulfillmentQueue As New SalesFulfillmentQueue
                                objSalesFulfillmentQueue.DomainID = CCommon.ToLong(Session("DomainID"))
                                objSalesFulfillmentQueue.SFQID = CCommon.ToLong(drTemp("numOppID"))
                                objSalesFulfillmentQueue.Message = "Shipping Label(s) Printed."
                                objSalesFulfillmentQueue.UpdateStatus(5, True)
                            Next
                        End If


                        Dim dtShip As New DataTable
                        dtShip = dsShippingLabels.Tables(0).DefaultView.ToTable(True, "numShippingCompany")

                        For Each drRow As DataRow In dtShip.Rows
                            Dim strService As String
                            If CCommon.ToLong(drRow("numShippingCompany")) = 91 Then
                                strService = "Fedex"
                            ElseIf CCommon.ToLong(drRow("numShippingCompany")) = 88 Then
                                strService = "UPS"
                            ElseIf CCommon.ToLong(drRow("numShippingCompany")) = 90 Then
                                strService = "USPS"
                            End If

                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Printing Shipping Labels :" & strService, "OpenPrintedShippingLabels(" & CCommon.ToLong(drRow("numShippingCompany")) & ");", True)
                        Next
                    End If

                    'ZPL Labels
                    If dsShippingLabels IsNot Nothing AndAlso dsShippingLabels.Tables.Count > 1 AndAlso dsShippingLabels.Tables(1).Rows.Count > 0 Then
                        Dim objUserAccess As New UserAccess
                        objUserAccess.DomainID = CCommon.ToLong(Session("DomainID"))
                        Dim dt As DataTable = objUserAccess.GetDomainPrinterDetail()

                        If Not dt Is Nothing AndAlso dt.Rows.Count > 0 AndAlso Not String.IsNullOrEmpty(dt.Rows(0)("vcPrinterIPAddress")) AndAlso Not String.IsNullOrEmpty(dt.Rows(0)("vcPrinterPort")) Then
                            For Each dr As DataRow In dsShippingLabels.Tables(1).Rows
                                If String.IsNullOrEmpty(dr("vcShippingLabelImage")) Then
                                    vcExcludedOrders = vcExcludedOrders & dr("vcPOppName") & "<br />"
                                Else
                                    Dim sb As New StringBuilder()
                                    Dim filePath As String = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & "\" & dr("vcShippingLabelImage")
                                    Using sr As New StreamReader(filePath, Encoding.Default)
                                        While Not sr.EndOfStream
                                            sb.AppendLine(sr.ReadLine())
                                        End While
                                    End Using

                                    Try
                                        Dim client As New System.Net.Sockets.TcpClient()
                                        client.Connect(CCommon.ToString(dt.Rows(0)("vcPrinterIPAddress")), CCommon.ToInteger(dt.Rows(0)("vcPrinterPort")))
                                        Dim writer As New System.IO.StreamWriter(client.GetStream())
                                        writer.Write(sb.ToString())
                                        writer.Flush()

                                        writer.Close()
                                        client.Close()

                                        Dim objSalesFulfillmentQueue As New SalesFulfillmentQueue
                                        objSalesFulfillmentQueue.DomainID = CCommon.ToLong(Session("DomainID"))
                                        objSalesFulfillmentQueue.SFQID = CCommon.ToLong(dr("numOppID"))
                                        objSalesFulfillmentQueue.Message = "Shipping Label(s) Printed."
                                        objSalesFulfillmentQueue.UpdateStatus(5, True)
                                    Catch ex As Exception
                                        If zplError.Length > 0 Then
                                            zplError = zplError & "Error occured while printing shipping label for order " & dr("vcPOppName") & ":" & ex.Message & "<br/>"
                                        Else
                                            zplError = "Error occured while printing shipping label for order " & dr("vcPOppName") & ":" & ex.Message & "<br/>"
                                        End If
                                    End Try
                                End If
                            Next
                        Else
                            If vcExcludedOrders.Length > 0 Then
                                vcExcludedOrders = "Shipping Labels are not available for following orders: <br/>" & vcExcludedOrders & "<br /><br />" & "Printer information is required to print zpl labels. Go to Administration -> Global Settings -> Shipping Settings."
                            Else
                                vcExcludedOrders = "Printer information is required to print zpl labels. Go to Administration -> Global Settings -> Shipping Settings"
                            End If
                        End If
                    End If

                    If vcExcludedOrders.Length > 0 AndAlso zplError.Length > 0 Then
                        DisplayAlert(vcExcludedOrders & "<br/><br/>" & zplError)
                    ElseIf vcExcludedOrders.Length > 0 Then
                        DisplayAlert(vcExcludedOrders)
                    ElseIf zplError.Length > 0 Then
                        DisplayAlert(zplError)
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub PrintPickList()
            'Try
            If (txtSelBizDocsId.Text <> "") Then

                Dim objOpp As New COpportunities
                Dim ds As DataSet
                objOpp.DomainID = Session("DomainID")
                objOpp.strBizDocsIds = txtSelBizDocsId.Text.Trim(",")
                objOpp.byteMode = 1

                ds = objOpp.GetPackingDetail()

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim objPDF As New HTMLToPDF
                    objPDF.CreatePickListPDF(ds.Tables(0))
                End If
            End If
        End Sub
        Private Sub PrintGroupList()
            'Try
            If (txtSelBizDocsId.Text <> "") Then

                Dim objOpp As New COpportunities
                Dim ds As DataSet
                objOpp.DomainID = Session("DomainID")
                objOpp.strBizDocsIds = txtSelBizDocsId.Text.Trim(",")
                objOpp.byteMode = 2

                ds = objOpp.GetPackingDetail()

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim objPDF As New HTMLToPDF
                    objPDF.CreateGroupListPDF(ds.Tables(0))
                End If
            End If
            'Catch ex As Exception
            '    Throw ex
            'End Try
        End Sub
        Private Sub PrintPackingSlip()
            Try
                If (txtSelBizDocsId.Text <> "") Then
                    Dim objOpp As New COpportunities
                    Dim ds As DataSet
                    objOpp.DomainID = Session("DomainID")
                    objOpp.BizDocId = enmBizDocTemplate_BizDocID.Packing_Slip_Template
                    objOpp.strBizDocsIds = txtSelBizDocsId.Text.Trim(",")
                    objOpp.byteMode = 2

                    ds = objOpp.GetSFItemsforImport

                    Dim dt As DataTable = ds.Tables(0)
                    'Dim strArr() As String = txtSelOppAndValues.Text.Split(",")
                    'Dim ht As New Hashtable
                    'For i As Integer = 0 To strArr.Length - 1
                    '    If strArr(i).Length > 0 Then
                    '        If ht.ContainsKey(strArr(i).Split("~")(0)) Then
                    '            ht(strArr(i).Split("~")(0)) = strArr(i).Split("~")(1)
                    '        Else
                    '            ht.Add(strArr(i).Split("~")(0), strArr(i).Split("~")(1))
                    '        End If
                    '    End If
                    'Next

                    'For Each dr As DataRow In dt.Rows
                    '    dr("numUnitHour") = IIf(ht(dr("numoppitemtCode").ToString) Is Nothing, 0, ht(dr("numoppitemtCode").ToString))
                    'Next
                    'dt.AcceptChanges()
                    If dt.Rows.Count > 0 Then
                        Dim objPDF As New HTMLToPDF

                        Dim intResult As Integer = objPDF.CreatePackingSlipExportPdf(dt)
                        If intResult = 101 Then
                            DisplayAlert("Please enable ""Packing Slip"" BizDoc from ""Administration->Global Settings->Order Management(subtab)->BizDoc Template"", as well Set One template as default template.")
                        ElseIf intResult = 102 Then
                            DisplayAlert("Please configure Packing Slip line items from ""Administration->BizForm Wizard->BizDoc(Sales)"".")
                        ElseIf intResult = 103 Then
                            DisplayAlert("Please contact support by creating bug or feedback with following error : ""Invalid page break in template.""")
                        End If

                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "RefereshPage", "document.location.href=document.location.href;", True)
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub AddFulfillmentOrder()
            Try
                If radDtShipped.SelectedDate Is Nothing Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "NoRecordSelected", "alert('Shipped Date is required.')", True)
                    Exit Sub
                End If

                Dim selectedRecordCount As Integer = 0
                Dim errorMessageSerialLot As String = ""
                Dim errorMessageWorkorder As String = ""
                Dim errorMessageInSufficientInventory As String = ""
                Dim listFulFillmentOrder As New System.Collections.Generic.List(Of FulFillmentOrder)

                For Each item As GridDataItem In gvSearch.MasterTableView.Items
                    Dim chkSelect As CheckBox = DirectCast(item.FindControl("chkSelect"), CheckBox)

                    If chkSelect.Checked AndAlso chkSelect.Visible Then
                        selectedRecordCount = selectedRecordCount + 1

                        Dim objFulFillmentOrder As New FulFillmentOrder
                        objFulFillmentOrder.DivisionID = CCommon.ToLong(item.GetDataKeyValue("numDivisionID"))
                        objFulFillmentOrder.OppID = CCommon.ToLong(item.GetDataKeyValue("numOppID"))
                        objFulFillmentOrder.OppName = CCommon.ToString(item.GetDataKeyValue("vcpOppName"))

                        Dim objOpportunity As New OppotunitiesIP
                        objOpportunity.OpportunityId = CCommon.ToLong(item.GetDataKeyValue("numOppID"))

                        Dim dtTable As DataTable
                        dtTable = objOpportunity.ValidateOppSerializLot()

                        If dtTable.Rows.Count > 0 Then
                            errorMessageSerialLot += objFulFillmentOrder.OppName & "<br/>"
                            Continue For
                        End If

                        If objOpportunity.CheckWOStatus > 0 Then
                            errorMessageWorkorder += objFulFillmentOrder.OppName & "<br/>"
                            Continue For
                        End If

                        listFulFillmentOrder.Add(objFulFillmentOrder)
                    End If
                Next

                If selectedRecordCount = 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "NoRecordSelected", "alert('No record selected or selected record are already fulfilled.')", True)
                    Exit Sub
                End If

                Dim errorMessage As String = ""
                For Each objFulfillmentOrder In listFulFillmentOrder
                    Dim objOpportunity As New MOpportunity
                    objOpportunity.OpportunityId = objFulfillmentOrder.OppID
                    Dim dt As DataSet = objOpportunity.CheckOrderedAndInvoicedOrBilledQty()

                    Dim bitOrderedAndFulfilledQtyNotSame As Boolean = CCommon.ToBool(dt.Tables(0).Select("ID=3")(0)("bitTrue"))

                    'CREATE FULFILLMENT BIZ DOC OF REMAINING QTY AND MARK IF AS SHIPPED (RELEASE FROM ALLOCATION)
                    If bitOrderedAndFulfilledQtyNotSame Then
                        Try
                            Dim dtItemList As DataTable
                            dtItemList = objOpportunity.CheckCanbeShipped()
                            If dtItemList.Rows.Count > 0 Then
                                errorMessageInSufficientInventory += objFulfillmentOrder.OppName & "<br/>"
                                Continue For
                            End If

                            Dim objCOpportunity As New COpportunities
                            objCOpportunity.CreateFulfillmentBizDoc(Session("DomainID"), Session("UserContactID"), objFulfillmentOrder.OppID)
                        Catch ex As Exception
                            If ex.Message.Contains("WORK_ORDER_NOT_COMPLETED") Then
                                errorMessage += objFulfillmentOrder.OppName & ": Not able to add fulfillment bizdoc because some work orders are still not completed."
                            ElseIf ex.Message.Contains("QTY_MISMATCH") Then
                                errorMessage += objFulfillmentOrder.OppName & ": Not able to add fulfillment bizdoc because qty is not same as required qty.<br />"
                            ElseIf ex.Message.Contains("REQUIRED_SERIALS_NOT_PROVIDED") Then
                                errorMessage += objFulfillmentOrder.OppName & ": Not able to add fulfillment bizdoc because required number of serials are not provided.<br />"
                            ElseIf ex.Message.Contains("INVALID_SERIAL_NUMBERS") Then
                                errorMessage += objFulfillmentOrder.OppName & ": Not able to add fulfillment bizdoc because invalid serials are provided.<br />"
                            ElseIf ex.Message.Contains("REQUIRED_LOTNO_NOT_PROVIDED") Then
                                errorMessage += objFulfillmentOrder.OppName & ": Not able to add fulfillment bizdoc because required number of Lots are not provided.<br />"
                            ElseIf ex.Message.Contains("INVALID_LOT_NUMBERS") Then
                                errorMessage += objFulfillmentOrder.OppName & ": Not able to add fulfillment bizdoc because invalid Lots are provided.<br />"
                            ElseIf ex.Message.Contains("SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY") Then
                                errorMessage += objFulfillmentOrder.OppName & ": Not able to add fulfillment bizdoc because Lot number do not have enough qty to fulfill.<br />"
                            ElseIf ex.Message.Contains("NOTSUFFICIENTQTY_ALLOCATION") Then
                                errorMessage += objFulfillmentOrder.OppName & ": Not able to add fulfillment bizdoc because warehouse allocation is invlid.<br />"
                            ElseIf ex.Message.Contains("NOTSUFFICIENTQTY_ONHAND") Then
                                errorMessage += "Not able to add fulfillment bizdoc because warehouse does not have enought on hand quantity.<br />"
                            Else
                                errorMessage += objFulfillmentOrder.OppName & ": Not able to add fulfillment order due to unknown error.<br />"
                            End If
                            Exit Sub
                        End Try
                    End If
                Next

                If Not String.IsNullOrEmpty(errorMessageSerialLot) Or Not String.IsNullOrEmpty(errorMessageWorkorder) Or Not String.IsNullOrEmpty(errorMessageInSufficientInventory) Or Not String.IsNullOrEmpty(errorMessage) Then
                    Dim message As String = ""

                    If Not String.IsNullOrEmpty(errorMessageSerialLot) Then
                        message += "Not able to ship followring order(s) because serial/lot numbers required: <br />" & errorMessageSerialLot
                    End If

                    If Not String.IsNullOrEmpty(errorMessageWorkorder) Then
                        message += "Not able to ship followring order(s) because work order(s) are pending: <br />" & errorMessageWorkorder
                    End If

                    If Not String.IsNullOrEmpty(errorMessageInSufficientInventory) Then
                        message += "Not able to ship followring order(s) because of insufficient allocation: <br />" & errorMessageInSufficientInventory
                    End If

                    If Not String.IsNullOrEmpty(errorMessage) Then
                        message += "Not able to ship followring order(s) because error occurred: <br />" & errorMessage
                    End If

                    DisplayAlert(message)
                ElseIf CCommon.ToLong(hdnFromSalesOrderID.Value) > 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "RedirectToOrder", "document.location.href='../opportunity/frmOpportunities.aspx?opId=" & CCommon.ToLong(hdnFromSalesOrderID.Value) & "'", True)
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Function GetShippingMethod(ByVal arrDDL As ArrayList, _
                                      ByVal lngOppID As Double, _
                                      ByVal lngCoutryID As Long, _
                                      ByVal lngStateID As Long, _
                                      ByVal PostalCode As String) As String
            Dim strResult As String = ""
            Dim strErrLog As New StringBuilder
            Try

                'To get itemcount for Flat Rate Per Item method ...
                Dim dtItems As DataTable
                Dim dsItems As New DataSet
                objOpportunity = New MOpportunity
                objOpportunity.OpportunityId = lngOppID
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.UserCntID = Session("UserContactID")
                objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dsItems = objOpportunity.ItemsByOppId
                dtItems = dsItems.Tables(0)

                If dtItems.Rows.Count = 0 Then
                    strErrLog.Append("<p>Order Items are not found.</p>")
                    Exit Function
                End If

                Dim intCartItemCount As Integer = 0
                Dim dtShippingMethod As New DataTable()
                Dim TotalWeight As Decimal = 1
                Dim strMessage As String = ""
                '#Region "Get Shipping Rule"
                Dim objRule As New ShippingRule()
                objRule.DomainID = Session("DomainID")
                objRule.SiteID = -1
                objRule.DivisionID = CCommon.ToDouble(dtItems.Rows(0)("numDivisionID"))
                objRule.CountryID = lngCoutryID
                objRule.StateID = lngStateID
                dtShippingMethod = objRule.GetShippingMethodForItem1()
                '#End Region

                '#Region "Get Shipping Method only when one Of Items in Cart having Free Shipping = false"
                If dtShippingMethod.Rows.Count > 0 Then
                    'Default Column Add

                    Dim newColumn As New System.Data.DataColumn("IsShippingRuleValid", GetType(System.Boolean))
                    newColumn.DefaultValue = True
                    dtShippingMethod.Columns.Add(newColumn)

                    'Add this column to store it as a value field in the dropdown of ddlShippingMethod
                    CCommon.AddColumnsToDataTable(dtShippingMethod, "vcServiceTypeID1")

                    '#Region "foreach start"

                    For Each dr As DataRow In dtShippingMethod.Rows
                        Dim decShipingAmount As [Decimal] = 0
                        Dim totalCartAmount As Decimal = 0
                        If TotalWeight > 0 Then
                            '#Region "If All Items are not free shipping"
                            '#Region "Get Warehouse"
                            Dim objItem As New CItems()
                            Dim dtTable As DataTable = Nothing
                            'objItem.WarehouseID = CCommon.ToLong(HttpContext.Current.Session("DefaultWareHouseID"))
                            'objItem.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                            'dtTable = objItem.GetWareHouses()

                            Dim objContacts As New CContacts()
                            objContacts.DivisionID = CCommon.ToDouble(Session("UserDivisionID"))
                            objContacts.ContactID = CCommon.ToDouble(Session("UserContactID"))
                            objContacts.DomainID = CCommon.ToDouble(Session("DomainID"))
                            dtTable = objContacts.GetBillOrgorContAdd()
                            '#End Region

                            '#Region "Warehouse checking"
                            If dtTable.Rows.Count > 0 Then
                                'If CCommon.ToLong(dtTable.Rows(0)("numWCountry")) > 0 AndAlso CCommon.ToLong(dtTable.Rows(0)("numWState")) > 0 Then
                                If CCommon.ToLong(dtTable.Rows(0)("vcShipCountry")) > 0 AndAlso CCommon.ToLong(dtTable.Rows(0)("vcShipState")) > 0 Then
                                    '#Region "Get Shipping Rates when Warehouse is Configured"

                                    '#Region "Get Bizdoc"
                                    Dim objOppBizDocs As New OppBizDocs()
                                    objOppBizDocs.ShipCompany = CCommon.ToInteger(dr("numShippingCompanyID"))
                                    objOppBizDocs.ShipFromCountry = CCommon.ToLong(dtTable.Rows(0)("vcShipCountry")) 'CCommon.ToLong(dtTable.Rows(0)("numWCountry"))
                                    objOppBizDocs.ShipFromState = CCommon.ToLong(dtTable.Rows(0)("vcShipState")) 'CCommon.ToLong(dtTable.Rows(0)("numWState"))
                                    objOppBizDocs.ShipToCountry = lngCoutryID
                                    objOppBizDocs.ShipToState = lngStateID
                                    objOppBizDocs.strShipFromCountry = ""
                                    objOppBizDocs.strShipFromState = ""
                                    objOppBizDocs.strShipToCountry = ""
                                    objOppBizDocs.strShipToState = ""
                                    objOppBizDocs.GetShippingAbbreviation()
                                    '#End Region

                                    Dim blnValid As Boolean = False
                                    If CCommon.ToInteger(dr("tintFixedShippingQuotesMode")) = 1 Then
                                        '//Ship by order weight
                                        If TotalWeight >= CCommon.ToDecimal(dr("intFrom")) AndAlso TotalWeight <= CCommon.ToDecimal(dr("intTo")) Then
                                            blnValid = True
                                        End If

                                    ElseIf CCommon.ToInteger(dr("tintFixedShippingQuotesMode")) = 2 Then
                                        '//Ship by Order Total
                                        If totalCartAmount >= CCommon.ToDecimal(dr("intFrom")) AndAlso totalCartAmount <= CCommon.ToDecimal(dr("intTo")) Then
                                            blnValid = True
                                        End If

                                    End If

                                    If blnValid = True Then

                                        '#Region "Get Shipping Rates"
                                        Dim objShipping As New Shipping()
                                        objShipping.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                                        objShipping.WeightInLbs = CCommon.ToDouble(TotalWeight)
                                        objShipping.NoOfPackages = 1
                                        objShipping.UseDimentions = If(CCommon.ToInteger(dr("numShippingCompanyID")) = 91, True, False)
                                        'for fedex dimentions are must
                                        objShipping.GroupCount = 1
                                        If objShipping.UseDimentions Then
                                            objShipping.Height = objRule.Height
                                            objShipping.Width = objRule.Width
                                            objShipping.Length = objRule.Length
                                        End If

                                        objShipping.SenderState = objOppBizDocs.strShipFromState
                                        objShipping.SenderZipCode = CCommon.ToString(dtTable.Rows(0)("vcShipPostCode")) 'CCommon.ToString(dtTable.Rows(0)("vcWPinCode"))
                                        objShipping.SenderCountryCode = objOppBizDocs.strShipFromCountry

                                        objShipping.RecepientState = objOppBizDocs.strShipToState
                                        objShipping.RecepientZipCode = CCommon.ToString(PostalCode)
                                        objShipping.RecepientCountryCode = objOppBizDocs.strShipToCountry

                                        objShipping.ServiceType = Short.Parse(CCommon.ToString(dr("intNsoftEnum")))
                                        objShipping.PackagingType = 21
                                        objShipping.ItemCode = objRule.ItemID
                                        objShipping.OppBizDocItemID = 0
                                        objShipping.ID = 9999
                                        objShipping.Provider = CCommon.ToInteger(dr("numShippingCompanyID"))
                                        Dim dtShipRates As DataTable = objShipping.GetRates()

                                        If Not String.IsNullOrEmpty(objShipping.ErrorMsg) Then
                                            'log for error Message strMessage = objShipping.ErrorMsg;
                                            Try
                                                '''Throw New CustomException(objShipping.ErrorMsg)
                                                'strErrLog.Append("<p>" & objShipping.ErrorMsg & "</p>")
                                            Catch ex As Exception
                                                '''strMessage = GetErrorMessage("ERR068") & "<font color = ""WhiteSmoke"">" & ex.ToString().Replace("red", "WhiteSmoke") & "</font>"
                                                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(Session("DomainID")), CCommon.ToLong(Session("UserContactID")), CCommon.ToLong(Session("SiteId")), Request)
                                            End Try
                                        End If

                                        '#Region "Calculation Of Shipping Rates"
                                        If dtShipRates.Rows.Count > 0 Then
                                            If CCommon.ToDecimal(dtShipRates.Rows(0)("Rate")) > 0 Then
                                                'Currency conversion of shipping rate
                                                decShipingAmount = CCommon.ToDecimal(dtShipRates.Rows(0)("Rate")) / If(CCommon.ToDecimal(CCommon.ToString(Session("ExchangeRate"))) = 0, 1, CCommon.ToDecimal(CCommon.ToString(Session("ExchangeRate"))))

                                                If CCommon.ToBool(dr("bitMarkupType")) = True AndAlso CCommon.ToDecimal(dr("fltMarkup")) > 0 Then
                                                    dr("fltMarkup") = decShipingAmount / CCommon.ToDecimal(dr("fltMarkup"))
                                                End If
                                                'Percentage
                                                decShipingAmount = decShipingAmount + CCommon.ToDecimal(dr("fltMarkup"))
                                                dr("vcServiceName") = CCommon.ToString(dr("vcServiceName")) & " - " & CCommon.ToString(Session("CurrSymbol")) & " " & String.Format("{0:#,##0.00}", decShipingAmount)
                                                dr("vcServiceTypeID1") = dr("numServiceTypeID").ToString() & "~" & CCommon.ToString(decShipingAmount) & "~" & CCommon.ToString(dr("numRuleID")) & "~" & CCommon.ToString(dr("numShippingCompanyID")) & "~" & CCommon.ToString(dr("vcServiceName"))
                                            End If

                                        Else
                                            dr("IsShippingRuleValid") = False

                                        End If
                                    Else
                                        dr("IsShippingRuleValid") = False
                                    End If
                                Else
                                    Page.MaintainScrollPositionOnPostBack = False
                                End If
                            Else
                                Page.MaintainScrollPositionOnPostBack = False
                            End If
                        Else
                            'Set for not including it as rule if weight is 0 in the case of real Time Shipping Quotes .
                            dr("IsShippingRuleValid") = False
                            '#End Region
                        End If
                    Next
                    '#End Region
                    Dim drShippingMethod As DataRow() = dtShippingMethod.[Select]("IsShippingRuleValid=true")
                    If drShippingMethod.Length > 0 Then
                        dtShippingMethod = dtShippingMethod.[Select]("IsShippingRuleValid=true").CopyToDataTable()
                    Else
                        dtShippingMethod = New DataTable()
                    End If
                End If

                If dtShippingMethod.Rows.Count = 0 Then
                    ' It is necessary otherwise it will show dublicate records .
                    Dim list As New ListItem()
                    list.Selected = True
                    list.Text = "NA - $0"
                    list.Value = "0~0.00"

                    For Each ddlItem As DropDownList In arrDDL
                        ddlItem.Items.Add(list)
                        ddlItem.ClearSelection()
                    Next

                Else
                    For Each ddlItem As DropDownList In arrDDL
                        ddlItem.Items.Clear()

                        dtShippingMethod.Columns.Add("monRate1", GetType(System.Decimal))
                        For Each drService As DataRow In dtShippingMethod.Rows
                            If CCommon.ToString(drService("vcServiceName")).Contains("-") AndAlso CCommon.ToString(drService("vcServiceName")).Split("-").Length > 0 Then
                                drService("monRate1") = CCommon.ToDecimal(CCommon.ToString(drService("vcServiceName")).Split("-")(1).Trim())
                            Else
                                drService("monRate1") = 0
                            End If
                            dtShippingMethod.AcceptChanges()
                        Next

                        Dim dvShippingMethod As DataView
                        dvShippingMethod = dtShippingMethod.DefaultView
                        dvShippingMethod.Sort = "monRate1 ASC"
                        ddlItem.DataTextField = "vcServiceName"
                        ddlItem.DataValueField = "vcServiceTypeID1"
                        ddlItem.DataSource = dvShippingMethod
                        ddlItem.DataBind()
                    Next

                End If

            Catch ex As Exception
                strErrLog.Append("<p>" & ex.Message.ToString() & "</p>")
            End Try

            If strErrLog.ToString.Length > 0 Then
                strResult = strErrLog.ToString()
            End If

            Return strResult
        End Function

        Private Function GetOrderItemShippingRules(ByVal OppId As Long, _
                                                  ByVal OppBizDocID As Long, _
                                                  ByVal intDomainId As Integer, _
                                                  Optional ByVal intMode As Integer = 0) As DataTable
            'Use intMode = 0 for getting bizdoc items
            'Use intMode = 1 for getting opportunity items without using bizdocid
            Dim dtResult As DataTable = Nothing
            Try
                Dim dsItems As New DataSet
                Dim dtOrderDetail As New DataTable
                objOppBizDocs = New OppBizDocs
                objOppBizDocs.OppId = OppId
                objOppBizDocs.OppBizDocId = OppBizDocID
                objOppBizDocs.DomainID = Session("DomainID")

                If intMode = 0 Then
                    dsItems = objOppBizDocs.GetOppInItems()
                ElseIf intMode = 1 Then
                    If objOpportunity Is Nothing Then objOpportunity = New OppotunitiesIP
                    objOpportunity.OpportunityId = OppId
                    objOpportunity.DomainID = Session("DomainID")
                    objOpportunity.UserCntID = Session("UserContactID")
                    objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    dsItems = objOpportunity.ItemsByOppId
                End If


                'Tracing Part
                '------------------------------------------------------------------------------------------------------
                'Trace.Write("Start Tracing Items detail.")
                'Trace.Write("Is Item Dataset is initialized or not ? : " & IIf(dsItems Is Nothing, "NO", "YES"))
                'Trace.Write("Is Item Dataset has tables or not? : " & IIf(dsItems.Tables.Count > 0, dsItems.Tables.Count, 0))
                'Trace.Write("End Tracing Items detail.")

                '------------------------------------------------------------------------------------------------------

                If dsItems IsNot Nothing AndAlso dsItems.Tables.Count > 0 AndAlso dsItems.Tables(0).Rows.Count > 0 Then
                    dtOrderDetail.Columns.Add("OppId", GetType(Long))
                    dtOrderDetail.Columns.Add("OppBizDocItemID", GetType(Long))
                    dtOrderDetail.Columns.Add("ItemCode", GetType(Long))
                    dtOrderDetail.Columns.Add("ShippingRuleID", GetType(Long))
                    dtOrderDetail.Columns.Add("numShippingCompanyID1", GetType(Long))
                    dtOrderDetail.Columns.Add("numShippingCompanyID2", GetType(Long))
                    dtOrderDetail.Columns.Add("DomesticServiceID", GetType(Integer))
                    dtOrderDetail.Columns.Add("InternationalServiceID", GetType(Integer))
                    dtOrderDetail.Columns.Add("PackageTypeID", GetType(Integer))
                    dtOrderDetail.Columns.Add("numShipClass", GetType(Long))
                    dtOrderDetail.Columns.Add("numUnitHour", GetType(Long))
                    dtOrderDetail.Columns.Add("fltWeight", GetType(Double))
                    dtOrderDetail.Columns.Add("fltWidth", GetType(Double))
                    dtOrderDetail.Columns.Add("fltHeight", GetType(Double))
                    dtOrderDetail.Columns.Add("fltLength", GetType(Double))
                    dtOrderDetail.Columns.Add("vcItemName", GetType(String))

                    Trace.Write("Start Tracing Rules detail.")
                    ' GET SHIPPING RULES AS PER ITEM'S NEEDS
                    Dim dtShipInfo As New DataTable
                    For Each drItem As DataRow In dsItems.Tables(0).Rows
                        objOppBizDocs.OppId = OppId
                        objOppBizDocs.OppBizDocId = OppBizDocID
                        objOppBizDocs.BizDocItemId = 0 ' CCommon.ToLong(drItem("OppBizDocItemID"))
                        objOppBizDocs.ShipClassID = CCommon.ToLong(drItem("numShipClass"))
                        objOppBizDocs.UnitHour = CCommon.ToLong(drItem("numUnitHour"))
                        objOppBizDocs.DomainID = Session("DomainID")
                        dtShipInfo = objOppBizDocs.GetShippingRuleInfo()

                        'Trace.Write("Shipping Rule dataset available or not: " & If(dtShipInfo Is Nothing, "NO", "YES, Then its row count is :" & dtShipInfo.Rows.Count))
                        If dtShipInfo IsNot Nothing AndAlso dtShipInfo.Rows.Count > 0 Then
                            Dim drOrderDetail As DataRow
                            drOrderDetail = dtOrderDetail.NewRow

                            drOrderDetail("OppId") = OppId
                            drOrderDetail("OppBizDocItemID") = 0 'CCommon.ToLong(drItem("OppBizDocItemID"))
                            drOrderDetail("ItemCode") = CCommon.ToLong(drItem("numItemCode"))
                            drOrderDetail("ShippingRuleID") = CCommon.ToLong(dtShipInfo.Rows(0)("numShippingRuleID"))
                            drOrderDetail("numShippingCompanyID1") = CCommon.ToInteger(dtShipInfo.Rows(0)("numShippingCompanyID1"))
                            drOrderDetail("numShippingCompanyID2") = CCommon.ToInteger(dtShipInfo.Rows(0)("numShippingCompanyID2"))

                            drOrderDetail("DomesticServiceID") = CCommon.ToInteger(dtShipInfo.Rows(0)("numDomesticShipID"))
                            drOrderDetail("InternationalServiceID") = CCommon.ToInteger(dtShipInfo.Rows(0)("numInternationalShipID"))
                            drOrderDetail("PackageTypeID") = CCommon.ToInteger(dtShipInfo.Rows(0)("numPackageTypeID"))
                            drOrderDetail("numShipClass") = CCommon.ToLong(drItem("numShipClass"))
                            drOrderDetail("numUnitHour") = CCommon.ToLong(drItem("numUnitHour"))

                            drOrderDetail("fltWeight") = CCommon.ToDouble(drItem("fltWeight"))
                            drOrderDetail("fltWidth") = CCommon.ToDouble(drItem("fltWidth"))
                            drOrderDetail("fltHeight") = CCommon.ToDouble(drItem("fltHeight"))
                            drOrderDetail("fltLength") = CCommon.ToDouble(drItem("fltLength"))
                            drOrderDetail("vcItemName") = CCommon.ToString(drItem("vcItemName"))

                            dtOrderDetail.Rows.Add(drOrderDetail)
                            dtOrderDetail.AcceptChanges()
                        End If

                    Next

                Else
                    dtResult = Nothing
                End If

                'Trace.Write("End Tracing Rules Detail.")

                If dtOrderDetail IsNot Nothing Then
                    Dim dtView As DataView = dtOrderDetail.DefaultView
                    dtView.Sort = "numShipClass"
                    dtResult = dtView.ToTable()
                Else
                    dtResult = Nothing
                End If

            Catch ex As Exception
                dtResult = Nothing
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            End Try

            Return dtResult
        End Function

        Private Sub GetBoxLog(ByVal lngOppId As Long)
            Try
                Dim dblTotalWeight As Double = 0
                If objShipping Is Nothing Then objShipping = New Shipping()
                Dim ds As New DataSet
                Dim dsFinal As New DataSet
                Dim intBoxCounter As Integer = 0
                Dim blnSameCountry As Boolean = False

                Dim dtBox As New DataTable("Box")
                dtBox.Columns.Add("vcBoxName")
                dtBox.Columns.Add("vcPackageName")
                dtBox.Columns.Add("fltTotalWeight")
                dtBox.Columns.Add("fltHeight")
                dtBox.Columns.Add("fltWidth")
                dtBox.Columns.Add("fltLength")
                dtBox.Columns.Add("numPackageTypeID")
                dtBox.Columns.Add("numServiceTypeID")
                dtBox.Columns.Add("fltDimensionalWeight")


                Dim dtItem As New DataTable("Item")
                dtItem.Columns.Add("vcBoxName")
                dtItem.Columns.Add("numOppBizDocItemID")
                dtItem.Columns.Add("intBoxQty")
                dtItem.Columns.Add("UnitWeight")
                dtItem.Columns.Add("vcItemName")
                dtItem.Columns.Add("fltHeight")
                dtItem.Columns.Add("fltLength")
                dtItem.Columns.Add("fltWidth")

                Dim dsItems As New DataSet
                Dim dtItems As New DataTable
                Dim dtShipDetail As DataTable = GetOrderItemShippingRules(lngOppId, 0, Session("DomainID"), 1)
                If dtShipDetail Is Nothing Then
                    DisplayAlert(lblException.Text & "Order ID " & lngOppId & " : " & vbCrLf & " Shipping Rule has not been found." & "<br/>")
                    Exit Sub
                End If

                dsItems.Tables.Add(dtShipDetail.Copy)
                If dsItems IsNot Nothing AndAlso dsItems.Tables.Count > 0 AndAlso dsItems.Tables(0).Rows.Count > 0 Then

                    If lngOppId > 0 Then
                        dtItems = dsItems.Tables(0).Clone
                        Dim objOpportunity As New MOpportunity
                        For i As Integer = 0 To dsItems.Tables(0).Rows.Count - 1

                            objOpportunity.OpportunityId = 0
                            objOpportunity.DomainID = Session("DomainID")
                            objOpportunity.ItemCode = CCommon.ToLong(dsItems.Tables(0).Rows(i)("ItemCode"))
                            If objOpportunity.CheckServiceItemInOrder() = True Then
                                Continue For
                            End If

                            Dim objOpp As New MOpportunity()
                            Dim objCommon As New CCommon()
                            objOpp.OpportunityId = lngOppId
                            objOpp.Mode = 1
                            Dim dtTable As DataTable = Nothing
                            dtTable = objOpp.GetOpportunityAddress()

                            Dim dtShipFrom As DataTable = Nothing
                            Dim objContacts As New CContacts()
                            objContacts.ContactID = Session("UserContactID")
                            objContacts.DomainID = Session("DomainID")
                            dtShipFrom = objContacts.GetBillOrgorContAdd()
                            If objCommon Is Nothing Then
                                objCommon = New CCommon()
                            End If

                            ' GET SHIPPING COMPANY INFORMATION
                            Dim dtShipCompany As DataTable = Nothing
                            objOppBizDocs.OppId = lngOppId
                            objOppBizDocs.ShipClassID = CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShipClass"))
                            objOppBizDocs.BizDocItemId = CCommon.ToLong(dsItems.Tables(0).Rows(i)("OppBizDocItemID"))

                            'IF Shipping Detail is not available continue process with another order detail
                            If dtTable.Rows.Count = 0 OrElse dtShipFrom.Rows.Count = 0 Then
                                Continue For
                            End If

                            If CCommon.ToLong(dtTable.Rows(0)("Country")) = CCommon.ToLong(dtShipFrom.Rows(0)("vcShipCountry")) Then
                                objOppBizDocs.ShipCompany = CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShippingCompanyID1"))
                                objOppBizDocs.BoxServiceTypeID = CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID"))
                            Else
                                objOppBizDocs.ShipCompany = CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShippingCompanyID2"))
                                objOppBizDocs.BoxServiceTypeID = CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID"))
                            End If

                            'Get Order Items and Process Shipping Label Generating Logic
                            If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs

                            objOppBizDocs.ShippingReportId = 0
                            objOppBizDocs.DomainID = Session("DomainID")
                            objOppBizDocs.OppBizDocId = 0

                            ' FROM DETAIL
                            objOppBizDocs.FromName = CCommon.ToString(dtShipFrom.Rows(0)("vcFirstname")) & " " & CCommon.ToString(dtShipFrom.Rows(0)("vcLastname"))
                            objOppBizDocs.FromCompany = CCommon.ToString(dtShipFrom.Rows(0)("vcCompanyName"))
                            objOppBizDocs.FromPhone = CCommon.ToString(dtShipFrom.Rows(0)("vcPhone"))
                            If dtShipFrom.Rows(0)("vcShipStreet").ToString().Contains(Environment.NewLine) Then
                                Dim strAddressLines As String() = dtShipFrom.Rows(0)("vcShipStreet").ToString().Split(System.Environment.NewLine) ' dtShipFrom.Rows(0)("vcShipStreet").ToString().Split(New Char() {Convert.ToChar(Environment.NewLine)}, StringSplitOptions.RemoveEmptyEntries)
                                objOppBizDocs.FromAddressLine1 = (If(strAddressLines.Length >= 0, strAddressLines(0).ToString(), "")).ToString()
                                objOppBizDocs.FromAddressLine2 = (If(strAddressLines.Length >= 1, strAddressLines(1).ToString(), "")).ToString()
                            Else
                                objOppBizDocs.FromAddressLine1 = CCommon.ToString(dtShipFrom.Rows(0)("vcShipStreet"))
                                objOppBizDocs.FromAddressLine2 = ""
                            End If

                            'objOppBizDocs.FromAddressLine1 = dtShipFrom.Rows[0]["vcShipStreet"].ToString().Substring(dtShipFrom.Rows[0]["vcShipStreet"].ToString().IndexOf(Environment.NewLine)).Replace(Environment.NewLine, " ");
                            'objOppBizDocs.FromAddressLine2 = dtShipFrom.Rows[0]["vcShipStreet"].ToString().Substring(0, dtShipFrom.Rows[0]["vcShipStreet"].ToString().IndexOf(Environment.NewLine));
                            objOppBizDocs.FromCountry = CCommon.ToString(dtShipFrom.Rows(0)("vcShipCountry"))
                            objOppBizDocs.FromState = CCommon.ToString(dtShipFrom.Rows(0)("vcShipState"))
                            objOppBizDocs.FromCity = CCommon.ToString(dtShipFrom.Rows(0)("vcShipCity"))
                            objOppBizDocs.FromZip = CCommon.ToString(dtShipFrom.Rows(0)("vcShipPostCode"))
                            objOppBizDocs.IsFromResidential = False

                            objOppBizDocs.UserCntID = Session("UserContactID")
                            ' Order Record Owner
                            objOppBizDocs.strText = Nothing

                            'TO DETAILS
                            objOppBizDocs.ToName = CCommon.ToString(dtTable.Rows(0)("Name"))
                            objOppBizDocs.ToCompany = CCommon.ToString(dtTable.Rows(0)("Company"))
                            objOppBizDocs.ToPhone = CCommon.ToString(dtTable.Rows(0)("Phone"))

                            If dtTable.Rows(0)("Street").ToString().Contains(Environment.NewLine) Then
                                Dim strAddressLines As String() = dtTable.Rows(0)("Street").ToString().Split(System.Environment.NewLine) 'dtTable.Rows(0)("Street").ToString().Split(New Char() {Convert.ToChar(Environment.NewLine)}, StringSplitOptions.RemoveEmptyEntries)
                                objOppBizDocs.ToAddressLine1 = (If(strAddressLines.Length >= 0, strAddressLines(0).ToString(), "")).ToString()
                                objOppBizDocs.ToAddressLine2 = (If(strAddressLines.Length >= 1, strAddressLines(1).ToString(), "")).ToString()
                            Else
                                objOppBizDocs.ToAddressLine1 = CCommon.ToString(dtTable.Rows(0)("Street"))
                                objOppBizDocs.ToAddressLine2 = ""
                            End If

                            objOppBizDocs.ToCity = CCommon.ToString(dtTable.Rows(0)("City"))
                            objOppBizDocs.ToState = CCommon.ToString(dtTable.Rows(0)("State"))
                            objOppBizDocs.ToCountry = CCommon.ToString(dtTable.Rows(0)("Country"))
                            objOppBizDocs.ToZip = CCommon.ToString(dtTable.Rows(0)("PostCode"))
                            objOppBizDocs.IsToResidential = False

                            ' SPECIAL SERVICES
                            objOppBizDocs.IsCOD = False
                            objOppBizDocs.IsDryIce = False
                            objOppBizDocs.IsHoldSaturday = False
                            objOppBizDocs.IsHomeDelivery = False
                            objOppBizDocs.IsInsideDelivery = False
                            objOppBizDocs.IsInsidePickup = False
                            objOppBizDocs.IsReturnShipment = False
                            objOppBizDocs.IsSaturdayDelivery = False
                            objOppBizDocs.IsSaturdayPickup = False
                            objOppBizDocs.CODAmount = 0
                            objOppBizDocs.CODType = ""
                            objOppBizDocs.TotalInsuredValue = 0
                            objOppBizDocs.IsAdditionalHandling = False
                            objOppBizDocs.LargePackage = False
                            objOppBizDocs.DeliveryConfirmation = ""
                            objOppBizDocs.Description = ""

                            'Update Shipping report address values and create shipping report items 
                            objShipping.DomainID = Session("DomainID")
                            objShipping.UserCntID = Session("UserContactID")
                            objShipping.ShippingReportId = 0
                            objShipping.OpportunityId = lngOppId
                            objShipping.OppBizDocId = objOppBizDocs.OppBizDocId
                            objShipping.InvoiceNo = CCommon.ToString(objOppBizDocs.OppBizDocId)

                            'If dtShipCompany IsNot Nothing AndAlso dtShipCompany.Rows.Count > 0 Then
                            objShipping.Provider = CCommon.ToInteger(dsItems.Tables(0).Rows(i)("numShippingCompanyID1"))
                            objShipping.PackagingType = CCommon.ToShort(dsItems.Tables(0).Rows(i)("PackageTypeID"))

                            If CCommon.ToLong(dtTable.Rows(0)("Country")) = CCommon.ToLong(dtShipFrom.Rows(0)("vcShipCountry")) Then
                                objShipping.ServiceType = CCommon.ToShort(dsItems.Tables(0).Rows(i)("DomesticServiceID"))
                                blnSameCountry = True
                            Else
                                objShipping.ServiceType = CCommon.ToShort(dsItems.Tables(0).Rows(i)("InternationalServiceID"))
                            End If

                            'FOR FOLLOWING SERVICE TYPES,IN FEDEX THERE IS ONLY ONE "YOUR PACKAGING" IS AVAILABLE
                            If objShipping.Provider = 91 AndAlso _
                               (objShipping.ServiceType = 15 OrElse objShipping.ServiceType = 16 OrElse _
                                objShipping.ServiceType = 17 OrElse objShipping.ServiceType = 18 OrElse _
                                objShipping.ServiceType = 19) Then

                                objShipping.PackagingType = 21

                            End If
                            '------------------------------------------------------------------------------------

                            Dim dtFields As New DataTable
                            'If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                            dtFields.Columns.Add("numBoxID")
                            dtFields.Columns.Add("tintServiceType")
                            dtFields.Columns.Add("dtDeliveryDate")
                            dtFields.Columns.Add("monShippingRate")
                            dtFields.Columns.Add("fltTotalWeight")
                            dtFields.Columns.Add("intNoOfBox")
                            dtFields.Columns.Add("fltHeight")
                            dtFields.Columns.Add("fltWidth")
                            dtFields.Columns.Add("fltLength")
                            dtFields.Columns.Add("fltDimensionalWeight")
                            dtFields.TableName = "Box"

                            Dim lngBoxID As Long = 0
                            Dim strBoxID As String = 0
                            Dim lngTotalQty As Long = 0
                            Dim dblWidth As Double = 0
                            Dim dblHeight As Double = 0
                            Dim dblLength As Double = 0
                            Dim dblWeight As Double = 0

                            Dim lngPackageTypeID As Long = 0
                            Dim lngServiceTypeID As Long = 0

                            If dtItems.Select("numShipClass = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShipClass")) & _
                                              " AND PackageTypeID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("PackageTypeID")) & _
                                              " AND DomesticServiceID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID")) & _
                                              " AND InternationalServiceID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("InternationalServiceID"))).Length > 0 Then
                                Continue For
                            Else
                                dtItems.Rows.Clear()
                            End If

                            lngTotalQty = 0
                            Dim drClass() As DataRow = dsItems.Tables(0).Select("numShipClass = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShipClass")) & _
                                                                                " AND PackageTypeID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("PackageTypeID")) & _
                                                                                " AND DomesticServiceID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID")) & _
                                                                                " AND InternationalServiceID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("InternationalServiceID")))
                            For Each dr As DataRow In drClass
                                dtItems.ImportRow(dr)
                                lngTotalQty = lngTotalQty + CCommon.ToLong(dr("numUnitHour"))
                            Next

                            Dim dblBoxQty As Double
                            Dim dblItemQty As Double
                            Dim dblCarryFwdQty As Double = 0
                            Dim dblQtyToBox As Double = 0
                            Dim dblWeightToBox As Double = 0
                            Dim dblDimensionalWeight As Double = 0

                            For iItem As Integer = 0 To dtItems.Rows.Count - 1

                                objOppBizDocs.OppId = lngOppId
                                objOppBizDocs.ShipClassID = CCommon.ToLong(dtItems.Rows(iItem)("numShipClass"))
                                objOppBizDocs.ItemCode = CCommon.ToLong(dtItems.Rows(iItem)("ItemCode"))
                                objOppBizDocs.UnitHour = IIf(lngTotalQty = 0, CCommon.ToDouble(dtItems.Rows(iItem)("numUnitHour")), lngTotalQty)
                                objOppBizDocs.PackageTypeID = CCommon.ToLong(dtItems.Rows(iItem)("PackageTypeID"))
                                objOppBizDocs.DomainID = Session("DomainID")
                                lngPackageTypeID = objOppBizDocs.PackageTypeID
                                lngServiceTypeID = objShipping.ServiceType

                                objOpportunity.OpportunityId = 0
                                objOpportunity.DomainID = Session("DomainID")
                                objOpportunity.ItemCode = CCommon.ToLong(dtItems.Rows(iItem)("ItemCode"))
                                If objOpportunity.CheckServiceItemInOrder() = True Then
                                    Continue For
                                End If

                                Dim dtPackages As DataTable = Nothing
                                Dim intBoxCount As Integer = 0
                                If objShipping.PackagingType <> 21 Then
                                    'ADD ITEMS INTO PACKAGE AS PER PACKAGE RULES
                                    dtPackages = objOppBizDocs.GetPackageInfo()
                                    'If package rule not found then set default 
                                    If dtPackages IsNot Nothing AndAlso dtPackages.Rows.Count > 0 Then
                                        intBoxCount = Math.Ceiling(objOppBizDocs.UnitHour / If(dtPackages.Rows(0)("numFromQty") = 0, 1, CCommon.ToDouble(dtPackages.Rows(0)("numFromQty"))))

                                        dblWeight = CCommon.ToDouble(dtPackages.Rows(0)("fltTotalWeight"))
                                        dblWidth = CCommon.ToDouble(dtPackages.Rows(0)("fltWidth"))
                                        dblHeight = CCommon.ToDouble(dtPackages.Rows(0)("fltHeight"))
                                        dblLength = CCommon.ToDouble(dtPackages.Rows(0)("fltLength"))
                                        dblDimensionalWeight = (If(dblWidth = 0, 1, dblWidth) * If(dblHeight = 0, 1, dblHeight) * If(dblLength = 0, 1, dblLength)) / 166

                                    Else
                                        DisplayAlert(lblException.Text & "Order ID " & objOppBizDocs.OppId & " : " & vbCrLf & " Package Rule has not been found." & "<br/>")
                                        Continue For

                                    End If

                                Else
                                    intBoxCount = 1
                                    dblWeight = 1
                                    dblWidth = 1
                                    dblHeight = 1
                                    dblLength = 1
                                    dblDimensionalWeight = (If(dblWidth = 0, 1, dblWidth) * If(dblHeight = 0, 1, dblHeight) * If(dblLength = 0, 1, dblLength)) / 166

                                End If

                                If objOppBizDocs.UnitHour <= 0 Then
                                    DisplayAlert(lblException.Text & "Order ID " & objOppBizDocs.OppId & " : " & "Quantity can not be zero." & "<br/>")
                                    Continue For
                                End If

                                dblBoxQty = CCommon.ToDouble(dtPackages.Rows(0)("numFromQty"))
                                dblItemQty = CCommon.ToDouble(dtItems.Rows(iItem)("numUnitHour"))

                                If lngTotalQty <> CCommon.ToDouble(dtItems.Rows(iItem)("numUnitHour")) Then
                                    dblItemQty = dblCarryFwdQty + CCommon.ToDouble(dtItems.Rows(iItem)("numUnitHour"))
                                End If

                                If intBoxCounter = intBoxCount - 1 AndAlso intBoxCounter > 1 Then
                                    Exit For
                                Else
                                    For intCount As Integer = 0 To intBoxCount - 1

                                        Dim dr As DataRow
                                        If dblItemQty <= 0 Then
                                            If dblItemQty = 0 Then dblCarryFwdQty = 0
                                            intBoxCounter = intBoxCounter - 1
                                            Exit For

                                        ElseIf dblItemQty < dblBoxQty Then
                                            intBoxCounter = intBoxCounter + 1
                                            dblCarryFwdQty = dblItemQty

                                            If dblCarryFwdQty > 0 AndAlso dtItems.Rows.Count <> iItem + 1 Then
                                                dr = dtItem.NewRow
                                                dr("vcBoxName") = "Box" & intBoxCounter ' (intCount + i + 1).ToString()
                                                dr("numOppBizDocItemID") = CCommon.ToLong(dtItems.Rows(iItem)("OppBizDocItemID"))
                                                dr("intBoxQty") = dblCarryFwdQty
                                                dr("UnitWeight") = CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")) ' dblWeight
                                                dr("vcItemName") = CCommon.ToString(dtItems.Rows(iItem)("vcItemName"))
                                                dr("fltHeight") = CCommon.ToDouble(dtItems.Rows(iItem)("fltHeight"))
                                                dr("fltLength") = CCommon.ToDouble(dtItems.Rows(iItem)("fltLength"))
                                                dr("fltWidth") = CCommon.ToDouble(dtItems.Rows(iItem)("fltWidth"))

                                                dtItem.Rows.Add(dr)
                                                dblTotalWeight = dblTotalWeight + CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")) 'dblWeight

                                                dblQtyToBox = dblCarryFwdQty
                                            Else
                                                ''Check for other Shipping Rule / Package Rule available for Remaining Quantity or not.

                                                Dim dtShipInfoNew As DataTable
                                                objOppBizDocs.OppId = lngOppId
                                                objOppBizDocs.OppBizDocId = 0
                                                objOppBizDocs.BizDocItemId = 0
                                                objOppBizDocs.ShipClassID = CCommon.ToLong(dtItems.Rows(iItem)("numShipClass"))
                                                objOppBizDocs.UnitHour = dblCarryFwdQty
                                                dtShipInfoNew = objOppBizDocs.GetShippingRuleInfo()

                                                ''If available then use that package otherwise same package will be used.
                                                If dtShipInfoNew IsNot Nothing AndAlso dtShipInfoNew.Rows.Count > 0 Then
                                                    objOppBizDocs.OppId = lngOppId
                                                    objOppBizDocs.ShipClassID = CCommon.ToLong(dtItems.Rows(iItem)("numShipClass"))
                                                    objOppBizDocs.ItemCode = CCommon.ToLong(dtItems.Rows(iItem)("ItemCode"))
                                                    objOppBizDocs.UnitHour = dblCarryFwdQty 'IIf(lngTotalQty = 0, CCommon.ToLong(dtItems.Rows(iItem)("numUnitHour")), lngTotalQty)
                                                    objOppBizDocs.PackageTypeID = CCommon.ToLong(dtShipInfoNew.Rows(0)("numPackageTypeID")) 'objShipping.PackagingType
                                                    objOppBizDocs.DomainID = Session("DomainID")

                                                    'ADD ITEMS INTO PACKAGE AS PER PACKAGE RULES
                                                    Dim dtCarryFPackage As DataTable = Nothing
                                                    dtCarryFPackage = objOppBizDocs.GetPackageInfo()
                                                    'If package rule not found then set default 
                                                    If dtPackages IsNot Nothing AndAlso dtCarryFPackage.Rows.Count > 0 Then
                                                        lngPackageTypeID = CCommon.ToLong(dtCarryFPackage.Rows(0)("numCustomPackageID"))
                                                        lngServiceTypeID = If(blnSameCountry = True, CCommon.ToShort(dtShipInfoNew.Rows(0)("numDomesticShipID")), CCommon.ToShort(dtShipInfoNew.Rows(0)("numInternationalShipID")))

                                                    End If
                                                End If


                                                dr = dtItem.NewRow
                                                dr("vcBoxName") = "Box" & intBoxCounter
                                                dr("numOppBizDocItemID") = CCommon.ToLong(dtItems.Rows(iItem)("OppBizDocItemID"))
                                                dr("vcItemName") = CCommon.ToString(dtItems.Rows(iItem)("vcItemName"))
                                                dr("fltHeight") = CCommon.ToDouble(dtItems.Rows(iItem)("fltHeight"))
                                                dr("fltLength") = CCommon.ToDouble(dtItems.Rows(iItem)("fltLength"))
                                                dr("fltWidth") = CCommon.ToDouble(dtItems.Rows(iItem)("fltWidth"))

                                                If dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'").Length > 0 Then
                                                    Dim intExistBoxQty As Integer
                                                    For Each drQty As DataRow In dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'")
                                                        intExistBoxQty = intExistBoxQty + CCommon.ToDouble(drQty("intBoxQty"))
                                                    Next
                                                    If dblBoxQty <= intExistBoxQty Then
                                                        Continue For
                                                    Else
                                                        dblCarryFwdQty = dblBoxQty - intExistBoxQty
                                                    End If
                                                End If

                                                dr("intBoxQty") = dblCarryFwdQty
                                                dr("UnitWeight") = CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight"))
                                                dtItem.Rows.Add(dr)

                                                dblTotalWeight = dblTotalWeight + CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight"))
                                                dblQtyToBox = dblCarryFwdQty
                                            End If

                                            dblWeightToBox = dblQtyToBox * CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight"))
                                        Else

                                            intBoxCounter = intBoxCounter + 1
                                            If intBoxCounter = intBoxCount - 1 AndAlso iItem = dtItems.Rows.Count - 1 Then
                                            End If

                                            dr = dtItem.NewRow
                                            dr("vcBoxName") = "Box" & intBoxCounter
                                            dr("numOppBizDocItemID") = CCommon.ToLong(dtItems.Rows(iItem)("OppBizDocItemID"))
                                            dr("vcItemName") = CCommon.ToString(dtItems.Rows(iItem)("vcItemName"))
                                            dr("fltHeight") = CCommon.ToDouble(dtItems.Rows(iItem)("fltHeight"))
                                            dr("fltLength") = CCommon.ToDouble(dtItems.Rows(iItem)("fltLength"))
                                            dr("fltWidth") = CCommon.ToDouble(dtItems.Rows(iItem)("fltWidth"))

                                            Dim dblExistBoxQty As Double
                                            If dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'").Length > 0 Then

                                                For Each drQty As DataRow In dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'")
                                                    dblExistBoxQty = dblExistBoxQty + CCommon.ToDouble(drQty("intBoxQty"))
                                                Next
                                                If dblBoxQty <= dblExistBoxQty Then
                                                    Continue For
                                                Else
                                                    dblExistBoxQty = dblBoxQty - dblExistBoxQty
                                                End If
                                            Else
                                                dblExistBoxQty = dblBoxQty - dblCarryFwdQty
                                            End If

                                            dr("intBoxQty") = dblExistBoxQty
                                            dr("UnitWeight") = CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight"))

                                            dtItem.Rows.Add(dr)
                                            dblTotalWeight = dblTotalWeight + CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight"))

                                            dblQtyToBox = dblBoxQty - dblCarryFwdQty

                                            If dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'").Length > 1 Then
                                                For Each drI As DataRow In dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'")
                                                    dblWeightToBox = dblWeightToBox + (drI("intBoxQty") * drI("UnitWeight"))
                                                Next
                                            Else
                                                dblWeightToBox = (dblQtyToBox * CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")))

                                            End If

                                        End If

                                        ''Creating Box Entry
                                        dr = dtBox.NewRow
                                        dr("vcBoxName") = "Box" & intBoxCounter
                                        dr("vcPackageName") = CCommon.ToString(dtPackages.Rows(0)("vcPackageName"))
                                        dr("fltTotalWeight") = dblWeightToBox
                                        dr("fltLength") = dblLength
                                        dr("fltWidth") = dblWidth
                                        dr("fltHeight") = dblHeight
                                        dr("numPackageTypeID") = lngPackageTypeID
                                        dr("numServiceTypeID") = lngServiceTypeID
                                        dr("fltDimensionalWeight") = dblDimensionalWeight

                                        If dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'").Length > 1 Then
                                            dtBox.Rows.Remove(dtBox.Select("vcBoxName='" & "Box" & intBoxCounter & "'")(0))
                                        End If
                                        dtBox.Rows.Add(dr)

                                        dblWeightToBox = 0
                                        dblTotalWeight = 0
                                        dblItemQty = dblItemQty - dblQtyToBox
                                    Next
                                End If
                            Next
                        Next

                        dtBox.TableName = "Box"
                        dtItem.TableName = "Item"
                        ds.Tables.Add(dtBox)
                        ds.Tables.Add(dtItem)

                        Dim dtMain As New DataTable
                        dtMain.Columns.Add("AddedBy")
                        dtMain.Columns.Add("AddedOn")
                        dtMain.Columns.Add("RegularWeight")
                        dtMain.Columns.Add("DimensionalWeight")
                        Dim dblRegularWeight As Double = 0
                        Dim dblDimWeight As Double = 0

                        For Each drBox As DataRow In dtBox.Rows
                            dblRegularWeight = dblRegularWeight + drBox("fltTotalWeight")
                            dblDimWeight = dblDimWeight + drBox("fltDimensionalWeight")
                        Next

                        Dim drMain As DataRow
                        drMain = dtMain.NewRow
                        drMain("AddedBy") = objOppBizDocs.FromName
                        drMain("AddedOn") = CCommon.ToSqlDate(DateTime.Now)
                        drMain("RegularWeight") = dblRegularWeight
                        drMain("DimensionalWeight") = dblDimWeight
                        dtMain.Rows.Add(drMain)
                        dtMain.TableName = "Main"
                        dtMain.AcceptChanges()
                        ds.Tables.Add(dtMain)

                        If Not Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) Then
                            Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
                        End If

                        ds.WriteXml(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & "OrderShippingDetail_" & lngOppId & "_" & CCommon.ToString(Session("DomainID")) & ".xml")

                        Dim str As String
                        str = String.Format("<script language='javascript'>window.open('../opportunity/frmOrderLog.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID={0}','','toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')</script>",
                                            lngOppId.ToString())

                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "ShippingLog", str, False)
                    Else
                        DisplayAlert("Shipping Rules are not defined/found.")
                    End If
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Function AddShippingItem(ByVal lngOppId As Double, _
                                    ByVal strShippingValue As String) As String
            Dim strResult As String = ""
            Dim strErrLog As New StringBuilder
            Try
                Dim dblShippingCharges As Double = 0
                Dim strDesc As String = 0

                If strShippingValue <> "" Then
                    Dim strShippingCharge As String() = strShippingValue.Split("~"c)
                    If strShippingCharge.Length >= 2 Then
                        dblShippingCharges = strShippingCharge(1)
                    End If
                    If strShippingCharge.Length >= 4 Then
                        strDesc = strShippingCharge(4)
                    End If
                End If

                Dim dsTemp As New DataSet
                If objOpportunity Is Nothing Then objOpportunity = New MOpportunity
                objOpportunity.OpportunityId = lngOppId
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.UserCntID = Session("UserContactID")
                objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dsTemp = objOpportunity.ItemsByOppId

                objCommon = New CCommon
                Dim dtUnit As DataTable
                objCommon.DomainID = Session("DomainID")
                objCommon.ItemCode = CCommon.ToDouble(Session("ShippingServiceItem"))
                objCommon.UOMAll = True
                dtUnit = objCommon.GetItemUOM()

                Dim dtTable, dtItemTax As DataTable
                Dim objItems As New CItems
                Dim strUnit As String

                objOpportunity.OppItemCode = 0 'CCommon.ToLong(txtHidEditOppItem.Text)
                objOpportunity.OpportunityId = lngOppId
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.Mode = 2
                objItems.ItemCode = CCommon.ToDouble(Session("ShippingServiceItem"))
                dtTable = objItems.ItemDetails

                strUnit = "numSaleUnit"
                Dim strApplicable As String
                objItems.DomainID = Session("DomainID")
                dtItemTax = objItems.ItemTax()
                For Each dr As DataRow In dtItemTax.Rows
                    strApplicable = strApplicable & dr("bitApplicable") & ","
                Next

                If dtTable.Rows.Count > 0 Then

                    strApplicable = strApplicable & dtTable.Rows(0).Item("bitTaxable")
                    objItems.ItemName = CCommon.ToString(dtTable.Rows(0).Item("vcItemName"))
                    objItems.type = CCommon.ToInteger(dtTable.Rows(0).Item("tintStandardProductIDType"))
                    objItems.ItemDesc = strDesc
                    objItems.Description = strDesc
                    objItems.ModelID = CCommon.ToString(dtTable.Rows(0).Item("vcModelID"))
                    objItems.PathForImage = CCommon.ToString(dtTable.Rows(0).Item("vcPathForImage"))
                    objItems.ItemType = dtTable.Rows(0).Item("ItemType")

                    objItems.AllowDropShip = CCommon.ToBool(dtTable.Rows(0).Item("bitAllowDropShip"))
                    objItems.VendorID = CCommon.ToLong(dtTable.Rows(0).Item("numVendorID"))
                    objItems.IsPrimaryVendor = True

                    objItems.Weight = CCommon.ToDouble(dtTable.Rows(0).Item("fltWeight"))
                    objItems.FreeShipping = CCommon.ToBool(dtTable.Rows(0).Item("bitFreeShipping"))
                    objItems.Height = CCommon.ToDouble(dtTable.Rows(0).Item("fltHeight"))
                    objItems.Width = CCommon.ToDouble(dtTable.Rows(0).Item("fltWidth"))
                    objItems.Length = CCommon.ToDouble(dtTable.Rows(0).Item("fltLength"))

                    'Dim dtUnit As DataTable
                    If strUnit.Length > 0 Then
                        objItems.UnitofMeasure = dtTable.Rows(0).Item("vcUnitofMeasure")

                        objItems.BaseUnit = dtTable.Rows(0).Item("numBaseUnit")
                        objItems.PurchaseUnit = dtTable.Rows(0).Item("numPurchaseUnit")
                        objItems.SaleUnit = dtTable.Rows(0).Item("numSaleUnit")

                        objCommon.DomainID = Session("DomainID")
                        objCommon.ItemCode = objItems.ItemCode
                        objCommon.UnitId = dtTable.Rows(0).Item("numSaleUnit")
                        dtUnit = objCommon.GetItemUOMConversion()

                    End If

                    objCommon = New CCommon
                    Dim objItem As New CItems
                    If Not objItem.ValidateItemAccount(CCommon.ToDouble(Session("ShippingServiceItem")), Session("DomainID"), 1) = True Then
                        strErrLog.Append("<p>Please Set Income,Asset,COGs(Expense) Account for selected Item from Administration->Inventory->Item Details</p>")
                        Exit Try
                    End If

                    Dim dtTaxTypes As DataTable
                    Dim ObjTaxItems As New TaxDetails
                    ObjTaxItems.DomainID = Session("DomainID")
                    dtTaxTypes = ObjTaxItems.GetTaxItems

                    For Each drTax As DataRow In dtTaxTypes.Rows
                        dtTable.Columns.Add("Tax" & drTax("numTaxItemID"), GetType(Decimal))
                        dtTable.Columns.Add("bitTaxable" & drTax("numTaxItemID"))
                    Next

                    Dim dr As DataRow
                    dr = dtTaxTypes.NewRow
                    dr("numTaxItemID") = 0
                    dr("vcTaxName") = "Sales Tax(Default)"
                    dtTaxTypes.Rows.Add(dr)

                    Dim chkTaxItemsForService As New CheckBoxList
                    chkTaxItemsForService.DataTextField = "vcTaxName"
                    chkTaxItemsForService.DataValueField = "numTaxItemID"
                    chkTaxItemsForService.DataSource = dtTaxTypes
                    chkTaxItemsForService.DataBind()

                    If Not dsTemp.Tables(0).Columns.Contains("Op_Flag") Then dsTemp.Tables(0).Columns.Add("Op_Flag")
                    If Not dsTemp.Tables(0).Columns.Contains("bitIsAuthBizDoc") Then dsTemp.Tables(0).Columns.Add("bitIsAuthBizDoc")
                    If Not dsTemp.Tables(0).Columns.Contains("numUnitHourReceived") Then dsTemp.Tables(0).Columns.Add("numUnitHourReceived")
                    If Not dsTemp.Tables(0).Columns.Contains("numQtyShipped") Then dsTemp.Tables(0).Columns.Add("numQtyShipped")
                    If Not dsTemp.Tables(0).Columns.Contains("numUOM") Then dsTemp.Tables(0).Columns.Add("numUOM")
                    If Not dsTemp.Tables(0).Columns.Contains("vcUOMName") Then dsTemp.Tables(0).Columns.Add("vcUOMName")
                    If Not dsTemp.Tables(0).Columns.Contains("UOMConversionFactor") Then dsTemp.Tables(0).Columns.Add("UOMConversionFactor")
                    If Not dsTemp.Tables(0).Columns.Contains("monTotAmtBefDiscount") Then dsTemp.Tables(0).Columns.Add("monTotAmtBefDiscount")
                    If Not dsTemp.Tables(0).Columns.Contains("vcBaseUOMName") Then dsTemp.Tables(0).Columns.Add("vcBaseUOMName")
                    If Not dsTemp.Tables(0).Columns.Contains("bitDiscountType") Then dsTemp.Tables(0).Columns.Add("bitDiscountType")
                    If Not dsTemp.Tables(0).Columns.Contains("numVendorWareHouse") Then dsTemp.Tables(0).Columns.Add("numVendorWareHouse")
                    If Not dsTemp.Tables(0).Columns.Contains("numShipmentMethod") Then dsTemp.Tables(0).Columns.Add("numShipmentMethod")
                    If Not dsTemp.Tables(0).Columns.Contains("numSOVendorId") Then dsTemp.Tables(0).Columns.Add("numSOVendorId")
                    If Not dsTemp.Tables(0).Columns.Contains("numWarehouseID") Then dsTemp.Tables(0).Columns.Add("numWarehouseID")
                    If Not dsTemp.Tables(0).Columns.Contains("numWarehouseItmsID") Then dsTemp.Tables(0).Columns.Add("numWarehouseItmsID")
                    If Not dsTemp.Tables(0).Columns.Contains("vcSKU") Then dsTemp.Tables(0).Columns.Add("vcSKU")

                    dsTemp = OpportunityMangagement.AddEditItemtoDataTable(objCommon, dsTemp, True, objItems.ItemType, objItems.ItemType, objItems.AllowDropShip,
                                                                         objItems.KitParent, CCommon.ToDouble(Session("ShippingServiceItem")), 1, dblShippingCharges,
                                                                         objItems.ItemDesc, 0, objItems.ItemName, "", 0, "", 0, "", 1,
                                                                         1, False, 0, strApplicable, objItems.Tax, "", chkTaxItemsForService, False, "", Nothing,
                                                                         0, 0, 0, 0, 0, 0, True, 0, 0, 0, 0, 0, 0, 0)


                    Dim drRow As DataRow
                    Dim dsItems As DataSet = dsTemp
                    drRow = dsItems.Tables(0).Select("numItemCode = " & CCommon.ToDouble(Session("ShippingServiceItem")))(0)
                    If drRow IsNot Nothing Then
                        drRow("tintOppType") = 1
                        drRow("numOppId") = lngOppId
                        drRow("numReturnID") = 0
                        'drRow("ReturnedQty") = 0
                        drRow("numOriginalUnitHour") = 0
                        drRow("Source") = ""
                        drRow("numSourceID") = 0
                        drRow("bitKitParent") = 0
                        drRow("numShipClass") = 0
                        'drRow("vcPathForImage") = ""
                        drRow("vcItemName") = objItems.ItemName
                        drRow("vcModelId") = objItems.ModelID
                        drRow("vcWarehouse") = ""
                        drRow("numOnHand") = 1
                        drRow("vcLocation") = ""
                        drRow("vcItemDesc") = objItems.ItemDesc
                        drRow("vcAttributes") = ""
                        drRow("DropShip") = False
                        drRow("bitDropShip") = False
                        drRow("vcSKU") = ""
                        drRow("vcManufacturer") = ""
                        drRow("numItemClassification") = objItems.ItemClassification
                        drRow("numItemGroup") = objItems.ItemGroupID
                        drRow("fltWeight") = 0
                        drRow("numBarCodeId") = 0
                        drRow("vcWorkOrderStatus") = ""
                        drRow("DiscAmt") = 0
                        drRow("bitItemAddedToAuthBizDoc") = True
                        drRow("numClassID") = objItems.ItemClass
                        drRow("vcProjectStageName") = ""
                        drRow("numQty") = 1
                        drRow("bitSerialized") = False
                        drRow("bitLotNo") = False
                        drRow("numUOMId") = 0
                        drRow("vcUOMName") = ""
                        dsItems.Tables(0).Columns.Remove("numWarehouseId")
                        dsItems.Tables(0).TableName = "Item"
                        If dsItems.Tables.Count > 1 Then
                            dsItems.Tables.Remove("Table2")
                        End If
                        dsItems.Tables(0).AcceptChanges()

                    End If

                    If dsItems.Tables(0).Rows.Count > 0 Then
                        For Each drCheck As DataRow In dsItems.Tables(0).Rows
                            If String.IsNullOrEmpty(CCommon.ToString(drCheck("Op_Flag"))) Then
                                drCheck("Op_Flag") = 2
                            Else
                                drCheck("Op_Flag") = 1
                            End If
                            dsItems.Tables(0).AcceptChanges()
                        Next

                        Dim objOpp As New OppotunitiesIP
                        objOpp.OpportunityId = lngOppId
                        objOpp.DomainID = Session("DomainID")
                        objOpp.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                        objOpp.OpportunityDetails()

                        objOpp.OpportunityId = lngOppId
                        objOpp.PublicFlag = 0
                        objOpp.AssignedTo = Session("UserContactID")
                        objOpp.UserCntID = Session("UserContactID")
                        objOpp.DomainID = Session("DomainId")
                        objOpp.OppType = 1

                        If dsItems IsNot Nothing Then
                            objOpp.strItems = "<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & dsItems.GetXml
                            If objOpp.strItems.Contains("<numWarehouseItmsID>0</numWarehouseItmsID>") OrElse objOpp.strItems.Contains("<numWarehouseItmsID></numWarehouseItmsID>") Then
                                objOpp.strItems = objOpp.strItems.Replace("<numWarehouseItmsID>0</numWarehouseItmsID>", "")
                                objOpp.strItems = objOpp.strItems.Replace("<numWarehouseItmsID></numWarehouseItmsID>", "")
                            End If
                            Try
                                objOpp.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                                objOpp.Save()
                            Catch ex As Exception
                                strErrLog.Append("<p>Error(While Inserting Record) occurred while saving record for OrderID:" & lngOppId & "</p>")
                            End Try
                        End If
                    End If
                End If
            Catch ex As Exception
                strErrLog.Append("<p>Error (Thrown Error) occurred while saving record for OrderID:" & lngOppId & "</p>")
            End Try

            If strErrLog.ToString.Length > 0 Then
                strResult = strErrLog.ToString()
            End If
            Return strResult
        End Function

        'Private Function GetShippingChargesLabel(lngOppId) As String
        '    Dim strResult As String = ""
        '    Try
        '        Dim dsTemp As New DataSet
        '        If objOpportunity Is Nothing Then objOpportunity = New MOpportunity
        '        objOpportunity.OpportunityId = lngOppId
        '        objOpportunity.DomainID = Session("DomainID")
        '        objOpportunity.UserCntID = Session("UserContactID")
        '        objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
        '        dsTemp = objOpportunity.ItemsByOppId

        '        If dsTemp IsNot Nothing AndAlso dsTemp.Tables.Count > 0 AndAlso dsTemp.Tables(0).Rows.Count > 0 Then
        '            Dim drItem() As DataRow
        '            drItem = dsTemp.Tables(0).Select("numItemCode=" & CCommon.ToDouble(Session("ShippingServiceItem")))
        '            If drItem IsNot Nothing AndAlso drItem.Length > 0 Then
        '                Dim strServiceName As String = ""
        '                Dim dblServiceCharges As Double = 0
        '                dblServiceCharges = CCommon.ToDouble(drItem(0)("monTotAmount"))
        '                strServiceName = If(CCommon.ToString(drItem(0)("vcItemDesc")).Contains("-"), CCommon.ToString(drItem(0)("vcItemDesc")).Split("-")(0), CCommon.ToString(drItem(0)("vcItemDesc")))

        '                strResult = "Shipping Charges " & strServiceName & " - " & [String].Format("{0:#,##0.00}", dblServiceCharges) & " " & _
        '                            CCommon.ToString(HttpContext.Current.Session("CurrSymbol")) & " is added."
        '            End If
        '        Else
        '            strResult = "No shipping charges rates are available."
        '        End If
        '    Catch ex As Exception
        '        strResult = "Error in getting shipping charges."
        '    End Try

        '    Return strResult
        'End Function

        Private Sub chkExpandGrid_CheckedChanged(sender As Object, e As EventArgs) Handles chkExpandGrid.CheckedChanged
            Try
                For Each item As GridItem In gvSearch.MasterTableView.Items
                    item.Expanded = chkExpandGrid.Checked
                Next

                PersistTable(chkExpandGrid.ID) = chkExpandGrid.Checked

                If CCommon.ToLong(hdnFromSalesOrderID.Value) = 0 Then
                    PersistTable.Save()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub txLot_TextChanged(sender As Object, e As EventArgs)
            Try
                Dim remainingSerialLot As Int32 = 0
                Dim isSerialLotExists As Boolean = False
                Dim rgx As New Regex("^[^\(\)]*\(\d+\)$")
                Dim txtLot As TextBox = DirectCast(sender, TextBox)
                Dim item As GridDataItem = DirectCast(sender, TextBox).NamingContainer

                If Not item Is Nothing Then
                    Dim bitSerial As Boolean = CCommon.ToBool(item.GetDataKeyValue("bitSerialized"))
                    Dim bitLot As Boolean = CCommon.ToBool(item.GetDataKeyValue("bitLotNo"))
                    Dim lblSelectedLot As Label = DirectCast(item.FindControl("lblSelectedLot"), Label)
                    Dim oppItemID As Long = CCommon.ToLong(item.GetDataKeyValue("numoppitemtCode"))
                    Dim numWarehouseItemID As Long = CCommon.ToLong(item.GetDataKeyValue("numWarehouseItmsID"))
                    Dim numOrgUnitQty As Int32 = CCommon.ToInteger(item.GetDataKeyValue("numUnitHourOrig"))

                    Dim listSerialLot As New System.Collections.Generic.Dictionary(Of String, Int32)

                    If Not String.IsNullOrEmpty(lblSelectedLot.Text) Then
                        Dim arrSelectedSerialLot As String() = lblSelectedLot.Text.Split(",")
                        For Each vcSelectedNo As String In arrSelectedSerialLot
                            If bitLot Then
                                listSerialLot.Add(vcSelectedNo.Substring(0, vcSelectedNo.Trim().IndexOf("(")).Trim(), CCommon.ToInteger(vcSelectedNo.Substring(vcSelectedNo.Trim().IndexOf("(") + 1, vcSelectedNo.Length - vcSelectedNo.IndexOf("(") - 2)))
                            Else
                                listSerialLot.Add(vcSelectedNo, 1)
                            End If
                        Next
                    End If

                    Dim arrSerialLots As String() = txtLot.Text.Trim().Split(",")

                    For Each vcSerialLot In arrSerialLots
                        If bitLot Then
                            vcSerialLot = vcSerialLot.Trim()

                            Dim tempLotNo As String = vcSerialLot
                            Dim currentQty As Int32 = 1
                            Dim newQty As Int32 = currentQty

                            'When input came from barcode scanner it will not provide quantity in brackets instead user will scan same lot number multiple times 
                            If Not rgx.IsMatch(vcSerialLot) Then
                                If listSerialLot.ContainsKey(tempLotNo) Then
                                    newQty += CCommon.ToInteger(listSerialLot(tempLotNo))
                                    listSerialLot(tempLotNo) = newQty
                                Else
                                    listSerialLot.Add(tempLotNo, newQty)
                                End If
                            Else
                                tempLotNo = vcSerialLot.Substring(0, vcSerialLot.Trim().IndexOf("(")).Trim()
                                currentQty = CCommon.ToInteger(vcSerialLot.Substring(vcSerialLot.Trim().IndexOf("(") + 1, vcSerialLot.Length - vcSerialLot.IndexOf("(") - 2))
                                newQty = currentQty

                                If listSerialLot.ContainsKey(tempLotNo) Then
                                    newQty += CCommon.ToInteger(listSerialLot(tempLotNo))
                                    listSerialLot(tempLotNo) = newQty
                                Else
                                    listSerialLot.Add(tempLotNo, currentQty)
                                End If
                            End If

                            'VERIFIES WHETHER SERIAL/LOT# IS VALID OR NOT
                            Dim objOpp As New COpportunities
                            objOpp.OppItemCode = oppItemID
                            objOpp.WarehouseItmsID = numWarehouseItemID
                            objOpp.bitLotNo = bitLot
                            isSerialLotExists = objOpp.VerifySerialLot(tempLotNo & "(" & newQty & ")")

                            If Not isSerialLotExists Then
                                DisplayAlert("Either Lot number <b>" & vcSerialLot & "</b> does not exists or does not have required quantity to fulfill.")
                                txtLot.Text = ""
                                txtLot.Focus()
                                Exit Sub
                            End If

                            Dim totalQty As Int32 = listSerialLot.Sum(Function(x) x.Value)

                            If totalQty <= numOrgUnitQty Then
                                Dim finalLot As String = ""

                                For Each objKey As System.Collections.Generic.KeyValuePair(Of String, Int32) In listSerialLot
                                    finalLot &= objKey.Key & "(" & objKey.Value & ")" & ","
                                Next

                                finalLot = finalLot.TrimEnd(",")

                                lblSelectedLot.Text = finalLot

                                If totalQty = numOrgUnitQty Then
                                    txtLot.Text = ""
                                Else
                                    txtLot.Text = ""
                                    txtLot.Focus()
                                End If
                            Else
                                DisplayAlert("Total lot qty selected is more than required quantity.")
                                txtLot.Text = ""
                                Exit Sub
                            End If
                        Else
                            If listSerialLot.ContainsKey(vcSerialLot) Then
                                DisplayAlert("Serial number <b>" & vcSerialLot & "</b> is already selected.")
                                txtLot.Text = ""
                                txtLot.Focus()
                                Exit Sub
                            Else
                                'VERIFIES WHETHER SERIAL/LOT# IS VALID OR NOT
                                Dim objOpp As New COpportunities
                                objOpp.OppItemCode = oppItemID
                                objOpp.WarehouseItmsID = numWarehouseItemID
                                objOpp.bitLotNo = bitLot
                                isSerialLotExists = objOpp.VerifySerialLot(vcSerialLot)

                                If Not isSerialLotExists Then
                                    DisplayAlert("Serial number <b>" & vcSerialLot & "</b> does not exists.")
                                    txtLot.Text = ""
                                    txtLot.Focus()
                                    Exit Sub
                                End If

                                listSerialLot.Add(vcSerialLot, 1)

                                Dim totalQty As Int32 = listSerialLot.Sum(Function(x) x.Value)

                                If totalQty <= numOrgUnitQty Then
                                    Dim finalLot As String = ""

                                    For Each objKey As System.Collections.Generic.KeyValuePair(Of String, Int32) In listSerialLot
                                        finalLot &= objKey.Key & ","
                                    Next

                                    finalLot = finalLot.TrimEnd(",")

                                    lblSelectedLot.Text = finalLot

                                    If totalQty = numOrgUnitQty Then
                                        txtLot.Text = ""
                                    Else
                                        txtLot.Text = ""
                                        txtLot.Focus()
                                    End If
                                Else
                                    DisplayAlert("Total serials selected is more than required quantity.")
                                    txtLot.Text = ""
                                    Exit Sub
                                End If
                            End If
                        End If
                    Next
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub imgClearSerial_Click(sender As Object, e As ImageClickEventArgs)
            Try
                Dim item As GridDataItem = DirectCast(sender, ImageButton).NamingContainer

                If Not item Is Nothing Then
                    Dim lblSelectedLot As Label = DirectCast(item.FindControl("lblSelectedLot"), Label)
                    Dim txtLot As TextBox = DirectCast(item.FindControl("txLot"), TextBox)

                    If Not lblSelectedLot Is Nothing Then
                        lblSelectedLot.Text = ""
                    End If

                    If Not txtLot Is Nothing Then
                        txtLot.Focus()
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Private Sub DisplayAlert(ByVal message As String)
            Try
                lblException.Text = message
                divAlert.Visible = True
                divAlert.Focus()
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub btnPrintLabels_Click(sender As Object, e As EventArgs) Handles btnPrintLabels.Click
            Try
                'If not browser refresh
                If CCommon.ToString(Session("BizBrowserRefresh")) = CCommon.ToString(ViewState("BizBrowserRefresh")) Then
                    Session("BizBrowserRefresh") = Guid.NewGuid.ToString()

                    If (txtSelOppId.Text <> "") Then
                        If CCommon.ToLong(radcmbWarehouses.SelectedValue) > 0 Then
                            If String.IsNullOrEmpty(CCommon.ToString(radcmbWarehouses.SelectedItem.Attributes("PrintNodeAPIKey"))) Or String.IsNullOrEmpty(CCommon.ToString(radcmbWarehouses.SelectedItem.Attributes("PrintNodePrinterID"))) Then
                                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "PrintNode", "alert('PrintNode is not configured for selected warehouse');", True)
                                PrintShippingLabels()
                                divWarehouse.Style.Add("display", "none")
                            Else
                                Dim service As New PrintNodeService("https://api.printnode.com/", radcmbWarehouses.SelectedItem.Attributes("PrintNodeAPIKey"))

                                Dim objOppBizDocs As New OppBizDocs()
                                Dim dsShippingLabels As New DataSet

                                objOppBizDocs.BoxID = 0
                                objOppBizDocs.tintMode = 3
                                objOppBizDocs.strOrderID = txtSelOppId.Text.Trim(",")
                                objOppBizDocs.DomainID = Session("DomainID")
                                dsShippingLabels = objOppBizDocs.GetShippingLabelList()

                                Dim vcExcludedOrders As String = ""
                                Dim zplError As String = ""

                                If dsShippingLabels IsNot Nothing AndAlso dsShippingLabels.Tables.Count > 0 AndAlso dsShippingLabels.Tables(0).Rows.Count > 0 Then
                                    For Each dr As DataRow In dsShippingLabels.Tables(0).Rows
                                        If String.IsNullOrEmpty(dr("vcShippingLabelImage")) Then
                                            vcExcludedOrders = vcExcludedOrders & dr("vcPOppName") & "<br />"
                                        Else
                                            Try
                                                service.SubmitPrintJob(CCommon.GetDocumentPath(Session("DomainID")) & CCommon.ToString(dr("vcShippingLabelImage")), radcmbWarehouses.SelectedItem.Attributes("PrintNodePrinterID"))

                                                Dim objSalesFulfillmentQueue As New SalesFulfillmentQueue
                                                objSalesFulfillmentQueue.DomainID = CCommon.ToLong(Session("DomainID"))
                                                objSalesFulfillmentQueue.SFQID = CCommon.ToLong(dr("numOppID"))
                                                objSalesFulfillmentQueue.Message = "Shipping Label(s) Printed."
                                                objSalesFulfillmentQueue.UpdateStatus(5, True)
                                            Catch ex As Exception
                                                If zplError.Length > 0 Then
                                                    zplError = zplError & dr("vcPOppName") & ":" & ex.Message & "<br/>"
                                                Else
                                                    zplError = "Error occured while printing shipping label for order:" & "<br/>" & dr("vcPOppName") & ":" & ex.Message & "<br/>"
                                                End If
                                            End Try
                                        End If
                                    Next
                                End If

                                'ZPL Labels
                                If dsShippingLabels IsNot Nothing AndAlso dsShippingLabels.Tables.Count > 1 AndAlso dsShippingLabels.Tables(1).Rows.Count > 0 Then
                                    For Each dr As DataRow In dsShippingLabels.Tables(1).Rows
                                        If String.IsNullOrEmpty(dr("vcShippingLabelImage")) Then
                                            vcExcludedOrders = vcExcludedOrders & dr("vcPOppName") & "<br />"
                                        Else
                                            Try
                                                service.SubmitPrintJob(CCommon.GetDocumentPath(Session("DomainID")) & CCommon.ToString(dr("vcShippingLabelImage")), radcmbWarehouses.SelectedItem.Attributes("PrintNodePrinterID"))

                                                Dim objSalesFulfillmentQueue As New SalesFulfillmentQueue
                                                objSalesFulfillmentQueue.DomainID = CCommon.ToLong(Session("DomainID"))
                                                objSalesFulfillmentQueue.SFQID = CCommon.ToLong(dr("numOppID"))
                                                objSalesFulfillmentQueue.Message = "Shipping Label(s) Printed."
                                                objSalesFulfillmentQueue.UpdateStatus(5, True)
                                            Catch ex As Exception
                                                If zplError.Length > 0 Then
                                                    zplError = zplError & dr("vcPOppName") & ":" & ex.Message & "<br/>"
                                                Else
                                                    zplError = "Error occured while printing shipping label for order:" & "<br/>" & dr("vcPOppName") & ":" & ex.Message & "<br/>"
                                                End If
                                            End Try
                                        End If
                                    Next
                                End If

                                If vcExcludedOrders.Length > 0 AndAlso zplError.Length > 0 Then
                                    DisplayAlert("Shipping Labels are not available for following orders: <br/>" & vcExcludedOrders & "<br/><br/>" & zplError)
                                ElseIf vcExcludedOrders.Length > 0 Then
                                    DisplayAlert("Shipping Labels are not available for following orders: <br/>" & vcExcludedOrders)
                                ElseIf zplError.Length > 0 Then
                                    DisplayAlert(zplError)
                                End If

                                divWarehouse.Style.Add("display", "none")
                            End If
                        Else
                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "SelectWarehouse", "alert('Select warehouse to print label');", True)
                            divWarehouse.Style.Add("display", "")
                        End If
                    End If

                    BindDatagrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Protected Sub lkbClearFilter_Click(sender As Object, e As EventArgs)
            Try
                radShippingZone.SelectedValue = "0"
                chkExpandGrid.Checked = True
                txtSortChar.Text = ""
                txtSortColumn.Text = ""
                txtGridColumnFilter.Text = ""
                txtSortOrder.Text = ""

                If CCommon.ToLong(hdnFromSalesOrderID.Value) = 0 Then
                    PersistTable.Clear()
                    PersistTable.Add(PersistKey.CurrentPage, "1")
                    PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim)
                    PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text)
                    PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text)
                    PersistTable.Add(PersistKey.GridColumnSearch, txtGridColumnFilter.Text)
                    PersistTable.Add(chkExpandGrid.ID, False)
                    PersistTable.Add(radShippingZone.ID, radShippingZone.SelectedValue)
                    PersistTable.Add(radOrderStatus.ID, "")
                    PersistTable.Add(ddlFilterBy.ID, "0")
                    PersistTable.Add(radFilterValue.ID, "")
                    PersistTable.Save()
                End If

                Response.Redirect(Request.RawUrl)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub GridColumnSearchCriteria()
            Try
                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")

                    Dim strIDValue(), strID(), strCustom As String
                    Dim strRegularCondition As New ArrayList
                    Dim strCustomCondition As New ArrayList

                    For i As Integer = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")
                        strID = strIDValue(0).Split("~")

                        If strID(0).Contains("CFW.Cust") Then

                            Select Case strID(3).Trim()
                                Case "TextBox", "TextArea"
                                    strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & " ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                Case "CheckBox"
                                    If strIDValue(1).ToLower() = "yes" Then
                                        strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and COALESCE(CFW.Fld_Value,'0') " & "='1')")
                                    ElseIf strIDValue(1).ToLower() = "no" Then
                                        strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND 1 = (CASE WHEN (SELECT COUNT(*) FROM CFW_Fld_Values_Opp CFWInner WHERE CFWInner.RecId=OpportunityMaster.numOppId AND CFWInner.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND CFWInner.Fld_Value='1') > 0 THEN 0 ELSE 1 END))")
                                    End If
                                Case "SelectBox"
                                    strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & "=" & strIDValue(1) & ")")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " >= '" & fromDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " <= '" & toDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    End If
                                Case "CheckBoxList"
                                    Dim items As String() = strIDValue(1).Split(",")
                                    Dim searchString As String = ""

                                    For Each item As String In items
                                        searchString = searchString & If(searchString.Length > 0, " OR ", "") & " GetCustFldValueOpp(" & strID(0).Replace("CFW.Cust", "") & ",OpportunityMaster.numOppID) " & " ilike '%" & item.Replace("'", "''") & "%'"
                                    Next

                                    strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND (" & searchString & "))")
                                Case Else
                                    strCustomCondition.Add(" Opp.numOppid in (select distinct OpportunityMaster.numOppId from OpportunityMaster left join CFW_Fld_Values_Opp CFW ON OpportunityMaster.numOppId=CFW.RecId where OpportunityMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & strCustom & ")")
                            End Select
                        ElseIf strID(0) = "OBD.vcBizDocsList" Then
                            strRegularCondition.Add("(SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId = OM.numOppId AND vcBizDocID IN ('" & strIDValue(1).Replace("'", "").Replace(",", "','") & "')) > 0")
                        ElseIf strID(0) = "OM.tintInvoicingTextbox" Then
                            strRegularCondition.Add("(SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId = OM.numOppId AND vcBizDocID IN ('" & strIDValue(1).Replace("'", "").Replace(",", "','") & "')) > 0")
                        ElseIf strID(0) = "OI.vcInventoryStatus" Then
                            strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                        ElseIf strID(0) = "AD.numShipState" Then
                            strRegularCondition.Add("fn_getOPPAddressState(Opp.numOppId,Opp.numDomainID,2) ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                        Else
                            Select Case strID(3).Trim()
                                Case "Website", "Email", "TextBox", "Label"
                                    strRegularCondition.Add(strID(0) & " ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                Case "SelectBox"
                                    If strID(0) = "OL.numWebApiId" Then
                                        If strIDValue(1).StartsWith("Sites~") Then
                                            strRegularCondition.Add("OL.numSiteID=" & strIDValue(1).Replace("Sites~", ""))
                                        Else
                                            strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                                        End If
                                    ElseIf strID(0) = "OM.tintSource" Then
                                        strRegularCondition.Add("OM.tintSource=" & strIDValue(1).ToString.Split("~")(0) & " and OM.tintSourceType=" & strIDValue(1).ToString.Split("~")(1))
                                    ElseIf strID(0) = "DMSC.vcSignatureType" Then
                                        strRegularCondition.Add("COALESCE(" & strID(0) & ",-1) =" & strIDValue(1))
                                    Else
                                        strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                                    End If

                                Case "TextArea"
                                    strRegularCondition.Add(" Cast(" & strID(0) & " as varchar(5000)) ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strRegularCondition.Add(strID(0) & " >= '" & fromDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strRegularCondition.Add(strID(0) & " <= '" & toDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    End If
                            End Select
                        End If
                    Next
                    RegularSearch = String.Join(" and ", strRegularCondition.ToArray())
                    CustomSearch = String.Join(" and ", strCustomCondition.ToArray())
                Else
                    RegularSearch = ""
                    CustomSearch = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub btnFilterValue_Click(sender As Object, e As EventArgs)
            Try
                ''txtFilterOrder.Text = ""
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub btnOrderStatus_Click(sender As Object, e As EventArgs)
            Try
                ''txtFilterOrder.Text = ""
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub ddlFilterBy_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFilterBy.SelectedIndexChanged
            Try
                LoadFilterValueComboBox(CCommon.ToLong(ddlFilterBy.SelectedValue))

                If ddlFilterBy.SelectedValue = "0" Then
                    Dim strOrderStatusValue As String = ""

                    If radOrderStatus.CheckedItems.Count > 0 Then
                        For Each item As RadComboBoxItem In radOrderStatus.CheckedItems
                            strOrderStatusValue = strOrderStatusValue & item.Value & ","
                        Next
                    End If

                    PersistTable.Clear()
                    PersistTable.Add(PersistKey.CurrentPage, "1")
                    PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim)
                    PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text)
                    PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text)
                    PersistTable.Add(PersistKey.GridColumnSearch, txtGridColumnFilter.Text)
                    PersistTable.Add(chkExpandGrid.ID, chkExpandGrid.Checked)
                    PersistTable.Add(radShippingZone.ID, radShippingZone.SelectedValue)
                    PersistTable.Add(radOrderStatus.ID, strOrderStatusValue)
                    PersistTable.Add(ddlFilterBy.ID, ddlFilterBy.SelectedValue)
                    PersistTable.Add(radFilterValue.ID, "")

                    If CCommon.ToLong(hdnFromSalesOrderID.Value) = 0 Then
                        PersistTable.Save()
                    End If

                    Response.Redirect(Request.RawUrl)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
    End Class

    Class FulFillmentOrder
        Public Property DivisionID As Long
        Public Property OppID As Long
        Public Property OppName As String
        Public Property vcItems As String
    End Class
End Namespace
