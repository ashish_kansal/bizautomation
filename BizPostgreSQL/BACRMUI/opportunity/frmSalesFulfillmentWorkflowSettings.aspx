﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/DetailPage.Master"
    CodeBehind="frmSalesFulfillmentWorkflowSettings.aspx.vb" Inherits=".frmSalesFulfillmentWorkflowSettings" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Sales Fulfillment Workflow Settings</title>
    <link href="../CSS/GridColorScheme.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function Save() {
            var rule1Trigger = -1;
            var rule2Trigger = -1;
            var rule3Trigger = -1;
            var rule4Trigger = -1;

            if ($("[id$=chkActivateRule1]").is(":checked")) {
                if ($("[id$=ddlRule1BizDoc]").val() == "0") {
                    alert('Select bizdoc for rule 1.');
                    $("[id$=ddlRule1BizDoc]").focus()
                    return false;
                } else if ($("[id$=rdbOrderStatusChange]").is(":checked")) {
                    if ($("[id$=ddlRule1OrderStatus]").val() == "0") {
                        alert('Select order status for rule 1.');
                        $("[id$=ddlRule1OrderStatus]").focus()
                        return false;
                    } else {
                        rule1Trigger = $("[id$=ddlRule1OrderStatus]").val();
                    }
                }
            }

            if ($("[id$=chkActivateRule2]").is(":checked")) {
                if ($("[id$=ddlRule2OrderStatus]").val() == "0") {
                    alert('Select order status to trigger rule 2.');
                    $("[id$=ddlRule2OrderStatus]").focus()
                    return false;
                } else if ($("[id$=ddlRule2OrderStatus]").val() == rule1Trigger) {
                    alert('Order status to trigger rule 2 is same as rule 1. Select unique status for each rule.');
                    return false;
                }

                rule2Trigger = $("[id$=ddlRule2OrderStatus]").val();
            }

            if ($("[id$=chkActivateRule3]").is(":checked")) {
                if ($("[id$=ddlRule3OrderStatus]").val() == "0") {
                    alert('Select order status to trigger rule 3.');
                    $("[id$=ddlRule3OrderStatus]").focus()
                    return false;
                } else if ($("[id$=ddlRule3OrderStatus]").val() == rule1Trigger) {
                    alert('Order status to trigger rule 3 is same as rule 1. Select unique status for each rule.');
                    return false;
                } else if ($("[id$=ddlRule3OrderStatus]").val() == rule2Trigger) {
                    alert('Order status to trigger rule 3 is same as rule 2. Select unique status for each rule.');
                    return false;
                }

                rule3Trigger = $("[id$=ddlRule3OrderStatus]").val();
            }

            if ($("[id$=chkActivateRule4]").is(":checked")) {
                if ($("[id$=ddlRule4OrderStatus]").val() == "0") {
                    alert('Select order status to trigger rule 4.');
                    $("[id$=ddlRule4OrderStatus]").focus()
                    return false;
                } else if ($("[id$=ddlRule4OrderStatus]").val() == rule1Trigger) {
                    alert('Order status to trigger rule 4 is same as rule 1. Select unique status for each rule.');
                    return false;
                } else if ($("[id$=ddlRule4OrderStatus]").val() == rule2Trigger) {
                    alert('Order status to trigger rule 4 is same as rule 2. Select unique status for each rule.');
                    return false;
                } else if ($("[id$=ddlRule4OrderStatus]").val() == rule3Trigger) {
                    alert('Order status to trigger rule 4 is same as rule 3. Select unique status for each rule.');
                    return false;
                }

                rule4Trigger = $("[id$=ddlRule4OrderStatus]").val();
            }
        }

        function SaveColor() {
            if (document.getElementById("ddlColorOrderStatus").selectedIndex == 0) {
                alert("Select a order Status");
                document.getElementById("ddlColorOrderStatus").focus();
                return false;
            }

            if ($find('ddlColorScheme').get_value() == 0) {
                alert("Select Color Scheme");
                return false;
            }

            return true;
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function pageLoaded() {
            $("[id$=chkActivateRule3]").change(function () {
                if ($(this).is(":checked")) {
                    alert('Rule may not execute due to one of the following reasons:\n\n1.Fulfillment Order BizDoc is not selected from the “Select Sales BizDoc responsible for Shipping Labels & Tracking #s” Section within Administration | Global Settings | Order Management.\n\n2. Insufficient “Containers” for all inventory items within Sales Order(s).\n\n3. FedEx & UPS not configured to generate labels & tracking #s.');
                }
            });
        }
    </script>
    <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server" ClientIDMode="Static">
    Mass Sales Fulfillment Rules&nbsp;<a href="#" onclick="return OpenHelpPopUp('opportunity/frmSalesFulfillmentWorkflowSettings.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Label ID="litMessage" EnableViewState="false" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <telerik:RadWindowManager ID="RadWindowManager1" runat="server">
        <ConfirmTemplate>
            <div class="rwDialogPopup radconfirm">
                <div class="rwDialogText">
                    {1}
                </div>
                <div class="rwDialogButtons">
                    <a class="rwPopupButton" onclick="$find('{0}').close(true); return false;"><span class="rwOuterSpan"><span class="rwInnerSpan">Yes</span></span></a>
                    <a class="rwPopupButton" onclick="$find('{0}').close(false); return false;"><span class="rwOuterSpan"><span class="rwInnerSpan">No</span></span></a>
                </div>
            </div>
        </ConfirmTemplate>
    </telerik:RadWindowManager>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:CheckBox ID="chkActivate" runat="server" Text="Activate" />
                <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <ol class="form-inline">
                <%--Rule is removed now--%>
               <%-- <li style="padding-bottom: 15px; font-weight: bold">&nbsp;&nbsp;<asp:CheckBox ID="chkActivateRule1" runat="server" />&nbsp;&nbsp;Create an Invoice (BizDoc)&nbsp;<asp:DropDownList ID="ddlRule1BizDoc" CssClass="form-control" runat="server"></asp:DropDownList>&nbsp;&nbsp;<asp:RadioButton ID="rdbOrderCreated" GroupName="Rule1" runat="server" Checked="true" />&nbsp;WHEN Sales Order is created&nbsp;&nbsp;<asp:RadioButton ID="rdbOrderStatusChange" GroupName="Rule1" runat="server" />&nbsp;WHEN Sales Order Status changes to&nbsp;<asp:DropDownList ID="ddlRule1OrderStatus" runat="server" CssClass="form-control"></asp:DropDownList>
                </li>--%>
                <li style="padding-bottom: 15px; font-weight: bold">&nbsp;&nbsp;<asp:CheckBox ID="chkActivateRule2" runat="server" />&nbsp;&nbsp;Charge Customer’s credit card type / card # on file WHEN Sales Order Status changes to&nbsp;<asp:DropDownList ID="ddlRule2OrderStatus" runat="server" CssClass="form-control"></asp:DropDownList>&nbsp;&nbsp;If charge goes through, change Sales Order Status to&nbsp;<asp:DropDownList ID="ddlRule2SuccessOrderStatus" runat="server" CssClass="form-control"></asp:DropDownList>&nbsp;and if it fails change it to&nbsp;<asp:DropDownList ID="ddlRule2FailOrderStatus" runat="server" CssClass="form-control"></asp:DropDownList>
                </li>
                <li style="padding-bottom: 15px; font-weight: bold">&nbsp;&nbsp;<asp:CheckBox ID="chkActivateRule3" runat="server" />&nbsp;&nbsp;Create shipping label(s) / tracking number(s) WHEN Sales Order Status changes to&nbsp;<asp:DropDownList ID="ddlRule3OrderStatus" runat="server" CssClass="form-control"></asp:DropDownList>&nbsp;&nbsp;If unable to add change status to&nbsp;<asp:DropDownList ID="ddlRule3FailOrderStatus" runat="server" CssClass="form-control"></asp:DropDownList>
                </li>
                <li style="padding-bottom: 15px; font-weight: bold">&nbsp;&nbsp;<asp:CheckBox ID="chkActivateRule5" runat="server" />&nbsp;&nbsp;After selecting the “Print Shipping Label” from the “Action” drop-down, change Sales Order status to&nbsp;<asp:DropDownList ID="ddlRule5OrderStatus" runat="server" CssClass="form-control"></asp:DropDownList>
                </li><%--For Database this is rule 5--%>
                <li style="padding-bottom: 15px; font-weight: bold">&nbsp;&nbsp;<asp:CheckBox ID="chkActivateRule6" runat="server" />&nbsp;&nbsp;After selecting the “Print Packing Slip” from the “Action” drop-down, change Sales Order status to&nbsp;<asp:DropDownList ID="ddlRule6OrderStatus" runat="server" CssClass="form-control"></asp:DropDownList>
                </li><%--For Database this is rule 6--%>
                <li style="padding-bottom: 15px; font-weight: bold">&nbsp;&nbsp;<asp:CheckBox ID="chkActivateRule4" runat="server" />&nbsp;&nbsp;<span style="color: red">* </span>Release item(s) allocation and close the Sales Order WHEN Sales Order Status changes to&nbsp;<asp:DropDownList ID="ddlRule4OrderStatus" runat="server" CssClass="form-control"></asp:DropDownList>&nbsp;&nbsp;If unable to close the Sales Order, change status to&nbsp;<asp:DropDownList ID="ddlRule4FailOrderStatus" runat="server" CssClass="form-control"></asp:DropDownList>
                </li>
            </ol>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="box box-default box-solid">
                <div class="box-header with-border">
                    <h3 class="box-title">Customize Order Status Colors</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-inline padbottom10">
                        <div class="form-group">
                            <label>Order Status:</label>
                            <asp:DropDownList ID="ddlColorOrderStatus" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <label>Color:</label>
                            <telerik:RadComboBox runat="server" ID="ddlColorScheme" ClientIDMode="Static">
                                <Items>
                                    <telerik:RadComboBoxItem Value="0" Text="-- Select --" />
                                    <telerik:RadComboBoxItem Value="ForeColor1" Text="Color1" CssClass="ForeColor1" />
                                    <telerik:RadComboBoxItem Value="ForeColor2" Text="Color2" CssClass="ForeColor2" />
                                    <telerik:RadComboBoxItem Value="ForeColor3" Text="Color3" CssClass="ForeColor3" />
                                    <telerik:RadComboBoxItem Value="ForeColor4" Text="Color4" CssClass="ForeColor4" />
                                    <telerik:RadComboBoxItem Value="ForeColor5" Text="Color5" CssClass="ForeColor5" />
                                    <telerik:RadComboBoxItem Value="ForeColor6" Text="Color6" CssClass="ForeColor6" />
                                    <telerik:RadComboBoxItem Value="ForeColor7" Text="Color7" CssClass="ForeColor7" />
                                </Items>
                            </telerik:RadComboBox>
                        </div>
                        <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-primary" />
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <asp:GridView ID="gvColorScheme" runat="server" EnableViewState="true" AutoGenerateColumns="false" CssClass="table table-bordered table-striped" Width="100%" UseAccessibleHeader="true" ShowHeaderWhenEmpty="true" DataKeyNames="numFieldColorSchemeID">
                                    <Columns>
                                        <asp:BoundField DataField="vcFieldValue" HeaderText="BizDoc Status" />
                                        <asp:TemplateField ItemStyle-Width="25">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="LinkButton1" runat="server" CssClass="btn btn-danger btn-xs" CommandName="Delete" CommandArgument='<%# Eval("numFieldColorSchemeID") %>'><i class="fa fa-trash"></i></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
    </div>
    <p>
        <span style="color: red">* </span>Closing the Sales Order will release inventory from allocation, add an Invoice to the order (if all items are not already invoiced), and remove the Fulfillment Order from within the Sales Fulfillment Queue. If any inventory item within a Sales Order has back-order, it will not be eligible for closing. 
    </p>
</asp:Content>