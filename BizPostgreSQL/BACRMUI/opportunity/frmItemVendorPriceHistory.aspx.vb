﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item

Public Class frmItemVendorPriceHistory
    Inherits BACRMPage

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then

                If CCommon.ToLong(GetQueryStringVal("numItemCode")) > 0 AndAlso CCommon.ToLong(GetQueryStringVal("numVendorID")) > 0 Then
                    BindGrid()
                Else
                    lblException.Text = "Item or Vendor is not selected."
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(CCommon.ToString(ex))
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub BindGrid()
        Try
            Dim objItem As New CItems
            objItem.DomainID = CCommon.ToLong(Session("DomainID"))
            objItem.ItemCode = CCommon.ToLong(GetQueryStringVal("numItemCode"))
            objItem.VendorID = CCommon.ToLong(GetQueryStringVal("numVendorID"))
            objItem.ClientZoneOffsetTime = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            gvVendorPrice.DataSource = objItem.GetItemVendorPriceHistory()
            gvVendorPrice.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

#End Region

End Class