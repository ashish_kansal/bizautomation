﻿Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Opportunities

    Public Class frmPOCostOptimizationAndMerge
        Inherits BACRMPage

#Region "Page Events"
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not Page.IsPostBack Then
                    GetUserRightsForPage(46, 1)
                    hdnMSWarehouseID.Value = CCommon.ToLong(Session("DefaultWarehouse"))
                    hdnMSCurrencyID.Value = CCommon.ToString(Session("Currency"))
                    hdnDecimalPoints.Value = CCommon.ToShort(Session("DecimalPoints"))
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DirectCast(Page.Master, BizMaster).DisplayError(ex.Message)
            End Try
        End Sub
#End Region

    End Class

End Namespace