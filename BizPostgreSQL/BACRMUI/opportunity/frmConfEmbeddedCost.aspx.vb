﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports Telerik.Web.UI
Partial Public Class frmConfEmbeddedCost
    Inherits BACRMPage
    
    Dim dtChartAcntDetails As New DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblMessage.Text = ""
            If Not IsPostBack Then
                
                objCommon.sb_FillComboFromDBwithSel(ddlCostCategory, 198, Session("DomainID"))
                'Dim dt As DataTable = objCommon.GetMasterListItems(36, Session("DomainID"))
                'chkClassification.DataTextField = "vcData"
                'chkClassification.DataValueField = "numListItemID"
                'chkClassification.DataSource = dt
                'chkClassification.DataBind()
                'BindClass()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Dim dtCostCenter, dtPaymentMethod As DataTable
    Sub BindGrid()
        Try
            Dim objOpp As New MOpportunity
            objOpp.DomainID = Session("DomainID")
            objOpp.CostCategoryID = ddlCostCategory.SelectedValue
            Dim dtData As DataTable = objOpp.GetEmbeddedCostDefault()
            dtCostCenter = objCommon.GetMasterListItems(202, Session("DomainID"))
            dtPaymentMethod = objCommon.GetMasterListItems(31, Session("DomainID"))

            CreateEmptyRows(dtData)

            gvReturns.DataSource = dtData
            gvReturns.DataBind()

            Dim i As Int16 = 0
            For Each dr As DataRow In dtData.Rows
                If CCommon.ToLong(dr("numEmbeddedCostID")) > 0 Then
                    If Not CType(gvReturns.Rows(i).FindControl("ddlCostCenter"), DropDownList) Is Nothing Then
                        If Not CType(gvReturns.Rows(i).FindControl("ddlCostCenter"), DropDownList).Items.FindByValue(dr("numCostCenterID").ToString) Is Nothing Then
                            CType(gvReturns.Rows(i).FindControl("ddlCostCenter"), DropDownList).Items.FindByValue(dr("numCostCenterID").ToString).Selected = True
                        End If
                    End If
                    If Not CType(gvReturns.Rows(i).FindControl("ddlAccount"), DropDownList) Is Nothing Then
                        If Not CType(gvReturns.Rows(i).FindControl("ddlAccount"), DropDownList).Items.FindByValue(dr("numAccountID").ToString) Is Nothing Then
                            CType(gvReturns.Rows(i).FindControl("ddlAccount"), DropDownList).Items.FindByValue(dr("numAccountID").ToString).Selected = True
                        End If
                    End If
                    If Not CType(gvReturns.Rows(i).FindControl("ddlPaymentMethod"), DropDownList) Is Nothing Then
                        If Not CType(gvReturns.Rows(i).FindControl("ddlPaymentMethod"), DropDownList).Items.FindByValue(dr("numPaymentMethod").ToString) Is Nothing Then
                            CType(gvReturns.Rows(i).FindControl("ddlPaymentMethod"), DropDownList).Items.FindByValue(dr("numPaymentMethod").ToString).Selected = True
                        End If
                    End If
                    If Not CType(gvReturns.Rows(i).FindControl("radCmbCompany"), RadComboBox) Is Nothing Then
                        CType(gvReturns.Rows(i).FindControl("radCmbCompany"), RadComboBox).Text = dr("vcCompanyName").ToString
                        CType(gvReturns.Rows(i).FindControl("radCmbCompany"), RadComboBox).SelectedValue = dr("numDivisionID").ToString
                    End If
                End If
                i = i + 1
            Next

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub gvReturns_OnRowDataBound(ByVal o As Object, ByVal e As GridViewRowEventArgs) Handles gvReturns.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim ddlPaymentMethod As DropDownList = CType(e.Row.FindControl("ddlPaymentMethod"), DropDownList)
                If Not dtPaymentMethod Is Nothing Then
                    ddlPaymentMethod.DataSource = dtPaymentMethod
                    ddlPaymentMethod.DataTextField = "vcData"
                    ddlPaymentMethod.DataValueField = "numListItemID"
                    ddlPaymentMethod.DataBind()
                    ddlPaymentMethod.Items.Insert(0, "--Select One--")
                    ddlPaymentMethod.Items.FindByText("--Select One--").Value = "0"
                End If

                Dim ddlAccount As DropDownList = CType(e.Row.FindControl("ddlAccount"), DropDownList)
                BindCOA(ddlAccount)

                Dim ddlCostCenter As DropDownList = CType(e.Row.FindControl("ddlCostCenter"), DropDownList)
                If Not dtCostCenter Is Nothing Then
                    ddlCostCenter.DataSource = dtCostCenter
                    ddlCostCenter.DataTextField = "vcData"
                    ddlCostCenter.DataValueField = "numListItemID"
                    ddlCostCenter.DataBind()
                    ddlCostCenter.Items.Insert(0, "--Select One--")
                    ddlCostCenter.Items.FindByText("--Select One--").Value = "0"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Dim item As ListItem
    Private Sub BindCOA(ByRef ddlAccount As DropDownList)
        Try
            If dtChartAcntDetails.Rows.Count = 0 Then
                Dim objCOA As New ChartOfAccounting
                objCOA.DomainId = Session("DomainId")
                objCOA.AccountCode = "01020101"
                dtChartAcntDetails = objCOA.GetParentCategory()
            End If
            For Each dr As DataRow In dtChartAcntDetails.Rows
                item = New ListItem()
                item.Text = dr("vcAccountName")
                item.Value = dr("numAccountID")
                item.Attributes("OptionGroup") = dr("vcAccountType")
                ddlAccount.Items.Add(item)
            Next
            ddlAccount.Items.Insert(0, New ListItem("--Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlCostCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCostCategory.SelectedIndexChanged
        Try
            If ddlCostCategory.SelectedValue > 0 Then
                BindGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub CreateEmptyRows(ByRef dt As DataTable)
        Try
            If dt Is Nothing Then dt = New DataTable
            If dt.Rows.Count = 0 And dt.Columns.Count = 0 Then
                dt.Columns.Add("numEmbeddedCostID")
                dt.Columns.Add("numCostCatID")
                dt.Columns.Add("numCostCenterID")
                dt.Columns.Add("numAccountID")
                dt.Columns.Add("numDivisionID")
                dt.Columns.Add("numPaymentMethod")
                dt.Columns.Add("vcCompanyName")
            End If
            Dim OriginalRowCount As Integer = dt.Rows.Count
            Dim dr As DataRow
            For i As Integer = 0 To 9 - OriginalRowCount
                dr = dt.NewRow
                dr("numEmbeddedCostID") = 0
                dr("numCostCatID") = ddlCostCategory.SelectedValue
                dr("numCostCenterID") = 0
                dr("numAccountID") = 0
                dr("numDivisionID") = 0
                dr("numPaymentMethod") = 0
                dr("vcCompanyName") = ""
                dt.Rows.Add(dr)
            Next

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            Dim objOpp As New MOpportunity
            objOpp.DomainID = Session("DomainID")
            objOpp.UserCntID = Session("UserContactID")
            objOpp.CostCategoryID = ddlCostCategory.SelectedValue
            objOpp.strItems = GetItems()
            objOpp.ManageEmbeddedCostDefaults()
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Function GetItems() As String
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            dt.Columns.Add("numCostCenterID")
            dt.Columns.Add("numAccountID")
            dt.Columns.Add("numDivisionID")
            dt.Columns.Add("numPaymentMethod")

            Dim dr As DataRow
            Dim objOppBizDocs As New OppBizDocs
            Dim lngVendorAPAccountID As Long
            For Each gvr As GridViewRow In gvReturns.Rows
                Dim ddlCostCenter As DropDownList = CType(gvr.FindControl("ddlCostCenter"), DropDownList)
                Dim ddlAccount As DropDownList = CType(gvr.FindControl("ddlAccount"), DropDownList)
                Dim ddlPaymentMethod As DropDownList = CType(gvr.FindControl("ddlPaymentMethod"), DropDownList)
                Dim radCmbCompany As RadComboBox = CType(gvr.FindControl("radCmbCompany"), RadComboBox)

                lngVendorAPAccountID = objOppBizDocs.ValidateCustomerAR_APAccounts("AP", Session("DomainID"), CCommon.ToLong(radCmbCompany.SelectedValue))
                If lngVendorAPAccountID = 0 Then
                    lblMessage.Text = "Please Set AP Relationship for vendor '" + radCmbCompany.Text.Trim + "' from ""Administration->Global Settings->Accounting->Accounts for RelationShip"" To Save"
                    Exit Function
                End If
                If CCommon.ToLong(ddlCostCenter.SelectedValue) > 0 And CCommon.ToLong(ddlAccount.SelectedValue) > 0 And CCommon.ToLong(ddlPaymentMethod.SelectedValue) > 0 And CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                    dr = dt.NewRow()
                    dr("numCostCenterID") = CCommon.ToLong(ddlCostCenter.SelectedValue)
                    dr("numAccountID") = CCommon.ToLong(ddlAccount.SelectedValue)
                    dr("numDivisionID") = CCommon.ToLong(radCmbCompany.SelectedValue)
                    dr("numPaymentMethod") = CCommon.ToLong(ddlPaymentMethod.SelectedValue)
                    dt.Rows.Add(dr)
                End If
            Next
            ds.Tables.Add(dt)
            Return ds.GetXml()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    'Private Sub btnSaveClass_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClass.Click
    '    Try
    '        Dim strSlected As String = ""
    '        For Each chkListItem As ListItem In chkClassification.Items
    '            If chkListItem.Selected = True Then
    '                strSlected = strSlected + chkListItem.Value + ","
    '            End If
    '        Next
    '        Dim objOpp As New MOpportunity
    '        objOpp.DomainID = Session("DomainID")
    '        objOpp.strItems = strSlected.TrimEnd(",")
    '        objOpp.bytemode = 0
    '        objOpp.ManageEmbeddedCostClass()

    '        BindClass()


    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub
    'Private Sub BindClass()
    '    Try
    '        Dim objOpp As New MOpportunity
    '        objOpp.DomainID = Session("DomainID")
    '        Dim dt As DataTable = objOpp.GetEmbeddedCostClass()
    '        For Each row As DataRow In dt.Rows
    '            If Not chkClassification.Items.FindByValue(row("numItemClassification")) Is Nothing Then
    '                chkClassification.Items.FindByValue(row("numItemClassification")).Selected = True
    '            End If
    '        Next
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub
End Class
