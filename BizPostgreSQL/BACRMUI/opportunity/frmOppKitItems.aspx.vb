﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common

Public Class frmOppKitItems
    Inherits BACRMPage

    Dim lngOppId As Long
    Dim lngOppItemCode As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngOppId = GetQueryStringVal("OppID")
            lngOppItemCode = GetQueryStringVal("OppItemCode")

            If Not IsPostBack Then
                If lngOppId > 0 AndAlso lngOppItemCode > 0 Then
                    BindDatagrid()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindDatagrid()
        Try
            Dim objOpportunity As New MOpportunity
            objOpportunity.OpportunityId = lngOppId
            objOpportunity.OppItemCode = lngOppItemCode

            gvOppKitItems.DataSource = objOpportunity.GetOppKitItemsList
            gvOppKitItems.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class