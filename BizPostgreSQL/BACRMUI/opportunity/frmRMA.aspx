﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmRMA.aspx.vb" Inherits=".frmRMA" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>RMA</title>
    <script language="javascript" type="text/javascript">
        function PrintIt() {
            document.getElementById('tblButtons').style.display = 'none';
            document.getElementById('tblComment').style.display = 'none';
            window.print();
            return false;
        }
        function UpdateComment() {
            document.getElementById('lblComments').innerHTML = document.getElementById('txtComments').value;
            return false;
        }
        function Close() {
            window.close();
        }
        function openeditAddress(a, b) {
            window.open('../opportunity/frmEditOppAddress.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&AddType=' + a + '&OppID=' + b, '', 'toolbar=no,titlebar=no,left=100,top=350,width=300,height=250,scrollbars=yes,resizable=yes')
            return false;
        }
        function GrabHTML() {
            document.getElementById('hdnBizDocHTML').value = document.getElementById("pnlBizDoc").innerHTML;
            return true;
        }
    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
    <br>
    <asp:Table ID="Table1" BorderWidth="1" runat="server" Width="100%" BorderColor="black"
        GridLines="None">
        <asp:TableRow>
            <asp:TableCell>
                <asp:Panel ID="pnlBizDoc" runat="server">
                    <table width="100%" border="0" cellpadding="0">
                        <tr>
                            <td valign="top" align="left">
                                <asp:Image ID="imgLogo" runat="server"></asp:Image>
                            </td>
                            <td align="right" colspan="4" nowrap>
                                <asp:Label ID="lblBizDoc" ForeColor="#c0c0c0" Font-Name="Arial black" runat="server"
                                    Font-Size="26" Font-Bold="True"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                    <table width="100%" border="0" cellpadding="0">
                        <tr class="normal1">
                            <td colspan="5">
                                <table height="100%" width="100%">
                                    <tr bgcolor="#ACA899">
                                        <td class="normal1" colspan="2">
                                            <font color="white">Sales Order ID</font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="normal1">
                                            <asp:Label ID="lblOppName" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr bgcolor="#ACA899">
                                        <td class="normal1">
                                            <a id="hplBillto" runat="server" class="hyperlink"><font color="white">Bill To</font></a>
                                        </td>
                                        <td class="normal1">
                                            <a id="hplShipTo" runat="server" class="hyperlink"><font color="white">Ship To</font></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="normal1">
                                            <asp:Label ID="lblBillTo" runat="server"></asp:Label>
                                        </td>
                                        <td class="normal1">
                                            <asp:Label ID="lblShipTo" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr class="normal1" bgcolor="#ACA899">
                            <td class="normal1" colspan="5">
                                <font color="white">Comments</font>
                            </td>
                        </tr>
                        <tr class="normal1">
                            <td colspan="5">
                                <asp:Label ID="lblComments" runat="server" CssClass="signup" Width="630"></asp:Label>
                            </td>
                        </tr>
                        <tr class="normal1">
                            <td colspan="5">
                                <asp:DataGrid ID="dgBizDocs" runat="server" ForeColor="" Font-Size="10px" Width="100%"
                                    BorderStyle="None"  BackColor="White" GridLines="Vertical"
                                    HorizontalAlign="Center" AutoGenerateColumns="False">
                                    <AlternatingItemStyle Font-Size="8pt" Font-Names="tahoma,verdana,arial,helvetica"
                                        BackColor="White"></AlternatingItemStyle>
                                    <ItemStyle Font-Size="8pt" Font-Names="tahoma,verdana,arial,helvetica" HorizontalAlign="Center"
                                        BorderStyle="None"  VerticalAlign="Middle" BackColor="#e0dfe3">
                                    </ItemStyle>
                                    <HeaderStyle Font-Size="8pt" Font-Names="tahoma,verdana,arial,helvetica" Wrap="False"
                                        HorizontalAlign="Center" Height="20px" ForeColor="White" BorderColor="Black"
                                        VerticalAlign="Middle" BackColor="#9d9da1"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn DataField="Item" HeaderText="Item"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="vcModelID" HeaderText="Model ID"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Desc" HeaderText="Desc"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Unit" DataFormatString="{0:#,##0}" HeaderText="Units">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="monPrice" DataFormatString="{0:#,##0}" HeaderText="Price">
                                        </asp:BoundColumn>
                                        <asp:BoundColumn DataField="ReturnAmount" DataFormatString="{0:#,##0}" HeaderText="Amount">
                                        </asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1" colspan="5" align="right">
                                &nbsp;&nbsp;&nbsp; SubTotal:<asp:Label ID="lblSubTotal" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <table id="tblButtons" width="100%">
                    <tr>
                        <td align="right" class="normal1" colspan="2">
                            <asp:Button ID="btnSendEmail" Width="100" runat="server" Text="Send as Email" CssClass="button" OnClientClick="GrabHTML();">
                            </asp:Button>&nbsp;
                            <asp:Button ID="btnPrint" runat="server" Text="Print" Width="50" CssClass="button">
                            </asp:Button>&nbsp;
                            <asp:Button ID="btnClose" runat="server" Text="Close" Width="50" CssClass="button">
                            </asp:Button>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <br />
    <table cellpadding="0" cellspacing="0" width="100%" border="0" id="tblComment">
        <tr>
            <td class="normal1" align="right">
                Comments&nbsp;
            </td>
            <td width="400px">
                <asp:TextBox ID="txtComments" Height="40" TextMode="MultiLine" runat="server" CssClass="signup"
                    Width="400"></asp:TextBox>
            </td>
            <td align="left">
                &nbsp;
                <asp:Button ID="btnSave" runat="server" Width="50" Text="Update" CssClass="button"
                    OnClientClick="return UpdateComment();"></asp:Button>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnContactId" runat="server" />
    <asp:HiddenField ID="hdnSendMailTo" runat="server" />
    <asp:HiddenField runat="server" ID="hdnBizDocHTML" />
    </form>
</body>
</html>
