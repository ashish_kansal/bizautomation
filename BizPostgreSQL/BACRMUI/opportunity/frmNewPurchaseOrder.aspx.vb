﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.TimeAndExpense

Partial Public Class frmNewPurchaseOrder
    Inherits BACRMPage
    Dim objItems As CItems
    Dim strValues As String
    Dim dtOppAtributes As DataTable
    Dim dsTemp As DataSet
    Dim lngDivId, lngCntID, lngOppId, OppBizDocID, JournalId As Long
    Dim dtOppBiDocItems As DataTable
    Dim strMessage As String
    Dim ResponseMessage As String
    Dim boolPurchased As Boolean = False
    Dim boolFlag As Boolean = True
    Dim IsFromCreateBizDoc As Boolean = False
    Dim IsFromSaveAndOpenOrderDetails As Boolean = False
    Dim IsFromSaveAndNew As Boolean = False
    Dim IsStockTransfer As Boolean = False

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            If Not IsPostBack Then
                radCmbCompany.Focus()
            End If
           
            CCommon.UpdateItemRadComboValues("2", radCmbCompany.SelectedValue)
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblException.Text = ""
            'StockTransfer
            If GetQueryStringVal("ST") = "1" Then IsStockTransfer = True

            order1.OppType = 2
            order1._PageType = order1.PageType.Purchase
            order1.IsStockTransfer = IsStockTransfer
            CType(order1.FindControl("chkDropShip"), CheckBox).Visible = False

            If Me.IsPostBack Then
                Dim eventTarget As String = If((Me.Request("__EVENTTARGET") Is Nothing), String.Empty, Me.Request("__EVENTTARGET"))

                If eventTarget = "btnReloadAddress" Then
                    FillExistingAddress(CType(order1.FindControl("txtDivID"), TextBox).Text)
                End If
            End If

            If Not IsPostBack Then
                PersistTable.Load(strParam:="frmNewPurchaseOrder", isMasterPage:=True)
                If PersistTable.Count > 0 Then
                    CType(order1.FindControl("hdnSearchPageName"), HiddenField).Value = "frmNewPurchaseOrder"
                    CType(order1.FindControl("hdnSearchType"), HiddenField).Value = CCommon.ToString(PersistTable("frmNewPurchaseOrder"))
                End If

                Me.FillExistingAddress(Session("UserDivisionID")) 'Bug ID 432 'Me.FillExistingAddress(txtDivID.Text)
                SetAssignedToEnabled(0, 0)

                BindMultiCurrency()
                BindSalesTemplate()
                FillClass()
                BindShipVia()
                rdcmbShippingService.Items.Clear()
                rdcmbShippingService.Items.Add(New Telerik.Web.UI.RadComboBoxItem("-- Select One --", "0"))
                OppBizDocs.LoadServiceTypes(CCommon.ToLong(rdcmbShipVia.SelectedValue), rdcmbShippingService)

                objCommon.UserCntID = Session("UserContactID")
                If CCommon.ToLong(Session("UserContactID")) > 0 AndAlso CCommon.ToLong(Session("UserID")) = 0 Then
                    objCommon.DivisionID = CCommon.ToLong(Session("UserDivisionID"))
                    objCommon.charModule = "D"
                    radCmbCompany.Enabled = False
                    CType(order1.FindControl("txtprice"), TextBox).ReadOnly = True
                    CType(order1.FindControl("txtItemDiscount"), TextBox).ReadOnly = True

                    objCommon.GetCompanySpecificValues1()
                    Dim strCompany, strContactID As String
                    strContactID = objCommon.ContactID
                    strCompany = objCommon.GetCompanyName
                    radCmbCompany.Text = strCompany
                    radCmbCompany.SelectedValue = objCommon.DivisionID
                    CType(order1.FindControl("txtDivID"), TextBox).Text = radCmbCompany.SelectedValue
                    Try
                        If Session("PopulateUserCriteria") = 1 Then
                            objCommon.DivisionID = radCmbCompany.SelectedValue
                            objCommon.charModule = "D"
                            objCommon.GetCompanySpecificValues1()
                            objCommon.sb_FillConEmpFromTerritories(ddlAssignTo, Session("DomainID"), 0, 0, objCommon.TerittoryID)
                        ElseIf Session("PopulateUserCriteria") = 2 Then
                            objCommon.sb_FillConEmpFromDBUTeam(ddlAssignTo, Session("DomainID"), Session("UserContactID"))
                        Else : objCommon.sb_FillConEmpFromDBSel(ddlAssignTo, Session("DomainID"), 0, 0)
                        End If

                    Catch ex As Exception

                    End Try

                    radCmbCompany_SelectedIndexChanged() 'Added by chintan- Reason:remaining credit wasn't showing

                    If Not ddlContact.Items.FindByValue(strContactID) Is Nothing Then
                        ddlContact.ClearSelection()
                        ddlContact.Items.FindByValue(strContactID).Selected = True
                    End If
                    If Session("PopulateUserCriteria") = 1 Then
                        objCommon.sb_FillConEmpFromTerritories(ddlNewAssignTo, Session("DomainID"), 0, 0, objCommon.TerittoryID)
                    ElseIf Session("PopulateUserCriteria") = 2 Then
                        objCommon.sb_FillConEmpFromDBUTeam(ddlNewAssignTo, Session("DomainID"), Session("UserContactID"))
                    Else : objCommon.sb_FillConEmpFromDBSel(ddlNewAssignTo, Session("DomainID"), 0, 0)
                    End If
                ElseIf GetQueryStringVal("uihTR") <> "" Or GetQueryStringVal("fghTY") <> "" Or GetQueryStringVal("rtyWR") <> "" Or GetQueryStringVal("tyrCV") <> "" Or GetQueryStringVal("pluYR") <> "" Then

                    If GetQueryStringVal("uihTR") <> "" Then
                        objCommon.ContactID = GetQueryStringVal("uihTR")
                        objCommon.charModule = "C"
                    ElseIf GetQueryStringVal("rtyWR") <> "" Then
                        objCommon.DivisionID = GetQueryStringVal("rtyWR")
                        objCommon.charModule = "D"
                    ElseIf GetQueryStringVal("tyrCV") <> "" Then
                        objCommon.ProID = GetQueryStringVal("tyrCV")
                        objCommon.charModule = "P"
                    ElseIf GetQueryStringVal("pluYR") <> "" Then
                        objCommon.OppID = GetQueryStringVal("pluYR")
                        objCommon.charModule = "O"
                    ElseIf GetQueryStringVal("fghTY") <> "" Then
                        objCommon.CaseID = GetQueryStringVal("fghTY")
                        objCommon.charModule = "S"
                    End If

                    objCommon.GetCompanySpecificValues1()
                    Dim strCompany, strContactID As String
                    strContactID = objCommon.ContactID
                    strCompany = objCommon.GetCompanyName
                    radCmbCompany.Text = strCompany
                    radCmbCompany.SelectedValue = objCommon.DivisionID
                    CType(order1.FindControl("txtDivID"), TextBox).Text = radCmbCompany.SelectedValue
                    Try
                        If Session("PopulateUserCriteria") = 1 Then
                            objCommon.DivisionID = radCmbCompany.SelectedValue
                            objCommon.charModule = "D"
                            objCommon.GetCompanySpecificValues1()
                            objCommon.sb_FillConEmpFromTerritories(ddlAssignTo, Session("DomainID"), 0, 0, objCommon.TerittoryID)
                        ElseIf Session("PopulateUserCriteria") = 2 Then
                            objCommon.sb_FillConEmpFromDBUTeam(ddlAssignTo, Session("DomainID"), Session("UserContactID"))
                        Else : objCommon.sb_FillConEmpFromDBSel(ddlAssignTo, Session("DomainID"), 0, 0)
                        End If

                    Catch ex As Exception

                    End Try

                    radCmbCompany_SelectedIndexChanged() 'Added by chintan- Reason:remaining credit wasn't showing

                    If Not ddlContact.Items.FindByValue(strContactID) Is Nothing Then
                        ddlContact.ClearSelection()
                        ddlContact.Items.FindByValue(strContactID).Selected = True
                    End If
                    If Session("PopulateUserCriteria") = 1 Then
                        objCommon.sb_FillConEmpFromTerritories(ddlNewAssignTo, Session("DomainID"), 0, 0, objCommon.TerittoryID)
                    ElseIf Session("PopulateUserCriteria") = 2 Then
                        objCommon.sb_FillConEmpFromDBUTeam(ddlNewAssignTo, Session("DomainID"), Session("UserContactID"))
                    Else : objCommon.sb_FillConEmpFromDBSel(ddlNewAssignTo, Session("DomainID"), 0, 0)
                    End If
                End If
                If GetQueryStringVal("StageName") <> "" Then
                    lblTitle.Text = "Purchase Order for Stage (" & GetQueryStringVal("StageName") & ")"
                End If

                If IsStockTransfer Then lblTitle.Text = "Stock Transfer"
            End If

            If chkShipDifferent.Checked = True Then
                pnlShipping.Visible = True
            Else : pnlShipping.Visible = False
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

    Private Sub FillExistingAddress(ByVal sDivisionId As String)
        Try
            lblBillTo1.Text = ""
            lblShipTo1.Text = ""
            CType(order1.FindControl("hdnBillAddressID"), HiddenField).Value = 0
            CType(order1.FindControl("hdnShipAddressID"), HiddenField).Value = 0

            If Not IsNothing(sDivisionId) Then
                Dim oCOpportunities As New COpportunities
                oCOpportunities.DomainID = CType((Session("DomainID")), Long)
                Dim dtAddress As DataTable = oCOpportunities.GetExistingAddress(CInt(Session("UserID")), CInt(sDivisionId))
                lblBillTo1.Text = CCommon.ToString(dtAddress(0)("vcBillAddress"))
                CType(order1.FindControl("hdnBillAddressID"), HiddenField).Value = CCommon.ToLong(dtAddress(0)("numBillAddress"))
                CType(order1.FindControl("hdnShipAddressID"), HiddenField).Value = CCommon.ToLong(dtAddress(0)("numShipAddress"))

                If CCommon.ToLong(Session("DefaultWarehouse")) > 0 Then
                    Dim objItem As New CItems
                    Dim dtTable As DataTable
                    objItem.WarehouseID = CCommon.ToLong(Session("DefaultWarehouse"))
                    objItem.DomainID = Session("DomainID")
                    dtTable = objItem.GetWareHouses

                    If Not dtTable Is Nothing AndAlso dtTable.Rows.Count > 0 AndAlso CCommon.ToLong(dtTable.Rows(0)("numAddressID")) > 0 Then
                        lblShipTo1.Text = CCommon.ToString(dtTable.Rows(0)("vcAddress"))
                        hdnShipToWarehouseID.Value = CCommon.ToLong(Session("DefaultWarehouse"))
                        CType(order1.FindControl("hdnShipAddressID"), HiddenField).Value = CCommon.ToLong(dtTable.Rows(0)("numAddressID"))
                    Else
                        lblShipTo1.Text = CCommon.ToString(dtAddress(0)("vcShipAddress"))
                        hdnShipToWarehouseID.Value = CCommon.ToLong(dtAddress(0)("numWarehouseID"))
                    End If
                Else
                    lblShipTo1.Text = CCommon.ToString(dtAddress(0)("vcShipAddress"))
                    hdnShipToWarehouseID.Value = CCommon.ToLong(dtAddress(0)("numWarehouseID"))
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public ReadOnly Property GetCompanyName() As String
        Get
            radCmbCompany.Text.Trim()
        End Get
    End Property

    Private Sub CreateClone()
        '' Create Clone Item of order.
        Try
            If GetQueryStringVal("IsClone") <> "" Then
                Dim dtItems As DataTable
                Dim objOpportunity As New OppotunitiesIP
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.OpportunityId = CCommon.ToLong(GetQueryStringVal("OppId"))
                objOpportunity.Mode = 2
                dtItems = objOpportunity.GetOrderItems().Tables(0)
                Dim objItems As New CItems
                Dim ds As New DataSet
                Dim drow As DataRow
                Dim i As Integer = 1
                Dim k As Integer
                Dim dtItemTax As DataTable
                objCommon = New CCommon

                If order1.SOItems Is Nothing Then
                    order1.createSet()
                End If

                ds = order1.SOItems
                ds.Tables(0).Clear()
                ds.Tables(1).Clear()
                ds.Tables(2).Clear()
                objCommon = New CCommon
                Dim objItem As New CItems
                Dim strScript As String
                For Each dr As DataRow In dtItems.Rows
                    If Not objItem.ValidateItemAccount(CCommon.ToLong(dr("numItemCode")), Session("DomainID"), order1.OppType) = True Then
                        strScript = "<script language='JavaScript'>"
                        strScript += "alert('Please Set Income,Asset,COGs(Expense) Account for " + dr("vcItemName") + " from Administration->Inventory->Item Details.');</script>"
                        'Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "AccountValidation", "alert('" + msg + "')", True)
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "clientScript", strScript, True)
                        radCmbCompany.SelectedValue = ""
                        radCmbCompany.Text = ""
                        Return
                    End If
                    drow = ds.Tables(0).NewRow()
                    drow("numoppitemtCode") = dr("numoppitemtCode")
                    drow("numItemCode") = dr("numItemCode")
                    drow("numUnitHour") = dr("numUnitHour")
                    drow("monPrice") = String.Format("{0:0.00}", dr("monPrice"))
                    drow("monTotAmount") = String.Format("{0:0.00}", dr("monTotAmount"))
                    drow("numSourceID") = dr("numSourceID")
                    drow("vcItemDesc") = dr("vcItemDesc")
                    drow("numWarehouseID") = dr("numWarehouseID")
                    drow("vcItemName") = dr("vcItemName")
                    drow("Warehouse") = dr("Warehouse")
                    drow("numWarehouseItmsID") = dr("numWarehouseItmsID")
                    drow("ItemType") = dr("vcType")
                    drow("Attributes") = dr("vcAttributes")
                    drow("Op_Flag") = 1 '1 for insertion
                    drow("DropShip") = dr("bitDropShip")
                    drow("bitDiscountType") = dr("bitDiscountType")
                    drow("fltDiscount") = String.Format("{0:0.00}", dr("fltDiscount"))
                    drow("monTotAmtBefDiscount") = String.Format("{0:0.00}", dr("monTotAmtBefDiscount"))
                    drow("numUOM") = dr("numUOMId")
                    drow("vcUOMName") = dr("vcUOMName")
                    drow("UOMConversionFactor") = dr("UOMConversionFactor")
                    drow("bitWorkOrder") = dr("bitWorkOrder")
                    drow("charItemType") = dr("charItemType")

                    drow("numVendorWareHouse") = 0
                    drow("numShipmentMethod") = 0
                    drow("numSOVendorId") = dr("numSOVendorId")
                    drow("numProjectID") = 0
                    drow("numProjectStageID") = 0
                    drow("bitIsAuthBizDoc") = False

                    i += 1
                    ds.Tables(0).Rows.Add(drow)
                Next
                ds.Tables(0).AcceptChanges()
                order1.SOItems = ds

                order1.UpdateDetails()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
        Try
            radCmbCompany_SelectedIndexChanged()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

    Private Sub radCmbCompany_SelectedIndexChanged()
        Try
            If CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then

                If IsStockTransfer Then
                    If Not CCommon.ToLong(Session("UserDivisionID")) = CCommon.ToLong(radCmbCompany.SelectedValue) Then
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Employer comapny must be selected to make stock transfer' );", True)
                        radCmbCompany.SelectedValue = ""
                        radCmbCompany.Text = ""
                        Exit Sub
                    End If
                End If

                Dim objOpp As New OppBizDocs
                If objOpp.ValidateARAP(radCmbCompany.SelectedValue, 0, Session("DomainID")) = False Then
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting->Accounts for RelationShip"" To Save' );", True)
                    radCmbCompany.SelectedValue = ""
                    radCmbCompany.Text = ""
                    Exit Sub
                End If
                FillContact(ddlContact)
                objItems = New CItems
                objItems.DivisionID = radCmbCompany.SelectedValue
                CType(order1.FindControl("txtDivID"), TextBox).Text = radCmbCompany.SelectedValue
                Dim dtTable As DataTable
                dtTable = objItems.GetAmountDue
                Dim dtTableOrder As DataTable
                dtTableOrder = objItems.GetOpenOrderDetails

                Dim strBaseCurrency As String = ""
                If (Session("MultiCurrency") = True) Then
                    strBaseCurrency = Session("Currency") + " "
                End If
                If dtTableOrder.Rows.Count > 0 Then

                    If IsDBNull(dtTableOrder.Rows(0).Item("TotalPOAmount")) Then
                        CType(order1.FindControl("divOpenPO"), HtmlGenericControl).Visible = False
                    Else
                        If CCommon.ToLong(dtTableOrder.Rows(0).Item("TotalPOAmount")) > 0 Then
                            CType(order1.FindControl("lblOpenPOToals"), Label).Text = "$" + String.Format("{0:#,##0.00}", dtTableOrder.Rows(0).Item("TotalPOAmount")) + "," + CCommon.ToString(dtTableOrder.Rows(0).Item("TotalWeight")) + " Lbs," + String.Format("{0:#,##0.00}", dtTableOrder.Rows(0).Item("TotalQty")) + " Qty"
                            CType(order1.FindControl("divOpenPO"), HtmlGenericControl).Visible = True
                        Else
                            CType(order1.FindControl("divOpenPO"), HtmlGenericControl).Visible = False
                        End If
                    End If
                    If IsDBNull(dtTableOrder.Rows(0).Item("Purchaseincentives")) Then
                        CType(order1.FindControl("tablePurchaseIncentiveLabel"), HtmlTable).Visible = False
                    Else
                        If CCommon.ToString(dtTableOrder.Rows(0).Item("Purchaseincentives")) <> "" Then
                            CType(order1.FindControl("lblPurchaseIncentives"), Literal).Text = "<Span>" & HttpUtility.HtmlDecode(dtTableOrder.Rows(0).Item("Purchaseincentives")) & "</span>"
                            CType(order1.FindControl("tablePurchaseIncentiveLabel"), HtmlTable).Visible = True
                        Else
                            CType(order1.FindControl("tablePurchaseIncentiveLabel"), HtmlTable).Visible = False
                        End If
                    End If
                End If
                lblBalanceDue.Text = strBaseCurrency + String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("AmountDuePO"))
                lblRemaningCredit.Text = strBaseCurrency + String.Format("{0:#,##0.00}", If(dtTable.Rows(0).Item("RemainingCredit") < 0, 0, dtTable.Rows(0).Item("RemainingCredit")))
                lblTotalAmtPastDue.Text = strBaseCurrency + String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("AmountPastDuePO"))
                lblCreditBalance.Text = strBaseCurrency + String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("PCreditMemo"))
                lblCreditLimit.Text = strBaseCurrency + String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("CreditLimit"))

                If Not rdcmbShipVia.FindItemByValue(dtTable.Rows(0).Item("intShippingCompany")) Is Nothing Then
                    rdcmbShipVia.SelectedValue = dtTable.Rows(0).Item("intShippingcompany")
                End If

                If Not rdcmbShippingService.FindItemByValue(dtTable.Rows(0).Item("numDefaultShippingServiceID")) Is Nothing Then
                    rdcmbShippingService.FindItemByValue(dtTable.Rows(0).Item("numDefaultShippingServiceID")).Selected = True
                End If

                objCommon = New CCommon

                Try
                    objCommon.DivisionID = radCmbCompany.SelectedValue
                    objCommon.charModule = "D"
                    objCommon.GetCompanySpecificValues1()

                    SetAssignedToEnabled(objCommon.RecordOwner, objCommon.TerittoryID)

                    If objCommon.CurrencyID > 0 Then
                        If Not ddlCurrency.Items.FindByValue(objCommon.CurrencyID) Is Nothing Then
                            ddlCurrency.ClearSelection()
                            ddlCurrency.Items.FindByValue(objCommon.CurrencyID).Selected = True
                        End If
                    Else
                        If Not ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")) Is Nothing Then
                            ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")).Selected = True
                        End If
                    End If

                    If Session("PopulateUserCriteria") = 1 Then
                        objCommon.sb_FillConEmpFromTerritories(ddlAssignTo, Session("DomainID"), 0, 0, objCommon.TerittoryID)
                    ElseIf Session("PopulateUserCriteria") = 2 Then
                        objCommon.sb_FillConEmpFromDBUTeam(ddlAssignTo, Session("DomainID"), Session("UserContactID"))
                    Else : objCommon.sb_FillConEmpFromDBSel(ddlAssignTo, Session("DomainID"), 0, 0)
                    End If

                    If CCommon.ToInteger(Session("DefaultClassType")) = 2 AndAlso Not ddlClass.Items.FindByValue(objCommon.AccountClassID) Is Nothing Then
                        ddlClass.Items.FindByValue(objCommon.AccountClassID).Selected = True
                    End If

                    If CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                        Dim lngOrgAssignedTo As Long
                        objCommon = New CCommon
                        objCommon.DomainID = Session("DomainID")
                        objCommon.Mode = 14
                        objCommon.Str = radCmbCompany.SelectedValue
                        lngOrgAssignedTo = objCommon.GetSingleFieldValue()

                        If ddlAssignTo.Items.FindByValue(lngOrgAssignedTo) IsNot Nothing Then
                            ddlAssignTo.ClearSelection()
                            ddlAssignTo.Items.FindByValue(lngOrgAssignedTo).Selected = True
                        Else
                            ddlAssignTo.SelectedValue = "0"
                        End If
                    End If
                Catch ex As Exception

                End Try

                If ddlPurchaseTemplate.SelectedValue = "0" Then
                    BindSalesTemplate()
                End If
                If IsStockTransfer = False Then LoadVendorWareHouse()

                CCommon.UpdateItemRadComboValues("2", radCmbCompany.SelectedValue)

                CreateClone()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SetAssignedToEnabled(ByVal numRecordOwner As Long, ByVal numTerritoryID As Long)
        Try
            Dim m_aryRightsForAssignedTo() As Integer = GetUserRightsForPage_Other(10, 128)
            If m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 0 Then
                ddlAssignTo.Enabled = False
            ElseIf m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 1 AndAlso CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                Try
                    If numRecordOwner <> Session("UserContactID") Then
                        ddlAssignTo.Enabled = False
                    End If
                Catch ex As Exception
                End Try
            ElseIf m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 2 AndAlso CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                Dim i As Integer
                Dim dtTerritory As DataTable
                dtTerritory = Session("UserTerritory")
                If numTerritoryID = 0 Then
                    ddlAssignTo.Enabled = False
                Else
                    Dim chkDelete As Boolean = False
                    For i = 0 To dtTerritory.Rows.Count - 1
                        If numTerritoryID = dtTerritory.Rows(i).Item("numTerritoryId") Then
                            chkDelete = True
                        End If
                    Next
                    If chkDelete = False Then
                        ddlAssignTo.Enabled = False
                    End If
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Sub LoadVendorWareHouse()
        Try
            Dim dtVendorsWareHouse As DataTable
            objItems.DomainID = Session("DomainID")
            objItems.VendorID = radCmbCompany.SelectedValue
            objItems.byteMode = 2
            dtVendorsWareHouse = objItems.GetVendorsWareHouse

            If dtVendorsWareHouse.Rows.Count > 0 Then
                ddlVendorAddresses.DataSource = dtVendorsWareHouse
                ddlVendorAddresses.DataTextField = "vcFullAddress"
                ddlVendorAddresses.DataValueField = "numAddressID"
                ddlVendorAddresses.DataBind()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function FillContact(ByVal ddlCombo As DropDownList)
        Try
            Dim fillCombo As New COpportunities
            With fillCombo
                .DivisionID = radCmbCompany.SelectedValue
                ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                ddlCombo.DataTextField = "Name"
                ddlCombo.DataValueField = "numcontactId"
                ddlCombo.DataBind()
            End With
            ddlCombo.Items.Insert(0, New ListItem("---Select One---", "0"))
            If ddlCombo.Items.Count >= 2 Then
                ddlCombo.Items(1).Selected = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub BindShipVia()
        Try
            If objCommon Is Nothing Then objCommon = New CCommon
            rdcmbShipVia.DataSource = objCommon.GetMasterListItems(82, Session("DomainID"))
            rdcmbShipVia.DataTextField = "vcData"
            rdcmbShipVia.DataValueField = "numListItemID"
            rdcmbShipVia.DataBind()

            Dim listItem As Telerik.Web.UI.RadComboBoxItem
            If rdcmbShipVia.Items.FindItemByText("Fedex") IsNot Nothing Then
                rdcmbShipVia.Items.Remove(rdcmbShipVia.Items.FindItemByText("Fedex"))
            End If
            If rdcmbShipVia.Items.FindItemByText("UPS") IsNot Nothing Then
                rdcmbShipVia.Items.Remove(rdcmbShipVia.Items.FindItemByText("UPS"))
            End If
            If rdcmbShipVia.Items.FindItemByText("USPS") IsNot Nothing Then
                rdcmbShipVia.Items.Remove(rdcmbShipVia.Items.FindItemByText("USPS"))
            End If

            listItem = New Telerik.Web.UI.RadComboBoxItem
            listItem.Text = "-- Select One --"
            listItem.Value = "0"
            rdcmbShipVia.Items.Insert(0, listItem)

            listItem = New Telerik.Web.UI.RadComboBoxItem
            listItem.Text = "Fedex"
            listItem.Value = "91"
            listItem.ImageUrl = "../images/FedEx.png"
            rdcmbShipVia.Items.Insert(1, listItem)

            listItem = New Telerik.Web.UI.RadComboBoxItem
            listItem.Text = "UPS"
            listItem.Value = "88"
            listItem.ImageUrl = "../images/UPS.png"
            rdcmbShipVia.Items.Insert(2, listItem)

            listItem = New Telerik.Web.UI.RadComboBoxItem
            listItem.Text = "USPS"
            listItem.Value = "90"
            listItem.ImageUrl = "../images/USPS.png"
            rdcmbShipVia.Items.Insert(3, listItem)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub lkbNewCustomer_Click(sender As Object, e As EventArgs) Handles lkbNewCustomer.Click
        Try
            ClearExistingCustomerField()
            trNewCust.Visible = True
            trExistCust.Visible = False

            Dim strBaseCurrency As String = ""
            If (Session("MultiCurrency") = True) Then
                strBaseCurrency = Session("Currency") + " "
            End If

            lblBalanceDue.Text = strBaseCurrency + String.Format("{0:#,##0.00}", 0)
            lblRemaningCredit.Text = strBaseCurrency + String.Format("{0:#,##0.00}", 0)
            lblTotalAmtPastDue.Text = strBaseCurrency + String.Format("{0:#,##0.00}", 0)
            lblCreditBalance.Text = strBaseCurrency + String.Format("{0:#,##0.00}", 0)
            lblCreditLimit.Text = strBaseCurrency + String.Format("{0:#,##0.00}", 0)

            objCommon.sb_FillComboFromDBwithSel(ddlBillCountry, 40, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlShipCountry, 40, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))

            If Not ddlBillCountry.Items.FindByValue(Session("DefCountry")) Is Nothing Then
                ddlBillCountry.Items.FindByValue(Session("DefCountry")).Selected = True
                If ddlBillCountry.SelectedIndex > 0 Then FillState(ddlBillState, ddlBillCountry.SelectedItem.Value, Session("DomainID"))
            End If
            If Not ddlShipCountry.Items.FindByValue(Session("DefCountry")) Is Nothing Then
                ddlShipCountry.Items.FindByValue(Session("DefCountry")).Selected = True
                If ddlShipCountry.SelectedIndex > 0 Then FillState(ddlShipState, ddlShipCountry.SelectedItem.Value, Session("DomainID"))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

    Private Sub lkbExistingCustomer_Click(sender As Object, e As EventArgs) Handles lkbExistingCustomer.Click
        Try
            ClearNewCustomerFields()

            trNewCust.Visible = False
            trExistCust.Visible = True
            radCmbCompany.Focus()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ClearNewCustomerFields()
        Try
            txtFirstName.Text = ""
            txtLastName.Text = ""
            txtPhone.Text = ""
            txtExt.Text = ""
            txtEmail.Text = ""
            txtCompany.Text = ""
            txtBillStreet.Text = ""
            txtBillCity.Text = ""
            If ddlBillState.Items.Count > 0 AndAlso Not ddlBillState.Items.FindByValue("0") Is Nothing Then
                ddlBillState.SelectedIndex = 0
            End If
            If ddlBillCountry.Items.Count > 0 AndAlso Not ddlBillCountry.Items.FindByValue("0") Is Nothing Then
                ddlBillCountry.SelectedIndex = 0
            End If
            If ddlRelationship.Items.Count > 0 AndAlso Not ddlRelationship.Items.FindByValue("0") Is Nothing Then
                ddlRelationship.SelectedIndex = 0
            End If
            If ddlNewAssignTo.Items.Count > 0 AndAlso Not ddlNewAssignTo.Items.FindByValue("0") Is Nothing Then
                ddlNewAssignTo.SelectedIndex = 0
            End If
            If ddlProfile.Items.Count > 0 AndAlso Not ddlProfile.Items.FindByValue("0") Is Nothing Then
                ddlProfile.SelectedIndex = 0
            End If

            txtBillPostal.Text = ""
            chkShipDifferent.Checked = False
            txtShipStreet.Text = ""
            txtShipCity.Text = ""
            If ddlShipState.Items.Count > 0 AndAlso Not ddlShipState.Items.FindByValue("0") Is Nothing Then
                ddlShipState.SelectedIndex = 0
            End If
            If ddlShipCountry.Items.Count > 0 AndAlso Not ddlShipCountry.Items.FindByValue("0") Is Nothing Then
                ddlShipCountry.SelectedIndex = 0
            End If
            txtShipPostal.Text = ""
        Catch ex As Exception

        End Try

    End Sub

    Private Sub ClearExistingCustomerField()
        Try
            radCmbCompany.Text = ""
            If ddlContact.Items.Count > 0 AndAlso Not ddlContact.Items.FindByValue("0") Is Nothing Then
                ddlContact.SelectedIndex = 0
            End If
            If ddlAssignTo.Items.Count > 0 AndAlso Not ddlAssignTo.Items.FindByValue("0") Is Nothing Then
                ddlAssignTo.SelectedIndex = 0
            End If
            If ddlPurchaseTemplate.Items.Count > 0 AndAlso Not ddlPurchaseTemplate.Items.FindByValue("0") Is Nothing Then
                ddlPurchaseTemplate.SelectedIndex = 0
            End If
            ddlVendorAddresses.Items.Clear()
            lblBillTo1.Text = ""
            lblBillTo2.Text = ""
            lblShipTo1.Text = ""
            lblShipTo2.Text = ""
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlBillCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBillCountry.SelectedIndexChanged
        Try
            If ddlBillCountry.SelectedIndex > 0 Then FillState(ddlBillState, ddlBillCountry.SelectedItem.Value, Session("DomainID"))

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

    Private Sub ddlShipCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShipCountry.SelectedIndexChanged
        Try

            If ddlShipCountry.SelectedIndex > 0 Then FillState(ddlShipState, ddlShipCountry.SelectedItem.Value, Session("DomainID"))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub


    Private Sub ddlRelationship_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRelationship.SelectedIndexChanged
        Try
            order1.UpdateDetails()
            LoadProfile()
            Dim objOpp As New OppBizDocs
            If objOpp.ValidateARAP(0, ddlRelationship.SelectedValue, Session("DomainID")) = False Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting->Accounts for RelationShip"" To Save' );", True)
                ddlRelationship.SelectedIndex = 0
                Exit Sub
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub


    Sub LoadProfile()
        Try
            Dim objUserAccess As New UserAccess
            objUserAccess.RelID = ddlRelationship.SelectedItem.Value
            objUserAccess.DomainID = Session("DomainID")
            ddlProfile.DataSource = objUserAccess.GetRelProfileD
            ddlProfile.DataTextField = "ProName"
            ddlProfile.DataValueField = "numProfileID"
            ddlProfile.DataBind()
            ddlProfile.Items.Insert(0, New ListItem("---Select One---", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub ddlPurchaseTemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPurchaseTemplate.SelectedIndexChanged
        Try
            If ddlPurchaseTemplate.SelectedIndex = 0 Then Exit Sub
            Dim objOpp As New COpportunities
            Dim dtTemplate As DataTable
            objOpp.DomainID = Session("DomainID")
            objOpp.SalesTemplateID = ddlPurchaseTemplate.SelectedValue.Split(",")(0)
            objOpp.byteMode = 2
            dtTemplate = objOpp.GetSalesTemplate()
            If dtTemplate.Rows.Count > 0 Then
                Dim dtItems As DataTable
                Dim objOpportunity As New MOpportunity

                'Fetch items from Opportunity
                If CCommon.ToLong(dtTemplate.Rows(0)("numOppId")) > 0 Then
                    objOpportunity.OpportunityId = CCommon.ToLong(dtTemplate.Rows(0)("numOppId"))
                    objOpportunity.DomainID = Session("DomainID")
                    objOpportunity.Mode = 2
                    dtItems = objOpportunity.GetOrderItems().Tables(0)
                Else 'Fetch item from sales template items 
                    dtItems = objOpp.GetSalesTemplateItems()
                End If


                Dim objItems As New CItems
                Dim ds As New DataSet
                Dim drow As DataRow
                Dim i As Integer = 1
                'Dim k As Integer
                'Dim dtItemTax As DataTable
                objCommon = New CCommon
                ds = order1.SOItems
                ds.Tables(0).Clear()
                ds.Tables(1).Clear()
                ds.Tables(2).Clear()
                For Each dr As DataRow In dtItems.Rows
                    drow = ds.Tables(0).NewRow()
                    drow("numoppitemtCode") = dr("numoppitemtCode")
                    drow("numItemCode") = dr("numItemCode")
                    drow("numUnitHour") = dr("numUnitHour")
                    drow("monPrice") = String.Format("{0:0.00}", dr("monPrice"))
                    drow("monTotAmount") = String.Format("{0:0.00}", dr("monTotAmount"))
                    drow("numSourceID") = dr("numSourceID")
                    drow("vcItemDesc") = dr("vcItemDesc")
                    drow("numWarehouseID") = dr("numWarehouseID")
                    drow("vcItemName") = dr("vcItemName")
                    drow("Warehouse") = dr("Warehouse")
                    drow("numWarehouseItmsID") = dr("numWarehouseItmsID")
                    drow("ItemType") = dr("vcType")
                    drow("Attributes") = dr("vcAttributes")
                    drow("Op_Flag") = 1 '1 for insertion
                    drow("DropShip") = dr("bitDropShip")
                    drow("bitDiscountType") = dr("bitDiscountType")
                    drow("fltDiscount") = String.Format("{0:0.00}", dr("fltDiscount"))
                    drow("monTotAmtBefDiscount") = String.Format("{0:0.00}", dr("monTotAmtBefDiscount"))
                    drow("numUOM") = dr("numUOMId")
                    drow("vcUOMName") = dr("vcUOMName")
                    drow("UOMConversionFactor") = dr("UOMConversionFactor")
                    drow("bitWorkOrder") = 0
                    drow("numVendorWareHouse") = dr("numVendorWareHouse")
                    drow("numShipmentMethod") = dr("numShipmentMethod")
                    drow("numSOVendorId") = 0
                    drow("numProjectID") = 0
                    drow("numProjectStageID") = 0
                    drow("charItemType") = dr("charItemType")
                    drow("vcBaseUOMName") = dr("vcBaseUOMName")
                    drow("bitIsAuthBizDoc") = False

                    i += 1
                    ds.Tables(0).Rows.Add(drow)
                Next
                ds.Tables(0).AcceptChanges()
                order1.SOItems = ds
                CType(order1.FindControl("dgItems"), DataGrid).DataSource = ds
                CType(order1.FindControl("dgItems"), DataGrid).DataBind()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub
    Private Sub BindSalesTemplate()
        Try
            Dim objOpp As New COpportunities
            Dim dtTemplate As DataTable
            objOpp.DomainID = Session("DomainID")
            objOpp.byteMode = 1
            objOpp.DivisionID = IIf(radCmbCompany.SelectedValue.Length > 0, radCmbCompany.SelectedValue, 0)
            dtTemplate = objOpp.GetSalesTemplate()
            ddlPurchaseTemplate.DataTextField = "vcTemplateName"
            ddlPurchaseTemplate.DataValueField = "numSalesTemplateID1"
            ddlPurchaseTemplate.DataSource = dtTemplate
            ddlPurchaseTemplate.DataBind()
            ddlPurchaseTemplate.Items.Insert(0, "--Select One--")
            ddlPurchaseTemplate.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub BindMultiCurrency()
        Try
            If Session("MultiCurrency") = True Then
                pnlCurrency.Visible = True
                Dim objCurrency As New CurrencyRates
                objCurrency.DomainID = Session("DomainID")
                objCurrency.GetAll = 0
                ddlCurrency.DataSource = objCurrency.GetCurrencyWithRates()
                ddlCurrency.DataTextField = "vcCurrencyDesc"
                ddlCurrency.DataValueField = "numCurrencyID"
                ddlCurrency.DataBind()
                ddlCurrency.Items.Insert(0, "--Select One--")
                ddlCurrency.Items.FindByText("--Select One--").Value = "0"
                If Not ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")) Is Nothing Then
                    ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")).Selected = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnTemp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTemp.Click
        Try
            If (CCommon.ToLong(CType(order1.FindControl("hdnBillAddressID"), HiddenField).Value) > 0) Then
                BindAddressByAddressID(CCommon.ToLong(CType(order1.FindControl("hdnBillAddressID"), HiddenField).Value), "Bill")
            End If
            If (CCommon.ToLong(CType(order1.FindControl("hdnShipAddressID"), HiddenField).Value) > 0) Then
                BindAddressByAddressID(CCommon.ToLong(CType(order1.FindControl("hdnShipAddressID"), HiddenField).Value), "Ship")
            End If

            If hdnIsDropshipClicked.Value = "1" Then
                chkDropshipPO.Checked = True
                CType(order1.FindControl("chkDropShip"), CheckBox).Checked = True
            End If

            If hdnClearItems.Value = "1" Then
                Dim ds As DataSet = order1.SOItems
                ds.Tables(0).Clear()
                ds.Tables(1).Clear()
                ds.Tables(2).Clear()
                order1.SOItems = ds
                CType(order1.FindControl("dgItems"), DataGrid).DataSource = ds.Tables(0)
                CType(order1.FindControl("dgItems"), DataGrid).DataBind()
            End If

            hdnClearItems.Value = "0"
            hdnIsDropshipClicked.Value = "0"
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub
    Private Sub BindAddressByAddressID(ByVal lngAddressID As Long, ByVal AddType As String)
        Try
            Dim objContact As New CContacts
            objContact.DomainID = Session("DomainID")
            objContact.AddressID = lngAddressID
            objContact.byteMode = 1
            Dim dtTable As DataTable = objContact.GetAddressDetail

            If AddType = "Bill" Then
                If dtTable.Rows.Count > 0 Then
                    lblBillTo1.Text = CCommon.ToString(dtTable.Rows(0)("vcFullAddress"))
                    lblBillTo2.Text = ""
                Else
                    lblBillTo1.Text = ""
                    lblBillTo2.Text = ""
                End If
            Else
                If dtTable.Rows.Count > 0 Then
                    lblShipTo1.Text = CCommon.ToString(dtTable.Rows(0)("vcFullAddress"))
                    lblShipTo2.Text = ""
                    hdnShipToWarehouseID.Value = CCommon.ToLong(dtTable.Rows(0)("numWarehouseID"))
                Else
                    lblShipTo1.Text = ""
                    lblShipTo2.Text = ""
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub lnkClick_Click(sender As Object, e As System.EventArgs) Handles lnkClick.Click
        Try
            tblMultipleOrderMessage.Visible = False
            order1.SOItems = Nothing
            Response.Redirect(Request.Url.ToString())
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As String)
        lblException.Text = ex
        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ScrollTo", "ScrollToError();", True)
    End Sub

    Private Sub FillClass()
        Try
            If CCommon.ToLong(Session("DefaultClassType")) > 0 Then
                Dim dtClass As DataTable
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.Mode = 1
                dtClass = objAdmin.GetClass()

                If dtClass.Rows.Count > 0 Then
                    ddlClass.DataTextField = "ClassName"
                    ddlClass.DataValueField = "numChildClassID"
                    ddlClass.DataSource = dtClass
                    ddlClass.DataBind()

                    Dim intDefaultClassType As Int16 = CCommon.ToInteger(Session("DefaultClassType"))
                    If intDefaultClassType = 1 AndAlso ddlClass.Items.FindByValue(Session("DefaultClass")) IsNot Nothing Then 'intDefaultClassType = 1 => User Level Account Class
                        ddlClass.ClearSelection()
                        ddlClass.Items.FindByValue(Session("DefaultClass")).Selected = True
                    End If
                End If

                divClass.Visible = True
            End If

            ddlClass.Items.Insert(0, New ListItem("--Select One --", "0"))
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub rdcmbShipVia_SelectedIndexChanged(sender As Object, e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs)
        rdcmbShippingService.Items.Clear()
        rdcmbShippingService.Items.Add(New Telerik.Web.UI.RadComboBoxItem("-- Select One --", "0"))
        OppBizDocs.LoadServiceTypes(rdcmbShipVia.SelectedValue, rdcmbShippingService)
    End Sub

    Protected Sub chkDropshipPO_CheckedChanged(sender As Object, e As EventArgs)
        Try
            If chkDropshipPO.Checked Then
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "OpenAdressWindow", "OpenAddressWindow('ShipTo',1);", True)
                chkDropshipPO.Checked = False
            Else
                Me.FillExistingAddress(Session("UserDivisionID"))
                CType(order1.FindControl("chkDropShip"), CheckBox).Checked = False
            End If


            Dim ds As DataSet = order1.SOItems
            ds.Tables(0).Clear()
            ds.Tables(1).Clear()
            ds.Tables(2).Clear()
            order1.SOItems = ds
            CType(order1.FindControl("dgItems"), DataGrid).DataSource = ds.Tables(0)
            CType(order1.FindControl("dgItems"), DataGrid).DataBind()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

    Private Sub btnSelectedVendor_Click(sender As Object, e As EventArgs) Handles btnSelectedVendor.Click
        Try
            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "HidePOVendor", "$('#imgPODistance').hide(); $('#imgPODistance').removeAttr('onclick');", True)

            objCommon.DivisionID = CCommon.ToLong(hdnVendorID.Value)
            objCommon.charModule = "D"
            objCommon.GetCompanySpecificValues1()

            Dim strCompany, strContactID As String
            strContactID = objCommon.ContactID
            strCompany = objCommon.GetCompanyName
            radCmbCompany.Text = strCompany
            radCmbCompany.SelectedValue = objCommon.DivisionID
            CType(order1.FindControl("txtDivID"), TextBox).Text = radCmbCompany.SelectedValue
            CType(order1.FindControl("txtPrice"), TextBox).Text = CCommon.ToDouble(hdnVendorCost.Value)

            radCmbCompany_SelectedIndexChanged()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub
End Class



