﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic

Partial Public Class frmEmbeddedCost
    Inherits BACRMPage
    
    Dim dtChartAcntDetails As New DataTable
    Dim lngOppBizDocID As Long
    Dim lngOppID As Long
    Dim lngCostCatID As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngOppBizDocID = CCommon.ToLong(GetQueryStringVal( "OppBizDocId"))
            lngOppID = CCommon.ToLong(GetQueryStringVal( "OppID"))
            lblMessage.Text = ""
            If Not IsPostBack Then
                
                objCommon.sb_FillComboFromDBwithSel(ddlCostCategory, 198, Session("DomainID"))
                'Select Last viewed Category 
                Dim objOpp As New MOpportunity
                objOpp.DomainID = Session("DomainID")
                objOpp.OppBizDocID = lngOppBizDocID
                Dim dt As DataTable = objOpp.GetEmbeddedCostConfig()
                If dt.Rows.Count > 0 Then
                    If Not ddlCostCategory.Items.FindByValue(dt.Rows(0)("numLastViewedCostCatID")) Is Nothing Then
                        ddlCostCategory.Items.FindByValue(dt.Rows(0)("numLastViewedCostCatID")).Selected = True
                        BindGrid()
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Dim dtCostCenter, dtPaymentMethod As DataTable
    Sub BindGrid()
        Try
            Dim objOpp As New MOpportunity
            objOpp.DomainID = Session("DomainID")
            objOpp.OppBizDocID = lngOppBizDocID
            objOpp.CostCategoryID = ddlCostCategory.SelectedValue
            Dim dtData As DataTable = objOpp.GetEmbeddedCost()
            dtCostCenter = objCommon.GetMasterListItems(202, Session("DomainID"))
            dtPaymentMethod = objCommon.GetMasterListItems(31, Session("DomainID"))
            CreateEmptyRows(dtData)
            dgECost.DataSource = dtData
            dgECost.DataBind()

            Dim i As Int16 = 0
            Dim boolDisabled As Boolean
            For Each dr As DataRow In dtData.Rows
                If CCommon.ToLong(dr("numDefaultEmbeddedCostID")) > 0 Then
                    boolDisabled = Not CCommon.ToBool(dr("IsDisabled"))
                    If Not CType(dgECost.Items(i).FindControl("ddlCostCenter"), DropDownList) Is Nothing Then
                        If Not CType(dgECost.Items(i).FindControl("ddlCostCenter"), DropDownList).Items.FindByValue(dr("numCostCenterID").ToString) Is Nothing Then
                            CType(dgECost.Items(i).FindControl("ddlCostCenter"), DropDownList).Items.FindByValue(dr("numCostCenterID").ToString).Selected = True
                            CType(dgECost.Items(i).FindControl("ddlCostCenter"), DropDownList).Enabled = boolDisabled
                        End If
                    End If
                    If Not CType(dgECost.Items(i).FindControl("ddlPaymentMethod"), DropDownList) Is Nothing Then
                        If Not CType(dgECost.Items(i).FindControl("ddlPaymentMethod"), DropDownList).Items.FindByValue(dr("numPaymentMethod").ToString) Is Nothing Then
                            CType(dgECost.Items(i).FindControl("ddlPaymentMethod"), DropDownList).Items.FindByValue(dr("numPaymentMethod").ToString).Selected = True
                            CType(dgECost.Items(i).FindControl("ddlPaymentMethod"), DropDownList).Enabled = boolDisabled
                        End If
                    End If
                    If Not CType(dgECost.Items(i).FindControl("radCmbCompany"), RadComboBox) Is Nothing Then
                        CType(dgECost.Items(i).FindControl("radCmbCompany"), RadComboBox).Text = dr("vcCompanyName").ToString
                        CType(dgECost.Items(i).FindControl("radCmbCompany"), RadComboBox).SelectedValue = dr("numDivisionID").ToString
                        CType(dgECost.Items(i).FindControl("radCmbCompany"), RadComboBox).Enabled = boolDisabled
                    End If
                    Dim hpl As HyperLink = CType(dgECost.Items(i).FindControl("hplItems"), HyperLink)
                    If Not hpl Is Nothing Then
                        hpl.Text = String.Format("{0:##,#00.00}", CCommon.ToDecimal(dr("monCost")))
                        hpl.Attributes.Add("onclick", "OpenItems(" & dr("numEmbeddedCostID") & ")")
                        If CCommon.ToLong(dr("numEmbeddedCostID")) = 0 Then
                            hpl.Visible = False
                        End If
                        'hpl.Enabled = boolDisabled
                    End If
                    If Not CType(dgECost.Items(i).FindControl("txtActualCost"), TextBox) Is Nothing Then
                        CType(dgECost.Items(i).FindControl("txtActualCost"), TextBox).Text = String.Format("{0:##,#00.00}", CCommon.ToDecimal(dr("monActualCost")))
                        CType(dgECost.Items(i).FindControl("txtActualCost"), TextBox).Enabled = boolDisabled
                    End If

                    If Not CType(dgECost.Items(i).FindControl("txtMemo"), TextBox) Is Nothing Then
                        CType(dgECost.Items(i).FindControl("txtMemo"), TextBox).Text = dr("vcMemo").ToString
                        CType(dgECost.Items(i).FindControl("txtMemo"), TextBox).Enabled = boolDisabled
                    End If
                    If Not CType(dgECost.Items(i).FindControl("calDueDate"), BACRM.Include.calandar) Is Nothing Then
                        CType(dgECost.Items(i).FindControl("calDueDate"), BACRM.Include.calandar).SelectedDate = dr("dtDueDate").ToString
                    End If
                Else
                    If Not CType(dgECost.Items(i).FindControl("calDueDate"), BACRM.Include.calandar) Is Nothing Then
                        CType(dgECost.Items(i).FindControl("calDueDate"), BACRM.Include.calandar).SelectedDate = Date.Now.Date
                    End If
                End If


                i = i + 1
            Next

        Catch ex As Exception
            Throw ex
        End Try
    End Sub



    Private Sub ddlCostCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCostCategory.SelectedIndexChanged
        Try
            If ddlCostCategory.SelectedValue > 0 Then
                Dim objOpp As New MOpportunity
                objOpp.DomainID = Session("DomainID")
                objOpp.OppBizDocID = lngOppBizDocID
                objOpp.CostCategoryID = ddlCostCategory.SelectedValue
                objOpp.ManageEmbeddedCostConfig()
                BindGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub CreateEmptyRows(ByRef dt As DataTable)
        Try
            If dt Is Nothing Then dt = New DataTable
            If dt.Rows.Count = 0 And dt.Columns.Count = 0 Then
                dt.Columns.Add("numEmbeddedCostID")
                dt.Columns.Add("numCostCatID")
                dt.Columns.Add("numCostCenterID")
                dt.Columns.Add("numAccountID")
                dt.Columns.Add("numDivisionID")
                dt.Columns.Add("numPaymentMethod")
                dt.Columns.Add("vcCompanyName")
            End If
            Dim OriginalRowCount As Integer = dt.Rows.Count
            Dim dr As DataRow
            For i As Integer = 0 To 9 - OriginalRowCount
                dr = dt.NewRow
                dr("numEmbeddedCostID") = 0
                dr("numCostCatID") = ddlCostCategory.SelectedValue
                dr("numCostCenterID") = 0
                dr("numAccountID") = 0
                dr("numDivisionID") = 0
                dr("numPaymentMethod") = 0
                dr("vcCompanyName") = ""
                dt.Rows.Add(dr)
            Next

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            Dim objOpp As New MOpportunity
            objOpp.DomainID = Session("DomainID")
            objOpp.UserCntID = Session("UserContactID")
            objOpp.CostCategoryID = ddlCostCategory.SelectedValue
            objOpp.OppBizDocID = lngOppBizDocID
            objOpp.strItems = GetItems()
            'Try
            objOpp.ManageEmbeddedCost()
            'Catch ex As Exception
            'If ex.Message = "NOCLASS" Then
            '    lblMessage.Text = "No item classification is set, please cofigure embedded cost classification form domain details"
            'If ex.Message = "NOITEM" Then
            '    lblMessage.Text = "Selected Invoice doesn't contain any items which are set to receive embedded costs, please cofigure embedded cost classification form domain details"
            'Else
            '    Throw ex
            'End If
            'End Try

            'redirecting because when refreshing page after saving value it creates duplicate
            Response.Redirect("frmEmbeddedCost.aspx?OppBizDocId=" & lngOppBizDocID.ToString & "&OppID=" & lngOppID.ToString, False)
            'BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Function GetItems() As String
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            Dim objOppBizDocs As New Opportunities.OppBizDocs

            dt.Columns.Add("numEmbeddedCostID")
            dt.Columns.Add("numCostCenterID")
            dt.Columns.Add("numAccountID")
            dt.Columns.Add("numDivisionID")
            dt.Columns.Add("numPaymentMethod")
            'dt.Columns.Add("monCost")
            dt.Columns.Add("monActualCost")
            dt.Columns.Add("vcMemo")
            dt.Columns.Add("dtDueDate")

            Dim dr As DataRow
            Dim lngVendorAPAccountID As Long
            For Each gvr As DataGridItem In dgECost.Items
                Dim ddlCostCenter As DropDownList = CType(gvr.FindControl("ddlCostCenter"), DropDownList)
                Dim ddlPaymentMethod As DropDownList = CType(gvr.FindControl("ddlPaymentMethod"), DropDownList)
                Dim radCmbCompany As RadComboBox = CType(gvr.FindControl("radCmbCompany"), RadComboBox)
                'Dim txtEstimatedCost As TextBox = CType(gvr.FindControl("txtEstimatedCost"), TextBox)
                Dim txtActualCost As TextBox = CType(gvr.FindControl("txtActualCost"), TextBox)
                Dim txtMemo As TextBox = CType(gvr.FindControl("txtMemo"), TextBox)
                Dim calDueDate As BACRM.Include.calandar = CType(gvr.FindControl("calDueDate"), BACRM.Include.calandar)
                Dim lblAccountID As Label = CType(gvr.FindControl("lblAccountID"), Label)
                Dim lngEmbeddedCostID As Long = CCommon.ToLong(gvr.Cells(0).Text)

                If CCommon.ToLong(ddlCostCenter.SelectedValue) > 0 And CCommon.ToLong(radCmbCompany.SelectedValue) > 0 And CCommon.ToLong(ddlPaymentMethod.SelectedValue) > 0 And Not calDueDate.SelectedDate Is Nothing Then

                    lngVendorAPAccountID = objOppBizDocs.ValidateCustomerAR_APAccounts("AP", Session("DomainID"), CCommon.ToLong(radCmbCompany.SelectedValue))
                    If lngVendorAPAccountID = 0 Then
                        lblMessage.Text = "Please Set AP Relationship for vendor '" + radCmbCompany.Text.Trim + "' from ""Administration->Global Settings->Accounting->Accountd for RelationShip"" To Save"
                        Exit Function
                    End If

                    dr = dt.NewRow()
                    dr("numEmbeddedCostID") = lngEmbeddedCostID
                    dr("numCostCenterID") = CCommon.ToLong(ddlCostCenter.SelectedValue)
                    dr("numAccountID") = CCommon.ToLong(lblAccountID.Text)
                    dr("numDivisionID") = CCommon.ToLong(radCmbCompany.SelectedValue)
                    dr("numPaymentMethod") = CCommon.ToLong(ddlPaymentMethod.SelectedValue)
                    'dr("monCost") = CCommon.ToDecimal(txtEstimatedCost.Text)
                    dr("monActualCost") = CCommon.ToDecimal(txtActualCost.Text)
                    dr("vcMemo") = txtMemo.Text
                    dr("dtDueDate") = calDueDate.SelectedDate
                    dt.Rows.Add(dr)
                End If

            Next
            ds.Tables.Add(dt)
            Return ds.GetXml()
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        Try
            Dim objOpp As New MOpportunity
            objOpp.DomainID = Session("DomainID")
            objOpp.CostCategoryID = ddlCostCategory.SelectedValue
            objOpp.OppBizDocID = lngOppBizDocID
            Try
                objOpp.DeleteEmbeddedCost()
            Catch ex As Exception
                If ex.Message = "PAID" Then
                    lblMessage.Text = "Paid Bill Records can not be deleted,your option is to remove paid amount from respective Chart of Account and try again."
                Else
                    Throw ex
                End If
            End Try

            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgECost_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgECost.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

                Dim ddlPaymentMethod As DropDownList = CType(e.Item.FindControl("ddlPaymentMethod"), DropDownList)
                If Not dtPaymentMethod Is Nothing Then
                    ddlPaymentMethod.DataSource = dtPaymentMethod
                    ddlPaymentMethod.DataTextField = "vcData"
                    ddlPaymentMethod.DataValueField = "numListItemID"
                    ddlPaymentMethod.DataBind()
                    ddlPaymentMethod.Items.Insert(0, "--Select One--")
                    ddlPaymentMethod.Items.FindByText("--Select One--").Value = "0"
                End If

                Dim ddlCostCenter As DropDownList = CType(e.Item.FindControl("ddlCostCenter"), DropDownList)
                If Not dtCostCenter Is Nothing Then
                    ddlCostCenter.DataSource = dtCostCenter
                    ddlCostCenter.DataTextField = "vcData"
                    ddlCostCenter.DataValueField = "numListItemID"
                    ddlCostCenter.DataBind()
                    ddlCostCenter.Items.Insert(0, "--Select One--")
                    ddlCostCenter.Items.FindByText("--Select One--").Value = "0"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class