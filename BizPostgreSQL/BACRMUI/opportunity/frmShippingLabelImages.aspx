﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmShippingLabelImages.aspx.vb"
    Inherits=".frmShippingLabelImages" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title></title>
    <script type="text/javascript">
        window.onunload = refreshParent;
        function refreshParent() {
            window.opener.location.href = window.opener.location.href;
        }
</script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:DataList ID="dlShipLabel" runat="server" CssClass="dg" RepeatLayout="Table"
        RepeatDirection="Vertical">
        <ItemStyle CssClass="is" />
        <AlternatingItemStyle CssClass="ais" />
        <ItemTemplate>
            <table width="100%" cellpadding="3" cellspacing="3">
                <tr>
                    <td>
                        <asp:Image ID="Img" runat="server" ImageUrl='<%# "../../" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName") & "/Documents/Docs/" & Session("DomainID") & "/" & Eval("vcShippingLabelImage").ToString()%>' />
                    </td>
                </tr>
            </table>
        </ItemTemplate>
    </asp:DataList>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
