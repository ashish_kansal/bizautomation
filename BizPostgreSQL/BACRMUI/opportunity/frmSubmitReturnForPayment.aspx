﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSubmitReturnForPayment.aspx.vb"
    Inherits="BACRM.UserInterface.Opportunities.frmSubmitReturnForPayment" MasterPageFile="~/common/Popup.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script type="text/javascript">
        function Close(type) {
            window.opener.location = '../Opportunity/frmSalesReturns.aspx?type=' + type;
            window.close()
            return false;
        }

        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }

        function OpenRMA(a, b) {
            window.open('../opportunity/frmRMA.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&Name=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=680,height=500,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSubmit" runat="server" Text="Submit for Payment" CssClass="button"
                CausesValidation="true" />&nbsp;
            <asp:Button ID="btnCreditMemo" runat="server" Text="Credit Memo" CssClass="button">
            </asp:Button>&nbsp;
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" CausesValidation="false" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Payment Submission
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table>
        <tr>
            <td class="normal1" colspan="2">
                <asp:ValidationSummary ID="valSummary" runat="server" DisplayMode="BulletList" ShowMessageBox="true"
                    ShowSummary="false" />
            </td>
        </tr>
        <tr>
            <td class="normal4" align="center" colspan="2">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                <b>Organization Credit Balance</b> :
            </td>
            <td class="normal1">
                <asp:Label ID="lblOrganizationCreditBalance" runat="server" CssClass="signup"></asp:Label>
                <asp:HiddenField runat="server" ID="hfDivisionCreditBalance"></asp:HiddenField>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                <b>Item Credit Balance</b> :
            </td>
            <td class="normal1">
                <asp:Label ID="lblItemCreditBalance" runat="server" CssClass="signup"></asp:Label>
                <asp:HiddenField runat="server" ID="hfItemCreditBalance"></asp:HiddenField>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Item Unit Price :
            </td>
            <td class="normal1">
                <asp:Label ID="lblPrice" runat="server" CssClass="signup"></asp:Label>
                <asp:HiddenField runat="server" ID="hfItemPrice"></asp:HiddenField>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right" nowrap="nowrap">
                <asp:Label ID="lblAmountPaidTitle" runat="server">Amount Paid to Vendor</asp:Label>
                :
            </td>
            <td class="normal1">
                <asp:Label ID="lblAmountPaid" runat="server" CssClass="signup"></asp:Label>
                <asp:HiddenField runat="server" ID="hfAmountPaid"></asp:HiddenField>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Qty to Return :
            </td>
            <td class="normal1">
                <asp:Label ID="lblQtytoReturn" runat="server" CssClass="signup"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Qty Returned :
            </td>
            <td class="normal1">
                <asp:Label ID="lblQtyReturned" runat="server" CssClass="signup"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Qty Pending Returned :
            </td>
            <td class="normal1">
                <asp:TextBox ID="txtQReturned" runat="server" 
                    CssClass="signup" Width="50px" onkeypress="javascript:CheckNumber(2,event);"></asp:TextBox>
                <asp:TextBox runat="server" ID="txtQtyReturnCmp" Style="display: none"></asp:TextBox>
                <asp:CompareValidator Operator="LessThanEqual" ControlToValidate="txtQReturned" ControlToCompare="txtQtyReturnCmp"
                    Type="Integer" Display="Dynamic" ID="cvQty" runat="server" ErrorMessage="Qty Returned can not be greater than Qty To Return"
                    Text="*"></asp:CompareValidator>
                <asp:RequiredFieldValidator ID="rfvQty" runat="server" ErrorMessage="Enter Quantity to Return"
                    Display="Dynamic" Text="*" ControlToValidate="txtQReturned"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="cvQty1" runat="server" ErrorMessage="Quantity to Return is numeic(1-9)"
                    Operator="DataTypeCheck" Display="Dynamic" Text="*" ControlToValidate="txtQReturned"
                    Type="Integer"></asp:CompareValidator>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Payment Action :
            </td>
            <td class="normal1">
                <asp:DropDownList ID="ddlPaymentAction" runat="server" CssClass="signup" AutoPostBack="true">
                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                    <asp:ListItem Value="1">Issue credit</asp:ListItem>
                    <asp:ListItem Value="2">Reverse issued credit</asp:ListItem>
                    <asp:ListItem Value="3">Create a sales return in money-out</asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rfvPaymentAction" runat="server" ErrorMessage="Select Payment Action"
                    Display="Dynamic" Text="*" ControlToValidate="ddlPaymentAction" InitialValue="0"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr id="trPaymentMethod" runat="server" visible="false">
            <td class="normal1" align="right">
                Payment Method :
            </td>
            <td class="normal1">
                <asp:DropDownList ID="ddlPaymentMethod" runat="server" CssClass="signup">
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="trAmountCredit" runat="server" visible="false">
            <td class="normal1" align="right">
                Amount to credit :
            </td>
            <td class="normal1">
                <asp:TextBox ID="txtAmountCredit" runat="server" CssClass="signup" onkeypress="javascript:CheckNumber(1,event);"></asp:TextBox>
                <asp:TextBox runat="server" ID="txtAmountCreditCmp" Style="display: none"></asp:TextBox>
                <asp:CompareValidator Operator="LessThanEqual" ControlToValidate="txtAmountCredit"
                    ControlToCompare="txtAmountCreditCmp" Type="Double" Display="Dynamic" ID="CompareValidator1"
                    runat="server" ErrorMessage="Amount to credit can not be greater than Amount Customer Paid"
                    Text="*"></asp:CompareValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter Amount to credit"
                    Display="Dynamic" Text="*" ControlToValidate="txtAmountCredit"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Return Date :
            </td>
            <td class="normal1">
                <BizCalendar:Calendar ID="calReturnDate" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Memo :
            </td>
            <td class="normal1">
                <asp:TextBox ID="txtMemo" runat="server" Text="" Width="250px" CssClass="signup"
                    MaxLength="200"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvMemo" runat="server" ErrorMessage="Enter Memo"
                    Display="Dynamic" Text="*" ControlToValidate="txtMemo"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <%-- <tr>
                                    <td class="normal1" align="right">
                                        Reference # :
                                    </td>
                                    <td class="normal1">
                                        <asp:TextBox ID="txtReference" runat="server" Width="250px" CssClass="signup" MaxLength="200"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rfvReference" runat="server" ErrorMessage="Enter Reference"
                                            Display="Dynamic" Text="*" ControlToValidate="txtReference"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>--%>
    </table>
    <asp:HiddenField ID="hdnItemCode" runat="server" />
    <asp:HiddenField ID="hdnDivisionID" runat="server" />
</asp:Content>
