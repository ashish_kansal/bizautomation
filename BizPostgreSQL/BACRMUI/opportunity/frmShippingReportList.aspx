﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmShippingReportList.aspx.vb"
    Inherits=".frmShippingReportList" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>List of Shipping Reports</title>
    <script type="text/javascript">
        function OpenShippingReport(a, b, c) {
            window.open('../opportunity/frmShippingReport.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppBizDocId=' + a + '&ShipReportId=' + b + '&OppId=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=800,height=300,scrollbars=yes,resizable=yes');
        }
        function OpenBizInvoice(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <table id="Table1" cellspacing="0" cellpadding="0" width="100%" border="0" runat="server">
        <tr>
            <td align="right">
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClientClick="return Close();"></asp:Button>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Shipping Report List
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="Table2" BorderWidth="1" CssClass="aspTable" CellPadding="0" CellSpacing="0"
        Height="" runat="server" Width="100%" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgShippingReportList" runat="server" AutoGenerateColumns="False"
                    CssClass="dg" Width="100%" style="min-width:800px">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is" HorizontalAlign="Left"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="numShippingReportId" Visible="false"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Report">
                            <ItemTemplate>
                                <a href="#" onclick="OpenShippingReport('<%#Eval("numOppBizDocId")%>','<%#Eval("numShippingReportId")%>','<%#Eval("numOppId")%>')">
                                    <%#Eval("vcReportName")%></a></ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="SumTotal" HeaderText="Shipping Amount" DataFormatString="{0:##,#00.00}">
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="BizDoc ID">
                            <ItemTemplate>
                                <a href="javascript:void(0)" onclick="OpenBizInvoice('<%# Eval("numOppId") %>','<%# Eval("numOppBizDocId") %>');">
                                    <%#Eval("vcbizdocid")%>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="ShippingCompany" HeaderText="Shipping Company"></asp:BoundColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete"
                                    CommandArgument='<%# Eval("numShippingReportId")%>'></asp:Button>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
