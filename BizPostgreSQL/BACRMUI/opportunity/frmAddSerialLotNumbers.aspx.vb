﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities

Namespace BACRM.UserInterface.Opportunities
    Public Class frmAddSerialLotNumbers
        Inherits BACRMPage

#Region "Member Variables"
        Private _objOpportunity As COpportunities
#End Region

#Region "Constructor"
        Sub New()
            _objOpportunity = New COpportunities
        End Sub
#End Region

#Region "Page Events"
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not Page.IsPostBack Then
                    hdnOppID.Value = GetQueryStringVal("oppID")
                    hdnOppItemID.Value = GetQueryStringVal("oppItemID")
                    hdnItemID.Value = GetQueryStringVal("itemID")
                    hdnWarehouseItemID.Value = GetQueryStringVal("warehouseItemID")
                    hdnItemType.Value = GetQueryStringVal("itemType")
                    hdnQty.Value = GetQueryStringVal("qty")
                    hdnType.Value = GetQueryStringVal("type")
                    lblMaxQty.Text = "(Max " & hdnQty.Value & ")"
                    hdnRowID.Value = GetQueryStringVal("rowId")

                    If hdnItemType.Value = "Serial" Then
                        trLotQty.Visible = False
                    ElseIf hdnItemType.Value = "Lot" Then
                        trLotQty.Visible = True
                    End If

                    BindData()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
           
        End Sub
#End Region

#Region "Private Methods"

        Private Sub BindData()
            Try
                _objOpportunity.OpportID = CCommon.ToLong(hdnOppID.Value)
                _objOpportunity.OppItemCode = CCommon.ToLong(hdnOppItemID.Value)
                _objOpportunity.WarehouseItmsID = CCommon.ToLong(hdnWarehouseItemID.Value)

                Dim ds As DataSet = _objOpportunity.GetOppSerialLotNumber()

                If Not ds Is Nothing AndAlso ds.Tables.Count = 2 Then
                    If hdnItemType.Value = "Serial" Then

                        lstAvailablefld.DataSource = ds.Tables(0)
                        lstAvailablefld.DataTextField = "vcSerialNo"
                        lstAvailablefld.DataValueField = "numWareHouseItmsDTLID"
                        lstAvailablefld.DataBind()

                        Dim listItem As New ListItem
                        For Each dr As DataRow In ds.Tables(1).Rows
                            ListItem = New ListItem
                            listItem.Text = CCommon.ToString(dr("vcSerialNo"))
                            listItem.Value = CCommon.ToString(dr("numWareHouseItmsDTLID"))
                            listItem.Attributes.Add("numOppBizDocsId", CCommon.ToLong(dr("numOppBizDocsId")))
                            lstSelectedfld.Items.Add(ListItem)
                        Next
                    ElseIf hdnItemType.Value = "Lot" Then
                        Dim listItem As New ListItem
                        For Each dr As DataRow In ds.Tables(0).Rows
                            listItem = New ListItem
                            listItem.Attributes.Add("Date", CCommon.ToString(dr("dExpirationDate")))
                            listItem.Attributes.Add("SerialNo", CCommon.ToString(dr("vcSerialNo")))
                            listItem.Attributes.Add("Qty", CCommon.ToString(dr("numQty")))
                            listItem.Attributes.Add("Order", CCommon.ToString(dr("OrderNo")))
                            listItem.Text = CCommon.ToString(dr("vcSerialNo")) & "(" & CCommon.ToString(dr("dExpirationDate")) & ")" & "(" & CCommon.ToString(dr("numQty")) & ")"
                            listItem.Value = CCommon.ToString(dr("numWareHouseItmsDTLID"))
                            lstAvailablefld.Items.Add(listItem)
                        Next

                        For Each dr As DataRow In ds.Tables(1).Rows
                            listItem = New ListItem
                            listItem.Attributes.Add("Date", CCommon.ToString(dr("dExpirationDate")))
                            listItem.Attributes.Add("SerialNo", CCommon.ToString(dr("vcSerialNo")))
                            listItem.Attributes.Add("Qty", CCommon.ToString(dr("numQty")))
                            listItem.Attributes.Add("Order", CCommon.ToString(dr("OrderNo")))
                            listItem.Text = CCommon.ToString(dr("vcSerialNo")) & "(" & CCommon.ToString(dr("dExpirationDate")) & ")" & "(" & CCommon.ToString(dr("numQty")) & ")"
                            listItem.Value = CCommon.ToString(dr("numWareHouseItmsDTLID"))
                            listItem.Attributes.Add("numOppBizDocsId", CCommon.ToLong(dr("numOppBizDocsId")))
                            lstSelectedfld.Items.Add(listItem)
                        Next

                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

#End Region

#Region "Event Handlers"
        Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
            Try
                Dim selectedItems As String

                If Not String.IsNullOrEmpty(hdnSelectedItems.Value) Then
                    selectedItems = hdnSelectedItems.Value.Substring(0, hdnSelectedItems.Value.Length - 1)
                End If

                If CCommon.ToLong(hdnType.Value) = 2 Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Close", "window.opener.SetSelectedSerialLot('" & hdnSelectedItems.Value.Trim(",") & "','" & hdnRowID.Value & "'); window.close();", True)
                ElseIf CCommon.ToLong(hdnType.Value) = 1 Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Close", "window.opener.SetSelectedSerialLot('" & hdnSelectedItems.Value.Trim(",") & "'); window.close();", True)
                Else
                    Dim tintItemType As Int16

                    If hdnItemType.Value = "Lot" Then
                        tintItemType = 2
                    ElseIf hdnItemType.Value = "Serial" Then
                        tintItemType = 1
                    End If

                    _objOpportunity = New COpportunities()
                    _objOpportunity.OpportID = CCommon.ToLong(hdnOppID.Value)
                    _objOpportunity.OppItemCode = CCommon.ToLong(hdnOppItemID.Value)
                    _objOpportunity.SaveOppSerialLotNumber(selectedItems, tintItemType)

                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Close", "window.opener.location.href = window.opener.location.href; window.close();", True)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
#End Region

    End Class
End Namespace