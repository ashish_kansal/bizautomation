﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPlangProcurement.aspx.vb"
    Inherits=".frmPlangProcurement" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script src="../Scripts/tooltip/jquery.tooltip.js"></script>
    <link href="../Scripts/tooltip/tooltip.css" rel="stylesheet" />
    <link href="../Scripts/tooltip/tooltip-white.css" rel="stylesheet" />
    <style type="text/css">
        .RadGrid .form-control {
            display: inherit;
        }

        .cont2 {
            background: #fff;
            border: 1px solid #C7D2E4;
            padding: 5px;
        }

        .cont1 {
            background: #E8ECF9;
            border: 1px solid #C7D2E4;
        }

        .list-days {
            white-space: nowrap;
        }

        #gvSearch .badge {
            padding: 3px 7px;
            font-size: 17px;
        }

        #divGrid > div {
            min-height: .01%;
            overflow-x: auto;
        }

        .tip {
            background: #ffc !important;
            padding: 1px 7px !important;
            border: 1px solid #8a8181 !important;
            color: #000 !important;
            font-weight: 600 !important;
            z-index: 10000;
        }

        .innertooltipTitle .headingsmallfont {
            text-decoration: underline;
            font-weight: bold;
        }

        .innertooltipTitle .subheading {
            text-decoration: underline;
        }
    </style>
    <script type="text/javascript">
        function Close() {
            window.close()
            return false;
        }

        function OpenSetting() {
            window.open('../Items/frmConfItemList.aspx?FormID=139', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function WOConfirm() {
            if (confirm('You\'re about to create one or more simple Work Order(s), if you want to add special instructions, completion dates, assignees, etc... you need to click the "Create WO" link on the assembly / BOM line item.')) {
                return true;
            }
            else {
                return false;
            }
        }

        function CeateWO(a, b, c) {
            if (document.getElementById(c).value == "") {
                alert("Enter Units")
                document.getElementById(c).focus();
                return false;
            }

            if (parseFloat(document.getElementById(c).value) <= 0) {
                alert("Units must greater than 0")
                document.getElementById(c).focus();
                return false;
            }

            window.open('../Items/frmManageAssembly.aspx?ItemCode=' + a + '&WItemID=' + b + '&Qty=' + document.getElementById(c).value + "&ReloadParent=1", '', 'toolbar=no,titlebar=no,left=200,top=250,width=750,height=550,scrollbars=yes,resizable=yes');
            return false;
        }

        function SelectAll(headerCheckBox, ItemCheckboxClass) {
            $("." + ItemCheckboxClass + " input[type = 'checkbox']").each(function () {
                $(this).prop('checked', $('[id$=' + headerCheckBox + ']').is(':checked'))
            });
        }

        function SelectionChanged(id) {
            if (id.checked == false) {
                var chkSelectAll = $('input[id$="ChkSelectAll"]');

                if (chkSelectAll != null && chkSelectAll[0].checked == true) {
                    chkSelectAll[0].checked = false;
                }
            }
        }

        function OpenDemandPlanWindow(id) {
            window.open('../DemandForecast/frmNewDemandForecast.aspx?DFID=' + id, '', 'toolbar=no,titlebar=no,top=200,left=200,width=700,height=350,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenPopup(url) {
            url = '../DemandForecast/' + url + '?DFID=' + $("#hdnDFID").val();
            window.open(url, '', 'toolbar=no,titlebar=no,top=100,left=100,width=1255,height=880,scrollbars=yes,resizable=yes');
            return false;
        }


        $(window).unload(function () {
            $.ajax({
                type: "POST",
                url: "common.asmx/ClearDemandForecastSession",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    // Do something interesting here.
                }
            });
        });

        function OpenItem(a) {
            window.open('../Items/frmKitDetails.aspx?ItemCode=' + a, '_blank');
        }

        function OpenHelp() {
            window.open('../Help/Planning_Procurement.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenDFRecords(type, forecastDayes, itemCode, warehouseItemID, analysisDays, isLastYear, opportunityPercentComplete) {
            var h = screen.height;
            var w = screen.width;

            window.open('../DemandForecast/frmDemandForecastDetails.aspx?type=' + type + '&forecastDayes=' + forecastDayes + '&itemCode=' + itemCode + '&warehouseItemID=' + warehouseItemID + '&analysisDays=' + analysisDays + '&isLastYear=' + isLastYear + '&opportunityPercentComplete=' + opportunityPercentComplete, '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenDFReleaseDateRecords(itemCode, warehouseItemID, releaseDate) {
            var h = screen.height;
            var w = screen.width;

            window.open('../DemandForecast/frmDemandForecastReleaseDetails.aspx?itemCode=' + itemCode + '&warehouseItemID=' + warehouseItemID + '&releaseDate=' + releaseDate, '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }

        $("#hplClearGridCondition").click(function () {
            $('#txtGridColumnFilter').val("");
            $('#txtSortColumn').val("");
            $('#btnGo1').trigger('click');
        });


        function ChangedShipVia(ddlShipVia) {
            var tr = $(ddlShipVia).closest('tr');
            

            $.when(BindShippingServices(parseInt($(ddlShipVia).val()), $(tr).find("[id$=ddlShipService]"))).then(function () {
                LoadLeadDays(ddlShipVia, false);
            });

            return false;
        }

        function BindShippingServices(shipVia, ddlShipService) {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/GetShippingServices',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "shipVia": shipVia
                }),
                beforeSend: function () {
                    $("#UpdateProgress").show();
                },
                complete: function () {
                    $("#UpdateProgress").hide();
                },
                success: function (data) {
                    try {
                        $("#ddlShipService").find("option").remove();

                        var obj = $.parseJSON(data.GetShippingServicesResult);
                        if (obj != null && obj.length > 0) {
                            $.each(obj, function (index, value) {
                                $("#ddlShipService").append("<option value='" + value.numShippingServiceID.toString() + "'>" + (value.vcShipmentService || "") + "</option>");
                            });
                        }
                    } catch (err) {
                        alert("Unknown error occurred while loading shipping carrier services.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Unknown error ocurred while loading shipping carrier services.");
                }
            });
        }

        function ChangeShippingService(ddlShipService) {
            var tr = $(ddlShipService).closest('tr');
            LoadLeadDays($(tr).find("[id$=ddlShipVia]"), true);
        }

        function LoadLeadDays(ddlShipVia, isFromShippingServiceChange) {
            var tr = $(ddlShipVia).closest('tr');
            var days = parseInt($(ddlShipVia).val().split("#")[1]);
            var shippingServices = [];
            if ($(ddlShipVia).val().split("#")[2] != "") {
                shippingServices = ($(ddlShipVia).val().split("#")[2]).split(",").map(function (item) {
                    return item.trim();
                });
            }

            if (shippingServices.length > 0 && !isFromShippingServiceChange) {
                isShippingServiceSelected = false;

                shippingServices.forEach(function (item, index) {
                    if (!isShippingServiceSelected && $(tr).find('[id$=ddlShipService] option[value="' + item + '"]').prop("selected", true).length) {
                        isShippingServiceSelected = true;
                    }
                });
            }

            if (days != -1 && (shippingServices.length == 0 || shippingServices.includes($(tr).find("[id$=ddlShipService]").val()))) {
                if ($(tr).find("#lblExpectedDelivery").length > 0) {
                    var format = '<%# Convert.ToString(Session("DateFormat")).ToLower() %>';
                    format = format.replace("mm", "MM");
                    var today = new Date();
                    today.setDate(today.getDate() + days);

                    $(tr).find("#lblExpectedDelivery")[0].innerText = today.format(format);
                }

                if ($(tr).find("#lblLeadDays").length > 0) {
                    $(tr).find("#lblLeadDays")[0].innerText = "(" + days + " Days)";
                }
            } else {
                if ($(tr).find("#lblExpectedDelivery").length > 0) {
                    var format = '<%# Convert.ToString(Session("DateFormat")).ToLower() %>';
                    format = format.replace("mm", "MM");
                    var today = new Date();
                    today.setDate(today.getDate() + 0);

                    $(tr).find("#lblExpectedDelivery")[0].innerText = today.format(format);
                }

                if ($(tr).find("#lblLeadDays").length > 0) {
                    $(tr).find("#lblLeadDays")[0].innerText = "-";
                }
            }

            if ($("#chkIncludeShipVia").is(":checked")) {
                UpdateGrid(true);
            }
        }

        function GetLeadDays(tr) {
            var days = -1; 
            var shippingServices = [];
            if ($(tr).find("[id$=ddlShipVia] option").length > 0) {
                days = parseInt($(tr).find("[id$=ddlShipVia]").val().split("#")[1]);
                if ($(tr).find("[id$=ddlShipVia]").val().split("#")[2] != "") {
                    shippingServices = ($(tr).find("[id$=ddlShipVia]").val().split("#")[2]).split(",").map(function (item) {
                        return item.trim();
                    });
                }
            }

            if (days != -1 && (shippingServices.length == 0 || shippingServices.includes($(tr).find("[id$=ddlShipService]").val()))) {
                return days;
            } else {
                return -1;
            }
        }

        function ChangedVendor(ddlVendor, itemCode, numWarehouseItemID) {
            var tr = $(ddlVendor).closest('tr');
            var qtyToPurchase = 0;

            if ($(tr).find("[id$=txtnumQtyToPurchase]").length > 0) {
                qtyToPurchase = parseFloat($(tr).find("[id$=txtnumQtyToPurchase]").first().val());
            } else {
                qtyToPurchase = parseFloat($(tr).find("#hdnQtyToBuyBasedOnPurchasePlan").first().val());
            }

            $('#UpdateProgress').show();

            $.ajax({
                type: "POST",
                url: "../common/Common.asmx/GetVendorDetailPlanningProcurement",
                data: "{vendorID:" + $(ddlVendor).val() + ",itemCode:" + itemCode + ",numWarehouseItemID:" + numWarehouseItemID + ",numQtyToPurchase:" + qtyToPurchase + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var obj = $.parseJSON(data.d);

                    var vendorCost = 0;
                    var vendors = $.parseJSON(obj.vendors);
                    if (vendors != null && vendors.length > 0) {
                        vendorCost = vendors[0].monCost;
                    }

                    if ($(tr).find("#txtmonCost").length > 0) {
                        $(tr).find("#txtmonCost").first().val(vendorCost);
                    }

                    if ($(tr).find("#hfPrice").length > 0) {
                        $(tr).find("#hfPrice").first().val(vendorCost);
                    }

                    var shipmentMethods = $.parseJSON(obj.shipmentMethods);
                    if ($(tr).find("#ddlShipVia").length > 0) {
                        var ddlShipVia = $(tr).find("#ddlShipVia").first();
                        ddlShipVia.find('option').remove();
                        shipmentMethods.forEach(function (e) {
                            ddlShipVia.append("<option value='" + e.numListItemID + "#" + e.numListValue + "#" + e.vcShippingServices + "'>" + e.vcData + "</option>")
                        });

                        $.when(BindShippingServices(parseInt($(ddlShipVia).val()), $(tr).find("[id$=ddlShipService]"))).then(function () {
                            LoadLeadDays(ddlShipVia,false);
                        });
                    }

                    $('#UpdateProgress').hide();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $('#UpdateProgress').hide();
                    alert(textStatus);
                }
            });

            return false;
        }

        function OpenDemandForecastPOWindow() {
            var h = screen.height;
            var w = screen.width;

            window.open('../DemandForecast/frmDemandForecastPO.aspx', '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenDemandForecastBuildWindow() {
            var h = screen.height;
            var w = screen.width;

            window.open('../Items/frmManageAssembly.aspx', '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }

        function UpdateGrid(isDisplayLoader) {
            if (isDisplayLoader) {
                $('#UpdateProgress').show();
            }

            try {
                if ($("[id$=ddlDemandPlanBasedOn]").val() == "2") {
                    $("#gvSearch > tbody").children("tr").not(":first").each(function () {
                        var tr = $(this);
                        var qtyToPurchase = 0;
                        var leadDays = -1;

                        if ($(this).find("[id$=ddlShipVia]").length > 0) {
                            leadDays = parseInt(GetLeadDays(tr));
                        }

                        if ($(this).find("[id$=txtnumQtyToPurchase]").length > 0) {
                            tr.find("[id$=txtnumQtyToPurchase]").first().val(tr.find("[id$=hdnQtyToBuyBasedOnPurchasePlan]").first().val());
                            qtyToPurchase = parseFloat(tr.find("[id$=txtnumQtyToPurchase]").first().val());
                        } else {
                            qtyToPurchase = parseFloat(tr.find("#hdnQtyToBuyBasedOnPurchasePlan").first().val());
                        }

                        var forecastDays = $(this).find("[id*=aRelease]");
                        forecastDays.each(function (i, obj) {
                            var id = $(obj).attr("id");
                            var releaseDays = parseInt(id.replace("aRelease", ""))
                            var qtyRequired = parseFloat(tr.find("#" + id.replace("aRelease", "hRelease")).first().val());

                            if (!$("#chkIncludeShipVia").is(":checked")) {
                                if (qtyToPurchase < qtyRequired) {
                                    tr.find("#" + id).first().html("<span class=\"badge bg-red\">" + (qtyToPurchase - qtyRequired) + "</span>");
                                } else {
                                    tr.find("#" + id).first().html("<span class=\"badge bg-light-blue\">" + qtyRequired + "</span>");
                                }
                            } else {
                                if (leadDays == -1 || leadDays > releaseDays) {
                                    tr.find("#" + id).first().html("<span class=\"badge bg-red\">" + qtyRequired * -1 + "</span>");
                                } else {
                                    tr.find("#" + id).first().html("<span class=\"badge bg-light-blue\">" + qtyRequired + "</span>");
                                }
                            }
                        });
                    });
                } else {
                    $("#gvSearch > tbody").children("tr").not(":first").each(function () {
                        var tr = $(this);
                        var leadDays = -1;

                        if ($(this).find("[id$=ddlShipVia]").length > 0) {
                            leadDays = parseInt(GetLeadDays(tr));
                        }

                        if ($(this).find("[id$=txtnumQtyToPurchase]").length > 0) {
                            tr.find("[id$=txtnumQtyToPurchase]").first().val(tr.find("[id$=hfQtyToPurchase]").first().val());
                        }

                        var forecastDays = $(this).find("[id*=aRelease]");
                        forecastDays.each(function (i, obj) {
                            var id = $(obj).attr("id");
                            var releaseDays = parseInt(id.replace("aRelease", ""))
                            var qtyRequired = parseFloat(tr.find("#" + id.replace("aRelease", "hRelease")).first().val());

                            if (!$("#chkIncludeShipVia").is(":checked")) {
                                tr.find("#" + id).first().html("<span class=\"badge bg-red\">" + qtyRequired * -1 + "</span>");
                            } else {
                                if (leadDays == -1 || leadDays > releaseDays) {
                                    tr.find("#" + id).first().html("<span class=\"badge bg-red\">" + qtyRequired * -1 + "</span>");
                                } else {
                                    tr.find("#" + id).first().html("<span class=\"badge bg-light-blue\">" + qtyRequired + "</span>");
                                }
                            }
                        });
                    });
                }
            }
            catch (err) {
                alert('Error occurred:' + err.message);
            }
            finally {
                if (isDisplayLoader) {
                    $('#UpdateProgress').hide();
                }
            }
            return false;
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
        });

        function pageLoaded() {
            //$('.tip').tooltip({
            //    position: {
            //        my: "center bottom",
            //        at: "right+10"
            //    }
            //    , forcePosition: true
            //});

            SetSelectedCheckboxes();

            $("#gvSearch > tbody > tr [id$=chkSelect]").change(function () {
                UpdateSelectedProducts($(this));
            });

            $("#chkIncludeShipVia").change(function () {
                UpdateGrid(true);
            });
        }

        function UpdateSelectedProducts(chk) {
            var arrSelectedProducts = [];

            if ($("[id$=hdnSelectedItems]").val() != "") {
                arrSelectedProducts = $.parseJSON($("[id$=hdnSelectedItems]").val());
            }

            var tr = $(chk).closest('tr');

            if (tr != null) {
                var ObjItem = GetItem(tr);

                if ($(chk).is(":checked")) {
                    if (ObjItem.Quantity > 0 && ObjItem.VendorID > 0) {
                        var isItemSelected = false;
                        if (arrSelectedProducts != null && arrSelectedProducts.length > 0) {
                            $.map(arrSelectedProducts, function (item, indexInArray) {
                                if (item.ItemCode == ObjItem.ItemCode && item.VendorID == ObjItem.VendorID && item.WarehouseID == ObjItem.WarehouseID && item.WarehouseItemID == ObjItem.WarehouseItemID && item.Attributes == ObjItem.Attributes) {
                                    isItemSelected = true;
                                }
                            });
                        }
                        if (!isItemSelected) {
                            arrSelectedProducts.push(ObjItem);
                        }
                    }
                } else {
                    if (arrSelectedProducts != null && arrSelectedProducts.length > 0) {
                        arrSelectedProducts = arrSelectedProducts.filter(item => item.ItemCode != ObjItem.ItemCode && item.VendorID != ObjItem.VendorID && item.WarehouseID != ObjItem.WarehouseID && item.WarehouseItemID != ObjItem.WarehouseItemID && item.Attributes != ObjItem.Attributes);
                    }
                }
            }

            if (arrSelectedProducts != null && arrSelectedProducts.length > 0) {
                $("[id$=hdnSelectedItems]").val(JSON.stringify(arrSelectedProducts))
            } else {
                $("[id$=hdnSelectedItems]").val("");
            }
        }

        function GetItem(tr) {
            var ObjItem = new Object();
            ObjItem.ItemCode = parseInt($(tr).find("#hfItemCode").length > 0 ? $(tr).find("#hfItemCode").val() : 0);
            ObjItem.ItemName = $(tr).find("#hfItemName").length > 0 ? $(tr).find("#hfItemName").val() : "";
            if ($(tr).find("#ddlVendor").length > 0) {
                ObjItem.VendorID = parseInt($(tr).find("#ddlVendor").val());
            } else {
                ObjItem.VendorID = parseInt($(tr).find("#hfVendorID").val());
            }
            if ($(tr).find("#txtnumQtyToPurchase").length > 0) {
                ObjItem.Quantity = parseFloat($(tr).find("#txtnumQtyToPurchase").val());
            } else {
                ObjItem.Quantity = parseFloat($(tr).find("#hfQtyToPurchase").val());
            }
            if ($(tr).find("#ddlShipVia").length > 0) {
                ObjItem.ddlShipmentMethod = parseInt($(tr).find("#ddlShipVia").val());
            } else {
                ObjItem.ddlShipmentMethod = 0;
            }
            if ($(tr).find("#ddlShipService").length > 0) {
                ObjItem.ddlShippingService = parseInt($(tr).find("#ddlShipService").val());
            } else {
                ObjItem.ddlShippingService = 0;
            }
            if ($(tr).find("#hfReleaseDate").length > 0) {
                ObjItem.ReleaseDates = $(tr).find("#hfReleaseDate").val();
            } else {
                ObjItem.ReleaseDates = "";
            }
            ObjItem.SKU = $(tr).find("#hfSKU").length > 0 ? $(tr).find("#hfSKU").val() : "";
            ObjItem.Attributes = $(tr).find("#hfAttributes").length > 0 ? $(tr).find("#hfAttributes").val() : "";
            ObjItem.BusinessProcess = $(tr).find("#hfBusinessProcess").length > 0 ? $(tr).find("#hfBusinessProcess").val() : "";
            ObjItem.BusinessProcessID = parseInt($(tr).find("#hfBusinessProcessID").length > 0 ? $(tr).find("#hfBusinessProcessID").val() : 0);
            ObjItem.AverageCost = parseFloat($(tr).find("#hfAverageCost").length > 0 ? $(tr).find("#hfAverageCost").val() : 0);
            ObjItem.BuildManager = $(tr).find("#hfBuildManager").length > 0 ? $(tr).find("#hfBuildManager").val() : "";
            ObjItem.BuildManagerID = parseInt($(tr).find("#hfBuildManagerID").length > 0 ? $(tr).find("#hfBuildManagerID").val() : 0);
            ObjItem.AssetAccountID = parseInt($(tr).find("#hfAssetAccountID").length > 0 ? $(tr).find("#hfAssetAccountID").val() : 0);
            ObjItem.Warehouse = $(tr).find("#hfWarehouse").length > 0 ? $(tr).find("#hfWarehouse").val() : "";
            ObjItem.WarehouseID = parseInt($(tr).find("#hfWarehouseID").length > 0 ? $(tr).find("#hfWarehouseID").val() : 0);
            ObjItem.WarehouseItemID = parseInt($(tr).find("#hfWarehouseItemID").length > 0 ? $(tr).find("#hfWarehouseItemID").val() : 0);
            ObjItem.ItemType = $(tr).find("#hfItemType").length > 0 ? $(tr).find("#hfItemType").val() : "";

            return ObjItem;
        }

        function SetSelectedCheckboxes() {
            if ($("[id$=hdnSelectedItems]").val() != "") {
                arrSelectedProducts = $.parseJSON($("[id$=hdnSelectedItems]").val());

                $("#gvSearch > tbody").children("tr").not(":first").each(function () {
                    var ObjItem = GetItem($(this));

                    var isItemSelected = false;
                    $.map(arrSelectedProducts, function (item, indexInArray) {
                        if (item.ItemCode == ObjItem.ItemCode && item.VendorID == ObjItem.VendorID && item.WarehouseID == ObjItem.WarehouseID && item.WarehouseItemID == ObjItem.WarehouseItemID && item.Attributes == ObjItem.Attributes) {
                            isItemSelected = true;
                        }
                    });

                    if (isItemSelected) {
                        $(this).find("[id$=chkSelect]").prop("checked", true);
                    }
                });
            }

        }

        function OpenAutoCreatePoConfig() {
            window.open('../admin/frmAutoCreatePOConfig.aspx', '', 'toolbar=no,titlebar=no,top=200,left=200,width=700,height=350,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenModalReorderPoint() {
            try {
                $.when(LoadReorderPointConfiguration()).then(function () {
                    $("#modalReorderPoint").modal("show");
                });
            } catch (e) {
                alert("Unknown error ocurred");
            }
        }

        function LoadReorderPointConfiguration() {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/GetReorderPointConfiguration',
                contentType: "application/json",
                dataType: "json",
                beforeSend: function () {
                    $("#UpdateProgress").show();
                },
                complete: function () {
                    $("#UpdateProgress").hide();
                },
                success: function (data) {
                    var obj = $.parseJSON(data.GetReorderPointConfigurationResult);

                    $("#ddlReorderPointDays").val(obj.Days);
                    $("#ddlReorderPointPercent").val(obj.Percent);
                    $("input[name=ReorderPointBasedOn][value=" + (obj.BasedOn || 1) + "]").attr('checked', 'checked');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Unknown error ocurred");
                }
            });
        }

        function SaveReorderPoint() {
            $.when(SaveReorderPoint1(), SaveReorderPoint2(), SaveReorderPoint3()).then(function () {
                $("#modalReorderPoint").modal("hide");
            });
        }

        function SaveReorderPoint1() {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/UpdateSingleFieldValue',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "mode": 54
                    , "updateRecordID": 0
                    , "updateValueID": parseInt($('input[name="ReorderPointBasedOn"]:checked').val())
                    , "comments": ""
                }),
                beforeSend: function () {
                    $("#UpdateProgress").show();
                },
                complete: function () {
                    $("#UpdateProgress").hide();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Unknown error ocurred");
                }
            });
        }

        function SaveReorderPoint2() {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/UpdateSingleFieldValue',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "mode": 55
                    , "updateRecordID": 0
                    , "updateValueID": parseInt($("#ddlReorderPointDays").val())
                    , "comments": ""
                }),
                beforeSend: function () {
                    $("#UpdateProgress").show();
                },
                complete: function () {
                    $("#UpdateProgress").hide();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Unknown error ocurred");
                }
            });
        }

        function SaveReorderPoint3() {
            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/UpdateSingleFieldValue',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "mode": 56
                    , "updateRecordID": 0
                    , "updateValueID": parseInt($("#ddlReorderPointPercent").val())
                    , "comments": ""
                }),
                beforeSend: function () {
                    $("#UpdateProgress").show();
                },
                complete: function () {
                    $("#UpdateProgress").hide();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Unknown error ocurred");
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group">
                        <asp:DropDownList runat="server" ID="ddlDemandPlanBasedOn" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlDemandPlanBasedOn_SelectedIndexChanged" >
                            <asp:ListItem Text="Show item qty release report" Value="1" />
                            <asp:ListItem Text="Use item replenishment plan" Value="2" />
                        </asp:DropDownList>
                    </div>
                </div>

            </div>
            <asp:UpdatePanel ID="UpdatePanelSave" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true" class="pull-right">
                <ContentTemplate>
                    <input type="checkbox" id="chkIncludeShipVia" /><i>&nbsp;<b>Include Ship-Via & Buy (Eaches) in forecast</i></b>
                    <asp:HyperLink ID="hplConfigure" CssClass="btn btn-md btn-primary" runat="server">Item Filter</asp:HyperLink>
                    <asp:Button ID="btnLoadNewOrder" runat="server" CssClass="btn btn-primary" Text="Load Items to Purchase" />
                    <%-- <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" OnClientClick="return GetSelectedItems();" Text="Create Purchase Order(s)" />--%>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnLoadNewOrder" />
                    <%--<asp:PostBackTrigger ControlID="btnSave" />--%>
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Demand Planning&nbsp;<a href="#" onclick="return OpenHelpPopUp('opportunity/frmplangprocurement.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="BizPager" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
        ShowPageIndexBox="Never"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="GridSettingPopup">
    <a href="#" id="hplClearGridCondition" title="Clear All Filters" class="btn-box-tool"><i class="fa fa-2x fa-filter"></i></a>
    <a id="tdGridConfiguration" runat="server" href="#" onclick="return OpenSetting()" title="Show Grid Settings." class="btn-box-tool"><i class="fa fa-2x fa-cog"></i></a>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <ul class="list-unstyled">
                    <li>
                        <asp:RadioButton runat="server" ID="rbPurchasePlan" GroupName="ItemDisplay" AutoPostBack="true" OnCheckedChanged="rbPurchasePlan_CheckedChanged" /><b>Purchase Demand Plan</b></li>
                    <li>
                        <asp:RadioButton runat="server" ID="rbBuildPlan" GroupName="ItemDisplay" AutoPostBack="true" OnCheckedChanged="rbBuildPlan_CheckedChanged" /><b>Build Demand Plan</b></li>
                    <li style="margin-top:10px;">
                        <asp:Label ID="Label112" data-toggle="tooltip"  Text="?" CssClass="tip" runat="server" title="<div  class='innertooltipTitle'>	<h5>Demand Plan: Item Replenishment Formula</h5>	The formula selected here can be used to power both the purchase as and manufacturing <br/>	demand. A purchase demand plan deals with items you buy from vendors, resulting in a <br/>	item quantity recommendation you can use to generate purchase orders, where as a <br/>	manufacturing demand plan deals with finished goods or assemblies resulting in an item <br/>    quantity recommendations you you can use to generate work orders.</div><div  class='innertooltipTitle'>	<h5>Reorder Point & Quantity Formula</h5>	Within the inventory sub-tab of each item record you’ll find the “Reorder Point” value.<br/>	This value is a trigger point used for whichever procurement formula you select (see  <br/>	Global Settings | Order Mgt | Procurement).  The reorder point value can be static (based  <br/>	on whatever value you enter manually which doesn’t change) or dynamic, meaning it will <br/>   fluctuate based on based on the formula configured here. </div>" />
                        &nbsp; <b>Demand Plan :</b>
                        &nbsp; <a href="#" onclick="return OpenAutoCreatePoConfig()">Item Replenishment Formula</a>
                        &nbsp;<a href="javascript:OpenModalReorderPoint();">Re-Order Point & Qty Formula</a>
                    </li>
                </ul>
            </div>
            <div class="pull-right">
                <ul class="list-unstyled">
                    <li>
                        <div class="form-inline padbottom10">
                            <asp:CheckBox runat="server" ID="chkHistoricalValues" AutoPostBack="true" />
                            <b><i>Show historical sales</i></b>
                            <asp:DropDownList ID="ddlAnalysisPatternLastWeek" AutoPostBack="true" CssClass="form-control" runat="server" Font-Bold="true"></asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="chkBasedOnLastYear" runat="server" AutoPostBack="true" /><b><i>Based on sales from last year</i></b>
                        </div>
                    </li>
                    <li>
                        <div class="form-inline">
                            <asp:CheckBox runat="server" ID="chkIncludeOpportunity" AutoPostBack="true" />
                            <b><i>Add quantity impact from sales opportunities with completion %=></i></b>
                            <asp:DropDownList runat="server" ID="ddlOpportunityPercentComplete" CssClass="form-control" AutoPostBack="true">
                                <asp:ListItem Text="50%" Value="50" />
                                <asp:ListItem Text="60%" Value="60" />
                                <asp:ListItem Text="70%" Value="70" />
                                <asp:ListItem Text="80%" Value="80" />
                                <asp:ListItem Text="90%" Value="90" />
                            </asp:DropDownList>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12" id="divGrid">
            <asp:GridView ID="gvSearch" runat="server" CssClass="table table-bordered table-striped" UseAccessibleHeader="true" ShowHeader="true"
                OnRowDataBound="gvItems_RowDataBound" EnableViewState="true" AutoGenerateColumns="false" Width="100%">
                <Columns>
                </Columns>
                <EmptyDataTemplate>
                    No records Found.
                </EmptyDataTemplate>
            </asp:GridView>
        </div>
    </div>
    <div id="modalReorderPoint" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Re-Order Point</h4>
                </div>
                <div class="modal-body">
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <ul class="list-inline">
                                <li><a data-toggle="title" data-placement="bottom" title="Values will automatically set for subscribed items (Check box next to re-order value in item record). Example: Assume 30 days is selected, and an item sold 300 units in that period. Daily average (30) x Primary Vendor lead time (days) which is set from that vendor’s Ship-to address link (assume it’s 30 days) = a Re-Order point of 300. Re-Order Qty would be the 300 at 0% selected, or 330 if 10% is selected (etc.).">?</a><b>Re-Order Point:</b> Based on</li>
                                <li style="padding-left: 0px; padding-right: 0px;">
                                    <select id="ddlReorderPointDays" class="form-control">
                                        <option value="30">30</option>
                                        <option value="60">60</option>
                                        <option value="90">90</option>
                                    </select></li>
                                <li>days of unit sales</li>
                            </ul>
                            <input type="radio" name="ReorderPointBasedOn" value="1" checked="checked" /><label>Period ending this week</label>
                            <br />
                            <input type="radio" name="ReorderPointBasedOn" value="2" /><label>Period ending 1 year ago</label>
                        </div>
                    </div>
                    <div class="row padbottom10">
                        <div class="col-xs-12">
                            <ul class="list-inline">
                                <li><b>Re-Order Qty: </b>Re-Order Point + </li>
                                <li style="padding-left: 0px;">
                                    <select id="ddlReorderPointPercent" class="form-control">
                                        <option value="0">0</option>
                                        <option value="5">5</option>
                                        <option value="10">10</option>
                                    </select>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnSaveReorderPoint" onclick="return SaveReorderPoint();">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnDFID" runat="server" />
    <asp:HiddenField ID="hdnItemIds" runat="server" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:Button ID="btnReleaseBuild" runat="server" Text="" Style="display: none;" />
    <asp:Button ID="btnEstimatedCloseBuild" runat="server" Text="" Style="display: none;" />
    <asp:HiddenField ID="hdnSelectedItems" runat="server" />
</asp:Content>
