﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmItemVendors.aspx.vb" Inherits=".frmItemVendors" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../CSS/bootstrap.min.css" rel="Stylesheet" />
    <link href="../CSS/font-awesome.min.css" rel="Stylesheet" />
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhNGPlxkVhGbVYUw9jEa8HnmS0KXebFEU" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            if ($("[id$=hdnShipPostCode]").val() != "") {

                $("[id$=gvVendor] tr").not(':first').each(function (k, v) {
                    if ($(this).find("select[id$=ddlShipAddress]").find("option").length == 0) {
                        $(this).find("div[id$=divLoading]").html("NA");
                    } else {
                        var postCode = $(this).find("select[id$=ddlShipAddress]").val();
                        postCode = postCode.split("#")[1] || "";

                        
                        calculateDistance(postCode).then(function (response, status) {
                            if (status !== google.maps.DistanceMatrixStatus.OK) {
                                $(this).find("div[id$=divLoading]").html("Not found");
                            } else {
                                if (response.rows[0].elements[0].status == "OK") {
                                    if (response.rows[0].elements[0].distance != null) {
                                        $(v).find("div[id$=divLoading]").html(response.rows[0].elements[0].distance.text);
                                    } else {
                                        $(v).find("div[id$=divLoading]").html("Not found");
                                    }

                                } else {
                                    $(v).find("div[id$=divLoading]").html("Not found");
                                }

                            }
                        });
                    }
                });
            }

            $("select[id$=ddlShipAddress]").change(function () {
                $(select).closest('tr').find("div[id$=divLoading]").html("<i class=\"fa fa-2x fa-refresh fa-spin\"></i>");

                var select = $(this);

                var postCode = $(this).val();
                postCode = postCode.split("#")[1] || "";


                calculateDistance(postCode).then(function (response, status) {
                    if (status !== google.maps.DistanceMatrixStatus.OK) {
                        $(this).find("div[id$=divLoading]").html("Not found");
                    } else {
                        if (response.rows[0].elements[0].status == "OK") {
                            if (response.rows[0].elements[0].distance != null) {
                                $(select).closest('tr').find("div[id$=divLoading]").html(response.rows[0].elements[0].distance.text);
                            } else {
                                $(select).closest('tr').find("div[id$=divLoading]").html("Not found");
                            }

                        } else {
                            $(select).closest('tr').find("div[id$=divLoading]").html("Not found");
                        }

                    }
                });
            });
        });

        function calculateDistance(position) {
            var service = new google.maps.DistanceMatrixService();
            var destination = $("[id$=hdnShipPostCode]").val();
            var dfd = $.Deferred();
            service.getDistanceMatrix({
                origins: [position],
                destinations: [destination],
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.IMPERIAL,
                avoidHighways: false,
                avoidTolls: false
            }, function (response, status) {
                if (status == google.maps.DistanceMatrixStatus.OK)
                    dfd.resolve(response, status);
                else
                    dfd.reject(response, status);
            });
            return dfd.promise();
        }

        function SelectVendor(vendorID,vendorCost) {
            window.opener.document.getElementById("hdnVendorID").value = vendorID;
            window.opener.document.getElementById("hdnVendorCost").value = vendorCost;
            window.opener.document.getElementById("btnSelectedVendor").click();
            self.close();
            return false;
        }

        function openVendorPriceHistory(vendorID) {
            window.open('../opportunity/frmItemVendorPriceHistory.aspx?numItemCode=' + $("[id$=hdnItemCode]").val() + '&numVendorID=' + vendorID, '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="row">
        <div class="col-xs-12 pull-right">
            <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-primary pull-right" OnClientClick="Close();" />
        </div>
        <div class="col-xs-12">
            <asp:Label ID="lblException" runat="server" Text="" ForeColor="Red"></asp:Label>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Item Vendors
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:GridView ID="gvVendor" Width="1200" UseAccessibleHeader="true" DataKeyNames="numVendorID" runat="server" HeaderStyle-BackColor="#ebebeb" HeaderStyle-HorizontalAlign="Center" AutoGenerateColumns="false" CssClass="table table-bordered table-striped">
        <Columns>
            <asp:BoundField DataField="vcCompanyName" HeaderText="Vendor" />
            <asp:BoundField DataField="monVendorCost" HeaderText="Unit Cost / Buy UOM" ItemStyle-Width="135" />
            <asp:TemplateField HeaderText="Vendor Shipping" ItemStyle-Width="200">
                <ItemTemplate>
                    <asp:DropDownList ID="ddlShipAddress" runat="server" Width="198"></asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="monVendorCost" HeaderText="Min. Order Qty" ItemStyle-Width="100" />
            <asp:TemplateField HeaderText="Distance from Ship-to" ItemStyle-Width="145" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:PlaceHolder ID="plhDistance" runat="server"></asp:PlaceHolder>
                    <asp:Label ID="lblDistance" runat="server" Text="" Visible="false"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="vcLastOrder" HeaderText="Last PO Unit Cost (Units Purchased)" HtmlEncode="false" />
            <asp:TemplateField ItemStyle-Width="25">
                <ItemTemplate>
                    <asp:Image ID="imgSelectVendor" runat="server" ImageUrl="~/images/AddRecord.png" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:HiddenField ID="hdnItemCode" runat="server" />
    <asp:HiddenField ID="hdnShipPostCode" runat="server" />
</asp:Content>
