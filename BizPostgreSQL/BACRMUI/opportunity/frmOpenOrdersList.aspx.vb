﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Item
Imports System.Collections.Generic

Namespace BACRM.UserInterface.Opportunities
    Public Class frmOpenOrdersList
        Inherits BACRMPage
        Dim RegularSearch As String
        Dim CustomSearch As String

#Region "PAGE EVENTS"

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                litMessage.Text = ""

                UserCntID = Session("UserContactID")
                DomainID = Session("DomainID")

                If Not IsPostBack Then
                    LoadViewPersistTable() 'LoadPersistTable()

                    'PersistTable.Load()
                    'If PersistTable.Count > 0 Then
                    '    txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                    '    txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                    '    txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))
                    '    txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                    '    txtGridColumnFilter.Text = CCommon.ToString(PersistTable(PersistKey.GridColumnSearch))

                    '    If Not ddlView.Items.FindByValue(CCommon.ToString(PersistTable(ddlView.ID))) Is Nothing Then
                    '        ddlView.ClearSelection()
                    '        ddlView.Items.FindByValue(CCommon.ToString(PersistTable(ddlView.ID))).Selected = True
                    '    End If
                    'End If
                    'Changed by Sachin Sadhu||Date:23rdJune2014
                    'Purpose :Filter OrderStatus as per Order Type
                    objCommon.sb_FillComboFromDBwithSel(ddlOrderStatus, 176, Session("DomainID"), "1")
                    'end of code
                    BindDatagrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

#End Region

#Region "BUTTON EVENTS"

        Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                BindDatagrid(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, DomainID, UserCntID, Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnChangeStatus_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChangeStatus.Click
            Try
                If txtDelOppId.Text <> "" Then
                    Dim strDelOppId() As String = txtDelOppId.Text.Split(",") 'numOppId~numOppBizDocsID~numOppBizDocItemID

                    'Fetch Only Opp ID for changing Status
                    Dim strOppID() As String = New [String](strDelOppId.Count - 1) {}
                    For i As Integer = 0 To strDelOppId.Length - 1
                        strOppID(i) = strDelOppId(i).Split("~")(0)
                    Next

                    'Change Opp Status
                    objCommon = New CCommon
                    objCommon.Mode = 24
                    objCommon.Comments = String.Join(",", strOppID)
                    objCommon.UpdateValueID = ddlOrderStatus.SelectedValue
                    objCommon.UpdateSingleFieldValue()

                    'Check for Shipping Label Batch Process
                    Dim dtConfig As New DataTable
                    Dim objShip As New BusinessLogic.ShioppingCart.ShippingRule
                    With objShip
                        .DomainID = DomainID
                        dtConfig = .GetShippingLabelConfiguration()
                    End With

                    If dtConfig IsNot Nothing AndAlso dtConfig.Rows.Count > 0 Then

                        'If Order Status match with shipping configuration then only process
                        If ddlOrderStatus.SelectedValue = CCommon.ToLong(dtConfig.Rows(0)("numReadyToShipStatus")) Then
                            Dim lstOrders As List(Of String)
                            lstOrders = txtDelOppId.Text.Split(",").ToList()
                            GenerateShippingLabel(lstOrders)
                        End If

                    End If
                End If
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, DomainID, UserCntID, Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnActions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActions.Click

            If ddlActions.SelectedValue = 1 Then
                PrintShippingLabels(txtOppIDs.Text)
            ElseIf ddlActions.SelectedValue = 2 Then
                PrintPickList()
            ElseIf ddlActions.SelectedValue = 3 Then
                PrintPackingSlip()
            End If

            BindDatagrid()

        End Sub

#End Region

#Region "DROP DOWN EVENTS"

        Private Sub ddlView_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlView.SelectedIndexChanged
            Try
                LoadPersistTable()

                BindDatagrid(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, DomainID, UserCntID, Request)
                Response.Write(ex)
            End Try
        End Sub

#End Region

#Region "PRIVATE/PUBLIC METHOD & FUNCTIONS"

        Sub LoadViewPersistTable()
            Try
                PersistTable.Load()
                If PersistTable.Count > 0 Then

                    If Not ddlView.Items.FindByValue(CCommon.ToString(PersistTable(ddlView.ID))) Is Nothing Then
                        ddlView.ClearSelection()
                        ddlView.Items.FindByValue(CCommon.ToString(PersistTable(ddlView.ID))).Selected = True
                    End If

                    If Not ddlActions.Items.FindByValue(CCommon.ToString(PersistTable(ddlActions.ID))) Is Nothing Then
                        ddlActions.ClearSelection()
                        ddlActions.Items.FindByValue(CCommon.ToString(PersistTable(ddlActions.ID))).Selected = True
                    End If

                    If Not ddlOrderStatus.Items.FindByValue(CCommon.ToString(PersistTable(ddlOrderStatus.ID))) Is Nothing Then
                        ddlOrderStatus.ClearSelection()
                        ddlOrderStatus.Items.FindByValue(CCommon.ToString(PersistTable(ddlOrderStatus.ID))).Selected = True
                    End If

                    LoadPersistTable()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadPersistTable()
            Try
                PersistTable.Load(ddlView.SelectedValue)
                If PersistTable.Count > 0 Then
                    txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                    txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                    txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))
                    txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                    txtGridColumnFilter.Text = CCommon.ToString(PersistTable(PersistKey.GridColumnSearch))

                    'If Not ddlActions.Items.FindByValue(CCommon.ToString(PersistTable(ddlActions.ID))) Is Nothing Then
                    '    ddlActions.ClearSelection()
                    '    ddlActions.Items.FindByValue(CCommon.ToString(PersistTable(ddlActions.ID))).Selected = True
                    'End If

                    'If Not ddlOrderStatus.Items.FindByValue(CCommon.ToString(PersistTable(ddlOrderStatus.ID))) Is Nothing Then
                    '    ddlOrderStatus.ClearSelection()
                    '    ddlOrderStatus.Items.FindByValue(CCommon.ToString(PersistTable(ddlOrderStatus.ID))).Selected = True
                    'End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveViewPersistTable()
            Try
                'Persist Form Settings
                PersistTable.Clear()
                PersistTable.Add(ddlView.ID, ddlView.SelectedValue)
                PersistTable.Add(ddlOrderStatus.ID, ddlOrderStatus.SelectedValue)
                PersistTable.Add(ddlActions.ID, ddlActions.SelectedValue)
                PersistTable.Save()

                SavePersistTable()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SavePersistTable()
            Try

                'Persist Form Settings
                PersistTable.Clear()
                PersistTable.Add(PersistKey.CurrentPage, txtCurrrentPage.Text)
                PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())
                PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
                PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text.Trim())
                PersistTable.Add(PersistKey.GridColumnSearch, txtGridColumnFilter.Text)
                'PersistTable.Add(ddlOrderStatus.ID, ddlOrderStatus.SelectedValue)
                'PersistTable.Add(ddlActions.ID, ddlActions.SelectedValue)
                PersistTable.Save(ddlView.SelectedValue)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindDatagrid(Optional ByVal CreateCol As Boolean = True)
            Try

                Dim dtOpportunity As DataTable
                Dim objOpportunity As New COpportunities

                With objOpportunity
                    .UserCntID = UserCntID
                    .DomainID = DomainID
                    .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    .SortCharacter = txtSortChar.Text.Trim()

                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()

                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0

                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If

                    GridColumnSearchCriteria()

                    .RegularSearchCriteria = RegularSearch
                    .CustomSearchCriteria = CustomSearch
                    .ViewID = ddlView.SelectedValue
                End With

                If txtSortColumn.Text <> "" Then
                    objOpportunity.columnName = txtSortColumn.Text
                Else : objOpportunity.columnName = "opp.bintcreateddate"
                End If


                Dim dsList As DataSet

                dsList = objOpportunity.GetOpenOrdersList()
                dtOpportunity = dsList.Tables(0)

                Dim dtTableInfo As DataTable
                dtTableInfo = dsList.Tables(1)

                bizPager.PageSize = Session("PagingRows")
                bizPager.RecordCount = objOpportunity.TotalRecords
                bizPager.CurrentPageIndex = txtCurrrentPage.Text

                SaveViewPersistTable()
                'SavePersistTable()

                Dim i As Integer

                For i = 0 To dtOpportunity.Columns.Count - 1
                    dtOpportunity.Columns(i).ColumnName = dtOpportunity.Columns(i).ColumnName.Replace(".", "")
                Next

                Dim htGridColumnSearch As New Hashtable

                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")
                    Dim strIDValue() As String

                    For i = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")

                        htGridColumnSearch.Add(strIDValue(0), strIDValue(1))
                    Next
                End If

                If CreateCol = True Then
                    Dim Tfield As TemplateField
                    gvSearch.Columns.Clear()

                    For Each drRow As DataRow In dtTableInfo.Rows
                        Tfield = New TemplateField

                        Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, drRow, 0, htGridColumnSearch, 57, objOpportunity.columnName, objOpportunity.columnSortOrder)

                        Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, drRow, 0, htGridColumnSearch, 57, objOpportunity.columnName, objOpportunity.columnSortOrder)
                        gvSearch.Columns.Add(Tfield)
                    Next

                    Dim dr As DataRow
                    dr = dtTableInfo.NewRow()
                    dr("vcAssociatedControlType") = "DeleteCheckBox"
                    dr("intColumnWidth") = "30"

                    Tfield = New TemplateField
                    Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, dr, 0, htGridColumnSearch, 57)
                    Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, dr, 0, htGridColumnSearch, 57)
                    gvSearch.Columns.Add(Tfield)
                End If

                gvSearch.DataSource = dtOpportunity
                gvSearch.DataBind()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub GridColumnSearchCriteria()
            Try
                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")

                    Dim strIDValue(), strID(), strCustom As String
                    Dim strRegularCondition As New ArrayList
                    Dim strCustomCondition As New ArrayList


                    For i As Integer = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")
                        strID = strIDValue(0).Split("~")

                        If strID(0).Contains("CFW.Cust") Then

                            Select Case strID(3).Trim()
                                Case "TextBox"
                                    strCustom = " like '%" & strIDValue(1).Replace("'", "''") & "%'"
                                Case "SelectBox"
                                    strCustom = "=" & strIDValue(1)
                            End Select

                            strCustomCondition.Add("CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & strCustom)
                        Else
                            Select Case strID(3).Trim()
                                Case "Website", "Email", "TextBox"
                                    strRegularCondition.Add(strID(0) & " like '%" & strIDValue(1).Replace("'", "''") & "%'")
                                Case "SelectBox"
                                    If strID(0) = "OL.numWebApiId" Then
                                        If strIDValue(1).StartsWith("Sites~") Then
                                            strRegularCondition.Add("OL.numSiteID=" & strIDValue(1).Replace("Sites~", ""))
                                        Else
                                            strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                                        End If
                                    Else
                                        strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                                    End If
                                Case "TextArea"
                                    strRegularCondition.Add(" Cast(" & strID(0) & " as varchar(5000)) like '%" & strIDValue(1).Replace("'", "''") & "%'")
                            End Select
                        End If
                    Next
                    RegularSearch = String.Join(" and ", strRegularCondition.ToArray())
                    CustomSearch = String.Join(" and ", strCustomCondition.ToArray())
                Else
                    RegularSearch = ""
                    CustomSearch = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

#Region "Shipping Label Generation Method"

        Dim lngOppID As Long = 0
        Dim lngOppBizDocId As Long = 0
        Dim lngBizDocItemId As Long = 0
        Dim objOppBizDocs As New OppBizDocs()
        Dim specialService As Integer = 0

        Public Sub GenerateShippingLabel(ByVal lstOrders As List(Of String))
            Dim strErrorOppID As String = ""
            Dim strSuccessOppID As String = ""

            litMessage.Text = ""
            Try
                Dim dblTotalWeight As Double = 0
                Dim lngShippingReportID As Long = 0

                For Each OrderId As String In lstOrders
                    Dim strOrderDetail As String() = OrderId.Split("~")
                    lngOppID = CCommon.ToLong(strOrderDetail(0))
                    lngOppBizDocId = CCommon.ToLong(strOrderDetail(1))
                    lngBizDocItemId = CCommon.ToLong(strOrderDetail(2))
                    objOppBizDocs.BizDocId = lngOppBizDocId
                    objOppBizDocs.BizDocItemId = lngBizDocItemId
                    objOppBizDocs.ItemClassID = 0
                    objOppBizDocs.DomainID = DomainID

                    Dim objShipping As New Shipping()
                    Dim ds As New DataSet
                    Dim dsFinal As New DataSet
                    Dim intBoxCounter As Integer = 0
                    Dim blnSameCountry As Boolean = False

                    Dim dtBox As New DataTable("Box")
                    dtBox.Columns.Add("vcBoxName")
                    dtBox.Columns.Add("fltTotalWeight")
                    dtBox.Columns.Add("fltHeight")
                    dtBox.Columns.Add("fltWidth")
                    dtBox.Columns.Add("fltLength")
                    dtBox.Columns.Add("numPackageTypeID")
                    dtBox.Columns.Add("numServiceTypeID")

                    Dim dtItem As New DataTable("Item")
                    dtItem.Columns.Add("vcBoxName")
                    dtItem.Columns.Add("numOppBizDocItemID")
                    dtItem.Columns.Add("intBoxQty")
                    dtItem.Columns.Add("UnitWeight")

                    Dim dsItems As New DataSet
                    Dim dtItems As New DataTable
                    Dim dtShipDetail As DataTable = GetOrderItemShippingRules(lngOppID, lngOppBizDocId, DomainID)
                    If dtShipDetail Is Nothing Then
                        If strErrorOppID.Contains(lngOppID) = False Then strErrorOppID = lngOppID & "," & strErrorOppID
                        litMessage.Text = litMessage.Text & "Order ID " & lngOppID & " : " & vbCrLf & " Shipping Rule has not been found." & "<br/>"
                        Continue For
                    End If

                    dsItems.Tables.Add(dtShipDetail.Copy)
                    If dsItems IsNot Nothing AndAlso dsItems.Tables.Count > 0 AndAlso dsItems.Tables(0).Rows.Count > 0 Then

                        If lngOppID > 0 AndAlso lngOppBizDocId > 0 Then

                            dtItems = dsItems.Tables(0).Clone
                            'Dim dtDistinct As DataTable = dsItems.Tables(0).DefaultView.ToTable(True, "numShipClass")
                            For i As Integer = 0 To dsItems.Tables(0).Rows.Count - 1

                                Dim objOpp As New MOpportunity()
                                Dim objCommon As New CCommon()
                                objOpp.OpportunityId = lngOppID
                                objOpp.Mode = 1
                                Dim dtTable As DataTable = Nothing
                                dtTable = objOpp.GetOpportunityAddress()

                                Dim dtShipFrom As DataTable = Nothing
                                Dim objContacts As New CContacts()
                                objContacts.ContactID = UserCntID
                                objContacts.DomainID = DomainID
                                dtShipFrom = objContacts.GetBillOrgorContAdd()
                                If objCommon Is Nothing Then
                                    objCommon = New CCommon()
                                End If

                                ' GET SHIPPING COMPANY INFORMATION
                                Dim dtShipCompany As DataTable = Nothing
                                objOppBizDocs.OppId = lngOppID
                                objOppBizDocs.ShipClassID = CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShipClass"))
                                objOppBizDocs.BizDocItemId = CCommon.ToLong(dsItems.Tables(0).Rows(i)("OppBizDocItemID"))

                                'dtShipCompany = objOppBizDocs.GetShippingRuleInfo()
                                'If dtShipCompany IsNot Nothing AndAlso dtShipCompany.Rows.Count > 0 Then

                                'IF Shipping Detail is not available continue process with another order detail
                                If dtTable.Rows.Count = 0 OrElse dtShipFrom.Rows.Count = 0 Then
                                    Continue For
                                End If

                                If CCommon.ToLong(dtTable.Rows(0)("Country")) = CCommon.ToLong(dtShipFrom.Rows(0)("vcShipCountry")) Then
                                    objOppBizDocs.ShipCompany = CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShippingCompanyID1"))
                                    'objOppBizDocs.Value2 = CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID")) ' CCommon.ToString(dtShipCompany.Rows(0)("numDomesticShipID"))
                                    objOppBizDocs.BoxServiceTypeID = CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID"))
                                Else
                                    objOppBizDocs.ShipCompany = CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShippingCompanyID2"))
                                    'objOppBizDocs.Value2 = CCommon.ToString(dsItems.Tables(0).Rows(i)("InternationalServiceID"))
                                    objOppBizDocs.BoxServiceTypeID = CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID"))
                                End If
                                'End If

                                'Get Order Items and Process Shipping Label Generating Logic
                                If objOppBizDocs Is Nothing Then
                                    objOppBizDocs = New OppBizDocs()
                                End If

                                objOppBizDocs.ShippingReportId = 0
                                objOppBizDocs.DomainID = DomainID
                                objOppBizDocs.OppBizDocId = lngOppBizDocId

                                ' THIRD PARTY BILLING NOT SUPPORTED
                                objOppBizDocs.PayorType = 0
                                objOppBizDocs.PayorAccountNo = "0"
                                objOppBizDocs.PayorCountryCode = "0"
                                objOppBizDocs.PayorZipCode = ""

                                ' FROM DETAIL
                                objOppBizDocs.FromName = CCommon.ToString(dtShipFrom.Rows(0)("vcFirstname")) & " " & CCommon.ToString(dtShipFrom.Rows(0)("vcLastname"))
                                objOppBizDocs.FromCompany = CCommon.ToString(dtShipFrom.Rows(0)("vcCompanyName"))
                                objOppBizDocs.FromPhone = CCommon.ToString(dtShipFrom.Rows(0)("vcPhone"))
                                If dtShipFrom.Rows(0)("vcShipStreet").ToString().Contains(Environment.NewLine) Then
                                    Dim strAddressLines As String() = dtShipFrom.Rows(0)("vcShipStreet").ToString().Split(System.Environment.NewLine) ' dtShipFrom.Rows(0)("vcShipStreet").ToString().Split(New Char() {Convert.ToChar(Environment.NewLine)}, StringSplitOptions.RemoveEmptyEntries)
                                    objOppBizDocs.FromAddressLine1 = (If(strAddressLines.Length >= 0, strAddressLines(0).ToString(), "")).ToString()
                                    objOppBizDocs.FromAddressLine2 = (If(strAddressLines.Length >= 1, strAddressLines(1).ToString(), "")).ToString()
                                Else
                                    objOppBizDocs.FromAddressLine1 = CCommon.ToString(dtShipFrom.Rows(0)("vcShipStreet"))
                                    objOppBizDocs.FromAddressLine2 = ""
                                End If

                                'objOppBizDocs.FromAddressLine1 = dtShipFrom.Rows[0]["vcShipStreet"].ToString().Substring(dtShipFrom.Rows[0]["vcShipStreet"].ToString().IndexOf(Environment.NewLine)).Replace(Environment.NewLine, " ");
                                'objOppBizDocs.FromAddressLine2 = dtShipFrom.Rows[0]["vcShipStreet"].ToString().Substring(0, dtShipFrom.Rows[0]["vcShipStreet"].ToString().IndexOf(Environment.NewLine));
                                objOppBizDocs.FromCountry = CCommon.ToString(dtShipFrom.Rows(0)("vcShipCountry"))
                                objOppBizDocs.FromState = CCommon.ToString(dtShipFrom.Rows(0)("vcShipState"))
                                objOppBizDocs.FromCity = CCommon.ToString(dtShipFrom.Rows(0)("vcShipCity"))
                                objOppBizDocs.FromZip = CCommon.ToString(dtShipFrom.Rows(0)("vcShipPostCode"))
                                objOppBizDocs.IsFromResidential = False

                                objOppBizDocs.UserCntID = UserCntID
                                ' Order Record Owner
                                objOppBizDocs.strText = Nothing

                                'TO DETAILS
                                objOppBizDocs.ToName = CCommon.ToString(dtTable.Rows(0)("Name"))
                                objOppBizDocs.ToCompany = CCommon.ToString(dtTable.Rows(0)("Company"))
                                objOppBizDocs.ToPhone = CCommon.ToString(dtTable.Rows(0)("Phone"))

                                If dtTable.Rows(0)("Street").ToString().Contains(Environment.NewLine) Then
                                    Dim strAddressLines As String() = dtTable.Rows(0)("Street").ToString().Split(System.Environment.NewLine) 'dtTable.Rows(0)("Street").ToString().Split(New Char() {Convert.ToChar(Environment.NewLine)}, StringSplitOptions.RemoveEmptyEntries)
                                    objOppBizDocs.ToAddressLine1 = (If(strAddressLines.Length >= 0, strAddressLines(0).ToString(), "")).ToString()
                                    objOppBizDocs.ToAddressLine2 = (If(strAddressLines.Length >= 1, strAddressLines(1).ToString(), "")).ToString()
                                Else
                                    objOppBizDocs.ToAddressLine1 = CCommon.ToString(dtTable.Rows(0)("Street"))
                                    objOppBizDocs.ToAddressLine2 = ""
                                End If

                                objOppBizDocs.ToCity = CCommon.ToString(dtTable.Rows(0)("City"))
                                objOppBizDocs.ToState = CCommon.ToString(dtTable.Rows(0)("State"))
                                objOppBizDocs.ToCountry = CCommon.ToString(dtTable.Rows(0)("Country"))
                                objOppBizDocs.ToZip = CCommon.ToString(dtTable.Rows(0)("PostCode"))
                                objOppBizDocs.IsToResidential = False

                                ' SPECIAL SERVICES
                                objOppBizDocs.IsCOD = False
                                objOppBizDocs.IsDryIce = False
                                objOppBizDocs.IsHoldSaturday = False
                                objOppBizDocs.IsHomeDelivery = False
                                objOppBizDocs.IsInsideDelivery = False
                                objOppBizDocs.IsInsidePickup = False
                                objOppBizDocs.IsReturnShipment = False
                                objOppBizDocs.IsSaturdayDelivery = False
                                objOppBizDocs.IsSaturdayPickup = False
                                objOppBizDocs.CODAmount = 0
                                objOppBizDocs.CODType = ""
                                objOppBizDocs.TotalInsuredValue = 0
                                objOppBizDocs.IsAdditionalHandling = False
                                objOppBizDocs.LargePackage = False
                                objOppBizDocs.DeliveryConfirmation = ""
                                objOppBizDocs.Description = ""

                                Try
                                    If lngShippingReportID <= 0 Then
                                        lngShippingReportID = objOppBizDocs.ManageShippingReport()
                                    End If

                                Catch ex As Exception
                                    strErrorOppID = objOppBizDocs.OppId & "," & strErrorOppID
                                    Continue For
                                End Try

                                'Update Shipping report address values and create shipping report items 
                                objShipping.DomainID = DomainID
                                objShipping.UserCntID = UserCntID
                                objShipping.ShippingReportId = lngShippingReportID
                                objShipping.OpportunityId = lngOppID
                                objShipping.OppBizDocId = objOppBizDocs.OppBizDocId
                                objShipping.InvoiceNo = CCommon.ToString(objOppBizDocs.OppBizDocId)

                                'If dtShipCompany IsNot Nothing AndAlso dtShipCompany.Rows.Count > 0 Then
                                objShipping.Provider = CCommon.ToInteger(dsItems.Tables(0).Rows(i)("numShippingCompanyID1"))
                                objShipping.PackagingType = CCommon.ToShort(dsItems.Tables(0).Rows(i)("PackageTypeID"))

                                If CCommon.ToLong(dtTable.Rows(0)("Country")) = CCommon.ToLong(dtShipFrom.Rows(0)("vcShipCountry")) Then
                                    objShipping.ServiceType = CCommon.ToShort(dsItems.Tables(0).Rows(i)("DomesticServiceID"))
                                    blnSameCountry = True
                                Else
                                    objShipping.ServiceType = CCommon.ToShort(dsItems.Tables(0).Rows(i)("InternationalServiceID"))
                                End If
                                'End If

                                'FOR FOLLOWING SERVICE TYPES,IN FEDEX THERE IS ONLY ONE "YOUR PACKAGING" IS AVAILABLE
                                If objShipping.Provider = 91 AndAlso _
                                   (objShipping.ServiceType = 15 OrElse objShipping.ServiceType = 16 OrElse _
                                    objShipping.ServiceType = 17 OrElse objShipping.ServiceType = 18 OrElse _
                                    objShipping.ServiceType = 19) Then

                                    objShipping.PackagingType = 21

                                End If
                                '------------------------------------------------------------------------------------

                                ' THIRD PARTY INFO. IS NOT SUPPORTED.
                                objShipping.PayorType = 0
                                objShipping.PayorAccountNo = ""
                                objShipping.PayorCountryCode = "0"
                                objShipping.PayorZipCode = ""

                                Dim dtFields As New DataTable
                                If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                                'dtFields.Columns.Add("numItemCode")
                                dtFields.Columns.Add("numBoxID")
                                dtFields.Columns.Add("tintServiceType")
                                dtFields.Columns.Add("dtDeliveryDate")
                                dtFields.Columns.Add("monShippingRate")
                                dtFields.Columns.Add("fltTotalWeight")
                                dtFields.Columns.Add("intNoOfBox")
                                dtFields.Columns.Add("fltHeight")
                                dtFields.Columns.Add("fltWidth")
                                dtFields.Columns.Add("fltLength")
                                'dtFields.Columns.Add("numOppBizDocItemID")
                                dtFields.TableName = "Box"

                                Dim lngBoxID As Long = 0
                                Dim strBoxID As String = 0
                                Dim lngTotalQty As Long = 0
                                Dim dblWidth As Double = 0
                                Dim dblHeight As Double = 0
                                Dim dblLength As Double = 0
                                Dim dblWeight As Double = 0

                                Dim lngPackageTypeID As Long = 0
                                Dim lngServiceTypeID As Long = 0

                                If dtItems.Select("numShipClass = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShipClass")) & _
                                                  " AND PackageTypeID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("PackageTypeID")) & _
                                                  " AND DomesticServiceID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID")) & _
                                                  " AND InternationalServiceID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("InternationalServiceID"))).Length > 0 Then
                                    Continue For
                                Else
                                    dtItems.Rows.Clear()
                                End If

                                lngTotalQty = 0

                                'ShippingRuleID
                                'DomesticServiceID"
                                'InternationalServiceID"
                                'PackageTypeID"
                                'numShipClass"
                                Dim drClass() As DataRow = dsItems.Tables(0).Select("numShipClass = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShipClass")) & _
                                                                                    " AND PackageTypeID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("PackageTypeID")) & _
                                                                                    " AND DomesticServiceID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID")) & _
                                                                                    " AND InternationalServiceID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("InternationalServiceID")))
                                For Each dr As DataRow In drClass
                                    dtItems.ImportRow(dr)
                                    lngTotalQty = lngTotalQty + CCommon.ToLong(dr("numUnitHour"))
                                Next

                                Dim dblBoxQty As Double
                                Dim dblItemQty As Double
                                Dim dblCarryFwdQty As Double = 0
                                Dim dblQtyToBox As Double = 0
                                Dim dblWeightToBox As Double = 0

                                For iItem As Integer = 0 To dtItems.Rows.Count - 1

                                    'If dtItems.Rows.Count > 1 Then
                                    '    intCarryFwdQty = 0
                                    'End If

                                    objOppBizDocs.OppId = lngOppID
                                    objOppBizDocs.ShipClassID = CCommon.ToLong(dtItems.Rows(iItem)("numShipClass"))
                                    objOppBizDocs.ItemCode = CCommon.ToLong(dtItems.Rows(iItem)("ItemCode"))
                                    objOppBizDocs.UnitHour = IIf(lngTotalQty = 0, CCommon.ToDouble(dtItems.Rows(iItem)("numUnitHour")), lngTotalQty)
                                    objOppBizDocs.PackageTypeID = CCommon.ToLong(dtItems.Rows(iItem)("PackageTypeID"))
                                    objOppBizDocs.DomainID = DomainID
                                    lngPackageTypeID = objOppBizDocs.PackageTypeID
                                    lngServiceTypeID = objShipping.ServiceType

                                    Dim dtPackages As DataTable = Nothing
                                    Dim intBoxCount As Integer = 0
                                    If objShipping.PackagingType <> 21 Then
                                        'ADD ITEMS INTO PACKAGE AS PER PACKAGE RULES
                                        dtPackages = objOppBizDocs.GetPackageInfo()
                                        'If package rule not found then set default 
                                        If dtPackages IsNot Nothing AndAlso dtPackages.Rows.Count > 0 Then
                                            intBoxCount = Math.Ceiling(objOppBizDocs.UnitHour / If(dtPackages.Rows(0)("numFromQty") = 0, 1, CCommon.ToDouble(dtPackages.Rows(0)("numFromQty"))))

                                            dblWeight = CCommon.ToDouble(dtPackages.Rows(0)("fltTotalWeight"))
                                            dblWidth = CCommon.ToDouble(dtPackages.Rows(0)("fltWidth"))
                                            dblHeight = CCommon.ToDouble(dtPackages.Rows(0)("fltHeight"))
                                            dblLength = CCommon.ToDouble(dtPackages.Rows(0)("fltLength"))
                                        Else
                                            If strErrorOppID.Contains(objOppBizDocs.OppId) = False Then strErrorOppID = objOppBizDocs.OppId & "," & strErrorOppID
                                            litMessage.Text = litMessage.Text & "Order ID " & objOppBizDocs.OppId & " : " & vbCrLf & " Package Rule has not been found." & "<br/>"
                                            Continue For
                                        End If
                                    Else
                                        intBoxCount = 1
                                        dblWeight = 1
                                        dblWidth = 2
                                        dblHeight = 2
                                        dblLength = 2
                                    End If

                                    If objOppBizDocs.UnitHour <= 0 Then
                                        If strErrorOppID.Contains(objOppBizDocs.OppId) = False Then strErrorOppID = objOppBizDocs.OppId & "," & strErrorOppID
                                        litMessage.Text = litMessage.Text & "Order ID " & objOppBizDocs.OppId & " : " & "Quantity can not be zero." & "<br/>"
                                        Continue For
                                    End If

                                    ''Create Blank shipping report and use Shipreportid in reference of box
                                    If lngOppBizDocId > 0 And lngOppID > 0 And objOppBizDocs.ShipCompany > 0 AndAlso lngShippingReportID <= 0 Then
                                        With objOppBizDocs
                                            .DomainID = DomainID
                                            .OppBizDocId = lngOppBizDocId
                                            .ShipCompany = objOppBizDocs.ShipCompany
                                            .UserCntID = UserCntID
                                            ds.Tables.Add(New DataTable())
                                            .strText = ds.GetXml()

                                            Try
                                                .ShippingReportId = .ManageShippingReport
                                            Catch ex As Exception
                                                Continue For
                                            End Try


                                            lngShippingReportID = .ShippingReportId
                                        End With

                                    End If

                                    '--------------------------------------------------------------------------------

                                    'For iCnt As Integer = 0 To dtItems.Rows.Count - 1
                                    'Next
                                    'dblWeight = CCommon.ToInteger(dtItems.Rows(iItem)("fltWeight"))
                                    'dblWidth = CCommon.ToInteger(dtItems.Rows(iItem)("fltWidth"))
                                    'dblHeight = CCommon.ToInteger(dtItems.Rows(iItem)("fltHeight"))
                                    'dblLength = CCommon.ToInteger(dtItems.Rows(iItem)("fltLength"))

                                    dblBoxQty = CCommon.ToDouble(dtPackages.Rows(0)("numFromQty"))
                                    dblItemQty = CCommon.ToDouble(dtItems.Rows(iItem)("numUnitHour"))

                                    If lngTotalQty <> CCommon.ToDouble(dtItems.Rows(iItem)("numUnitHour")) Then
                                        dblItemQty = dblCarryFwdQty + CCommon.ToDouble(dtItems.Rows(iItem)("numUnitHour"))
                                    End If

                                    If intBoxCounter = intBoxCount - 1 AndAlso intBoxCounter > 1 Then
                                        Exit For
                                    Else
                                        For intCount As Integer = 0 To intBoxCount - 1

                                            Dim dr As DataRow
                                            If dblItemQty <= 0 Then
                                                If dblItemQty = 0 Then dblCarryFwdQty = 0
                                                intBoxCounter = intBoxCounter - 1
                                                Exit For

                                            ElseIf dblItemQty < dblBoxQty Then
                                                intBoxCounter = intBoxCounter + 1
                                                dblCarryFwdQty = dblItemQty

                                                If dblCarryFwdQty > 0 AndAlso dtItems.Rows.Count <> iItem + 1 Then
                                                    dr = dtItem.NewRow
                                                    dr("vcBoxName") = "Box" & intBoxCounter ' (intCount + i + 1).ToString()
                                                    dr("numOppBizDocItemID") = CCommon.ToLong(dtItems.Rows(iItem)("OppBizDocItemID"))
                                                    dr("intBoxQty") = dblCarryFwdQty
                                                    dr("UnitWeight") = CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")) ' dblWeight

                                                    dtItem.Rows.Add(dr)
                                                    dblTotalWeight = dblTotalWeight + CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")) 'dblWeight

                                                    dblQtyToBox = dblCarryFwdQty
                                                Else
                                                    ''Check for other Shipping Rule / Package Rule available for Remaining Quantity or not.

                                                    Dim dtShipInfoNew As DataTable
                                                    objOppBizDocs.OppId = lngOppID
                                                    objOppBizDocs.OppBizDocId = lngOppBizDocId
                                                    objOppBizDocs.BizDocItemId = lngBizDocItemId
                                                    objOppBizDocs.ShipClassID = CCommon.ToLong(dtItems.Rows(iItem)("numShipClass"))
                                                    objOppBizDocs.UnitHour = dblCarryFwdQty
                                                    dtShipInfoNew = objOppBizDocs.GetShippingRuleInfo()

                                                    ''If available then use that package otherwise same package will be used.
                                                    If dtShipInfoNew IsNot Nothing AndAlso dtShipInfoNew.Rows.Count > 0 Then
                                                        objOppBizDocs.OppId = lngOppID
                                                        objOppBizDocs.ShipClassID = CCommon.ToLong(dtItems.Rows(iItem)("numShipClass"))
                                                        objOppBizDocs.ItemCode = CCommon.ToLong(dtItems.Rows(iItem)("ItemCode"))
                                                        objOppBizDocs.UnitHour = dblCarryFwdQty 'IIf(lngTotalQty = 0, CCommon.ToLong(dtItems.Rows(iItem)("numUnitHour")), lngTotalQty)
                                                        objOppBizDocs.PackageTypeID = CCommon.ToLong(dtShipInfoNew.Rows(0)("numPackageTypeID")) 'objShipping.PackagingType
                                                        objOppBizDocs.DomainID = DomainID

                                                        'ADD ITEMS INTO PACKAGE AS PER PACKAGE RULES
                                                        Dim dtCarryFPackage As DataTable = Nothing
                                                        dtCarryFPackage = objOppBizDocs.GetPackageInfo()
                                                        'If package rule not found then set default 
                                                        If dtPackages IsNot Nothing AndAlso dtCarryFPackage.Rows.Count > 0 Then
                                                            lngPackageTypeID = CCommon.ToLong(dtCarryFPackage.Rows(0)("numCustomPackageID"))
                                                            lngServiceTypeID = If(blnSameCountry = True, CCommon.ToShort(dtShipInfoNew.Rows(0)("numDomesticShipID")), CCommon.ToShort(dtShipInfoNew.Rows(0)("numInternationalShipID")))

                                                            'Else
                                                            '    dr = dtItem.NewRow
                                                            '    dr("vcBoxName") = "Box" & intBoxCounter ' (intCount + i + 1).ToString()
                                                            '    dr("numOppBizDocItemID") = CCommon.ToLong(dtItems.Rows(iCnt)("OppBizDocItemID"))
                                                            '    dr("intBoxQty") = intCarryFwdQty
                                                            '    dr("UnitWeight") = dblWeight
                                                            '    dtItem.Rows.Add(dr)
                                                            '    dblTotalWeight = dblTotalWeight + dblWeight
                                                            '    intQtyToBox = intCarryFwdQty
                                                        End If
                                                    End If


                                                    dr = dtItem.NewRow
                                                    dr("vcBoxName") = "Box" & intBoxCounter ' (intCount + i + 1).ToString()
                                                    dr("numOppBizDocItemID") = CCommon.ToLong(dtItems.Rows(iItem)("OppBizDocItemID"))

                                                    If dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'").Length > 0 Then
                                                        'Dim drQty As DataRow() = dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'")
                                                        Dim intExistBoxQty As Integer
                                                        For Each drQty As DataRow In dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'")
                                                            intExistBoxQty = intExistBoxQty + CCommon.ToDouble(drQty("intBoxQty"))
                                                        Next
                                                        If dblBoxQty <= intExistBoxQty Then
                                                            Continue For
                                                        Else
                                                            dblCarryFwdQty = dblBoxQty - intExistBoxQty
                                                        End If
                                                    End If

                                                    dr("intBoxQty") = dblCarryFwdQty
                                                    dr("UnitWeight") = CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")) 'dblWeight
                                                    dtItem.Rows.Add(dr)

                                                    'dblWeight = CCommon.ToInteger(dtItems.Rows(iItem)("fltWeight"))
                                                    'dblWidth = CCommon.ToInteger(dtItems.Rows(iItem)("fltWidth"))
                                                    'dblHeight = CCommon.ToInteger(dtItems.Rows(iItem)("fltHeight"))
                                                    'dblLength = CCommon.ToInteger(dtItems.Rows(iItem)("fltLength"))

                                                    dblTotalWeight = dblTotalWeight + CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight"))
                                                    'dblTotalWeight = dblTotalWeight + dblWeight
                                                    dblQtyToBox = dblCarryFwdQty
                                                End If

                                                dblWeightToBox = dblQtyToBox * CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")) 'dblWeight
                                            Else

                                                intBoxCounter = intBoxCounter + 1
                                                If intBoxCounter = intBoxCount - 1 AndAlso iItem = dtItems.Rows.Count - 1 Then
                                                    'intCarryFwdQty = 0
                                                End If

                                                dr = dtItem.NewRow
                                                dr("vcBoxName") = "Box" & intBoxCounter '(intCount + i + 1).ToString()
                                                dr("numOppBizDocItemID") = CCommon.ToLong(dtItems.Rows(iItem)("OppBizDocItemID"))

                                                Dim dblExistBoxQty As Double
                                                If dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'").Length > 0 Then

                                                    For Each drQty As DataRow In dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'")
                                                        dblExistBoxQty = dblExistBoxQty + CCommon.ToDouble(drQty("intBoxQty"))
                                                    Next
                                                    If dblBoxQty <= dblExistBoxQty Then
                                                        Continue For
                                                    Else
                                                        dblExistBoxQty = dblBoxQty - dblExistBoxQty
                                                    End If
                                                Else
                                                    dblExistBoxQty = dblBoxQty - dblCarryFwdQty
                                                End If

                                                dr("intBoxQty") = dblExistBoxQty ' intBoxQty - intCarryFwdQty ' If(intItemQty > intBoxQty AndAlso intCarryFwdQty = 0, intBoxQty - intCarryFwdQty, intItemQty - intBoxQty)
                                                dr("UnitWeight") = CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")) 'dblWeight

                                                dtItem.Rows.Add(dr)
                                                dblTotalWeight = dblTotalWeight + CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")) 'dblWeight

                                                dblQtyToBox = dblBoxQty - dblCarryFwdQty

                                                If dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'").Length > 1 Then
                                                    For Each drI As DataRow In dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'")
                                                        dblWeightToBox = dblWeightToBox + (drI("intBoxQty") * drI("UnitWeight"))
                                                    Next
                                                Else
                                                    dblWeightToBox = (dblQtyToBox * CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")))
                                                    'intWeightToBox = (intQtyToBox * dblWeight)
                                                End If

                                            End If

                                            ''Creating Box Entry
                                            'Dim dblFinalWeight As Double
                                            'dblFinalWeight = dblTotalWeight * intQtyToBox 'IIf(intItemQty > intBoxQty, intBoxQty, intItemQty)

                                            dr = dtBox.NewRow
                                            dr("vcBoxName") = "Box" & intBoxCounter '(intCount + i + 1).ToString()
                                            dr("fltTotalWeight") = dblWeightToBox 'CCommon.ToLong(dsItems.Tables(0).Rows(i)("fltWeight"))
                                            dr("fltLength") = dblLength  'CCommon.ToLong(dsItems.Tables(0).Rows(i)("fltLength"))
                                            dr("fltWidth") = dblWidth 'CCommon.ToLong(dsItems.Tables(0).Rows(i)("fltWidth"))
                                            dr("fltHeight") = dblHeight 'CCommon.ToLong(dsItems.Tables(0).Rows(i)("fltHeight"))
                                            dr("numPackageTypeID") = lngPackageTypeID
                                            dr("numServiceTypeID") = lngServiceTypeID


                                            If dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'").Length > 1 Then
                                                dtBox.Rows.Remove(dtBox.Select("vcBoxName='" & "Box" & intBoxCounter & "'")(0))
                                            End If
                                            dtBox.Rows.Add(dr)

                                            dblWeightToBox = 0
                                            dblTotalWeight = 0
                                            dblItemQty = dblItemQty - dblQtyToBox 'intBoxQty
                                        Next
                                    End If
                                Next
                            Next

                            ds.Tables.Add(dtBox)
                            ds.Tables.Add(dtItem)

                            With objOppBizDocs
                                .ShippingReportId = lngShippingReportID
                                .strText = ds.GetXml()
                                .UserCntID = UserCntID
                                .byteMode = 0
                                .PageMode = 1
                                Try
                                    .ManageShippingBox()
                                Catch ex As Exception
                                    If strErrorOppID.Contains(objOppBizDocs.OppId) = False Then strErrorOppID = objOppBizDocs.OppId & "," & strErrorOppID
                                    litMessage.Text = litMessage.Text & "Order ID " & objOppBizDocs.OppId & " : " & objShipping.ErrorMsg & "<br/>"
                                End Try

                            End With

                            'GET BOX DETAILS AS PER SHIPPING REPORT ID
                            dtBox = Nothing
                            If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                            objOppBizDocs.ShippingReportId = lngShippingReportID
                            dtBox = objOppBizDocs.GetShippingBoxes().Tables(0)

                            '-----------------------------------------

                            For i As Integer = 0 To dtBox.Rows.Count - 1

                                ' Add condition by which we can specify whether we use dimensions or not
                                If objShipping.PackagingType <> 21 Then

                                    If objOppBizDocs.ShipCompany = 91 Then
                                        objShipping.UseDimentions = True
                                    Else
                                        objShipping.UseDimentions = False
                                    End If

                                Else
                                    objShipping.UseDimentions = True
                                End If

                                objShipping.Height = CCommon.ToDouble(dtBox.Rows(i)("fltHeight"))
                                objShipping.Length = CCommon.ToDouble(dtBox.Rows(i)("fltLength"))
                                objShipping.Width = CCommon.ToDouble(dtBox.Rows(i)("fltWidth"))
                                objShipping.WeightInLbs = CCommon.ToDouble(dtBox.Rows(i)("fltTotalWeight"))
                                objShipping.WeightInOunce = 0 'CCommon.ToLong(dsItems.Tables(0).Rows(i)("fltWeight"))
                                objShipping.BoxID = CCommon.ToDouble(dtBox.Rows(i)("numBoxID"))

                                If objShipping.WeightInLbs = 0 Then
                                    'litMessage.Text = "Please specify weight of box.";
                                    If strErrorOppID.Contains(objOppBizDocs.OppId) = False Then strErrorOppID = objOppBizDocs.OppId & "," & strErrorOppID
                                    litMessage.Text = litMessage.Text & "Order ID " & objOppBizDocs.OppId & " : " & "Please specify weight of box." & "<br/>"
                                    Continue For
                                End If
                                objShipping.ReferenceNo = ""
                                objShipping.AddPackage()
                                objShipping.ShipperSpecialServices = specialService

                                Try
                                    objShipping.GetShippingLabel()
                                    strSuccessOppID = objOppBizDocs.OppId & "," & strSuccessOppID
                                Catch ex As Exception
                                    If strErrorOppID.Contains(objOppBizDocs.OppId) = False Then strErrorOppID = objOppBizDocs.OppId & "," & strErrorOppID
                                    Continue For
                                End Try

                                dblTotalWeight = 0
                                If Not String.IsNullOrEmpty(objShipping.ErrorMsg) Then
                                    If strErrorOppID.Contains(objOppBizDocs.OppId) = False Then strErrorOppID = objOppBizDocs.OppId & "," & strErrorOppID

                                    If (objShipping.ErrorMsg.Contains("6532")) Then
                                        litMessage.Text = litMessage.Text & "Order ID " & objOppBizDocs.OppId & " : " & "Shipper's phone number is required, your option is to update phone number of primary contact." & "<br/>"
                                    ElseIf (objShipping.ErrorMsg.Contains("6541")) Then
                                        litMessage.Text = litMessage.Text & "Order ID " & objOppBizDocs.OppId & " : " & objShipping.ErrorMsg & "<br/>"
                                    Else
                                        litMessage.Text = litMessage.Text & "Order ID " & objOppBizDocs.OppId & " : " & objShipping.ErrorMsg & "<br/>"
                                    End If
                                End If
                            Next
                        End If
                    End If

                    Try
                        objOppBizDocs.UpdateBizDocsShippingFields()
                    Catch ex As Exception
                        Continue For
                    End Try

                    lngShippingReportID = 0
                Next

                'IF SUCCESS/SHIPPING LABEL IS GENERATED, THEN ORDER WILL BE SET STATUS AS PER CONFIGURED IN SHIPPING LABEL CONFIGURATION 
                If Not String.IsNullOrEmpty(strSuccessOppID) AndAlso strSuccessOppID <> "" Then

                    'Check for Shipping Label Batch Process
                    Dim dtConfig As New DataTable
                    Dim objShip As New BusinessLogic.ShioppingCart.ShippingRule
                    With objShip
                        .DomainID = DomainID
                        dtConfig = .GetShippingLabelConfiguration()
                    End With

                    'Change Opp Status
                    objCommon = New CCommon
                    objCommon.Mode = 24
                    objCommon.Comments = String.Join(",", strSuccessOppID)
                    objCommon.UpdateValueID = CCommon.ToLong(dtConfig.Rows(0)("numSuccessStatus"))
                    objCommon.UpdateSingleFieldValue()

                End If

                'IF ERROR/SHIPPING LABEL IS NOT GENERATED, THEN ORDER WILL BE SET STATUS AS PER CONFIGURED IN SHIPPING LABEL CONFIGURATION 
                If Not String.IsNullOrEmpty(strErrorOppID) AndAlso strErrorOppID <> "" Then

                    'Check for Shipping Label Batch Process
                    Dim dtConfig As New DataTable
                    Dim objShip As New BusinessLogic.ShioppingCart.ShippingRule
                    With objShip
                        .DomainID = DomainID
                        dtConfig = .GetShippingLabelConfiguration()
                    End With

                    'Change Opp Status
                    objCommon = New CCommon
                    objCommon.Mode = 24
                    objCommon.Comments = String.Join(",", strErrorOppID)
                    objCommon.UpdateValueID = CCommon.ToLong(dtConfig.Rows(0)("numErrorStatus"))
                    objCommon.UpdateSingleFieldValue()

                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

#End Region

        Private Function GetOrderItemShippingRules(ByVal OppId As Long, _
                                                   ByVal OppBizDocID As Long, _
                                                   ByVal intDomainId As Integer) As DataTable
            Dim dtResult As DataTable = Nothing
            Try
                Dim dsItems As New DataSet
                Dim dtOrderDetail As New DataTable
                objOppBizDocs = New OppBizDocs

                objOppBizDocs.OppId = OppId
                objOppBizDocs.OppBizDocId = OppBizDocID
                objOppBizDocs.DomainID = DomainID

                dsItems = objOppBizDocs.GetOppInItems()

                'Tracing Part
                '------------------------------------------------------------------------------------------------------
                Trace.Write("Start Tracing Items detail.")
                Trace.Write("Is Item Dataset is initialized or not ? : " & IIf(dsItems Is Nothing, "NO", "YES"))
                Trace.Write("Is Item Dataset has tables or not? : " & IIf(dsItems.Tables.Count > 0, dsItems.Tables.Count, 0))
                Trace.Write("End Tracing Items detail.")
                
                '------------------------------------------------------------------------------------------------------

                If dsItems IsNot Nothing AndAlso dsItems.Tables.Count > 0 AndAlso dsItems.Tables(0).Rows.Count > 0 Then
                    dtOrderDetail.Columns.Add("OppId", GetType(Long))
                    dtOrderDetail.Columns.Add("OppBizDocItemID", GetType(Long))
                    dtOrderDetail.Columns.Add("ItemCode", GetType(Long))
                    dtOrderDetail.Columns.Add("ShippingRuleID", GetType(Long))
                    dtOrderDetail.Columns.Add("numShippingCompanyID1", GetType(Long))
                    dtOrderDetail.Columns.Add("numShippingCompanyID2", GetType(Long))
                    dtOrderDetail.Columns.Add("DomesticServiceID", GetType(Integer))
                    dtOrderDetail.Columns.Add("InternationalServiceID", GetType(Integer))
                    dtOrderDetail.Columns.Add("PackageTypeID", GetType(Integer))
                    dtOrderDetail.Columns.Add("numShipClass", GetType(Long))
                    dtOrderDetail.Columns.Add("numUnitHour", GetType(Long))
                    dtOrderDetail.Columns.Add("fltWeight", GetType(Double))
                    dtOrderDetail.Columns.Add("fltWidth", GetType(Double))
                    dtOrderDetail.Columns.Add("fltHeight", GetType(Double))
                    dtOrderDetail.Columns.Add("fltLength", GetType(Double))

                    Trace.Write("Start Tracing Rules detail.")
                    ' GET SHIPPING RULES AS PER ITEM'S NEEDS
                    Dim dtShipInfo As New DataTable
                    For Each drItem As DataRow In dsItems.Tables(0).Rows
                        objOppBizDocs.OppId = lngOppID
                        objOppBizDocs.OppBizDocId = OppBizDocID
                        objOppBizDocs.BizDocItemId = CCommon.ToLong(drItem("OppBizDocItemID"))
                        objOppBizDocs.ShipClassID = CCommon.ToLong(drItem("numShipClass"))
                        objOppBizDocs.UnitHour = CCommon.ToLong(drItem("numUnitHour"))
                        dtShipInfo = objOppBizDocs.GetShippingRuleInfo()

                        Trace.Write("Shipping Rule dataset available or not: " & If(dtShipInfo Is Nothing, "NO", "YES, Then its row count is :" & dtShipInfo.Rows.Count))

                        If dtShipInfo IsNot Nothing AndAlso dtShipInfo.Rows.Count > 0 Then
                            Dim drOrderDetail As DataRow
                            drOrderDetail = dtOrderDetail.NewRow

                            drOrderDetail("OppId") = lngOppID
                            drOrderDetail("OppBizDocItemID") = CCommon.ToLong(drItem("OppBizDocItemID"))
                            drOrderDetail("ItemCode") = CCommon.ToLong(drItem("ItemCode"))
                            drOrderDetail("ShippingRuleID") = CCommon.ToLong(dtShipInfo.Rows(0)("numShippingRuleID"))
                            drOrderDetail("numShippingCompanyID1") = CCommon.ToInteger(dtShipInfo.Rows(0)("numShippingCompanyID1"))
                            drOrderDetail("numShippingCompanyID2") = CCommon.ToInteger(dtShipInfo.Rows(0)("numShippingCompanyID2"))

                            drOrderDetail("DomesticServiceID") = CCommon.ToInteger(dtShipInfo.Rows(0)("numDomesticShipID"))
                            drOrderDetail("InternationalServiceID") = CCommon.ToInteger(dtShipInfo.Rows(0)("numInternationalShipID"))
                            drOrderDetail("PackageTypeID") = CCommon.ToInteger(dtShipInfo.Rows(0)("numPackageTypeID"))
                            drOrderDetail("numShipClass") = CCommon.ToLong(drItem("numShipClass"))
                            drOrderDetail("numUnitHour") = CCommon.ToLong(drItem("numUnitHour"))

                            drOrderDetail("fltWeight") = CCommon.ToDouble(drItem("fltWeight"))
                            drOrderDetail("fltWidth") = CCommon.ToDouble(drItem("fltWidth"))
                            drOrderDetail("fltHeight") = CCommon.ToDouble(drItem("fltHeight"))
                            drOrderDetail("fltLength") = CCommon.ToDouble(drItem("fltLength"))

                            dtOrderDetail.Rows.Add(drOrderDetail)
                            dtOrderDetail.AcceptChanges()
                        Else
                            dtOrderDetail = Nothing
                        End If

                    Next

                Else
                    dtResult = Nothing
                End If

                Trace.Write("End Tracing Rules Detail.")

                If dtOrderDetail IsNot Nothing Then
                    Dim dtView As DataView = dtOrderDetail.DefaultView
                    dtView.Sort = "numShipClass"
                    dtResult = dtView.ToTable()
                Else
                    dtResult = Nothing
                End If

            Catch ex As Exception
                dtResult = Nothing
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                'Response.Write(ex)
            End Try

            Return dtResult
        End Function

        Public Sub PrintShippingLabels(ByVal strOrderIDs As String)

            Dim objOppBizDocs As New OppBizDocs()
            Dim dsShippingLabels As New DataSet

            objOppBizDocs.BoxID = 0
            objOppBizDocs.tintMode = 1
            objOppBizDocs.strOrderID = strOrderIDs
            objOppBizDocs.DomainID = DomainID
            dsShippingLabels = objOppBizDocs.GetShippingLabelList()

            If dsShippingLabels IsNot Nothing AndAlso dsShippingLabels.Tables.Count > 0 AndAlso dsShippingLabels.Tables(0).Rows.Count > 0 Then
                Session("ShippingLabelImages") = dsShippingLabels
                Dim dtShip As New DataTable
                dtShip = dsShippingLabels.Tables(0).DefaultView.ToTable(True, "numShippingCompany")

                For Each drRow As DataRow In dtShip.Rows
                    Dim strService As String
                    If CCommon.ToLong(drRow("numShippingCompany")) = 91 Then
                        strService = "Fedex"
                    ElseIf CCommon.ToLong(drRow("numShippingCompany")) = 88 Then
                        strService = "UPS"
                    ElseIf CCommon.ToLong(drRow("numShippingCompany")) = 90 Then
                        strService = "USPS"
                    End If
                    ClientScript.RegisterStartupScript(Me.GetType, "Printing Shipping Labels :" & strService, "OpenPrintedShippingLabels(" & CCommon.ToLong(drRow("numShippingCompany")) & ");", True)
                Next
                'ClientScript.RegisterStartupScript(Me.GetType, "Printing Shipping Labels : Fedex", "OpenPrintedShippingLabels(91)", True)
                'ClientScript.RegisterStartupScript(Me.GetType, "Printing Shipping Labels : UPS", "OpenPrintedShippingLabels(88)", True)
                'ClientScript.RegisterStartupScript(Me.GetType, "Printing Shipping Labels : USPS", "OpenPrintedShippingLabels(90)", True)

            Else
                litMessage.Text = "No Shipping Labels found for selected orders."
            End If

        End Sub

        Private Sub PrintPickList()
            'Try
            If (txtPickOppIDs.Text <> "") Then
                'SavePersistTable()

                Dim strPickIds() As String = {""}
                Dim strOrderIds As String = ""
                Dim strOppItemCodes As String = ""

                txtPickOppIDs.Text = txtPickOppIDs.Text.Trim(",")
                strPickIds = txtPickOppIDs.Text.Split(",")

                For i As Integer = 0 To strPickIds.Length - 1
                    strOrderIds = strOrderIds & IIf(strPickIds(i).Split("~").Length > 0, strPickIds(i).Split("~")(0), "") & ","
                    strOppItemCodes = strOppItemCodes & IIf(strPickIds(i).Split("~").Length = 0 Or strPickIds(i).Split("~")(1) > 0, strPickIds(i).Split("~")(1), "") & ","
                Next

                strOppItemCodes = strOppItemCodes.Trim(",")
                strOrderIds = strOrderIds.Trim(",")

                Dim objOpp As New COpportunities
                Dim ds As DataSet
                objOpp.DomainID = Session("DomainID")
                objOpp.strItemDtls = strOppItemCodes
                objOpp.strOrderID = strOrderIds
                objOpp.BizDocId = enmBizDocTemplate_BizDocID.Packing_Slip_Template
                ds = objOpp.GetSFItemsforImport

                Dim dt As DataTable = ds.Tables(0)

                'To fix the bug : 1795
                If Not dt.Columns.Contains("SrNo") Then
                    Dim column As DataColumn = New DataColumn("SrNo")
                    column.DataType = System.Type.GetType("System.Int32")
                    With column
                        .AutoIncrement = True
                        .AutoIncrementSeed = 1
                        .AutoIncrementStep = 1
                    End With
                    dt.Columns.Add(column)
                    dt.Columns("SrNo").SetOrdinal(0)
                End If
                dt.AcceptChanges()

                Dim dr1 As DataRow
                Dim intCnt As Integer = 0
                For index As Integer = 0 To dt.Rows.Count - 1
                    dr1 = dt.Rows(index)
                    If Not CCommon.ToInteger(dr1("Qty To Pick")) > 0 Then
                        dr1.Delete()
                    Else
                        intCnt = intCnt + 1
                        dr1("SrNo") = intCnt
                    End If
                Next
                dt.Columns.Remove("numWareHouseItemID")
                dt.AcceptChanges()

                dg.DataSource = dt
                dg.DataBind()
                ExportToExcel.DataGridToExcel(dg, Response)
            Else
                BindDatagrid()
            End If

        End Sub

        Private Sub PrintPackingSlip()
            'Try
            If (txtPickOppIDs.Text <> "") Then
                Dim strPickIds() As String = {""}
                Dim strOrderIds As String = ""
                Dim strOppItemCodes As String = ""

                txtPickOppIDs.Text = txtPickOppIDs.Text.Trim(",")
                strPickIds = txtPickOppIDs.Text.Split(",")

                For i As Integer = 0 To strPickIds.Length - 1
                    strOrderIds = strOrderIds & IIf(strPickIds(i).Split("~").Length > 0, strPickIds(i).Split("~")(0), "") & ","
                    strOppItemCodes = strOppItemCodes & IIf(strPickIds(i).Split("~").Length = 0 Or strPickIds(i).Split("~")(1) > 0, strPickIds(i).Split("~")(1), "") & ","
                Next

                strOppItemCodes = strOppItemCodes.Trim(",")
                strOrderIds = strOrderIds.Trim(",")

                Dim objOpp As New COpportunities
                Dim ds As DataSet
                objOpp.DomainID = Session("DomainID")
                objOpp.strItemDtls = strOppItemCodes
                objOpp.strOrderID = strOrderIds
                objOpp.BizDocId = enmBizDocTemplate_BizDocID.Packing_Slip_Template
                ds = objOpp.GetSFItemsforImport

                Dim dt As DataTable = ds.Tables(1)
                If dt.Rows.Count > 0 Then
                    Dim objPDF As New HTMLToPDF

                    Dim intResult As Integer = objPDF.CreatePackingSlipExportPdf(dt)
                    If intResult = 101 Then
                        litMessage.Text = "Please enable ""Packing Slip"" BizDoc from ""Administration->Global Settings->Order Management(subtab)->BizDoc Template"", as well Set One template as default template."
                    ElseIf intResult = 102 Then
                        litMessage.Text = "Please configure Packing Slip line items from ""Administration->BizForm Wizard->BizDoc(Sales)""."
                    ElseIf intResult = 103 Then
                        litMessage.Text = " Please contact support by creating bug or feedback with following error : ""Invalid page break in template."""
                    End If

                    BindDatagrid()
                End If
            Else
                BindDatagrid()
            End If

        End Sub

#End Region


    End Class
End Namespace
