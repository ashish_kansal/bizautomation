﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmSelectPickList.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmSelectPickList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="pull-right">
    <asp:Button ID="btnSaveClose" CssClass="btn btn-primary" runat="server" Text="Save & Close" />
    <asp:Button ID="btnClose" CssClass="btn btn-primary" runat="server" Text="Close" OnClientClick="return Close();" />
        </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Select Pick List
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <div class="alert alert-danger" id="divException" runat="server" style="display:none">
        <h4><i class="icon fa fa-ban"></i>Alert!</h4>
        <asp:Label ID="lblException" runat="server" Text=""></asp:Label>
    </div>
    <div class="alert alert-info">
        <h4><i class="icon fa fa-info"></i>Alert!</h4>
        <p>"Allocate inventory from Pick Lists" option is checked in customer default settings. Select Pick List to add fulfillment bizdoc.</p>
    </div>
    <p>
        <asp:RadioButtonList ID="rblPickLists" runat="server"></asp:RadioButtonList>
    </p>
    <asp:HiddenField runat="server" ID="hdnOppID" />
</asp:Content>
