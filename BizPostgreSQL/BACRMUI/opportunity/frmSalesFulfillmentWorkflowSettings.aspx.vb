﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Accounting

Public Class frmSalesFulfillmentWorkflowSettings
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            'CLEAR ALERT MESSAGE
            divMessage.Style.Add("display", "none")
            litMessage.Text = ""

            If Not IsPostBack Then
                BindData()
                BindRuleData()
                BindColorGrid()
                btnSave.Attributes.Add("onclick", "return Save()")
                btnAdd.Attributes.Add("onclick", "return SaveColor()")
            End If

            Dim eventTarget As String = If(Page.Request("__EVENTTARGET") = Nothing, String.Empty, Page.Request("__EVENTTARGET"))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub

    Sub BindData()
        Try
            Dim dtOrderStatus As DataTable = objCommon.GetMasterListItems(176, Session("DomainID"), 1, 2)
            FillOrderStatus(ddlRule2OrderStatus, dtOrderStatus)
            FillOrderStatus(ddlRule2SuccessOrderStatus, dtOrderStatus)
            FillOrderStatus(ddlRule2FailOrderStatus, dtOrderStatus)
            FillOrderStatus(ddlRule3OrderStatus, dtOrderStatus)
            FillOrderStatus(ddlRule3FailOrderStatus, dtOrderStatus)
            FillOrderStatus(ddlRule4OrderStatus, dtOrderStatus)
            FillOrderStatus(ddlRule4FailOrderStatus, dtOrderStatus)
            FillOrderStatus(ddlRule5OrderStatus, dtOrderStatus)
            FillOrderStatus(ddlRule6OrderStatus, dtOrderStatus)
            FillOrderStatus(ddlColorOrderStatus, dtOrderStatus)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub FillOrderStatus(ByRef ddlOrderStatus As DropDownList, ByVal dtOrderStatus As DataTable)
        Try
            ddlOrderStatus.DataSource = dtOrderStatus
            ddlOrderStatus.DataTextField = "vcData"
            ddlOrderStatus.DataValueField = "numListItemID"
            ddlOrderStatus.DataBind()
            ddlOrderStatus.Items.Insert(0, "--Select One--")
            ddlOrderStatus.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindRuleData()
        Try
            Dim objSalesFulfillmentConfiguration As New SalesFulfillmentConfiguration
            objSalesFulfillmentConfiguration.DomainID = Session("DomainID")
            Dim dt As DataTable = objSalesFulfillmentConfiguration.GetByDomainID()

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                chkActivate.Checked = CCommon.ToBool(dt.Rows(0)("bitActive"))
                chkActivateRule2.Checked = CCommon.ToBool(dt.Rows(0)("bitRule2IsActive"))
                chkActivateRule3.Checked = CCommon.ToBool(dt.Rows(0)("bitRule3IsActive"))
                chkActivateRule4.Checked = CCommon.ToBool(dt.Rows(0)("bitRule4IsActive"))
                chkActivateRule5.Checked = CCommon.ToBool(dt.Rows(0)("bitRule5IsActive"))
                chkActivateRule6.Checked = CCommon.ToBool(dt.Rows(0)("bitRule6IsActive"))
                ddlRule2OrderStatus.SelectedValue = CCommon.ToLong(dt.Rows(0)("numRule2OrderStatus"))
                ddlRule2SuccessOrderStatus.SelectedValue = CCommon.ToLong(dt.Rows(0)("numRule2SuccessOrderStatus"))
                ddlRule2FailOrderStatus.SelectedValue = CCommon.ToLong(dt.Rows(0)("numRule2FailOrderStatus"))
                ddlRule3OrderStatus.SelectedValue = CCommon.ToLong(dt.Rows(0)("numRule3OrderStatus"))
                ddlRule3FailOrderStatus.SelectedValue = CCommon.ToLong(dt.Rows(0)("numRule3FailOrderStatus"))
                ddlRule4OrderStatus.SelectedValue = CCommon.ToLong(dt.Rows(0)("numRule4OrderStatus"))
                ddlRule4FailOrderStatus.SelectedValue = CCommon.ToLong(dt.Rows(0)("numRule4FailOrderStatus"))
                ddlRule5OrderStatus.SelectedValue = CCommon.ToLong(dt.Rows(0)("numRule5OrderStatus"))
                ddlRule6OrderStatus.SelectedValue = CCommon.ToLong(dt.Rows(0)("numRule6OrderStatus"))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            Dim numRule1OrderStatus As Long = -1
            Dim numRule2OrderStatus As Long = -1
            Dim numRule3OrderStatus As Long = -1
            Dim numRule4OrderStatus As Long = -1


            If chkActivateRule2.Checked Then
                If ddlRule2OrderStatus.SelectedValue = "0" Then
                    ShowMessage("Select order status to trigger rule 2.")
                    Exit Sub
                ElseIf ddlRule2OrderStatus.SelectedValue = numRule1OrderStatus Then
                    ShowMessage("Order status to trigger rule 2 is same as rule 1. Select unique status for each rule.")
                    Exit Sub
                End If

                numRule2OrderStatus = ddlRule2OrderStatus.SelectedValue
            End If

            If chkActivateRule3.Checked Then
                If ddlRule3OrderStatus.SelectedValue = "0" Then
                    ShowMessage("Select order status to trigger rule 3.")
                    Exit Sub
                ElseIf ddlRule3OrderStatus.SelectedValue = numRule1OrderStatus Then
                    ShowMessage("Order status to trigger rule 3 is same as rule 1. Select unique status for each rule.")
                    Exit Sub
                ElseIf ddlRule3OrderStatus.SelectedValue = numRule2OrderStatus Then
                    ShowMessage("Order status to trigger rule 3 is same as rule 2. Select unique status for each rule.")
                    Exit Sub
                End If

                numRule3OrderStatus = ddlRule3OrderStatus.SelectedValue

                'validate account mapping
                Dim UndepositedFundAccountID As Long = ChartOfAccounting.GetDefaultAccount("UF", Session("DomainID")) 'Undeposited Fund
                If Not UndepositedFundAccountID > 0 Then
                    ShowMessage("Please Map Default Undeposited Funds Account for Your Company from ""Administration->Global Settings->Accounting->Default Accounts"".")
                    Exit Sub
                End If

                Dim lngDefaultAPAccount As Long = ChartOfAccounting.GetDefaultAccount("PL", Session("DomainID"))
                If Not lngDefaultAPAccount > 0 Then
                    ShowMessage("Please Map Default Payroll Liablity Account for Your Company from ""Administration->Global Settings->Accounting->Default Accounts"".")
                    Exit Sub
                End If

                Dim lngEmpPayRollAccount As Long = ChartOfAccounting.GetDefaultAccount("EP", Session("DomainID"))
                If Not lngEmpPayRollAccount > 0 Then
                    ShowMessage("Please Map Default Employee Payroll Expense Account for Your Company from ""Administration->Global Settings->Accounting->Default Accounts"" .")
                    Exit Sub
                End If

                Dim lngDefaultForeignExchangeAccountID As Long = ChartOfAccounting.GetDefaultAccount("FE", Session("DomainID"))
                If lngDefaultForeignExchangeAccountID = 0 Then
                    ShowMessage("Please Map Default Foreign Exchange Gain/Loss Account for Your Company from ""Administration->Global Settings->Accounting->Default Accounts"" .")
                    Exit Sub
                End If
            End If

            If chkActivateRule4.Checked Then
                If ddlRule4OrderStatus.SelectedValue = "0" Then
                    ShowMessage("Select order status to trigger rule 4.")
                    Exit Sub
                ElseIf ddlRule4OrderStatus.SelectedValue = numRule1OrderStatus Then
                    ShowMessage("Order status to trigger rule 4 is same as rule 1. Select unique status for each rule.")
                    Exit Sub
                ElseIf ddlRule4OrderStatus.SelectedValue = numRule2OrderStatus Then
                    ShowMessage("Order status to trigger rule 4 is same as rule 2. Select unique status for each rule.")
                    Exit Sub
                ElseIf ddlRule4OrderStatus.SelectedValue = numRule3OrderStatus Then
                    ShowMessage("Order status to trigger rule 4 is same as rule 3. Select unique status for each rule.")
                    Exit Sub
                End If

                numRule4OrderStatus = ddlRule4OrderStatus.SelectedValue
            End If

            Save()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        Try
            Dim objField As New FormConfigWizard()
            objField.DomainID = Session("DomainID")
            objField.FormID = 64

            objField.FormFieldId = 0
            objField.strFieldValue = ddlColorOrderStatus.SelectedValue
            objField.strFieldValue1 = 0
            objField.strColorScheme = ddlColorScheme.SelectedValue

            objField.tintType = 1
            objField.ManageDycFieldColorScheme()

            ShowMessage("Color scheme added!")

            BindColorGrid()
        Catch ex As Exception
            If ex.Message = "Field_DEPENDANT" Then
                litMessage.Text = "Some other field already used for this Form."
            ElseIf ex.Message = "Value_DEPENDANT" Then
                litMessage.Text = "Filed value already exists."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End If
        End Try
    End Sub

    Sub BindColorGrid()
        Try

            Dim dtTable As DataTable
            Dim objField As New FormConfigWizard()
            objField.DomainID = Session("DomainID")
            objField.FormID = 64
            objField.tintType = 3
            dtTable = objField.GetDycFieldColorScheme

            gvColorScheme.DataSource = dtTable
            gvColorScheme.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub gvColorScheme_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvColorScheme.RowCommand
        Try
            If e.CommandName = "Delete" Then
                Dim objField As New FormConfigWizard()
                objField.DomainID = Session("DomainID")
                objField.FormID = 64
                objField.FieldColorSchemeID = e.CommandArgument

                objField.tintType = 2
                objField.ManageDycFieldColorScheme()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvColorScheme_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvColorScheme.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                e.Row.Cells(0).CssClass = DataBinder.Eval(e.Row.DataItem, "vcColorScheme")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub Save()
        Try
            Dim objSalesFulfillmentConfiguration As New SalesFulfillmentConfiguration
            objSalesFulfillmentConfiguration.DomainID = Session("DomainID")
            objSalesFulfillmentConfiguration.UserCntID = Session("UserContactID")
            objSalesFulfillmentConfiguration.IsActive = chkActivate.Checked
            objSalesFulfillmentConfiguration.IsRule2Active = chkActivateRule2.Checked
            objSalesFulfillmentConfiguration.IsRule3Active = chkActivateRule3.Checked
            objSalesFulfillmentConfiguration.IsRule4Active = chkActivateRule4.Checked
            objSalesFulfillmentConfiguration.IsRule5Active = chkActivateRule5.Checked
            objSalesFulfillmentConfiguration.IsRule6Active = chkActivateRule6.Checked

            If chkActivateRule2.Checked Then
                objSalesFulfillmentConfiguration.Rule2OrderStatus = ddlRule2OrderStatus.SelectedValue
                objSalesFulfillmentConfiguration.Rule2SuccessOrderStatus = ddlRule2SuccessOrderStatus.SelectedValue
                objSalesFulfillmentConfiguration.Rule2FailOrderStatus = ddlRule2FailOrderStatus.SelectedValue
            End If

            If chkActivateRule3.Checked Then
                objSalesFulfillmentConfiguration.Rule3OrderStatus = ddlRule3OrderStatus.SelectedValue
                objSalesFulfillmentConfiguration.Rule3FailOrderStatus = ddlRule3FailOrderStatus.SelectedValue
            End If

            If chkActivateRule4.Checked Then
                objSalesFulfillmentConfiguration.Rule4OrderStatus = ddlRule4OrderStatus.SelectedValue
                objSalesFulfillmentConfiguration.Rule4FailOrderStatus = ddlRule4FailOrderStatus.SelectedValue
            End If

            If chkActivateRule5.Checked Then
                objSalesFulfillmentConfiguration.Rule5OrderStatus = ddlRule5OrderStatus.SelectedValue
            End If

            If chkActivateRule6.Checked Then
                objSalesFulfillmentConfiguration.Rule6OrderStatus = ddlRule6OrderStatus.SelectedValue
            End If

            objSalesFulfillmentConfiguration.Save()
        Catch ex As Exception
            If ex.Message.Contains("WORKFLOW AUTOMATION RULE EXISTS") Then
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "AutomationError", "alert('Automation rules with sales Order statuses used as triggers in these rules exists. Remove rules you have set up within Administration | Workflow Automation | Automation Rules.');", True)
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End If
        End Try
    End Sub

    Private Sub ShowMessage(ByVal message As String)
        Try
            divMessage.Style.Add("display", "")
            litMessage.Text = message
            divMessage.Focus()
        Catch ex As Exception
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub gvColorScheme_RowDeleted(sender As Object, e As GridViewDeletedEventArgs) Handles gvColorScheme.RowDeleted
        Try
            BindRuleData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvColorScheme_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gvColorScheme.RowDeleting

    End Sub
End Class