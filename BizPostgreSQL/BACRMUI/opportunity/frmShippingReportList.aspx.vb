﻿Imports System.Data
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Partial Public Class frmShippingReportList
    Inherits BACRMPage
    Dim lngOppID As Long
    Dim lngOppBizDocID As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngOppID = GetQueryStringVal( "OppId")
            lngOppBizDocID = GetQueryStringVal( "OppBizDocId")
            If Not Page.IsPostBack Then
                BindShippingReportList()
                
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub BindShippingReportList()
        Try
            Dim objOppBizDocs As New OppBizDocs
            Dim ds As New DataSet
            objOppBizDocs.DomainID = Session("DomainID")
            objOppBizDocs.OppId = lngOppID
            objOppBizDocs.OppBizDocId = lngOppBizDocID
            ds = objOppBizDocs.GetShippingReportList
            dgShippingReportList.DataSource = ds.Tables(0)
            dgShippingReportList.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub dgShippingReportList_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgShippingReportList.ItemCommand
        Try
            If e.CommandName = "Delete" Then
                Dim objOppBizDocs As New OppBizDocs
                Dim ds1 As New DataSet
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.OppBizDocId = lngOppBizDocID
                objOppBizDocs.ShippingReportId = e.CommandArgument
                ds1 = objOppBizDocs.GetShippingReport

                For Each dr As DataRow In ds1.Tables(0).Rows
                    'delete shipping label image file
                    If System.IO.File.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & CCommon.ToString(dr("vcShippingLabelImage"))) Then
                        System.IO.File.Delete(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & CCommon.ToString(dr("vcShippingLabelImage")))
                    End If
                Next

                objOppBizDocs.ShippingReportId = e.CommandArgument
                objOppBizDocs.DeleteShippingReport()
                BindShippingReportList()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub dgShippingReportList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgShippingReportList.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim btnDelete As Button
                btnDelete = e.Item.FindControl("btnDelete")
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class