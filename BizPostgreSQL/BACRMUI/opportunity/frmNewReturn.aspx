﻿<%@ Page Language="vb" EnableViewState="true" ValidateRequest="false" AutoEventWireup="true"
    CodeBehind="frmNewReturn.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmNewReturn"
    MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>New Return</title>
    <style type="text/css">
        .column {
            float: left;
        }

        html > body .RadComboBoxDropDown_Vista .rcbItem, html > body .RadComboBoxDropDown_Vista .rcbHovered, html > body .RadComboBoxDropDown_Vista .rcbDisabled, html > body .RadComboBoxDropDown_Vista .rcbLoading {
            display: inline-block;
        }

        .multipleRowsColumns .rcbItem, .multipleRowsColumns .rcbHovered {
            float: left;
            margin: 0 1px;
            min-height: 13px;
            overflow: hidden;
            padding: 2px 19px 2px 6px; /*width: 125px;*/
        }

        .rcbHeader ul, .rcbFooter ul, .rcbItem ul, .rcbHovered ul, .rcbDisabled ul, .ulItems {
            width: 100%;
            display: inline-block;
            margin: 0;
            padding: 0;
            list-style-type: none;
        }

        .rcbList {
            display: inline-block;
            width: 100%;
            list-style-type: none;
        }

        .col1, .col2, .col3, .col4, .col5, .col6 {
            float: left;
            width: 80px;
            margin: 0;
            padding: 0 5px 0 0;
            line-height: 14px;
            display: block; /*border:0px solid red;*/
        }

        .col1 {
            width: 125px;
        }

        .col2 {
            width: 200px;
        }

        .col3 {
            width: 50px;
        }

        .col4 {
            width: 50px;
        }

        .col5 {
            width: 75px;
        }

        .col6 {
            width: 50px;
        }

        .tableLayout {
            width: 100%;
            border-spacing: 0;
            border-collapse: collapse;
        }

            .tableLayout > tbody > tr {
                height: 40px;
            }

            .tableLayout td:nth-child(1) {
                width: 145px;
                font-weight: bold;
                text-align: right;
            }

            .tableLayout td:nth-child(2) {
                padding-left: 5px;
                width: 230px;
            }

            .tableLayout td:nth-child(3) {
                width: 50px;
                font-weight: bold;
                text-align: right;
            }

            .tableLayout td:nth-child(4) {
                padding-left: 5px;
                width: 230px;
            }

            .tableLayout td:nth-child(5) {
                padding-left: 10px;
            }

        .tableInner > tbody > tr > td:nth-child(1) {
            width: 150px;
            text-align: right;
            font-weight: bold;
        }

        .tableInner {
            width: 250px;
        }

            .tableInner > tbody > tr > td:nth-child(2) {
                width: 100px;
                text-align: left;
                padding-right: 2px;
            }

        .cont2 {
            background: #fff;
            border: 1px solid #C7D2E4;
            padding: 0px;
        }

        .cont1 {
            background: rgba(60, 141, 188, 0.09);
            border: 1px solid #C7D2E4;
        }

        .rcbInput {
            height: 19px !important;
        }

        input[type='radio'] {
            background-color: #FFFF99;
            color: Navy;
            position: relative;
            bottom: 3px;
            vertical-align: middle;
        }

        .gridHeader td {
            background: #1473B4;
            color: white;
            font-weight: bold;
            padding: 6px;
            border: 1px solid #CACED9;
            text-align: center;
        }

        .gridFooter td {
            background: #1473B4;
            color: white;
            font-weight: bold;
            padding: 6px;
            border: 1px solid #CACED9;
        }

        .gridItem td {
            border: 1px solid #CACED9;
            background-color: #FFF;
        }

        .gridAlternateItem td {
            background: #FFF;
            border: 1px solid #CACED9;
        }

        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
    <script src="../JavaScript/Orders.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script src="../JavaScript/Common.js" type="text/javascript"></script>
    <script src="../JavaScript/jquery-1.9.1.min.js" type="text/javascript"></script>
    <script src="../JavaScript/comboClientSide.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">

        function UnAppliedChanged() {
            var ddlUnAppliedPaymentsValue = $("#ddlUnAppliedPayments").val();

            if (ddlUnAppliedPaymentsValue != "0") {
                $("#txtAmount").val(ddlUnAppliedPaymentsValue.split("~")[1]);
            }
        }

        function CheckAllChanged() {
            $("#ctl00_Content_dgItems tr input[type = 'checkbox']").not("#ctl00_Content_dgItems_ctl01_chkAll").prop("checked", $("#ctl00_Content_dgItems_ctl01_chkAll").is(':checked'))
            $("#ctl00_Content_dgItems tr input[type = 'checkbox']").not("#ctl00_Content_dgItems_ctl01_chkAll").change();

            if ($("#ctl00_Content_dgItems_ctl01_chkAll").is(':checked')) {
                var selectedItems = [];
                $('#ctl00_Content_dgItems tr').not(':first,:last').each(function () {
                    selectedItems.push($(this).find("span[id*='lblOppItemCode']").html());
                });

                $("#hdnSelectedItems").val(selectedItems);
            } else {
                $("#hdnSelectedItems").val("");
            }

            CalculateTotal();
        }

        function ItemSelectionChanged(chk, itemcode) {
            var selectedItems = $("#hdnSelectedItems").val();

            if (selectedItems.length == 0) {
                var arraySelecteditems = [];
            } else {
                var arraySelecteditems = selectedItems.split(',');
            }

            if (chk.checked) {
                if ($.inArray(itemcode.toString(), arraySelecteditems) < 0) {
                    arraySelecteditems.push(itemcode.toString());
                    $("#hdnSelectedItems").val(arraySelecteditems.join(","))
                }
            }
            else {
                if ($.inArray(itemcode.toString(), arraySelecteditems) >= 0) {

                    arraySelecteditems = $.grep(arraySelecteditems,
                   function (o, i) {
                       return o === itemcode.toString();
                   },
                   true);

                    $("#hdnSelectedItems").val(arraySelecteditems.join(","))
                }
            }

            CalculateTotal();
        }

        function Save(a) {

            if ($find('radCmbCompany').get_value() == "") {
                alert("Select Customer")
                return false;
            }

            if (document.getElementById("ddlContact").value == 0) {
                alert("Select Contact")
                return false;
            }

            if (a == 3 || a == 4) {
                if (document.getElementById("txtAmount").value == "") {
                    alert("Enter Amount")
                    document.getElementById("txtAmount").focus();
                    return false;
                }
            }

            if (a == 4) {
                var ddlUnAppliedPaymentsValue = $("#ddlUnAppliedPayments").val();

                if (ddlUnAppliedPaymentsValue == "0") {
                    alert("Please select Unapplied Payment / Credit Memo for issuing Refund.");
                    return false;
                }
                else if (ddlUnAppliedPaymentsValue != "0" && Number($("#txtAmount").val()) > Number(ddlUnAppliedPaymentsValue.split("~")[1])) {
                    alert("Refund Amount can't be more than selected Unapplied Payment / Credit Memo Amount.");
                    return false;
                }
            }
        }

        function FillModifiedAddress(AddressType, AddressID, FullAddress, warehouseID, isDropshipClicked) {
            if (AddressType == "BillTo") {
                document.getElementById('hdnBillAddressID').value = AddressID;
            } else {
                document.getElementById('hdnShipAddressID').value = AddressID;
            }

            document.getElementById("btnTemp").click();

            return false;
        }

        function CalculateTotal() {
            var Total, subTotal, TaxAmount, ShipAmt, IndTaxAmt, discount;
            var index, findex;
            Total = 0;
            TaxAmount = 0;
            subTotal = 0;
            IndTaxAmt = 0;

            var strtaxItemIDs = $('#TaxItemsId').val().split(',')
            var strTax = $('#txtTax').val().split(',')
            $('#ctl00_Content_dgItems tr').not(':first,:last').each(function () {
                if ($(this).find("input[id$=chkSelect]").is(":checked")) {
                    if (parseFloat($(this).find("input[id*='txtUnits']").val()) <= 0) {
                        alert("Units must greater than 0.")
                        $(this).find("input[id*='txtUnits']").focus();
                        return false;
                    }

                    var tintDiscountType = $(this).find("input[id*='hdnDiscountType']").val();
                    var monDiscount = $(this).find("input[id*='hdnDiscount']").val();
                    var bitMarkupDiscount = $(this).find("input[id*='hdnbitMarkupDiscount']").val();

                    if (monDiscount > 0) {
                        if (tintDiscountType == "False") {

                            if (bitMarkupDiscount == "1") {
                                 subTotal = (parseFloat($(this).find("input[id*='txtUnits']").val()) * parseFloat($(this).find("input[id*='txtUOMConversionFactor']").val())) * (parseFloat($(this).find("input[id*='hdnOrigPrice']").val()) + (parseFloat($(this).find("input[id*='hdnOrigPrice']").val()) * (monDiscount / 100)));
                            }
                            else {
                                 subTotal = (parseFloat($(this).find("input[id*='txtUnits']").val()) * parseFloat($(this).find("input[id*='txtUOMConversionFactor']").val())) * (parseFloat($(this).find("input[id*='hdnOrigPrice']").val()) - (parseFloat($(this).find("input[id*='hdnOrigPrice']").val()) * (monDiscount / 100)));
                            }
                           
                        } else {
                            var unitDiscount = monDiscount / parseFloat($(this).find("input[id*='txtUnits']").val());
                            if (bitMarkupDiscount == "1") {
                                subTotal = (parseFloat($(this).find("input[id*='txtUnits']").val()) * parseFloat($(this).find("input[id*='txtUOMConversionFactor']").val())) * (parseFloat($(this).find("input[id*='hdnOrigPrice']").val()) + parseFloat(unitDiscount));
                            }
                            else {
                                subTotal = (parseFloat($(this).find("input[id*='txtUnits']").val()) * parseFloat($(this).find("input[id*='txtUOMConversionFactor']").val())) * (parseFloat($(this).find("input[id*='hdnOrigPrice']").val()) - parseFloat(unitDiscount));
                            }
                        }
                    }
                    else {
                        subTotal = (parseFloat($(this).find("input[id*='txtUnits']").val()) * parseFloat($(this).find("input[id*='txtUOMConversionFactor']").val())) * parseFloat($(this).find("input[id*='hdnOrigPrice']").val());
                    }

                    $(this).find("[id*='lblTotal']").text(CommaFormatted(roundNumber(subTotal, 2)));

                    Total = parseFloat(Total) + parseFloat(subTotal);

                    if ($('#txtTax').val().length > 0) {
                        for (k = 0; k < strtaxItemIDs.length; k++) {
                            if ($(this).find("[id*='lblTaxable" + strtaxItemIDs[k] + "']").text() != 'False') {
                                $(this).find("[id*='lblTaxAmt" + strtaxItemIDs[k] + "']").text(parseFloat(strTax[k] * parseFloat(subTotal) / 100));
                                TaxAmount = parseFloat(TaxAmount) + (parseFloat($(this).find("[id*='lblTaxAmt" + strtaxItemIDs[k] + "']").text()) || 0);
                            }
                        }
                    }
                }
            });

            var discount = 0;

            //discount = Number($('#txtDiscount').val());

            $('#ctl00_Content_dgItems tr:last').find("[id*='lblFTotal']").text(CommaFormatted(roundNumber((Total), 2)));
            $('#lblSubTotal').text(roundNumber(Total, 2));
            $('#lblTaxAmount').text(roundNumber(TaxAmount, 2));
            $('#lblGrandTotal').text(CommaFormatted(roundNumber((Total) + (parseFloat(TaxAmount) || 0) - (parseFloat(discount) || 0), 2)));

            if ($('#txtTax').val().length > 0) {
                for (k = 0; k < strtaxItemIDs.length; k++) {
                    $('#ctl00_Content_dgItems tr').not(':first,:last').each(function () {
                        if ($(this).find("input[id$=chkSelect]").is(":checked")) {
                            IndTaxAmt = IndTaxAmt + parseFloat($(this).find("[id*='lblTaxAmt" + strtaxItemIDs[k] + "']").text())
                        }
                    });

                    $('#ctl00_Content_dgItems tr:last').find("[id*='lblFTaxAmt" + strtaxItemIDs[k] + "']").text(CommaFormatted(roundNumber(IndTaxAmt, 2)));
                    IndTaxAmt = 0;
                }
            }
        }


        function OpenAddressWindow(sAddressType) {
            var radValue = $find('radCmbCompany').get_value();
            window.open('frmChangeDefaultAdd.aspx?AddressType=' + sAddressType + '&CompanyId=' + radValue + "&CompanyName=" + $find('radCmbCompany').get_text(), '', 'toolbar=no,titlebar=no,left=200, top=300,width=800,height=170,scrollbars=no,resizable=no');
            return false;
        }

        var IsAddClicked = false;
        function Add() {

            var IsAddCalidate; // = Save();

            if (IsAddCalidate == false) {
                return false;
            }

            if (IsAddClicked == true) {
                return false;
            }
            if ($find('radItem').get_value() == '') {
                alert("Select Item")
                return false;
            }

            if (document.getElementById("txtUnits").value == "") {
                alert("Enter Units")
                document.getElementById("txtUnits").focus();
                return false;
            }
            if (parseFloat(document.getElementById("txtUnits").value) <= 0) {
                alert("Units must greater than 0.")
                document.getElementById("txtUnits").focus();
                return false;
            }

            if (document.getElementById("txtprice").value == "" || document.getElementById("txtprice").value == 0) {
                alert("Enter Price")
                document.getElementById("txtprice").focus();
                return false;
            }

            IsAddClicked = true;
        }


        function OpenAdd() {
            if ($find('radCmbCompany').get_value() == "") {
                alert("Select Customer")
                return false;
            }
            window.open("../prospects/frmProspectsAdd.aspx?pwer=dfsdfdsfdsfdsf&rtyWR=" + $find('radCmbCompany').get_value() + "&Reload=1", '', 'toolbar=no,titlebar=no,top=300,width=700,height=300,scrollbars=no,resizable=no')
            return false;
        }

        function OpenInTransit() {
            if ($find('radItem').get_value() == '') {
                alert("Select Item");
                return false;
            }

            window.open('../opportunity/frmOrderTransit.aspx?ItemCode=' + $find('radItem').get_value(), '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=yes,resizable=yes')
            return false;
        }

        function openVendorCostTable(a) {
            if ($find('radItem').get_value() == '') {
                alert("Select Item");
                return false;
            }

            window.open('../opportunity/frmVendorCostTable.aspx?ItemCode=' + $find('radItem').get_value() + '&OppType=' + a + '&Vendor=' + document.getElementById('hdnVendorId').value, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenRelatedItems(a, b, c, d) {
            if ($find('radItem').get_value() == '') {
                alert("Select Item");
                return false;
            }

            if (d == 0) {
                alert("Select Company");
                return false;
            }

            window.open('../opportunity/frmOpenRelatedItems.aspx?ItemCode=' + $find('radItem').get_value() + '&OppType=' + a + '&PageType=' + b + '&opid=' + c + '&DivId=' + d, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=yes,resizable=yes')
            return false;
        }

        function CheckCharacter(e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            //document.Form1.txtdesc.value = document.Form1.txtdesc.value + '-' + k;
            if (k == 13) { //|| k == 106 Bug Fix Id 1744 #3
                if (e.preventDefault) {
                    e.preventDefault();
                }
                else
                    e.returnValue = false;
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="FiltersAndViews1">
    <asp:ScriptManager ID="ScriptManager2" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanelTop" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <table width="100%" runat="server" id="tblMultipleOrderMessage" visible="false">
                <tr>
                    <td class="normal4" align="center">You can not create multiple orders simultaneously,
                <asp:LinkButton Text="Click here" runat="server" ID="lnkClick" />
                        to clear other order and proceed.
                    </td>
                </tr>
            </table>

            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="center" style="color: red">
                        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="color: red">
                        <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
     
    <asp:UpdatePanel ID="UpdatePanelReturn" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Label ID="lblReturns" runat="server" Text="Sales Return"></asp:Label>&nbsp;<asp:Label ID="lblToolTip" Text="[?]" CssClass="tip" ForeColor="White" runat="server" ToolTip="1. Select the organization &#013;2. Select items you want to return. Here you have an option to select the item from closed orders. &#013;3. Select the check box on the items which you want to return. &#013;4. Click on save and open details." />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:UpdatePanel runat="server" ID="UpdatePanelMain" ChildrenAsTriggers="true">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="radCmbCompany" />
            <asp:AsyncPostBackTrigger ControlID="radItem" />
            <asp:AsyncPostBackTrigger ControlID="radWareHouse" />
            <asp:AsyncPostBackTrigger ControlID="rBtnAddItems" />
            <asp:AsyncPostBackTrigger ControlID="rBtnExistingOrder" />
            <asp:AsyncPostBackTrigger ControlID="txtUnits" />
            <asp:AsyncPostBackTrigger ControlID="txtprice" />
        </Triggers>
        <ContentTemplate>
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td class="cont2">
                        <b style="font-weight: bold; font-size: 14px">Step 1. </b><b>Select organization and contact detail</b>
                        <table class="tableLayout">
                            <tr>
                                <td>
                                    <u>O</u>rganization<font color="#ff3333">*</font>
                                </td>
                                <td>
                                    <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="200" DropDownWidth="600px"
                                        OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                        ClientIDMode="Static"
                                        ShowMoreResultsBox="true"
                                        Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True">
                                        <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                    </telerik:RadComboBox>
                                </td>
                                <td>Contact
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlContact" Width="200" runat="server" CssClass="signup">
                                    </asp:DropDownList>
                                </td>
                                <td rowspan="3" style="white-space: nowrap; padding-top: 8px;" valign="top">
                                    <table class="tableInner">
                                        <tr>
                                            <td style="font-weight: bold">Sales Credit</td>
                                            <td>
                                                <asp:Label ID="lblCreditBalance" runat="server" CssClass="text"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold">Credit Limit (A)</td>
                                            <td>
                                                <asp:Label ID="lblCreditLimit" runat="server" CssClass="text"></asp:Label></td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold">Balance Due (B)</td>
                                            <td>
                                                <asp:Label ID="lblBalanceDue" runat="server" CssClass="text"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Remaining Credit(A-B)</td>
                                            <td>
                                                <asp:Label ID="lblRemaningCredit" runat="server" CssClass="text"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight: bold">
                                                <asp:Label ID="lblTotalAmtPastDue1" runat="server">Amount Past Due</asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblTotalAmtPastDue" runat="server" CssClass="text"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>Assign To
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlAssignTo" Width="200" runat="server" CssClass="signup">
                                        <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="white-space:nowrap">
                                    <asp:label id="lblReferenceSalesOrder" runat="server" Text="Reference Sales Order" Visible="false"></asp:label>
                                </td>
                                <td>
                                    <telerik:RadComboBox ID="radCmbSalesOrder" runat="server" AllowCustomText="True" AutoPostBack="true"  Width="200" ShowMoreResultsBox="true" EnableLoadOnDemand="True" Visible="false">
                                        <WebServiceSettings Path="../common/Common.asmx" Method="GetSalesOrders" />
                                    </telerik:RadComboBox>
                                    <asp:CheckBoxList ID="chkTaxItems" CssClass="normal1" runat="server" RepeatDirection="Horizontal"
                                        RepeatColumns="3" Visible="false">
                                    </asp:CheckBoxList>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Bill to</td>
                                <td>
                                    <asp:Label ID="lblBillTo1" runat="server" Text=""></asp:Label>
                                    <br />
                                    <asp:Label ID="lblBillTo2" runat="server" Text=""></asp:Label>
                                </td>
                                <td>Ship to</td>
                                <td>
                                    <asp:Label ID="lblShipTo1" runat="server" Text=""></asp:Label>
                                    <br />
                                    <asp:Label ID="lblShipTo2" runat="server" Text=""></asp:Label>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <asp:Panel ID="pnlOrder" runat="server">
                    <tr>
                        <td class="cont1">

                            <div style="line-height: 30px;">
                                <b style="font-weight: bold; font-size: 14px">Step 2. </b><b>Add items manually or select from closed orders.</b>
                                <asp:RadioButton ID="rBtnAddItems" runat="server" Text="Add New Items" GroupName="Items" AutoPostBack="true" Checked="true" />
                                <asp:RadioButton ID="rBtnExistingOrder" runat="server" Text="Choose Items from existing Order" AutoPostBack="true" GroupName="Items" />
                            </div>

                            <asp:Panel ID="pnlItems" runat="server">
                                <table class="tableLayout">
                                    <tr>
                                        <td>
                                            <u>I</u>tem
                                        </td>
                                        <td>
                                            <telerik:RadComboBox runat="server" ID="radItem" AccessKey="I" Skin="Vista" Height="200px"
                                                AutoPostBack="true" Width="200" DropDownWidth="515px" ShowMoreResultsBox="true"
                                                EnableVirtualScrolling="false" ChangeTextOnKeyBoardNavigation="false" OnClientItemsRequesting="OnClientItemsRequesting"
                                                OnClientItemsRequested="OnClientItemsRequested" OnClientDropDownOpening="OnClientDropDownOpening"
                                                EnableLoadOnDemand="true" ClientIDMode="Static" onkeypress="CheckCharacter(event)">
                                                <WebServiceSettings Path="../common/Common.asmx" Method="GetItemNamesForReturn" />
                                            </telerik:RadComboBox>
                                            <asp:HiddenField ID="hdnSearchOrderCustomerHistory" runat="server" Value="0" />
                                            <asp:HiddenField ID="hdnCmbDivisionID" runat="server" />
                                            <asp:HiddenField ID="hdnCmbOppType" runat="server" Value="1" />
                                        </td>
                                        <td>Type
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddltype" runat="server" CssClass="signup" Width="200" Enabled="False">
                                                <asp:ListItem Value="P">Inventory Item</asp:ListItem>
                                                <asp:ListItem Value="N">Non-Inventory Item</asp:ListItem>
                                                <asp:ListItem Value="S">Service</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td rowspan="4">
                                            <asp:Image ID="imgItem" runat="server" BorderWidth="1" Width="100" BorderColor="black"
                                                Height="100"></asp:Image><br />
                                            <asp:HyperLink ID="hplImage" runat="server" CssClass="hyperlink" NavigateUrl="">View Full Image</asp:HyperLink>
                                        </td>
                                    </tr>
                                    <tr runat="server" id="trWarehouse">
                                        <td>
                                            <span id="tdWareHouseLabel" runat="server">Return To <u>W</u>arehouse<font color="red">*</font></span>
                                        </td>
                                        <td>
                                            <span id="tdWareHouse">
                                                <telerik:RadComboBox ID="radWareHouse" runat="server" Width="200" DropDownWidth="650px"
                                                    AccessKey="W" AutoPostBack="true" ClientIDMode="Static" DropDownCssClass="multipleRowsColumns">
                                                    <HeaderTemplate>
                                                        <ul>
                                                            <li class="col1">Warehouse</li>
                                                            <li class="col2">Attributes</li>
                                                            <li class="col3">On Hand</li>
                                                            <li class="col4">On Order</li>
                                                            <li class="col5">On Allocation</li>
                                                            <li class="col6">BackOrder</li>
                                                        </ul>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <ul>
                                                            <li style="display: none">
                                                                <%# DataBinder.Eval(Container.DataItem, "numWareHouseItemId")%></li>
                                                            <li class="col1">
                                                                <%# DataBinder.Eval(Container.DataItem, "vcWareHouse")%></li>
                                                            <li class="col2">
                                                                <%# DataBinder.Eval(Container.DataItem, "Attr") %>&nbsp;</li>
                                                            <li class="col3">
                                                                <%# DataBinder.Eval(Container.DataItem, "numOnHand")%>&nbsp;
                                                                                    <%# DataBinder.Eval(Container.DataItem, "vcUnitName")%></li>
                                                            <li class="col4">
                                                                <%# DataBinder.Eval(Container.DataItem, "numOnOrder")%>
                                                                                    &nbsp;</li>
                                                            <li class="col5">
                                                                <%# DataBinder.Eval(Container.DataItem, "numAllocation")%>&nbsp;</li>
                                                            <li class="col6">
                                                                <%# DataBinder.Eval(Container.DataItem, "numBackOrder")%>&nbsp;
                                                                                    <asp:Label runat="server" ID="lblWareHouseID" Text='<%#DataBinder.Eval(Container.DataItem, "numWareHouseID")%>'
                                                                                        Style="display: none"></asp:Label></li>
                                                        </ul>
                                                    </ItemTemplate>
                                                </telerik:RadComboBox>
                                            </span>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Description</td>
                                        <td colspan="3">
                                            <asp:TextBox ID="txtdesc" runat="server" Rows="3" TextMode="MultiLine" Width="482px"></asp:TextBox>
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>Units<font color="red">*</font>
                                        </td>
                                        <td colspan="3" style="float: left; white-space: nowrap">
                                            <table style="margin-top: 5px;">
                                                <tr>
                                                    <td>
                                                        <asp:TextBox ID="txtUnits" runat="server" CssClass="signup" Width="50px" Text="1"
                                                            AutoPostBack="true"></asp:TextBox>
                                                        <asp:Label ID="lblUOM" runat="server"></asp:Label>
                                                        <asp:TextBox ID="txtOldUnits" CssClass="signup" Style="display: none" runat="server">
                                                        </asp:TextBox>
                                                        <asp:TextBox ID="txtnumUnitHourReceived" runat="server" Style="display: none" />
                                                        <asp:TextBox ID="txtnumQtyShipped" runat="server" Style="display: none" />
                                                        <asp:TextBox ID="txtUOMConversionFactor" runat="server" Style="display: none" />
                                                    </td>
                                                    <td><b>UoM</b>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddUOM" runat="server" CssClass="signup" AutoPostBack="true" Width="120">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td><b>Unit Price</b><font color="red">*</font>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtprice" runat="server" CssClass="signup" Width="90px" MaxLength="11"
                                                            AutoPostBack="true"></asp:TextBox>
                                                        /
                                                                            <asp:Label ID="lblBaseUOMName" runat="server" Text="-"></asp:Label>
                                                        <asp:HiddenField ID="hdnOrigUnitPrice" runat="server" />
                                                    </td>
                                                    <td class="normal1">
                                                        <asp:Button AccessKey="A" ID="btnAdd" runat="server" Width="60px" CssClass="button"
                                                            Text="Add"></asp:Button>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnEditCancel" runat="server" Visible="false" CssClass="button" Text="Cancel"></asp:Button>
                                                    </td>
                                                    <td>
                                                        <asp:Button ID="btnOptionsNAccessories" runat="server" CssClass="button" Text="Customize"
                                                            Visible="false" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="9" class="normal1" id="tdSalesProfit" runat="server" visible="false">
                                                        <asp:HyperLink ID="hplUnitCost" runat="server" CssClass="hyperlink">Unit-Cost</asp:HyperLink>&nbsp;
                                            <asp:Label ID="lblPUnitCost" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;<strong>Profit - Unit/Total </strong>
                                                        <asp:Label ID="lblProfit" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <asp:Panel ID="pnlOrders" runat="server" Style="display: none;">
                                <table class="tableLayout">
                                    <tr>
                                        <td>
                                            <u>E</u>xisting Orders
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlExistingOrders" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td class="cont2">
                            <div style="padding: 5px; height: 21px;">
                                <div style="float: left">
                                    <b>Items to return</b>
                                </div>
                                <div style="float: right">
                                    <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" />
                                </div>
                            </div>
                            <asp:DataGrid ID="dgItems" runat="server" ShowFooter="true" AutoGenerateColumns="false"
                                Width="100%" ClientIDMode="AutoID" CellPadding="0" CellSpacing="0" UseAccessibleHeader="true" CssClass="tblPrimary" FooterStyle-CssClass="gridFooter">
                                <Columns>
                                    <asp:BoundColumn DataField="numItemCode" HeaderText="Item ID"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Item" HeaderStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblOppItemCode" runat="server" CssClass="text" Text='<%#Eval("numoppitemtCode")%>'></asp:Label>
                                            <asp:Label ID="lblItemName" runat="server" CssClass="text" Text='<%#Eval("vcItemName")%>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="vcModelID" HeaderText="Model ID"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Attributes" HeaderText="Attributes"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Warehouse" HeaderText="Warehouse"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="vcItemDesc" HeaderText="Description" HeaderStyle-Width="20%"></asp:BoundColumn>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblUnitCaption" runat="server">Units</asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtUnits" CssClass="signup" Width="40" runat="server" Text='<%# ReturnMoney(Eval("numUnitHour"))%>'>
                                            </asp:TextBox>
                                            <%# DataBinder.Eval(Container, "DataItem.vcUOMName")%>
                                            <asp:TextBox ID="txtUOMConversionFactor" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.UOMConversionFactor") %>'>></asp:TextBox>
                                            <asp:HiddenField ID="hdnOrigUnitHour" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.numOrigUnitHour")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                            <asp:Label ID="lblUnitPriceCaption" runat="server">Unit Price</asp:Label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtUnitPrice" CssClass="signup" Width="40" runat="server" Text='<%# String.Format("{0:0.################}", DataBinder.Eval(Container, "DataItem.monPrice") * DataBinder.Eval(Container, "DataItem.UOMConversionFactor"))%>'>
                                            </asp:TextBox>
                                            /
                                            <%# DataBinder.Eval(Container, "DataItem.vcUOMName")%>
                                            <asp:HiddenField runat="server" ID="hdnOrigPrice" Value='<%#DataBinder.Eval(Container, "DataItem.monPrice")%>' />
                                            <asp:HiddenField runat="server" ID="hdnUnitId" Value='<%#DataBinder.Eval(Container,"DataItem.numUOM") %>' />
                                            <asp:HiddenField ID="hdnDiscount" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.fltDiscount")%>' />
                                            <asp:HiddenField ID="hdnDiscountType" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.bitDiscountType")%>' />
                                            <asp:HiddenField ID="hdnbitMarkupDiscount" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.bitMarkupDiscount")%>' />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            Total :
                                        </FooterTemplate>
                                        <HeaderStyle Width="120" />
                                        <FooterStyle HorizontalAlign="Right" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Total" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTotal" runat="server" CssClass="text" Text='<%# ReturnMoney( DataBinder.Eval(Container,"DataItem.monTotAmount")) %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFTotal" runat="server" CssClass="text"></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Sales Tax" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTaxAmt0" runat="server" CssClass="text" Text='<%# DataBinder.Eval(Container,"DataItem.Tax0") %>'></asp:Label>
                                            <asp:Label ID="lblTaxable0" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.bitTaxable0") %>'></asp:Label>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Label ID="lblFTaxAmt0" runat="server" CssClass="text" Text=""></asp:Label>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-Width="2%" ItemStyle-HorizontalAlign="Right">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="chkAll" runat="server" onchange="CheckAllChanged();" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" CssClass="chkSelect" runat="server" onClick='<%# "javascript:ItemSelectionChanged(this," & Eval("numoppitemtCode") & ");" %>' />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <div runat="server" id="divTD" visible="false">
                                <table class="tableLayout">
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td style="white-space: nowrap; padding-top: 8px;">
                                            <table class="tableInner">
                                                <tr>
                                                    <td>Sub Total
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblSubTotal" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Tax
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblTaxAmount" runat="server" Text="0.00"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <b>Grand Total</b>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblGrandTotal" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </asp:Panel>
                <tr>
                    <td class="cont1">
                        <asp:Panel ID="pnlUnAppliedPayment" runat="server" Visible="false">
                            <table>
                                <tr>
                                    <td style="white-space: nowrap;">
                                        <table>
                                            <tr>
                                                <td><b>UnApplied Payment / Credit Memo</b>
                                                </td>
                                                <td>
                                                    <asp:DropDownList ID="ddlUnAppliedPayments" runat="server" AutoPostBack="false" Width="200" onchange="UnAppliedChanged();">
                                                        <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <table class="tableLayout">
                            <tr>
                                <td>Reason</td>
                                <td>
                                    <asp:DropDownList runat="server" ID="ddlReturnReason" Width="200"></asp:DropDownList>
                                </td>
                                <td>
                                    <asp:Label ID="lblAmount" runat="server" Text="Amount"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAmount" runat="server" CssClass="signup" MaxLength="11"></asp:TextBox>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Comments
                                </td>
                                <td colspan="4">
                                    <asp:TextBox runat="server" Rows="3" TextMode="MultiLine" ID="txtComments" Width="472">
                                    </asp:TextBox>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                        <table width="100%">
                            <tr>
                                <td style="text-align: center">
                                    <asp:Button ID="btnSaveAndOpenDetails" runat="server" CssClass="button" Text="Save & Open Details" />&nbsp;
                            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Cancel" />
                                    <input id="Taxable" runat="server" type="hidden" value="" />
                                    <input id="TaxItemsId" runat="server" type="hidden" value="" />
                                    <asp:TextBox ID="txtTax" runat="server" Style="display: none"></asp:TextBox>
                                    <asp:HiddenField ID="hdnBillAddressID" runat="server" />
                                    <asp:HiddenField ID="hdnShipAddressID" runat="server" />
                                    <asp:HiddenField ID="hdvSalesUOMConversionFactor" runat="server" Value="1" />
                                    <asp:HiddenField ID="hdvUOMConversionFactor" runat="server" Value="1" />
                                    <asp:TextBox ID="txtHidEditOppItem" runat="server" Style="display: none"></asp:TextBox>
                                    <asp:HiddenField ID="hdnUnitCost" runat="server" />
                                    <asp:HiddenField ID="hdnSelectedItems" runat="server" />
                                    <asp:Button ID="btnTemp" runat="server" Style="display: none"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
