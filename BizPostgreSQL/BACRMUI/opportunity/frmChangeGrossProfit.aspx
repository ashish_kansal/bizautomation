﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmChangeGrossProfit.aspx.vb" Inherits=".frmChangeGrossProfit" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close"></asp:Button>
            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close" OnClientClick="Close();" CausesValidation="false"></asp:Button>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Change Profit Percentage
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <table width="400">
        <tr>
            <td width="60">Profit (%):</td>
            <td style="float:left;">
                <telerik:RadScriptManager ID="radScriptManager" runat="server" />
               <telerik:RadNumericTextBox ID="radNumTxtProfitPercent" runat="server" MaxValue="100" NumberFormat-DecimalDigits="2" NumberFormat-DecimalSeparator="." />
            </td>
        </tr>
    </table>
</asp:Content>
