﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/SalesOrder.Master" CodeBehind="frmMassStockTransfer.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmMassStockTransfer" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../CSS/style.css" rel="stylesheet" type="text/css" />
    <script src="../JavaScript/Orders.js" type="text/javascript"></script>
    <link href="../CSS/style.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../JavaScript/jquery-1.8.3.js"></script>
    <script type='text/javascript' src="../JavaScript/select2.js"></script>
    <link rel="stylesheet" href="../CSS/select2.css" />
    <script type="text/javascript" src="../JavaScript/jquery.validate.js"></script>
    <link rel="Stylesheet" href="../CSS/font-awesome.min.css" />
    <style type="text/css">
        .multipleRowsColumns .rcbItem, .multipleRowsColumns .rcbHovered {
            float: left;
            margin: 0 1px;
            min-height: 13px;
            overflow: hidden;
            padding: 2px 19px 2px 6px; /*width: 125px;*/
        }

        .rcbHeader ul, .rcbFooter ul, .rcbItem ul, .rcbHovered ul, .rcbDisabled ul, .ulItems {
            width: 100%;
            display: inline-block;
            margin: 0;
            padding: 0;
            list-style-type: none;
        }

        .rcbList {
            display: inline-block;
            width: 100%;
            list-style-type: none;
        }


        .RadComboBox {
            width: 100%;
            margin-top: 8px !important;
            vertical-align: middle;
        }

        .RadComboBox, .rcbInputCell, .rcbInput {
            width: 100%;
            background: none repeat scroll 0% 0% transparent;
            border: 0px none;
            vertical-align: middle;
            padding: 4px 0px 1px;
            outline: 0px none;
            margin: 0px !important;
        }

        .RadComboBoxDropDown .rcbHeader, .RadComboBoxDropDown .rcbFooter {
            padding: 5px 7px 4px;
            border-width: 0;
            border-style: solid;
            background-image: none !important;
        }

        #radWareHouse_DropDown {
            left: 34% !important;
        }

        .col1, .col2, .col3, .col4, .col5, .col6 {
            float: left;
            width: 80px;
            margin: 0;
            padding: 0 5px 0 0;
            line-height: 14px;
            display: block; /*border:0px solid red;*/
        }

        .col1 {
            width: 240px;
        }

        .col2 {
            width: 200px;
        }

        .col3 {
            width: 80px;
        }

        .col4 {
            width: 80px;
        }

        .col5 {
            width: 85px;
        }

        .col6 {
            width: 80px;
        }

        #chkTransferToItem {
            margin-top: 0px;
        }

        .btn {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
        }

        .btn-primary {
            background-color: #1473B4;
            color:#fff !important;
            border-color: #367fa9;
        }

        .btn-danger {
			background-color: #dd4b39;
			border-color: #d73925;
			color:#fff;
		}
		
		.btn-xs {
			padding: 1px 5px;
			font-size: 12px;
			line-height: 1.5;
			border-radius: 3px;
		}
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_endRequest(EndRequestHandler);
        });

        function EndRequestHandler(sender, args) {
            if ($("#hdnCurrentTextBox").val() != "") {
                var next = $("[id=" + $("#hdnCurrentTextBox").val() + "]").closest("tr").next().find("input[id$=txtUnits]");;
                if (next.length > 0) {
                    setTimeout(function () {
                        next.focus();
                    }, 0);
                } else {
                    next = $("[id=" + $("#hdnCurrentTextBox").val() + "]").closest("tr").next().find("a[id$=hplSerialLot]");;
                    if (next.length > 0) {
                        setTimeout(function () {
                            next.focus();
                        }, 0);
                    }
                }

                $("#hdnCurrentTextBox").val("");
            }
        }

        function Save(a) {
            var errorMessage = "";

            var radcmbSTFrom = $find('radcmbSTFrom');
            var radcmbSTTo = $find('radcmbSTTo');
            var ddlFromContact = document.getElementById("ddlFromContact");
            var ddlToContact = document.getElementById("ddlToContact");

            if (radcmbSTFrom != null && radcmbSTFrom.get_value() == "") {
                errorMessage += "Select Storck Transfer From Field. \n";
            }

            if (radcmbSTTo != null && radcmbSTTo.get_value() == "") {
                errorMessage += "Select Storck Transfer To Field. \n";
            }

            if (ddlFromContact != null && ddlFromContact.value == 0) {
                errorMessage += "Select From Contact. \n";
            }

            if (ddlToContact != null && ddlToContact.value == 0) {
                errorMessage += "Select To Contact. \n";
            }

            if (errorMessage.length > 0) {
                alert(errorMessage);
                return false;
            }

            return true;
        }

        function SetSelectedItem(itemName, itemCode, warehouseItemID) {
            $("#txtItem").select2("data", { numItemCode: itemCode, numWareHouseItemID: warehouseItemID, text: itemName });
            $("#txtItem").val(itemCode).trigger("change");
        }

        function GetSelectedItems() {

            if ($find('radcmbSTFrom') != null && $find('radcmbSTFrom').get_value() == '') {
                alert("Select From");
                $find('radcmbSTFrom').focus();
                return false;
            }

            if ($find('radcmbSTTo') != null && $find('radcmbSTTo').get_value() == '') {
                alert("Select To");
                $find('radcmbSTTo').focus();
                return false;
            }

            if (document.getElementById("ddlFromContact") != null && ((document.getElementById("ddlFromContact").selectedIndex == -1) || (document.getElementById("ddlFromContact").value == 0))) {
                alert("Select From Contact")
                document.getElementById("ddlFromContact").focus();
                return false;
            }

            if (document.getElementById("ddlToContact") != null && ((document.getElementById("ddlToContact").selectedIndex == -1) || (document.getElementById("ddlToContact").value == 0))) {
                alert("Select To Contact")
                document.getElementById("ddlToContact").focus();
                return false;
            }

            if (document.getElementById("txtUnitsToTransfer") != null && parseFloat(document.getElementById("txtUnitsToTransfer").value || 0) <= 0) {
                alert("Select Qty to transfer.")
                document.getElementById("txtUnitsToTransfer").focus();
                return false;
            }

            var obj = $('#txtItem').select2('data');
            var objTo = $('#txtItemTo').select2('data');

            if (obj == null) {
                alert("Please select item.");
                return false;
            } else if ($("[id$=chkTransferToItem]").is(":checked") && objTo == null) {
                alert("Please transfer to item.");
                return false;
            } else {
                var item = new Object();
                item.numItemCode = obj.numItemCode;
                item.numWareHouseItemID = obj.numWareHouseItemID;
                item.bitHasKitAsChild = Boolean(obj.bitHasKitAsChild);
                item.kitChildItems = obj.childKitItems;
                item.numQuantity = obj.numQuantity || 1;
                item.vcAttributes = obj.Attributes || "";
                item.vcAttributeIDs = obj.AttributeIDs || "";
                if ($("[id$=chkTransferToItem]").is(":checked")) {
                    item.numToItemCode = objTo.numItemCode;
                } else {
                    item.numToItemCode = obj.numItemCode;
                }

                $("#hdnSelectedItems").val(JSON.stringify(item));

                return true;
            }
        }

        $(function () {
            var columns;
            var userDefaultPageSize = '<%= Session("PagingRows")%>';
            var userDefaultCharSearch = '<%= Session("ChrForItemSearch") %>';
            var varPageSize = parseInt(userDefaultPageSize, 10);
            var varCharSearch = 1;
            if (parseInt(userDefaultCharSearch, 10) > 0) {
                varCharSearch = parseInt(userDefaultCharSearch, 10);
            }

            $.ajax({
                type: "POST",
                url: '../common/Common.asmx/GetSearchedItems',
                data: '{ searchText: "abcxyz", pageIndex: 1, pageSize: 10, divisionId: 0, isGetDefaultColumn: true, warehouseID: 0, searchOrdCusHistory: 0, oppType: 1, isTransferTo: false,IsCustomerPartSearch:false,searchType:"1" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    // Replace the div's content with the page method's return.
                    if (data.hasOwnProperty("d"))
                        columns = $.parseJSON(data.d)
                    else
                        columns = $.parseJSON(data);
                },
                results: function (data) {
                    columns = $.parseJSON(data);
                }
            });

            function getDivisionId() {

                var radCmbCompany = $find('radcmbSTFrom');

                if (radCmbCompany != null) {
                    if (radCmbCompany.get_value() != null && radCmbCompany.get_value().length > 0) {
                        return radCmbCompany.get_value();
                    }
                    else {
                        return 0;
                    }
                }
                else {
                    return 0;
                }
            }

            function formatItem(row) {
                var numOfColumns = 0;
                var ui = "<table style=\"width: 100%;font-size:12px;\" cellpadding=\"0\" cellspacing=\"0\">";
                ui = ui + "<tbody>";
                ui = ui + "<tr>";
                ui = ui + "<td style=\"width:auto; padding: 3px; vertical-align:top; text-align:center\">";
                if (row["vcPathForTImage"] != null && row["vcPathForTImage"].indexOf('.') > -1) {
                    ui = ui + "<img id=\"Img7\" height=\"75\" width=\"75\" title=\"Item Image\" src=\"" + row["vcPathForTImage"] + "\" alt=\"Item Image\" >";
                } else {
                    ui = ui + "<img id=\"Img7\" height=\"75\" width=\"75\" title=\"Item Image\" src=\"" + "../images/icons/cart_large.png" + "\" alt=\"Item Image\" >";
                }

                ui = ui + "</td>";
                ui = ui + "<td style=\"width: 100%; vertical-align:top\">";
                ui = ui + "<table style=\"width: 100%;\" cellpadding=\"0\" cellspacing=\"0\"  >";
                ui = ui + "<tbody>";
                ui = ui + "<tr>";
                $.each(columns, function (index, column) {
                    if (numOfColumns == 4) {
                        ui = ui + "</tr><tr>";
                        numOfColumns = 0;
                    }

                    if (numOfColumns == 0) {
                        ui = ui + "<td style=\"white-space: nowrap; margin-left:10px\"><strong>" + column.vcFieldName + ":</strong></td><td style=\"width:80%\">" + row[column.vcDbColumnName] + "</td>";
                    }
                    else {
                        ui = ui + "<td style=\"white-space: nowrap; margin-left:10px\"><strong>" + column.vcFieldName + ":</strong></td><td style=\"width:20%\">" + row[column.vcDbColumnName] + "</td>";
                    }
                    numOfColumns += 2;
                });
                ui = ui + "</tr>";
                ui = ui + "</tbody>";
                ui = ui + "</table>";
                ui = ui + "</td>";
                ui = ui + "</tr>";
                ui = ui + "</tbody>";
                ui = ui + "</table>";

                return ui;
            }

            $('#txtItem').on("change", function (e) {
                if (e.added != null) {
                    $("#hdnCurrentSelectedItem").val(e.added.id.toString().split("-")[0]);
                    document.getElementById("lkbItemSelected").click();
                }
            })

            $('#txtItem').on("select2-removed", function (e) {
                $("#hdnCurrentSelectedItem").val("");
                document.getElementById("lkbItemRemoved").click();
            })

            $('#txtItem').select2(
            {
                placeholder: 'Select Item',
                minimumInputLength: varCharSearch,
                multiple: false,
                formatResult: formatItem,
                width: "100%",
                dropdownCssClass: 'bigdrop',
                dataType: "json",
                allowClear: true,
                ajax: {
                    quietMillis: 500,
                    url: '../common/Common.asmx/GetSearchedItems',
                    type: 'POST',
                    params: {
                        contentType: 'application/json; charset=utf-8'
                    },
                    dataType: 'json',
                    data: function (term, page) {
                        return JSON.stringify({
                            searchText: term,
                            pageIndex: page,
                            pageSize: varPageSize,
                            divisionId: getDivisionId(),
                            isGetDefaultColumn: false,
                            warehouseID: 0,
                            searchOrdCusHistory: 0,
                            oppType: 2,
                            isTransferTo: false,
                            IsCustomerPartSearch: false,
                            searchType: "1"
                        });
                    },
                    results: function (data, page) {

                        if (data.hasOwnProperty("d")) {
                            if (data.d == "Session Expired") {
                                alert("Session expired.");
                                window.opener.location.href = window.opener.location.href;
                                window.close();
                            } else {
                                data = $.parseJSON(data.d);
                            }
                        }
                        else
                            data = $.parseJSON(data);

                        var more = (page.page * varPageSize) < data.Total;
                        return { results: $.parseJSON(data.results), more: more };
                    }
                }
            });

            if (parseInt($("[id$=hdnUserSelectedItem]").val()) > 0 && parseInt($("[id$=hdnUserSelectedWarehouseItemID]").val()) > 0) {
                SetSelectedItem($("[id$=hdnUserSelectedItemName]").val(), parseInt($("[id$=hdnUserSelectedItem]").val()), parseInt($("[id$=hdnUserSelectedWarehouseItemID]").val()))

                $("[id$=hdnUserSelectedItem]").val("");
                $("[id$=hdnUserSelectedWarehouseItemID]").val("");
                $("[id$=hdnUserSelectedItemName]").val("");
            }

            $('#txtItemTo').on("change", function (e) {
                if (e.added != null) {
                    $("#hdnCurrentSelectedItemTo").val(e.added.id.toString().split("-")[0]);
                    document.getElementById("lkbItemToSelected").click();
                }
            })

            $('#txtItemTo').on("select2-removed", function (e) {
                $("#hdnCurrentSelectedItemTo").val("");
                document.getElementById("lkbItemToRemoved").click();
            })

            $('#txtItemTo').select2(
            {
                placeholder: 'Select Item',
                minimumInputLength: varCharSearch,
                multiple: false,
                formatResult: formatItem,
                width: "100%",
                dropdownCssClass: 'bigdrop',
                dataType: "json",
                allowClear: true,
                ajax: {
                    quietMillis: 500,
                    url: '../common/Common.asmx/GetSearchedItems',
                    type: 'POST',
                    params: {
                        contentType: 'application/json; charset=utf-8'
                    },
                    dataType: 'json',
                    data: function (term, page) {
                        return JSON.stringify({
                            searchText: term,
                            pageIndex: page,
                            pageSize: varPageSize,
                            divisionId: getDivisionId(),
                            isGetDefaultColumn: false,
                            warehouseID: 0,
                            searchOrdCusHistory: 0,
                            oppType: 2,
                            isTransferTo: true,
                            IsCustomerPartSearch: false,
                            searchType: "1"
                        });
                    },
                    results: function (data, page) {

                        if (data.hasOwnProperty("d")) {
                            if (data.d == "Session Expired") {
                                alert("Session expired.");
                                window.opener.location.href = window.opener.location.href;
                                window.close();
                            } else {
                                data = $.parseJSON(data.d);
                            }
                        }
                        else
                            data = $.parseJSON(data);

                        var more = (page.page * varPageSize) < data.Total;
                        return { results: $.parseJSON(data.results), more: more };
                    }
                }
            });
            $("#s2id_txtItemTo").addClass("select2-container-disabled");

            var cancelDropDownClosing = false;
            $('[id$=raditems] .rcbList li.rcbTemplate').click(function (e) {
                e.cancelBubble = true;
                if (e.stopPropagation) {
                    e.stopPropagation();
                }
            });
            StopPropagation = function (e) {
                e.cancelBubble = true;
                if (e.stopPropagation) {
                    e.stopPropagation();
                }
            }
            onDropDownClosing = function (sender, args) {
                cancelDropDownClosing = false;
            }
            onCheckBoxClick = function (chk, comboBoxID) {
                //Prevent second RadComboBox from closing.
                cancelDropDownClosing = true;
                var text = "";
                var values = "";
                var combo = $find(comboBoxID);
                var items = combo.get_items();
                for (var i = 0; i < items.get_count() ; i++) {
                    var item = items.getItem(i);
                    var chk1 = $get(combo.get_id() + "_i" + i + "_chkSelect");
                    if (chk1.checked) {
                        text += item.get_text() + ",";
                    }
                }
                text = removeLastComma(text);
                if (text.length > 0) {
                    combo.set_text(text);
                }
                else {
                    combo.set_text("");
                }
            }

            $("[id$=chkTransferToItem]").change(function () {
                if ($(this).is(":checked")) {
                    $("#s2id_txtItemTo").removeClass("select2-container-disabled")
                    $("#s2id_txtItemTo").addClass("select2-container-enabled");
                } else {
                    $("#s2id_txtItemTo").removeClass("select2-container-enabled")
                    $("#s2id_txtItemTo").addClass("select2-container-disabled");
                    document.getElementById("lkbTansferToItemUnchecked").click();
                }
            })
        });

        function AddItem(event) {
            var keycode = event.which || event.keyCode;
            if (keycode == '13') {
                $("[id$=btnAdd]").click();
            }
        }

        function FocusNextTextBox(txtUnits) {
            var next = $(txtUnits).closest("tr").next().find("input[id$=txtUnits]");
            if (next.length > 0) {
                setTimeout(function () {
                    next.focus();
                }, 0);
            } else {
                next = $(txtUnits).closest("tr").next().find("a[id$=hplSerialLot]");
                if (next.length > 0) {
                    setTimeout(function () {
                        next.focus();
                    }, 0);
                }
            }
        }

        function OpenSertailLot(itemID, warehouseItemID, itemType, rowId) {
            if (warehouseItemID === -1) {
                if ($find('radFromLocation') != null && $find('radFromLocation').get_value() == '') {
                    alert("Select From Location");
                    $find('radFromLocation').focus();
                    return false;
                } else {
                    window.open('../opportunity/frmAddSerialLotNumbers.aspx?oppID=0&oppItemID=0&itemID=' + itemID + '&warehouseItemID=' + $find('radFromLocation').get_value() + "&itemType=" + itemType + '&type=2' + '&rowId=' + rowId, '', 'toolbar=no,titlebar=no,left=300,top=100,width=700,height=350,scrollbars=yes,resizable=no')
                    return false;
                }
            } else {
                window.open('../opportunity/frmAddSerialLotNumbers.aspx?oppID=0&oppItemID=0&itemID=' + itemID + '&warehouseItemID=' + warehouseItemID + "&itemType=" + itemType + '&type=2' + '&rowId=' + rowId, '', 'toolbar=no,titlebar=no,left=300,top=100,width=700,height=350,scrollbars=yes,resizable=no')
                return false;
            }
        }

        function SetSelectedSerialLot(vcSerialLot, Id) {
            if (Id == "-1") {
                $("[id$=hdnSerialLotToTransfer]").val(vcSerialLot);
                $("[id$=hdnSelectedId]").val(Id);
                document.getElementById("lkbSelectedSerialLot").click();
            } else {
                $("[id$=hdnSelectedSerialLot]").val(vcSerialLot);
                $("[id$=hdnSelectedId]").val(Id);
                document.getElementById("lkbSelectedSerialLot").click();
            }
        }

        function OpenMassTransferHistory() {
            var h = screen.height;
            var w = screen.width;

            window.open('../opportunity/frmMassStockTransferHistory.aspx', '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }

        function Confirmed() {
            $("[id$=divConfirmation]").css("display", "none");
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanelException" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Label ID="lblException" runat="server" Style="color: red;"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Stock Transfer
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <div class="cont2">
        <div class="col-1-1" style="float: left">
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" class="col-1-1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="col-1-2">
                    <div class="content">
                        <div class="form-inline">
                            <div class="col-9-12">
                                <div class="form-group">
                                    <label class="col-6-12">
                                        <img src="../images/TruckGreen.png" style="position: absolute; left: 18px; top: 62px; width: 28px" />
                                        <input type="image" name="ibtnMassTransferHistory" id="ibtnMassTransferHistory" title="Last 20 Transferred Items" src="../images/Audit.png" onclick="return OpenMassTransferHistory();" style="position: absolute; left: 50px; top: 62px;" />
                                        Organization From <span class="required">*</span></label>
                                    <div class="col-6-12">
                                        <telerik:RadComboBox ID="radcmbSTFrom" ClientIDMode="Static" Width="100%" runat="server" TabIndex="5" AutoPostBack="true"></telerik:RadComboBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-3-12">
                                <div class="form-group">
                                    <label class="col-3-12">Contact <span class="required">*</span></label>
                                    <div class="col-9-12">
                                        <asp:DropDownList ID="ddlFromContact" runat="server" TabIndex="6"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-1-2">
                    <div class="content">
                        <div class="form-inline">
                            <div class="col-9-12">
                                <div class="form-group">
                                    <label class="col-3-12">Organization To <span class="required">*</span></label>
                                    <div class="col-9-12">
                                        <telerik:RadComboBox ID="radcmbSTTo" ClientIDMode="Static" Width="100%" runat="server" TabIndex="7" AutoPostBack="true"></telerik:RadComboBox>
                                    </div>
                                </div>
                            </div>
                            <div class="col-3-12">
                                <div class="form-group">
                                    <label class="col-3-12">Contact <span class="required">*</span></label>
                                    <div class="col-9-12">
                                        <asp:DropDownList ID="ddlToContact" runat="server" TabIndex="8"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="radcmbSTFrom" />
                <asp:AsyncPostBackTrigger ControlID="radcmbSTTo" />
                <asp:AsyncPostBackTrigger ControlID="ddlFromContact" />
                <asp:AsyncPostBackTrigger ControlID="ddlToContact" />
                <asp:AsyncPostBackTrigger ControlID="gvItems" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    <asp:Panel ID="pnlItem" runat="server" onkeypress="AddItem(event);" TabIndex="10">
        <div class="cont1" style="border-bottom: 0px !important; padding-bottom: 0px !important;">
            <div class="col-1-2">
                <asp:UpdatePanel class="col-1-1" ID="UpdatePanelItem" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                    <ContentTemplate>
                        <div class="col-1-1" id="divItem" runat="server">
                            <div class="form-group">
                                <label class="col-3-12" style="width: 18%;">
                                    Item <span class="required">*</span>
                                </label>
                                <div class="col-9-12" style="width: 81.5%;">
                                    <asp:TextBox ID="txtItem" ClientIDMode="Static" runat="server" TabIndex="300"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="col-1-2">
                <asp:UpdatePanel class="col-1-1" ID="UpdatePanelItemTo" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                    <ContentTemplate>
                        <div class="col-1-1" id="divItemTo" runat="server">
                            <div class="form-group">
                                <label class="col-3-12" style="width: 31%;">
                                    <asp:CheckBox runat="server" ID="chkTransferToItem" Text="Transfer to another Item" />
                                </label>
                                <div class="col-9-12" style="width: 68%;">
                                    <asp:TextBox ID="txtItemTo" ClientIDMode="Static" runat="server" TabIndex="300"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
        <asp:UpdatePanel ID="upnl" runat="server" ChildrenAsTriggers="true">
            <ContentTemplate>
                <asp:HiddenField ID="hdnCurrentSelectedItem" runat="server" />
                <asp:HiddenField ID="hdnCurrentSelectedItemTo" runat="server" />
                <asp:HiddenField ID="hdnSelectedItems" ClientIDMode="Static" runat="server" />
                <asp:HiddenField ID="hdnCurrentTextBox" ClientIDMode="Static" runat="server" />
                <asp:LinkButton ID="lkbItemSelected" runat="server" Style="display: none"></asp:LinkButton>
                <asp:LinkButton ID="lkbItemRemoved" runat="server" Style="display: none"></asp:LinkButton>
                <asp:LinkButton ID="lkbItemToSelected" runat="server" Style="display: none"></asp:LinkButton>
                <asp:LinkButton ID="lkbItemToRemoved" runat="server" Style="display: none"></asp:LinkButton>
                <asp:LinkButton ID="lkbTansferToItemUnchecked" runat="server" Style="display: none"></asp:LinkButton>
                <asp:HiddenField ID="hdnSelectedSerialLot" runat="server" />
                <asp:HiddenField ID="hdnSelectedId" runat="server" />
                <asp:LinkButton ID="lkbSelectedSerialLot" runat="server" Style="display: none"></asp:LinkButton>
                <div class="cont1" style="border-top: 0px !important;">
                    <div class="col-1-1">
                        <div class="col-1-2">
                            <div class="content">
                                <div class="form-group">
                                    <label class="col-3-12" style="width: 18%;">From Location <span class="required">*</span></label>
                                    <div class="col-9-12" style="width: 81.5%;">
                                        <telerik:RadComboBox ID="radFromLocation" runat="server" Width="100%" DropDownWidth="800px" AutoPostBack="true" ClientIDMode="Static" DropDownCssClass="multipleRowsColumns">
                                            <HeaderTemplate>
                                                <ul>
                                                    <li class="col1">Warehouse</li>
                                                    <li class="col2">Attributes</li>
                                                    <li class="col3">On Hand</li>
                                                    <li class="col4">On Order</li>
                                                    <li class="col5">On Allocation</li>
                                                    <li class="col6">BackOrder</li>
                                                </ul>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <ul>
                                                    <li style="display: none">
                                                        <%# DataBinder.Eval(Container.DataItem, "numWareHouseItemId")%></li>
                                                    <li class="col1">
                                                        <%# DataBinder.Eval(Container.DataItem, "vcWareHouse")%></li>
                                                    <li class="col2">
                                                        <%# DataBinder.Eval(Container.DataItem, "Attr") %>&nbsp;</li>
                                                    <li class="col3">
                                                        <%# DataBinder.Eval(Container.DataItem, "numOnHand")%>&nbsp;
                                            <%# DataBinder.Eval(Container.DataItem, "vcUnitName")%></li>
                                                    <li class="col4">
                                                        <%# DataBinder.Eval(Container.DataItem, "numOnOrder")%>
                                            &nbsp;</li>
                                                    <li class="col5">
                                                        <%# DataBinder.Eval(Container.DataItem, "numAllocation")%>&nbsp;</li>
                                                    <li class="col6">
                                                        <%# DataBinder.Eval(Container.DataItem, "numBackOrder")%>&nbsp;
                                            <asp:Label runat="server" ID="lblWareHouseID" Text='<%#DataBinder.Eval(Container.DataItem, "numWareHouseID")%>'
                                                Style="display: none"></asp:Label></li>
                                                </ul>
                                            </ItemTemplate>
                                        </telerik:RadComboBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-1-2">
                            <div class="content">
                                <div class="form-group">
                                    <label class="col-3-12" style="width: 31%;">To Location <span class="required">*</span></label>
                                    <div class="col-9-12" style="width: 68%;">
                                        <telerik:RadComboBox ID="radToLocation" runat="server" Width="100%" DropDownWidth="800px" AutoPostBack="true" ClientIDMode="Static" DropDownCssClass="multipleRowsColumns">
                                            <HeaderTemplate>
                                                <ul>
                                                    <li class="col1">Warehouse</li>
                                                    <li class="col2">Attributes</li>
                                                    <li class="col3">On Hand</li>
                                                    <li class="col4">On Order</li>
                                                    <li class="col5">On Allocation</li>
                                                    <li class="col6">BackOrder</li>
                                                </ul>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <ul>
                                                    <li style="display: none">
                                                        <%# DataBinder.Eval(Container.DataItem, "numWareHouseItemId")%></li>
                                                    <li class="col1">
                                                        <%# DataBinder.Eval(Container.DataItem, "vcWareHouse")%></li>
                                                    <li class="col2">
                                                        <%# DataBinder.Eval(Container.DataItem, "Attr") %>&nbsp;</li>
                                                    <li class="col3">
                                                        <%# DataBinder.Eval(Container.DataItem, "numOnHand")%>&nbsp;
                                            <%# DataBinder.Eval(Container.DataItem, "vcUnitName")%></li>
                                                    <li class="col4">
                                                        <%# DataBinder.Eval(Container.DataItem, "numOnOrder")%>
                                            &nbsp;</li>
                                                    <li class="col5">
                                                        <%# DataBinder.Eval(Container.DataItem, "numAllocation")%>&nbsp;</li>
                                                    <li class="col6">
                                                        <%# DataBinder.Eval(Container.DataItem, "numBackOrder")%>&nbsp;
                                                    <asp:Label runat="server" ID="lblWareHouseToID" Text='<%#DataBinder.Eval(Container.DataItem, "numWareHouseID")%>' Style="display: none"></asp:Label></li>
                                                </ul>
                                            </ItemTemplate>
                                        </telerik:RadComboBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-1-1">
                        <div class="col-1-2">
                            <div class="content" style="line-height: 27px">
                                <div class="col-4-12">
                                    <div class="form-group">
                                        <label class="col-3-12">SKU:</label>
                                        <div class="col-9-12">
                                            <asp:Label ID="lblSKU" runat="server" Style="line-height: 40px" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4-12">
                                    <div class="form-group">
                                        <label class="col-3-12">Item ID:</label>
                                        <div class="col-9-12">
                                            <asp:Label ID="lblItemID" runat="server" Style="line-height: 40px" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4-12">
                                    <div class="form-group">
                                        <label class="col-3-12">Attributes:</label>
                                        <div class="col-9-12">
                                            <asp:Label ID="lblAttributes" runat="server" Style="line-height: 40px" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-1-2">
                            <div class="content" style="line-height: 27px">
                                <div class="col-4-12">
                                    <div class="form-group">
                                        <label class="col-3-12">SKU:</label>
                                        <div class="col-9-12">
                                            <asp:Label ID="lblToSKU" runat="server" Style="line-height: 40px" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4-12">
                                    <div class="form-group">
                                        <label class="col-3-12">Item ID:</label>
                                        <div class="col-9-12">
                                            <asp:Label ID="lblToItemID" runat="server" Style="line-height: 40px" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-4-12">
                                    <div class="form-group">
                                        <label class="col-3-12">Attributes:</label>
                                        <div class="col-9-12">
                                            <asp:Label ID="lblToAttributes" runat="server" Style="line-height: 40px" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-1-1 text-center" style="padding-top: 20px">
                        <div class="col-2-12">
                            &nbsp;
                        </div>
                        <div class="col-6-12">
                            <div class="form-group">
                                <b>Qty / Serial #s to transfer:</b>
                                <asp:TextBox ID="txtUnitsToTransfer" runat="server" Style="width: 80px; display: inline"></asp:TextBox>
                                <asp:HyperLink ID="hplSerialLotToTransfer" NavigateUrl="#" runat="server" Visible="false">Added(0)</asp:HyperLink>
                                <asp:HiddenField ID="hdnSerialLotToTransfer" runat="server" />
                                <asp:Button AccessKey="A" ID="btnAdd" runat="server" CssClass="OrderButton btn btn-primary" Text="Add" OnClientClick="return GetSelectedItems();" style="padding:10px;"></asp:Button>
                                <asp:Button ID="btnEditCancel" runat="server" Text="Cancel" CssClass="btn btn-primary OrderButton" style="padding:10px;"></asp:Button>
                            </div>
                        </div>
                        <div class="col-2-12">
                            &nbsp;
                        </div>
                    </div>
                </div>
                <div class="cont2 no-pad">
                    <div class="col-1-1" style="font-weight: bold; text-align: center;">
                        <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                    </div>
                    <div class="col-1-1">
                        <asp:GridView ID="gvItems" runat="server" CssClass="tblPrimary" ShowFooter="true" AutoGenerateColumns="false" DataKeyField="numFromItemCode"
                            ClientIDMode="AutoID" Width="100%" ShowHeaderWhenEmpty="true" UseAccessibleHeader="true">
                            <Columns>
                                <asp:BoundField DataField="numFromItemCode" HeaderText="From Item ID" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"></asp:BoundField>
                                <asp:TemplateField HeaderText="Transfer Qty" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnSerialLot" runat="server" />
                                        <asp:HyperLink ID="hplSerialLot" NavigateUrl="#" runat="server" onblur="FocusNextTextBox(this)">Added(0)</asp:HyperLink>
                                        <asp:TextBox ID="txtUnits" CssClass="signup" Width="40" runat="server" onblur="FocusNextTextBox(this)" Text='<%# DataBinder.Eval(Container,"DataItem.numUnitHour") %>' OnTextChanged="txtUnitsGrid_TextChanged" AutoPostBack="true"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="vcFrom" HeaderText="From" HeaderStyle-Width="50%"></asp:BoundField>
                                <asp:BoundField DataField="numFromAvailable" HeaderText="Current Availability" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"></asp:BoundField>
                                <asp:BoundField DataField="numFromBackorder" HeaderText="Back Order" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"></asp:BoundField>
                                <asp:BoundField DataField="numToItemCode" HeaderText="To Item ID" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"></asp:BoundField>
                                <asp:BoundField DataField="vcTo" HeaderText="To" HeaderStyle-Width="50%"></asp:BoundField>
                                <asp:BoundField DataField="numToAvailable" HeaderText="Projected Availability" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"></asp:BoundField>
                                <asp:BoundField DataField="numToBackOrder" HeaderText="Back Order" HeaderStyle-Wrap="false" ItemStyle-Wrap="false"></asp:BoundField>
                                <asp:TemplateField ItemStyle-Width="25">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkDelete" CommandName="Delete" CssClass="btn btn-xs btn-danger" runat="server"><i class="fa fa-trash"></i></asp:LinkButton>
                                        <asp:HiddenField ID="hdnRowId" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.Id")%>' />
                                        <asp:HiddenField ID="hdnIsSerial" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.IsSerial")%>' />
                                        <asp:HiddenField ID="hdnIsLot" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.IsLot")%>' />
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        Items are not added for stock transfer.
                                    </EditItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                No items added.
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="radFromLocation" />
                <asp:AsyncPostBackTrigger ControlID="radToLocation" />
                <asp:AsyncPostBackTrigger ControlID="lkbItemSelected" />
                <asp:AsyncPostBackTrigger ControlID="lkbItemToRemoved" />
                <asp:AsyncPostBackTrigger ControlID="lkbItemToSelected" />
                <asp:AsyncPostBackTrigger ControlID="lkbItemRemoved" />
                <asp:AsyncPostBackTrigger ControlID="lkbSelectedSerialLot" />
                <asp:AsyncPostBackTrigger ControlID="lkbTansferToItemUnchecked" />
            </Triggers>
        </asp:UpdatePanel>
    </asp:Panel>
    <asp:UpdatePanel ID="UpdatePanelHiddenField" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div class="overlay" id="divConfirmation" runat="server" style="display: none">
                <div class="overlayContent" style="color: #000; background-color: #FFF; width: 400px; border: 1px solid #ddd;">
                    <div class="dialog-body" style="margin:10px; font-size:14px">
                        <asp:Label runat="server" ID="lblConfirmation"></asp:Label>
                    </div>
                    <div class="dialog-footer" style="margin:10px; text-align:center">
                        <asp:HiddenField runat="server" ID="hdnConfirmClicked" />
                        <asp:Button ID="btnSaveCloseConfirmation" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="" OnClientClick="return Confirmed();" />
                        <asp:Button ID="btnCancelConfirmation" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="Cancel" />

                        <asp:HiddenField ID="hdnUserSelectedItem" runat="server" />
                        <asp:HiddenField ID="hdnUserSelectedWarehouseItemID" runat="server" />
                        <asp:HiddenField ID="hdnUserSelectedItemName" runat="server" />
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div class="cont1">
        <asp:UpdatePanel ID="UpdatePanelButtons" runat="server" class="col-1-1">
            <ContentTemplate>
                <div style="float: none; margin-top: 10px; text-align: center">
                    <asp:LinkButton ID="btnTransferStock" CssClass="btn btn-primary OrderButton StockTransfer" Width="145" runat="server" style="padding:10px;"><i class="fa fa-shopping-cart" style="font-size:20px;"></i>&nbsp;<span> Transfer Stock</span> </asp:LinkButton>
                    <asp:LinkButton ID="btnTransferOrder" CssClass="btn btn-primary OrderButton StockTransfer" Width="190" runat="server" style="padding:10px;" OnClick="btnTransferOrder_Click"><i class="fa fa-truck" style="font-size:20px;"></i>&nbsp; Create Transfer Order</span></asp:LinkButton>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
