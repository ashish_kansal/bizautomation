﻿<%@ Page Title="Related Items" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master"
    CodeBehind="frmOpenRelatedItems.aspx.vb" Inherits=".frmOpenRelatedItems" %>

<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        function Close() {
            //opener.location.reload(true);
            window.close()
            return false;
        }
    </script>
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="FiltersAndViews1">
    <table style="width:100%">
        <tr>
            <td style="text-align:right;">
                <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="javascript: return Close();" Text="Close" />
            </td>
        </tr>
    </table>
    
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="PageTitle">
    Related Items
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <asp:GridView ID="gvRelatedItems" runat="server" AutoGenerateColumns="false" CssClass="dg"
        Width="1010px" HeaderStyle-HorizontalAlign="Center" CellPadding="3" CellSpacing="3">
        <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is" />
        <HeaderStyle CssClass="hs" Height="30" Font-Size="11" />
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <%#CCommon.GetImageHTML(Eval("vcPathForTImage"), 1, 100, 56)%>
                </ItemTemplate>
                <ItemStyle HorizontalAlign="Center" Width="100px" Height="56px" />
            </asp:TemplateField>
            <asp:BoundField DataField="vcItemName" HeaderText="Item" />
            <asp:BoundField DataField="txtItemDesc" HeaderText="Description" />
            <asp:TemplateField HeaderText="Qty" ItemStyle-Width="70" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:TextBox ID="txtQty" runat="server" Width="50" Text="1"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="monListPrice" HeaderText="List"  ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="vcRelationship" HeaderText="Relationship" />
            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25">
                <ItemTemplate>
                    <asp:LinkButton ID="lnkAdd" CommandName="Add" CssClass="hyperlink" runat="server" CommandArgument='<%# Eval("numItemCode") %>'><img alt="" src="../images/AddRecord.png" /></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    <asp:TextBox ID="txtTax" runat="server" Style="display: none"></asp:TextBox>
    <input id="Taxable" runat="server" type="hidden" />
    <asp:CheckBoxList ID="chkTaxItems" CssClass="normal1" runat="server" RepeatDirection="Horizontal"
        RepeatColumns="3" Visible="false">
    </asp:CheckBoxList>
</asp:Content>
