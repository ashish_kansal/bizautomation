﻿Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Opportunities
    Public Class frmMassStockTransferHistory
        Inherits BACRMPage

#Region "Page Events"

#End Region
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lblError.Text = ""
                divError.Style.Add("display", "none")

                If Not Page.IsPostBack Then
                    BindData()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                lblError.Text = ex.Message
                divError.Style.Add("display", "")
            End Try
        End Sub

#Region "Private Methods"

        Private Sub BindData()
            Try
                Dim objItem As New BACRM.BusinessLogic.Item.CItems
                objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                objItem.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                gvTransfer.DataSource = objItem.GetMassStockTransferHistory()
                gvTransfer.DataBind()
            Catch ex As Exception
                Throw
            End Try
        End Sub

#End Region

    End Class
End Namespace