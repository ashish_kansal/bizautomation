﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace BACRM.UserInterface.Opportunities

    Partial Public Class frmSalesFulfillmentOrder

        '''<summary>
        '''divWarehouse control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents divWarehouse As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''btnClosePriceHistory control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnClosePriceHistory As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''radcmbWarehouses control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents radcmbWarehouses As Global.Telerik.Web.UI.RadComboBox

        '''<summary>
        '''btnPrintLabels control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnPrintLabels As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''chkExpandGrid control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents chkExpandGrid As Global.System.Web.UI.WebControls.CheckBox

        '''<summary>
        '''UpdatePanel1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents UpdatePanel1 As Global.System.Web.UI.UpdatePanel

        '''<summary>
        '''ddlBatchAction control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlBatchAction As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''btnGoBatch control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnGoBatch As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''UpdatePanel2 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents UpdatePanel2 As Global.System.Web.UI.UpdatePanel

        '''<summary>
        '''ddlFilterBy control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents ddlFilterBy As Global.System.Web.UI.WebControls.DropDownList

        '''<summary>
        '''radFilterValue control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents radFilterValue As Global.Telerik.Web.UI.RadComboBox

        '''<summary>
        '''radOrderStatus control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents radOrderStatus As Global.Telerik.Web.UI.RadComboBox

        '''<summary>
        '''radShippingZone control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents radShippingZone As Global.Telerik.Web.UI.RadComboBox

        '''<summary>
        '''PanelShipping control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents PanelShipping As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''radDtShipped control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents radDtShipped As Global.Telerik.Web.UI.RadDatePicker

        '''<summary>
        '''divAlert control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents divAlert As Global.System.Web.UI.HtmlControls.HtmlGenericControl

        '''<summary>
        '''lblException control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblException As Global.System.Web.UI.WebControls.Label

        '''<summary>
        '''bizPager control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents bizPager As Global.Wuqi.Webdiyer.AspNetPager

        '''<summary>
        '''lkbClearFilter control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lkbClearFilter As Global.System.Web.UI.WebControls.LinkButton

        '''<summary>
        '''tdGridConfiguration control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents tdGridConfiguration As Global.System.Web.UI.HtmlControls.HtmlAnchor

        '''<summary>
        '''gvSearch control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents gvSearch As Global.Telerik.Web.UI.RadGrid

        '''<summary>
        '''txtDelItemIds control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtDelItemIds As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtSaveAPIItemIds control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtSaveAPIItemIds As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtSortColumn control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtSortColumn As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtSortOrder control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtSortOrder As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtTotalPageItems control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtTotalPageItems As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtTotalRecordsItems control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtTotalRecordsItems As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtSortChar control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtSortChar As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''btnGo1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnGo1 As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''txtCurrrentPage control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtCurrrentPage As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtSelOppId control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtSelOppId As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtSelBizDocsId control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtSelBizDocsId As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''hdnFromSalesOrderID control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdnFromSalesOrderID As Global.System.Web.UI.WebControls.HiddenField

        '''<summary>
        '''txtGridColumnFilter control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtGridColumnFilter As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''UpdateProgress control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents UpdateProgress As Global.System.Web.UI.UpdateProgress
    End Class
End Namespace
