﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.TimeAndExpense
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Documents

Public Class orderPOS
    Inherits BACRMUserControl

    Dim objCommon As CCommon
    Dim objItems As CItems
    Dim strValues As String
    Dim dtOppAtributes As DataTable
    Dim dsTemp As DataSet
    Dim lngDivId, lngCntID, lngOppId, OppBizDocID, JournalId As Long
    Dim dtOppBiDocItems As DataTable
    Dim strMessage As String
    Dim ResponseMessage As String
    Dim boolPurchased As Boolean = False
    Dim IsFromCreateBizDoc As Boolean = False
    Dim IsFromSaveAndOpenOrderDetails As Boolean = False
    Dim IsFromSaveAndNew As Boolean = False
    Dim boolFlag As Boolean = True
    Dim m_aryRightsForItems(), m_aryRightsForAuthoritativ(), m_aryRightsForNonAuthoritativ() As Integer
    Dim IsPOS As Short

    Private _OppType As Integer
    Public Property OppType() As Integer
        Get
            Return _OppType
        End Get
        Set(ByVal value As Integer)
            _OppType = value
        End Set
    End Property

    Public _PageType As PageType
    Enum PageType As Integer
        Sales
        Purchase
        AddOppertunity
        AddEditOrder
    End Enum

    Private _IsStockTransfer As Boolean
    Public Property IsStockTransfer() As Boolean
        Get
            Return _IsStockTransfer
        End Get
        Set(ByVal value As Boolean)
            _IsStockTransfer = value
        End Set
    End Property

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            Dim objCommon As New CCommon
            objCommon.InitializeClientSideTemplate(radCmbItem)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            IsPOS = 1

            If Not IsPostBack Then

                hdnPOS.Value = IsPOS
                If (_PageType <> PageType.AddEditOrder) Then
                    createSet()
                End If

                chkSearchOrderCustomerHistory.Visible = IIf(Session("SearchOrderCustomerHistory") = True, True, False)

                'imgItem.Visible = False
                'hplImage.Visible = False
                txtDivID.Text = 0

                objCommon = New CCommon
                objCommon.UserCntID = Session("UserContactID")
                'For template hover selection
                Dim ds As DataSet
                ds = objCommon.NewOppItems(Session("DomainID"), 0, 0, "")
                CreateColums(ds.Tables(1))

                BindButtons()

                objCommon.sb_FillConEmpFromDBSel(ddlWOAssignTo, Session("DomainID"), 0, 0)
                ' CreateClone()
            End If

            Dim eventTarget As String = If((Me.Request("__EVENTTARGET") Is Nothing), String.Empty, Me.Request("__EVENTTARGET"))

            If eventTarget = "btnVendorCostChange" Then
                CalculateProfit()
            Else
                If ViewState("SOItems") Is Nothing Then
                    createSet()
                End If
                UpdateDataTable()


                If ddltype.SelectedValue = "S" Then
                    txtunits.Attributes.Add("onkeypress", "CheckNumber(1,event);TempHideButton()")
                Else
                    txtunits.Attributes.Add("onkeypress", "CheckNumber(2,event);TempHideButton()")
                End If

                txtprice.Attributes.Add("onkeypress", "CheckNumber(1,event)")
                txtItemDiscount.Attributes.Add("onkeypress", "CheckNumber(1,event)")
                ddUOM.Attributes.Add("OnChange", "TempHideButton()")

                If (_PageType = PageType.Purchase) Then
                    dgItems.Columns(4).Visible = False
                    dgItems.Columns(14).Visible = False


                    btnSave.Text = "Save & Create PO "

                    radPer.Checked = False
                    pnlDiscount.Visible = False
                End If


                If (_PageType = PageType.AddOppertunity) Then
                    dgItems.Columns(4).Visible = False
                    dgItems.Columns(14).Visible = False
                End If

                If (_PageType = PageType.AddOppertunity) Then
                    btnSave.Text = "Save Proceed "
                    'btnClose.Text = "Close "
                End If

                If (_PageType = PageType.AddEditOrder) Then
                    btnSave.Text = "Save & Close "
                    'btnClose.Text = "Close "
                    If OppType = 2 Then
                        radPer.Checked = False
                        pnlDiscount.Visible = False
                    End If
                End If

                If (OppType = 2) Then
                    dgItems.Columns(15).Visible = False
                End If


                m_aryRightsForItems = GetUserRightsForPage_Other(10, 5)

                If m_aryRightsForItems(RIGHTSTYPE.ADD) = 0 Then btnAdd.Visible = False
                'If m_aryRightsForItems(RIGHTSTYPE.UPDATE) = 0 Then dgItems.Columns(16).Visible = False
                If m_aryRightsForItems(RIGHTSTYPE.DELETE) = 0 Then dgItems.Columns(16).Visible = False

                m_aryRightsForAuthoritativ = GetUserRightsForPage_Other(10, 28)
                If m_aryRightsForAuthoritativ(RIGHTSTYPE.ADD) = 0 Then btnSave.Visible = False

                'If (OrderType = PageType.Sales Or OrderType = PageType.Purchase Or OrderType = PageType.AddOppertunity) Then
                '    dgItems.Columns(16).Visible = False
                'End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


    Private Sub CalculateUnitPrice(Optional ByVal boolBindGrid As Boolean = True)
        Try
            If boolBindGrid Then
                UpdateDetails()
            End If

            Dim objItems As New CItems
            objCommon = New CCommon
            If Not (radCmbItem.SelectedValue = "") Then

                Dim lngItemClass As Long
                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")
                objCommon.Mode = 13
                objCommon.Str = radCmbItem.SelectedValue
                lngItemClass = objCommon.GetSingleFieldValue()

                Dim dtUnit As DataTable
                objCommon.DomainID = Session("DomainID")
                objCommon.ItemCode = radCmbItem.SelectedValue
                objCommon.UnitId = ddUOM.SelectedValue
                dtUnit = objCommon.GetItemUOMConversion()
                hdvUOMConversionFactor.Value = dtUnit(0)("BaseUOMConversion")
                hdvPurchaseUOMConversionFactor.Value = dtUnit(0)("PurchaseUOMConversion")
                hdvSalesUOMConversionFactor.Value = dtUnit(0)("SaleUOMConversion")

                objItems.ItemCode = radCmbItem.SelectedValue

                If CCommon.ToBool(Session("bitRentalItem")) = True AndAlso CCommon.ToLong(Session("RentalItemClass")) > 0 AndAlso CCommon.ToLong(Session("RentalItemClass")) = lngItemClass Then
                    objItems.NoofUnits = 1
                    btnSave.Visible = False
                Else
                    objItems.NoofUnits = Math.Abs(CCommon.ToInteger(txtunits.Text)) * hdvUOMConversionFactor.Value
                End If

                objItems.OppId = CCommon.ToLong(GetQueryStringVal("opid"))

                If (_PageType = PageType.Sales Or _PageType = PageType.Purchase Or _PageType = PageType.AddOppertunity) Then
                    objItems.DivisionID = IIf(CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue)
                Else
                    objItems.DivisionID = CCommon.ToLong(GetQueryStringVal("DivId"))
                End If

                objItems.DomainID = Session("DomainID")

                objItems.byteMode = OppType

                objItems.Amount = 0
                If Not radWareHouse.SelectedValue = "" Then
                    objItems.WareHouseItemID = radWareHouse.SelectedValue
                Else
                    objItems.WareHouseItemID = Nothing
                End If

                Dim ds As DataSet
                Dim dtItemPricemgmt As DataTable

                ds = objItems.GetPriceManagementList1
                dtItemPricemgmt = ds.Tables(0)

                If dtItemPricemgmt.Rows.Count = 0 Then
                    dtItemPricemgmt = ds.Tables(2)
                End If

                dtItemPricemgmt.Merge(ds.Tables(1))

                tdSalesProfit.Visible = False

                If dtItemPricemgmt.Rows.Count > 0 Then
                    If Session("PriceBookDiscount") = 1 And OppType = 1 Then 'Display Price Book Discount seperately
                        txtprice.Text = String.Format("{0:0.00}", ds.Tables(2).Rows(0)("ListPrice"))

                        If dtItemPricemgmt.Rows(0)("tintDiscountTypeOriginal") = 1 Then 'Percentage
                            txtItemDiscount.Text = String.Format("{0:0.00}", dtItemPricemgmt.Rows(0)("decDiscountOriginal"))

                            radPer.Checked = True
                            radAmt.Checked = False
                        ElseIf dtItemPricemgmt.Rows(0)("tintDiscountTypeOriginal") = 2 Then 'Flat Amount
                            txtItemDiscount.Text = String.Format("{0:0.00}", CCommon.ToDecimal(dtItemPricemgmt.Rows(0)("decDiscountOriginal")) * objItems.NoofUnits)

                            radPer.Checked = False
                            radAmt.Checked = True
                        End If
                    Else
                        txtprice.Text = String.Format("{0:0.00}", dtItemPricemgmt.Rows(0)("ListPrice"))
                    End If

                    txtprice.Text = CCommon.ToDecimal(txtprice.Text) * IIf(OppType = 1, 1, hdvPurchaseUOMConversionFactor.Value)
                    hdnUnitCost.Value = CCommon.ToDecimal(dtItemPricemgmt.Rows(0)("ListPrice"))

                    If objItems.byteMode = 1 Then
                        CalculateProfit()
                    End If
                End If

                'Related Items Count
                objItems.ParentItemCode = radCmbItem.SelectedValue
                objItems.byteMode = 1
                Dim dtRelatedItems As DataTable = objItems.GetSimilarItem

                hplRelatedItems.Visible = False

                If dtRelatedItems(0)("Total") > 0 AndAlso objItems.DivisionID > 0 Then
                    hplRelatedItems.Visible = True
                    hplRelatedItems.Text = "Related Items (" & dtRelatedItems(0)("Total") & ")"
                    hplRelatedItems.Attributes.Add("onclick", String.Format("OpenRelatedItems('{0}','{1}','{2}','{3}')", OppType, _PageType, objItems.OppId, objItems.DivisionID))
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub CalculateProfit()
        If OppType = 1 Then

            Dim m_aryRightsForSalesProfit() As Integer
            m_aryRightsForSalesProfit = GetUserRightsForPage_Other(10, 21)

            If m_aryRightsForSalesProfit(RIGHTSTYPE.VIEW) <> 0 Then
                Dim objItems As New CItems

                tdSalesProfit.Visible = True

                Dim ds As DataSet
                Dim dtItemPricemgmt As DataTable

                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")

                Dim decVendorCost, decProfit, decSellPrice As Decimal

                decSellPrice = CCommon.ToDecimal(hdnUnitCost.Value)
                ''Primay VendorCost
                'objCommon.Mode = 6
                'objCommon.Str = radCmbItem.SelectedValue
                'decVendorCost = CCommon.ToDecimal(objCommon.GetSingleFieldValue())

                objItems.ItemCode = radCmbItem.SelectedValue
                objItems.NoofUnits = Math.Abs(CCommon.ToInteger(txtunits.Text)) * hdvUOMConversionFactor.Value
                objItems.OppId = CCommon.ToLong(GetQueryStringVal("opid"))

                objItems.DomainID = Session("DomainID")

                objItems.Amount = 0
                If Not radWareHouse.SelectedValue = "" Then
                    objItems.WareHouseItemID = radWareHouse.SelectedValue
                Else
                    objItems.WareHouseItemID = Nothing
                End If

                objItems.DivisionID = CCommon.ToLong(hdnVendorId.Value)  'Set Vendor
                objItems.byteMode = 2 'Set Purchase
                ds = objItems.GetPriceManagementList1
                dtItemPricemgmt = ds.Tables(0)

                If dtItemPricemgmt.Rows.Count = 0 Then
                    dtItemPricemgmt = ds.Tables(2)
                End If

                dtItemPricemgmt.Merge(ds.Tables(1))

                Dim dtUnit As DataTable
                objCommon.DomainID = Session("DomainID")
                objCommon.ItemCode = radCmbItem.SelectedValue
                objCommon.UnitId = ddUOM.SelectedValue
                dtUnit = objCommon.GetItemUOMConversion()
                hdvPurchaseUOMConversionFactor.Value = dtUnit(0)("PurchaseUOMConversion")

                decVendorCost = CCommon.ToDecimal(dtItemPricemgmt.Rows(0)("ListPrice")) * IIf(OppType = 1, hdvPurchaseUOMConversionFactor.Value, 1)

                lblPUnitCost.Text = String.Format("{0:#,##0.00}", decVendorCost)

                decProfit = (decSellPrice - decVendorCost)
                hdnVendorId.Value = dtItemPricemgmt.Rows(0)("numDivisionID")
                'lblProfit.Text = String.Format("{0:#,##0.00} / {1:#,##0.00} ({2})", decProfit, decVendorCost, FormatPercent(decProfit / IIf(decVendorCost = 0, 1, decVendorCost), 2))
                lblProfit.Text = String.Format("{0:#,##0.00} / {1:#,##0.00} ({2})", decProfit, decSellPrice, FormatPercent(decProfit / IIf(decSellPrice = 0, 1, decSellPrice), 2))
            End If
        End If
    End Sub
    'Sub LoadItems()
    '    Try
    '        If objCommon Is Nothing Then objCommon = New CCommon
    '        radCmbItem.DataSource = objCommon.NewOppItems(Session("DomainID"), 0, 1, radCmbItem.Text)
    '        radCmbItem.DataValueField = "numItemCode"
    '        radCmbItem.DataBind()

    '        hplPrice.CssClass = "hyperlink"
    '        hplPrice.Text = "Unit Price"
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    Private Sub radCmbItem_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbItem.SelectedIndexChanged
        Try
            If ViewState("SOItems") Is Nothing Then
                createSet()
            End If
            'UpdateDetails()
            If radCmbItem.SelectedValue <> "" Then
                ItemDropDownSelect()
            End If
            CalculateUnitPrice()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub ItemDropDownSelect()
        Try
            LoadItemDetails()

            Dim ds As DataSet
            Dim dt As DataTable
            objItems.ItemCode = radCmbItem.SelectedValue
            'dt = objItems.GetChildItems
            objItems.ItemGroupID = 0
            objItems.DomainID = Session("DomainID")
            ds = objItems.GetItemGroups
            dt = ds.Tables(0)
            dt.TableName = "ChildItems"

            dsTemp = ViewState("SOItems")
            Dim dtitem As DataTable
            dtitem = dsTemp.Tables(2)
            dtitem.Rows.Clear()

            If dt.Rows.Count > 0 Then
                btnOptionsNAccessories.Attributes.Add("onclick", "javascript:window.open('frmOptionsNAccessories.aspx?ItemCode=" & radCmbItem.SelectedValue & " ','','toolbar=no,titlebar=no,left=300,top=250,width=650,height=400,scrollbars=yes,resizable=yes');return false;")
                btnOptionsNAccessories.Visible = True

                Dim i As Int32 = 0
                Dim dr2 As DataRow

                For Each drow As DataRow In dt.Rows

                    'Dim dtKitItem As DataTable
                    'objItems.ItemCode = drow("numItemCode")
                    'dtKitItem = objItems.GetChildItems
                    'dtKitItem.TableName = "ChildItems"

                    'If dtKitItem.Rows.Count > 0 Then
                    '    Dim drSub As DataRow() = dtKitItem.Select("numItemCode = " & drow("numDefaultSelect").ToString())
                    '    If drSub.Count = 1 Then
                    '        dr2 = dtitem.NewRow

                    '        dr2("numOppChildItemID") = CType(IIf(IsDBNull(dtitem.Compute("MAX(numOppChildItemID)", "")), 0, dtitem.Compute("MAX(numOppChildItemID)", "")), Integer) + 1 'dtChildItems.Rows.Count + 1
                    '        dr2("numoppitemtCode") = 0
                    '        dr2("numItemCode") = drSub(0)("numItemCode")
                    '        dr2("numQtyItemsReq") = drSub(0)("numQtyItemsReq")
                    '        dr2("vcItemName") = drSub(0)("vcItemName")
                    '        dr2("monListPrice") = drSub(0)("monListPrice")
                    '        dr2("UnitPrice") = drSub(0)("UnitPrice").Split("/")(0)
                    '        dr2("charItemType") = drSub(0)("charItemType")
                    '        dr2("txtItemDesc") = drSub(0)("txtItemDesc")
                    '        dr2("Op_Flag") = 1
                    '        dr2("ItemType") = drSub(0)("ItemType")
                    '        dr2("numWarehouseItmsID") = drSub(0)("numWarehouseItmsID")
                    '        dr2("vcWareHouse") = drSub(0)("vcWareHouse")
                    '        'If sColumnsArrayForSearch(6) = "P" And chkDropShip.Checked = False Then dr2("numWarehouseItmsID") = IIf(CType(dgGridItem.FindControl("radKitWareHouse"), Telerik.Web.UI.RadComboBox).SelectedValue = "", 0, CType(dgGridItem.FindControl("radKitWareHouse"), Telerik.Web.UI.RadComboBox).SelectedValue)
                    '        dtitem.Rows.Add(dr2)
                    '    End If
                    'Else
                    dr2 = dtitem.NewRow
                    dr2("numOppChildItemID") = CType(IIf(IsDBNull(dtitem.Compute("MAX(numOppChildItemID)", "")), 0, dtitem.Compute("MAX(numOppChildItemID)", "")), Integer) + 1 'dtChildItems.Rows.Count + 1
                    dr2("numoppitemtCode") = 0
                    dr2("numItemCode") = drow("numItemCode")
                    dr2("numQtyItemsReq") = drow("numQtyItemsReq")
                    dr2("vcItemName") = drow("vcItemName")
                    dr2("monListPrice") = drow("monListPrice")
                    dr2("UnitPrice") = drow("UnitPrice").Split("/")(0)
                    dr2("charItemType") = drow("charItemType")
                    dr2("txtItemDesc") = drow("txtItemDesc")
                    dr2("Op_Flag") = 1
                    dr2("ItemType") = drow("ItemType")
                    dr2("numWarehouseItmsID") = drow("numWarehouseItmsID")
                    dr2("vcWareHouse") = drow("vcWareHouse")

                    'If sColumnsArrayForSearch(6) = "P" And chkDropShip.Checked = False Then dr2("numWarehouseItmsID") = IIf(CType(dgGridItem.FindControl("radKitWareHouse"), Telerik.Web.UI.RadComboBox).SelectedValue = "", 0, CType(dgGridItem.FindControl("radKitWareHouse"), Telerik.Web.UI.RadComboBox).SelectedValue)
                    dtitem.Rows.Add(dr2)
                    'End If
                    i += 1
                Next
            Else
                btnOptionsNAccessories.Visible = False
            End If

            dsTemp.AcceptChanges()
            ViewState("SOItems") = dsTemp

            If ddltype.SelectedValue = "P" Then
                objItems = New CItems
                objItems.ItemCode = radCmbItem.SelectedValue
                ds = objItems.GetItemWareHouses()
                If ds.Tables(2).Rows.Count > 0 Then
                    btnConfigureAttribute.Attributes.Add("onclick", "javascript:window.open('frmProductAttribute.aspx?ItemCode=" & radCmbItem.SelectedValue & " ','','toolbar=no,titlebar=no,top=200,left=400,width=450,height=200,scrollbars=yes,resizable=yes'); return false;")
                    btnConfigureAttribute.Visible = True
                Else
                    btnConfigureAttribute.Visible = False
                End If

                If objCommon Is Nothing Then objCommon = New CCommon
                Dim dtWareHouse As DataTable = objCommon.GetWarehouseAttrBasedItem(radCmbItem.SelectedValue)

                Dim dr() As DataRow
                If ViewState("SelectedWarehouse") IsNot Nothing Then
                    dr = dtWareHouse.Select("numWareHouseID = " & ViewState("SelectedWarehouse").ToString())
                ElseIf CCommon.ToLong(Session("DefaultWarehouse")) > 0 Then
                    dr = dtWareHouse.Select("numWareHouseID = " & Session("DefaultWarehouse").ToString())
                End If
                radWareHouse.DataSource = dtWareHouse
                radWareHouse.DataBind()
                If (radWareHouse.Items.Count > 0) Then
                    'btnConfigureAttribute.Visible = True
                    If Not dr Is Nothing Then 'Persist Value of perviously selected warehouse if it exist 
                        If dr.Length > 0 Then
                            If Not radWareHouse.FindItemByValue(CType(dr(0), DataRow)("numWareHouseItemId").ToString()) Is Nothing Then radWareHouse.FindItemByValue(CType(dr(0), DataRow)("numWareHouseItemId").ToString()).Selected = True
                        End If
                    Else
                        radWareHouse.Items(0).Selected = True
                    End If
                    'Response.Write(radWareHouse.SelectedValue + ":" + radWareHouse.SelectedItem.Value + ":" + radWareHouse.Items(radWareHouse.SelectedIndex).Value)
                End If
                If _IsStockTransfer Then
                    radWarehouseTo.DataSource = dtWareHouse
                    radWarehouseTo.DataBind()
                    If (radWarehouseTo.Items.Count > 0) Then
                        If dr IsNot Nothing Then 'Persist Value of perviously selected warehouse if it exist 
                            If dr.Length > 0 Then
                                If Not radWarehouseTo.FindItemByValue(CType(dr(0), DataRow)("numWareHouseItemId").ToString()) Is Nothing Then radWareHouse.FindItemByValue(CType(dr(0), DataRow)("numWareHouseItemId").ToString()).Selected = True
                            End If
                        Else
                            radWarehouseTo.Items(0).Selected = True
                        End If
                    End If
                End If

            Else
                btnConfigureAttribute.Visible = False
            End If

            If ddltype.SelectedValue = "S" Then
                txtunits.Attributes.Add("onkeypress", "CheckNumber(1,event);TempHideButton()")
            Else
                txtunits.Attributes.Add("onkeypress", "CheckNumber(2,event);TempHideButton()")
            End If

            If (_PageType = PageType.AddOppertunity) Then
                If OppType = 2 Then
                    If CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue = "" Then
                        CType(Me.Parent.FindControl("pnlPurchase"), Panel).Visible = True
                        CType(Me.Parent.FindControl("btnSelectVendor"), Button).Attributes.Add("onclick", "return openSelVendor(" & radCmbItem.SelectedValue & ")")
                    End If
                End If
            End If

            LoadOrderInTransit()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub LoadOrderInTransit()
        Dim objOpportunity As New MOpportunity
        objOpportunity.ItemCode = radCmbItem.SelectedValue
        objOpportunity.DomainID = Session("DomainID")
        objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
        Dim dtTransit As DataTable = objOpportunity.GetItemTransit(1)

        If dtTransit.Rows(0)("Total") > 0 Then
            hplInTransit.Text = dtTransit.Rows(0)("Total")
            divTransit.Visible = True
        Else
            divTransit.Visible = False
        End If
    End Sub
    Sub LoadItemDetails()
        Try
            If radCmbItem.SelectedValue <> "" Then

                objCommon = New CCommon
                Dim dtUnit As DataTable
                objCommon.DomainID = Session("DomainID")
                If CCommon.ToLong(txtHidEditOppItem.Text) > 0 Then
                    objCommon.ItemCode = CCommon.ToLong(txtHidEditOppItem.Text)
                Else
                    objCommon.ItemCode = radCmbItem.SelectedValue
                End If
                objCommon.UOMAll = True
                dtUnit = objCommon.GetItemUOM()

                With ddUOM
                    .DataSource = dtUnit
                    .DataTextField = "vcUnitName"
                    .DataValueField = "numUOMId"
                    .DataBind()
                    .Items.Insert(0, "--Select One--")
                    .Items.FindByText("--Select One--").Value = "0"
                End With

                Dim dtTable, dtItemTax As DataTable
                If objItems Is Nothing Then objItems = New CItems

                Dim strUnit As String

                If CCommon.ToLong(txtHidEditOppItem.Text) > 0 And CCommon.ToLong(GetQueryStringVal("opid")) > 0 Then
                    Dim objOpportunity As New MOpportunity
                    objOpportunity.OppItemCode = CCommon.ToLong(txtHidEditOppItem.Text)
                    objOpportunity.OpportunityId = GetQueryStringVal("opid")
                    objOpportunity.DomainID = Session("DomainID")
                    objOpportunity.Mode = 2
                    Dim ds As DataSet = objOpportunity.GetOrderItems()
                    If ds.Tables(0).Rows.Count > 0 Then
                        dtTable = ds.Tables(0)
                        dtTable.Columns("monPrice").ColumnName = "monListPrice"
                        dtTable.Columns("vcPathForTImage").ColumnName = "vcPathForImage"
                        dtTable.Columns("vcItemDesc").ColumnName = "txtItemDesc"
                        dtTable.AcceptChanges()
                        strUnit = "numUOM"
                    Else
                        objItems.ItemCode = radCmbItem.SelectedValue
                        dtTable = objItems.ItemDetails

                        If (OppType = 2) Then
                            strUnit = "numPurchaseUnit"
                        ElseIf (OppType = 1) Then
                            strUnit = "numSaleUnit"
                        End If
                    End If
                Else
                    objItems.ItemCode = radCmbItem.SelectedValue
                    dtTable = objItems.ItemDetails

                    If (OppType = 2) Then
                        strUnit = "numPurchaseUnit"
                    ElseIf (OppType = 1) Then
                        strUnit = "numSaleUnit"
                    End If
                End If

                Dim strApplicable As String

                If (_PageType = PageType.Sales) Then
                    objItems.DomainID = Session("DomainID")
                    dtItemTax = objItems.ItemTax()

                    For Each dr As DataRow In dtItemTax.Rows
                        strApplicable = strApplicable & dr("bitApplicable") & ","
                    Next
                End If

                If (_PageType = PageType.AddOppertunity Or _PageType = PageType.AddEditOrder) Then
                    If OppType = 2 Then
                        BindBudgetValue(radCmbItem.SelectedValue)
                    End If
                End If

                If dtTable.Rows.Count > 0 Then

                    If (_PageType = PageType.Sales) Then
                        strApplicable = strApplicable & dtTable.Rows(0).Item("bitTaxable")
                        Taxable.Value = strApplicable
                    End If

                    If dtTable.Rows(0).Item("bitKitParent") = True And dtTable.Rows(0).Item("bitAssembly") = False Then
                        hdKit.Value = dtTable.Rows(0).Item("bitKitParent")
                    Else : hdKit.Value = ""
                    End If

                    If OppType = 1 And dtTable.Rows(0).Item("bitAssembly") = True Then
                        chkWorkOrder.Visible = True
                    Else
                        chkWorkOrder.Visible = False
                    End If

                    'lblUnits.Text = dtTable.Rows(0).Item("vcUnitofMeasure")
                    ddltype.SelectedItem.Selected = False
                    If ddltype.Items.FindByValue(dtTable.Rows(0).Item("charItemType")) IsNot Nothing Then
                        ddltype.Items.FindByValue(dtTable.Rows(0).Item("charItemType")).Selected = True
                    End If
                    txtdesc.Text = CCommon.ToString(dtTable.Rows(0).Item("txtItemDesc"))
                    txtModelID.Text = CCommon.ToString(dtTable.Rows(0).Item("vcModelID"))

                    'lblListPrice.Text = String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("monListPrice"))
                    'If Not IsDBNull(dtTable.Rows(0).Item("vcPathForImage")) Then
                    '    If dtTable.Rows(0).Item("vcPathForImage") <> "" Then
                    '        imgItem.Visible = True
                    '        hplImage.Visible = True
                    '        imgItem.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtTable.Rows(0).Item("vcPathForImage")
                    '        hplImage.Attributes.Add("onclick", "return OpenImage(" & radCmbItem.SelectedValue & ")")
                    '    Else
                    '        imgItem.Visible = False
                    '        hplImage.Visible = False
                    '    End If
                    'Else
                    '    imgItem.Visible = False
                    '    hplImage.Visible = False
                    'End If
                    If dtTable.Rows(0).Item("charItemType") = "P" Then
                        trWarehouse.Visible = True
                        If dtTable.Rows(0).Item("bitSerialized") = True Then
                            txtSerialize.Text = 1
                        Else : txtSerialize.Text = 0
                        End If
                        chkDropShip.Attributes.Add("style", "display:''")
                    Else
                        chkDropShip.Attributes.Add("style", "display:none")
                        If dtTable.Rows(0).Item("bitSerialized") = True Then
                            txtSerialize.Text = 1
                        Else : txtSerialize.Text = 0
                        End If
                        txtHidValue.Text = ""
                        trWarehouse.Visible = False
                        radWareHouse.SelectedValue = ""
                    End If


                    'If CCommon.ToLong(txtHidEditOppItem.Text) > 0 And CCommon.ToLong(GetQueryStringVal( "opid")) > 0 Then
                    '    strUnit = "numUOM"
                    'Else
                    '    If (OppType = 2) Then
                    '        strUnit = "numPurchaseUnit"
                    '    ElseIf (OppType = 1) Then
                    '        strUnit = "numSaleUnit"
                    '    End If
                    'End If
                    If strUnit.Length > 0 Then
                        If ddUOM.Items.FindByValue(dtTable.Rows(0).Item(strUnit)) IsNot Nothing Then
                            ddUOM.ClearSelection()
                            ddUOM.Items.FindByValue(dtTable.Rows(0).Item(strUnit)).Selected = True
                        End If
                    End If

                End If

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Sub BindBudgetValue(ByVal itemcode)
        Dim lobjBudget As New Budget
        lobjBudget.ItemCode = itemcode
        lobjBudget.DomainID = Session("DomainId")
        lobjBudget.GetBudgetForPurchaseOpportunity()
        If lobjBudget.bitPurchaseOpp = True Then
            lblBudget.Visible = True
            lblBudgetMonth.Visible = True
            lblBudgetYear.Visible = True
            lblBudgetMonth.Text = ReturnMoney(lobjBudget.MonthlyAmt) & "(mo)"
            lblBudgetYear.Text = ReturnMoney(lobjBudget.TotalYearlyAmt) & "(yr)"
        Else
            lblBudget.Visible = False
            lblBudgetMonth.Visible = False
            lblBudgetYear.Visible = False
        End If
    End Sub


    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function IsDuplicate(ByVal ItemCode As String, ByVal WareHouseItemID As String, ByVal DropShip As Boolean) As Boolean
        'validate duplicate entry on refresh / when js is disabled
        Dim dRows As DataRow() = dsTemp.Tables(0).Select(" numItemCode='" & ItemCode & "'" & IIf(WareHouseItemID <> "", " and numWarehouseItmsID = '" & WareHouseItemID & "'", "") & " and DropShip ='" & DropShip & "'")

        If (dRows.Length > 0) Then
            'UpdateDetails()
            txtHidEditOppItem.Text = dRows(0)("numoppitemtCode")
            txtunits.Text = CCommon.ToInteger(dRows(0)("numUnitHour")) + CCommon.ToInteger(txtunits.Text)
            btnAdd.Text = "Update"
            btnEditCancel.Visible = True
            'Return True
        End If
        Return False
    End Function

    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            AddNewItem()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            objCommon = Nothing
            Response.Write(ex)
        End Try
    End Sub

    Sub AddNewItem()
        Try
            dsTemp = ViewState("SOItems")
            objCommon = New CCommon
            Dim objItem As New CItems
            If Not objItem.ValidateItemAccount(radCmbItem.SelectedValue, Session("DomainID"), OppType) = True Then
                'Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "AccountValidation", "alert('Please Set Income,Asset,COGs(Expense) Account for selected Item from Administration->Inventory->Item Details')", True)
                ScriptManager.RegisterStartupScript(Me.updatepanel, Me.updatepanel.GetType(), "AccountValidation", "alert('Please Set Income,Asset,COGs(Expense) Account for selected Item from Administration->Inventory->Item Details.');", True)
                Return
            End If

            If btnAdd.Text = "Add" AndAlso ddltype.SelectedValue = "P" Then If IsDuplicate(radCmbItem.SelectedValue, radWareHouse.SelectedValue, chkDropShip.Checked) Then Return

            If chkWorkOrder.Checked Then
                If ChartOfAccounting.GetDefaultAccount("WP", Session("DomainID")) = 0 Then
                    'Page.ClientScript.RegisterStartupScript(Me.GetType(), "Validation", "alert('Can not save record, your option is to set default Work in Progress Account from Administration->Domain Details->Accounting tab->Default Accounts')", True)
                    ScriptManager.RegisterStartupScript(Me.updatepanel, Me.updatepanel.GetType(), "Validation", "alert('Can not save record, your option is to set default Work in Progress Account from Administration->Global Settings->Accounting tab->Default Accounts.');", True)
                    Exit Sub
                End If
            End If

            If _IsStockTransfer Then
                If CCommon.ToLong(radWareHouse.SelectedValue) = CCommon.ToLong(radWarehouseTo.SelectedValue) Then
                    'Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "StockTransfer", "alert('Transfer from and to warehouse can not be same.')", True)
                    ScriptManager.RegisterStartupScript(Me.updatepanel, Me.updatepanel.GetType(), "StockTransfer", "alert('Transfer from and to warehouse can not be same.');", True)
                    Return
                End If
            End If

            objCommon = New CCommon

            Dim chkTaxItems As CheckBoxList

            If (_PageType = PageType.Sales) Then
                chkTaxItems = CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList)
            End If

            ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon, dsTemp, IIf(btnAdd.Text = "Add", True, False), ddltype.SelectedValue, ddltype.SelectedItem.Text,
                                                                               chkDropShip.Checked, hdKit.Value, radCmbItem.SelectedValue, Math.Abs(CCommon.ToDecimal(txtunits.Text)),
                                                                               Math.Abs(CCommon.ToDecimal(txtprice.Text)), txtdesc.Text, CCommon.ToLong(radWareHouse.SelectedValue),
                                                                               radCmbItem.Text, radWareHouse.SelectedItem.Text, IIf(btnAdd.Text = "Add", 0, CCommon.ToLong(txtHidEditOppItem.Text)), txtModelID.Text.Trim, ddUOM.SelectedValue, ddUOM.SelectedItem.Text.Replace("--Select One--", ""), hdvUOMConversionFactor.Value, _PageType.ToString(),
                                                                               IIf(radPer.Checked, True, False), IIf(IsNumeric(txtItemDiscount.Text), txtItemDiscount.Text, 0),
                                                                               Taxable.Value, txtTax.Text, "", chkTaxItems, IIf(chkWorkOrder.Checked, 1, 0), txtInstruction.Text, calCompliationDate.SelectedDate, ddlWOAssignTo.SelectedValue, CCommon.ToLong(ddlVendorWarehouse.SelectedValue), CCommon.ToLong(ddlVendorShipmentMethod.SelectedValue.Split("~")(0)), CCommon.ToLong(hdnVendorId.Value),
                                                                               CCommon.ToLong(GetQueryStringVal("Source")), CCommon.ToLong(GetQueryStringVal("StageID")), ToWarehouseItemID:=CCommon.ToLong(radWarehouseTo.SelectedValue), IsPOS:=True)

            'BindGrid()

            UpdateDetails()
            radCmbItem.Items.Clear()
            radCmbItem.SelectedValue = ""
            txtItemDiscount.Text = ""
            radCmbItem.Text = ""
            chkDropShip.Checked = False

            chkWorkOrder.Checked = False
            chkWorkOrder.Visible = False
            trWorkOrder.Visible = False

            hdvUOMConversionFactor.Value = ""

            btnConfigureAttribute.Visible = False
            btnOptionsNAccessories.Visible = False
            'lblListPrice.Text = ""
            txtunits.Text = "1"
            txtprice.Text = ""
            radCmbItem.Items.Clear()
            txtdesc.Text = ""
            btnEditCancel.Visible = False
            ddltype.SelectedIndex = 0
            txtModelID.Text = ""

            lblBudget.Visible = False
            lblBudgetMonth.Visible = False
            lblBudgetYear.Visible = False
            chkDropShip.Enabled = True

            txtHidEditOppItem.Text = ""
            btnAdd.Text = "Add"

            ddlVendorWarehouse.SelectedIndex = 0
            ddlVendorShipmentMethod.SelectedIndex = 0
            lblExpectedDelivery.Text = ""

            hdnUnitCost.Value = "0"
            hdnVendorId.Value = "0"

            hplRelatedItems.Visible = False
            divTransit.Visible = False
            tdSalesProfit.Visible = False



            'tblKits.Visible = False
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "DoubleClickPrevent", "IsAddClicked = false;", True) 'added to avoid multiple click of add button before completing postback- by chintan
            objCommon = Nothing
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub BindGrid()
        dgItems.DataSource = dsTemp.Tables(0)
        dgItems.DataBind()
    End Sub
    Sub createSet()
        Try
            dsTemp = New DataSet
            Dim dtItem As New DataTable
            Dim dtSerItem As New DataTable
            Dim dtChildItems As New DataTable
            dtItem.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
            dtItem.Columns.Add("numItemCode")
            dtItem.Columns.Add("numUnitHour")

            dtItem.Columns.Add("monPrice")
            dtItem.Columns.Add("numUOM")
            dtItem.Columns.Add("vcUOMName")
            dtItem.Columns.Add("UOMConversionFactor")

            dtItem.Columns.Add("monTotAmount", GetType(Decimal))
            dtItem.Columns.Add("numSourceID")
            dtItem.Columns.Add("vcItemDesc")
            dtItem.Columns.Add("vcModelID")
            dtItem.Columns.Add("numWarehouseID")
            dtItem.Columns.Add("vcItemName")
            dtItem.Columns.Add("Warehouse")
            dtItem.Columns.Add("numWarehouseItmsID")
            dtItem.Columns.Add("ItemType")
            dtItem.Columns.Add("Attributes")
            dtItem.Columns.Add("Op_Flag")
            dtItem.Columns.Add("bitWorkOrder")
            dtItem.Columns.Add("vcInstruction")
            dtItem.Columns.Add("bintCompliationDate")
            dtItem.Columns.Add("numWOAssignedTo")

            dtItem.Columns.Add("DropShip", GetType(Boolean))
            dtItem.Columns.Add("bitDiscountType", GetType(Boolean))
            dtItem.Columns.Add("fltDiscount", GetType(Decimal))
            dtItem.Columns.Add("monTotAmtBefDiscount", GetType(Decimal))

            dtItem.Columns.Add("bitTaxable0")
            dtItem.Columns.Add("Tax0", GetType(Decimal))

            dtItem.Columns.Add("numVendorWareHouse")
            dtItem.Columns.Add("numShipmentMethod")
            dtItem.Columns.Add("numSOVendorId")

            dtItem.Columns.Add("numProjectID")
            dtItem.Columns.Add("numProjectStageID")
            dtItem.Columns.Add("charItemType")
            dtItem.Columns.Add("numToWarehouseItemID") 'added by chintan for stock transfer
            dtItem.Columns.Add("bitIsAuthBizDoc", GetType(Boolean))
            dtItem.Columns.Add("numUnitHourReceived")
            dtItem.Columns.Add("numQtyShipped")
            dtItem.Columns.Add("vcBaseUOMName")
            dtItem.Columns.Add("numVendorID", System.Type.GetType("System.Int32"))
            dtItem.Columns.Add("numCost", GetType(Decimal))
            dtItem.Columns.Add("vcVendorNotes")

            If (_PageType = PageType.Sales) Then
                '''Loading Other Taxes
                Dim dtTaxTypes As DataTable
                Dim ObjTaxItems As New TaxDetails
                ObjTaxItems.DomainID = Session("DomainID")
                dtTaxTypes = ObjTaxItems.GetTaxItems

                For Each drTax As DataRow In dtTaxTypes.Rows
                    dtItem.Columns.Add("Tax" & drTax("numTaxItemID"), GetType(Decimal))
                    dtItem.Columns.Add("bitTaxable" & drTax("numTaxItemID"))
                Next

                Dim dr As DataRow
                dr = dtTaxTypes.NewRow
                dr("numTaxItemID") = 0
                dr("vcTaxName") = "Sales Tax(Default)"
                dtTaxTypes.Rows.Add(dr)

                Dim chkTaxItems As CheckBoxList = CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList)

                chkTaxItems.DataTextField = "vcTaxName"
                chkTaxItems.DataValueField = "numTaxItemID"
                chkTaxItems.DataSource = dtTaxTypes
                chkTaxItems.DataBind()
            End If

            dtSerItem.Columns.Add("numWarehouseItmsDTLID")
            dtSerItem.Columns.Add("numWItmsID")
            dtSerItem.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
            dtSerItem.Columns.Add("vcSerialNo")
            dtSerItem.Columns.Add("Comments")
            dtSerItem.Columns.Add("Attributes")

            dtChildItems.Columns.Add("numOppChildItemID", System.Type.GetType("System.Int32"))
            dtChildItems.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
            dtChildItems.Columns.Add("numItemCode")
            dtChildItems.Columns.Add("numQtyItemsReq")
            dtChildItems.Columns.Add("vcItemName")
            dtChildItems.Columns.Add("monListPrice")
            dtChildItems.Columns.Add("UnitPrice")
            dtChildItems.Columns.Add("charItemType")
            dtChildItems.Columns.Add("txtItemDesc")
            dtChildItems.Columns.Add("numWarehouseItmsID")
            dtChildItems.Columns.Add("Op_Flag")
            dtChildItems.Columns.Add("ItemType")
            dtChildItems.Columns.Add("Attr")
            dtChildItems.Columns.Add("vcWareHouse")

            dtItem.TableName = "Item"
            dtSerItem.TableName = "SerialNo"
            dtChildItems.TableName = "ChildItems"

            dsTemp.Tables.Add(dtItem)
            dsTemp.Tables.Add(dtSerItem)
            dsTemp.Tables.Add(dtChildItems)
            dtItem.PrimaryKey = New DataColumn() {dsTemp.Tables(0).Columns("numoppitemtCode")}
            dtChildItems.PrimaryKey = New DataColumn() {dsTemp.Tables(2).Columns("numOppChildItemID")}
            ViewState("SOItems") = dsTemp
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            save()

            If (_PageType = PageType.Purchase) Then
                If CCommon.ToLong(GetQueryStringVal("StageID")) > 0 Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Reload", "window.opener.location.reload(true);Close();self.close();", True)
                    Exit Sub
                ElseIf GetQueryStringVal("Source") <> "" Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Open", "OpenOpp(" & lngOppId.ToString & ")", True)
                    Exit Sub
                End If
                If boolFlag Then
                    If OppBizDocID > 0 Then
                        Response.Redirect("../opportunity/frmBizInvoice.aspx?OpID=" & lngOppId & "&OppBizId=" & OppBizDocID)
                    ElseIf CCommon.ToLong(GetQueryStringVal("StageID")) > 0 Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Reload", "window.opener.location.reload(true);Close();self.close();", True)
                    Else
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Open", "window.opener.reDirectPage('../opportunity/frmOpportunities.aspx?opId=" & lngOppId.ToString() & "'); self.close();", True)
                    End If
                End If
            ElseIf (_PageType = PageType.Sales) Then
                Dim objRule As New OrderAutoRules
                objRule.GenerateAutoPO(OppBizDocID)
                If boolFlag Then
                    'Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "OpenAmtPaid", "OpenAmtPaid(" & OppBizDocID.ToString & "," & lngOppId.ToString & ");", True)
                    CType(Me.Parent.FindControl("ifrmPayAmount"), HtmlGenericControl).Attributes.Add("src", "../opportunity/frmAmtPaid.aspx?swipe=1&DivID=" & lngDivId.ToString() & "&OppBizId=" & OppBizDocID.ToString & "&OppId=" & lngOppId.ToString)
                    CType(Me.Parent.FindControl("btnSection2Next"), HtmlGenericControl).Visible = True
                    CType(Me.Parent.FindControl("tdSection3"), TableCell).Visible = True
                    CType(Me.Parent.FindControl("hdnCurrentSection"), HiddenField).Value = "dvsection3"

                    ManageGlobalButton(lngOppId, OppBizDocID)
                End If
            End If
            UpdateDetails()
        Catch ex As Exception
            If ex.Message = "FY_CLOSED" Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Sub ManageGlobalButton(lngOppId As Long, OppBizDocID As Long)
        Try
            btnSave.Visible = False
            btnSaveOpenOrder.Visible = False
            btnSaveNew.Visible = False

            CType(Me.Parent.FindControl("btnOpenInvoice"), Button).Attributes.Add("onclick", "return OpenBizInvoice(" & lngOppId.ToString & "," & OppBizDocID.ToString & ",'0')")
            CType(Me.Parent.FindControl("btnOpenInvoice"), Button).Text = btnSave.Text.Replace("Save & Create", "Open")
            CType(Me.Parent.FindControl("btnOpenInvoice"), Button).Visible = True

            CType(Me.Parent.FindControl("btnPrintInvoice"), Button).Attributes.Add("onclick", "return OpenBizInvoice(" & lngOppId.ToString & "," & OppBizDocID.ToString & ",'1')")
            CType(Me.Parent.FindControl("btnPrintInvoice"), Button).Text = btnSave.Text.Replace("Save & Create", "Print")
            CType(Me.Parent.FindControl("btnPrintInvoice"), Button).Visible = True

            CType(Me.Parent.FindControl("btnEmailInvoice"), Button).Text = btnSave.Text.Replace("Save & Create", "Email")
            CType(Me.Parent.FindControl("btnEmailInvoice"), Button).Visible = True

            CType(Me.Parent.FindControl("btnOpenOrderDetails"), Button).Attributes.Add("onclick", "return OpenOrderDetails(" & lngOppId.ToString & "," & OppType & ")")
            CType(Me.Parent.FindControl("btnOpenOrderDetails"), Button).Visible = True

            CType(Me.Parent.FindControl("hdnOppID"), HiddenField).Value = lngOppId.ToString
            CType(Me.Parent.FindControl("hdnOppBizDocID"), HiddenField).Value = OppBizDocID.ToString
        Catch ex As Exception
            Throw (ex)
        End Try
    End Sub

    Sub save()
        Try
            If (_PageType = PageType.AddEditOrder) Then
                SaveOpportunity()
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Reload", "window.opener.location.reload(true);Close();self.close();", True)
            Else
                If (_PageType = PageType.Purchase) Then
                    If GetQueryStringVal("Source") IsNot Nothing AndAlso GetQueryStringVal("StageID") IsNot Nothing Then

                        Dim dtItem As DataTable

                        dtItem = dsTemp.Tables(0)

                        If dtItem.Rows.Count > 0 Then
                            Dim TotalAmount As Decimal
                            TotalAmount = CType(dsTemp.Tables(0).Compute("Sum(monTotAmount)", ""), Decimal)

                            Dim objTimeExp As New TimeExpenseLeave
                            Dim dtTimeDetails As DataTable

                            objTimeExp.DomainID = Session("DomainID")
                            objTimeExp.ProID = CCommon.ToLong(GetQueryStringVal("Source"))
                            objTimeExp.StageId = CCommon.ToLong(GetQueryStringVal("StageID"))

                            dtTimeDetails = objTimeExp.GetTimeAndExpense_BudgetTotal()

                            If dtTimeDetails.Rows.Count <> 0 Then
                                If dtTimeDetails.Rows(0).Item("bitExpenseBudget") = True Then
                                    If (TotalAmount > dtTimeDetails.Rows(0).Item("TotalExpense")) Then
                                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('You’ve exceeded the amount budgeted for this stage.' );", True)
                                        boolFlag = False
                                        Exit Sub
                                    End If
                                    'Else
                                    '    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please set Expense Budget for this stage.' );", True)
                                    '    boolFlag = False
                                    '    Exit Sub
                                End If
                            End If
                        End If
                    End If
                End If

                If OppType = 2 Then
                    If _PageType = PageType.Purchase Or (_PageType = PageType.AddOppertunity And chkCreatePurchaseOrder.Checked) Then
                        'Accounting validation->Do not allow to save PO untill Default COGs account is mapped 
                        Dim objJournal As New JournalEntry
                        If ChartOfAccounting.GetDefaultAccount("CG", Session("DomainID")) = 0 Then
                            'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default COGs account from Administration->Domain Details->Accounting->Default Accounts Mapping To Save' );", True)
                            ScriptManager.RegisterStartupScript(Me.updatepanel, Me.updatepanel.GetType(), "Alert", "alert('Please Set Default COGs account from Administration->Global Settings->Accounting->Default Accounts Mapping To Save' );", True)
                            boolFlag = False
                            Exit Sub
                        End If
                    End If

                    If ChartOfAccounting.GetDefaultAccount("PC", Session("DomainID")) = 0 Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Purchase Clearing account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                        boolFlag = False
                        Exit Sub
                    End If

                    If ChartOfAccounting.GetDefaultAccount("PV", Session("DomainID")) = 0 Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Purchase Price Variance account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                        boolFlag = False
                        Exit Sub
                    End If
                End If

                If (_PageType = PageType.Purchase) Or (_PageType = PageType.Sales) Then
                    If CType(Me.Parent.FindControl("radNewCustomer"), RadioButton).Checked = True Then
                        InsertProspect()
                    Else
                        lngDivId = CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue
                        lngCntID = CType(Me.Parent.FindControl("ddlContact"), DropDownList).SelectedValue
                    End If
                Else
                    lngDivId = CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue
                    lngCntID = CType(Me.Parent.FindControl("ddlContact"), DropDownList).SelectedValue
                End If

                Opportunity()
            End If
        Catch ex As Exception
            boolFlag = False
            Throw ex
        End Try
    End Sub

    Sub SaveOpportunity()
        Try
            Dim arrOutPut() As String
            Dim objOpportunity As New OppotunitiesIP
            objOpportunity.OpportunityId = GetQueryStringVal("opid")
            objOpportunity.DomainID = Session("DomainID")
            objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            objOpportunity.OpportunityDetails()

            objOpportunity.OpportunityId = GetQueryStringVal("opid")
            objOpportunity.PublicFlag = 0
            objOpportunity.UserCntID = Session("UserContactID")
            objOpportunity.DomainID = Session("DomainId")
            objOpportunity.OppType = OppType
            objOpportunity.SourceType = 1
            dsTemp.Tables(2).Rows.Clear()
            dsTemp.AcceptChanges()

            If Not ViewState("SOItems") Is Nothing Then
                dsTemp = ViewState("SOItems")
                objOpportunity.strItems = "<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & dsTemp.GetXml
            End If
            objOpportunity.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            arrOutPut = objOpportunity.Save()
            objOpportunity.OpportunityId = arrOutPut(0)

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSaveNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNew.Click
        Try
            IsFromSaveAndNew = True
            'If ProcessPayment() Then
            save()
            Dim objRule As New OrderAutoRules
            objRule.GenerateAutoPO(OppBizDocID)
            If boolFlag Then
                Response.Redirect("../opportunity/frmNewSalesOrderPOS.aspx?I=1")
            End If
            'Else
            'Exit Sub
            'End If
        Catch ex As Exception
            If ex.Message = "FY_CLOSED" Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Private Function InsertProspect() As Boolean
        Try

            Dim objLeads As New CLeads
            With objLeads
                Dim txtCompany As TextBox = CType(Me.Parent.FindControl("txtCompany"), TextBox)
                Dim txtFirstName As TextBox = CType(Me.Parent.FindControl("txtFirstName"), TextBox)
                Dim txtLastName As TextBox = CType(Me.Parent.FindControl("txtLastName"), TextBox)

                Dim txtPhone As TextBox = CType(Me.Parent.FindControl("txtPhone"), TextBox)
                Dim txtExt As TextBox = CType(Me.Parent.FindControl("txtExt"), TextBox)
                Dim txtEmail As TextBox = CType(Me.Parent.FindControl("txtEmail"), TextBox)

                If Len(txtCompany.Text) < 1 Then txtCompany.Text = txtLastName.Text & "," & txtFirstName.Text
                .CompanyName = txtCompany.Text.Trim
                CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).Text = txtCompany.Text
                .CustName = txtCompany.Text.Trim
                .CRMType = 1
                .DivisionName = "-"
                .LeadBoxFlg = 1
                .UserCntID = Session("UserContactId")
                .Country = Session("DefCountry")
                .SCountry = Session("DefCountry")
                .FirstName = txtFirstName.Text
                .LastName = txtLastName.Text
                .ContactPhone = txtPhone.Text
                .PhoneExt = txtExt.Text
                .Email = txtEmail.Text
                .DomainID = Session("DomainID")
                .ContactType = 0 'commented by chintan, contact type 70 is obsolete. default contact type is 0
                .PrimaryContact = True

                .UpdateDefaultTax = False
                If CType(Me.Parent.FindControl("ddlRelationship"), DropDownList).SelectedIndex > 0 Then .CompanyType = CType(Me.Parent.FindControl("ddlRelationship"), DropDownList).SelectedValue
                If CType(Me.Parent.FindControl("ddlProfile"), DropDownList).SelectedIndex > 0 Then .Profile = CType(Me.Parent.FindControl("ddlProfile"), DropDownList).SelectedValue

                If (_PageType = PageType.Sales) Then
                    .NoTax = IIf(CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList).Items.FindByValue(0).Selected = True, False, True)
                End If
            End With
            objLeads.CompanyID = objLeads.CreateRecordCompanyInfo
            lngDivId = objLeads.CreateRecordDivisionsInfo
            objLeads.DivisionID = lngDivId
            lngCntID = objLeads.CreateRecordAddContactInfo()

            If (_PageType = PageType.Sales) Then
                SaveTaxTypes()
            End If

            Dim txtStreet As TextBox = CType(Me.Parent.FindControl("txtStreet"), TextBox)
            Dim txtCity As TextBox = CType(Me.Parent.FindControl("txtCity"), TextBox)
            Dim txtPostal As TextBox = CType(Me.Parent.FindControl("txtPostal"), TextBox)
            Dim txtShipStreet As TextBox = CType(Me.Parent.FindControl("txtShipStreet"), TextBox)
            Dim txtShipPostal As TextBox = CType(Me.Parent.FindControl("txtShipPostal"), TextBox)
            Dim txtShipCity As TextBox = CType(Me.Parent.FindControl("txtShipCity"), TextBox)

            Dim ddlBillCountry As DropDownList = CType(Me.Parent.FindControl("ddlBillCountry"), DropDownList)
            Dim ddlBillState As DropDownList = CType(Me.Parent.FindControl("ddlBillState"), DropDownList)
            Dim ddlShipState As DropDownList = CType(Me.Parent.FindControl("ddlShipState"), DropDownList)
            Dim ddlShipCountry As DropDownList = CType(Me.Parent.FindControl("ddlShipCountry"), DropDownList)

            Dim chkShipDifferent As CheckBox = CType(Me.Parent.FindControl("chkShipDifferent"), CheckBox)

            Dim objContacts As New CContacts
            objContacts.BillStreet = txtStreet.Text
            objContacts.BillCity = txtCity.Text
            objContacts.BillCountry = ddlBillCountry.SelectedItem.Value
            objContacts.BillPostal = txtPostal.Text
            objContacts.BillState = ddlBillState.SelectedItem.Value
            objContacts.BillingAddress = IIf(chkShipDifferent.Checked = True, 0, 1)
            objContacts.ShipStreet = IIf(chkShipDifferent.Checked = True, txtShipStreet.Text, txtStreet.Text)
            objContacts.ShipState = IIf(chkShipDifferent.Checked = True, ddlShipState.SelectedItem.Value, ddlBillState.SelectedItem.Value)
            objContacts.ShipPostal = IIf(chkShipDifferent.Checked = True, txtShipPostal.Text, txtPostal.Text)
            objContacts.ShipCountry = IIf(chkShipDifferent.Checked = True, ddlShipCountry.SelectedItem.Value, ddlBillCountry.SelectedItem.Value)
            objContacts.ShipCity = IIf(chkShipDifferent.Checked = True, txtShipCity.Text, txtCity.Text)
            objContacts.DivisionID = lngDivId
            objContacts.UpdateCompanyAddress()
        Catch ex As Exception
            boolFlag = False
            Throw ex
        End Try
    End Function


    Sub SaveTaxTypes()
        Try
            Dim dtCompanyTaxTypes As New DataTable
            dtCompanyTaxTypes.Columns.Add("numTaxItemID")
            dtCompanyTaxTypes.Columns.Add("bitApplicable")
            Dim dr As DataRow
            Dim i As Integer
            For i = 0 To CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList).Items.Count - 1
                If CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList).Items(i).Selected = True Then
                    dr = dtCompanyTaxTypes.NewRow
                    dr("numTaxItemID") = CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList).Items(i).Value
                    dr("bitApplicable") = 1
                    dtCompanyTaxTypes.Rows.Add(dr)
                End If
            Next
            Dim ds As New DataSet
            Dim strdetails As String
            dtCompanyTaxTypes.TableName = "Table"
            ds.Tables.Add(dtCompanyTaxTypes)
            strdetails = ds.GetXml
            ds.Tables.Remove(ds.Tables(0))
            Dim objProspects As New CProspects
            objProspects.DivisionID = lngDivId
            objProspects.strCompanyTaxTypes = strdetails
            objProspects.ManageCompanyTaxTypes()
            ds.Dispose()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub Opportunity()
        Try
            Dim arrOutPut() As String
            Dim objOpportunity As New MOpportunity
            objOpportunity.OpportunityId = 0
            objOpportunity.ContactID = lngCntID
            objOpportunity.DivisionID = lngDivId

            If (_PageType = PageType.Purchase) Then
                objOpportunity.OpportunityName = CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).Text & "-PO-" & Format(Now(), "MMMM")
                objOpportunity.IsStockTransfer = _IsStockTransfer
            ElseIf (_PageType = PageType.Sales) Then
                objOpportunity.OpportunityName = CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).Text & "-SO-" & Format(Now(), "MMMM")
            ElseIf (_PageType = PageType.AddOppertunity) Then
                objOpportunity.OpportunityName = CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).Text & "-" & Format(Now(), "MMMM")
            End If

            objOpportunity.UserCntID = Session("UserContactID")
            objOpportunity.EstimatedCloseDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
            objOpportunity.PublicFlag = 0
            objOpportunity.DomainID = Session("DomainId")

            'when address value is >0 then it will Copy addressid's address to Opportunity address
            objOpportunity.BillToAddressID = CCommon.ToLong(hdnBillAddressID.Value)
            objOpportunity.ShipToAddressID = CCommon.ToLong(hdnShipAddressID.Value)

            If Session("MultiCurrency") = True Then
                objOpportunity.CurrencyID = CType(Me.Parent.FindControl("ddlCurrency"), DropDownList).SelectedValue
            Else
                objOpportunity.CurrencyID = Session("BaseCurrencyID")
            End If

            If (_PageType = PageType.Purchase) Or (_PageType = PageType.Sales) Then
                If CType(Me.Parent.FindControl("radNewCustomer"), RadioButton).Checked = True Then
                    If CType(Me.Parent.FindControl("ddlNewAssignTo"), DropDownList).SelectedIndex >= 0 Then objOpportunity.AssignedTo = CType(Me.Parent.FindControl("ddlNewAssignTo"), DropDownList).SelectedValue
                Else
                    If CType(Me.Parent.FindControl("ddlAssignTo"), DropDownList).SelectedIndex >= 0 Then objOpportunity.AssignedTo = CType(Me.Parent.FindControl("ddlAssignTo"), DropDownList).SelectedValue
                    'If ddlCampaign.SelectedIndex > 0 Then objOpportunity.CampaignID = ddlCampaign.SelectedValue
                End If
            End If

            objOpportunity.OppType = OppType

            If (_PageType = PageType.Purchase Or _PageType = PageType.Sales) Then
                objOpportunity.DealStatus = objOpportunity.EnmDealStatus.DealWon
                objOpportunity.SourceType = 1
            ElseIf (_PageType = PageType.AddOppertunity) Then
                objOpportunity.Source = CType(Me.Parent.FindControl("ddlSource"), DropDownList).SelectedItem.Value
                objOpportunity.SourceType = 1

                If chkCreatePurchaseOrder.Checked And OppType = 2 Then
                    objOpportunity.DealStatus = objOpportunity.EnmDealStatus.DealWon
                End If
            End If

            dsTemp.Tables(2).Rows.Clear()
            dsTemp.AcceptChanges()
            Dim TotalAmount As Decimal = 0
            If Not ViewState("SOItems") Is Nothing Then
                dsTemp = ViewState("SOItems")
                If dsTemp.Tables(0).Rows.Count > 0 Then
                    TotalAmount = CType(dsTemp.Tables(0).Compute("Sum(monTotAmount)", ""), Decimal)
                    objOpportunity.strItems = "<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & dsTemp.GetXml
                End If
            End If

            Dim objAdmin As New CAdmin
            Dim dtBillingTerms As DataTable
            objAdmin.DivisionID = lngDivId
            dtBillingTerms = objAdmin.GetBillingTerms()

            objOpportunity.boolBillingTerms = IIf(dtBillingTerms.Rows(0).Item("tintBillingTerms") = 1, True, False)
            objOpportunity.BillingDays = dtBillingTerms.Rows(0).Item("numBillingDays")
            objOpportunity.boolInterestType = IIf(dtBillingTerms.Rows(0).Item("tintInterestType") = 1, True, False)
            objOpportunity.Interest = dtBillingTerms.Rows(0).Item("fltInterest")
            objOpportunity.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            arrOutPut = objOpportunity.Save
            lngOppId = arrOutPut(0)


            '''AddingBizDocs
            Dim objOppBizDocs As New OppBizDocs

            If (_PageType = PageType.Purchase) Then

                'Set Bill To and Ship To based on domain details settings
                If objOpportunity.BillToAddressID = 0 And objOpportunity.ShipToAddressID = 0 Then
                    objOpportunity.OpportunityId = lngOppId
                    objOpportunity.BillToType = IIf(Session("DefaultBillToForPO") = 1, 0, 0)
                    objOpportunity.ShipToType = IIf(Session("DefaultShipToForPO") = 1, 0, 0)
                    objOpportunity.UpdateOpportunityLinkingDtls()
                End If

                'IMPORTANT - CREATE BIZDOC METHOD IS COMMENTED BECAUSE USER CAN NOW USE WORKFLOW WORKLFLOW AUTOMATION AUTOMATION RULES
                'CreateOppBizDoc(objOppBizDocs, False)

                'Link PO with Project if Source Exist
                'If objOpportunity.Source > 0 Then
                If GetQueryStringVal("Source") <> "" AndAlso GetQueryStringVal("StageID") <> "" Then
                    objOpportunity.OpportunityId = lngOppId
                    objOpportunity.ProjectID = CCommon.ToLong(GetQueryStringVal("Source"))
                    objOpportunity.ManageOppLinking()
                    'Link BizDoc with Project
                    Dim objProject As New Project
                    objProject.ProjectID = CCommon.ToLong(GetQueryStringVal("Source"))
                    objProject.OpportunityId = lngOppId
                    'objProject.OppBizDocID = OppBizDocID
                    objProject.DomainID = Session("DomainID")
                    objProject.StageID = CCommon.ToLong(GetQueryStringVal("StageID"))
                    objProject.SaveProjectOpportunities()
                End If

                'stock transfer,update order name 
                If _IsStockTransfer Then
                    If objCommon Is Nothing Then objCommon = New CCommon()
                    objCommon.Mode = 19
                    objCommon.UpdateRecordID = lngOppId
                    objCommon.Comments = "Stock Transfer-" & lngOppId.ToString()
                    objCommon.UpdateSingleFieldValue()

                    'Create Journal entries.
                End If
            ElseIf (_PageType = PageType.AddOppertunity) Then
                Try
                    Dim objAlerts As New CAlerts
                    If OppType <> 1 Then
                        'When a Purchase Opportunity is created by a “non” employee, & when a Purchase Order is created by anyone.
                        Dim dtDetails As DataTable
                        objAlerts.AlertDTLID = 22 'Alert DTL ID for sending alerts in opportunities
                        objAlerts.DomainID = Session("DomainID")
                        dtDetails = objAlerts.GetIndAlertDTL
                        If dtDetails.Rows.Count > 0 Then
                            If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
                                Dim dtEmailTemplate As DataTable
                                Dim objDocuments As New DocumentList
                                objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
                                objDocuments.DomainID = Session("DomainID")
                                dtEmailTemplate = objDocuments.GetDocByGenDocID
                                If dtEmailTemplate.Rows.Count > 0 Then
                                    Dim objSendMail As New Email
                                    Dim dtMergeFields As New DataTable
                                    Dim drNew As DataRow
                                    dtMergeFields.Columns.Add("OppID")
                                    dtMergeFields.Columns.Add("DealAmount")
                                    dtMergeFields.Columns.Add("Organization")
                                    dtMergeFields.Columns.Add("ContactName")
                                    drNew = dtMergeFields.NewRow
                                    drNew("OppID") = arrOutPut(1)
                                    drNew("DealAmount") = TotalAmount
                                    drNew("ContactName") = CType(Me.Parent.FindControl("ddlContacts"), DropDownList).SelectedItem.Text
                                    drNew("Organization") = CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).Text
                                    dtMergeFields.Rows.Add(drNew)

                                    Dim dtEmail As DataTable
                                    objAlerts.AlertDTLID = 22
                                    objAlerts.DomainID = Session("DomainId")
                                    dtEmail = objAlerts.GetAlertEmails
                                    Dim strCC As String = ""
                                    Dim p As Integer
                                    For p = 0 To dtEmail.Rows.Count - 1
                                        strCC = strCC & dtEmail.Rows(p).Item("vcEmailID") & ";"
                                    Next
                                    strCC = strCC.TrimEnd(";")
                                    objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), "", Session("UserEmail"), strCC, dtMergeFields)
                                End If
                            End If
                        End If
                    End If
                    objAlerts.AlertDTLID = 19
                    objAlerts.ContactID = CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue ''pass CompanyID eventhough declared as contact
                    If objAlerts.GetCountCompany > 0 Then
                        ' Sending Mail for Key Organization Opportunity
                        Dim dtDetails As DataTable
                        objAlerts.AlertDTLID = 19 'Alert DTL ID for sending alerts in opportunities
                        objAlerts.DomainID = Session("DomainID")
                        dtDetails = objAlerts.GetIndAlertDTL
                        If dtDetails.Rows.Count > 0 Then
                            If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
                                Dim dtEmailTemplate As DataTable
                                Dim objDocuments As New DocumentList
                                objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
                                objDocuments.DomainID = Session("DomainID")
                                dtEmailTemplate = objDocuments.GetDocByGenDocID
                                If dtEmailTemplate.Rows.Count > 0 Then
                                    Dim objSendMail As New Email
                                    Dim dtMergeFields As New DataTable
                                    Dim drNew As DataRow
                                    dtMergeFields.Columns.Add("OppID")
                                    dtMergeFields.Columns.Add("DealAmount")
                                    dtMergeFields.Columns.Add("Organization")
                                    drNew = dtMergeFields.NewRow
                                    drNew("OppID") = arrOutPut(1)
                                    drNew("DealAmount") = TotalAmount
                                    drNew("Organization") = CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).Text

                                    Dim dtEmail As DataTable
                                    objAlerts.AlertDTLID = 19
                                    objAlerts.DomainID = Session("DomainId")
                                    dtEmail = objAlerts.GetAlertEmails
                                    Dim strCC As String = ""
                                    Dim p As Integer
                                    For p = 0 To dtEmail.Rows.Count - 1
                                        strCC = strCC & dtEmail.Rows(p).Item("vcEmailID") & ";"
                                    Next
                                    strCC = strCC.TrimEnd(";")
                                    dtMergeFields.Rows.Add(drNew)
                                    If objCommon Is Nothing Then objCommon = New CCommon
                                    objCommon.byteMode = 1
                                    objCommon.ContactID = Session("UserContactID")
                                    objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtDetails.Rows(0).Item("tintCCManager") = 1, objCommon.GetManagerEmail, ""), Session("UserEmail"), strCC, dtMergeFields)
                                End If
                            End If
                        End If
                    End If
                Catch ex As Exception
                End Try

                Dim strScript As String = "<script language=JavaScript>"
                strScript += "window.opener.reDirectPage('../opportunity/frmOpportunities.aspx?opId=" & arrOutPut(0) & "'); self.close();"
                strScript += "</script>"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
            End If

        Catch ex As Exception
            boolFlag = False
            Throw ex
        End Try
    End Sub

    Private Sub dgItems_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgItems.ItemCommand
        Try
            dsTemp = ViewState("SOItems")
            If e.CommandName = "Delete" Then
                Dim dtItem As DataTable
                dtItem = dsTemp.Tables(0)
                dtItem.Rows.Find(CType(e.Item.Cells(0).FindControl("lblOppItemCode"), Label).Text).Delete()
                dtItem.AcceptChanges()
                'Dim index As Integer = 1
                'For Each dr As DataRow In dtItem.Rows
                '    dr("numoppitemtCode") = index
                '    index += 1
                'Next
                'dtItem.AcceptChanges()
                UpdateDetails()
            ElseIf e.CommandName = "Edit" Then
                txtHidEditOppItem.Text = CType(e.Item.Cells(0).FindControl("lblOppItemCode"), Label).Text

                If Not dtOppAtributes Is Nothing Then dtOppAtributes.Rows.Clear()
                Dim dr As DataRow
                Dim dtItem As DataTable
                Dim dtSerItem As DataTable
                'Dim dtChildItems As DataTable
                dtItem = dsTemp.Tables(0)
                dtSerItem = dsTemp.Tables(1)
                'dtChildItems = dsTemp.Tables(2)

                Dim dRows As DataRow() = dtItem.Select("numoppitemtCode = " & CCommon.ToLong(txtHidEditOppItem.Text).ToString())
                If dRows.Length > 0 Then
                    dr = dRows(0)

                    radCmbItem.Text = CCommon.ToString(dr("vcItemName"))
                    radCmbItem.SelectedValue = CCommon.ToLong(dr("numItemCode"))

                    If Not IsDBNull(dr("ItemType")) Then
                        If Not ddltype.Items.FindByText(dr("ItemType")) Is Nothing Then
                            ddltype.ClearSelection()
                            ddltype.Items.FindByText(dr("ItemType")).Selected = True
                        End If
                    End If

                    LoadItemDetails()

                    If ddUOM.Items.FindByValue(dr("numUOM")) IsNot Nothing Then
                        ddUOM.ClearSelection()
                        ddUOM.Items.FindByValue(dr("numUOM")).Selected = True
                    End If

                    If ddlVendorWarehouse.Items.FindByValue(dr("numVendorWareHouse")) IsNot Nothing Then
                        ddlVendorWarehouse.ClearSelection()
                        ddlVendorWarehouse.Items.FindByValue(dr("numVendorWareHouse")).Selected = True

                        bindVendorShipmentMethod()
                    End If

                    If dr("numShipmentMethod") IsNot Nothing Then
                        For Each liVendorShipment As ListItem In ddlVendorShipmentMethod.Items
                            If liVendorShipment.Value.Contains(dr("numShipmentMethod") & "~") Then
                                ddlVendorShipmentMethod.ClearSelection()
                                liVendorShipment.Selected = True

                                bindVendorExpectedDelivery()
                                Exit For
                            End If
                        Next
                    End If

                    hdnVendorId.Value = CCommon.ToLong(dr("numSOVendorId"))

                    If ddltype.SelectedValue = "P" Then
                        If objCommon Is Nothing Then objCommon = New CCommon
                        radWareHouse.DataSource = objCommon.GetWarehouseAttrBasedItem(radCmbItem.SelectedValue)
                        radWareHouse.DataBind()

                        If _IsStockTransfer Then
                            radWarehouseTo.DataSource = objCommon.GetWarehouseAttrBasedItem(radCmbItem.SelectedValue)
                            radWarehouseTo.DataBind()
                        End If


                        trWarehouse.Visible = True
                        chkDropShip.Attributes.Add("style", "display:''")

                    Else
                        chkDropShip.Attributes.Add("style", "display:none")
                        trWarehouse.Visible = False
                        radWareHouse.SelectedValue = ""

                    End If

                    chkWorkOrder.Checked = dr("bitWorkOrder")
                    txtHidEditOppItem.Text = CCommon.ToString(dr("numoppitemtCode"))

                    If chkWorkOrder.Checked Then
                        trWorkOrder.Visible = True
                        txtInstruction.Text = CCommon.ToString(dr("vcInstruction"))
                        calCompliationDate.SelectedDate = CCommon.ToString(dr("bintCompliationDate"))

                        If Not IsDBNull(dr("numWOAssignedTo")) Then
                            If ddlWOAssignTo.Items.FindByValue(dr("numWOAssignedTo")) IsNot Nothing Then
                                ddlWOAssignTo.ClearSelection()
                                ddlWOAssignTo.Items.FindByValue(dr("numWOAssignedTo")).Selected = True
                            End If
                        End If
                    End If

                    Dim i, k As Integer
                    'If hdKit.Value = "True" Then
                    '    Dim dv As New DataView(dtChildItems)
                    '    dv.RowFilter = "numoppitemtCode=" & CCommon.ToString(dr("numoppitemtCode"))
                    '    'dgKitItems.DataSource = dv
                    '    'dgKitItems.DataBind()
                    'End If
                    txtprice.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(dr("monPrice")))
                    hdnUnitCost.Value = CCommon.ToDecimal(dr("monPrice"))

                    If ddltype.SelectedValue = "S" Then
                        txtunits.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(dr("numUnitHour")))
                        txtunits.Attributes.Add("onkeypress", "CheckNumber(1,event);TempHideButton()")
                    Else
                        txtunits.Text = CCommon.ToInteger(dr("numUnitHour"))
                        txtunits.Attributes.Add("onkeypress", "CheckNumber(2,event);TempHideButton()")
                    End If

                    hdvUOMConversionFactor.Value = dr("UOMConversionFactor")

                    If OppType = 1 Then
                        txtItemDiscount.Text = CCommon.ToDecimal(dr("fltDiscount"))

                        If dr("bitDiscountType") = True Then
                            radAmt.Checked = True
                        Else
                            radPer.Checked = True
                        End If
                    End If

                    If ddUOM.Items.FindByValue(dr("numUOM")) IsNot Nothing Then
                        ddUOM.ClearSelection()

                        ddUOM.Items.FindByValue(dr("numUOM")).Selected = True
                    End If

                    If (radWareHouse.Items.Count > 0) Then
                        If Not IsDBNull(dr("numWarehouseItmsID")) Then
                            If Not radWareHouse.FindItemByValue(dr("numWarehouseItmsID")) Is Nothing Then
                                radWareHouse.FindItemByValue(dr("numWarehouseItmsID")).Selected = True
                            End If
                        End If
                    End If
                    If _IsStockTransfer Then
                        If (radWarehouseTo.Items.Count > 0) Then
                            If Not IsDBNull(dr("numWarehouseItmsID")) Then
                                If Not radWarehouseTo.FindItemByValue(dr("numWarehouseItmsID")) Is Nothing Then
                                    radWarehouseTo.FindItemByValue(dr("numWarehouseItmsID")).Selected = True
                                End If
                            End If
                        End If
                    End If

                    If IIf(IsDBNull(dr("DropShip")), "", dr("DropShip")) = "True" Then
                        chkDropShip.Checked = True
                        radWareHouse.Visible = False
                        'trWarehouse.Attributes.Add("style", "display:none")
                    Else
                        chkDropShip.Checked = False
                        radWareHouse.Visible = True
                        'trWarehouse.Attributes.Add("style", "display:''")
                    End If
                    btnAdd.Text = "Update"
                    btnEditCancel.Visible = True
                End If
                UpdateDetails()
                CalculateProfit()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub UpdateDataTable()
        Try
            Dim strTax As String = ""
            Dim strTaxIds As String = ""
            Dim chkTaxItems As CheckBoxList

            If (_PageType = PageType.Sales) Then
                chkTaxItems = CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList)

                If CType(Me.Parent.FindControl("radNewCustomer"), RadioButton).Checked = True Then
                    objCommon = New CCommon
                    For Each Item As ListItem In chkTaxItems.Items
                        If Item.Selected = True Then

                            If Session("BaseTaxCalcOn") = 1 Then 'Base tax calculation on Shipping Address(2) or Billing Address(1) 
                                strTax = strTax & objCommon.TaxPercentage(0, CType(Me.Parent.FindControl("ddlBillCountry"), DropDownList).SelectedValue, CType(Me.Parent.FindControl("ddlBillState"), DropDownList).SelectedValue, Session("DomainID"), Item.Value, CType(Me.Parent.FindControl("txtCity"), TextBox).Text, CType(Me.Parent.FindControl("txtPostal"), TextBox).Text, Session("BaseTaxOnArea"), Session("BaseTaxCalcOn")) & ","
                            Else
                                strTax = strTax & objCommon.TaxPercentage(0, CType(Me.Parent.FindControl("ddlShipCountry"), DropDownList).SelectedValue, CType(Me.Parent.FindControl("ddlShipState"), DropDownList).SelectedValue, Session("DomainID"), Item.Value, CType(Me.Parent.FindControl("txtShipCity"), TextBox).Text, CType(Me.Parent.FindControl("txtShipPostal"), TextBox).Text, Session("BaseTaxOnArea"), Session("BaseTaxCalcOn")) & ","
                            End If

                        Else
                            strTax = strTax & "0#1,"
                        End If
                        strTaxIds = strTaxIds & Item.Value & ","
                    Next
                    strTax = strTax.TrimEnd(",")
                    strTaxIds = strTaxIds.TrimEnd(",")
                    txtTax.Text = strTax
                    TaxItemsId.Value = strTaxIds

                Else
                    If CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue <> "" Then
                        objCommon = New CCommon
                        Dim dtTaxTypes As DataTable
                        Dim ObjTaxItems As New TaxDetails
                        ObjTaxItems.DomainID = Session("DomainID")
                        dtTaxTypes = ObjTaxItems.GetTaxItems
                        For Each dr As DataRow In dtTaxTypes.Rows

                            'Base tax calculation on Shipping Address(2) or Billing Address(1) 
                            strTax = strTax & objCommon.TaxPercentage(CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue, 0, 0, Session("DomainID"), dr("numTaxItemID"), "", "", Session("BaseTaxOnArea"), Session("BaseTaxCalcOn")) & ","
                            strTaxIds = strTaxIds & dr("numTaxItemID") & ","

                        Next
                        strTax = strTax & objCommon.TaxPercentage(CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue, 0, 0, Session("DomainID"), 0, "", "", Session("BaseTaxOnArea"), Session("BaseTaxCalcOn"))

                        strTaxIds = strTaxIds & "0"
                        txtTax.Text = strTax
                        TaxItemsId.Value = strTaxIds
                    Else
                        For k As Integer = 0 To chkTaxItems.Items.Count - 1
                            strTax = strTax & "0" & ","
                        Next
                        txtTax.Text = strTax
                    End If
                End If
            End If

            If Not ViewState("SOItems") Is Nothing Then
                dsTemp = ViewState("SOItems")
                Dim dtItem As DataTable
                dtItem = dsTemp.Tables(0)
                Dim i, k As Integer
                i = 0

                Dim strTaxes(), strTaxApplicable() As String

                If (_PageType = PageType.Sales) Then
                    strTaxes = txtTax.Text.Split(",")
                    strTaxApplicable = Taxable.Value.Split(",")
                End If

                For Each dgGridItem As DataGridItem In dgItems.Items

                    dtItem.Rows(i).Item("numUnitHour") = Math.Abs(CCommon.ToDecimal(CType(dgGridItem.FindControl("txtUnits"), TextBox).Text))

                    dtItem.Rows(i).Item("monPrice") = Math.Abs(CCommon.ToDecimal(CType(dgGridItem.FindControl("txtUnitPrice"), TextBox).Text))
                    dtItem.Rows(i).Item("fltDiscount") = Math.Abs(CCommon.ToDecimal(CType(dgGridItem.FindControl("txtTotalDiscount"), TextBox).Text))

                    dtItem.Rows(i).Item("monTotAmtBefDiscount") = (dtItem.Rows(i).Item("numUnitHour") * dtItem.Rows(i).Item("UOMConversionFactor")) * dtItem.Rows(i).Item("monPrice")
                    If dtItem.Rows(i).Item("bitDiscountType") = 0 Then
                        dtItem.Rows(i).Item("monTotAmount") = dtItem.Rows(i).Item("monTotAmtBefDiscount") - dtItem.Rows(i).Item("monTotAmtBefDiscount") * dtItem.Rows(i).Item("fltDiscount") / 100
                    Else
                        dtItem.Rows(i).Item("monTotAmount") = dtItem.Rows(i).Item("monTotAmtBefDiscount") - dtItem.Rows(i).Item("fltDiscount")
                    End If
                    'dtItem.Rows(i).Item("monTotAmount") = dtItem.Rows(i).Item("numUnitHour") * dtItem.Rows(i).Item("monPrice")
                    If dtItem.Rows(i).Item("Op_Flag") = 0 Then
                        dtItem.Rows(i).Item("Op_Flag") = 2
                    End If

                    If (_PageType = PageType.Sales) Then
                        For k = 0 To chkTaxItems.Items.Count - 1
                            If dtItem.Columns.Contains("bitTaxable" & chkTaxItems.Items(k).Value) Then
                                If IIf(IsDBNull(dtItem.Rows(i).Item("bitTaxable" & chkTaxItems.Items(k).Value)), False, dtItem.Rows(i).Item("bitTaxable" & chkTaxItems.Items(k).Value)) = True Then
                                    Dim decTaxValue As Decimal = CCommon.ToDecimal(strTaxes(k).Split("#")(0))
                                    Dim tintTaxType As Short = CCommon.ToShort(strTaxes(k).Split("#")(1))

                                    If tintTaxType = 2 Then 'FLAT AMOUNT
                                        dtItem.Rows(i).Item("Tax" & chkTaxItems.Items(k).Value) = decTaxValue * (dtItem.Rows(i).Item("numUnitHour") * dtItem.Rows(i).Item("UOMConversionFactor"))
                                    Else 'PERCENTAGE
                                        dtItem.Rows(i).Item("Tax" & chkTaxItems.Items(k).Value) = IIf(IsDBNull(dtItem.Rows(i).Item("monTotAmount")), 0, dtItem.Rows(i).Item("monTotAmount")) * decTaxValue / 100
                                    End If

                                End If
                            End If
                        Next
                    End If
                    i = i + 1
                Next

                dsTemp.AcceptChanges()

                ViewState("SOItems") = dsTemp
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgItems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgItems.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then

                If DataBinder.Eval(e.Item.DataItem, "charItemType") = "S" Then
                    'Do not allow edit/delete for service item with project
                    If CCommon.ToLong(CType(e.Item.FindControl("lblnumProjectID"), Label).Text) > 0 AndAlso _PageType = PageType.AddEditOrder AndAlso OppType = 1 Then
                        CType(e.Item.FindControl("txtUnits"), TextBox).ReadOnly = True
                        CType(e.Item.FindControl("txtUnitPrice"), TextBox).ReadOnly = True

                        e.Item.FindControl("lnkEdit").Visible = False
                        e.Item.FindControl("lnkDelete").Visible = False
                    End If

                    CType(e.Item.FindControl("txtUnits"), TextBox).Attributes.Add("onkeypress", "CheckNumber(1,event)")
                Else
                    CType(e.Item.FindControl("txtUnits"), TextBox).Attributes.Add("onkeypress", "CheckNumber(2,event)")
                End If

                CType(e.Item.FindControl("txtUnits"), TextBox).Attributes.Add("onkeyup", "return CalculateTotal()")

                CType(e.Item.FindControl("txtUnitPrice"), TextBox).Attributes.Add("onkeyup", "return CalculateTotal()")
                CType(e.Item.FindControl("txtUnitPrice"), TextBox).Attributes.Add("onkeypress", "CheckNumber(1,event)")

                CType(e.Item.FindControl("txtTotalDiscount"), TextBox).Attributes.Add("onkeyup", "return CalculateTotal()")
                CType(e.Item.FindControl("txtTotalDiscount"), TextBox).Attributes.Add("onkeypress", "CheckNumber(1,event)")

                If (OppType = 1) Then
                    If (_PageType = PageType.AddEditOrder) Then
                        If (Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "bitWorkOrder")) = True) Then
                            e.Item.FindControl("lnkEdit").Visible = False
                            e.Item.FindControl("lnkDelete").Visible = False
                            CType(e.Item.FindControl("txtUnits"), TextBox).ReadOnly = True
                        End If
                    End If

                    If (Convert.ToBoolean(DataBinder.Eval(e.Item.DataItem, "bitDiscountType")) = True) Then
                        CType(e.Item.FindControl("lblDiscount"), Label).Text = DataBinder.Eval(e.Item.DataItem, "fltDiscount")
                    Else
                        CType(e.Item.FindControl("lblDiscount"), Label).Text = String.Format("{0:N2}", (DataBinder.Eval(e.Item.DataItem, "monTotAmtBefDiscount") - DataBinder.Eval(e.Item.DataItem, "monTotAmount"))) & " (" & DataBinder.Eval(e.Item.DataItem, "fltDiscount") & "%)"
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub radKitWareHouse_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemEventArgs)
        Try
            e.Item.Text = CType(e.Item.DataItem, DataRowView)("vcWareHouse").ToString() & " ---- " & CType(e.Item.DataItem, DataRowView)("Attr").ToString().TrimEnd(",")
            e.Item.Value = CType(e.Item.DataItem, DataRowView)("numWareHouseItemId").ToString()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub radWareHouse_ItemDataBound(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemEventArgs) Handles radWareHouse.ItemDataBound
        Try
            e.Item.Text = CType(e.Item.DataItem, DataRowView)("vcWareHouse").ToString() & " (" & CType(e.Item.DataItem, DataRowView)("numOnHand").ToString() & ") " & " ---- " & CType(e.Item.DataItem, DataRowView)("Attr").ToString().TrimEnd(",")
            e.Item.Value = CType(e.Item.DataItem, DataRowView)("numWareHouseItemId").ToString()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub radWareHouseTo_ItemDataBound(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemEventArgs) Handles radWarehouseTo.ItemDataBound
        Try
            e.Item.Text = CType(e.Item.DataItem, DataRowView)("vcWareHouse").ToString() & " (" & CType(e.Item.DataItem, DataRowView)("numOnHand").ToString() & ") " & " ---- " & CType(e.Item.DataItem, DataRowView)("Attr").ToString().TrimEnd(",")
            e.Item.Value = CType(e.Item.DataItem, DataRowView)("numWareHouseItemId").ToString()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub CreateColums(ByVal dtTable As DataTable)
        radCmbItem.HeaderTemplate = New RadComboBoxTemplate(ListItemType.Header, dtTable)
        radCmbItem.ItemTemplate = New RadComboBoxTemplate(ListItemType.Item, dtTable)
    End Sub

    Sub UpdateDetails()
        Try
            If dsTemp Is Nothing Then
                dsTemp = ViewState("SOItems")
            End If

            Dim dtAllItems As DataTable
            dtAllItems = dsTemp.Tables(0)

            If (_PageType = PageType.Sales) Then
                Dim TaxTemplateColumn As TemplateColumn

                For Each Item As ListItem In CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList).Items
                    If Item.Value <> 0 Then
                        TaxTemplateColumn = New TemplateColumn
                        TaxTemplateColumn.HeaderText = Item.Text
                        TaxTemplateColumn.ItemTemplate = New CreateTempColnPOS(ListItemType.Item, Item.Value)
                        TaxTemplateColumn.FooterTemplate = New CreateTempColnPOS(ListItemType.Footer, Item.Value)
                        'TaxTemplateColumn.Visible = False
                        dgItems.Columns.AddAt(15, TaxTemplateColumn)
                    End If
                Next
            End If

            dgItems.DataSource = dtAllItems
            dgItems.DataBind()
            If dtAllItems.Rows.Count > 0 Then
                Dim decGrandTotal As Decimal
                decGrandTotal = IIf(IsDBNull(dtAllItems.Compute("sum(monTotAmount)", "")), 0, dtAllItems.Compute("sum(monTotAmount)", ""))
                CType(dgItems.Controls(0).Controls(dtAllItems.Rows.Count + 1).FindControl("lblFTotal"), Label).Text = String.Format("{0:#,##0.00}", decGrandTotal)

                If (_PageType = PageType.Sales) Then
                    For Each Item As ListItem In CType(Me.Parent.FindControl("chkTaxItems"), CheckBoxList).Items
                        Dim decTax As Decimal = IIf(IsDBNull(dtAllItems.Compute("sum(Tax" & Item.Value & " )", "")), 0, dtAllItems.Compute("sum(Tax" & Item.Value & " )", ""))
                        decGrandTotal = decGrandTotal + decTax
                        CType(dgItems.Controls(0).Controls(dtAllItems.Rows.Count + 1).FindControl("lblFTaxAmt" & Item.Value), Label).Text = String.Format("{0:#,##0.00}", decTax)
                    Next
                End If

                lblTotal.Text = String.Format("{0:#,##0.00}", decGrandTotal) '+ IIf(IsNumeric(txtShipCost.Text), txtShipCost.Text, 0))
            Else
                lblTotal.Text = "0.00"
            End If
            Dim strBaseCurrency As String = ""
            If (Session("MultiCurrency") = True) And (_PageType <> PageType.AddEditOrder) Then
                lblCurrencyTotal.Text = "<b>Grand Total:</b> " + IIf(CType(Me.Parent.FindControl("ddlCurrency"), DropDownList).SelectedValue > 0, CType(Me.Parent.FindControl("ddlCurrency"), DropDownList).SelectedItem.Text.Substring(0, 3), "")
            Else
                lblCurrencyTotal.Text = "<b>Grand Total:</b> "
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub radWareHouse_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radWareHouse.SelectedIndexChanged
        Try
            If (radWareHouse.SelectedValue <> "" And txtunits.Text <> "") Then
                CalculateUnitPrice()
                ViewState("SelectedWarehouse") = CType(radWareHouse.Items(radWareHouse.SelectedIndex).FindControl("lblWareHouseID"), Label).Text
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub txtunits_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtunits.TextChanged
        Try
            CalculateUnitPrice()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub txtprice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtprice.TextChanged
        Try
            If OppType = 1 Then
                hdnUnitCost.Value = CCommon.ToDecimal(txtprice.Text)

                CalculateProfit()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddUOM_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddUOM.SelectedIndexChanged
        Try
            CalculateUnitPrice()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSaveOpenOrder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveOpenOrder.Click
        Try
            IsFromSaveAndOpenOrderDetails = True
            'If ProcessPayment() Then
            save()

            If (_PageType = PageType.Sales) Then
                Dim objRule As New OrderAutoRules
                objRule.GenerateAutoPO(OppBizDocID)
            End If

            If boolFlag Then
                If IsPOS = 1 Then
                    Response.Redirect("../opportunity/frmOpportunities.aspx?opId=" & lngOppId.ToString())
                Else
                    Dim strScript As String = "<script language=JavaScript>"
                    If GetQueryStringVal("IsClone") = "" Then
                        strScript += "window.opener.reDirectPage('../opportunity/frmOpportunities.aspx?opId=" & lngOppId.ToString() & "'); self.close();"
                    Else
                        strScript += "window.opener.top.frames[0].location.href=('../opportunity/frmOpportunities.aspx?opId=" & lngOppId.ToString() & "'); self.close();"
                    End If

                    strScript += "</script>"
                    If (Not Page.IsStartupScriptRegistered("clientScript")) Then Page.RegisterStartupScript("clientScript", strScript)
                End If
            End If
        Catch ex As Exception
            If ex.Message = "FY_CLOSED" Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Private Sub BindButtons()
        Try
            If (_PageType = PageType.Purchase Or _PageType = PageType.AddOppertunity Or _PageType = PageType.AddEditOrder) Then
                btnSaveOpenOrder.Visible = IIf(_PageType = PageType.Purchase, True, False)
                btnSaveNew.Visible = False
            End If

            'btnClose.Attributes.Add("onclick", "return Close()")

            If (_PageType = PageType.Purchase Or _PageType = PageType.Sales) Then
                CType(Me.Parent.FindControl("lnkBillTo"), LinkButton).Attributes.Add("onclick", "return OpenAddressWindow('BillTo')")
                CType(Me.Parent.FindControl("lnkShipTo"), LinkButton).Attributes.Add("onclick", "return OpenAddressWindow('ShipTo')")
            End If

            btnAdd.Attributes.Add("onclick", "return Add('')")
            chkDropShip.Attributes.Add("onclick", "return DropShip()")
            btnSave.Attributes.Add("onclick", "return Save()")

            If (_PageType = PageType.Purchase Or _PageType = PageType.Sales Or _PageType = PageType.AddOppertunity) Then
                hplPrice.Attributes.Add("onclick", "return openItem('" & radCmbItem.ClientID & "','" & txtunits.ClientID & "','" & CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).ClientID & "','" & txtSerialize.ClientID & "',' " & OppType & "')")
            ElseIf (_PageType = PageType.AddEditOrder) Then
                hplPrice.Attributes.Add("onclick", "return openItem('" & radCmbItem.ClientID & "','" & txtunits.ClientID & "','" & GetQueryStringVal("DivId") & "','" & txtSerialize.ClientID & "','" & OppType & "')")
            End If

            If (OppType = 2) Then
                hplUnits.Attributes.Add("onclick", "return openVendorCostTable('2')")
            ElseIf (OppType = 1) Then
                hplUnitCost.Attributes.Add("onclick", "return openVendorCostTable('1')")
            End If

            btnSaveNew.Attributes.Add("onclick", "return Save()")
            btnSaveOpenOrder.Attributes.Add("onclick", "return Save()")
            'txtAmountpaid.Attributes.Add("onkeyup", "return CalDue()")
            'txtShipCost.Attributes.Add("onkeyup", "return CalculateTotal()")


            'Get Default BizDoc Name Set in Domain details 
            Dim objRpt As New BACRM.BusinessLogic.Reports.PredefinedReports
            Dim dtBizDoc As DataTable
            objRpt.BizDocID = CCommon.ToLong(Session("AuthoritativeSalesBizDoc"))
            dtBizDoc = objRpt.GetBizDocType()
            If dtBizDoc.Rows.Count > 0 Then
                btnSave.Text = "Save & Create " + CCommon.ToString(dtBizDoc.Rows(0)("vcData")) & " "
            End If

            If _IsStockTransfer Then
                chkDropShip.Checked = False
                chkDropShip.Visible = False
                hplPrice.Visible = False
                txtprice.Visible = False
                trWarehouseTransferTo.Visible = True
                tdWareHouseLabel.InnerText = "Transfer From"
                trVendorWareHouse.Visible = False
                trVendorShipmentMethod.Visible = False
                btnSave.Visible = False
                btnSaveOpenOrder.Text = "Transfer Stock"
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub


    Private Sub btnEditCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditCancel.Click
        Try
            radCmbItem.Items.Clear()
            radCmbItem.SelectedValue = ""
            txtItemDiscount.Text = ""
            radCmbItem.Text = ""
            chkDropShip.Checked = False

            hdvUOMConversionFactor.Value = ""

            btnConfigureAttribute.Visible = False
            btnOptionsNAccessories.Visible = False
            'lblListPrice.Text = ""
            txtunits.Text = "1"
            txtprice.Text = ""
            radCmbItem.Items.Clear()
            txtdesc.Text = ""
            btnEditCancel.Visible = False
            ddltype.SelectedIndex = 0
            txtModelID.Text = ""

            lblBudget.Visible = False
            lblBudgetMonth.Visible = False
            lblBudgetYear.Visible = False
            chkDropShip.Enabled = True

            txtHidEditOppItem.Text = ""
            btnAdd.Text = "Add"
            ddUOM.ClearSelection()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub chkWorkOrder_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs) Handles chkWorkOrder.CheckedChanged
        If chkWorkOrder.Checked Then
            trWorkOrder.Visible = True
        Else
            trWorkOrder.Visible = False
        End If
    End Sub

    Private Sub ddlVendorWarehouse_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVendorWarehouse.SelectedIndexChanged
        Try
            bindVendorShipmentMethod()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub bindVendorShipmentMethod()
        Dim dtVendorShipmentMethod As DataTable

        If objItems Is Nothing Then
            objItems = New CItems
        End If
        objItems.DomainID = Session("DomainID")
        objItems.ShipAddressId = ddlVendorWarehouse.SelectedValue

        If (_PageType = PageType.Sales Or _PageType = PageType.Purchase Or _PageType = PageType.AddOppertunity) Then
            objItems.VendorID = IIf(CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue = "", 0, CType(Me.Parent.FindControl("radCmbCompany"), RadComboBox).SelectedValue)
        Else
            objItems.VendorID = GetQueryStringVal("DivId")
        End If

        objItems.byteMode = 2
        dtVendorShipmentMethod = objItems.GetVendorShipmentMethod

        ddlVendorShipmentMethod.DataSource = dtVendorShipmentMethod
        ddlVendorShipmentMethod.DataTextField = "ShipmentMethod"
        ddlVendorShipmentMethod.DataValueField = "vcListValue"
        ddlVendorShipmentMethod.DataBind()

        ddlVendorShipmentMethod.Items.Insert(0, New ListItem("--Select One--", "0~0"))

        bindVendorExpectedDelivery()
    End Sub

    Sub bindVendorExpectedDelivery()
        If ddlVendorShipmentMethod.SelectedIndex > 0 Then
            If _PageType = PageType.AddEditOrder Then
                lblExpectedDelivery.Text = FormattedDateFromDate(DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateTime.Parse(hdnOrderCreateDate.Value)).Date.AddDays(ddlVendorShipmentMethod.SelectedValue.Split("~")(1)), Session("DateFormat"))
            Else
                lblExpectedDelivery.Text = FormattedDateFromDate(DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow).Date.AddDays(ddlVendorShipmentMethod.SelectedValue.Split("~")(1)), Session("DateFormat"))
            End If
        Else
            lblExpectedDelivery.Text = ""
        End If
    End Sub
    Private Sub ddlVendorShipmentMethod_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVendorShipmentMethod.SelectedIndexChanged
        Try
            bindVendorExpectedDelivery()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnBindOrderGrid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBindOrderGrid.Click
        Try
            UpdateDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub chkSearchOrderCustomerHistory_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSearchOrderCustomerHistory.CheckedChanged
        Try
            hdnSearchOrderCustomerHistory.Value = IIf(chkSearchOrderCustomerHistory.Checked, 1, 0)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


    Private Sub btnUPCSKU_Click(sender As Object, e As System.EventArgs) Handles btnUPCSKU.Click
        Try
            If ViewState("SOItems") Is Nothing Then
                createSet()
            End If
            'UpdateDetails()
            If txtUPCSKU.Text.Trim <> "" Then
                If objItems Is Nothing Then objItems = New CItems

                objItems.DomainID = Session("DomainID")
                objItems.SKU = txtUPCSKU.Text.Trim

                Dim dtUPCSKU As DataTable
                dtUPCSKU = objItems.GetItemFromUPCSKU.Tables(0)

                If dtUPCSKU.Rows.Count > 0 Then
                    radCmbItem.Text = CCommon.ToString(dtUPCSKU.Rows(0)("vcItemName"))
                    radCmbItem.SelectedValue = CCommon.ToLong(dtUPCSKU.Rows(0)("numItemCode"))

                    ItemDropDownSelect()

                    If radWareHouse.FindItemByValue(dtUPCSKU.Rows(0)("numWareHouseItemID").ToString()) IsNot Nothing Then
                        radWareHouse.ClearSelection()
                        radWareHouse.FindItemByValue(dtUPCSKU.Rows(0)("numWareHouseItemID").ToString()).Selected = True
                    End If

                    CalculateUnitPrice(False)
                    AddNewItem()
                End If

                txtUPCSKU.Text = ""
                txtUPCSKU.Focus()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class


Public Class CreateTempColnPOS
    Implements ITemplate

    Dim TemplateType As ListItemType
    Dim Field1 As String

    Sub New(ByVal type As ListItemType, ByVal fld1 As String)
        Try
            TemplateType = type
            Field1 = fld1
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub InstantiateIn(ByVal Container As Control) Implements ITemplate.InstantiateIn
        Try
            Dim lbl1 As Label = New Label()
            Dim lbl2 As Label = New Label()
            Dim lbl3 As Label = New Label()
            'Dim lbl4 As Label = New Label()
            Select Case TemplateType
                Case ListItemType.Footer
                    lbl3.ID = "lblFTaxAmt" & Field1
                    AddHandler lbl3.DataBinding, AddressOf BindStringColumn3
                    Container.Controls.Add(lbl3)
                Case ListItemType.Item
                    lbl1.ID = "lblTaxAmt" & Field1
                    lbl2.ID = "lblTaxable" & Field1
                    'lbl4.ID = "lblID" & Field1
                    AddHandler lbl1.DataBinding, AddressOf BindStringColumn1
                    AddHandler lbl2.DataBinding, AddressOf BindStringColumn2
                    'AddHandler lbl4.DataBinding, AddressOf BindStringColumn4
                    lbl2.Attributes.Add("style", "display:none")
                    Container.Controls.Add(lbl1)
                    Container.Controls.Add(lbl2)
                    'Container.Controls.Add(lbl4)

            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindStringColumn1(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl1 As Label = CType(Sender, Label)
            Dim Container As DataGridItem = CType(lbl1.NamingContainer, DataGridItem)
            lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "Tax" & Field1)), "", DataBinder.Eval(Container.DataItem, "Tax" & Field1))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindStringColumn2(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl1 As Label = CType(Sender, Label)
            Dim Container As DataGridItem = CType(lbl1.NamingContainer, DataGridItem)
            lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "bitTaxable" & Field1)), "", DataBinder.Eval(Container.DataItem, "bitTaxable" & Field1))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindStringColumn3(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl1 As Label = CType(Sender, Label)
            Dim Container As DataGridItem = CType(lbl1.NamingContainer, DataGridItem)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class