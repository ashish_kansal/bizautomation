<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmCloneBizDoc.aspx.vb"
    Inherits="BACRM.UserInterface.Opportunities.frmCloneBizDoc" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Select Bizdoc To Clone</title>
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            
        });

        function ReturnAndClose() {            
            opener.location.reload(true);
            window.close();
            //return false;
            
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Select Bizdoc To Clone
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table cellspacing="0" border="0" cellpadding="0" width="400px">
        <tr>
            <td colspan="3">
                <br />
            </td>
        </tr>
        <tr>
            <td align="right">Clone To Bizdoc : </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlBizDocs" ></asp:DropDownList>
            </td>
            <td>
                <asp:Button ID="btnClone" Text="Clone & Close" CssClass="button" runat="server" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <br />
            </td>
        </tr>
    </table>
</asp:Content>
