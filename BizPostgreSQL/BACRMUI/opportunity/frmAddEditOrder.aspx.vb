﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports System.Reflection
Imports BACRM.BusinessLogic.Item
Imports Infragistics.WebUI.UltraWebGrid
Imports BACRM.BusinessLogic.Accounting
Imports Telerik.Web.UI

Public Class frmAddEditOrder
    Inherits BACRMPage

    Dim objOpportunity As New OppotunitiesIP
    Dim dsTemp As DataSet
    Dim lngOppId As Long

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.UpdateItemRadComboValues(GetQueryStringVal("OppType"), GetQueryStringVal("DivId"))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblException.Text = ""
            lngOppId = GetQueryStringVal("opid")
            order1.OppType = GetQueryStringVal("OppType")
            order1.IsStockTransfer = CCommon.ToBool(CCommon.ToInteger(GetQueryStringVal("bitStock")))
            order1._PageType = order1.PageType.AddEditOrder
            If Not IsPostBack Then
                CType(order1.FindControl("hdnEnabledItemLevelUOM"), HiddenField).Value = CCommon.ToBool(Session("EnableItemLevelUOM"))
                hdnCmbDivisionID.Value = GetQueryStringVal("DivId")
                LoadDataToSession()
                LoadPurchaseIncentives()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

    Private Sub LoadPurchaseIncentives()
        If GetQueryStringVal("OppType") = "2" Then

            Dim objItems As CItems = New CItems
            objItems.DivisionID = hdnCmbDivisionID.Value
            Dim dtTableOrder As DataTable
            dtTableOrder = objItems.GetOpenOrderDetails
            If dtTableOrder.Rows.Count > 0 Then
                If IsDBNull(dtTableOrder.Rows(0).Item("TotalPOAmount")) Then
                    CType(order1.FindControl("divOpenPO"), HtmlGenericControl).Visible = False
                Else
                    If CCommon.ToLong(dtTableOrder.Rows(0).Item("TotalPOAmount")) > 0 Then
                        CType(order1.FindControl("lblOpenPOToals"), Label).Text = "$" + String.Format("{0:#,##0.00}", dtTableOrder.Rows(0).Item("TotalPOAmount")) + "," + CCommon.ToString(dtTableOrder.Rows(0).Item("TotalWeight")) + " Lbs," + String.Format("{0:#,##0.00}", dtTableOrder.Rows(0).Item("TotalQty")) + " Qty"
                        CType(order1.FindControl("divOpenPO"), HtmlGenericControl).Visible = True
                    Else
                        CType(order1.FindControl("divOpenPO"), HtmlGenericControl).Visible = False
                    End If
                End If
                If IsDBNull(dtTableOrder.Rows(0).Item("Purchaseincentives")) Then
                    CType(order1.FindControl("tablePurchaseIncentiveLabel"), HtmlTable).Visible = False
                Else
                    If CCommon.ToString(dtTableOrder.Rows(0).Item("Purchaseincentives")) <> "" Then
                        CType(order1.FindControl("lblPurchaseIncentives"), Literal).Text = "<Span>" & HttpUtility.HtmlDecode(dtTableOrder.Rows(0).Item("Purchaseincentives")) & "</span>"
                        CType(order1.FindControl("tablePurchaseIncentiveLabel"), HtmlTable).Visible = True
                    Else
                        CType(order1.FindControl("tablePurchaseIncentiveLabel"), HtmlTable).Visible = False
                    End If
                End If
            End If
        End If

    End Sub
    Private Sub LoadDataToSession()
        Try
            Dim dtItems As DataTable
            objOpportunity.DomainID = Session("DomainID")
            objOpportunity.OpportunityId = lngOppId
            objOpportunity.Mode = 4
            dsTemp = objOpportunity.GetOrderItems()

            ViewState("UsedPromotion") = objOpportunity.GetUsedPromotions()

            dtItems = dsTemp.Tables(0)
            Dim k As Integer
            Dim intCheck As Integer = 0
            For k = 0 To dtItems.Rows.Count - 1
                'If dtItems.Rows(k).Item("numContainer") > 0 Then
                '    dtItems.Rows(k).Item("numContainerQty") = Math.Ceiling(dtItems.Rows(k)("numUnitHour") / dtItems.Rows(k).Item("numContainerQty"))
                'End If
                If dtItems.Rows(k).Item("charItemType") <> "S" Then intCheck = 1
                dtItems.Rows(k)("numUnitHour") = Math.Abs(dtItems.Rows(k)("numUnitHour") / dtItems.Rows(k)("UOMConversionFactor"))
            Next

            dtItems.Columns.Add("numVendorID", System.Type.GetType("System.Int32"))
            dtItems.Columns.Add("vcInstruction")
            dtItems.Columns.Add("bintCompliationDate")
            dtItems.Columns.Add("dtPlannedStart")
            dtItems.Columns.Add("numWOAssignedTo")
            dtItems.Columns("ItemReleaseDate").DateTimeMode = System.Data.DataSetDateTime.Unspecified

            dtItems.AcceptChanges()
            Dim dtItem As DataTable
            Dim dtSerItem As DataTable
            Dim dtChildItems As New DataTable
            dtItem = dsTemp.Tables(0)
            dtSerItem = dsTemp.Tables(1)
            'dtChildItems = dsTemp.Tables(2)
            dtItem.TableName = "Item"
            dtSerItem.TableName = "SerialNo"
            'dtChildItems.TableName = "ChildItems"
            dtItem.PrimaryKey = New DataColumn() {dsTemp.Tables(0).Columns("numoppitemtCode")}

            dtChildItems.Columns.Add("numOppChildItemID", System.Type.GetType("System.Int32"))
            dtChildItems.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
            dtChildItems.Columns.Add("numItemCode")
            dtChildItems.Columns.Add("numQtyItemsReq")
            dtChildItems.Columns.Add("vcItemName")
            dtChildItems.Columns.Add("monListPrice")
            dtChildItems.Columns.Add("UnitPrice")
            dtChildItems.Columns.Add("charItemType")
            dtChildItems.Columns.Add("txtItemDesc")
            dtChildItems.Columns.Add("numWarehouseItmsID")
            dtChildItems.Columns.Add("Op_Flag")
            dtChildItems.Columns.Add("ItemType")
            dtChildItems.Columns.Add("Attr")
            dtChildItems.Columns.Add("vcWareHouse")

            dtChildItems.TableName = "ChildItems"

            dsTemp.Tables.Add(dtChildItems)

            If dtItem.Columns("bitTaxable0") Is Nothing Then
                dtItem.Columns.Add("bitTaxable0")
            End If

            If dtItem.Columns("Tax0") Is Nothing Then
                dtItem.Columns.Add("Tax0", GetType(Decimal))
            End If

            If dtItem.Columns("TotalTax") Is Nothing Then
                dtItem.Columns.Add("TotalTax", GetType(Decimal))
            End If

            If dtSerItem.ParentRelations.Count = 0 Then dsTemp.Relations.Add("Item", dsTemp.Tables(0).Columns("numoppitemtCode"), dsTemp.Tables(1).Columns("numoppitemtCode"))
            dsTemp.AcceptChanges()
            order1.SOItems = dsTemp

            CType(order1.FindControl("dgItems"), DataGrid).Columns(8).Visible = False

            Dim dtDetails As DataTable
            objOpportunity.OpportunityId = lngOppId
            objOpportunity.DomainID = Session("DomainID")
            objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            dtDetails = objOpportunity.OpportunityDTL.Tables(0)

            order1.OppStatus = CCommon.ToShort(dtDetails.Rows(0).Item("tintOppStatus"))
            order1.OrderSource = CCommon.ToLong(dtDetails.Rows(0).Item("tintSource"))
            order1.SourceType = CCommon.ToShort(dtDetails.Rows(0).Item("tintSourceType"))

            Dim vcPageName As String = ""
            If lngOppId > 0 Then
                vcPageName = "frmAddEdit"
            Else
                vcPageName = "frmNew"
            End If

            If order1.OppType = 1 AndAlso order1.OppStatus = 0 Then 'Sales Opportunity
                vcPageName = vcPageName & "SalesOpportunity"
            ElseIf order1.OppType = 1 AndAlso order1.OppStatus = 1 Then 'Sales Order
                vcPageName = vcPageName & "SalesOrder"
            ElseIf order1.OppType = 2 AndAlso order1.OppStatus = 0 Then 'Purchase Opportunity
                vcPageName = vcPageName & "PurchaseOpportunity"
                CType(order1.FindControl("chkDropShip"), CheckBox).Visible = False
            ElseIf order1.OppType = 2 AndAlso order1.OppStatus = 1 Then 'Purchase Order
                vcPageName = vcPageName & "PurchaseOrder"
                CType(order1.FindControl("chkDropShip"), CheckBox).Visible = False
            End If

            PersistTable.Load(strParam:=vcPageName, isMasterPage:=True)
            If PersistTable.Count > 0 Then
                CType(order1.FindControl("hdnSearchPageName"), HiddenField).Value = vcPageName
                CType(order1.FindControl("hdnSearchType"), HiddenField).Value = CCommon.ToString(PersistTable(vcPageName))
            End If

            CType(order1.FindControl("hdnOrderCreateDate"), HiddenField).Value = dtDetails.Rows(0).Item("bintCreatedDate")
            CType(order1.FindControl("hdnShipFromLocation"), HiddenField).Value = CCommon.ToLong(dtDetails.Rows(0).Item("numShipFromWarehouse"))
            CType(order1.FindControl("hdnCountry"), HiddenField).Value = dtDetails.Rows(0).Item("numShipCountry")
            CType(order1.FindControl("hdnState"), HiddenField).Value = dtDetails.Rows(0).Item("numShipState")
            CType(order1.FindControl("hdnReleaseDate"), HiddenField).Value = CCommon.ToString(dtDetails.Rows(0).Item("dtReleaseDate"))
            CType(order1.FindControl("hdnExpectedDate"), HiddenField).Value = CCommon.ToString(dtDetails.Rows(0).Item("dtExpectedDate"))
            CType(order1.FindControl("hdnShipVia"), HiddenField).Value = CCommon.ToLong(dtDetails.Rows(0).Item("intUsedShippingCompany"))
            CType(order1.FindControl("hdnShippingService"), HiddenField).Value = CCommon.ToLong(dtDetails.Rows(0).Item("numOrderShippingService"))

            hdnCurrencyID.Value = CCommon.ToLong(dtDetails.Rows(0).Item("numCurrencyID"))

            CType(order1.FindControl("dgItems"), DataGrid).DataSource = dsTemp.Tables(0)
            CType(order1.FindControl("dgItems"), DataGrid).DataBind()

            If order1.OppType = 2 AndAlso dsTemp.Tables(0).Select("bitDropShip=0").Length = 0 Then
                CType(order1.FindControl("chkDropShip"), CheckBox).Checked = True
            End If


            Dim totalWeight As Double = 0
            For Each dr As DataRow In dsTemp.Tables(0).Rows
                totalWeight = totalWeight + (CCommon.ToDouble(dr("fltItemWeight")) * CCommon.ToDouble(dr("numUnitHour")))
            Next

            CType(order1.FindControl("txtTotalWeight"), TextBox).Text = totalWeight
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function CheckIfColumnExist(ByVal columnName As String) As Boolean
        Try
            Dim isColumnExist As Boolean = False

            For Each column As DataGridColumn In CType(order1.FindControl("dgItems"), DataGrid).Columns
                If column.HeaderText = columnName Then
                    isColumnExist = True
                    Exit For
                End If
            Next

            Return isColumnExist
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub lnkClick_Click(sender As Object, e As System.EventArgs) Handles lnkClick.Click
        Try
            tblMultipleOrderMessage.Visible = False
            order1.SOItems = Nothing
            Response.Redirect(Request.Url.ToString())
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As String)
        lblException.Text = ex
        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ScrollTo", "ScrollToError();", True)
    End Sub
End Class