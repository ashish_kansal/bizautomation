﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contacts

Namespace BACRM.UserInterface.Opportunities

    Public Class frmSelectPickList
        Inherits BACRMPage

#Region "Page Events"

        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                lblException.Text = ""
                divException.Style.Add("display", "none")

                If Not Page.IsPostBack Then
                    hdnOppID.Value = CCommon.ToLong(GetQueryStringVal("OppID"))
                    BindPickList()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

#End Region

#Region "Private Methods"

        ''' <summary>
        ''' Gets list pick list added to order with no fulfillment bizdocs generated from it
        ''' </summary>
        ''' <remarks></remarks>
        Private Sub BindPickList()
            Try
                Dim objOppBizDocs As New OppBizDocs
                objOppBizDocs.OppId = CCommon.ToLong(hdnOppID.Value)
                Dim dt As DataTable = objOppBizDocs.GetPickListWithNoFulfillment()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    rblPickLists.DataSource = dt
                    rblPickLists.DataTextField = "vcBizDocID"
                    rblPickLists.DataValueField = "numOppBizDocsId"
                    rblPickLists.DataBind()

                    rblPickLists.Items(0).Selected = True
                Else
                    DisplayError("Pick list bizdocs are not added to order or Fulfillment bizdoc is already added against pick list bizdoc(s)")
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub DisplayError(ByVal errorMessage As String)
            Try
                lblException.Text = errorMessage
                divException.Style.Remove("display")
            Catch ex As Exception

            End Try
        End Sub

        Private Function CreateFulfillmentOrder(ByVal lngOppId As Long) As Long
            Try
                Dim OppBizDocID, lngDivId, JournalId As Long
                Dim lngCntID As Long = 0

                Using objTransaction As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})

                    If CCommon.ToBool(Session("IsMinUnitPriceRule")) Then
                        If CheckIfApprovalRequired(lngOppId) Then
                            DisplayError("Not able to create bill because approval of unit price(s) is required.")
                            Exit Function
                        End If
                    End If

                    Dim dtDetails, dtBillingTerms As DataTable

                    Dim objPageLayout As New CPageLayout
                    objPageLayout.OpportunityId = lngOppId
                    objPageLayout.DomainID = Session("DomainID")
                    dtDetails = objPageLayout.OpportunityDetails.Tables(0)
                    lngDivId = dtDetails.Rows(0).Item("numDivisionID")
                    lngCntID = dtDetails.Rows(0).Item("numContactID")


                    Dim objOppBizDocs As New OppBizDocs
                    objOppBizDocs.UserCntID = Session("UserContactID")
                    objOppBizDocs.DomainID = Session("DomainID")

                    If objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), lngDivId) = 0 Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip"" To Save BizDoc' );", True)
                        Exit Function
                    End If

                    objOppBizDocs.OppId = lngOppId
                    objOppBizDocs.OppType = 1
                    objOppBizDocs.FromDate = DateTime.UtcNow
                    objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    objOppBizDocs.BizDocId = 296 'Fulfillment BizDoc


                    objOppBizDocs.numSourceBizDocId = CCommon.ToLong(rblPickLists.SelectedValue)
                    objOppBizDocs.FromOppBizDocsId = CCommon.ToLong(rblPickLists.SelectedValue)


                    'SETS BIZDOC TEMPLETE NEEDS TO BE USED
                    '-------------------------------------
                    objOppBizDocs.byteMode = 1
                    Dim dtDefaultBizDocTemplate As DataTable = objOppBizDocs.GetBizDocTemplateList()
                    objOppBizDocs.BizDocTemplateID = CCommon.ToLong(dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID"))
                    '-------------------------------------

                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.Mode = 33
                    objCommon.Str = 296 'DEFUALT BIZ DOC ID FOR BILL
                    objOppBizDocs.SequenceId = objCommon.GetSingleFieldValue()

                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.Mode = 34
                    objCommon.Str = lngOppId
                    objOppBizDocs.RefOrderNo = objCommon.GetSingleFieldValue()
                    objOppBizDocs.bitPartialShipment = True

                    OppBizDocID = objOppBizDocs.SaveBizDoc()
                    If OppBizDocID = 0 Then
                        DisplayError("Not able to create fulfillment bizdoc. A BizDoc by the same name is already created. Please select another BizDoc !")
                        Exit Function
                    End If

                    'AUTO FULFILLS bizdoc AND MAKED QTY AS SHIPPED AND RELEASES IT FROM ALLOCATION
                    Dim objOppBizDoc As New OppBizDocs
                    objOppBizDoc.DomainID = CCommon.ToLong(Session("DomainID"))
                    objOppBizDoc.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objOppBizDoc.OppId = lngOppId
                    objOppBizDoc.OppBizDocId = OppBizDocID
                    objOppBizDoc.AutoFulfillBizDoc()


                    'IMPORTANT: WE ARE MAKING ACCOUNTING CHANGES BECAUSE WE ARE CREATING FULFILLMENT ORDER IN BACKGROUP AND MARKIGN IT AS SHIPPED
                    '           OTHERWISE ACCOUNTING IS ONLY IMPACTED WHEN USER SHIPPED IT FROM SALES FULFILLMENT PAGE
                    'CREATE SALES CLEARING ENTERIES
                    'CREATE JOURNALS ONLY FOR FILFILLMENT BIZDOCS
                    '-------------------------------------------------
                    Dim ds As New DataSet
                    Dim dtOppBiDocItems As DataTable

                    objOppBizDocs.OppBizDocId = OppBizDocID
                    ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                    dtOppBiDocItems = ds.Tables(0)

                    Dim objCalculateDealAmount As New CalculateDealAmount
                    objCalculateDealAmount.CalculateDealAmount(lngOppId, OppBizDocID, 1, CCommon.ToLong(Session("DomainID")), dtOppBiDocItems)

                    JournalId = objOppBizDocs.GetJournalIdForAuthorizativeBizDocs

                    'WHEN FROM COA SOMEONE DELETED JOURNALS THEN WE NEED TO CREATE NEW JOURNAL HEADER ENTRY
                    JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, Entry_Date:=Date.UtcNow.Date, Description:=CStr(ds.Tables(1).Rows(0).Item("vcBizDocID")))

                    Dim objJournalEntries As New BACRM.BusinessLogic.Accounting.JournalEntry
                    If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                        objJournalEntries.SaveJournalEnteriesSalesClearing(lngOppId, CCommon.ToLong(Session("DomainID")), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                    Else
                        objJournalEntries.SaveJournalEnteriesSalesClearingNew(lngOppId, CCommon.ToLong(Session("DomainID")), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                    End If
                    '---------------------------------------------------------------------------------


                    'IF fulfillment order qty is less than pack qty than change packing slip quantity to match fulfillment order qty
                    objOppBizDocs.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objOppBizDocs.DomainID = CCommon.ToLong(Session("DomainID"))
                    objOppBizDocs.OppId = lngOppId
                    objOppBizDocs.OppBizDocId = CCommon.ToLong(rblPickLists.SelectedValue)
                    objOppBizDocs.ChangePackingSlipQty()

                    objTransaction.Complete()
                End Using

                ''Added By Sachin Sadhu||Date:1stMay2014
                ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                ''          Using Change tracking
                Dim objWfA As New BACRM.BusinessLogic.Workflow.Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = OppBizDocID
                objWfA.SaveWFBizDocQueue()
                'end of code

                Return OppBizDocID
            Catch ex As Exception
                If ex.Message.Contains("WORK_ORDER_NOT_COMPLETED") Then
                    DisplayError("Not able to add fulfillment bizdoc because some work orders are still not completed.<br />")
                ElseIf ex.Message.Contains("QTY_MISMATCH") Then
                    DisplayError("Not able to add fulfillment bizdoc because qty is not same as required qty.<br />")
                ElseIf ex.Message.Contains("REQUIRED_SERIALS_NOT_PROVIDED") Then
                    DisplayError("Not able to add fulfillment bizdoc because required number of serials are not provided.<br />")
                ElseIf ex.Message.Contains("INVALID_SERIAL_NUMBERS") Then
                    DisplayError("Not able to add fulfillment bizdoc because invalid serials are provided.")
                ElseIf ex.Message.Contains("REQUIRED_LOTNO_NOT_PROVIDED") Then
                    DisplayError("Not able to add fulfillment bizdoc because required number of Lots are not provided.")
                ElseIf ex.Message.Contains("INVALID_LOT_NUMBERS") Then
                    DisplayError("Not able to add fulfillment bizdoc because invalid Lots are provided.")
                ElseIf ex.Message.Contains("SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY") Then
                    DisplayError("Not able to add fulfillment bizdoc because Lot number do not have enough qty to fulfill.")
                ElseIf ex.Message.Contains("NOTSUFFICIENTQTY_ALLOCATION") Then
                    DisplayError("Not able to add fulfillment bizdoc because warehouse allocation is invalid.")
                ElseIf ex.Message.Contains("NOTSUFFICIENTQTY_ONHAND") Then
                    DisplayError("Not able to add fulfillment bizdoc because warehouse does not have enought on hand quantity.")
                ElseIf ex.Message = "NOT_ALLOWED" Then
                    DisplayError("To split order items multiple bizdocs you must create all bizdocs with ""partial fulfilment"" checked!")
                ElseIf ex.Message = "NOT_ALLOWED_FulfillmentOrder" Then
                    DisplayError("Only a Sales Order can create a Fulfillment Order")
                ElseIf ex.Message = "FY_CLOSED" Then
                    DisplayError("This transaction can not be posted,Because transactions date belongs to closed financial year.")
                ElseIf ex.Message = "AlreadyInvoice" Then
                    DisplayError("You can’t create a Invoice against a Fulfillment Order that already contains a Invoice")
                ElseIf ex.Message = "AlreadyPackingSlip" Then
                    DisplayError("You can’t create a Packing-Slip against a Fulfillment Order that already contains a Packing-Slip")
                ElseIf ex.Message = "AlreadyInvoice_NOT_ALLOWED_FulfillmentOrder" Then
                    DisplayError("You can’t create a fulfillment order against a sales order that already contains an Invoice or Packing-Slip")
                ElseIf ex.Message = "AlreadyFulfillmentOrder" Then
                    DisplayError("Manually adding Invoices or Packing Slips to a Sales Order is not allowed after a Fulfillment Order (FO) is added. Your options are to edit the FO status to trigger creation of Invoices and/or Packing Slips, Delete the Fulfillment Order, or Create Invoices and/or Packing Slips from the Sales Fulfillment section")
                ElseIf ex.Message.Contains("FULFILLMENT_BIZDOC_REQUIRED_PICKLIST_FOR_ALLOCATE_ON_PICKLIST_SETTING") Then
                    DisplayError("You must have to select Pick List to create a fulfillment bizdoc when customer have ""Allocate on pick list"" default settings checked.")
                ElseIf ex.Message.Contains("BIZDOC_ALREADY_GENERATED_AGAINST_SOURCE") Then
                    DisplayError("Fulfillment bizdoc is already generated against selected pick list")
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(CCommon.ToString(ex) & "<br />")
                End If
            End Try
        End Function

        Private Function CheckIfApprovalRequired(ByVal lngOppId As Long) As Boolean
            Try
                Dim objOpportunity As New MOpportunity
                objOpportunity.OpportunityId = CCommon.ToLong(hdnOppID.Value)
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.UserCntID = Session("UserContactID")
                objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                Dim dsTemp As DataSet = objOpportunity.GetOrderItemsProductServiceGrid

                If Not dsTemp Is Nothing AndAlso dsTemp.Tables.Count > 0 AndAlso dsTemp.Tables(0).Rows.Count > 0 Then
                    Dim results = From myRow In dsTemp.Tables(0).AsEnumerable()
                                  Where CCommon.ToBool(myRow.Field(Of Boolean)("bitItemPriceApprovalRequired")) = True
                                  Select myRow
                    If results.Count > 0 Then
                        Return True
                    Else
                        Return False
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

#End Region

#Region "Event Handlers"

        Private Sub btnSaveClose_Click(sender As Object, e As EventArgs) Handles btnSaveClose.Click
            Try
                If CCommon.ToLong(rblPickLists.SelectedValue) > 0 AndAlso CCommon.ToLong(hdnOppID.Value) Then
                    Dim oppBizDocID As Long = CreateFulfillmentOrder(CCommon.ToLong(hdnOppID.Value))

                    If oppBizDocID > 0 Then
                        Try
                            Dim objOppBizDocs As New OppBizDocs
                            objOppBizDocs.CreateInvoiceFromFulfillmentOrder(CCommon.ToLong(Session("DomainID")), CCommon.ToLong(Session("UserContactID")), CCommon.ToLong(hdnOppID.Value), oppBizDocID, CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")))
                            ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "RefreshAndClose", "window.opener.location.href=window.opener.location.href; window.close();", True)
                        Catch ex As Exception
                            'DO NOT THROW ERROR
                        End Try
                    End If
                Else
                    lblException.Text = "Select Pick list."
                    divException.Style.Remove("display")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

#End Region

        

        
    End Class
End Namespace