Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Partial Public Class opportunity_frmRecomBillTime
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            btnAdd.Attributes.Add("onclick", "return Close()")
            
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlItem_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlItem.SelectedIndexChanged
        Try
            Dim objItems As New CItems
            objItems.ItemCode = ddlItem.SelectedValue
            objItems.NoofUnits = IIf(GetQueryStringVal( "Hour") = "", 0, GetQueryStringVal( "Hour"))
            objItems.DivisionID = GetQueryStringVal( "DivID")
            lblRate.Text = String.Format("{0:#,##0.00}", objItems.GetItemPriceafterdiscount())
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            Dim objItems As New CItems
            objItems.str = txtItem.Text.Trim
            ddlItem.DataSource = objItems.FindItem
            ddlItem.DataTextField = "vcItemName"
            ddlItem.DataValueField = "numItemCode"
            ddlItem.DataBind()
            ddlItem.Items.Insert(0, "--Select One--")
            ddlItem.Items.FindByText("--Select One--").Value = 0
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class