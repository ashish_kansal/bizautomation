'Created By Anoop Jayaraj
Imports System.IO
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Opportunities
    Public Class frmLogoUpload
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents txtMagFile As System.Web.UI.HtmlControls.HtmlInputFile

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object
        Dim strFilePath As String
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
        Dim strFileName As String

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                btnCancel.Attributes.Add("onclick", "return Close()")

                If GetQueryStringVal("frm") = "BizDocs" Then pnlLogoTip.Visible = True
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub MagUpload()
            Try
                Dim lobjUserAccess As New UserAccess

                If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                    Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
                End If
                Dim FilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID"))
                Dim ArrFilePath, ArrFileExt, ArrFileName
                Dim strFileExt, strFileName As String

                If txtMagFile.PostedFile.FileName = "" Then Exit Sub

                strFilePath = FilePath
                ArrFileExt = Split(txtMagFile.PostedFile.FileName, ".")
                ArrFileName = Split(txtMagFile.PostedFile.FileName, "\")
                strFileExt = ArrFileExt(UBound(ArrFileExt))
                strFileName = "BizDocLogoFile" & Day(DateTime.Now) & Month(DateTime.Now) & Year(DateTime.Now) & Hour(DateTime.Now) & Minute(DateTime.Now) & Second(DateTime.Now) & "." & strFileExt
                ''strFileName = "Logo.gif"
                strFilePath = strFilePath & strFileName

                If Not txtMagFile.PostedFile Is Nothing Then
                    txtMagFile.PostedFile.SaveAs(strFilePath)
                    lobjUserAccess.DomainID = Session("DomainID")
                    lobjUserAccess.ImagePath = strFileName
                    If GetQueryStringVal("frm") = "BizDocs" Then
                        lobjUserAccess.byteMode = 0
                    ElseIf GetQueryStringVal("frm") = "CheckCompany" Then
                        lobjUserAccess.byteMode = 1
                    ElseIf GetQueryStringVal("frm") = "CheckBank" Then
                        lobjUserAccess.byteMode = 2
                    End If
                    lobjUserAccess.SaveImagePathDetails()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        'Added By Sachin Sadhu||Date:23rdDec2013
        'Purpose:To Save different  Logo  for bizDocs
        Sub BizdocLogoUpload()
            Try
                Dim lobjUserAccess As New UserAccess

                If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                    Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
                End If
                Dim FilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID"))
                Dim ArrFilePath, ArrFileExt, ArrFileName
                Dim strFileExt, strFileName As String
                Dim iTemplateID As Int32
                iTemplateID = Convert.ToInt32(Server.UrlDecode(CCommon.ToString(GetQueryStringVal("TemplateID"))))
                If txtMagFile.PostedFile.FileName = "" Then Exit Sub

                strFilePath = FilePath
                ArrFileExt = Split(txtMagFile.PostedFile.FileName, ".")
                ArrFileName = Split(txtMagFile.PostedFile.FileName, "\")
                strFileExt = ArrFileExt(UBound(ArrFileExt))
                strFileName = "BizDocLogoFile" & Day(DateTime.Now) & Month(DateTime.Now) & Year(DateTime.Now) & Hour(DateTime.Now) & Minute(DateTime.Now) & Second(DateTime.Now) & "." & strFileExt
                ''strFileName = "Logo.gif"
                strFilePath = strFilePath & strFileName

                If Not txtMagFile.PostedFile Is Nothing Then
                    txtMagFile.PostedFile.SaveAs(strFilePath)
                    lobjUserAccess.DomainID = Session("DomainID")
                    lobjUserAccess.ImagePath = strFileName
                    If GetQueryStringVal("frm") = "BizDocs" Then
                        lobjUserAccess.byteMode = 0
                    ElseIf GetQueryStringVal("frm") = "CheckCompany" Then
                        lobjUserAccess.byteMode = 1
                    ElseIf GetQueryStringVal("frm") = "CheckBank" Then
                        lobjUserAccess.byteMode = 2
                    End If
                    lobjUserAccess.BizDocSaveLogo(iTemplateID)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        'End of Code by sachin Sadhu

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                'commented by Sachin
                'MagUpload()
                'Added By Sachin Sadhu||Date:24thDec2013
                'Purpose:only BizDoc related pages save Logo in -BizDocTemplate table ,other pages save in  Domain Table
                If GetQueryStringVal("frm") = "BizDocs" Then

                    BizdocLogoUpload()

                Else
                    MagUpload()
                End If

                Response.Write("<script>opener.location.reload(true);window.close();</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
