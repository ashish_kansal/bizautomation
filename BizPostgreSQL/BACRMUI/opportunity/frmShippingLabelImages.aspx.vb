﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Partial Public Class frmShippingLabelImages
    Inherits BACRMPage
    Dim lngBoxID As Long
    Dim intServiceType As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            litMessage.Text = ""

            lngBoxID = CCommon.ToLong(GetQueryStringVal("BoxID"))
            intServiceType = If(GetQueryStringVal("ServiceType") IsNot Nothing AndAlso GetQueryStringVal("ServiceType") <> "", GetQueryStringVal("ServiceType"), 0)

            If Not Page.IsPostBack Then
                If lngBoxID > 0 Then
                    BindShippingLabelList()
                Else
                    PrintShippingLabels()

                    If intServiceType = 91 Then
                        Page.Title = "Fedex Shipping Labels"
                    ElseIf intServiceType = 88 Then
                        Page.Title = "UPS Shipping Labels"
                    ElseIf intServiceType = 90 Then
                        Page.Title = "USPS Shipping Labels"
                    End If

                    ClientScript.RegisterStartupScript(Me.GetType, "Print Shipping Labels", "window.print();", True)
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub BindShippingLabelList()
        Try
            Dim objOppBizDocs As New OppBizDocs
            Dim ds As New DataSet
            objOppBizDocs.BoxID = lngBoxID
            objOppBizDocs.tintMode = 0
            ds = objOppBizDocs.GetShippingLabelList
            If ds.Tables(0).Rows.Count > 0 Then
                dlShipLabel.DataSource = ds.Tables(0)
                dlShipLabel.DataBind()
            Else
                litMessage.Text = "No Shipping Label Found"
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub PrintShippingLabels()
        Try
            Dim dsShippingLabels As New DataSet
            If Session("ShippingLabelImages") IsNot Nothing Then
                dsShippingLabels = Session("ShippingLabelImages")

                Dim dtShip As New DataTable
                Dim dvShip As New DataView
                dtShip = dsShippingLabels.Tables(0)

                dvShip = dtShip.DefaultView
                dvShip.RowFilter = "numShippingCompany = " & intServiceType
                dtShip = dvShip.ToTable()

                dlShipLabel.DataSource = dtShip
                dlShipLabel.DataBind()
            Else
                litMessage.Text = "Shipping Label Images not available."
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class