﻿Imports System.ServiceModel
Imports System.ServiceModel.Activation
Imports System.ServiceModel.Web
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Item
Imports Newtonsoft.Json
Imports BACRM.BusinessLogic.Admin

'Namespace BACRM.UserInterface.Opportunities

<ServiceContract(Namespace:="MassSalesFulfillmentService")>
<AspNetCompatibilityRequirements(RequirementsMode:=AspNetCompatibilityRequirementsMode.Allowed)>
Public Class MassSalesFulfillmentService

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetOrderStatus")>
    Public Function GetOrderStatus(ByVal oppType As Short) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objMassSalesFulfillment As New MassSalesFulfillment
                objMassSalesFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                Dim dt As DataTable = objMassSalesFulfillment.GetOrderStatus(oppType)

                Return JsonConvert.SerializeObject(dt, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="GET", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetShippingZone")>
    Public Function GetShippingZone() As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objCommon As New CCommon
                Dim dt As DataTable = objCommon.GetMasterListItems(834, CCommon.ToLong(HttpContext.Current.Session("DomainID")))

                Return JsonConvert.SerializeObject(dt, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="GET", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetCountries")>
    Public Function GetCountries() As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objCommon As New CCommon
                Dim dt As DataTable = objCommon.GetMasterListItems(40, CCommon.ToLong(HttpContext.Current.Session("DomainID")))

                Return JsonConvert.SerializeObject(dt, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetStates")>
    Public Function GetStates(ByVal countryID As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objUserAccess.Country = countryID
                Return JsonConvert.SerializeObject(objUserAccess.SelState(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetFilterValues")>
    Public Function GetFilterValues(ByVal filterBy As Short) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim dt As DataTable = Nothing

                If filterBy = 1 Then
                    Dim objCommon As New CCommon
                    dt = objCommon.GetMasterListItems(36, CCommon.ToLong(HttpContext.Current.Session("DomainID")))
                End If

                Return JsonConvert.SerializeObject(dt, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="GET", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetWarehouses")>
    Public Function GetWarehouses() As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objMassSalesFulfillment As New MassSalesFulfillment
                objMassSalesFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                Dim dt As DataTable = objMassSalesFulfillment.GetWarehouses()

                Return JsonConvert.SerializeObject(dt, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="GET", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetShipVia")>
    Public Function GetShipVia() As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objCommon As New CCommon
                Dim dt As DataTable = objCommon.GetMasterListItems(82, CCommon.ToLong(HttpContext.Current.Session("DomainID")))

                Return JsonConvert.SerializeObject(dt, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetRecords")>
    Public Function GetRecords(ByVal viewID As Short, ByVal warehouseID As Long, ByVal isGroupByOrder As Boolean, ByVal isRemoveBO As Boolean, ByVal shippingZone As Long, ByVal isIncludeSearch As Boolean, ByVal selectedOrderStatus As String, ByVal filterBy As Short, ByVal selectedFilterValue As String, ByVal customSearchValue As String, ByVal pageIndex As Integer, ByVal sortColumn As String, ByVal sortOrder As String, ByVal batchID As Long, ByVal packingViewMode As Short, ByVal printBizDocViewMode As Short, ByVal pendingCloseFilter As Short) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objMassSalesFulfillment As New MassSalesFulfillment
                objMassSalesFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objMassSalesFulfillment.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objMassSalesFulfillment.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                objMassSalesFulfillment.ViewID = viewID
                objMassSalesFulfillment.WarehouseID = warehouseID
                objMassSalesFulfillment.IsGroupByOrder = isGroupByOrder
                objMassSalesFulfillment.IsRemoveBO = isRemoveBO
                objMassSalesFulfillment.ShippingZone = shippingZone
                objMassSalesFulfillment.IsIncludeSearch = isIncludeSearch
                objMassSalesFulfillment.SelectedOrderStaus = selectedOrderStatus
                objMassSalesFulfillment.FilterBy = filterBy
                objMassSalesFulfillment.SelectedFilterValue = selectedFilterValue
                objMassSalesFulfillment.CustomSearchValue = customSearchValue
                objMassSalesFulfillment.PageIndex = pageIndex
                objMassSalesFulfillment.SortColumn = sortColumn
                objMassSalesFulfillment.SortOrder = sortOrder
                objMassSalesFulfillment.BatchID = batchID
                objMassSalesFulfillment.PackingViewMode = packingViewMode
                objMassSalesFulfillment.PrintBizDocViewMode = printBizDocViewMode
                objMassSalesFulfillment.PendingCloseFilter = pendingCloseFilter

                Dim ds As DataSet = objMassSalesFulfillment.GetRecords()

                Return JsonConvert.SerializeObject(New With {Key .Records = JsonConvert.SerializeObject(ds.Tables(0), Formatting.None), Key .LeftPaneFields = JsonConvert.SerializeObject(ds.Tables(1), Formatting.None), Key .TotalRecords = objMassSalesFulfillment.TotalRecords, Key .SortColumn = objMassSalesFulfillment.SortColumn, Key .SortOrder = objMassSalesFulfillment.SortOrder}, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED") Then
                Dim objMSException As New MSFException
                objMSException.ErrorMessage = "DEFAULT_SHIPPING_BIZDOC_NOT_CONFIGURED"
                objMSException.ErrorCode = 500

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Dim objMSException As New MSFException
                objMSException.ErrorMessage = ex.Message
                objMSException.ErrorCode = 500

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            End If
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="GET", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetMassSalesFulfillmentConfiguration")>
    Public Function GetMassSalesFulfillmentConfiguration() As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objMassSalesFulfillment As New MassSalesFulfillment
                objMassSalesFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objMassSalesFulfillment.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                Dim dt As DataTable = objMassSalesFulfillment.GetConfiguration()

                Return JsonConvert.SerializeObject(dt, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="GET", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetBatches")>
    Public Function GetBatches() As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objMassSalesFulfillmentBatch As New MassSalesFulfillmentBatch
                objMassSalesFulfillmentBatch.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                Dim dt As DataTable = objMassSalesFulfillmentBatch.GetAll()

                Return JsonConvert.SerializeObject(dt, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/AddRecordsToBatch")>
    Public Function AddRecordsToBatch(ByVal batchID As Long, ByVal selectedRecords As String)
        Try
            Dim objMassSalesFulfillmentBatch As New MassSalesFulfillmentBatch
            objMassSalesFulfillmentBatch.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
            objMassSalesFulfillmentBatch.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
            objMassSalesFulfillmentBatch.Mode = 1
            objMassSalesFulfillmentBatch.BatchID = batchID
            objMassSalesFulfillmentBatch.BatchRecords = selectedRecords
            objMassSalesFulfillmentBatch.Save()
        Catch ex As Exception
            If ex.Message.Contains("INVALID_BATCH") Then
                Dim objMSException As New MSFException
                objMSException.ErrorMessage = "INVALID_BATCH"
                objMSException.ErrorCode = 500

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                Dim objMSException As New MSFException
                objMSException.ErrorMessage = ex.Message
                objMSException.ErrorCode = 500

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            End If
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/CreateNewBatchAndRecordsToBatch")>
    Public Function CreateNewBatchAndRecordsToBatch(ByVal batchName As String, ByVal selectedRecords As String)
        Try
            Dim objMassSalesFulfillmentBatch As New MassSalesFulfillmentBatch
            objMassSalesFulfillmentBatch.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
            objMassSalesFulfillmentBatch.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
            objMassSalesFulfillmentBatch.Mode = 2
            objMassSalesFulfillmentBatch.BatchName = batchName
            objMassSalesFulfillmentBatch.BatchRecords = selectedRecords
            objMassSalesFulfillmentBatch.Save()
        Catch ex As Exception
            If ex.Message.Contains("MAX_ACTIVE_BATCH_LIMIT_EXCEED") Then
                Dim objMSException As New MSFException
                objMSException.ErrorMessage = "MAX_ACTIVE_BATCH_LIMIT_EXCEED"
                objMSException.ErrorCode = 500

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            ElseIf ex.Message.Contains("BATCH_WITH_SAME_NAME_EXISTS") Then
                Dim objMSException As New MSFException
                objMSException.ErrorMessage = "BATCH_WITH_SAME_NAME_EXISTS"
                objMSException.ErrorCode = 500

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)

                Dim objMSException As New MSFException
                objMSException.ErrorMessage = ex.Message
                objMSException.ErrorCode = 500

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            End If

        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/RemoveRecordsFromBatch")>
    Public Function RemoveRecordsFromBatch(ByVal batchID As Long, ByVal selectedRecords As String)
        Try
            Dim objMassSalesFulfillmentBatch As New MassSalesFulfillmentBatch
            objMassSalesFulfillmentBatch.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
            objMassSalesFulfillmentBatch.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
            objMassSalesFulfillmentBatch.Mode = 3
            objMassSalesFulfillmentBatch.BatchID = batchID
            objMassSalesFulfillmentBatch.BatchRecords = selectedRecords
            objMassSalesFulfillmentBatch.Save()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/MarkBatchAsComplete")>
    Public Function MarkBatchAsComplete(ByVal batchID As Long)
        Try
            Dim objMassSalesFulfillmentBatch As New MassSalesFulfillmentBatch
            objMassSalesFulfillmentBatch.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
            objMassSalesFulfillmentBatch.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
            objMassSalesFulfillmentBatch.Mode = 4
            objMassSalesFulfillmentBatch.BatchID = batchID
            objMassSalesFulfillmentBatch.Save()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetRecordsForPicking")>
    Public Function GetRecordsForPicking(ByVal isGroupByOrder As Boolean, ByVal batchID As Long, ByVal selectedRecords As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objMassSalesFulfillment As New MassSalesFulfillment
                objMassSalesFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objMassSalesFulfillment.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objMassSalesFulfillment.IsGroupByOrder = isGroupByOrder
                objMassSalesFulfillment.BatchID = batchID
                objMassSalesFulfillment.SelectedRecords = selectedRecords

                Dim ds As DataSet = objMassSalesFulfillment.GetRecordsForPicking()

                Return JsonConvert.SerializeObject(New With {Key .Records = JsonConvert.SerializeObject(ds.Tables(0), Formatting.None), Key .RightPaneFields = JsonConvert.SerializeObject(ds.Tables(1), Formatting.None)}, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/PrintPickList")>
    Public Function PrintPickList(ByVal isPickListByOrder As Boolean, ByVal batchName As String, ByVal oppItems As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim table As DataTable = JsonConvert.DeserializeObject(Of DataTable)(oppItems)

                Dim objHtmlToPDF As New HTMLToPDF
                Dim bytest As Byte() = objHtmlToPDF.CreatePickListPDFMassSalesFulfillment(isPickListByOrder, batchName, CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset")), table)
                Return Convert.ToBase64String(bytest, 0, bytest.Length)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    '<OperationContract()>
    '<WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/PrintBizDoc")>
    'Public Function PrintBizDoc(ByVal oppID As Long, ByVal oppBizDocID As Long, ByVal bizDocName As String) As String
    '    Try
    '        If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
    '            Dim objHtmlToPDF As New HTMLToPDF
    '            Dim bytest As Byte() = objHtmlToPDF.CreateBizDocPDF(oppID, oppBizDocID, CCommon.ToLong(HttpContext.Current.Session("DomainID")), CCommon.ToLong(HttpContext.Current.Session("UserContactID")), CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset")), CCommon.ToString(HttpContext.Current.Session("DateFormat")))

    '            Return JsonConvert.SerializeObject(New With {Key .bizDocName = bizDocName, Key .bizDocBytes = Convert.ToBase64String(bytest, 0, bytest.Length)}, Formatting.None)
    '        Else
    ''            Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
    '        End If
    '    Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
    '        Throw
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
    '        Dim objMSException As New MSFException
    '        objMSException.ErrorMessage = ex.Message
    '        objMSException.ErrorCode = 500

    '        Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
    '    End Try
    'End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/PrintBizDoc")>
    Public Function PrintBizDoc(ByVal selectdRecords As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim dt As DataTable = JsonConvert.DeserializeObject(Of DataTable)(selectdRecords)
                Dim objHtmlToPDF As New HTMLToPDF
                Dim bytest As Byte() = objHtmlToPDF.PrintBizDocMassSalesFulfillment(dt, CCommon.ToLong(HttpContext.Current.Session("DomainID")), CCommon.ToLong(HttpContext.Current.Session("UserContactID")), CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset")), CCommon.ToString(HttpContext.Current.Session("DateFormat")))

                Return JsonConvert.SerializeObject(New With {Key .bizDocName = "", Key .bizDocBytes = Convert.ToBase64String(bytest, 0, bytest.Length)}, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/UpdateItemsPickedQty")>
    Public Function UpdateItemsPickedQty(ByVal oppItems As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim dt As DataTable = JsonConvert.DeserializeObject(Of DataTable)(oppItems)
                Dim xmlString As String = "<NewDataSet>"
                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    For Each dr As DataRow In dt.Rows
                        xmlString += "<Table1>"
                        xmlString += "<numPickedQty>" & CCommon.ToString(dr("numPickedQty")) & "</numPickedQty>"
                        xmlString += "<WarehouseItemIDs>" & CCommon.ToString(dr("WarehouseItemIDs")) & "</WarehouseItemIDs>"
                        xmlString += "<vcOppItems>" & CCommon.ToString(dr("vcOppItems")) & "</vcOppItems>"
                        xmlString += "<numOppChildItemID>" & CCommon.ToLong(dr("numOppChildItemID")) & "</numOppChildItemID>"
                        xmlString += "<numOppKitChildItemID>" & CCommon.ToLong(dr("numOppKitChildItemID")) & "</numOppKitChildItemID>"
                        xmlString += "</Table1>"
                    Next
                End If
                xmlString += "</NewDataSet>"

                Dim objMassSalesFulfillment As New MassSalesFulfillment
                objMassSalesFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objMassSalesFulfillment.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                objMassSalesFulfillment.SelectedRecords = xmlString
                objMassSalesFulfillment.ClientTimeZoneOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                Return JsonConvert.SerializeObject(objMassSalesFulfillment.UpdateItemsPickedQty(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("INVALID_SERIALLOT_FORMAT") Then
                Dim objMSException As New MSFException
                objMSException.ErrorMessage = "Serial/Lot # are not provied in proper format. (must be for serial quantity 2 as abc,xyz and for lot quanity 2 as abc(1),xyz(1))."
                objMSException.ErrorCode = 500

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            ElseIf ex.Message.Contains("SUFFICIENT_SERIALLOT_QTY_NOT_PICKED") Then
                Dim objMSException As New MSFException
                objMSException.ErrorMessage = "Serial/Lot # pircked are not same as quantity picked."
                objMSException.ErrorCode = 500

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            ElseIf ex.Message.Contains("INVLAID_SERIALLOT_QTY") Then
                Dim objMSException As New MSFException
                objMSException.ErrorMessage = "Serial/Lot # are not exists in biz or don't have enough quantity."
                objMSException.ErrorCode = 500

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            ElseIf ex.Message.Contains("PICKED_QTY_IS_GREATER_THEN_REMAINING_QTY") Then
                Dim objMSException As New MSFException
                objMSException.ErrorMessage = "PICKED_QTY_IS_GREATER_THEN_REMAINING_QTY"
                objMSException.ErrorCode = 500

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)

                Dim objMSException As New MSFException
                objMSException.ErrorMessage = ex.Message
                objMSException.ErrorCode = 500

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            End If
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="GET", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetShippingConfiguration")>
    Public Function GetShippingConfiguration() As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objShippingFieldValues As New BACRM.BusinessLogic.Admin.ShippingFieldValues()
                objShippingFieldValues.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))

                Return JsonConvert.SerializeObject(objShippingFieldValues.GetAllByDomain(), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetShippingRate")>
    Public Function GetShippingRate(ByVal selectedRecords As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim dt As DataTable = JsonConvert.DeserializeObject(Of DataTable)(selectedRecords)
                Dim domainID As Long = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    System.Threading.Tasks.Parallel.ForEach(dt.AsEnumerable(), Sub(dr)
                                                                                   GetOrderRate(domainID, dr)
                                                                               End Sub)
                End If


                dt.AcceptChanges()
                Return JsonConvert.SerializeObject(dt, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)

            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500
            objMSException.OppID = 0

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function


    Private Sub GetOrderRate(ByVal domainID As Long, ByRef dr As DataRow)
        Try
            Dim objShipping As New ShippingCalculation
            Dim objResult As Tuple(Of Double, Integer, String, Boolean, String) = objShipping.MSCalculateOrderShippingRate(domainID, CCommon.ToLong(dr("OppID")), CCommon.ToLong(dr("WarehouseID")), If(String.IsNullOrEmpty(CCommon.ToString(dr("ShipByDate"))), Nothing, Convert.ToDateTime(dr("ShipByDate")).Date))

            If Not objResult Is Nothing Then
                dr("Rate") = objResult.Item1
                dr("TransitTime") = objResult.Item2
                dr("Description") = objResult.Item3
                dr("IsRateFetchedSuccessfully") = objResult.Item4
                dr("ErrorMessage") = objResult.Item5
            Else
                dr("Rate") = 0
                dr("TransitTime") = 0
                dr("Description") = ""
                dr("IsRateFetchedSuccessfully") = False
                dr("ErrorMessage") = ""
            End If
        Catch ex As Exception
            dr("Rate") = 0
            dr("TransitTime") = 0
            dr("Description") = ""
            dr("IsRateFetchedSuccessfully") = False
            dr("ErrorMessage") = ex.Message
        End Try
    End Sub

    '<OperationContract()>
    '<WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetShippingRate")>
    'Public Function GetShippingRate(ByVal oppID As Long, ByVal warehouseID As Long, ByVal shipByDate As String) As String
    '    Try
    '        If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
    '            Dim objShipping As New Shipping
    '            Dim objResult As Tuple(Of Double, Integer, String, Boolean, String) = objShipping.MSCalculateOrderShippingRate(CCommon.ToLong(HttpContext.Current.Session("DomainID")), oppID, warehouseID, shipByDate)

    '            If Not objResult Is Nothing Then
    '                Dim shippingRate = New With {Key .OppID = oppID, .ShipByDate = shipByDate, .Rate = objResult.Item1, .TransitTime = objResult.Item2, .Description = objResult.Item3, .IsRateFetchedSuccessfully = objResult.Item4, .ErrorMessage = objResult.Item5}
    '                Return JsonConvert.SerializeObject(shippingRate, Formatting.None)
    '            Else
    '                Dim shippingRate = New With {Key .OppID = oppID, .ShipByDate = shipByDate, .Rate = 0, .TransitTime = 0, .Description = "", .IsRateFetchedSuccessfully = False, .ErrorMessage = ""}
    '                Return JsonConvert.SerializeObject(shippingRate, Formatting.None)
    '            End If
    '        Else
    '            Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
    '        End If
    '    Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
    '        Throw
    '    Catch exShip As nsoftware.InShip.InShipException
    '        Dim objMSException As New MSFException
    '        objMSException.ErrorMessage = exShip.Code & " - " & exShip.Message
    '        objMSException.ErrorCode = 500
    '        objMSException.OppID = oppID
    '        objMSException.ShipByDate = shipByDate

    '        Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
    '    Catch ex As Exception
    '        If ex.Message.Contains("SHIP_FROM_ADDRESS_NOT_AVAILABLE") Then
    '            Dim objMSException As New MSFException
    '            objMSException.ErrorMessage = "SHIP_FROM_ADDRESS_NOT_AVAILABLE"
    '            objMSException.ErrorCode = 500
    '            objMSException.OppID = oppID

    '            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
    '        ElseIf ex.Message.Contains("SHIP_TO_FROM_ADDRESS_NOT_AVAILABLE") Then
    '            Dim objMSException As New MSFException
    '            objMSException.ErrorMessage = "SHIP_TO_FROM_ADDRESS_NOT_AVAILABLE"
    '            objMSException.ErrorCode = 500
    '            objMSException.OppID = oppID

    '            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
    '        Else
    '            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)

    '            Dim objMSException As New MSFException
    '            objMSException.ErrorMessage = ex.Message
    '            objMSException.ErrorCode = 500
    '            objMSException.OppID = oppID

    '            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
    '        End If
    '    End Try
    'End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/UpdateShippingRate")>
    Public Function UpdateShippingRate(ByVal orders As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim isSuccesss As Boolean = True
                Dim dt As DataTable = JsonConvert.DeserializeObject(Of DataTable)(orders)

                Dim objOpportunities As New MOpportunity
                objOpportunities.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))

                For Each dr As DataRow In dt.Rows
                    Try
                        objOpportunities.OpportunityId = CCommon.ToLong(dr("OppID"))
                        objOpportunities.ReleaseDate = If(String.IsNullOrEmpty(Convert.ToString(dr("ShipByDate"))), Nothing, Convert.ToDateTime(dr("ShipByDate")).Date)
                        objOpportunities.ShipFromLocation = CCommon.ToLong(dr("WarehouseID"))
                        objOpportunities.UnitPrice = CCommon.ToDouble(dr("ShipRate"))
                        objOpportunities.Desc = CCommon.ToString(dr("Description"))
                        objOpportunities.AddAutoCalculatedShippingRate()
                    Catch ex As Exception
                        isSuccesss = False

                        If ex.Message.Contains("SHIPPING_ITEM_NOT_SET") Then
                            Throw
                        End If
                    End Try
                Next

                Return JsonConvert.SerializeObject(New With {Key .isSuccess = isSuccesss}, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            If ex.Message.Contains("SHIPPING_ITEM_NOT_SET") Then
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)

                Dim objMSException As New MSFException
                objMSException.ErrorMessage = "SHIPPING_ITEM_NOT_SET"
                objMSException.ErrorCode = 500

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)

                Dim objMSException As New MSFException
                objMSException.ErrorMessage = ex.Message
                objMSException.ErrorCode = 500

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            End If
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/ShipOrders")>
    Public Function ShipOrders(ByVal orders As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim dt As DataTable = JsonConvert.DeserializeObject(Of DataTable)(orders)

                Dim objMassSalesFulfillment As New MassSalesFulfillment
                objMassSalesFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objMassSalesFulfillment.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))

                For Each dr As DataRow In dt.Rows
                    objMassSalesFulfillment.OppID = CCommon.ToLong(dr("OppID"))
                    objMassSalesFulfillment.SelectedRecords = CCommon.ToString(dr("OppItemIDs"))
                    Dim dtOrderItems As DataTable = objMassSalesFulfillment.GetOrderItems(1)

                    If Not dtOrderItems Is Nothing AndAlso dtOrderItems.Rows.Count > 0 Then
                        Try
                            Dim objCOpportunity As New COpportunities
                            objCOpportunity.CreateFulfillmentBizDocWithItems(CCommon.ToLong(HttpContext.Current.Session("DomainID")), CCommon.ToLong(HttpContext.Current.Session("UserContactID")), CCommon.ToLong(dr("OppID")), GetBizDocItems(dtOrderItems), CCommon.ToInteger(dr("ParentBizDocID")))

                            Try
                                objMassSalesFulfillment.UpdateOrderStauts(2, 2)
                            Catch ex As Exception
                                'DO NOT THROW ERROR
                            End Try

                            dr("IsSucess") = True
                            dr("ErrorMessage") = ""
                        Catch ex As Exception
                            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)

                            For Each drItem In dt.Rows
                                If CCommon.ToLong(drItem("OppID")) = CCommon.ToLong(dr("OppID")) Then
                                    drItem("IsSucess") = False

                                    If ex.Message.Contains("WORK_ORDER_NOT_COMPLETED") Then
                                        drItem("ErrorMessage") = "Not able to add fulfillment bizdoc because some work orders are still not completed."
                                    ElseIf ex.Message.Contains("QTY_MISMATCH") Then
                                        drItem("ErrorMessage") = "Not able to add fulfillment bizdoc because qty is not same as required qty."
                                    ElseIf ex.Message.Contains("REQUIRED_SERIALS_NOT_PROVIDED") Then
                                        drItem("ErrorMessage") = "Not able to add fulfillment bizdoc because required number of serials are not provided."
                                    ElseIf ex.Message.Contains("INVALID_SERIAL_NUMBERS") Then
                                        drItem("ErrorMessage") = "Not able to add fulfillment bizdoc because invalid serials are provided."
                                    ElseIf ex.Message.Contains("REQUIRED_LOTNO_NOT_PROVIDED") Then
                                        drItem("ErrorMessage") = "Not able to add fulfillment bizdoc because required number of Lots are not provided."
                                    ElseIf ex.Message.Contains("INVALID_LOT_NUMBERS") Then
                                        drItem("ErrorMessage") = "Not able to add fulfillment bizdoc because invalid Lots are provided."
                                    ElseIf ex.Message.Contains("SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY") Then
                                        drItem("ErrorMessage") = "Not able to add fulfillment bizdoc because Lot number do not have enough qty to fulfill."
                                    ElseIf ex.Message.Contains("NOTSUFFICIENTQTY_ALLOCATION") Then
                                        drItem("ErrorMessage") = "Not able to add fulfillment bizdoc because warehouse allocation is invlid."
                                    ElseIf ex.Message.Contains("NOTSUFFICIENTQTY_ONHAND") Then
                                        drItem("ErrorMessage") = "Not able to add fulfillment bizdoc because warehouse does not have enought on hand quantity."
                                    ElseIf ex.Message.Contains("BIZDOC_QTY_MORE_THEN_ORDERED_QTY") Then
                                        drItem("ErrorMessage") = "Not able to add fulfillment bizdoc because bizdoc qty is more than ordered quantity for some item(s)."
                                    Else
                                        drItem("ErrorMessage") = "Not able to add fulfillment order due to unknown error."
                                    End If
                                End If
                            Next
                        End Try
                    Else
                        dr("IsSucess") = False
                        dr("ErrorMessage") = "No item(s) left to fulfilled"
                    End If
                Next

                dt.AcceptChanges()

                Return JsonConvert.SerializeObject(dt, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)

            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    Private Function GetBizDocItems(ByVal dtOrderedItems As DataTable) As String
        Try
            Dim dtBizDocItems As New DataTable
            Dim strOppBizDocItems As String

            dtBizDocItems.TableName = "BizDocItems"
            dtBizDocItems.Columns.Add("OppItemID")
            dtBizDocItems.Columns.Add("Quantity")
            dtBizDocItems.Columns.Add("Notes")
            dtBizDocItems.Columns.Add("monPrice")

            Dim dr As DataRow
            'Dim BizCalendar As UserControl
            Dim strDeliveryDate As String

            Dim CalRentalStartDate As UserControl
            Dim CalRentalReturnDate As UserControl

            Dim RentalStartDate, RentalReturnDate As DateTime

            For Each drItem As DataRow In dtOrderedItems.Rows
                dr = dtBizDocItems.NewRow
                dr("OppItemID") = CCommon.ToLong(drItem("numoppitemtCode"))
                dr("Notes") = ""
                dr("Quantity") = CCommon.ToDouble(drItem("numUnitHour"))
                dr("monPrice") = CCommon.ToDecimal(drItem("monPrice"))
                dtBizDocItems.Rows.Add(dr)
            Next

            Dim dsNew As New DataSet
            dsNew.Tables.Add(dtBizDocItems)
            strOppBizDocItems = dsNew.GetXml
            dsNew.Tables.Remove(dsNew.Tables(0))
            Return strOppBizDocItems
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetRecordsForPacking")>
    Public Function GetRecordsForPacking(ByVal oppID As Long, ByVal oppItemID As Long, ByVal oppBizDocID As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                If oppID > 0 AndAlso oppBizDocID > 0 Then
                    Dim objMassSalesFulfillment As New MassSalesFulfillment
                    objMassSalesFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                    objMassSalesFulfillment.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                    objMassSalesFulfillment.OppID = oppID
                    objMassSalesFulfillment.OppItemID = oppItemID
                    objMassSalesFulfillment.OppBizDocID = oppBizDocID
                    Dim ds As DataSet = objMassSalesFulfillment.GetShippingBizDocForPacking()

                    If ds.Tables.Count > 1 Then
                        Return JsonConvert.SerializeObject(New With {Key .Boxes = JsonConvert.SerializeObject(ds.Tables(0), Formatting.None), Key .BoxItems = JsonConvert.SerializeObject(ds.Tables(1), Formatting.None), Key .BizDocItems = "[]"}, Formatting.None)
                    Else
                        Return JsonConvert.SerializeObject(New With {Key .Boxes = "[]", Key .BoxItems = "[]", Key .BizDocItems = JsonConvert.SerializeObject(ds.Tables(0), Formatting.None)}, Formatting.None)
                    End If
                Else
                    Dim objMassSalesFulfillment As New MassSalesFulfillment
                    objMassSalesFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                    objMassSalesFulfillment.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                    objMassSalesFulfillment.OppID = oppID
                    objMassSalesFulfillment.OppItemID = oppItemID
                    Return JsonConvert.SerializeObject(objMassSalesFulfillment.GetRecordsForPacking(), Formatting.None)
                End If
                
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="GET", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetContainers")>
    Public Function GetContainers() As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim dsPackages As DataSet = OppBizDocs.LoadAllPackages(CCommon.ToLong(HttpContext.Current.Session("DomainID")))
                Return JsonConvert.SerializeObject(dsPackages.Tables(0), Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GenerateShippingLabel")>
    Public Function GenerateShippingLabel(ByVal oppID As Long, ByVal oppBizDocID As Long, ByVal shippingReportID As Long, ByVal shipViaID As Long, ByVal shipServiceID As Long, ByVal shippingDetail As String, ByVal bizDocType As Long, ByVal items As String, ByVal boxes As String, ByVal boxItems As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                If shippingReportID > 0 Then
                    Dim vcShippingDetail As String = ""

                    If shippingDetail.Length > 0 Then
                        Dim dsShippingDetail As New DataSet
                        Dim dtShippingDetail As DataTable = JsonConvert.DeserializeObject(Of DataTable)(shippingDetail)
                        dsShippingDetail.Tables.Add(dtShippingDetail)
                        dsShippingDetail.AcceptChanges()

                        vcShippingDetail = dsShippingDetail.GetXml()

                        Dim objShippingReport As New BACRM.BusinessLogic.ItemShipping.ShippingReport
                        objShippingReport.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                        objShippingReport.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                        objShippingReport.OppID = oppID
                        objShippingReport.ShippingReportID = shippingReportID
                        objShippingReport.UpdateShippingReportMassSalesFilfillment(vcShippingDetail)
                    End If

                    Dim objShipping As New Shipping
                    objShipping.GenerateShippingLabel(oppID, oppBizDocID, shippingReportID, CCommon.ToLong(HttpContext.Current.Session("DomainID")), CCommon.ToLong(HttpContext.Current.Session("UserContactID")))

                    If CCommon.ToString(objShipping.ErrorMsg).Trim.Length > 0 Then
                        Throw New Exception(CCommon.ToString(objShipping.ErrorMsg))
                    Else
                        Try
                            Dim objMassSalesFulfillment As New MassSalesFulfillment
                            objMassSalesFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                            objMassSalesFulfillment.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                            objMassSalesFulfillment.OppID = oppID
                            objMassSalesFulfillment.UpdateOrderStauts(2, 4)
                        Catch ex As Exception
                            'DO NOT THROW ERROR
                        End Try
                    End If

                    Return JsonConvert.SerializeObject(New With {Key .isSuccess = True, .oppID = oppID, .oppBizDocID = oppBizDocID}, Formatting.None)
                ElseIf oppBizDocID > 0 Then
                    Dim dsBoxes As New DataSet
                    Dim dtBoxes As DataTable = JsonConvert.DeserializeObject(Of DataTable)(boxes)
                    dsBoxes.Tables.Add(dtBoxes)
                    dsBoxes.AcceptChanges()

                    Dim dsBoxItems As New DataSet
                    Dim dtBoxItems As DataTable = JsonConvert.DeserializeObject(Of DataTable)(boxItems)
                    dsBoxItems.Tables.Add(dtBoxItems)
                    dsBoxItems.AcceptChanges()

                    Dim vcShippingDetail As String = ""

                    If shippingDetail.Length > 0 Then
                        Dim dsShippingDetail As New DataSet
                        Dim dtShippingDetail As DataTable = JsonConvert.DeserializeObject(Of DataTable)(shippingDetail)
                        dsShippingDetail.Tables.Add(dtShippingDetail)
                        dsShippingDetail.AcceptChanges()

                        vcShippingDetail = dsShippingDetail.GetXml()
                    End If

                    Dim objOpportunity As New COpportunities
                    objOpportunity.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                    objOpportunity.OpportID = oppID
                    Dim dtOrder As DataTable = objOpportunity.GetOrderDetailForBizDoc()

                    If Not dtOrder Is Nothing AndAlso dtOrder.Rows.Count > 0 Then
                        Dim objOppBizDocs As New OppBizDocs
                        objOppBizDocs.OppBizDocId = oppBizDocID
                        objOppBizDocs.OppId = oppID
                        objOppBizDocs.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                        objOppBizDocs.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                        objOppBizDocs.ClientTimeZoneOffset = CCommon.ToLong(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                        Dim dtOppBiDocDtl As DataTable = objOppBizDocs.GetOppBizDocDtl

                        Dim objOpportunityBizDocs As New OpportunityBizDocs
                        objOpportunityBizDocs.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                        objOpportunityBizDocs.OppID = oppID
                        objOpportunityBizDocs.OppBizDocID = oppBizDocID
                        objOpportunityBizDocs.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                        objOpportunityBizDocs.ClientMachineUTCTimeOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                        objOpportunityBizDocs.DivisionID = CCommon.ToLong(dtOrder.Rows(0)("numDivisionID"))
                        objOpportunityBizDocs.CompanyName = CCommon.ToString(dtOrder.Rows(0)("vcCompanyName"))
                        objOpportunityBizDocs.OppType = CCommon.ToShort(dtOrder.Rows(0)("tintOppType"))

                        If Not dtOppBiDocDtl Is Nothing AndAlso dtOppBiDocDtl.Rows.Count > 0 Then
                            objOpportunityBizDocs.Comments = CCommon.ToString(dtOppBiDocDtl.Rows(0)("vcComments"))
                            objOpportunityBizDocs.ShipVia = shipViaID
                            objOpportunityBizDocs.TrackingURL = ""
                            objOpportunityBizDocs.BizDocType = bizDocType
                            If Not IsDBNull(dtOppBiDocDtl.Rows(0)("dtFromDate")) Then
                                objOpportunityBizDocs.FromDate = Convert.ToDateTime(dtOppBiDocDtl.Rows(0)("dtFromDate")).ToShortDateString()
                            End If
                            objOpportunityBizDocs.BizDocStatus = CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numBizDocStatus"))
                            objOpportunityBizDocs.BizDocSequenceId = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("numSequenceId"))
                            objOpportunityBizDocs.BizDocTemplateID = CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numBizDocTempID"))
                            objOpportunityBizDocs.TrackingNo = CCommon.ToString(dtOppBiDocDtl.Rows(0)("vcTrackingNo"))
                            objOpportunityBizDocs.RefOrderNo = CCommon.ToString(dtOppBiDocDtl.Rows(0)("vcRefOrderNo"))
                            objOpportunityBizDocs.ExchangeRate = CCommon.ToDouble(dtOppBiDocDtl.Rows(0)("fltExchangeRateBizDoc"))
                            objOpportunityBizDocs.VendorInvoiceName = CCommon.ToString(dtOppBiDocDtl.Rows(0)("vcVendorInvoice"))
                            objOpportunityBizDocs.ARAccountID = CCommon.ToLong(dtOppBiDocDtl.Rows(0)("numARAccountID"))


                            Dim dr As DataRow
                            Dim dtBizDocItems As New DataTable
                            Dim strOppBizDocItems As String = ""

                            dtBizDocItems.TableName = "BizDocItems"
                            dtBizDocItems.Columns.Add("OppItemID")
                            dtBizDocItems.Columns.Add("Quantity")
                            dtBizDocItems.Columns.Add("Notes")
                            dtBizDocItems.Columns.Add("monPrice")

                            Dim dtOrderedItems As DataTable = JsonConvert.DeserializeObject(Of DataTable)(items)
                            For Each drItem As DataRow In dtOrderedItems.Rows
                                dr = dtBizDocItems.NewRow
                                dr("OppItemID") = CCommon.ToLong(drItem("OppItemID"))
                                dr("Notes") = ""
                                dr("Quantity") = CCommon.ToDouble(drItem("Quantity"))
                                dr("monPrice") = 0 'In case of sales order no need to provide value
                                dtBizDocItems.Rows.Add(dr)
                            Next

                            Dim shippingBizDocID As Long
                            Dim dsNew As New DataSet
                            dsNew.Tables.Add(dtBizDocItems)
                            strOppBizDocItems = dsNew.GetXml
                            dsNew.Tables.Remove(dsNew.Tables(0))

                            objOpportunityBizDocs.BizDocItems = strOppBizDocItems


                            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                objOpportunityBizDocs.CreateBizDoc(False)

                                Dim objShippingReport As New BACRM.BusinessLogic.ItemShipping.ShippingReport
                                objShippingReport.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                                objShippingReport.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                                objShippingReport.OppID = oppID
                                objShippingReport.OppBizDocId = oppBizDocID
                                objShippingReport.ShipVia = shipViaID
                                objShippingReport.ShipService = shipServiceID
                                Dim lngShippingReportID As Long = objShippingReport.SaveMassSalesFilfillment(dsBoxes.GetXml(), dsBoxItems.GetXml(), vcShippingDetail)

                                Dim objShipping As New Shipping
                                objShipping.GenerateShippingLabel(oppID, oppBizDocID, lngShippingReportID, CCommon.ToLong(HttpContext.Current.Session("DomainID")), CCommon.ToLong(HttpContext.Current.Session("UserContactID")))

                                If CCommon.ToString(objShipping.ErrorMsg).Trim.Length > 0 Then
                                    Throw New Exception(CCommon.ToString(objShipping.ErrorMsg))
                                Else
                                    Try
                                        Dim objMassSalesFulfillment As New MassSalesFulfillment
                                        objMassSalesFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                                        objMassSalesFulfillment.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                                        objMassSalesFulfillment.OppID = oppID
                                        objMassSalesFulfillment.UpdateOrderStauts(2, 4)
                                    Catch ex As Exception
                                        'DO NOT THROW ERROR
                                    End Try
                                End If

                                objTransactionScope.Complete()
                            End Using

                            Return JsonConvert.SerializeObject(New With {Key .isSuccess = True, .oppID = oppID, .oppBizDocID = oppBizDocID}, Formatting.None)
                        Else
                            Dim objMSException As New MSFException
                            objMSException.ErrorMessage = "BizDoc doesn't exists."
                            objMSException.ErrorCode = 500

                            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
                        End If
                    Else
                        Dim objMSException As New MSFException
                        objMSException.ErrorMessage = "Order doesn't exists."
                        objMSException.ErrorCode = 500

                        Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
                    End If
                Else
                    Dim dtBizDocItems As DataTable
                    Dim dr As DataRow
                    Dim strOppBizDocItems As String

                    Dim objOpportunity As New COpportunities
                    objOpportunity.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                    objOpportunity.OpportID = oppID
                    Dim dtOrder As DataTable = objOpportunity.GetOrderDetailForBizDoc()

                    If Not dtOrder Is Nothing AndAlso dtOrder.Rows.Count > 0 Then
                        Dim dsBoxes As New DataSet
                        Dim dtBoxes As DataTable = JsonConvert.DeserializeObject(Of DataTable)(boxes)
                        dsBoxes.Tables.Add(dtBoxes)
                        dsBoxes.AcceptChanges()

                        Dim dsBoxItems As New DataSet
                        Dim dtBoxItems As DataTable = JsonConvert.DeserializeObject(Of DataTable)(boxItems)
                        dsBoxItems.Tables.Add(dtBoxItems)
                        dsBoxItems.AcceptChanges()

                        Dim vcShippingDetail As String = ""

                        If shippingDetail.Length > 0 Then
                            Dim dsShippingDetail As New DataSet
                            Dim dtShippingDetail As DataTable = JsonConvert.DeserializeObject(Of DataTable)(shippingDetail)
                            dsShippingDetail.Tables.Add(dtShippingDetail)
                            dsShippingDetail.AcceptChanges()

                            vcShippingDetail = dsShippingDetail.GetXml()
                        End If


                        Dim objOpportunityBizDocs As New OpportunityBizDocs
                        objOpportunityBizDocs.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                        objOpportunityBizDocs.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                        objOpportunityBizDocs.ClientMachineUTCTimeOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                        objOpportunityBizDocs.DivisionID = CCommon.ToLong(dtOrder.Rows(0)("numDivisionID"))
                        objOpportunityBizDocs.CompanyName = CCommon.ToString(dtOrder.Rows(0)("vcCompanyName"))
                        objOpportunityBizDocs.OppID = oppID
                        objOpportunityBizDocs.OppType = CCommon.ToShort(dtOrder.Rows(0)("tintOppType"))
                        objOpportunityBizDocs.ShipVia = shipViaID
                        objOpportunityBizDocs.BizDocType = bizDocType
                        objOpportunityBizDocs.IsDeferred = False

                        dtBizDocItems = New DataTable
                        strOppBizDocItems = ""

                        dtBizDocItems.TableName = "BizDocItems"
                        dtBizDocItems.Columns.Add("OppItemID")
                        dtBizDocItems.Columns.Add("Quantity")
                        dtBizDocItems.Columns.Add("Notes")
                        dtBizDocItems.Columns.Add("monPrice")

                        Dim dtOrderedItems As DataTable = JsonConvert.DeserializeObject(Of DataTable)(items)
                        For Each drItem As DataRow In dtOrderedItems.Rows
                            dr = dtBizDocItems.NewRow
                            dr("OppItemID") = CCommon.ToLong(drItem("OppItemID"))
                            dr("Notes") = ""
                            dr("Quantity") = CCommon.ToDouble(drItem("Quantity"))
                            dr("monPrice") = 0 'In case of sales order no need to provide value
                            dtBizDocItems.Rows.Add(dr)
                        Next

                        Dim shippingBizDocID As Long
                        Dim dsNew As New DataSet
                        dsNew.Tables.Add(dtBizDocItems)
                        strOppBizDocItems = dsNew.GetXml
                        dsNew.Tables.Remove(dsNew.Tables(0))

                        objOpportunityBizDocs.BizDocItems = strOppBizDocItems
                        objOpportunityBizDocs.IsEnableDeferredIncome = CCommon.ToBool(HttpContext.Current.Session("IsEnableDeferredIncome"))
                        objOpportunityBizDocs.IsAutolinkUnappliedPayment = CCommon.ToBool(HttpContext.Current.Session("AutolinkUnappliedPayment"))
                        objOpportunityBizDocs.ExchangeRate = CCommon.ToDouble(HttpContext.Current.Session("fltExchangeRate"))

                        Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                            shippingBizDocID = objOpportunityBizDocs.CreateBizDoc(False)

                            If bizDocType <> 296 Then
                                Dim objOpportunityBizDocsTemp As New OpportunityBizDocs
                                objOpportunityBizDocsTemp = objOpportunityBizDocs
                                objOpportunityBizDocsTemp.BizDocType = 296
                                objOpportunityBizDocsTemp.OppBizDocID = 0
                                objOpportunityBizDocsTemp.SourceBizDocID = shippingBizDocID
                                objOpportunityBizDocsTemp.CreateBizDoc(False)
                            End If

                            Dim objShippingReport As New BACRM.BusinessLogic.ItemShipping.ShippingReport
                            objShippingReport.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                            objShippingReport.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                            objShippingReport.OppID = oppID
                            objShippingReport.OppBizDocId = shippingBizDocID
                            objShippingReport.ShipVia = shipViaID
                            objShippingReport.ShipService = shipServiceID
                            Dim lngShippingReportID As Long = objShippingReport.SaveMassSalesFilfillment(dsBoxes.GetXml(), dsBoxItems.GetXml(), vcShippingDetail)

                            Dim objShipping As New Shipping
                            objShipping.GenerateShippingLabel(oppID, shippingBizDocID, lngShippingReportID, CCommon.ToLong(HttpContext.Current.Session("DomainID")), CCommon.ToLong(HttpContext.Current.Session("UserContactID")))

                            If CCommon.ToString(objShipping.ErrorMsg).Trim.Length > 0 Then
                                Throw New Exception(CCommon.ToString(objShipping.ErrorMsg))
                            Else
                                Try
                                    Dim objMassSalesFulfillment As New MassSalesFulfillment
                                    objMassSalesFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                                    objMassSalesFulfillment.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                                    objMassSalesFulfillment.OppID = oppID
                                    objMassSalesFulfillment.UpdateOrderStauts(2, 4)
                                Catch ex As Exception
                                    'DO NOT THROW ERROR
                                End Try
                            End If

                            objTransactionScope.Complete()
                        End Using

                        Return JsonConvert.SerializeObject(New With {Key .isSuccess = True, .oppID = oppID, .oppBizDocID = shippingBizDocID}, Formatting.None)
                    Else
                        Dim objMSException As New MSFException
                        objMSException.ErrorMessage = "Order doesn't exists."
                        objMSException.ErrorCode = 500

                        Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
                    End If
                End If
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/InvoiceOrders")>
    Public Function InvoiceOrders(ByVal orders As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim dt As DataTable = JsonConvert.DeserializeObject(Of DataTable)(orders)

                Dim objOpportunity As COpportunities
                Dim objMassSalesFulfillment As New MassSalesFulfillment
                objMassSalesFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objMassSalesFulfillment.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))

                For Each dr As DataRow In dt.Rows
                    Try
                        objOpportunity = New COpportunities
                        objOpportunity.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                        objOpportunity.OpportID = CCommon.ToLong(dr("OppID"))
                        Dim dtOrder As DataTable = objOpportunity.GetOrderDetailForBizDoc()

                        If Not dtOrder Is Nothing AndAlso dtOrder.Rows.Count > 0 Then
                            objMassSalesFulfillment.OppID = CCommon.ToLong(dr("OppID"))
                            objMassSalesFulfillment.SelectedRecords = CCommon.ToString(dr("OppItemIDs"))
                            Dim dtOrderItems As DataTable = objMassSalesFulfillment.GetOrderItems(2)

                            If Not dtOrderItems Is Nothing AndAlso dtOrderItems.Rows.Count > 0 Then
                                Dim objOpportunityBizDocs As New OpportunityBizDocs
                                objOpportunityBizDocs.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                                objOpportunityBizDocs.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                                objOpportunityBizDocs.ClientMachineUTCTimeOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                                objOpportunityBizDocs.DivisionID = CCommon.ToLong(dtOrder.Rows(0)("numDivisionID"))
                                objOpportunityBizDocs.CompanyName = CCommon.ToString(dtOrder.Rows(0)("vcCompanyName"))
                                objOpportunityBizDocs.Relationship = CCommon.ToLong(dtOrder.Rows(0)("numCompanyType"))
                                objOpportunityBizDocs.Profile = CCommon.ToLong(dtOrder.Rows(0)("vcProfile"))
                                objOpportunityBizDocs.OppID = CCommon.ToLong(dr("OppID"))
                                objOpportunityBizDocs.OppType = CCommon.ToShort(dtOrder.Rows(0)("tintOppType"))
                                objOpportunityBizDocs.ShipVia = CCommon.ToLong(dtOrder.Rows(0)("intUsedShippingCompany"))
                                objOpportunityBizDocs.BizDocType = 287
                                objOpportunityBizDocs.IsDeferred = False
                                objOpportunityBizDocs.BizDocItems = GetBizDocItems(dtOrderItems)
                                objOpportunityBizDocs.IsEnableDeferredIncome = CCommon.ToBool(HttpContext.Current.Session("IsEnableDeferredIncome"))
                                objOpportunityBizDocs.IsAutolinkUnappliedPayment = CCommon.ToBool(HttpContext.Current.Session("AutolinkUnappliedPayment"))
                                objOpportunityBizDocs.ExchangeRate = CCommon.ToDouble(HttpContext.Current.Session("fltExchangeRate"))
                                objOpportunityBizDocs.SourceBizDocID = CCommon.ToInteger(dr("ParentBizDocID"))
                                objOpportunityBizDocs.CreateBizDoc(False)

                                Try
                                    objMassSalesFulfillment.OppID = CCommon.ToLong(dr("OppID"))
                                    objMassSalesFulfillment.UpdateOrderStauts(3, 0)
                                Catch ex As Exception
                                    'DO NOT THROW ERROR
                                End Try

                                dr("IsSucess") = True
                                dr("ErrorMessage") = ""
                            Else
                                dr("IsSucess") = False
                                dr("ErrorMessage") = "No item(s) left to invoice"
                            End If
                        Else
                            dr("IsSucess") = False
                            dr("ErrorMessage") = "Order doesn't exists."
                        End If
                    Catch ex As Exception
                        dr("IsSucess") = False

                        If ex.Message.Contains("WORK_ORDER_NOT_COMPLETED") Then
                            dr("ErrorMessage") = "Not able to add invoice because some work orders are still not completed."
                        ElseIf ex.Message.Contains("QTY_MISMATCH") Then
                            dr("ErrorMessage") = "Not able to add invoice because qty is not same as required qty."
                        ElseIf ex.Message.Contains("REQUIRED_SERIALS_NOT_PROVIDED") Then
                            dr("ErrorMessage") = "Not able to add invoice because required number of serials are not provided."
                        ElseIf ex.Message.Contains("INVALID_SERIAL_NUMBERS") Then
                            dr("ErrorMessage") = "Not able to add invoice because invalid serials are provided."
                        ElseIf ex.Message.Contains("REQUIRED_LOTNO_NOT_PROVIDED") Then
                            dr("ErrorMessage") = "Not able to add invoice because required number of Lots are not provided."
                        ElseIf ex.Message.Contains("INVALID_LOT_NUMBERS") Then
                            dr("ErrorMessage") = "Not able to add invoice because invalid Lots are provided."
                        ElseIf ex.Message.Contains("SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY") Then
                            dr("ErrorMessage") = "Not able to add invoice because Lot number do not have enough qty to fulfill."
                        ElseIf ex.Message.Contains("NOTSUFFICIENTQTY_ALLOCATION") Then
                            dr("ErrorMessage") = "Not able to add invoice because warehouse allocation is invlid."
                        ElseIf ex.Message.Contains("NOTSUFFICIENTQTY_ONHAND") Then
                            dr("ErrorMessage") = "Not able to add invoice because warehouse does not have enought on hand quantity."
                        ElseIf ex.Message.Contains("BIZDOC_QTY_MORE_THEN_ORDERED_QTY") Then
                            dr("ErrorMessage") = "Not able to add invoice because bizdoc qty is more than ordered quantity for some item(s)."
                        Else
                            dr("ErrorMessage") = "Not able to add invoice due to unknown error."
                        End If
                    End Try
                Next

                dt.AcceptChanges()

                Return JsonConvert.SerializeObject(dt, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)

            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/PackOrders")>
    Public Function PackOrders(ByVal orders As String, ByVal isAddFulfillmentBizDoc As Boolean) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim dt As DataTable = JsonConvert.DeserializeObject(Of DataTable)(orders)

                Dim objOpportunity As COpportunities
                Dim objMassSalesFulfillment As New MassSalesFulfillment
                objMassSalesFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objMassSalesFulfillment.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))

                For Each dr As DataRow In dt.Rows
                    Try
                        objOpportunity = New COpportunities
                        objOpportunity.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                        objOpportunity.OpportID = CCommon.ToLong(dr("OppID"))
                        Dim dtOrder As DataTable = objOpportunity.GetOrderDetailForBizDoc()

                        If Not dtOrder Is Nothing AndAlso dtOrder.Rows.Count > 0 Then
                            objMassSalesFulfillment.OppID = CCommon.ToLong(dr("OppID"))
                            objMassSalesFulfillment.SelectedRecords = CCommon.ToString(dr("OppItemIDs"))
                            Dim dtOrderItems As DataTable = objMassSalesFulfillment.GetOrderItems(3)

                            If Not dtOrderItems Is Nothing AndAlso dtOrderItems.Rows.Count > 0 Then
                                Dim objOpportunityBizDocs As New OpportunityBizDocs
                                objOpportunityBizDocs.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                                objOpportunityBizDocs.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                                objOpportunityBizDocs.ClientMachineUTCTimeOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                                objOpportunityBizDocs.DivisionID = CCommon.ToLong(dtOrder.Rows(0)("numDivisionID"))
                                objOpportunityBizDocs.CompanyName = CCommon.ToString(dtOrder.Rows(0)("vcCompanyName"))
                                objOpportunityBizDocs.Relationship = CCommon.ToLong(dtOrder.Rows(0)("numCompanyType"))
                                objOpportunityBizDocs.Profile = CCommon.ToLong(dtOrder.Rows(0)("vcProfile"))
                                objOpportunityBizDocs.OppID = CCommon.ToLong(dr("OppID"))
                                objOpportunityBizDocs.OppType = CCommon.ToShort(dtOrder.Rows(0)("tintOppType"))
                                objOpportunityBizDocs.ShipVia = CCommon.ToLong(dtOrder.Rows(0)("intUsedShippingCompany"))
                                objOpportunityBizDocs.BizDocType = CCommon.ToLong(HttpContext.Current.Session("numDefaultSalesShippingDoc"))
                                objOpportunityBizDocs.IsDeferred = False
                                objOpportunityBizDocs.BizDocItems = GetBizDocItems(dtOrderItems)
                                objOpportunityBizDocs.IsEnableDeferredIncome = CCommon.ToBool(HttpContext.Current.Session("IsEnableDeferredIncome"))
                                objOpportunityBizDocs.IsAutolinkUnappliedPayment = CCommon.ToBool(HttpContext.Current.Session("AutolinkUnappliedPayment"))
                                objOpportunityBizDocs.ExchangeRate = CCommon.ToDouble(HttpContext.Current.Session("fltExchangeRate"))

                                Dim lngOppBizDocID As Long = 0
                                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                    lngOppBizDocID = objOpportunityBizDocs.CreateBizDoc(False)

                                    If isAddFulfillmentBizDoc AndAlso objOpportunityBizDocs.BizDocType <> 296 Then
                                        objMassSalesFulfillment.OppID = CCommon.ToLong(dr("OppID"))
                                        objMassSalesFulfillment.SelectedRecords = CCommon.ToString(dr("OppItemIDs"))
                                        dtOrderItems = objMassSalesFulfillment.GetOrderItems(1)

                                        If Not dtOrderItems Is Nothing AndAlso dtOrderItems.Rows.Count > 0 Then
                                            Dim objOpportunityBizDocs296 As New OpportunityBizDocs
                                            objOpportunityBizDocs296.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                                            objOpportunityBizDocs296.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                                            objOpportunityBizDocs296.ClientMachineUTCTimeOffset = CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset"))
                                            objOpportunityBizDocs296.DivisionID = CCommon.ToLong(dtOrder.Rows(0)("numDivisionID"))
                                            objOpportunityBizDocs296.CompanyName = CCommon.ToString(dtOrder.Rows(0)("vcCompanyName"))
                                            objOpportunityBizDocs296.Relationship = CCommon.ToLong(dtOrder.Rows(0)("numCompanyType"))
                                            objOpportunityBizDocs296.Profile = CCommon.ToLong(dtOrder.Rows(0)("vcProfile"))
                                            objOpportunityBizDocs296.OppID = CCommon.ToLong(dr("OppID"))
                                            objOpportunityBizDocs296.OppType = CCommon.ToShort(dtOrder.Rows(0)("tintOppType"))
                                            objOpportunityBizDocs296.ShipVia = CCommon.ToLong(dtOrder.Rows(0)("intUsedShippingCompany"))
                                            objOpportunityBizDocs296.BizDocType = 296
                                            objOpportunityBizDocs296.IsDeferred = False
                                            objOpportunityBizDocs296.BizDocItems = GetBizDocItems(dtOrderItems)
                                            objOpportunityBizDocs296.IsEnableDeferredIncome = CCommon.ToBool(HttpContext.Current.Session("IsEnableDeferredIncome"))
                                            objOpportunityBizDocs296.IsAutolinkUnappliedPayment = CCommon.ToBool(HttpContext.Current.Session("AutolinkUnappliedPayment"))
                                            objOpportunityBizDocs296.ExchangeRate = CCommon.ToDouble(HttpContext.Current.Session("fltExchangeRate"))
                                            objOpportunityBizDocs296.SourceBizDocID = lngOppBizDocID
                                            objOpportunityBizDocs296.CreateBizDoc(False)
                                        End If
                                    End If

                                    objTransactionScope.Complete()
                                End Using

                                dr("IsSucess") = True
                                dr("ErrorMessage") = ""
                                dr("OppBizDocID") = lngOppBizDocID
                            Else
                                dr("IsSucess") = False
                                dr("ErrorMessage") = "No item(s) left to be added in shipping bizdoc"
                            End If
                        Else
                            dr("IsSucess") = False
                            dr("ErrorMessage") = "Order doesn't exists."
                        End If
                    Catch ex As Exception
                        dr("IsSucess") = False

                        If ex.Message.Contains("WORK_ORDER_NOT_COMPLETED") Then
                            dr("ErrorMessage") = "Not able to add bizdoc because some work orders are still not completed."
                        ElseIf ex.Message.Contains("QTY_MISMATCH") Then
                            dr("ErrorMessage") = "Not able to add bizdoc because qty is not same as required qty."
                        ElseIf ex.Message.Contains("REQUIRED_SERIALS_NOT_PROVIDED") Then
                            dr("ErrorMessage") = "Not able to add bizdoc because required number of serials are not provided."
                        ElseIf ex.Message.Contains("INVALID_SERIAL_NUMBERS") Then
                            dr("ErrorMessage") = "Not able to add bizdoc because invalid serials are provided."
                        ElseIf ex.Message.Contains("REQUIRED_LOTNO_NOT_PROVIDED") Then
                            dr("ErrorMessage") = "Not able to add bizdoc because required number of Lots are not provided."
                        ElseIf ex.Message.Contains("INVALID_LOT_NUMBERS") Then
                            dr("ErrorMessage") = "Not able to add bizdoc because invalid Lots are provided."
                        ElseIf ex.Message.Contains("SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY") Then
                            dr("ErrorMessage") = "Not able to add bizdoc because Lot number do not have enough qty to fulfill."
                        ElseIf ex.Message.Contains("NOTSUFFICIENTQTY_ALLOCATION") Then
                            dr("ErrorMessage") = "Not able to add bizdoc because warehouse allocation is invlid."
                        ElseIf ex.Message.Contains("NOTSUFFICIENTQTY_ONHAND") Then
                            dr("ErrorMessage") = "Not able to add bizdoc because warehouse does not have enought on hand quantity."
                        ElseIf ex.Message.Contains("BIZDOC_QTY_MORE_THEN_ORDERED_QTY") Then
                            dr("ErrorMessage") = "Not able to add bizdoc because bizdoc qty is more than ordered quantity for some item(s)."
                        Else
                            dr("ErrorMessage") = "Not able to add bizdoc due to unknown error." & ex.Message
                        End If
                    End Try
                Next

                dt.AcceptChanges()

                Return JsonConvert.SerializeObject(dt, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)

            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/CloseOrders")>
    Public Function CloseOrders(ByVal orders As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim dt As DataTable = JsonConvert.DeserializeObject(Of DataTable)(orders)
                For Each dr As DataRow In dt.Rows
                    Try
                        Dim objOpportunityAutomation As New OpportunityAutomation
                        objOpportunityAutomation.CloseOrder(CCommon.ToLong(HttpContext.Current.Session("DomainID")), CCommon.ToLong(HttpContext.Current.Session("UserContactID")), CCommon.ToLong(dr("OppID")), 1, 1)

                        Try
                            Dim objMassSalesFulfillment As New MassSalesFulfillment
                            objMassSalesFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                            objMassSalesFulfillment.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                            objMassSalesFulfillment.OppID = CCommon.ToLong(dr("OppID"))
                            objMassSalesFulfillment.UpdateOrderStauts(5, 0)
                        Catch ex As Exception
                            'DO NOT THROW ERROR
                        End Try

                        dr("IsSucess") = True
                        dr("ErrorMessage") = ""
                    Catch ex As Exception
                        dr("IsSucess") = False
                        dr("ErrorMessage") = ex.Message
                    End Try
                Next

                dt.AcceptChanges()
                Return JsonConvert.SerializeObject(dt, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)

            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/PayOrders")>
    Public Function PayOrders(ByVal invoices As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objOpportunity As COpportunities
                Dim dt As DataTable = JsonConvert.DeserializeObject(Of DataTable)(invoices)

                For Each dr As DataRow In dt.Rows
                    Try
                        objOpportunity = New COpportunities
                        objOpportunity.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                        objOpportunity.OpportID = CCommon.ToLong(dr("OppID"))
                        Dim dtOrder As DataTable = objOpportunity.GetOrderDetailForBizDoc()

                        If Not dtOrder Is Nothing AndAlso dtOrder.Rows.Count > 0 Then
                            Dim objOpportunityAutomation As New OpportunityAutomation
                            objOpportunityAutomation.ChargeCreditCardMassSalesFulfillment(CCommon.ToLong(HttpContext.Current.Session("DomainID")), CCommon.ToLong(HttpContext.Current.Session("UserContactID")), CCommon.ToLong(dtOrder.Rows(0)("numDivisionID")), CCommon.ToLong(dtOrder.Rows(0)("numContactID")), CCommon.ToLong(dr("OppID")), CCommon.ToLong(dr("OppBizDocID")), CCommon.ToDecimal(dr("AmountToPay")), CCommon.ToLong(dtOrder.Rows(0)("numBaseCurrencyID")), CCommon.ToLong(dtOrder.Rows(0)("numCurrencyID")))

                            Try
                                Dim objMassSalesFulfillment As New MassSalesFulfillment
                                objMassSalesFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                                objMassSalesFulfillment.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))
                                objMassSalesFulfillment.OppID = CCommon.ToLong(dr("OppID"))
                                objMassSalesFulfillment.UpdateOrderStauts(4, 0)
                            Catch ex As Exception
                                'DO NOT THROW ERROR
                            End Try

                            dr("IsSucess") = True
                            dr("ErrorMessage") = ""
                        Else
                            dr("IsSucess") = False
                            dr("ErrorMessage") = "Order doesn't exists."
                        End If
                    Catch ex As Exception
                        dr("IsSucess") = False
                        dr("ErrorMessage") = ex.Message
                    End Try
                Next

                dt.AcceptChanges()
                Return JsonConvert.SerializeObject(dt, Formatting.None)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)

            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetOrderShippingDetail")>
    Public Function GetOrderShippingDetail(ByVal oppID As Long, ByVal shippingReportID As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objOpportunity As New COpportunities
                objOpportunity.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objOpportunity.OpportID = oppID
                Dim dt As DataTable = objOpportunity.GetShippingDetail(shippingReportID)

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    Return JsonConvert.SerializeObject(dt, Formatting.None)
                Else
                    Dim objMSException As New MSFException
                    objMSException.ErrorMessage = "Order doesn't exists."
                    objMSException.ErrorCode = 500

                    Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
                End If
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/PrintPackingSlip")>
    Public Function PrintPackingSlip(ByVal oppID As Long, ByVal oppBizDocID As Long) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim objHtmlToPdf As New HTMLToPDF
                Dim bytePackingSlip As Byte() = objHtmlToPdf.CreatePackingSlipPDF(oppID, oppBizDocID, CCommon.ToLong(HttpContext.Current.Session("DomainID")), CCommon.ToLong(HttpContext.Current.Session("UserContactID")), CCommon.ToInteger(HttpContext.Current.Session("ClientMachineUTCTimeOffset")), CCommon.ToString(HttpContext.Current.Session("DateFormat")))

                Return Convert.ToBase64String(bytePackingSlip, 0, bytePackingSlip.Length)
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/PrintShippingLabel")>
    Public Function PrintShippingLabel(ByVal warehouseID As Long, ByVal selectdRecords As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                If warehouseID > 0 Then
                    Dim objItem As New CItems
                    objItem.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                    objItem.WarehouseID = warehouseID
                    Dim dtWarehouse As DataTable = objItem.GetWareHouses()

                    If Not dtWarehouse Is Nothing AndAlso dtWarehouse.Rows.Count > 0 AndAlso Not String.IsNullOrEmpty(dtWarehouse.Rows(0)("vcPrintNodeAPIKey")) AndAlso Not String.IsNullOrEmpty(dtWarehouse.Rows(0)("vcPrintNodePrinterID")) Then
                        Dim objMassSalesFulfillment As New MassSalesFulfillment
                        objMassSalesFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                        objMassSalesFulfillment.UserCntID = CCommon.ToLong(HttpContext.Current.Session("UserContactID"))

                        Dim objOpportunity As COpportunities
                        Dim dtBizDocs As DataTable = JsonConvert.DeserializeObject(Of DataTable)(selectdRecords)

                        For Each drBizDoc As DataRow In dtBizDocs.Rows
                            Try
                                objMassSalesFulfillment.OppID = CCommon.ToLong(drBizDoc("OppID"))
                                objMassSalesFulfillment.OppBizDocID = CCommon.ToLong(drBizDoc("OppBizDocID"))
                                Dim dtShippingLabel As DataTable = objMassSalesFulfillment.GetShippingLabelForPrint()

                                If Not dtShippingLabel Is Nothing AndAlso dtShippingLabel.Rows.Count > 0 Then
                                    For Each dr As DataRow In dtShippingLabel.Rows
                                        Dim service As New PrintNodeService("https://api.printnode.com/", CCommon.ToString(dtWarehouse.Rows(0)("vcPrintNodeAPIKey")))
                                        service.SubmitPrintJob(CCommon.GetDocumentPath(HttpContext.Current.Session("DomainID")) & CCommon.ToString(dr("vcShippingLabelImage")), CCommon.ToString(dtWarehouse.Rows(0)("vcPrintNodePrinterID")))
                                    Next

                                    drBizDoc("IsSucess") = True
                                    drBizDoc("ErrorMessage") = ""
                                Else
                                    drBizDoc("IsSucess") = False
                                    drBizDoc("ErrorMessage") = "Shipping label(s) doesn't exists"
                                End If
                            Catch ex As Exception
                                drBizDoc("IsSucess") = False
                                drBizDoc("ErrorMessage") = ex.Message
                            End Try
                        Next

                        dtBizDocs.AcceptChanges()
                        Return JsonConvert.SerializeObject(dtBizDocs, Formatting.None)
                    Else
                        Dim objMSException As New MSFException
                        objMSException.ErrorMessage = "PrintNode is not configured for selected warehouse."
                        objMSException.ErrorCode = 500

                        Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
                    End If
                Else
                    Dim objMSException As New MSFException
                    objMSException.ErrorMessage = "Select warehouse."
                    objMSException.ErrorCode = 500

                    Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
                End If
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/SaveLastView")>
    Public Function SaveLastView(ByVal viewID As Integer, ByVal subViewID As Integer) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim p As New Generic.List(Of PersistAttribute)
                p.Add(New PersistAttribute("viewID", viewID))
                p.Add(New PersistAttribute("subViewID", subViewID))
                Dim strFileFullPath As String = CCommon.GetDocumentPhysicalPath() & "Persistance_" & "frmmasssalesfulfillment.aspx".GetHashCode() & "_" & HttpContext.Current.Session("DomainID").ToString & "_" & HttpContext.Current.Session("UserContactID").ToString & ".xml"

                'Serialize object to a text file.
                Dim objStreamWriter As New IO.StreamWriter(strFileFullPath)
                Dim x As New System.Xml.Serialization.XmlSerializer(p.GetType)
                x.Serialize(objStreamWriter, p)
                objStreamWriter.Close()
            Else
                Throw New WebFaultException(Net.HttpStatusCode.Unauthorized)
            End If
        Catch exWeb As WebFaultException When exWeb.StatusCode = Net.HttpStatusCode.Unauthorized
            Throw
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = ex.Message
            objMSException.ErrorCode = 500

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        End Try
    End Function

    <OperationContract()>
    <WebInvoke(BodyStyle:=WebMessageBodyStyle.Wrapped, Method:="POST", RequestFormat:=WebMessageFormat.Json, ResponseFormat:=WebMessageFormat.Json, UriTemplate:="/GetNewShippingRate")>
    Public Function GetNewShippingRate(ByVal oppID As Long, ByVal shipVia As Long, ByVal shipService As Long, ByVal shippingConfiguration As String, ByVal boxes As String) As String
        Try
            If CCommon.ToLong(HttpContext.Current.Session("DomainID")) > 0 AndAlso CCommon.ToLong(HttpContext.Current.Session("UserContactID")) > 0 Then
                Dim sRateType As Short = 0
                Dim dtShippingConfiguration As DataTable = JsonConvert.DeserializeObject(Of DataTable)(shippingConfiguration)

                Dim ezRates As New nsoftware.InShip.Ezrates
                If shipVia = 91 Then 'Fedex
                    ezRates.Provider = nsoftware.InShip.EzratesProviders.pFedEx
                    ezRates.Account.Server = CCommon.ToString(dtShippingConfiguration.Select("numShipVia=91 AND intShipFieldID=9")(0)("vcShipFieldValue"))
                    ezRates.Account.DeveloperKey = CCommon.ToString(dtShippingConfiguration.Select("numShipVia=91 AND intShipFieldID=11")(0)("vcShipFieldValue"))
                    ezRates.Account.Password = CCommon.ToString(dtShippingConfiguration.Select("numShipVia=91 AND intShipFieldID=12")(0)("vcShipFieldValue"))
                    ezRates.Account.AccountNumber = CCommon.ToString(dtShippingConfiguration.Select("numShipVia=91 AND intShipFieldID=6")(0)("vcShipFieldValue"))
                    ezRates.Account.MeterNumber = CCommon.ToString(dtShippingConfiguration.Select("numShipVia=91 AND intShipFieldID=7")(0)("vcShipFieldValue"))
                    sRateType = CCommon.ToShort(dtShippingConfiguration.Select("numShipVia=91 AND intShipFieldID=20")(0)("vcShipFieldValue"))
                ElseIf shipVia = 88 Then 'UPS
                    ezRates.Provider = nsoftware.InShip.EzratesProviders.pUPS
                    ezRates.Account.Server = CCommon.ToString(dtShippingConfiguration.Select("numShipVia=88 AND intShipFieldID=5")(0)("vcShipFieldValue"))
                    ezRates.Account.AccessKey = CCommon.ToString(dtShippingConfiguration.Select("numShipVia=88 AND intShipFieldID=1")(0)("vcShipFieldValue"))
                    ezRates.Account.UserId = CCommon.ToString(dtShippingConfiguration.Select("numShipVia=88 AND intShipFieldID=2")(0)("vcShipFieldValue"))
                    ezRates.Account.Password = CCommon.ToString(dtShippingConfiguration.Select("numShipVia=88 AND intShipFieldID=3")(0)("vcShipFieldValue"))
                    ezRates.Account.AccountNumber = CCommon.ToString(dtShippingConfiguration.Select("numShipVia=88 AND intShipFieldID=4")(0)("vcShipFieldValue"))
                Else 'USPS
                    ezRates.Provider = nsoftware.InShip.EzratesProviders.pUSPS
                    ezRates.Account.Server = CCommon.ToString(dtShippingConfiguration.Select("numShipVia=90 AND intShipFieldID=17")(0)("vcShipFieldValue"))
                    ezRates.Account.UserId = CCommon.ToString(dtShippingConfiguration.Select("numShipVia=90 AND intShipFieldID=15")(0)("vcShipFieldValue"))
                    ezRates.Account.Password = CCommon.ToString(dtShippingConfiguration.Select("numShipVia=90 AND intShipFieldID=16")(0)("vcShipFieldValue"))
                End If
                ezRates.RequestedService = shipService

                Dim objMassSalesFulfillment As New MassSalesFulfillment
                objMassSalesFulfillment.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objMassSalesFulfillment.OppID = oppID
                objMassSalesFulfillment.ShipVia = shipVia
                Dim dsOrder As DataSet = objMassSalesFulfillment.GetOrderDetailsForShippingRate()

                If Not dsOrder Is Nothing AndAlso dsOrder.Tables.Count >= 0 AndAlso dsOrder.Tables(0).Rows.Count > 0 Then
                    ezRates.SenderAddress.Address1 = CCommon.ToString(dsOrder.Tables(0).Rows(0)("vcStreet"))
                    ezRates.SenderAddress.City = CCommon.ToString(dsOrder.Tables(0).Rows(0)("vcCity"))
                    ezRates.SenderAddress.State = CCommon.ToString(dsOrder.Tables(0).Rows(0)("vcState"))
                    ezRates.SenderAddress.ZipCode = CCommon.ToString(dsOrder.Tables(0).Rows(0)("vcZipCode"))
                    ezRates.SenderAddress.CountryCode = CCommon.ToString(dsOrder.Tables(0).Rows(0)("vcCountry"))
                Else
                    Throw New Exception("SHIP_FROM_ADDRESS_NOT_AVAILABLE")
                End If

                If Not dsOrder Is Nothing AndAlso dsOrder.Tables.Count >= 1 AndAlso dsOrder.Tables(1).Rows.Count > 0 Then
                    ezRates.RecipientAddress.Address1 = CCommon.ToString(dsOrder.Tables(1).Rows(0)("vcStreet"))
                    ezRates.RecipientAddress.City = CCommon.ToString(dsOrder.Tables(1).Rows(0)("vcCity"))
                    ezRates.RecipientAddress.State = CCommon.ToString(dsOrder.Tables(1).Rows(0)("vcState"))
                    ezRates.RecipientAddress.ZipCode = CCommon.ToString(dsOrder.Tables(1).Rows(0)("vcZipCode"))
                    ezRates.RecipientAddress.CountryCode = CCommon.ToString(dsOrder.Tables(1).Rows(0)("vcCountry"))
                Else
                    Throw New Exception("SHIP_TO_FROM_ADDRESS_NOT_AVAILABLE")
                End If

                Dim totalWeight As Double = 0
                Dim dtBoxes As DataTable = JsonConvert.DeserializeObject(Of DataTable)(boxes)

                If Not dtBoxes Is Nothing AndAlso dtBoxes.Rows.Count > 0 Then
                    For Each dr As DataRow In dtBoxes.Rows
                        Dim objPackageDetail As New nsoftware.InShip.PackageDetail()
                        objPackageDetail.PackagingType = CCommon.ToInteger(dr("PackageType"))
                        objPackageDetail.Length = CCommon.ToInteger(dr("Length"))
                        objPackageDetail.Width = CCommon.ToInteger(dr("Width"))
                        objPackageDetail.Height = CCommon.ToInteger(dr("Height"))
                        objPackageDetail.Girth = (2 * CCommon.ToInteger(dr("Length"))) + (2 * CCommon.ToInteger(dr("Width")))
                        If (ezRates.Provider = nsoftware.InShip.EzratesProviders.pUSPS) Then
                            Dim intWeight As Integer

                            If Integer.TryParse(CCommon.ToString(Convert.ToDecimal(dr("Weight"))), intWeight) Then
                                objPackageDetail.Weight = Convert.ToString(intWeight) + " lbs 0 oz"
                            Else
                                objPackageDetail.Weight = CCommon.ToString(Decimal.Truncate(Convert.ToDecimal(dr("Weight")))) + " lbs " + Convert.ToInt64((Convert.ToDecimal(dr("Weight")) - Decimal.Truncate(Convert.ToDecimal(dr("Weight")))) * 16).ToString() + " oz"
                            End If
                        Else
                            objPackageDetail.Weight = CCommon.ToString(Convert.ToDecimal(dr("Weight")))
                        End If

                        ezRates.Packages.Add(objPackageDetail)

                        totalWeight = totalWeight + Convert.ToDecimal(dr("Weight"))
                    Next

                    ezRates.TotalWeight = CCommon.ToString(totalWeight)
                Else
                    ezRates.Packages.Add(New nsoftware.InShip.PackageDetail())
                    ezRates.Packages(0).PackagingType = nsoftware.InShip.TPackagingTypes.ptYourPackaging
                    ezRates.Packages(0).Length = 0
                    ezRates.Packages(0).Width = 0
                    ezRates.Packages(0).Height = 0
                    ezRates.Packages(0).Girth = 0
                    If (ezRates.Provider = nsoftware.InShip.EzratesProviders.pUSPS) Then
                        Dim intWeight As Integer

                        If Integer.TryParse(CCommon.ToString(totalWeight), intWeight) Then
                            ezRates.Packages(0).Weight = CCommon.ToString(intWeight) + " lbs 0 oz"
                        Else
                            ezRates.Packages(0).Weight = CCommon.ToString(Decimal.Truncate(Convert.ToDecimal(totalWeight))) + " lbs " + Convert.ToInt64((Convert.ToDecimal(totalWeight) - Decimal.Truncate(Convert.ToDecimal(totalWeight))) * 16).ToString() + " oz"
                        End If
                    Else
                        ezRates.Packages(0).Weight = CCommon.ToString(totalWeight)
                    End If

                    ezRates.TotalWeight = "0.0"
                End If

                ezRates.GetRates()

                If ezRates.Config("Warning") = "" Then
                    Dim rate As Double = 0
                    Dim transitTime As String
                    Dim deliveryDate As String

                    If ezRates.Services.Count > 0 Then
                        If (ezRates.Services(0).AccountNetCharge <> "") AndAlso sRateType = 1 Then 'negotiated rate
                            rate = CCommon.ToDouble(ezRates.Services(0).AccountNetCharge)
                        Else
                            rate = CCommon.ToDouble(ezRates.Services(0).ListNetCharge)
                        End If

                        If (ezRates.Provider = nsoftware.InShip.EzratesProviders.pUPS) Then
                            transitTime = ezRates.Services(0).TransitTime
                        Else
                            deliveryDate = ezRates.Services(0).DeliveryDate
                        End If

                        Dim shippingRate = New With {Key .OppID = oppID, .Rate = rate, .DeliveryDate = deliveryDate, .TransitTime = transitTime}

                        Return JsonConvert.SerializeObject(shippingRate, Formatting.None)
                    Else
                        Dim objMSException As New MSFException
                        objMSException.ErrorMessage = "Not able to fetch rates"
                        objMSException.ErrorCode = 500
                        objMSException.OppID = oppID

                        Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
                    End If
                Else
                    Dim objMSException As New MSFException
                    objMSException.ErrorMessage = ezRates.Config("Warning")
                    objMSException.ErrorCode = 500

                    Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
                End If
            Else
            Throw New WebFaultException(Of System.Net.HttpStatusCode)(Net.HttpStatusCode.Unauthorized, Net.HttpStatusCode.Unauthorized)
            End If
        Catch exShip As nsoftware.InShip.InShipException
            Dim objMSException As New MSFException
            objMSException.ErrorMessage = exShip.Code & " - " & exShip.Message
            objMSException.ErrorCode = 500
            objMSException.OppID = oppID

            Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
        Catch ex As Exception
            If ex.Message.Contains("SHIP_FROM_ADDRESS_NOT_AVAILABLE") Then
                Dim objMSException As New MSFException
                objMSException.ErrorMessage = "SHIP_FROM_ADDRESS_NOT_AVAILABLE"
                objMSException.ErrorCode = 500
                objMSException.OppID = oppID

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            ElseIf ex.Message.Contains("SHIP_TO_FROM_ADDRESS_NOT_AVAILABLE") Then
                Dim objMSException As New MSFException
                objMSException.ErrorMessage = "SHIP_TO_FROM_ADDRESS_NOT_AVAILABLE"
                objMSException.ErrorCode = 500
                objMSException.OppID = oppID

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            Else
                ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(HttpContext.Current.Session("DomainID")), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)

                Dim objMSException As New MSFException
                objMSException.ErrorMessage = ex.Message
                objMSException.ErrorCode = 500
                objMSException.OppID = oppID

                Throw New WebFaultException(Of MSFException)(objMSException, Net.HttpStatusCode.InternalServerError)
            End If
        End Try
    End Function

End Class

Public Class MSFException
    Public Property ErrorMessage As String
    Public Property ErrorCode As Integer
    Public Property OppID As Long
    Public Property ShipByDate As String
End Class

'End Namespace


