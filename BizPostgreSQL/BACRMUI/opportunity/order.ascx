﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="order.ascx.vb" Inherits=".order"
    ClientIDMode="Static" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type='text/javascript' src="../JavaScript/select2.js"></script>
<script type="text/javascript" src="../javascript/slick.min.js"></script>
<script type='text/javascript' src="../JavaScript/biz.kitswithchildkits.js"></script>
<script type="text/javascript" src="../JavaScript/Orders.js"></script>
<script type="text/javascript" src="../JavaScript/jscal2.js"></script>
<script type="text/javascript" src="../JavaScript/en.js"></script>
<script type="text/javascript" src="../JavaScript/Common.js"></script>
<script type="text/javascript" src="../javascript/CustomFieldValidation.js"></script>
<script type="text/javascript" src="../JavaScript/jquery.validate.js"></script>
<link rel="stylesheet" type="text/css" href="../CSS/style.css" />
<link rel="stylesheet" href="../CSS/select2.css" />
<link rel="Stylesheet" href="../CSS/font-awesome.min.css" />
<link rel="Stylesheet" href="../CSS/slick.css" />
<link rel="Stylesheet" href="../CSS/slick-theme.css" />
<link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />


<script type="text/javascript">

    function FocuonItem() {
        $("#txtItem").select2("open");
        $("#txtItem").focus();
    }

    function FocusUnits() {
        var tmpStr = $("#txtUnits").val();
        $("#txtUnits").val('');
        $("#txtUnits").val(tmpStr);
        $("#txtUnits").focus();
    }

    function FocusPrice() {
        var tmpStr = $("#txtprice").val();
        $("#txtprice").val('');
        $("#txtprice").val(tmpStr);
        $("#txtprice").focus();
    }

    function FocusDiscount() {
        var tmpStr = $("#txtItemDiscount").val();
        $("#txtItemDiscount").val('');
        $("#txtItemDiscount").val(tmpStr);
        $("#txtItemDiscount").focus();
    }

    function AddItemToOrder(e) {
        var k;
        document.all ? k = e.keyCode : k = e.which;

        if (k === 13) {
            $("[id$=btnUpdate]").click();
        }
    }

    function openLastCostPop() {
        window.open('../opportunity/frmItemPriceRecommd.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=' + $("#hdnCurrentSelectedItem").val() + '&Unit=' + 0 + '&DivID=' + 0 + '&OppType=' + 0 + '&CalPrice=' + 0 + '&WarehouseItemID=' + 0, '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
    }

    function OpenItemVendors(itemCode, ShipAddress) {
        var isHideDistance = false;
        var radCmbCompany = $find('radCmbCompany');

        if (radCmbCompany != null) {
            if (radCmbCompany.get_value() != null && radCmbCompany.get_value().length > 0) {
                isHideDistance = true;
            }
        }

        window.open('../opportunity/frmItemVendors.aspx?numItemCode=' + itemCode + '&numShipAddressID=' + ShipAddress + '&isHideDistance=' + isHideDistance, '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
    }

    function Confirm() {
        if ($("#divPromotionDetailRI").is(':visible') && $("#lblCouponText").is(':visible')) {
            if ($("#txtCouponCodeRI").val().length == 0) {
                var confirm_value = document.createElement("INPUT");
                confirm_value.type = "hidden";
                confirm_value.name = "confirm_value";
                if (confirm("You’re about to add an item at a regular price. You can cancel this window and add coupon code first, then add the item, or simply click continue and item will be added to the cart.")) {
                    confirm_value.value = "Yes";
                } else {
                    confirm_value.value = "No";
                }
                document.forms[0].appendChild(confirm_value);
            }
        }
    }

    var columns;
    var userDefaultPageSize = '<%= Session("PagingRows")%>';
    var userDefaultCharSearch = '<%= Session("ChrForItemSearch") %>';
    var varPageSize = parseInt(userDefaultPageSize, 10);
    var varCharSearch = 1;
    if (parseInt(userDefaultCharSearch, 10) > 0) {
        varCharSearch = parseInt(userDefaultCharSearch, 10);
    }

    $(function () {
        Scroll(true);

        $.ajax({
            type: "POST",
            url: '../common/Common.asmx/GetSearchedItems',
            data: '{ searchText: "abcxyz", pageIndex: 1, pageSize: 10, divisionId: 0, isGetDefaultColumn: true, warehouseID: 0, searchOrdCusHistory: 0, oppType: 1, isTransferTo: false,IsCustomerPartSearch:false,searchType:"1"}',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (data) {
                // Replace the div's content with the page method's return.
                if (data.hasOwnProperty("d"))
                    columns = $.parseJSON(data.d)
                else
                    columns = $.parseJSON(data);
            },
            results: function (data) {
                columns = $.parseJSON(data);
            }
        });

        $('#txtItem').on("change", function (e) {
            ItemSelectionChanged(e.added);
        })

        $('#txtItem').on("select2-removed", function (e) {
            $("#hdnCurrentSelectedItem").val("");
            $("#hdnHasKitAsChild").val("");
            document.getElementById("lkbItemRemoved").click();
        })

        $('#txtItem').select2(
            {
                placeholder: 'Select Item',
                minimumInputLength: varCharSearch,
                multiple: false,
                formatResult: formatItem,
                width: "89%",
                dropdownCssClass: 'bigdrop',
                dataType: "json",
                allowClear: true,
                ajax: {
                    quietMillis: 500,
                    url: '../common/Common.asmx/GetSearchedItems',
                    type: 'POST',
                    params: {
                        contentType: 'application/json; charset=utf-8'
                    },
                    dataType: 'json',
                    data: function (term, page) {
                        return JSON.stringify({
                            searchText: term,
                            pageIndex: page,
                            pageSize: varPageSize,
                            divisionId: getDivisionId(),
                            isGetDefaultColumn: false,
                            warehouseID: getvalue(),
                            searchOrdCusHistory: 0,
                            oppType: getOppType(),
                            isTransferTo: false,
                            IsCustomerPartSearch: $("#chkAutoCheckCustomerPart").prop("checked"),
                            searchType: $("#ddlSearchType").val()
                        });
                    },
                    results: function (data, page) {

                        if (data.hasOwnProperty("d")) {
                            if (data.d == "Session Expired") {
                                alert("Session expired.");
                                window.opener.location.href = window.opener.location.href;
                                window.close();
                            } else {
                                data = $.parseJSON(data.d);
                            }
                        }
                        else
                            data = $.parseJSON(data);

                        var more = (page.page * varPageSize) < data.Total;
                        return { results: $.parseJSON(data.results), more: more };
                    }
                }
            });

        $('#txtChildItem').select2({
            placeholder: 'Select Item',
            minimumInputLength: varCharSearch,
            multiple: false,
            formatResult: formatItem,
            width: "200px",
            dropdownCssClass: "bigdrop",
            dataType: "json",
            allowClear: true,
            ajax: {
                quietMillis: 500,
                url: '../common/Common.asmx/GetKitChildsFirstLevel',
                type: 'POST',
                params: {
                    contentType: 'application/json; charset=utf-8'
                },
                dataType: 'json',
                data: function (term, page) {
                    return JSON.stringify({
                        searchText: term,
                        pageIndex: page,
                        pageSize: varPageSize,
                        isGetDefaultColumn: false,
                        isAssembly: false,
                        itemCode: ($("[id$=hdnCurrentSelectedItem]").val() || 0)
                    });
                },
                results: function (data, page) {

                    if (data.hasOwnProperty("d")) {
                        if (data.d == "Session Expired") {
                            alert("Session expired.");
                            window.opener.location.href = window.opener.location.href;
                            window.close();
                        } else {
                            data = $.parseJSON(data.d)
                        }
                    }
                    else
                        data = $.parseJSON(data);

                    var more = (page.page * varPageSize) < data.Total;
                    return { results: $.parseJSON(data.results), more: more };
                }
            }
        });

        if ($("[id$=hdnSearchType]").val() != "") {
            $("#ddlSearchType").val($("[id$=hdnSearchType]").val());
        }
        $("#ddlSearchType").change(function () {
            $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/SavePersistTable',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "isMasterPage": true
                    , "boolOnlyURL": false
                    , "strParam": "frmNewOrderSearchType"
                    , "strPageName": ""
                    , "values": "[{\"key\": \"" + $("[id$=hdnSearchPageName]").val() + "\",\"value\" : \"" + $("#ddlSearchType").val() + "\"}]"
                }),
                error: function (jqXHR, textStatus, errorThrown) {
                    alert("Unknown error occurred.");
                }
            });
        });

        var cancelDropDownClosing = false;

        StopPropagation = function (e) {
            e.cancelBubble = true;
            if (e.stopPropagation) {
                e.stopPropagation();
            }
        }
        onDropDownClosing = function (sender, args) {
            cancelDropDownClosing = false;
        }

        FocuonItem();
    });

    function ItemSelectionChanged(item) {
        if ($("#gvMultiSelectItems").is(':visible')) {
            if ($("#<%=gvMultiSelectItems.ClientID %> tr").length != 0) {

                if (confirm('Before you can use the item autocomplete search, you first should add your multi-selected items or remove them from the select grid. Selecting' + "'OK'" + 'will remove your items.')) {
                    $("#gvMultiSelectItems").html("");
                    $("#divMultiSelect").hide();
                    return true;
                }
                else {
                    return false;
                }
            }
        }

        if (item != null) {
            $("#hdnCurrentSelectedItem").val(item.id.toString().split("-")[0]);
            $("#hdnHasKitAsChild").val(item.bitHasKitAsChild);
            $("#hdnIsSKUMatch").val(Boolean(item.bitSKUMatch));
            $("#txtCustomerPartNo").val(item.CustomerPartNo);
            if (item.bitHasKitAsChild) {
                Scroll(false);
            } else {
                Scroll(true);
            }
            document.getElementById("lkbItemSelected").click();

            if ($("#CustomerSection") != null) {
                $("#CustomerSection").collapse('hide');
            }
        } else {
            HideOtherTopSectionAfter1stItemAdded();
        }
    }

    function HideOtherTopSectionAfter1stItemAdded() {
        var lengthRows = $('table[id$=dgItems] > tbody > tr').not(':first').not(':last').length;

        if (lengthRows >= 1 && $("#CustomerSection") != null) {
            $("#CustomerSection").collapse('hide');
        }

        $("#divItem").show();
    }

    function getOppType() {
        return document.getElementById("hdnOppType").value;
    }

    function getDivisionId() {

        var radCmbCompany = $find('radCmbCompany');

        if (radCmbCompany != null) {
            if (radCmbCompany.get_value() != null && radCmbCompany.get_value().length > 0) {
                return radCmbCompany.get_value();
            }
            else {
                return 0;
            }
        }
        else {
            var hdnCmbDivisionID = document.getElementById("hdnCmbDivisionID");

            if (hdnCmbDivisionID != null) {
                if (hdnCmbDivisionID.value != null && hdnCmbDivisionID.value.length > 0) {
                    return hdnCmbDivisionID.value;
                }
            }
            else {
                return 0;
            }

        }
    }

    function getvalue() {
        if ($("[id$=hdnShipToWarehouseID]") != null && parseInt($("[id$=hdnShipToWarehouseID]").val()) > 0) {
            return $("[id$=hdnShipToWarehouseID]").val();
        } else if (document.getElementById("hdnOppType").value == "1" && parseInt($("[id$=hdnShipFromLocation]").val()) > 0) {
            return $("[id$=hdnShipFromLocation]").val();
        } else {
            var combo = $("[id$=hdnDefaultUserExternalLocation]");
            if (combo != null) {
                return combo.val();
            }
            else {
                return 0;
            }
        }
    }

    function formatItem(row) {
        var numOfColumns = 0;
        var ui = "<table style=\"width: 100%;font-size:12px;\" cellpadding=\"0\" cellspacing=\"0\">";
        ui = ui + "<tbody>";
        ui = ui + "<tr>";
        ui = ui + "<td style=\"width:auto; padding: 3px; vertical-align:top; text-align:center\">";
        if (row["vcPathForTImage"] != null && row["vcPathForTImage"].indexOf('.') > -1) {
            ui = ui + "<img id=\"Img7\" height=\"75\" width=\"75\" title=\"Item Image\" src=\"" + row["vcPathForTImage"] + "\" alt=\"Item Image\" >";
        } else {
            ui = ui + "<img id=\"Img7\" height=\"75\" width=\"75\" title=\"Item Image\" src=\"" + "../images/icons/cart_large.png" + "\" alt=\"Item Image\" >";
        }

        ui = ui + "</td>";
        ui = ui + "<td style=\"width: 100%; vertical-align:top\">";
        ui = ui + "<table style=\"width: 100%;\" cellpadding=\"0\" cellspacing=\"0\"  >";
        ui = ui + "<tbody>";
        ui = ui + "<tr>";
        $.each(columns, function (index, column) {
            if (numOfColumns == 4) {
                ui = ui + "</tr><tr>";
                numOfColumns = 0;
            }

            if (numOfColumns == 0) {
                ui = ui + "<td style=\"white-space: nowrap; margin-left:10px\"><strong>" + column.vcFieldName + ":</strong></td><td style=\"width:80%\">" + row[column.vcDbColumnName] + "</td>";
            }
            else {
                ui = ui + "<td style=\"white-space: nowrap; margin-left:10px\"><strong>" + column.vcFieldName + ":</strong></td><td style=\"width:20%\">" + row[column.vcDbColumnName] + "</td>";
            }
            numOfColumns += 2;
        });
        ui = ui + "</tr>";
        ui = ui + "</tbody>";
        ui = ui + "</table>";
        ui = ui + "</td>";
        ui = ui + "</tr>";
        ui = ui + "</tbody>";
        ui = ui + "</table>";

        return ui;
    }

    function AddChildItem() {
        var obj = $('#txtChildItem').select2('data');

        if (obj == null) {
            alert("Please Select Child Item")
            return false;
        }

        $("[id$=hdnSelectedChildItem]").val(obj.id.toString().split("-")[0]);
        $("[id$=btnAddChilItem]").click();

        return false;
    }

    function getCheckedItems(radCmbId) {
        var combo = $find(radCmbId);
        var selectedItemsValues = "";
        var items = combo.get_items();
        var itemsCount = items.get_count();

        for (var itemIndex = 0; itemIndex < itemsCount; itemIndex++) {
            var item = items.getItem(itemIndex);
            var checkbox = $telerik.findElement(item.get_element(), "chkSelect");

            if (checkbox.checked) {
                selectedItemsValues += item.get_value() + ",";
            }
        }

        return selectedItemsValues;
    }

    //This method removes the ending comma from a string.
    function removeLastComma(str) {
        return str.replace(/,$/, "");
    }

    function RemoveCurrentSelectedItem(itemcode) {
        new_data = $.grep($('#txtItem').select2('data'), function (value) {
            return value['id'].toString().split("-")[0] != itemcode;
        });

        $('#txtItem').select2('data', new_data);
        $('#imgPODistance').hide();
        $('#imgPODistance').removeAttr('onclick');
        //$('[id$=btnAdd]').removeAttr("disabled");
    }

    function SetItemUOMAndQuantity(itemcode, uomId, uom, quantity) {
        var item = $('#txtItem').select2('data');
        item.Attributes = vcAttributes;
        item.AttributeIDs = vcAttributeIDs;
        $('#txtItem').select2("data", item);
        $('#hdnTempSelectedItems').val(JSON.stringify($('#txtItem').select2('data')));
    }

    function SetItemAttributes(itemcode, numWarehouseItemID, vcAttributes, vcAttributeIDs) {
        var item = $('#txtItem').select2('data');
        item.numItemCode = itemcode;
        item.numWareHouseItemID = numWarehouseItemID;
        item.bitHasKitAsChild = Boolean(false);
        item.kitChildItems = "";
        item.Attributes = vcAttributes;
        item.AttributeIDs = vcAttributeIDs;
        $('#txtItem').select2("data", item);
        $('#hdnTempSelectedItems').val(JSON.stringify($('#txtItem').select2('data')));
    }

    function SetItemStockTransferWarehouse(itemCode, fromWarehouse, toWarehouse) {
        var items = $('#txtItem').select2('data');

        $.each($('#txtItem').select2('data'), function (i, obj) {
            if (obj.numItemCode == itemCode) {
                obj.numWareHouseItemID = fromWarehouse
                obj.numToWarehouseItemID = toWarehouse
            }
        });

        $('#txtItem').select2("data", items);
        $('#hdnTempSelectedItems').val(JSON.stringify($('#txtItem').select2('data')));

        $('#txtItem').select2('open');
    }

    function GetSelectedItems(isFromAddButton) {

        if ($find('radCmbCompany') != null && $find('radCmbCompany').get_value() == '') {
            alert("Select Company")
            return false;
        }

        if (document.getElementById("ddlContact") != null && ((document.getElementById("ddlContact").selectedIndex == -1) || (document.getElementById("ddlContact").value == 0))) {
            alert("Select Contact")
            document.getElementById("ddlContact").focus();
            return false;
        }

        if (document.getElementById("ddlOppType") != null && document.getElementById("ddlOppType").value == 0) {
            alert("Select Opportunity Type")
            document.getElementById("ddlOppType").focus();
            return false;
        }

        if ($("#txtHidEditOppItem").val() == "") {
            var plannedStart = $find("<%=rdpPlannedStart.ClientID%>");
            var requestedFinish = $find("<%=radCalCompliationDate.ClientID%>");

            if ($("[id$=chkWorkOrder]").is(":checked") && plannedStart != null && plannedStart.get_selectedDate() == null) {
                alert("Planned start date is required.")
                plannedStart.focus();
                return false;
            }

            if ($("[id$=chkWorkOrder]").is(":checked") && requestedFinish != null && requestedFinish.get_selectedDate() == null) {
                alert("Requested finish date is required.")
                requestedFinish.focus();
                return false;
            }

            if ($("[id$=chkWorkOrder]").is(":checked") && plannedStart != null && requestedFinish != null && requestedFinish.get_selectedDate() <= plannedStart.get_selectedDate()) {
                alert("Requested finish date must be atleast one day after planned start date.")
                requestedFinish.focus();
                return false;
            }
        }

        var obj = $('#txtItem').select2('data');

        if (obj == null && $("#txtHidEditOppItem").val() == "") {
            alert("Please select item.");
            return false;
        } else {
            var item = new Object();
            item.numItemCode = obj.numItemCode;
            item.numWareHouseItemID = obj.numWareHouseItemID;
            item.bitHasKitAsChild = Boolean(obj.bitHasKitAsChild);
            item.kitChildItems = obj.childKitItems;
            item.numQuantity = obj.numQuantity || 1;
            item.vcAttributes = obj.Attributes || "";
            item.vcAttributeIDs = obj.AttributeIDs || "";

            $("#hdnSelectedItems").val(JSON.stringify(item));

            if (isFromAddButton) {
                $("[id$=btnUpdate]").click();
            } else {
                return true;
            }
        }
    }

    function UpdateSelectedItems(items) {
        $('#txtItem').select2("data", items);
    }

    function OpenSetting() {
        window.open('../Items/frmConfItemList.aspx?FormID=123', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
        return false
    }

    function DropShip() {
        if ($ControlFind('chkDropShip').checked == true) {
            if ($ControlFind('divWarehouse') != null) {
                $ControlFind('divWarehouse').style.display = 'none';
            }
        }
        else {
            if ($ControlFind('divWarehouse') != null) {
                $ControlFind('divWarehouse').style.display = '';
            }
        }

    }

    function OpenKitSelectionWindow() {
        $("[id$=divEditKit]").show();
        return false;
    }

    function CloseEditKitChildWindow() {
        $("[id$=divEditKit]").hide();
        return false;
    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('#upnl').find('table').find('#txtdesc').on("blur", function () {
            var strLength;
            var strDesiredLength;
            strDesiredLength = 1500;
            strLength = jQuery(this).val().toString().length;
            if (strLength > strDesiredLength) {
                alert("You cannot insert characters more than " + strDesiredLength + ".");
                jQuery(this).focus();
                return false;
            }
        });
    });

    function reloadPage(type) {
        //alert("Success");
        //alert(type);
        if (type == 'add') {
            document.getElementById("hdnType").value = type;
            __doPostBack('btnBindOrderGrid', '');
            //document.getElementById("btnBindOrderGrid").click();
            return false;
        }
        else {
            document.getElementById("hdnType").value = '';
        }
        //window.location.reload();
    }

    function OpenInTransit(selectedItemCode, selectedWarehouseItemID) {
        var w = screen.width - 200;
        var h = screen.height - 100;
        var left = (screen.width / 2) - (w / 2);
        var top = (screen.height / 2) - (h / 2);
        window.open('../opportunity/frmOrderTransit.aspx?ItemCode=' + selectedItemCode + '&WarehouseItemID=' + selectedWarehouseItemID, '', 'toolbar=no,titlebar=no,width=' + w + ',height=' + h + ',top=' + top + ',left=' + left + ',scrollbars=yes,resizable=yes')
        return false;
    }

    function openVendorCostTable(a, itemCode, vendorID) {
        if (a == "2") {
            var w = screen.width;
            var h = 700;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            window.open("../common/frmManageVendorCostTable.aspx?IsUpdateDefaultVendorCostRange=0&VendorID=" + vendorID + "&ItemID=" + itemCode + "&VendorTCode=-1", '', 'toolbar=no,titlebar=no,width=' + w + ',height=' + h + ',top=' + top + ',left=' + left + ',scrollbars=yes,resizable=yes');
            return false;
        } else {
        window.open('../opportunity/frmVendorCostTable.aspx?ItemCode=' + itemCode + '&OppType=' + a + '&Vendor=' + document.getElementById('hdnVendorId').value, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=yes,resizable=yes')
            return false;
        }
    }

    function OpenRelatedItems(a, b, c, d, selectedItemCode) {

        if (d == 0) {
            alert("Select Company");
            return false;
        }

        window.open('../opportunity/frmOpenRelatedItems.aspx?ItemCode=' + selectedItemCode + '&OppType=' + a + '&PageType=' + b + '&opid=' + c + '&DivId=' + d, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=yes,resizable=yes')
        return false;
    }

    function BindOrderGrid() {
        document.getElementById("btnBindOrderGrid").click();
        return false;
    }

    function CheckCharacter(e) {
        var k;
        document.all ? k = e.keyCode : k = e.which;
        //document.Form1.txtdesc.value = document.Form1.txtdesc.value + '-' + k;
        if (k == 13) { //|| k == 106 Bug Fix Id 1744 #3
            if (e.preventDefault) {
                e.preventDefault();
            }
            else
                e.returnValue = false;
            return false;
        }
    }

    function NotAllowMessage(a) {
        if (a == 1)
            alert("You are not allowed to edit selected item, since it is being used in Authoritative BizDoc!");
        else if (a == 2)
            alert("You are not allowed to edit selected item, since it is being used in Project!");
        else if (a == 3)
            alert("You are not allowed to edit selected item, since it is being used in Workorder!");
        else if (a == 4)
            alert("You are not allowed to edit selected item, since serial/lot# assigned to item!");
        else if (a == 5)
            alert("You are not allowed to edit selected item, since it is being used in Fulfillment Order!");
        return false;
    }

    function ConfirmApplyCoupon() {
        if (confirm('Coupon Discount will be applied to the whole order. Do you wish to proceed ?')) {
            return true;
        }
        else {
            return false;
        }
    }

    function ConfirmShippingItem() {
        if (confirm('This will add selected shipping charges to the “Products & Services” section”.Do you want to proceed ?')) {
            return true;
        }
        else {
            return false;
        }
    }

    function DeleteAllConfirm() {
        if (confirm('All items are deleted. Do you want to proceed?')) {
            __doPostBack('DeleteAllItemsAndSave', '');
            return false;
        }
        else {
            return false;
        }
    }

    function Scroll(isEnabled) {
        if (isEnabled) {
            $('body').css('overflow', '');
        } else {
            $('body').css('overflow', 'hidden');
        }
    }

    function FocusStFrom() {
        var comboBox = $find("radcmbSTFromWarehouse");
        if (comboBox != null) {
            var input = comboBox.get_inputDomElement();
            if (input != null) {
                input.focus();
            }
        }
    }

    function RemovePromotion() {
        if (confirm('All the items where this promotion is applied will be removed. Do you want to proceed?')) {
            $("#btnRemovePromotion").click();
            return true;
        } else {
            return false;
        }
    }

    function DeleteItem(numOppItemID) {
        if (confirm('This items and all other items where this promotion is applied will be removed. Do you want to proceed?')) {
            $("#hdnDeleteItemID").val(numOppItemID);
            $("#btnDeleteItem").click();
        }

        /* Important - Always return false*/
        return false;
    }

    function hideUnitPriceDialog() {
        $("#dialogUnitPriceApproval").hide();
        return true;
    }

    function ValidateOnMultiSelect() {

        var radCmbCompany = ""
        var hdnCustAddr = ""
        if ($find('radCmbCompany') != null) {
            if ($find('radCmbCompany').get_value() == "") {
                alert("Select Company")
                return false;
            }
            else {
                radCmbCompany = $find('radCmbCompany').get_value();
                hdnCustAddr = $("#radCmbCompany").val();
            }
        }
        else {
            radCmbCompany = document.getElementById("hdnCmbDivisionID").value;
        }
        if (document.getElementById("ddlContact") != null && ((document.getElementById("ddlContact").selectedIndex == -1) || (document.getElementById("ddlContact").value == 0))) {
            alert("Select Contact")
            return false;
        }

        var child = window.open('../Items/frmItemList.aspx?CustId=' + radCmbCompany + '&CustAddrId=' + hdnCustAddr, '');
        window.external.comeback = function () {
            var back = confirm('Your multi Selected Items will be added.');
            if (back) {
                child.close();
                //  document.getElementById('<%= btn2.ClientID %>').click();
            } else {
                child.focus();
            }
        }

    }

    function onRadComboBoxLoad(sender) {
        var div = sender.get_element();

        if (sender.get_id() != "radcmbUOM") {

            $telerik.$(div).bind('mouseenter', function () {
                if (!sender.get_dropDownVisible()) {
                    sender.showDropDown();
                }
            });

            $telerik.$(".RadComboBoxDropDown").mouseleave(function (e) {
                hideDropDown("#" + sender.get_id(), sender, e);
            });

            $telerik.$(div).mouseleave(function (e) {
                hideDropDown(".RadComboBoxDropDown", sender, e);
            });
        }
    }

    function OpenNewItemPopup(url) {
        if (url != "") {
            OpenPopUp(url + "&isFromNewOrder=true");
        }
    }

    function SelectNewAddedItem(itemCode, itemName) {
        $("#txtItem").select2("search", itemName);
    }

    function OpenPopUp(url) {
        window.open(url, "", "toolbar = no, titlebar = no, left = 100, top = 100, width = 1200, height = 645, scrollbars = yes, resizable = yes");
    }

    function LoadNewKitItem(itemCode) {
        try {
            $("[id$=divKitChild]").show();

            $.ajax({
                type: "POST",
                url: '../common/Common.asmx/GetSearchedItems',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: JSON.stringify({
                    "searchText": ("preselectitemcode:" + itemCode.toString()),
                    "pageIndex": 1,
                    "pageSize": 10,
                    "divisionId": getDivisionId(),
                    "isGetDefaultColumn": false,
                    "warehouseID": getvalue(),
                    "searchOrdCusHistory": 0,
                    "oppType": 1,
                    "isTransferTo": false,
                    "IsCustomerPartSearch": false,
                    "searchType": $("#ddlSearchType").val()
                }),
                success: function (data) {
                    try {
                        if (data.hasOwnProperty("d")) {
                            if (data.d == "Session Expired") {
                                alert("Session expired.");
                                window.opener.location.href = window.opener.location.href;
                                window.close();
                            } else {
                                data = $.parseJSON(data.d)
                            }
                        }
                        else
                            data = $.parseJSON(data);

                        if ($.parseJSON(data.results).length > 0) {
                            $('#txtItem').select2("data", $.parseJSON(data.results)[0]);
                            ItemSelectionChanged($.parseJSON(data.results)[0]);
                        }

                        $("#divLoader").hide();
                    } catch (e) {
                        $("#divLoader").hide();
                        throw e;
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $("#divLoader").hide();
                    alert(textStatus);
                }
            });
        } catch (e) {
            $("#divLoader").hide();
            alert("Unkown error occurred while selecting new created item")
        }
    }
</script>
<style type="text/css">
    .text-warning {
        color: #de8b0d;
        font-style: italic;
    }

    .btn-xs {
        padding: 4px !important;
        font-size: 13px !important;
    }

    .btn-warning {
        background-color: #eca12f !important;
    }

    .nav-tabs {
        border-top: 4px solid #1473b4;
        background: #1473b4;
        border-radius: 4px;
        border-left: 4px solid #1473b4;
    }

    .nav-tabs {
        border-bottom: 1px solid #ddd;
    }

    .nav {
        padding-left: 0;
        margin-bottom: 0;
        list-style: none;
    }

    .nav-tabs > li {
        float: left;
        margin-bottom: -1px;
    }

    .nav > li {
        position: relative;
        display: block;
    }

    .nav-tabs > li.active > a {
        color: #1473b4 !important;
    }

        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
            color: #555;
            cursor: default;
            background-color: #fff;
            border: 1px solid #ddd;
            border-bottom-color: transparent;
        }

    .nav-tabs > li > a {
        color: #fff !important;
    }

    .nav-tabs > li > a {
        margin-right: 2px;
        line-height: 1.42857143;
        border: 1px solid transparent;
        border-radius: 4px 4px 0 0;
    }

    .nav > li > a {
        position: relative;
        display: block;
        padding: 10px 15px;
    }

    .tab-content {
        border: 1px solid #1473b4;
        border-top: 0px;
    }

    .table-bordered {
        border: 1px solid #e4e4e4;
    }

    .table-bordered {
        border: 1px solid #ddd;
    }

    .table {
        width: 100%;
        max-width: 100%;
        margin-bottom: 20px;
    }

    table {
        background-color: transparent;
    }

    table {
        border-spacing: 0;
        border-collapse: collapse;
    }

    .table-striped > tbody > tr:nth-of-type(odd) {
        background-color: #f9f9f9;
    }

    .table-bordered > tbody > tr > th {
        border-bottom-width: 2px;
    }

    .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
        border: 1px solid #e4e4e4;
    }

    .table > tbody > tr > th, .table > thead > tr > td {
        background-color: #ebebeb;
        border-bottom: 2px solid #e4e4e4;
    }

    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        border-top: 1px solid #e4e4e4;
    }

    .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
        border: 1px solid #ddd;
    }

    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }

    .table-bordered th {
        text-align: center;
    }

    .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
        border: 1px solid #e4e4e4;
    }

    .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
        border-top: 1px solid #e4e4e4;
    }

    .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
        border: 1px solid #ddd;
    }

    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }

    .pager-shipping span, .pager-shipping a {
        font-size: 14px;
        font-weight: bold;
        padding: 5px;
    }

    .multipleRowsColumns .rcbItem, .multipleRowsColumns .rcbHovered {
        float: left;
        margin: 0 1px;
        min-height: 13px;
        overflow: hidden;
        padding: 2px 19px 2px 6px; /*width: 125px;*/
    }

    .rcbHeader ul, .rcbFooter ul, .rcbItem ul, .rcbHovered ul, .rcbDisabled ul, .ulItems {
        width: 100%;
        display: inline-block;
        margin: 0;
        padding: 0;
        list-style-type: none;
    }

    .rcbList {
        display: inline-block;
        width: 100%;
        list-style-type: none;
    }


    .RadComboBox {
        width: 100%;
        margin-top: 8px !important;
        vertical-align: middle;
    }

    .RadComboBox, .rcbInputCell, .rcbInput {
        width: 100%;
        background: none repeat scroll 0% 0% transparent;
        border: 0px none;
        vertical-align: middle;
        outline: 0px none;
        margin: 0px !important;
    }

    .col1, .col2, .col3, .col4, .col5, .col6 {
        float: left;
        width: 80px;
        margin: 0;
        padding: 0 5px 0 0;
        line-height: 14px;
        display: block; /*border:0px solid red;*/
    }

    .col1 {
        width: 240px;
    }

    .col2 {
        width: 200px;
    }

    .col3 {
        width: 80px;
    }

    .col4 {
        width: 80px;
    }

    .col5 {
        width: 85px;
    }

    .col6 {
        width: 80px;
    }

    .dialog-header {
        position: relative;
        text-indent: 1em;
        height: 4em;
        line-height: 4em;
        background-color: #f7f7f7;
    }

    .dialog-body {
        padding: .5em;
        word-break: break-all;
        overflow-y: auto;
    }

    .RadComboBoxDropDown .rcbHeader, .RadComboBoxDropDown .rcbFooter {
        padding: 5px 7px 4px;
        border-width: 0;
        border-style: solid;
        background-image: none !important;
    }

    .KitChildItem .rcbItem {
        padding: 2px !important;
        border-bottom: 1px solid #d8d8d8;
    }

    .KitChildItem .rcbHeader {
        font-weight: bold;
        text-align: center;
    }

    .bigdrop {
        width: 600px !important;
    }

    .btn.btn-flat {
        border-radius: 0;
        -webkit-box-shadow: none;
        -moz-box-shadow: none;
        box-shadow: none;
        border-width: 1px;
        color: white;
        font-weight: bold;
    }

    .bg-purple {
        background-color: #605ca8 !important;
    }

    .bg-olive {
        background-color: #3d9970 !important;
    }

    .bg-orange {
        background-color: #ff851b !important;
    }

    .bg-aqua {
        background-color: #00c0ef !important;
    }

    .bg-maroon {
        background-color: #d81b60 !important;
    }


    .btn {
        display: inline-block;
        padding: 6px 12px;
        margin-bottom: 0;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857143;
        text-align: center;
        vertical-align: middle;
        -ms-touch-action: manipulation;
        cursor: pointer;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
        color: #fff;
    }

    .duplicateitem table td input {
        display: block;
        width: 20px !important;
        padding: 0px !important;
        margin: 0px !important;
        border: 1px solid #ccc;
        font-size: 12px;
        min-width: 20px;
        height: 20px !important;
    }

    .duplicateitem {
        height: 20px !important;
        width: 20px !important;
        border: 0px !important;
    }

    /*.btn-success.active.focus, .btn-success.active:focus, .btn-success.active:hover, .btn-success:active.focus, .btn-success:active:focus, .btn-success:active:hover, .open > .dropdown-toggle.btn-success.focus, .open > .dropdown-toggle.btn-success:focus, .open > .dropdown-toggle.btn-success:hover {
        color: #fff;
        background-color: #398439;
        border-color: #255625;
    }*/

    .btn-success {
        color: #fff;
        background-color: #398439;
        border-color: #255625;
    }

    .btn-danger {
        color: #fff;
        background-color: #d9534f;
        border-color: #d43f3a;
    }

    .table-bordered {
        border: 1px solid #ddd;
    }

    .table {
        width: 100%;
        max-width: 100%;
        margin-bottom: 20px;
    }

    .table-striped > tbody > tr:nth-of-type(odd) {
        background-color: #f9f9f9;
    }

    .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
        border: 1px solid #ddd;
    }

    .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
        padding: 8px;
        line-height: 1.42857143;
        vertical-align: top;
        border-top: 1px solid #ddd;
    }

    table#grdShippingInformation td {
        padding: 3px;
    }

    .rcbSlide {
        z-index: 1000000 !important;
    }

    #tblKitChild label {
        padding-left: 5px;
        font-weight: normal;
    }

    #divKitChild img.ul-child-kit-item-image {
        margin: 0 auto;
    }

    #divKitChild ul.ul-child-kit-item {
        font-size: 16px;
    }

    #divKitChild ul.ul-child-kit-item {
        font-size: 16px;
    }

    #divKitChild li.ul-child-kit-item-name {
        font-weight: bold;
    }

    #divKitChild .slick-track {
        display: flex !important;
    }

    #divKitChild .slick-item {
        background-color: #fff;
        border: 1px solid #dbdbdb;
        margin: 0 17px;
        height: inherit !important;
        outline: none;
    }

        #divKitChild .slick-item:hover, #divKitChild .slick-item:focus {
            background-color: #efefef;
            color: #282828;
            border: 1px solid #000;
        }

        #divKitChild .slick-item.active {
            background-color: #efefef;
            color: #282828;
            border: 1px solid #dbdbdb !important;
        }

    .slick-prev:before, .slick-next:before {
        font-family: FontAwesome;
        font-size: 20px;
        line-height: 1;
        color: #9e9e9e;
        opacity: 0.75;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
    }

    .slick-prev:before {
        content: "\f053";
    }

    [dir="rtl"] .slick-prev:before {
        content: "\f054";
    }

    .slick-next:before {
        content: "\f054";
    }

    [dir="rtl"] .slick-next:before {
        content: "\f053";
    }

    #chkDropShip {
        margin-top: 0px !important;
        margin-right: 4px;
    }
</style>
<div class="cont1" style="border-bottom: 0px !important; padding-bottom: 0px !important;">
    <div class="col-1-1" style="float: left">
        <asp:UpdatePanel ID="UpdatePanelItem" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
            <ContentTemplate>
                <div class="col-1-1" id="divItem" runat="server">
                    <div class="form-group">
                        <label class="col-0-12">
                            <a id="lkbNewItem" title="New Item" href="javascript:OpenPopUp('../Items/frmAddItem.aspx?isFromNewOrder=true')" style="text-decoration: none">
                                <asp:Image ID="Image5" ImageUrl="~/images/AddRecord.png" runat="server" Style="margin-right: 2px" />
                            </a>
                            <img id="imgPODistance" style="display: none; height: 35px; float: left; margin-top: -16px;" src="../images/vendordistance.png" />
                            Item <span class="required">*</span>
                        </label>
                        <div class="col-12-12">
                            <table border="0" width="100%">
                                <tr>
                                    <td style="width: 90px">
                                        <select id="ddlSearchType" class="select2-choice" style="width: 85px;">
                                            <option value="1" selected="selected">Contains</option>
                                            <option value="2">Starts-with</option>
                                            <option value="3">Ends-with</option>
                                        </select>
                                        <asp:HiddenField runat="server" ID="hdnSearchPageName" />
                                        <asp:HiddenField runat="server" ID="hdnSearchType" />
                                    </td>
                                    <td style="width: 71%;">
                                        <asp:TextBox ID="txtItem" ClientIDMode="Static" runat="server" TabIndex="300"></asp:TextBox>
                                        
                                    </td>
                                    <td style="float: left;">
                                        <input type="button" value="Add Item" runat="server" id="btnAddItem" onclick="return GetSelectedItems(true);" style="padding: 4px; margin-left: 5px; margin-top: 0px; margin-bottom: 0px;" class="btn btn-primary" />
                                        <asp:Button ID="btnMultiSelect" Text="Multi-Select" class="btn btn-primary" Style="text-align: center; padding: 4px; margin: 0px;" OnClientClick="return ValidateOnMultiSelect();" runat="server"></asp:Button>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="left">
                                        <asp:CheckBox ID="chkAutoCheckCustomerPart" runat="server" />&nbsp;Customer Part#
                                    </td>
                                </tr>
                            </table>

                        </div>
                    </div>
                </div>

            </ContentTemplate>
            <Triggers>
            </Triggers>
        </asp:UpdatePanel>
    </div>
</div>
<asp:UpdatePanel ID="upnl" runat="server" ChildrenAsTriggers="true">
    <ContentTemplate>

        <div id="divMultiSelect" visible="false" runat="server">
            <table width="100%">
                <tr>
                    <td>
                        <asp:GridView ID="gvMultiSelectItems" HeaderStyle-CssClass="" CssClass="tblPrimary" FooterStyle-CssClass="gridFooter"
                            OnRowDataBound="gvMultiSelectItems_RowDataBound" OnRowDeleting="gvMultiSelectItems_RowDeleting" OnRowCommand="gvMultiSelectItems_RowCommand"
                            runat="server" RowStyle-CssClass="gridViewRow" AlternatingRowStyle-CssClass="gridViewAlternateRow" PageSize="10" AutoGenerateColumns="false"
                            AllowPaging="true" OnPageIndexChanging="gvMultiSelectItems_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField HeaderStyle-Width="20%" ItemStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        <label>Item Name</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnID" Value='<%# DataBinder.Eval(Container.DataItem, "ID")%>' runat="server" />
                                        <asp:HiddenField ID="hdnItemCode" Value='<%# DataBinder.Eval(Container.DataItem, "numItemCode")%>' runat="server" />
                                        <asp:HiddenField ID="hdnItemName" Value='<%# DataBinder.Eval(Container.DataItem, "ItemName")%>' runat="server" />
                                        <asp:HiddenField ID="hdnAttr" Value='<%# DataBinder.Eval(Container.DataItem, "Attr")%>' runat="server" />
                                        <asp:Label ID="lblMultiSelectItemNAttr" runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        <label>Units</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtMultiSelectUnits" AutoPostBack="true" OnTextChanged="txtMultiSelectUnits_TextChanged" Text='<%# DataBinder.Eval(Container, "DataItem.numUnitHour", "{0:#,##0.00}") %>' Width="90%" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        <label>Unit Price</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtMultiSelectUnitPrice" AutoPostBack="true" OnTextChanged="txtMultiSelectUnitPrice_TextChanged" Text='<%# DataBinder.Eval(Container, "DataItem.numUnitHour", "{0:#,##0.00}") %>' Width="90%" runat="server"></asp:TextBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="15%">
                                    <HeaderTemplate>
                                        <label>Item Release Date</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <telerik:RadDatePicker ID="rdMultiSelectItemReleaseDate" AutoPostBack="true" runat="server" Width="90%" OnSelectedDateChanged="rdMultiSelectItemReleaseDate_SelectedDateChanged"></telerik:RadDatePicker>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-Width="25%">
                                    <HeaderTemplate>
                                        <label>Ship-From</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <telerik:RadComboBox ID="rdcmbMultiSelectShipFrom" OnItemDataBound="rdcmbMultiSelectShipFrom_ItemDataBound" Width="95%" ExpandDirection="Down" runat="server" DropDownWidth="800px"
                                            AccessKey="W" AutoPostBack="true" DropDownCssClass="multipleRowsColumns" OnClientLoad="onRadComboBoxLoad" OnSelectedIndexChanged="rdcmbMultiSelectShipFrom_SelectedIndexChanged">
                                            <HeaderTemplate>
                                                <ul>
                                                    <li class="col1">Warehouse</li>
                                                    <li class="col2">Attributes</li>
                                                    <li class="col3">On Hand</li>
                                                    <li class="col4">On Order</li>
                                                    <li class="col5">On Allocation</li>
                                                    <li class="col6">BackOrder</li>
                                                </ul>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <ul>
                                                    <li style="display: none">
                                                        <%# DataBinder.Eval(Container.DataItem, "numWareHouseItemId")%></li>
                                                    <li class="col1">
                                                        <%# DataBinder.Eval(Container.DataItem, "vcWareHouse")%></li>
                                                    <li class="col2">
                                                        <%# DataBinder.Eval(Container.DataItem, "Attr") %>&nbsp;</li>
                                                    <li class="col3">
                                                        <%# DataBinder.Eval(Container.DataItem, "numOnHand")%>&nbsp;
                                            <%# DataBinder.Eval(Container.DataItem, "vcUnitName")%></li>
                                                    <li class="col4">
                                                        <%# DataBinder.Eval(Container.DataItem, "numOnOrder")%>
                                            &nbsp;</li>
                                                    <li class="col5">
                                                        <%# DataBinder.Eval(Container.DataItem, "numAllocation")%>&nbsp;</li>
                                                    <li class="col6">
                                                        <%# DataBinder.Eval(Container.DataItem, "numBackOrder")%>&nbsp;
                                        <asp:Label runat="server" ID="lblWareHouseID" Text='<%#DataBinder.Eval(Container.DataItem, "numWareHouseID")%>'
                                            Style="display: none"></asp:Label>
                                                        <asp:Label runat="server" ID="lblWarehouse" Text='<%#DataBinder.Eval(Container.DataItem, "vcWareHouse")%>'
                                                            Style="display: none"></asp:Label>

                                                        <asp:Label runat="server" ID="lblOnHandAllocation" Style="display: none"></asp:Label>
                                                    </li>
                                                </ul>
                                            </ItemTemplate>
                                        </telerik:RadComboBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-Width="20%">
                                    <HeaderTemplate>
                                        <label>Ship-To</label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <telerik:RadComboBox ID="rdcmbMultiSelectShipTo" AutoPostBack="true" OnSelectedIndexChanged="rdcmbMultiSelectShipTo_SelectedIndexChanged" Width="95%" ExpandDirection="Down" runat="server"></telerik:RadComboBox>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-Width="5%">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgDeleteMultiSelectItem" CommandName="Delete" CommandArgument="<%# Container.DataItemIndex %>" runat="server" ImageUrl="~/images/delete2.gif" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <div>
                            <asp:Button ID="btnAddtoOpp" OnClick="btnAddtoOpp_Click" CssClass="btn btn-primary" runat="server" Text="Add to Opportunity/Order & Close" />
                            <asp:Button ID="btnAddMoreItems" OnClientClick="return ValidateOnMultiSelect();" CssClass="btn btn-primary" runat="server" Text="Add more Items" />
                            <asp:Button ID="btnClearItems" OnClick="btnClearItems_Click" CssClass="btn btn-primary" runat="server" Text="Clear Items & Start Over" />
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <asp:LinkButton ID="lkbItemSelected" runat="server" Style="display: none"></asp:LinkButton>
        <asp:LinkButton ID="lkbItemRemoved" runat="server" Style="display: none"></asp:LinkButton>

        <div class="cont1" style="border-top: 0px !important;">
            <div class="col-1-1">
                <div class="col-1-3">
                    <div class="form-group">
                        <label class="col-3-12">
                            <asp:HyperLink ID="hplUnits" runat="server" CssClass="hyperlink">Units</asp:HyperLink><span class="required">*</span>
                        </label>
                        <asp:TextBox ID="txtUnits" runat="server" CssClass="col-3-12" Style="margin-right: 3px;" Width="50px" Text="1" AutoPostBack="true" TabIndex="302"></asp:TextBox>
                        <telerik:RadComboBox ID="radcmbUOM" runat="server" CssClass="form-control col-9-12" AutoPostBack="true" TabIndex="307" Width="120"></telerik:RadComboBox>

                        <asp:TextBox ID="txtOldUnits" CssClass="signup" Style="display: none" runat="server"></asp:TextBox>
                        <asp:TextBox ID="txtnumUnitHourReceived" runat="server" Style="display: none" />
                        <asp:TextBox ID="txtnumQtyShipped" runat="server" Style="display: none" />
                        <asp:TextBox ID="txtUOMConversionFactor" runat="server" Style="display: none" />
                    </div>
                    <div class="form-group text-warning" id="divMinOrderQty" runat="server" visible="false">
                        <label class="col-3-12">Min Order Qty</label>
                        <div class="col-9-12" style="padding-top: 7px">
                            <asp:Label ID="lblMinOrderQty" runat="server" Text="-"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="col-1-3">
                    <div class="form-group" id="divPrice" runat="server" style="margin-bottom:0px">
                        <label class="col-3-12">
                            <asp:HyperLink ID="hplPrice" runat="server" CssClass="hyperlink">Unit Price</asp:HyperLink><span class="required">*</span></label>
                        <div class="col-9-12">
                            <asp:TextBox ID="txtprice" runat="server" CssClass="wid100 float-left" MaxLength="11" AutoPostBack="true" TabIndex="303"></asp:TextBox>
                            <span class="textdiv">/<asp:Label ID="lblBaseUOMName" runat="server" Text="-"></asp:Label></span>
                        </div>
                    </div>
                    <div class="form-group text-warning">
                        <label class="col-3-12"></label>
                        <asp:Label ID="lblDynamicCostUpdatedDate" runat="server"></asp:Label>
                    </div>
                </div>
                <div class="col-1-3">
                    <div class="form-group">
                        <label class="col-3-12">
                            <div style="float: left">
                                <asp:CheckBox ID="chkDropShip" Text="Drop Ship" runat="server" />
                            </div>
                            Type
                        </label>
                        <div class="col-9-12">
                            <asp:Label ID="lblItemType" Font-Bold="true" Style="font-style: italic;padding-top: 7px;display: inline-block;" runat="server"></asp:Label>
                            <asp:DropDownList ID="ddltype" runat="server" Enabled="False" Visible="false">
                                <asp:ListItem Value="P">Inventory Item</asp:ListItem>
                                <asp:ListItem Value="N">Non-Inventory Item</asp:ListItem>
                                <asp:ListItem Value="S">Service</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-1-1" id="tdSalesProfit" runat="server" visible="false">
                <div class="col-1-3">
                    <ul class="list-inline">
                        <li>
                            <asp:Label ID="lblDiscount" Text="Discount" runat="server" Style="float: left; width: 60px;"></asp:Label>
                            <asp:DropDownList ID="ddlMarkupDiscountOption" Style="float: left; width: 80px;" runat="server">
                                <asp:ListItem Text="- Discount" Value="0"></asp:ListItem>
                                <asp:ListItem Text="+ Markup" Value="1"></asp:ListItem>
                            </asp:DropDownList></li>
                        <li>
                            <asp:TextBox ID="txtItemDiscount" runat="server" CssClass="float-left" Width="50px" TabIndex="304"></asp:TextBox></li>
                        <li>
                            <table>
                                <tr>
                                    <td>
                                        <asp:RadioButton ID="radPer" runat="server" GroupName="radDiscount" Checked="true" Text="%" TabIndex="305" /></td>
                                    <td>
                                        <asp:RadioButton ID="radAmt" runat="server" GroupName="radDiscount" Text="Amount Entered" TabIndex="306" /></td>
                                    <td>
                                        <asp:CheckBox ID="chkUsePromotion" runat="server" Text="Promotion" />
                                    </td>
                                </tr>
                            </table>
                        </li>
                    </ul>
                </div>
                <div class="col-1-3">
                    <div class="form-group" id="divUnitCost" runat="server" visible="false">
                        <label class="col-3-12 unit-cost">
                            <asp:HyperLink ID="hplUnitCost" NavigateUrl="#" CssClass="hyperlink" runat="server" TabIndex="524">Unit-Cost</asp:HyperLink></label>
                        <div class="col-9-12">
                            <asp:TextBox ID="txtPUnitCost" runat="server" TabIndex="525" Width="50" AutoPostBack="true"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-1-3">
                    <div class="form-group" id="divProfit" runat="server" visible="false">
                        <label class="col-3-12">Profit - Unit/Total</label>
                        <div class="col-9-12">
                            <div style="float: left">
                                <asp:Label ID="lblProfit" class="textdiv" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-1-1">
                <div class="col-1-6">
                    <table style="margin-left: 5px;" id="tablePurchaseIncentiveLabel" visible="false" runat="server">
                        <tr>
                            <td>
                                <img src="../images/Deal_icon.png" style="height: 40px;" />
                            </td>
                            <td>
                                <asp:Literal ID="lblPurchaseIncentives" runat="server" Text=""></asp:Literal>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-1-6">
                    <div class="form-group" id="divOpenPO" visible="false" runat="server">
                        <label class="col-3-12">Open PO Totals</label>
                        <asp:Label ID="lblOpenPOToals" runat="server" Text=""></asp:Label>
                    </div>
                </div>
            </div>
            <div class="col-1-5">
            </div>

            <div class="col-1-1">
                <div class="col-1-2">
                    <div class="form-group">
                        <label class="col-3-12" style="width: 24%">Description</label>
                        <div class="col-9-12">
                            <asp:TextBox ID="txtdesc" runat="server" TextMode="MultiLine"></asp:TextBox>
                        </div>
                    </div>
                    <div class="form-group" id="divVendorDetail" runat="server" visible="false">
                        <label class="col-3-12" style="width: 24%">Notes</label>
                        <div class="col-9-12">
                            <asp:TextBox ID="txtNotes" runat="server" TextMode="MultiLine" Rows="2"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-1-2" id="divWarehouse" runat="server">
                    <div class="form-group">
                        <label class="col-3-12" style="width: 24%" id="tdWareHouseLabel" runat="server">Warehouse<span class="required">*</span></label>
                        <div class="col-9-12">
                            <telerik:RadComboBox ID="radWareHouse" runat="server" Width="100%" AccessKey="W" AutoPostBack="true" ClientIDMode="Static">
                            </telerik:RadComboBox>
                        </div>
                    </div>
                    <table class="table table-bordered tblPrimary">
                        <thead>
                            <tr>
                                <th>Available <i> (On-Hand)</i></th>
                                <th>Allocation</th>
                                <th>On-Order</th>
                                <th>Back Order</th>
                                <th><asp:Label runat="server" ID="lblDateType" Text="Item Release Date"></asp:Label></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="gvItemsrow" style="font-weight:bold">
                                <td>
                                    <asp:Label runat="server" ID="lblOnHand"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblOnAllocation"></asp:Label>
                                </td>
                                <td>
                                    <asp:Hyperlink runat="server" ID="hplOnOrder" ForeColor="Blue" Font-Underline="true"></asp:Hyperlink>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblOnBackOrder"></asp:Label>
                                </td>
                                <td>
                                    <telerik:RadDatePicker runat="server" id="rdpItemDate" Width="100" DateInput-Width="80"></telerik:RadDatePicker>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <asp:GridView ID="gvLocations" RowStyle-CssClass="gvItemsrow" CssClass="" AlternatingRowStyle-CssClass="gvItemsAlternaterow" runat="server" Width="100%" HeaderStyle-CssClass="gridHeader" FooterStyle-CssClass="gridFooter" AutoGenerateColumns="false" DataKeyNames="numWareHouseItemId">
                        <Columns>
                            <asp:TemplateField HeaderStyle-Width="7%">
                                <HeaderTemplate>
                                    
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <b style="color: #00a65a;"><%# Eval("numOnHand")%></b> (<i><%# Convert.ToDouble(Eval("numOnHand")) + Convert.ToDouble(Eval("numAllocation"))%></i>)
                                    <asp:HiddenField ID="hdnOnHand" runat="server" Value='<%# Eval("numOnHand") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Allocation" HeaderStyle-Width="5%" DataField="numAllocation" />
                            <asp:BoundField HeaderText="On-Order" HeaderStyle-Width="7%" DataField="numOnOrder" />
                            <asp:BoundField HeaderText="Back Order" HeaderStyle-Width="8%" DataField="numBackOrder" />
                            <asp:TemplateField HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="Center">
                                <HeaderTemplate>
                                    <label>Qty</label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:TextBox ID="txtLocationsQty" Width="90%" runat="server"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="12%">
                                <HeaderTemplate>
                                    <label>Item Required Date</label>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblReleaseDateLocation" runat="server"></asp:Label>
                                    <telerik:RadDatePicker ID="rdLocationItemReleaseDate" runat="server" Width="90%"></telerik:RadDatePicker>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
            <div class="col-1-1 text-center">
                <asp:LinkButton ID='hplRelatedItems' runat="server" Style="padding-left: 30px" CssClass="hyperlink" Text="Related Items" NavigateUrl="#" Visible="false"></asp:LinkButton>
                <asp:LinkButton ID="hplLastPrices" runat="server" CssClass="hyperlink" Style="padding-left: 30px" Visible="false">Last Prices</asp:LinkButton>
            </div>
            <div class="col-1-1">
                <div class="col-1-3">
                    <div class="form-group" runat="server" id="trWarehouseTransferTo" visible="false">
                        <label class="col-3-12">Transfer To</label>
                        <div class="col-9-12">
                            <telerik:RadComboBox ID="radWarehouseTo" runat="server" Width="100%" DropDownWidth="630px"
                                AccessKey="T" AutoPostBack="true" ClientIDMode="Static">
                                <HeaderTemplate>
                                    <table style="width: 550px; text-align: left">
                                        <tr>
                                            <td></td>
                                            <td class="normal1" style="width: 125px;">Warehouse
                                            </td>
                                            <td class="normal1" style="width: 200px;">Attributes
                                            </td>
                                            <td class="normal1" style="width: 50px;">On Hand
                                            </td>
                                            <td class="normal1" style="width: 50px;">On Order
                                            </td>
                                            <td class="normal1" style="width: 75px;">On Allocation
                                            </td>
                                            <td class="normal1" style="width: 50px;">BackOrder
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table style="width: 550px; text-align: left">
                                        <tr>
                                            <td style="display: none">
                                                <%#DataBinder.Eval(Container.DataItem, "numWareHouseItemId")%>
                                            </td>
                                            <td style="width: 125px;">
                                                <%#DataBinder.Eval(Container.DataItem, "vcWareHouse")%>
                                            </td>
                                            <td style="width: 200px;">
                                                <%#DataBinder.Eval(Container.DataItem, "Attr")%>
                                            </td>
                                            <td style="width: 50px;">
                                                <%#DataBinder.Eval(Container.DataItem, "numOnHand")%>
                                                <%# DataBinder.Eval(Container.DataItem, "vcUnitName")%>
                                            </td>
                                            <td style="width: 50px;">
                                                <%#DataBinder.Eval(Container.DataItem, "numOnOrder")%>
                                            </td>
                                            <td style="width: 75px;">
                                                <%#DataBinder.Eval(Container.DataItem, "numAllocation")%>
                                            </td>
                                            <td style="width: 50px;">
                                                <%#DataBinder.Eval(Container.DataItem, "numBackOrder")%>
                                                <asp:Label runat="server" ID="Label1" Text='<%#DataBinder.Eval(Container.DataItem, "numWareHouseID")%>'
                                                    Style="display: none"></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </telerik:RadComboBox>
                        </div>
                    </div>
                </div>
                <div class="col-1-3" id="divCustomerPartNo" runat="server">
                    <div class="form-group">
                        <label class="col-3-12">Customer Part#</label>
                        <div class="col-9-12">
                            <asp:TextBox ID="txtCustomerPartNo" runat="server" TabIndex="507"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-1-3">
                    <div class="form-group">
                        <label class="col-3-12"></label>
                        <div class="col-9-12">
                            <asp:CheckBox ID="chkSearchOrderCustomerHistory" Text="Search within customer history"
                                runat="server" CssClass="signup" AutoPostBack="true" Font-Bold="false" />
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-1-1">
                <div class="form-group" id="spnPriceLevel" runat="server" visible="false">
                    <label class="col-3-12" style="width: 17.66%; padding-right: 10px;">Price Level / Rule</label>
                    <div class="col-9-12" style="width: 82.34%;">
                        <asp:DropDownList ID="ddlPriceLevel" runat="server" AutoPostBack="true">
                            <asp:ListItem Value="0">--Select One--</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>


            <div class="col-1-1 text-center" style="padding-top: 10px; padding-bottom: 10px;" id="divPricingOption" runat="server" visible="false">
                <asp:LinkButton ID="lkbPriceLevel" runat="server" CssClass="btn btn-flat bg-purple">Use Price Level</asp:LinkButton>
                <asp:LinkButton ID="lkbPriceRule" runat="server" CssClass="btn btn-flat bg-orange">Use Price Rule</asp:LinkButton>
                <asp:LinkButton ID="lkbLastPrice" runat="server" CssClass="btn btn-flat bg-olive">Use Last Price</asp:LinkButton>
                <asp:LinkButton ID="lkbListPrice" runat="server" CssClass="btn btn-flat bg-aqua">Use List Price</asp:LinkButton>
                <asp:LinkButton ID="lkbEditKitSelection" CssClass="btn btn-flat bg-maroon" Visible="false" runat="server" Text="Edit Kit Selection" OnClientClick="return OpenKitSelectionWindow();"></asp:LinkButton>
                <asp:LinkButton ID="lkbAddPromotion" runat="server" CssClass="btn btn-flat bg-maroon" Visible="false">Use Promotion</asp:LinkButton></span>

                <asp:HiddenField ID="hdnPricingBasedOn" runat="server" />
                <asp:HiddenField ID="hdnListPrice" runat="server" />
                <asp:HiddenField ID="hdnLastPrice" runat="server" />
                <asp:HiddenField ID="hdnLastOrderDate" runat="server" />
            </div>
            <div class="col-1-1 text-center" id="divWorkOrder" runat="server" visible="false">
                <asp:HiddenField ID="hdnIsCreateWO" runat="server" Value="0" />
                <ul class="list-inline">
                    <li style="vertical-align: top; padding-left: 0px; padding-right: 0px; margin-top: 2px;">
                        <asp:CheckBox ID="chkWorkOrder" runat="server" />
                    </li>
                    <li style="vertical-align: top; padding-left: 0px; padding-right: 0px; margin-top: 5px;">Create Work Order</li>
                    <li style="vertical-align: top; padding-left: 0px; padding-right: 0px; margin-top: 5px;">For
                    </li>
                    <li style="vertical-align: top; padding-left: 0px; padding-right: 0px">
                        <asp:TextBox ID="txtWOQty" runat="server" Width="60"></asp:TextBox>
                    </li>
                    <li style="vertical-align: top; padding-left: 0px; padding-right: 0px; margin-top: 5px;">qty</li>
                    <li style="vertical-align: top; padding-left: 0px; padding-right: 0px; margin-top: 5px;">(Max WO Qty:</li>
                    <li style="vertical-align: top; padding-left: 0px; padding-right: 0px;">
                        <asp:Label ID="lblWOQty" runat="server" Text="" Style="line-height: 27px"></asp:Label></li>
                    <li style="vertical-align: top; padding-left: 0px; padding-right: 0px; margin-top: 5px;">Planned Start<span style="color: red"> *</span>
                    </li>
                    <li style="vertical-align: top; padding-left: 0px; padding-right: 0px">
                        <telerik:RadDatePicker ID="rdpPlannedStart" runat="server" DateInput-DateFormat="MM/dd/yyyy" TabIndex="515">
                            <Calendar>
                                <SpecialDays>
                                    <telerik:RadCalendarDay Repeatable="Today" ItemStyle-CssClass="rcToday">
                                    </telerik:RadCalendarDay>
                                </SpecialDays>
                            </Calendar>
                        </telerik:RadDatePicker>
                    </li>
                    <li style="vertical-align: top; padding-left: 0px; padding-right: 0px; margin-top: 5px;">Requested Finish<span style="color: red"> *</span>
                    </li>
                    <li style="vertical-align: top; padding-left: 0px; padding-right: 0px">
                        <telerik:RadDatePicker ID="radCalCompliationDate" runat="server" DateInput-DateFormat="MM-dd-yyyy" AutoPostBack="true">
                            <Calendar>
                                <SpecialDays>
                                    <telerik:RadCalendarDay Repeatable="Today" ItemStyle-CssClass="rcToday">
                                    </telerik:RadCalendarDay>
                                </SpecialDays>
                            </Calendar>
                        </telerik:RadDatePicker>
                    </li>
                    <li style="vertical-align: top; padding-left: 0px; padding-right: 0px; margin-top: 5px;">Comments
                    </li>
                    <li style="vertical-align: top; padding-left: 0px; padding-right: 0px">
                        <asp:TextBox ID="txtInstruction" runat="server" TextMode="MultiLine" MaxLength="1000"></asp:TextBox>
                    </li>
                </ul>
            </div>
            <div class="col-1-1" id="trWorkOrder" runat="server" visible="false">
            </div>
            <div class="col-1-1" style="font-size: 14px;">
                <asp:HiddenField ID="hdnPromotionID" runat="server" />
                <asp:HiddenField ID="hdnPromotionTriggered" runat="server" />
                <asp:HiddenField ID="hdnPromotionDescription" runat="server" />
                <asp:HiddenField ID="hdnMinQuanity" runat="server" />
                <asp:HiddenField ID="hdnCouponNotRequire" Value="0" runat="server" />
                <asp:HiddenField ID="hdnPromotionName" Value="0" runat="server" />
                <div class="col-1-1" id="divPromotionDetail" runat="server" visible="false">
                    <div class="col-9-12">
                        <div id="divPromotionDesription" runat="server" visible="false">
                            <div style="float: left; padding-right: 5px">
                                <img src="../images/Admin-Price-Control.png" height="40" alt="" />
                            </div>
                            <div>
                                <b>Promotion:</b><asp:Label ID="lblPromotionName" runat="server" Text=""></asp:Label><asp:Label ID="lblExpiration" runat="server" Text=""></asp:Label><br />
                                <b>Details:</b><asp:Label ID="lblPromotionDescription" runat="server" Text=""></asp:Label><br />

                            </div>
                        </div>
                    </div>
                    <div class="col-3-12">
                        <div class="form-inline" id="divCouponCode" runat="server" visible="false">
                            <label class="col-3-12">Coupon Code</label>
                            <div class="col-9-12">
                                <asp:TextBox ID="txtCouponCode" AutoPostBack="true" OnTextChanged="txtCouponCode_TextChanged" runat="server"></asp:TextBox>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-1-1" id="divPromotionShippingDetail" runat="server" visible="false">
                    <div style="float: left; padding-right: 5px">
                        <img src="../images/TruckGreen.png" height="40" alt="" />
                    </div>
                    <div style="float: left; padding-top: 10px;">
                        <asp:Label ID="lblShippingDetail" runat="server" Text=""></asp:Label>
                    </div>
                </div>
            </div>
            <asp:HiddenField ID="hdnBaseUOMName" runat="server" />
            <asp:HiddenField ID="hdnVendorCost" runat="server" />
            <asp:HiddenField ID="hdnItemClassification" runat="server" />
            <div class="col-1-1 text-center">
                <asp:Button AccessKey="A" ID="btnUpdate" runat="server" CssClass="OrderButton" Style="display: none" Text="Add" OnClientClick="return GetSelectedItems(false);"></asp:Button>

                <asp:Button ID="btnEditCancel" runat="server" Text="Cancel" Style="display: none" CssClass="OrderButton"></asp:Button>
                <asp:Button ID="btnOptionsNAccessories" CssClass="button" runat="server" Text="Customize" Visible="false" />
                <asp:Button ID="btnRemovePromotion" runat="server" Text="" Style="display: none;" />
            </div>
        </div>
        <asp:HiddenField ID="hdnDeleteItemID" runat="server" />
        <asp:Button ID="btnDeleteItem" runat="server" Text="" Style="display: none;" />
        <div class="cont1 no-pad">
            <div class="col-1-1" style="font-weight: bold; text-align: center;">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </div>
            <div class="col-1-1">
                <asp:DataGrid ID="dgItems" runat="server" CssClass="tblPrimary" UseAccessibleHeader="true" FooterStyle-CssClass="gridFooter" ItemStyle-CssClass="gridItem" AlternatingItemStyle-CssClass="gridAlternateItem" ShowFooter="true" AutoGenerateColumns="false" DataKeyField="numItemCode"
                    ClientIDMode="AutoID" Width="100%">
                    <Columns>
                        <asp:TemplateColumn HeaderText="Line" ItemStyle-Wrap="false" FooterStyle-Wrap="false">
                            <ItemTemplate>
                                <asp:TextBox ID="txtRowOrder" onkeypress="CheckNumber(2,event)" AutoPostBack="true" OnTextChanged="txtRowOrder_TextChanged" Width="40" Text='<%# DataBinder.Eval(Container,"DataItem.numSortOrder") %>' runat="server"></asp:TextBox>
                                <asp:HiddenField ID="hdnPromotionID" runat="server" Value='<%#Eval("numPromotionID")%>' />
                                <asp:HiddenField ID="hdnIsPromotionTrigerred" runat="server" Value='<%#Eval("bitPromotionTriggered")%>' />
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:LinkButton ID="lnkBtnUpdateOrder" Style="color: #fff;" OnClick="lnkBtnUpdateOrder_Click" runat="server">A-Z</asp:LinkButton>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn HeaderText="Item ID" DataField="numItemCode" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"></asp:BoundColumn>
                        <%-- <asp:BoundColumn DataField="vcItemName" HeaderText="Item" ItemStyle-Width="100%"></asp:BoundColumn>  --%>
                        <asp:TemplateColumn HeaderText="Item" ItemStyle-CssClass="duplicateitem" FooterStyle-Wrap="false">
                            <ItemTemplate>
                                <table style="width: 100%">
                                    <tr>
                                        <td style="width: 98%">
                                            <asp:Label ID="lblItemName" Width="400px" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcItemName") %>'></asp:Label>
                                        </td>
                                        <td class="duplicateitem" style="width: 2% !important; border: 0px !important; height: 20px !important;">
                                            <asp:ImageButton ID="imgDuplicateLineItem" Height="20px" Width="20px" OnClick="imgDuplicateLineItem_Click" CssClass="duplicateitem" ImageUrl="~/images/CloneDoc.png" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="vcItemDesc" HeaderText="Description"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="SKU" DataField="vcSKU"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Warehouse" DataField="Warehouse"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Units" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtUnits" CssClass="signup" Width="40" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.numUnitHour", "{0:#,##0.00}") %>' OnTextChanged="txtUnitsGrid_TextChanged"></asp:TextBox>
                                        </td>
                                        <td>
                                            <%# DataBinder.Eval(Container,"DataItem.vcUOMName") %>
                                        </td>
                                    </tr>
                                </table>
                                <asp:TextBox ID="txtOldUnits" CssClass="signup" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.numUnitHour") %>'></asp:TextBox>
                                <asp:TextBox ID="txtUOMConversionFactor" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.UOMConversionFactor") %>'>></asp:TextBox>
                                <asp:TextBox ID="txtnumUnitHourReceived" runat="server" Style="display: none" Text='<%#Eval("numUnitHourReceived")%>' />
                                <asp:TextBox ID="txtnumQtyShipped" runat="server" Style="display: none" Text='<%#Eval("numQtyShipped")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                            <HeaderTemplate>
                                <asp:Label ID="lblUnitPriceCaption" runat="server">Unit Price</asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtUnitPrice" CssClass="signup" Width="40" runat="server"></asp:TextBox>
                                            <asp:HiddenField ID="hdnUnitPrice" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.monPrice")%>' />
                                        </td>
                                        <td>/
                                            <asp:Label ID="lblUOMName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcBaseUOMName")%>'></asp:Label>
                                            <asp:Label ID="lblItemUOM" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcUOMName")%>'></asp:Label>
                                        </td>
                                        <td>&nbsp;<asp:Label ID="lblUnitPriceApproval" runat="server" ForeColor="Red" Font-Bold="true" Text='<%# IIf(Not IsDBNull(DataBinder.Eval(Container, "DataItem.bitItemPriceApprovalRequired")) AndAlso DataBinder.Eval(Container, "DataItem.bitItemPriceApprovalRequired") = "True", "!", "")%>' />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <FooterTemplate>
                                Total :
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Sales Tax" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" ItemStyle-Wrap="false" HeaderStyle-Wrap="false" FooterStyle-Wrap="False">
                            <ItemTemplate>
                                <asp:Label ID="lblTaxAmt0" runat="server" CssClass="text"></asp:Label>
                                <asp:Label ID="lblTaxable0" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container, "DataItem.bitTaxable0", "{0:#,##0.00}") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblFTaxAmt0" runat="server" CssClass="text" Text=""></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Discount" ItemStyle-HorizontalAlign="Right" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                            <ItemTemplate>
                                <asp:HiddenField ID="hdnMarkupDiscount" runat="server" />
                                <asp:HiddenField ID="hdnDBMarkupDiscount" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.bitMarkupDiscount") %>' />
                                <asp:Label ID="lblDiscount" runat="server" CssClass="text"></asp:Label>
                                <asp:TextBox ID="txtDiscountType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.bitDiscountType", "{0:#,##0.00}") %>' Style="display: none"></asp:TextBox>
                                <asp:TextBox ID="txtTotalDiscount" runat="server" Style="display: none"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Total" HeaderStyle-Wrap="false" FooterStyle-Wrap="False" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" ItemStyle-Wrap="false">
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblTotal" runat="server" CssClass="text" Text='<%# DataBinder.Eval(Container, "DataItem.monTotAmount", "{0:#,##0.00}")%>'></asp:Label>
                                        </td>
                                        <td>&nbsp;<asp:Label ID="lblAmountApproval" runat="server" ForeColor="Red" Font-Bold="true" Text='<%# IIf(Not IsDBNull(DataBinder.Eval(Container, "DataItem.bitItemPriceApprovalRequired")) AndAlso DataBinder.Eval(Container, "DataItem.bitItemPriceApprovalRequired") = "True", "!", "")%>' />
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Label ID="lblFTotal" runat="server" CssClass="text" Text=""></asp:Label>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkEdit" CommandName="Edit" CssClass="hyperlink" runat="server">
                                    <asp:Image ID="Image3" runat="server" ImageUrl="~/images/pencil-24.gif" Height="15" />
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkDelete" CommandName="Delete" CssClass="hyperlink" runat="server">
                                    <asp:Image ID="Image4" runat="server" ImageUrl="~/images/delete2.gif" Height="15" />
                                </asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="No" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblOppItemCode" runat="server" CssClass="text" Text='<%#Eval("numoppitemtCode")%>'></asp:Label>
                                <asp:Label ID="lblID" runat="server" CssClass="text" Text='<%# "!!" + Eval("numItemCode").ToString() + "," + Eval("numWarehouseItmsID").ToString() + "," + Eval("DropShip").ToString() %>'
                                    Style="display: none"></asp:Label>
                                <asp:Label ID="lblcharItemType" runat="server" CssClass="text" Text='<%#Eval("charItemType")%>'
                                    Style="display: none"></asp:Label>
                                <asp:Label ID="lblnumProjectID" runat="server" CssClass="text" Text='<%#Eval("numProjectID")%>'
                                    Style="display: none"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </div>
        </div>

        <div class="cont2">
            <div id="divShipping" runat="server" class="col-1-1" visible="false">
                <div class="col-1-1">
                    <div class="col-1-2">
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-2-12">Ship From</label>
                            <div class="col-10-12" style="padding-top: 7px">
                                <asp:Label ID="lblShipFrom" runat="server"></asp:Label>
                            </div>
                        </div>
                        <asp:HiddenField ID="hdnWillCallWarehouseID" runat="server" />
                    </div>
                    <div class="col-1-2">
                        <div class="form-group" style="margin-bottom: 5px;">
                            <ul class="list-inline" style="margin-bottom: 0px;">
                                <li style="padding-left: 0px; padding-right: 0px">
                                    <asp:CheckBox runat="server" ID="chkMarkupShippingCharges" /></li>
                                <li><b>Mark up shipping charges</b></li>
                                <li>
                                    <asp:TextBox runat="server" ID="txtMarkupShippingRate" Width="35"></asp:TextBox></li>
                                <li>
                                    <asp:RadioButtonList ID="rblMarkupType" RepeatLayout="Flow" RepeatDirection="Horizontal" runat="server">
                                        <asp:ListItem Text="%" Selected="True" Value="1" />
                                        <asp:ListItem Text="Flat Amt" Selected="True" Value="2" />
                                    </asp:RadioButtonList>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-1-1">
                    <div class="col-1-2">
                        <div class="form-group" style="margin-bottom: 5px;">
                            <label class="col-2-12">Ship To</label>
                            <div class="col-10-12" style="padding-top: 7px">
                                <asp:Label ID="lblShipTo" runat="server"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <div class="col-1-2">
                        <div class="form-group" style="margin-bottom: 5px;">
                            <ul class="list-inline" style="margin-bottom: 0px;">
                                <li style="padding-left: 0px; padding-right: 0px">
                                    <asp:CheckBox runat="server" ID="chkUseCustomerShippingAccount" onchange="ValidateShipperAccount()" /></li>
                                <li><b>Use Customer's shipping account</b></li>
                                <li style="padding-right: 0px"><b>Total Weight:</b></li>
                                <li style="padding-left: 0px;">
                                    <asp:TextBox ID="txtTotalWeight" runat="server" Width="50"></asp:TextBox></li>
                                <li style="padding-left: 0px; padding-right: 0px">lbs</li>
                            </ul>
                            <asp:HiddenField runat="server" ID="hdnShipperAccountNo" />
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <ul class="nav nav-tabs" style="border-bottom: 0px; border-radius: 4px 4px 0px 0px;">
                        <li class="active"><a data-toggle="tab" href="#ddlParcel" aria-expanded="true">Parcel</a></li>
                        <div class="pull-right">
                            <div class="pull-left">
                                <label style="color: #fff;">Ship Via</label>
                                <telerik:RadComboBox runat="server" ID="radShipVia" AccessKey="I" Skin="Vista" Height="130px" Width="130px" DropDownWidth="290"
                                    EnableVirtualScrolling="false" CssClass="form-control" AutoPostBack="true" ChangeTextOnKeyBoardNavigation="false" ClientIDMode="Static" OnSelectedIndexChanged="radShipVia_SelectedIndexChanged">
                                </telerik:RadComboBox>
                            </div>
                            <div class="pull-left">
                                <label style="color: #fff; padding-left: 10px;">Shipping Estimates</label>
                                <telerik:RadComboBox ID="radCmbShippingMethod" CssClass="form-control" Width="300px" runat="server" TabIndex="530"></telerik:RadComboBox>
                            </div>
                            <div class="pull-left" style="padding-right: 10px">
                                <asp:Button ID="btnCalculateShipping" CssClass="btn btn-warning btn-flat btn-xs" Style="margin-top: 1px; margin-left: 2px;"
                                    runat="server" Text="Get Rates" OnClientClick="javascript:return Save();" TabIndex="535" />
                            </div>
                        </div>
                    </ul>
                    <div class="tab-content" style="padding: 5px;">
                        <div id="ddlParcel" class="tab-pane fade active in">
                            <asp:GridView ID="grdShippingInformation" CssClass="table table-bordered" AutoGenerateColumns="false" AllowSorting="true" OnSorting="OnSorting" OnPageIndexChanging="OnPageIndexChanging" AllowPaging="true" PageSize="10" ShowHeaderWhenEmpty="true" EmptyDataText="No record found" runat="server" PagerSettings-Mode="NumericFirstLast" PagerSettings-FirstPageText="<<" PagerSettings-LastPageText=">>" PagerStyle-CssClass="pager-shipping">
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="200" HeaderText="Carrier">
                                        <ItemTemplate>
                                            <asp:Image runat="server" ID="ImgCatStatus" Height="28" ImageUrl='<%#Eval("ShippingCarrierImage") %>' />
                                            <asp:Label ID="lblShippingCompanyName" runat="server" Text='<%#Eval("vcShippingCompanyName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="vcServiceName" HeaderText="Service" ItemStyle-Width="300" />
                                    <asp:BoundField DataField="AnticipateDelivery" HeaderText="Anticipated Delivery" ItemStyle-Width="150" />
                                    <asp:BoundField DataField="ReleaseDate" HeaderText="Release Date" ItemStyle-Width="150" />
                                    <asp:BoundField DataField="ExpectedDate" HeaderText="Expected Date" ItemStyle-Width="150" />
                                    <asp:TemplateField HeaderText="Price">
                                        <ItemTemplate>
                                            <asp:Label ID="lblShippingAmount" runat="server" Text='<%#"$" + Eval("ShippingAmount") %>'></asp:Label>

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-Width="100" HeaderText="">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hdnShipViaID" runat="server" Value='<%#Eval("numShippingCompanyID")%>' />
                                            <asp:HiddenField ID="hdnShipServiceID" runat="server" Value='<%#Eval("intNsoftEnum") %>' />
                                            <asp:HiddenField ID="hdnShipServiceName" runat="server" Value='<%#Eval("vcServiceName")%>' />
                                            <asp:HiddenField ID="hdnShipRate" runat="server" Value='<%#Eval("ShippingAmount")%>' />
                                            <asp:HiddenField ID="hdnReleaseDate" runat="server" Value='<%#Eval("ReleaseDate")%>' />
                                            <asp:Button ID="btnAddOrder" runat="server" CssClass="btn btn-flat btn-xs btn-primary" CommandName="AddShippingRate" Text="Add to order" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </div>

                </div>

                <asp:HiddenField ID="hdnShipVia" runat="server" />
                <asp:HiddenField ID="hdnShippingService" runat="server" />

                <asp:HiddenField ID="hdnShipFromState" runat="server" />
                <asp:HiddenField ID="hdnShipFromCountry" runat="server" />
                <asp:HiddenField ID="hdnShipFromPostalCode" runat="server" />

                <asp:HiddenField ID="hdnShipToState" runat="server" />
                <asp:HiddenField ID="hdnShipToCountry" runat="server" />
                <asp:HiddenField ID="hdnShipToPostalCode" runat="server" />
            </div>
            <div class="col-1-1">
                <div style="float: none; margin-top: 10px; text-align: center">
                    <asp:HiddenField ID="hdnKitChildItems" runat="server" />
                    <asp:Button ID="btnSave" CssClass="OrderButton SaveOpenOrder" Style="width: 142px; padding-right: 10px;" runat="server" Text="Save & Create Invoice" />
                    <asp:Button ID="btnSaveOpenOrder" CssClass="OrderButton SaveOpenOrder" Style="width: 142px; padding-right: 10px;" runat="server" Text="Save & Open" />
                    <asp:Button ID="btnSaveNew" CssClass="OrderButton SaveNew" Style="width: 142px; padding-right: 10px;" runat="server" Text="Save & New" />
                    <asp:Button ID="btnClose" runat="server" CssClass="OrderButton CancelOrder" ToolTip="Cancel" />
                    <asp:Button ID="btnAddToCart" runat="server" CssClass="OrderButton Cart" Width="106" Text="Add to cart" Visible="false" OnClientClick="return Save(1);" />
                </div>
                <div style="float: right" runat="server" id="divItemGridColumnSetting">
                    <a href="#" onclick="return OpenSetting()" title="Show Item Grid Settings.">
                        <img src="../images/settings30x30.png" alt=""></a>
                </div>
            </div>
        </div>
        <div class="cont1">
            <div class="float-left">
            </div>
            <div class="float-right total-cont">
                <div class="totWid">
                    <div>
                        <asp:Label ID="lblShippingCharges" runat="server" Style="width: 180px !important" Text=""></asp:Label><asp:Label ID="lblShippingCostCurrency" runat="server"></asp:Label><asp:Label ID="lblShippingCost" runat="server" CssClass="text"></asp:Label>
                    </div>
                    <div>

                        <asp:Label ID="lblCurrencyTotal" runat="server" Style="width: 180px !important" Text=""></asp:Label><asp:Label ID="lblTotalCurrency" runat="server"></asp:Label><asp:Label ID="lblTotal" CssClass="text" runat="server"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <div class="overlay1" id="dialogUnitPriceApproval" runat="server" visible="false">
            <div class="overlayContent" style="color: #000; background-color: #FFF; width: 420px; padding: 10px; border: 2px solid black;">
                <p style="text-align: center">The unit price you’ve entered for one or more items requires approval.</p>
                <p style="text-align: center">
                    <asp:Button ID="btnApproveUnitPrice" CssClass="btn btn-primary" ForeColor="White" runat="server" Text="Approve Unit Price" Visible="false" OnClientClick="return hideUnitPriceDialog();" />
                    <asp:Button ID="btnSaveAndProceed" CssClass="btn btn-primary" ForeColor="White" runat="server" Text="Save & Proceed" OnClientClick="return hideUnitPriceDialog();" />
                    <asp:Button ID="btnClear" CssClass="btn btn-primary" ForeColor="White" runat="server" Text="Clear" OnClientClick="return hideUnitPriceDialog();" />
                    <asp:HiddenField runat="server" ID="hdnIsUnitPriceApprovalRequired" />
                    <asp:HiddenField ID="hdnClickedButton" runat="server" />
                    <asp:HiddenField runat="server" ID="hdnApprovalActionTaken" />
                </p>
            </div>
        </div>
        <div class="overlay1" id="divKitChild" runat="server" style="display: none">
            <div class="overlayContent" style="color: #000; background-color: #FFF; width: 97%; height: 96%; margin: 20px; border: 1px solid #ddd; overflow: hidden">
                <div class="dialog-header" style="height: 70px">
                    <div style="float: left">
                        <img alt="" src="../images/ItemConfig.png" style="vertical-align: middle; padding-right: 5px; max-height: 50px" id="imgKit" />
                        <b style="font-size: 14px">
                            <label id="lblKitItem"></label>
                        </b>
                        &nbsp;&nbsp;
                            <label id="lblKitDescription" style="max-width: 400px; line-height: 20px"></label>
                        <b>CONFIGURED PRICE:</b>
                        <label id="lblKitCalculaedPrice" style="color: #00b050; font-size: 18px">$ 0.00</label>
                    </div>
                    <div style="float: right; padding: 7px;">
                        <asp:HiddenField runat="server" ID="hdnKitSingleSelect" />
                        <asp:HiddenField ID="hdnSelectedText" runat="server"></asp:HiddenField>
                        <asp:Button ID="btnSaveCloseKit" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="Add Item" OnClientClick="return GetChildKitItemSelection();" />
                        <input type="button" class="btn btn-primary" value="Create New Kit" onclick="CreateNewItem(false,true,true);" />
                        <input type="button" class="btn btn-primary" value="Create New Assembly" onclick="CreateNewItem(true,false,true);" />
                    </div>
                </div>
                <div class="dialog-body" style="overflow-y: auto; max-height: 94%; margin-top: 1px;">
                    <div style="text-align: center">
                        <asp:Label ID="lblKitChildError" runat="server" ForeColor="Red" Text=""></asp:Label>
                    </div>
                    <div class="panel-group" id="divChildKitContainer"></div>
                </div>
            </div>
        </div>
        <div class="overlay1" id="divItemAttributes" runat="server" style="display: none">
            <div class="overlayContent" style="color: #000; background-color: #FFF; width: 400px; border: 1px solid #ddd;">
                <div class="dialog-header">
                    <b style="font-size: 14px;">Select Item Attributes?</b>
                    <div style="float: right; padding: 7px;">
                        <asp:Button ID="btnSaveCloseAttributes" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="Save & Close" />
                    </div>
                </div>
                <div class="dialog-body">
                    <div style="text-align: center">
                        <asp:Label ID="lblAttributeError" runat="server" Text="" ForeColor="Red"></asp:Label>
                    </div>
                    <asp:Repeater ID="rptAttributes" runat="server">
                        <HeaderTemplate>
                            <table style="width: 100%; padding: 0px; border-spacing: 10px;">
                        </HeaderTemplate>
                        <ItemTemplate>
                            <tr style="min-height: 30px;">
                                <td style="white-space: nowrap; text-align: right">
                                    <asp:Label ID="lblAttributeName" runat="server" Text='<%# Eval("Fld_label")%>' Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    <asp:HiddenField ID="hdnFldID" runat="server" Value='<%# Eval("Fld_id")%>' />
                                    <asp:HiddenField ID="hdnListID" runat="server" Value='<%# Eval("numlistid")%>' />
                                    <asp:DropDownList ID="ddlAttributes" runat="server" Width="150"></asp:DropDownList>
                                </td>
                            </tr>
                        </ItemTemplate>
                        <FooterTemplate>
                            </table>
                        </FooterTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>


        <asp:HiddenField ID="hdnHasKitAsChild" runat="server" />
        <asp:TextBox ID="txtSerialize" runat="server" Style="display: none"></asp:TextBox>
        <asp:TextBox ID="txtDivID" runat="server" Style="display: none"></asp:TextBox>
        <asp:TextBox ID="txtHidValue" runat="server" Style="display: none"></asp:TextBox>
        <asp:TextBox ID="txtTax" runat="server" Style="display: none"></asp:TextBox>
        <input id="Taxable" runat="server" type="hidden" />
        <input id="TaxItemsId" runat="server" type="hidden" />
        <input id="hdKit" runat="server" type="hidden" />
        <asp:HiddenField ID="hdnBillAddressID" runat="server" />
        <asp:HiddenField ID="hdnShipAddressID" runat="server" />
        <asp:HiddenField ID="hdnOrderCreateDate" runat="server" />
        <asp:HiddenField ID="hdnVendorId" runat="server" />
        <asp:HiddenField ID="hdnPrimaryVendorID" runat="server" />
        <asp:HiddenField ID="hdnUnitCost" runat="server" />
        <asp:HiddenField ID="hdnSearchOrderCustomerHistory" runat="server" Value="0" />
        <asp:TextBox ID="txtModelID" runat="server" Style="display: none">
        </asp:TextBox>
        <asp:HiddenField ID="hdvPurchaseUOMConversionFactor" runat="server" Value="1" />
        <asp:HiddenField ID="hdvSalesUOMConversionFactor" runat="server" Value="1" />
        <asp:HiddenField ID="hdnPOS" runat="server" Value="0" />
        <asp:TextBox ID="txtHidEditOppItem" runat="server" Style="display: none"></asp:TextBox>
        <asp:Button ID="btnBindOrderGrid" runat="server" CssClass="button" Text="BindOrderGrid"
            Style="display: none" />

        <asp:HiddenField ID="hdnCountry" runat="server" Value="0" />
        <asp:HiddenField ID="hdnState" runat="server" Value="0" />
        <asp:HiddenField ID="hdnCity" runat="server" Value="0" />
        <asp:HiddenField ID="hdnPostal" runat="server" Value="0" />
        <asp:HiddenField ID="hdnItemWeight" runat="server" Value="0" />
        <asp:HiddenField ID="hdnFreeShipping" runat="server" Value="0" />
        <asp:HiddenField ID="hdnHeight" runat="server" Value="0" />
        <asp:HiddenField ID="hdnWidth" runat="server" Value="0" />
        <asp:HiddenField ID="hdnLength" runat="server" Value="0" />
        <asp:HiddenField ID="hdnSKU" runat="server" Value="" />
        <asp:Button ID="btnGo" runat="server" Visible="false" />
        <asp:HiddenField ID="hdnType" runat="server" />
        <asp:HiddenField ID="hdnCurrentSelectedItem" runat="server" />
        <asp:HiddenField ID="hdnIsMatrix" runat="server" />
        <asp:HiddenField ID="hdnIsSKUMatch" runat="server" />
        <asp:HiddenField ID="hdnCurrentSelectedItemWarehouse" runat="server" />
        <asp:HiddenField ID="hdnCurrentSelectedItemVendor" runat="server" />
        <asp:HiddenField ID="hdnChangedProfitPercent" runat="server" />
        <asp:HiddenField ID="hdnEditUnitPriceRight" runat="server" />
        <asp:HiddenField ID="hdnSelectedItems" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnOppType" ClientIDMode="Static" runat="server" />
        <asp:HiddenField ID="hdnTempSelectedItems" runat="server" />
        <asp:HiddenField ID="hdnTempSelectedItemWarehouse" runat="server" />
        <asp:HiddenField ID="hdnOppItemCodeToUpdatePrice" runat="server" />
        <asp:HiddenField ID="hdnItemQuantity" runat="server" />
        <asp:HiddenField ID="hdnEnabledItemLevelUOM" runat="server" />
        <asp:HiddenField ID="hdnTempPurchaseUOM" runat="server" />
        <asp:HiddenField ID="hdnDefaultUserExternalLocation" runat="server" />
        <asp:HiddenField ID="hdnShipFromLocation" runat="server" />
        <asp:HiddenField ID="hdnReleaseDate" runat="server" />
        <asp:HiddenField ID="hdnExpectedDate" runat="server" />
        <asp:HiddenField ID="hdnIsAutoWarehouseSelection" runat="server" />
        <asp:HiddenField ID="hdnWarehousePriority" runat="server" />
        <asp:HiddenField runat="server" ID="hdnPortalURL"></asp:HiddenField>
    </ContentTemplate>
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="radWareHouse" />
        <asp:AsyncPostBackTrigger ControlID="radWarehouseTo" />
        <asp:AsyncPostBackTrigger ControlID="txtunits" />
        <asp:AsyncPostBackTrigger ControlID="txtPUnitCost" />
        <asp:AsyncPostBackTrigger ControlID="txtprice" />
        <asp:AsyncPostBackTrigger ControlID="btnUpdate" />
        <asp:AsyncPostBackTrigger ControlID="btnEditCancel" />
        <asp:AsyncPostBackTrigger ControlID="btnOptionsNAccessories" />
        <asp:AsyncPostBackTrigger ControlID="chkWorkOrder" />
        <asp:AsyncPostBackTrigger ControlID="ddlPriceLevel" />
        <asp:AsyncPostBackTrigger ControlID="radcmbUOM" />
        <asp:AsyncPostBackTrigger ControlID="dgItems" />
        <asp:AsyncPostBackTrigger ControlID="btnClear" />
        <asp:PostBackTrigger ControlID="btnSave" />
        <asp:PostBackTrigger ControlID="btnSaveOpenOrder" />
        <asp:PostBackTrigger ControlID="btnSaveNew" />
        <asp:PostBackTrigger ControlID="btnClose" />
        <asp:PostBackTrigger ControlID="btnAddToCart" />
        <asp:PostBackTrigger ControlID="btnSaveAndProceed" />
        <asp:PostBackTrigger ControlID="btnApproveUnitPrice" />
        <asp:AsyncPostBackTrigger ControlID="lkbPriceLevel" />
        <asp:AsyncPostBackTrigger ControlID="lkbPriceRule" />
        <asp:AsyncPostBackTrigger ControlID="lkbLastPrice" />
        <asp:AsyncPostBackTrigger ControlID="lkbListPrice" />
        <asp:AsyncPostBackTrigger ControlID="lkbItemRemoved" />
        <asp:AsyncPostBackTrigger ControlID="btnDeleteItem" />
        <asp:AsyncPostBackTrigger ControlID="btnRemovePromotion" />
        <asp:AsyncPostBackTrigger ControlID="lkbItemSelected" />
        <asp:AsyncPostBackTrigger ControlID="radcmbShippingMethod" />
        <asp:AsyncPostBackTrigger ControlID="btnCalculateShipping" />
        <asp:AsyncPostBackTrigger ControlID="radShipVia" />
    </Triggers>
</asp:UpdatePanel>
<asp:UpdatePanel ID="UpdatePanelActionWindow" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
    <ContentTemplate>
        <div class="overlay1" id="divRelatedItems" runat="server" style="display: none">
            <div class="overlayContent" style="color: #000; background-color: #FFF; width: 1000px; border: 1px solid #ddd;">
                <div class="dialog-header">
                    <%--<b style="font-size: 14px;">Related Items</b>--%>
                    <div class="col-1-1">
                        <div class="col-9-12" id="divPromotionDetailRI" runat="server" visible="false">
                            <div id="divPromotionDesriptionRI" runat="server">
                                <div style="float: left; padding-right: 5px; line-height: 1; display: inline;">
                                    <img src="../images/Admin-Price-Control.png" height="30" alt="" /><b>Promotion: </b>
                                    <asp:Label ID="lblPromotionNameRI" runat="server" Text=""></asp:Label><asp:Label ID="lblExpirationRI" runat="server" Text=""></asp:Label><br />
                                    b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Details: </b><asp:Label ID="lblPromotionDescriptionRI" runat="server" Text=""></asp:Label><br />
                                    <div style="display: -webkit-inline-box;">
                                        <label runat="server" id="lblCouponText" visible="false" style="padding-top: 10px;">Coupon Code:&nbsp; </label>
                                        <asp:TextBox runat="server" ID="txtCouponCodeRI" Visible="false" Width="100px"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style="float: right; padding: 7px;">
                            <asp:Button ID="btnCloseRelatedItems" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="Close Window" />
                        </div>
                    </div>
                    <asp:HiddenField ID="hdnRelatedItemPromotionID" runat="server" />
                </div>
                <div class="dialog-body">
                    <asp:PlaceHolder ID="plhPromotion" runat="server" Visible="false">
                        <b>Promotion Alert:</b>
                        <br />
                        <div style="float: left; padding-right: 5px">
                            <img src="../images/Admin-Price-Control.png" height="40" alt="" />
                        </div>
                        <div style="float: left; margin-top: 10px; font-size: 13px; color: green; font-weight: bold;">
                            <asp:Label ID="lblRelatedItemPromotion" runat="server" Text=""></asp:Label>
                        </div>
                    </asp:PlaceHolder>
                    <asp:GridView ID="gvRelatedItems" CssClass="table table-bordered table-striped" runat="server" AutoGenerateColumns="false" Width="980px" CellPadding="3" CellSpacing="3" BorderColor="#f0f0f0">
                        <RowStyle BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" Font-Size="10" BackColor="#e8e8e8" BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                        <Columns>
                            <%-- <asp:TemplateField>
                                <ItemTemplate>
                                    <%#CCommon.GetImageHTML(Eval("vcPathForTImage"), 1, 100, 56)%>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="100px" Height="56px" />
                            </asp:TemplateField>--%>
                            <asp:BoundField DataField="vcItemName" HeaderText="Item" />
                            <asp:BoundField DataField="txtItemDesc" HeaderText="Description" />
                            <asp:BoundField DataField="vcRelationship" HeaderText="Relation to selected item(s)" />
                            <asp:TemplateField HeaderText="Qty" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false">
                                <ItemTemplate>
                                    <table>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtQty" runat="server" Width="50" Text="1"></asp:TextBox></td>
                                            <td>/ </td>
                                            <td><%# Eval("vcUOMName")%></td>
                                        </tr>
                                    </table>
                                    <asp:HiddenField ID="hdnWarehouseID" runat="server" Value='<%# Eval("numWareHouseItemID")%>' />
                                    <asp:HiddenField ID="hdnPrice" runat="server" Value='<%# Eval("monListPrice")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="monListPrice" HeaderText="Unit List" ItemStyle-HorizontalAlign="Center" DataFormatString="USD {0:#,##0.00}" />
                            <asp:TemplateField HeaderText="Total" ItemStyle-HorizontalAlign="Center" ItemStyle-Wrap="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblTotal" runat="server" Width="100" Text="1"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <%-- <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkAdd" CommandName="Add" CssClass="hyperlink" runat="server" CommandArgument='<%# Eval("numItemCode") %>'><img alt="" src="../images/AddRecord.png" OnClientClick="Confirm()" ></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>--%>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25">
                                <ItemTemplate>
                                    <asp:Button ID="lnkAdd" CommandName="Add" CssClass="button" Text="Add" runat="server" OnClientClick="Confirm()" CommandArgument='<%# Eval("numItemCode") %>'></asp:Button>
                                    <asp:Label ID="lblOutOfStock" runat="server" Width="100" Text="Out of stock" Visible="false"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="numOnHand" Visible="false" />
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
        <div class="overlay1" id="divItemPriceHistory" runat="server" style="display: none">
            <div class="overlayContent" style="color: #000; background-color: #FFF; width: 1000px; border: 1px solid #ddd; margin: 100px auto !important;">
                <div class="dialog-header">
                    <b style="font-size: 14px;">Item Price History</b>
                    <div style="float: right; padding-right: 7px;">
                        <asp:CheckBox ID="chkPriceHistoryByCustomer" runat="server" Text="Show price history of selected customer" Font-Bold="true" Style="margin-right: 20px" AutoPostBack="true" />
                        <asp:Button ID="btnClosePriceHistory" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="Close Window" />
                    </div>
                </div>
                <div class="dialog-body" style="max-height: 600px; width: 998px;">
                    <asp:GridView ID="gvPriceHistory" runat="server" AutoGenerateColumns="false" Width="100%" CellPadding="3" CellSpacing="3" BorderColor="#f0f0f0">
                        <RowStyle BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" Font-Size="10" BackColor="#e8e8e8" BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                        <Columns>
                            <asp:BoundField DataField="vcCreatedDate" HeaderText="DateCreated" />
                            <asp:BoundField DataField="vcPOppName" HeaderText="Source" />
                            <asp:BoundField DataField="numUnitHour" HeaderText="Units" />
                            <asp:BoundField DataField="vcUOMName" HeaderText="UOM" />
                            <asp:BoundField DataField="monPrice" HeaderText="Price" />
                            <asp:BoundField DataField="vcDiscount" HeaderText="Discount" />
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnDate" runat="server" Value='<%# Eval("vcCreatedDate")%>' />
                                    <asp:HiddenField ID="hdnUnits" runat="server" Value='<%# Eval("numUnitHour")%>' />
                                    <asp:HiddenField ID="hdnUOMId" runat="server" Value='<%# Eval("numUOMId")%>' />
                                    <asp:HiddenField ID="hdnUOMConversionFactor" runat="server" Value='<%# Eval("decUOMConversionFactor")%>' />
                                    <asp:HiddenField ID="hdnPrice" runat="server" Value='<%# Eval("monPrice") %>' />
                                    <asp:HiddenField ID="hdnDiscount" runat="server" Value='<%# Eval("fltDiscount")%>' />
                                    <asp:HiddenField ID="hdnDiscountType" runat="server" Value='<%# Eval("bitDiscountType")%>' />
                                    <asp:LinkButton ID="lnkAdd" CommandName="Add" CssClass="hyperlink" runat="server"><img alt="" src="../images/AddRecord.png" /></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<div class="overlay1" id="divEditKit" style="display: none">
    <div class="overlayContent" style="color: #000; background-color: #FFF; width: 90%; border: 1px solid #ddd; max-height: 530px; overflow: hidden">
        <div class="dialog-header">
            <div style="float: left">
                <img alt="" src="../images/ItemConfig.png" style="vertical-align: middle; padding-right: 5px;" />
                <b style="font-size: 14px">Select Item(s)</b>
            </div>
            <asp:UpdatePanel ID="uplEditKit1" runat="server" style="float: right; padding-right: 7px;" UpdateMode="Conditional" ChildrenAsTriggers="true">
                <ContentTemplate>
                    <asp:Button ID="btnSaveEditKit" CssClass="btn btn-primary" ForeColor="White" runat="server" Text="Save & Close" OnClick="btnSaveEditKit_Click" />
                    <asp:Button ID="btnCloseEditKit" CssClass="btn btn-primary" ForeColor="White" runat="server" Text="Close" OnClientClick="return CloseEditKitChildWindow();" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="btnSaveEditKit" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <div class="dialog-body" style="width: 100%">
            <div class="col-1-1" style="padding-bottom: 10px">
                <asp:UpdatePanel ID="UpdatePanel4" runat="server" class="pull-left" ChildrenAsTriggers="true" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="form-inline">
                            <div class="form-group">
                                <label>Add Item:</label>
                                <input type="text" id="txtChildItem" />
                                <input type="button" class="btn btn-success" value="Add Item" onclick="return AddChildItem()" style="margin-left: 5px; margin-right: 5px;" />
                                <input type="button" class="btn btn-danger" value="Remove Item" onclick="$('[id$=btnRemoveChilItem]').click();" />
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="UpdatePanel5" runat="server" class="pull-right" UpdateMode="Always">
                    <ContentTemplate>
                        <div class="form-inline">
                            <ul style="list-style: none" id="divKitPrice" runat="server" visible="false">
                                <li>
                                    <div class="form-inline">
                                        <label>Parent kit unit price:</label>
                                        <asp:Label runat="server" ID="lblKitPrice"></asp:Label>
                                    </div>
                                </li>
                                <li>
                                    <div class="form-inline">
                                        <label>Price set to:</label>
                                        <asp:Label runat="server" ID="lblKitPriceType"></asp:Label>
                                        <asp:HiddenField runat="server" ID="hdnKitPricing" />
                                        <asp:HiddenField runat="server" ID="hdnKitListPrice" />
                                    </div>
                                </li>
                            </ul>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <asp:UpdatePanel ID="UpdatePanel3" runat="server" class="col-1-1" ChildrenAsTriggers="true" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:HiddenField ID="hdnSelectedChildItem" runat="server" />
                    <asp:Button ID="btnAddChilItem" runat="server" Style="display: none;" OnClick="btnAddChilItem_Click" />
                    <asp:Button ID="btnRemoveChilItem" runat="server" OnClick="btnRemoveChilItem_Click" Style="display: none" />
                    <div class="table-responsive" style="max-height: 400px">
                        <asp:GridView ID="gvChildItems" ShowHeaderWhenEmpty="true" runat="server" AutoGenerateColumns="false" UseAccessibleHeader="true" CssClass="table table-bordered table-striped">
                            <Columns>
                                <asp:TemplateField HeaderText="Item Code" HeaderStyle-Width="80" ItemStyle-Width="80">
                                    <ItemTemplate>
                                        <asp:Label ID="lblItemCode" runat="server" Text='<%# Eval("numItemCode")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item">
                                    <ItemTemplate>
                                        <asp:Label ID="lblItemName" runat="server" Text='<%# Eval("vcItemName")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SKU">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSKU" runat="server" Text='<%# Eval("vcSKU")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type">
                                    <ItemTemplate>
                                        <asp:Label ID="lblType" runat="server" Text='<%# Eval("vcType")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:BoundField HeaderText="Qty" DataField="numQty" HeaderStyle-Width="80" ItemStyle-Width="80" />--%>
                                <asp:TemplateField HeaderText="Qty" HeaderStyle-Width="80" ItemStyle-Width="80">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtUnits" runat="server" Text='<%# Eval("numQty")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="List Price" HeaderStyle-Width="80" ItemStyle-Width="80">
                                    <ItemTemplate>
                                        <asp:Label ID="lblListPrice" runat="server" Text='<%# Eval("monListPrice")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Vendor Cost" HeaderStyle-Width="88" ItemStyle-Width="88">
                                    <ItemTemplate>
                                        <asp:Label ID="lblVendorCost" runat="server" Text='<%# Eval("monVendorCost")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Average Cost" HeaderStyle-Width="94" ItemStyle-Width="94">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAverageCost" runat="server" Text='<%# Eval("monAverageCost")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Price" HeaderStyle-Width="80" ItemStyle-Width="80">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPrice" runat="server" Text='<%# Eval("monPrice")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sequence" HeaderStyle-Width="74" ItemStyle-Width="74">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtSequence" runat="server" Text='<%# Eval("numSequence")%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-Width="25" ItemStyle-Width="25">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                        <asp:HiddenField runat="server" ID="hdnItemCode" Value='<%# Eval("numItemCode") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                No item(s) added
                            </EmptyDataTemplate>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
<asp:Button ID="btn2" runat="server" Style="display: none" OnClick="btn2_Click" />
<div class="overlay" id="divLoader" style="display: none">
    <div class="overlayContent" style="background-color: #fff; color: #000; text-align: center; width: 280px; padding: 20px">
        <i class="fa fa-2x fa-refresh fa-spin"></i>
        <h3>Processing Request</h3>
    </div>
</div>
