﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Projects

Namespace BACRM.UserInterface.Opportunities

    Public Class frmNewReturn
        Inherits BACRMPage


        'if button click on Refund button then 1, if they click on credit memo button thrn 2 .. else 0

#Region "PRIVATE/PUBLIC DECLARATIONS"

        Dim objItems As CItems
        Dim dsTemp As DataSet
        Dim objReturnHeader As New ReturnHeader
        Dim tintReturnType As Short
        Dim lngReturnHeaderID As Long = 0
        Dim decDiscount, decTaxTotal, decSubTotal As Decimal
        Dim arrSelectedItem As String()

#End Region

#Region "PAGE EVENTS"

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lblError.Text = ""
                arrSelectedItem = hdnSelectedItems.Value.Split(",")
                tintReturnType = CCommon.ToShort(GetQueryStringVal("ReturnType"))

                If Not IsPostBack Then
                    radCmbCompany.Focus()
                    objCommon.UserCntID = Session("UserContactID")

                    If CCommon.ToLong(Session("UserContactID")) > 0 AndAlso CCommon.ToLong(Session("UserID")) = 0 Then
                        objCommon.DivisionID = CCommon.ToLong(Session("UserDivisionID"))
                        objCommon.charModule = "D"
                        radCmbCompany.Enabled = False

                        rBtnAddItems.Checked = False
                        rBtnAddItems.Enabled = False
                        rBtnExistingOrder.Checked = True

                        objCommon.GetCompanySpecificValues1()
                        Dim strCompany, strContactID As String
                        strCompany = objCommon.GetCompanyName
                        strContactID = objCommon.ContactID
                        radCmbCompany.Text = strCompany
                        radCmbCompany.SelectedValue = objCommon.DivisionID

                        radCmbCompany_SelectedIndexChanged() 'Added by chintan- Reason:remaining credit wasn't showing

                        If Not ddlContact.Items.FindByValue(strContactID) Is Nothing Then
                            ddlContact.ClearSelection()
                            ddlContact.Items.FindByValue(strContactID).Selected = True
                        End If
                    ElseIf GetQueryStringVal("uihTR") <> "" Or GetQueryStringVal("fghTY") <> "" Or GetQueryStringVal("rtyWR") <> "" Or GetQueryStringVal("tyrCV") <> "" Or GetQueryStringVal("pluYR") <> "" Then

                        If GetQueryStringVal("uihTR") <> "" Then
                            objCommon.ContactID = CCommon.ToLong(GetQueryStringVal("uihTR"))
                            objCommon.charModule = "C"
                        ElseIf GetQueryStringVal("rtyWR") <> "" Then
                            objCommon.DivisionID = CCommon.ToLong(GetQueryStringVal("rtyWR"))
                            objCommon.charModule = "D"
                        ElseIf GetQueryStringVal("tyrCV") <> "" Then
                            objCommon.ProID = CCommon.ToLong(GetQueryStringVal("tyrCV"))
                            objCommon.charModule = "P"
                        ElseIf GetQueryStringVal("pluYR") <> "" Then
                            objCommon.OppID = CCommon.ToLong(GetQueryStringVal("pluYR"))
                            objCommon.charModule = "O"
                        ElseIf GetQueryStringVal("fghTY") <> "" Then
                            objCommon.CaseID = CCommon.ToLong(GetQueryStringVal("fghTY"))
                            objCommon.charModule = "S"
                        End If

                        objCommon.GetCompanySpecificValues1()
                        Dim strCompany, strContactID As String
                        strCompany = objCommon.GetCompanyName
                        strContactID = objCommon.ContactID
                        radCmbCompany.Text = strCompany
                        radCmbCompany.SelectedValue = objCommon.DivisionID

                        radCmbCompany_SelectedIndexChanged() 'Added by chintan- Reason:remaining credit wasn't showing

                        If Not ddlContact.Items.FindByValue(strContactID) Is Nothing Then
                            ddlContact.ClearSelection()
                            ddlContact.Items.FindByValue(strContactID).Selected = True
                        End If


                    End If

                    If tintReturnType = 1 Then
                        lblReturns.Text = "Sales Return"
                        Page.Title = "Sales Return"
                        lblToolTip.Visible = True
                        lblAmount.Visible = False
                        txtAmount.Visible = False
                        btnSaveAndOpenDetails.Text = "Save & Open Details"
                    ElseIf tintReturnType = 2 Then
                        lblReturns.Text = "Purchase Return"
                        Page.Title = "Purchase Return"
                        lblToolTip.Visible = True
                        lblAmount.Visible = False
                        txtAmount.Visible = False
                        btnSaveAndOpenDetails.Text = "Save & Open Details"
                    ElseIf tintReturnType = 3 Then
                        lblReturns.Text = "Credit Memo"
                        Page.Title = "Credit Memo"
                        lblToolTip.Visible = False
                        pnlOrder.Visible = False
                        btnSaveAndOpenDetails.Text = "Save & Issue Credit Memo"
                        lblReferenceSalesOrder.Visible = True
                        radCmbSalesOrder.Visible = True
                    ElseIf tintReturnType = 4 Then
                        lblReturns.Text = "Refund"
                        Page.Title = "Refund"
                        lblToolTip.Visible = False
                        pnlOrder.Visible = False
                        pnlUnAppliedPayment.Visible = True
                        lblReferenceSalesOrder.Visible = True
                        radCmbSalesOrder.Visible = True
                        btnSaveAndOpenDetails.Text = "Save & Issue Refund"
                    End If

                    rBtnAddItems_CheckedChanged(Nothing, Nothing)

                    objCommon.sb_FillComboFromDBwithSel(ddlReturnReason, 48, Session("DomainID"))

                    imgItem.Visible = False
                    hplImage.Visible = False
                    objCommon = New CCommon
                    objCommon.UserCntID = Session("UserContactID")
                    'For template hover selection
                    Dim ds As DataSet
                    ds = objCommon.NewOppItems(Session("DomainID"), 0, 0, "")
                    CreateColums(ds.Tables(1))
                    BindButtons()

                    ViewState("SOItems") = Nothing

                    If tintReturnType = 2 Then
                        dgItems.Columns(8).Visible = False
                    End If

                    UpdateDetails()
                End If

                Dim eventTarget As String = If((Me.Request("__EVENTTARGET") Is Nothing), String.Empty, Me.Request("__EVENTTARGET"))
                If ViewState("SOItems") Is Nothing AndAlso (tintReturnType = 1 Or tintReturnType = 2) Then
                    createSet()
                End If
                'UpdateDetails()
                UpdateDataTable()

                btnSaveAndOpenDetails.Attributes.Add("onclick", "return Save('" & tintReturnType & "')")

                If ddltype.SelectedValue = "S" Then
                    txtUnits.Attributes.Add("onkeypress", "CheckNumber(1,event);TempHideButton()")
                Else
                    txtUnits.Attributes.Add("onkeypress", "CheckNumber(2,event);TempHideButton()")
                End If

                txtprice.Attributes.Add("onkeypress", "CheckNumber(1,event)")
                'txtDiscount.Attributes.Add("onkeypress", "CheckNumber(1,event)")
                'txtDiscount.Attributes.Add("onkeyup", "return CalculateTotal()")

            Catch ex As Exception
                LogError(ex)
            End Try
        End Sub

        Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            Try
                Dim objCommon As New CCommon

                CCommon.UpdateItemRadComboValues(tintReturnType, radCmbCompany.SelectedValue)
                objCommon.InitializeClientSideTemplate(radItem)
                CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
            Catch ex As Exception
                LogError(ex)
            End Try
        End Sub

        Private Sub rBtnAddItems_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rBtnAddItems.CheckedChanged
            Try
                If rBtnAddItems.Checked = True Then
                    pnlOrders.Style.Add("display", "none")
                    pnlItems.Style.Add("display", "")
                Else
                    pnlOrders.Style.Add("display", "")
                    pnlItems.Style.Add("display", "none")
                End If

                ViewState("SOItems") = Nothing

                dsTemp = Nothing

                If Page.IsPostBack Then
                    UpdateDetails()
                End If
            Catch ex As Exception
                LogError(ex)
            End Try
        End Sub

        Private Sub rBtnExistingOrder_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rBtnExistingOrder.CheckedChanged
            Try
                If rBtnExistingOrder.Checked = True Then
                    pnlOrders.Style.Add("display", "")
                    pnlItems.Style.Add("display", "none")
                Else
                    pnlOrders.Style.Add("display", "none")
                    pnlItems.Style.Add("display", "")
                End If

                ViewState("SOItems") = Nothing

                dsTemp = Nothing
                UpdateDetails()
            Catch ex As Exception
                LogError(ex)
            End Try
        End Sub

#End Region

#Region "DROP DOWN EVENTS"

        Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
            Try
                ViewState("SOItems") = Nothing
                dsTemp = Nothing
                UpdateDetails()

                radCmbCompany_SelectedIndexChanged()
                'CreateClone()
            Catch ex As Exception
                LogError(ex)
            End Try
        End Sub

        Private Sub radItem_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radItem.SelectedIndexChanged
            Try
                If ViewState("SOItems") Is Nothing Then
                    createSet()
                End If
                'UpdateDetails()
                If radItem.SelectedValue <> "" Then
                    LoadItemDetails()

                    Dim ds As DataSet
                    Dim dt As DataTable
                    objItems.ItemCode = radItem.SelectedValue
                    'dt = objItems.GetChildItems
                    objItems.ItemGroupID = 0
                    objItems.DomainID = Session("DomainID")
                    ds = objItems.GetItemGroups
                    dt = ds.Tables(0)
                    dt.TableName = "ChildItems"

                    dsTemp = ViewState("SOItems")
                    Dim dtitem As DataTable
                    dtitem = dsTemp.Tables(2)
                    dtitem.Rows.Clear()

                    dsTemp.AcceptChanges()
                    ViewState("SOItems") = dsTemp

                    If ddltype.SelectedValue = "P" Then
                        If objCommon Is Nothing Then objCommon = New CCommon
                        Dim dtWareHouse As DataTable = objCommon.GetWarehousesForSelectedItem(radItem.SelectedValue, 0, 0, 0, 0, 0, 0, "")

                        Dim dr() As DataRow
                        If ViewState("SelectedWarehouse") IsNot Nothing Then
                            dr = dtWareHouse.Select("numWareHouseID = " & ViewState("SelectedWarehouse").ToString())
                        ElseIf CCommon.ToLong(Session("DefaultWarehouse")) > 0 Then
                            dr = dtWareHouse.Select("numWareHouseID = " & Session("DefaultWarehouse").ToString())
                        End If
                        radWareHouse.DataSource = dtWareHouse
                        radWareHouse.DataBind()
                        If (radWareHouse.Items.Count > 0) Then
                            If Not dr Is Nothing Then 'Persist Value of perviously selected warehouse if it exist 
                                If dr.Length > 0 Then
                                    If Not radWareHouse.FindItemByValue(CType(dr(0), DataRow)("numWareHouseItemId").ToString()) Is Nothing Then radWareHouse.FindItemByValue(CType(dr(0), DataRow)("numWareHouseItemId").ToString()).Selected = True
                                End If
                            Else
                                radWareHouse.Items(0).Selected = True
                            End If
                        End If
                        trWarehouse.Visible = True
                    Else
                        trWarehouse.Visible = False
                        radWareHouse.SelectedValue = ""
                    End If

                    If ddltype.SelectedValue = "S" Then
                        txtUnits.Attributes.Add("onkeypress", "CheckNumber(1,event);TempHideButton()")
                    Else
                        txtUnits.Attributes.Add("onkeypress", "CheckNumber(2,event);TempHideButton()")
                    End If

                    'LoadOrderInTransit()
                End If
                CalculateUnitPrice()
            Catch ex As Exception
                LogError(ex)
            End Try
        End Sub

        Private Sub ddlExistingOrders_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlExistingOrders.SelectedIndexChanged
            Try
                If ddlExistingOrders.SelectedValue <> "0" Then
                    hdnSelectedItems.Value = ""
                    LoadDataToSession()

                    If dgItems.Items.Count > 0 Then
                        divTD.Visible = True
                    Else
                        divTD.Visible = False
                    End If
                End If
            Catch ex As Exception
                LogError(ex)
            End Try
        End Sub


        Private Sub LoadDataToSession()
            Try
                Dim dtItems As DataTable
                Dim objOpportunity As New OppotunitiesIP

                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.OpportunityId = CCommon.ToLong(ddlExistingOrders.SelectedValue.Split("-")(0))
                If tintReturnType = 2 Then
                    objOpportunity.Mode = 6
                    dsTemp = objOpportunity.GetOrderItems()
                Else
                    objOpportunity.OppBizDocID = CCommon.ToLong(ddlExistingOrders.SelectedValue.Split("-")(1))
                    dsTemp = objOpportunity.GetBizDocItemsForReturn()
                End If

                dtItems = dsTemp.Tables(0)
                Dim k As Integer
                Dim intCheck As Integer = 0
                For k = 0 To dtItems.Rows.Count - 1
                    If dtItems.Rows(k).Item("charItemType") <> "S" Then intCheck = 1
                    dtItems.Rows(k)("numUnitHour") = Math.Abs(dtItems.Rows(k)("numUnitHour") / dtItems.Rows(k)("UOMConversionFactor"))
                Next
                dtItems.AcceptChanges()
                Dim dtItem As DataTable
                Dim dtSerItem As DataTable
                Dim dtChildItems As New DataTable
                dtItem = dsTemp.Tables(0)
                dtSerItem = dsTemp.Tables(1)
                'dtChildItems = dsTemp.Tables(2)
                dtItem.TableName = "Item"
                dtSerItem.TableName = "SerialNo"
                'dtChildItems.TableName = "ChildItems"
                dtItem.PrimaryKey = New DataColumn() {dsTemp.Tables(0).Columns("numoppitemtCode")}

                dtChildItems.Columns.Add("numOppChildItemID", System.Type.GetType("System.Int32"))
                dtChildItems.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
                dtChildItems.Columns.Add("numItemCode")
                dtChildItems.Columns.Add("numQtyItemsReq")
                dtChildItems.Columns.Add("vcItemName")
                dtChildItems.Columns.Add("monListPrice")
                dtChildItems.Columns.Add("UnitPrice")
                dtChildItems.Columns.Add("charItemType")
                dtChildItems.Columns.Add("txtItemDesc")
                dtChildItems.Columns.Add("numWarehouseItmsID")
                dtChildItems.Columns.Add("Op_Flag")
                dtChildItems.Columns.Add("ItemType")
                dtChildItems.Columns.Add("Attr")
                dtChildItems.Columns.Add("vcWareHouse")

                dtChildItems.TableName = "ChildItems"

                dsTemp.Tables.Add(dtChildItems)

                'If dtItem.Columns("bitTaxable0") Is Nothing Then
                '    dtItem.Columns.Add("bitTaxable0")
                'End If

                'If dtItem.Columns("Tax0") Is Nothing Then
                '    dtItem.Columns.Add("Tax0", GetType(Decimal))
                'End If

                If tintReturnType = 1 Or (tintReturnType = 2 AndAlso CCommon.ToBool(Session("PurchaseTaxCredit"))) Then
                    getTaxPercentage()

                    For k = 0 To chkTaxItems.Items.Count - 1
                        dtItem.Columns.Add("Tax" & chkTaxItems.Items(k).Value, GetType(Decimal))
                        dtItem.Columns.Add("bitTaxable" & chkTaxItems.Items(k).Value)
                    Next

                    Dim strTax() As String
                    strTax = txtTax.Text.Split(",")

                    For Each dr As DataRow In dtItem.Rows
                        Dim dtTable, dtItemTax As DataTable

                        Dim dtOppTax As DataTable = objCommon.GetOpportunityMasterTaxItems(CCommon.ToLong(ddlExistingOrders.SelectedValue.Split("-")(0)), dr("numoppitemtCode"), 2)
                        Dim drTax As DataRow()

                        For k = 0 To chkTaxItems.Items.Count - 1
                            drTax = dtOppTax.Select("numTaxItemID = " & chkTaxItems.Items(k).Value)

                            If drTax.Length > 0 Then
                                Dim decTaxValue As Decimal = CCommon.ToDecimal(drTax(0)("fltPercentage"))
                                Dim tintTaxType As Short = CCommon.ToShort(drTax(0)("tintTaxType"))

                                If tintTaxType = 2 Then 'FLAT AMOUNT
                                    dr("Tax" & chkTaxItems.Items(k).Value) = decTaxValue * (dr("numUnitHour") * dr("UOMConversionFactor"))
                                Else 'PERCENTAGE
                                    dr("Tax" & chkTaxItems.Items(k).Value) = decTaxValue * dr("monTotAmount") / 100
                                End If

                                dr("bitTaxable" & chkTaxItems.Items(k).Value) = True
                            Else
                                dr("Tax" & chkTaxItems.Items(k).Value) = 0
                                dr("bitTaxable" & chkTaxItems.Items(k).Value) = False
                            End If
                        Next
                    Next

                End If


                If dtSerItem.ParentRelations.Count = 0 Then dsTemp.Relations.Add("Item", dsTemp.Tables(0).Columns("numoppitemtCode"), dsTemp.Tables(1).Columns("numoppitemtCode"))
                dsTemp.AcceptChanges()
                ViewState("SOItems") = dsTemp


                UpdateDetails()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub getTaxPercentage()
            Try
                Dim strTax As String = ""
                Dim strTaxIds As String = ""

                If tintReturnType = 1 Or (tintReturnType = 2 AndAlso CCommon.ToBool(Session("PurchaseTaxCredit"))) Then
                    If radCmbCompany.SelectedValue <> "" Then
                        objCommon = New CCommon
                        Dim dtTaxTypes As DataTable
                        Dim ObjTaxItems As New TaxDetails
                        ObjTaxItems.DomainID = Session("DomainID")
                        dtTaxTypes = ObjTaxItems.GetTaxItems

                        If rBtnExistingOrder.Checked AndAlso CCommon.ToLong(ddlExistingOrders.SelectedValue.Split("-")(0)) > 0 Then
                            Dim dtOppTax As DataTable = objCommon.GetOpportunityMasterTaxItems(CCommon.ToLong(ddlExistingOrders.SelectedValue.Split("-")(0)), 0, 1)
                            Dim drTax As DataRow()

                            For Each dr As DataRow In dtTaxTypes.Rows
                                drTax = dtOppTax.Select("numTaxItemID = " & dr("numTaxItemID"))

                                If drTax.Length > 0 Then
                                    strTax = strTax & drTax(0)("fltPercentage") & "#" & drTax(0)("tintTaxType") & "#0,"
                                Else
                                    strTax = strTax & "0#1#0,"
                                End If
                                strTaxIds = strTaxIds & dr("numTaxItemID") & ","
                            Next

                            drTax = dtOppTax.Select("numTaxItemID = 0")
                            If drTax.Length > 0 Then
                                strTax = strTax & drTax(0)("fltPercentage") & "#" & drTax(0)("tintTaxType") & "#0,"
                            Else
                                strTax = strTax & "0#1#0,"
                            End If

                            drTax = dtOppTax.Select("numTaxItemID = 1")
                            If drTax.Length > 0 Then
                                For Each dr As DataRow In drTax
                                    strTax = strTax & dr("fltPercentage") & "#" & dr("tintTaxType") & "#" & dr("numTaxID") & ","
                                Next
                            End If

                            strTaxIds = strTaxIds & "0,1"
                        Else
                            For Each dr As DataRow In dtTaxTypes.Rows
                                'Base tax calculation on Shipping Address(2) or Billing Address(1) 
                                strTax = strTax & objCommon.TaxPercentage(radCmbCompany.SelectedValue, 0, 0, Session("DomainID"), dr("numTaxItemID"), "", "", Session("BaseTaxOnArea"), Session("BaseTaxCalcOn")) & ","
                                strTaxIds = strTaxIds & dr("numTaxItemID") & ","
                            Next
                            strTax = strTax & objCommon.TaxPercentage(radCmbCompany.SelectedValue, 0, 0, Session("DomainID"), 0, "", "", Session("BaseTaxOnArea"), Session("BaseTaxCalcOn"))

                            strTaxIds = strTaxIds & "0"
                        End If

                        txtTax.Text = strTax
                        TaxItemsId.Value = strTaxIds
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
#End Region

#Region "BUTTON EVENTS"

        Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Try
                dsTemp = ViewState("SOItems")
                objCommon = New CCommon
                Dim objItem As New CItems
                If Not objItem.ValidateItemAccount(radItem.SelectedValue, Session("DomainID"), 1) = True Then
                    ScriptManager.RegisterClientScriptBlock(UpdatePanelMain, UpdatePanelMain.GetType(), "AccountValidation", "alert('Please Set Income,Asset,COGs(Expense) Account for selected Item from Administration->Inventory->Item Details')", True)
                    Return
                End If

                If btnAdd.Text = "Add" Then If IsDuplicate(radItem.SelectedValue, radWareHouse.SelectedValue) Then Return

                If Not CCommon.ToDouble(hdvUOMConversionFactor.Value) > 0 Then
                    hdvUOMConversionFactor.Value = "1"
                End If

                objCommon = New CCommon
                ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon, dsTemp, IIf(btnAdd.Text = "Add", True, False), ddltype.SelectedValue,
                                                                                    ddltype.SelectedItem.Text, 0, 0, radItem.SelectedValue, CCommon.ToDecimal(txtUnits.Text),
                                                                                    CCommon.ToDecimal(hdnOrigUnitPrice.Value), txtdesc.Text, CCommon.ToLong(radWareHouse.SelectedValue), radItem.Text, radWareHouse.Text, 0, "",
                                                                                    ddUOM.SelectedValue, ddUOM.SelectedItem.Text.Replace("--Select One--", ""), CCommon.ToDecimal(hdvUOMConversionFactor.Value), IIf(tintReturnType = 1, "Sales", "Purchase"), False, 0,
                                                                                    Taxable.Value, txtTax.Text, "", chkTaxItems, 0, "", DateTime.Now, 0, CCommon.ToLong(0), 0, CCommon.ToLong(0),
                                                                                    False, CCommon.ToLong(GetQueryStringVal("StageID")),
                                                                                    ToWarehouseItemID:=0, vcBaseUOMName:=ddUOM.SelectedItem.Text.Replace("--Select One--", ""))

                UpdateDetails()
                clearControls()
                ScriptManager.RegisterClientScriptBlock(UpdatePanelMain, UpdatePanelMain.GetType(), "DoubleClickPrevent", "IsAddClicked = false;", True) 'added to avoid multiple click of add button before completing postback- by chintan
                objCommon = Nothing

                If dgItems.Items.Count > 0 Then
                    divTD.Visible = True
                Else
                    divTD.Visible = False
                End If

            Catch ex As Exception
                LogError(ex)
                objCommon = Nothing
            End Try
        End Sub

        Private Sub btnTemp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTemp.Click
            Try
                If (CCommon.ToLong(hdnBillAddressID.Value) > 0) Then
                    BindAddressByAddressID(CCommon.ToLong(hdnBillAddressID.Value), "Bill")
                End If
                If (CCommon.ToLong(hdnShipAddressID.Value) > 0) Then
                    BindAddressByAddressID(CCommon.ToLong(hdnShipAddressID.Value), "Ship")
                End If
            Catch ex As Exception
                LogError(ex)
            End Try
        End Sub

        Private Sub btnSaveAndOpenDetails_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveAndOpenDetails.Click
            Try
                Save()
            Catch ex As Exception
                LogError(ex)
            End Try
        End Sub

        Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
            Try
                ScriptManager.RegisterClientScriptBlock(UpdatePanelMain, UpdatePanelMain.GetType(), "Reload1", "self.close();", True)
            Catch ex As Exception
                LogError(ex)
            End Try
        End Sub

#End Region

#Region "FUNCTIONS & METHODS"

        Sub clearControls()
            radItem.Items.Clear()
            radItem.SelectedValue = ""
            radItem.Text = ""
            txtUnits.Text = "1"
            txtprice.Text = ""
            hdnOrigUnitPrice.Value = ""
            radItem.Items.Clear()
            btnEditCancel.Visible = False
            ddltype.SelectedIndex = 0
            btnAdd.Text = "Add"
            'divTransit.Visible = False
            tdSalesProfit.Visible = False
            ddUOM.Enabled = True
        End Sub

        Private Sub BindButtons()
            Try
                'lnkBillTo.Attributes.Add("onclick", "return OpenAddressWindow('BillTo')")
                'lnkShipTo.Attributes.Add("onclick", "return OpenAddressWindow('ShipTo')")
                btnAdd.Attributes.Add("onclick", "return Add()")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub CreateColums(ByVal dtTable As DataTable)
            radItem.HeaderTemplate = New RadComboBoxTemplate(ListItemType.Header, dtTable)
            radItem.ItemTemplate = New RadComboBoxTemplate(ListItemType.Item, dtTable)
        End Sub

        Sub UpdateDetails()
            Try
                If dsTemp Is Nothing Then
                    If ViewState("SOItems") Is Nothing Then
                        createSet()
                    End If

                    dsTemp = ViewState("SOItems")
                End If

                Dim dtAllItems As DataTable
                dtAllItems = dsTemp.Tables(0)

                If tintReturnType = 1 Then
                    Dim TaxTemplateColumn As TemplateColumn
                ElseIf tintReturnType = 2 Then
                    If Not dtAllItems.Columns.Contains("Tax0") Then
                        CCommon.AddColumnsToDataTable(dtAllItems, "Tax0,bitTaxable0")
                    End If

                    For Each dgCol As DataGridColumn In dgItems.Columns
                        If dgCol.HeaderText.Contains("Sales Tax") = True And dgCol.Visible = True Then
                            dgCol.Visible = False
                        End If
                    Next
                End If

                dgItems.DataSource = dtAllItems
                dgItems.DataBind()

                decDiscount = decTaxTotal = decSubTotal = 0


                For Each dr As DataRow In dtAllItems.Rows
                    If arrSelectedItem.Contains(dr("numoppitemtCode")) Then
                        decSubTotal += CCommon.ToDecimal(dr("monTotAmount"))

                        If tintReturnType = 1 Or (tintReturnType = 2 AndAlso CCommon.ToBool(Session("PurchaseTaxCredit"))) Then
                            For Each Item As ListItem In chkTaxItems.Items
                                Dim decTax As Decimal = CCommon.ToDecimal(dr("Tax" & Item.Value))
                                decTaxTotal = decTaxTotal + decTax
                            Next
                        End If
                    End If
                Next

                lblSubTotal.Text = String.Format("{0:#,##0.00}", decSubTotal)
                lblTaxAmount.Text = decTaxTotal
                lblGrandTotal.Text = String.Format("{0:#,##0.00}", decSubTotal + decTaxTotal - decDiscount)
                CType(dgItems.Controls(0).Controls(dtAllItems.Rows.Count + 1).FindControl("lblFTotal"), Label).Text = String.Format("{0:#,##0.00}", decSubTotal)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub CalculateUnitPrice()
            Try
                UpdateDetails()
                Dim objItems As New CItems
                objCommon = New CCommon
                If Not (radItem.SelectedValue = "") Then

                    Dim lngItemClass As Long
                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.Mode = 13
                    objCommon.Str = radItem.SelectedValue
                    lngItemClass = objCommon.GetSingleFieldValue()

                    Dim dtUnit As DataTable
                    objCommon.DomainID = Session("DomainID")
                    objCommon.ItemCode = radItem.SelectedValue
                    objCommon.UnitId = ddUOM.SelectedValue
                    dtUnit = objCommon.GetItemUOMConversion()

                    If Not dtUnit Is Nothing AndAlso dtUnit.Rows.Count > 0 Then
                        hdvUOMConversionFactor.Value = dtUnit(0)("BaseUOMConversion")
                    End If

                    If Not ddUOM.SelectedItem Is Nothing AndAlso CCommon.ToLong(ddUOM.SelectedValue) > 0 Then
                        lblBaseUOMName.Text = ddUOM.SelectedItem.Text
                    Else
                        lblBaseUOMName.Text = dtUnit(0)("vcBaseUOMName")
                    End If

                    objItems.ItemCode = radItem.SelectedValue

                    objItems.NoofUnits = Math.Abs(CCommon.ToInteger(txtUnits.Text)) * hdvUOMConversionFactor.Value

                    objItems.OppId = tintReturnType
                    objItems.DivisionID = radCmbCompany.SelectedValue
                    objItems.DomainID = Session("DomainID")
                    objItems.byteMode = tintReturnType

                    objItems.Amount = 0
                    If Not radWareHouse.SelectedValue = "" Then
                        objItems.WareHouseItemID = radWareHouse.SelectedValue
                    Else
                        objItems.WareHouseItemID = Nothing
                    End If

                    Dim ds As DataSet
                    Dim dtItemPricemgmt As DataTable

                    ds = objItems.GetPriceManagementList1
                    dtItemPricemgmt = ds.Tables(0)

                    If dtItemPricemgmt.Rows.Count = 0 Then
                        dtItemPricemgmt = ds.Tables(2)
                    End If

                    dtItemPricemgmt.Merge(ds.Tables(1))

                    tdSalesProfit.Visible = False

                    If tintReturnType = 2 Then
                        hdnOrigUnitPrice.Value = CCommon.ToDecimal(dtItemPricemgmt.Rows(0)("ListPrice")) / CCommon.ToDecimal(hdvUOMConversionFactor.Value)
                        txtprice.Text = String.Format("{0:0.################}", dtItemPricemgmt.Rows(0)("ListPrice"))
                    Else
                        hdnOrigUnitPrice.Value = dtItemPricemgmt.Rows(0)("ListPrice")
                        txtprice.Text = String.Format("{0:0.################}", dtItemPricemgmt.Rows(0)("ListPrice") * CCommon.ToDouble(hdvUOMConversionFactor.Value))
                    End If

                    'Related Items Count
                    objItems.ParentItemCode = radItem.SelectedValue
                    objItems.byteMode = 1

                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddUOM_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddUOM.SelectedIndexChanged
            Try
                CalculateUnitPrice()
            Catch ex As Exception
                LogError(ex)
            End Try
        End Sub

        'Sub LoadOrderInTransit()
        '    Dim objOpportunity As New MOpportunity
        '    objOpportunity.ItemCode = radItem.SelectedValue
        '    objOpportunity.DomainID = Session("DomainID")
        '    objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
        '    Dim dtTransit As DataTable = objOpportunity.GetItemTransit(1)

        '    If dtTransit.Rows(0)("Total") > 0 Then
        '        hplInTransit.Text = dtTransit.Rows(0)("Total")
        '        divTransit.Visible = True
        '    Else
        '        divTransit.Visible = False
        '    End If
        'End Sub

        Sub LoadItemDetails()
            Try
                If radItem.SelectedValue <> "" Then

                    objCommon = New CCommon
                    Dim dtUnit As DataTable
                    objCommon.DomainID = Session("DomainID")
                    objCommon.ItemCode = radItem.SelectedValue
                    objCommon.UOMAll = True
                    dtUnit = objCommon.GetItemUOM()

                    With ddUOM
                        .DataSource = dtUnit
                        .DataTextField = "vcUnitName"
                        .DataValueField = "numUOMId"
                        .DataBind()
                        .Items.Insert(0, "--Select One--")
                        .Items.FindByText("--Select One--").Value = "0"
                    End With

                    Dim dtTable, dtItemTax As DataTable
                    If objItems Is Nothing Then objItems = New CItems

                    Dim strUnit As String

                    If tintReturnType = 1 Then
                        strUnit = "numBaseUnit"
                    Else
                        strUnit = "numPurchaseUnit"
                    End If

                    Dim strApplicable As String
                    objItems.ItemCode = radItem.SelectedValue

                    If tintReturnType = 1 Or (tintReturnType = 2 AndAlso CCommon.ToBool(Session("PurchaseTaxCredit"))) Then
                        objItems.DomainID = Session("DomainID")
                        dtItemTax = objItems.ItemTax()

                        For Each dr As DataRow In dtItemTax.Rows
                            strApplicable = strApplicable & dr("bitApplicable") & ","
                        Next
                    End If

                    dtTable = objItems.ItemDetails

                    If dtTable.Rows.Count > 0 Then

                        If tintReturnType = 1 Or (tintReturnType = 2 AndAlso CCommon.ToBool(Session("PurchaseTaxCredit"))) Then
                            strApplicable = strApplicable & dtTable.Rows(0).Item("bitTaxable")
                            Taxable.Value = strApplicable
                        End If

                        ddltype.SelectedItem.Selected = False
                        If ddltype.Items.FindByValue(dtTable.Rows(0).Item("charItemType")) IsNot Nothing Then
                            ddltype.Items.FindByValue(dtTable.Rows(0).Item("charItemType")).Selected = True
                        End If

                        txtdesc.Text = CCommon.ToString(dtTable.Rows(0).Item("txtItemDesc"))

                        If Not IsDBNull(dtTable.Rows(0).Item("vcPathForImage")) Then
                            If dtTable.Rows(0).Item("vcPathForImage") <> "" Then
                                imgItem.Visible = True
                                hplImage.Visible = True
                                imgItem.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtTable.Rows(0).Item("vcPathForImage")
                                hplImage.Attributes.Add("onclick", "return OpenImage(" & radItem.SelectedValue & ")")
                            Else
                                imgItem.Visible = False
                                hplImage.Visible = False
                            End If
                        Else
                            imgItem.Visible = False
                            hplImage.Visible = False
                        End If

                        If strUnit.Length > 0 Then
                            If ddUOM.Items.FindByValue(dtTable.Rows(0).Item(strUnit)) IsNot Nothing Then
                                ddUOM.ClearSelection()
                                ddUOM.Items.FindByValue(dtTable.Rows(0).Item(strUnit)).Selected = True
                            End If
                        End If
                    End If

                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub createSet()
            Try

                If Not IsPostBack And Not ViewState("SOItems") Is Nothing Then
                    Dim ds1 As DataSet = ViewState("SOItems")
                    If ds1.Tables(0).Rows.Count > 0 Then
                        Exit Sub
                    End If
                End If
                dsTemp = New DataSet
                Dim dtItem As New DataTable
                Dim dtSerItem As New DataTable
                Dim dtChildItems As New DataTable
                dtItem.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
                dtItem.Columns.Add("numItemCode")
                dtItem.Columns.Add("numUnitHour", GetType(Decimal))
                dtItem.Columns.Add("numOrigUnitHour")
                dtItem.Columns.Add("monPrice", GetType(Decimal))
                dtItem.Columns.Add("numUOM")
                dtItem.Columns.Add("vcUOMName")
                dtItem.Columns.Add("UOMConversionFactor")

                dtItem.Columns.Add("monTotAmount", GetType(Decimal))
                dtItem.Columns.Add("numSourceID")
                dtItem.Columns.Add("vcItemDesc")
                dtItem.Columns.Add("vcModelID")
                dtItem.Columns.Add("numWarehouseID")
                dtItem.Columns.Add("vcItemName")
                dtItem.Columns.Add("Warehouse")
                dtItem.Columns.Add("numWarehouseItmsID")
                dtItem.Columns.Add("ItemType")
                dtItem.Columns.Add("Attributes")
                dtItem.Columns.Add("Op_Flag")
                dtItem.Columns.Add("bitWorkOrder")
                dtItem.Columns.Add("vcInstruction")
                dtItem.Columns.Add("bintCompliationDate")
                dtItem.Columns.Add("numWOAssignedTo")

                dtItem.Columns.Add("DropShip", GetType(Boolean))
                dtItem.Columns.Add("bitDiscountType", GetType(Boolean))
                dtItem.Columns.Add("fltDiscount", GetType(Decimal))
                dtItem.Columns.Add("monTotAmtBefDiscount", GetType(Decimal))
                dtItem.Columns.Add("bitMarkupDiscount")

                If tintReturnType = 1 Or (tintReturnType = 2 AndAlso CCommon.ToBool(Session("PurchaseTaxCredit"))) Then
                    dtItem.Columns.Add("bitTaxable0")
                    dtItem.Columns.Add("Tax0", GetType(Decimal))
                    dtItem.Columns.Add("Tax1", GetType(Decimal))

                    '''Loading Other Taxes
                    Dim dtTaxTypes As DataTable
                    Dim ObjTaxItems As New TaxDetails
                    ObjTaxItems.DomainID = Session("DomainID")
                    dtTaxTypes = ObjTaxItems.GetTaxItems

                    For Each drTax As DataRow In dtTaxTypes.Rows
                        dtItem.Columns.Add("Tax" & drTax("numTaxItemID"), GetType(Decimal))
                        dtItem.Columns.Add("bitTaxable" & drTax("numTaxItemID"))
                    Next

                    Dim dr As DataRow
                    dr = dtTaxTypes.NewRow
                    dr("numTaxItemID") = 0
                    dr("vcTaxName") = "Sales Tax(Default)"
                    dtTaxTypes.Rows.Add(dr)

                    dr = dtTaxTypes.NewRow
                    dr("numTaxItemID") = 1
                    dr("vcTaxName") = "CRV"
                    dtTaxTypes.Rows.Add(dr)

                    chkTaxItems.DataTextField = "vcTaxName"
                    chkTaxItems.DataValueField = "numTaxItemID"
                    chkTaxItems.DataSource = dtTaxTypes
                    chkTaxItems.DataBind()
                End If

                dtItem.Columns.Add("numVendorWareHouse")
                dtItem.Columns.Add("numShipmentMethod")
                dtItem.Columns.Add("numSOVendorId")

                dtItem.Columns.Add("numProjectID")
                dtItem.Columns.Add("numProjectStageID")
                dtItem.Columns.Add("charItemType")
                dtItem.Columns.Add("numToWarehouseItemID") 'added by chintan for stock transfer
                dtItem.Columns.Add("bitIsAuthBizDoc", GetType(Boolean))
                dtItem.Columns.Add("numUnitHourReceived")
                dtItem.Columns.Add("numQtyShipped")
                dtItem.Columns.Add("vcBaseUOMName")
                dtItem.Columns.Add("numVendorID", System.Type.GetType("System.Int32"))
                dtItem.Columns.Add("vcSKU")
                dtItem.Columns.Add("numItemClassification")
                dtItem.Columns.Add("numPromotionID")
                dtItem.Columns.Add("bitPromotionTriggered")
                dtItem.Columns.Add("vcPromotionDetail")

                dtSerItem.Columns.Add("numWarehouseItmsDTLID")
                dtSerItem.Columns.Add("numWItmsID")
                dtSerItem.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
                dtSerItem.Columns.Add("vcSerialNo")
                dtSerItem.Columns.Add("Comments")
                dtSerItem.Columns.Add("Attributes")

                dtChildItems.Columns.Add("numOppChildItemID", System.Type.GetType("System.Int32"))
                dtChildItems.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
                dtChildItems.Columns.Add("numItemCode")
                dtChildItems.Columns.Add("numQtyItemsReq")
                dtChildItems.Columns.Add("vcItemName")
                dtChildItems.Columns.Add("monListPrice")
                dtChildItems.Columns.Add("UnitPrice")
                dtChildItems.Columns.Add("charItemType")
                dtChildItems.Columns.Add("txtItemDesc")
                dtChildItems.Columns.Add("numWarehouseItmsID")
                dtChildItems.Columns.Add("Op_Flag")
                dtChildItems.Columns.Add("ItemType")
                dtChildItems.Columns.Add("Attr")
                dtChildItems.Columns.Add("vcWareHouse")

                dtItem.TableName = "Item"
                dtSerItem.TableName = "SerialNo"
                dtChildItems.TableName = "ChildItems"

                dsTemp.Tables.Add(dtItem)
                dsTemp.Tables.Add(dtSerItem)
                dsTemp.Tables.Add(dtChildItems)
                dtItem.PrimaryKey = New DataColumn() {dsTemp.Tables(0).Columns("numoppitemtCode")}
                dtChildItems.PrimaryKey = New DataColumn() {dsTemp.Tables(2).Columns("numOppChildItemID")}
                ViewState("SOItems") = dsTemp
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function FillContact(ByVal ddlCombo As DropDownList)
            Try
                Dim fillCombo As New COpportunities
                With fillCombo
                    .DivisionID = radCmbCompany.SelectedValue
                    ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                    ddlCombo.DataTextField = "Name"
                    ddlCombo.DataValueField = "numcontactId"
                    ddlCombo.DataBind()
                End With
                ddlCombo.Items.Insert(0, New ListItem("---Select One---", "0"))
                If ddlCombo.Items.Count = 2 Then
                    ddlCombo.Items(1).Selected = True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub radCmbCompany_SelectedIndexChanged()
            Try
                If CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then

                    Dim objProj As New Project
                    objProj.DomainID = Session("DomainId")
                    objProj.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                    objProj.OppType = tintReturnType
                    objProj.bytemode = 5
                    ddlExistingOrders.DataSource = objProj.GetOpportunities()
                    ddlExistingOrders.DataTextField = "vcPOppName"
                    ddlExistingOrders.DataValueField = "numOppBizDocsId"
                    ddlExistingOrders.DataBind()
                    ddlExistingOrders.Items.Insert(0, New ListItem("--Select One--", "0"))

                    Dim objOpp As New OppBizDocs
                    If objOpp.ValidateARAP(radCmbCompany.SelectedValue, 0, Session("DomainID")) = False Then
                        ScriptManager.RegisterClientScriptBlock(UpdatePanelMain, UpdatePanelMain.GetType(), "Alert", "alert('Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting->Accounts for RelationShip"" To Save' );", True)
                        radCmbCompany.SelectedValue = ""
                        radCmbCompany.Text = ""
                        Exit Sub
                    End If

                    FillContact(ddlContact)
                    objItems = New CItems
                    objItems.DivisionID = radCmbCompany.SelectedValue
                    FillExistingAddress(CCommon.ToLong(radCmbCompany.SelectedValue))
                    Dim dtTable As DataTable
                    dtTable = objItems.GetAmountDue

                    Dim strBaseCurrency As String = ""
                    If (Session("MultiCurrency") = True) Then
                        strBaseCurrency = Session("Currency") + " "
                    End If

                    lblCreditLimit.Text = strBaseCurrency + String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("CreditLimit"))
                    lblRemaningCredit.Text = strBaseCurrency + String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemainingCredit"))

                    lblBalanceDue.Text = strBaseCurrency + String.Format("{0:#,##0.00}", IIf(tintReturnType = 2, dtTable.Rows(0).Item("AmountDuePO"), dtTable.Rows(0).Item("AmountDueSO")))
                    lblTotalAmtPastDue.Text = strBaseCurrency + String.Format("{0:#,##0.00}", IIf(tintReturnType = 2, dtTable.Rows(0).Item("AmountPastDuePO"), dtTable.Rows(0).Item("AmountPastDueSO")))
                    lblCreditBalance.Text = strBaseCurrency + String.Format("{0:#,##0.00}", IIf(tintReturnType = 2, dtTable.Rows(0).Item("PCreditMemo"), dtTable.Rows(0).Item("SCreditMemo")))


                    ViewState("PastDueAmount") = dtTable.Rows(0).Item("AmountPastDueSO")
                    If CCommon.ToBool(Session("bitAmountPastDue")) = True AndAlso CCommon.ToDecimal(dtTable.Rows(0).Item("AmountPastDueSO")) > CCommon.ToDecimal(Session("AmountPastDue")) Then
                        ScriptManager.RegisterClientScriptBlock(UpdatePanelMain, UpdatePanelMain.GetType(), "BlinkAmount", "setInterval('blinkIt()',500);", True)
                        lblTotalAmtPastDue.ForeColor = Color.Red
                        lblTotalAmtPastDue.Font.Bold = True
                    Else
                        lblTotalAmtPastDue.ForeColor = Color.Black
                        lblTotalAmtPastDue.Font.Bold = False

                    End If
                    objCommon = New CCommon
                    Try
                        objCommon.DivisionID = radCmbCompany.SelectedValue
                        objCommon.charModule = "D"
                        objCommon.GetCompanySpecificValues1()

                        If Session("PopulateUserCriteria") = 1 Then
                            objCommon.sb_FillConEmpFromTerritories(ddlAssignTo, Session("DomainID"), 0, 0, objCommon.TerittoryID)
                        ElseIf Session("PopulateUserCriteria") = 2 Then
                            objCommon.sb_FillConEmpFromDBUTeam(ddlAssignTo, Session("DomainID"), Session("UserContactID"))
                        Else : objCommon.sb_FillConEmpFromDBSel(ddlAssignTo, Session("DomainID"), 0, 0)
                        End If

                        Dim objUser As New UserAccess
                        objUser.DomainID = Session("DomainID")
                        objUser.byteMode = 1

                        Dim dt As DataTable = objUser.GetCommissionsContacts

                        Dim item As ListItem
                        Dim dr As DataRow
                        For Each dr In dt.Rows
                            item = New ListItem()
                            item.Text = dr("vcUserName")
                            item.Value = dr("numContactID")
                            ddlAssignTo.Items.Add(item)
                        Next

                        Dim lngOrgAssignedTo As Long
                        objCommon = New CCommon
                        objCommon.DomainID = Session("DomainID")
                        objCommon.Mode = 14
                        objCommon.Str = radCmbCompany.SelectedValue
                        lngOrgAssignedTo = objCommon.GetSingleFieldValue()

                        If ddlAssignTo.Items.FindByValue(lngOrgAssignedTo) IsNot Nothing Then
                            ddlAssignTo.ClearSelection()
                            ddlAssignTo.Items.FindByValue(lngOrgAssignedTo).Selected = True
                        End If
                    Catch ex As Exception

                    End Try

                    CCommon.UpdateItemRadComboValues(tintReturnType, radCmbCompany.SelectedValue)

                    BindUnAppliedPayments()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Function IsDuplicate(ByVal ItemCode As String, ByVal WareHouseItemID As String) As Boolean
            'validate duplicate entry on refresh / when js is disabled
            If (dsTemp.Tables(0).Select(" numItemCode='" & ItemCode & "'" & IIf(WareHouseItemID <> "", " and numWarehouseItmsID = '" & WareHouseItemID & "'", "")).Length > 0) Then
                UpdateDetails()
                Return True
            End If
            Return False
        End Function

        Private Sub BindAddressByAddressID(ByVal lngAddressID As Long, ByVal AddType As String)
            Try
                Dim objContact As New CContacts
                objContact.DomainID = Session("DomainID")
                objContact.AddressID = lngAddressID
                objContact.byteMode = 1
                Dim dtTable As DataTable = objContact.GetAddressDetail
                If dtTable.Rows.Count > 0 Then
                    If AddType = "Bill" Then
                        lblBillTo1.Text = CCommon.ToString(dtTable.Rows(0)("vcFullAddress"))
                        lblBillTo2.Text = ""
                    Else
                        lblShipTo1.Text = CCommon.ToString(dtTable.Rows(0)("vcFullAddress"))
                        lblShipTo2.Text = ""
                    End If
                Else
                    lblBillTo1.Text = ""
                    lblBillTo2.Text = ""
                    lblShipTo1.Text = ""
                    lblShipTo2.Text = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub UpdateDataTable()
            Try
                getTaxPercentage()

                If Not ViewState("SOItems") Is Nothing Then
                    dsTemp = ViewState("SOItems")
                    Dim dtItem As DataTable
                    dtItem = dsTemp.Tables(0)
                    If dtItem.Rows.Count > 0 Then
                        Dim i, k As Integer
                        i = 0

                        Dim strTaxes(), strTaxApplicable() As String

                        If tintReturnType = 1 Or (tintReturnType = 2 AndAlso CCommon.ToBool(Session("PurchaseTaxCredit"))) Then
                            strTaxes = txtTax.Text.Split(",")
                            strTaxApplicable = Taxable.Value.Split(",")
                        End If

                        For Each dgGridItem As DataGridItem In dgItems.Items

                            dtItem.Rows(i).Item("numUnitHour") = Math.Abs(CCommon.ToDecimal(DirectCast(dgGridItem.FindControl("txtUnits"), TextBox).Text))
                            dtItem.Rows(i).Item("monPrice") = CCommon.ToDecimal(CType(dgGridItem.FindControl("hdnOrigPrice"), HiddenField).Value)
                            dtItem.Rows(i).Item("monTotAmtBefDiscount") = CCommon.ToDouble((dtItem.Rows(i).Item("numUnitHour") * dtItem.Rows(i).Item("UOMConversionFactor")) * dtItem.Rows(i).Item("monPrice"))
                            If dtItem.Rows(i).Item("bitDiscountType") = 0 Then
                                If (dtItem.Rows(i).Item("bitMarkupDiscount").ToString() = "1") Then  ' Markup N Discount logic added
                                    dtItem.Rows(i).Item("monTotAmount") = CCommon.ToDouble(dtItem.Rows(i).Item("monTotAmtBefDiscount") + dtItem.Rows(i).Item("monTotAmtBefDiscount") * dtItem.Rows(i).Item("fltDiscount") / 100)
                                Else
                                    dtItem.Rows(i).Item("monTotAmount") = CCommon.ToDouble(dtItem.Rows(i).Item("monTotAmtBefDiscount") - dtItem.Rows(i).Item("monTotAmtBefDiscount") * dtItem.Rows(i).Item("fltDiscount") / 100)
                                End If
                            Else
                                Dim monPrice As Decimal = 0
                                Dim orgQty As Double = CCommon.ToDouble(dtItem.Rows(i).Item("numOrigUnitHour"))
                                Dim discount As Decimal = CCommon.ToDecimal(dtItem.Rows(i).Item("fltDiscount"))

                                If orgQty > 0 Then
                                    If (dtItem.Rows(i).Item("bitMarkupDiscount").ToString() = "1") Then  ' Markup N Discount logic added
                                        monPrice = dtItem.Rows(i).Item("monPrice") + CCommon.ToDouble(discount / orgQty)
                                    Else
                                        monPrice = dtItem.Rows(i).Item("monPrice") - CCommon.ToDouble(discount / orgQty)
                                    End If
                                Else
                                    monPrice = 0
                                End If

                                dtItem.Rows(i).Item("monTotAmount") = dtItem.Rows(i).Item("numUnitHour") * monPrice * dtItem.Rows(i).Item("UOMConversionFactor")
                            End If
                            'dtItem.Rows(i).Item("monTotAmount") = dtItem.Rows(i).Item("numUnitHour") * dtItem.Rows(i).Item("monPrice")
                            If dtItem.Rows(i).Item("Op_Flag") = 0 Then
                                dtItem.Rows(i).Item("Op_Flag") = 2
                            End If

                            If tintReturnType = 1 Or (tintReturnType = 2 AndAlso CCommon.ToBool(Session("PurchaseTaxCredit"))) Then
                                If rBtnExistingOrder.Checked Then
                                    If tintReturnType = 1 Or (tintReturnType = 2 AndAlso CCommon.ToBool(Session("PurchaseTaxCredit"))) Then
                                        Dim strTax() As String
                                        strTax = txtTax.Text.Split(",")

                                        Dim dtOppTax As DataTable = objCommon.GetOpportunityMasterTaxItems(CCommon.ToLong(ddlExistingOrders.SelectedValue.Split("-")(0)), dtItem.Rows(i).Item("numoppitemtCode"), 2)
                                        Dim drTax As DataRow()

                                        For k = 0 To chkTaxItems.Items.Count - 1
                                            drTax = dtOppTax.Select("numTaxItemID = " & chkTaxItems.Items(k).Value)

                                            If drTax.Length > 0 Then
                                                Dim decTaxValue As Decimal = CCommon.ToDecimal(drTax(0)("fltPercentage"))
                                                Dim tintTaxType As Short = CCommon.ToShort(drTax(0)("tintTaxType"))

                                                If tintTaxType = 2 Then 'FLAT AMOUNT
                                                    dtItem.Rows(i).Item("Tax" & chkTaxItems.Items(k).Value) = decTaxValue * (dtItem.Rows(i).Item("numUnitHour") * dtItem.Rows(i).Item("UOMConversionFactor"))
                                                Else 'PERCENTAGE
                                                    dtItem.Rows(i).Item("Tax" & chkTaxItems.Items(k).Value) = decTaxValue * dtItem.Rows(i).Item("monTotAmount") / 100
                                                End If

                                                dtItem.Rows(i).Item("bitTaxable" & chkTaxItems.Items(k).Value) = True
                                            Else
                                                dtItem.Rows(i).Item("Tax" & chkTaxItems.Items(k).Value) = 0
                                                dtItem.Rows(i).Item("bitTaxable" & chkTaxItems.Items(k).Value) = False
                                            End If
                                        Next
                                    End If
                                Else
                                    For k = 0 To chkTaxItems.Items.Count - 1
                                        If dtItem.Columns.Contains("bitTaxable" & chkTaxItems.Items(k).Value) Then
                                            If IIf(IsDBNull(dtItem.Rows(i).Item("bitTaxable" & chkTaxItems.Items(k).Value)), False, dtItem.Rows(i).Item("bitTaxable" & chkTaxItems.Items(k).Value)) = True Then
                                                Dim decTaxValue As Decimal = CCommon.ToDecimal(strTaxes(k).Split("#")(0))
                                                Dim tintTaxType As Short = CCommon.ToShort(strTaxes(k).Split("#")(1))

                                                If tintTaxType = 2 Then 'FLAT AMOUNT
                                                    dtItem.Rows(i).Item("Tax" & chkTaxItems.Items(k).Value) = decTaxValue * (dtItem.Rows(i).Item("numUnitHour") * dtItem.Rows(i).Item("UOMConversionFactor"))
                                                Else 'PERCENTAGE
                                                    dtItem.Rows(i).Item("Tax" & chkTaxItems.Items(k).Value) = IIf(IsDBNull(dtItem.Rows(i).Item("monTotAmount")), 0, dtItem.Rows(i).Item("monTotAmount")) * decTaxValue / 100
                                                End If
                                            End If
                                        End If
                                    Next
                                End If



                            End If

                            i = i + 1
                        Next
                    End If

                    dsTemp.AcceptChanges()

                    ViewState("SOItems") = dsTemp
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub Save()
            Try

                If tintReturnType = 3 Or tintReturnType = 4 Then
                    decSubTotal = CCommon.ToDecimal(txtAmount.Text)
                    decTaxTotal = 0
                    decDiscount = 0
                Else
                    UpdateDataTable()
                    UpdateDetails()
                End If

                objReturnHeader.ReturnHeaderID = 0
                objReturnHeader.Rma = ""
                objReturnHeader.BizDocName = ""
                objReturnHeader.DomainID = Session("DomainID")
                objReturnHeader.DivisionId = CCommon.ToLong(radCmbCompany.SelectedValue)
                objReturnHeader.ContactId = CCommon.ToLong(ddlContact.SelectedValue)

                If rBtnExistingOrder.Checked Then
                    objReturnHeader.OppId = CCommon.ToLong(ddlExistingOrders.SelectedValue.Split("-")(0))
                    objReturnHeader.OppBizDocID = CCommon.ToLong(ddlExistingOrders.SelectedValue.Split("-")(1))
                Else
                    objReturnHeader.OppId = 0
                    objReturnHeader.OppBizDocID = 0
                End If
                
                objReturnHeader.ReturnType = tintReturnType
                objReturnHeader.ReturnReason = CCommon.ToLong(ddlReturnReason.SelectedValue)
                objReturnHeader.ReturnStatus = CCommon.ToLong(enmReturnStatus.Pending)
                objReturnHeader.Amount = decSubTotal + decTaxTotal - decDiscount
                objReturnHeader.TotalTax = decTaxTotal
                objReturnHeader.TotalDiscount = decDiscount
                objReturnHeader.Comments = txtComments.Text
                objReturnHeader.UserCntID = Session("UserContactID")
                objReturnHeader.BillToAddressID = hdnBillAddressID.Value
                objReturnHeader.ShipToAddressID = hdnShipAddressID.Value

                If tintReturnType = 4 Then
                    If ddlUnAppliedPayments.SelectedValue <> "0" AndAlso ddlUnAppliedPayments.SelectedValue.Contains("~") Then
                        Dim strCol As String() = ddlUnAppliedPayments.SelectedValue.Split("~")
                        objReturnHeader.DepositIDRef = IIf(strCol(2) = 1, strCol(0), 0)
                        objReturnHeader.BillPaymentIDRef = IIf(strCol(2) = 2, strCol(0), 0)
                        objReturnHeader.lngParentID = IIf(strCol.Length = 4, CCommon.ToLong(strCol(3)), 0)
                    End If
                End If

                If ViewState("SOItems") IsNot Nothing AndAlso (tintReturnType = 1 Or tintReturnType = 2) Then

                    dsTemp = DirectCast(ViewState("SOItems"), DataSet)

                    Dim dtItems As New DataTable
                    Dim dsItems As New DataSet

                    dtItems.TableName = "Item"
                    dtItems.Columns.Add("numReturnItemID")
                    dtItems.Columns.Add("numItemCode")
                    dtItems.Columns.Add("numUnitHour")
                    dtItems.Columns.Add("numUnitHourReceived")
                    dtItems.Columns.Add("monPrice")
                    dtItems.Columns.Add("bitDiscountType")
                    dtItems.Columns.Add("fltDiscount")
                    dtItems.Columns.Add("monTotAmount")
                    dtItems.Columns.Add("vcItemDesc")
                    dtItems.Columns.Add("numWareHouseItemID")
                    dtItems.Columns.Add("vcModelID")
                    dtItems.Columns.Add("vcManufacturer")
                    dtItems.Columns.Add("numUOMId")
                    dtItems.Columns.Add("numOppItemID")
                    dtItems.Columns.Add("bitMarkupDiscount")
                    If tintReturnType = 1 AndAlso rBtnExistingOrder.Checked Then
                        dtItems.Columns.Add("numOppBizDocItemID")
                    End If

                    Dim drItem As DataRow
                    For i As Integer = 0 To dsTemp.Tables(0).Rows.Count - 1
                        Dim units As Double = CCommon.ToDouble(dsTemp.Tables(0).Rows(i)("numUnitHour")) * CCommon.ToDouble(dsTemp.Tables(0).Rows(i)("UOMConversionFactor"))
                        Dim price As Decimal = CCommon.ToDecimal(dsTemp.Tables(0).Rows(i)("monPrice"))

                        drItem = dtItems.NewRow
                        drItem("numReturnItemID") = 0
                        drItem("numItemCode") = CCommon.ToLong(dsTemp.Tables(0).Rows(i)("numItemCode"))
                        drItem("numUnitHour") = units
                        drItem("numUnitHourReceived") = CCommon.ToDouble(dsTemp.Tables(0).Rows(i)("numUnitHour"))
                        drItem("monPrice") = CCommon.ToDecimal(dsTemp.Tables(0).Rows(i)("monPrice"))
                        drItem("bitDiscountType") = CCommon.ToBool(dsTemp.Tables(0).Rows(i)("bitDiscountType"))
                        drItem("fltDiscount") = CCommon.ToDecimal(dsTemp.Tables(0).Rows(i)("fltDiscount"))
                        drItem("monTotAmount") = CCommon.ToDecimal(dsTemp.Tables(0).Rows(i)("monTotAmount"))
                        drItem("bitMarkupDiscount") = CCommon.ToString(dsTemp.Tables(0).Rows(i)("bitMarkupDiscount"))

                        If CCommon.ToDecimal(dsTemp.Tables(0).Rows(i)("fltDiscount")) > 0 Then
                            Dim orgQty As Double = CCommon.ToDouble(dsTemp.Tables(0).Rows(i)("numOrigUnitHour"))
                            Dim monPrice As Decimal = 0
                            Dim discount As Decimal = CCommon.ToDecimal(dsTemp.Tables(0).Rows(i)("fltDiscount"))

                            'Discount in amount
                            If CCommon.ToBool(dsTemp.Tables(0).Rows(i)("bitDiscountType")) Then

                                If orgQty > 0 Then
                                    If (drItem("bitMarkupDiscount").ToString() = "1") Then  ' Markup,Discount logic change
                                        monPrice = CCommon.ToDouble(price + (discount / orgQty))
                                    Else
                                        monPrice = CCommon.ToDouble(price - (discount / orgQty))
                                    End If
                                Else
                                    monPrice = 0
                                End If

                                drItem("monTotAmount") = units * monPrice
                            Else 'Discount in percent
                                drItem("monTotAmount") = CCommon.ToDouble(units * price) - (CCommon.ToDouble(units * price) * (discount / 100))
                            End If
                        Else
                            drItem("monTotAmount") = CCommon.ToDecimal(dsTemp.Tables(0).Rows(i)("monPrice")) * units
                        End If

                        drItem("vcItemDesc") = CCommon.ToString(dsTemp.Tables(0).Rows(i)("vcItemDesc"))
                        drItem("numWareHouseItemID") = CCommon.ToLong(dsTemp.Tables(0).Rows(i)("numWareHouseItmsID"))
                        drItem("vcModelID") = CCommon.ToString(dsTemp.Tables(0).Rows(i)("vcModelID"))
                        drItem("vcManufacturer") = ""
                        drItem("numUOMId") = CCommon.ToLong(dsTemp.Tables(0).Rows(i)("numUOM"))
                        drItem("numOppItemID") = IIf(rBtnExistingOrder.Checked, CCommon.ToLong(dsTemp.Tables(0).Rows(i)("numoppitemtCode")), 0)
                        If tintReturnType = 1 AndAlso rBtnExistingOrder.Checked Then
                            drItem("numOppBizDocItemID") = IIf(rBtnExistingOrder.Checked, CCommon.ToLong(dsTemp.Tables(0).Rows(i)("numOppBizDocItemID")), 0)
                        End If

                        If (arrSelectedItem.Contains(dsTemp.Tables(0).Rows(i)("numoppitemtCode"))) Then
                            dtItems.Rows.Add(drItem)
                            dtItems.AcceptChanges()
                        End If
                    Next

                    If dtItems.Rows.Count = 0 Then
                        litMessage.Text = "Please select line item from ""items to return"" grid to return!"
                        Exit Sub
                    End If

                    dsItems.Tables.Add(dtItems)
                    objReturnHeader.strItems = dsItems.GetXml()
                Else
                    objReturnHeader.strItems = ""
                End If

                objReturnHeader.ReceiveType = 0
                If (tintReturnType = 3 Or tintReturnType = 4) AndAlso radCmbSalesOrder.Visible AndAlso CCommon.ToLong(radCmbSalesOrder.SelectedValue) > 0 Then
                    objReturnHeader.ReferenceSalesOrder = CCommon.ToLong(radCmbSalesOrder.SelectedValue)
                Else
                    objReturnHeader.ReferenceSalesOrder = 0
                End If

                If tintReturnType = 3 OrElse tintReturnType = 4 OrElse dgItems.Items.Count >= 1 Then
                    lngReturnHeaderID = objReturnHeader.ManageReturnHeader()
                Else
                    litMessage.Text = "Add items to create Sales Return."
                    Exit Sub
                End If

                If lngReturnHeaderID > 0 Then
                    ScriptManager.RegisterClientScriptBlock(UpdatePanelMain, UpdatePanelMain.GetType(), "Open", "window.opener.reDirectPage('../opportunity/frmReturnDetail.aspx?ReturnID=" & lngReturnHeaderID.ToString() & "&type=" & tintReturnType & "'); self.close();", True)
                ElseIf lngReturnHeaderID = 0 Then
                    litMessage.Text = "Error occurred while saving records."
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub FillExistingAddress(ByVal lngDivisionId As Long)
            Try
                lblBillTo1.Text = ""
                lblShipTo1.Text = ""
                hdnBillAddressID.Value = 0
                hdnShipAddressID.Value = 0
                If Not IsNothing(lngDivisionId) Then
                    Dim oCOpportunities As New COpportunities
                    oCOpportunities.DomainID = CType((Session("DomainID")), Long)
                    Dim dtAddress As DataTable = oCOpportunities.GetExistingAddress(CInt(Session("UserID")), CInt(lngDivisionId))
                    lblBillTo1.Text = CCommon.ToString(dtAddress(0)("vcBillAddress"))
                    lblShipTo1.Text = CCommon.ToString(dtAddress(0)("vcShipAddress"))

                    hdnBillAddressID.Value = dtAddress(0)("numBillAddress")
                    hdnShipAddressID.Value = dtAddress(0)("numShipAddress")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindUnAppliedPayments()
            Try
                'Bind data only for Refund
                If tintReturnType = 4 Then
                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.UserCntID = Session("UserContactID")
                    objCommon.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)

                    'Get all Unapplied Payment & Credit Memo for Bill Payment & Receive Payment
                    Dim objReturns As New Returns
                    objReturns.DivisionId = CCommon.ToLong(radCmbCompany.SelectedValue)
                    objReturns.DomainID = Session("DomainID")
                    objReturns.ReferenceID = 0
                    objReturns.CurrencyID = 0
                    objReturns.AccountClass = objCommon.GetAccountingClass()

                    ddlUnAppliedPayments.Items.Clear()
                    ddlUnAppliedPayments.Items.Add(New ListItem("--Select--", 0))

                    'Deposit Credits
                    objReturns.Mode = 1
                    Dim dtCredits As DataTable = objReturns.GetCustomerCredits

                    Dim item As ListItem
                    For Each dr As DataRow In dtCredits.Rows
                        item = New ListItem()
                        item.Text = CCommon.ToString(dr("vcReference")) & " - " & ReturnMoney(dr("monAmount")) & " - " & ReturnMoney(dr("monNonAppliedAmount"))
                        item.Value = CCommon.ToLong(dr("numReferenceID")) & "~" & ReturnMoney(dr("monNonAppliedAmount")) & "~1~" & CCommon.ToLong(dr("numReturnHeaderID"))
                        item.Attributes("OptionGroup") = "Sales"
                        ddlUnAppliedPayments.Items.Add(item)
                    Next

                    'Bill Payment Credits
                    objReturns.Mode = 2
                    Dim dtBPCredits As DataTable = objReturns.GetCustomerCredits

                    For Each dr As DataRow In dtBPCredits.Rows
                        item = New ListItem()
                        item.Text = dr("vcReference") & " - " & ReturnMoney(dr("monAmount")) & " - " & ReturnMoney(dr("monNonAppliedAmount"))
                        item.Value = dr("numReferenceID") & "~" & ReturnMoney(dr("monNonAppliedAmount")) & "~2~" & CCommon.ToLong(dr("numReturnHeaderID"))
                        item.Attributes("OptionGroup") = "Purcase"
                        ddlUnAppliedPayments.Items.Add(item)
                    Next

                    'pnlUnAppliedPayment.Visible = IIf(dtCredits.Rows.Count > 0 Or dtBPCredits.Rows.Count > 0, True, False)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
#End Region

#Region "GRID EVENTS"

        Private Sub dgItems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgItems.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then

                    If DataBinder.Eval(e.Item.DataItem, "charItemType") = "S" Then
                        CType(e.Item.FindControl("txtUnits"), TextBox).Attributes.Add("onkeypress", "CheckNumber(1,event)")
                    Else
                        CType(e.Item.FindControl("txtUnits"), TextBox).Attributes.Add("onkeypress", "CheckNumber(2,event)")
                    End If

                    CType(e.Item.FindControl("txtUnits"), TextBox).Attributes.Add("onkeyup", "return CalculateTotal()")

                    CType(e.Item.FindControl("txtUnitPrice"), TextBox).Attributes.Add("onkeyup", "return CalculateTotal()")
                    CType(e.Item.FindControl("txtUnitPrice"), TextBox).Attributes.Add("onkeypress", "CheckNumber(1,event)")


                    If pnlOrders.Visible = True Then
                        CType(e.Item.FindControl("txtUnitPrice"), TextBox).Enabled = False
                    End If

                    Dim chkSelect As CheckBox
                    Dim lblItemCode As Label
                    If Not e.Item.FindControl("chkSelect") Is Nothing AndAlso Not e.Item.FindControl("lblOppItemCode") Is Nothing Then
                        chkSelect = DirectCast(e.Item.FindControl("chkSelect"), CheckBox)
                        lblItemCode = DirectCast(e.Item.FindControl("lblOppItemCode"), Label)
                        If arrSelectedItem.Contains(lblItemCode.Text) Then
                            chkSelect.Checked = True
                        End If
                    End If

                ElseIf e.Item.ItemType = ListItemType.Header Then
                    CType(e.Item.FindControl("lblUnitPriceCaption"), Label).Text = IIf(1 = 1, "Unit Price", "Unit Cost")

                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

#End Region

        Private Sub radWareHouse_ItemDataBound(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemEventArgs) Handles radWareHouse.ItemDataBound
            Try
                e.Item.Text = CType(e.Item.DataItem, DataRowView)("vcWareHouse").ToString() & " (" & CType(e.Item.DataItem, DataRowView)("numOnHand").ToString() & ") " & " ---- " & CType(e.Item.DataItem, DataRowView)("Attr").ToString().TrimEnd(",")
                e.Item.Value = CType(e.Item.DataItem, DataRowView)("numWareHouseItemId").ToString()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub radWareHouse_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radWareHouse.SelectedIndexChanged
            Try
                If (radWareHouse.SelectedValue <> "" And txtUnits.Text <> "") Then
                    CalculateUnitPrice()
                    ViewState("SelectedWarehouse") = CType(radWareHouse.Items(radWareHouse.SelectedIndex).FindControl("lblWareHouseID"), Label).Text
                End If
            Catch ex As Exception
                LogError(ex)
            End Try
        End Sub

        Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
            Try
                Dim dtItem As DataTable
                dtItem = dsTemp.Tables(0)

                For Each dgItem As DataGridItem In dgItems.Items
                    If DirectCast(dgItem.FindControl("chkSelect"), CheckBox).Checked = True Then
                        dtItem.Rows.Find(CType(dgItem.FindControl("lblOppItemCode"), Label).Text).Delete()
                        dtItem.AcceptChanges()
                    End If
                Next

                UpdateDetails()
                clearControls()
            Catch ex As Exception
                LogError(ex)
            End Try
        End Sub
        Public Function ReturnMoney(ByVal Money As Object) As String
            Try
                If Not IsDBNull(Money) Then Return String.Format("{0:###0.00}", Money)

                Return ""
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub ddlUnAppliedPayments_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUnAppliedPayments.SelectedIndexChanged
            Try

            Catch ex As Exception
                LogError(ex)
            End Try
        End Sub

        Private Sub LogError(ByVal ex As Exception)
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            lblError.Text = ex.Message
        End Sub

        Private Sub txtprice_TextChanged(sender As Object, e As EventArgs) Handles txtprice.TextChanged
            Try
                hdnOrigUnitPrice.Value = CCommon.ToDecimal(txtprice.Text) / CCommon.ToDecimal(hdvUOMConversionFactor.Value)
            Catch ex As Exception
                LogError(ex)
            End Try
        End Sub
    End Class
End Namespace


Public Class CreateTempColn1
    Implements ITemplate

    Dim TemplateType As ListItemType
    Dim Field1 As String

    Sub New(ByVal type As ListItemType, ByVal fld1 As String)
        Try
            TemplateType = type
            Field1 = fld1
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub InstantiateIn(ByVal Container As Control) Implements ITemplate.InstantiateIn
        Try
            Dim lbl1 As Label = New Label()
            Dim lbl2 As Label = New Label()
            Dim lbl3 As Label = New Label()
            'Dim lbl4 As Label = New Label()
            Select Case TemplateType
                Case ListItemType.Footer
                    lbl3.ID = "lblFTaxAmt" & Field1
                    lbl3.EnableViewState = False
                    AddHandler lbl3.DataBinding, AddressOf BindStringColumn3
                    Container.Controls.Add(lbl3)
                Case ListItemType.Item
                    lbl1.ID = "lblTaxAmt" & Field1
                    lbl1.EnableViewState = False
                    lbl2.ID = "lblTaxable" & Field1
                    lbl2.EnableViewState = False
                    'lbl4.ID = "lblID" & Field1
                    AddHandler lbl1.DataBinding, AddressOf BindStringColumn1
                    AddHandler lbl2.DataBinding, AddressOf BindStringColumn2
                    'AddHandler lbl4.DataBinding, AddressOf BindStringColumn4
                    lbl2.Attributes.Add("style", "display:none")
                    Container.Controls.Add(lbl1)
                    Container.Controls.Add(lbl2)
                    'Container.Controls.Add(lbl4)

            End Select
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindStringColumn1(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl1 As Label = CType(Sender, Label)
            Dim Container As DataGridItem = CType(lbl1.NamingContainer, DataGridItem)
            lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "Tax" & Field1)), "", DataBinder.Eval(Container.DataItem, "Tax" & Field1))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindStringColumn2(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl1 As Label = CType(Sender, Label)
            Dim Container As DataGridItem = CType(lbl1.NamingContainer, DataGridItem)
            lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "bitTaxable" & Field1)), "", DataBinder.Eval(Container.DataItem, "bitTaxable" & Field1))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindStringColumn3(ByVal Sender As Object, ByVal e As EventArgs)
        Try
            Dim lbl1 As Label = CType(Sender, Label)
            Dim Container As DataGridItem = CType(lbl1.NamingContainer, DataGridItem)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class