﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities

Public Class frm3PLEDIHistory
    Inherits BACRMPage

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblError.Text = ""
            divError.Style.Add("display", "none")
            divNoData.Attributes.Add("display", "none")

            If Not Page.IsPostBack Then
                hdnOppID.Value = GetQueryStringVal("numOppID")

                If CCommon.ToLong(hdnOppID.Value) = 0 Then
                    DisplayError("Sales order does not exists.")
                Else
                    BindOrder3PLEDIHistory()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub BindOrder3PLEDIHistory()
        Try
            Dim objEDIQueueLog As New EDIQueueLog
            objEDIQueueLog.DomainID = CCommon.ToLong(Session("DomainID"))
            objEDIQueueLog.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            objEDIQueueLog.OppID = CCommon.ToLong(hdnOppID.Value)
            Dim dtData As DataTable = objEDIQueueLog.GetByOrder()

            If Not dtData Is Nothing AndAlso dtData.Rows.Count > 0 Then
                rptEDIHistory.DataSource = dtData
                rptEDIHistory.DataBind()
            Else
                rptEDIHistory.Visible = False
                divNoData.Style.Add("display", "")
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplayError(ByVal errorMessage As String)
        Try
            lblError.Text = errorMessage
            divError.Style.Add("display", "")
        Catch ex As Exception
            'DO NOT THROW ERROR
        End Try
    End Sub

#End Region
    
End Class