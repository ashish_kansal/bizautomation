﻿'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Alerts
Imports System.IO
Partial Public Class frmBillofLading
    Inherits BACRMPage
    Dim dtOppBiDocItems As New DataTable
    Dim lintJournalId As Integer
    Dim lngOppBizDocID, lngOppId As Long


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            btnClose.Attributes.Add("onclick", "return Close()")
            btnPrint.Attributes.Add("onclick", "return PrintIt()")
            lblBizDoc.Text = GetQueryStringVal( "Name")
            lngOppBizDocID = GetQueryStringVal( "OppBizId")
            lngOppId = GetQueryStringVal( "OpID")
            'btnClose.Attributes.Add("onclick", "return Close()")
            If Not IsPostBack Then
                
                Dim objCommon As New CCommon

                getAddDetails()
                getBizDocDetails()
            End If
            'btnPrint.Attributes.Add("onclick", "return PrintIt();")

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub getBizDocDetails()
        Try
            Dim dtOppBiDocDtl As DataTable
            Dim objOppBizDocs As New OppBizDocs
            objOppBizDocs.OppBizDocId = lngOppBizDocID
            objOppBizDocs.OppId = lngOppId
            objOppBizDocs.DomainID = Session("DomainID")
            objOppBizDocs.UserCntID = Session("UserContactID")
            objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl
            Dim dtDetails As DataTable
            Dim objOpportunity As New MOpportunity
            objOpportunity.OpportunityId = lngOppId
            objOpportunity.DomainID = Session("DomainID")
            objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            dtDetails = objOpportunity.OpportunityDTL.Tables(0)

            If dtOppBiDocDtl.Rows.Count <> 0 Then
                hplBillto.Attributes.Add("onclick", "return openeditAddress('Bill','" & lngOppId & "')")
                hplShipTo.Attributes.Add("onclick", "return openeditAddress('Ship','" & lngOppId & "')")
                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcBizDocImagePath")) Then
                    imgLogo.Visible = True
                    imgLogo.ImageUrl = "../../" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName") & "/documents/docs/" & dtOppBiDocDtl.Rows(0).Item("vcBizDocImagePath")
                Else : imgLogo.Visible = False
                End If
                txtComments.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("txtComments")), "", dtDetails.Rows(0).Item("txtComments"))
            End If
            Dim ds As New DataSet
            Dim dtOppBiDocItems As DataTable
            objOppBizDocs.DomainID = Session("DomainID")
            objOppBizDocs.OppBizDocId = lngOppBizDocID
            objOppBizDocs.OppId = lngOppId
            ds = objOppBizDocs.GetOppInItems
            dtOppBiDocItems = ds.Tables(0)
            BindItems(dtOppBiDocItems)
        
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub getAddDetails()
        Try
            Dim dtOppBizAddDtl As DataTable
            Dim objOppBizDocs As New OppBizDocs
            objOppBizDocs.OppBizDocId = lngOppBizDocID
            objOppBizDocs.OppId = lngOppId
            objOppBizDocs.DomainID = Session("DomainID")
            dtOppBizAddDtl = objOppBizDocs.GetOppAddressDetail
            If dtOppBizAddDtl.Rows.Count <> 0 Then
                'lblAddress.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("CompName")), "", dtOppBizAddDtl.Rows(0).Item("CompName"))
                lblBillTo.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("BillAdd")), "", dtOppBizAddDtl.Rows(0).Item("BillAdd"))
                lblShipTo.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("ShipAdd")), "", dtOppBizAddDtl.Rows(0).Item("ShipAdd"))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindItems(ByVal dt As DataTable)
        Try
            dgBizDocs.DataSource = dt
            dgBizDocs.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    

    Function ReturnMoney(ByVal Money)
        Try
            Return String.Format("{0:#,###.##}", Math.Round(Money, 2))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class