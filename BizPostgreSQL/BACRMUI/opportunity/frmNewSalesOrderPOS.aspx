﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmNewSalesOrderPOS.aspx.vb"
    Inherits="BACRM.UserInterface.Opportunities.frmNewSalesOrderPOS" ClientIDMode="Static"
    MaintainScrollPositionOnPostback="true" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Src="orderPOS.ascx" TagName="order" TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>POS : Sales Order</title>
    <script src="../JavaScript/Orders.js" type="text/javascript"></script>
    <script src="../JavaScript/Common.js" type="text/javascript"></script>
    <script src="../JavaScript/jquery.scrollTo-min.js" type="text/javascript"></script>
    <style type="text/css">
        .column {
            float: left;
        }

        html > body .RadComboBoxDropDown_Vista .rcbItem, html > body .RadComboBoxDropDown_Vista .rcbHovered, html > body .RadComboBoxDropDown_Vista .rcbDisabled, html > body .RadComboBoxDropDown_Vista .rcbLoading {
            display: inline-block;
        }

        .keypad_clear, .keypad_Mode, .keypad_digit, .Button_POS {
            background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #cfcdcf) );
            background: -moz-linear-gradient( center top, #ededed 5%, #cfcdcf 100% );
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#cfcdcf');
            background-color: #ededed;
            -moz-border-radius: 42px;
            -webkit-border-radius: 42px;
            border-radius: 42px;
            border: 1px solid #52658c;
            display: inline-block;
            color: #777777;
            font-family: arial;
            font-size: 15px;
            font-weight: bold;
            padding: 7px 15px;
            text-decoration: none;
        }

            .keypad_clear:hover, .keypad_Mode:hover, .keypad_digit:hover, .Button_POS:hover {
                background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #cfcdcf), color-stop(1, #ededed) );
                background: -moz-linear-gradient( center top, #cfcdcf 5%, #ededed 100% );
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#cfcdcf', endColorstr='#ededed');
                background-color: #cfcdcf;
            }

            .keypad_clear:active, .keypad_Mode:active, .keypad_digit:active, .Button_POS:active {
                position: relative;
                top: 1px;
            }

        .keypad_ModeSelect {
            background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #52658c), color-stop(1, #52658c) );
            background: -moz-linear-gradient( center top, #52658c 5%, #52658c 100% );
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#52658c', endColorstr='#52658c');
            background-color: #52658c;
            -moz-border-radius: 42px;
            -webkit-border-radius: 42px;
            border-radius: 42px;
            border: 1px solid #52658c;
            display: inline-block;
            color: #ffffff;
            font-family: arial;
            font-size: 15px;
            font-weight: bold;
            padding: 7px 15px;
            text-decoration: none;
        }

            .keypad_ModeSelect:hover {
                background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #52658c), color-stop(1, #52658c) );
                background: -moz-linear-gradient( center top, #52658c 5%, #52658c 100% );
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#52658c', endColorstr='#52658c');
                background-color: #52658c;
            }

            .keypad_ModeSelect:active {
                position: relative;
                top: 1px;
            }

        TABLE.NewGrid {
            background-color: #74aefa;
            font-family: 'Segoe UI' Arial;
            font-size: 8pt;
            border-color: #BBB;
        }

            TABLE.NewGrid TR {
                background-color: white;
            }

            TABLE.NewGrid .hs, TABLE.NewGrid .footer, .footer {
                background: -webkit-gradient( linear, left top, left bottom, color-stop(0.05, #ededed), color-stop(1, #cfcdcf) );
                background: -moz-linear-gradient( center top, #ededed 5%, #cfcdcf 100% );
                filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ededed', endColorstr='#cfcdcf');
                background-color: #ededed;
                font-size: 10pt;
                color: Gray;
            }

            TABLE.NewGrid TD, .selected td {
                padding: 5px;
                border-color: #BBB;
            }

        .selected td {
            background-color: Gray;
            color: White;
        }

        .Section {
            width: 800px;
            height: 400px;
            overflow-y: scroll;
        }

        .fixedTable {
            position: fixed;
        }

        .blink {
            color: Red;
            font-weight: bold;
            font-family: Verdana, Tahoma;
            font-size: 14px;
        }
    </style>
    <script language="javascript" type="text/javascript">
        jQuery.noConflict();

        function openItem(a, b, c, d, e) {

            if ($find('radCmbItem').get_value() == "") {
                a = 0
            }
            else {
                a = $find('radCmbItem').get_value()
            }
            if (document.getElementById("radExistingCustomer").checked == true) {
                if ($find('radCmbCompany').get_value() == "") {
                    c = 0
                }
                else {
                    c = $find('radCmbCompany').get_value();
                }
            }
            else {
                c = 0;
            }

            if ($ControlFind(b).value == "") {
                b = 0
            }
            else {
                b = $ControlFind(b).value
            }

            var CalPrice = 0;
            if ($ControlFind('hdKit').value == 'True' && ($ControlFind('dgKitItem') != null)) {
                jQuery('ctl00_Content_order1_dgKitItem tr').not(':first,:last').each(function () {
                    var str;
                    str = jQuery(this).find("[id*='lblUnitPrice']").text();
                    CalPrice = CalPrice + (parseFloat(str.split('/')[0]) * jQuery(this).find("input[id*='txtKitUits']").val());
                });
            }

            //            var CalPrice = 0;
            //            if ($('hdKit').value == 'True' && ($('dgKitItem') != null)) {

            //                var index;
            //                for (i = 1; i < $('dgKitItem').rows.length; i++) {
            //                    if (i <= 8) {
            //                        index = '0' + (i + 1)
            //                    }
            //                    else {
            //                        index = (i + 1);
            //                    }
            //                    var str;
            //                    if (document.all) {
            //                        str = $('dgKitItem_ctl' + index + '_lblUnitPrice').innerText;
            //                    } else {
            //                        str = $('dgKitItem_ctl' + index + '_lblUnitPrice').textContent;
            //                    }

            //                    CalPrice = CalPrice + (parseFloat(str.split('/')[0]) * $('dgKitItem_ctl' + index + '_txtKitUits').value)

            //                }
            //            }
            var WareHouseItemID = 0;
            if ($find('radWareHouse') != null) {
                if ($find('radWareHouse').get_value() == "") {
                    WareHouseItemID = 0
                }
                else {
                    WareHouseItemID = $find('radWareHouse').get_value()
                }
            }
            if (a != 0 && b != 0) {
                window.open('../opportunity/frmItemPriceRecommd.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=' + a + '&Unit=' + b + '&DivID=' + c + '&OppType=' + e + '&CalPrice=' + CalPrice + '&WarehouseItemID=' + WareHouseItemID, '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')

            }
        }

        function Save() {
            if (document.getElementById("radExistingCustomer").checked == true) {
                if ($find('radCmbCompany').get_value() == "") {
                    alert("Select Customer")
                    return false;
                }
                if (document.getElementById("ddlContact").value == 0) {
                    alert("Select Contact")
                    return false;
                }
            }
            else {
                if (document.getElementById("txtFirstName").value == '') {
                    alert("Enter First Name")
                    return false;
                }
                if (document.getElementById("txtLastName").value == '') {
                    alert("Enter Last Name")
                    return false;
                }
                if (document.getElementById("ddlRelationship").value == 0) {
                    alert("Select Relationship ")
                    return false;
                }
            }
            return true;
        }

        function CalculateTotal() {
            var Total, subTotal, TaxAmount, ShipAmt, IndTaxAmt, discount;
            var index, findex;
            Total = 0;
            TaxAmount = 0;
            subTotal = 0;
            IndTaxAmt = 0;
            var strtaxItemIDs = $ControlFind('TaxItemsId').value.split(',')
            var strTax = $ControlFind('txtTax').value.split(',')
            //            if (isNaN($('txtShipCost').value) == false && $('txtShipCost').value != '') {
            //                ShipAmt = parseFloat($('txtShipCost').value);

            //            }
            //            else {
            ShipAmt = 0;
            //            }

            jQuery('#ctl00_Content_order1_dgItems tr').not(':first,:last').each(function () {
                //                if (parseFloat(jQuery(this).find("input[id*='txtUnits']").val()) <= 0) {
                //                    alert("Units must greater than 0.")
                //                    jQuery(this).find("input[id*='txtUnits']").focus();
                //                    return false;
                //                }

                subTotal = (parseFloat(jQuery(this).find("input[id*='txtUnits']").val()) * parseFloat(jQuery(this).find("input[id*='txtUOMConversionFactor']").val())) * parseFloat(jQuery(this).find("input[id*='txtUnitPrice']").val());

                if (jQuery(this).find("input[id*='txtDiscountType']").val() == 'True') {
                    subTotal = subTotal - parseFloat(jQuery(this).find("input[id*='txtTotalDiscount']").val());
                }
                else {
                    discount = (subTotal * parseFloat(jQuery(this).find("input[id*='txtTotalDiscount']").val())) / 100;
                    subTotal = subTotal - discount;

                    jQuery(this).find("[id*='lblDiscount']").text(CommaFormatted(roundNumber(discount, 2)) + ' (' + parseFloat(jQuery(this).find("input[id*='txtTotalDiscount']").val()) + '%)');
                }

                jQuery(this).find("[id*='lblTotal']").text(CommaFormatted(roundNumber(subTotal, 2)));

                Total = parseFloat(Total) + parseFloat(subTotal);

                for (k = 0; k < strtaxItemIDs.length; k++) {
                    if (jQuery(this).find("[id*='lblTaxable" + strtaxItemIDs[k] + "']").text() != 'False') {
                        jQuery(this).find("[id*='lblTaxAmt" + strtaxItemIDs[k] + "']").text(parseFloat(strTax[k] * parseFloat(subTotal) / 100));
                        TaxAmount = parseFloat(TaxAmount) + parseFloat(jQuery(this).find("[id*='lblTaxAmt" + strtaxItemIDs[k] + "']").text());
                    }
                }
            });

            //            for (i = 1; i < $('order1_dgItems').rows.length - 1; i++) {
            //                if (i <= 8) {
            //                    index = '0' + (i + 1)
            //                }
            //                else {
            //                    index = (i + 1);
            //                }

            //                //                if (parseFloat($('order1_dgItems_ctl' + index + '_txtUnits').value) <= 0) {
            //                //                    alert("Units must greater than 0.")
            //                //                    $('order1_dgItems_ctl' + index + '_txtUnits').focus();
            //                //                    return false;
            //                //                }

            //                subTotal = (parseFloat($('order1_dgItems_ctl' + index + '_txtUnits').value) * parseFloat($('order1_dgItems_ctl' + index + '_txtUOMConversionFactor').value)) * parseFloat($('order1_dgItems_ctl' + index + '_txtUnitPrice').value);

            //                if ($('order1_dgItems_ctl' + index + '_txtDiscountType').value == 'True') {
            //                    subTotal = subTotal - parseFloat($('order1_dgItems_ctl' + index + '_txtTotalDiscount').value);
            //                }
            //                else {
            //                    discount = (subTotal * parseFloat($('order1_dgItems_ctl' + index + '_txtTotalDiscount').value)) / 100;
            //                    subTotal = subTotal - discount;

            //                    if (document.all) {
            //                        $('order1_dgItems_ctl' + index + '_lblDiscount').innerText = CommaFormatted(roundNumber(discount, 2)) + ' (' + parseFloat($('order1_dgItems_ctl' + index + '_txtTotalDiscount').value) + '%)';
            //                    } else {
            //                        $('order1_dgItems_ctl' + index + '_lblDiscount').textContent = CommaFormatted(roundNumber(discount, 2)) + ' (' + parseFloat($('order1_dgItems_ctl' + index + '_txtTotalDiscount').value) + '%)';
            //                    }
            //                }

            //                if (document.all) {
            //                    $('order1_dgItems_ctl' + index + '_lblTotal').innerText = CommaFormatted(roundNumber(subTotal, 2));
            //                } else {
            //                    $('order1_dgItems_ctl' + index + '_lblTotal').textContent = CommaFormatted(roundNumber(subTotal, 2));
            //                }

            //                Total = parseFloat(Total) + parseFloat(subTotal);

            //                for (k = 0; k < strtaxItemIDs.length; k++) {

            //                    if (document.all) {
            //                        if ($('order1_dgItems_ctl' + index + '_lblTaxable' + strtaxItemIDs[k]).innerText != 'False') {
            //                            $('order1_dgItems_ctl' + index + '_lblTaxAmt' + strtaxItemIDs[k]).innerText = parseFloat(strTax[k] * parseFloat(subTotal) / 100)
            //                            TaxAmount = parseFloat(TaxAmount) + parseFloat($('order1_dgItems_ctl' + index + '_lblTaxAmt' + strtaxItemIDs[k]).innerText)
            //                        }
            //                    } else {
            //                        if ($('order1_dgItems_ctl' + index + '_lblTaxable' + strtaxItemIDs[k]).textContent != 'False') {
            //                            $('order1_dgItems_ctl' + index + '_lblTaxAmt' + strtaxItemIDs[k]).textContent = parseFloat(strTax[k] * parseFloat(subTotal) / 100)
            //                            TaxAmount = parseFloat(TaxAmount) + parseFloat($('order1_dgItems_ctl' + index + '_lblTaxAmt' + strtaxItemIDs[k]).textContent)
            //                        }
            //                    }

            //                }

            //            }

            jQuery('#ctl00_Content_order1_dgItems tr:last').find("[id*='lblFTotal']").text(CommaFormatted(roundNumber((Total), 2)));

            //            if ($('order1_dgItems').rows.length <= 8) {
            //                findex = '0' + ($('order1_dgItems').rows.length)
            //            }
            //            else {
            //                findex = ($('order1_dgItems').rows.length);
            //            }
            //            if (document.all) {
            //                $('order1_dgItems_ctl' + findex + '_lblFTotal').innerText = CommaFormatted(roundNumber((Total), 2))
            //            } else {
            //                $('order1_dgItems_ctl' + findex + '_lblFTotal').textContent = CommaFormatted(roundNumber((Total), 2))
            //            }

            jQuery('#lblTotal').text(CommaFormatted(roundNumber((Total) + parseFloat(TaxAmount) + parseFloat(ShipAmt), 2)));

            //            if (document.all) {
            //                $('lblTotal').innerText = CommaFormatted(roundNumber((Total) + parseFloat(TaxAmount) + parseFloat(ShipAmt), 2))
            //            } else {
            //                $('lblTotal').textContent = CommaFormatted(roundNumber((Total) + parseFloat(TaxAmount) + parseFloat(ShipAmt), 2))
            //            }


            for (k = 0; k < strtaxItemIDs.length; k++) {
                jQuery('#ctl00_Content_order1_dgItems tr').not(':first,:last').each(function () {
                    IndTaxAmt = IndTaxAmt + parseFloat(jQuery(this).find("[id*='lblTaxAmt" + strtaxItemIDs[k] + "']").text())
                });

                jQuery('#ctl00_Content_order1_dgItems tr:last').find("[id*='lblFTaxAmt" + strtaxItemIDs[k] + "']").text(CommaFormatted(roundNumber(IndTaxAmt, 2)));
                IndTaxAmt = 0;
            }

            //            for (k = 0; k < strtaxItemIDs.length; k++) {

            //                for (i = 1; i < $('order1_dgItems').rows.length - 1; i++) {
            //                    if (i <= 8) {
            //                        index = '0' + (i + 1)
            //                    }
            //                    else {
            //                        index = (i + 1);
            //                    }
            //                    if (document.all) {
            //                        IndTaxAmt = IndTaxAmt + parseFloat($('order1_dgItems_ctl' + index + '_lblTaxAmt' + strtaxItemIDs[k]).innerText)
            //                    } else {
            //                        IndTaxAmt = IndTaxAmt + parseFloat($('order1_dgItems_ctl' + index + '_lblTaxAmt' + strtaxItemIDs[k]).textContent)
            //                    }


            //                }
            //                if (document.all) {
            //                    $('order1_dgItems_ctl' + findex + '_lblFTaxAmt' + strtaxItemIDs[k]).innerText = CommaFormatted(roundNumber(IndTaxAmt, 2))
            //                } else {
            //                    $('order1_dgItems_ctl' + findex + '_lblFTaxAmt' + strtaxItemIDs[k]).textContent = CommaFormatted(roundNumber(IndTaxAmt, 2))
            //                }

            //                IndTaxAmt = 0;
            //            }
            //            CalDue()
        }


        function OpenAddressWindow(sAddressType) {
            var radValue = $find('radCmbCompany').get_value();
            window.open('frmChangeDefaultAdd.aspx?AddressType=' + sAddressType + '&CompanyId=' + radValue + "&CompanyName=" + $find('radCmbCompany').get_text(), '', 'toolbar=no,titlebar=no,left=200, top=300,width=800,height=170,scrollbars=no,resizable=no');
            return false;
        }

        function FillModifiedAddress(AddressType, AddressID, FullAddress, warehouseID, isDropshipClicked) {
            if (AddressType == "BillTo") {
                $ControlFind('hdnBillAddressID').value = AddressID;
            } else {
                $ControlFind('hdnShipAddressID').value = AddressID;
            }

            document.getElementById("btnTemp").click()
            return false;
        }

        var IsAddClicked = false;
        function Add() {

            var IsAddCalidate = Save();

            if (IsAddCalidate == false) {
                return false;
            }

            if (IsAddClicked == true) {
                return false;
            }
            if ($find('radCmbItem').get_value() == '') {
                alert("Select Item")
                return false;
            }
            if ($find('radWareHouse') != null) {
                if ($find('radWareHouse').get_value() == "") {
                    alert("Select Warehouse")
                    return false;
                }
            }
            if (document.getElementById("txtunits").value == "") {
                alert("Enter Units")
                document.getElementById("txtunits").focus();
                return false;
            }
            if (parseFloat(document.getElementById("txtunits").value) <= 0) {
                alert("Units must greater than 0.")
                document.getElementById("txtunits").focus();
                return false;
            }

            if (document.getElementById("txtprice").value == "") {
                alert("Enter Price")
                document.getElementById("txtprice").focus();
                return false;
            }


            //Check for Duplicate Item
            var table = $ControlFind('order1_dgItems');
            var WareHouesID;
            //            alert($('radWareHouse'));
            if ($('radWareHouse') == null) {
                WareHouesID = '';
            }
            else {
                WareHouesID = $find('radWareHouse').get_value();
            }

            var dropship;
            if ($ControlFind('chkDropShip').checked == true) {
                dropship = 'True';
                WareHouesID = '';
            }
            else {
                dropship = 'False';
            }

            if (document.getElementById('ddltype').value == 'P') {
                var ID = '!!' + $find('radCmbItem').get_value() + ',' + WareHouesID + ',' + dropship;
                //            alert(ID);
                for (var i = 0; i < table.rows.length - 1; i++) {
                    //                alert(trim(getTableCell('order1_dgItems', i, 11).innerHTML));
                    //                alert(ID);
                    if (document.getElementById('btnAdd').value == 'Add') {
                        if (trim(getTableCell('ctl00_Content_order1_dgItems', i, 0).innerHTML).indexOf(ID) > 0) {
                            alert("This Item is already added to opportunity.")
                            return false;
                        }
                    }
                }
            }

            var checkbox = document.getElementById("chkWorkOrder");
            if (checkbox != null) {
                if (checkbox.checked) {
                    if (document.getElementById('txtDate').value == 0) {
                        alert("Enter Expected Completion Date")
                        document.getElementById('txtDate').focus();
                        return false;
                    }
                }
            }

            IsAddClicked = true;
        }

        function SetWareHouseId(id) {
            var combo = $find('radWareHouse')
            var node = combo.findItemByValue(id);
            node.select();
        }

        BindJquery();

        function BindJquery() {

            jQuery(document).ready(function ($) {
                jQuery.easing.elasout = function (x, t, b, c, d) {
                    var s = 1.70158; var p = 0; var a = c;
                    if (t == 0) return b; if ((t /= d) == 1) return b + c; if (!p) p = d * .3;
                    if (a < Math.abs(c)) { a = c; var s = p / 4; }
                    else var s = p / (2 * Math.PI) * Math.asin(c / a);
                    return a * Math.pow(2, -10 * t) * Math.sin((t * d - s) * (2 * Math.PI) / p) + c + b;
                };

                jQuery(".Section").css("width", jQuery(window).width());
                jQuery('#section1').on("click", function (e) {
                    if (Save() == true) {
                        jQuery.scrollTo('#dvsection2', 1000, { queue: true });
                        document.getElementById('hdnCurrentSection').value = 'dvsection2';
                        jQuery('#txtUPCSKU').focus();
                        e.preventDefault();
                    }
                });
                jQuery('#section1back').on("click", function (e) {
                    jQuery.scrollTo('#dvsection1', 1000, { queue: true });
                    document.getElementById('hdnCurrentSection').value = 'dvsection1';
                    e.preventDefault();
                });
                jQuery('#section2back').on("click", function (e) {
                    jQuery.scrollTo('#dvsection2', 1000, { queue: true });
                    document.getElementById('hdnCurrentSection').value = 'dvsection2';
                    jQuery('#txtUPCSKU').focus();
                    e.preventDefault();
                });
                jQuery('#section2Next').on("click", function (e) {
                    jQuery.scrollTo('#dvsection3', 1000, { queue: true });
                    document.getElementById('hdnCurrentSection').value = 'dvsection3';
                    e.preventDefault();
                });

                var tr = jQuery('#ctl00_Content_order1_dgItems tr').not('.hs').not('.footer');
                tr.on("click", function (event) {
                    tr.removeClass("selected");
                    jQuery(this).addClass("selected");
                });

                var keyPadMode = 'Units';

                jQuery('.keypad_digit').on("click", function () {

                    if (jQuery("#ctl00_Content_order1_dgItems tr").hasClass("selected")) {
                        var Rows = jQuery("#ctl00_Content_order1_dgItems tr.selected");

                        switch (keyPadMode.trim()) {
                            case 'Units':
                                var txtUnits = Rows.find("input[id*='txtUnits']");
                                txtUnits.val((txtUnits.val() == 0 ? '' : txtUnits.val()) + jQuery(this).val());
                                break;
                            case 'Disc':
                                var txtTotalDiscount = Rows.find("input[id*='txtTotalDiscount']");
                                txtTotalDiscount.val((txtTotalDiscount.val() == 0 ? '' : txtTotalDiscount.val()) + jQuery(this).val());
                                break;
                            case 'Price':
                                var txtUnitPrice = Rows.find("input[id*='txtUnitPrice']");
                                txtUnitPrice.val((txtUnitPrice.val() == 0 ? '' : txtUnitPrice.val()) + jQuery(this).val());
                                break;
                        }

                        return CalculateTotal();
                    }
                    else {
                        alert('Please select item.');
                    }


                });

                jQuery('.keypad_clear').on("click", function () {
                    if (jQuery("#ctl00_Content_order1_dgItems tr").hasClass("selected")) {
                        var Rows = jQuery("#ctl00_Content_order1_dgItems tr.selected");

                        switch (keyPadMode.trim()) {
                            case 'Units':
                                var txtUnits = Rows.find("input[id*='txtUnits']");
                                txtUnits.val('0');
                                break;
                            case 'Disc':
                                var txtTotalDiscount = Rows.find("input[id*='txtTotalDiscount']");
                                txtTotalDiscount.val('0');
                                break;
                            case 'Price':
                                var txtUnitPrice = Rows.find("input[id*='txtUnitPrice']");
                                txtUnitPrice.val('0');
                                break;
                        }

                        return CalculateTotal();
                    }
                    else {
                        alert('Please select item.');
                    }
                });

                jQuery('.keypad_Mode,.keypad_ModeSelect').on("click", function () {
                    jQuery('#tblkeypad input').removeClass("keypad_ModeSelect").addClass("keypad_Mode");
                    jQuery(this).addClass("keypad_ModeSelect");
                    keyPadMode = jQuery(this).val();

                });

                window.onload = function () {
                    if (document.getElementById('hdnCurrentSection').value.length > 0) {
                        if (document.getElementById('hdnCurrentSection').value == 'dvsection3') {
                            jQuery.scrollTo('#' + document.getElementById('hdnCurrentSection').value, 1000, { queue: true });
                        }
                    }
                    setInterval('blinkIt()', 500);
                };
            });
        }


        function OpenAmtPaid(a, b) {
            window.open('../opportunity/frmAmtPaid.aspx?swipe=1&a=' + a + '&b=' + b, '', 'toolbar=no,titlebar=no,top=300,width=700,height=400,scrollbars=no,resizable=no');
            return false;
        }

        function OpenBizInvoice(a, b, c) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b + '&Print=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenOrderDetails(a, b) {
            str = "../opportunity/frmOpportunities.aspx?frm=OpenBizDoc&OpID=" + a + "&OppType=" + b;
            document.location.href = str;
            return false;
        }

        function ClearRadComboBox() {
            $find('radCmbItem').clearSelection();
            return false;
        }

        function DeleteRecord() {
            if (confirm('Do you want to cancel this order ?')) {
                return true;
            }
            else {
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    POS : Sales Order
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
        <Scripts>
            <asp:ScriptReference Path="~/JavaScript/prototype-1.6.0.2.compressed.js" />
            <asp:ScriptReference Path="~/JavaScript/comboClientSide.js" />
        </Scripts>
    </asp:ScriptManager>
    <table cellpadding="0" cellspacing="0" width="100%" border="0" class="fixedTable">
        <tr>
            <td align="right">
                <table cellpadding="0" cellspacing="0" border="0" align="right">
                    <tr>
                        <td align="left">
                            <asp:Button ID="btnEmailInvoice" runat="server" CssClass="Button_POS" Text="Email Invoice to Customer"
                                Visible="false" />&nbsp;
                        </td>
                        <td align="left">
                            <asp:Button ID="btnPrintInvoice" runat="server" CssClass="Button_POS" Text="Print Invoice"
                                Visible="false" />&nbsp;
                        </td>
                        <td align="left">
                            <asp:Button ID="btnOpenInvoice" runat="server" CssClass="Button_POS" Text="Open Invoice"
                                Visible="false" />&nbsp;
                        </td>
                        <td align="left">
                            <asp:Button ID="btnOpenOrderDetails" runat="server" CssClass="Button_POS" Text="Open Order Details"
                                Visible="false" />&nbsp;
                        </td>
                        <td align="left">
                            <asp:Button ID="btnCancelOrder" runat="server" CssClass="Button_POS" Text="Cancel Order" />&nbsp;
                        </td>
                        <td align="left">
                            <asp:Button ID="btnNewOrder" runat="server" CssClass="Button_POS" Text="New Order" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" class="normal4" align="center">
                            <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                        </td>
                    </tr>
                </table>
            </td>
            <td align="right">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <a href="frmNewSalesOrderPOS.aspx" style="text-decoration: none">
                                <img src="../images/AddRecord.png" alt="New Order" title="New Order" border="0" />
                            </a>&nbsp;
                        </td>
                        <td>
                            <span id="spnMD" onclick="parent.document.getElementById('frmTickler').rows = '110px,*'; document.getElementById('spnMU').style.display='block';document.getElementById('spnMD').style.display='none'; ">
                                <img src="../images/Down-arrow-inv1.png" alt="Move Down" title="Move Down" />
                            </span>
                        </td>
                        <td>
                            <span id="spnMU" onclick="parent.document.getElementById('frmTickler').rows = '0px,*';document.getElementById('spnMD').style.display='block';document.getElementById('spnMU').style.display='none';">
                                <img src="../images/Up-arrow-inv.png" alt="Move Up" title="Move Up" />
                            </span>
                        </td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
                <script type="text/javascript">
                    document.getElementById('spnMD').style.display = 'none';
                </script>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="0" width="100%" border="0" style='padding-top: 38px;'>
        <tr>
            <td colspan="2">
                <asp:Table ID="tblOppr" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
                    CssClass="aspTable" Width="100%" BorderColor="black" GridLines="None">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top" Width="100%">
                            <div class="Section" id="dvsection1">
                                <table width="100%" border="0" align="center" cellspacing="10px">
                                    <tr>
                                        <td class="normal1" align="right" width="350px">
                                            <asp:RadioButton AutoPostBack="true" Checked="true" ID="radExistingCustomer" runat="server"
                                                Text="Existing Customer" GroupName="rad" />
                                        </td>
                                        <td align="left" class="normal1">
                                            <asp:RadioButton AutoPostBack="true" ID="radNewCustomer" TabIndex="0" runat="server"
                                                Text="New Customer" GroupName="rad" />
                                        </td>
                                    </tr>
                                    <tr id="trCurrency" runat="server" visible="false">
                                        <td class="normal1" align="right">Currency
                                        </td>
                                        <td>
                                            <asp:DropDownList Width="200px" ID="ddlCurrency" runat="server" CssClass="signup">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top" colspan="2">
                                            <asp:Panel ID="trNewCust" runat="server">
                                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td align="right" valign="top">
                                                            <table border="0" cellpadding="0" cellspacing="10px" width="100%">
                                                                <tr>
                                                                    <td width="350px" class="normal1" align="right">First Name<font color="red">*</font>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtFirstName" runat="server" Width="200" MaxLength="50" CssClass="signup">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="normal1" align="right">Last Name<font color="red">*</font>
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtLastName" runat="server" Width="200" MaxLength="50" CssClass="signup">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="normal1" align="right">Phone
                                                                    </td>
                                                                    <td class="normal1">
                                                                        <asp:TextBox ID="txtPhone" runat="server" Width="150" MaxLength="50" CssClass="signup">
                                                                        </asp:TextBox>
                                                                        &nbsp;Ext
                                                                        <asp:TextBox ID="txtExt" runat="server" Width="30" MaxLength="10" CssClass="signup">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="normal1" align="right">Email
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtEmail" runat="server" Width="200" MaxLength="50" CssClass="signup">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="normal1" align="right">Organization
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtCompany" runat="server" Width="200" MaxLength="50" CssClass="signup">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="normal1" align="right">Assign To
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlNewAssignTo" Width="200" runat="server" CssClass="signup">
                                                                            <asp:ListItem Text="--Select One--" Value="0">
                                                                            </asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="normal1" align="right">Relationship<font color="red">*</font>
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlRelationship" Width="200" runat="server" AutoPostBack="true"
                                                                            CssClass="signup">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="normal1" align="right">Profile
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlProfile" Width="200" runat="server" CssClass="signup">
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="normal1" align="right"></td>
                                                                    <td>
                                                                        <asp:CheckBoxList ID="chkTaxItems" CssClass="normal1" runat="server" RepeatDirection="Horizontal"
                                                                            RepeatColumns="3">
                                                                        </asp:CheckBoxList>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td align="left" valign="top">
                                                            <table border="0" cellpadding="0" cellspacing="10px" width="100%">
                                                                <tr>
                                                                    <td class="text_bold" colspan="2">Billing Address
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="normal1" align="right">Street
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtStreet" TabIndex="10" runat="server" CssClass="signup" TextMode="MultiLine"
                                                                            Width="200">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="normal1" align="right">City
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtCity" TabIndex="11" runat="server" CssClass="signup" Width="200">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="normal1" align="right">Postal
                                                                    </td>
                                                                    <td>
                                                                        <asp:TextBox ID="txtPostal" runat="server">
                                                                        </asp:TextBox>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="normal1" align="right">State
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlBillState" runat="server" TabIndex="12" Width="200" CssClass="signup">
                                                                            <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="normal1" align="right">Country
                                                                    </td>
                                                                    <td>
                                                                        <asp:DropDownList ID="ddlBillCountry" TabIndex="14" AutoPostBack="True" runat="server"
                                                                            Width="200" CssClass="signup">
                                                                            <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                                        </asp:DropDownList>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="right">
                                                                        <asp:CheckBox AutoPostBack="true" ID="chkShipDifferent" runat="server" />
                                                                    </td>
                                                                    <td class="normal1" colspan="3">
                                                                        <label for="chkShipDifferent">
                                                                            Click here if Shipping Address is different than Billing Address</label>
                                                                    </td>
                                                                </tr>
                                                                <asp:Panel ID="pnlShipping" runat="server" Visible="false">
                                                                    <tr>
                                                                        <td class="text_bold" colspan="2">Shipping Address
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="normal1" align="right">Street
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtShipStreet" TabIndex="10" runat="server" CssClass="signup" TextMode="MultiLine"
                                                                                Width="200">
                                                                            </asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="normal1" align="right">City
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtShipCity" TabIndex="11" runat="server" CssClass="signup" Width="200">
                                                                            </asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="normal1" align="right">Postal
                                                                        </td>
                                                                        <td>
                                                                            <asp:TextBox ID="txtShipPostal" runat="server">
                                                                            </asp:TextBox>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="normal1" align="right">State
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlShipState" runat="server" TabIndex="12" Width="200" CssClass="signup">
                                                                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="normal1" align="right">Country
                                                                        </td>
                                                                        <td>
                                                                            <asp:DropDownList ID="ddlShipCountry" TabIndex="14" AutoPostBack="True" runat="server"
                                                                                Width="200" CssClass="signup">
                                                                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </asp:Panel>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                            <asp:Panel ID="trExistCust" runat="server">
                                                <table border="0" cellpadding="0" cellspacing="10px" width="100%">
                                                    <tr>
                                                        <td width="350px" class="normal1" align="right">
                                                            <u>C</u>ustomer<font color="#ff3333"> *</font>
                                                        </td>
                                                        <td align="left">
                                                            <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                                                                OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                                                ClientIDMode="Static"
                                                                ShowMoreResultsBox="true"
                                                                Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True">
                                                                <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                                            </telerik:RadComboBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal1" align="right">Contact
                                                        </td>
                                                        <td align="left">
                                                            <asp:DropDownList ID="ddlContact" Width="200" runat="server" CssClass="signup">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal1" align="right">Assign To
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlAssignTo" Width="200" runat="server" CssClass="signup">
                                                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal1" align="right">Sales Template
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlSalesTemplate" runat="server" CssClass="signup" Width="200"
                                                                AutoPostBack="true">
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <asp:Panel ID="pnlPurchase" runat="server" Visible="false">
                                                            <td class="normal1" align="right"></td>
                                                            <td>
                                                                <asp:Button ID="btnSelectVendor" CssClass="button" runat="server" Text="Select Vendor" />
                                                            </td>
                                                        </asp:Panel>
                                                    </tr>
                                                    <asp:Panel ID="pnlBillTo" runat="server" Visible="true">
                                                        <tr>
                                                            <td class="normal1" valign="top" align="right">
                                                                <asp:LinkButton ID="lnkBillTo" runat="server" Text="Bill to:" ForeColor="Blue" Font-Underline="true">
                                                                </asp:LinkButton>
                                                            </td>
                                                            <td class="normal1">
                                                                <asp:Label ID="lblBillTo1" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td class="normal1" valign="top">
                                                                <asp:Label ID="lblBillTo2" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </asp:Panel>
                                                    <asp:Panel ID="pnlShipTo" runat="server" Visible="true">
                                                        <tr>
                                                            <td class="normal1" valign="top" align="right">
                                                                <asp:LinkButton ID="lnkShipTo" runat="server" Text="Ship to:" ForeColor="Blue" Font-Underline="true">
                                                                </asp:LinkButton>
                                                            </td>
                                                            <td class="normal1">
                                                                <asp:Label ID="lblShipTo1" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td class="normal1" valign="top">
                                                                <asp:Label ID="lblShipTo2" runat="server" Text=""></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </asp:Panel>
                                                    <tr>
                                                        <td class="normal1" align="right">Sales Credit :
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblCreditBalance" runat="server" CssClass="text"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal1" align="right">Credit Limit (A) :
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblCreditLimit" runat="server" CssClass="text"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal1" align="right">Balance Due (B) :
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblBalanceDue" runat="server" CssClass="text"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal1" align="right">Remaining Credit (A - B) :
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblRemaningCredit" runat="server" CssClass="text"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="normal1" align="right">
                                                            <asp:Label ID="lblTotalAmtPastDue1" runat="server">Amount Past Due : 	</asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label ID="lblTotalAmtPastDue" runat="server" CssClass="text"></asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                                <div style="position: absolute; z-index: 9999; cursor: pointer; float: right; top: 60px; right: 5px">
                                    <span style="float: right;"><span id="section1" class="Button_POS">Next</span>&nbsp;&nbsp;&nbsp;&nbsp;
                                    </span>
                                </div>
                            </div>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top" HorizontalAlign="Center">
                            <div class="Section" id="dvsection2" style="position: relative">
                                <uc1:order ID="order1" runat="server" />
                                <asp:Button ID="btnTemp" runat="server" Style="display: none"></asp:Button>
                                <asp:HiddenField ID="hdnCmbDivisionID" runat="server" />
                                <asp:HiddenField ID="hdnCmbOppType" runat="server" />
                                <div style="position: absolute; z-index: 9999; cursor: pointer; float: right; top: 4px; right: 5px">
                                    <span style="float: right;" id="btnSection2Next" runat="server" visible="false"><span
                                        id="section2Next" class="Button_POS">Next</span>&nbsp;&nbsp;&nbsp;&nbsp; </span>
                                </div>
                                <div style="position: absolute; z-index: 9999; cursor: pointer; float: right; top: 4px; right: 80px">
                                    <span style="float: right;"><span id="section1back" class="Button_POS">Back</span>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                </div>
                            </div>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top" HorizontalAlign="Center" ID="tdSection3" Visible="false">
                            <div class="Section" id="dvsection3" style="position: relative">
                                <table width="100%" border="0" align="center" cellspacing="10px">
                                    <tr>
                                        <td align="center">
                                            <iframe width="750" height="450" id="ifrmPayAmount" runat="server"></iframe>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <div style="position: absolute; z-index: 9999; cursor: pointer; float: right; top: 4px; right: 5px">
                                                            <span style="float: right; vertical-align: bottom"><span id="section2back" class="Button_POS">Back</span>&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnmonVal1" runat="server" />
    <asp:HiddenField ID="hdnmonVal2" runat="server" />
    <asp:HiddenField ID="hdnCurrentSection" runat="server" />
    <asp:HiddenField ID="hdnOppID" runat="server" Value="0" />
    <asp:HiddenField ID="hdnOppBizDocID" runat="server" Value="0" />
    <script type="text/javascript">
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(EndRequestHandler);
        function EndRequestHandler(sender, args) {
            BindJquery();
        }
    </script>
</asp:Content>
