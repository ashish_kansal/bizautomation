﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master"
    CodeBehind="frmOppKitItems.aspx.vb" Inherits=".frmOppKitItems" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Oppertunity Kit Items</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Oppertunity Kit Items
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:GridView ID="gvOppKitItems" AutoGenerateColumns="False" runat="server" Width="900px"
        CssClass="tbl">
        <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is" />
        <Columns>
            <asp:BoundField DataField="vcItemName" HeaderText="Item"></asp:BoundField>
            <asp:BoundField DataField="vcItemType" HeaderText="Type"></asp:BoundField>
            <asp:BoundField DataField="vcWareHouse" HeaderText="WareHouse"></asp:BoundField>
            <asp:BoundField DataField="numQtyItemsReq" HeaderText="Units"></asp:BoundField>
            <asp:BoundField DataField="numQtyShipped" HeaderText="Shipped Units"></asp:BoundField>
            <asp:BoundField DataField="numOnHand" HeaderText="OnHand"></asp:BoundField>
            <asp:BoundField DataField="numOnOrder" HeaderText="OnOrder"></asp:BoundField>
            <asp:BoundField DataField="numReorder" HeaderText="Reorder"></asp:BoundField>
            <asp:BoundField DataField="numAllocation" HeaderText="Allocation"></asp:BoundField>
            <asp:BoundField DataField="numBackOrder" HeaderText="BackOrder"></asp:BoundField>
        </Columns>
        <HeaderStyle CssClass="hs"></HeaderStyle>
    </asp:GridView>
</asp:Content>
