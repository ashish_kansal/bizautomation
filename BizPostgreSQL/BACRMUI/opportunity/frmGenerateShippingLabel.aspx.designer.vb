﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmGenerateShippingLabel

    '''<summary>
    '''litMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents litMessage As Global.System.Web.UI.WebControls.Literal

    '''<summary>
    '''chkUseDimensions control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkUseDimensions As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''btnUpdateRates control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnUpdateRates As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnGetRates control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnGetRates As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnGenShippingLabel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnGenShippingLabel As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''sc1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents sc1 As Global.System.Web.UI.ScriptManager

    '''<summary>
    '''tblItems control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblItems As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''radBoxes control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents radBoxes As Global.Telerik.Web.UI.RadGrid

    '''<summary>
    '''tblConfig control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblConfig As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''txtFromName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFromName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtFromPhone control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFromPhone As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtFromCompany control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFromCompany As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtToName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtToName As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtToPhone control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtToPhone As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtToCompany control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtToCompany As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtFromAddressLine1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFromAddressLine1 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtFromAddressLine2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFromAddressLine2 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtFromCity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFromCity As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlFromState control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlFromState As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtFromZip control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtFromZip As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlFromCountry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlFromCountry As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''chkFromResidential control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkFromResidential As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''txtToAddressLine1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtToAddressLine1 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtToAddressLine2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtToAddressLine2 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtToCity control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtToCity As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlToState control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlToState As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtToZip control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtToZip As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlToCountry control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlToCountry As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''chkToResidential control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkToResidential As Global.System.Web.UI.WebControls.CheckBox

    '''<summary>
    '''ddlPayorType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlPayorType As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''tdPaymentOption1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdPaymentOption1 As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''lblPayorlabel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPayorlabel As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''tdPaymentOption2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdPaymentOption2 As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''txtThirdPartyAccountNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtThirdPartyAccountNo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtThirdPartyZipCode control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtThirdPartyZipCode As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''ddlThirdPartyCounty control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlThirdPartyCounty As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''txtReferenceNo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtReferenceNo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''chkOther control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents chkOther As Global.System.Web.UI.WebControls.CheckBoxList

    '''<summary>
    '''trSignType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trSignType As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''ddlSignatureType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlSignatureType As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''trDesc control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trDesc As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''txtDescription control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtDescription As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtTotalInsuredValue control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTotalInsuredValue As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtTotalCustomsValue control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtTotalCustomsValue As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''trCOD1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trCOD1 As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''ddlCODType control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlCODType As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''trCOD2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents trCOD2 As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''txtCODTotalAmount control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtCODTotalAmount As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''hdnSelectedIDs control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnSelectedIDs As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''tblShipping control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblShipping As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''radRateSelection control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents radRateSelection As Global.Telerik.Web.UI.RadGrid

    '''<summary>
    '''btnAddToShippingReport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnAddToShippingReport As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''hdnShippingCompany control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnShippingCompany As Global.System.Web.UI.WebControls.HiddenField
End Class
