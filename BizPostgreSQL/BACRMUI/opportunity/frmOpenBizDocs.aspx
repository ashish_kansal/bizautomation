﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmOpenBizDocs.aspx.vb"
    Inherits="BACRM.UserInterface.Opportunities.frmOpenBizDocs" MasterPageFile="~/common/GridMasterRegular.Master"
    ClientIDMode="Static" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <%--<link rel="stylesheet" href="~/CSS/master.css" type="text/css" />--%>
    <%--<script src="../JavaScript/jquery.min.js" type="text/javascript"></script>--%>
    <script src="../JavaScript/SalesFulfillment.js"></script>
    <title></title>
    <script type="text/javascript" language="javascript">

        $(document).ready(function () {
            //ExpandCollapseGrid();

            var chkBox1 = $("#ctl00_ctl00_MainContent_GridPlaceHolder_gvBizDocs_ctl01_chkSelectAll");
            $(chkBox1).click(function () {
                $("#ctl00_ctl00_MainContent_GridPlaceHolder_gvBizDocs INPUT[type='checkbox']").each(function () {
                    if (this.id.indexOf("chkSelect") >= 0) {
                        $(this).prop('checked', chkBox1.is(':checked'))
                    }
                })
            });

            $("#ctl00_ctl00_MainContent_FiltersAndViews_radBizDocStatus_Footer_chkBizDocsStatusAll").click(function () {
                var Combo = $find("<%= radBizDocStatus.ClientID%>");
                var items = Combo.get_items();

                for (var x = 0; x < Combo.get_items().get_count() ; x++) {
                    if ($(this).is(':checked'))
                        items._array[x].check();
                    else
                        items._array[x].uncheck();
                }
            });

            //$("#ddlAction").select(function () {
            //    return GetRecords();
            //});
        });

        function GetRecords() {
            var OppId = '';
            var BizDocsId = '';
            var finalValue = '';
            var intCnt;
            intCnt = 0;
            $("#ctl00_ctl00_MainContent_GridPlaceHolder_gvBizDocs tr").not(':first').each(function () {
                chk = $(this).find("[id*='chkSelect']");
                if ($(chk).is(':checked')) {
                    intCnt = intCnt + 1
                    //OppId = OppId + ',' + $(this).find("[id*='txtOppID']").val();
                    //BizDocsId = BizDocsId + ',' + $(this).find("[id*='txtBizDocID']").val();
                    OppId = $(this).find("[id*='txtOppID']").val()
                    BizDocsId = $(this).find("[id*='txtBizDocID']").val()
                    //alert(OppId);
                    //alert(BizDocsId);
                    finalValue = finalValue + ',' + OppId + ':' + BizDocsId;
                }
            });

            if (intCnt > 0) {
                document.getElementById('txtRecordIds').value = finalValue;
                if ($('#ddlAction').val() == 2) {
                    if (confirm("You're about to broadcast an indivudual Bizdoc to within the following email template to the customers selected. Are you sure you want to proceed ?") == true) {
                        return true;
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return true;
                }
                
            }
            else {
                alert("Please select atleast one Bizdoc.");
                $('#ddlAction').val(0);
                return false;
            }
            //document.getElementById('txtSelOppId').value = OppId;
            //return true;
        }

        function OpenOpp(a, b) {

            var str;
            str = "../opportunity/frmOpportunities.aspx?frm=OpenBizDoc&OpID=" + a + "&OppType=" + b;
            document.location.href = str;
        }
        function OpenBizInvoice(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenAmountPaid(Opptype) {
            var elems = document.getElementsByTagName('input');
            var BizDocID = '';
            for (var i = 0; i < elems.length; i++) {
                if (elems[i].type == 'checkbox') {
                    if (elems[i].checked) {
                        BizDocID = BizDocID + ',' + elems[i].parentNode.title;
                    }
                }
            }
            //alert(DivisionIDs);
            RPPopup = window.open('../opportunity/frmAmtPaid.aspx?Page=' + document.getElementById('txtCurrrentPage').value + "&OppType=" + Opptype + "&BizDocs=" + BizDocID, 'ReceivePayment', 'toolbar=0,titlebar=0,menubar=0,location=1,left=100,top=50,width=800,height=500,scrollbars=yes,resizable=yes');
            console.log(RPPopup.name);
            return false;
        }
        function OpenAmtPaid(a, b, c) {
            RPPopup = window.open('../opportunity/frmAmtPaid.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=' + a + '&OppBizId=' + b + '&DivId=' + c, 'ReceivePayment', 'toolbar=no,titlebar=no,top=300,width=700,height=400,scrollbars=no,resizable=no');
            console.log(RPPopup.name);
            return false;
        }
        function reDirect(a) {
            document.location.href = a;
        }
        function OpenWindow(a, b) {
            var str;
            if (b == 0) {

                str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&DivID=" + a;

            }
            else if (b == 1) {

                str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&DivID=" + a;

            }
            else if (b == 2) {

                str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&klds+7kldf=fjk-las&DivId=" + a;

            }

            document.location.href = str;

        }
        function OpenEdit(a, b, c) {
            window.location.href = '../opportunity/frmAddBizDocs.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=BizDocs&OpID=' + a + '&OppBizId=' + b + '&OppType=' + c;
            return false;
        }
        function openReportList(a, b) {
            window.open('../opportunity/frmShippingReportList.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=' + a + '&OppBizDocId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=680,height=250,scrollbars=yes,resizable=yes');
        }
        function openShippingLabel(a, b) {
            //var index = document.getElementById('ddlShipCompany').selectedIndex;
            window.open('../opportunity/frmShippingBox.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=' + a + '&OppBizDocId=' + b + '&ShipCompID=0', '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=1000,height=350,scrollbars=yes,resizable=yes');
            return false;
        }

        function ConfirmMail() {
            if (confirm("You're about to broadcast an indivudual PDF Bizdoc to within the following email template to the customers selected. Are you sure you want to proceed ?") == true) {
                return true;
            }
            else {
                return false;
            }
        }

    </script>
    <style type="text/css">
        .table {
        border:1px solid #cacaca;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
      <div class="col-md-8">
        <div class="pull-left">
     
            <div class="form-inline">
                <div class="form-group">
            <label class="col-md-3">Status</label>
            <div  class="col-md-9">
             <telerik:RadComboBox ID="radBizDocStatus"
                    Skin="Default" runat="server" CheckBoxes="true" DropDownWidth="250px">
                    <FooterTemplate>
                        <asp:CheckBox ID="chkBizDocsStatusAll" runat="server" Text="Select All" />&nbsp;
                        <asp:CheckBox ID="chkIncludeHistory" runat="server" Text="Completed" /><br />
                        <asp:Button ID="btnBizDocStatus" CssClass="button" runat="server" Text="Search" OnClick="btnBizDocStatus_Click"></asp:Button>
                    </FooterTemplate>
                </telerik:RadComboBox>
                </div>
            </div>
            <div class="form-group">
            <label  class="col-md-6">Created Date</label>
                <div  class="col-md-6">
               <asp:UpdatePanel ID="updatepanel1" runat="server">
                        <ContentTemplate>
                             <asp:DropDownList runat="server" ID="ddlDateFilter" CssClass="form-control" AutoPostBack="true">
                        <asp:ListItem Value="1">All Dates</asp:ListItem>
                        <asp:ListItem Value="2" Selected="True">Today</asp:ListItem>
                        <%--<asp:ListItem Value="3">Yesterday</asp:ListItem>--%>
                        <asp:ListItem Value="3">This Week</asp:ListItem>
                        <asp:ListItem Value="4">Last Week</asp:ListItem>
                        <asp:ListItem Value="5">This Month</asp:ListItem>
                        <asp:ListItem Value="6">Last Month</asp:ListItem>
                        <%--<asp:ListItem Value="6">This Quarter</asp:ListItem>
                        <asp:ListItem Value="7">This Year</asp:ListItem>                       
                        <asp:ListItem Value="10">Last Quarter</asp:ListItem>
                        <asp:ListItem Value="11">Last Year</asp:ListItem>--%>
                    </asp:DropDownList>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="ddlDateFilter" />
                        </Triggers>
                    </asp:UpdatePanel>
                    </div>
                </div>
                </div>
             </div>
        </div>
    <div class="pull-right">
            <div>
                <div class="form-inline">
                 <div class="form-group">
                <label>Action</label>
               
                 <asp:DropDownList runat="server" ID="ddlAction" CssClass="form-control">
                                <asp:ListItem Text="-- Select One --" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Print To Pdf" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Mass Email" Value="2"></asp:ListItem>
                            </asp:DropDownList> 
                     &nbsp;
                        <asp:Button CssClass="btn btn-primary" Style="text-align: center" ID="btnGo" runat="server" Text="Go" OnClientClick="return GetRecords();" />
                    </div>
                    </div>
            </div>
            <div class="form-group  col-md-4"><label>&nbsp;</label>
                
            </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center" colspan="3">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                <asp:Label ID="lblmsg" runat="server" style="display:none" CssClass="btn btn-success" Width="750px"></asp:Label>
            </td>
        </tr>
    </table>
    <%-- Organization&nbsp;
    <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="200px"
        Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True">
        <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
    </telerik:RadComboBox>
    <br />--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblOpenBizDocs" runat="server" Text="Label"></asp:Label>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <br />
    <table cellpadding="0" cellspacing="0" border="0" width="100%">

        <tr>
            <td>
                <asp:GridView ID="gvBizDocs" runat="server" EnableViewState="true" AutoGenerateColumns="false"
                    CssClass="table table-responsive" Width="100%" AllowSorting="true" ClientIDMode="AutoID">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" HorizontalAlign="Center"></HeaderStyle>
                    <Columns>
                        <asp:TemplateField HeaderStyle-Width="10">
                            <ItemTemplate>
                                <asp:Image ID="editImage" runat="server" ImageUrl="~/images/edit.png" ImageAlign="Middle"
                                    onclick='' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="BizDoc ID" SortExpression="vcBizDocID">
                            <ItemTemplate>
                                <%--<asp:HyperLink ID="hplBizdcoc" runat="server" NavigateUrl="#" Text='<%#BACRM.BusinessLogic.Common.CCommon.ToString(GetQueryStringVal("BizDocName")) %>'>
                    </asp:HyperLink>--%>
                                <asp:Label ID="lblBizDocID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numBizDocId") %>'>
                                </asp:Label>
                                <%--/--%>
                                <%--<asp:HyperLink ID="hplEditBizdcoc" runat="server" NavigateUrl="#" Text='<%# Eval("vcBizDocID") %>'>
                    </asp:HyperLink>--%>
                                <a href="#" onclick="OpenBizInvoice('<%# Eval("numOppID")%>','<%# Eval("numOppBizDocsID") %>');">
                                    <%--<asp:Label ID="lblEditBizdcoc" runat="server" Text='<%# Eval("vcBizDocID") %>'>
                        </asp:Label>--%>
                                    <asp:Label ID="lblEditBizdcoc" runat="server" Text='<%# Eval("vcBizDocID") %>'>
                                    </asp:Label>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Billing Date / Due Date">
                           <%-- <ItemTemplate>
                                <%# ReturnName(DataBinder.Eval(Container.DataItem, "dtFromDate")) %>
                    /
                    <%# ReturnName(DataBinder.Eval(Container.DataItem, "dtDueDate")) %>
                            </ItemTemplate>--%>
                             <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "dtFromDate") %> / <%# DataBinder.Eval(Container.DataItem, "dtDueDate")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total Amt / Amt Paid / Amt Due">
                            <ItemTemplate>
                                <%-- <%#Eval("varCurrSymbol") + " " + String.Format("{0:###00.00}", Eval("monDealAmount"))%>
                            <%# IIf(Eval("BaseCurrencySymbol") <> Eval("varCurrSymbol"),
                                                             "<span class='gray'>(" + Eval("BaseCurrencySymbol") + " " + String.Format("{0:###00.00}",
                                                              Eval("monDealAmount") * Eval("fltExchangeRateOfOrder")) + ")</span>", "")%>	--%>
                                <%--<%#Eval("BaseCurrencySymbol")%>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <%#Eval("varCurrSymbol")%>
                    &nbsp;&nbsp;&nbsp;--%>
                                <%#Eval("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", Eval("monDealAmount"))%>
                                <%# IIf(Eval("BaseCurrencySymbol") <> Eval("varCurrSymbol"),
                                                             "<span class='gray'>(" + Eval("BaseCurrencySymbol") + " " + String.Format("{0:###00.00}",
                                                              Eval("monDealAmount") * Eval("fltExchangeRateOfOrder")) + ")</span>", "")%>
                    &nbsp;/&nbsp;
                    <%#Eval("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", Eval("monAmountPaid"))%>
                                <%# IIf(Eval("BaseCurrencySymbol") <> Eval("varCurrSymbol"),
                                                             "<span class='gray'>(" + Eval("BaseCurrencySymbol") + " " + String.Format("{0:###00.00}",
                                                              Eval("monAmountPaid") * Eval("fltExchangeRateOfOrder")) + ")</span>", "")%>
                    &nbsp;/&nbsp;
                    <%#Eval("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", Eval("BalDue"))%>
                                <%# IIf(Eval("BaseCurrencySymbol") <> Eval("varCurrSymbol"),
                                                             "<span class='gray'>(" + Eval("BaseCurrencySymbol") + " " + String.Format("{0:###00.00}",
                                                              Eval("BalDue") * Eval("fltExchangeRateOfOrder")) + ")</span>", "")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Fulfillment / Comments">
                            <ItemTemplate>
                                <%# Eval("vcComments")%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label Text="P.O #" runat="server" ID="lblRefOrderNo" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("vcRefOrderNo").ToString()%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="BizDoc Status" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="210">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlBizDocStatus" runat="server" CssClass="BizDocStatus form-control">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Actions" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle"
                            HeaderStyle-Width="120">
                            <ItemTemplate>
                                <asp:TextBox ID="txtConEmail" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "vcEmail") %>' />
                                <asp:TextBox ID="txtConID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numContactid") %>' />
                                <asp:TextBox ID="txtBizDocTemplate" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numBizDocTempID") %>' />
                                <%-- <a href="javascript:void(0);" onclick="openShippingLabel('<%# Eval("numOppId") %>','<%# Eval("numoppbizdocsid")%>');">
                        <img src="../images/box.png" border="0" alt="" />
                    </a>&nbsp;
                    <a href="javascript:void(0);" onclick="openReportList('<%# Eval("numOppId") %>','<%# Eval("numoppbizdocsid")%>');">
                        <img src="../images/boxlabel.png" border="0" alt="" />
                    </a>--%>
                                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                    <ContentTemplate>

                                <asp:ImageButton runat="server" ID="ibtnPayments" AlternateText="Receive payment"
                                    ToolTip="Receive payment" ImageUrl="~/images/moneyup-24.gif" Width="16" Height="16" />&nbsp;
                    <asp:ImageButton runat="server" ID="ibtnSendEmail" AlternateText="Email as PDF" ToolTip="Email as PDF"
                        ImageUrl="~/images/Email.png" CommandName="Email" />&nbsp;
                    <asp:ImageButton runat="server" ID="ibtnExportPDF" AlternateText="Export to PDF"
                        ToolTip="Export to PDF" ImageUrl="~/images/pdf.png" CommandName="PDF" />&nbsp;
                    <asp:ImageButton runat="server" ID="ibtnPrint" AlternateText="Print" ToolTip="Print"
                        ImageUrl="~/images/Print.png" />
                                        
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:PostBackTrigger ControlID="ibtnPayments" />
                                        <asp:PostBackTrigger ControlID="ibtnSendEmail" />
                                        <asp:PostBackTrigger ControlID="ibtnExportPDF" />
                                        <asp:PostBackTrigger ControlID="ibtnPrint" />
                                    </Triggers>
                                </asp:UpdatePanel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:TextBox ID="txtOppID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numoppid")%>'>
                                </asp:TextBox>
                                <asp:TextBox ID="txtBizDocStatus" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numBizDocStatus")%>'>
                                </asp:TextBox>
                                <asp:TextBox ID="txtBizDocID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numoppbizdocsid")%>'>
                                </asp:TextBox>
                                <asp:TextBox ID="txtIsAuthBizDoc" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "bitAuthoritativeBizDocs") %>'>
                                </asp:TextBox>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
                                <asp:LinkButton ID="lnkDelete" runat="server" Visible="false">
														<font color="#730000">*</font></asp:LinkButton>
                                <asp:HiddenField ID="hdnOppType" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "tintOppType") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkSelectAll" runat="server" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkSelect" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <Columns>
                    </Columns>
                </asp:GridView>
                <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
                <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
                <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
                <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
                <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
                <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
                <asp:Button ID="btnGo1" runat="server" Style="display: none" />
                <asp:TextBox ID="txtRecordIds" runat="server" Style="display: none"></asp:TextBox>
            </td>
        </tr>
    </table>
      <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
