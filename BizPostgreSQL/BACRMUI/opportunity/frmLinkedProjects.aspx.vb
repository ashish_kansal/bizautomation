Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin

Namespace BACRM.UserInterface.Opportunities
    Partial Public Class frmLinkedProjects
        Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                Dim objOpportunity As New MOpportunity
                objOpportunity.OpportunityId = GetQueryStringVal("opId")
                Dim dtTable As DataTable
                dtTable = objOpportunity.GetLinkedProjects
                gvLinkProjects.DataSource = dtTable
                gvLinkProjects.DataBind()


                Dim objActionItem As New ActionItem
                objActionItem.OpenRecordID = GetQueryStringVal("opId")
                objActionItem.DomainID = Session("DomainID")
                objActionItem.DomainID = Session("DomainID")
                objActionItem.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                gvCommunication.DataSource = objActionItem.GetCommunicationOpenRecord().Tables(0)
                gvCommunication.DataBind()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
