﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmOpportunityAutomationQueueExecutionLog.aspx.vb" Inherits=".frmOpportunityAutomationQueueExecutionLog" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function fn_doPage(pgno, CurrentPageWebControlClientID, PostbackButtonClientID) {
            var currentPage;
            var postbackButton;

            if (CurrentPageWebControlClientID != '') {
                currentPage = CurrentPageWebControlClientID;
            }
            else {
                currentPage = 'txtCurrentPageCorr';
            }

            if (PostbackButtonClientID != '') {
                postbackButton = PostbackButtonClientID;
            }
            else {
                postbackButton = 'btnCorresGo';
            }

            $('#' + currentPage + '').val(pgno);
            if ($('#' + currentPage + '').val() != 0 || $('#' + currentPage + '').val() > 0) {
                $('#' + postbackButton + '').trigger('click');
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <asp:Label ID="lblTitle" runat="server">Order Automation Execution Log</asp:Label>
            </td>
            <td align="right" valign="middle" style="font-size: 12px;">
                <webdiyer:AspNetPager ID="bizPager" runat="server" Direction="RightToLeft" HorizontalAlign="Right"
                    LayoutType="div" UrlPaging="false" CssClass="pagn" ShowMoreButtons="true" ShowPageIndexBox="Never"
                    Width="" AlwaysShow="true" ShowCustomInfoSection="Left" CustomInfoHTML="Showing records %startrecordindex% to %endrecordindex% of %recordcount% "
                    CustomInfoSectionWidth="300px" CustomInfoStyle="line-height:20px;margin-right:3px;text-align:right !important;" CurrentPageWebControlClientID="txtCurrrentPage" PostbackButtonClientID="btnGo1">
                </webdiyer:AspNetPager>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:GridView ID="gvAutomationLog" AutoGenerateColumns="False" runat="server" Width="1200px"
        CssClass="tbl" >
        <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is" />
        <Columns>
            <asp:TemplateField ItemStyle-Width="75px">
                <ItemTemplate>
                    <%# Eval("dtExecutionDate")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-Width="75px">
                <ItemTemplate>
                    <%# String.Format("{0:hh:mm tt}", Eval("dtExecutionTime"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="vcMessage" HeaderText="Description"></asp:BoundField>
        </Columns>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <EmptyDataTemplate>
            No records found.
        </EmptyDataTemplate>
    </asp:GridView>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPageDeals" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
