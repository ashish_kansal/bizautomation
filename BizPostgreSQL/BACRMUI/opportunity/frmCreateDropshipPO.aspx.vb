﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Workflow
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Contacts
Imports Telerik.Web.UI

Public Class frmCreateDropshipPO
    Inherits BACRMPage

#Region "Memeber Variables"

    Private objOPP As COpportunities

#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblError.Text = ""
            divError.Style.Add("display", "none")

            If Not Page.IsPostBack Then
                hdnOppID.Value = GetQueryStringVal("numOppID")

                If CCommon.ToLong(hdnOppID.Value) = 0 Then
                    DisplayError("Sales order does not exists.")
                Else
                    BinData()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
#End Region

#Region "Private Methods"

    Private Sub BinData()
        Try
            objOPP = New COpportunities
            objOPP.DomainID = CCommon.ToLong(Session("DomainID"))
            objOPP.OpportID = CCommon.ToLong(hdnOppID.Value)
            Dim ds As DataSet = objOPP.GetSODropshipItems()

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    gvDropshipItems.DataSource = ds.Tables(0)
                    gvDropshipItems.DataBind()
                End If

                If ds.Tables.Count > 1 AndAlso ds.Tables(1).Rows.Count > 0 Then
                    hdnShipCompanyName.Value = CCommon.ToString(ds.Tables(1).Rows(0)("Company"))
                    hdnShipStreet.Value = CCommon.ToString(ds.Tables(1).Rows(0)("Street"))
                    hdnShipCity.Value = CCommon.ToString(ds.Tables(1).Rows(0)("City"))
                    hdnShipState.Value = CCommon.ToLong(ds.Tables(1).Rows(0)("State"))
                    hdnShipCountry.Value = CCommon.ToLong(ds.Tables(1).Rows(0)("Country"))
                    hdnShipZipCode.Value = CCommon.ToString(ds.Tables(1).Rows(0)("PostCode"))
                End If

                If ds.Tables.Count > 2 AndAlso ds.Tables(2).Rows.Count > 0 Then
                    lblAddress.Text = CCommon.ToString(ds.Tables(2).Rows(0)("ShippingAdderss")).Replace("<pre>", "").Replace("</pre>", "<br/>")
                End If
            Else
                DisplayError("POs are already created for all dropship item(s).")
            End If

        Catch ex As Exception
            If ex.Message.Contains("INVALID_OPPID") Then
                DisplayError("Sales order does not exists.")
            Else
                Throw
            End If
        End Try
    End Sub

    Private Sub DisplayError(ByVal errorMessage As String)
        Try
            lblError.Text = errorMessage
            divError.Style.Add("display", "")
        Catch ex As Exception
            'DO NOT THROW ERROR
        End Try
    End Sub

    Private Sub CreateOrder(ByVal DivisionID As Long, ByVal ContactID As Long, ByVal CompanyName As String)
        Try
            Using objTransctionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                Dim i As Integer
                Dim objOpportunity As New MOpportunity
                objOpportunity.OpportunityId = 0
                objOpportunity.OppType = 2
                objOpportunity.OpportunityName = CompanyName & "-" & IIf(objOpportunity.OppType = 1, "SO", "PO") & "-" & Format(Now(), "MMMM")
                objOpportunity.ContactID = ContactID
                objOpportunity.DivisionID = DivisionID
                objOpportunity.UserCntID = Session("UserContactID")
                objOpportunity.EstimatedCloseDate = Now
                objOpportunity.DomainID = Session("DomainId")
                objOpportunity.Active = 1
                objOpportunity.DealStatus = objOpportunity.EnmDealStatus.DealWon
                objOpportunity.SourceType = 1

                Dim objAdmin As New CAdmin
                Dim dtBillingTerms As DataTable
                objAdmin.DivisionID = DivisionID
                dtBillingTerms = objAdmin.GetBillingTerms()

                objOpportunity.boolBillingTerms = IIf(dtBillingTerms.Rows(0).Item("tintBillingTerms") = 1, True, False)
                objOpportunity.BillingDays = dtBillingTerms.Rows(0).Item("numBillingDays")
                objOpportunity.boolInterestType = IIf(dtBillingTerms.Rows(0).Item("tintInterestType") = 1, True, False)
                objOpportunity.Interest = dtBillingTerms.Rows(0).Item("fltInterest")

                'objOpportunity.strItems = dsTemp.GetXml
                objOpportunity.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                Dim lngOppID As Long = objOpportunity.Save()(0)
                objOpportunity.OpportunityId = lngOppID

                ''Added By Sachin Sadhu||Date:29thApril12014
                ''Purpose :To Added Opportunity data in work Flow queue based on created Rules
                ''          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = lngOppID
                objWfA.SaveWFOrderQueue()
                'end of code

                Dim dg As GridViewRow
                For i = 0 To gvDropshipItems.Rows.Count - 1
                    dg = gvDropshipItems.Rows(i)

                    objOpportunity.ItemCode = CCommon.ToLong(DirectCast(dg.FindControl("lblItemCode"), Label).Text.Trim)
                    objOpportunity.Units = Math.Abs(CCommon.ToDecimal(CType(dg.FindControl("txtUnits"), TextBox).Text)) * (CType(dg.FindControl("hdnUOMConversionFactor"), HiddenField).Value)
                    objOpportunity.UnitPrice = CCommon.ToDecimal(CType(dg.FindControl("txtUnitCost"), TextBox).Text / CType(dg.FindControl("hdnUOMConversionFactor"), HiddenField).Value)
                    objOpportunity.Desc = ""
                    objOpportunity.ItemName = CCommon.ToString(DirectCast(dg.FindControl("lblItemName"), Label).Text.Trim)
                    objOpportunity.Notes = ""
                    objOpportunity.ModelName = CCommon.ToString(DirectCast(dg.FindControl("lblModelID"), Label).Text.Trim)
                    objOpportunity.ItemThumbImage = ""
                    objOpportunity.ItemType = CCommon.ToString(DirectCast(dg.FindControl("hdnCharItemType"), HiddenField).Value.Trim)
                    objOpportunity.Dropship = True
                    objOpportunity.WarehouseItemID = 0
                    objOpportunity.numUOM = CCommon.ToLong(CType(dg.FindControl("hdnBaseUnit"), HiddenField).Value)
                    objOpportunity.ProjectID = 0
                    objOpportunity.ClassID = 0

                    Dim ddlVendor As DropDownList
                    ddlVendor = dg.FindControl("ddlVendor")

                    Dim strValue As String() = ddlVendor.SelectedValue.Split(CChar("~"))

                    If CCommon.ToLong(strValue(0)) = DivisionID Then
                        objOpportunity.UpdateOpportunityItems()
                    End If
                Next
                'Fix of Bug id 638 -by chintan, Update inventory counts
                Dim objCustomReport As New BACRM.BusinessLogic.Reports.CustomReports
                objCustomReport.DynamicQuery = " PERFORM USP_UpdatingInventoryonCloseDeal(" & objOpportunity.OpportunityId & ",0," & Session("UserContactID") & ");"
                objCustomReport.ExecuteDynamicSql()

                Dim objContact As New CContacts
                objContact.AddressType = CContacts.enmAddressType.BillTo
                objContact.DomainID = CCommon.ToLong(Session("DomainID"))
                objContact.RecordID = CCommon.ToLong(Session("UserDivisionID"))
                objContact.AddresOf = CContacts.enmAddressOf.Organization
                objContact.byteMode = 2
                Dim dtAddress As DataTable = objContact.GetAddressDetail()


                objOpportunity.BillToType = 0

                If Not dtAddress Is Nothing AndAlso dtAddress.Rows.Count > 0 Then
                    objOpportunity.BillToAddressID = CCommon.ToLong(dtAddress.Rows(0)("numAddressID"))
                End If

                objOpportunity.ShipToType = 2
                objOpportunity.ShipStreet = hdnShipStreet.Value
                objOpportunity.ShipCity = hdnShipCity.Value
                objOpportunity.ShipState = hdnShipState.Value
                objOpportunity.ShipCountry = hdnShipCountry.Value
                objOpportunity.ShipPostal = hdnShipZipCode.Value
                objOpportunity.ShipCompanyName = hdnShipCompanyName.Value
                'Update Order Billing /Shipping Address

                objOpportunity.ParentOppID = CCommon.ToLong(hdnOppID.Value)
                objOpportunity.ParentBizDocID = 0
                objOpportunity.UpdateOpportunityLinkingDtls()

                If CCommon.ToLong(Session("PODropShipBizDoc")) AndAlso objOpportunity.OppType = 2 Then
                    Dim objOppBizDocs As New OppBizDocs
                    objOppBizDocs.OppId = lngOppID
                    objOppBizDocs.OppType = objOpportunity.OppType

                    objOppBizDocs.UserCntID = Session("UserContactID")
                    objOppBizDocs.DomainID = Session("DomainID")

                    objOppBizDocs.BizDocId = CCommon.ToLong(Session("PODropShipBizDoc"))
                    objOppBizDocs.BizDocTemplateID = CCommon.ToLong(Session("PODropShipBizDocTemplate"))

                    objOppBizDocs.OppBizDocId = 0

                    Dim dtOppItems As DataTable = objOppBizDocs.GetOppItemsForBizDocs()
                    Dim dsBizDocItems As New DataSet
                    Dim dtBizDocItems As New DataTable
                    If dtOppItems IsNot Nothing AndAlso dtOppItems.Rows.Count > 0 Then
                        dtBizDocItems.TableName = "BizDocItems"
                        dtBizDocItems.Columns.Add("OppItemID")
                        dtBizDocItems.Columns.Add("Quantity")
                        dtBizDocItems.Columns.Add("Notes")
                        dtBizDocItems.Columns.Add("monPrice")
                        dtBizDocItems.AcceptChanges()

                        Dim drNew As DataRow
                        For Each drItem As DataRow In dtOppItems.Rows
                            If (CCommon.ToBool(drItem("bitDropShip"))) Then
                                drNew = dtBizDocItems.NewRow
                                drNew("OppItemID") = CCommon.ToDouble(drItem("numoppitemtCode"))
                                drNew("Quantity") = CCommon.ToDouble(drItem("QtyOrdered"))
                                drNew("Notes") = CCommon.ToString(drItem("vcNotes"))
                                drNew("monPrice") = CCommon.ToDouble(drItem("monOppItemPrice"))

                                dtBizDocItems.Rows.Add(drNew)
                                dtBizDocItems.AcceptChanges()
                            End If
                        Next
                        If dtBizDocItems IsNot Nothing AndAlso dtBizDocItems.Rows.Count > 0 Then

                            dsBizDocItems.Tables.Add(dtBizDocItems)
                            dsBizDocItems.Tables(0).TableName = "BizDocItems"

                            objOppBizDocs.strBizDocItems = dsBizDocItems.GetXml()

                            objOppBizDocs.vcPONo = "" 'txtPO.Text
                            objOppBizDocs.vcComments = "" 'txtComments.Text

                            objOppBizDocs.FromDate = DateTime.UtcNow
                            objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                            objCommon = New CCommon
                            objCommon.DomainID = Session("DomainID")
                            objCommon.Mode = 33
                            objCommon.Str = objOppBizDocs.BizDocId
                            objOppBizDocs.SequenceId = objCommon.GetSingleFieldValue()

                            objCommon = New CCommon
                            objCommon.DomainID = Session("DomainID")
                            objCommon.Mode = 34
                            objCommon.Str = lngOppID
                            objOppBizDocs.RefOrderNo = objCommon.GetSingleFieldValue()

                            objOppBizDocs.bitPartialShipment = True
                            Dim OppBizDocID As Long
                            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                OppBizDocID = objOppBizDocs.SaveBizDoc()

                                Dim lintAuthorizativeBizDocsId As Long
                                objOppBizDocs.DomainID = Session("DomainID")
                                objOppBizDocs.OppId = lngOppID
                                lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()

                                'Create Journals only for authoritative bizdocs
                                '-------------------------------------------------
                                If lintAuthorizativeBizDocsId = objOppBizDocs.BizDocId Then
                                    Dim ds As New DataSet
                                    Dim dtOppBiDocItems As DataTable

                                    objOppBizDocs.OppBizDocId = OppBizDocID
                                    ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                                    dtOppBiDocItems = ds.Tables(0)

                                    Dim objCalculateDealAmount As New CalculateDealAmount

                                    objCalculateDealAmount.CalculateDealAmount(lngOppID, OppBizDocID, 2, Session("DomainID"), dtOppBiDocItems)

                                    ''---------------------------------------------------------------------------------
                                    Dim JournalId As Long = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, objOppBizDocs.FromDate, Description:=ds.Tables(1).Rows(0).Item("vcBizDocID"))
                                    Dim objJournalEntries As New JournalEntry

                                    If CCommon.ToBool(ds.Tables(1).Rows(0).Item("bitPPVariance")) Then
                                        objJournalEntries.SaveJournalEntriesPurchaseVariance(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, DivisionID, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), ds.Tables(1).Rows(0).Item("fltExchangeRateBizDoc"), vcBaseCurrency:=ds.Tables(1).Rows(0).Item("vcBaseCurrency"), vcForeignCurrency:=ds.Tables(1).Rows(0).Item("vcForeignCurrency"))
                                    Else
                                        objJournalEntries.SaveJournalEntriesPurchase(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, DivisionID, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"))
                                    End If
                                End If

                                objTransactionScope.Complete()
                            End Using


                            ''Added By Sachin Sadhu||Date:1stMay2014
                            ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                            ''          Using Change tracking
                            objWfA = New Workflow()
                            objWfA.DomainID = Session("DomainID")
                            objWfA.UserCntID = Session("UserContactID")
                            objWfA.RecordID = OppBizDocID
                            objWfA.SaveWFBizDocQueue()

                            '------ Send BizDoc Alerts - Notify record owners, their supervisors, and your trading partners when a BizDoc is created, modified, or approved.
                            Dim objAlert As New CAlerts
                            objAlert.SendBizDocAlerts(lngOppID, OppBizDocID, objOppBizDocs.BizDocId, Session("DomainID"), CAlerts.enmBizDoc.IsCreated)

                            Dim objAutomatonRule As New AutomatonRule
                            objAutomatonRule.ExecuteAutomationRule(49, OppBizDocID, 1)
                        End If
                    End If
                End If

                objTransctionScope.Complete()
            End Using
        Catch ex As Exception
            If ex.Message = "NOT_ALLOWED" Then
                Exit Sub
            Else
                Throw ex
            End If
        End Try
    End Sub

    Private Sub BindVendorAddresses(ByVal vendorID As Long, ByRef ddlVendorWarehouses As DropDownList)
        Try
            ddlVendorWarehouses.Items.Clear()

            Dim objItems As New CItems
            objItems.DomainID = Session("DomainID")
            objItems.VendorID = vendorID
            objItems.byteMode = 2
            Dim dtVendorsWareHouse As DataTable = objItems.GetVendorsWareHouse

            If dtVendorsWareHouse.Rows.Count > 0 Then
                ddlVendorWarehouses.DataSource = dtVendorsWareHouse
                ddlVendorWarehouses.DataTextField = "vcFullAddress"
                ddlVendorWarehouses.DataValueField = "numAddressID"
                ddlVendorWarehouses.DataBind()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BindVendorShipmentMethod(ByVal vendorID As Long, ByVal vendorWarehouse As Long, ByRef ddlVendorShipmentMethod As DropDownList)
        Try
            ddlVendorShipmentMethod.Items.Clear()

            Dim objItems As New CItems
            objItems.DomainID = Session("DomainID")
            objItems.ShipAddressId = vendorWarehouse
            objItems.VendorID = vendorID
            objItems.byteMode = 2
            Dim dtVendorShipmentMethod As DataTable = objItems.GetVendorShipmentMethod

            If Not dtVendorShipmentMethod Is Nothing AndAlso dtVendorShipmentMethod.Rows.Count > 0 Then
                ddlVendorShipmentMethod.DataSource = dtVendorShipmentMethod
                ddlVendorShipmentMethod.DataTextField = "ShipmentMethod"
                ddlVendorShipmentMethod.DataValueField = "vcListValue"
                ddlVendorShipmentMethod.DataBind()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

#End Region

#Region "Event Handlers"

    Protected Sub ddlVendor_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            Dim ddlVendor As DropDownList = DirectCast(sender, DropDownList)
            Dim grv As GridViewRow = DirectCast(ddlVendor.NamingContainer, GridViewRow)

            If Not grv Is Nothing Then
                Dim ddlVendorWarehouses As DropDownList = DirectCast(grv.FindControl("ddlVendorWarehouses"), DropDownList)
                Dim ddlVendorShipmentMethod As DropDownList = DirectCast(grv.FindControl("ddlVendorShipmentMethod"), DropDownList)
                Dim lblLeadTimeDays As Label = DirectCast(grv.FindControl("lblLeadTimeDays"), Label)
                Dim lblMinOrderQty As Label = DirectCast(grv.FindControl("lblMinOrderQty"), Label)


                BindVendorAddresses(CCommon.ToLong(ddlVendor.SelectedValue.Split("~")(0)), ddlVendorWarehouses:=ddlVendorWarehouses)

                If ddlVendorWarehouses.Items.Count > 0 Then
                    lblMinOrderQty.Text = ddlVendor.SelectedValue.Split("~")(3)
                    BindVendorShipmentMethod(CCommon.ToLong(ddlVendor.SelectedValue.Split("~")(0)), CCommon.ToLong(ddlVendorWarehouses.SelectedValue), ddlVendorShipmentMethod:=ddlVendorShipmentMethod)

                    If ddlVendorShipmentMethod.Items.Count > 0 Then
                        ddlVendorShipmentMethod.Attributes.Add("onchange", "ChangeLeadTimeDays('" & lblLeadTimeDays.ClientID & "','" & ddlVendorShipmentMethod.ClientID & "') ")
                        lblLeadTimeDays.Text = ddlVendorShipmentMethod.SelectedValue.Split("~")(1)
                    Else
                        lblLeadTimeDays.Text = "-"
                    End If
                Else
                    lblMinOrderQty.Text = "-"
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub ddlVendorWarehouses_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            Dim ddlVendorWarehouses As DropDownList = DirectCast(sender, DropDownList)
            Dim grv As GridViewRow = DirectCast(ddlVendorWarehouses.NamingContainer, GridViewRow)

            If Not grv Is Nothing Then
                Dim ddlVendor As DropDownList = DirectCast(grv.FindControl("ddlVendor"), DropDownList)
                Dim lblLeadTimeDays As Label = DirectCast(grv.FindControl("lblLeadTimeDays"), Label)
                Dim ddlVendorShipmentMethod As DropDownList = DirectCast(grv.FindControl("ddlVendorShipmentMethod"), DropDownList)

                BindVendorShipmentMethod(CCommon.ToLong(ddlVendor.SelectedValue.Split("~")(0)), ddlVendorWarehouses.SelectedValue, ddlVendorShipmentMethod)

                If ddlVendorShipmentMethod.Items.Count > 0 Then
                    lblLeadTimeDays.Text = ddlVendorShipmentMethod.SelectedValue.Split("~")(1)
                Else
                    lblLeadTimeDays.Text = "-"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub ddlVendorShipmentMethod_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            Dim ddlVendorShipmentMethod As DropDownList = DirectCast(sender, DropDownList)
            Dim grv As GridViewRow = DirectCast(ddlVendorShipmentMethod.NamingContainer, GridViewRow)

            If Not grv Is Nothing Then
                Dim lblLeadTimeDays As Label = DirectCast(grv.FindControl("lblLeadTimeDays"), Label)

                If ddlVendorShipmentMethod.Items.Count > 0 Then
                    lblLeadTimeDays.Text = ddlVendorShipmentMethod.SelectedValue.Split("~")(1)
                Else
                    lblLeadTimeDays.Text = "-"
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnCreateDropshipPO_Click(sender As Object, e As EventArgs) Handles btnCreateDropshipPO.Click
        Try
            'If radcmbCustomerShipToAddresses.SelectedItem Is Nothing Then
            '    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please select customer Ship-To Location.' );", True)
            '    Exit Sub
            'End If

            Dim objJournal As New JournalEntry
            If ChartOfAccounting.GetDefaultAccount("CG", Session("DomainID")) = 0 Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please Set Default COGs account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                Exit Sub
            End If

            If ChartOfAccounting.GetDefaultAccount("PC", Session("DomainID")) = 0 Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please Set Default Purchase Clearing account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                Exit Sub
            End If

            If ChartOfAccounting.GetDefaultAccount("PV", Session("DomainID")) = 0 Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please Set Default Purchase Price Variance account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                Exit Sub
            End If

            'validate item income,asset , cogs account are mapped based on item type
            Dim objItem As New CItems
            Dim objOppBizDocs As New OppBizDocs
            For Each dgGridItem As GridViewRow In gvDropshipItems.Rows
                If Not objItem.ValidateItemAccount(DirectCast(dgGridItem.FindControl("lblItemCode"), Label).Text, Session("DomainID"), 2) = True Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "AccountValidation", "alert('Please Set Income,Asset,COGs(Expense) Account for " & DirectCast(dgGridItem.FindControl("lblItemName"), Label).Text & " from Administration->Inventory->Item Details')", True)
                    Exit Sub
                End If

                Dim ddlVendor As DropDownList
                ddlVendor = dgGridItem.FindControl("ddlVendor")
                If ddlVendor.SelectedItem Is Nothing Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please select vendor for all item(s).' );", True)
                    Exit Sub
                End If

                Dim strValue As String() = ddlVendor.SelectedValue.Split(CChar("~"))

                Dim txtUnits As TextBox = DirectCast(dgGridItem.FindControl("txtUnits"), TextBox)
                Dim units As Double = 0
                If Not Double.TryParse(txtUnits.Text, units) Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Enter units to purchase for all item(s).' );", True)
                    Exit Sub
                ElseIf units = 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Enter units to purchase for all item(s).' );", True)
                    Exit Sub
                End If

                If objOppBizDocs.ValidateARAP(CCommon.ToLong(strValue(0)), 0, Session("DomainID")) = False Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting->Accounts for RelationShip"" To Save' );", True)
                    Exit Sub
                End If
            Next

            Dim dr As DataRow
            Dim gridRow As GridViewRow

            'Create table to group item by selected vendor
            Dim dtItem As New DataTable
            dtItem.Columns.Add("ItemCode")
            dtItem.Columns.Add("Units")
            dtItem.Columns.Add("UnitPrice")
            dtItem.Columns.Add("Desc")
            dtItem.Columns.Add("ItemName")
            dtItem.Columns.Add("ModelName")
            dtItem.Columns.Add("ItemThumbImage")
            dtItem.Columns.Add("ItemType")
            dtItem.Columns.Add("Dropship")
            dtItem.Columns.Add("WarehouseItemID")
            dtItem.Columns.Add("numVendorID")
            dtItem.Columns.Add("numVendorContactId")
            dtItem.Columns.Add("VendorCompanyName")
            dtItem.Columns.Add("numUOM")
            dtItem.Columns.Add("txtNotes")


            Dim ErrorMessage As String = ""
            For i = 0 To gvDropshipItems.Rows.Count - 1
                gridRow = gvDropshipItems.Rows(i)

                Dim ddlVendor As DropDownList
                ddlVendor = gridRow.FindControl("ddlVendor")

                If ddlVendor.Items.Count > 0 Then
                    dr = dtItem.NewRow

                    dr("ItemCode") = CCommon.ToLong(DirectCast(gridRow.FindControl("lblItemCode"), Label).Text.Trim)
                    dr("Units") = CCommon.ToDouble(CType(gridRow.FindControl("txtUnits"), TextBox).Text) * (CType(gridRow.FindControl("hdnUOMConversionFactor"), HiddenField).Value)
                    dr("UnitPrice") = CCommon.ToDecimal(CType(gridRow.FindControl("txtUnitCost"), TextBox).Text / CType(gridRow.FindControl("hdnUOMConversionFactor"), HiddenField).Value)
                    dr("Desc") = ""
                    dr("ItemName") = CCommon.ToString(DirectCast(gridRow.FindControl("lblItemName"), Label).Text.Trim)
                    dr("ModelName") = CCommon.ToString(DirectCast(gridRow.FindControl("lblModelID"), Label).Text.Trim)
                    dr("ItemThumbImage") = ""
                    dr("ItemType") = CCommon.ToString(DirectCast(gridRow.FindControl("hdnCharItemType"), HiddenField).Value.Trim)
                    dr("Dropship") = True
                    dr("WarehouseItemID") = 0
                    dr("txtNotes") = ""
                    dr("numUOM") = CCommon.ToLong(CType(gridRow.FindControl("hdnBaseUnit"), HiddenField).Value)

                    Dim strValue As String() = ddlVendor.SelectedValue.Split(CChar("~"))

                    dr("numVendorID") = CCommon.ToLong(strValue(0))
                    dr("numVendorContactId") = CCommon.ToLong(strValue(1))
                    dr("VendorCompanyName") = ddlVendor.SelectedItem.Text

                    dtItem.Rows.Add(dr)
                Else
                    If String.IsNullOrEmpty(ErrorMessage) Then
                        ErrorMessage = "PO can not be created, Your option is to select a vender for following item(s):"
                    Else
                        ErrorMessage += "<br/>=>" & CCommon.ToString(DirectCast(gridRow.FindControl("lblItemName"), Label).Text.Trim)
                    End If
                End If
            Next

            If Not String.IsNullOrEmpty(ErrorMessage) Then
                DisplayError(ErrorMessage)
                Exit Sub
            End If

            Dim dtVendor As DataTable
            dtVendor = dtItem.DefaultView.ToTable(True, "numVendorID")

            For Each drRow As DataRow In dtVendor.Select()

                Dim drItemColl() As DataRow
                'Create single order for all item which are referenced to same vendor
                drItemColl = dtItem.Select("(numVendorID = '" & drRow.Item("numVendorID") & "')")

                'Create Order against primary vendors
                CreateOrder(drItemColl(0).Item("numVendorID"), drItemColl(0).Item("numVendorContactId"), drItemColl(0).Item("VendorCompanyName"))
            Next

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "CloseWindow", "window.opener.location.href=window.opener.location.href; Close();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvDropshipItems_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvDropshipItems.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim drv As DataRowView = DirectCast(e.Row.DataItem, DataRowView)
                Dim ddlVendor As DropDownList = DirectCast(e.Row.FindControl("ddlVendor"), DropDownList)
                Dim ddlVendorWarehouses As DropDownList = DirectCast(e.Row.FindControl("ddlVendorWarehouses"), DropDownList)
                Dim ddlVendorShipmentMethod As DropDownList = DirectCast(e.Row.FindControl("ddlVendorShipmentMethod"), DropDownList)
                Dim txtUnitCost As TextBox = DirectCast(e.Row.FindControl("txtUnitCost"), TextBox)
                Dim txtUnits As TextBox = DirectCast(e.Row.FindControl("txtUnits"), TextBox)
                Dim lblMinOrderQty As Label = DirectCast(e.Row.FindControl("lblMinOrderQty"), Label)
                Dim lblLeadTimeDays As Label = DirectCast(e.Row.FindControl("lblLeadTimeDays"), Label)
                Dim imgThumb As System.Web.UI.WebControls.Image = DirectCast(e.Row.FindControl("imgThumb"), System.Web.UI.WebControls.Image)

                txtUnits.Text = String.Format("{0:#,##0}", (Math.Abs(drv("numUnitHour") / drv("fltUOMConversionFactor"))))
                txtUnitCost.Text = CCommon.GetDecimalFormat(CCommon.ToDouble(drv("monCost") * drv("fltUOMConversionFactor")))
                imgThumb.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & If(IsDBNull(drv("vcPathForTImage")), "", drv("vcPathForTImage"))

                If Not ddlVendor Is Nothing Then
                    Dim objItems As New CItems
                    objItems.DomainID = Session("DomainID")
                    objItems.ItemCode = CCommon.ToLong(drv("numItemCode"))
                    Dim dtVendors As DataTable = objItems.GetVendors


                    For Each drVendor As DataRow In dtVendors.Rows
                        ddlVendor.Items.Add(New ListItem(drVendor.Item("Vendor").ToString(), drVendor.Item("numVendorID").ToString() & "~" & drVendor.Item("numContactId").ToString() & "~" & String.Format("{0:#,##0.00}", drVendor.Item("monCost")) & "~" & drVendor.Item("intMinQty").ToString()))
                    Next

                    If CCommon.ToShort(drv("tintDefaultCost")) = 3 Then 'Vendor Cost
                        ddlVendor.Attributes.Add("onchange", "ChangeVendorCost('" & txtUnitCost.ClientID & "','" & ddlVendor.ClientID & "'," & CCommon.ToDouble(drv("fltUOMConversionFactor")) & "," & CCommon.ToDouble(drv("fltUOMConversionFactorForPurchase")) & ") ")
                    End If

                    If Not ddlVendor.Items.FindByValue(CCommon.ToLong(drv("numVendorID"))) Is Nothing Then
                        ddlVendor.Items.FindByValue(CCommon.ToLong(drv("numVendorID"))).Selected = True
                    End If

                    If Not ddlVendor.SelectedItem Is Nothing Then
                        BindVendorAddresses(CCommon.ToLong(ddlVendor.SelectedValue.Split("~")(0)), ddlVendorWarehouses:=ddlVendorWarehouses)

                        If ddlVendorWarehouses.Items.Count > 0 Then
                            BindVendorShipmentMethod(CCommon.ToLong(ddlVendor.SelectedValue.Split("~")(0)), CCommon.ToLong(ddlVendorWarehouses.SelectedValue), ddlVendorShipmentMethod:=ddlVendorShipmentMethod)

                            If ddlVendorShipmentMethod.Items.Count > 0 Then
                                lblLeadTimeDays.Text = ddlVendorShipmentMethod.SelectedValue.Split("~")(1)
                            Else
                                lblLeadTimeDays.Text = "-"
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    'Private Sub radcmbCustomerShipToAddresses_SelectedIndexChanged(sender As Object, e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radcmbCustomerShipToAddresses.SelectedIndexChanged
    '    Try
    '        If Not radcmbCustomerShipToAddresses.SelectedItem Is Nothing Then
    '            lblAddress.Text = radcmbCustomerShipToAddresses.SelectedItem.Attributes("Address")
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        DisplayError(ex.Message)
    '    End Try
    'End Sub

#End Region

    
    
End Class