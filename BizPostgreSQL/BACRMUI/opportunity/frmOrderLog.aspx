<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmOrderLog.aspx.vb"
    Inherits=".frmOrderLog" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Order Shipping Details</title>
        <script type="text/javascript">
           
            function CloseWindow() {
                this.close();
            }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
           <asp:Button runat="server" ID="btnClose" Text="Close" OnClientClick="CloseWindow()" CssClass="button" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Order Shipping Details
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
     <asp:ScriptManager runat="server" ID="sc1" />
    <table width="600px">
        <tr>
            <td class="text_bold" align="right">Added By&nbsp;:
            </td>
            <td>
                <asp:Label runat="server" ID="lblAddedBy"></asp:Label></td>
            <td class="text_bold" align="right">Added On&nbsp;:
            </td>
            <td>
                <asp:Label runat="server" ID="lblAddedOn"></asp:Label></td>
        </tr>
        <tr>
            <td class="text_bold" align="right">Regular Weight&nbsp;:
            </td>
            <td>
                <asp:Label runat="server" ID="lblRegularWeight"></asp:Label></td>
            <td class="text_bold" align="right">Dimensional Weight&nbsp;:
            </td>
            <td>
                <asp:Label runat="server" ID="lblDimensionalWeight"></asp:Label>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <br />
                <telerik:RadGrid ID="radBoxes" runat="server" Width="100%" AutoGenerateColumns="False"
                    GridLines="None" ShowFooter="false" Skin="windows" EnableEmbeddedSkins="false"
                    CssClass="dg" AlternatingItemStyle-CssClass="ais" ItemStyle-CssClass="is" HeaderStyle-CssClass="hs"
                    AutoGenerateHierarchy="false">
                    <MasterTableView DataKeyNames="vcBoxName" HierarchyLoadMode="Client" DataMember="Box">
                        <DetailTables>
                            <telerik:GridTableView Width="100%" runat="server" ItemStyle-HorizontalAlign="Center"
                                AlternatingItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                ShowFooter="false" DataMember="BoxItems">
                                <ParentTableRelation>
                                    <telerik:GridRelationFields DetailKeyField="vcBoxName" MasterKeyField="vcBoxName" />
                                </ParentTableRelation>
                                <Columns>
                                    <telerik:GridBoundColumn HeaderText="vcBoxName" DataField="vcBoxName" Visible="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="numItemCode" DataField="numItemCode" Visible="false">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="Item" DataField="vcItemName">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="Weight per Unit" DataField="UnitWeight">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Qty">
                                        <ItemTemplate>
                                            <%# DataBinder.Eval(Container.DataItem, "intBoxQty")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Dimensions = ( H x L x W ) ">
                                        <ItemTemplate>
                                            <%# DataBinder.Eval(Container.DataItem, "fltHeight")%>
                                                        x
                                                        <%# DataBinder.Eval(Container.DataItem, "fltLength")%>
                                                        x
                                                        <%# DataBinder.Eval(Container.DataItem, "fltWidth")%>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                            </telerik:GridTableView>
                        </DetailTables>
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="numBoxID" DataField="numBoxID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Box" DataField="vcBoxName">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Dimensions = ( H x L x W )">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "fltHeight")%>
                                                x
                                                <%# DataBinder.Eval(Container.DataItem, "fltLength")%>
                                                x
                                                <%# DataBinder.Eval(Container.DataItem, "fltWidth")%>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Total weight">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "fltTotalWeight")%>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Service">
                                <ItemTemplate>
                                    <%# TranslateServiceType(Eval("numServiceTypeID"))%>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Package Type">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "vcPackageName")%>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <%--<telerik:GridBoundColumn HeaderText="Rate" DataFormatString="{0:#,###.00}">
                            </telerik:GridBoundColumn>--%>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings AllowExpandCollapse="true"  />
                </telerik:RadGrid>
            </td>
        </tr>
       <%-- <tr>
            <td colspan="4">
                <br />
                <asp:GridView ID="gvBox" runat="server" EnableViewState="true" AutoGenerateColumns="true"
                    CssClass="tbl" Width="100%" ShowHeaderWhenEmpty="true">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <Columns>
                    </Columns>
                    <EmptyDataTemplate>
                        No records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <br />
                <asp:GridView ID="gvItem" runat="server" EnableViewState="true" AutoGenerateColumns="true"
                    CssClass="tbl" Width="100%" ShowHeaderWhenEmpty="true">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <Columns>
                    </Columns>
                    <EmptyDataTemplate>
                        No records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>--%>
    </table>
</asp:Content>
