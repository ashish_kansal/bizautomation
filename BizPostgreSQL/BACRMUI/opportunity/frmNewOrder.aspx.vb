﻿Imports System.Collections.Generic
Imports System.Text
Imports System.Text.RegularExpressions
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Reports
Imports BACRM.BusinessLogic.ShioppingCart
Imports BACRM.BusinessLogic.TimeAndExpense
Imports BACRM.BusinessLogic.Workflow
Imports ConsumedByCode.SignatureToImage
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Promotion

Public Class frmNewOrder
    Inherits BACRMPage

#Region "Enum"
    Enum PageTypeEnum As Integer
        Sales
        Purchase
        AddOppertunity
        AddEditOrder
    End Enum
#End Region

#Region "Member Variables"
    Private isPOS As Short
    Private dsTemp As DataSet
    Private objItems As CItems
    Private oppType As Integer
    Private oppBulk As Boolean = False
    Private pageType As PageTypeEnum
    Private boolFlag As Boolean = True
    Private dtOppAtributes As DataTable
    Private dtOppBiDocItems As DataTable
    Private m_aryRightsForItems() As Integer
    Private m_aryRightsForEditUnitPrice() As Integer
    Private m_aryRightsForAddEditCustomerPartNo() As Integer
    Private lngShippingItemCode As Double = 0
    Private IsFromSaveAndNew As Boolean = False
    Private IsFromCreateBizDoc As Boolean = False
    Private m_aryRightsForAuthoritativ() As Integer
    Private isShippingMethodNeeded As Boolean = False
    Private m_aryRightsForNonAuthoritativ() As Integer
    Private IsFromSaveAndOpenOrderDetails As Boolean = False
    Private chkSelectedItem As ListItem
    Private dtOppTableInfo As DataTable
    Private objOppBizDocs As New OppBizDocs
    Private objOppPageControls As New PageControls
    Private objSalesOrderConfiguration As SalesOrderConfiguration
    Private ResponseMessage As String
    Private ReturnTransactionID As String = ""
    Private lngDivId, lngCntID, lngOppId, OppBizDocID, JournalId, lngDepositeID, lngTransactionHistoryID As Long
    Private strDocumentPath As String
    Private dtMultiSelect As DataTable
    Private lngDiscountServiceItemForOrder As Double = 0
    Private lngDiscountAmt As Double = 0
    Private bitPromotionDiscountType As Boolean = True
    Private bitRequireCouponCode As Boolean = False
    ' Order Promotion Variables
    ' Shipping Promotion Variables
    Private ShippingAmt As Double
    Private bitFreeShipping As Boolean
    ' Shipping Promotion Variables
#End Region

#Region "Public Properties"
    Public ReadOnly Property GetCompanyName() As String
        Get
            radCmbCompany.Text.Trim()
        End Get
    End Property
#End Region

#Region "Private Property"
    Private Property dtLocationsGrid As DataTable
        Get
            Return ViewState("VSdtLocationsGrid")
        End Get
        Set(value As DataTable)
            ViewState("VSdtLocationsGrid") = value
        End Set
    End Property

    Private Property dtRelatedItems As DataTable
        Get
            Return ViewState("VSdtRelatedItems")
        End Get
        Set(value As DataTable)
            ViewState("VSdtRelatedItems") = value
        End Set
    End Property

    Private Property dtShippingShipFromTo As DataTable
        Get
            Return ViewState("VSdtShippingShipFromTo")
        End Get
        Set(value As DataTable)
            ViewState("VSdtShippingShipFromTo") = value
        End Set
    End Property

    Private Property dtMultiSelectShipFromAddr As DataTable
        Get
            Return ViewState("VSdtMultiSelectShipFromAddr")
        End Get
        Set(value As DataTable)
            ViewState("VSdtMultiSelectShipFromAddr") = value
        End Set
    End Property

    Private Property dtMultiSelectShipToAddr As DataTable
        Get
            Return ViewState("VSdtMultiSelectShipToAddr")
        End Get
        Set(value As DataTable)
            ViewState("VSdtMultiSelectShipToAddr") = value
        End Set
    End Property

    Private Property SelectedWarehouseItemIDs() As ArrayList
        Get
            If ViewState("SelectedWarehouseItemIDs") Is Nothing Then
                ViewState("SelectedWarehouseItemIDs") = New ArrayList
            End If

            Return CType(ViewState("SelectedWarehouseItemIDs"), ArrayList)
        End Get
        Set(ByVal Value As ArrayList)
            ViewState("SelectedWarehouseItemIDs") = Value
        End Set
    End Property
#End Region

#Region "Page Events"

    Dim AuthorizeStatus As Boolean
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            If Not IsPostBack Then
                Session("CouponCode") = Nothing
                Session("TotalDiscount") = Nothing
                radCmbCompany.Focus()

                'Hide unit price approvar button if user does not have right to approve unit price
                Dim userID As String = CCommon.ToString(Session("UserID"))
                Dim unitPriceApprover() As String = CCommon.ToString(Session("UnitPriceApprover")).Split(",")

                If Not unitPriceApprover Is Nothing AndAlso unitPriceApprover.Contains(userID) Then
                    btnApproveUnitPrice.Visible = True
                End If

            End If

            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
            CCommon.UpdateItemRadComboValues("1", radCmbCompany.SelectedValue)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            txtUnits.Attributes.Add("onkeyup", "return AddItemToOrder(event);")
            txtprice.Attributes.Add("onkeyup", "return AddItemToOrder(event);")
            txtCustomerPartNo.Attributes.Add("onkeyup", "return AddItemToOrder(event);")
            txtItemDiscount.Attributes.Add("onkeyup", "return AddItemToOrder(event);")
            If GetQueryStringVal("OppType") = "1" And GetQueryStringVal("OppStatus") = "0" Then
                oppType = 1
                pageType = PageTypeEnum.AddOppertunity
                lblTitle.Text = "Opportunity"
            ElseIf GetQueryStringVal("OppType") = "1" And GetQueryStringVal("OppStatus") = "1" Then
                oppType = 1
                pageType = PageTypeEnum.Sales
                lblTitle.Text = "Order"

                If GetQueryStringVal("bulk") = "1" Then
                    oppBulk = True
                    divselcompany.Visible = False
                    divselcompany1.Visible = False
                    divExistingCustomerColumn2.Visible = False
                    divbulksales.Visible = True
                    Dim contIds As String() = GetQueryStringVal("CntIds").ToString().Split(",")
                    lblbulksales.Text = contIds.Length.ToString() & " customers selected"
                    lkbNewCustomer.Visible = False
                    plhPaymentMethods.Visible = False
                End If
            End If
            lblException.Text = ""

            Dim eventTarget As String = If((Me.Request("__EVENTTARGET") Is Nothing), String.Empty, Me.Request("__EVENTTARGET"))

            If eventTarget = "btnReloadAddress" Then
                FillExistingAddress(txtDivID.Text)
                ShippingPromotion()
            End If

            'Loading dynamic controls is required on each postback
            LoadOrderDetailSection()

            If Not Session("SalesOrderConfiguration") Is Nothing Then
                objSalesOrderConfiguration = Session("SalesOrderConfiguration")

                If (objSalesOrderConfiguration.bitDisplayCustomerPartEntry = False) Then
                    txtCustomerPartNo.Visible = False
                    lblCustomerPartNo.Visible = False
                    divCustomerPart.Visible = False
                Else
                    txtCustomerPartNo.Visible = True
                    lblCustomerPartNo.Visible = True
                    divCustomerPart.Visible = True
                End If
            End If

            'Start - Code from order.ascx page load
            If CCommon.ToDouble(Session("ShippingServiceItem")) > 0 Then
                lngShippingItemCode = CCommon.ToDouble(Session("ShippingServiceItem"))
            Else
                lngShippingItemCode = 0
            End If

            If CCommon.ToDouble(Session("DiscountServiceItem")) > 0 Then
                lngDiscountServiceItemForOrder = CCommon.ToDouble(Session("DiscountServiceItem"))
            Else
                lngDiscountServiceItemForOrder = 0
            End If

            DomainID = Session("DomainID")
            UserCntID = Session("UserContactID")
            isPOS = 0
            GetUserRightsForPage(10, 1)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AS")
            If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnSave.Visible = False
                btnSaveNew.Visible = False
                btnSaveBizDoc.Visible = False
                btnSaveOpenOrder.Visible = False
            End If
            'End - Code from order.ascx page load

            If Not IsPostBack Then
                PersistTable.Load(strParam:="frmNewOrderSearchType", isMasterPage:=True)
                If PersistTable.Count > 0 Then
                    hdnSearchType.Value = CCommon.ToString(PersistTable("frmNewOrderSearchType"))
                End If

                Dim objMassSalesFulfillmentConfiguration As New MassSalesFulfillmentConfiguration
                objMassSalesFulfillmentConfiguration.DomainID = CCommon.ToLong(Session("DomainID"))
                hdnIsAutoWarehouseSelection.Value = CCommon.ToBool(objMassSalesFulfillmentConfiguration.GetColumnValue("bitAutoWarehouseSelection"))
                hdnPortalURL.Value = CCommon.GetDocumentPath(Session("DomainID"))
                BindShippingCompanyAndMethods(radShipVia, radCmbShippingMethod)
                hdnEnabledItemLevelUOM.Value = CCommon.ToBool(Session("EnableItemLevelUOM"))

                LoadLocations()

                objSalesOrderConfiguration = New SalesOrderConfiguration
                objSalesOrderConfiguration.DomainID = Session("DomainID")
                objSalesOrderConfiguration.GetSalesOrderConfiguration()
                Session("SalesOrderConfiguration") = objSalesOrderConfiguration

                FillClass()
                BindMultiCurrency()
                BindSalesTemplate()
                FillAssignToDropdown(radcmbAssignedTo)
                FillAssignToDropdown(radcmbNewAssignTo)
                SetAssignedToEnabled(0, 0)
                'Start - Code from order.ascx page load
                hdnPOS.Value = isPOS
                If (pageType <> PageTypeEnum.AddEditOrder) Then
                    createSet()
                End If

                If GetQueryStringVal("bulk") = "1" Then
                    Dim strTax As String = ""
                    For k As Integer = 0 To chkTaxItems.Items.Count - 1
                        strTax = strTax & "0#1" & ","
                    Next
                    txtTax.Text = strTax
                End If

                txtDivID.Text = 0

                objCommon = New CCommon
                objCommon.UserCntID = Session("UserContactID")

                BindButtons()
                'End - Code from order.ascx page load

                If CCommon.ToLong(Session("UserContactID")) > 0 AndAlso CCommon.ToLong(Session("UserID")) = 0 Then
                    objCommon.DivisionID = CCommon.ToLong(Session("UserDivisionID"))
                    objCommon.charModule = "D"
                    radCmbCompany.Enabled = False
                    txtprice.ReadOnly = True
                    txtItemDiscount.ReadOnly = True

                    objCommon.GetCompanySpecificValues1()
                    Dim strCompany, strContactID As String
                    strCompany = objCommon.GetCompanyName
                    strContactID = objCommon.ContactID
                    radCmbCompany.Text = strCompany
                    radCmbCompany.SelectedValue = objCommon.DivisionID

                    radCmbCompany_SelectedIndexChanged() 'Added by chintan- Reason:remaining credit wasn't showing

                    If Not radcmbContact.Items.FindItemByValue(strContactID) Is Nothing Then
                        radcmbContact.ClearSelection()
                        radcmbContact.Items.FindItemByValue(strContactID).Selected = True
                    End If
                ElseIf GetQueryStringVal("uihTR") <> "" Or GetQueryStringVal("fghTY") <> "" Or GetQueryStringVal("rtyWR") <> "" Or GetQueryStringVal("tyrCV") <> "" Or GetQueryStringVal("pluYR") <> "" Then

                    If GetQueryStringVal("uihTR") <> "" Then
                        objCommon.ContactID = CCommon.ToLong(GetQueryStringVal("uihTR"))
                        objCommon.charModule = "C"
                    ElseIf GetQueryStringVal("rtyWR") <> "" Then
                        objCommon.DivisionID = CCommon.ToLong(GetQueryStringVal("rtyWR"))
                        objCommon.charModule = "D"
                    ElseIf GetQueryStringVal("tyrCV") <> "" Then
                        objCommon.ProID = CCommon.ToLong(GetQueryStringVal("tyrCV"))
                        objCommon.charModule = "P"
                    ElseIf GetQueryStringVal("pluYR") <> "" Then
                        objCommon.OppID = CCommon.ToLong(GetQueryStringVal("pluYR"))
                        objCommon.charModule = "O"
                    ElseIf GetQueryStringVal("fghTY") <> "" Then
                        objCommon.CaseID = CCommon.ToLong(GetQueryStringVal("fghTY"))
                        objCommon.charModule = "S"
                    End If

                    objCommon.GetCompanySpecificValues1()
                    Dim strCompany, strContactID As String
                    strCompany = objCommon.GetCompanyName
                    strContactID = objCommon.ContactID
                    radCmbCompany.Text = strCompany
                    radCmbCompany.SelectedValue = objCommon.DivisionID

                    radCmbCompany_SelectedIndexChanged() 'Added by chintan- Reason:remaining credit wasn't showing

                    If Not radcmbContact.Items.FindItemByValue(strContactID) Is Nothing Then
                        radcmbContact.ClearSelection()
                        radcmbContact.Items.FindItemByValue(strContactID).Selected = True
                    End If
                End If

                'Show hide sections based on sales order configured
                If objSalesOrderConfiguration.SalesConfigurationID <> -1 Then
                    LoadSalesOrderConfiguration()
                End If

                'Loads payment methods
                LoadPaymentMethods()
                LoadBizDocStatus()

                txtCCNo.Attributes.Add("onkeypress", "CheckNumber(2,event)")
                txtCVV2.Attributes.Add("onkeypress", "CheckNumber(2,event)")
                txtSwipe.Attributes.Add("onchange", "swipedCreditCard()")

                If pageType = PageTypeEnum.Sales Then
                    'Check if user has right to edit unit price
                    m_aryRightsForEditUnitPrice = GetUserRightsForPage_Other(10, 31)
                    If m_aryRightsForEditUnitPrice(RIGHTSTYPE.UPDATE) = 0 Then
                        hdnEditUnitPriceRight.Value = "False"
                        txtprice.Enabled = False
                        hlProfitPerc.Enabled = False
                    Else
                        hlProfitPerc.Attributes.Add("onclick", "OpenChangeProfitWindow()")
                    End If

                    m_aryRightsForAddEditCustomerPartNo = GetUserRightsForPage_Other(10, 136)
                    If m_aryRightsForAddEditCustomerPartNo(RIGHTSTYPE.ADD) = 0 Or m_aryRightsForAddEditCustomerPartNo(RIGHTSTYPE.UPDATE) = 0 Then
                        txtCustomerPartNo.Enabled = False
                    Else
                        txtCustomerPartNo.Enabled = True
                    End If
                Else
                    hlProfitPerc.Attributes.Add("onclick", "OpenChangeProfitWindow()")
                End If

                objCommon = New CCommon
                Dim dtUnit As DataTable
                objCommon.DomainID = Session("DomainID")
                objCommon.UOMAll = True
                dtUnit = objCommon.GetItemUOM()

                With radcmbUOM
                    .DataSource = dtUnit
                    .DataTextField = "vcUnitName"
                    .DataValueField = "numUOMId"
                    .DataBind()
                    .Items.Insert(0, New RadComboBoxItem("--Select One--", "0"))
                End With
            End If

            If eventTarget = "btnVendorCostChange" Then
                CalculateProfit()

            ElseIf eventTarget = "btnBindOrderGrid" AndAlso hdnType.Value = "add" Then
                dsTemp = ViewState("SOItems")
                hdnType.Value = ""
                UpdateDetails()
                clearControls()
                objCommon = Nothing
            Else
                If ViewState("SOItems") Is Nothing Then
                    createSet()
                End If

                UpdateDataTable()

                If radcmbType.SelectedValue = "S" Then
                    txtUnits.Attributes.Add("onkeypress", "CheckNumber(1,event);TempHideButton()")
                Else
                    txtUnits.Attributes.Add("onkeypress", "CheckNumber(2,event);TempHideButton()")
                End If

                txtprice.Attributes.Add("onkeypress", "CheckNumber(3,event)")
                txtItemDiscount.Attributes.Add("onkeypress", "CheckNumber(1,event)")
                radcmbUOM.Attributes.Add("OnChange", "TempHideButton()")

                If (pageType = PageTypeEnum.AddOppertunity) Then
                    For Each column As DataGridColumn In dgItems.Columns
                        If column.HeaderText = "Sales Tax" Then
                            column.Visible = False
                        End If
                    Next

                    btnSave.Text = "Save & Open"
                Else
                    btnSave.Visible = False
                End If

                If (pageType = PageTypeEnum.Sales) AndAlso CCommon.ToBool(objSalesOrderConfiguration.bitDisplayAddToCart) Then
                    btnAddToCart.Visible = True
                End If

                If (pageType = PageTypeEnum.AddEditOrder) Then
                    btnSave.Text = "Save & Close "
                    If oppType = 2 Then
                        radPer.Checked = False
                        pnlDiscount.Visible = False
                    End If
                End If

                m_aryRightsForItems = GetUserRightsForPage_Other(10, 5)

                If m_aryRightsForItems(RIGHTSTYPE.ADD) = 0 Then btnAddItem.Visible = False
                If m_aryRightsForItems(RIGHTSTYPE.UPDATE) = 0 Then dgItems.Columns(10).Visible = False
                If m_aryRightsForItems(RIGHTSTYPE.DELETE) = 0 Then dgItems.Columns(11).Visible = False

                If (pageType = PageTypeEnum.Purchase) Or (pageType = PageTypeEnum.Sales) Then
                    m_aryRightsForAuthoritativ = GetUserRightsForPage_Other(10, 28)
                    If m_aryRightsForAuthoritativ(RIGHTSTYPE.ADD) = 0 Then
                        btnSave.Visible = False
                    End If
                End If

                'imgBtnAddShippingItem.Attributes.Add("onclick", "return ConfirmShippingItem();")
            End If

            Dim SValbuilder As New StringBuilder
            Dim strValidation As String = ""

            SValbuilder.AppendLine("function validateFixedFields() {")
            'SValbuilder.AppendLine("if(RequiredField('txtShippingRate','txtShippingRate','Please provide shipping rate!')==false) {return false;} ")
            'SValbuilder.AppendLine("if(IsNumeric('txtShippingRate','txtShippingRate','Please provide numeric value!')==false) {return false;} ")
            'SValbuilder.AppendLine("if(RequiredField('txtMarkupShippingCharge','txtMarkupShippingCharge','Please provide markup shipping charge!')==false) {return false;} ")
            'SValbuilder.AppendLine("if(IsNumeric('txtMarkupShippingCharge','txtMarkupShippingCharge','Please provide numeric value!')==false) {return false;} ")
            SValbuilder.AppendLine("if(RangeValidator('txtMarkupShippingRate','txtMarkupShippingRate','" & 0.1 & "','" & 99.99 & "','" & "Enter markup shipping rate in between 0.1 to 99.99!" & "')==false) {return false;} ")
            SValbuilder.AppendLine("return true;}")

            strValidation = SValbuilder.ToString()
            ClientScript.RegisterClientScriptBlock(Me.GetType, "FixedValidation", strValidation, True)
            ClientScript.RegisterClientScriptBlock(Me.GetType, "TotalCalculation", "CalculateTotal();", True)

            If Not String.IsNullOrEmpty(hdnBillState.Value) Then
                ddlBillCountry_SelectedIndexChanged(Nothing, Nothing)
            End If

            If Not String.IsNullOrEmpty(hdnShipState.Value) Then
                ddlShipCountry_SelectedIndexChanged(Nothing, Nothing)
            End If

            If GetQueryStringVal("bulk") = "1" Then
                btnSave.Visible = False
                btnSaveBizDoc.Visible = False
                btnAddToCart.Visible = False
                btnSaveOpenOrder.Visible = False
                btnSaveNew.Visible = False
                btnBulkSave.Visible = True
            End If
            'AddHandler dgItems.ItemCommand, AddressOf dgItems_ItemCommand

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

#End Region

#Region "Event Handlers"

    Private Sub PopulateDependentOppDropdown(ByVal sender As Object, ByVal e As EventArgs)
        Try
            objOppPageControls.PopulateDropdowns(CType(sender, RadComboBox), dtOppTableInfo, objCommon, plhOrderDetail, Session("DomainID"))
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub lnkClick_Click(sender As Object, e As System.EventArgs) Handles lnkClick.Click
        Try
            tblMultipleOrderMessage.Visible = False
            ViewState("SOItems") = Nothing
            Response.Redirect(Request.Url.ToString())
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub lkbItemRemoved_Click(sender As Object, e As EventArgs) Handles lkbItemRemoved.Click
        Try
            clearControls()
            plhItemDetails.Controls.Clear()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub lkbNewCustomer_Click(sender As Object, e As EventArgs) Handles lkbNewCustomer.Click
        Try
            ClearFinancialStamp()
            ClearExistingCustomerField()

            divNewCustomer.Visible = True
            divExistingCustomerColumn1.Visible = False
            divExistingCustomerColumn2.Visible = False

            objCommon.sb_FillComboFromDBwithSel(ddlBillCountry, 40, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlShipCountry, 40, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(radcmbRelationship, 5, Session("DomainID"))

            If Not ddlBillCountry.Items.FindByValue(Session("DefCountry")) Is Nothing Then
                ddlBillCountry.Items.FindByValue(Session("DefCountry")).Selected = True
                If ddlBillCountry.SelectedIndex > 0 Then
                    FillState(ddlBillState, ddlBillCountry.SelectedItem.Value, Session("DomainID"))
                End If
            End If
            If Not ddlShipCountry.Items.FindByValue(Session("DefCountry")) Is Nothing Then
                ddlShipCountry.Items.FindByValue(Session("DefCountry")).Selected = True
                If ddlShipCountry.SelectedIndex > 0 Then
                    FillState(ddlShipState, ddlShipCountry.SelectedItem.Value, Session("DomainID"))
                End If
            End If

            If Not radcmbRelationship.Items.FindItemByValue("46") Is Nothing Then
                radcmbRelationship.ClearSelection()
                radcmbRelationship.Items.FindItemByValue("46").Selected = True
                radcmbRelationship_SelectedIndexChanged()
            End If

            litMessage.Text = ""

            If chkTaxItems.Items.FindByValue("0") IsNot Nothing Then
                chkTaxItems.ClearSelection()
                chkTaxItems.Items.FindByValue("0").Selected = True
            End If

            txtFirstName.Focus()

            UpdatePanelFinancialStamp.Update()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub lkbItemSelected_Click(sender As Object, e As EventArgs) Handles lkbItemSelected.Click
        Try
            If hdnHasKitAsChild.Value = "1" Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "LoadChildKits", "LoadChildKits(" & CCommon.ToLong(hdnCurrentSelectedItem.Value) & ")", True)
                hdnHasKitAsChild.Value = "0"
                hdnFromItemPromoPopup.Value = "0"
            Else
                LoadSelectedItemDetail(hdnCurrentSelectedItem.Value, emptyFieldsInitially:=True)
                If divItemAttributes.Style("display") = "none" Then
                    LoadItemPromotions(CCommon.ToLong(hdnCurrentSelectedItem.Value))
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnChangedProfit_Click(sender As Object, e As EventArgs) Handles btnChangedProfit.Click
        Try
            hlProfitPerc.Text = FormatPercent(CCommon.ToDecimal(hdnChangedProfitPercent.Value / 100), 2)

            If (CCommon.ToDecimal(txtPUnitCost.Text) = 0) Then
                txtprice.Text = FormatNumber(CCommon.ToDecimal(hdnChangedProfitPercent.Value), 2)
                hlProfitPerc.Text = "100%"
            Else
                If CCommon.ToDecimal(hdnChangedProfitPercent.Value) = 100 Then
                    txtprice.Text = FormatNumber(CCommon.ToDecimal(txtPUnitCost.Text) * 1000000, 2)
                Else
                    txtprice.Text = FormatNumber((CCommon.ToDecimal(txtPUnitCost.Text) / (100 - CCommon.ToDecimal(hdnChangedProfitPercent.Value))) * 100, 2)
                End If
            End If

            lblProfit.Text = String.Format("{0:#,##0.00} / {1:#,##0.00}", (CCommon.ToDecimal(txtprice.Text) - CCommon.ToDecimal(txtPUnitCost.Text)), txtprice.Text)
            hdnUnitCost.Value = CCommon.ToDecimal(txtprice.Text)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnTemp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTemp.Click
        Try
            If (hdnBillAddressID.Value > 0) Then
                BindAddressByAddressID(CCommon.ToLong(hdnBillAddressID.Value), "Bill")
            End If
            If (CCommon.ToLong(hdnShipAddressID.Value) > 0) Then
                'hdnShipAddressID.Value = "0"
                'hdnShipVia.Value = "0"
                'hdnWillCallWarehouseID.Value = "0"

                BindAddressByAddressID(CCommon.ToLong(hdnShipAddressID.Value), "Ship")
                lblShipTo.Text = lblShipTo1.Text
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnPayment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTemp.Click
        Try

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
        Try
            Dim AnyInventoryRowSelected As Boolean
            If CCommon.ToDouble(txtUnits.Text) = 0 Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "UnitsValidation", "alert(""Enter Units""); $('.itemsArea').css('visibility', 'visible');$('.itemsArea').css('display', '');", True)
                txtUnits.Focus()
                Return
            End If
            If (lblItemType.Text = "Inventory Item") Then
                For Each itemrow As GridViewRow In gvLocations.Rows
                    If CType(itemrow.FindControl("chkLocations"), CheckBox).Checked = True Then
                        AnyInventoryRowSelected = True
                        Exit For
                    Else
                        AnyInventoryRowSelected = False
                    End If
                Next
                If (AnyInventoryRowSelected = False) Then
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "InventoryRowSelectionValidation", "alert(""Please select at least one warehouse.""); $('.itemsArea').css('visibility', 'visible');$('.itemsArea').css('display', '');", True)
                    Return
                End If

                If (chkWorkOrder.Checked = False) Then

                    Dim WareHouseItemID As Long
                    Dim userAuthGrpid As Long = CCommon.ToLong(Session("UserGroupID"))
                    Dim objItems As New CItems
                    objItems.DomainID = Session("DomainID")
                    objItems.ItemCode = hdnCurrentSelectedItem.Value

                    For Each itemrow As GridViewRow In gvLocations.Rows
                        If CType(itemrow.FindControl("chkLocations"), CheckBox).Checked = True Then
                            WareHouseItemID = CCommon.ToLong(gvLocations.DataKeys(itemrow.RowIndex)("numWareHouseItemId"))
                            Dim Quantity As Long = CCommon.ToDouble(IIf(CType(itemrow.FindControl("txtLocationsQty"), TextBox).Text Is "", txtUnits.Text, CType(itemrow.FindControl("txtLocationsQty"), TextBox).Text))

                            If CCommon.ToLong(txtHidEditOppItem.Text) > 0 Then
                                For Each drItem As DataRow In dsTemp.Tables(0).Rows
                                    If CCommon.ToLong(drItem("numWarehouseItmsID")) = WareHouseItemID Then
                                        If CCommon.ToLong(drItem("numoppitemtCode")) <> CCommon.ToLong(txtHidEditOppItem.Text) Then
                                            Quantity += CCommon.ToDouble(drItem("numUnitHour") * drItem("UOMConversionFactor"))
                                        End If
                                    End If
                                Next
                            Else
                                For Each drItem As DataRow In dsTemp.Tables(0).Rows
                                    If CCommon.ToLong(drItem("numWarehouseItmsID")) = WareHouseItemID Then
                                        Quantity += CCommon.ToDouble(drItem("numUnitHour") * drItem("UOMConversionFactor"))
                                    End If
                                Next
                            End If

                            Dim numBOPermission As Integer = objItems.CheckBackOrderPermission(userAuthGrpid, Quantity, WareHouseItemID, hdnKitChildItems.Value.ToString())
                            If (numBOPermission = 0) Then
                                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "BackOrderValidation", "alert(""Item can’t be added because it’s on back-order""); $('.itemsArea').css('visibility', 'visible');$('.itemsArea').css('display', '');", True)
                                Return
                            End If
                        End If
                    Next
                End If
            End If
            If txtprice.Text.Trim() = "" Then
                txtprice.Text = "0"
            End If
            If txtItemDiscount.Text.Trim() = "" Then
                txtItemDiscount.Text = "0"
            End If


            If CCommon.ToLong(txtHidEditOppItem.Text) > 0 Or hdnSelectedItems.Value <> "" Then
                CalculateItemSalePrice(txtprice.Text, txtUnits.Text, txtItemDiscount.Text, radPer.Checked, IIf(CCommon.ToDecimal(txtUOMConversionFactor.Text) = 0, 1, CCommon.ToDecimal(txtUOMConversionFactor.Text)), ddlMarkupDiscountOption.SelectedValue.ToString())
            End If


            If hdnExitSub.Value = "Exit" Then
                Exit Sub
            End If

            If CCommon.ToLong(txtHidEditOppItem.Text) > 0 Then
                If oppType = 1 AndAlso chkWorkOrder.Checked AndAlso CCommon.ToLong(txtUnits.Text) > CCommon.ToLong(lblWOQty.Text) Then
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "WOQtyValidation", "alert('You’ve exceeded the qty of finished product that can be assembled. Child items may be placed in backorder.')", True)
                End If

                litMessage.Text = ""
                dsTemp = ViewState("SOItems")

                If chkWorkOrder.Checked Then
                    If ChartOfAccounting.GetDefaultAccount("WP", Session("DomainID")) = 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Validation", "alert('Can not save record, your option is to set default Work in Progress Account from Administration->Global Settings->Accounting tab->Default Accounts')", True)
                        Exit Sub
                    End If
                End If

                AddItemToSession()
                UpdateDataTable()
            Else
                litMessage.Text = ""
                hdnApprovalActionTaken.Value = ""
                hdnClickedButton.Value = ""
                hdnIsUnitPriceApprovalRequired.Value = ""

                dsTemp = ViewState("SOItems")

                Dim dtitem As DataTable
                dtitem = dsTemp.Tables(2)
                dtitem.Rows.Clear()
                Dim MaxRowOrder As Long
                MaxRowOrder = dsTemp.Tables(0).Rows.Count
                Dim alert As String

                If hdnSelectedItems.Value <> "" Then
                    Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer()
                    Dim objSelectedItem As SelectedItems = serializer.Deserialize(Of SelectedItems)(hdnSelectedItems.Value)

                    If objSelectedItem.bitHasKitAsChild AndAlso String.IsNullOrEmpty(hdnKitChildItems.Value) Then
                        alert = "alert(""Child kit items are not configured. please try to add kit item again"")"
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "AccountValidation", alert, True)
                        Return
                    End If

                    Dim itemReleaseDate As String
                    Dim ShipFrom As String

                    If (lblItemType.Text = "Inventory Item") Then
                        For Each itemrow As GridViewRow In gvLocations.Rows
                            If CType(itemrow.FindControl("chkLocations"), CheckBox).Checked = True Then
                                Dim WareHouseItemID As Long = CCommon.ToLong(gvLocations.DataKeys(itemrow.RowIndex)("numWareHouseItemId"))
                                Dim Quantity As Double = CCommon.ToDouble(IIf(CType(itemrow.FindControl("txtLocationsQty"), TextBox).Text Is "", txtUnits.Text, CType(itemrow.FindControl("txtLocationsQty"), TextBox).Text))
                                itemReleaseDate = CType(itemrow.FindControl("rdLocationItemReleaseDate"), RadDatePicker).SelectedDate.Value.ToString("MM/dd/yyyy")
                                ShipFrom = itemrow.Cells(7).Text
                                Dim numShipToAddressID As String = CCommon.ToLong(CType(itemrow.FindControl("rdcmbShipToLocation"), RadComboBox).SelectedValue)
                                Dim ShipToFullAddress As String = ""
                                If (CType(itemrow.FindControl("rdcmbShipToLocation"), RadComboBox).SelectedItem IsNot Nothing And hdnDropShp.Value <> "1") Then
                                    ShipToFullAddress = CType(itemrow.FindControl("rdcmbShipToLocation"), RadComboBox).SelectedItem.Text
                                ElseIf hdnDropShp.Value = "1" Then
                                    ShipToFullAddress = lblShipTo1.Text
                                End If
                                Dim OnHandAllocation As String = CType(itemrow.FindControl("hdnOnHand"), HiddenField).Value + "/" + itemrow.Cells(2).Text

                                AddSelectedItemsToItemGrid(objSelectedItem, MaxRowOrder, WareHouseItemID, Quantity, itemReleaseDate, ShipFrom, numShipToAddressID, ShipToFullAddress, OnHandAllocation)
                                MaxRowOrder = MaxRowOrder + 1
                            End If
                        Next
                    Else
                        itemReleaseDate = GetReleaseDate()
                        ShipFrom = radcmbLocation.SelectedItem.Text

                        AddSelectedItemsToItemGrid(objSelectedItem, MaxRowOrder, CCommon.ToLong(radWareHouse.SelectedValue), CCommon.ToDouble(txtUnits.Text), itemReleaseDate, ShipFrom, 0, lblShipTo1.Text, 0)
                    End If

                    If pageType = PageTypeEnum.Sales Or pageType = PageTypeEnum.AddOppertunity Then
                        divShipping.Visible = True
                    Else
                        divShipping.Visible = False
                    End If

                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ClearSelectedItems", "$('#txtItem').select2('data', null);", True)
                    hdnSelectedItems.Value = ""
                    divShipping.Visible = True
                    radcmbLocation.Enabled = False
                    UpdatePanel1.Update()
                    plhItemDetails.Controls.Clear()
                End If

                UpdatePanelItemDetials.Update()
                UpdatePanelShipping.Update()
                UpdatePanelItems.Update()
                UpdateDataTable()

            End If

            btnUpdate.Text = ""
            btnUpdate.Style.Add("display", "none")
            btnEditCancel.Style.Add("display", "none")
            lkbEditKitSelection.Visible = False

            CheckLeadTimeNeedPO()
            clearControls()

            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideOtherTopSectionAfter1stItemAdded", "HideOtherTopSectionAfter1stItemAdded();FocuonItem();  $('.itemsArea').css('visibility', 'hidden');$('.itemsArea').css('display', 'none');", True)

            OrderPromotion()
            ShippingPromotion()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            objCommon = Nothing
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Function AddSelectedItemsToItemGrid(objSelectedItem As SelectedItems, MaxRowOrder As Long, WareHouseItemID As Long, Quantity As Double, itemReleaseDate As String, ShipFromLocation As String, numShipToAddressID As Long, ShipToFullAddress As String, OnHandAllocation As String, Optional ByVal MultiSelectItemPrice As Decimal = 0)
        Try
            Dim objItem As New CItems
            objItem.DomainID = CCommon.ToLong(Session("DomainID"))
            objItem.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
            objItem.byteMode = CCommon.ToShort(oppType)
            objItem.ItemCode = objSelectedItem.numItemCode
            objItem.WareHouseItemID = WareHouseItemID 'CCommon.ToLong(radWareHouse.SelectedValue)
            objItem.vcKitChildItems = hdnKitChildItems.Value
            objItem.GetItemAndWarehouseDetails()
            objItem.ItemDesc = txtdesc.Text
            objItem.CustomerPartNo = txtCustomerPartNo.Text
            objItem.bitHasKitAsChild = objSelectedItem.bitHasKitAsChild
            objItem.SelectedUOMID = radcmbUOM.SelectedValue
            objItem.SelectedUOM = IIf(radcmbUOM.SelectedValue > 0, radcmbUOM.Text, "")
            objItem.AttributeIDs = objSelectedItem.vcAttributeIDs
            objItem.Attributes = objSelectedItem.vcAttributes
            MaxRowOrder = MaxRowOrder + 1
            objItem.numSortOrder = MaxRowOrder
            If objItem.ItemType = "P" AndAlso IsDuplicate(objItem.ItemCode, objItem.WareHouseItemID, chkDropShip.Checked) Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "DuplicateItemWarehouse", "alert(""Inventory item with same warehouse location is already added. Please select another location."")", True)
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ClearSelectedItems", "$('#divItem').show(); $('#txtItem').select2('data', null);", True)
                Return ""
            End If

            If (objItem.numContainer > 0) Then
                Dim containerqty As Long = Math.Ceiling((CCommon.ToDouble(txtUnits.Text) * If(CCommon.ToDouble(txtUOMConversionFactor.Text) > 0, CCommon.ToDouble(txtUOMConversionFactor.Text), 1.0)) / objItem.numNoItemIntoContainer)

                Dim objContainerItem As New CItems
                objContainerItem.DomainID = CCommon.ToLong(Session("DomainID"))
                objContainerItem.WarehouseID = objItem.WarehouseID
                objContainerItem.ItemCode = objItem.numContainer
                Dim dtContainer As DataTable = objContainerItem.GetItemWarehouseLocations()

                If Not dtContainer Is Nothing AndAlso dtContainer.Rows.Count > 0 Then
                    Dim objItem1 As New CItems
                    objItem1.byteMode = CCommon.ToShort(oppType)
                    objItem1.ItemCode = objItem.numContainer
                    objItem1.WareHouseItemID = CCommon.ToLong(dtContainer.Rows(0)("numWarehouseItemID"))
                    objItem1.GetItemAndWarehouseDetails()

                    objItem1.bitHasKitAsChild = False
                    objItem1.vcKitChildItems = ""
                    MaxRowOrder = MaxRowOrder + 1
                    objItem1.numSortOrder = MaxRowOrder
                    objItem1.SelectedUOMID = objItem1.BaseUnit
                    objItem1.SelectedQuantity = containerqty
                    objItem1.AttributeIDs = ""
                    objItem1.Attributes = ""
                    objItem1.numNoItemIntoContainer = -1

                    If objItem.ItemType = "P" AndAlso IsDuplicate(objItem1.ItemCode, objItem1.WareHouseItemID, chkDropShip.Checked) Then
                        If objItem1.numNoItemIntoContainer = -1 Then
                            AddItemToSession(objItem, True, Quantity, itemReleaseDate, ShipFromLocation, numShipToAddressID, ShipToFullAddress, OnHandAllocation, MultiSelectItemPrice)
                        End If
                    Else
                        AddItemToSession(objItem, True, Quantity, itemReleaseDate, ShipFromLocation, numShipToAddressID, ShipToFullAddress, OnHandAllocation, MultiSelectItemPrice)
                        AddItemToSession(objItem1, True, CCommon.ToDouble(txtUnits.Text), itemReleaseDate, ShipFromLocation, numShipToAddressID, ShipToFullAddress, OnHandAllocation, MultiSelectItemPrice)
                    End If

                Else
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "DuplicateItemWarehouse", "alert(""Item associated container item does not contain warehouse. Please add warehouse to container first."")", True)
                    Return ""
                End If
            Else
                AddItemToSession(objItem, True, Quantity, itemReleaseDate, ShipFromLocation, numShipToAddressID, ShipToFullAddress, OnHandAllocation, MultiSelectItemPrice)
            End If
            AddSimilarRequiredRow()
            RecalculateContainerQty(dsTemp.Tables(0))
            UpdateDetails()
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub lkbExistingCustomer_Click(sender As Object, e As EventArgs) Handles lkbExistingCustomer.Click
        Try

            ClearFinancialStamp()
            ClearNewCustomerFields()

            divNewCustomer.Visible = False
            divExistingCustomerColumn1.Visible = True
            divExistingCustomerColumn2.Visible = True

            radCmbCompany.Focus()
            UpdatePanelFinancialStamp.Update()
            'ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "googleAddress", "googleAddress();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnCalculateShipping_Click(sender As Object, e As EventArgs) Handles btnCalculateShipping.Click
        Try
            litMessage.Text = ""
            Dim isItemToShipExists As Boolean
            ViewState("dtShippingMethod") = Nothing

            Dim IsAllCartItemsFreeShipping As Boolean = False
            Dim dtItems As DataTable
            If dsTemp Is Nothing Then
                dsTemp = ViewState("SOItems")
            End If

            dtItems = dsTemp.Tables(0)
            If dtItems IsNot Nothing AndAlso dtItems.Rows.Count > 0 Then

                Dim viewReleaseDates As New DataView(dtItems)
                Dim dtReleaseDates As DataTable = viewReleaseDates.ToTable(True, "ItemReleaseDate")

                If Not dtReleaseDates Is Nothing AndAlso dtReleaseDates.Rows.Count > 0 Then
                    isItemToShipExists = False

                    For Each drReleaseDate As DataRow In dtReleaseDates.Rows
                        Dim fltTotalWeight As Double = 0
                        Dim fltDimensionalWeight As Double = 0
                        Dim fltItemWeight As Double = 0
                        Dim fltItemHeight As Double = 0
                        Dim fltItemWidth As Double = 0
                        Dim fltItemLegnth As Double = 0
                        Dim dblTotalAmount As Double = 0

                        For Each drRow As DataRow In dtItems.Rows
                            If CCommon.ToString(drRow("charItemType")).ToLower() = "p" AndAlso Not CCommon.ToBool(drRow("DropShip")) Then
                                If drReleaseDate("ItemReleaseDate") = drRow("ItemReleaseDate") Then
                                    IsAllCartItemsFreeShipping = CCommon.ToBool(drRow("IsFreeShipping"))
                                    If IsAllCartItemsFreeShipping = False Then
                                        fltItemHeight = If(CCommon.ToDouble(drRow("fltHeight")) > 0, CCommon.ToDouble(drRow("fltHeight")), 1)
                                        fltItemWidth = If(CCommon.ToDouble(drRow("fltWidth")) > 0, CCommon.ToDouble(drRow("fltWidth")), 1)
                                        fltItemLegnth = If(CCommon.ToDouble(drRow("fltLength")) > 0, CCommon.ToDouble(drRow("fltLength")), 1)
                                        fltTotalWeight = fltTotalWeight + (CCommon.ToDouble(drRow("fltItemWeight")) * CCommon.ToDouble(drRow("numUnitHour")))
                                        fltDimensionalWeight = fltDimensionalWeight + ((fltItemHeight * fltItemWidth * fltItemLegnth) / 166)
                                        dblTotalAmount = dblTotalAmount + CCommon.ToDouble(drRow("monTotAmount"))

                                        isItemToShipExists = True
                                    End If
                                End If
                            End If
                        Next

                        If isItemToShipExists Then
                            Dim dblMarkupRate As Double = 1
                            If (chkMarkupShippingCharges.Checked = True) Then
                                dblMarkupRate = If(CCommon.ToDouble(txtMarkupShippingRate.Text) = 0, 1, CCommon.ToDouble(txtMarkupShippingRate.Text))
                            End If

                            If dtReleaseDates.Rows.Count = 1 Then
                                fltTotalWeight = CCommon.ToDouble(txtTotalWeight.Text)
                            End If

                            If fltTotalWeight > 0 OrElse fltDimensionalWeight > 0 Then
                                Dim intShippingCompany As Integer = CCommon.ToLong(radShipVia.SelectedValue)

                                If CCommon.ToInteger(radCmbShippingMethod.SelectedValue) >= 10 AndAlso CCommon.ToInteger(radCmbShippingMethod.SelectedValue) <= 28 Then
                                    intShippingCompany = 91
                                ElseIf CCommon.ToInteger(radCmbShippingMethod.SelectedValue) >= 40 AndAlso CCommon.ToInteger(radCmbShippingMethod.SelectedValue) <= 55 Then
                                    intShippingCompany = 88
                                ElseIf CCommon.ToInteger(radCmbShippingMethod.SelectedValue) >= 70 AndAlso CCommon.ToInteger(radCmbShippingMethod.SelectedValue) <= 76 Then
                                    intShippingCompany = 90
                                End If

                                Try
                                    GetShippingMethod(True, fltTotalWeight, dblTotalAmount, IsAllCartItemsFreeShipping, intShippingCompany, CCommon.ToInteger(radCmbShippingMethod.SelectedValue), drReleaseDate("ItemReleaseDate"), dblMarkupRate, isFlatMarkUp:=If(rblMarkupType.SelectedValue = 2, True, False))
                                Catch ex As Exception
                                    litMessage.Text = ex.Message
                                End Try
                            Else
                                litMessage.Text = "Item Weight is not available."
                            End If
                        End If
                    Next
                End If

                BindShippingGrid()
            End If

            UpdateDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnEditCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEditCancel.Click
        Try
            'If CCommon.ToLong(txtHidEditOppItem.Text) = 0 AndAlso lkbAddPromotion.Visible AndAlso lkbAddPromotion.Text.Contains("Remove") Then
            '    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "RemovePromotion", "alert('First remove applied promotion.');", True)
            '    Exit Sub
            'End If

            btnUpdate.Text = ""
            btnUpdate.Style.Add("display", "none")
            btnEditCancel.Style.Add("display", "none")
            lkbEditKitSelection.Visible = False
            divItem.Style.Add("display", "")
            clearControls()

            plhItemDetails.Controls.Clear()
            hdnCurrentSelectedItem.Value = ""
            UpdatePanelItemDetials.Update()

            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "FocusItemSelection", "FocuonItem();", True)
            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideItemSearch", "$('.itemsArea').css('visibility', 'hidden');$('.itemsArea').css('display', 'none');", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub txtunits_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtUnits.TextChanged
        Try
            If CCommon.ToLong(hdnCurrentSelectedItem.Value) > 0 Then
                ItemFieldsChanged(hdnCurrentSelectedItem.Value, If(CCommon.ToLong(txtHidEditOppItem.Text) > 0, True, False))
            End If

            If CCommon.ToLong(txtHidEditOppItem.Text) = 0 AndAlso CCommon.ToBool(hdnIsAutoWarehouseSelection.Value) AndAlso CCommon.ToLong(hdnCurrentSelectedItem.Value) > 0 Then
                If objCommon Is Nothing Then objCommon = New CCommon
                Dim dtWareHouses As DataTable = objCommon.GetWarehousesForSelectedItem(hdnCurrentSelectedItem.Value, IIf(radcmbLocation.SelectedValue = "" Or (CCommon.ToBool(hdnIsAutoWarehouseSelection.Value) AndAlso hdnWarehousePriority.Value <> "-1"), 0, CCommon.ToLong(radcmbLocation.SelectedValue)), CCommon.ToDouble(txtUnits.Text), GetOrderSource(), 1, CCommon.ToLong(hdnShipToCountry.Value), CCommon.ToLong(hdnShipToState.Value), hdnKitChildItems.Value)

                radWareHouse.DataSource = dtWareHouses
                radWareHouse.DataBind()

                gvLocations.DataSource = dtWareHouses
                gvLocations.DataBind()
            End If

            If hdnFlagRelatedItems.Value = "True" Then
                btnUpdate.Text = "Update"
                btnUpdate.Style.Remove("display")
                btnEditCancel.Style.Remove("display")
            End If

            If hdnIsCreateWO.Value = "1" Then
                txtWOQty.Text = txtUnits.Text
            ElseIf gvLocations.Rows.Count > 0 AndAlso divWorkOrder.Visible AndAlso Not gvLocations.Rows(0).FindControl("hdnOnHand") Is Nothing AndAlso hdnIsCreateWO.Value <> "1" Then
                If CCommon.ToDouble(txtUnits.Text) > CCommon.ToDouble(DirectCast(gvLocations.Rows(0).FindControl("hdnOnHand"), HiddenField).Value) Then
                    chkWorkOrder.Checked = True
                    txtWOQty.Text = CCommon.ToDouble(txtUnits.Text) - CCommon.ToDouble(DirectCast(gvLocations.Rows(0).FindControl("hdnOnHand"), HiddenField).Value)
                Else
                    chkWorkOrder.Checked = False
                    txtWOQty.Text = 0
                End If
            End If

            txtprice.Focus()

            'Setting Quantity value in 1st row of Locations grid, quantity
            If (gvLocations.Rows.Count > 0) Then
                CType(gvLocations.Rows(0).FindControl("txtLocationsQty"), TextBox).Text = txtUnits.Text
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try

    End Sub

    Private Sub txtprice_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtprice.TextChanged
        Try
            hdnUnitCost.Value = CCommon.ToDecimal(txtprice.Text)
            CalculateProfit()
            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideItemSearch", "$('.itemsArea').css('visibility', 'visible');$('.itemsArea').css('display', 'flow-root');FocusDiscount();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try

    End Sub

    Private Sub chkShipDifferent_CheckedChanged(sender As Object, e As EventArgs) Handles chkShipDifferent.CheckedChanged
        Try
            If chkShipDifferent.Checked Then
                divShippingAddress.Visible = True
                txtShipStreet.Focus()
            Else
                divShippingAddress.Visible = False
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnBindOrderGrid_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBindOrderGrid.Click
        Try
            Dim eventTarget As String = If((Me.Request("__EVENTTARGET") Is Nothing), String.Empty, Me.Request("__EVENTTARGET"))
            If eventTarget = "btnBindOrderGrid" AndAlso hdnType.Value = "add" Then
                dsTemp = ViewState("SOItems")
                hdnType.Value = ""
            End If
            UpdateDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub radcmbPriceLevel_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles radcmbPriceLevel.SelectedIndexChanged
        Try
            If radcmbPriceLevel.SelectedIndex > 0 Then
                hdnPricingBasedOn.Value = "1"
                CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(radcmbPriceLevel.SelectedValue), txtprice)
                txtItemDiscount.Text = "0"
                hdnUnitCost.Value = CCommon.ToDecimal(txtprice.Text)

                hdnItemSalePrice.Value = CCommon.ToDecimal(txtprice.Text) * CCommon.ToDecimal(txtUnits.Text)


                lkbPriceLevel.Text = "<i class=""fa fa-check""></i>&nbsp;Use Price Level"
                lkbLastPrice.Text = lkbLastPrice.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "")
                lkbPriceRule.Text = lkbPriceRule.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "")
                lkbListPrice.Text = lkbListPrice.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "")

                CalculateProfit()

                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideItemSearch", "$('.itemsArea').css('visibility', 'visible');$('.itemsArea').css('display', 'flow-root');", True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub radcmbUOM_SelectedIndexChanged(ByVal sender As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs) Handles radcmbUOM.SelectedIndexChanged
        Try
            If CCommon.ToLong(hdnCurrentSelectedItem.Value) > 0 Then
                ItemFieldsChanged(hdnCurrentSelectedItem.Value, If(CCommon.ToLong(txtHidEditOppItem.Text) > 0, True, False))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub radcmbShippingMethod_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbShippingMethod.SelectedIndexChanged
        'After changing Shipping method in dropdown there is no need to 
        'refill dropdown again .Thats why i set below given variable...
        'Me.isShippingMethodNeeded = False
        'Session("ShippingMethodValue") = ddlShippingMethod.SelectedItem.Value
        Try
            UpdateDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

#Region "Commented"

    'Private Sub radNewCustomer_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radNewCustomer.CheckedChanged
    '    Try
    '        ClearFinancialStamp()
    '        ClearExistingCustomerField()
    '        If radNewCustomer.Checked = True Then

    '            divNewCustomer.Visible = True
    '            divExistingCustomerColumn1.Visible = False
    '            divExistingCustomerColumn2.Visible = False

    '            objCommon.sb_FillComboFromDBwithSel(radcmbBillCountry, 40, Session("DomainID"))
    '            objCommon.sb_FillComboFromDBwithSel(radcmbShipCountry, 40, Session("DomainID"))
    '            objCommon.sb_FillComboFromDBwithSel(radcmbRelationship, 5, Session("DomainID"))

    '            If Not radcmbBillCountry.Items.FindItemByValue(Session("DefCountry")) Is Nothing Then
    '                radcmbBillCountry.Items.FindItemByValue(Session("DefCountry")).Selected = True
    '                If radcmbBillCountry.SelectedIndex > 0 Then FillState(radcmbBillState, radcmbBillCountry.SelectedItem.Value, Session("DomainID"))
    '            End If
    '            If Not radcmbShipCountry.Items.FindItemByValue(Session("DefCountry")) Is Nothing Then
    '                radcmbShipCountry.Items.FindItemByValue(Session("DefCountry")).Selected = True
    '                If radcmbShipCountry.SelectedIndex > 0 Then FillState(radcmbShipState, radcmbShipCountry.SelectedItem.Value, Session("DomainID"))
    '            End If


    '            If Not radcmbRelationship.Items.FindItemByValue("46") Is Nothing Then
    '                radcmbRelationship.ClearSelection()
    '                radcmbRelationship.Items.FindItemByValue("46").Selected = True
    '                radcmbRelationship_SelectedIndexChanged()
    '            End If

    '            FillAssignToDropdown(radcmbAssignedTo)

    '            litMessage.Text = ""

    '            If chkTaxItems.Items.FindByValue("0") IsNot Nothing Then
    '                chkTaxItems.ClearSelection()
    '                chkTaxItems.Items.FindByValue("0").Selected = True
    '            End If

    '            txtFirstName.Focus()
    '        End If
    '        UpdatePanelFinancialStamp.Update()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        DisplayError(CCommon.ToString(ex))
    '    End Try
    'End Sub

    'Private Sub radExistingCustomer_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radExistingCustomer.CheckedChanged
    '    Try
    '        ClearFinancialStamp()
    '        ClearNewCustomerFields()
    '        If radExistingCustomer.Checked = True Then
    '            divNewCustomer.Visible = False
    '            divExistingCustomerColumn1.Visible = True
    '            divExistingCustomerColumn2.Visible = True

    '        End If
    '        radCmbCompany.Focus()
    '        UpdatePanelFinancialStamp.Update()
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        DisplayError(CCommon.ToString(ex))
    '    End Try
    'End Sub

    'Private Sub radcmbPaymentMethods_SelectedIndexChanged(ByVal sender As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs) Handles radcmbPaymentMethods.SelectedIndexChanged
    '    Try
    '        If radcmbPaymentMethods.SelectedValue = "1" Then
    '            divCreditCard.Visible = True
    '            LoadCreditCardDetail()
    '            radcmbCards.Focus()
    '        Else
    '            divCreditCard.Visible = False
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        DisplayError(ex.ToString())
    '    End Try
    'End Sub
#End Region

    Private Sub radWareHouse_ItemDataBound(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemEventArgs) Handles radWareHouse.ItemDataBound
        Try
            e.Item.Text = CType(e.Item.DataItem, DataRowView)("vcWareHouse").ToString() & " (" & CType(e.Item.DataItem, DataRowView)("numOnHand").ToString() & ") " & " ---- " & CType(e.Item.DataItem, DataRowView)("Attr").ToString().TrimEnd(",")
            e.Item.Value = CType(e.Item.DataItem, DataRowView)("numWareHouseItemId").ToString()
            CType(e.Item.FindControl("lblOnHandAllocation"), Label).Text = CType(e.Item.DataItem, DataRowView)("numOnHand").ToString() + "/" + CType(e.Item.DataItem, DataRowView)("numAllocation").ToString()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlShipCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShipCountry.SelectedIndexChanged
        Try
            FillState(ddlShipState, ddlShipCountry.SelectedItem.Value, Session("DomainID"))

            If Not String.IsNullOrEmpty(hdnShipState.Value) AndAlso ddlShipState.SelectedItem.Text <> hdnShipState.Value Then
                If Not ddlShipState.Items.FindByText(hdnShipState.Value) Is Nothing Then
                    ddlShipState.ClearSelection()
                    ddlShipState.Items.FindByText(hdnShipState.Value).Selected = True
                End If

                hdnShipState.Value = ""
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlBillCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBillCountry.SelectedIndexChanged
        Try
            FillState(ddlBillState, ddlBillCountry.SelectedItem.Value, Session("DomainID"))

            If Not String.IsNullOrEmpty(hdnBillState.Value) AndAlso ddlBillState.SelectedItem.Text <> hdnBillState.Value Then
                If Not ddlBillState.Items.FindByText(hdnBillState.Value) Is Nothing Then
                    ddlBillState.ClearSelection()
                    ddlBillState.Items.FindByText(hdnBillState.Value).Selected = True
                End If

                hdnBillState.Value = ""
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub radcmbRelationship_SelectedIndexChanged(ByVal sender As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs) Handles radcmbRelationship.SelectedIndexChanged
        Try
            radcmbRelationship_SelectedIndexChanged()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub


    Private Sub dgItems_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgItems.ItemCommand
        Try
            dsTemp = ViewState("SOItems")
            hdnApprovalActionTaken.Value = ""
            hdnClickedButton.Value = ""
            hdnIsUnitPriceApprovalRequired.Value = ""

            If e.CommandName = "Delete" Then

                Dim dtItem As DataTable
                dtItem = dsTemp.Tables(0)

                If Not dtItem.Rows.Find(CType(e.Item.FindControl("lblOppItemCode"), Label).Text) Is Nothing Then
                    Dim decQtyShipped As Decimal = Math.Abs(CCommon.ToDecimal(CType(e.Item.FindControl("txtnumQtyShipped"), TextBox).Text))

                    If decQtyShipped > 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ValidationDelete", "alert('You are not allowed to delete this item,since item has shipped qty.')", True)
                        Exit Sub
                    End If

                    'Code to check Child Items and delete along with Parent Item - START - Neelam
                    Dim dataItem As DataGridItem = CType(e.Item, DataGridItem)
                    Dim dataKeyNumItemCode As Long = CCommon.ToLong(dgItems.DataKeys(dataItem.ItemIndex))

                    If dtRelatedItems IsNot Nothing AndAlso dtRelatedItems.Rows.Count > 0 Then
                        Dim childRows = (From dRow In dtRelatedItems.Rows
                                         Where CCommon.ToLong(dRow("numParentItemCode")) = dataKeyNumItemCode
                                         Select New With {Key .col1 = dRow("numItemCode"), .col2 = dRow("numWareHouseItemID")}).Distinct()

                        If childRows IsNot Nothing Then
                            For Each r In childRows
                                Dim _numItemCode As String = r.col1
                                Dim _numWareHouseItemID As String = r.col2

                                'Dim obj = dtItem.Select("numItemCode='" + _numItemCode + "'")
                                Dim obj = Nothing
                                'obj = dtItem.Select("numItemCode=" & _numItemCode & " AND numWarehouseItmsID=" & _numWareHouseItemID)
                                obj = dtItem.Select("numItemCode=" & _numItemCode)
                                If obj IsNot Nothing AndAlso obj.Length > 0 Then
                                    For Each q In obj
                                        Dim drr As DataRow = dtItem.Rows.Find(q.ItemArray(0))
                                        If drr IsNot Nothing Then
                                            drr.Delete()
                                        End If
                                    Next
                                End If
                            Next
                        End If
                    End If
                    'Code to check Child Items and delete along with Parent Item - END

                    dtItem.Rows.Find(CType(e.Item.FindControl("lblOppItemCode"), Label).Text).Delete()
                    RecalculateContainerQty(dtItem)
                    dtItem.AcceptChanges()

                    UseItemPromotionIfAvailable(dtItem, False, "")

                    dgItems.DataSource = dtItem
                    dgItems.DataBind()

                    If dtItem.Rows.Count = 0 Then
                        divShipping.Visible = False
                        radcmbLocation.Enabled = True
                        UpdatePanel1.Update()
                    End If
                    dtItem = dsTemp.Tables(0)
                    'dtItem.DefaultView.Sort = "vcItemName ASC"
                    dtItem = dtItem.DefaultView.ToTable()
                    dtItem.AcceptChanges()
                    Dim i As Long = 1
                    For Each dr As DataRow In dtItem.Rows
                        dr("numSortOrder") = i
                        i = i + 1
                        dr.AcceptChanges()
                    Next
                    Dim perserveChanges As Boolean = True
                    Dim msAction As System.Data.MissingSchemaAction = System.Data.MissingSchemaAction.Ignore

                    Dim changes As System.Data.DataTable = dsTemp.Tables(0).GetChanges()
                    If changes IsNot Nothing Then
                        changes.Merge(dtItem, perserveChanges, msAction)
                        dsTemp.Tables(0).Merge(changes, False, msAction)
                    Else
                        dsTemp.Tables(0).Merge(dtItem, False, msAction)
                    End If
                    litMessage.Text = ""
                    UpdateDetails()
                    clearControls()

                    plhItemDetails.Controls.Clear()
                    hdnCurrentSelectedItem.Value = ""
                    UpdatePanelItemDetials.Update()
                    UpdatePanelItems.Update()
                    If dtItem.Rows.Count <= 0 Then
                        dtRelatedItems = Nothing
                    End If

                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ClearSelectedItems", "$('#divItem').show(); $('#txtItem').select2('data', null);", True)

                    OrderPromotion()
                    ShippingPromotion()
                End If
                CheckLeadTimeNeedPO()
            ElseIf e.CommandName = "Edit" Then
                btnUpdate.Text = "Update"
                btnUpdate.Style.Remove("display")
                btnEditCancel.Style.Remove("display")
                divCustomerPart.Style.Add("display", "none")
                txtHidEditOppItem.Text = CType(e.Item.FindControl("lblOppItemCode"), Label).Text

                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideItemSearch", "$('#divItem').hide();$('.itemsArea').css('visibility', 'visible');$('.itemsArea').css('display', 'flow-root');", True)

                If Not dtOppAtributes Is Nothing Then dtOppAtributes.Rows.Clear()
                Dim dr As DataRow
                Dim dtItem As DataTable
                Dim dtSerItem As DataTable

                dtItem = dsTemp.Tables(0)
                dtSerItem = dsTemp.Tables(1)

                Dim dRows As DataRow() = dtItem.Select("numoppitemtCode = " & CCommon.ToLong(txtHidEditOppItem.Text).ToString())
                If dRows.Length > 0 Then
                    dr = dRows(0)

                    hdnCurrentSelectedItem.Value = CCommon.ToString(dr("numItemCode"))
                    hdnKitChildItems.Value = CCommon.ToString(dr("KitChildItems"))

                    If objSalesOrderConfiguration.SalesConfigurationID = -1 Or objSalesOrderConfiguration.bitDisplayItemDetails Then
                        LoadSelectedItemDetailSection(hdnCurrentSelectedItem.Value)
                    End If

                    If Not IsDBNull(dr("ItemType")) Then
                        If Not radcmbType.Items.FindItemByText(dr("ItemType")) Is Nothing Then
                            radcmbType.ClearSelection()
                            radcmbType.Items.FindItemByText(dr("ItemType")).Selected = True
                            lblItemType.Text = radcmbType.SelectedItem.Text
                        End If
                    End If

                    LoadItemDetails(hdnCurrentSelectedItem.Value)

                    If radcmbUOM.Items.FindItemByValue(dr("numUOM")) IsNot Nothing Then
                        radcmbUOM.ClearSelection()
                        radcmbUOM.Items.FindItemByValue(dr("numUOM")).Selected = True
                    End If

                    hdnVendorId.Value = CCommon.ToLong(dr("numSOVendorId"))
                    txtCustomerPartNo.Text = CCommon.ToString(dr("CustomerPartNo"))
                    If radcmbType.SelectedValue = "P" Then
                        Dim objContact As New CContacts
                        objContact.AddressType = CContacts.enmAddressType.ShipTo

                        objContact.DomainID = Session("DomainID")
                        If (radCmbCompany.SelectedValue = "") Then
                            objContact.RecordID = 0
                        Else
                            objContact.RecordID = radCmbCompany.SelectedValue
                        End If
                        objContact.AddresOf = CContacts.enmAddressOf.Organization
                        objContact.byteMode = 2
                        dtLocationsGrid = objContact.GetAddressDetail()

                        If chkPickUpWillCall.Checked = True Then
                            dtLocationsGrid.Rows.Clear()
                            Dim drA As DataRow
                            drA = dtLocationsGrid.NewRow()
                            If radcmbLocation.SelectedItem IsNot Nothing Then
                                drA("FullAddress") = radcmbLocation.SelectedItem.Attributes("Address")
                                drA("numAddressID") = radcmbLocation.SelectedItem.Attributes("numAddressID")
                                dtLocationsGrid.Rows.Add(drA)
                            End If
                        End If

                        If objCommon Is Nothing Then objCommon = New CCommon
                        Dim dtWareHouses As DataTable = objCommon.GetWarehousesForSelectedItem(hdnCurrentSelectedItem.Value, IIf(radcmbLocation.SelectedValue = "" Or (CCommon.ToBool(hdnIsAutoWarehouseSelection.Value) AndAlso hdnWarehousePriority.Value <> "-1"), 0, CCommon.ToLong(radcmbLocation.SelectedValue)), 1, GetOrderSource(), 1, CCommon.ToLong(hdnShipToCountry.Value), CCommon.ToLong(hdnShipToState.Value), hdnKitChildItems.Value)
                        radWareHouse.DataSource = dtWareHouses
                        radWareHouse.DataBind()

                        gvLocations.DataSource = dtWareHouses
                        gvLocations.DataBind()

                        If Not radWareHouse.FindItemByValue(CCommon.ToString(dr("numWarehouseItmsID"))) Is Nothing Then
                            radWareHouse.FindItemByValue(CCommon.ToString(dr("numWarehouseItmsID"))).Selected = True
                        End If

                        divWarehouse.Visible = True
                        chkDropShip.Visible = True
                        divLocations.Visible = True
                    Else
                        chkDropShip.Checked = False
                        chkDropShip.Visible = False
                        divWarehouse.Visible = False
                        radWareHouse.SelectedValue = ""
                        divLocations.Visible = False
                    End If

                    If radcmbType.SelectedValue = "S" Then
                        txtUnits.Attributes.Add("onkeypress", "CheckNumber(1,event);TempHideButton()")
                    Else
                        txtUnits.Attributes.Add("onkeypress", "CheckNumber(2,event);TempHideButton()")
                    End If

                    isShippingMethodNeeded = False

                    chkDropShip.Checked = CCommon.ToBool(dr("DropShip"))
                    If chkDropShip.Checked Then
                        divWarehouse.Visible = False
                        divLocations.Visible = False
                    End If

                    hdnIsCreateWO.Value = If(CCommon.ToBool(dr("bitAlwaysCreateWO")), "1", "0")
                    txtWOQty.Text = CCommon.ToDouble(dr("numWOQty"))
                    chkWorkOrder.Checked = CCommon.ToBool(dr("bitWorkOrder"))
                    lblWOQty.Text = CCommon.ToString(dr("numMaxWorkOrderQty"))
                    txtPUnitCost.Text = CCommon.ToDouble(dr("numCost"))

                    If chkWorkOrder.Checked Then
                        divWorkOrder.Visible = True

                        txtInstruction.Text = CCommon.ToString(dr("vcInstruction"))
                        If Not String.IsNullOrEmpty(CCommon.ToString(dr("dtPlannedStart"))) Then
                            rdpPlannedStart.SelectedDate = CCommon.ToSqlDate(dr("dtPlannedStart"))
                        End If
                        If Not String.IsNullOrEmpty(CCommon.ToString(dr("bintCompliationDate"))) Then
                            radCalCompliationDate.SelectedDate = CCommon.ToSqlDate(dr("bintCompliationDate"))
                        End If
                    End If

                    Dim i, k As Integer

                    txtprice.Text = CCommon.ToDecimal(dr("monPrice"))
                    hdnUnitCost.Value = CCommon.ToDecimal(dr("monPrice"))
                    hdnItemSalePrice.Value = CCommon.ToDecimal(dr("monSalePrice"))

                    If radcmbType.SelectedValue = "S" Then
                        txtUnits.Text = CCommon.ToDouble(dr("numUnitHour"))
                        txtUnits.Attributes.Add("onkeypress", "CheckNumber(1,event);TempHideButton()")
                    Else
                        txtUnits.Text = CCommon.ToDouble(dr("numUnitHour"))
                        txtUnits.Attributes.Add("onkeypress", "CheckNumber(2,event);TempHideButton()")
                    End If

                    If pageType = PageTypeEnum.AddEditOrder Then
                        txtUnits.Attributes.Add("onkeyup", "return validateQty('" & txtUnits.ClientID & "'," & oppType & ");")
                        radcmbUOM.Enabled = False
                    End If

                    txtOldUnits.Text = txtUnits.Text
                    txtnumUnitHourReceived.Text = CCommon.ToInteger(dr("numUnitHourReceived"))
                    txtnumQtyShipped.Text = CCommon.ToInteger(dr("numQtyShipped"))
                    txtUOMConversionFactor.Text = dr("UOMConversionFactor")
                    If dr("vcBaseUOMName") IsNot Nothing AndAlso dr("vcBaseUOMName") IsNot DBNull.Value Then
                        lblBaseUOMName.Text = dr("vcBaseUOMName")
                    End If
                    If oppType = 1 Then
                        txtItemDiscount.Text = CCommon.ToDecimal(dr("fltDiscount"))
                        If CCommon.ToBool(dr("bitPromotionDiscount")) Then
                            chkUsePromotion.Checked = Not CCommon.ToBool(dr("bitDisablePromotion"))
                        Else
                            chkUsePromotion.Checked = False
                        End If

                        If dr("bitDiscountType") = True Then
                            radAmt.Checked = True
                            radPer.Checked = False
                        Else
                            radPer.Checked = True
                            radAmt.Checked = False
                        End If

                        If (dr("bitMarkupDiscount").ToString() = "1") Then
                            ddlMarkupDiscountOption.SelectedValue = "1"
                        Else
                            ddlMarkupDiscountOption.SelectedValue = "0"
                        End If
                    End If

                    If radcmbUOM.Items.FindItemByValue(dr("numUOM")) IsNot Nothing Then
                        radcmbUOM.ClearSelection()

                        radcmbUOM.Items.FindItemByValue(dr("numUOM")).Selected = True
                    End If

                    If (radWareHouse.Items.Count > 0) Then
                        If Not IsDBNull(dr("numWarehouseItmsID")) Then
                            If Not radWareHouse.FindItemByValue(dr("numWarehouseItmsID")) Is Nothing Then
                                radWareHouse.FindItemByValue(dr("numWarehouseItmsID")).Selected = True
                            End If
                        End If
                    End If


                    If IIf(IsDBNull(dr("DropShip")), "", dr("DropShip")) = "True" Then
                        chkDropShip.Checked = True
                        radWareHouse.Visible = False
                    Else
                        chkDropShip.Checked = False
                        radWareHouse.Visible = True
                    End If
                    If dr("numItemClassification") IsNot Nothing AndAlso dr("numItemClassification") IsNot DBNull.Value Then
                        hdnItemClassification.Value = dr("numItemClassification")
                    Else
                        hdnItemClassification.Value = 0
                    End If

                    If CCommon.ToLong(hdnCurrentSelectedItem.Value) > 0 Then
                        ItemFieldsChanged(hdnCurrentSelectedItem.Value, True)
                    End If
                    BindPriceLevel(hdnCurrentSelectedItem.Value)
                    CalculateProfit()

                    Dim objItem As New CItems
                    objItem.ItemCode = CCommon.ToLong(hdnCurrentSelectedItem.Value)
                    objItem.ChildItemID = 0
                    Dim dsItem As DataSet = objItem.GetSelectedChildItemDetails(CCommon.ToString(dr("KitChildItems")), False)

                    If Not dsItem Is Nothing And dsItem.Tables.Count > 1 Then
                        If CCommon.ToBool(dsItem.Tables(0).Rows(0)("bitKit")) AndAlso Not CCommon.ToBool(dsItem.Tables(0).Rows(0)("bitHasChildKits")) Then
                            divPricingOption.Visible = True
                            lkbPriceLevel.Visible = False
                            lkbPriceRule.Visible = False
                            lkbLastPrice.Visible = False
                            lkbListPrice.Visible = False

                            gvChildItems.DataSource = Nothing
                            gvChildItems.DataBind()

                            UpdatePanel3.Update()

                            lkbEditKitSelection.Visible = True

                            If CCommon.ToBool(dsItem.Tables(0).Rows(0)("bitCalAmtBasedonDepItems")) Then
                                lblKitPrice.Text = CCommon.ToDouble(dsItem.Tables(0).Rows(0)("monPrice"))
                                hdnKitPricing.Value = CCommon.ToShort(dsItem.Tables(0).Rows(0)("tintKitAssemblyPriceBasedOn"))
                                hdnKitListPrice.Value = CCommon.ToDouble(dsItem.Tables(0).Rows(0)("monListPrice"))

                                divKitPrice.Visible = True
                                If CCommon.ToShort(dsItem.Tables(0).Rows(0)("tintKitAssemblyPriceBasedOn")) = 4 Then
                                    lblKitPriceType.Text = "Kit/Assembly list price plus sum total of child item's list price"
                                ElseIf CCommon.ToShort(dsItem.Tables(0).Rows(0)("tintKitAssemblyPriceBasedOn")) = 3 Then
                                    lblKitPriceType.Text = "Sum total of child item's Primary Vendor Cost"
                                ElseIf CCommon.ToShort(dsItem.Tables(0).Rows(0)("tintKitAssemblyPriceBasedOn")) = 2 Then
                                    lblKitPriceType.Text = "Sum total of child item's Average Cost"
                                Else
                                    lblKitPriceType.Text = "Sum total of child item's List Price"
                                End If
                            Else
                                divKitPrice.Visible = False
                                lblKitPrice.Text = ""
                                hdnKitPricing.Value = ""
                                hdnKitListPrice.Value = ""
                            End If

                            gvChildItems.DataSource = dsItem.Tables(1)
                            gvChildItems.DataBind()

                            UpdatePanel3.Update()
                        Else
                            lkbEditKitSelection.Visible = False
                            divKitPrice.Visible = False
                            lblKitPrice.Text = ""
                            hdnKitPricing.Value = ""
                            hdnKitListPrice.Value = ""
                        End If
                    End If
                End If
                'UpdateDetails()
                radWareHouse.Focus()
            ElseIf e.CommandName = "ShowPromotionStatus" Then
                Dim oppItemCode As Long = CType(e.Item.FindControl("lblOppItemCode"), Label).Text
                Dim dRows As DataRow() = dsTemp.Tables(0).Select("numoppitemtCode = " & oppItemCode)

                If dRows.Length > 0 Then
                    Dim dr As DataRow = dRows(0)
                    Dim promotionID As Long = CCommon.ToLong(e.CommandArgument)

                    hdnIPSPromotionID.Value = promotionID
                    hdnIPSItemID.Value = CCommon.ToLong(dr("numItemCode"))
                    hdnIPSWarehouseItemID.Value = CCommon.ToLong(dr("numWarehouseItmsID"))
                    hdnIPSOppItemID.Value = oppItemCode

                    Dim objPromotionOffer As New PromotionOffer
                    objPromotionOffer.DomainID = Session("DomainID")
                    objPromotionOffer.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                    objPromotionOffer.PromotionID = promotionID

                    If CType(e.Item.FindControl("lnkItemPromoDesc"), LinkButton).Enabled AndAlso CType(e.Item.FindControl("lnkItemPromoDesc"), LinkButton).ForeColor = System.Drawing.Color.Green Then
                        divPromotionDiscountItems.Visible = True

                        bizPager.PageSize = 20
                        bizPager.CurrentPageIndex = 1

                        Dim dt As DataTable = objPromotionOffer.GetItemPromotionDiscountItems(CCommon.ToLong(radCmbCompany.SelectedValue), CCommon.ToLong(dr("numItemCode")), CCommon.ToLong(dr("numWarehouseItmsID")), bizPager.CurrentPageIndex, bizPager.PageSize)

                        If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                            bizPager.RecordCount = CCommon.ToInteger(dt.Rows(0)("TotalRecords"))

                            gvItemPromotionStatus.DataSource = dt
                            gvItemPromotionStatus.DataBind()
                        Else
                            bizPager.RecordCount = 0
                            bizPager.CurrentPageIndex = 0
                        End If

                        gvItemPromotionStatus.Visible = True
                        lblItemsWithLessPrice.Visible = False
                        bizPager.Visible = True
                    Else
                        Dim dsItemProm As DataTable = objPromotionOffer.GetItemPromotionDetails(CCommon.ToLong(dr("numItemCode")), CCommon.ToDouble(dr("numUnitHour")) * CCommon.ToDouble(dr("UOMConversionFactor")), CCommon.ToDouble(dr("monTotAmount")))
                        If Not dsItemProm Is Nothing AndAlso dsItemProm.Rows.Count > 0 Then
                            lblPromotionDescription.Text = CCommon.ToString(dsItemProm.Rows(0)("vcLongDesc"))
                            lblPromotionStatus.Text = CCommon.ToString(dsItemProm.Rows(0)("vcPromotionStatus"))
                            hdnIsCouponCodeRequired.Value = CCommon.ToBool(dsItemProm.Rows(0)("bitRequireCouponCode"))

                            If CCommon.ToBool(dsItemProm.Rows(0)("canUsePromotion")) Then
                                divPromotionDiscountItems.Visible = True

                                If CCommon.ToShort(dsItemProm.Rows(0)("tintOfferBasedOn")) = 4 Then
                                    gvItemPromotionStatus.Visible = False
                                    lblItemsWithLessPrice.Visible = True
                                    bizPager.Visible = False
                                Else
                                    bizPager.PageSize = 20
                                    bizPager.CurrentPageIndex = 1

                                    Dim dt As DataTable = objPromotionOffer.GetItemPromotionDiscountItems(CCommon.ToLong(radCmbCompany.SelectedValue), CCommon.ToLong(dr("numItemCode")), CCommon.ToLong(dr("numWarehouseItmsID")), bizPager.CurrentPageIndex, bizPager.PageSize)

                                    If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                                        bizPager.RecordCount = CCommon.ToInteger(dt.Rows(0)("TotalRecords"))

                                        gvItemPromotionStatus.DataSource = dt
                                        gvItemPromotionStatus.DataBind()
                                    Else
                                        bizPager.RecordCount = 0
                                        bizPager.CurrentPageIndex = 0
                                    End If

                                    gvItemPromotionStatus.Visible = True
                                    lblItemsWithLessPrice.Visible = False
                                    bizPager.Visible = True
                                End If

                            Else
                                divPromotionDiscountItems.Visible = False
                            End If
                        End If
                    End If



                    divItemPromotionStatus.Visible = True

                    uplItemPromotionStatus.Update()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub dgItems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgItems.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                If CCommon.ToLong(Session("UserContactID")) > 0 AndAlso CCommon.ToLong(Session("UserID")) = 0 Then
                    CType(e.Item.FindControl("txtUnitPrice"), TextBox).ReadOnly = True
                End If

                Dim dataRowView As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

                If CCommon.ToBool(hdnEnabledItemLevelUOM.Value) Then
                    Dim monPrice = If(dataRowView("monPrice") Is DBNull.Value, 0, dataRowView("monPrice"))
                    Dim UOMConversionFactor = If(dataRowView("UOMConversionFactor") Is DBNull.Value, 0, dataRowView("UOMConversionFactor"))
                    Dim unitPrice = monPrice * UOMConversionFactor
                    Dim monSalePrice = If(dataRowView("monSalePrice") Is DBNull.Value, 0, dataRowView("monSalePrice"))

                    CType(e.Item.FindControl("txtUnitPrice"), TextBox).Text = CCommon.GetDecimalFormat(Math.Round(unitPrice, 5))
                    Dim unitSalePrice = monSalePrice * UOMConversionFactor

                    CType(e.Item.FindControl("lblUnitSalePrice"), Label).Text = CCommon.GetDecimalFormat(Math.Round(unitSalePrice, 5))
                    CType(e.Item.FindControl("txtUnitSalePrice"), TextBox).Text = CCommon.GetDecimalFormat(monSalePrice * UOMConversionFactor)

                    CType(e.Item.FindControl("lblUOMName"), Label).Visible = False
                    CType(e.Item.FindControl("lblSaleUOMName"), Label).Visible = False
                    CType(e.Item.FindControl("lblItemUOM"), Label).Visible = True
                Else
                    Dim monPrice = If(dataRowView("monPrice") Is DBNull.Value, 0, dataRowView("monPrice"))
                    Dim unitPrice = monPrice

                    CType(e.Item.FindControl("txtUnitPrice"), TextBox).Text = CCommon.GetDecimalFormat(Math.Round(unitPrice, 5))

                    Dim monSalePrice = If(dataRowView("monSalePrice") Is DBNull.Value, 0, dataRowView("monSalePrice"))
                    Dim unitSalePrice = monSalePrice

                    CType(e.Item.FindControl("lblUnitSalePrice"), Label).Text = CCommon.GetDecimalFormat(Math.Round(unitSalePrice, 5))
                    CType(e.Item.FindControl("txtUnitSalePrice"), TextBox).Text = CCommon.GetDecimalFormat(monSalePrice)

                    CType(e.Item.FindControl("lblUOMName"), Label).Visible = True
                    CType(e.Item.FindControl("lblItemUOM"), Label).Visible = False
                End If

                CType(e.Item.FindControl("hdnUnitSalePrice"), HiddenField).Value = CCommon.ToDouble(If(dataRowView("monSalePrice") Is DBNull.Value, 0, dataRowView("monSalePrice"))).ToString("R")

                Dim txtUnits As TextBox = CType(e.Item.FindControl("txtUnits"), TextBox)
                txtUnits.Attributes.Add("OppItemCode", CType(e.Item.FindControl("lblOppItemCode"), Label).Text)

                If hdnEditUnitPriceRight.Value = "False" Then
                    CType(e.Item.FindControl("txtUnitPrice"), TextBox).Enabled = False
                    CType(e.Item.FindControl("txtUnitSalePrice"), TextBox).Enabled = False
                End If

                If DataBinder.Eval(e.Item.DataItem, "charItemType") = "S" Then
                    If CCommon.ToLong(CType(e.Item.FindControl("lblnumProjectID"), Label).Text) > 0 AndAlso pageType = PageTypeEnum.AddEditOrder AndAlso oppType = 1 Then
                        CType(e.Item.FindControl("txtUnits"), TextBox).Enabled = False
                        CType(e.Item.FindControl("txtUnitPrice"), TextBox).Enabled = False
                        CType(e.Item.FindControl("txtUnitSalePrice"), TextBox).Enabled = False

                        CType(e.Item.FindControl("lnkEdit"), LinkButton).Attributes.Add("onclick", "return NotAllowMessage(2)")
                        CType(e.Item.FindControl("lnkDelete"), LinkButton).Attributes.Add("onclick", "return NotAllowMessage(2)")
                    End If

                    CType(e.Item.FindControl("txtUnits"), TextBox).Attributes.Add("onkeypress", "CheckNumber(1,event)")
                Else
                    CType(e.Item.FindControl("txtUnits"), TextBox).Attributes.Add("onkeypress", "CheckNumber(2,event)")
                End If

                If pageType = PageTypeEnum.AddEditOrder Then
                    CType(e.Item.FindControl("txtUnits"), TextBox).Attributes.Add("onkeyup", "return validateQty('" & CType(e.Item.FindControl("txtUnits"), TextBox).ClientID & "'," & oppType & ");")
                End If

                CType(e.Item.FindControl("txtUnitPrice"), TextBox).Attributes.Add("onkeyup", "CalculateTotal()")
                CType(e.Item.FindControl("txtUnitPrice"), TextBox).Attributes.Add("onkeypress", "CheckNumber(3,event)")

                CType(e.Item.FindControl("txtUnitSalePrice"), TextBox).Attributes.Add("onkeyup", "UnitSalePriceChanged(this)")
                CType(e.Item.FindControl("txtUnitSalePrice"), TextBox).Attributes.Add("onkeypress", "CheckNumber(3,event)")

                If (oppType = 1) Then
                    If (pageType = PageTypeEnum.AddEditOrder) Then
                        If (CCommon.ToBool(DataBinder.Eval(e.Item.DataItem, "bitWorkOrder")) = True) Then
                            CType(e.Item.FindControl("lnkEdit"), LinkButton).Attributes.Add("onclick", "return NotAllowMessage(3)")
                            CType(e.Item.FindControl("lnkDelete"), LinkButton).Attributes.Add("onclick", "return NotAllowMessage(3)")
                            CType(e.Item.FindControl("txtUnits"), TextBox).Enabled = False
                        End If
                    End If

                    If (CCommon.ToBool(DataBinder.Eval(e.Item.DataItem, "bitDiscountType")) = True) Then
                        CType(e.Item.FindControl("lblDiscount"), Label).Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(DataBinder.Eval(e.Item.DataItem, "fltDiscount")))
                        CType(e.Item.FindControl("hdnDiscountAmount"), HiddenField).Value = CCommon.ToDecimal(DataBinder.Eval(e.Item.DataItem, "fltDiscount"))
                    Else
                        If CCommon.ToDouble(DataBinder.Eval(e.Item.DataItem, "monTotAmount")) > CCommon.ToDecimal(DataBinder.Eval(e.Item.DataItem, "monTotAmtBefDiscount")) Then
                            CType(e.Item.FindControl("lblDiscount"), Label).Text = "0 (0%)"
                            CType(e.Item.FindControl("hdnDiscountAmount"), HiddenField).Value = "0"
                        Else
                            CType(e.Item.FindControl("lblDiscount"), Label).Text = CCommon.GetDecimalFormat(Math.Truncate(10000 * (CCommon.ToDecimal(DataBinder.Eval(e.Item.DataItem, "monTotAmtBefDiscount")) - CCommon.ToDecimal(DataBinder.Eval(e.Item.DataItem, "monTotAmount")))) / 10000) & " (" & CCommon.ToDecimal(DataBinder.Eval(e.Item.DataItem, "fltDiscount")) & "%)"
                            CType(e.Item.FindControl("hdnDiscountAmount"), HiddenField).Value = CCommon.ToDouble(DataBinder.Eval(e.Item.DataItem, "monTotAmtBefDiscount")) - CCommon.ToDecimal(DataBinder.Eval(e.Item.DataItem, "monTotAmount"))
                        End If
                    End If
                    CType(e.Item.FindControl("hdnMarkupDiscount"), HiddenField).Value = ddlMarkupDiscountOption.SelectedValue
                End If

                'If Auth BizDocs associated with item then do not allow edit or delete
                If (CCommon.ToBool(DataBinder.Eval(e.Item.DataItem, "bitIsAuthBizDoc")) = True) Then
                    CType(e.Item.FindControl("txtUnits"), TextBox).Enabled = False
                    CType(e.Item.FindControl("txtUnitPrice"), TextBox).Enabled = False
                    CType(e.Item.FindControl("txtUnitSalePrice"), TextBox).Enabled = False
                    CType(e.Item.FindControl("lnkEdit"), LinkButton).Attributes.Add("onclick", "return NotAllowMessage(1)")
                    CType(e.Item.FindControl("lnkDelete"), LinkButton).Attributes.Add("onclick", "return NotAllowMessage(1)")
                End If

                Dim numItemCode As Long = CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numItemCode"))

                If CCommon.ToLong(dataRowView("numPromotionID")) > 0 Then
                    CType(e.Item.FindControl("ulItemPromo"), HtmlGenericControl).Visible = True
                    CType(e.Item.FindControl("lnkItemPromoDesc"), LinkButton).ForeColor = System.Drawing.Color.Green

                    If CCommon.ToBool(dataRowView("bitPromotionTriggered")) Then
                        CType(e.Item.FindControl("lnkItemPromoDesc"), LinkButton).ForeColor = System.Drawing.Color.Green
                        CType(e.Item.FindControl("lnkItemPromoDesc"), LinkButton).CommandName = "ShowPromotionStatus"
                        CType(e.Item.FindControl("lnkItemPromoDesc"), LinkButton).CommandArgument = CCommon.ToLong(dataRowView("numPromotionID"))
                        CType(e.Item.FindControl("lnkItemPromoDesc"), LinkButton).Text = CCommon.ToString(dataRowView("vcPromotionDetail"))
                        CType(e.Item.FindControl("lnkItemPromoDesc"), LinkButton).Enabled = True
                    Else
                        CType(e.Item.FindControl("lnkItemPromoDesc"), LinkButton).CommandName = ""
                        CType(e.Item.FindControl("lnkItemPromoDesc"), LinkButton).CommandArgument = ""
                        CType(e.Item.FindControl("lnkItemPromoDesc"), LinkButton).Text = CCommon.ToString(dataRowView("vcPromotionDetail"))
                        CType(e.Item.FindControl("lnkItemPromoDesc"), LinkButton).Enabled = False
                    End If
                Else
                    Dim dtItemProm As DataTable = ItemPromotion(numItemCode, CCommon.ToLong(dataRowView("numSelectedPromotionID")), CCommon.ToDouble(dataRowView("numUnitHour")) * CCommon.ToDouble(dataRowView("UOMConversionFactor")), CCommon.ToDouble(dataRowView("monTotAmount")))
                    If Not dtItemProm Is Nothing AndAlso dtItemProm.Rows.Count > 0 Then
                        If Not CCommon.ToBool(dtItemProm.Rows(0)("bitRequireCouponCode")) Then
                            CType(e.Item.FindControl("ulItemPromo"), HtmlGenericControl).Visible = True
                            CType(e.Item.FindControl("lnkItemPromoDesc"), LinkButton).CommandName = "ShowPromotionStatus"
                            CType(e.Item.FindControl("lnkItemPromoDesc"), LinkButton).CommandArgument = CCommon.ToLong(dtItemProm.Rows(0)("numProId"))
                            CType(e.Item.FindControl("lnkItemPromoDesc"), LinkButton).Text = CCommon.ToString(dtItemProm.Rows(0)("vcShortDesc"))

                            If CCommon.ToBool(dtItemProm.Rows(0)("canUsePromotion")) Then
                                CType(e.Item.FindControl("lnkItemPromoDesc"), LinkButton).ForeColor = System.Drawing.Color.Green

                                If dsTemp.Tables(0).Select("numoppitemtCode=" & CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numoppitemtCode"))).Length > 0 Then
                                    If dsTemp.Tables(0).Select("bitDisablePromotion=false AND numoppitemtCode=" & CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numoppitemtCode"))).Length > 0 Then
                                        Dim drItem As DataRow = dsTemp.Tables(0).Select("bitDisablePromotion=false AND numoppitemtCode=" & CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numoppitemtCode")))(0)

                                        drItem("numPromotionID") = CCommon.ToLong(dtItemProm.Rows(0)("numProId"))
                                        drItem("bitPromotionTriggered") = True
                                        drItem("vcPromotionDetail") = CCommon.ToString(dtItemProm.Rows(0)("vcShortDesc"))
                                    End If
                                End If
                            Else
                                CType(e.Item.FindControl("lnkItemPromoDesc"), LinkButton).ForeColor = System.Drawing.Color.Red
                            End If
                        Else
                            CType(e.Item.FindControl("lnkItemPromoDesc"), LinkButton).ForeColor = System.Drawing.Color.Red
                            CType(e.Item.FindControl("lnkItemPromoDesc"), LinkButton).Text = CCommon.ToString(dtItemProm.Rows(0)("vcShortDesc"))
                            CType(e.Item.FindControl("lnkItemPromoDesc"), LinkButton).CommandName = "ShowPromotionStatus"
                            CType(e.Item.FindControl("lnkItemPromoDesc"), LinkButton).CommandArgument = CCommon.ToLong(dtItemProm.Rows(0)("numProId"))
                            CType(e.Item.FindControl("ulItemPromo"), HtmlGenericControl).Visible = True
                        End If
                    Else
                        CType(e.Item.FindControl("lnkItemPromoDesc"), LinkButton).Text = ""
                        CType(e.Item.FindControl("ulItemPromo"), HtmlGenericControl).Visible = False
                    End If
                End If
            ElseIf e.Item.ItemType = ListItemType.Header Then
                CType(e.Item.FindControl("lblUnitPriceCaption"), Label).Text = IIf(oppType = 1, "Unit List Price", "Unit Cost")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub txtUnitsGrid_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim dr As DataRow
            Dim dtItem As DataTable
            Dim dtSerItem As DataTable
            Dim txtUnits As TextBox = CType(sender, TextBox)

            dtItem = dsTemp.Tables(0)
            dtSerItem = dsTemp.Tables(1)

            Dim dRows As DataRow() = dtItem.Select("numoppitemtCode = " & CCommon.ToLong(txtUnits.Attributes("OppItemCode")).ToString())
            If dRows.Length > 0 Then
                dr = dRows(0)

                Dim price As Decimal
                Dim discount As Decimal
                Dim onHand As Double
                Dim bitKitParent As Boolean

                If CCommon.ToLong(dr("numPromotionID")) > 0 AndAlso Not CCommon.ToBool(dr("bitPromotionTriggered")) Then
                    price = CCommon.ToDecimal(dr("monPrice"))
                    discount = CCommon.ToDecimal(dr("fltDiscount"))
                Else
                    Dim objOpportunity As New COpportunities
                    objOpportunity.DomainID = Session("DomainID")
                    objOpportunity.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                    objOpportunity.OppType = 1
                    objOpportunity.ItemCode = CCommon.ToLong(dr("numItemCode"))
                    objOpportunity.UnitHour = CCommon.ToDouble(txtUnits.Text)
                    objOpportunity.WarehouseItmsID = CCommon.ToLong(dr("numWarehouseItmsID"))
                    objOpportunity.CurrencyID = CCommon.ToLong(radcmbCurrency.SelectedValue)
                    Dim ds As DataSet = objOpportunity.GetOrderItemDetails(CCommon.ToLong(dr("numUOM")), CCommon.ToString(dr("KitChildItems")), CCommon.ToLong(hdnCountry.Value))

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                        price = CCommon.ToDecimal(ds.Tables(0).Rows(0)("monPrice"))
                        discount = CCommon.ToDecimal(ds.Tables(0).Rows(0)("Discount"))
                        onHand = CCommon.ToDouble(ds.Tables(0).Rows(0)("numOnHand"))
                        bitKitParent = CCommon.ToBool(ds.Tables(0).Rows(0)("bitKitParent"))

                        If price = 0 AndAlso dr("monPrice") <> 0 Then
                            price = CCommon.ToDecimal(dr("monPrice"))
                        End If
                        If discount = 0 AndAlso dr("fltDiscount") <> 0 Then
                            discount = CCommon.ToDecimal(dr("fltDiscount"))
                        End If

                        If CCommon.ToString(ds.Tables(0).Rows(0)("DiscountType")) = "1" Then
                            dr("bitDiscountType") = 0
                        Else
                            dr("bitDiscountType") = 1
                        End If

                        If CCommon.ToBool(ds.Tables(0).Rows(0)("bitMarkupDiscount")) Then
                            dr("bitMarkupDiscount") = "1"
                        Else
                            dr("bitMarkupDiscount") = "0"
                        End If
                    End If
                End If

                Dim salePrice As Decimal = price

                dr("monPrice") = price
                dr("fltDiscount") = discount
                dr("monTotAmtBefDiscount") = ((CCommon.ToInteger(txtUnits.Text) * CCommon.ToDecimal(dr("UOMConversionFactor"))) * dr("monPrice"))

                If CCommon.ToString(dr("bitDiscountType")) = "1" Then
                    If CCommon.ToInteger(txtUnits.Text) > 0 Then
                        If (dr("bitMarkupDiscount") = "1") Then  'MarkUp N Discount Logic
                            salePrice = ((price * (CCommon.ToInteger(txtUnits.Text) * dr("UOMConversionFactor"))) + discount) / (CCommon.ToInteger(txtUnits.Text) * dr("UOMConversionFactor"))
                        Else
                            salePrice = ((price * (CCommon.ToInteger(txtUnits.Text) * dr("UOMConversionFactor"))) - discount) / (CCommon.ToInteger(txtUnits.Text) * dr("UOMConversionFactor"))
                        End If
                    Else
                        salePrice = 0
                    End If
                Else
                    If (dr("bitMarkupDiscount") = "1") Then  'MarkUp N Discount Logic
                        salePrice = price + (price * (discount / 100))
                    Else
                        salePrice = price - (price * (discount / 100))
                    End If
                End If

                dr("monSalePrice") = Math.Truncate(10000 * salePrice) / 10000
                dr("monTotAmount") = (dr("numUnitHour") * dr("UOMConversionFactor")) * salePrice

                If dtItem.Columns.Contains("TotalDiscountAmount") Then
                    If Not CCommon.ToString(dr("bitDiscountType")) = "1" Then
                        If dr("monTotAmount") > dr("monTotAmtBefDiscount") Then
                            dr("TotalDiscountAmount") = 0
                        Else
                            dr("TotalDiscountAmount") = dr("monTotAmtBefDiscount") - dr("monTotAmount")
                        End If
                    Else
                        dr("TotalDiscountAmount") = dr("fltDiscount")
                    End If
                End If

                If Not CCommon.ToBool(dr("DropShip")) AndAlso Not bitKitParent AndAlso CCommon.ToBool(hdnIsAutoWarehouseSelection.Value) AndAlso hdnWarehousePriority.Value <> "-1" AndAlso (CCommon.ToDouble(txtUnits.Text) - CCommon.ToDouble(dr("numWOQty"))) > onHand Then
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "WarehouseMapping", "alert('Current selected warehouse for item doesn't have enough available quantity. Please edit item and select appropriate warehouse or you have to manually change warehouse in sales fulfillment.');", True)
                End If


                dr.AcceptChanges()
                dsTemp.AcceptChanges()

                UpdateDetails()

                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideOtherTopSectionAfter1stItemAdded", "HideOtherTopSectionAfter1stItemAdded();  $('.itemsArea').css('visibility', 'hidden');$('.itemsArea').css('display', 'none');$('#txtItem').select2('data', null);", True)
                clearControls()

                OrderPromotion()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub txtRowOrder_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim dr As DataRow
            Dim dtItem As DataTable
            Dim dtSerItem As DataTable
            Dim txtRowOrder As TextBox = CType(sender, TextBox)
            Dim row As DataGridItem = TryCast(sender.NamingContainer, DataGridItem)
            dtItem = dsTemp.Tables(0)
            dtSerItem = dsTemp.Tables(1)
            For Each dgGridItem As DataGridItem In dgItems.Items
                Dim dRows As DataRow() = dtItem.Select("numoppitemtCode = " & CCommon.ToLong(CType(dgGridItem.FindControl("lblOppItemCode"), Label).Text).ToString())
                txtRowOrder = CType(dgGridItem.FindControl("txtRowOrder"), TextBox)
                If dRows.Length > 0 Then
                    dr = dRows(0)
                    dr("numSortOrder") = txtRowOrder.Text
                    dr.AcceptChanges()
                    dsTemp.AcceptChanges()

                End If
            Next
            UpdateDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub txtPUnitCost_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtPUnitCost.TextChanged
        Try
            Dim decVendorCost, decProfit, decSellPrice As Decimal

            decSellPrice = CCommon.ToDecimal(hdnUnitCost.Value)
            decVendorCost = CCommon.ToDouble(txtPUnitCost.Text)
            decProfit = (decSellPrice - decVendorCost)

            lblProfit.Text = String.Format("{0:#,##0.00} / {1:#,##0.00}", decProfit, decSellPrice)
            hlProfitPerc.Text = FormatPercent(decProfit / IIf(decSellPrice = 0, 1, decSellPrice), 2)
            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideItemSearch", "$('.itemsArea').css('visibility', 'visible');$('.itemsArea').css('display', 'flow-root');", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub radcmbSalesTemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs) Handles radcmbSalesTemplate.SelectedIndexChanged
        Try
            If radcmbSalesTemplate.SelectedIndex = 0 Then Exit Sub
            Dim objOpp As New COpportunities
            Dim dtTemplate As DataTable
            objOpp.DomainID = Session("DomainID")
            objOpp.SalesTemplateID = radcmbSalesTemplate.SelectedValue.Split(",")(0)
            objOpp.byteMode = 2
            dtTemplate = objOpp.GetSalesTemplate()
            If dtTemplate.Rows.Count > 0 Then
                Dim dtItems As DataTable
                Dim objOpportunity As New MOpportunity
                objOpportunity.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                dtItems = objOpp.GetSalesTemplateItems()
                If dtItems Is Nothing OrElse dtItems.Rows.Count = 0 Then
                    'Fetch Items from Opportunity
                    If CCommon.ToLong(dtTemplate.Rows(0)("numOppId")) > 0 Then
                        objOpportunity.OpportunityId = CCommon.ToLong(dtTemplate.Rows(0)("numOppId"))
                        objOpportunity.DomainID = Session("DomainID")
                        objOpportunity.Mode = 2
                        dtItems = objOpportunity.GetOrderItems().Tables(0)
                    End If
                End If
                'Fetch items from Opportunity
                'If CCommon.ToLong(dtTemplate.Rows(0)("numOppId")) > 0 Then
                '    objOpportunity.OpportunityId = CCommon.ToLong(dtTemplate.Rows(0)("numOppId"))
                '    objOpportunity.DomainID = Session("DomainID")
                '    objOpportunity.Mode = 2
                '    dtItems = objOpportunity.GetOrderItems().Tables(0)
                'Else 'Fetch item from sales template items 
                '    dtItems = objOpp.GetSalesTemplateItems()
                'End If

                Dim objItems As New CItems
                Dim ds As New DataSet
                Dim drow As DataRow
                Dim i As Integer = 1
                Dim k As Integer
                Dim dtItemTax As DataTable
                objCommon = New CCommon
                ds = ViewState("SOItems")
                ds.Tables(0).Clear()
                ds.Tables(1).Clear()
                ds.Tables(2).Clear()
                For Each dr As DataRow In dtItems.Rows
                    drow = ds.Tables(0).NewRow()
                    If dr("numoppitemtCode") IsNot Nothing AndAlso dr("numoppitemtCode") IsNot DBNull.Value Then
                        drow("numoppitemtCode") = dr("numoppitemtCode")
                    End If
                    If dr("numItemCode") IsNot Nothing AndAlso dr("numItemCode") IsNot DBNull.Value Then
                        drow("numItemCode") = dr("numItemCode")
                    End If
                    If dr("numUnitHour") IsNot Nothing AndAlso dr("numUnitHour") IsNot DBNull.Value Then
                        drow("numUnitHour") = dr("numUnitHour")
                    End If
                    If dr("monPrice") IsNot Nothing AndAlso dr("monPrice") IsNot DBNull.Value Then
                        drow("monPrice") = String.Format("{0:0.00}", dr("monPrice"))
                        drow("monSalePrice") = String.Format("{0:0.00}", dr("monPrice"))
                    End If
                    'If dr("numItemClassification") IsNot Nothing AndAlso dr("numItemClassification") IsNot DBNull.Value Then
                    '    drow("numItemClassification") = dr("numItemClassification")
                    'End If
                    If dr("monTotAmount") IsNot Nothing AndAlso dr("monTotAmount") IsNot DBNull.Value Then
                        drow("monTotAmount") = String.Format("{0:0.00}", dr("monTotAmount"))
                    End If
                    If dr("numSourceID") IsNot Nothing AndAlso dr("numSourceID") IsNot DBNull.Value Then
                        drow("numSourceID") = dr("numSourceID")
                    End If
                    If dr("vcItemDesc") IsNot Nothing AndAlso dr("vcItemDesc") IsNot DBNull.Value Then
                        drow("vcItemDesc") = dr("vcItemDesc")
                    End If
                    If dr("numWarehouseID") IsNot Nothing AndAlso dr("numWarehouseID") IsNot DBNull.Value Then
                        drow("numWarehouseID") = dr("numWarehouseID")
                    End If
                    If dr("vcItemName") IsNot Nothing AndAlso dr("vcItemName") IsNot DBNull.Value Then
                        drow("vcItemName") = dr("vcItemName")
                    End If
                    If dr("Warehouse") IsNot Nothing AndAlso dr("Warehouse") IsNot DBNull.Value Then
                        drow("Warehouse") = dr("Warehouse")
                    End If
                    If dr("numWarehouseItmsID") IsNot Nothing AndAlso dr("numWarehouseItmsID") IsNot DBNull.Value Then
                        drow("numWarehouseItmsID") = dr("numWarehouseItmsID")
                    End If
                    If dr("vcType") IsNot Nothing AndAlso dr("vcType") IsNot DBNull.Value Then
                        drow("ItemType") = dr("vcType")
                    End If
                    If dr("vcAttributes") IsNot Nothing AndAlso dr("vcAttributes") IsNot DBNull.Value Then
                        drow("Attributes") = dr("vcAttributes")
                    End If
                    drow("Op_Flag") = 1 '1 for insertion
                    If dr("bitDropShip") IsNot Nothing AndAlso dr("bitDropShip") IsNot DBNull.Value Then
                        drow("DropShip") = dr("bitDropShip")
                    End If
                    If dr("bitDiscountType") IsNot Nothing AndAlso dr("bitDiscountType") IsNot DBNull.Value Then
                        drow("bitDiscountType") = dr("bitDiscountType")
                    End If
                    If dr("fltDiscount") IsNot Nothing AndAlso dr("fltDiscount") IsNot DBNull.Value Then
                        drow("fltDiscount") = String.Format("{0:0.00}", dr("fltDiscount"))
                    End If
                    If dr("monTotAmtBefDiscount") IsNot Nothing AndAlso dr("monTotAmtBefDiscount") IsNot DBNull.Value Then
                        drow("monTotAmtBefDiscount") = String.Format("{0:0.00}", dr("monTotAmtBefDiscount"))
                    End If
                    If dr("numUOMId") IsNot Nothing AndAlso dr("numUOMId") IsNot DBNull.Value Then
                        drow("numUOM") = dr("numUOMId")
                    End If
                    If dr("vcUOMName") IsNot Nothing AndAlso dr("vcUOMName") IsNot DBNull.Value Then
                        drow("vcUOMName") = dr("vcUOMName")
                    End If
                    If dr("UOMConversionFactor") IsNot Nothing AndAlso dr("UOMConversionFactor") IsNot DBNull.Value Then
                        drow("UOMConversionFactor") = dr("UOMConversionFactor")
                    End If
                    If dr("bitWorkOrder") IsNot Nothing AndAlso dr("bitWorkOrder") IsNot DBNull.Value Then
                        drow("bitWorkOrder") = dr("bitWorkOrder")
                    End If
                    If dr("charItemType") IsNot Nothing AndAlso dr("charItemType") IsNot DBNull.Value Then
                        drow("charItemType") = dr("charItemType")
                    End If
                    drow("numVendorWareHouse") = 0
                    drow("numShipmentMethod") = 0
                    If dr("numSOVendorId") IsNot Nothing AndAlso dr("numSOVendorId") IsNot DBNull.Value Then
                        drow("numSOVendorId") = dr("numSOVendorId")
                    End If
                    drow("numProjectID") = 0
                    drow("numProjectStageID") = 0
                    If dr("vcBaseUOMName") IsNot Nothing AndAlso dr("vcBaseUOMName") IsNot DBNull.Value Then
                        drow("vcBaseUOMName") = dr("vcBaseUOMName")
                    End If
                    drow("bitIsAuthBizDoc") = False
                    drow("bitItemPriceApprovalRequired") = False
                    drow("numSortOrder") = i

                    'Calculate Tax 
                    objItems.DomainID = Session("DomainID")
                    objItems.ItemCode = dr("numItemCode")
                    dtItemTax = objItems.ItemTax()
                    Dim strApplicable1 As String = ""
                    For Each dr1 As DataRow In dtItemTax.Rows
                        strApplicable1 = strApplicable1 & dr1("bitApplicable") & ","
                    Next
                    strApplicable1 = strApplicable1 & dr("bitTaxable")
                    Taxable.Value = strApplicable1

                    Dim strApplicable(), strTax() As String
                    strApplicable = Taxable.Value.Split(",")
                    strTax = txtTax.Text.Split(",")
                    drow("TotalTax") = 0
                    For k = 0 To chkTaxItems.Items.Count - 1
                        If strApplicable(k) = True Then
                            Dim decTaxValue As Decimal = CCommon.ToDecimal(strTax(k).Split("#")(0))
                            Dim tintTaxType As Short = CCommon.ToShort(strTax(k).Split("#")(1))

                            If tintTaxType = 2 AndAlso dr("numUnitHour") IsNot DBNull.Value AndAlso dr("UOMConversionFactor") IsNot DBNull.Value Then
                                drow("Tax" & chkTaxItems.Items(k).Value) = String.Format("{0:0.00}", decTaxValue * (dr("numUnitHour") * dr("UOMConversionFactor")))
                            ElseIf dr("monTotAmount") IsNot DBNull.Value Then
                                drow("Tax" & chkTaxItems.Items(k).Value) = String.Format("{0:0.00}", decTaxValue * dr("monTotAmount") / 100)
                            End If

                            drow("TotalTax") = CCommon.ToDecimal(drow("TotalTax")) + CCommon.ToDecimal(drow("Tax" & chkTaxItems.Items(k).Value))
                            drow("bitTaxable" & chkTaxItems.Items(k).Value) = True
                        Else
                            drow("Tax" & chkTaxItems.Items(k).Value) = 0
                            drow("bitTaxable" & chkTaxItems.Items(k).Value) = False
                        End If
                    Next
                    i += 1
                    ds.Tables(0).Rows.Add(drow)
                Next
                ds.Tables(0).AcceptChanges()
                ViewState("SOItems") = ds

                UpdateDetails()
                UpdatePanelItems.Update()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Public Sub radWareHouse_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radWareHouse.SelectedIndexChanged
        Try
            If CCommon.ToLong(hdnCurrentSelectedItem.Value) > 0 Then
                ItemFieldsChanged(hdnCurrentSelectedItem.Value, If(CCommon.ToLong(txtHidEditOppItem.Text) > 0, True, False))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
        Try
            ClearFinancialStamp()
            radCmbCompany_SelectedIndexChanged()
            LoadCreditCardDetail()
            UpdatePanelFinancialStamp.Update()
            LoadCustomerDetailSection()
            UpdatePanelCustomerDetail1.Update()
            UpdatePanelCustomerDetail2.Update()
            UpdatePanelItems.Update()
            radcmbContact.Focus()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub radcmbCards_SelectedIndexChanged(ByVal sender As Object, ByVal e As RadComboBoxSelectedIndexChangedEventArgs) Handles radcmbCards.SelectedIndexChanged
        Try
            LoadSavedCreditCardInfo()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

    Protected Sub lnkBtnUpdateOrder_Click(sender As Object, e As EventArgs)
        Try

            Dim dtItem As DataTable
            Dim dtSerItem As DataTable
            dtItem = dsTemp.Tables(0)
            dtItem.DefaultView.Sort = "vcItemName ASC"
            dtItem = dtItem.DefaultView.ToTable()
            dtItem.AcceptChanges()

            Dim i As Long = 1
            For Each dr As DataRow In dtItem.Rows
                dr("numSortOrder") = i
                i = i + 1
                dr.AcceptChanges()
            Next
            Dim perserveChanges As Boolean = True
            Dim msAction As System.Data.MissingSchemaAction = System.Data.MissingSchemaAction.Ignore

            Dim changes As System.Data.DataTable = dsTemp.Tables(0).GetChanges()
            If changes IsNot Nothing Then
                changes.Merge(dtItem, perserveChanges, msAction)
                dsTemp.Tables(0).Merge(changes, False, msAction)
            Else
                dsTemp.Tables(0).Merge(dtItem, False, msAction)
            End If
            UpdateDetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim sequences As List(Of String) = dgItems.Items.Cast(Of DataGridItem)().[Select](Function(r) TryCast(r.FindControl("txtRowOrder"), TextBox).Text.Trim()).ToList()
            'Try
            '    Dim userSequence As Integer() = sequences.[Select](Function(x) Integer.Parse(x)).OrderBy(Function(x) x).ToArray()
            '    Dim isInSequence As Boolean = userSequence.SequenceEqual(Enumerable.Range(1, userSequence.Count()))
            '    If isInSequence = False Then
            '        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ValidationItemDelete", "alert('Sequential row order is required, make corrections and try again.')", True)
            '        Exit Sub
            '    Else

            '    End If
            'Catch generatedExceptionName As Exception

            'End Try
            'Check tf minimum unit price approval rule is set for domin 
            If CCommon.ToBool(Session("IsMinUnitPriceRule")) Then
                If Not hdnApprovalActionTaken.Value = "Approved" And Not hdnApprovalActionTaken.Value = "Save&Proceed" Then
                    If CheckIfUnitPriceApprovalRequired() Then
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "UnitPriceApproval", "$( ""#dialogUnitPriceApproval"" ).show();", True)
                        hdnClickedButton.Value = "btnSave"
                        Exit Sub
                    End If
                End If
            End If
            Dim dtEnforcedMinSubTotal As DataTable = CheckIfMinOrderAmountRuleMeets()
            If dtEnforcedMinSubTotal IsNot Nothing AndAlso dtEnforcedMinSubTotal.Rows.Count > 0 AndAlso dtEnforcedMinSubTotal.Rows(0)("returnVal") = 0 Then
                Dim subTotal = dtEnforcedMinSubTotal.Rows(0)("MinOrderAmount")
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "MinOrderAmountRuleMeets", "alert('You must spend a minimum of $" & subTotal & " to complete this order.');", True)
                Exit Sub
            End If
            save()

            'If unit price approval is not required and unit price is already approved then only then create bizdoc
            If CCommon.ToBool(hdnIsUnitPriceApprovalRequired.Value) = False Or hdnApprovalActionTaken.Value = "Approved" Then
                If (pageType = PageTypeEnum.Sales) Then
                    Dim objRule As New OrderAutoRules
                    objRule.GenerateAutoPO(OppBizDocID)


                    If (CCommon.ToInteger(radcmbPaymentMethods.SelectedValue) > 0) Then
                        ReceivePayment()
                    End If
                End If
            End If

            ViewState("SOItems") = Nothing
            Session("CouponCode") = Nothing
            Session("TotalDiscount") = Nothing

            If (pageType = PageTypeEnum.Purchase) Then
                If CCommon.ToLong(GetQueryStringVal("StageID")) > 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Reload", "window.opener.location.reload(true);Close();self.close();", True)
                    Exit Sub
                ElseIf GetQueryStringVal("Source") <> "" Then
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Open", "OpenOpp(" & lngOppId.ToString & ")", True)
                    Exit Sub
                End If
                If boolFlag Then
                    If OppBizDocID > 0 Then
                        Response.Redirect("../opportunity/frmBizInvoice.aspx?OpID=" & lngOppId & "&OppBizId=" & OppBizDocID, False)
                    ElseIf CCommon.ToLong(GetQueryStringVal("StageID")) > 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Reload", "window.opener.location.reload(true);Close();self.close();", True)
                    Else
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Open", "window.opener.location.href = ""../opportunity/frmOpportunities.aspx?opId=" & lngOppId.ToString() & """; self.close();", True)
                    End If
                End If
            ElseIf (pageType = PageTypeEnum.Sales) Then
                If OppBizDocID = 0 Then
                    Dim strScript As String = "window.opener.reDirectPage('../opportunity/frmOpportunities.aspx?opId=" & lngOppId.ToString() & "'); self.close();"
                    If (Not Page.IsStartupScriptRegistered("clientScript")) Then
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "clientScript", strScript, True)
                    End If
                Else
                    Dim url As String
                    url = String.Format("../opportunity/frmBizInvoice.aspx?OpID={0}&OppBizId={1}", lngOppId, OppBizDocID)
                    Response.Redirect(url, False)
                End If
            Else
                Dim strScript As String = ""

                strScript += "window.opener.reDirectPage('../opportunity/frmOpportunities.aspx?opId=" & lngOppId.ToString() & "'); self.close();"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "clientScript", strScript, True)
                End If
            End If
        Catch ex As Exception
            If ex.Message.Contains("ITEM_PROMOTION_EXPIRED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Item promotion(s) used are expired.' );", True)
            ElseIf ex.Message.Contains("MULTIPLE_COUPON_BASED_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Multiple coupon based Item promotion(s) used.' );", True)
            ElseIf ex.Message.Contains("INVALID_COUPON_CODE_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Invalid item promotion coupon Code.' );", True)
            ElseIf ex.Message.Contains("ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Item promotion Coupon usage limit exceeds, Promotion can not be applied. Please clear code to Proceed.' );", True)
            ElseIf ex.Message.Contains("COUPON_CODE_REQUIRED_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Coupon code is required to use item promotion.' );", True)
            ElseIf ex.Message.Contains("ORDER_PROMOTION_EXPIRED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Order promotion used is expired.' );", True)
            ElseIf ex.Message.Contains("INVALID_COUPON_CODE_ORDER_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Invalid order promotion coupon Code.' );", True)
            ElseIf ex.Message.Contains("ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Order promotion Coupon usage limit exceeds, Promotion can not be applied. Please clear code to Proceed.' );", True)
            ElseIf ex.Message.Contains("COUPON_CODE_REQUIRED_ORDER_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Coupon code is required to use order promotion.' );", True)
            ElseIf ex.Message.Contains("SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Warehouse selected for kit/assembly item(s) is not available for all child items.' );", True)
            ElseIf ex.Message.Contains("FY_CLOSED") Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub

    Private Sub btnSaveNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNew.Click
        Try
            IsFromSaveAndNew = True
            Dim sequences As List(Of String) = dgItems.Items.Cast(Of DataGridItem)().[Select](Function(r) TryCast(r.FindControl("txtRowOrder"), TextBox).Text.Trim()).ToList()
            'Try
            '    Dim userSequence As Integer() = sequences.[Select](Function(x) Integer.Parse(x)).OrderBy(Function(x) x).ToArray()
            '    Dim isInSequence As Boolean = userSequence.SequenceEqual(Enumerable.Range(1, userSequence.Count()))
            '    If isInSequence = False Then
            '        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ValidationItemDelete", "alert('Sequential row order is required, make corrections and try again.')", True)
            '        Exit Sub
            '    Else

            '    End If
            'Catch generatedExceptionName As Exception

            'End Try
            'Check tf minimum unit price approval rule is set for domin 
            If CCommon.ToBool(Session("IsMinUnitPriceRule")) Then
                If Not hdnApprovalActionTaken.Value = "Approved" And Not hdnApprovalActionTaken.Value = "Save&Proceed" Then
                    If CheckIfUnitPriceApprovalRequired() Then
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "UnitPriceApproval", "$( ""#dialogUnitPriceApproval"" ).show();", True)
                        hdnClickedButton.Value = "btnSaveNew"
                        Exit Sub
                    End If
                End If
            End If
            Dim dtEnforcedMinSubTotal As DataTable = CheckIfMinOrderAmountRuleMeets()
            If dtEnforcedMinSubTotal IsNot Nothing AndAlso dtEnforcedMinSubTotal.Rows.Count > 0 AndAlso dtEnforcedMinSubTotal.Rows(0)("returnVal") = 0 Then
                Dim subTotal = dtEnforcedMinSubTotal.Rows(0)("MinOrderAmount")
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "MinOrderAmountRuleMeets", "alert('You must spend a minimum of $" & subTotal & " to complete this order.');", True)
                Exit Sub
            End If
            If CCommon.ToInteger(radcmbPaymentMethods.SelectedValue) = 1 And (rdbAuthorizeAndCharge.Checked Or rdbAuthorizeOnly.Checked) Then
                SaveCreditCardInfo(Convert.ToDecimal(lblTotal.Text))
            End If

            save()

            'If unit price approval is not required and unit price is already approved then only then create bizdoc
            If CCommon.ToBool(hdnIsUnitPriceApprovalRequired.Value) = False Or hdnApprovalActionTaken.Value = "Approved" Then
                Dim objRule As New OrderAutoRules
                objRule.GenerateAutoPO(OppBizDocID)

                If pageType = PageTypeEnum.Sales AndAlso (CCommon.ToInteger(radcmbPaymentMethods.SelectedValue) > 0) Then
                    ReceivePayment()
                End If
            End If

            ViewState("SOItems") = Nothing
            Session("CouponCode") = Nothing
            Session("TotalDiscount") = Nothing

            If boolFlag Then
                If isPOS = 1 Then
                    Response.Redirect("../opportunity/frmNewOrder.aspx?OppStatus=" & GetQueryStringVal("OppStatus") & "&OppType=" & GetQueryStringVal("OppType") & "IsPOS=1")
                Else
                    Response.Redirect("../opportunity/frmNewOrder.aspx?OppStatus=" & GetQueryStringVal("OppStatus") & "&OppType=" & GetQueryStringVal("OppType"))
                End If
            End If

        Catch ex As Exception
            If ex.Message.Contains("ITEM_PROMOTION_EXPIRED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Item promotion(s) used are expired.' );", True)
            ElseIf ex.Message.Contains("MULTIPLE_COUPON_BASED_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Multiple coupon based Item promotion(s) used.' );", True)
            ElseIf ex.Message.Contains("INVALID_COUPON_CODE_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Invalid item promotion coupon Code.' );", True)
            ElseIf ex.Message.Contains("ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Item promotion Coupon usage limit exceeds, Promotion can not be applied. Please clear code to Proceed.' );", True)
            ElseIf ex.Message.Contains("COUPON_CODE_REQUIRED_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Coupon code is required to use item promotion.' );", True)
            ElseIf ex.Message.Contains("ORDER_PROMOTION_EXPIRED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Order promotion used is expired.' );", True)
            ElseIf ex.Message.Contains("INVALID_COUPON_CODE_ORDER_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Invalid order promotion coupon Code.' );", True)
            ElseIf ex.Message.Contains("ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Order promotion Coupon usage limit exceeds, Promotion can not be applied. Please clear code to Proceed.' );", True)
            ElseIf ex.Message.Contains("COUPON_CODE_REQUIRED_ORDER_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Coupon code is required to use order promotion.' );", True)
            ElseIf ex.Message.Contains("SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Warehouse selected for kit/assembly item(s) is not available for all child items.' );", True)
            ElseIf ex.Message.Contains("FY_CLOSED") Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub

    Private Sub btnAddToCart_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddToCart.Click
        Try

            Session("CouponCode") = Nothing
            Session("TotalDiscount") = Nothing

            'Check tf minimum unit price approval rule is set for domin 
            If CCommon.ToBool(Session("IsMinUnitPriceRule")) Then
                If Not hdnApprovalActionTaken.Value = "Approved" And Not hdnApprovalActionTaken.Value = "Save&Proceed" Then
                    If CheckIfUnitPriceApprovalRequired() Then
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "UnitPriceApproval", "$( ""#dialogUnitPriceApproval"" ).show();", True)
                        hdnClickedButton.Value = "btnAddToCart"
                        Exit Sub
                    End If
                End If
            End If
            Dim dtEnforcedMinSubTotal As DataTable = CheckIfMinOrderAmountRuleMeets()
            If dtEnforcedMinSubTotal IsNot Nothing AndAlso dtEnforcedMinSubTotal.Rows.Count > 0 AndAlso dtEnforcedMinSubTotal.Rows(0)("returnVal") = 0 Then
                Dim subTotal = dtEnforcedMinSubTotal.Rows(0)("MinOrderAmount")
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "MinOrderAmountRuleMeets", "alert('You must spend a minimum of $" & subTotal & " to complete this order.');", True)
                Exit Sub
            End If
            'Add customer or use existing
            If (pageType = PageTypeEnum.Purchase) Or (pageType = PageTypeEnum.Sales) Then
                If divNewCustomer.Visible Then
                    InsertProspect()
                Else
                    lngDivId = radCmbCompany.SelectedValue
                    lngCntID = radcmbContact.SelectedValue
                End If
            Else
                lngDivId = radCmbCompany.SelectedValue
                lngCntID = radcmbContact.SelectedValue
            End If


            'Step1:Get items added to grid and insert it into cartitems table
            Dim objUserAccess As New UserAccess()
            objUserAccess.DomainID = Sites.ToLong(Session("DomainID"))
            Dim dtData As DataTable = objUserAccess.GetECommerceDetails()

            Dim lngSiteID As Long
            Dim lngWarehouseID As Long
            If dtData.Rows.Count = 1 Then
                lngSiteID = CCommon.ToString(dtData.Rows(0)("numSiteID"))
                lngWarehouseID = CCommon.ToString(dtData.Rows(0)("numDefaultWareHouseID"))
            End If

            If lngSiteID = 0 Then
                litMessage.Text = "Please set Default site from e-Commerce Settings to continue."
                Return
            End If
            If lngWarehouseID = 0 Then
                litMessage.Text = "Please set Default warehouse from e-Commerce Settings to continue."
                Return
            End If
            Dim ds As DataSet = Nothing
            Dim dtItemDetails As DataTable = Nothing
            Dim objItems As New CItems()
            Dim dr As DataRow = Nothing
            Dim objcart As BACRM.BusinessLogic.ShioppingCart.Cart
            For Each dgGridItem As DataGridItem In dgItems.Items
                Dim numItemCode As Long = CCommon.ToLong(dgItems.DataKeys(dgGridItem.ItemIndex))

                objItems.ItemCode = Sites.ToInteger(numItemCode.ToString())
                objItems.WarehouseID = lngWarehouseID
                objItems.DomainID = Sites.ToLong(Session("DomainID"))
                objItems.SiteID = lngSiteID
                objItems.DivisionID = lngDivId
                If ViewState("SOItems") Is Nothing Then
                    ds = objItems.ItemDetailsForEcomm()
                Else
                    ds = ViewState("SOItems")
                    If Not ds.Tables(0).Columns.Contains("txtItemDesc") Then ds.Tables(0).Columns.Add("txtItemDesc")
                    If Not ds.Tables(0).Columns.Contains("numWareHouseItemID") Then ds.Tables(0).Columns.Add("numWareHouseItemID")
                    If Not ds.Tables(0).Columns.Contains("bitFreeShipping") Then ds.Tables(0).Columns.Add("bitFreeShipping")
                    If Not ds.Tables(0).Columns.Contains("vcCategoryName") Then ds.Tables(0).Columns.Add("vcCategoryName")
                    If Not ds.Tables(0).Columns.Contains("fltWeight") Then ds.Tables(0).Columns.Add("fltWeight")
                    If Not ds.Tables(0).Columns.Contains("bitDiscountType") Then ds.Tables(0).Columns.Add("bitDiscountType")
                    If Not ds.Tables(0).Columns.Contains("fltDiscount") Then ds.Tables(0).Columns.Add("fltDiscount")

                    Dim dsItemDetailsForEcomm As New DataSet
                    dsItemDetailsForEcomm = objItems.ItemDetailsForEcomm()
                    Dim drValue As DataRow = dsItemDetailsForEcomm.Tables(0).Select("numItemCode = " & numItemCode)(0)

                    Dim drItem As DataRow = ds.Tables(0).Select("TRIM(numItemCode) = " & numItemCode)(0)
                    drItem("txtItemDesc") = CCommon.ToString(drValue("txtItemDesc"))
                    drItem("numWareHouseItemID") = CCommon.ToLong(drValue("numWareHouseItemID"))
                    drItem("bitFreeShipping") = CCommon.ToBool(drValue("bitFreeShipping"))
                    drItem("vcCategoryName") = CCommon.ToString(drValue("vcCategoryName"))
                    drItem("fltWeight") = CCommon.ToDouble(drValue("fltWeight"))
                    drItem("bitDiscountType") = CCommon.ToBool(CType(dgGridItem.FindControl("txtDiscountType"), TextBox).Text)
                    drItem("fltDiscount") = CCommon.ToDouble(CType(dgGridItem.FindControl("txttotalDiscount"), TextBox).Text)

                    drItem.AcceptChanges()
                    ds.Tables(0).AcceptChanges()

                    'End If

                    'dtItemDetails = ds.Tables(0)

                    'If dtItemDetails.Rows.Count > 0 Then
                    dr = drItem 'dtItemDetails.Rows(0)

                    objcart = New BACRM.BusinessLogic.ShioppingCart.Cart()
                    objcart.ContactId = lngCntID
                    objcart.DomainID = CCommon.ToLong(Session("DomainID"))
                    objcart.CookieId = ""
                    objcart.OppItemCode = dgGridItem.ItemIndex 'CCommon.ToLong(dr("numOppItemCode"))
                    objcart.ItemCode = numItemCode
                    objcart.UnitHour = Math.Abs(CCommon.ToDecimal(CType(dgGridItem.FindControl("txtUnits"), TextBox).Text))
                    objcart.Price = CCommon.ToDecimal(CType(dgGridItem.FindControl("hdnUnitPrice"), HiddenField).Value)
                    objcart.SourceId = 0
                    objcart.ItemDesc = CCommon.ToString(dr("txtItemDesc"))
                    objcart.WarehouseId = lngWarehouseID 'CCommon.ToLong(dr("numWarehouseID"))
                    objcart.ItemName = CCommon.ToString(dr("vcItemName"))
                    objcart.Warehouse = ""
                    objcart.WarehouseItemsId = IIf(IsDBNull(dr("numWareHouseItemID")), 0, CCommon.ToLong(dr("numWareHouseItemID")))
                    If CCommon.ToString(dr("charItemType")) = "P" Then
                        objcart.ItemType = "Inventory Item"
                    ElseIf CCommon.ToString(dr("charItemType")) = "S" Then
                        objcart.ItemType = "Service Item"
                    ElseIf CCommon.ToString(dr("charItemType")) = "N" Then
                        objcart.ItemType = "Non-Inventory Item"
                    ElseIf CCommon.ToString(dr("charItemType")) = "A" Then
                        objcart.ItemType = "Accessory"
                    End If


                    objcart.FreeShipping = CCommon.ToBool(dr("bitFreeShipping"))
                    objcart.OPFlag = 1 'CShort(CCommon.ToInteger(dr("tintOpFlag")))
                    objcart.DiscountType = CCommon.ToBool(dr("bitDiscountType")) ''' REMOVE COMMENT TO ADD DiscountType
                    objcart.Discount = CCommon.ToDouble(dr("fltDiscount")) ''' REMOVE COMMENT TO ADD Discount
                    objcart.TotAmtBeforeDisc = objcart.UnitHour * objcart.Price 'CCommon.ToDecimal(dr("monTotAmtBefDiscount"))
                    objcart.ItemURL = GetProductURL(CCommon.ToString(dr("vcCategoryName")), objcart.ItemName, numItemCode.ToString)
                    objcart.UOM = CCommon.ToLong(dr("numUOM"))
                    objcart.UOMName = If(CCommon.ToString(dr("vcUOMName")) = "", "Units", Sites.ToString(dr("vcUOMName"))) 'CCommon.ToString(dr("vcUOMName"))
                    objcart.UOMConversionFactor = CCommon.ToDecimal(dr("UOMConversionFactor"))
                    objcart.Weight = CCommon.ToLong(dr("fltWeight"))
                    objcart.Height = CCommon.ToLong(dr("fltHeight"))
                    objcart.Length = CCommon.ToLong(dr("fltLength"))
                    objcart.Width = CCommon.ToLong(dr("fltWidth"))
                    objcart.ShippingMethod = ""
                    objcart.ServiceTypeId = 0
                    objcart.ShippingCompany = 0
                    objcart.ServiceType = 0
                    objcart.DeliveryDate = DateTime.Now.[Date]
                    objcart.TotAmount = objcart.UnitHour * objcart.Price
                    objcart.SiteID = -1
                    objcart.InsertCartItems()
                End If
            Next


            'Step2:Create Extranet account for selected contact if not exist
            Dim objUserGroups As New UserGroups
            objUserGroups.ContactID = lngCntID
            objUserGroups.DomainID = Session("DomainID")
            If objCommon Is Nothing Then
                objCommon = New CCommon
            End If
            objUserGroups.Password = objCommon.Encrypt(PasswordGenerator.Generate(10, 10))
            objUserGroups.AccessAllowed = 1
            Dim lngExtranetID As Long = objUserGroups.CreateExtranetAccount()

            objUserGroups.ExtranetID = lngExtranetID
            dsTemp = objUserGroups.GetExtranetDetails()
            dtData = dsTemp.Tables(1)
            If dtData.Rows.Count > 0 Then
                'Add SMTPServerIntegration = true because it checks while sending Email .
                'System.Web.HttpContext.Current.Session("SMTPServerIntegration") = True
                Dim dr1() As DataRow = dtData.Select("numContactID = '" + lngCntID.ToString + "'")
                If dr1.Length > 0 Then
                    objUserGroups.Password = CCommon.ToString(dr1(0)("Password"))
                End If
                'Step3:Send email template with username and password to  complete shopping
                Dim objSendEmail As New Email
                objSendEmail.DomainID = Sites.ToLong(Session("DomainID"))
                objSendEmail.TemplateCode = "#SYS#ECOMMERCE_ADDTOCART_FROM_BIZ"
                objSendEmail.ModuleID = 1
                objSendEmail.RecordIds = Sites.ToString(lngCntID)
                objSendEmail.AdditionalMergeFields.Add("PortalPassword", objUserGroups.Password)
                objSendEmail.FromEmail = Session("UserEmail")
                objSendEmail.ToEmail = "##ContactFirstName## <##ContactEmail##>"
                objSendEmail.SendEmailTemplate()
            Else
                litMessage.Text = "unable to create external access to selected customer, please contact administrator."
                Return
            End If

            If boolFlag Then
                Response.Redirect("../opportunity/frmNewOrder.aspx?OppStatus=" & GetQueryStringVal("OppStatus") & "&OppType=" & GetQueryStringVal("OppType"))
            End If

        Catch ex As Exception
            If ex.Message.Contains("FY_CLOSED") Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub

    Private Sub btnSaveBizDoc_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveBizDoc.Click
        Try
            IsFromCreateBizDoc = True

            If pageType = PageTypeEnum.AddOppertunity AndAlso oppType = 1 AndAlso CCommon.ToLong(Session("DefaultSalesOppBizDocID")) > 0 Then
                save()
            Else
                'Check tf minimum unit price approval rule is set for domin 
                If CCommon.ToBool(Session("IsMinUnitPriceRule")) Then
                    If Not hdnApprovalActionTaken.Value = "Approved" And Not hdnApprovalActionTaken.Value = "Save&Proceed" Then
                        If CheckIfUnitPriceApprovalRequired() Then
                            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "UnitPriceApproval", "$( ""#dialogUnitPriceApproval"" ).show();", True)
                            hdnClickedButton.Value = "btnSaveBizDoc"
                            Exit Sub
                        End If
                    End If
                End If
                Dim dtEnforcedMinSubTotal As DataTable = CheckIfMinOrderAmountRuleMeets()
                If dtEnforcedMinSubTotal IsNot Nothing AndAlso dtEnforcedMinSubTotal.Rows.Count > 0 AndAlso dtEnforcedMinSubTotal.Rows(0)("returnVal") = 0 Then
                    Dim subTotal = dtEnforcedMinSubTotal.Rows(0)("MinOrderAmount")
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "MinOrderAmountRuleMeets", "alert('You must spend a minimum of $" & subTotal & " to complete this order.');", True)
                    Exit Sub
                End If

                save()

                'If unit price approval is not required and unit price is already approved then only then create bizdoc
                If CCommon.ToBool(hdnIsUnitPriceApprovalRequired.Value) = False Or hdnApprovalActionTaken.Value = "Approved" Then
                    Dim objRule As New OrderAutoRules
                    objRule.GenerateAutoPO(OppBizDocID)
                End If
            End If

            ViewState("SOItems") = Nothing
            Session("CouponCode") = Nothing
            Session("TotalDiscount") = Nothing

            If boolFlag Then
                Response.Redirect("../opportunity/frmBizInvoice.aspx?OpID=" & lngOppId & "&OppBizId=" & OppBizDocID)
            End If
        Catch ex As Exception
            If ex.Message.Contains("ITEM_PROMOTION_EXPIRED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Item promotion(s) used are expired.' );", True)
            ElseIf ex.Message.Contains("MULTIPLE_COUPON_BASED_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Multiple coupon based Item promotion(s) used.' );", True)
            ElseIf ex.Message.Contains("INVALID_COUPON_CODE_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Invalid item promotion coupon Code.' );", True)
            ElseIf ex.Message.Contains("ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Item promotion Coupon usage limit exceeds, Promotion can not be applied. Please clear code to Proceed.' );", True)
            ElseIf ex.Message.Contains("COUPON_CODE_REQUIRED_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Coupon code is required to use item promotion.' );", True)
            ElseIf ex.Message.Contains("ORDER_PROMOTION_EXPIRED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Order promotion used is expired.' );", True)
            ElseIf ex.Message.Contains("INVALID_COUPON_CODE_ORDER_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Invalid order promotion coupon Code.' );", True)
            ElseIf ex.Message.Contains("ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Order promotion Coupon usage limit exceeds, Promotion can not be applied. Please clear code to Proceed.' );", True)
            ElseIf ex.Message.Contains("COUPON_CODE_REQUIRED_ORDER_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Coupon code is required to use order promotion.' );", True)
            ElseIf ex.Message.Contains("SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Warehouse selected for kit/assembly item(s) is not available for all child items.' );", True)
            ElseIf ex.Message.Contains("FY_CLOSED") Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub

    Private Sub btnSaveOpenOrder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveOpenOrder.Click
        Try
            IsFromSaveAndOpenOrderDetails = True
            Dim sequences As List(Of String) = dgItems.Items.Cast(Of DataGridItem)().[Select](Function(r) TryCast(r.FindControl("txtRowOrder"), TextBox).Text.Trim()).ToList()
            'Try
            '    Dim userSequence As Integer() = sequences.[Select](Function(x) Integer.Parse(x)).OrderBy(Function(x) x).ToArray()
            '    Dim isInSequence As Boolean = userSequence.SequenceEqual(Enumerable.Range(1, userSequence.Count()))
            '    If isInSequence = False Then
            '        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ValidationItemDelete", "alert('Sequential row order is required, make corrections and try again.')", True)
            '        Exit Sub
            '    Else

            '    End If
            'Catch generatedExceptionName As Exception

            'End Try
            'Check tf minimum unit price approval rule is set for domin 
            If CCommon.ToBool(Session("IsMinUnitPriceRule")) Then
                If Not hdnApprovalActionTaken.Value = "Approved" And Not hdnApprovalActionTaken.Value = "Save&Proceed" Then
                    If CheckIfUnitPriceApprovalRequired() Then
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "UnitPriceApproval", "$( ""#dialogUnitPriceApproval"" ).show();", True)
                        hdnClickedButton.Value = "btnSaveOpenOrder"
                        Exit Sub
                    End If
                End If
            End If
            'Added By Neelam On 02/05/2018 - To Check the Minimum Order Amount rule
            Dim dtEnforcedMinSubTotal As DataTable = CheckIfMinOrderAmountRuleMeets()
            If dtEnforcedMinSubTotal IsNot Nothing AndAlso dtEnforcedMinSubTotal.Rows.Count > 0 AndAlso dtEnforcedMinSubTotal.Rows(0)("returnVal") = 0 Then
                Dim subTotal = dtEnforcedMinSubTotal.Rows(0)("MinOrderAmount")
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "MinOrderAmountRuleMeets", "alert('You must spend a minimum of $" & subTotal & " to complete this order.');", True)
                Exit Sub
            End If
            If CCommon.ToInteger(radcmbPaymentMethods.SelectedValue) = 1 And (rdbAuthorizeAndCharge.Checked Or rdbAuthorizeOnly.Checked) Then
                SaveCreditCardInfo(Convert.ToDecimal(lblTotal.Text))
            End If
            'If ProcessPayment() Then
            save()

            If (pageType = PageTypeEnum.Sales) Then
                'If unit price approval is not required and unit price is already approved then only then create bizdoc
                If CCommon.ToBool(hdnIsUnitPriceApprovalRequired.Value) = False Or hdnApprovalActionTaken.Value = "Approved" Then
                    Dim objRule As New OrderAutoRules
                    objRule.GenerateAutoPO(OppBizDocID)

                    If (CCommon.ToInteger(radcmbPaymentMethods.SelectedValue) > 0) Then
                        ReceivePayment()
                    End If
                End If
            End If

            ViewState("SOItems") = Nothing
            Session("CouponCode") = Nothing
            Session("TotalDiscount") = Nothing

            If boolFlag Then
                If isPOS = 1 Then
                    Response.Redirect("../opportunity/frmOpportunities.aspx?opId=" & lngOppId.ToString())
                Else
                    Dim strScript As String = "window.opener.reDirectPage('../opportunity/frmOpportunities.aspx?opId=" & lngOppId.ToString() & "'); self.close();"
                    If (Not Page.IsStartupScriptRegistered("clientScript")) Then
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "clientScript", strScript, True)
                    End If
                End If
            End If
        Catch ex As Exception
            If ex.Message.Contains("ITEM_PROMOTION_EXPIRED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Item promotion(s) used are expired.' );", True)
            ElseIf ex.Message.Contains("MULTIPLE_COUPON_BASED_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Multiple coupon based Item promotion(s) used.' );", True)
            ElseIf ex.Message.Contains("INVALID_COUPON_CODE_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Invalid item promotion coupon Code.' );", True)
            ElseIf ex.Message.Contains("ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Item promotion Coupon usage limit exceeds, Promotion can not be applied. Please clear code to Proceed.' );", True)
            ElseIf ex.Message.Contains("COUPON_CODE_REQUIRED_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Coupon code is required to use item promotion.' );", True)
            ElseIf ex.Message.Contains("ORDER_PROMOTION_EXPIRED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Order promotion used is expired.' );", True)
            ElseIf ex.Message.Contains("INVALID_COUPON_CODE_ORDER_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Invalid order promotion coupon Code.' );", True)
            ElseIf ex.Message.Contains("ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Order promotion Coupon usage limit exceeds, Promotion can not be applied. Please clear code to Proceed.' );", True)
            ElseIf ex.Message.Contains("COUPON_CODE_REQUIRED_ORDER_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Coupon code is required to use order promotion.' );", True)
            ElseIf ex.Message.Contains("SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Warehouse selected for kit/assembly item(s) is not available for all child items.' );", True)
            ElseIf ex.Message.Contains("FY_CLOSED") Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        Try
            hdnApprovalActionTaken.Value = "Clear"

            If Not dsTemp Is Nothing AndAlso dsTemp.Tables.Count > 0 AndAlso dsTemp.Tables(0).Rows.Count > 0 Then
                For Each dr As DataRow In dsTemp.Tables(0).Rows
                    dr("bitItemPriceApprovalRequired") = False
                Next

                UpdateDetails()
                UpdatePanelItems.Update()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnSaveAndProceed_Click(sender As Object, e As EventArgs) Handles btnSaveAndProceed.Click
        Try
            hdnApprovalActionTaken.Value = "Save&Proceed"

            Select Case hdnClickedButton.Value
                Case "btnSave"
                    btnSave_Click(Nothing, Nothing)
                Case "btnSaveNew"
                    btnSaveNew_Click(Nothing, Nothing)
                Case "btnSaveBizDoc"
                    btnSaveBizDoc_Click(Nothing, Nothing)
                Case "btnAddToCart"
                    btnAddToCart_Click(Nothing, Nothing)
                Case "btnSaveOpenOrder"
                    btnSaveOpenOrder_Click(Nothing, Nothing)
                Case "btnCreditCard"
                    btnCreditCard_Click(Nothing, Nothing)
                Case "btnCash"
                    btnCash_Click(Nothing, Nothing)
                Case "btnCheck"
                    btnCheck_Click(Nothing, Nothing)
                Case "btnBillMe"
                    btnBillMe_Click(Nothing, Nothing)
                Case "btnOthersPayment"
                    btnOthersPayment_Click(Nothing, Nothing)
            End Select
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnApproveUnitPrice_Click(sender As Object, e As EventArgs) Handles btnApproveUnitPrice.Click
        Try
            hdnApprovalActionTaken.Value = "Approved"

            For Each dr As DataRow In dsTemp.Tables(0).Rows
                dr("bitItemPriceApprovalRequired") = False
            Next

            Select Case hdnClickedButton.Value
                Case "btnSave"
                    btnSave_Click(Nothing, Nothing)
                Case "btnSaveNew"
                    btnSaveNew_Click(Nothing, Nothing)
                Case "btnAddToCart"
                    btnAddToCart_Click(Nothing, Nothing)
                Case "btnSaveOpenOrder"
                    btnSaveOpenOrder_Click(Nothing, Nothing)
                Case "btnSaveBizDoc"
                    btnSaveBizDoc_Click(Nothing, Nothing)
                Case "btnCreditCard"
                    btnCreditCard_Click(Nothing, Nothing)
                Case "btnCash"
                    btnCash_Click(Nothing, Nothing)
                Case "btnCheck"
                    btnCheck_Click(Nothing, Nothing)
                Case "btnBillMe"
                    btnBillMe_Click(Nothing, Nothing)
                Case "btnOthersPayment"
                    btnOthersPayment_Click(Nothing, Nothing)
            End Select
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub LoadMarkupDiscount()
        Try
            Dim objCommon As New CCommon
            Dim MarkupDiscount As DataTable
            objCommon.DomainID = CCommon.ToLong(Session("DomainID"))

            MarkupDiscount = objCommon.GetDomainSettingValue("tintMarkupDiscountOption")

            If (MarkupDiscount.Rows.Count > 0) Then
                ddlMarkupDiscountOption.SelectedValue = MarkupDiscount.Rows(0)("tintMarkupDiscountOption").ToString()
            End If

            MarkupDiscount = objCommon.GetDomainSettingValue("tintMarkupDiscountValue")

            If (MarkupDiscount.Rows.Count > 0) Then
                If (MarkupDiscount.Rows(0)("tintMarkupDiscountValue").ToString() = "1") Then
                    radPer.Checked = True
                    radAmt.Checked = False
                Else
                    radPer.Checked = False
                    radAmt.Checked = True
                End If

            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SetAssignedToEnabled(ByVal numRecordOwner As Long, ByVal numTerritoryID As Long)
        Try
            Dim m_aryRightsForAssignedTo() As Integer = GetUserRightsForPage_Other(10, 128)
            If m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 0 Then
                radcmbAssignedTo.Enabled = False
            ElseIf m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 1 AndAlso CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                Try
                    If numRecordOwner <> Session("UserContactID") Then
                        radcmbAssignedTo.Enabled = False
                    End If
                Catch ex As Exception
                End Try
            ElseIf m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 2 AndAlso CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                Dim i As Integer
                Dim dtTerritory As DataTable
                dtTerritory = Session("UserTerritory")
                If numTerritoryID = 0 Then
                    radcmbAssignedTo.Enabled = False
                Else
                    Dim chkDelete As Boolean = False
                    For i = 0 To dtTerritory.Rows.Count - 1
                        If numTerritoryID = dtTerritory.Rows(i).Item("numTerritoryId") Then
                            chkDelete = True
                        End If
                    Next
                    If chkDelete = False Then
                        radcmbAssignedTo.Enabled = False
                    End If
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub save()
        Try
            OrderPromotion()

            ' Order based Promotion
            If (CCommon.ToLong(hdnOrderPromId.Value) > 0) Then
                If lngDiscountServiceItemForOrder = 0 Then
                    boolFlag = False
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ValidationOrderProm", "alert('In order to set order discounts, You have to select an order discount line item from within Global Settings | Accounting | Default Item used for discounting orders.');", True)
                    Exit Sub
                Else
                    If bitRequireCouponCode = False And lngDiscountAmt > 0 Then
                        ApplyOrderPromotion()
                    ElseIf bitRequireCouponCode = True And lngDiscountAmt > 0 AndAlso (CCommon.ToLong(hdnApplyCouponCode.Value) = CCommon.ToLong(hdnOrderPromId.Value) Or txtApplyCouponCode.Text <> "") Then
                        ApplyOrderPromotion()
                    End If
                End If
            End If

            ShippingPromotion()

            If (CCommon.ToLong(hdnShipPromId.Value) > 0) Then
                If lngShippingItemCode = 0 Then
                    boolFlag = False
                    litMessage.Text = "Please first set Default Shipping Item from Administration->Global Settings->Accounting Tab->Shipping Service Item."
                    Exit Sub
                Else
                    ApplyShippingPromotion()
                End If
            End If

            If (pageType = PageTypeEnum.AddEditOrder) Then
                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")

                objCommon.Mode = 27
                objCommon.Str = CCommon.ToLong(GetQueryStringVal("opid"))
                If objCommon.GetSingleFieldValue() = 1 Then
                    boolFlag = False
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ValidationClosedOrder", "alert('You are not allowed to edit closed order.')", True)
                    Exit Sub
                End If

                Dim dtItem As DataTable

                dtItem = dsTemp.Tables(0)

                dsTemp.Tables(0).Columns.Remove("numContainer")
                dsTemp.Tables(0).Columns.Remove("numContainerQty")
                If dtItem.Rows.Count > 0 Then

                    For Each dr As DataRow In dtItem.Rows
                        Dim decUnits As Decimal = Math.Abs(CCommon.ToDecimal(dr("numUnitHour"))) * Math.Abs(CCommon.ToDecimal(dr("UOMConversionFactor")))
                        Dim decUnitHourReceived As Decimal = Math.Abs(CCommon.ToDecimal(dr("numUnitHourReceived")))
                        Dim decQtyShipped As Decimal = Math.Abs(CCommon.ToDecimal(dr("numQtyShipped")))

                        If oppType = 1 Then
                            If decQtyShipped > decUnits Then
                                boolFlag = False
                                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ValidationItemDelete", "alert('You are not allowed to edit this item,since item has shipped qty more than order qty.')", True)
                                Exit Sub
                            End If
                        ElseIf oppType = 2 Then
                            If decUnitHourReceived > decUnits Then
                                boolFlag = False
                                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ValidationItemDelete", "alert('You are not allowed to edit this item,since item has received qty more than order qty.')", True)
                                Exit Sub
                            End If
                        End If
                    Next
                End If

                SaveOpportunity()
                lngOppId = CCommon.ToLong(GetQueryStringVal("opid"))
            Else
                If (pageType = PageTypeEnum.Purchase) Then
                    If GetQueryStringVal("Source") <> "" AndAlso GetQueryStringVal("StageID") <> "" Then

                        Dim dtItem As DataTable

                        dtItem = dsTemp.Tables(0)

                        dsTemp.Tables(0).Columns.Remove("numContainer")
                        dsTemp.Tables(0).Columns.Remove("numContainerQty")
                        If dtItem.Rows.Count > 0 Then
                            Dim TotalAmount As Decimal
                            TotalAmount = CType(dsTemp.Tables(0).Compute("Sum(monTotAmount)", ""), Decimal)

                            Dim objTimeExp As New TimeExpenseLeave
                            Dim dtTimeDetails As DataTable

                            objTimeExp.DomainID = Session("DomainID")
                            objTimeExp.ProID = CCommon.ToLong(GetQueryStringVal("Source"))
                            objTimeExp.StageId = CCommon.ToLong(GetQueryStringVal("StageID"))

                            dtTimeDetails = objTimeExp.GetTimeAndExpense_BudgetTotal()

                            If dtTimeDetails.Rows.Count <> 0 Then
                                If dtTimeDetails.Rows(0).Item("bitExpenseBudget") = True Then
                                    If (TotalAmount > dtTimeDetails.Rows(0).Item("TotalExpense")) Then
                                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('You’ve exceeded the amount budgeted for this stage.' );", True)
                                        boolFlag = False
                                        Exit Sub
                                    End If
                                End If
                            End If
                        End If
                    End If
                End If

                If (pageType = PageTypeEnum.Purchase) Or (pageType = PageTypeEnum.Sales) Or (pageType = PageTypeEnum.AddOppertunity) Then
                    If divNewCustomer.Visible Then
                        InsertProspect()
                    Else
                        lngDivId = radCmbCompany.SelectedValue
                        lngCntID = radcmbContact.SelectedValue
                    End If
                Else
                    lngDivId = radCmbCompany.SelectedValue
                    lngCntID = radcmbContact.SelectedValue
                End If

                Opportunity()

                AddToRecentlyViewed(RecetlyViewdRecordType.Opportunity, lngOppId)
            End If

            ''Added By Sachin Sadhu||Date:29thApril12014
            ''Purpose :To Added Opportunity data in work Flow queue based on created Rules
            ''          Using Change tracking
            Dim objWfA As New Workflow()
            objWfA.DomainID = Session("DomainID")
            objWfA.UserCntID = Session("UserContactID")
            objWfA.RecordID = lngOppId
            objWfA.SaveWFOrderQueue()
            'end of code

            If pageType = PageTypeEnum.AddEditOrder Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Reload", "opener.__doPostBack('btnRefreshProducsServices', '');self.close();", True)
            End If

        Catch ex As Exception
            boolFlag = False
            Throw
        End Try
    End Sub

    Protected Sub ApplyOrderPromotion()
        Dim dsItems As DataSet = Nothing
        dsItems = If(dsTemp Is Nothing, ViewState("SOItems"), dsTemp)

        Dim OrderPromotionPrice As Double

        If dsItems.Tables.Count > 0 AndAlso dsItems.Tables(0).Rows.Count > 0 Then

            If (bitPromotionDiscountType = True) Then
                OrderPromotionPrice = CCommon.ToDouble(CType(dgItems.Controls(0).Controls(dsItems.Tables(0).Rows.Count + 1).FindControl("lblFTotal"), Label).Text) * lngDiscountAmt / 100
            Else
                OrderPromotionPrice = lngDiscountAmt
            End If
        End If
        Dim MaxRowOrder As Long
        MaxRowOrder = dsTemp.Tables(0).Rows.Count
        hdnCurrentSelectedItem.Value = lngDiscountServiceItemForOrder

        Dim objItem As New CItems
        objItem.DomainID = CCommon.ToLong(Session("DomainID"))
        objItem.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
        objItem.byteMode = CCommon.ToShort(oppType)
        objItem.ItemCode = lngDiscountServiceItemForOrder
        objItem.WareHouseItemID = 0
        objItem.GetItemAndWarehouseDetails()
        MaxRowOrder = MaxRowOrder + 1
        objItem.numSortOrder = MaxRowOrder

        ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon,
                                                                          dsTemp,
                                                                          True,
                                                                          objItem.ItemType,
                                                                          "",
                                                                          False,
                                                                          objItem.KitParent,
                                                                          objItem.ItemCode,
                                                                          1,
                                                                          (-OrderPromotionPrice),
                                                                          "Promotion applied on Order", objItem.WareHouseItemID, objItem.ItemName, "", 0, objItem.ModelID, 0, "-",
                                                                          1,
                                                                          pageType.ToString(), True, 0, "", txtTax.Text, txtCustomerPartNo.Text, chkTaxItems,
                                                                          numProjectID:=CCommon.ToLong(GetQueryStringVal("Source")),
                                                                          numProjectStageID:=CCommon.ToLong(GetQueryStringVal("StageID")),
                                                                          vcBaseUOMName:=hdnBaseUOMName.Value,
                                                                          objItem:=objItem, primaryVendorCost:=0, salePrice:=(-OrderPromotionPrice), numPromotionID:=hdnOrderPromId.Value,
                                                                          IsPromotionTriggered:=True,
                                                                          numSortOrder:=objItem.numSortOrder, itemReleaseDate:=GetReleaseDate(),
                                                                          numShipToAddressID:=0)


        UpdateDetails()
    End Sub

    Protected Sub ApplyShippingPromotion()

        Dim dsItems As DataSet = Nothing
        dsItems = If(dsTemp Is Nothing, ViewState("SOItems"), dsTemp)
        Dim ShipPromotionPrice As Double

        If (dsItems.Tables(0).Select("numItemCode=" & lngShippingItemCode).Length > 0) Then
            Exit Sub
        Else

            If dsItems.Tables.Count > 0 AndAlso dsItems.Tables(0).Rows.Count > 0 Then
                If dgShippingExceptions.Items.Count > 0 Then
                    For Each dgitem As DataGridItem In dgShippingExceptions.Items
                        ShipPromotionPrice = ShipPromotionPrice + CCommon.ToDouble(dgitem.Cells(2).Text)
                    Next
                End If
            End If
            ShipPromotionPrice = ShipPromotionPrice + ShippingAmt
        End If

        Dim MaxRowOrder As Long
        MaxRowOrder = dsTemp.Tables(0).Rows.Count
        hdnCurrentSelectedItem.Value = lngShippingItemCode

        Dim objItem As New CItems
        objItem.DomainID = CCommon.ToLong(Session("DomainID"))
        objItem.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
        objItem.byteMode = CCommon.ToShort(oppType)
        objItem.ItemCode = lngShippingItemCode
        objItem.WareHouseItemID = 0
        objItem.GetItemAndWarehouseDetails()
        MaxRowOrder = MaxRowOrder + 1
        objItem.numSortOrder = MaxRowOrder

        ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon,
                                                                          dsTemp,
                                                                          True,
                                                                          objItem.ItemType,
                                                                          "",
                                                                          False,
                                                                          objItem.KitParent,
                                                                          objItem.ItemCode,
                                                                          1,
                                                                         ShipPromotionPrice,
                                                                          "Shipping charges applied on Order", objItem.WareHouseItemID, objItem.ItemName, "", 0, objItem.ModelID, 0, "-",
                                                                          1,
                                                                          pageType.ToString(), True, 0, "", txtTax.Text, txtCustomerPartNo.Text, chkTaxItems,
                                                                          numProjectID:=CCommon.ToLong(GetQueryStringVal("Source")),
                                                                          numProjectStageID:=CCommon.ToLong(GetQueryStringVal("StageID")),
                                                                          vcBaseUOMName:=hdnBaseUOMName.Value,
                                                                          objItem:=objItem, primaryVendorCost:=0, salePrice:=ShipPromotionPrice, numPromotionID:=0,
                                                                          IsPromotionTriggered:=False,
                                                                          numSortOrder:=objItem.numSortOrder, itemReleaseDate:=GetReleaseDate(),
                                                                          numShipToAddressID:=0)


        UpdateDetails()
    End Sub

    Private Sub createSet()
        Try

            If Not IsPostBack And Not ViewState("SOItems") Is Nothing Then
                Dim ds1 As DataSet = ViewState("SOItems")
                If ds1.Tables(0).Rows.Count > 0 Then
                    Exit Sub
                End If
            End If
            dsTemp = New DataSet
            Dim dtItem As New DataTable
            Dim dtSerItem As New DataTable
            Dim dtChildItems As New DataTable
            dtItem.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
            dtItem.Columns.Add("numItemCode")
            dtItem.Columns.Add("numItemClassification")
            dtItem.Columns.Add("numUnitHour", GetType(Decimal))
            dtItem.Columns.Add("monPrice", GetType(Decimal))
            dtItem.Columns.Add("monSalePrice", GetType(Decimal))
            dtItem.Columns.Add("numUOM")
            dtItem.Columns.Add("vcUOMName")
            dtItem.Columns.Add("UOMConversionFactor")

            dtItem.Columns.Add("monTotAmount", GetType(Decimal))
            dtItem.Columns.Add("numSourceID")
            dtItem.Columns.Add("vcItemDesc")
            dtItem.Columns.Add("vcModelID")
            dtItem.Columns.Add("numWarehouseID")
            dtItem.Columns.Add("vcItemName")
            dtItem.Columns.Add("Warehouse")
            dtItem.Columns.Add("numWarehouseItmsID")
            dtItem.Columns.Add("ItemType")
            dtItem.Columns.Add("Attributes")
            dtItem.Columns.Add("AttributeIDs")
            dtItem.Columns.Add("Op_Flag")
            dtItem.Columns.Add("bitWorkOrder")
            dtItem.Columns.Add("bitAlwaysCreateWO")
            dtItem.Columns.Add("numWOQty")
            dtItem.Columns.Add("numMaxWorkOrderQty")
            dtItem.Columns.Add("vcInstruction")
            dtItem.Columns.Add("bintCompliationDate")
            dtItem.Columns.Add("dtPlannedStart")
            dtItem.Columns.Add("numWOAssignedTo")

            dtItem.Columns.Add("DropShip", GetType(Boolean))
            dtItem.Columns.Add("bitDiscountType", GetType(Boolean))
            dtItem.Columns.Add("fltDiscount", GetType(Decimal))
            dtItem.Columns.Add("TotalDiscountAmount", GetType(Decimal))
            dtItem.Columns.Add("monTotAmtBefDiscount", GetType(Decimal))

            dtItem.Columns.Add("bitTaxable0")
            dtItem.Columns.Add("Tax0", GetType(Decimal))
            dtItem.Columns.Add("TotalTax", GetType(Decimal))

            dtItem.Columns.Add("numVendorWareHouse")
            dtItem.Columns.Add("numShipmentMethod")
            dtItem.Columns.Add("numSOVendorId")

            dtItem.Columns.Add("numProjectID")
            dtItem.Columns.Add("numProjectStageID")
            dtItem.Columns.Add("charItemType")
            dtItem.Columns.Add("numToWarehouseItemID") 'added by chintan for stock transfer
            dtItem.Columns.Add("bitIsAuthBizDoc", GetType(Boolean))
            dtItem.Columns.Add("numUnitHourReceived")
            dtItem.Columns.Add("numQtyShipped")
            dtItem.Columns.Add("vcBaseUOMName")
            dtItem.Columns.Add("numVendorID", System.Type.GetType("System.Int32"))
            dtItem.Columns.Add("fltItemWeight")
            dtItem.Columns.Add("IsFreeShipping")
            dtItem.Columns.Add("fltHeight")
            dtItem.Columns.Add("fltWidth")
            dtItem.Columns.Add("fltLength")
            dtItem.Columns.Add("vcSKU")
            dtItem.Columns.Add("bitCouponApplied")
            dtItem.Columns.Add("bitItemPriceApprovalRequired")
            dtItem.Columns.Add("bitHasKitAsChild", GetType(Boolean))
            dtItem.Columns.Add("KitChildItems", GetType(String))
            dtItem.Columns.Add("numContainer")
            dtItem.Columns.Add("numContainerQty")
            dtItem.Columns.Add("numPromotionID")
            dtItem.Columns.Add("bitPromotionTriggered")
            dtItem.Columns.Add("bitPromotionDiscount", GetType(Boolean))
            dtItem.Columns.Add("numSelectedPromotionID")
            dtItem.Columns.Add("vcPromotionDetail")
            dtItem.Columns.Add("numCost", GetType(Decimal))
            dtItem.Columns.Add("numSortOrder")
            dtItem.Columns.Add("vcVendorNotes")
            dtItem.Columns.Add("CustomerPartNo")

            Dim itemReleaseDate As New DataColumn("ItemReleaseDate", GetType(Date))
            itemReleaseDate.DateTimeMode = System.Data.DataSetDateTime.Unspecified
            dtItem.Columns.Add(itemReleaseDate)

            dtItem.Columns.Add("Location")
            dtItem.Columns.Add("numShipToAddressID")
            dtItem.Columns.Add("ShipToFullAddress")
            dtItem.Columns.Add("InclusionDetail")
            dtItem.Columns.Add("OnHandAllocation")
            dtItem.Columns.Add("bitMarkupDiscount")
            dtItem.Columns.Add("bitDisablePromotion", GetType(Boolean))

            If (pageType = PageTypeEnum.Sales) Then
                '''Loading Other Taxes
                Dim dtTaxTypes As DataTable
                Dim ObjTaxItems As New TaxDetails
                ObjTaxItems.DomainID = Session("DomainID")
                dtTaxTypes = ObjTaxItems.GetTaxItems

                For Each drTax As DataRow In dtTaxTypes.Rows
                    dtItem.Columns.Add("Tax" & drTax("numTaxItemID"), GetType(Decimal))
                    dtItem.Columns.Add("bitTaxable" & drTax("numTaxItemID"))
                Next

                Dim dr As DataRow
                dr = dtTaxTypes.NewRow
                dr("numTaxItemID") = 0
                dr("vcTaxName") = "Sales Tax(Default)"
                dtTaxTypes.Rows.Add(dr)

                chkTaxItems.DataTextField = "vcTaxName"
                chkTaxItems.DataValueField = "numTaxItemID"
                chkTaxItems.DataSource = dtTaxTypes
                chkTaxItems.DataBind()
            End If

            dtSerItem.Columns.Add("numWarehouseItmsDTLID")
            dtSerItem.Columns.Add("numWItmsID")
            dtSerItem.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
            dtSerItem.Columns.Add("vcSerialNo")
            dtSerItem.Columns.Add("Comments")
            dtSerItem.Columns.Add("Attributes")

            dtChildItems.Columns.Add("numOppChildItemID", System.Type.GetType("System.Int32"))
            dtChildItems.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
            dtChildItems.Columns.Add("numItemCode")
            dtChildItems.Columns.Add("numQtyItemsReq")
            dtChildItems.Columns.Add("vcItemName")
            dtChildItems.Columns.Add("monListPrice")
            dtChildItems.Columns.Add("UnitPrice")
            dtChildItems.Columns.Add("charItemType")
            dtChildItems.Columns.Add("txtItemDesc")
            dtChildItems.Columns.Add("numWarehouseItmsID")
            dtChildItems.Columns.Add("Op_Flag")
            dtChildItems.Columns.Add("ItemType")
            dtChildItems.Columns.Add("Attr")
            dtChildItems.Columns.Add("vcWareHouse")

            dtItem.TableName = "Item"
            dtSerItem.TableName = "SerialNo"
            dtChildItems.TableName = "ChildItems"

            dsTemp.Tables.Add(dtItem)
            dsTemp.Tables.Add(dtSerItem)
            dsTemp.Tables.Add(dtChildItems)
            dtItem.PrimaryKey = New DataColumn() {dsTemp.Tables(0).Columns("numoppitemtCode")}
            dtChildItems.PrimaryKey = New DataColumn() {dsTemp.Tables(2).Columns("numOppChildItemID")}
            ViewState("SOItems") = dsTemp

            dgItems.DataSource = dtItem
            dgItems.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub Opportunity()
        Try
            Dim arrOutPut() As String
            Dim objOpportunity As New MOpportunity
            objOpportunity.OpportunityId = 0
            objOpportunity.ContactID = lngCntID
            objOpportunity.DivisionID = lngDivId
            objOpportunity.ReleaseDate = GetReleaseDate()

            If (pageType = PageTypeEnum.Purchase) Then
                objOpportunity.OpportunityName = radCmbCompany.Text & "-PO-" & Format(Now(), "MMMM")
            ElseIf (pageType = PageTypeEnum.Sales) Then
                objOpportunity.OpportunityName = radCmbCompany.Text & "-SO-" & Format(Now(), "MMMM")
            ElseIf (pageType = PageTypeEnum.AddOppertunity) Then
                objOpportunity.OpportunityName = radCmbCompany.Text & "-" & Format(Now(), "MMMM")
            End If

            objOpportunity.UserCntID = Session("UserContactID")
            objOpportunity.EstimatedCloseDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
            objOpportunity.PublicFlag = 0
            objOpportunity.DomainID = Session("DomainId")

            objOpportunity.BillToAddressID = CCommon.ToLong(hdnBillAddressID.Value)
            objOpportunity.ShipToAddressID = CCommon.ToLong(hdnShipAddressID.Value)

            ' IF Drop Ship Option Selected From Edit ShipTo Address Window
            If (hdnDropShp.Value = "1") Then
                objOpportunity.bitDropShipAddress = True
            Else
                objOpportunity.bitDropShipAddress = False
            End If

            objOpportunity.CouponCode = ""
            objOpportunity.Discount = 0
            objOpportunity.boolDiscType = False
            objOpportunity.dtExpectedDate = radOrderExpectedDate.SelectedDate

            'End If

            If Session("MultiCurrency") = True Then
                objOpportunity.CurrencyID = radcmbCurrency.SelectedValue
            Else
                objOpportunity.CurrencyID = Session("BaseCurrencyID")
            End If

            If (pageType = PageTypeEnum.Purchase) Or (pageType = PageTypeEnum.Sales) Then
                If divNewCustomer.Visible Then
                    If radcmbNewAssignTo.SelectedIndex >= 0 Then objOpportunity.AssignedTo = radcmbNewAssignTo.SelectedValue
                Else
                    If radcmbAssignedTo.SelectedIndex >= 0 Then objOpportunity.AssignedTo = radcmbAssignedTo.SelectedValue
                End If
            End If
            If (pageType = PageTypeEnum.AddOppertunity) Then
                If Not radcmbAssignedTo Is Nothing Then
                    If radcmbAssignedTo.SelectedIndex >= 0 Then objOpportunity.AssignedTo = radcmbAssignedTo.SelectedValue

                End If
            End If
            objOpportunity.OppType = oppType

            If (pageType = PageTypeEnum.Purchase Or pageType = PageTypeEnum.Sales) Then
                objOpportunity.DealStatus = objOpportunity.EnmDealStatus.DealWon
                objOpportunity.SourceType = 1
                If hdnApprovalActionTaken.Value = "Save&Proceed" And Convert.ToBoolean(Session("bitMarginPriceViolated")) = True Then
                    objOpportunity.tintClickBtn = 1
                    objOpportunity.DealStatus = objOpportunity.EnmDealStatus.DealOpen
                End If
            ElseIf (pageType = PageTypeEnum.AddOppertunity) Then
                objOpportunity.SourceType = 1
            End If

            dsTemp.Tables(2).Rows.Clear()
            dsTemp.AcceptChanges()
            Dim TotalAmount As Decimal = 0
            If Not ViewState("SOItems") Is Nothing Then
                dsTemp = ViewState("SOItems")

                If dsTemp.Tables(0).Rows.Count > 0 Then
                    TotalAmount = CType(dsTemp.Tables(0).Compute("Sum(monTotAmount)", ""), Decimal)
                    objOpportunity.strItems = "<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & dsTemp.GetXml
                End If
            End If

            Dim objAdmin As New CAdmin
            Dim dtBillingTerms As DataTable
            objAdmin.DivisionID = lngDivId
            dtBillingTerms = objAdmin.GetBillingTerms()

            objOpportunity.boolBillingTerms = IIf(dtBillingTerms.Rows(0).Item("tintBillingTerms") = 1, True, False)
            objOpportunity.BillingDays = dtBillingTerms.Rows(0).Item("numBillingDays")
            objOpportunity.boolInterestType = IIf(dtBillingTerms.Rows(0).Item("tintInterestType") = 1, True, False)
            objOpportunity.Interest = dtBillingTerms.Rows(0).Item("fltInterest")
            objOpportunity.UseShippersAccountNo = CCommon.ToBool(chkUseCustomerShippingAccount.Checked)
            objOpportunity.UseMarkupShippingRate = CCommon.ToBool(chkMarkupShippingCharges.Checked)
            objOpportunity.dcMarkupShippingRate = CCommon.ToDecimal(txtMarkupShippingRate.Text)
            objOpportunity.intUsedShippingCompany = CCommon.ToInteger(hdnShipVia.Value)
            objOpportunity.ShippingService = CCommon.ToLong(hdnShippingService.Value)
            objOpportunity.ClassID = CCommon.ToLong(ddlClass.SelectedValue)
            objOpportunity.WillCallWarehouseID = CCommon.ToLong(hdnWillCallWarehouseID.Value)

            Dim objOpp As New MOpportunity
            objOpp.DivisionID = lngDivId
            objOpp.DomainID = objOpportunity.DomainID
            objOpportunity.CampaignID = objOpp.GetCampaignForOrganization()

            'get the value of dyamic orpportunity fields
            Dim objPageControls As New PageControls

            If Not dtOppTableInfo Is Nothing AndAlso dtOppTableInfo.Rows.Count > 0 Then
                For Each dr As DataRow In dtOppTableInfo.Rows
                    If (dr("fld_type") <> "Popup" And dr("fld_type") <> "Label" And dr("fld_type") <> "Website") Then
                        If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False And dr("bitCanBeUpdated") = True Then
                            objPageControls.SetValueForStaticFields(dr, objOpportunity, plhOrderDetail)
                        End If
                    End If
                Next
            End If
            objOpportunity.ShipFromLocation = CCommon.ToLong(radcmbLocation.SelectedValue)
            objOpportunity.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))

            objOpportunity.PromotionCouponCode = txtApplyCouponCode.Text
            objOpportunity.numPromotionId = CCommon.ToLong(hdnApplyCouponCode.Value)

            arrOutPut = objOpportunity.Save
            lngOppId = arrOutPut(0)

            If (lngOppId > 0) Then
                Dim objCustomFields As New CustomFields
                objCustomFields.locId = 2
                objCustomFields.RelId = 0
                objCustomFields.DomainID = DomainID
                objCustomFields.RecordId = lngOppId
                objCustomFields.UserCntID = CLng(Session("UserContactId"))
                objCustomFields.FormId = 90

                Dim ds As DataSet = objCustomFields.GetCustFlds
                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    objPageControls.CustomFieldsTable = ds.Tables(0)
                    objPageControls.SaveCusField(lngOppId, 0, Session("DomainID"), objPageControls.Location.SalesOppotunity, plhOrderDetail)
                End If
            End If

            ' IF Drop Ship Option Selected From Edit ShipTo Address Window
            If (hdnDropShp.Value = "1") Then
                objOpportunity.OpportunityId = lngOppId
                objOpportunity.ShipToType = 2
                objOpportunity.ShipStreet = hdnDropShpStreet.Value
                objOpportunity.ShipCity = hdnDropShpCity.Value
                objOpportunity.ShipState = CCommon.ToLong(hdnDropShpStateID.Value)
                objOpportunity.ShipCountry = CCommon.ToLong(hdnDropShpCountryId.Value)
                objOpportunity.ShipPostal = hdnDropShpPostal.Value
                objOpportunity.IsAltShippingContact = True
                objOpportunity.AltShippingContact = hdnDropShipAltShippingcontact.Value
                objOpportunity.UpdateDropShipToAddressForOrder()
            End If

            ' IF Change Zip is done from Shipping Promotion Rule
            'If (numChangeZipStateId > 0 And txtChangeZip.Text <> "") Then
            '    objOpportunity.OpportunityId = lngOppId
            '    objOpportunity.ShipToType = 2
            '    objOpportunity.ShipStreet = ""
            '    objOpportunity.ShipCity = ""
            '    objOpportunity.ShipState = numChangeZipStateId
            '    objOpportunity.ShipCountry = 0
            '    objOpportunity.ShipPostal = txtChangeZip.Text
            '    objOpportunity.IsAltShippingContact = True
            '    objOpportunity.AltShippingContact = ""
            '    objOpportunity.UpdateDropShipToAddressForOrder()
            'End If

            '''AddingBizDocs
            objOppBizDocs = New OppBizDocs
            If (pageType = PageTypeEnum.Purchase) Then

                'Set Bill To and Ship To based on Global Settings settings
                If objOpportunity.BillToAddressID = 0 And objOpportunity.ShipToAddressID = 0 Then
                    objOpportunity.OpportunityId = lngOppId
                    objOpportunity.BillToType = IIf(Session("DefaultBillToForPO") = 1, 0, 0)
                    objOpportunity.ShipToType = IIf(Session("DefaultShipToForPO") = 1, 0, 0)
                    objOpportunity.UpdateOpportunityLinkingDtls()
                End If

                'Link PO with Project if Source Exist
                'If objOpportunity.Source > 0 Then
                If GetQueryStringVal("Source") <> "" AndAlso GetQueryStringVal("StageID") <> "" Then
                    objOpportunity.OpportunityId = lngOppId
                    objOpportunity.ProjectID = CCommon.ToLong(GetQueryStringVal("Source"))
                    objOpportunity.ManageOppLinking()
                    'Link BizDoc with Project
                    Dim objProject As New Project
                    objProject.ProjectID = CCommon.ToLong(GetQueryStringVal("Source"))
                    objProject.OpportunityId = lngOppId
                    'objProject.OppBizDocID = OppBizDocID
                    objProject.DomainID = Session("DomainID")
                    objProject.StageID = CCommon.ToLong(GetQueryStringVal("StageID"))
                    objProject.SaveProjectOpportunities()
                End If
            ElseIf (pageType = PageTypeEnum.Sales) Then
                Dim ShouldReturn As Boolean
                'Create an Invoice when “Create Invoice” is clicked within New Sales Order
                If IsFromCreateBizDoc = True Then
                    If CCommon.ToBool(hdnIsUnitPriceApprovalRequired.Value) = False Or hdnApprovalActionTaken.Value = "Approved" Then
                        CreateOppBizDoc(objOppBizDocs, ShouldReturn)
                    End If
                End If

                If ShouldReturn Then
                    Return
                End If
            ElseIf (pageType = PageTypeEnum.AddOppertunity) Then
                CreateOppBizDoc(objOppBizDocs, False)
                Try
                    Dim objAlerts As New CAlerts
                    If oppType <> 1 Then
                        'When a Purchase Opportunity is created by a “non” employee, & when a Purchase Order is created by anyone.
                        Dim dtDetails As DataTable
                        objAlerts.AlertDTLID = 22 'Alert DTL ID for sending alerts in opportunities
                        objAlerts.DomainID = Session("DomainID")
                        dtDetails = objAlerts.GetIndAlertDTL
                        If dtDetails.Rows.Count > 0 Then
                            If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
                                Dim dtEmailTemplate As DataTable
                                Dim objDocuments As New DocumentList
                                objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
                                objDocuments.DomainID = Session("DomainID")
                                dtEmailTemplate = objDocuments.GetDocByGenDocID
                                If dtEmailTemplate.Rows.Count > 0 Then
                                    Dim objSendMail As New Email
                                    Dim dtMergeFields As New DataTable
                                    Dim drNew As DataRow
                                    dtMergeFields.Columns.Add("OppID")
                                    dtMergeFields.Columns.Add("DealAmount")
                                    dtMergeFields.Columns.Add("Organization")
                                    dtMergeFields.Columns.Add("ContactName")
                                    drNew = dtMergeFields.NewRow
                                    drNew("OppID") = arrOutPut(1)
                                    drNew("DealAmount") = TotalAmount
                                    drNew("ContactName") = radcmbContact.SelectedItem.Text
                                    drNew("Organization") = radCmbCompany.Text
                                    dtMergeFields.Rows.Add(drNew)

                                    Dim dtEmail As DataTable
                                    objAlerts.AlertDTLID = 22
                                    objAlerts.DomainID = Session("DomainId")
                                    dtEmail = objAlerts.GetAlertEmails
                                    Dim strCC As String = ""
                                    Dim p As Integer
                                    For p = 0 To dtEmail.Rows.Count - 1
                                        strCC = strCC & dtEmail.Rows(p).Item("vcEmailID") & ";"
                                    Next
                                    strCC = strCC.TrimEnd(";")
                                    objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), "", Session("UserEmail"), strCC, dtMergeFields)
                                End If
                            End If
                        End If
                    End If
                    objAlerts.AlertDTLID = 19
                    objAlerts.ContactID = radCmbCompany.SelectedValue ''pass CompanyID eventhough declared as contact
                    If objAlerts.GetCountCompany > 0 Then
                        ' Sending Mail for Key Organization Opportunity
                        Dim dtDetails As DataTable
                        objAlerts.AlertDTLID = 19 'Alert DTL ID for sending alerts in opportunities
                        objAlerts.DomainID = Session("DomainID")
                        dtDetails = objAlerts.GetIndAlertDTL
                        If dtDetails.Rows.Count > 0 Then
                            If dtDetails.Rows(0).Item("tintAlertOn") = 1 Then
                                Dim dtEmailTemplate As DataTable
                                Dim objDocuments As New DocumentList
                                objDocuments.GenDocID = dtDetails.Rows(0).Item("numEmailTemplate")
                                objDocuments.DomainID = Session("DomainID")
                                dtEmailTemplate = objDocuments.GetDocByGenDocID
                                If dtEmailTemplate.Rows.Count > 0 Then
                                    Dim objSendMail As New Email
                                    Dim dtMergeFields As New DataTable
                                    Dim drNew As DataRow
                                    dtMergeFields.Columns.Add("OppID")
                                    dtMergeFields.Columns.Add("DealAmount")
                                    dtMergeFields.Columns.Add("Organization")
                                    drNew = dtMergeFields.NewRow
                                    drNew("OppID") = arrOutPut(1)
                                    drNew("DealAmount") = TotalAmount
                                    drNew("Organization") = radCmbCompany.Text

                                    Dim dtEmail As DataTable
                                    objAlerts.AlertDTLID = 19
                                    objAlerts.DomainID = Session("DomainId")
                                    dtEmail = objAlerts.GetAlertEmails
                                    Dim strCC As String = ""
                                    Dim p As Integer
                                    For p = 0 To dtEmail.Rows.Count - 1
                                        strCC = strCC & dtEmail.Rows(p).Item("vcEmailID") & ";"
                                    Next
                                    strCC = strCC.TrimEnd(";")
                                    dtMergeFields.Rows.Add(drNew)
                                    If objCommon Is Nothing Then objCommon = New CCommon
                                    objCommon.byteMode = 1
                                    objCommon.ContactID = Session("UserContactID")
                                    objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtDetails.Rows(0).Item("tintCCManager") = 1, objCommon.GetManagerEmail, ""), Session("UserEmail"), strCC, dtMergeFields)
                                End If
                            End If
                        End If
                    End If
                Catch ex As Exception
                End Try

                Dim strScript As String = ""
                strScript += "window.opener.reDirectPage('../opportunity/frmOpportunities.aspx?opId=" & arrOutPut(0) & "'); self.close();"
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "clientScript", strScript, True)
            End If
        Catch ex As Exception
            boolFlag = False
            Throw ex
        End Try
    End Sub

    Private Sub LoadProfile()
        Try
            Dim objUserAccess As New UserAccess
            objUserAccess.RelID = radcmbRelationship.SelectedItem.Value
            objUserAccess.DomainID = Session("DomainID")
            radcmbProfile.DataSource = objUserAccess.GetRelProfileD
            radcmbProfile.DataTextField = "ProName"
            radcmbProfile.DataValueField = "numProfileID"
            radcmbProfile.DataBind()
            radcmbProfile.Items.Insert(0, New RadComboBoxItem("---Select One---", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub CreateClone()
        Try
            If GetQueryStringVal("IsClone") <> "" Then
                Dim dtItems As DataTable
                Dim objOpportunity As New OppotunitiesIP
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.OpportunityId = CCommon.ToLong(GetQueryStringVal("OppId"))
                objOpportunity.Mode = 2
                dtItems = objOpportunity.GetOrderItems().Tables(0)
                Dim objItems As New CItems
                Dim ds As New DataSet
                Dim drow As DataRow
                Dim i As Integer = 1
                Dim k As Integer
                Dim dtItemTax As DataTable
                objCommon = New CCommon

                If ViewState("SOItems") Is Nothing Then
                    createSet()
                End If

                UpdateDataTable()
                ds = ViewState("SOItems")
                ds.Tables(0).Clear()
                ds.Tables(1).Clear()
                ds.Tables(2).Clear()
                objCommon = New CCommon
                Dim objItem As New CItems
                Dim strScript As String
                Dim j As Int32 = 1
                For Each dr As DataRow In dtItems.Rows
                    If Not objItem.ValidateItemAccount(CCommon.ToLong(dr("numItemCode")), Session("DomainID"), oppType) = True Then
                        strScript = "<script language='JavaScript'>"
                        strScript += "alert('Please Set Income,Asset,COGs(Expense) Account for " + dr("vcItemName") + " from Administration->Inventory->Item Details.');</script>"

                        Page.RegisterClientScriptBlock("clientScript", strScript)
                        radCmbCompany.SelectedValue = ""
                        radCmbCompany.Text = ""
                        Return
                    End If
                    drow = ds.Tables(0).NewRow()
                    drow("numoppitemtCode") = dr("numoppitemtCode")
                    drow("numItemCode") = dr("numItemCode")
                    drow("CustomerPartNo") = dr("CustomerPartNo")
                    drow("numUnitHour") = dr("numUnitHour")
                    drow("monPrice") = CCommon.ToDouble(dr("monPrice"))
                    drow("monSalePrice") = CCommon.ToDouble(dr("monPrice"))
                    drow("monTotAmount") = CCommon.ToDouble(dr("monTotAmount"))
                    drow("numSourceID") = dr("numSourceID")
                    drow("vcItemDesc") = dr("vcItemDesc")
                    drow("numWarehouseID") = dr("numWarehouseID")
                    drow("vcItemName") = dr("vcItemName")
                    drow("Warehouse") = dr("Warehouse")
                    drow("numWarehouseItmsID") = dr("numWarehouseItmsID")
                    drow("ItemType") = dr("vcType")
                    drow("Attributes") = dr("vcAttributes")
                    drow("AttributeIDs") = dr("vcAttrValues")
                    drow("Op_Flag") = 1 '1 for insertion
                    drow("DropShip") = dr("bitDropShip")
                    drow("bitDiscountType") = dr("bitDiscountType")
                    drow("fltDiscount") = CCommon.ToDouble(dr("fltDiscount"))
                    drow("monTotAmtBefDiscount") = CCommon.ToDouble(dr("monTotAmtBefDiscount"))
                    drow("numUOM") = dr("numUOMId")
                    drow("vcUOMName") = dr("vcUOMName")
                    drow("UOMConversionFactor") = dr("UOMConversionFactor")
                    drow("bitWorkOrder") = dr("bitWorkOrder")
                    drow("charItemType") = dr("charItemType")

                    drow("numVendorWareHouse") = 0
                    drow("numShipmentMethod") = 0
                    drow("numSOVendorId") = dr("numSOVendorId")
                    drow("numProjectID") = 0
                    drow("numProjectStageID") = 0
                    drow("bitIsAuthBizDoc") = False
                    drow("numSortOrder") = j
                    j = j + 1
                    'Calculate Tax 
                    objItems.DomainID = Session("DomainID")
                    objItems.ItemCode = dr("numItemCode")
                    dtItemTax = objItems.ItemTax()
                    Dim strApplicable1 As String = ""
                    For Each dr1 As DataRow In dtItemTax.Rows
                        strApplicable1 = strApplicable1 & dr1("bitApplicable") & ","
                    Next
                    strApplicable1 = strApplicable1 & dr("bitTaxable")
                    Taxable.Value = strApplicable1

                    Dim strApplicable(), strTax() As String
                    strApplicable = Taxable.Value.Split(",")
                    strTax = txtTax.Text.Split(",")
                    drow("TotalTax") = 0
                    For k = 0 To chkTaxItems.Items.Count - 1
                        If strApplicable(k) = True Then
                            Dim decTaxValue As Decimal = CCommon.ToDecimal(strTax(k).Split("#")(0))
                            Dim tintTaxType As Short = CCommon.ToShort(strTax(k).Split("#")(1))

                            If tintTaxType = 2 Then
                                drow("Tax" & chkTaxItems.Items(k).Value) = CCommon.ToDouble(decTaxValue * (dr("numUnitHour") * dr("UOMConversionFactor")))
                            Else
                                drow("Tax" & chkTaxItems.Items(k).Value) = CCommon.ToDouble(decTaxValue * dr("monTotAmount") / 100)
                            End If

                            drow("TotalTax") = CCommon.ToDecimal(drow("TotalTax")) + CCommon.ToDecimal(drow("Tax" & chkTaxItems.Items(k).Value))
                            drow("bitTaxable" & chkTaxItems.Items(k).Value) = True
                        Else
                            drow("Tax" & chkTaxItems.Items(k).Value) = 0
                            drow("bitTaxable" & chkTaxItems.Items(k).Value) = False
                        End If
                    Next
                    i += 1
                    ds.Tables(0).Rows.Add(drow)
                Next
                ds.Tables(0).AcceptChanges()
                ViewState("SOItems") = ds

                UpdateDetails()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindButtons()
        Try
            If (pageType = PageTypeEnum.Purchase Or pageType = PageTypeEnum.AddOppertunity Or pageType = PageTypeEnum.AddEditOrder) Then
                btnSaveOpenOrder.Visible = IIf(pageType = PageTypeEnum.Purchase, True, False)
                btnSaveNew.Visible = False
                btnSaveBizDoc.Visible = False
            End If

            If (pageType = PageTypeEnum.Sales AndAlso objSalesOrderConfiguration.bitDisplaySaveNew) Or objSalesOrderConfiguration.SalesConfigurationID = -1 Then
                btnSaveNew.Visible = True
            Else
                btnSaveNew.Visible = False
            End If

            If (pageType = PageTypeEnum.Sales AndAlso objSalesOrderConfiguration.bitDisplayPayButton) Or objSalesOrderConfiguration.SalesConfigurationID = -1 Then
                btnPay.Visible = True
            Else
                btnPay.Visible = False
            End If

            If (pageType = PageTypeEnum.Sales AndAlso objSalesOrderConfiguration.bitDisplayPOSPay) Or objSalesOrderConfiguration.SalesConfigurationID = -1 Then
                btnPOSPay.Visible = True
            Else
                btnPOSPay.Visible = False
            End If

            If (pageType = PageTypeEnum.Sales AndAlso objSalesOrderConfiguration.bitDisplayCreateInvoice) Or objSalesOrderConfiguration.SalesConfigurationID = -1 Then
                btnSaveBizDoc.Visible = True
            ElseIf pageType = PageTypeEnum.AddOppertunity AndAlso oppType = 1 AndAlso CCommon.ToLong(Session("DefaultSalesOppBizDocID")) > 0 Then
                btnSaveBizDoc.Text = "Create Quote"
                btnSaveBizDoc.Visible = True
            Else
                btnSaveBizDoc.Visible = False
            End If

            lnkBillTo.Attributes.Add("onclick", "return OpenAddressWindow('BillTo')")
            lnkShipTo.Attributes.Add("onclick", "return OpenAddressWindow('ShipTo')")
            btnSave.Attributes.Add("onclick", "return Save(" & oppType & ")")

            If (oppType = 1) Then
                hplUnitCost.Attributes.Add("onclick", "return openVendorCostTable('1')")
                'hplPrice.Text = "Unit Price"
            End If

            btnSaveNew.Attributes.Add("onclick", "return Save(" & oppType & ")")
            btnSaveBizDoc.Attributes.Add("onclick", "return Save(" & oppType & ")")
            btnSaveOpenOrder.Attributes.Add("onclick", "return Save(" & oppType & ")")

            btnPay.Attributes.Add("onclick", "return Save(" & oppType & ")")
            btnPOSPay.Attributes.Add("onclick", "return Save(" & oppType & ")")
            btnCreditCard.Attributes.Add("onclick", "return Save(" & oppType & ")")
            btnCash.Attributes.Add("onclick", "return Save(" & oppType & ")")
            btnCheck.Attributes.Add("onclick", "return Save(" & oppType & ")")
            btnBillMe.Attributes.Add("onclick", "return Save(" & oppType & ")")
            btnOthers.Attributes.Add("onclick", "return Save(" & oppType & ")")

            Dim objRpt As New BACRM.BusinessLogic.Reports.PredefinedReports
            Dim dtBizDoc As DataTable

            objRpt.BizDocID = CCommon.ToLong(Session("AuthoritativeSalesBizDoc"))
            dtBizDoc = objRpt.GetBizDocType()
            If dtBizDoc.Rows.Count > 0 Then
                btnSave.Text = "Save & Create " + CCommon.ToString(dtBizDoc.Rows(0)("vcData")) & " "
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SaveTaxTypes()
        Try
            Dim dtCompanyTaxTypes As New DataTable
            dtCompanyTaxTypes.Columns.Add("numTaxItemID")
            dtCompanyTaxTypes.Columns.Add("bitApplicable")
            Dim dr As DataRow
            Dim i As Integer
            For i = 0 To chkTaxItems.Items.Count - 1
                If chkTaxItems.Items(i).Selected = True Then
                    dr = dtCompanyTaxTypes.NewRow
                    dr("numTaxItemID") = chkTaxItems.Items(i).Value
                    dr("bitApplicable") = 1
                    dtCompanyTaxTypes.Rows.Add(dr)
                End If
            Next
            Dim ds As New DataSet
            Dim strdetails As String
            dtCompanyTaxTypes.TableName = "Table"
            ds.Tables.Add(dtCompanyTaxTypes)
            strdetails = ds.GetXml
            ds.Tables.Remove(ds.Tables(0))
            Dim objProspects As New CProspects
            objProspects.DivisionID = lngDivId
            objProspects.strCompanyTaxTypes = strdetails
            objProspects.ManageCompanyTaxTypes()
            ds.Dispose()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub clearControls()
        hdnCurrentSelectedItem.Value = ""
        hdnIsMatrix.Value = ""
        hdnKitChildItems.Value = ""
        hdnHasKitAsChild.Value = ""
        hdnIsSKUMatch.Value = ""
        txtUnits.Text = ""
        txtprice.Text = ""
        lblBaseUOMName.Text = ""
        txtdesc.Text = ""
        txtCustomerPartNo.Text = ""
        radcmbType.SelectedIndex = 0
        lblItemType.Text = ""
        txtItemDiscount.Text = ""
        chkUsePromotion.Checked = False
        radPer.Checked = True
        txtUnits.Enabled = True
        If hdnEditUnitPriceRight.Value <> "False" Then
            txtprice.Enabled = True
            hlProfitPerc.Enabled = True
        End If
        txtItemDiscount.Enabled = True
        radcmbUOM.Enabled = True
        hdnItemClassification.Value = ""
        radPer.Enabled = True
        radAmt.Enabled = True

        hplRelatedItems.Visible = False
        hplLastPrices.Visible = False

        radWareHouse.Items.Clear()
        radWareHouse.Text = ""
        radWareHouse.DataSource = Nothing
        radWareHouse.DataBind()
        radWareHouse.Enabled = True

        lblMatrixAndKitAttributes.Text = ""
        gvLocations.DataSource = Nothing
        gvLocations.DataBind()

        gvMultiSelectItems.DataSource = Nothing
        gvMultiSelectItems.DataBind()
        divMultiSelect.Visible = False

        radcmbPriceLevel.Text = ""
        radcmbPriceLevel.SelectedValue = ""
        radcmbPriceLevel.DataSource = Nothing
        radcmbPriceLevel.DataBind()
        radcmbPriceLevel.Enabled = True
        divPriceLevel.Visible = False
        divWarehouse.Visible = True
        chkDropShip.Checked = False
        chkDropShip.Visible = False
        chkDropShip.Enabled = True
        divWorkOrder.Visible = False
        chkWorkOrder.Checked = False
        hdnIsCreateWO.Value = "0"
        txtWOQty.Text = "0"
        txtUOMConversionFactor.Text = ""
        lblBaseUOMName.Text = "-"
        btnOptionsNAccessories.Visible = False
        txtModelID.Text = ""


        txtHidEditOppItem.Text = ""
        hdnItemSalePrice.Value = ""
        hdnUnitCost.Value = "0"
        hdnVendorId.Value = "0"
        lblProfit.Text = ""

        radcmbUOM.SelectedValue = "0"
        radcmbUOM.Enabled = True
        hdnPrimaryVendorID.Value = "0"
        divPricingOption.Visible = False
        lkbPriceLevel.Visible = True
        lkbPriceRule.Visible = True
        lkbLastPrice.Visible = True
        lkbListPrice.Visible = True
        lkbEditKitSelection.Visible = False

        lkbPriceLevel.Text = "Use Price Level"
        lkbPriceRule.Text = "Use Price Rule"
        lkbLastPrice.Text = "Use Last Price"
        lkbListPrice.Text = "Use List Price"
        'lkbAddPromotion.Text = "Use Promotion"
        lkbPriceLevel.Visible = True
        lkbPriceRule.Visible = True
        lkbLastPrice.Visible = True
        lkbListPrice.Visible = True
        hdnListPrice.Value = ""
        hdnLastPrice.Value = ""
        hdnPricingBasedOn.Value = ""
        hdnBaseUOMName.Value = ""
        hdnVendorCost.Value = ""
        hplRelatedItems.Visible = False
        hplLastPrices.Visible = False
        lkbEditKitSelection.Visible = False

        divPromotionDetailRI.Visible = False
        lblPromotionNameRI.Text = ""
        lblPromotionDescriptionRI.Text = ""
        lblExpirationRI.Text = ""
        divCouponCodeRI.Visible = False
        txtCouponCodeRI.Text = ""
        divItemProm.Visible = False
        lblItemPromLongDesc.Text = ""
        UpdateItemPromotion.Update()
        updatePanelItemTransit.Update()

        hdnSelectedItemPromo.Value = ""
        rblItemPromo.Items.Clear()
        divCustomerPart.Style.Remove("display")
        hdnNewItemPromotionID.Value = "0"
        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ClearSelectedItems", "$('#divItem').show(); $('#txtItem').select2('data', null);", True)
    End Sub

    Private Sub UpdateDetails()
        Try
            If dsTemp Is Nothing AndAlso ViewState("SOItems") IsNot Nothing Then
                dsTemp = ViewState("SOItems")
            ElseIf dsTemp Is Nothing AndAlso ViewState("SOItems") Is Nothing Then
                Exit Sub
            End If

            Dim dtAllItems As DataTable
            dtAllItems = dsTemp.Tables(0)
            'If dtAllItems.Rows.Count = 0 Then
            '    If dtRelatedItems IsNot Nothing AndAlso dtRelatedItems.Rows.Count > 0 Then
            '        dtAllItems =dtRelatedItems
            '    End If
            'End If
            If pageType = PageTypeEnum.Sales Then
                Dim strTax As String = ""
                Dim strTaxIds As String = ""

                If divNewCustomer.Visible Then
                    objCommon = New CCommon
                    For Each Item As ListItem In chkTaxItems.Items
                        If Item.Selected = True Then

                            If Session("BaseTaxCalcOn") = 1 Then 'Base tax calculation on Shipping Address(2) or Billing Address(1) 
                                strTax = strTax & objCommon.TaxPercentage(0, ddlBillCountry.SelectedValue, ddlBillState.SelectedValue, Session("DomainID"), Item.Value, txtBillCity.Text, txtBillPostal.Text, Session("BaseTaxOnArea"), Session("BaseTaxCalcOn")) & ","
                            Else
                                strTax = strTax & objCommon.TaxPercentage(0, ddlShipCountry.SelectedValue, ddlShipState.SelectedValue, Session("DomainID"), Item.Value, txtShipCity.Text, txtShipPostal.Text, Session("BaseTaxOnArea"), Session("BaseTaxCalcOn")) & ","
                            End If

                        Else
                            strTax = strTax & "0#1,"
                        End If
                        strTaxIds = strTaxIds & Item.Value & ","
                    Next
                    strTax = strTax.TrimEnd(",")
                    strTaxIds = strTaxIds.TrimEnd(",")
                    txtTax.Text = strTax
                    TaxItemsId.Value = strTaxIds

                Else
                    If radCmbCompany.SelectedValue <> "" Then
                        objCommon = New CCommon
                        Dim dtTaxTypes As DataTable
                        Dim ObjTaxItems As New TaxDetails
                        ObjTaxItems.DomainID = Session("DomainID")
                        dtTaxTypes = ObjTaxItems.GetTaxItems
                        For Each dr As DataRow In dtTaxTypes.Rows

                            'Base tax calculation on Shipping Address(2) or Billing Address(1) 
                            strTax = strTax & objCommon.TaxPercentage(radCmbCompany.SelectedValue, 0, 0, Session("DomainID"), dr("numTaxItemID"), "", "", Session("BaseTaxOnArea"), Session("BaseTaxCalcOn")) & ","
                            strTaxIds = strTaxIds & dr("numTaxItemID") & ","

                        Next
                        strTax = strTax & objCommon.TaxPercentage(radCmbCompany.SelectedValue, 0, 0, Session("DomainID"), 0, "", "", Session("BaseTaxOnArea"), Session("BaseTaxCalcOn"))

                        strTaxIds = strTaxIds & "0"
                        txtTax.Text = strTax
                        TaxItemsId.Value = strTaxIds
                    Else
                        For k As Integer = 0 To chkTaxItems.Items.Count - 1
                            strTax = strTax & "0#1" & ","
                        Next
                        txtTax.Text = strTax
                    End If
                End If
            End If

            If Not dtAllItems Is Nothing AndAlso dtAllItems.Rows.Count > 0 AndAlso pageType = PageTypeEnum.Sales Then
                Dim strTaxes(), strTaxApplicable() As String
                strTaxes = txtTax.Text.Split(",")
                strTaxApplicable = Taxable.Value.Split(",")

                For Each drRow As DataRow In dtAllItems.Rows

                    drRow("TotalTax") = 0

                    For k = 0 To chkTaxItems.Items.Count - 1
                        If dtAllItems.Columns.Contains("bitTaxable" & chkTaxItems.Items(k).Value) Then
                            If IIf(IsDBNull(drRow("bitTaxable" & chkTaxItems.Items(k).Value)), False, drRow.Item("bitTaxable" & chkTaxItems.Items(k).Value)) = True Then
                                Dim decTaxValue As Decimal = CCommon.ToDecimal(strTaxes(k).Split("#")(0))
                                Dim tintTaxType As Short = CCommon.ToShort(strTaxes(k).Split("#")(1))

                                If tintTaxType = 2 Then 'FLAT AMOUNT
                                    drRow("Tax" & chkTaxItems.Items(k).Value) = decTaxValue * (drRow("numUnitHour") * drRow("UOMConversionFactor"))
                                Else 'PERCENTAGE
                                    drRow("Tax" & chkTaxItems.Items(k).Value) = IIf(IsDBNull(drRow("monTotAmount")), 0, drRow("monTotAmount")) * decTaxValue / 100
                                End If

                                drRow("TotalTax") = CCommon.ToDecimal(drRow("TotalTax")) + CCommon.ToDecimal(drRow("Tax" & chkTaxItems.Items(k).Value))
                            End If
                        End If
                    Next
                Next
            End If

            UseItemPromotionIfAvailable(dtAllItems, False, "")

            dgItems.DataSource = dtAllItems
            dgItems.DataBind()
            'Session("dtAllItems") = dtAllItems

            If dtAllItems.Rows.Count > 0 Then
                Dim decGrandTotal As Decimal
                Dim discount As Decimal
                If dtAllItems.Columns.Contains("TotalDiscountAmount") Then
                    discount = IIf(IsDBNull(dtAllItems.Compute("sum(TotalDiscountAmount)", "")), 0, dtAllItems.Compute("sum(TotalDiscountAmount)", ""))
                End If
                decGrandTotal = IIf(IsDBNull(dtAllItems.Compute("sum(monTotAmount)", "")), 0, dtAllItems.Compute("sum(monTotAmount)", ""))
                CType(dgItems.Controls(0).Controls(dtAllItems.Rows.Count + 1).FindControl("lblFTotal"), Label).Text = CCommon.GetDecimalFormat(Math.Truncate(10000 * decGrandTotal) / 10000)

                If (pageType = PageTypeEnum.Sales) Then
                    For Each Item As ListItem In chkTaxItems.Items
                        Dim decTax As Decimal = IIf(IsDBNull(dtAllItems.Compute("sum(Tax" & Item.Value & " )", "")), 0, dtAllItems.Compute("sum(Tax" & Item.Value & " )", ""))
                        decGrandTotal = decGrandTotal + decTax

                    Next
                End If

                lblCouponDiscountAmount.Text = String.Format("{0:#,##0.00}", CCommon.ToDecimal(Session("TotalDiscount")))
                lblShippingCost.Text = String.Format("{0:#,##0.00}", CCommon.ToDecimal(GetShippingCharge()))
                lblTotal.Text = String.Format("{0:#,##0.00}", decGrandTotal - CCommon.ToDouble(Session("TotalDiscount")))
                lblDiscountAmount.Text = String.Format("{0:#,##0.00}", discount)
            Else
                lblTotal.Text = String.Format("{0:#,##0.00}", 0)
                lblShippingCost.Text = String.Format("{0:#,##0.00}", 0)
                lblCouponDiscountAmount.Text = String.Format("{0:#,##0.00}", 0)
                lblDiscountAmount.Text = String.Format("{0:#,##0.00}", 0)
            End If
            Dim strBaseCurrency As String = ""

            If (Session("MultiCurrency") = True) And (pageType <> PageTypeEnum.AddEditOrder) Then
                lblCurrencyTotal.Text = "Grand Total:"
                lblCouponDiscount.Text = "Coupon Discount:"
                lblShippingCharges.Text = "Shipping Charges:"
                lblDiscountTitle.Text = "Discount:"

                lblCouponCurrency.Text = IIf(radcmbCurrency.SelectedValue > 0, radcmbCurrency.SelectedItem.Text.Substring(0, 3), "")
                lblShippingCostCurrency.Text = IIf(radcmbCurrency.SelectedValue > 0, radcmbCurrency.SelectedItem.Text.Substring(0, 3), "")
                lblTotalCurrency.Text = IIf(radcmbCurrency.SelectedValue > 0, radcmbCurrency.SelectedItem.Text.Substring(0, 3), "")
                lblDiscountCurrency.Text = IIf(radcmbCurrency.SelectedValue > 0, radcmbCurrency.SelectedItem.Text.Substring(0, 3), "")
            Else
                lblCurrencyTotal.Text = "Grand Total:"
                lblCouponDiscount.Text = "Coupon Discount:"
                lblShippingCharges.Text = "Shipping Charges:"
                lblDiscountTitle.Text = "Discount:"
            End If

            Dim totalWeight As Double = 0
            For Each dr As DataRow In dtAllItems.Rows
                totalWeight = totalWeight + (CCommon.ToDouble(dr("fltItemWeight")) * CCommon.ToDouble(dr("numUnitHour")))
            Next

            txtTotalWeight.Text = totalWeight
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindPriceLevel(ByVal itemCode As Long)
        Try
            If oppType = 1 Then
                Dim dtItemsPriceLevel As New DataTable
                Dim dtItemsPriceRule As New DataTable

                objItems = New CItems

                objItems.ItemCode = itemCode
                objItems.DomainID = Session("DomainID")

                If Not radWareHouse.SelectedValue = "" Then
                    objItems.WareHouseItemID = radWareHouse.SelectedValue
                Else
                    objItems.WareHouseItemID = Nothing
                End If

                If (pageType = PageTypeEnum.Sales Or pageType = PageTypeEnum.Purchase Or pageType = PageTypeEnum.AddOppertunity) Then
                    objItems.DivisionID = IIf(radCmbCompany.SelectedValue = "", 0, radCmbCompany.SelectedValue)
                Else
                    objItems.DivisionID = CCommon.ToLong(GetQueryStringVal("DivId"))
                End If

                Dim dsPriceLevel As DataSet = objItems.GetPriceLevelItemPrice()
                dtItemsPriceLevel = dsPriceLevel.Tables(0)

                radcmbPriceLevel.Items.Clear()
                radcmbPriceLevel.Items.Insert(0, New RadComboBoxItem("--Select One--", "0"))

                divPriceLevel.Visible = False

                Dim Litem As New RadComboBoxItem

                If dtItemsPriceLevel.Rows.Count > 0 Then
                    Dim item As RadComboBoxItem = New RadComboBoxItem()
                    item.Text = "Price Table"
                    item.IsSeparator = True
                    radcmbPriceLevel.Items.Add(item)

                    For Each dr As DataRow In dtItemsPriceLevel.Rows
                        Litem = New RadComboBoxItem()

                        If CCommon.ToInteger(dr("intFromQty")) = 0 AndAlso CCommon.ToInteger(dr("intToQty")) = 0 AndAlso Not String.IsNullOrEmpty(dr("vcName")) Then
                            Litem.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(dr("decDiscount"))) & " - " & CCommon.ToString(dr("vcName"))
                        Else
                            Litem.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(dr("decDiscount"))) & " (" & CCommon.ToLong(dr("intFromQty")) & "-" & CCommon.ToLong(dr("intToQty")) & ")"
                        End If

                        Litem.Value = dr("decDiscount")

                        If hdnEditUnitPriceRight.Value = "False" Then
                            If CCommon.ToLong(txtUnits.Text) >= CCommon.ToLong(dr("intFromQty")) AndAlso CCommon.ToLong(txtUnits.Text) <= CCommon.ToLong(dr("intToQty")) Then
                                Litem.Enabled = True
                            Else
                                Litem.Enabled = False
                            End If
                        End If

                        radcmbPriceLevel.Items.Add(Litem)
                    Next
                End If

                dtItemsPriceRule = dsPriceLevel.Tables(1)

                If dtItemsPriceRule.Rows.Count > 0 Then
                    Dim item As RadComboBoxItem = New RadComboBoxItem()
                    item.Text = "Price Rule"
                    item.IsSeparator = True
                    radcmbPriceLevel.Items.Add(item)

                    For Each dr As DataRow In dtItemsPriceRule.Rows
                        Litem = New RadComboBoxItem()
                        Litem.Text = CCommon.GetDecimalFormat(CCommon.ToDecimal(dr("decDiscount"))) & " (" & CCommon.ToLong(dr("intFromQty")) & "-" & CCommon.ToLong(dr("intToQty")) & ")"
                        Litem.Value = dr("decDiscount")

                        If hdnEditUnitPriceRight.Value = "False" Then
                            If CCommon.ToLong(txtUnits.Text) >= CCommon.ToLong(dr("intFromQty")) AndAlso CCommon.ToLong(txtUnits.Text) <= CCommon.ToLong(dr("intToQty")) Then
                                Litem.Enabled = True
                            Else
                                Litem.Enabled = False
                            End If
                        End If

                        radcmbPriceLevel.Items.Add(Litem)
                    Next
                End If

                radcmbPriceLevel.ClearSelection()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub UpdateDataTable()
        Try
            If Not ViewState("SOItems") Is Nothing Then
                dsTemp = ViewState("SOItems")
                Dim dtItem As DataTable
                dtItem = dsTemp.Tables(0)
                If dtItem.Rows.Count > 0 Then
                    Dim i, k As Integer
                    i = 0

                    Dim strTaxes(), strTaxApplicable() As String

                    If (pageType = PageTypeEnum.Sales) Then
                        strTaxes = txtTax.Text.Split(",")
                        strTaxApplicable = Taxable.Value.Split(",")
                    End If

                    For Each dgGridItem As DataGridItem In dgItems.Items
                        dtItem.Rows(i).Item("vcItemName") = CCommon.ToString(CType(dgGridItem.FindControl("txtItemName"), TextBox).Text)
                        dtItem.Rows(i).Item("numUnitHour") = Math.Abs(CCommon.ToDecimal(CType(dgGridItem.FindControl("txtUnits"), TextBox).Text))

                        dtItem.Rows(i).Item("monPrice") = CCommon.ToDecimal(CType(dgGridItem.FindControl("hdnUnitPrice"), HiddenField).Value)
                        dtItem.Rows(i).Item("monTotAmtBefDiscount") = (dtItem.Rows(i).Item("numUnitHour") * dtItem.Rows(i).Item("UOMConversionFactor")) * dtItem.Rows(i).Item("monPrice")
                        dtItem.Rows(i).Item("monSalePrice") = CCommon.ToDecimal(CType(dgGridItem.FindControl("hdnUnitSalePrice"), HiddenField).Value)
                        dtItem.Rows(i).Item("fltDiscount") = CCommon.ToDecimal(CType(dgGridItem.FindControl("txtTotalDiscount"), TextBox).Text)
                        dtItem.Rows(i).Item("bitDiscountType") = CCommon.ToBool(CType(dgGridItem.FindControl("txtDiscountType"), TextBox).Text)

                        If dtItem.Rows(i).Item("bitDiscountType") = 0 Then
                            If CCommon.ToDouble(dtItem.Rows(i).Item("fltDiscount")) = 0 Then
                                dtItem.Rows(i).Item("monTotAmount") = dtItem.Rows(i).Item("numUnitHour") * dtItem.Rows(i).Item("UOMConversionFactor") * dtItem.Rows(i).Item("monSalePrice")
                            Else
                                ' Markup,Discount logic change
                                If (CCommon.ToString(dtItem.Rows(i).Item("bitMarkupDiscount")) = "1") Then
                                    dtItem.Rows(i).Item("monTotAmount") = dtItem.Rows(i).Item("monTotAmtBefDiscount") + Math.Round(dtItem.Rows(i).Item("monTotAmtBefDiscount") * dtItem.Rows(i).Item("fltDiscount") / 100, 4)
                                Else
                                    dtItem.Rows(i).Item("monTotAmount") = dtItem.Rows(i).Item("monTotAmtBefDiscount") - Math.Round(dtItem.Rows(i).Item("monTotAmtBefDiscount") * dtItem.Rows(i).Item("fltDiscount") / 100, 4)
                                End If
                            End If
                        Else
                            If CCommon.ToDouble(dtItem.Rows(i).Item("fltDiscount")) = 0 Then
                                dtItem.Rows(i).Item("monTotAmount") = dtItem.Rows(i).Item("numUnitHour") * dtItem.Rows(i).Item("UOMConversionFactor") * dtItem.Rows(i).Item("monSalePrice")
                            Else
                                ' Markup,Discount logic change
                                If (CCommon.ToString(dtItem.Rows(i).Item("bitMarkupDiscount")) = "1") Then
                                    dtItem.Rows(i).Item("monTotAmount") = dtItem.Rows(i).Item("monTotAmtBefDiscount") + dtItem.Rows(i).Item("fltDiscount")
                                Else
                                    dtItem.Rows(i).Item("monTotAmount") = dtItem.Rows(i).Item("monTotAmtBefDiscount") - dtItem.Rows(i).Item("fltDiscount")
                                End If

                            End If
                        End If

                        If dtItem.Rows(i).Item("Op_Flag") = 0 Then
                            dtItem.Rows(i).Item("Op_Flag") = 2
                        End If
                        i = i + 1
                    Next

                    RecalculateContainerQty(dtItem)
                End If

                dsTemp.AcceptChanges()
                ViewState("SOItems") = dsTemp
                CheckLeadTimeNeedPO()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub CheckLeadTimeNeedPO()
        Try
            dsTemp = ViewState("SOItems")
            Dim results = From myRow In dsTemp.Tables(0).AsEnumerable()
                          Where CCommon.ToString(myRow.Field(Of String)("charItemType")) = "P" AndAlso
                              (CCommon.ToDecimal(CCommon.ToString(myRow.Field(Of String)("OnHandAllocation")).Split("/")(0)) +
                              CCommon.ToDecimal(CCommon.ToString(myRow.Field(Of String)("OnHandAllocation")).Split("/")(1))) <
                              CCommon.ToDecimal(myRow.Field(Of Decimal)("numUnitHour"))
                          Select myRow("numItemCode")

            If results.Count > 0 Then
                btnLeadTimesNeedPO.Visible = True
                btnLeadTimesNeedPO.Attributes.Add("onclick", "openLeadTimes('" & String.Join(",", results) & "')")
            Else
                btnLeadTimesNeedPO.Visible = False
            End If
        Catch ex As Exception
            btnLeadTimesNeedPO.Visible = False
            Dim strEx As String
            strEx = ex.ToString()
        End Try
    End Sub

    Private Sub AddSimilarRequiredRow()
        Dim dt As New DataTable()
        Dim objItem As New CItems
        objItem.DomainID = CCommon.ToLong(Session("DomainID"))
        objItem.ParentItemCode = hdnCurrentSelectedItem.Value
        objItem.WarehouseID = CCommon.ToLong(radcmbLocation.SelectedValue)
        dt = objItem.GetSimilarRequiredItem()

        Dim MaxRowOrder As Integer
        MaxRowOrder = dsTemp.Tables(0).Rows.Count

        If dt.Rows.Count > 0 Then
            If dtRelatedItems Is Nothing Then
                dtRelatedItems = New DataTable()
            End If
            'Code to import child item rows in the shared datatable object - Neelam
            If dtRelatedItems IsNot Nothing Then
                If dtRelatedItems.Rows.Count > 0 Then
                    For Each d As DataRow In dt.Rows
                        dtRelatedItems.ImportRow(d)
                    Next
                Else
                    dtRelatedItems = dt
                End If
            End If

            For Each dr As DataRow In dt.Rows

                objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                objItem.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                objItem.byteMode = CCommon.ToShort(oppType)
                objItem.ItemCode = dr("numItemCode")
                'objItem.WareHouseItemID = CCommon.ToLong(radWareHouse.SelectedValue) 'dr("numWareHouseItemID")
                objItem.WareHouseItemID = IIf(IsDBNull(dr("numWareHouseItemID")), 0, CCommon.ToLong(dr("numWareHouseItemID"))) 'dr("numWareHouseItemID")
                objItem.GetItemAndWarehouseDetails()
                objItem.ItemDesc = dr("txtItemDesc")
                objItem.bitHasKitAsChild = 0
                objItem.vcKitChildItems = ""
                objItem.SelectedUOMID = CCommon.ToLong(dr("numSaleUnit"))
                objItem.SelectedUOM = ""
                objItem.AttributeIDs = ""
                objItem.Attributes = ""
                MaxRowOrder = MaxRowOrder + 1
                objItem.numSortOrder = MaxRowOrder
                objItem.numContainer = dr("numContainer")

                If objItem.ItemType = "P" AndAlso IsDuplicate(objItem.ItemCode, objItem.WareHouseItemID, dr("bitAllowDropShip")) Then
                    Continue For
                End If

                If (objItem.numContainer > 0) Then
                    'Dim containerqty As Long = Math.Ceiling((CCommon.ToDouble(txtUnits.Text) * If(CCommon.ToDouble(txtUOMConversionFactor.Text) > 0, CCommon.ToDouble(txtUOMConversionFactor.Text), 1.0)) / objItem.numNoItemIntoContainer)
                    Dim containerqty As Long = Math.Ceiling((1 * If(CCommon.ToDouble(dr("fltUOMConversionFactor")) > 0, CCommon.ToDouble(dr("fltUOMConversionFactor")), 1.0)) / objItem.numNoItemIntoContainer)

                    Dim objContainerItem As New CItems
                    objContainerItem.DomainID = CCommon.ToLong(Session("DomainID"))
                    objContainerItem.WarehouseID = objItem.WarehouseID
                    objContainerItem.ItemCode = objItem.numContainer
                    Dim dtContainer As DataTable = objContainerItem.GetItemWarehouseLocations()

                    If Not dtContainer Is Nothing AndAlso dtContainer.Rows.Count > 0 Then
                        Dim objItem1 As New CItems
                        objItem1.byteMode = CCommon.ToShort(oppType)
                        objItem1.ItemCode = objItem.numContainer
                        objItem1.WareHouseItemID = CCommon.ToLong(radWareHouse.SelectedValue) 'CCommon.ToLong(dtContainer.Rows(0)("numWarehouseItemID"))
                        objItem1.GetItemAndWarehouseDetails()

                        objItem1.bitHasKitAsChild = False
                        objItem1.vcKitChildItems = ""
                        MaxRowOrder = MaxRowOrder + 1
                        objItem1.numSortOrder = MaxRowOrder
                        objItem1.SelectedUOMID = objItem1.BaseUnit
                        objItem1.SelectedQuantity = containerqty
                        objItem1.AttributeIDs = ""
                        objItem1.Attributes = ""
                        objItem1.numNoItemIntoContainer = -1

                        If objItem.ItemType = "P" AndAlso IsDuplicate(objItem1.ItemCode, objItem1.WareHouseItemID, dr("bitAllowDropShip")) Then
                            If objItem1.numNoItemIntoContainer = -1 Then
                                Dim dsContainer As DataSet
                                dsContainer = ViewState("SOItems")

                                Dim itemQtyInContainer As Double = 0
                                Dim containerMaxQty As Double = 0


                                Dim arrContainers As DataRow() = dsContainer.Tables(0).Select("numItemCode=" & objItem1.ItemCode & " AND numWarehouseItmsID=" & objItem1.WareHouseItemID)
                                If Not arrContainers Is Nothing AndAlso arrContainers.Length > 0 Then
                                    For Each drSameContainer As DataRow In arrContainers
                                        containerMaxQty = containerMaxQty + CCommon.ToDouble(drSameContainer("numUnitHour") * objItem.numNoItemIntoContainer)
                                    Next
                                End If

                                'Check if there are items exists which uses same container and has same qty which can be added to container and container qty is not fully used
                                Dim arrItems As DataRow() = dsContainer.Tables(0).Select("numContainer=" & objItem1.ItemCode & " AND numWarehouseID=" & objItem1.WarehouseID & " AND numContainerQty=" & objItem.numNoItemIntoContainer)
                                If Not arrItems Is Nothing AndAlso arrItems.Length > 0 Then
                                    For Each drSameContainer As DataRow In arrItems
                                        itemQtyInContainer = itemQtyInContainer + CCommon.ToDouble(drSameContainer("numUnitHour") * drSameContainer("UOMConversionFactor"))
                                    Next
                                End If

                                If containerMaxQty > 0 AndAlso itemQtyInContainer > 0 Then
                                    Dim itemCanbeAddedToContainer As Double = containerMaxQty - itemQtyInContainer

                                    If itemCanbeAddedToContainer < CCommon.ToDouble(txtUnits.Text) * If(CCommon.ToDouble(txtUOMConversionFactor.Text) > 0, CCommon.ToDouble(txtUOMConversionFactor.Text), 1.0) Then
                                        containerqty = containerqty - (CCommon.ToDouble(txtUnits.Text) * If(CCommon.ToDouble(txtUOMConversionFactor.Text) > 0, CCommon.ToDouble(txtUOMConversionFactor.Text), 1.0) - itemCanbeAddedToContainer)
                                        objItem1.SelectedQuantity = containerqty
                                        Dim drContainer As DataRow() = dsContainer.Tables(0).Select("numItemCode=" & objItem1.ItemCode & " AND numWarehouseItmsID=" & objItem1.WareHouseItemID)
                                        For Each drowContainer As DataRow In drContainer
                                            drowContainer("numUnitHour") = (drowContainer("numUnitHour")) + objItem1.SelectedQuantity
                                        Next
                                    End If
                                Else
                                    Dim drContainer As DataRow() = dsContainer.Tables(0).Select("numItemCode=" & objItem1.ItemCode & " AND numWarehouseItmsID=" & objItem1.WareHouseItemID)
                                    For Each drowContainer As DataRow In drContainer
                                        drowContainer("numUnitHour") = (drowContainer("numUnitHour")) + objItem1.SelectedQuantity
                                    Next
                                End If

                                AddRelatedItemToSession(objItem, True, dr("fltUOMConversionFactor"))
                                ViewState("SOItems") = dsContainer
                                dsTemp = dsContainer
                            End If
                        Else
                            AddRelatedItemToSession(objItem, True, dr("fltUOMConversionFactor"))
                            AddRelatedItemToSession(objItem1, True, dr("fltUOMConversionFactor"))
                        End If

                    Else
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "DuplicateItemWarehouse", "alert(""Item associated container item does not contain warehouse. Please add warehouse to container first."")", True)
                        Return
                    End If

                Else
                    AddRelatedItemToSession(objItem, True, dr("fltUOMConversionFactor"))
                End If
            Next
        End If
    End Sub

    Private Sub AddRelatedItemToSession(objItem As CItems, ByVal bitFromAddClick As Boolean, ByVal UOMConversionFactor As Double)
        Try
            objCommon = New CCommon
            objCommon.DomainID = CCommon.ToLong(Session("DomainID"))

            Dim units As Double
            Dim price As Decimal
            Dim salePrice As Decimal
            Dim discount As Decimal = 0

            Dim isDiscountInPer As Boolean = False

            If objItem.numNoItemIntoContainer = -1 Then
                units = CCommon.ToDouble(objItem.SelectedQuantity)
                price = CCommon.ToDecimal(objItem.Price)
                salePrice = CCommon.ToDecimal(0)
            Else
                units = 1 'CCommon.ToDouble(txtUnits.Text)
                price = CCommon.ToDecimal(objItem.ListPrice)
                salePrice = CCommon.ToDecimal(objItem.ListPrice)
                discount = 0
                isDiscountInPer = radPer.Checked
                If lngShippingItemCode = objItem.ItemCode Then
                    salePrice = price
                    discount = 0
                    isDiscountInPer = False
                    objItem.Price = price
                ElseIf objItem.ItemCode = CCommon.ToLong(Session("DiscountServiceItem")) Then
                    salePrice = price
                    discount = 0
                    isDiscountInPer = False
                    objItem.Price = price
                    objItem.ItemDesc = txtdesc.Text
                End If
            End If

            'customerPartNo = CCommon.ToInteger(objItem.CustomerPartNo)
            'Start - Get Tax Detail
            Dim strApplicable As String
            Dim dtItemTax As DataTable

            If (pageType = PageTypeEnum.Sales) Then
                dtItemTax = objItem.ItemTax()

                For Each dr As DataRow In dtItemTax.Rows
                    strApplicable = strApplicable & dr("bitApplicable") & ","
                Next

                strApplicable = strApplicable & objItem.Taxable
            End If
            If objItem.numNoItemIntoContainer = -1 Then
                ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon,
                                                                               dsTemp,
                                                                               True,
                                                                               objItem.ItemType,
                                                                               "",
                                                                               False,
                                                                               objItem.KitParent,
                                                                               objItem.ItemCode,
                                                                               units,
                                                                               price,
                                                                               objItem.ItemDesc, objItem.WareHouseItemID, objItem.ItemName, "", 0, objItem.ModelID, CCommon.ToLong(objItem.SelectedUOMID), If(CCommon.ToLong(objItem.SelectedUOMID) > 0, objItem.SelectedUOM, "-"),
                                                                               1,
                                                                               pageType.ToString(), isDiscountInPer, discount, strApplicable, txtTax.Text, 0, chkTaxItems,
                                                                               0, "", CCommon.ToString(""),
                                                                               CCommon.ToLong(0),
                                                                               CCommon.ToLong(0),
                                                                               numProjectID:=CCommon.ToLong(GetQueryStringVal("Source")),
                                                                               numProjectStageID:=CCommon.ToLong(GetQueryStringVal("StageID")),
                                                                               vcBaseUOMName:=0,
                                                                               numSOVendorId:=CCommon.ToLong(0),
                                                                                strSKU:=objItem.SKU, objItem:=objItem, primaryVendorCost:=0, salePrice:=salePrice, numMaxWOQty:=objItem.numMaxWOQty, vcAttributes:=objItem.Attributes, vcAttributeIDs:=objItem.AttributeIDs, numContainer:=objItem.numContainer, numContainerQty:=objItem.numNoItemIntoContainer,
                                                                               numItemClassification:=CCommon.ToLong(0),
                                                                               numPromotionID:=CCommon.ToLong(0),
                                                                               IsPromotionTriggered:=CCommon.ToBool(0),
                                                                               vcPromotionDetail:=CCommon.ToString(""), numSortOrder:=objItem.numSortOrder, vcVendorNotes:=txtNotes.Text, itemReleaseDate:=GetReleaseDate())
            Else
                ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon,
                                                                               dsTemp,
                                                                               True,
                                                                               objItem.ItemType,
                                                                               "",
                                                                               objItem.AllowDropShip,
                                                                               objItem.KitParent,
                                                                               objItem.ItemCode,
                                                                               units,
                                                                               price,
                                                                               objItem.ItemDesc, objItem.WareHouseItemID, objItem.ItemName, "", 0, objItem.ModelID, objItem.SaleUnit, "",
                                                                              UOMConversionFactor,
                                                                               pageType.ToString(), isDiscountInPer, discount, strApplicable, txtTax.Text, 0, chkTaxItems,
                                                                               0, "", "",
                                                                              0,
                                                                               0,
                                                                               numProjectID:=CCommon.ToLong(GetQueryStringVal("Source")),
                                                                               numProjectStageID:=CCommon.ToLong(GetQueryStringVal("StageID")),
                                                                               vcBaseUOMName:=hdnBaseUOMName.Value,
                                                                               numPrimaryVendorID:=CCommon.ToLong(objItem.VendorID),
                                                                               numSOVendorId:=CCommon.ToLong(hdnPrimaryVendorID.Value),
                                                                                strSKU:=objItem.SKU, objItem:=objItem, primaryVendorCost:=objItem.monCost, salePrice:=salePrice, numMaxWOQty:=objItem.numMaxWOQty, vcAttributes:=objItem.Attributes, vcAttributeIDs:=objItem.AttributeIDs, numContainer:=objItem.numContainer, numContainerQty:=objItem.numNoItemIntoContainer,
                                                                               numItemClassification:=CCommon.ToLong(objItem.ItemClassification),
                                                                               numPromotionID:=0,
                                                                               IsPromotionTriggered:=0,
                                                                               vcPromotionDetail:="", numSortOrder:=objItem.numSortOrder, vcVendorNotes:=txtNotes.Text, itemReleaseDate:=GetReleaseDate())
            End If


            objCommon = Nothing

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub RecalculateContainerQty(ByRef dtItem As DataTable)
        Try
            'FIRST MAKE ALL CONTAINER QTY 0
            Dim arrContainer As DataRow() = dtItem.Select("numContainer > 0")
            For Each drRowC In arrContainer
                Dim arrContainers As DataRow() = dtItem.Select("numItemCode=" & drRowC("numContainer"))

                If Not arrContainers Is Nothing AndAlso arrContainers.Length > 0 Then
                    For Each drCont As DataRow In arrContainers
                        drCont("numUnitHour") = 0
                    Next
                End If
            Next

            'Now calculate container qty again because item qty can be change from grid
            arrContainer = dtItem.Select("numContainer > 0")
            If Not arrContainer Is Nothing AndAlso arrContainer.Length > 0 Then
                Dim dtTempContainer As New DataTable
                dtTempContainer.Columns.Add("numContainer")
                dtTempContainer.Columns.Add("numContainerQty")
                dtTempContainer.Columns.Add("numWarehouseID")

                For Each dr As DataRow In arrContainer
                    If dtTempContainer.Select("numContainer =" & dr("numContainer") & " AND numWarehouseID=" & CCommon.ToLong(dr("numWarehouseID")) & " AND numContainerQty=" & dr("numContainerQty")).Length = 0 Then
                        Dim drTemp As DataRow = dtTempContainer.NewRow
                        drTemp("numContainer") = dr("numContainer")
                        drTemp("numContainerQty") = dr("numContainerQty")
                        drTemp("numWarehouseID") = CCommon.ToLong(dr("numWarehouseID"))
                        dtTempContainer.Rows.Add(drTemp)
                    End If
                Next

                For Each drContainer As DataRow In dtTempContainer.Rows
                    'Check if there are items exists which uses same container and has same qty which can be added to container and container qty is not fully used
                    Dim containerQty As Double = 0
                    Dim arrItems As DataRow() = dtItem.Select(" Dropship = 0 AND numContainer=" & drContainer("numContainer") & " AND numWarehouseID=" & drContainer("numWarehouseID") & " AND numContainerQty=" & drContainer("numContainerQty"))
                    If Not arrItems Is Nothing AndAlso arrItems.Length > 0 Then
                        Dim qty As Double = arrItems.Sum(Function(x) CCommon.ToDouble(x("numUnitHour") * x("UOMConversionFactor")))
                        If qty > CCommon.ToDouble(drContainer("numContainerQty")) Then
                            While qty >= CCommon.ToDouble(drContainer("numContainerQty"))
                                containerQty = containerQty + 1
                                qty = qty - CCommon.ToDouble(drContainer("numContainerQty"))
                            End While

                            If (qty > 0) Then
                                containerQty = containerQty + 1
                            End If
                        Else
                            containerQty = containerQty + 1
                        End If
                    End If

                    Dim drMain As DataRow = dtItem.Select("numItemCode=" & drContainer("numContainer") & " AND numWarehouseID=" & drContainer("numWarehouseID")).FirstOrDefault()

                    If Not drMain Is Nothing Then
                        drMain("numUnitHour") = drMain("numUnitHour") + containerQty
                    End If
                Next
            End If


            'Remove containers with qty 0
            Dim arrMainContainer As DataRow() = dtItem.Select("numContainer > 0")
            If Not arrMainContainer Is Nothing AndAlso arrMainContainer.Length > 0 Then
                For Each dr As DataRow In arrMainContainer
                    Dim arrItems As DataRow() = dtItem.Select("numContainer=" & CCommon.ToLong(dr("numContainer")) & " AND numUnitHour=0")

                    If arrItems.Length > 0 Then
                        For Each drContainer As DataRow In arrItems
                            dtItem.Rows.Remove(drContainer)
                        Next
                    End If
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CalculateProfit()
        If oppType = 1 Then

            Dim m_aryRightsForSalesProfit() As Integer
            m_aryRightsForSalesProfit = GetUserRightsForPage_Other(10, 21)

            If m_aryRightsForSalesProfit(RIGHTSTYPE.VIEW) <> 0 Then
                Dim objItems As New CItems

                Dim ds As DataSet
                Dim dtItemPricemgmt As DataTable

                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")

                Dim decVendorCost, decProfit, decSellPrice As Decimal

                decSellPrice = CCommon.ToDecimal(hdnUnitCost.Value)


                Dim dtUnit As DataTable
                objCommon.DomainID = Session("DomainID")

                objItems.ItemCode = CCommon.ToLong(hdnCurrentSelectedItem.Value)
                objCommon.UnitId = radcmbUOM.SelectedValue
                dtUnit = objCommon.GetItemUOMConversion()

                If Not dtUnit Is Nothing AndAlso dtUnit.Rows.Count > 0 Then
                    hdvPurchaseUOMConversionFactor.Value = dtUnit(0)("PurchaseUOMConversion")
                    lblBaseUOMName.Text = dtUnit(0)("vcBaseUOMName")
                End If

                If btnUpdate.Text = "Update" Then
                    decVendorCost = CCommon.ToDouble(txtPUnitCost.Text)
                Else
                    objItems.ItemCode = CCommon.ToLong(hdnCurrentSelectedItem.Value)
                    objItems.DomainID = Session("DomainID")
                    objItems.DivisionID = IIf(radCmbCompany.SelectedValue.Length > 0, radCmbCompany.SelectedValue, 0)
                    objItems.GetItemDetails()

                    If CCommon.ToInteger(Session("numCost")) = 2 Then 'Vendor Cost
                        decVendorCost = CCommon.ToDecimal(IIf(IsDBNull(objItems.AverageCost), 0, objItems.monCost) * IIf(oppType = 1, hdvPurchaseUOMConversionFactor.Value, 1))
                    ElseIf CCommon.ToInteger(Session("numCost")) = 3 Then 'Product & services cost
                        decVendorCost = CCommon.ToDecimal(IIf(IsDBNull(objItems.AverageCost), 0, objItems.monCost) * IIf(oppType = 1, hdvPurchaseUOMConversionFactor.Value, 1))
                    Else
                        decVendorCost = CCommon.ToDecimal(IIf(IsDBNull(objItems.AverageCost), 0, objItems.AverageCost) * IIf(oppType = 1, hdvPurchaseUOMConversionFactor.Value, 1))
                    End If

                    txtPUnitCost.Text = String.Format("{0:#,##0.00000}", decVendorCost)
                End If

                decProfit = (decSellPrice - decVendorCost)

                lblProfit.Text = String.Format("{0:#,##0.00} / {1:#,##0.00}", decProfit, decSellPrice)
                hlProfitPerc.Text = FormatPercent(decProfit / IIf(decSellPrice = 0, 1, decSellPrice), 2)
            End If
        End If
    End Sub

    Private Sub SaveOpportunity()
        Try
            Dim arrOutPut() As String
            Dim objOpportunity As New OppotunitiesIP
            objOpportunity.OpportunityId = CCommon.ToLong(GetQueryStringVal("opid"))
            objOpportunity.DomainID = Session("DomainID")
            objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            objOpportunity.OpportunityDetails()

            objOpportunity.OpportunityId = CCommon.ToLong(GetQueryStringVal("opid"))
            objOpportunity.PublicFlag = 0
            objOpportunity.UserCntID = Session("UserContactID")
            objOpportunity.DomainID = Session("DomainId")
            objOpportunity.OppType = oppType

            objOpportunity.CouponCode = ""
            objOpportunity.Discount = 0
            objOpportunity.boolDiscType = False
            objOpportunity.UseShippersAccountNo = CCommon.ToBool(chkUseCustomerShippingAccount.Checked)
            objOpportunity.UseMarkupShippingRate = CCommon.ToBool(chkMarkupShippingCharges.Checked)
            objOpportunity.dcMarkupShippingRate = CCommon.ToDecimal(txtMarkupShippingRate.Text)
            objOpportunity.intUsedShippingCompany = CCommon.ToInteger(hdnShipVia.Value)
            objOpportunity.ShippingService = CCommon.ToLong(hdnShippingService.Value)
            objOpportunity.ClassID = CCommon.ToLong(ddlClass.SelectedValue)

            dsTemp.Tables(2).Rows.Clear()
            dsTemp.AcceptChanges()

            If Not ViewState("SOItems") Is Nothing Then
                dsTemp = ViewState("SOItems")
                objOpportunity.strItems = "<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & dsTemp.GetXml
            End If

            'get the value of dyamic orpportunity fields
            Dim objPageControls As New PageControls
            For Each dr As DataRow In dtOppTableInfo.Rows
                If (dr("fld_type") <> "Popup" And dr("fld_type") <> "Label" And dr("fld_type") <> "Website") Then
                    If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False And dr("bitCanBeUpdated") = True Then
                        If dr("vcPropertyName") = "numPartner" Then
                            dr("vcPropertyName") = "numPartnerId"
                        End If
                        objPageControls.SetValueForStaticFields(dr, objOpportunity, plhOrderDetail)
                    End If
                End If
            Next
            If Convert.ToBoolean(objOpportunity.bitIsInitalSalesOrder) = True Then
                If hdnApprovalActionTaken.Value = "Save&Proceed" And Convert.ToBoolean(Session("bitMarginPriceViolated")) = True Then
                    objOpportunity.DealStatus = 0
                End If
                If hdnApprovalActionTaken.Value = "Approved" Then
                    objOpportunity.DealStatus = 1
                End If
            End If
            objOpportunity.ShipFromLocation = CCommon.ToLong(radcmbLocation.SelectedValue)
            objOpportunity.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            arrOutPut = objOpportunity.Save()

            objOpportunity.OpportunityId = arrOutPut(0)

            If (objOpportunity.OpportunityId > 0) Then
                objPageControls.SaveCusField(objOpportunity.OpportunityId, 0, Session("DomainID"), objPageControls.Location.SalesOppotunity, plhOrderDetail)
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub AddItemToSession()
        Try
            Dim objItem As New CItems
            objItem.DomainID = Session("DomainID")
            objItem.byteMode = CCommon.ToShort(oppType)
            objItem.ItemCode = hdnCurrentSelectedItem.Value
            objItem.vcKitChildItems = hdnKitChildItems.Value
            objItem.WareHouseItemID = CCommon.ToLong(radWareHouse.SelectedValue)
            objItem.GetItemAndWarehouseDetails()

            Dim itemReleaseDate As String = GetReleaseDate()

            Dim dr As DataRow = dsTemp.Tables(0).Rows.Find(CCommon.ToLong(txtHidEditOppItem.Text))

            If Not dr Is Nothing Then
                If CCommon.ToBool(dr("bitPromotionDiscount")) AndAlso Not chkUsePromotion.Checked Then
                    dr("numPromotionID") = 0
                    dr("bitPromotionTriggered") = False
                    dr("vcPromotionDetail") = ""
                    dr("bitDisablePromotion") = True
                    dr.AcceptChanges()
                    dsTemp.AcceptChanges()
                End If
            End If

            ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon, dsTemp, False, radcmbType.SelectedValue,
                                                            radcmbType.SelectedItem.Text, chkDropShip.Checked, hdKit.Value, CCommon.ToLong(hdnCurrentSelectedItem.Value),
                                                            Math.Abs(CCommon.ToDecimal(txtUnits.Text)),
                                                            CCommon.ToDecimal(txtprice.Text),
                                                            txtdesc.Text,
                                                            CCommon.ToLong(radWareHouse.SelectedValue), "", radWareHouse.Text,
                                                            CCommon.ToLong(txtHidEditOppItem.Text), txtModelID.Text.Trim,
                                                            CCommon.ToLong(radcmbUOM.SelectedValue),
                                                            If(CCommon.ToLong(radcmbUOM.SelectedValue) > 0, radcmbUOM.Text, "-"),
                                                            If(CCommon.ToDouble(txtUOMConversionFactor.Text) = 0, 1, CCommon.ToDouble(txtUOMConversionFactor.Text)),
                                                            pageType.ToString(), IIf(radPer.Checked, True, False),
                                                            IIf(IsNumeric(txtItemDiscount.Text), txtItemDiscount.Text, 0), CCommon.ToString(Taxable.Value), txtTax.Text, txtCustomerPartNo.Text, chkTaxItems,
                                                            IIf(chkWorkOrder.Checked, 1, 0), txtInstruction.Text, CCommon.ToString(radCalCompliationDate.SelectedDate),
                                                            0,
                                                            CCommon.ToLong(hdnVendorId.Value),
                                                            CCommon.ToLong(GetQueryStringVal("Source")), CCommon.ToLong(GetQueryStringVal("StageID")),
                                                            objItem:=objItem,
                                                            vcBaseUOMName:=lblBaseUOMName.Text,
                                                            numPrimaryVendorID:=CCommon.ToLong(hdnPrimaryVendorID.Value),
                                                            fltItemWeight:=CCommon.ToLong(hdnItemWeight.Value), IsFreeShipping:=CCommon.ToBool(hdnFreeShipping.Value),
                                                            fltHeight:=CCommon.ToDouble(hdnHeight.Value), fltWidth:=CCommon.ToDouble(hdnWidth.Value),
                                                            fltLength:=CCommon.ToDouble(hdnLength.Value), strSKU:=CCommon.ToString(hdnSKU.Value),
                                                            salePrice:=CCommon.ToDecimal(hdnItemSalePrice.Value), numMaxWOQty:=CCommon.ToInteger(lblWOQty.Text),
                                                            numItemClassification:=CCommon.ToLong(hdnItemClassification.Value), numPromotionID:=0,
                                                            IsPromotionTriggered:=False, vcPromotionDetail:="",
                                                            numCost:=CCommon.ToDouble(txtPUnitCost.Text), vcVendorNotes:=txtNotes.Text,
                                                            itemReleaseDate:=itemReleaseDate, ShipFromLocation:=radcmbLocation.SelectedItem.Text,
                                                            numShipToAddressID:=0, ShipToFullAddress:=lblShipTo1.Text, InclusionDetail:=lblMatrixAndKitAttributes.Text,
                                                            bitMarkupDiscount:=ddlMarkupDiscountOption.SelectedValue, bitAlwaysCreateWO:=CCommon.ToBool(hdnIsCreateWO.Value),
                                                            numWOQty:=CCommon.ToDouble(txtWOQty.Text), dtPlannedStart:=CCommon.ToString(rdpPlannedStart.SelectedDate))

            RecalculateContainerQty(dsTemp.Tables(0))
            UpdateDetails()
            clearControls()
            objCommon = Nothing

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindSalesTemplate()
        Try
            If radCmbCompany.SelectedValue.Length > 0 Then
                Dim objOpp As New COpportunities
                Dim dtTemplate As DataTable
                objOpp.DomainID = Session("DomainID")
                objOpp.byteMode = 1
                objOpp.DivisionID = IIf(radCmbCompany.SelectedValue.Length > 0, radCmbCompany.SelectedValue, 0)
                dtTemplate = objOpp.GetSalesTemplate()
                radcmbSalesTemplate.DataTextField = "vcTemplateName"
                radcmbSalesTemplate.DataValueField = "numSalesTemplateID1"
                radcmbSalesTemplate.DataSource = dtTemplate
                radcmbSalesTemplate.DataBind()
                radcmbSalesTemplate.Items.Insert(0, New RadComboBoxItem("--Select One--", "0"))
            Else
                radcmbSalesTemplate.Items.Insert(0, New RadComboBoxItem("--Select One--", "0"))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindMultiCurrency()
        Try
            If Session("MultiCurrency") = True Then
                pnlCurrency.Visible = True
                Dim objCurrency As New CurrencyRates
                objCurrency.DomainID = Session("DomainID")
                objCurrency.GetAll = 0
                radcmbCurrency.DataSource = objCurrency.GetCurrencyWithRates()
                radcmbCurrency.DataTextField = "vcCurrencyDesc"
                radcmbCurrency.DataValueField = "numCurrencyID"
                radcmbCurrency.DataBind()
                Dim item As New RadComboBoxItem
                item.Text = "--Select One--"
                item.Value = 0
                radcmbCurrency.Items.Insert(0, item)
                If Not radcmbCurrency.Items.FindItemByValue(Session("BaseCurrencyID")) Is Nothing Then
                    radcmbCurrency.Items.FindItemByValue(Session("BaseCurrencyID")).Selected = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ClearFinancialStamp()
        Try
            lblBalanceDue.Text = ""
            lblRemaningCredit.Text = ""
            lblTotalAmtPastDue.Text = ""
            lblCreditBalance.Text = ""
            lblCreditLimit.Text = ""
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ClearNewCustomerFields()
        Try
            txtFirstName.Text = ""
            txtLastName.Text = ""
            txtPhone.Text = ""
            txtExt.Text = ""
            txtEmail.Text = ""
            txtCompany.Text = ""
            If radcmbRelationship.Items.Count > 0 AndAlso Not radcmbRelationship.Items.FindItemByValue("0") Is Nothing Then
                radcmbRelationship.SelectedIndex = 0
            End If
            If radcmbNewAssignTo.Items.Count > 0 AndAlso Not radcmbNewAssignTo.Items.FindItemByValue("0") Is Nothing Then
                radcmbNewAssignTo.SelectedIndex = 0
            End If
            If radcmbProfile.Items.Count > 0 AndAlso Not radcmbProfile.Items.FindItemByValue("0") Is Nothing Then
                radcmbProfile.SelectedIndex = 0
            End If
            txtBillStreet.Text = ""
            txtBillCity.Text = ""
            If ddlBillState.Items.Count > 0 AndAlso Not ddlBillState.Items.FindByValue("0") Is Nothing Then
                ddlBillState.SelectedIndex = 0
            End If
            If ddlBillCountry.Items.Count > 0 AndAlso Not ddlBillCountry.Items.FindByValue("0") Is Nothing Then
                ddlBillCountry.SelectedIndex = 0
            End If
            txtBillPostal.Text = ""
            chkShipDifferent.Checked = False
            txtShipStreet.Text = ""
            txtShipCity.Text = ""
            If ddlShipState.Items.Count > 0 AndAlso Not ddlShipState.Items.FindByValue("0") Is Nothing Then
                ddlShipState.SelectedIndex = 0
            End If
            If ddlShipCountry.Items.Count > 0 AndAlso Not ddlShipCountry.Items.FindByValue("0") Is Nothing Then
                ddlShipCountry.SelectedIndex = 0
            End If
            txtShipPostal.Text = ""
        Catch ex As Exception

        End Try

    End Sub

    Private Sub ClearExistingCustomerField()
        Try
            radCmbCompany.Text = ""
            If radcmbContact.Items.Count > 0 AndAlso Not radcmbContact.Items.FindItemByValue("0") Is Nothing Then
                radcmbContact.SelectedIndex = 0
            End If
            If radcmbAssignedTo.Items.Count > 0 AndAlso Not radcmbAssignedTo.Items.FindItemByValue("0") Is Nothing Then
                radcmbAssignedTo.SelectedIndex = 0
            End If
            If radcmbSalesTemplate.Items.Count > 0 AndAlso Not radcmbSalesTemplate.Items.FindItemByValue("0") Is Nothing Then
                radcmbSalesTemplate.SelectedIndex = 0
            End If
            lblBillTo1.Text = ""
            lblBillTo2.Text = ""
            lblShipTo1.Text = ""
            lblShipTo2.Text = ""
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function InsertProspect() As Boolean
        Try

            Dim objLeads As New CLeads
            With objLeads

                If Len(txtCompany.Text) < 1 Then txtCompany.Text = txtLastName.Text & "," & txtFirstName.Text
                .CompanyName = txtCompany.Text.Trim
                radCmbCompany.Text = txtCompany.Text
                .CustName = txtCompany.Text.Trim
                .CRMType = 1
                .DivisionName = "-"
                .LeadBoxFlg = 1
                .UserCntID = Session("UserContactId")
                .Country = Session("DefCountry")
                .SCountry = Session("DefCountry")
                .FirstName = txtFirstName.Text
                .LastName = txtLastName.Text
                .ContactPhone = txtPhone.Text
                .PhoneExt = txtExt.Text
                .Email = txtEmail.Text
                .DomainID = Session("DomainID")
                .ContactType = 0 'commented by chintan, contact type 70 is obsolete. default contact type is 0
                .PrimaryContact = True

                .UpdateDefaultTax = False
                If radcmbRelationship.SelectedIndex > 0 Then .CompanyType = radcmbRelationship.SelectedValue
                If radcmbProfile.SelectedIndex > 0 Then .Profile = radcmbProfile.SelectedValue

                If (pageType = PageTypeEnum.Sales) Then
                    .NoTax = IIf(chkTaxItems.Items.FindByValue(0).Selected = True, False, True)
                End If
            End With
            objLeads.CompanyID = objLeads.CreateRecordCompanyInfo
            lngDivId = objLeads.CreateRecordDivisionsInfo
            objLeads.DivisionID = lngDivId
            lngCntID = objLeads.CreateRecordAddContactInfo()

            'Added By Sachin Sadhu||Date:22ndMay12014
            'Purpose :To Added Organization data in work Flow queue based on created Rules
            '          Using Change tracking
            Dim objWfA As New Workflow()
            objWfA.DomainID = Session("DomainID")
            objWfA.UserCntID = Session("UserContactID")
            objWfA.RecordID = lngDivId
            objWfA.SaveWFOrganizationQueue()
            'end of code

            'Added By Sachin Sadhu||Date:24thJuly2014
            'Purpose :To Added Contact data in work Flow queue based on created Rules
            '         Using Change tracking
            Dim objWF As New Workflow()
            objWF.DomainID = Session("DomainID")
            objWF.UserCntID = Session("UserContactID")
            objWF.RecordID = lngCntID
            objWF.SaveWFContactQueue()
            ' ss//end of code

            If (pageType = PageTypeEnum.Sales) Then
                SaveTaxTypes()
            End If

            Dim objContacts As New CContacts
            objContacts.BillStreet = txtBillStreet.Text
            objContacts.BillCity = txtBillCity.Text
            objContacts.BillCountry = ddlBillCountry.SelectedValue
            objContacts.BillPostal = txtBillPostal.Text
            objContacts.BillState = ddlBillState.SelectedValue
            objContacts.BillingAddress = IIf(chkShipDifferent.Checked, 0, 1)
            objContacts.ShipStreet = IIf(chkShipDifferent.Checked, txtShipStreet.Text, txtBillStreet.Text)
            objContacts.ShipState = IIf(chkShipDifferent.Checked, ddlShipState.SelectedValue, ddlBillState.SelectedValue)
            objContacts.ShipPostal = IIf(chkShipDifferent.Checked, txtShipPostal.Text, txtBillPostal.Text)
            objContacts.ShipCountry = IIf(chkShipDifferent.Checked, ddlShipCountry.SelectedValue, ddlBillCountry.SelectedValue)
            objContacts.ShipCity = IIf(chkShipDifferent.Checked, txtShipCity.Text, txtBillCity.Text)
            objContacts.DivisionID = lngDivId
            objContacts.UpdateCompanyAddress()
        Catch ex As Exception
            boolFlag = False
            Throw ex
        End Try
    End Function

    Private Function GetShippingCharge() As String
        Try
            If radCmbShippingMethod.SelectedIndex > 0 Then
                Dim strShippingCharge As String() = radCmbShippingMethod.SelectedValue.Split("~"c)
                If strShippingCharge.Length > 1 Then
                    Return String.Format("{0:#,##0.00}", CCommon.ToDecimal(strShippingCharge(1)))
                End If
            End If
            Return "0.00"

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub radCmbCompany_SelectedIndexChanged()
        Try
            If CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then

                Dim objOpp As New OppBizDocs
                If objOpp.ValidateARAP(radCmbCompany.SelectedValue, 0, Session("DomainID")) = False Then
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting->Accounts for RelationShip"" To Save' );", True)
                    radCmbCompany.SelectedValue = ""
                    radCmbCompany.Text = ""
                    Exit Sub
                End If
                FillContact(radcmbContact)
                LoadCustomerDetailSection()
                objItems = New CItems
                objItems.DivisionID = radCmbCompany.SelectedValue
                txtDivID.Text = radCmbCompany.SelectedValue
                Me.FillExistingAddress(txtDivID.Text)
                Dim dtTable As DataTable
                dtTable = objItems.GetAmountDue

                Dim strBaseCurrency As String = ""
                If (Session("MultiCurrency") = True) Then
                    strBaseCurrency = Session("Currency") + " "
                End If


                If CCommon.ToLong(dtTable.Rows(0).Item("numPartnerSource")) > 0 Then
                    Dim radcmbPartnerSource As RadComboBox = FindParnerCodeDropDown(plhOrderDetail)

                    If Not radcmbPartnerSource Is Nothing AndAlso Not radcmbPartnerSource.Items.FindItemByValue(dtTable.Rows(0).Item("numPartnerSource")) Is Nothing Then
                        radcmbPartnerSource.Items.FindItemByValue(dtTable.Rows(0).Item("numPartnerSource")).Selected = True
                    End If
                End If

                lblBalanceDue.Text = strBaseCurrency + String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("AmountDueSO"))
                lblRemaningCredit.Text = strBaseCurrency + String.Format("{0:#,##0.00}", If(dtTable.Rows(0).Item("RemainingCredit") < 0, 0, dtTable.Rows(0).Item("RemainingCredit")))
                hdnRemaningCredit.Value = If(dtTable.Rows(0).Item("RemainingCredit") < 0, 0, dtTable.Rows(0).Item("RemainingCredit"))
                lblTotalAmtPastDue.Text = strBaseCurrency + String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("AmountPastDueSO"))
                lblCreditBalance.Text = strBaseCurrency + String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("SCreditMemo"))
                lblCreditLimit.Text = strBaseCurrency + String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("CreditLimit"))
                chkAutoCheckCustomerPart.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitAutoCheckCustomerPart"))

                If CCommon.ToBool(dtTable.Rows(0)("bitUseShippersAccountNo")) Then
                    chkUseCustomerShippingAccount.Checked = True
                End If

                ViewState("PastDueAmount") = dtTable.Rows(0).Item("AmountPastDueSO")
                hdnmonVal1.Value = CCommon.ToDecimal(dtTable.Rows(0).Item("AmountPastDueSO"))
                hdnmonVal2.Value = CCommon.ToDecimal(Session("AmountPastDue"))
                hdnShipperAccountNo.Value = CCommon.ToString(dtTable.Rows(0).Item("vcShippersAccountNo"))

                If Not radShipVia.FindItemByValue(dtTable.Rows(0).Item("intShippingCompany")) Is Nothing Then
                    radShipVia.FindItemByValue(dtTable.Rows(0).Item("intShippingCompany")).Selected = True
                    hdnShipVia.Value = CCommon.ToLong(dtTable.Rows(0).Item("intShippingCompany"))
                End If

                radShipVia_SelectedIndexChanged(Nothing, Nothing)

                If Not radCmbShippingMethod.FindItemByValue(dtTable.Rows(0).Item("numDefaultShippingServiceID")) Is Nothing Then
                    radCmbShippingMethod.FindItemByValue(dtTable.Rows(0).Item("numDefaultShippingServiceID")).Selected = True
                    hdnShippingService.Value = CCommon.ToLong(dtTable.Rows(0).Item("numDefaultShippingServiceID"))
                End If

                If CCommon.ToBool(Session("bitAmountPastDue")) = True AndAlso CCommon.ToDecimal(dtTable.Rows(0).Item("AmountPastDueSO")) > CCommon.ToDecimal(Session("AmountPastDue")) Then
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "BlinkAmount", "setInterval('blinkIt()',500);", True)
                    lblTotalAmtPastDue.ForeColor = Color.Red
                    lblTotalAmtPastDue.Font.Bold = True
                Else
                    lblTotalAmtPastDue.ForeColor = Color.Black
                    lblTotalAmtPastDue.Font.Bold = False

                End If
                objCommon = New CCommon
                Try
                    objCommon.DivisionID = radCmbCompany.SelectedValue
                    objCommon.charModule = "D"
                    objCommon.GetCompanySpecificValues1()

                    SetAssignedToEnabled(objCommon.RecordOwner, objCommon.TerittoryID)

                    If objCommon.CurrencyID > 0 Then
                        If Not radcmbCurrency.Items.FindItemByValue(objCommon.CurrencyID) Is Nothing Then
                            radcmbCurrency.ClearSelection()
                            radcmbCurrency.Items.FindItemByValue(objCommon.CurrencyID).Selected = True
                        End If
                    Else
                        If Not radcmbCurrency.Items.FindItemByValue(Session("BaseCurrencyID")) Is Nothing Then
                            radcmbCurrency.Items.FindItemByValue(Session("BaseCurrencyID")).Selected = True
                        End If
                    End If

                    If CCommon.ToInteger(Session("DefaultClassType")) = 2 AndAlso Not ddlClass.Items.FindByValue(objCommon.AccountClassID) Is Nothing Then
                        ddlClass.Items.FindByValue(objCommon.AccountClassID).Selected = True
                    End If

                    If objCommon.OnCreditHold Then
                        btnAddItem.Visible = False
                        litMessage.Text = "This customer is on credit hold. To reinstate ability to create sales orders the ""On Credit Hold"" check box within the customer’s Accounting sub-tab section must be un-checked."
                    Else
                        btnAddItem.Visible = True
                        litMessage.Text = ""
                    End If

                    If objSalesOrderConfiguration.SalesConfigurationID <> -1 AndAlso objSalesOrderConfiguration.bitAutoAssignOrder Then
                        If Not radcmbAssignedTo.Items.FindItemByValue(CCommon.ToString(Session("UserContactID"))) Is Nothing Then
                            radcmbAssignedTo.SelectedValue = Session("UserContactID")
                        End If
                    Else
                        Dim lngOrgAssignedTo As Long
                        objCommon = New CCommon
                        objCommon.DomainID = Session("DomainID")
                        objCommon.Mode = 14
                        objCommon.Str = radCmbCompany.SelectedValue
                        lngOrgAssignedTo = objCommon.GetSingleFieldValue()

                        If radcmbAssignedTo.Items.FindItemByValue(lngOrgAssignedTo) IsNot Nothing Then
                            radcmbAssignedTo.ClearSelection()
                            radcmbAssignedTo.Items.FindItemByValue(lngOrgAssignedTo).Selected = True
                        Else
                            radcmbAssignedTo.SelectedValue = "0"
                        End If
                    End If
                Catch ex As Exception

                End Try

                BindSalesTemplate()
                OrderPromotion()
                CCommon.UpdateItemRadComboValues("1", radCmbCompany.SelectedValue)

                UpdateDetails()
                CreateClone()


            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Shared Function FindParnerCodeDropDown(control As Control) As Control
        Dim ctrl As Control

        If ctrl Is Nothing Then
            'search the children
            For Each child As Control In control.Controls
                If child.[GetType]() = GetType(RadComboBox) AndAlso child.ID.Contains("Partner Source~") Then
                    ctrl = child
                End If


                If ctrl IsNot Nothing Then
                    Exit For
                End If

                ctrl = FindParnerCodeDropDown(child)
            Next
        End If

        Return ctrl
    End Function

    Private Sub radcmbRelationship_SelectedIndexChanged()
        Try
            UpdateDetails()
            LoadProfile()
            Dim objOpp As New OppBizDocs
            If objOpp.ValidateARAP(0, radcmbRelationship.SelectedValue, Session("DomainID")) = False Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting->Accounting/RelationShip"" To Save' );", True)
                radcmbRelationship.SelectedIndex = 0
                Exit Sub
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function FillContact(ByVal ddlCombo As RadComboBox)
        Try
            Dim fillCombo As New COpportunities
            With fillCombo
                .DivisionID = radCmbCompany.SelectedValue
                ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                ddlCombo.DataTextField = "Name"
                ddlCombo.DataValueField = "numcontactId"
                ddlCombo.DataBind()
            End With
            ddlCombo.Items.Insert(0, New RadComboBoxItem("---Select One---", "0"))
            If ddlCombo.Items.Count >= 2 Then
                ddlCombo.Items(1).Selected = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub FillExistingAddress(ByVal sDivisionId As String)
        Try
            lblBillTo1.Text = ""
            lblShipTo1.Text = ""
            hdnBillAddressID.Value = 0
            hdnShipAddressID.Value = 0
            If Not IsNothing(sDivisionId) Then
                Dim oCOpportunities As New COpportunities
                oCOpportunities.DomainID = CType((Session("DomainID")), Long)
                Dim dtAddress As DataTable = oCOpportunities.GetExistingAddress(CInt(Session("UserID")), CInt(sDivisionId))
                lblBillTo1.Text = CCommon.ToString(dtAddress(0)("vcBillAddress"))
                hdnBillAddressID.Value = CCommon.ToLong(dtAddress(0)("numBillAddress"))

                If (hdnDropShp.Value = "1") Then
                    lblShipTo1.Text = hdnDropShpStreet.Value + "," + hdnDropShpCity.Value + "," + hdnDropShpStateName.Value + "," + hdnDropShpCountryName.Value + "," + hdnDropShpPostal.Value
                    lblShipTo.Text = lblShipTo1.Text
                    hdnShipToState.Value = hdnDropShpStateID.Value
                    hdnShipToCountry.Value = hdnDropShpCountryId.Value
                    hdnShipToPostalCode.Value = hdnDropShpPostal.Value

                    hdnWarehousePriority.Value = "-1"
                Else
                    lblShipTo1.Text = CCommon.ToString(dtAddress(0)("vcShipAddress"))
                    lblShipTo.Text = lblShipTo1.Text
                    hdnShipAddressID.Value = CCommon.ToLong(dtAddress(0)("numShipAddress"))

                    hdnShipToState.Value = CCommon.ToString(dtAddress(0)("numShipToState"))
                    hdnShipToCountry.Value = CCommon.ToString(dtAddress(0)("numShipToCountry"))
                    hdnShipToPostalCode.Value = CCommon.ToString(dtAddress(0)("vcShipToPostal"))

                    If CCommon.ToBool(hdnIsAutoWarehouseSelection.Value) Then
                        Dim vcWarehousePriority As String = ""

                        Dim objMassSalesFulfillmentWM As New MassSalesFulfillmentWarehouseMapping
                        objMassSalesFulfillmentWM.DomainID = CCommon.ToLong(Session("DomainID"))
                        objMassSalesFulfillmentWM.OrderSource = GetOrderSource()
                        objMassSalesFulfillmentWM.SourceType = 1
                        objMassSalesFulfillmentWM.ShipToCountry = CCommon.ToLong(hdnShipToCountry.Value)
                        objMassSalesFulfillmentWM.ShipToState = CCommon.ToLong(hdnShipToState.Value)
                        Dim dt As DataTable = objMassSalesFulfillmentWM.GetWarehousePriority()

                        If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                            For Each dr As DataRow In dt.Rows
                                vcWarehousePriority = vcWarehousePriority & IIf(vcWarehousePriority.Length > 0, ",", "") & CCommon.ToLong(dr("numWarehouseID"))
                            Next

                            hdnWarehousePriority.Value = CCommon.ToString(dt.Rows(0)("vcWarehousePriorities"))
                        Else
                            hdnWarehousePriority.Value = "-1"
                        End If
                    End If
                End If

                hdnCountry.Value = CCommon.ToLong(dtAddress(0)("numShipToCountry"))
                hdnState.Value = CCommon.ToLong(dtAddress(0)("numShipToState"))
                hdnCity.Value = CCommon.ToString(dtAddress(0)("vcShipToCity"))
                hdnPostal.Value = CCommon.ToString(dtAddress(0)("vcShipToPostal"))
                hdnCustomerAddr.Value = CCommon.ToLong(dtAddress(0)("numShipAddress"))
                hdnStateName.Value = CCommon.ToString(dtAddress(0)("vcState"))

                If Not dsTemp Is Nothing AndAlso dsTemp.Tables.Count > 0 Then
                    For Each dr As DataRow In dsTemp.Tables(0).Rows
                        If dsTemp.Tables(0).Columns.Contains("numShipToAddressID") Then
                            dr("numShipToAddressID") = CCommon.ToLong(dtAddress(0)("numShipAddress"))
                        End If
                        If dsTemp.Tables(0).Columns.Contains("ShipToFullAddress") Then
                            dr("ShipToFullAddress") = lblShipTo1.Text
                        End If
                        dr.AcceptChanges()
                    Next
                End If

                UpdateDataTable()
                UpdateDetails()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetOrderSource() As Long
        Try
            Dim orderSource As Long = 0
            Dim radcmbOrderSource As RadComboBox = FindOrderSourceDropDown(plhOrderDetail)

            If Not radcmbOrderSource Is Nothing Then
                orderSource = CCommon.ToLong(radcmbOrderSource.SelectedValue)
            End If

            Return orderSource
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Shared Function FindOrderSourceDropDown(control As Control) As Control
        Dim ctrl As Control

        If ctrl Is Nothing Then
            'search the children
            For Each child As Control In control.Controls
                If child.[GetType]() = GetType(RadComboBox) AndAlso child.ID.StartsWith("97") AndAlso child.ID.EndsWith("~9") Then
                    ctrl = child
                End If


                If ctrl IsNot Nothing Then
                    Exit For
                End If

                ctrl = FindOrderSourceDropDown(child)
            Next
        End If

        Return ctrl
    End Function

    Private Sub FillAssignToDropdown(ByRef ddlCombo As RadComboBox)
        Try
            If Session("PopulateUserCriteria") = 1 Then
                objCommon.sb_FillConEmpFromTerritories(ddlCombo, Session("DomainID"), 0, 0, objCommon.TerittoryID)
            ElseIf Session("PopulateUserCriteria") = 2 Then
                objCommon.sb_FillConEmpFromDBUTeam(ddlCombo, Session("DomainID"), Session("UserContactID"))
            Else : objCommon.sb_FillConEmpFromDBSel(ddlCombo, Session("DomainID"), 0, 0)
            End If

            Dim objUser As New UserAccess
            objUser.DomainID = Session("DomainID")
            objUser.byteMode = 1

            Dim dt As DataTable = objUser.GetCommissionsContacts

            Dim item As New RadComboBoxItem
            Dim dr As DataRow
            For Each dr In dt.Rows
                item = New RadComboBoxItem()
                item.Text = CCommon.ToString(dr("vcUserName"))
                item.Value = CCommon.ToString(dr("numContactID"))
                ddlCombo.Items.Add(item)
            Next

            If objSalesOrderConfiguration.SalesConfigurationID <> -1 AndAlso objSalesOrderConfiguration.bitAutoAssignOrder Then
                If Not radcmbAssignedTo.Items.FindItemByValue(CCommon.ToString(Session("UserContactID"))) Is Nothing Then
                    radcmbAssignedTo.Items.FindItemByValue(CCommon.ToString(Session("UserContactID"))).Selected = True
                End If
            Else
                If CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                    Dim lngOrgAssignedTo As Long
                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.Mode = 14
                    objCommon.Str = radCmbCompany.SelectedValue
                    lngOrgAssignedTo = objCommon.GetSingleFieldValue()

                    If radcmbAssignedTo.Items.FindItemByValue(lngOrgAssignedTo) IsNot Nothing Then
                        radcmbAssignedTo.ClearSelection()
                        radcmbAssignedTo.Items.FindItemByValue(lngOrgAssignedTo).Selected = True
                    Else
                        radcmbAssignedTo.SelectedValue = "0"
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function SaveDataToHeader(ByVal GrandTotal As Decimal) As Long
        Try
            Dim lngJournalId As Long

            Dim objJEHeader As New JournalEntryHeader
            With objJEHeader
                .JournalId = 0
                .RecurringId = 0
                .EntryDate = CDate(Date.UtcNow.Date & " 12:00:00")
                .Description = ""
                .Amount = GrandTotal
                .CheckId = 0
                .CashCreditCardId = 0
                .ChartAcntId = 0
                .OppId = lngOppId
                .OppBizDocsId = OppBizDocID
                .DepositId = 0
                .BizDocsPaymentDetId = 0
                .IsOpeningBalance = 0
                .LastRecurringDate = Date.Now
                .NoTransactions = 0
                .CategoryHDRID = 0
                .ReturnID = 0
                .CheckHeaderID = 0
                .BillID = 0
                .BillPaymentID = 0
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
            End With
            lngJournalId = objJEHeader.Save()
            Return lngJournalId
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function SaveDataToHeader(ByVal p_Amount As Decimal, ByVal p_OppBizDocId As Integer, ByVal p_OppId As Integer, ByVal fltExchangeRate As Decimal) As Integer
        Try
            Dim objJEHeader As New JournalEntryHeader
            Dim lngJournalID As Long
            With objJEHeader
                .JournalId = 0
                .RecurringId = 0
                .EntryDate = DateTime.Now.Date & " 12:00:00" 'Now.UtcNow
                .Description = "Payment received:"
                .Amount = p_Amount * IIf(fltExchangeRate = 0.0, 1, fltExchangeRate)
                .CheckId = 0
                .CashCreditCardId = 0
                .ChartAcntId = 0
                .OppId = p_OppId
                .OppBizDocsId = p_OppBizDocId
                .DepositId = lngDepositeID
                .BizDocsPaymentDetId = 0
                .IsOpeningBalance = 0
                .LastRecurringDate = Date.Now
                .NoTransactions = 0
                .CategoryHDRID = 0
                .ReturnID = 0
                .CheckHeaderID = 0
                .BillID = 0
                .BillPaymentID = 0
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainID")
            End With
            lngJournalID = objJEHeader.Save()
            Return lngJournalID
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function CheckIfColumnExist(ByVal columnName As String) As Boolean
        Dim isColumnExist As Boolean = False

        For Each column As DataGridColumn In dgItems.Columns
            If column.HeaderText = columnName Then
                isColumnExist = True
                Exit For
            End If
        Next

        Return isColumnExist
    End Function

    Private Sub BindAddressByAddressID(ByVal lngAddressID As Long, ByVal AddType As String)
        Try
            Dim objContact As New CContacts
            objContact.DomainID = Session("DomainID")
            objContact.AddressID = lngAddressID
            objContact.byteMode = 1
            Dim dtTable As DataTable = objContact.GetAddressDetail
            If dtTable.Rows.Count > 0 Then
                If AddType = "Bill" Then
                    lblBillTo1.Text = CCommon.ToString(dtTable.Rows(0)("vcFullAddress"))
                    lblBillTo2.Text = ""
                Else
                    lblShipTo1.Text = CCommon.ToString(dtTable.Rows(0)("vcFullAddress"))
                    lblShipTo.Text = CCommon.ToString(dtTable.Rows(0)("vcFullAddress"))
                    lblShipTo2.Text = ""

                    hdnCountry.Value = CCommon.ToLong(dtTable.Rows(0)("numCountry"))
                    hdnState.Value = CCommon.ToLong(dtTable.Rows(0)("numState"))
                    hdnCity.Value = CCommon.ToString(dtTable.Rows(0)("vcCity"))
                    hdnPostal.Value = CCommon.ToString(dtTable.Rows(0)("vcPostalCode"))

                    hdnShipToState.Value = CCommon.ToString(dtTable.Rows(0)("numState"))
                    hdnShipToCountry.Value = CCommon.ToString(dtTable.Rows(0)("numCountry"))
                    hdnShipToPostalCode.Value = CCommon.ToString(dtTable.Rows(0)("vcPostalCode"))

                    If CCommon.ToBool(hdnIsAutoWarehouseSelection.Value) Then
                        Dim vcWarehousePriority As String = ""

                        Dim objMassSalesFulfillmentWM As New MassSalesFulfillmentWarehouseMapping
                        objMassSalesFulfillmentWM.DomainID = CCommon.ToLong(Session("DomainID"))
                        objMassSalesFulfillmentWM.OrderSource = GetOrderSource()
                        objMassSalesFulfillmentWM.SourceType = 1
                        objMassSalesFulfillmentWM.ShipToCountry = CCommon.ToLong(hdnShipToCountry.Value)
                        objMassSalesFulfillmentWM.ShipToState = CCommon.ToLong(hdnShipToState.Value)
                        Dim dt As DataTable = objMassSalesFulfillmentWM.GetWarehousePriority()

                        If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                            For Each dr As DataRow In dt.Rows
                                vcWarehousePriority = vcWarehousePriority & IIf(vcWarehousePriority.Length > 0, ",", "") & CCommon.ToLong(dr("numWarehouseID"))
                            Next

                            hdnWarehousePriority.Value = vcWarehousePriority
                        Else
                            hdnWarehousePriority.Value = "-1"
                        End If
                    End If

                    For Each dr As DataRow In dsTemp.Tables(0).Rows
                        If dsTemp.Tables(0).Columns.Contains("numShipToAddressID") Then
                            dr("numShipToAddressID") = lngAddressID
                        End If
                        If dsTemp.Tables(0).Columns.Contains("ShipToFullAddress") Then
                            dr("ShipToFullAddress") = lblShipTo1.Text
                        End If
                        dr.AcceptChanges()
                    Next
                    UpdateDataTable()
                    UpdateDetails()
                End If
            Else
                lblBillTo1.Text = ""
                lblBillTo2.Text = ""
                lblShipTo1.Text = ""
                lblShipTo2.Text = ""

                For Each dr As DataRow In dsTemp.Tables(0).Rows
                    If dsTemp.Tables(0).Columns.Contains("numShipToAddressID") Then
                        dr("numShipToAddressID") = 0
                    End If
                    If dsTemp.Tables(0).Columns.Contains("ShipToFullAddress") Then
                        dr("ShipToFullAddress") = ""
                    End If
                    dr.AcceptChanges()
                Next
                UpdateDataTable()
                UpdateDetails()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub CreateOppBizDoc(ByVal objOppBizDocs As OppBizDocs, ByRef shouldReturn As Boolean)
        Try
            objOppBizDocs.OppId = lngOppId
            objOppBizDocs.OppType = oppType
            If (pageType = PageTypeEnum.Sales) Then
                shouldReturn = False
            End If

            objOppBizDocs.UserCntID = Session("UserContactID")
            objOppBizDocs.DomainID = Session("DomainID")
            objOppBizDocs.vcPONo = "" 'txtPO.Text
            objOppBizDocs.vcComments = "" 'txtComments.Text
            If Not radcmbBizDocStatus.SelectedValue = "0" Then
                objOppBizDocs.BizDocStatus = CCommon.ToLong(radcmbBizDocStatus.SelectedValue)
            End If


            Dim ddlTemplate As RadComboBox
            ddlTemplate = radcmbSalesTemplate

            If ddlTemplate IsNot Nothing Then
                If Not ddlTemplate.SelectedValue = "0" Then
                    Dim objOpp As New COpportunities
                    objOpp.OpportID = ddlTemplate.SelectedValue.Split(",")(1)
                    objOppBizDocs.DomainID = Session("DomainID")
                    objOppBizDocs.OppBizDocId = objOpp.GetFirstAuthoritativeBizDocId()
                    Dim dsBizDoc As DataSet = objOppBizDocs.GetBizDocsDetails()
                    objOppBizDocs.OppBizDocId = 0
                Else
                    Dim objAdmin As New CAdmin
                    Dim dtBillingTerms As DataTable
                    objAdmin.DivisionID = lngDivId
                    dtBillingTerms = objAdmin.GetBillingTerms()
                End If
            End If

            Dim lngBizDoc As Long = 0

            If (pageType = PageTypeEnum.Purchase) Then
                Select Case IsFromSaveAndOpenOrderDetails
                    Case False
                        lngBizDoc = CCommon.ToLong(Session("DefaultPurchaseBizDocId")) 'objOppBizDocs.GetAuthorizativeOpportuntiy()
                End Select
                If lngBizDoc = 0 Then Exit Sub
            ElseIf (pageType = PageTypeEnum.Sales) Then
                lngBizDoc = objOppBizDocs.GetAuthorizativeOpportuntiy()
            ElseIf (pageType = PageTypeEnum.AddOppertunity) Then
                m_aryRightsForNonAuthoritativ = GetUserRightsForPage_Other(10, 29)
                If oppType = 1 And m_aryRightsForNonAuthoritativ(RIGHTSTYPE.ADD) <> 0 Then
                    lngBizDoc = CCommon.ToLong(Session("DefaultSalesOppBizDocID"))
                End If
                If lngBizDoc = 0 Then Exit Sub
            End If

            If lngBizDoc = 0 Then Exit Sub

            objOppBizDocs.BizDocId = lngBizDoc

            objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            objCommon.Mode = 33
            objCommon.Str = lngBizDoc
            objOppBizDocs.SequenceId = objCommon.GetSingleFieldValue()

            objOppBizDocs.bitPartialShipment = True

            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                OppBizDocID = objOppBizDocs.SaveBizDoc()
                CreateJournalEntry(objOppBizDocs, OppBizDocID, lngBizDoc)

                objTransactionScope.Complete()
            End Using
            ''Added By Sachin Sadhu||Date:1stMay12014
            ''Purpose :To Added Opportunity data in work Flow queue based on created Rules
            ''          Using Change tracking
            Dim objWfA As New Workflow()
            objWfA.DomainID = Session("DomainID")
            objWfA.UserCntID = Session("UserContactID")
            objWfA.RecordID = OppBizDocID
            objWfA.SaveWFBizDocQueue()
            'end of code
        Catch ex As Exception
            boolFlag = False
            Throw ex
        End Try
    End Sub

    Public Function GetProductURL(CategoryName As String, ProductName As String, ItemCode As String) As String
        Try
            Dim strURL As String = Sites.ToString(Session("SitePath")) + "/Product/" + Sites.GenerateSEOFriendlyURL(CategoryName) + "/" + Sites.GenerateSEOFriendlyURL(ProductName) + "/" + ItemCode
            Return strURL
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub CreateJournalEntry(ByVal objOppBizDocs As OppBizDocs, ByVal OppBizDocID As Long, ByVal lngBizDoc As Long)
        Try
            'Create Journals only for authoritative bizdocs
            '-------------------------------------------------
            Dim lintAuthorizativeBizDocsId As Long
            objOppBizDocs.DomainID = Session("DomainID")
            objOppBizDocs.OppId = lngOppId
            objOppBizDocs.UserCntID = Session("UserContactID")
            lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()

            If lintAuthorizativeBizDocsId = lngBizDoc Then
                Dim ds As New DataSet
                objOppBizDocs.OppBizDocId = OppBizDocID
                ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                dtOppBiDocItems = ds.Tables(0)

                Dim objCalculateDealAmount As New CalculateDealAmount

                objCalculateDealAmount.CalculateDealAmount(lngOppId, OppBizDocID, oppType, Session("DomainID"), dtOppBiDocItems)

                ''---------------------------------------------------------------------------------
                JournalId = SaveDataToHeader(objCalculateDealAmount.GrandTotal)
                Dim objJournalEntries As New JournalEntry

                If (pageType = PageTypeEnum.Purchase) Then
                    If CCommon.ToBool(ds.Tables(1).Rows(0).Item("bitPPVariance")) Then
                        objJournalEntries.SaveJournalEntriesPurchaseVariance(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), ds.Tables(1).Rows(0).Item("fltExchangeRateBizDoc"), vcBaseCurrency:=ds.Tables(1).Rows(0).Item("vcBaseCurrency"), vcForeignCurrency:=ds.Tables(1).Rows(0).Item("vcForeignCurrency"))
                    Else
                        objJournalEntries.SaveJournalEntriesPurchase(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"))
                    End If
                ElseIf (pageType = PageTypeEnum.Sales) Then
                    If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                        objJournalEntries.SaveJournalEntriesSales(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), 0)
                    Else
                        objJournalEntries.SaveJournalEntriesSalesNew(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), 0)
                    End If

                    If Session("AutolinkUnappliedPayment") Then
                        Dim objSalesFulfillment As New SalesFulfillmentWorkflow
                        objSalesFulfillment.DomainID = Session("DomainID")
                        objSalesFulfillment.UserCntID = Session("UserContactID")
                        objSalesFulfillment.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                        objSalesFulfillment.OppId = lngOppId
                        objSalesFulfillment.OppBizDocId = OppBizDocID

                        objSalesFulfillment.AutoLinkUnappliedPayment()
                    End If
                ElseIf (pageType = PageTypeEnum.AddOppertunity) Then
                    If oppType = 2 Then
                        If CCommon.ToBool(ds.Tables(1).Rows(0).Item("bitPPVariance")) Then
                            objJournalEntries.SaveJournalEntriesPurchaseVariance(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), ds.Tables(1).Rows(0).Item("fltExchangeRateBizDoc"), vcBaseCurrency:=ds.Tables(1).Rows(0).Item("vcBaseCurrency"), vcForeignCurrency:=ds.Tables(1).Rows(0).Item("vcForeignCurrency"))
                        Else
                            objJournalEntries.SaveJournalEntriesPurchase(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"))
                        End If
                    End If
                End If
            End If
            '------ Send BizDoc Alerts - Notify record owners, their supervisors, and your trading partners when a BizDoc is created, modified, or approved.
            Dim objAlert As New CAlerts
            objAlert.SendBizDocAlerts(lngOppId, OppBizDocID, lngBizDoc, Session("DomainID"), CAlerts.enmBizDoc.IsCreated)

            Dim objAutomatonRule As New AutomatonRule
            objAutomatonRule.ExecuteAutomationRule(49, OppBizDocID, 1)
        Catch ex As Exception
            boolFlag = False
            Throw ex
        End Try
    End Sub

    Private Function IsDuplicate(ByVal ItemCode As String, ByVal WareHouseItemID As String, ByVal DropShip As Boolean) As Boolean
        Try
            If Not CCommon.ToBool(Session("IsAllowDuplicateLineItems")) AndAlso (dsTemp.Tables(0).Select(" numItemCode='" & ItemCode & "'" & IIf(WareHouseItemID <> "", " and numWarehouseItmsID = '" & WareHouseItemID & "'", "") & " and DropShip ='" & DropShip & "'").Length > 0) Then
                UpdateDetails()
                Return True
            End If
            Return False
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub GetShippingMethod(ByVal ShowShippingAmount As Boolean,
                                  ByVal TotalWeight As Decimal,
                                  ByVal TotalAmount As Decimal,
                                  ByVal IsAllCartItemsFreeShipping As Boolean,
                                  ByVal shippingCompanyID As Long,
                                  ByVal shippingService As Long,
                                  ByVal itemReleaseDate As Date,
                                  Optional ByVal dcMarkupRates As Decimal = 1,
                                  Optional ByVal isFlatMarkUp As Boolean = False)
        Dim strErrLog As New StringBuilder

        Dim intCartItemCount As Integer = 0
        Dim dsItems As New DataSet()
        dsItems = If(dsTemp Is Nothing, ViewState("SOItems"), dsTemp)
        Dim dtShippingMethod As New DataTable()
        Dim strMessage As String = ""
        If Not IsAllCartItemsFreeShipping Then
            '#Region "Get Shipping Rule"
            Dim objRule As New ShippingRule()
            objRule.DomainID = CCommon.ToLong(Session("DomainID"))
            objRule.SiteID = -1
            objRule.DivisionID = CCommon.ToLong(txtDivID.Text)
            If CCommon.ToLong(hdnCountry.Value) <> 0 AndAlso
               CCommon.ToLong(hdnState.Value) <> 0 Then
                objRule.CountryID = CCommon.ToLong(hdnCountry.Value)
                objRule.StateID = CCommon.ToLong(hdnState.Value)

            End If

            dtShippingMethod = objRule.GetShippingMethodForItem1()

            '#Region "Get Shipping Method only when one Of Items in Cart having Free Shipping = false"
            If dtShippingMethod.Rows.Count > 0 Then
                If shippingCompanyID = 88 Or shippingCompanyID = 90 Or shippingCompanyID = 91 Then
                    If dtShippingMethod.Select("numShippingCompanyID=" & shippingCompanyID).Length > 0 Then
                        dtShippingMethod = dtShippingMethod.Select("numShippingCompanyID=" & shippingCompanyID).CopyToDataTable()
                    End If
                End If

                If shippingService > 0 AndAlso dtShippingMethod.Select("intNsoftEnum=" & shippingService).Length > 0 Then
                    dtShippingMethod = dtShippingMethod.Select("intNsoftEnum=" & shippingService).CopyToDataTable()
                End If

                'Default Column Add
                Dim newColumn As New System.Data.DataColumn("IsShippingRuleValid", GetType(System.Boolean))
                newColumn.DefaultValue = True
                dtShippingMethod.Columns.Add(newColumn)

                'Add this column to store it as a value field in the dropdown of ddlShippingMethod
                CCommon.AddColumnsToDataTable(dtShippingMethod, "vcServiceTypeID1")
                CCommon.AddColumnsToDataTable(dtShippingMethod, "TransitTime")
                CCommon.AddColumnsToDataTable(dtShippingMethod, "AnticipateDelivery")
                CCommon.AddColumnsToDataTable(dtShippingMethod, "ReleaseDate")
                CCommon.AddColumnsToDataTable(dtShippingMethod, "ExpectedDate")
                CCommon.AddColumnsToDataTable(dtShippingMethod, "ShippingAmount")
                CCommon.AddColumnsToDataTable(dtShippingMethod, "ShippingCarrierImage")

                ' dtShippingMethod = dtShippingMethod.Select("IsValid=TRUE").CopyToDataTable();
                '#Region "foreach start"

                For Each dr As DataRow In dtShippingMethod.Rows
                    Dim decShipingAmount As [Decimal] = 0

                    If Not IsAllCartItemsFreeShipping AndAlso TotalWeight > 0 Then
                        '#Region "Get Shipping Rates"
                        Dim objShipping As New Shipping()
                        objShipping.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                        objShipping.WeightInLbs = CCommon.ToDouble(TotalWeight)
                        objShipping.NoOfPackages = 1
                        objShipping.UseDimentions = If(CCommon.ToInteger(dr("numShippingCompanyID")) = 91, True, False)
                        'for fedex dimentions are must
                        objShipping.GroupCount = 1
                        If objShipping.UseDimentions Then
                            objShipping.Height = objRule.Height
                            objShipping.Width = objRule.Width
                            objShipping.Length = objRule.Length
                        End If

                        Dim objOppBizDocs As New OppBizDocs()
                        objOppBizDocs.ShipCompany = CCommon.ToInteger(dr("numShippingCompanyID"))
                        objOppBizDocs.ShipFromCountry = CCommon.ToLong(hdnShipFromCountry.Value) 'CCommon.ToLong(dtTable.Rows(0)("numWCountry"))
                        objOppBizDocs.ShipFromState = CCommon.ToLong(hdnShipFromState.Value) 'CCommon.ToLong(dtTable.Rows(0)("numWState"))
                        objOppBizDocs.ShipToCountry = CCommon.ToString(hdnShipToCountry.Value)
                        objOppBizDocs.ShipToState = CCommon.ToString(hdnShipToState.Value)
                        objOppBizDocs.strShipFromCountry = ""
                        objOppBizDocs.strShipFromState = ""
                        objOppBizDocs.strShipToCountry = ""
                        objOppBizDocs.strShipToState = ""
                        objOppBizDocs.GetShippingAbbreviation()

                        objShipping.SenderState = objOppBizDocs.strShipFromState
                        objShipping.SenderZipCode = hdnShipFromPostalCode.Value
                        objShipping.SenderCountryCode = objOppBizDocs.strShipFromCountry

                        objShipping.RecepientState = objOppBizDocs.strShipToState
                        objShipping.RecepientZipCode = hdnShipToPostalCode.Value
                        objShipping.RecepientCountryCode = objOppBizDocs.strShipToCountry

                        objShipping.ServiceType = Short.Parse(CCommon.ToString(dr("intNsoftEnum")))
                        objShipping.PackagingType = 31
                        'ptYourPackaging 
                        objShipping.ItemCode = objRule.ItemID
                        objShipping.OppBizDocItemID = 0
                        objShipping.ID = 9999
                        objShipping.Provider = CCommon.ToInteger(dr("numShippingCompanyID"))

                        Dim dtShipRates As DataTable = objShipping.GetRates()

                        If dtShipRates.Rows.Count > 0 Then
                            If CCommon.ToDecimal(dtShipRates.Rows(0)("Rate")) > 0 Then
                                'Currency conversion of shipping rate
                                decShipingAmount = CCommon.ToDecimal(dtShipRates.Rows(0)("Rate")) / If(CCommon.ToDecimal(CCommon.ToString(Session("ExchangeRate"))) = 0, 1, CCommon.ToDecimal(CCommon.ToString(Session("ExchangeRate"))))

                                If CCommon.ToBool(dr("bitMarkupType")) = True AndAlso CCommon.ToDecimal(dr("fltMarkup")) > 0 Then
                                    dr("fltMarkup") = decShipingAmount / CCommon.ToDecimal(dr("fltMarkup"))
                                End If
                                'Percentage
                                decShipingAmount = decShipingAmount + CCommon.ToDecimal(dr("fltMarkup"))
                                If dcMarkupRates > 1 Then
                                    If isFlatMarkUp Then
                                        decShipingAmount = decShipingAmount + dcMarkupRates
                                    Else
                                        decShipingAmount = decShipingAmount + ((decShipingAmount * dcMarkupRates) / 100)
                                    End If
                                End If
                                If (dr("numShippingCompanyID") = "91") Then
                                    dr("ShippingCarrierImage") = "../images/FedEx.png"
                                End If
                                If (dr("numShippingCompanyID") = "88") Then
                                    dr("ShippingCarrierImage") = "../images/UPS.png"
                                End If
                                If (dr("numShippingCompanyID") = "90") Then
                                    dr("ShippingCarrierImage") = "../images/USPS.png"
                                End If
                                dr("ShippingAmount") = decShipingAmount
                                dr("TransitTime") = dtShipRates.Rows(0)("TransitTIme")
                                Dim strExpectedDate As String = "-"
                                If radOrderExpectedDate.SelectedDate IsNot Nothing Then
                                    strExpectedDate = FormattedDateFromDate(radOrderExpectedDate.SelectedDate, Session("DateFormat"))
                                End If
                                dr("AnticipateDelivery") = FormattedDateFromDate(CCommon.ToSqlDate(itemReleaseDate).AddDays(CCommon.ToInteger(dtShipRates.Rows(0)("TransitTIme"))), Session("DateFormat"))
                                dr("ReleaseDate") = FormattedDateFromDate(itemReleaseDate, Session("DateFormat"))
                                dr("ExpectedDate") = strExpectedDate
                                dr("vcServiceTypeID1") = dr("numServiceTypeID").ToString() & "~" & CCommon.ToString(decShipingAmount) & "~" & CCommon.ToString(dr("numRuleID")) & "~" & CCommon.ToString(dr("numShippingCompanyID")) & "~" & CCommon.ToString(dr("vcServiceName")) & "~" & CCommon.ToString(dr("intNsoftEnum"))

                            End If
                        Else
                            dr("IsShippingRuleValid") = False
                        End If
                    Else
                        dr("IsShippingRuleValid") = False
                    End If
                Next
                '#End Region
                Dim drShippingMethod As DataRow() = dtShippingMethod.[Select]("IsShippingRuleValid=true")
                If drShippingMethod.Length > 0 Then
                    dtShippingMethod = dtShippingMethod.[Select]("IsShippingRuleValid=true").CopyToDataTable()
                Else
                    dtShippingMethod = New DataTable()
                End If
            End If
        End If

        '#Region "Bind Shipping Cost"
        If dtShippingMethod.Rows.Count > 0 Then
            dtShippingMethod.Columns.Add("monRate1", GetType(System.Decimal))
            For Each drService As DataRow In dtShippingMethod.Rows
                If CCommon.ToString(drService("vcServiceName")).Contains("-") AndAlso CCommon.ToString(drService("vcServiceName")).Split("-").Length > 0 Then
                    drService("monRate1") = CCommon.ToDecimal(CCommon.ToString(drService("vcServiceName")).Split("-")(1).Trim())
                Else
                    drService("monRate1") = 0
                End If
                dtShippingMethod.AcceptChanges()
            Next


            If ViewState("dtShippingMethod") Is Nothing Then
                ViewState("dtShippingMethod") = dtShippingMethod
            Else
                Dim dtTemp As DataTable = DirectCast(ViewState("dtShippingMethod"), DataTable)
                dtTemp.Merge(dtShippingMethod)
                ViewState("dtShippingMethod") = dtTemp
            End If
        End If

        If strMessage.Length > 0 Then
            Page.MaintainScrollPositionOnPostBack = False
        End If
    End Sub
    Private Property SortDirection As String
        Get
            Return IIf(ViewState("SortDirection") IsNot Nothing, Convert.ToString(ViewState("SortDirection")), "ASC")
        End Get
        Set(value As String)
            ViewState("SortDirection") = value
        End Set
    End Property
    Private Sub BindShippingGrid(Optional ByVal sortExpression As String = Nothing)
        Try
            If Not ViewState("dtShippingMethod") Is Nothing Then
                Dim dtShippingMethod As DataTable = ViewState("dtShippingMethod")
                Dim dvShippingMethod As DataView
                dvShippingMethod = dtShippingMethod.DefaultView
                dvShippingMethod.Sort = "ReleaseDate ASC"
                grdShippingInformation.DataSource = dvShippingMethod
                grdShippingInformation.DataBind()
            Else
                grdShippingInformation.DataSource = Nothing
                grdShippingInformation.DataBind()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Protected Sub OnPageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        Try
            grdShippingInformation.PageIndex = e.NewPageIndex
            Me.BindShippingGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Protected Sub OnSorting(ByVal sender As Object, ByVal e As GridViewSortEventArgs)
        Try
            Me.BindShippingGrid(e.SortExpression)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Private Sub AddItemToSession(objItem As CItems, ByVal bitFromAddClick As Boolean, QunatityinUnits As Double, itemReleaseDate As String, ShipFromLocation As String, numShipToAddressID As Long, ShipToFullAddress As String, OnHandAllocation As String, Optional ByVal MultiSelectItemPrice As Decimal = 0)
        Try
            objCommon = New CCommon
            objCommon.DomainID = CCommon.ToLong(Session("DomainID"))

            Dim units As Double
            Dim price As Decimal
            Dim salePrice As Decimal
            Dim discount As Decimal = 0
            Dim customerPartNo As String
            Dim isDiscountInPer As Boolean = False

            If objItem.numNoItemIntoContainer = -1 Then
                units = CCommon.ToDouble(objItem.SelectedQuantity)
                price = CCommon.ToDecimal(objItem.Price)
                'discount = CCommon.ToDecimal(txtItemDiscount.Text)
                salePrice = CCommon.ToDecimal(0)
            Else
                units = QunatityinUnits 'CCommon.ToDouble(txtUnits.Text)
                price = CCommon.ToDecimal(txtprice.Text)
                salePrice = CCommon.ToDecimal(hdnItemSalePrice.Value)
                discount = CCommon.ToDecimal(txtItemDiscount.Text)
                isDiscountInPer = radPer.Checked
                If lngShippingItemCode = objItem.ItemCode Then
                    If Not bitFromAddClick Then
                        price = CCommon.ToDouble(txtprice.Text)
                    End If

                    salePrice = price
                    discount = 0
                    isDiscountInPer = False
                    objItem.Price = price
                    objItem.ItemDesc = txtdesc.Text
                ElseIf objItem.ItemCode = CCommon.ToLong(Session("DiscountServiceItem")) Then
                    If Not bitFromAddClick Then
                        price = CCommon.ToDouble(txtprice.Text)
                    End If

                    salePrice = price
                    discount = 0
                    isDiscountInPer = False
                    objItem.Price = price
                    objItem.ItemDesc = txtdesc.Text
                End If
            End If
            customerPartNo = objItem.CustomerPartNo
            'Start - Get Tax Detail
            Dim strApplicable As String
            Dim dtItemTax As DataTable

            If (pageType = PageTypeEnum.Sales) Then
                dtItemTax = objItem.ItemTax()

                For Each dr As DataRow In dtItemTax.Rows
                    strApplicable = strApplicable & dr("bitApplicable") & ","
                Next

                strApplicable = strApplicable & objItem.Taxable
            End If

            ''''' Add Unit Price For Multi Select Grid Item
            If (MultiSelectItemPrice > 0) Then
                price = MultiSelectItemPrice
            End If

            If objItem.numNoItemIntoContainer = -1 Then

                ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon,
                                                                               dsTemp,
                                                                               True,
                                                                               objItem.ItemType,
                                                                               "",
                                                                               False,
                                                                               objItem.KitParent,
                                                                               objItem.ItemCode,
                                                                               units,
                                                                               price,
                                                                               objItem.ItemDesc, objItem.WareHouseItemID, objItem.ItemName, "", 0, objItem.ModelID, CCommon.ToLong(objItem.SelectedUOMID), If(CCommon.ToLong(objItem.SelectedUOMID) > 0, objItem.SelectedUOM, "-"),
                                                                               1,
                                                                               pageType.ToString(), isDiscountInPer, discount, strApplicable, "", customerPartNo, chkTaxItems,
                                                                               0, "", CCommon.ToString(""),
                                                                               CCommon.ToLong(0),
                                                                               CCommon.ToLong(0),
                                                                               numProjectID:=CCommon.ToLong(GetQueryStringVal("Source")),
                                                                               numProjectStageID:=CCommon.ToLong(GetQueryStringVal("StageID")),
                                                                               vcBaseUOMName:=0,
                                                                               numSOVendorId:=CCommon.ToLong(0),
                                                                               strSKU:=objItem.SKU, objItem:=objItem, primaryVendorCost:=0, salePrice:=salePrice, numMaxWOQty:=objItem.numMaxWOQty, vcAttributes:=objItem.Attributes, vcAttributeIDs:=objItem.AttributeIDs, numContainer:=objItem.numContainer, numContainerQty:=objItem.numNoItemIntoContainer,
                                                                               numItemClassification:=CCommon.ToLong(0),
                                                                               numPromotionID:=CCommon.ToLong(0),
                                                                               IsPromotionTriggered:=CCommon.ToBool(0),
                                                                               vcPromotionDetail:=CCommon.ToString(""), numSortOrder:=objItem.numSortOrder, vcVendorNotes:=txtNotes.Text, itemReleaseDate:=itemReleaseDate,
                                                                               ShipFromLocation:=radcmbLocation.SelectedItem.Text, InclusionDetail:=lblMatrixAndKitAttributes.Text,
                                                                               OnHandAllocation:=hdnOnHandAllocation.Value, bitMarkupDiscount:=ddlMarkupDiscountOption.SelectedValue, isDisablePromotion:=(CCommon.ToLong(hdnNewItemPromotionID.Value) > 0 AndAlso Not chkUsePromotion.Checked))
            Else
                ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon,
                                                                               dsTemp,
                                                                               True,
                                                                               objItem.ItemType,
                                                                               "",
                                                                               chkDropShip.Checked,
                                                                               objItem.KitParent,
                                                                               objItem.ItemCode,
                                                                               units,
                                                                               price,
                                                                               objItem.ItemDesc, objItem.WareHouseItemID, objItem.ItemName, "", 0, objItem.ModelID, CCommon.ToLong(radcmbUOM.SelectedValue), If(CCommon.ToLong(radcmbUOM.SelectedValue) > 0, radcmbUOM.Text, "-"),
                                                                               If(CCommon.ToDecimal(txtUOMConversionFactor.Text) = 0, 1, CCommon.ToDouble(txtUOMConversionFactor.Text)),
                                                                               pageType.ToString(), isDiscountInPer, discount, strApplicable, txtTax.Text, customerPartNo, chkTaxItems,
                                                                               IIf(chkWorkOrder.Checked, 1, 0), txtInstruction.Text, CCommon.ToString(radCalCompliationDate.SelectedDate),
                                                                               0,
                                                                               CCommon.ToLong(hdnVendorId.Value),
                                                                               numProjectID:=CCommon.ToLong(GetQueryStringVal("Source")),
                                                                               numProjectStageID:=CCommon.ToLong(GetQueryStringVal("StageID")),
                                                                               vcBaseUOMName:=hdnBaseUOMName.Value,
                                                                               numPrimaryVendorID:=CCommon.ToLong(hdnPrimaryVendorID.Value),
                                                                               numSOVendorId:=CCommon.ToLong(hdnPrimaryVendorID.Value),
                                                                               strSKU:=objItem.SKU, objItem:=objItem, primaryVendorCost:=If(hdnVendorCost.Value = "", 0, CCommon.ToDecimal(hdnVendorCost.Value)),
                                                                               salePrice:=salePrice, numMaxWOQty:=objItem.numMaxWOQty, vcAttributes:=objItem.Attributes, vcAttributeIDs:=objItem.AttributeIDs, numContainer:=objItem.numContainer, numContainerQty:=objItem.numNoItemIntoContainer,
                                                                               numItemClassification:=CCommon.ToLong(hdnItemClassification.Value),
                                                                               numPromotionID:=0,
                                                                               IsPromotionTriggered:=False,
                                                                               vcPromotionDetail:="", numSortOrder:=objItem.numSortOrder, numCost:=CCommon.ToDouble(txtPUnitCost.Text), vcVendorNotes:=txtNotes.Text,
                                                                               itemReleaseDate:=itemReleaseDate, ShipFromLocation:=ShipFromLocation, numShipToAddressID:=numShipToAddressID,
                                                                               ShipToFullAddress:=ShipToFullAddress, InclusionDetail:=lblMatrixAndKitAttributes.Text, OnHandAllocation:=OnHandAllocation,
                                                                               bitMarkupDiscount:=ddlMarkupDiscountOption.SelectedValue, numSelectedPromotionID:=CCommon.ToLong(hdnSelectedItemPromo.Value),
                                                                               bitAlwaysCreateWO:=CCommon.ToBool(hdnIsCreateWO.Value), numWOQty:=CCommon.ToDouble(txtWOQty.Text), dtPlannedStart:=CCommon.ToString(rdpPlannedStart.SelectedDate), isDisablePromotion:=(CCommon.ToLong(hdnNewItemPromotionID.Value) > 0 AndAlso Not chkUsePromotion.Checked))
            End If


            objCommon = Nothing

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub LoadItemDetails(itemCode As String)
        Try
            If itemCode <> "" Then

                Dim dtTable, dtItemTax As DataTable
                If objItems Is Nothing Then objItems = New CItems

                Dim strUnit As String = String.Empty

                If CCommon.ToLong(txtHidEditOppItem.Text) > 0 And CCommon.ToLong(GetQueryStringVal("opid")) > 0 Then
                    Dim objOpportunity As New MOpportunity
                    objOpportunity.OppItemCode = CCommon.ToLong(txtHidEditOppItem.Text)
                    objOpportunity.OpportunityId = CCommon.ToLong(GetQueryStringVal("opid"))
                    objOpportunity.DomainID = Session("DomainID")
                    objOpportunity.Mode = 2
                    Dim ds As DataSet = objOpportunity.GetOrderItems()
                    If ds.Tables(0).Rows.Count > 0 Then
                        dtTable = ds.Tables(0)
                        dtTable.Columns("monPrice").ColumnName = "monListPrice"
                        dtTable.Columns("vcPathForTImage").ColumnName = "vcPathForImage"
                        dtTable.Columns("vcItemDesc").ColumnName = "txtItemDesc"
                        dtTable.AcceptChanges()
                        strUnit = "numUOM"
                    Else

                        objItems.ItemCode = CCommon.ToInteger(itemCode)
                        dtTable = objItems.ItemDetails

                        If (oppType = 2) Then
                            strUnit = "numPurchaseUnit"
                        ElseIf (oppType = 1) Then
                            strUnit = "numSaleUnit"
                        End If
                    End If
                Else
                    objItems.ItemCode = CCommon.ToInteger(itemCode)
                    dtTable = objItems.ItemDetails

                    If (oppType = 2) Then
                        strUnit = "numPurchaseUnit"
                    ElseIf (oppType = 1) Then
                        strUnit = "numSaleUnit"
                    End If
                End If

                If (oppType = 1) Then
                    hplUnitCost.Attributes.Add("onclick", "return openSalesVendorCostTable(" + itemCode + ")")
                End If

                Dim strApplicable As String

                If (pageType = PageTypeEnum.Sales) Then
                    objItems.DomainID = Session("DomainID")
                    dtItemTax = objItems.ItemTax()

                    For Each dr As DataRow In dtItemTax.Rows
                        strApplicable = strApplicable & dr("bitApplicable") & ","
                    Next
                End If

                If dtTable.Rows.Count > 0 Then

                    If (pageType = PageTypeEnum.Sales) Then
                        strApplicable = strApplicable & dtTable.Rows(0).Item("bitTaxable")
                        Taxable.Value = strApplicable
                    End If

                    If dtTable.Rows(0).Item("bitKitParent") = True And dtTable.Rows(0).Item("bitAssembly") = False Then
                        hdKit.Value = dtTable.Rows(0).Item("bitKitParent")
                    Else : hdKit.Value = ""
                    End If

                    If oppType = 1 And dtTable.Rows(0).Item("bitAssembly") = True Then
                        divWorkOrder.Visible = True
                    Else
                        divWorkOrder.Visible = False
                    End If

                    radcmbType.SelectedItem.Selected = False
                    lblItemType.Text = ""
                    If radcmbType.Items.FindItemByValue(dtTable.Rows(0).Item("charItemType")) IsNot Nothing Then
                        radcmbType.Items.FindItemByValue(dtTable.Rows(0).Item("charItemType")).Selected = True
                        lblItemType.Text = radcmbType.SelectedItem.Text
                    End If
                    txtdesc.Text = CCommon.ToString(dtTable.Rows(0).Item("txtItemDesc"))
                    txtModelID.Text = CCommon.ToString(dtTable.Rows(0).Item("vcModelID"))
                    txtCustomerPartNo.Text = CCommon.ToString(dtTable.Rows(0).Item("CustomerPartNo"))

                    If dtTable.Rows(0).Item("charItemType") = "P" Then
                        divWarehouse.Visible = True
                        If dtTable.Rows(0).Item("bitSerialized") = True Then
                            txtSerialize.Text = 1
                        Else : txtSerialize.Text = 0
                        End If

                        chkDropShip.Visible = True

                        If (hdnDropShp.Value = "1") Then
                            chkDropShip.Checked = True
                            chkDropShip.Enabled = False
                        Else
                            chkDropShip.Enabled = True
                        End If

                    Else
                        chkDropShip.Visible = False
                        If dtTable.Rows(0).Item("bitSerialized") = True Then
                            txtSerialize.Text = 1
                        Else : txtSerialize.Text = 0
                        End If
                        txtHidValue.Text = ""
                        divWarehouse.Visible = False
                        radWareHouse.SelectedValue = ""
                    End If

                    chkDropShip.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitAllowDropShip"))
                    If chkDropShip.Checked Then
                        divWarehouse.Visible = False
                    End If

                    hdnPrimaryVendorID.Value = CCommon.ToLong(dtTable.Rows(0).Item("numVendorID"))

                    hdnItemWeight.Value = CCommon.ToDouble(dtTable.Rows(0).Item("fltWeight"))
                    hdnFreeShipping.Value = CCommon.ToBool(dtTable.Rows(0).Item("bitFreeShipping"))
                    hdnHeight.Value = CCommon.ToDouble(dtTable.Rows(0).Item("fltHeight"))
                    hdnWidth.Value = CCommon.ToDouble(dtTable.Rows(0).Item("fltWidth"))
                    hdnLength.Value = CCommon.ToDouble(dtTable.Rows(0).Item("fltLength"))
                    hdnSKU.Value = CCommon.ToString(dtTable.Rows(0).Item("vcSKU"))

                    If strUnit.Length > 0 Then
                        If radcmbUOM.Items.FindItemByValue(dtTable.Rows(0).Item(strUnit)) IsNot Nothing Then
                            radcmbUOM.ClearSelection()
                            radcmbUOM.Items.FindItemByValue(dtTable.Rows(0).Item(strUnit)).Selected = True
                        End If
                    End If

                End If
            End If

            UpdatePanelItemDetials.Update()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadLocations()
        Try
            Dim objItem As New CItems
            objItem.DomainID = Session("DomainID")
            Dim dtWareHouse As DataTable
            dtWareHouse = objItem.GetWareHouses()

            If Not dtWareHouse Is Nothing AndAlso dtWareHouse.Rows.Count > 0 Then
                Dim listItem As Telerik.Web.UI.RadComboBoxItem

                For Each dr As DataRow In dtWareHouse.Rows
                    listItem = New RadComboBoxItem()
                    listItem.Text = CCommon.ToString(dr("vcWareHouse"))
                    listItem.Value = CCommon.ToString(dr("numWareHouseID"))
                    listItem.Attributes.Add("Address", CCommon.ToString(dr("vcAddress")))
                    listItem.Attributes.Add("State", CCommon.ToString(dr("numWState")))
                    listItem.Attributes.Add("Country", CCommon.ToString(dr("numWCountry")))
                    listItem.Attributes.Add("PostalCode", CCommon.ToString(dr("vcWPinCode")))
                    listItem.Attributes.Add("numAddressID", CCommon.ToString(dr("numAddressID")))
                    radcmbLocation.Items.Add(listItem)
                Next
            End If

            If Not Session("DefaultWarehouse") Is Nothing AndAlso Not radcmbLocation.Items.FindItemByValue(Session("DefaultWarehouse").ToString()) Is Nothing Then
                radcmbLocation.Items.FindItemByValue(Session("DefaultWarehouse")).Selected = True
                lblShipFrom.Text = CCommon.ToString(radcmbLocation.Items.FindItemByValue(Session("DefaultWarehouse")).Attributes("Address"))
                hdnShipFromState.Value = CCommon.ToString(radcmbLocation.Items.FindItemByValue(Session("DefaultWarehouse")).Attributes("State"))
                hdnShipFromCountry.Value = CCommon.ToString(radcmbLocation.Items.FindItemByValue(Session("DefaultWarehouse")).Attributes("Country"))
                hdnShipFromPostalCode.Value = CCommon.ToString(radcmbLocation.Items.FindItemByValue(Session("DefaultWarehouse")).Attributes("PostalCode"))
            ElseIf radcmbLocation.Items.Count > 0 Then
                radcmbLocation.Items(0).Selected = True
                lblShipFrom.Text = CCommon.ToString(radcmbLocation.Items(0).Attributes("Address"))
                hdnShipFromState.Value = CCommon.ToString(radcmbLocation.Items(0).Attributes("State"))
                hdnShipFromCountry.Value = CCommon.ToString(radcmbLocation.Items(0).Attributes("Country"))
                hdnShipFromPostalCode.Value = CCommon.ToString(radcmbLocation.Items(0).Attributes("PostalCode"))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadBizDocStatus()
        Try
            Dim dtData As DataTable
            dtData = objCommon.GetMasterListItems(11, Session("DomainID"))
            radcmbBizDocStatus.DataSource = dtData
            radcmbBizDocStatus.DataTextField = "vcData"
            radcmbBizDocStatus.DataValueField = "numListItemID"
            radcmbBizDocStatus.DataBind()
            radcmbBizDocStatus.Items.Insert(0, New RadComboBoxItem("---Select One---", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadPaymentMethods()
        Try

            If Not pageType = PageTypeEnum.AddOppertunity AndAlso objSalesOrderConfiguration.bitDisplayPaymentMethods Then
                Dim arrPaymentMethodIDs() As String = Split(objSalesOrderConfiguration.vcPaymentMethodIDs, ",")
                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")
                objCommon.ListID = 31
                Dim dtPaymentMethods As New DataTable
                dtPaymentMethods = objCommon.GetMasterListItemsWithRights()

                Dim lst As New RadComboBoxItem
                Dim lstItem As New ListItem
                lst.Text = "-- Select One --"
                lst.Value = 0

                lstItem.Text = "-- Select One --"
                lstItem.Value = 0
                radcmbPaymentMethods.Items.Add(lst)

                ddlPaymentMethods.Items.Add(lstItem)
                If arrPaymentMethodIDs.Length > 0 AndAlso Not dtPaymentMethods Is Nothing Then
                    For Each dr As DataRow In dtPaymentMethods.Rows
                        If arrPaymentMethodIDs.Contains(CCommon.ToString(dr("numListItemID"))) Then
                            lst = New RadComboBoxItem
                            lstItem = New ListItem
                            lst.Text = CCommon.ToString(dr("vcData"))
                            lst.Value = CCommon.ToString(dr("numListItemID"))

                            lstItem.Text = CCommon.ToString(dr("vcData"))
                            lstItem.Value = CCommon.ToString(dr("numListItemID"))
                            'lst.Attributes.Add("onclick", "CheckBoxListSingleSelection(this," + CCommon.ToString(dr("numListItemID")) + ");")

                            radcmbPaymentMethods.Items.Add(lst)
                            If (CCommon.ToString(dr("vcData")) <> "Cash" AndAlso CCommon.ToString(dr("vcData")) <> "Credit Card" AndAlso CCommon.ToString(dr("vcData")) <> "Checks" AndAlso CCommon.ToString(dr("vcData")) <> "Bill Me") Then
                                ddlPaymentMethods.Items.Add(lstItem)
                            End If


                            'chklPaymentMethods.Items.Add(lst)
                        End If
                    Next
                    ddlPaymentMethods.DataTextField = "vcData"
                    ddlPaymentMethods.DataValueField = "numListItemID"
                    ddlPaymentMethods.DataBind()
                    'ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ChangeDropDownStyle", "ChangeDropDownStyle();", True)
                Else
                    plhPaymentMethods.Visible = False
                End If
            Else
                plhPaymentMethods.Visible = False
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadOrderDetailSection()
        Try
            plhOrderDetail1.Controls.Clear()
            plhOrderDetail2.Controls.Clear()
            Dim ds As DataSet
            Dim objTempOpportunity As New MOpportunity
            Dim objPageLayout As New CPageLayout
            Dim tabIndex As Short = 100

            Dim fields() As String

            objPageLayout.UserCntID = 0
            objPageLayout.CoType = "b" 'this will retun 0 rows if fields are not selected
            objPageLayout.DomainID = Session("DomainID")
            objPageLayout.PageId = 2
            objPageLayout.numRelCntType = 0
            objPageLayout.FormId = 90
            objPageLayout.PageType = 2

            ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                dtOppTableInfo = ds.Tables(0)

                Dim divColumn1 As New HtmlGenericControl("div")
                'divColumn1.Attributes.Add("class", "col-1-2")
                'divColumn1.Attributes.Add("style", "float:cnter")

                Dim divColumn2 As New HtmlGenericControl("div")
                'divColumn2.Attributes.Add("class", "td2")
                'divColumn2.Attributes.Add("style", "width:100%")

                For Each dr As DataRow In dtOppTableInfo.Rows
                    If dr("vcPropertyName") = "numPartner" Then
                        dr("vcPropertyName") = "numPartnerId"
                    End If
                    If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False Then
                        dr("vcValue") = objTempOpportunity.GetType.GetProperty(dr("vcPropertyName")).GetValue(objTempOpportunity, Nothing)

                        If CCommon.ToString(dr("vcDbColumnName")) = "dtReleaseDate" Then
                            dr("vcValue") = DateTime.UtcNow.AddMinutes(CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")) * -1).Date
                            hdnReleaseDateControlID.Value = CCommon.ToString(dr("numFieldId")) & CCommon.ToString(dr("vcFieldName"))
                        End If
                    End If

                    If (dr("fld_type") = "ListBox" Or dr("fld_type") = "SelectBox") Then
                        Dim dtData As DataTable
                        dtData = Nothing
                        If Not IsDBNull(dr("vcPropertyName")) Then
                            If dr("vcPropertyName") = "CampaignID" Then
                                Dim objCampaign As New PredefinedReports
                                objCampaign.byteMode = 2
                                objCampaign.DomainID = Session("DomainID")
                                dtData = objCampaign.GetCampaign()
                            ElseIf dr("vcPropertyName") = "numPartnerId" Then
                                Dim objCommon As New CCommon
                                objCommon.DomainID = Session("DomainID")
                                dtData = objCommon.GetPartnerSource()
                            ElseIf Not IsDBNull(dr("numListID")) And dr("numListID") <> 0 Then
                                If dr("ListRelID") > 0 Then
                                    objCommon.Mode = 3
                                    objCommon.DomainID = Session("DomainID")
                                    objCommon.PrimaryListItemID = objOppPageControls.GetPrimaryListItemID(dr("ListRelID"), dtOppTableInfo, objTempOpportunity)
                                    objCommon.SecondaryListID = dr("numListId")
                                    dtData = objCommon.GetFieldRelationships.Tables(0)
                                Else
                                    If CCommon.ToString(dr("numListID")) = "176" Then
                                        dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"), "1", If(pageType = PageTypeEnum.AddOppertunity, 1, 2))
                                    Else
                                        dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                    End If
                                End If
                            End If
                        End If

                        Dim ddl As RadComboBox

                        ddl = objOppPageControls.CreateDivFormControls(IIf(dr("intcoulmn") = 1, divColumn1, divColumn2), dr, False, False, tabIndex, dtData, boolAdd:=True)


                        If CLng(dr("DependentFields")) > 0 And Not False Then
                            ddl.AutoPostBack = True
                            AddHandler ddl.SelectedIndexChanged, AddressOf PopulateDependentOppDropdown
                        End If


                        ddl.EnableViewState = True
                    Else
                        objOppPageControls.CreateDivFormControls(IIf(dr("intcoulmn") = 1, divColumn1, divColumn2), dr, False, False, tabIndex, RecordID:=0, boolAdd:=True)
                    End If

                    Dim objPageControls As New PageControls
                    Dim strValidation As String = objPageControls.GenerateValidationScript(dtOppTableInfo)
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "CFvalidation", strValidation, True)
                Next

                plhOrderDetail1.Controls.Add(divColumn1)
                plhOrderDetail2.Controls.Add(divColumn2)

                ''Add Client Side validation for custom fields
                'Dim strValidation As String = objOppPageControls.GenerateJqueryValidationScript(dtOppTableInfo)
                'ClientScript.RegisterClientScriptBlock(Me.GetType, "CFvalidation", strValidation, True)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadCustomerDetailSection()
        Try
            plhCustomerDetail1.Controls.Clear()
            plhCustomerDetail2.Controls.Clear()

            If (CCommon.ToLong(radCmbCompany.SelectedValue) > 0) Then
                Dim objPageLayout As New CPageLayout
                Dim objLeads As New LeadsIP
                Dim dtTableInfo As New DataTable
                Dim objPageControls As New PageControls
                Dim tabIndex As Short = 200

                Dim ds As DataSet
                Dim fields() As String

                objPageLayout.UserCntID = 0
                objPageLayout.CoType = "b" 'this will retun 0 rows if fields are not selected
                'objPageLayout.RecordId = CCommon.ToLong(radCmbCompany.SelectedValue)
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.PageId = 1
                objPageLayout.FormId = 93
                objPageLayout.PageType = 2

                ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                    dtTableInfo = ds.Tables(0)

                    Dim divColumn1 As New HtmlGenericControl("div")
                    'divColumn1.Attributes.Add("class", "col-1-2")
                    'divColumn1.Attributes.Add("style", "float:left")
                    divColumn1.Attributes.Add("style", "width:100%")

                    Dim divColumn2 As New HtmlGenericControl("div")
                    'divColumn2.Attributes.Add("class", "col-1-2")
                    'divColumn2.Attributes.Add("style", "float:right")
                    divColumn2.Attributes.Add("style", "width:100%")

                    objLeads.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                    objLeads.DomainID = Session("DomainID")
                    objLeads.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    objLeads.GetCompanyDetails()

                    ''If intermediatory Page is enabled then Add the text "Name" to dropdown fields

                    For Each dr As DataRow In dtTableInfo.Rows
                        If dr("vcFieldName").Contains("Organization") Or dr("vcFieldName").Contains("Company") Then
                            dr("vcFieldName") = dr("vcFieldName").ToString().Replace("Organization", "").Trim()
                            dr("vcFieldName") = dr("vcFieldName").ToString().Replace("Company", "").Trim()
                        End If

                        If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False Then
                            dr("vcValue") = objLeads.GetType.GetProperty(dr("vcPropertyName")).GetValue(objLeads, Nothing)
                        End If

                        If (dr("fld_type") = "ListBox" Or dr("fld_type") = "SelectBox") Then
                            Dim dtData As DataTable

                            If Not IsDBNull(dr("vcPropertyName")) Then
                                If dr("vcPropertyName") = "AssignedTo" Then
                                    If Session("PopulateUserCriteria") = 1 Then
                                        dtData = objCommon.ConEmpListFromTerritories(Session("DomainID"), 0, 0, objLeads.TerritoryID)
                                    ElseIf Session("PopulateUserCriteria") = 2 Then
                                        dtData = objCommon.ConEmpList(Session("DomainID"), Session("UserContactID"))
                                    Else
                                        dtData = objCommon.ConEmpList(Session("DomainID"), 0, 0)
                                    End If
                                ElseIf dr("vcPropertyName") = "CampaignID" Then
                                    Dim objCampaign As New PredefinedReports
                                    objCampaign.byteMode = 2
                                    objCampaign.DomainID = Session("DomainID")
                                    dtData = objCampaign.GetCampaign()
                                ElseIf dr("vcPropertyName") = "BillingDays" Then
                                    Dim objOpp As New OppotunitiesIP
                                    Dim dtTerms As New DataTable
                                    With objOpp
                                        .DomainID = Session("DomainID")
                                        .TermsID = 0
                                        .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                                        dtTerms = .GetTerms()
                                    End With

                                    dtData = New DataTable
                                    dtData.Columns.Add("numTermsID")
                                    dtData.Columns.Add("vcTerms")
                                    For Each drTemp As DataRow In dtTerms.Rows
                                        Dim newRow As DataRow
                                        newRow = dtData.NewRow
                                        newRow("numTermsID") = drTemp("numTermsID")
                                        newRow("vcTerms") = drTemp("vcTerms") & " (" & CCommon.ToLong(drTemp("numNetDueInDays")) & ", " & CCommon.ToLong(drTemp("numDiscount")) & ", " & CCommon.ToLong(drTemp("numDiscountPaidInDays")) & ")"
                                        dtData.Rows.Add(newRow)
                                    Next
                                ElseIf Not IsDBNull(dr("numListID")) And dr("numListID") <> 0 Then
                                    If dr("ListRelID") > 0 Then
                                        objCommon.Mode = 3
                                        objCommon.DomainID = Session("DomainID")
                                        objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objLeads)
                                        objCommon.SecondaryListID = dr("numListId")
                                        dtData = objCommon.GetFieldRelationships.Tables(0)
                                    Else
                                        dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                    End If
                                End If
                            End If

                            If dr("vcDbColumnName") = "numCompanyDiff" Then

                                Dim divFormGroup As New HtmlGenericControl("div")
                                divFormGroup.Attributes.Add("class", "form-group")

                                Dim divFormGroupLabel As New HtmlGenericControl("label")
                                divFormGroupLabel.Attributes.Add("class", "col-3-12")
                                divFormGroup.Controls.Add(divFormGroupLabel)

                                Dim divFormGroupControl As New HtmlGenericControl("div")
                                divFormGroupControl.Attributes.Add("class", "col-9-12")
                                divFormGroup.Controls.Add(divFormGroupControl)

                                Dim lblFieldName As Label
                                lblFieldName = New Label
                                lblFieldName.Text = dr("vcFieldName").ToString & IIf((CCommon.ToInteger(dr("bitIsRequired")) = 1 Or CCommon.ToBool(dr("bitIsEmail")) = True Or CCommon.ToBool(dr("bitIsNumeric")) = True Or CCommon.ToBool(dr("bitIsAlphaNumeric")) = True Or CCommon.ToBool(dr("bitIsLengthValidation")) = True), "<font color=""red"">*</font>", "").ToString()
                                divFormGroupLabel.Controls.Add(lblFieldName)

                                Dim lblvalueField As New Label
                                lblvalueField = New Label
                                lblvalueField.Attributes.Add("style", "max-width:250px")
                                lblvalueField.CssClass = "textdiv"
                                lblvalueField.TabIndex = tabIndex
                                tabIndex = CShort(tabIndex + 1)

                                If Not IsDBNull(dr("vcValue")) AndAlso Not dtData Is Nothing Then
                                    If CBool(dr("bitCustomField")) = False Then
                                        Dim foundRows As DataRow()
                                        foundRows = dtData.Select(dtData.Columns(0).ColumnName.ToString() + "=" + Convert.ToString(dr("vcValue")))
                                        If (foundRows.Length > 0) Then
                                            lblvalueField.Text = CCommon.ToString(foundRows(0)(dtData.Columns(1).ColumnName))
                                        End If
                                    Else
                                        lblvalueField.Text = dr("vcValue").ToString
                                    End If
                                End If


                                Dim divleft1 As New HtmlGenericControl("div")
                                divleft1.Attributes.Add("style", "float:left")
                                divleft1.Controls.Add(lblvalueField)
                                divFormGroupControl.Controls.Add(divleft1)

                                Dim lblDiffValue As New Label
                                lblDiffValue.TabIndex = tabIndex
                                lblDiffValue.CssClass = "textdiv"
                                lblDiffValue.Attributes.Add("style", "max-width:250px")
                                tabIndex = CShort(tabIndex + 1)
                                lblDiffValue.Text = IIf(CCommon.ToString(objLeads.CompanyDiffValue) <> "", " - " & CCommon.ToString(objLeads.CompanyDiffValue), "")

                                Dim divleft2 As New HtmlGenericControl("div")
                                divleft2.Attributes.Add("style", "float:left; margin-left:10px;word-wrap:break-word;")
                                divleft2.Controls.Add(lblDiffValue)
                                divFormGroupControl.Controls.Add(divleft2)

                                If dr("intcoulmn") = 1 Then
                                    divColumn1.Controls.Add(divFormGroup)
                                Else
                                    divColumn2.Controls.Add(divFormGroup)
                                End If
                            Else
                                Dim ddl As RadComboBox
                                ddl = objPageControls.CreateDivFormControls(IIf(dr("intcoulmn") = 1, divColumn1, divColumn2), dr, True, False, tabIndex, dtData, boolAdd:=True)
                            End If
                        Else
                            objPageControls.CreateDivFormControls(IIf(dr("intcoulmn") = 1, divColumn1, divColumn2), dr, True, False, tabIndex, boolAdd:=True, RecordID:=lngDivId)
                        End If
                    Next

                    plhCustomerDetail1.Controls.Add(divColumn1)
                    plhCustomerDetail2.Controls.Add(divColumn2)
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadSalesOrderConfiguration()

        If objSalesOrderConfiguration.bitAutoFocusCustomer Then
            radCmbCompany.Focus()
        End If

        If objSalesOrderConfiguration.bitAutoFocusItem Then
            txtItem.Focus()
        End If

        'divLocation.Visible = If(objSalesOrderConfiguration.bitDisplayRootLocation, True, False)
        divCurrency.Visible = If(objSalesOrderConfiguration.bitDisplayCurrency, True, False)
        divAssignedTo.Visible = If(objSalesOrderConfiguration.bitDisplayAssignTo, True, False)
        divTemplate.Visible = If(objSalesOrderConfiguration.bitDisplayTemplate, True, False)

        If Not objSalesOrderConfiguration.bitDisplayFinancialStamp Then
            PanelFinancialStamp.Visible = False
        End If

        If Not objSalesOrderConfiguration.bitDisplayItemDetails Then
            plhItemDetails.Visible = False
        End If

        If Not objSalesOrderConfiguration.bitDisplayUnitCost Then
            divUnitCost.Visible = False
        End If

        If Not objSalesOrderConfiguration.bitDisplayProfitTotal Then
            divProfit.Visible = False
        End If

        If Not objSalesOrderConfiguration.bitDisplayShippingRates Then
            PanelShipping.Visible = False
        End If

        'If Not objSalesOrderConfiguration.bitDisplayShipVia Then
        '    divShipVia.Visible = False
        'End If

        If Not objSalesOrderConfiguration.bitDisplayCouponDiscount Then
            pnlCouponDiscountConfig.Visible = False
        End If

        If Not objSalesOrderConfiguration.bitDisplayShippingCharges Then
            divShippingCharges.Visible = False
        End If

        If Not objSalesOrderConfiguration.bitDisplayDiscount Then
            divDiscount.Visible = False
        End If

        If Not objSalesOrderConfiguration.bitDisplayExpectedDate Then
            radOrderExpectedDate.Visible = False
            lblOrderExpectedDate.Visible = False
        End If
        For Each col As DataGridColumn In dgItems.Columns
            If (Not objSalesOrderConfiguration.bitDisplayItemGridOrderAZ) And (col.HeaderText = "Line") Then
                col.Visible = False
            End If
            If (Not objSalesOrderConfiguration.bitDisplayItemGridItemID) And (col.HeaderText = "Item ID") Then
                col.Visible = False
            End If
            If (Not objSalesOrderConfiguration.bitDisplayItemGridSKU) And (col.HeaderText = "SKU") Then
                col.Visible = False
            End If
            If (Not objSalesOrderConfiguration.bitDisplayItemGridUnitListPrice) And (col.HeaderText = "Unit List Price") Then
                col.HeaderStyle.CssClass = col.HeaderStyle.CssClass & If(col.HeaderStyle.CssClass <> "", " ", "") & " hiddenColumn"
                col.ItemStyle.CssClass = col.HeaderStyle.CssClass & If(col.HeaderStyle.CssClass <> "", " ", "") & " hiddenColumn"
                col.FooterStyle.CssClass = col.HeaderStyle.CssClass & If(col.HeaderStyle.CssClass <> "", " ", "") & " hiddenColumn"
            End If
            If (Not objSalesOrderConfiguration.bitDisplayItemGridUnitSalePrice) And (col.HeaderText = "Unit Sale Price") Then
                col.HeaderStyle.CssClass = col.HeaderStyle.CssClass & If(col.HeaderStyle.CssClass <> "", " ", "") & " hiddenColumn"
                col.ItemStyle.CssClass = col.HeaderStyle.CssClass & If(col.HeaderStyle.CssClass <> "", " ", "") & " hiddenColumn"
                col.FooterStyle.CssClass = col.HeaderStyle.CssClass & If(col.HeaderStyle.CssClass <> "", " ", "") & " hiddenColumn"
            End If
            If (Not objSalesOrderConfiguration.bitDisplayItemGridDiscount) And (col.HeaderText = "Discount") Then
                col.HeaderStyle.CssClass = col.HeaderStyle.CssClass & If(col.HeaderStyle.CssClass <> "", " ", "") & " hiddenColumn"
                col.ItemStyle.CssClass = col.HeaderStyle.CssClass & If(col.HeaderStyle.CssClass <> "", " ", "") & " hiddenColumn"
                col.FooterStyle.CssClass = col.HeaderStyle.CssClass & If(col.HeaderStyle.CssClass <> "", " ", "") & " hiddenColumn"
            End If
            If (Not objSalesOrderConfiguration.bitDisplayItemGridItemRelaseDate) And (col.HeaderText = "Item Release Date") Then
                col.Visible = False
            End If
            If (Not objSalesOrderConfiguration.bitDisplayItemGridLocation) And (col.HeaderText = "Location") Then
                col.Visible = False
            End If
            If (Not objSalesOrderConfiguration.bitDisplayItemGridShipTo) And (col.HeaderText = "Ship-To") Then
                col.Visible = False
            End If
            If (Not objSalesOrderConfiguration.bitDisplayItemGridOnHandAllocation) And (col.HeaderText = "On-Hand / Allocation") Then
                col.Visible = False
            End If
            If (Not objSalesOrderConfiguration.bitDisplayItemGridDescription) And (col.HeaderText = "Description") Then
                col.Visible = False
            End If
            If (Not objSalesOrderConfiguration.bitDisplayItemGridNotes) And (col.HeaderText = "Notes") Then
                col.Visible = False
            End If
            If (Not objSalesOrderConfiguration.bitDisplayItemGridAttributes) And (col.HeaderText = "Attributes") Then
                col.Visible = False
            End If
            If (Not objSalesOrderConfiguration.bitDisplayItemGridInclusionDetail) And (col.HeaderText = "Inclusion Detail") Then
                col.Visible = False
            End If
            If (Not objSalesOrderConfiguration.bitDisplayItemGridItemClassification) And (col.HeaderText = "Item Classification") Then
                col.Visible = False
            End If
            If (Not objSalesOrderConfiguration.bitDisplayItemGridItemPromotion) And (col.HeaderText = "Item Promotion") Then
                col.Visible = False
            End If

            If Not objSalesOrderConfiguration.bitDisplayApplyPromotionCode Then
                divCouponCode.Visible = False
            End If
        Next

    End Sub

    Private Sub LoadSelectedItemDetailSection(ByVal itemCode As Long)
        Try
            plhItemDetails.Controls.Clear()
            Dim ds As DataSet

            Dim objPageLayout As New CPageLayout
            Dim objPageControls As New PageControls
            Dim dtTableInfo As DataTable
            Dim fields() As String
            Dim tabIndex As Short = 400

            Dim objTempItem As New CItems
            objTempItem.DomainID = Session("DomainID")
            objTempItem.ItemCode = itemCode
            objTempItem.GetItemDetails()

            objPageLayout.RecordId = itemCode
            objPageLayout.UserCntID = 0
            objPageLayout.CoType = "b" 'this will retun 0 rows if fields are not selected
            objPageLayout.DomainID = Session("DomainID")

            objPageLayout.PageId = 5
            objPageLayout.numRelCntType = 0
            objPageLayout.FormId = 91
            objPageLayout.PageType = 2

            ds = objPageLayout.GetTableInfoDefault() ' getting the table structure 
            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                dtTableInfo = ds.Tables(0)

                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")
                objCommon.UOMAll = True
                Dim dtUnit As DataTable
                dtUnit = objCommon.GetItemUOM()

                Dim divColumn1 As New HtmlGenericControl("div")
                divColumn1.Attributes.Add("class", "col-1-2")

                Dim divColumn2 As New HtmlGenericControl("div")
                divColumn2.Attributes.Add("class", "col-1-2")

                For Each dr As DataRow In dtTableInfo.Rows

                    If CCommon.ToString(dr("vcPropertyName")) = "BarCodeID" Then
                        dr("vcFieldName") = "UPC"
                    ElseIf CCommon.ToString(dr("vcPropertyName")) = "SKU" Then
                        dr("vcFieldName") = "SKU"
                    End If

                    If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False Then
                        dr("vcValue") = objTempItem.GetType.GetProperty(dr("vcPropertyName")).GetValue(objTempItem, Nothing)
                    End If

                    If (dr("fld_type") = "ListBox" Or dr("fld_type") = "SelectBox") Then
                        Dim dtData As DataTable
                        dtData = Nothing
                        If Not IsDBNull(dr("vcPropertyName")) Then
                            If dr("vcPropertyName") = "AssetChartAcntId" Then
                                dtData = New DataTable
                                Dim dtChartAcntDetails As DataTable
                                Dim objCOA As New ChartOfAccounting
                                objCOA.DomainID = Session("DomainId")
                                objCOA.AccountCode = "0101"
                                dtChartAcntDetails = objCOA.GetParentCategory()
                                dtData.Columns.Add("numAccountID")
                                dtData.Columns.Add("vcAccountName")
                                dtData.Columns.Add("vcAccountType")
                                For Each drChartAcnt As DataRow In dtChartAcntDetails.Rows
                                    Dim newRow As DataRow
                                    newRow = dtData.NewRow
                                    newRow("numAccountID") = drChartAcnt("numAccountID")
                                    newRow("vcAccountName") = drChartAcnt("vcAccountName")
                                    newRow("vcAccountType") = drChartAcnt("vcAccountType")
                                    dtData.Rows.Add(newRow)
                                Next
                            ElseIf dr("vcPropertyName") = "IncomeChartAcntId" Then
                                dtData = New DataTable
                                Dim dtChartAcntDetails As DataTable
                                Dim objCOA As New ChartOfAccounting
                                objCOA.DomainID = Session("DomainId")
                                objCOA.AccountCode = "0103"
                                dtChartAcntDetails = objCOA.GetParentCategory()
                                dtData.Columns.Add("numAccountID")
                                dtData.Columns.Add("vcAccountName")
                                dtData.Columns.Add("vcAccountType")
                                For Each drChartAcnt As DataRow In dtChartAcntDetails.Rows
                                    Dim newRow As DataRow
                                    newRow = dtData.NewRow
                                    newRow("numAccountID") = drChartAcnt("numAccountID")
                                    newRow("vcAccountName") = drChartAcnt("vcAccountName")
                                    newRow("vcAccountType") = drChartAcnt("vcAccountType")
                                    dtData.Rows.Add(newRow)
                                Next
                            ElseIf dr("vcPropertyName") = "COGSChartAcntId" Then
                                dtData = New DataTable
                                Dim dtChartAcntDetails As DataTable
                                Dim objCOA As New ChartOfAccounting
                                objCOA.DomainID = Session("DomainId")
                                objCOA.AccountCode = "0106"
                                dtChartAcntDetails = objCOA.GetParentCategory()
                                dtData.Columns.Add("numAccountID")
                                dtData.Columns.Add("vcAccountName")
                                dtData.Columns.Add("vcAccountType")
                                For Each drChartAcnt As DataRow In dtChartAcntDetails.Rows
                                    Dim newRow As DataRow
                                    newRow = dtData.NewRow
                                    newRow("numAccountID") = drChartAcnt("numAccountID")
                                    newRow("vcAccountName") = drChartAcnt("vcAccountName")
                                    newRow("vcAccountType") = drChartAcnt("vcAccountType")
                                    dtData.Rows.Add(newRow)
                                Next
                            ElseIf dr("vcPropertyName") = "BaseUnit" Or dr("vcPropertyName") = "SaleUnit" Or dr("vcPropertyName") = "PurchaseUnit" Then
                                dtData = New DataTable
                                dtData = dtUnit
                            ElseIf dr("vcPropertyName") = "ItemGroupID" Then
                                dtData = objTempItem.GetItemGroups.Tables(0)
                            ElseIf dr("vcPropertyName") = "WarehouseID" Then
                                dtData = New DataTable
                                Dim dtWareHouses As DataTable
                                dtWareHouses = objTempItem.GetWareHouses()
                                dtData.Columns.Add("numWareHouseID")
                                dtData.Columns.Add("vcWareHouse")
                                For Each drChartAcnt As DataRow In dtWareHouses.Rows
                                    Dim newRow As DataRow
                                    newRow = dtData.NewRow
                                    newRow("numWareHouseID") = drChartAcnt("numWareHouseID")
                                    newRow("vcWareHouse") = drChartAcnt("vcWareHouse")
                                    dtData.Rows.Add(newRow)
                                Next
                            ElseIf dr("vcPropertyName") = "WarehouseLocationID" Then
                                dtData = New DataTable
                                Dim dtWareHouseLocation As DataTable
                                dtWareHouseLocation = objTempItem.GetWarehouseLocation()
                                dtData.Columns.Add("numWLocationID")
                                dtData.Columns.Add("vcLocation")
                                For Each drChartAcnt As DataRow In dtWareHouseLocation.Rows
                                    Dim newRow As DataRow
                                    newRow = dtData.NewRow
                                    newRow("numWLocationID") = drChartAcnt("numWLocationID")
                                    newRow("vcLocation") = drChartAcnt("vcLocation")
                                    dtData.Rows.Add(newRow)
                                Next
                            ElseIf Not IsDBNull(dr("numListID")) And dr("numListID") <> 0 Then
                                If dr("ListRelID") > 0 Then
                                    objCommon.Mode = 3
                                    objCommon.DomainID = Session("DomainID")
                                    objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objTempItem)
                                    objCommon.SecondaryListID = dr("numListId")
                                    dtData = objCommon.GetFieldRelationships.Tables(0)
                                Else
                                    dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                End If
                            End If
                        End If

                        objPageControls.CreateDivFormControls(IIf(dr("intcoulmn") = 1, divColumn1, divColumn2), dr, True, False, tabIndex, dtData, boolAdd:=True)
                    Else
                        objPageControls.CreateDivFormControls(IIf(dr("intcoulmn") = 1, divColumn1, divColumn2), dr, True, False, tabIndex, RecordID:=CCommon.ToLong(hdnCurrentSelectedItem.Value), boolAdd:=True)
                    End If
                Next

                plhItemDetails.Controls.Add(divColumn1)
                plhItemDetails.Controls.Add(divColumn2)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As String)
        lblException.Text = ex
        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ScrollTo", "ScrollToError();", True)
    End Sub

    Private Sub ReceivePayment()
        Try
            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                Dim lngBizDoc As Long
                objOppBizDocs.OppType = oppType
                objOppBizDocs.OppId = lngOppId
                objOppBizDocs.DomainID = CCommon.ToLong(Session("DomainID"))
                lngBizDoc = objOppBizDocs.GetAuthorizativeOpportuntiy()

                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")
                objCommon.Mode = 33
                objCommon.Str = lngBizDoc

                objOppBizDocs.BizDocId = lngBizDoc
                objOppBizDocs.bitPartialShipment = True
                objOppBizDocs.SequenceId = objCommon.GetSingleFieldValue()
                OppBizDocID = objOppBizDocs.SaveBizDoc()

                CreateJournalEntry(objOppBizDocs, OppBizDocID, lngBizDoc)

                Dim paidAmount As Decimal
                Dim exchangeRate As Double
                Dim lngJournalID As Long

                Dim decAmount As Decimal = CCommon.ToDecimal(lblTotal.Text)

                Dim dsoppBizDoc As DataSet
                Dim objTempOppBizDocs As New OppBizDocs
                objTempOppBizDocs.DomainID = Session("DomainID")
                objTempOppBizDocs.OppId = lngOppId
                objTempOppBizDocs.UserCntID = Session("UserContactID")
                objTempOppBizDocs.OppBizDocId = OppBizDocID
                dsoppBizDoc = objTempOppBizDocs.GetBizDocsDetails()

                If Not dsoppBizDoc Is Nothing AndAlso dsoppBizDoc.Tables.Count > 0 AndAlso dsoppBizDoc.Tables(0).Rows.Count > 0 Then
                    paidAmount = CCommon.ToDecimal(dsoppBizDoc.Tables(0).Rows(0)("monAmountPaid"))
                    decAmount = decAmount - paidAmount
                End If

                exchangeRate = GetExchangeRate()
                If decAmount > 0 Then
                    If ProcessPayment(decAmount) Then
                        Dim dtInvoices As DataTable

                        dtInvoices = GetItems(decAmount)

                        If decAmount <> 0 Then
                            If SaveDeposite(dtInvoices, decAmount, exchangeRate) > 0 Then
                                lngJournalID = SaveDataToHeader(decAmount, 0, 0, exchangeRate)
                                SaveDataToGeneralJournalDetailsForCashAndCheCks(decAmount, exchangeRate, lngJournalID, lngBizDoc)

                                Dim objRule As New OrderAutoRules
                                objRule.GenerateAutoPO(OppBizDocID)

                                If objSalesOrderConfiguration.numListItemID > 0 AndAlso objSalesOrderConfiguration.numListItemID <> lngBizDoc Then
                                    Try
                                        CreateSalesConfiguredBizDoc()
                                    Catch ex As Exception
                                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "RecievePaymentError", "alert('" & ex.Message & "');", True)
                                    End Try
                                End If
                            End If
                        End If

                    End If
                End If

                objTransactionScope.Complete()
            End Using

            ''Added By Sachin Sadhu||Date:1stMay12014
            ''Purpose :To Added Opportunity data in work Flow queue based on created Rules
            ''          Using Change tracking
            Dim objWfA As New Workflow()
            objWfA.DomainID = Session("DomainID")
            objWfA.UserCntID = Session("UserContactID")
            objWfA.RecordID = OppBizDocID
            objWfA.SaveWFBizDocQueue()
            'end of code
        Catch ex As Exception
            If ex.Message.Contains("IM_BALANCE") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "RecievePaymentError", "alert('Order was created, but payment is not processed because Credit and Debit amounts you’re entering must be of the same amount');", True)
            ElseIf ex.Message.Contains("FY_CLOSED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "RecievePaymentError", "alert('Order was created, but payment is not processed because transactions date belongs to closed financial year.');", True)
            ElseIf ex.Message.Contains("CONFIGURE_PAYMENT_GATEWAY") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "RecievePaymentError", "alert('Order was created, but payment is not processed because payment gateway is not configured.');", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "RecievePaymentError", "alert('Order was created, but credit card did not process successfully');", True)
            End If
        End Try
    End Sub

    Private Sub CreateSalesConfiguredBizDoc()
        Try
            If CCommon.ToBool(Session("IsMinUnitPriceRule")) Then
                If CheckIfApprovalRequired() Then
                    Throw New Exception("APPROVAL_REQUIRED")
                End If
            End If

            Dim objOppBizDocs As New OppBizDocs
            Dim OppBizDocID, lngDivId, JournalId As Long
            Dim lngCntID As Long = 0
            objOppBizDocs.OppId = lngOppId
            objOppBizDocs.OppType = oppType

            objOppBizDocs.UserCntID = Session("UserContactID")
            objOppBizDocs.DomainID = Session("DomainID")
            objOppBizDocs.vcPONo = "" 'txtPO.Text
            objOppBizDocs.vcComments = ""


            Dim dtDetails, dtBillingTerms As DataTable
            Dim objPageLayout As New CPageLayout
            objPageLayout.OpportunityId = lngOppId
            objPageLayout.DomainID = Session("DomainID")
            dtDetails = objPageLayout.OpportunityDetails.Tables(0)
            lngDivId = dtDetails.Rows(0).Item("numDivisionID")
            lngCntID = dtDetails.Rows(0).Item("numContactID")
            Dim objAdmin As New CAdmin
            objAdmin.DivisionID = lngDivId

            Dim lintAuthorizativeBizDocsId As Long
            objOppBizDocs.DomainID = Session("DomainID")
            objOppBizDocs.OppId = lngOppId
            lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()

            Dim objJournal As New JournalEntry
            If lintAuthorizativeBizDocsId = objSalesOrderConfiguration.numListItemID Then 'save bizdoc only if selected company's AR and AP account is mapped
                If objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), lngDivId) = 0 Then
                    Throw New Exception("AR_AP")
                End If
            End If
            If oppType = 2 Then
                'Accounting validation->Do not allow to save PO untill Default COGs account is mapped
                If ChartOfAccounting.GetDefaultAccount("CG", Session("DomainID")) = 0 Then
                    Throw New Exception("CG")
                End If
            End If

            If oppType = 2 Then
                If ChartOfAccounting.GetDefaultAccount("PC", Session("DomainID")) = 0 Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Purchase Clearing account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                    Exit Sub
                End If

                If ChartOfAccounting.GetDefaultAccount("PV", Session("DomainID")) = 0 Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Purchase Price Variance account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                    Exit Sub
                End If
            End If

            objOppBizDocs.FromDate = DateTime.UtcNow
            objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

            objOppBizDocs.BizDocId = objSalesOrderConfiguration.numListItemID

            objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            objCommon.Mode = 33
            objCommon.Str = objSalesOrderConfiguration.numListItemID
            objOppBizDocs.SequenceId = objCommon.GetSingleFieldValue()

            objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            objCommon.Mode = 34
            objCommon.Str = lngOppId
            objOppBizDocs.RefOrderNo = objCommon.GetSingleFieldValue()

            objOppBizDocs.bitPartialShipment = True

            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                OppBizDocID = objOppBizDocs.SaveBizDoc()

                If OppBizDocID = 0 Then
                    Throw New Exception("SAME_BIZDOC_EXISTS")
                End If

                'If OppBizDocID > 0 Then
                '    GetWorkFlowAutomation(OppBizDocID, lngCntID, lngDivId)

                'End If

                'Create Journals only for authoritative bizdocs
                '-------------------------------------------------
                If lintAuthorizativeBizDocsId = objSalesOrderConfiguration.numListItemID Then
                    Dim ds As New DataSet
                    Dim dtOppBiDocItems As DataTable

                    objOppBizDocs.OppBizDocId = OppBizDocID
                    ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                    dtOppBiDocItems = ds.Tables(0)

                    Dim objCalculateDealAmount As New CalculateDealAmount

                    objCalculateDealAmount.CalculateDealAmount(lngOppId, OppBizDocID, oppType, Session("DomainID"), dtOppBiDocItems)

                    ''---------------------------------------------------------------------------------
                    JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, objOppBizDocs.FromDate, Description:=ds.Tables(1).Rows(0).Item("vcBizDocID"))
                    Dim objJournalEntries As New JournalEntry

                    If oppType = 2 Then
                        If CCommon.ToBool(ds.Tables(1).Rows(0).Item("bitPPVariance")) Then
                            objJournalEntries.SaveJournalEntriesPurchaseVariance(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), ds.Tables(1).Rows(0).Item("fltExchangeRateBizDoc"), vcBaseCurrency:=ds.Tables(1).Rows(0).Item("vcBaseCurrency"), vcForeignCurrency:=ds.Tables(1).Rows(0).Item("vcForeignCurrency"))
                        Else
                            objJournalEntries.SaveJournalEntriesPurchase(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"))
                        End If
                    ElseIf oppType = 1 Then
                        If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                            objJournalEntries.SaveJournalEntriesSales(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), 0)
                        Else
                            objJournalEntries.SaveJournalEntriesSalesNew(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), 0)
                        End If

                        If Session("AutolinkUnappliedPayment") Then
                            Dim objSalesFulfillment As New SalesFulfillmentWorkflow
                            objSalesFulfillment.DomainID = Session("DomainID")
                            objSalesFulfillment.UserCntID = Session("UserContactID")
                            objSalesFulfillment.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                            objSalesFulfillment.OppId = lngOppId
                            objSalesFulfillment.OppBizDocId = OppBizDocID

                            objSalesFulfillment.AutoLinkUnappliedPayment()
                        End If
                    End If
                End If

                objTransactionScope.Complete()
            End Using

            ''Added By Sachin Sadhu||Date:1stMay2014
            ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
            ''          Using Change tracking
            Dim objWfA As New Workflow()
            objWfA.DomainID = Session("DomainID")
            objWfA.UserCntID = Session("UserContactID")
            objWfA.RecordID = OppBizDocID
            objWfA.SaveWFBizDocQueue()
            'end of code

            '------ Send BizDoc Alerts - Notify record owners, their supervisors, and your trading partners when a BizDoc is created, modified, or approved.
            Dim objAlert As New CAlerts
            objAlert.SendBizDocAlerts(lngOppId, OppBizDocID, objSalesOrderConfiguration.numListItemID, Session("DomainID"), CAlerts.enmBizDoc.IsCreated)

            Dim objAutomatonRule As New AutomatonRule
            objAutomatonRule.ExecuteAutomationRule(49, OppBizDocID, 1)
        Catch ex As Exception
            If ex.Message.Contains("NOT_ALLOWED") Then
                Throw New Exception("Order is created successfully but error occured wile creating bizdoc as per configuration made in Administrator -> New Order because to split order items multiple bizdocs you must create all bizdocs with ""partial fulfilment"" checked!")
            ElseIf ex.Message.Contains("AR_AP") Then
                Throw New Exception("Order is created successfully but error occured wile creating bizdoc as per configuration made in Administrator -> New Order because AR and AP Relationship is not set.")
            ElseIf ex.Message.Contains("CG") Then
                Throw New Exception("Order is created successfully but error occured wile creating bizdoc as per configuration made in Administrator -> New Order because default COGs account is not set.")
            ElseIf ex.Message.Contains("SAME_BIZDOC_EXISTS") Then
                Throw New Exception("Order is created successfully but error occured wile creating bizdoc as per configuration made in Administrator -> New Order because bizDoc by the same name is already created.")
            ElseIf ex.Message.Contains("APPROVAL_REQUIRED") Then
                Throw New Exception("Order is created successfully but error occured wile creating bizdoc as per configuration made in Administrator -> New Order because approval of unit price(s) is required.")
            ElseIf ex.Message.Contains("NOT_ALLOWED_FulfillmentOrder") Then
                Throw New Exception("Order is created successfully but error occured wile creating bizdoc as per configuration made in Administrator -> New Order because only a Sales Order can create a Fulfillment Order")
            ElseIf ex.Message.Contains("FY_CLOSED") Then
                Throw New Exception("Order is created successfully but error occured wile creating bizdoc as per configuration made in Administrator -> New Order because this transaction can not be posted,Because transactions date belongs to closed financial year.")
            ElseIf ex.Message.Contains("AlreadyInvoice") Then
                Throw New Exception("Order is created successfully but error occured wile creating bizdoc as per configuration made in Administrator -> New Order because you can’t create a Invoice against a Fulfillment Order that already contains a Invoice")
            ElseIf ex.Message.Contains("AlreadyPackingSlip") Then
                Throw New Exception("Order is created successfully but error occured wile creating bizdoc as per configuration made in Administrator -> New Order because you can’t create a Packing-Slip against a Fulfillment Order that already contains a Packing-Slip")
            ElseIf ex.Message.Contains("AlreadyInvoice_NOT_ALLOWED_FulfillmentOrder") Then
                Throw New Exception("Order is created successfully but error occured wile creating bizdoc as per configuration made in Administrator -> New Order because you can’t create a fulfillment order against a sales order that already contains an Invoice or Packing-Slip")
            ElseIf ex.Message.Contains("AlreadyFulfillmentOrder") Then
                Throw New Exception("Order is created successfully but error occured wile creating bizdoc as per configuration made in Administrator -> New Order because manually adding Invoices or Packing Slips to a Sales Order is not allowed after a Fulfillment Order (FO) is added. Your options are to edit the FO status to trigger creation of Invoices and/or Packing Slips, Delete the Fulfillment Order, or Create Invoices and/or Packing Slips from the Sales Fulfillment section")
            Else
                Throw New Exception("Order is created successfully but error occured wile creating bizdoc as per configuration made in Administrator -> New Order.")
            End If
        End Try
    End Sub

    Private Function CheckIfApprovalRequired() As Boolean
        Try
            Dim objOpportunity As New MOpportunity
            objOpportunity.OpportunityId = lngOppId
            objOpportunity.DomainID = Session("DomainID")
            objOpportunity.UserCntID = Session("UserContactID")
            objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            Dim dsTemp As DataSet = objOpportunity.ItemsByOppId

            If Not dsTemp Is Nothing AndAlso dsTemp.Tables.Count > 0 AndAlso dsTemp.Tables(0).Rows.Count > 0 Then
                Dim results = From myRow In dsTemp.Tables(0).AsEnumerable()
                              Where CCommon.ToBool(myRow.Field(Of Boolean)("bitItemPriceApprovalRequired")) = True
                              Select myRow
                If results.Count > 0 Then
                    Return True
                Else
                    Return False
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub LoadCreditCardDetail()
        Try
            'Load Card Year
            For i As Integer = Date.Now.Year To Date.Now.Year + 10
                radcmbYear.Items.Add(New RadComboBoxItem(i.ToString("00"), i.ToString.Substring(2, 2)))
            Next

            'Load card type
            objCommon.sb_FillComboFromDBwithSel(radcmbCardType, 120, Session("DomainID"))

            Dim objOppInvoice As New OppInvoice
            Dim dsCCInfo As DataSet
            objOppInvoice.DomainID = Session("DomainID")
            objOppInvoice.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
            objOppInvoice.numCCInfoID = 0
            objOppInvoice.IsDefault = 1
            objOppInvoice.bitflag = 1
            dsCCInfo = objOppInvoice.GetCustomerCreditCardInfo()
            Dim strCard As String = ""
            Dim objEncryption As New QueryStringValues
            For i As Integer = 0 To dsCCInfo.Tables(0).Rows.Count - 1
                strCard = objEncryption.Decrypt(dsCCInfo.Tables(0).Rows(i)("vcCreditCardNo").ToString())
                If strCard.Length > 4 Then
                    dsCCInfo.Tables(0).Rows(i)("vcCreditCardNo") = "************" & strCard.Substring(strCard.Length - 4, 4)
                Else
                    dsCCInfo.Tables(0).Rows(i)("vcCreditCardNo") = "NA"
                End If
            Next
            radcmbCards.DataTextField = "vcCreditCardNo"
            radcmbCards.DataValueField = "numCCInfoID"
            radcmbCards.DataSource = dsCCInfo
            radcmbCards.DataBind()
            radcmbCards.Items.Insert(0, New RadComboBoxItem("--Select One--", "0"))

            If dsCCInfo.Tables(0).Rows.Count > 0 Then
                radcmbCards.SelectedIndex = 1
                LoadSavedCreditCardInfo()
            End If

            UpdatePanelPaymentMethods.Update()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadSavedCreditCardInfo()
        Try
            Dim objOppInvoice As New OppInvoice
            Dim dsCCInfo As DataSet
            objOppInvoice.DomainID = Session("DomainID")
            objOppInvoice.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
            objOppInvoice.numCCInfoID = radcmbCards.SelectedValue
            objOppInvoice.IsDefault = 1
            objOppInvoice.bitflag = 1
            dsCCInfo = objOppInvoice.GetCustomerCreditCardInfo()


            If dsCCInfo.Tables(0).Rows.Count > 0 Then
                Dim objEncryption As New QueryStringValues
                txtCHName.Text = objEncryption.Decrypt(dsCCInfo.Tables(0).Rows(0)("vcCardHolder").ToString())
                Dim strCCNo As String
                strCCNo = objEncryption.Decrypt(dsCCInfo.Tables(0).Rows(0)("vcCreditCardNo").ToString())
                Session("CCNO") = strCCNo
                If strCCNo.Length > 4 Then
                    strCCNo = "************" & strCCNo.Substring(strCCNo.Length - 4, 4)
                Else
                    strCCNo = ""
                End If
                txtCCNo.Text = strCCNo

                'txtCVV2.Text = objEncryption.Decrypt(dsCCInfo.Tables(0).Rows(0)("vcCVV2").ToString())
                radcmbCardType.SelectedIndex = -1
                radcmbMonth.SelectedIndex = -1
                radcmbYear.SelectedIndex = -1
                If Not radcmbCardType.Items.FindItemByValue(dsCCInfo.Tables(0).Rows(0)("numCardTypeID")) Is Nothing Then
                    radcmbCardType.Items.FindItemByValue(dsCCInfo.Tables(0).Rows(0)("numCardTypeID").ToString()).Selected = True
                End If
                If Not radcmbMonth.Items.FindItemByValue(CInt(dsCCInfo.Tables(0).Rows(0)("tintValidMonth")).ToString("00")) Is Nothing Then
                    radcmbMonth.Items.FindItemByValue(CInt(dsCCInfo.Tables(0).Rows(0)("tintValidMonth")).ToString("00")).Selected = True
                End If
                If Not radcmbYear.Items.FindItemByValue(dsCCInfo.Tables(0).Rows(0)("intValidYear").ToString()) Is Nothing Then
                    radcmbYear.Items.FindItemByValue(dsCCInfo.Tables(0).Rows(0)("intValidYear").ToString()).Selected = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function GetExchangeRate() As Double
        Try
            If CCommon.ToLong(Session("BaseCurrencyID")) = CCommon.ToLong(radcmbCurrency.SelectedValue) Then
                Return 1
            End If

            Dim objCurrency As New CurrencyRates
            objCurrency.DomainID = Session("DomainID")
            objCurrency.CurrencyID = CCommon.ToLong(radcmbCurrency.SelectedValue)
            objCurrency.GetAll = 0
            Dim dt As DataTable = objCurrency.GetCurrencyWithRates()
            If dt.Rows.Count > 0 Then
                Return CCommon.ToDouble(dt.Rows(0)("fltExchangeRate"))
            Else
                Return 1
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function SaveSignetureImage() As String
        Try
            If CCommon.ToInteger(GetQueryStringVal("swipe")) = 1 Then

                Dim sigToImg As SignatureToImage = New SignatureToImage()
                sigToImg.CanvasHeight = "110"
                sigToImg.CanvasWidth = "680"
                Dim strSignatureFile As String = ""
                strSignatureFile = OppBizDocID & "_" & Day(DateTime.Now) & Month(DateTime.Now) & Year(DateTime.Now) & Hour(DateTime.Now) & Minute(DateTime.Now) & Second(DateTime.Now) & ".jpg"

                Dim signatureImage As Bitmap
                'TODO: Uncomment the code to make credit card swap functionality available
                'signatureImage = sigToImg.SigJsonToImage(hdnSignatureOutput.Value)
                signatureImage.Save(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strSignatureFile, System.Drawing.Imaging.ImageFormat.Jpeg)

                Return strSignatureFile
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Function GetItems(ByVal decAmount As Decimal) As DataTable
        Try
            Dim dr As DataRow
            Dim dtMain As New DataTable
            Dim dtCredits As New DataTable
            Dim dtDeposit As New DataTable

            CCommon.AddColumnsToDataTable(dtMain, "numDepositeDetailID,numOppBizDocsID,numOppID,monAmountPaid")
            CCommon.AddColumnsToDataTable(dtCredits, "numDepositeDetailID,numOppBizDocsID,numOppID,monAmountPaid")
            CCommon.AddColumnsToDataTable(dtDeposit, "numDepositeDetailID,numOppBizDocsID,numOppID,monAmountPaid")

            dr = dtMain.NewRow
            dr("numDepositeDetailID") = 0
            dr("numOppBizDocsID") = OppBizDocID
            dr("numOppID") = lngOppId
            dr("monAmountPaid") = decAmount
            dtMain.Rows.Add(dr)

            For Each drMain As DataRow In dtMain.Rows
                If decAmount > 0 Then
                    Dim monAmountPaid As Decimal = CCommon.ToDecimal(drMain("monAmountPaid"))

                    If monAmountPaid > 0 Then
                        dr = dtDeposit.NewRow

                        dr("numDepositeDetailID") = 0
                        dr("numOppBizDocsID") = OppBizDocID
                        dr("numOppID") = lngOppId

                        If decAmount > monAmountPaid Then
                            dr("monAmountPaid") = monAmountPaid
                            decAmount = decAmount - monAmountPaid
                        Else
                            dr("monAmountPaid") = decAmount
                            decAmount = 0
                        End If

                        drMain("monAmountPaid") = monAmountPaid - dr("monAmountPaid")
                        dtDeposit.Rows.Add(dr)
                    End If
                Else
                    Exit For
                End If
            Next

            dtMain.AcceptChanges()


            Return dtDeposit

        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function ProcessPayment(ByVal decAmount As Decimal) As Boolean
        Try
            Dim objOppInvoice As New OppInvoice

            If CCommon.ToInteger(radcmbPaymentMethods.SelectedValue) = 1 And (rdbAuthorizeAndCharge.Checked Or rdbAuthorizeOnly.Checked) Then ' Credit card

                Dim dtTable As DataTable
                objCommon.DomainID = Session("DomainID")
                dtTable = objCommon.GetPaymentGatewayDetails
                If dtTable.Rows.Count = 0 Then
                    Throw New Exception("CONFIGURE_PAYMENT_GATEWAY")
                Else
                    Dim responseCode As String = ""
                    Dim objPaymentGateway As New PaymentGateway
                    objPaymentGateway.DomainID = Session("DomainID")

                    If objPaymentGateway.GatewayTransaction(IIf(IsNumeric(decAmount), decAmount, 0), txtCHName.Text, IIf(txtCCNo.Text.Trim().Contains("*"), Session("CCNO"), txtCCNo.Text.Trim()), txtCVV2.Text, radcmbMonth.SelectedValue, radcmbYear.SelectedValue, CCommon.ToLong(radcmbContact.SelectedValue), ResponseMessage, ReturnTransactionID, responseCode, CreditCardAuthOnly:=IIf(rdbAuthorizeOnly.Checked, True, False), CreditCardTrack1:=hdnSwipeTrack1.Value) Then

                        'Insert Transaction details into TransactionHistory table
                        Dim objTransactionHistory As New TransactionHistory
                        objTransactionHistory.TransHistoryID = 0
                        objTransactionHistory.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                        objTransactionHistory.ContactID = CCommon.ToLong(radcmbContact.SelectedValue)
                        objTransactionHistory.OppID = lngOppId
                        objTransactionHistory.OppBizDocsID = OppBizDocID
                        objTransactionHistory.TransactionID = CCommon.ToString(ReturnTransactionID)
                        objTransactionHistory.TransactionStatus = IIf(rdbAuthorizeOnly.Checked, 1, 2)
                        objTransactionHistory.PGResponse = ResponseMessage
                        objTransactionHistory.Type = 2 '1= bizcart & 2 =Internal
                        If rdbAuthorizeOnly.Checked Then
                            objTransactionHistory.AuthorizedAmt = decAmount
                        ElseIf rdbAuthorizeAndCharge.Checked Then
                            objTransactionHistory.CapturedAmt = decAmount
                        End If
                        objTransactionHistory.RefundAmt = 0
                        objTransactionHistory.CardHolder = objCommon.Encrypt(txtCHName.Text)
                        objTransactionHistory.CreditCardNo = objCommon.Encrypt(IIf(txtCCNo.Text.Trim().Contains("*"), Session("CCNO"), txtCCNo.Text.Trim()))
                        objTransactionHistory.Cvv2 = objCommon.Encrypt(txtCVV2.Text)
                        objTransactionHistory.ValidMonth = radcmbMonth.SelectedValue
                        objTransactionHistory.ValidYear = radcmbYear.SelectedValue
                        objTransactionHistory.SignatureFile = ""
                        objTransactionHistory.UserCntID = Session("UserContactID")
                        objTransactionHistory.DomainID = Session("DomainID")
                        objTransactionHistory.CardType = radcmbCardType.SelectedValue
                        objTransactionHistory.ResponseCode = responseCode
                        lngTransactionHistoryID = objTransactionHistory.ManageTransactionHistory()



                        'If True Then
                        If dtTable.Rows(0)("bitSaveCreditCardInfo") = True And CCommon.ToLong(radcmbContact.SelectedValue) > 0 Then
                            'Save Credit Card Details
                            Dim objEncryption As New QueryStringValues
                            With objOppInvoice
                                .ContactID = CCommon.ToLong(radcmbContact.SelectedValue)
                                .CardHolder = objEncryption.Encrypt(txtCHName.Text.Trim())
                                .CardTypeID = radcmbCardType.SelectedValue
                                .CreditCardNumber = objEncryption.Encrypt(IIf(txtCCNo.Text.Trim().Contains("*"), Session("CCNO"), txtCCNo.Text.Trim()))
                                .CVV2 = objEncryption.Encrypt(txtCVV2.Text.Trim())
                                .ValidMonth = radcmbMonth.SelectedValue
                                .ValidYear = radcmbYear.SelectedValue
                                .UserCntID = Session("UserContactID")
                                .AddCustomerCreditCardInfo()
                            End With
                        End If

                        litMessage.Text = ResponseMessage
                    Else
                        Throw New Exception(ResponseMessage)
                        Return False
                    End If
                End If
            End If
            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Sub SaveCreditCardInfo(ByVal decAmount As Decimal)
        Try
            Dim objOppInvoice As New OppInvoice
            If CCommon.ToLong(radcmbContact.SelectedValue) > 0 Then
                'Save Credit Card Details
                Dim objEncryption As New QueryStringValues
                With objOppInvoice
                    .ContactID = CCommon.ToLong(radcmbContact.SelectedValue)
                    .CardHolder = objEncryption.Encrypt(txtCHName.Text.Trim())
                    .CardTypeID = radcmbCardType.SelectedValue
                    .CreditCardNumber = objEncryption.Encrypt(IIf(txtCCNo.Text.Trim().Contains("*"), Session("CCNO"), txtCCNo.Text.Trim()))
                    .CVV2 = objEncryption.Encrypt(txtCVV2.Text.Trim())
                    .ValidMonth = radcmbMonth.SelectedValue
                    .ValidYear = radcmbYear.SelectedValue
                    .UserCntID = Session("UserContactID")
                    .AddCustomerCreditCardInfo()
                End With
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function SaveDeposite(ByVal dtInvoices As DataTable, ByVal decAmount As Decimal, ByVal exchangeRate As Double) As Long
        Try
            objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            objCommon.UserCntID = Session("UserContactID")
            objCommon.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)

            Dim objMakeDeposit As New MakeDeposit
            With objMakeDeposit
                .DivisionId = CCommon.ToLong(radCmbCompany.SelectedValue)
                .Entry_Date = DateTime.Now.Date

                If CCommon.ToLong(radcmbPaymentMethods.SelectedValue) = 1 Then
                    .TransactionHistoryID = lngTransactionHistoryID
                End If

                .PaymentMethod = radcmbPaymentMethods.SelectedValue 'chklPaymentMethods.SelectedValue
                .DepositeToType = 2
                .numAmount = decAmount
                .AccountId = ChartOfAccounting.GetDefaultAccount("UF", Session("DomainID"))
                .RecurringId = 0
                .DepositId = 0
                .UserCntID = Session("UserContactID")
                .DomainID = Session("DomainId")

                Dim ds As New DataSet
                Dim str As String = ""

                If dtInvoices.Rows.Count > 0 Then
                    ds.Tables.Add(dtInvoices.Copy)
                    ds.Tables(0).TableName = "Item"
                    str = ds.GetXml()
                End If

                .StrItems = str
                .Mode = 0
                .DepositePage = 2
                .CurrencyID = CCommon.ToLong(radcmbCurrency.SelectedValue)
                .ExchangeRate = exchangeRate
                .AccountClass = objCommon.GetAccountingClass()
                lngDepositeID = .SaveDataToMakeDepositDetails()

                ''AUthor   :sachin sadhu||Date:22ndAug2014
                ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                ''          Using Change tracking

                For i = 0 To dtInvoices.Rows.Count - 1
                    Dim objWfA As New Workflow()
                    objWfA.DomainID = Session("DomainID")
                    objWfA.UserCntID = Session("UserContactID")
                    objWfA.RecordID = CCommon.ToLong(dtInvoices.Rows(i)("numOppBizDocsID").ToString())
                    objWfA.SaveWFBizDocQueue()
                Next

                'end of code

                Return lngDepositeID
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub SaveDataToGeneralJournalDetailsForCashAndCheCks(ByVal p_Amount As Decimal, ByVal FltExchangeRate As Double, ByVal lngJournalID As Long, ByVal lngBizDocID As Long)
        Try

            Dim objJEList As New JournalEntryCollection
            Dim objJE As New JournalEntryNew()
            Dim TotalAmountAtExchangeRateOfOrder As Decimal
            Dim TotalAmountVariationDueToCurrencyExRate As Decimal = 0


            'i.e foreign currency payment
            If FltExchangeRate <> 1 Then
                TotalAmountAtExchangeRateOfOrder = 0
                TotalAmountAtExchangeRateOfOrder = TotalAmountAtExchangeRateOfOrder + (p_Amount * FltExchangeRate)

                If TotalAmountAtExchangeRateOfOrder > 0 Then
                    TotalAmountVariationDueToCurrencyExRate = TotalAmountAtExchangeRateOfOrder - (p_Amount * FltExchangeRate)
                    If Math.Abs(TotalAmountVariationDueToCurrencyExRate) < 0.0 Then
                        TotalAmountVariationDueToCurrencyExRate = 0
                    End If
                End If
            End If



            'Debit : Undeposit Fund (Amount)
            objJE = New JournalEntryNew()


            objJE.TransactionId = 0
            objJE.DebitAmt = p_Amount * FltExchangeRate 'total amount at current exchange rate
            objJE.CreditAmt = 0
            objJE.Description = ""
            objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("UF", Session("DomainID"))
            objJE.CustomerId = CCommon.ToLong(radCmbCompany.SelectedValue)
            objJE.MainDeposit = 1
            objJE.MainCheck = 0
            objJE.MainCashCredit = 0
            objJE.OppitemtCode = 0
            objJE.BizDocItems = ""
            objJE.Reference = ""
            objJE.PaymentMethod = CCommon.ToLong(radcmbPaymentMethods.SelectedValue)
            objJE.Reconcile = False
            objJE.CurrencyID = CCommon.ToLong(radcmbCurrency.SelectedValue)
            objJE.FltExchangeRate = FltExchangeRate
            objJE.TaxItemID = 0
            objJE.BizDocsPaymentDetailsId = 0
            objJE.ContactID = 0
            objJE.ItemID = 0
            objJE.ProjectID = 0
            objJE.ClassID = 0
            objJE.CommissionID = 0
            objJE.ReconcileID = 0
            objJE.Cleared = 0
            objJE.ReferenceType = enmReferenceType.DepositHeader
            objJE.ReferenceID = lngDepositeID

            objJEList.Add(objJE)

            'For Each dr As DataRow In dtItems.Rows
            'Credit: Customer A/R With (Amount)
            objJE = New JournalEntryNew()

            objJE.TransactionId = 0
            objJE.DebitAmt = 0
            objJE.CreditAmt = IIf(TotalAmountAtExchangeRateOfOrder > 0, TotalAmountAtExchangeRateOfOrder, p_Amount * FltExchangeRate)

            objOppBizDocs.OppBizDocId = lngBizDocID
            Dim lngARAccountId As Long = CCommon.ToLong(objOppBizDocs.GetBizDocARAccountID())

            If lngARAccountId = 0 Then
                lngARAccountId = objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), CCommon.ToLong(radCmbCompany.SelectedValue))
            End If

            objJE.ChartAcntId = lngARAccountId
            objJE.Description = "Payment received-Credited to Customer A/R" '"Credit Customer's AR account"
            objJE.CustomerId = CCommon.ToLong(radCmbCompany.SelectedValue)
            objJE.MainDeposit = 0
            objJE.MainCheck = 0
            objJE.MainCashCredit = 0
            objJE.OppitemtCode = 0
            objJE.BizDocItems = ""
            objJE.Reference = ""
            objJE.PaymentMethod = CCommon.ToLong(radcmbPaymentMethods.SelectedValue)
            objJE.Reconcile = False
            objJE.CurrencyID = CCommon.ToLong(radcmbCurrency.SelectedValue)
            objJE.FltExchangeRate = FltExchangeRate
            objJE.TaxItemID = 0
            objJE.BizDocsPaymentDetailsId = 0
            objJE.ContactID = 0
            objJE.ItemID = 0
            objJE.ProjectID = 0
            objJE.ClassID = 0
            objJE.CommissionID = 0
            objJE.ReconcileID = 0
            objJE.Cleared = 0
            objJE.ReferenceType = enmReferenceType.DepositDetail
            objJE.ReferenceID = 0

            objJEList.Add(objJE)

            If CCommon.ToLong(radcmbCurrency.SelectedValue) > 0 And Math.Abs(TotalAmountVariationDueToCurrencyExRate) > 0 Then
                If CCommon.ToLong(radcmbCurrency.SelectedValue) <> CCommon.ToLong(Session("BaseCurrencyID")) And FltExchangeRate > 0.0 Then 'Foreign Currency Transaction

                    Dim lngDefaultForeignExchangeAccountID As Long = ChartOfAccounting.GetDefaultAccount("FE", Session("DomainID"))

                    'Credit: Customer A/R With (Amount)
                    objJE = New JournalEntryNew()
                    objJE.TransactionId = 0

                    ' difference in amount due to exchange rate variation between order placed exchange rate with Entered exchange rate
                    If TotalAmountVariationDueToCurrencyExRate < 0 Then
                        objJE.DebitAmt = 0
                        objJE.CreditAmt = Math.Abs(TotalAmountVariationDueToCurrencyExRate)
                    ElseIf TotalAmountVariationDueToCurrencyExRate > 0 Then
                        objJE.DebitAmt = Math.Abs(TotalAmountVariationDueToCurrencyExRate)
                        objJE.CreditAmt = 0
                    End If

                    objJE.ChartAcntId = lngDefaultForeignExchangeAccountID
                    objJE.Description = "Exchange Gain/Loss"
                    objJE.CustomerId = CCommon.ToLong(radCmbCompany.SelectedValue)
                    objJE.MainDeposit = 0
                    objJE.MainCheck = 0
                    objJE.MainCashCredit = 0
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = ""
                    objJE.Reference = ""
                    objJE.PaymentMethod = CCommon.ToLong(radcmbPaymentMethods.SelectedValue)
                    objJE.Reconcile = False
                    objJE.CurrencyID = CCommon.ToLong(radcmbCurrency.SelectedValue)
                    objJE.FltExchangeRate = FltExchangeRate
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = 0
                    objJE.ReferenceType = enmReferenceType.DepositDetail
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If
            End If

            objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalID, Session("DomainID"))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadItemAttributes(ByVal itemCode As Long)
        Try
            objItems = New CItems
            objItems.ItemCode = itemCode
            objItems.WareHouseItemID = 0
            Dim ds As DataSet = objItems.GetItemWareHouses()

            If Not ds Is Nothing AndAlso ds.Tables.Count >= 3 AndAlso ds.Tables(2).Rows.Count > 0 Then
                rptAttributes.DataSource = ds.Tables(2)
                rptAttributes.DataBind()

                divItemAttributes.Style.Add("display", "")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindShippingCompanyAndMethods(ByRef radShipVia As RadComboBox, ByRef radCmbShippingMethod As RadComboBox)
        Try
            If objCommon Is Nothing Then objCommon = New CCommon
            radShipVia.DataSource = objCommon.GetMasterListItems(82, Session("DomainID"))
            radShipVia.DataTextField = "vcData"
            radShipVia.DataValueField = "numListItemID"
            radShipVia.DataBind()

            Dim listItem As Telerik.Web.UI.RadComboBoxItem
            If radShipVia.Items.FindItemByText("Fedex") IsNot Nothing Then
                radShipVia.Items.Remove(radShipVia.Items.FindItemByText("Fedex"))
            End If
            If radShipVia.Items.FindItemByText("UPS") IsNot Nothing Then
                radShipVia.Items.Remove(radShipVia.Items.FindItemByText("UPS"))
            End If
            If radShipVia.Items.FindItemByText("USPS") IsNot Nothing Then
                radShipVia.Items.Remove(radShipVia.Items.FindItemByText("USPS"))
            End If

            listItem = New Telerik.Web.UI.RadComboBoxItem
            listItem.Text = "-- Select One --"
            listItem.Value = "0"
            radShipVia.Items.Insert(0, listItem)

            listItem = New Telerik.Web.UI.RadComboBoxItem
            listItem.Text = "Fedex"
            listItem.Value = "91"
            listItem.ImageUrl = "../images/FedEx.png"
            radShipVia.Items.Insert(1, listItem)

            listItem = New Telerik.Web.UI.RadComboBoxItem
            listItem.Text = "UPS"
            listItem.Value = "88"
            listItem.ImageUrl = "../images/UPS.png"
            radShipVia.Items.Insert(2, listItem)

            listItem = New Telerik.Web.UI.RadComboBoxItem
            listItem.Text = "USPS"
            listItem.Value = "90"
            listItem.ImageUrl = "../images/USPS.png"
            radShipVia.Items.Insert(3, listItem)

            radCmbShippingMethod.Items.Clear()
            radCmbShippingMethod.Items.Add(New RadComboBoxItem("-- Select One --", "0"))
            OppBizDocs.LoadServiceTypes(radShipVia.SelectedValue, radCmbShippingMethod)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Function CheckIfUnitPriceApprovalRequired() As Boolean
        Try
            Dim isApprovalRequired As Boolean = False

            Dim objOpportunity As New MOpportunity
            objOpportunity.DomainID = CCommon.ToLong(Session("DomainID"))
            objOpportunity.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
            objOpportunity.CurrencyID = CCommon.ToLong(radcmbCurrency.SelectedValue)
            'Checks whether unit price approval is required for any item added in order
            isApprovalRequired = objOpportunity.CheckIfUnitPriceApprovalRequired(dsTemp)
            hdnIsUnitPriceApprovalRequired.Value = isApprovalRequired

            UpdateDetails()
            UpdatePanelItems.Update()
            Return isApprovalRequired
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Function CheckIfMinOrderAmountRuleMeets() As DataTable
        Try
            Dim isMinOrderAmountRuleMeets As Boolean = False
            Dim _totalAmount As Decimal
            Dim dt = dsTemp.Tables(0)

            _totalAmount = dt.AsEnumerable().Sum(Function(row) row.Field(Of Decimal)("monTotAmount"))
            Dim objOpportunity As New MOpportunity
            objOpportunity.DomainID = CCommon.ToLong(Session("DomainID"))
            objOpportunity.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
            Return objOpportunity.CheckIfMinOrderAmountRuleMeets(_totalAmount)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub LoadSelectedItemDetail(ByVal itemCode As Long, Optional ByVal LoadAttributes As Boolean = True, Optional ByVal emptyFieldsInitially As Boolean = False)
        Try
            LoadMarkupDiscount()

            If divNewCustomer.Visible = False AndAlso CCommon.ToLong(radCmbCompany.SelectedValue) = 0 AndAlso divbulksales.Visible = False Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "CustomerValidation", "$('#txtItem').select2(""data"", """"); alert(""Select customer.""); $(""#CustomerSection"").collapse('show')", True)
                Return
            ElseIf divNewCustomer.Visible = False AndAlso CCommon.ToLong(radcmbContact.SelectedValue) = 0 AndAlso divbulksales.Visible = False Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "CustomerValidation", "$('#txtItem').select2(""data"", """"); alert(""Select contact."")", True)
                Return
            End If

            Dim objItem As New CItems
            If Not objItem.ValidateItemAccount(CCommon.ToLong(itemCode), Session("DomainID"), 1) Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "CustomerValidation", "$('#txtItem').select2(""data"", """"); alert(""Item Income,Asset,COGs(Expense) Account are not set. To set Income,Asset,COGs(Expense) Account go to Administration->Inventory->Item Details."")", True)
                Return
            End If

            If String.IsNullOrEmpty(txtUnits.Text) Then
                txtUnits.Text = "0"
            End If

            Dim objOpportunity As New COpportunities
            objOpportunity.DomainID = Session("DomainID")
            objOpportunity.DivisionID = IIf(divbulksales.Visible = False, CCommon.ToLong(radCmbCompany.SelectedValue), 0)
            objOpportunity.OppType = 1
            objOpportunity.ItemCode = itemCode
            objOpportunity.UnitHour = txtUnits.Text
            objOpportunity.CurrencyID = CCommon.ToLong(radcmbCurrency.SelectedValue)
            If divLocation.Visible Then
                objOpportunity.WarehouseID = CCommon.ToLong(radcmbLocation.SelectedValue)
            Else
                objOpportunity.WarehouseItmsID = CCommon.ToLong(radWareHouse.SelectedValue)
            End If

            Dim ds As DataSet = objOpportunity.GetOrderItemDetails(CCommon.ToLong(radcmbUOM.SelectedValue), hdnKitChildItems.Value, CCommon.ToLong(hdnCountry.Value))

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                If CCommon.ToLong(txtHidEditOppItem.Text) = 0 Then
                    radcmbType.SelectedValue = CCommon.ToString(ds.Tables(0).Rows(0)("ItemType"))
                    lblItemType.Text = radcmbType.SelectedItem.Text
                    If radcmbType.SelectedValue = "P" Then
                        Dim objContact As New CContacts
                        objContact.AddressType = CContacts.enmAddressType.ShipTo
                        objContact.DomainID = Session("DomainID")
                        If (radCmbCompany.SelectedValue = "") Then
                            objContact.RecordID = 0
                        Else
                            objContact.RecordID = radCmbCompany.SelectedValue
                        End If
                        objContact.AddresOf = CContacts.enmAddressOf.Organization
                        objContact.byteMode = 2
                        dtLocationsGrid = objContact.GetAddressDetail()

                        If chkPickUpWillCall.Checked = True Then
                            dtLocationsGrid.Rows.Clear()
                            Dim dr As DataRow
                            dr = dtLocationsGrid.NewRow()
                            If radcmbLocation.SelectedItem IsNot Nothing Then
                                dr("FullAddress") = radcmbLocation.SelectedItem.Attributes("Address")
                                dr("numAddressID") = radcmbLocation.SelectedItem.Attributes("numAddressID")
                                dtLocationsGrid.Rows.Add(dr)
                            End If
                        End If

                        If objCommon Is Nothing Then objCommon = New CCommon
                        Dim dtWareHouses As DataTable = objCommon.GetWarehousesForSelectedItem(itemCode, IIf(radcmbLocation.SelectedValue = "" Or (CCommon.ToBool(hdnIsAutoWarehouseSelection.Value) AndAlso hdnWarehousePriority.Value <> "-1"), 0, CCommon.ToLong(radcmbLocation.SelectedValue)), 1, GetOrderSource(), 1, CCommon.ToLong(hdnShipToCountry.Value), CCommon.ToLong(hdnShipToState.Value), hdnKitChildItems.Value)
                        radWareHouse.DataSource = dtWareHouses
                        radWareHouse.DataBind()

                        gvLocations.DataSource = dtWareHouses
                        gvLocations.DataBind()

                        If Not radWareHouse.FindItemByValue(CCommon.ToString(ds.Tables(0).Rows(0)("numWarehouseItemID"))) Is Nothing Then
                            radWareHouse.FindItemByValue(CCommon.ToString(ds.Tables(0).Rows(0)("numWarehouseItemID"))).Selected = True
                        End If

                        divLocations.Visible = True
                        divWarehouse.Visible = True
                        chkDropShip.Visible = True

                        If (hdnDropShp.Value = "1") Then         ' Drop Ship check from Edit Address window
                            chkDropShip.Checked = True
                            chkDropShip.Enabled = False
                        Else
                            chkDropShip.Checked = CCommon.ToBool(ds.Tables(0).Rows(0)("bitDropship"))
                            chkDropShip.Enabled = True
                        End If

                        If chkDropShip.Checked Then
                            divWarehouse.Visible = False
                            divLocations.Visible = False
                        End If
                    Else
                        chkDropShip.Visible = False
                        chkDropShip.Checked = CCommon.ToBool(ds.Tables(0).Rows(0)("bitDropship"))
                        divWarehouse.Visible = False
                        radWareHouse.SelectedValue = ""
                        divLocations.Visible = False
                    End If
                    txtdesc.Text = CCommon.ToString(ds.Tables(0).Rows(0)("ItemDescription"))
                    hdnListPrice.Value = CCommon.ToDecimal(ds.Tables(0).Rows(0)("monListPrice"))
                    hdnLastPrice.Value = CCommon.ToDecimal(ds.Tables(0).Rows(0)("monLastPrice"))
                    hdnVendorCost.Value = CCommon.ToDecimal(ds.Tables(0).Rows(0)("VendorCost"))
                    hdnPrimaryVendorID.Value = CCommon.ToLong(ds.Tables(0).Rows(0)("numVendorID"))
                    lblBaseUOMName.Text = CCommon.ToString(ds.Tables(0).Rows(0)("vcBaseUOMName"))
                    hdnBaseUOMName.Value = CCommon.ToString(ds.Tables(0).Rows(0)("vcBaseUOMName"))
                    hdnItemClassification.Value = CCommon.ToLong(ds.Tables(0).Rows(0)("ItemClassification"))
                    hdnIsMatrix.Value = CCommon.ToBool(ds.Tables(0).Rows(0)("bitMatrix"))
                    txtNotes.Text = CCommon.ToString(ds.Tables(0).Rows(0)("vcVendorNotes"))  'Added by Priya

                    If CCommon.ToBool(hdnIsSKUMatch.Value) AndAlso CCommon.ToBool(hdnIsMatrix.Value) Then
                        LoadAttributes = False
                    End If

                    txtUOMConversionFactor.Text = CCommon.ToString(ds.Tables(0).Rows(0)("UOMConversionFactor"))
                    If CCommon.ToBool(ds.Tables(0).Rows(0)("bitAssembly")) Then
                        divWorkOrder.Visible = True

                        If CCommon.ToBool(ds.Tables(0).Rows(0).Item("bitSOWorkOrder")) Then
                            chkWorkOrder.Checked = True
                            hdnIsCreateWO.Value = "1"
                        Else
                            hdnIsCreateWO.Value = "0"
                        End If

                        lblWOQty.Text = CCommon.ToString(ds.Tables(0).Rows(0)("MaxWorkOrderQty"))
                    End If

                    If Not radcmbUOM.FindItemByValue(CCommon.ToString(ds.Tables(0).Rows(0)("numSaleUnit"))) Is Nothing Then
                        radcmbUOM.FindItemByValue(CCommon.ToString(ds.Tables(0).Rows(0)("numSaleUnit"))).Selected = True
                    End If

                    BindPriceLevel(itemCode)
                    'Checking Promotion
                    txtprice.Text = CCommon.ToDecimal(ds.Tables(0).Rows(0)("monPrice"))
                    hdnUnitCost.Value = CCommon.ToDecimal(txtprice.Text)
                    txtItemDiscount.Text = CCommon.ToString(ds.Tables(0).Rows(0)("Discount"))

                    If CCommon.ToDouble(ds.Tables(0).Rows(0)("Discount")) <> 0 Then
                        If CCommon.ToString(ds.Tables(0).Rows(0)("DiscountType")) = "1" Then
                            radPer.Checked = True
                            radAmt.Checked = False
                        Else
                            radAmt.Checked = True
                            radPer.Checked = False
                        End If
                    End If

                    If CCommon.ToBool(ds.Tables(0).Rows(0)("bitMarkupDiscount")) Then
                        ddlMarkupDiscountOption.SelectedValue = "1"
                    Else
                        ddlMarkupDiscountOption.SelectedValue = "0"
                    End If

                    SetPricingOption(ds.Tables(0).Rows(0))

                    CalculateProfit()

                    If LoadAttributes And ((ds.Tables(0).Rows(0)("ItemType") <> "P" AndAlso ds.Tables(0).Rows(0)("ItemGroup") > 0) Or (ds.Tables(0).Rows(0)("ItemGroup") > 0 And CCommon.ToBool(ds.Tables(0).Rows(0)("bitMatrix")))) Then
                        LoadItemAttributes(itemCode)
                    End If

                    If plhItemDetails.Visible Then
                        LoadSelectedItemDetailSection(itemCode)
                    End If

                    If CCommon.ToBool(ds.Tables(0).Rows(0)("bitKitParent")) And Not CCommon.ToBool(ds.Tables(0).Rows(0)("bitHasChildKits")) Then
                        lkbEditKitSelection.Visible = True

                        Dim objTempItem As New CItems
                        objTempItem.ItemCode = itemCode
                        objTempItem.ChildItemID = 0
                        Dim dsItem As DataSet = objTempItem.GetSelectedChildItemDetails("", False)

                        If Not dsItem Is Nothing And dsItem.Tables.Count > 1 Then
                            gvChildItems.DataSource = dsItem.Tables(1)
                            gvChildItems.DataBind()
                        Else
                            gvChildItems.DataSource = Nothing
                            gvChildItems.DataBind()
                        End If

                        UpdatePanel3.Update()

                        If CCommon.ToBool(ds.Tables(0).Rows(0)("bitCalAmtBasedonDepItems")) Then
                            divKitPrice.Visible = True
                            hdnKitPricing.Value = CCommon.ToShort(ds.Tables(0).Rows(0)("tintKitAssemblyPriceBasedOn"))
                            hdnKitListPrice.Value = CCommon.ToDouble(ds.Tables(0).Rows(0)("monListPrice"))

                            If CCommon.ToShort(ds.Tables(0).Rows(0)("tintKitAssemblyPriceBasedOn")) = 4 Then
                                lblKitPriceType.Text = "Kit/Assembly list price plus sum total of child item's list price"
                            ElseIf CCommon.ToShort(ds.Tables(0).Rows(0)("tintKitAssemblyPriceBasedOn")) = 3 Then
                                lblKitPriceType.Text = "Sum total of child item's Primary Vendor Cost"
                            ElseIf CCommon.ToShort(ds.Tables(0).Rows(0)("tintKitAssemblyPriceBasedOn")) = 2 Then
                                lblKitPriceType.Text = "Sum total of child item's Average Cost"
                            Else
                                lblKitPriceType.Text = "Sum total of child item's List Price"
                            End If
                        End If
                    Else
                        lkbEditKitSelection.Visible = False
                    End If
                Else
                    SetPricingOption(ds.Tables(0).Rows(0))
                End If

                divPricingOption.Visible = True
                hplRelatedItems.Text = "Related Items(" & CCommon.ToLong(ds.Tables(0).Rows(0)("RelatedItemsCount")) & ")"
                hplRelatedItems.Visible = True
            End If

            UpdatePanelItems.Update()
            updatePanelItemTransit.Update()
            UPMatrixAndKitAttributes.Update()
            UpdateItemPromotion.Update()
            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideItemDescription", "HideItemDescription()", True)
            OrderPromotion()
            ShippingPromotion()
        Catch ex As Exception
            If ex.Message.Contains("WAREHOUSE_DOES_NOT_EXISTS") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "CustomerValidation", "$('#txtItem').select2(""data"", """"); alert(""No warehouse available for selected item."")", True)
            Else
                Throw
            End If
        End Try
    End Sub

    Private Sub OrderPromotion()
        Try
            Dim objOpportunity As New COpportunities
            objOpportunity.DomainID = Session("DomainID")
            objOpportunity.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
            objOpportunity.PromotionID = CCommon.ToLong(hdnApplyCouponCode.Value)
            Dim numSubTotal As Double = CCommon.ToDouble(lblTotal.Text)

            Dim dsOrderProm As DataSet = objOpportunity.GetPromotiondRuleForOrder(CCommon.ToDouble(lblTotal.Text))

            If dsOrderProm IsNot Nothing And dsOrderProm.Tables.Count >= 2 Then

                divOrderPromotionDetail.Visible = True

                Dim dtOrderPro As DataTable = dsOrderProm.Tables(0)
                Dim dtOrderDiscounts As DataTable = dsOrderProm.Tables(1)

                If (dsOrderProm.Tables(0).Rows.Count > 0) Then

                    Dim StrRuleDesc As String = ""
                    Dim RowCount As Integer = dtOrderDiscounts.Rows.Count
                    hdnOrderPromId.Value = dtOrderPro.Rows(0)("numProID").ToString()

                    For Each drrow As DataRow In dtOrderDiscounts.Rows
                        If (numSubTotal = 0 And CCommon.ToInteger(drrow("RowNum")) = 1) Then
                            If dtOrderPro.Rows(0)("tintDiscountType").ToString = "1" Then
                                StrRuleDesc = StrRuleDesc + "Spend $" & (CCommon.ToDouble(drrow("numOrderAmount")) - numSubTotal) & " and get " & drrow("fltDiscountValue").ToString() & "% off ENTIRE ORDER. "
                            ElseIf dtOrderPro.Rows(0)("tintDiscountType").ToString = "2" Then
                                StrRuleDesc = StrRuleDesc + "Spend $" & (CCommon.ToDouble(drrow("numOrderAmount")) - numSubTotal) & " and get $" & drrow("fltDiscountValue").ToString() & " off ENTIRE ORDER. "
                            End If
                        ElseIf (numSubTotal < CCommon.ToDouble(drrow("numOrderAmount"))) Then
                            If dtOrderPro.Rows(0)("tintDiscountType").ToString = "1" Then
                                StrRuleDesc = StrRuleDesc + "Spend $" & (CCommon.ToDouble(drrow("numOrderAmount")) - numSubTotal) & " more to make it " & drrow("fltDiscountValue").ToString() & "%. "
                            ElseIf dtOrderPro.Rows(0)("tintDiscountType").ToString = "2" Then
                                StrRuleDesc = StrRuleDesc + "Spend $" & (CCommon.ToDouble(drrow("numOrderAmount")) - numSubTotal) & " more to make it $" & drrow("fltDiscountValue").ToString() & ". "
                            End If
                        ElseIf (numSubTotal >= CCommon.ToDouble(drrow("numOrderAmount"))) And CCommon.ToInteger(drrow("RowNum")) <> RowCount Then
                            If dtOrderPro.Rows(0)("tintDiscountType").ToString = "1" Then
                                StrRuleDesc = "Order has qualified for " & drrow("fltDiscountValue").ToString() & "% discount. "
                                bitPromotionDiscountType = True
                            ElseIf dtOrderPro.Rows(0)("tintDiscountType").ToString = "2" Then
                                StrRuleDesc = "Order has qualified for $" & drrow("fltDiscountValue").ToString() & " discount. "
                                bitPromotionDiscountType = False
                            End If
                            lngDiscountAmt = CCommon.ToDouble(drrow("fltDiscountValue").ToString())

                        ElseIf (numSubTotal >= CCommon.ToDouble(drrow("numOrderAmount")) And CCommon.ToInteger(drrow("RowNum")) = RowCount) Then
                            If dtOrderPro.Rows(0)("tintDiscountType").ToString = "1" Then
                                StrRuleDesc = "Congratulations, this order qualifies for a " & drrow("fltDiscountValue").ToString() & "% discount (to be applied when order is placed). "
                                bitPromotionDiscountType = True
                            ElseIf dtOrderPro.Rows(0)("tintDiscountType").ToString = "2" Then
                                StrRuleDesc = "Congratulations, this order qualifies for a $" & drrow("fltDiscountValue").ToString() & " discount (to be applied when order is placed). "
                                bitPromotionDiscountType = False
                            End If
                            lngDiscountAmt = CCommon.ToDouble(drrow("fltDiscountValue").ToString())
                        End If
                    Next
                    lblOrderPromotionDesc.Text = StrRuleDesc

                    If (CCommon.ToLong(dtOrderPro.Rows(0)("bitRequireCouponCode")) = 1) Then
                        lblOrderPromotionDesc.Text = lblOrderPromotionDesc.Text + "<i> (Coupon code required) </i>"
                        bitRequireCouponCode = True
                    End If
                Else
                    divOrderPromotionDetail.Visible = False
                End If
            Else
                hdnOrderPromId.Value = ""
                divOrderPromotionDetail.Visible = False
            End If

            UpdateOrderPromRule.Update()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ShippingPromotion()
        Try
            Dim objOpportunity As New COpportunities
            objOpportunity.DomainID = Session("DomainID")
            objOpportunity.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
            objOpportunity.WarehouseID = CCommon.ToLong(radcmbLocation.SelectedValue)

            Dim numSubTotal As Double = CCommon.ToDouble(lblTotal.Text)

            Dim dsShippingProm As DataSet = objOpportunity.GetShippingRuleDetails(hdnPostal.Value, CCommon.ToLong(hdnState.Value), CCommon.ToLong(hdnCountry.Value))

            If dsShippingProm IsNot Nothing And dsShippingProm.Tables.Count > 0 Then

                Dim dtShipRule As DataTable = dsShippingProm.Tables(0)
                Dim dtRates As DataTable = dsShippingProm.Tables(1)

                If (dtShipRule.Rows.Count > 0) Then
                    divShippingProm.Visible = True
                    hdnShipPromId.Value = dtShipRule.Rows(0)("numRuleID").ToString

                    Dim StrRuleDesc As String = ""

                    If (dtRates.Rows.Count > 0) Then
                        Dim RowCount As Integer = dtRates.Rows.Count

                        If numSubTotal = 0 Then
                            If hdnPostal.Value <> "" Then
                                StrRuleDesc = "Shipping to " & hdnPostal.Value & " is $" & dtRates.Rows(0)("monRate").ToString() & "."
                            Else
                                StrRuleDesc = "Shipping to " & hdnStateName.Value & " is $" & dtRates.Rows(0)("monRate").ToString() & "."
                            End If
                            If (CCommon.ToBool(dtShipRule.Rows(0)("bitFreeShipping")) = True) Then
                                StrRuleDesc = StrRuleDesc + "Orders over $" & dtShipRule.Rows(0)("FreeShippingOrderAmt").ToString & " get free shipping !"
                            End If
                        End If
                        If numSubTotal > 0 Then
                            If (CCommon.ToBool(dtShipRule.Rows(0)("bitFreeShipping")) = True) Then

                                If (numSubTotal < CCommon.ToDouble(dtShipRule.Rows(0)("FreeShippingOrderAmt"))) Then
                                    StrRuleDesc = "Orders over $" & dtShipRule.Rows(0)("FreeShippingOrderAmt").ToString & " get free shipping !"
                                    For Each drrow As DataRow In dtRates.Rows
                                        If ((numSubTotal >= CCommon.ToDouble(drrow("intFrom")) And numSubTotal <= CCommon.ToDouble(drrow("intTo"))) And CCommon.ToInteger(drrow("RowNum")) = 1) Then
                                            StrRuleDesc = StrRuleDesc + " , order already qualifies for $" & drrow("monRate").ToString() & " shipping."
                                            ShippingAmt = CCommon.ToDouble(drrow("monRate"))
                                            bitFreeShipping = False
                                        ElseIf (numSubTotal < CCommon.ToDouble(drrow("intFrom"))) Then
                                            StrRuleDesc = StrRuleDesc + " or spend $" & (CCommon.ToDouble(drrow("intTo")) - numSubTotal) & " more for $" & drrow("monRate").ToString() & " shipping."
                                        ElseIf (numSubTotal >= CCommon.ToDouble(drrow("intFrom")) And numSubTotal <= CCommon.ToDouble(drrow("intTo"))) Then
                                            StrRuleDesc = StrRuleDesc + " , order already qualifies for $" & drrow("monRate").ToString() & " shipping."
                                            ShippingAmt = CCommon.ToDouble(drrow("monRate"))
                                            bitFreeShipping = False
                                        ElseIf numSubTotal >= CCommon.ToDouble(drrow("intTo")) And CCommon.ToInteger(drrow("RowNum")) = RowCount Then
                                            StrRuleDesc = "This order has qualified for $" & drrow("monRate").ToString() & " shipping."
                                            ShippingAmt = CCommon.ToDouble(drrow("monRate"))
                                            bitFreeShipping = False
                                        End If
                                    Next
                                ElseIf (numSubTotal >= CCommon.ToDouble(dtShipRule.Rows(0)("FreeShippingOrderAmt"))) Then
                                    StrRuleDesc = "This order will be picked up via Will-Call ! "
                                    ShippingAmt = 0
                                    bitFreeShipping = True
                                End If

                            ElseIf (CCommon.ToBool(dtShipRule.Rows(0)("bitFreeShipping")) = False And numSubTotal > 0) Then
                                For Each drrow As DataRow In dtRates.Rows
                                    If ((numSubTotal >= CCommon.ToDouble(drrow("intFrom")) And numSubTotal <= CCommon.ToDouble(drrow("intTo"))) And CCommon.ToInteger(drrow("RowNum")) = 1) Then
                                        StrRuleDesc = "Order already qualifies for $" & drrow("monRate").ToString() & " shipping."
                                        ShippingAmt = CCommon.ToDouble(drrow("monRate"))
                                        bitFreeShipping = False
                                    ElseIf (numSubTotal < CCommon.ToDouble(drrow("intFrom"))) Then
                                        StrRuleDesc = StrRuleDesc + " or spend $" & (CCommon.ToDouble(drrow("intTo")) - numSubTotal) & " more for $" & drrow("monRate").ToString() & " shipping."
                                    ElseIf (numSubTotal >= CCommon.ToDouble(drrow("intFrom")) And numSubTotal <= CCommon.ToDouble(drrow("intTo"))) Then
                                        StrRuleDesc = StrRuleDesc + " Order already qualifies for $" & drrow("monRate").ToString() & " shipping."
                                        ShippingAmt = CCommon.ToDouble(drrow("monRate"))
                                        bitFreeShipping = False
                                    ElseIf numSubTotal >= CCommon.ToDouble(drrow("intTo")) And CCommon.ToInteger(drrow("RowNum")) = RowCount Then
                                        StrRuleDesc = "This order has qualified for $" & drrow("monRate").ToString() & " shipping."
                                        ShippingAmt = CCommon.ToDouble(drrow("monRate"))
                                        bitFreeShipping = False
                                    End If
                                Next
                            End If
                        End If
                    ElseIf (CCommon.ToBool(dtShipRule.Rows(0)("bitFreeShipping")) = True) Then
                        If (numSubTotal < CCommon.ToDouble(dtShipRule.Rows(0)("FreeShippingOrderAmt"))) Then
                            StrRuleDesc = "Orders over $" & dtShipRule.Rows(0)("FreeShippingOrderAmt").ToString & " get free shipping !"
                        ElseIf (numSubTotal >= CCommon.ToDouble(dtShipRule.Rows(0)("FreeShippingOrderAmt"))) Then
                            StrRuleDesc = "This order will be picked up via Will-Call ! "
                            ShippingAmt = 0
                            bitFreeShipping = True
                        End If
                    End If
                    lblShipPromDesc.Text = StrRuleDesc
                Else
                    divShippingProm.Visible = False
                End If

                Dim objOpp As New COpportunities
                objOpp.DomainID = Session("DomainID")

                hdnstrShipItemCodes.Value = ""

                If (dgItems.Items.Count > 0) Then
                    For Each dgGridItem As DataGridItem In dgItems.Items
                        hdnstrShipItemCodes.Value = hdnstrShipItemCodes.Value + "," + dgItems.DataKeys(dgGridItem.ItemIndex).ToString
                    Next
                End If

                hdnstrShipItemCodes.Value = hdnstrShipItemCodes.Value + "," + hdnCurrentSelectedItem.Value

                If (hdnstrShipItemCodes.Value = "") Then
                    hdnstrShipItemCodes.Value = "0"
                End If

                Dim dtShipExceptions As DataTable = objOpp.GetShippingExceptionsForOrder(hdnstrShipItemCodes.Value.ToString, ShippingAmt, bitFreeShipping)

                If (dtShipExceptions.Rows.Count > 0) And (divShippingProm.Visible = True) Then
                    lnkShippingExceptions.Visible = True
                    dgShippingExceptions.DataSource = dtShipExceptions
                    dgShippingExceptions.DataBind()
                Else
                    lnkShippingExceptions.Visible = False
                End If

            End If
            UpdateShipPromRule.Update()
        Catch ex As Exception
            If ex.Message.Contains("WAREHOUSE_DOES_NOT_EXISTS") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "CustomerValidation", "$('#txtItem').select2(""data"", """"); alert(""No warehouse available for selected item."")", True)
            Else
                Throw
            End If
        End Try
    End Sub

    Public Function ItemPromotion(ByVal numItemCode As Long, ByVal numPromotionID As Long, ByVal numUnitHour As Double, ByVal monTotalAmount As Double) As DataTable
        Try
            Dim objPromotionOffer As New PromotionOffer
            objPromotionOffer.DomainID = Session("DomainID")
            objPromotionOffer.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
            objPromotionOffer.PromotionID = numPromotionID
            Dim dtItemProm As DataTable = objPromotionOffer.GetItemPromotionDetails(numItemCode, numUnitHour, monTotalAmount)

            If Not dtItemProm Is Nothing Then
                Return dtItemProm
            Else
                Return Nothing
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub SetPricingOption(ByVal dr As DataRow)
        Try
            hdnLastPrice.Value = CCommon.ToDecimal(dr("monLastPrice"))
            lkbListPrice.Text = "Use List Price (" & CCommon.ToString(dr("monListPrice")) & ")"
            lkbLastPrice.Text = "Use Last Price (" & CCommon.ToString(dr("monLastPrice")) & "," & CCommon.ToString(dr("LastOrderDate")) & ")"
            hdnLastOrderDate.Value = CCommon.ToString(dr("LastOrderDate"))
            hplLastPrices.Visible = True

            If dr("tintPriceBaseOn") = "1" Then 'Price Level
                hdnPricingBasedOn.Value = "1"
                lkbPriceLevel.Text = "<i class=""fa fa-check""></i>&nbsp; Use Price Level"
            ElseIf dr("tintPriceBaseOn") = "2" Then 'Price Rule
                hdnPricingBasedOn.Value = "2"
                lkbPriceRule.Text = "<i class=""fa fa-check""></i>&nbsp; Use Price Rule"
            ElseIf dr("tintPriceBaseOn") = "3" Then 'Last Price
                hdnPricingBasedOn.Value = "3"
                lkbListPrice.Text = "<i class=""fa fa-check""></i>&nbsp;Use Last Price (" & CCommon.ToString(dr("monLastPrice")) & "," & CCommon.ToString(dr("LastOrderDate")) & ")"
            ElseIf dr("tintPriceBaseOn") = "4" Then 'Calculated Based On Dependent Item
                hdnPricingBasedOn.Value = "4"
            Else
                lkbListPrice.Text = "<i class=""fa fa-check""></i>&nbsp;Use List Price (" & CCommon.ToString(dr("monListPrice")) & ")"
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CalculateItemSalePrice(ByVal price As Decimal, ByVal quantity As Decimal, ByVal discount As Decimal, ByVal isPercent As Boolean, ByVal uomConversionFactor As Decimal, ByVal bitMarkupDiscount As String)
        Try
            If isPercent Then
                If (bitMarkupDiscount = "1") Then    'Markup N Discount Logic 
                    hdnItemSalePrice.Value = price + (price * (discount / 100))
                Else
                    hdnItemSalePrice.Value = price - (price * (discount / 100))
                End If
            Else
                If quantity > 0 Then
                    If (bitMarkupDiscount = "1") Then   'Markup N Discount Logic 
                        hdnItemSalePrice.Value = ((price * (quantity * uomConversionFactor)) + discount) / (quantity * uomConversionFactor)
                    Else
                        hdnItemSalePrice.Value = ((price * (quantity * uomConversionFactor)) - discount) / (quantity * uomConversionFactor)
                    End If
                Else
                    hdnItemSalePrice.Value = 0
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    'Change Price

    Private Sub ItemFieldsChanged(ByVal itemCOde As Long, Optional ByVal isEditClicked As Boolean = False)
        Try
            Dim objOpportunity As New COpportunities
            objOpportunity.DomainID = Session("DomainID")
            objOpportunity.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
            objOpportunity.OppType = 1
            objOpportunity.ItemCode = itemCOde
            objOpportunity.UnitHour = If(txtUnits.Text = "", txtUnits.Text = "0", txtUnits.Text)
            objOpportunity.WarehouseItmsID = CCommon.ToLong(radWareHouse.SelectedValue)
            objOpportunity.CurrencyID = CCommon.ToLong(radcmbCurrency.SelectedValue)
            Dim ds As DataSet = objOpportunity.GetOrderItemDetails(CCommon.ToLong(radcmbUOM.SelectedValue), hdnKitChildItems.Value, CCommon.ToLong(hdnCountry.Value))

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                hdnListPrice.Value = CCommon.ToDecimal(ds.Tables(0).Rows(0)("monListPrice"))
                hdnVendorCost.Value = CCommon.ToDecimal(ds.Tables(0).Rows(0)("VendorCost"))
                hplRelatedItems.Text = "Related Items(" & CCommon.ToLong(ds.Tables(0).Rows(0)("RelatedItemsCount")) & ")"
                hplRelatedItems.Visible = True

                If Not isEditClicked Then
                    txtprice.Text = CCommon.ToDecimal(ds.Tables(0).Rows(0)("monPrice"))
                    hdnUnitCost.Value = CCommon.ToDecimal(txtprice.Text)
                    txtItemDiscount.Text = CCommon.ToString(ds.Tables(0).Rows(0)("Discount"))

                    If CCommon.ToDouble(ds.Tables(0).Rows(0)("Discount")) <> 0 Then
                        If CCommon.ToString(ds.Tables(0).Rows(0)("DiscountType")) = "1" Then
                            radPer.Checked = True
                            radAmt.Checked = False
                        Else
                            radAmt.Checked = True
                            radPer.Checked = False
                        End If
                    End If

                    If CCommon.ToBool(ds.Tables(0).Rows(0)("bitMarkupDiscount")) Then
                        ddlMarkupDiscountOption.SelectedValue = "1"
                    Else
                        ddlMarkupDiscountOption.SelectedValue = "0"
                    End If

                    If dsTemp Is Nothing AndAlso ViewState("SOItems") IsNot Nothing Then
                        dsTemp = ViewState("SOItems")
                    ElseIf dsTemp Is Nothing AndAlso ViewState("SOItems") Is Nothing Then
                        Exit Sub
                    End If

                    Dim dtAllItems As DataTable
                    dtAllItems = dsTemp.Tables(0)


                    UseItemPromotionIfAvailable(dtAllItems, False, "", isFromNewItem:=True)
                End If

                SetPricingOption(ds.Tables(0).Rows(0))
                CalculateProfit()
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideItemSearch", "$('.itemsArea').css('visibility', 'visible');$('.itemsArea').css('display', 'flow-root');", True)
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub RelatedItems(IsPreCheckout As Boolean)
        Dim objItems As New CItems
        objItems.DomainID = CCommon.ToLong(Session("DomainID"))
        objItems.ParentItemCode = hdnCurrentSelectedItem.Value
        If IsPreCheckout = False Then
            Session("IsPreCheckout") = "False"
            objItems.byteMode = 2
        ElseIf IsPreCheckout = True Then
            Session("IsPreCheckout") = "True"
            objItems.byteMode = 8
        End If
        objItems.WarehouseID = CCommon.ToLong(radcmbLocation.SelectedValue)
        Dim dtItemPrepopup As DataTable
        dtItemPrepopup = objItems.GetSimilarItem

        If dtItemPrepopup IsNot Nothing AndAlso dtItemPrepopup.Rows.Count > 0 Then
            hdnFlagRelatedItems.Value = "True"
            If dtRelatedItems Is Nothing Then
                dtRelatedItems = New DataTable()
            End If
            'Code to import child item rows in the shared datatable object - Neelam
            If dtRelatedItems IsNot Nothing Then
                If dtRelatedItems.Rows.Count > 0 Then
                    For Each d As DataRow In dtItemPrepopup.Rows
                        dtRelatedItems.ImportRow(d)
                    Next
                Else
                    dtRelatedItems = dtItemPrepopup
                End If
            End If

            gvRelatedItems.DataSource = dtItemPrepopup
            gvRelatedItems.DataBind()

            'divRelatedItems.Style.Add("display", "")
            Dim script As String = "function f(){$find(""" + RadWinRelatedItems.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, True)
            UpdatePanelActionWindow.Update()

            'Dim dt = RelatedItemsPromotion(dtItemPrepopup.Rows(0)("numItemCode"), dtItemPrepopup.Rows(0)("numWareHouseItemID
            'Dim dt = RelatedItemsPromotion(hdnCurrentSelectedItem.Value, dtItemPrepopup.Rows(0)("numWareHouseItemID"))

            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideItemSearch", "$('.itemsArea').css('visibility', 'visible');$('.itemsArea').css('display', 'flow-root');", True)
        Else
            hdnFlagRelatedItems.Value = "False"
        End If
    End Sub

    Private Sub BindMultiSelectItemsToDatatable(ByVal lngCustId As Long)
        Try
            If Not ViewState("dtMultiSelect") Is Nothing Then
                dtMultiSelect = DirectCast(ViewState("dtMultiSelect"), DataTable)
            Else
                dtMultiSelect = New DataTable()
                dtMultiSelect.Columns.Add("ID", GetType(String))
                dtMultiSelect.Columns.Add("numItemCode", GetType(String))
                dtMultiSelect.Columns.Add("ItemName", GetType(String))
                dtMultiSelect.Columns.Add("numUnitHour", GetType(String))
                dtMultiSelect.Columns.Add("monPrice", GetType(String))
                dtMultiSelect.Columns.Add("dtReleaseDate", GetType(String))
                dtMultiSelect.Columns.Add("numShipFrom", GetType(Long))
                dtMultiSelect.Columns.Add("vcWarehouse", GetType(String))
                dtMultiSelect.Columns.Add("numShipTo", GetType(Long))
                dtMultiSelect.Columns.Add("vcShipTo", GetType(String))
                dtMultiSelect.Columns.Add("Attr", GetType(String))
                dtMultiSelect.Columns.Add("numOnHand", GetType(String))
                dtMultiSelect.Columns.Add("numAllocation", GetType(String))
                dtMultiSelect.Columns.Add("numSaleUnit", GetType(String))
                dtMultiSelect.Columns.Add("UOMConversionFactor", GetType(Double))
                dtMultiSelect.Columns.Add("charItemType", GetType(String))
            End If

            Dim objContact As New CContacts

            objContact.AddressType = CContacts.enmAddressType.ShipTo
            objContact.DomainID = Session("DomainID")
            objContact.RecordID = lngCustId
            objContact.AddresOf = CContacts.enmAddressOf.Organization
            objContact.byteMode = 2
            dtMultiSelectShipToAddr = objContact.GetAddressDetail()

            Dim objItem As New CItems
            Dim dtfromDB As DataTable = objItem.GetMultiSelectItems(Session("DomainID"), Session("MultiSelectItemCodes"), 1)

            If Not dtfromDB Is Nothing AndAlso dtfromDB.Rows.Count > 0 Then
                For i As Integer = 0 To dtfromDB.Rows.Count - 1
                    Dim drnew As DataRow = dtMultiSelect.NewRow()
                    drnew("ID") = Guid.NewGuid().ToString()
                    drnew("numItemCode") = dtfromDB.Rows(i)("numItemCode")
                    drnew("ItemName") = dtfromDB.Rows(i)("ItemName")
                    drnew("Attr") = dtfromDB.Rows(i)("Attr")
                    drnew("numOnHand") = dtfromDB.Rows(i)("numOnHand")
                    drnew("numAllocation") = dtfromDB.Rows(i)("numAllocation")
                    drnew("numSaleUnit") = dtfromDB.Rows(i)("numSaleUnit")
                    drnew("UOMConversionFactor") = CCommon.ToDouble(dtfromDB.Rows(i)("fltUOMConversionFactor"))
                    drnew("charItemType") = CCommon.ToString(dtfromDB.Rows(i)("charItemType"))
                    dtMultiSelect.Rows.Add(drnew)
                Next
            End If

            ViewState("dtMultiSelect") = dtMultiSelect
            Session("MultiSelectItemCodes") = ""
            Session.Remove("MultiSelectItemCodes")
            gvMultiSelectItems.DataSource = dtMultiSelect
            gvMultiSelectItems.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function CalculatePriceForMultiSelectItem(ByVal ItemCode As Long, ByVal Units As Double, ByVal UOMId As Long, ByVal KitChildItems As String) As DataSet
        Dim objOpportunity As New COpportunities
        objOpportunity.DomainID = Session("DomainID")
        objOpportunity.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
        objOpportunity.OppType = 1
        objOpportunity.ItemCode = ItemCode
        objOpportunity.UnitHour = Units
        objOpportunity.WarehouseItmsID = 0
        objOpportunity.CurrencyID = CCommon.ToLong(radcmbCurrency.SelectedValue)
        Dim ds As DataSet = objOpportunity.GetOrderItemDetails(UOMId, KitChildItems, CCommon.ToLong(hdnCountry.Value))
        Return ds
    End Function
#End Region

    Private Sub btnSaveCloseKit_Click(sender As Object, e As EventArgs) Handles btnSaveCloseKit.Click
        Try
            If hdnFromItemPromoPopup.Value = "1" Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "EnableScroll", "Scroll(true); $('[id$=divKitChild]').hide(); $('#" & hdnItemPromokitChildsHiddeFieldID.Value & "').val('" & hdnKitChildItems.Value & "'); $('#" & hdnItemPromoAddbuttonID.Value & "').click();", True)
                divItemPromotionStatus.Style.Remove("display")
            Else
                lblMatrixAndKitAttributes.Text = hdnSelectedText.Value
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "EnableScroll", "Scroll(true); $('[id$=divKitChild]').hide();", True)
                LoadSelectedItemDetail(hdnCurrentSelectedItem.Value, emptyFieldsInitially:=True)
                LoadItemPromotions(CCommon.ToLong(hdnCurrentSelectedItem.Value))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub btnCloseKit_Click(sender As Object, e As EventArgs)
        Try
            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "EnableScroll", "$('#divItem').show();$('#txtItem').select2('data', null);$('[id$=divKitChild]').hide(); $('[id$=divItemPromotionStatus]').show();Scroll(true);", True)
            hdnFromItemPromoPopup.Value = "0"
            hdnItemPromokitChildsHiddeFieldID.Value = ""
            hdnItemPromoAddbuttonID.Value = ""
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Public Class SelectedItems
        Public Property numItemCode As Long
        Public Property numWarehouseItemID As String
        Public Property bitHasKitAsChild As Boolean
        Public Property kitChildItems As String
        Public Property numSelectedUOM As Long
        Public Property vcSelectedUOM As String
        Public Property numQuantity As Long
        Public Property vcAttributes As String
        Public Property vcAttributeIDs As String
    End Class

    Private Sub btnSaveCloseAttributes_Click(sender As Object, e As EventArgs) Handles btnSaveCloseAttributes.Click
        Try
            Dim vcAttributes As String
            Dim vcAttributeIDs As String

            For Each item As RepeaterItem In rptAttributes.Items
                Dim lblAttributeName As Label = DirectCast(item.FindControl("lblAttributeName"), Label)
                Dim ddlAttributes As DropDownList = DirectCast(item.FindControl("ddlAttributes"), DropDownList)
                Dim hdnFldID As HiddenField = DirectCast(item.FindControl("hdnFldID"), HiddenField)

                If CCommon.ToLong(ddlAttributes.SelectedValue) = 0 Then
                    lblAttributeError.Text = "No value is selected for attribute <b>" & lblAttributeName.Text & "</b>."
                    ddlAttributes.Focus()
                    Exit Sub
                Else
                    vcAttributes = IIf(String.IsNullOrEmpty(vcAttributes), "", vcAttributes & ",") & lblAttributeName.Text & ":" & ddlAttributes.SelectedItem.Text
                    vcAttributeIDs = IIf(String.IsNullOrEmpty(vcAttributeIDs), "", vcAttributeIDs & ",") & hdnFldID.Value & ":" & ddlAttributes.SelectedItem.Value
                End If
            Next

            'Added by Priya, 24 April 2018
            lblMatrixAndKitAttributes.Text = vcAttributes

            Dim warehouseItemID As Long = 0
            If CCommon.ToBool(hdnIsMatrix.Value) Then
                objItems = New CItems
                objItems.DomainID = CCommon.ToLong(Session("DomainID"))
                objItems.ItemCode = CCommon.ToLong(hdnCurrentSelectedItem.Value)
                objItems.AttributeIDs = vcAttributeIDs
                Dim dt As DataTable = objItems.GetMatrixItemFromAttributes()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    If hdnCurrentSelectedItem.Value <> CCommon.ToLong(dt.Rows(0)("numItemCode")) Then
                        hdnCurrentSelectedItem.Value = CCommon.ToLong(dt.Rows(0)("numItemCode"))
                        warehouseItemID = CCommon.ToLong(dt.Rows(0)("numWarehouseItemID"))
                        LoadSelectedItemDetail(CCommon.ToLong(dt.Rows(0)("numItemCode")), LoadAttributes:=False, emptyFieldsInitially:=True)
                        LoadItemPromotions(CCommon.ToLong(hdnCurrentSelectedItem.Value))
                    End If
                Else
                    lblAttributeError.Text = "No item available with selected attributes."
                    Exit Sub
                End If
            End If

            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "SetItemAttributes", "SetItemAttributes(" + hdnCurrentSelectedItem.Value + "," + warehouseItemID.ToString() + ",'" + vcAttributes + "','" + vcAttributeIDs + "');", True)
            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "EnableScroll", "Scroll(true);", True)
            divItemAttributes.Style.Add("display", "none")
            txtUnits.Focus()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub rptAttributes_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptAttributes.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim dr As DataRowView = DirectCast(e.Item.DataItem, System.Data.DataRowView)
                Dim ddlAttributes As DropDownList = DirectCast(e.Item.FindControl("ddlAttributes"), DropDownList)
                Dim hdnFldID As HiddenField = DirectCast(e.Item.FindControl("hdnFldID"), HiddenField)
                Dim hdnListID As HiddenField = DirectCast(e.Item.FindControl("hdnListID"), HiddenField)

                If e.Item.ItemIndex = 0 Then
                    ddlAttributes.Focus()
                End If

                objCommon.sb_FillAttibuesForEcommFromDB(ddlAttributes, hdnListID.Value, hdnCurrentSelectedItem.Value, "", 0)

                If Not dr("Fld_Value") Is Nothing AndAlso Not ddlAttributes.Items.FindByValue(dr("Fld_Value")) Is Nothing Then
                    ddlAttributes.Items.FindByValue(dr("Fld_Value")).Selected = True
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub lkbLastPrice_Click(sender As Object, e As EventArgs) Handles lkbLastPrice.Click
        Try
            txtprice.Text = CCommon.ToDecimal(hdnLastPrice.Value)
            hdnUnitCost.Value = CCommon.ToDecimal(txtprice.Text)
            txtItemDiscount.Text = "0"
            radPer.Checked = True
            hdnPricingBasedOn.Value = "3"

            lkbLastPrice.Text = "<i class=""fa fa-check""></i>&nbsp;Use Last Price (" & hdnLastPrice.Value & "," & hdnLastOrderDate.Value & ")"
            lkbPriceLevel.Text = lkbPriceLevel.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "")
            lkbPriceRule.Text = lkbPriceRule.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "")
            lkbListPrice.Text = lkbListPrice.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideItemSearch", "$('.itemsArea').css('visibility', 'visible');$('.itemsArea').css('display', 'flow-root');", True)
    End Sub

    Private Sub lkbListPrice_Click(sender As Object, e As EventArgs) Handles lkbListPrice.Click
        Try
            txtprice.Text = CCommon.ToDecimal(hdnListPrice.Value)
            hdnUnitCost.Value = CCommon.ToDecimal(txtprice.Text)
            txtItemDiscount.Text = "0"
            radPer.Checked = True
            hdnPricingBasedOn.Value = ""

            lkbListPrice.Text = "<i class=""fa fa-check""></i>&nbsp;Use List Price (" & hdnListPrice.Value & ")"
            lkbPriceLevel.Text = lkbPriceLevel.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "")
            lkbPriceRule.Text = lkbPriceRule.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "")
            lkbLastPrice.Text = lkbLastPrice.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideItemSearch", "$('.itemsArea').css('visibility', 'visible');$('.itemsArea').css('display', 'flow-root');", True)
    End Sub

    Private Sub lkbPriceLevel_Click(sender As Object, e As EventArgs) Handles lkbPriceLevel.Click
        Try
            divPriceLevel.Visible = True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideItemSearch", "$('.itemsArea').css('visibility', 'visible');$('.itemsArea').css('display', 'flow-root');", True)
    End Sub

    Private Sub lkbPriceRule_Click(sender As Object, e As EventArgs) Handles lkbPriceRule.Click
        Try
            Dim objOpportuniy As New COpportunities
            objOpportuniy.DomainID = CCommon.ToLong(Session("DomainID"))
            objOpportuniy.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
            objOpportuniy.OppType = 1 'Price Rule
            objOpportuniy.ItemCode = hdnCurrentSelectedItem.Value
            objOpportuniy.UnitHour = txtUnits.Text
            Dim dt As DataTable = objOpportuniy.GetItemPriceBasedOnPricingOption(2, hdnListPrice.Value, hdnVendorCost.Value, txtUOMConversionFactor.Text)

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                hdnPricingBasedOn.Value = "2"
                txtprice.Text = CCommon.ToDecimal(dt.Rows(0)("monPrice"))
                hdnUnitCost.Value = CCommon.ToDecimal(txtprice.Text)
                ddlMarkupDiscountOption.SelectedValue = "0"
                If CCommon.ToBool(dt.Rows(0)("tintDisountType")) Then
                    radPer.Checked = True
                    radAmt.Checked = False
                Else
                    radAmt.Checked = True
                    radPer.Checked = False
                End If

                txtItemDiscount.Text = CCommon.ToDecimal(dt.Rows(0)("decDiscount"))

                lkbPriceRule.Text = "<i class=""fa fa-check""></i>&nbsp;Use Price Rule"
                lkbLastPrice.Text = lkbLastPrice.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "")
                lkbPriceLevel.Text = lkbPriceLevel.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "")
                lkbListPrice.Text = lkbListPrice.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "")
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideItemSearch", "$('.itemsArea').css('visibility', 'visible');$('.itemsArea').css('display', 'flow-root');", True)
    End Sub

    Private Sub btnDeleteItem_Click(sender As Object, e As EventArgs) Handles btnDeleteItem.Click
        Try
            Dim dtUsedPromotion As DataTable = DirectCast(ViewState("UsedPromotion"), DataTable)
            Dim row As DataRow = dsTemp.Tables(0).Select("numoppitemtCode=" & hdnDeleteItemID.Value)(0)
            Dim dtItem As DataTable
            Dim dataKeyNumItemCode = Nothing
            dtItem = dsTemp.Tables(0)
            If dtItem IsNot Nothing AndAlso dtItem.Rows.Count > 0 Then
                dataKeyNumItemCode = (From r In dtItem.Rows
                                      Where CCommon.ToLong(r("numoppitemtCode")) = hdnDeleteItemID.Value
                                      Select r("numItemCode")).FirstOrDefault()
            End If
            If CCommon.ToBool(row("bitPromotionTriggered")) Then
                If dtUsedPromotion IsNot Nothing Then
                    Dim rowPromotion As DataRow = dtUsedPromotion.Select("numProId=" & row("numPromotionID"))(0)
                    dtUsedPromotion.Rows.Remove(rowPromotion)
                End If
                For Each dr As DataRow In dsTemp.Tables(0).Select("numPromotionID=" & row("numPromotionID").ToString())
                    dsTemp.Tables(0).Rows.Remove(dr)
                Next
            End If

            'Code to check Child Items and delete along with Parent Item - START - Neelam
            If dtRelatedItems IsNot Nothing AndAlso dtRelatedItems.Rows.Count > 0 Then
                Dim childRows = Nothing
                If dataKeyNumItemCode IsNot Nothing Then
                    childRows = (From dRow In dtRelatedItems.Rows
                                 Where CCommon.ToLong(dRow("numParentItemCode")) = dataKeyNumItemCode
                                 Select New With {Key .col1 = dRow("numItemCode"), .col2 = dRow("numWareHouseItemID")}).Distinct()
                End If
                If childRows IsNot Nothing Then
                    For Each r In childRows
                        Dim _numItemCode As String = r.col1
                        Dim _numWareHouseItemID As String = r.col2

                        Dim obj = Nothing
                        If dtItem IsNot Nothing AndAlso dtItem.Rows.Count > 0 Then
                            obj = dtItem.Select("numItemCode='" + _numItemCode + "'")
                            'obj = dtItem.Select("numItemCode=" & _numItemCode & " AND numWarehouseItmsID=" & _numWareHouseItemID.ToString())

                            If obj IsNot Nothing AndAlso obj.Length > 0 Then
                                For Each q In obj
                                    Dim drr As DataRow = dtItem.Rows.Find(q.ItemArray(0))
                                    If drr IsNot Nothing Then
                                        drr.Delete()
                                    End If
                                Next
                            End If
                        End If
                    Next
                End If
            End If
            'Code to check Child Items and delete along with Parent Item - END
            Dim objParent = Nothing
            If dtItem IsNot Nothing AndAlso dtItem.Rows.Count > 0 AndAlso dataKeyNumItemCode IsNot Nothing Then
                objParent = dtItem.Select("numItemCode='" + dataKeyNumItemCode + "'")
            End If
            If objParent IsNot Nothing AndAlso objParent.Length > 0 Then
                For Each t In objParent
                    t.Delete()
                Next
            End If
            RecalculateContainerQty(dtItem)
            dtItem.AcceptChanges()
            If dtUsedPromotion IsNot Nothing Then
                dtUsedPromotion.AcceptChanges()
                ViewState("UsedPromotion") = dtUsedPromotion
            End If
            dsTemp.AcceptChanges()
            hdnDeleteItemID.Value = ""
            UpdateDetails()
            UpdatePanelItems.Update()

            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ClearSelectedItems", "$('#divItem').show(); $('#txtItem').select2('data', null);", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub hplRelatedItems_Click(sender As Object, e As EventArgs) Handles hplRelatedItems.Click
        Try
            RelatedItems(False)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub hplLastPrices_Click(sender As Object, e As EventArgs) Handles hplLastPrices.Click
        Try
            BindPriceHistory(False)
            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideItemSearch", "$('.itemsArea').css('visibility', 'visible');$('.itemsArea').css('display', 'flow-root');", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub BindPriceHistory(ByVal forSelectedCustomer As Boolean)
        Try
            Dim objOpp As New COpportunities
            objOpp.DomainID = CCommon.ToLong(Session("DomainID"))
            objOpp.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            objOpp.ItemCode = hdnCurrentSelectedItem.Value
            objOpp.OppType = 1
            If forSelectedCustomer Then
                objOpp.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
            End If
            gvPriceHistory.DataSource = objOpp.GetItemPriceHistory()
            gvPriceHistory.DataBind()

            divItemPriceHistory.Style.Add("display", "")
            UpdatePanelActionWindow.Update()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub gvPriceHistory_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvPriceHistory.RowCommand
        Try
            If e.CommandName = "Add" Then
                Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, LinkButton).NamingContainer, GridViewRow)

                txtUnits.Text = CCommon.ToString(DirectCast(row.FindControl("hdnUnits"), HiddenField).Value)
                txtprice.Text = CCommon.ToString(DirectCast(row.FindControl("hdnPrice"), HiddenField).Value)
                txtItemDiscount.Text = CCommon.ToString(DirectCast(row.FindControl("hdnDiscount"), HiddenField).Value)
                If CCommon.ToString(DirectCast(row.FindControl("hdnDiscount"), HiddenField).Value) = "1" Then
                    radAmt.Checked = True
                Else
                    radPer.Checked = True
                End If
                txtUOMConversionFactor.Text = CCommon.ToString(DirectCast(row.FindControl("hdnUOMConversionFactor"), HiddenField).Value)
                If Not radcmbUOM.Items.FindItemByValue(DirectCast(row.FindControl("hdnUOMId"), HiddenField).Value) Is Nothing Then
                    radcmbUOM.Items.FindItemByValue(DirectCast(row.FindControl("hdnUOMId"), HiddenField).Value).Selected = True
                End If

                hdnPricingBasedOn.Value = "3"

                lkbLastPrice.Text = "<i class=""fa fa-check""></i>&nbsp;Use Last Price (" & txtprice.Text & "," & CCommon.ToString(DirectCast(row.FindControl("hdnDate"), HiddenField).Value) & ")"
                lkbPriceLevel.Text = lkbPriceLevel.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "")
                lkbPriceRule.Text = lkbPriceRule.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "")
                lkbListPrice.Text = lkbListPrice.Text.Replace("<i class=""fa fa-check""></i>&nbsp;", "")

                UpdatePanelItems.Update()

                divItemPriceHistory.Style.Add("display", "none")
                UpdatePanelActionWindow.Update()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnClosePriceHistory_Click(sender As Object, e As EventArgs) Handles btnClosePriceHistory.Click
        Try
            gvPriceHistory.DataSource = Nothing
            gvPriceHistory.DataBind()
            divItemPriceHistory.Style.Add("display", "none")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub gvPriceHistory_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvPriceHistory.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim lnkAdd As LinkButton = TryCast(e.Row.FindControl("lnkAdd"), LinkButton)
                ScriptManager.GetCurrent(Me).RegisterAsyncPostBackControl(lnkAdd)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub gvRelatedItems_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvRelatedItems.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                'Dim lnkAdd As Button = TryCast(e.Row.FindControl("lnkAdd"), Button)
                'ScriptManager.GetCurrent(Me).RegisterAsyncPostBackControl(lnkAdd)

                Dim lblOutOfStock As Label = TryCast(e.Row.FindControl("lblOutOfStock"), Label)
                Dim chkAdd As CheckBox = TryCast(e.Row.FindControl("chkAdd"), CheckBox)

                Dim drv As DataRowView = TryCast(e.Row.DataItem, DataRowView)
                If drv IsNot Nothing Then
                    Dim txtQty As TextBox = DirectCast(e.Row.FindControl("txtQty"), TextBox)
                    Dim qty As Integer = CCommon.ToInteger(txtQty.Text)
                    Dim price As Decimal = CCommon.ToDecimal(drv.Row("monListPrice"))
                    Dim total As Decimal = CCommon.ToDecimal(qty * price)

                    Dim lblTotal As Label = DirectCast(e.Row.FindControl("lblTotal"), Label)
                    lblTotal.Text = CCommon.ToString(Session("Currency")) + " " + CCommon.ToString(Math.Round(total, 2))

                    Dim _numOnHand = CCommon.ToInteger(drv.Row("numOnHand"))
                    If (_numOnHand = 0) Then
                        chkAdd.Visible = False
                        lblOutOfStock.Visible = True
                    End If
                    'Dim ItemImagePath = "/Images/DefaultProduct.png"
                    'If Not String.IsNullOrEmpty(drv.Row("vcPathForTImage").ToString()) Then
                    '    ItemImagePath = GetImagePath(ReplaceAllSpaces(drv.Row("vcPathForTImage").ToString()))
                    'End If
                    'drv.Row("vcPathForTImage") = ItemImagePath

                    Dim img As WebControls.Image = CType(e.Row.FindControl("imgPath"), WebControls.Image)
                    Dim imgPath As String = img.ImageUrl
                    If Not String.IsNullOrEmpty(imgPath) Then
                        imgPath = GetImagePath(CCommon.ToString(imgPath))
                        img.ImageUrl = imgPath
                    Else
                        img.ImageUrl = "../Images/DefaultProduct.png"
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Public Function GetImagePath(ByVal ImageFileName As String) As String
        Try
            Dim path As String = Sites.ToString(Session("ItemImagePath")) & "/" & ImageFileName
            If Request.IsSecureConnection Then
                Return path.Replace("http", "https")
            Else
                Return path
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Shared Function ReplaceAllSpaces(ByVal str As String) As String
        Return Regex.Replace(str, "\s+", "%20")
    End Function

    Protected Sub btnAuthorize_Click(sender As Object, e As EventArgs)
        Dim decAmount As Decimal = CCommon.ToDecimal(lblTotal.Text)
        Dim objPaymentGateway As New PaymentGateway
        objPaymentGateway.DomainID = Session("DomainID")
        Dim responseCode As String = ""
        Try
            AuthorizeStatus = objPaymentGateway.GatewayTransaction(IIf(IsNumeric(decAmount), decAmount, 0), txtCHName.Text, IIf(txtCCNo.Text.Trim().Contains("*"), Session("CCNO"), txtCCNo.Text.Trim()), txtCVV2.Text, radcmbMonth.SelectedValue, radcmbYear.SelectedValue, CCommon.ToLong(radcmbContact.SelectedValue), ResponseMessage, ReturnTransactionID, responseCode, CreditCardAuthOnly:=True, CreditCardTrack1:=hdnSwipeTrack1.Value)
        Catch ex As Exception
            AuthorizeStatus = False
        End Try

        If AuthorizeStatus = True Then
            ltrlAutorizeCard.Text = "<span style='color:green'>Credit Card is Verified</span>"
        Else
            ltrlAutorizeCard.Text = "<span style='color:red'>Credit Card is not Authorized</span>"
        End If
    End Sub

    Private Sub btnBulkSave_Click(sender As Object, e As EventArgs) Handles btnBulkSave.Click
        Try
            Dim arrOutPut() As String
            Dim dtBulkContacts As New DataTable
            dtBulkContacts.Columns.Add("numDivisionId")
            dtBulkContacts.Columns.Add("numContactId")

            Dim drBulk As DataRow

            Dim arrSelectedOrganizations As String() = GetQueryStringVal("CntIds").Split(",")

            For Each item As String In arrSelectedOrganizations
                drBulk = dtBulkContacts.NewRow()
                drBulk("numDivisionId") = item.Split("-")(0)
                drBulk("numContactId") = item.Split("-")(1)
                dtBulkContacts.Rows.Add(drBulk)
            Next

            Dim isMasterOrderCreated As Boolean = False

            For Each drOrg In dtBulkContacts.Rows
                If Not isMasterOrderCreated Then
                    Dim objOpportunity As New MOpportunity
                    objOpportunity.OpportunityId = 0
                    objOpportunity.ContactID = CCommon.ToLong(drOrg("numContactId"))
                    objOpportunity.DivisionID = CCommon.ToLong(drOrg("numDivisionId"))

                    objOpportunity.OppType = oppType
                    objOpportunity.DealStatus = 1
                    objOpportunity.ReleaseDate = GetReleaseDate()
                    objOpportunity.dtExpectedDate = radOrderExpectedDate.SelectedDate
                    objOpportunity.UserCntID = Session("UserContactID")
                    objOpportunity.EstimatedCloseDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    objOpportunity.PublicFlag = 0
                    objOpportunity.DomainID = Session("DomainId")
                    objOpportunity.OpportunityName = ""
                    objOpportunity.BillToAddressID = CCommon.ToLong(hdnBillAddressID.Value)
                    objOpportunity.ShipToAddressID = CCommon.ToLong(hdnShipAddressID.Value)

                    objOpportunity.CouponCode = ""
                    objOpportunity.Discount = 0
                    objOpportunity.boolDiscType = False

                    If Session("MultiCurrency") = True Then
                        objOpportunity.CurrencyID = radcmbCurrency.SelectedValue
                    Else
                        objOpportunity.CurrencyID = Session("BaseCurrencyID")
                    End If

                    If radcmbAssignedTo.SelectedIndex >= 0 Then objOpportunity.AssignedTo = radcmbAssignedTo.SelectedValue


                    dsTemp.Tables(2).Rows.Clear()
                    dsTemp.AcceptChanges()
                    Dim TotalAmount As Decimal = 0
                    If Not ViewState("SOItems") Is Nothing Then
                        dsTemp = ViewState("SOItems")

                        If dsTemp.Tables(0).Rows.Count > 0 Then
                            TotalAmount = CType(dsTemp.Tables(0).Compute("Sum(monTotAmount)", ""), Decimal)
                            objOpportunity.strItems = "<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & dsTemp.GetXml
                        End If
                    End If

                    Dim objAdmin As New CAdmin
                    Dim dtBillingTerms As DataTable
                    objAdmin.DivisionID = CCommon.ToLong(drOrg("numDivisionId"))
                    dtBillingTerms = objAdmin.GetBillingTerms()

                    objOpportunity.AssignedTo = CCommon.ToLong(dtBillingTerms.Rows(0).Item("numAssignedTo"))
                    objOpportunity.boolBillingTerms = IIf(dtBillingTerms.Rows(0).Item("tintBillingTerms") = 1, True, False)
                    objOpportunity.BillingDays = dtBillingTerms.Rows(0).Item("numBillingDays")
                    objOpportunity.boolInterestType = IIf(dtBillingTerms.Rows(0).Item("tintInterestType") = 1, True, False)
                    objOpportunity.Interest = dtBillingTerms.Rows(0).Item("fltInterest")
                    objOpportunity.UseShippersAccountNo = CCommon.ToBool(chkUseCustomerShippingAccount.Checked)
                    objOpportunity.UseMarkupShippingRate = CCommon.ToBool(chkMarkupShippingCharges.Checked)
                    objOpportunity.dcMarkupShippingRate = CCommon.ToDecimal(txtMarkupShippingRate.Text)
                    objOpportunity.ShippingService = CCommon.ToLong(dtBillingTerms.Rows(0).Item("numDefaultShippingServiceID"))
                    objOpportunity.intUsedShippingCompany = CCommon.ToLong(dtBillingTerms.Rows(0).Item("intShippingCompany"))

                    Dim objBOpp As New MOpportunity
                    objBOpp.DivisionID = CCommon.ToLong(drOrg("numDivisionId"))
                    objBOpp.DomainID = objOpportunity.DomainID
                    objOpportunity.CampaignID = objBOpp.GetCampaignForOrganization()

                    'get the value of dyamic orpportunity fields
                    Dim objPageControls As New PageControls

                    If Not dtOppTableInfo Is Nothing AndAlso dtOppTableInfo.Rows.Count > 0 Then
                        For Each dr As DataRow In dtOppTableInfo.Rows
                            If (dr("fld_type") <> "Popup" And dr("fld_type") <> "Label" And dr("fld_type") <> "Website") Then
                                If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False And dr("bitCanBeUpdated") = True Then
                                    objPageControls.SetValueForStaticFields(dr, objOpportunity, plhOrderDetail)
                                End If
                            End If
                        Next
                    End If
                    objOpportunity.ShipFromLocation = CCommon.ToLong(radcmbLocation.SelectedValue)
                    objOpportunity.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                    objOpportunity.WillCallWarehouseID = CCommon.ToLong(radcmbLocation.SelectedValue)
                    arrOutPut = objOpportunity.Save
                    lngOppId = arrOutPut(0)

                    If (lngOppId > 0) Then
                        ''Added By Sachin Sadhu||Date:29thApril12014
                        ''Purpose :To Added Opportunity data in work Flow queue based on created Rules
                        ''          Using Change tracking
                        Dim objWfA As New Workflow()
                        objWfA.DomainID = Session("DomainID")
                        objWfA.UserCntID = Session("UserContactID")
                        objWfA.RecordID = lngOppId
                        objWfA.SaveWFOrderQueue()
                        'end of code

                        Dim objCustomFields As New CustomFields
                        objCustomFields.locId = 2
                        objCustomFields.RelId = 0
                        objCustomFields.DomainID = DomainID
                        objCustomFields.RecordId = lngOppId
                        objCustomFields.UserCntID = CLng(Session("UserContactId"))
                        objCustomFields.FormId = 90

                        Dim ds As DataSet = objCustomFields.GetCustFlds
                        If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                            objPageControls.CustomFieldsTable = ds.Tables(0)
                            objPageControls.SaveCusField(lngOppId, 0, Session("DomainID"), objPageControls.Location.SalesOppotunity, plhOrderDetail)
                        End If
                    End If

                    isMasterOrderCreated = True
                Else
                    Dim objOpportunity As New MOpportunity()
                    objOpportunity.SaveBulkOrderSalesTemp(CCommon.ToLong(drOrg("numContactId")), CCommon.ToLong(drOrg("numDivisionId")), lngOppId, False)
                End If
            Next

            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Mass Sales Order", "bulksalesmessage();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub FillClass()
        Try
            If CCommon.ToLong(Session("DefaultClassType")) > 0 Then
                Dim dtClass As DataTable
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.Mode = 1
                dtClass = objAdmin.GetClass()

                If dtClass.Rows.Count > 0 Then
                    ddlClass.DataTextField = "ClassName"
                    ddlClass.DataValueField = "numChildClassID"
                    ddlClass.DataSource = dtClass
                    ddlClass.DataBind()

                    Dim intDefaultClassType As Int16 = CCommon.ToInteger(Session("DefaultClassType"))
                    If intDefaultClassType = 1 AndAlso ddlClass.Items.FindByValue(Session("DefaultClass")) IsNot Nothing Then 'intDefaultClassType = 1 => User Level Account Class
                        ddlClass.ClearSelection()
                        ddlClass.Items.FindByValue(Session("DefaultClass")).Selected = True
                    End If
                End If

                divClass.Visible = True
            End If

            ddlClass.Items.Insert(0, New ListItem("--Select One --", "0"))
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub radShipVia_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Try
            radCmbShippingMethod.Items.Clear()
            radCmbShippingMethod.Items.Add(New RadComboBoxItem("-- Select One --", "0"))
            OppBizDocs.LoadServiceTypes(radShipVia.SelectedValue, radCmbShippingMethod)

            If radShipVia.SelectedValue = "92" Then
                Dim warehosueID As Long = 0
                If CCommon.ToLong(radcmbLocation.SelectedValue) > 0 Then
                    warehosueID = CCommon.ToLong(radcmbLocation.SelectedValue)
                ElseIf dsTemp.Tables(0).Select("numWarehouseID > 0").Length > 0 Then
                    warehosueID = CCommon.ToLong(dsTemp.Tables(0).Select("numWarehouseID > 0")(0)("numWarehouseID"))
                End If

                Dim objItem As New CItems
                objItem.DomainID = Session("DomainID")
                objItem.WarehouseID = warehosueID
                Dim dtWarehouse As DataTable = objItem.GetWillCallWarehouseDetail()

                If Not dtWarehouse Is Nothing AndAlso dtWarehouse.Rows.Count > 0 Then
                    hdnWillCallWarehouseID.Value = CCommon.ToLong(dtWarehouse.Rows(0)("numWareHouseID"))
                    Dim address As String = CCommon.ToString(dtWarehouse.Rows(0)("vcFullAddress"))

                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "WillCallAddress", "$('[id$=lblShipTo1]').text('" & address & "');$('[id$=lblShipTo2]').text('');", True)
                End If
            Else
                hdnWillCallWarehouseID.Value = "0"
            End If

            hdnShipVia.Value = CCommon.ToLong(radShipVia.SelectedValue)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub chkPriceHistoryByCustomer_CheckedChanged(sender As Object, e As EventArgs) Handles chkPriceHistoryByCustomer.CheckedChanged
        Try
            BindPriceHistory(chkPriceHistoryByCustomer.Checked)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnCreditCard_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreditCard.Click
        Try
            'IsFromCreateBizDoc = True
            If (CreateOrder("btnCreditCard") = True) Then
                If CCommon.ToBool(hdnIsUnitPriceApprovalRequired.Value) = False Or hdnApprovalActionTaken.Value = "Approved" Then
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "OpenAmtPaid", "OpenAmtPaid(" + Convert.ToString(OppBizDocID) + "," + Convert.ToString(lngOppId) + "," + radCmbCompany.SelectedValue + ",1)", True)
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "OpenAmtPaid", "OpenAmtPaid(" + Convert.ToString(OppBizDocID) + "," + Convert.ToString(lngOppId) + "," + radCmbCompany.SelectedValue + ",1,1)", True)
                End If
            End If

        Catch ex As Exception
            If ex.Message.Contains("ITEM_PROMOTION_EXPIRED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Item promotion(s) used are expired.' );", True)
            ElseIf ex.Message.Contains("MULTIPLE_COUPON_BASED_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Multiple coupon based Item promotion(s) used.' );", True)
            ElseIf ex.Message.Contains("INVALID_COUPON_CODE_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Invalid item promotion coupon Code.' );", True)
            ElseIf ex.Message.Contains("ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Item promotion Coupon usage limit exceeds, Promotion can not be applied. Please clear code to Proceed.' );", True)
            ElseIf ex.Message.Contains("COUPON_CODE_REQUIRED_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Coupon code is required to use item promotion.' );", True)
            ElseIf ex.Message.Contains("ORDER_PROMOTION_EXPIRED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Order promotion used is expired.' );", True)
            ElseIf ex.Message.Contains("INVALID_COUPON_CODE_ORDER_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Invalid order promotion coupon Code.' );", True)
            ElseIf ex.Message.Contains("ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Order promotion Coupon usage limit exceeds, Promotion can not be applied. Please clear code to Proceed.' );", True)
            ElseIf ex.Message.Contains("COUPON_CODE_REQUIRED_ORDER_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Coupon code is required to use order promotion.' );", True)
            ElseIf ex.Message.Contains("SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Warehouse selected for kit/assembly item(s) is not available for all child items.' );", True)
            ElseIf ex.Message.Contains("FY_CLOSED") Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub

    Private Sub btnCash_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCash.Click
        Try
            'IsFromCreateBizDoc = True
            If (CreateOrder("btnCash") = True) Then
                If CCommon.ToBool(hdnIsUnitPriceApprovalRequired.Value) = False Or hdnApprovalActionTaken.Value = "Approved" Then
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "OpenAmtPaid", "OpenAmtPaid(" + Convert.ToString(OppBizDocID) + "," + Convert.ToString(lngOppId) + "," + radCmbCompany.SelectedValue + ",4)", True)
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "OpenAmtPaid", "OpenAmtPaid(" + Convert.ToString(OppBizDocID) + "," + Convert.ToString(lngOppId) + "," + radCmbCompany.SelectedValue + ",4,1)", True)
                End If
            End If

        Catch ex As Exception
            If ex.Message.Contains("ITEM_PROMOTION_EXPIRED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Item promotion(s) used are expired.' );", True)
            ElseIf ex.Message.Contains("MULTIPLE_COUPON_BASED_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Multiple coupon based Item promotion(s) used.' );", True)
            ElseIf ex.Message.Contains("INVALID_COUPON_CODE_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Invalid item promotion coupon Code.' );", True)
            ElseIf ex.Message.Contains("ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Item promotion Coupon usage limit exceeds, Promotion can not be applied. Please clear code to Proceed.' );", True)
            ElseIf ex.Message.Contains("COUPON_CODE_REQUIRED_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Coupon code is required to use item promotion.' );", True)
            ElseIf ex.Message.Contains("ORDER_PROMOTION_EXPIRED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Order promotion used is expired.' );", True)
            ElseIf ex.Message.Contains("INVALID_COUPON_CODE_ORDER_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Invalid order promotion coupon Code.' );", True)
            ElseIf ex.Message.Contains("ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Order promotion Coupon usage limit exceeds, Promotion can not be applied. Please clear code to Proceed.' );", True)
            ElseIf ex.Message.Contains("COUPON_CODE_REQUIRED_ORDER_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Coupon code is required to use order promotion.' );", True)
            ElseIf ex.Message.Contains("SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Warehouse selected for kit/assembly item(s) is not available for all child items.' );", True)
            ElseIf ex.Message.Contains("FY_CLOSED") Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub

    Protected Sub btnAdd_Click(sender As Object, e As EventArgs)
        AddSimmilarItems()
        clearControls()
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "close", "ClosePreCheckoutWindow();", True)
        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideOtherTopSectionAfter1stItemAdded", "HideOtherTopSectionAfter1stItemAdded();FocuonItem();  $('.itemsArea').css('visibility', 'hidden');$('.itemsArea').css('display', 'none');", True)

    End Sub

    Private Sub AddSimmilarItems()
        Try
            Dim MaxRowOrder As Integer
            MaxRowOrder = dsTemp.Tables(0).Rows.Count

            'Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)

            Dim dtItemCodes As DataTable = New DataTable()
            Dim column As DataColumn

            ' Create new DataColumn, set DataType, ColumnName and add to DataTable.    
            column = New DataColumn()
            column.DataType = System.Type.GetType("System.Int32")
            column.ColumnName = "numItemCode"
            dtItemCodes.Columns.Add(column)

            column = New DataColumn
            column.DataType = System.Type.GetType("System.Int32")
            column.ColumnName = "numQty"
            dtItemCodes.Columns.Add(column)

            column = New DataColumn
            column.DataType = System.Type.GetType("System.Int32")
            column.ColumnName = "numWareHouseItemID"
            dtItemCodes.Columns.Add(column)

            Dim _parentFlag = False


            For Each dgGridItem As DataGridItem In dgItems.Items
                Dim _numItemCode As Long = CCommon.ToLong(dgItems.DataKeys(dgGridItem.ItemIndex))
                If hdnCurrentSelectedItem.Value <> "" Then
                    If _numItemCode = hdnCurrentSelectedItem.Value Then
                        _parentFlag = True
                        Exit For
                    End If
                End If
            Next
            'If Session("IsPreCheckout") = "True" Then
            If dgItems.Items.Count = 0 Then
                dtItemCodes.Rows.Add(hdnCurrentSelectedItem.Value)
                dtItemCodes.Rows(0)("numQty") = 1
            Else
                If _parentFlag = False Then
                    dtItemCodes.Rows.Add(hdnCurrentSelectedItem.Value)
                End If
            End If
            'End If

            For Each rw As GridViewRow In gvRelatedItems.Rows
                If CType(rw.FindControl("chkAdd"), CheckBox).Checked Then
                    Dim _itemCode = CCommon.ToLong(rw.Cells(9).Text)
                    Dim _qty = CType(rw.FindControl("txtQty"), TextBox).Text
                    Dim _wareHouseItemID = CType(rw.FindControl("hdnWareHouseItemID"), HiddenField).Value

                    Dim rowItemCodes = dtItemCodes.NewRow()
                    rowItemCodes("numItemCode") = _itemCode
                    rowItemCodes("numQty") = _qty
                    rowItemCodes("numWareHouseItemID") = _wareHouseItemID
                    dtItemCodes.Rows.Add(rowItemCodes)
                End If
            Next

            For Each dr As DataRow In dtItemCodes.Rows
                Dim dtTable As DataTable
                Dim numUOMId As Long = 0, vcUOMName As String
                Dim objItems As New CItems
                'objItems.ItemCode = e.CommandArgument
                objItems.ItemCode = dr(("numItemCode"))
                dtTable = objItems.ItemDetails
                If dtTable.Rows.Count > 0 Then

                    If hdnSelectedItems.Value <> "" Then
                        Dim serializer As New Script.Serialization.JavaScriptSerializer()
                        Dim objSelectedItem As SelectedItems = serializer.Deserialize(Of SelectedItems)(hdnSelectedItems.Value)
                        Dim objItem As New CItems
                        objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                        objItem.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                        objItem.byteMode = CCommon.ToShort(oppType)
                        'objItem.ItemCode = objSelectedItem.numItemCode
                        objItem.ItemCode = dr("numItemCode")
                        objItem.WareHouseItemID = IIf(IsDBNull(dr("numWareHouseItemID")), 0, CCommon.ToLong(dr("numWareHouseItemID")))
                        objItem.bitHasKitAsChild = objSelectedItem.bitHasKitAsChild
                        objItem.vcKitChildItems = hdnKitChildItems.Value
                        objItem.GetItemAndWarehouseDetails()
                        objItem.ItemDesc = txtdesc.Text
                        objItem.CustomerPartNo = txtCustomerPartNo.Text
                        objItem.SelectedUOMID = radcmbUOM.SelectedValue
                        objItem.SelectedUOM = IIf(radcmbUOM.SelectedValue > 0, radcmbUOM.Text, "")
                        objItem.AttributeIDs = objSelectedItem.vcAttributeIDs
                        objItem.Attributes = objSelectedItem.vcAttributes
                        MaxRowOrder = MaxRowOrder + 1
                        objItem.numSortOrder = MaxRowOrder
                        If objItem.ItemType = "P" AndAlso IsDuplicate(objItem.ItemCode, objItem.WareHouseItemID, chkDropShip.Checked) Then
                            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "DuplicateItemWarehouse", "alert(""Inventory item with same warehouse location is already added. Please select another location."")", True)
                            Return
                        Else
                            MaxRowOrder = MaxRowOrder - 1
                        End If
                    End If

                    numUOMId = dtTable.Rows(0).Item("numSaleUnit")
                    vcUOMName = dtTable.Rows(0).Item("vcSaleUnit")

                    'UOM Conversion Factor
                    Dim dtUnit As DataTable
                    objCommon.DomainID = Session("DomainID")
                    objCommon.ItemCode = objItems.ItemCode
                    objCommon.UnitId = numUOMId
                    dtUnit = objCommon.GetItemUOMConversion()
                    Dim decUOMConversionFactor = dtUnit(0)("BaseUOMConversion")

                    'Start - Get Tax Detail
                    Dim strApplicable As String
                    Dim dtItemTax As DataTable

                    If (pageType = PageTypeEnum.Sales) Then
                        dtItemTax = objItems.ItemTax()

                        For Each drItemTax As DataRow In dtItemTax.Rows
                            strApplicable = strApplicable & drItemTax("bitApplicable") & ","
                        Next

                        strApplicable = strApplicable & objItems.Taxable
                    End If

                    'Update discount
                    Dim units As Integer = CCommon.ToInteger(dr("numQty"))
                    'Dim units As Integer = CCommon.ToInteger(DirectCast(rw.FindControl("txtQty"), TextBox).Text)

                    'Dim numWarehouseItemID As Long = CCommon.ToLong(DirectCast(row.FindControl("hdnWarehouseID"), HiddenField).Value)
                    'Dim price As Decimal = CCommon.ToDecimal(DirectCast(row.FindControl("hdnPrice"), HiddenField).Value)
                    Dim numWarehouseItemID As Long = CCommon.ToLong(radWareHouse.SelectedValue)
                    Dim price As Decimal = dtTable.Rows(0)("monListPrice")
                    Dim decDiscount As Decimal = 0
                    Dim IsDiscountInPer As Boolean = True
                    'Dim numPromotionID As Long = CCommon.ToLong(hdnRelatedItemPromotionID.Value)
                    Dim vcPromotionDetail As String = ""
                    Dim salePrice As Decimal = price
                    Dim numPromotionID As Long = 0

                    objItems.DomainID = CCommon.ToLong(Session("DomainID"))
                    objItems.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                    objItems.byteMode = CCommon.ToShort(oppType)
                    objItems.WareHouseItemID = numWarehouseItemID
                    objItems.GetItemAndWarehouseDetails()
                    MaxRowOrder = MaxRowOrder + 1
                    objItems.numSortOrder = MaxRowOrder

                    ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon, dsTemp, True, objItems.ItemType, "", False, objItems.KitParent, objItems.ItemCode, units, price, objItems.ItemDesc, numWarehouseItemID,
                                                                                       objItems.ItemName, "", 0, objItems.ModelID, numUOMId, vcUOMName, decUOMConversionFactor, pageType.ToString(), IsDiscountInPer, decDiscount, strApplicable, txtTax.Text, txtCustomerPartNo.Text, chkTaxItems,
                                                                                        numProjectID:=CCommon.ToLong(GetQueryStringVal("Source")),
                                                                                         numProjectStageID:=CCommon.ToLong(GetQueryStringVal("StageID")),
                                                                                        vcBaseUOMName:=dtTable.Rows(0).Item("vcBaseUnit"),
                                                                                        objItem:=objItems, primaryVendorCost:=0, salePrice:=salePrice, numPromotionID:=numPromotionID,
                                                                                        IsPromotionTriggered:=False,
                                                                                        vcPromotionDetail:=vcPromotionDetail, numSortOrder:=objItems.numSortOrder, itemReleaseDate:=GetReleaseDate())

                    UpdateDataTable()
                    UpdatePanelItems.Update()
                    'RegisterPostBackControl()
                End If
            Next
            AddSimilarRequiredRow()

            UpdateDetails()
            UpdateDataTable()

            UpdatePanelItems.Update()
            'RegisterPostBackControl()


        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub gvLocations_RowDataBound(sender As Object, e As GridViewRowEventArgs)

        If (e.Row.RowType = DataControlRowType.DataRow) Then
            Dim ShipToLocations As RadComboBox = CType(e.Row.FindControl("rdcmbShipToLocation"), RadComboBox)
            ShipToLocations.DataSource = dtLocationsGrid
            ShipToLocations.DataTextField = "FullAddress"
            ShipToLocations.DataValueField = "numAddressID"
            ShipToLocations.DataBind()

            Dim ReleaseDateLocations As RadDatePicker = CType(e.Row.FindControl("rdLocationItemReleaseDate"), RadDatePicker)
            ReleaseDateLocations.SelectedDate = Date.Today

            Dim rowView As DataRowView
            rowView = CType(e.Row.DataItem, DataRowView)

            Dim lblShipFromLocal As Label = CType(e.Row.FindControl("lblShipFromAttr"), Label)
            If (CType(e.Row.FindControl("hdnAttr"), HiddenField).Value IsNot "") Then
                lblShipFromLocal.Text = rowView("vcWareHouse").ToString + " ( " + CType(e.Row.FindControl("hdnAttr"), HiddenField).Value + " )"
            Else
                lblShipFromLocal.Text = rowView("vcWareHouse").ToString
            End If

            If (e.Row.RowIndex = 0) Then
                CType(e.Row.FindControl("chkLocations"), CheckBox).Checked = True
                CType(e.Row.FindControl("txtLocationsQty"), TextBox).Enabled = False
                CType(e.Row.FindControl("rdLocationItemReleaseDate"), RadDatePicker).SelectedDate = GetReleaseDate()
            End If
        End If
    End Sub

    Protected Sub gvLocations_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        Try
            gvLocations.PageIndex = e.NewPageIndex
            Dim dtWareHouses As DataTable = objCommon.GetWarehousesForSelectedItem(hdnCurrentSelectedItem.Value, IIf(radcmbLocation.SelectedValue = "" Or (CCommon.ToBool(hdnIsAutoWarehouseSelection.Value) AndAlso hdnWarehousePriority.Value <> "-1"), 0, CCommon.ToLong(radcmbLocation.SelectedValue)), CCommon.ToDouble(txtUnits.Text), GetOrderSource(), 1, CCommon.ToLong(hdnShipToCountry.Value), CCommon.ToLong(hdnShipToState.Value), hdnKitChildItems.Value)

            gvLocations.DataSource = dtWareHouses
            gvLocations.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub gvMultiSelectItems_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If (e.Row.RowType = DataControlRowType.DataRow) Then

            Dim dataRowView As DataRowView = DirectCast(e.Row.DataItem, DataRowView)
            dtMultiSelect = DirectCast(ViewState("dtMultiSelect"), DataTable)
            Dim dr As DataRow = dtMultiSelect.Select("ID='" & CCommon.ToString(dataRowView("ID")) & "'")(0)

            Dim numItemCode As Long = CCommon.ToLong(CType(e.Row.FindControl("hdnItemCode"), HiddenField).Value)
            Dim strItemName As String = CType(e.Row.FindControl("hdnItemName"), HiddenField).Value
            Dim strAttr As String = CType(e.Row.FindControl("hdnAttr"), HiddenField).Value
            Dim numUOMId As Long = CCommon.ToLong(dr("numSaleUnit"))

            Dim dtItemReleaseDate As RadDatePicker = CType(e.Row.FindControl("rdMultiSelectItemReleaseDate"), RadDatePicker)
            If Not String.IsNullOrEmpty(CCommon.ToString(dr("dtReleaseDate"))) Then
                dtItemReleaseDate.SelectedDate = Convert.ToDateTime(dr("dtReleaseDate"))
            Else
                dtItemReleaseDate.SelectedDate = Date.Today
                dr("dtReleaseDate") = dtItemReleaseDate.SelectedDate
            End If

            If CCommon.ToDouble(dr("numUnitHour")) > 0 Then
                CType(e.Row.FindControl("txtMultiSelectUnits"), TextBox).Text = dr("numUnitHour")
            Else
                CType(e.Row.FindControl("txtMultiSelectUnits"), TextBox).Text = 1
                dr("numUnitHour") = 1
            End If

            Dim Units As Double = CCommon.ToDouble(CType(e.Row.FindControl("txtMultiSelectUnits"), TextBox).Text)
            '''''' Bind ShipFrom Addressess''''''''''''''''
            dtMultiSelectShipFromAddr = objCommon.GetWarehouseAttrBasedItem(numItemCode)
            If Not dtMultiSelectShipFromAddr Is Nothing AndAlso dtMultiSelectShipFromAddr.Rows.Count > 0 Then
                Dim rdcmbShipFrom As RadComboBox = CType(e.Row.FindControl("rdcmbMultiSelectShipFrom"), RadComboBox)
                rdcmbShipFrom.DataSource = dtMultiSelectShipFromAddr
                rdcmbShipFrom.DataBind()

                If CCommon.ToLong(dataRowView("numShipFrom")) > 0 AndAlso Not rdcmbShipFrom.Items.FindItemByValue(dataRowView("numShipFrom")) Is Nothing Then
                    rdcmbShipFrom.Items.FindItemByValue(dataRowView("numShipFrom")).Selected = True
                Else
                    dr("numShipFrom") = rdcmbShipFrom.SelectedValue
                    dr("vcWareHouse") = CCommon.ToString(dtMultiSelectShipFromAddr.Select("numWareHouseItemId=" & rdcmbShipFrom.SelectedValue)(0)("vcWareHouse"))
                End If
            End If

            ''''''''''''''''''''''''''''

            '''''''''''' Bind ShipTo Addresses'''''''''''''
            Dim cmbShipTo As RadComboBox = CType(e.Row.FindControl("rdcmbMultiSelectShipTo"), RadComboBox)
            cmbShipTo.DataSource = dtMultiSelectShipToAddr
            cmbShipTo.DataTextField = "FullAddress"
            cmbShipTo.DataValueField = "numAddressID"
            cmbShipTo.DataBind()

            If CCommon.ToLong(dataRowView("numShipTo")) > 0 AndAlso Not cmbShipTo.Items.FindItemByValue(dataRowView("numShipTo")) Is Nothing Then
                cmbShipTo.Items.FindItemByValue(dataRowView("numShipTo")).Selected = True
            Else
                dr("numShipTo") = CCommon.ToLong(cmbShipTo.SelectedValue)
                If Not cmbShipTo.SelectedItem Is Nothing Then
                    dr("vcShipTo") = CCommon.ToString(cmbShipTo.SelectedItem.Text)
                End If
            End If
            '''''''''''''''''''''''''''''''''''''''''''''

            '''''''''''''Display Item Name with Attribute Info
            Dim lblItemNameAttr As Label = CType(e.Row.FindControl("lblMultiSelectItemNAttr"), Label)
            If (strAttr IsNot "") Then
                lblItemNameAttr.Text = strItemName + " ( " + strAttr + " )"
            Else
                lblItemNameAttr.Text = strItemName
            End If
            ''''''''''''''''''''''''''''''''''''''''''

            '''''''''''Calculate Unit Price for Every Item and Display in Unit Price textbox
            If Not String.IsNullOrEmpty(CCommon.ToString(dr("monPrice"))) Then
                CType(e.Row.FindControl("txtMultiSelectUnitPrice"), TextBox).Text = dr("monPrice")
            Else
                Dim dsPrice As DataSet = CalculatePriceForMultiSelectItem(numItemCode, Units, numUOMId, "")
                If (dsPrice.Tables IsNot Nothing) Then
                    If (dsPrice.Tables(0).Rows.Count > 0) Then
                        CType(e.Row.FindControl("txtMultiSelectUnitPrice"), TextBox).Text = CCommon.ToDecimal(dsPrice.Tables(0).Rows(0)("monPrice"))
                        dr("monPrice") = CCommon.ToDecimal(dsPrice.Tables(0).Rows(0)("monPrice"))
                    End If
                End If
            End If

            ''''''''''''''''''''''''''''''''''''''''''''''''''
            dr.AcceptChanges()
            dtMultiSelect.AcceptChanges()
            ViewState("dtMultiSelect") = dtMultiSelect
        End If
    End Sub

    Protected Sub btnAddtoOpp_Click(sender As Object, e As EventArgs)
        Try
            dtMultiSelect = CType(ViewState("dtMultiSelect"), DataTable)

            If Not dtMultiSelect Is Nothing AndAlso dtMultiSelect.Rows.Count > 0 Then
                For Each itemrow As DataRow In dtMultiSelect.Rows
                    If CCommon.ToDouble(itemrow("numUnitHour")) = 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "UnitsValidation", "alert(""Enter Units"");", True)
                        txtUnits.Focus()
                        Return
                    End If
                Next

                dsTemp = ViewState("SOItems")

                Dim MaxRowOrder As Long
                MaxRowOrder = dsTemp.Tables(0).Rows.Count

                Dim itemReleaseDate As String
                Dim ShipFrom As String = ""
                Dim WareHouseItemID As Long = 0

                For Each itemrow As DataRow In dtMultiSelect.Rows

                    Dim numItemCode As Long = CCommon.ToLong(itemrow("numItemCode"))
                    Dim Quantity As Double = CCommon.ToDouble(itemrow("numUnitHour"))
                    Dim Price As Decimal = CCommon.ToDecimal(itemrow("monPrice"))
                    Dim UOMConversionFactor As Decimal = CCommon.ToDecimal(itemrow("UOMConversionFactor"))
                    itemReleaseDate = CCommon.ToString(itemrow("dtReleaseDate"))
                    If CCommon.ToString(itemrow("charItemType")) = "P" Then
                        ShipFrom = CCommon.ToString(itemrow("vcWarehouse"))
                        WareHouseItemID = CCommon.ToLong(itemrow("numShipFrom"))
                    Else
                        ShipFrom = ""
                        WareHouseItemID = 0
                    End If

                    Dim numShipToAddressID As Long = CCommon.ToLong(itemrow("numShipTo"))
                    Dim ShipToFullAddress As String = CCommon.ToString(itemrow("vcShipTo"))

                    Dim OnHandAllocation As String = CCommon.ToString(itemrow("numOnHand")) + "/" + CCommon.ToString(itemrow("numAllocation"))

                    Dim objSelectedItem As New SelectedItems
                    objSelectedItem.numItemCode = numItemCode
                    objSelectedItem.numQuantity = Quantity
                    objSelectedItem.numSelectedUOM = CCommon.ToLong(itemrow("numSaleUnit"))
                    objSelectedItem.vcSelectedUOM = ""
                    objSelectedItem.numWarehouseItemID = WareHouseItemID
                    objSelectedItem.vcAttributeIDs = ""
                    objSelectedItem.vcAttributes = ""

                    hdnCurrentSelectedItem.Value = numItemCode

                    CalculateItemSalePrice(Price, Quantity, 0, True, IIf(UOMConversionFactor = 0, 1, UOMConversionFactor), "0")

                    AddSelectedItemsToItemGrid(objSelectedItem, MaxRowOrder, WareHouseItemID, Quantity, itemReleaseDate, ShipFrom, numShipToAddressID, ShipToFullAddress, OnHandAllocation, Price)
                    MaxRowOrder = MaxRowOrder + 1
                Next
            End If

            UpdatePanelShipping.Update()
            clearControls()
            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideOtherTopSectionAfter1stItemAdded", "HideOtherTopSectionAfter1stItemAdded();FocuonItem();  $('.itemsArea').css('visibility', 'hidden');$('.itemsArea').css('display', 'none');", True)
            ViewState("dtMultiSelect") = Nothing
            Session("MultiSelectItemCodes") = ""
            Session.Remove("MultiSelectItemCodes")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub btn2_Click(sender As Object, e As EventArgs)
        Try
            divMultiSelect.Visible = True
            BindMultiSelectItemsToDatatable(radCmbCompany.SelectedValue)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub gvMultiSelectItems_RowDeleting(sender As Object, e As GridViewDeleteEventArgs)

    End Sub

    Protected Sub txtMultiSelectUnits_TextChanged(sender As Object, e As EventArgs)

        Dim dr As DataRow
        Dim dtMultiSelect As DataTable = ViewState("dtMultiSelect")

        Dim txtUnits As TextBox = CType(sender, TextBox)
        Dim gvr As GridViewRow = CType(txtUnits.Parent.Parent, GridViewRow)
        Dim rowindex As Integer = gvr.RowIndex
        Dim ItemCode As Long = CCommon.ToLong(CType(gvMultiSelectItems.Rows(rowindex).FindControl("hdnItemCode"), HiddenField).Value)
        Dim uniqueID As String = CCommon.ToString(CType(gvMultiSelectItems.Rows(rowindex).FindControl("hdnID"), HiddenField).Value)

        Dim dRows As DataRow() = dtMultiSelect.Select("ID = '" & uniqueID & "'")
        If dRows.Length > 0 Then
            dr = dRows(0)

            dr("numUnitHour") = CCommon.ToLong(txtUnits.Text)
            Dim numUOMId As Long = CCommon.ToLong(dr("numSaleUnit"))
            Dim dsPrice As DataSet = CalculatePriceForMultiSelectItem(ItemCode, CCommon.ToLong(txtUnits.Text), numUOMId, "")
            If (dsPrice.Tables IsNot Nothing) Then
                If (dsPrice.Tables(0).Rows.Count > 0) Then
                    CType(gvMultiSelectItems.Rows(rowindex).FindControl("txtMultiSelectUnitPrice"), TextBox).Text = CCommon.ToDecimal(dsPrice.Tables(0).Rows(0)("monPrice"))
                    dr("monPrice") = CCommon.ToDecimal(dsPrice.Tables(0).Rows(0)("monPrice"))
                End If
            End If

            dr.AcceptChanges()
            dtMultiSelect.AcceptChanges()
            ViewState("dtMultiSelect") = dtMultiSelect
        End If
    End Sub

    Protected Sub gvMultiSelectItems_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        If (e.CommandName = "Delete") Then

            Dim dr As DataRow
            dtMultiSelect = CType(ViewState("dtMultiSelect"), DataTable)

            Dim uniqueID As String = CCommon.ToString(CType(gvMultiSelectItems.Rows(CCommon.ToInteger(e.CommandArgument)).FindControl("hdnID"), HiddenField).Value)
            Dim dRows As DataRow() = dtMultiSelect.Select("ID = '" & uniqueID & "'")

            If dRows.Length > 0 Then
                dr = dRows(0)
                dr.Delete()

                dtMultiSelect.AcceptChanges()
                ViewState("dtMultiSelect") = dtMultiSelect

                gvMultiSelectItems.DataSource = dtMultiSelect
                gvMultiSelectItems.DataBind()

                UpdatePanelItems.Update()

            End If
        End If
    End Sub

    Protected Sub btnClearItems_Click(sender As Object, e As EventArgs)
        If (gvMultiSelectItems IsNot Nothing) Then
            gvMultiSelectItems.DataSource = Nothing
            gvMultiSelectItems.DataBind()
            divMultiSelect.Visible = False
            ViewState("dtMultiSelect") = Nothing
            Session("MultiSelectItemCodes") = ""
            Session.Remove("MultiSelectItemCodes")
        End If
    End Sub

    Protected Sub gvMultiSelectItems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs)
        Try
            gvMultiSelectItems.PageIndex = e.NewPageIndex
            dtMultiSelect = CType(ViewState("dtMultiSelect"), DataTable)

            gvMultiSelectItems.DataSource = dtMultiSelect
            gvMultiSelectItems.DataBind()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub rdcmbMultiSelectShipFrom_ItemDataBound(sender As Object, e As RadComboBoxItemEventArgs)
        Try
            e.Item.Text = CType(e.Item.DataItem, DataRowView)("vcWareHouse").ToString() & " (" & CType(e.Item.DataItem, DataRowView)("numOnHand").ToString() & ") " & " ---- " & CType(e.Item.DataItem, DataRowView)("Attr").ToString().TrimEnd(",")
            e.Item.Value = CType(e.Item.DataItem, DataRowView)("numWareHouseItemId").ToString()
            CType(e.Item.FindControl("lblOnHandAllocation"), Label).Text = CType(e.Item.DataItem, DataRowView)("numOnHand").ToString() + "/" + CType(e.Item.DataItem, DataRowView)("numAllocation").ToString()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub rdcmbMultiSelectShipFrom_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)

        Dim dr As DataRow
        Dim dtMultiSelect As DataTable = ViewState("dtMultiSelect")
        Dim rdcmbShipFrom As RadComboBox = CType(sender, RadComboBox)
        Dim gvr As GridViewRow = CType(rdcmbShipFrom.Parent.Parent, GridViewRow)
        Dim rowindex As Integer = gvr.RowIndex
        Dim ItemCode As Long = CCommon.ToLong(CType(gvMultiSelectItems.Rows(rowindex).FindControl("hdnItemCode"), HiddenField).Value)
        Dim uniqueID As String = CCommon.ToString(CType(gvMultiSelectItems.Rows(rowindex).FindControl("hdnID"), HiddenField).Value)

        Dim dRows As DataRow() = dtMultiSelect.Select("ID = '" & uniqueID & "'")
        If dRows.Length > 0 Then
            dr = dRows(0)
            dr("numShipFrom") = rdcmbShipFrom.SelectedValue
            If Not rdcmbShipFrom.SelectedItem Is Nothing Then
                dr("vcWarehouse") = DirectCast(rdcmbShipFrom.SelectedItem.FindControl("lblWarehouse"), Label).Text
            End If


            dr.AcceptChanges()
            dtMultiSelect.AcceptChanges()
            ViewState("dtMultiSelect") = dtMultiSelect
        End If
    End Sub

    Protected Sub rdcmbMultiSelectShipTo_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
        Dim dr As DataRow
        Dim dtMultiSelect As DataTable = ViewState("dtMultiSelect")
        Dim rdcmbShipTo As RadComboBox = CType(sender, RadComboBox)
        Dim gvr As GridViewRow = CType(rdcmbShipTo.Parent.Parent, GridViewRow)
        Dim rowindex As Integer = gvr.RowIndex
        Dim ItemCode As Long = CCommon.ToLong(CType(gvMultiSelectItems.Rows(rowindex).FindControl("hdnItemCode"), HiddenField).Value)
        Dim uniqueID As String = CCommon.ToString(CType(gvMultiSelectItems.Rows(rowindex).FindControl("hdnID"), HiddenField).Value)

        Dim dRows As DataRow() = dtMultiSelect.Select("ID = '" & uniqueID & "'")
        If dRows.Length > 0 Then
            dr = dRows(0)
            dr("numShipTo") = rdcmbShipTo.SelectedValue
            If Not rdcmbShipTo.SelectedItem Is Nothing Then
                dr("vcShipTo") = rdcmbShipTo.SelectedItem.Text
            End If
            dr.AcceptChanges()
            dtMultiSelect.AcceptChanges()
            ViewState("dtMultiSelect") = dtMultiSelect
        End If
    End Sub

    Protected Sub txtMultiSelectUnitPrice_TextChanged(sender As Object, e As EventArgs)

        Dim dr As DataRow
        Dim dtMultiSelect As DataTable = ViewState("dtMultiSelect")

        Dim txtUnitPrice As TextBox = CType(sender, TextBox)
        Dim gvr As GridViewRow = CType(txtUnitPrice.Parent.Parent, GridViewRow)
        Dim rowindex As Integer = gvr.RowIndex
        Dim ItemCode As Long = CCommon.ToLong(CType(gvMultiSelectItems.Rows(rowindex).FindControl("hdnItemCode"), HiddenField).Value)
        Dim uniqueID As String = CCommon.ToString(CType(gvMultiSelectItems.Rows(rowindex).FindControl("hdnID"), HiddenField).Value)

        Dim dRows As DataRow() = dtMultiSelect.Select("ID = '" & uniqueID & "'")
        If dRows.Length > 0 Then
            dr = dRows(0)
        End If

        dr("monPrice") = CCommon.ToLong(txtUnitPrice.Text)

        dr.AcceptChanges()
        dtMultiSelect.AcceptChanges()
        ViewState("dtMultiSelect") = dtMultiSelect
    End Sub

    Protected Sub rdMultiSelectItemReleaseDate_SelectedDateChanged(sender As Object, e As Calendar.SelectedDateChangedEventArgs)
        Try
            Dim dr As DataRow
            Dim dtMultiSelect As DataTable = ViewState("dtMultiSelect")

            Dim rdMultiSelectItemReleaseDate As RadDatePicker = CType(sender, RadDatePicker)
            If Not rdMultiSelectItemReleaseDate Is Nothing Then
                Dim gvr As GridViewRow = DirectCast(rdMultiSelectItemReleaseDate.NamingContainer, GridViewRow)

                If Not gvr.FindControl("hdnID") Is Nothing Then
                    Dim uniqueID As String = CCommon.ToString(CType(gvr.FindControl("hdnID"), HiddenField).Value)

                    Dim dRows As DataRow() = dtMultiSelect.Select("ID = '" & uniqueID & "'")
                    If dRows.Length > 0 Then
                        dr = dRows(0)

                        dr("dtReleaseDate") = rdMultiSelectItemReleaseDate.SelectedDate

                        dr.AcceptChanges()
                        dtMultiSelect.AcceptChanges()
                        ViewState("dtMultiSelect") = dtMultiSelect
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub imgDuplicateLineItem_Click(sender As Object, e As ImageClickEventArgs)
        Try

            Dim dgGridItem As DataGridItem = TryCast(sender.NamingContainer, DataGridItem)

            Dim numItemCode As Long = CCommon.ToLong(dgItems.DataKeys(dgGridItem.ItemIndex))

            Dim dtSelectedItems As DataTable = dsTemp.Tables(0)
            Dim drnewitem As DataRow

            Dim dRows As DataRow() = dtSelectedItems.Select("numoppitemtCode = " & CCommon.ToLong(CType(dgGridItem.FindControl("lblOppItemCode"), Label).Text).ToString())

            drnewitem = dRows(0)
            Dim MaxRowOrder As Long = dgItems.Items.Count + 1

            Dim dgLastItem As DataGridItem = TryCast(dgItems.Items(dgItems.Items.Count - 1), DataGridItem)
            drnewitem("numoppitemtCode") = CCommon.ToLong(CType(dgLastItem.FindControl("lblOppItemCode"), Label).Text) + 1

            Dim txtUnits As TextBox = CType(dgGridItem.FindControl("txtUnits"), TextBox)
            drnewitem("numUnitHour") = txtUnits.Text

            Dim txtUnitPrice As TextBox = CType(dgGridItem.FindControl("txtUnitPrice"), TextBox)
            drnewitem("monPrice") = txtUnitPrice.Text

            Dim itemReleaseDate As String
            Dim ShipFrom As String = ""
            Dim WareHouseItemID As Long = 0
            Dim Quantity As Double = CCommon.ToDouble(CType(dgGridItem.FindControl("txtUnits"), TextBox).Text)
            Dim Price As Decimal = CCommon.ToDecimal(CType(dgGridItem.FindControl("txtUnitPrice"), TextBox).Text)
            Dim UOMConversionFactor As Decimal = CCommon.ToDecimal(drnewitem("UOMConversionFactor"))
            itemReleaseDate = CCommon.ToString(drnewitem("ItemReleaseDate"))
            ' If CCommon.ToString(drnewitem("charItemType")) = "P" Then
            ShipFrom = CCommon.ToString(drnewitem("Location"))
            WareHouseItemID = CCommon.ToLong(drnewitem("numWarehouseItmsID"))
            'Else
            '    ShipFrom = ""
            '    WareHouseItemID = 0
            'End If

            Dim numShipToAddressID As Long = CCommon.ToLong(drnewitem("numShipToAddressID"))
            Dim ShipToFullAddress As String = CCommon.ToString(drnewitem("ShipToFullAddress"))

            Dim OnHandAllocation As String = CCommon.ToString(drnewitem("OnHandAllocation"))

            Dim objItem As New CItems
            objItem.ItemCode = numItemCode
            objItem.WareHouseItemID = WareHouseItemID
            objItem.Attributes = ""
            objItem.AttributeIDs = ""

            hdnCurrentSelectedItem.Value = numItemCode

            If CCommon.ToString(drnewitem("charItemType")) = "P" AndAlso IsDuplicate(objItem.ItemCode, objItem.WareHouseItemID, chkDropShip.Checked) Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "DuplicateItemWarehouse", "alert(""Please enable Duplicate Item entry from Global Settings "")", True)
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ClearSelectedItems", "$('#divItem').show(); $('#txtItem').select2('data', null);", True)
                Exit Sub
            End If
            'Start - Get Tax Detail
            Dim strApplicable As String
            Dim dtItemTax As DataTable
            If (pageType = PageTypeEnum.Sales) Then
                dtItemTax = objItem.ItemTax()

                For Each dr As DataRow In dtItemTax.Rows
                    strApplicable = strApplicable & dr("bitApplicable") & ","
                Next

                strApplicable = strApplicable & objItem.Taxable
            End If

            objCommon = New CCommon
            objCommon.DomainID = CCommon.ToLong(Session("DomainID"))

            ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon,
                                                                               dsTemp,
                                                                               True,
                                                                               CCommon.ToString(drnewitem("charItemType")),
                                                                               "",
                                                                               CCommon.ToBool(drnewitem("DropShip")),
                                                                               "",
                                                                               numItemCode,
                                                                               Quantity,
                                                                               Price,
                                                                               CCommon.ToString(drnewitem("vcItemDesc")), WareHouseItemID, CCommon.ToString(drnewitem("vcItemName")), "", 0, CCommon.ToString(drnewitem("vcModelID")), CCommon.ToLong(drnewitem("numUOM")), CCommon.ToString(drnewitem("vcUOMName")),
                                                                               If(UOMConversionFactor = 0, 1, CCommon.ToDouble(UOMConversionFactor)),
                                                                               pageType.ToString(), False, CCommon.ToDecimal(drnewitem("fltDiscount")), strApplicable, CCommon.ToString(drnewitem("Tax0")), CCommon.ToString(drnewitem("CustomerPartNo")), chkTaxItems,
                                                                               CCommon.ToBool(drnewitem("bitWorkOrder")), CCommon.ToString(drnewitem("vcInstruction")), CCommon.ToString(drnewitem("bintCompliationDate")),
                                                                               CCommon.ToLong(drnewitem("numWOAssignedTo")),
                                                                               CCommon.ToLong(drnewitem("numVendorWareHouse")),
                                                                               numProjectID:=CCommon.ToLong(drnewitem("numProjectID")),
                                                                               numProjectStageID:=CCommon.ToLong(drnewitem("numProjectStageID")),
                                                                               vcBaseUOMName:=CCommon.ToString(drnewitem("vcBaseUOMName")),
                                                                               numPrimaryVendorID:=CCommon.ToLong(drnewitem("numVendorID")),
                                                                               numSOVendorId:=CCommon.ToLong(drnewitem("numSOVendorId")),
                                                                               strSKU:=CCommon.ToString(drnewitem("vcSKU")), objItem:=objItem,
                                                                               salePrice:=CCommon.ToDecimal(drnewitem("monSalePrice")), numMaxWOQty:=CCommon.ToInteger(drnewitem("numMaxWorkOrderQty")), vcAttributes:=objItem.Attributes, vcAttributeIDs:=objItem.AttributeIDs, numContainer:=objItem.numContainer, numContainerQty:=objItem.numNoItemIntoContainer,
                                                                               numItemClassification:=CCommon.ToLong(drnewitem("numItemClassification")),
                                                                               numPromotionID:=CCommon.ToLong(drnewitem("numPromotionID")),
                                                                               IsPromotionTriggered:=CCommon.ToBool(drnewitem("bitPromotionTriggered")),
                                                                               vcPromotionDetail:=CCommon.ToString(drnewitem("vcPromotionDetail")), numSortOrder:=MaxRowOrder, numCost:=CCommon.ToDouble(txtPUnitCost.Text), vcVendorNotes:=CCommon.ToString(drnewitem("vcVendorNotes")),
                                                                               itemReleaseDate:=itemReleaseDate, ShipFromLocation:=ShipFrom, numShipToAddressID:=numShipToAddressID,
                                                                               ShipToFullAddress:=ShipToFullAddress, InclusionDetail:=lblMatrixAndKitAttributes.Text,
                                                                               OnHandAllocation:=OnHandAllocation,
                                                                               dtPlannedStart:=CCommon.ToString(drnewitem("dtPlannedStart")))
            AddSimilarRequiredRow()
            RecalculateContainerQty(dsTemp.Tables(0))
            UpdateDetails()
            UpdatePanelShipping.Update()
            clearControls()
            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideOtherTopSectionAfter1stItemAdded", "HideOtherTopSectionAfter1stItemAdded();FocuonItem();  $('.itemsArea').css('visibility', 'hidden');$('.itemsArea').css('display', 'none');", True)


        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub btnapplyCouponCode_Click(sender As Object, e As EventArgs)
        Try
            If (String.IsNullOrEmpty(txtApplyCouponCode.Text) And btnapplyCouponCode.Text = "Apply Code") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ValidationOrderPromCode", "alert('Please enter Coupon Code to apply promotion.');", True)
                Exit Sub
            End If

            If btnapplyCouponCode.Text = "Clear Code" Then
                ClearCouponCodeItemPromotion(dsTemp.Tables(0))

                txtApplyCouponCode.Text = ""
                txtApplyCouponCode.Enabled = True
                btnapplyCouponCode.Text = "Apply Code"
                lblCouponMsg.Text = ""
                lblErrroCouponMsg.Text = ""
                hdnApplyCouponCode.Value = ""
            Else
                Dim objDiscountCodes As New DiscountCodes
                objDiscountCodes.DomainID = CCommon.ToLong(Session("DomainID"))
                objDiscountCodes.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                objDiscountCodes.DiscountCode = txtApplyCouponCode.Text.Trim()
                Dim dt As DataTable = objDiscountCodes.ValidateDiscountCode()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    lblCouponMsg.Text = "Code confirmed."
                    lblErrroCouponMsg.Text = ""
                    txtApplyCouponCode.Enabled = False
                    hdnApplyCouponCode.Value = CCommon.ToLong(dt.Rows(0)("numPromotionID"))
                    If btnapplyCouponCode.Text = "Apply Code" Then
                        btnapplyCouponCode.Text = "Clear Code"
                    End If

                    If CCommon.ToLong(dt.Rows(0)("numPromotionID")) > 0 Then
                        UseItemPromotionIfAvailable(dsTemp.Tables(0), True, CCommon.ToString(dt.Rows(0)("vcCouponCode")))
                        UpdateGridDataAndFooterFields()
                    End If
                Else
                    txtApplyCouponCode.Text = ""
                    txtApplyCouponCode.Enabled = True
                    hdnApplyCouponCode.Value = ""
                End If
            End If

        Catch ex As Exception
            txtApplyCouponCode.Text = ""
            txtApplyCouponCode.Enabled = True
            hdnApplyCouponCode.Value = ""

            If ex.Message.Contains("INVALID_COUPON_CODE") Then
                lblErrroCouponMsg.Text = "Invalid Coupon Code."
                lblCouponMsg.Text = ""
            ElseIf ex.Message.Contains("COUPON_CODE_EXPIRED") Then
                lblErrroCouponMsg.Text = "Coupon Code is expired."
                lblCouponMsg.Text = ""
            ElseIf ex.Message.Contains("COUPON_USAGE_LIMIT_EXCEEDED") Then
                lblErrroCouponMsg.Text = "Coupon Code usage limit exceeds. Promotion can not be applied."
                lblCouponMsg.Text = ""
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try

    End Sub

    Protected Sub btnConfirmProm_Click(sender As Object, e As EventArgs)
        Try
            Dim confirmValue As String = Request.Form("confirm_value")

            If (confirmValue = "No" And bitRequireCouponCode = False) Then
                save()
            ElseIf (confirmValue = "Yes" And bitRequireCouponCode = False) Then
                If lngDiscountServiceItemForOrder = 0 Then
                    boolFlag = False
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ValidationOrderProm", "alert('In order to set order discounts, You have to select an order discount line item from within Global Settings | Accounting | Default Item used for discounting orders.');", True)
                    Exit Sub
                Else
                    save()
                End If
            ElseIf (confirmValue = "No" And bitRequireCouponCode = True) Then
                save()
            ElseIf (confirmValue = "Yes" And bitRequireCouponCode = True) Then
                If lngDiscountServiceItemForOrder = 0 Then
                    boolFlag = False
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ValidationOrderProm", "alert('In order to set order discounts, You have to select an order discount line item from within Global Settings | Accounting | Default Item used for discounting orders.');", True)
                    Exit Sub
                End If

            End If
        Catch ex As Exception
            If ex.Message.Contains("ITEM_PROMOTION_EXPIRED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Item promotion(s) used are expired.' );", True)
            ElseIf ex.Message.Contains("MULTIPLE_COUPON_BASED_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Multiple coupon based Item promotion(s) used.' );", True)
            ElseIf ex.Message.Contains("INVALID_COUPON_CODE_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Invalid item promotion coupon Code.' );", True)
            ElseIf ex.Message.Contains("ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Item promotion Coupon usage limit exceeds, Promotion can not be applied. Please clear code to Proceed.' );", True)
            ElseIf ex.Message.Contains("COUPON_CODE_REQUIRED_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Coupon code is required to use item promotion.' );", True)
            ElseIf ex.Message.Contains("ORDER_PROMOTION_EXPIRED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Order promotion used is expired.' );", True)
            ElseIf ex.Message.Contains("INVALID_COUPON_CODE_ORDER_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Invalid order promotion coupon Code.' );", True)
            ElseIf ex.Message.Contains("ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Order promotion Coupon usage limit exceeds, Promotion can not be applied. Please clear code to Proceed.' );", True)
            ElseIf ex.Message.Contains("COUPON_CODE_REQUIRED_ORDER_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Coupon code is required to use order promotion.' );", True)
            ElseIf ex.Message.Contains("SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Warehouse selected for kit/assembly item(s) is not available for all child items.' );", True)
            ElseIf ex.Message.Contains("FY_CLOSED") Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub

    'Protected Sub lnkbtnSet_Click(sender As Object, e As EventArgs)
    '    Dim objOpportunity As New COpportunities
    '    objOpportunity.DomainID = Session("DomainID")

    '    Dim dtState As DataTable = objOpportunity.GetStateIDForZip(hdnChangeState.Value)

    '    If (dtState IsNot Nothing And dtState.Rows.Count > 0) Then
    '        numChangeZipStateId = CCommon.ToLong(dtState.Rows(0)("numStateID"))

    '        ShippingPromotion()
    '        DivChangeZip.Style.Add("display", "none")
    '    Else
    '        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ChangeZip", "alert('Zip Not Available.');", True)
    '        txtChangeZip.Text = ""
    '    End If
    'End Sub

    Private Sub btnCheck_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCheck.Click
        Try
            'IsFromCreateBizDoc = True
            If (CreateOrder("btnCheck") = True) Then
                If CCommon.ToBool(hdnIsUnitPriceApprovalRequired.Value) = False Or hdnApprovalActionTaken.Value = "Approved" Then
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "OpenAmtPaid", "OpenAmtPaid(" + Convert.ToString(OppBizDocID) + "," + Convert.ToString(lngOppId) + "," + radCmbCompany.SelectedValue + ",5)", True)
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "OpenAmtPaid", "OpenAmtPaid(" + Convert.ToString(OppBizDocID) + "," + Convert.ToString(lngOppId) + "," + radCmbCompany.SelectedValue + ",5,1)", True)
                End If
            End If

        Catch ex As Exception
            If ex.Message.Contains("ITEM_PROMOTION_EXPIRED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Item promotion(s) used are expired.' );", True)
            ElseIf ex.Message.Contains("MULTIPLE_COUPON_BASED_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Multiple coupon based Item promotion(s) used.' );", True)
            ElseIf ex.Message.Contains("INVALID_COUPON_CODE_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Invalid item promotion coupon Code.' );", True)
            ElseIf ex.Message.Contains("ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Item promotion Coupon usage limit exceeds, Promotion can not be applied. Please clear code to Proceed.' );", True)
            ElseIf ex.Message.Contains("COUPON_CODE_REQUIRED_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Coupon code is required to use item promotion.' );", True)
            ElseIf ex.Message.Contains("ORDER_PROMOTION_EXPIRED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Order promotion used is expired.' );", True)
            ElseIf ex.Message.Contains("INVALID_COUPON_CODE_ORDER_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Invalid order promotion coupon Code.' );", True)
            ElseIf ex.Message.Contains("ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Order promotion Coupon usage limit exceeds, Promotion can not be applied. Please clear code to Proceed.' );", True)
            ElseIf ex.Message.Contains("COUPON_CODE_REQUIRED_ORDER_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Coupon code is required to use order promotion.' );", True)
            ElseIf ex.Message.Contains("SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Warehouse selected for kit/assembly item(s) is not available for all child items.' );", True)
            ElseIf ex.Message.Contains("FY_CLOSED") Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub

    Private Sub btnBillMe_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnBillMe.Click
        Try
            Dim objProspects As New CProspects
            Dim dtComInfo As DataTable
            objProspects.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
            objProspects.DomainID = Session("DomainID")
            objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            dtComInfo = objProspects.GetCompanyInfoForEdit
            If Not IsDBNull(dtComInfo.Rows(0).Item("numBillingDays")) Then
                If (dtComInfo.Rows(0).Item("numBillingDays") > 0) Then
                    If (CreateOrder("btnBillMe") = True) Then
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "OpenAmtPaid", "OpenAmtPaid(" + Convert.ToString(OppBizDocID) + "," + Convert.ToString(lngOppId) + "," + radCmbCompany.SelectedValue + ",5,1)", True)
                    End If
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "AlertBox", "alert('You can't create a Sales Order with Bill-Me Credit Terms because the customer this order is for, doesn't have them set up in their record');self.close();", True)
                End If
            Else
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "AlertBox", "alert('You can't create a Sales Order with Bill-Me Credit Terms because the customer this order is for, doesn't have them set up in their record');self.close();", True)
            End If

        Catch ex As Exception
            If ex.Message.Contains("ITEM_PROMOTION_EXPIRED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Item promotion(s) used are expired.' );", True)
            ElseIf ex.Message.Contains("MULTIPLE_COUPON_BASED_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Multiple coupon based Item promotion(s) used.' );", True)
            ElseIf ex.Message.Contains("INVALID_COUPON_CODE_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Invalid item promotion coupon Code.' );", True)
            ElseIf ex.Message.Contains("ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Item promotion Coupon usage limit exceeds, Promotion can not be applied. Please clear code to Proceed.' );", True)
            ElseIf ex.Message.Contains("COUPON_CODE_REQUIRED_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Coupon code is required to use item promotion.' );", True)
            ElseIf ex.Message.Contains("ORDER_PROMOTION_EXPIRED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Order promotion used is expired.' );", True)
            ElseIf ex.Message.Contains("INVALID_COUPON_CODE_ORDER_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Invalid order promotion coupon Code.' );", True)
            ElseIf ex.Message.Contains("ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Order promotion Coupon usage limit exceeds, Promotion can not be applied. Please clear code to Proceed.' );", True)
            ElseIf ex.Message.Contains("COUPON_CODE_REQUIRED_ORDER_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Coupon code is required to use order promotion.' );", True)
            ElseIf ex.Message.Contains("SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Warehouse selected for kit/assembly item(s) is not available for all child items.' );", True)
            ElseIf ex.Message.Contains("FY_CLOSED") Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub

    Private Sub btnOthers_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOthers.Click
        Try
            'IsFromCreateBizDoc = True
            'CreateOrder()
            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "openOtherPaymentDropDown", "$('#dialogOtherPayment').show();", True)
        Catch ex As Exception
            If ex.Message.Contains("FY_CLOSED") Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub

    Private Sub btnOthersPayment_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOthersPayment.Click
        Try
            'IsFromCreateBizDoc = True
            If (CreateOrder("btnOthersPayment") = True) Then
                If CCommon.ToBool(hdnIsUnitPriceApprovalRequired.Value) = False Or hdnApprovalActionTaken.Value = "Approved" Then
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "OpenAmtPaid", "OpenAmtPaid(" + Convert.ToString(OppBizDocID) + "," + Convert.ToString(lngOppId) + "," + radCmbCompany.SelectedValue + "," + ddlPaymentMethods.SelectedValue + ")", True)
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "OpenAmtPaid", "OpenAmtPaid(" + Convert.ToString(OppBizDocID) + "," + Convert.ToString(lngOppId) + "," + radCmbCompany.SelectedValue + "," + ddlPaymentMethods.SelectedValue + ",1)", True)
                End If
            End If

        Catch ex As Exception
            If ex.Message.Contains("ITEM_PROMOTION_EXPIRED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Item promotion(s) used are expired.' );", True)
            ElseIf ex.Message.Contains("MULTIPLE_COUPON_BASED_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Multiple coupon based Item promotion(s) used.' );", True)
            ElseIf ex.Message.Contains("INVALID_COUPON_CODE_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Invalid item promotion coupon Code.' );", True)
            ElseIf ex.Message.Contains("ITEM_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Item promotion Coupon usage limit exceeds, Promotion can not be applied. Please clear code to Proceed.' );", True)
            ElseIf ex.Message.Contains("COUPON_CODE_REQUIRED_ITEM_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Coupon code is required to use item promotion.' );", True)
            ElseIf ex.Message.Contains("ORDER_PROMOTION_EXPIRED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Order promotion used is expired.' );", True)
            ElseIf ex.Message.Contains("INVALID_COUPON_CODE_ORDER_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Invalid order promotion coupon Code.' );", True)
            ElseIf ex.Message.Contains("ORDER_PROMOTION_COUPON_CODE_USAGE_LIMIT_EXCEEDED") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Order promotion Coupon usage limit exceeds, Promotion can not be applied. Please clear code to Proceed.' );", True)
            ElseIf ex.Message.Contains("COUPON_CODE_REQUIRED_ORDER_PROMOTION") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Coupon code is required to use order promotion.' );", True)
            ElseIf ex.Message.Contains("SELECTED_WAREHOUSE_IS_NOT_AVAILABLE_FOR_ALL_CHILD_ITEMS") Then
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Warehouse selected for kit/assembly item(s) is not available for all child items.' );", True)
            ElseIf ex.Message.Contains("FY_CLOSED") Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End If
        End Try
    End Sub

    Private Function CreateOrder(ByVal buttonID As String) As Boolean
        Try
            Dim sequences As List(Of String) = dgItems.Items.Cast(Of DataGridItem)().[Select](Function(r) TryCast(r.FindControl("txtRowOrder"), TextBox).Text.Trim()).ToList()

            If CCommon.ToBool(Session("IsMinUnitPriceRule")) Then
                If Not hdnApprovalActionTaken.Value = "Approved" And Not hdnApprovalActionTaken.Value = "Save&Proceed" Then
                    If CheckIfUnitPriceApprovalRequired() Then
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "UnitPriceApproval", "$( ""#dialogUnitPriceApproval"" ).show();", True)
                        hdnClickedButton.Value = buttonID
                        Return False
                        Exit Function
                    End If
                End If
            End If
            'Added By Neelam On 02/05/2018 - To Check the Minimum Order Amount rule
            Dim dtEnforcedMinSubTotal As DataTable = CheckIfMinOrderAmountRuleMeets()
            If dtEnforcedMinSubTotal IsNot Nothing AndAlso dtEnforcedMinSubTotal.Rows.Count > 0 AndAlso dtEnforcedMinSubTotal.Rows(0)("returnVal") = 0 Then
                Dim subTotal = dtEnforcedMinSubTotal.Rows(0)("MinOrderAmount")
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "MinOrderAmountRuleMeets", "alert('You must spend a minimum of $" & subTotal & " to complete this order.');", True)
                Return False
                Exit Function
            End If
            save()

            'If unit price approval is not required and unit price is already approved then only then create bizdoc
            If CCommon.ToBool(hdnIsUnitPriceApprovalRequired.Value) = False Or hdnApprovalActionTaken.Value = "Approved" Then
                If (pageType = PageTypeEnum.Sales) Then
                    Dim objRule As New OrderAutoRules
                    objRule.GenerateAutoPO(OppBizDocID)


                    If (CCommon.ToInteger(radcmbPaymentMethods.SelectedValue) > 0) Then
                        ReceivePayment()
                    End If
                End If
            End If

            ViewState("SOItems") = Nothing
            Session("CouponCode") = Nothing
            Session("TotalDiscount") = Nothing
            Return True
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub chkDropShip_CheckedChanged(sender As Object, e As EventArgs) Handles chkDropShip.CheckedChanged
        Try
            If chkDropShip.Checked Then
                divWarehouse.Visible = False
                radWareHouse.Visible = False
                divLocations.Visible = False
            Else
                divWarehouse.Visible = True
                radWareHouse.Visible = True
                divLocations.Visible = True
            End If

            ScriptManager.RegisterClientScriptBlock(UpdatePanelItems, UpdatePanelItems.GetType(), "HideItemSearch", "FocusPrice();$('.itemsArea').css('visibility', 'visible');$('.itemsArea').css('display', 'flow-root');", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub btnSaveEditKit_Click(sender As Object, e As EventArgs)
        Try
            Dim vcSelectedItems As String = ""
            For Each item As GridViewRow In gvChildItems.Rows
                vcSelectedItems = vcSelectedItems & If(vcSelectedItems.Length > 0, ",0-", "0-") & (DirectCast(item.FindControl("lblItemCode"), Label).Text & "-" & DirectCast(item.FindControl("txtUnits"), TextBox).Text & "-" & DirectCast(item.FindControl("txtSequence"), TextBox).Text)
            Next

            hdnKitChildItems.Value = vcSelectedItems
            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideEditKitWindow", "$('#divEditKit').hide();", True)

            gvChildItems.DataSource = Nothing
            gvChildItems.DataBind()

            divKitPrice.Visible = False
            lblKitPrice.Text = ""
            lblKitPriceType.Text = ""
            hdnKitPricing.Value = ""
            hdnKitListPrice.Value = ""

            UpdatePanel3.Update()

            ItemFieldsChanged(hdnCurrentSelectedItem.Value, False)

            UpdatePanelItems.Update()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub btnAddChilItem_Click(sender As Object, e As EventArgs)
        Try
            If CCommon.ToLong(hdnSelectedChildItem.Value) > 0 Then
                Dim vcSelectedItems As String = ""
                For Each item As GridViewRow In gvChildItems.Rows
                    vcSelectedItems = vcSelectedItems & If(vcSelectedItems.Length > 0, ",0-", "0-") & (DirectCast(item.FindControl("lblItemCode"), Label).Text & "-" & DirectCast(item.FindControl("txtUnits"), TextBox).Text & "-" & DirectCast(item.FindControl("txtSequence"), TextBox).Text)
                Next

                Dim objItem As New CItems
                objItem.ItemCode = CCommon.ToLong(hdnCurrentSelectedItem.Value)
                objItem.ChildItemID = CCommon.ToLong(hdnSelectedChildItem.Value)
                Dim dsItem As DataSet = objItem.GetSelectedChildItemDetails(vcSelectedItems, False)

                If Not dsItem Is Nothing And dsItem.Tables.Count > 1 Then
                    If CCommon.ToBool(dsItem.Tables(0).Rows(0)("bitCalAmtBasedonDepItems")) Then
                        lblKitPrice.Text = CCommon.ToDouble(dsItem.Tables(0).Rows(0)("monPrice"))
                        hdnKitPricing.Value = CCommon.ToShort(dsItem.Tables(0).Rows(0)("tintKitAssemblyPriceBasedOn"))
                        hdnKitListPrice.Value = CCommon.ToDouble(dsItem.Tables(0).Rows(0)("monListPrice"))
                    Else
                        lblKitPrice.Text = ""
                        hdnKitPricing.Value = ""
                        hdnKitListPrice.Value = ""
                    End If

                    gvChildItems.DataSource = dsItem.Tables(1)
                    gvChildItems.DataBind()
                End If

                hdnSelectedChildItem.Value = ""
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ClearSelectedChildItem", "$('#txtChildItem').select2('data', null);", True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub btnRemoveChilItem_Click(sender As Object, e As EventArgs)
        Try
            Dim vcSelectedItems As String = ""
            For Each item As GridViewRow In gvChildItems.Rows
                If Not DirectCast(item.FindControl("chkSelect"), CheckBox).Checked Then
                    vcSelectedItems = vcSelectedItems & If(vcSelectedItems.Length > 0, ",0-", "0-") & (DirectCast(item.FindControl("lblItemCode"), Label).Text & "-" & DirectCast(item.FindControl("txtUnits"), TextBox).Text & "-" & DirectCast(item.FindControl("txtSequence"), TextBox).Text)
                End If
            Next

            Dim objItem As New CItems
            objItem.ItemCode = CCommon.ToLong(hdnCurrentSelectedItem.Value)
            objItem.ChildItemID = CCommon.ToLong(hdnSelectedChildItem.Value)
            Dim dsItem As DataSet = objItem.GetSelectedChildItemDetails(vcSelectedItems, False)

            If Not dsItem Is Nothing And dsItem.Tables.Count > 1 Then
                If CCommon.ToBool(dsItem.Tables(0).Rows(0)("bitCalAmtBasedonDepItems")) Then
                    lblKitPrice.Text = CCommon.ToDouble(dsItem.Tables(0).Rows(0)("monPrice"))
                    hdnKitPricing.Value = CCommon.ToShort(dsItem.Tables(0).Rows(0)("tintKitAssemblyPriceBasedOn"))
                    hdnKitListPrice.Value = CCommon.ToDouble(dsItem.Tables(0).Rows(0)("monListPrice"))
                Else
                    lblKitPrice.Text = ""
                    hdnKitPricing.Value = ""
                    hdnKitListPrice.Value = ""
                End If

                gvChildItems.DataSource = dsItem.Tables(1)
                gvChildItems.DataBind()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub btnClosePromotionStatus_Click(sender As Object, e As EventArgs)
        Try
            gvItemPromotionStatus.DataSource = Nothing
            gvItemPromotionStatus.DataBind()

            lblPromotionDescription.Text = ""
            lblPromotionStatus.Text = ""

            hdnIPSPromotionID.Value = ""
            hdnIsCouponCodeRequired.Value = ""
            hdnIPSItemID.Value = ""
            hdnIPSOppItemID.Value = ""
            hdnIPSWarehouseItemID.Value = ""

            hdnFromItemPromoPopup.Value = "1"
            hdnItemPromokitChildsHiddeFieldID.Value = ""
            hdnItemPromoAddbuttonID.Value = ""

            divItemPromotionStatus.Visible = False

            uplItemPromotionStatus.Update()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
        Try
            Dim objPromotionOffer As New PromotionOffer
            objPromotionOffer.DomainID = Session("DomainID")
            objPromotionOffer.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
            objPromotionOffer.PromotionID = CCommon.ToLong(hdnIPSPromotionID.Value)

            Dim dt As DataTable = objPromotionOffer.GetItemPromotionDiscountItems(CCommon.ToLong(radCmbCompany.SelectedValue), CCommon.ToLong(hdnIPSItemID.Value), CCommon.ToLong(hdnIPSWarehouseItemID.Value), bizPager.CurrentPageIndex, bizPager.PageSize)

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                bizPager.RecordCount = CCommon.ToInteger(dt.Rows(0)("TotalRecords"))

                gvItemPromotionStatus.DataSource = dt
                gvItemPromotionStatus.DataBind()
            Else
                bizPager.RecordCount = 0
                bizPager.CurrentPageIndex = 0
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub btnAddItem_Click(sender As Object, e As EventArgs)
        Try

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub gvItemPromotionStatus_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Try
            If e.CommandName = "AddDiscountedItem" Then
                Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, Control).NamingContainer, GridViewRow)


                If DirectCast(row.FindControl("hdnHasChildKits"), HiddenField).Value = "1" AndAlso String.IsNullOrEmpty(DirectCast(row.FindControl("hdnSelectedChildKits"), HiddenField).Value) Then
                    hdnFromItemPromoPopup.Value = "1"
                    hdnItemPromokitChildsHiddeFieldID.Value = CCommon.ToString(DirectCast(row.FindControl("hdnSelectedChildKits"), HiddenField).ClientID)
                    hdnItemPromoAddbuttonID.Value = CCommon.ToString(DirectCast(row.FindControl("btnAddItem"), Button).ClientID)

                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "LoadChildKits", "LoadChildKits(" & CCommon.ToLong(DirectCast(row.FindControl("hdnItemCode"), HiddenField).Value) & ")", True)
                    hdnHasKitAsChild.Value = "0"
                    divItemPromotionStatus.Style.Add("display", "none")
                Else
                    Dim MaxRowOrder As Integer
                    MaxRowOrder = dsTemp.Tables(0).Rows.Count

                    Dim units As Integer = CCommon.ToDouble(DirectCast(row.FindControl("txtUnits"), TextBox).Text)
                    Dim isDiscountInPer As Boolean = True
                    Dim decDiscount As Double = 0
                    Dim price As Double = 0
                    Dim salePrice As Double = 0

                    Dim objItems As New CItems
                    objItems.DomainID = CCommon.ToLong(Session("DomainID"))
                    objItems.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                    objItems.byteMode = CCommon.ToShort(oppType)
                    objItems.ItemCode = CCommon.ToLong(DirectCast(row.FindControl("hdnItemCode"), HiddenField).Value)
                    objItems.WareHouseItemID = CCommon.ToLong(DirectCast(row.FindControl("hdnWarehouseItemID"), HiddenField).Value)
                    objItems.bitHasKitAsChild = If(DirectCast(row.FindControl("hdnHasChildKits"), HiddenField).Value = "1", True, False)
                    objItems.vcKitChildItems = CCommon.ToString(DirectCast(row.FindControl("hdnSelectedChildKits"), HiddenField).Value)
                    objItems.GetItemAndWarehouseDetails()
                    MaxRowOrder = MaxRowOrder + 1
                    objItems.numSortOrder = MaxRowOrder



                    If DirectCast(row.FindControl("hdnCalAmtBasedonDepItems"), HiddenField).Value = "1" Then
                        price = objItems.GetPriceBasedOnPricingMethod(units, CCommon.ToString(DirectCast(row.FindControl("hdnSelectedChildKits"), HiddenField).Value), CCommon.ToLong(radcmbCurrency.SelectedValue))
                        salePrice = price
                    Else
                        price = CCommon.ToDouble(DirectCast(row.FindControl("lblListPrice"), Label).Text.Replace(",", ""))
                        salePrice = CCommon.ToDouble(DirectCast(row.FindControl("lblListPrice"), Label).Text.Replace(",", ""))
                    End If

                    Dim strApplicable As String
                    Dim dtItemTax As DataTable

                    dtItemTax = objItems.ItemTax()

                    For Each dr As DataRow In dtItemTax.Rows
                        strApplicable = strApplicable & dr("bitApplicable") & ","
                    Next

                    strApplicable = strApplicable & objItems.Taxable


                    ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon, dsTemp, True, objItems.ItemType, "", False, objItems.KitParent, objItems.ItemCode, units, price, objItems.ItemDesc, objItems.WareHouseItemID,
                                                                                        objItems.ItemName, "", 0, objItems.ModelID, 0, "", 1, pageType.ToString(), isDiscountInPer, decDiscount, strApplicable, txtTax.Text, txtCustomerPartNo.Text, chkTaxItems,
                                                                                        numProjectID:=CCommon.ToLong(GetQueryStringVal("Source")),
                                                                                        numProjectStageID:=CCommon.ToLong(GetQueryStringVal("StageID")),
                                                                                        vcBaseUOMName:="",
                                                                                        objItem:=objItems, primaryVendorCost:=0, salePrice:=salePrice, numPromotionID:=hdnIPSPromotionID.Value,
                                                                                        IsPromotionTriggered:=False,
                                                                                        vcPromotionDetail:=lblPromotionDescription.Text, numSortOrder:=objItems.numSortOrder, itemReleaseDate:=GetReleaseDate())

                    If Not CCommon.ToBool(hdnIsCouponCodeRequired.Value) Then
                        Dim dRows As DataRow() = dsTemp.Tables(0).Select("numoppitemtCode = " & CCommon.ToLong(hdnIPSOppItemID.Value))

                        If dRows.Length > 0 Then
                            dRows(0)("numPromotionID") = hdnIPSPromotionID.Value
                            dRows(0)("bitPromotionTriggered") = True
                            dRows(0)("vcPromotionDetail") = lblPromotionDescription.Text
                        End If
                    End If

                    UpdateDetails()
                    UpdateDataTable()
                    UpdatePanelItems.Update()

                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ClearSelectedItems", "alert('Item added.');", True)
                End If


            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub UseItemPromotionIfAvailable(ByRef dtItems As DataTable, ByVal isBasedOnDiscountCode As Boolean, ByVal vcDiscountCode As String, Optional ByVal isFromNewItem As Boolean = False)
        Try
            Dim dt As New DataTable("Item")
            dt.Columns.Add("numOppItemID", GetType(Long))
            dt.Columns.Add("numItemCode", GetType(Long))
            dt.Columns.Add("numUnits", GetType(Double))
            dt.Columns.Add("numWarehouseItemID", GetType(Long))
            dt.Columns.Add("monUnitPrice", GetType(Double))
            dt.Columns.Add("fltDiscount", GetType(Double))
            dt.Columns.Add("bitDiscountType", GetType(Boolean))
            dt.Columns.Add("monTotalAmount", GetType(Double))
            dt.Columns.Add("numPromotionID", GetType(Long))
            dt.Columns.Add("bitPromotionTriggered", GetType(Boolean))
            dt.Columns.Add("bitPromotionDiscount", GetType(Boolean))
            dt.Columns.Add("vcPromotionDescription", GetType(String))
            dt.Columns.Add("vcKitChildItems", GetType(String))
            dt.Columns.Add("vcInclusionDetail", GetType(String))
            dt.Columns.Add("numSelectedPromotionID", GetType(Long))
            dt.Columns.Add("bitDisablePromotion", GetType(Boolean))

            Dim drItem As DataRow

            If Not dtItems Is Nothing AndAlso dtItems.Rows.Count > 0 AndAlso CCommon.ToLong(radCmbCompany.SelectedValue) Then
                For Each dr As DataRow In dtItems.Rows
                    drItem = dt.NewRow()
                    drItem("numOppItemID") = CCommon.ToLong(dr("numoppitemtCode"))
                    drItem("numItemCode") = CCommon.ToLong(dr("numItemCode"))
                    drItem("numUnits") = CCommon.ToDouble(dr("numUnitHour")) * CCommon.ToDouble(dr("UOMConversionFactor"))
                    drItem("numWarehouseItemID") = CCommon.ToLong(dr("numWarehouseItmsID"))
                    drItem("monUnitPrice") = CCommon.ToDouble(dr("monPrice"))
                    drItem("fltDiscount") = CCommon.ToDouble(dr("fltDiscount"))
                    drItem("bitDiscountType") = CCommon.ToBool(dr("bitDiscountType"))
                    drItem("monTotalAmount") = CCommon.ToDouble(dr("monTotAmount"))
                    drItem("numPromotionID") = CCommon.ToLong(dr("numPromotionID"))
                    drItem("bitPromotionTriggered") = CCommon.ToBool(dr("bitPromotionTriggered"))
                    drItem("bitPromotionDiscount") = CCommon.ToBool(dr("bitPromotionDiscount"))
                    drItem("vcPromotionDescription") = CCommon.ToString(dr("vcPromotionDetail"))
                    drItem("vcKitChildItems") = CCommon.ToString(dr("KitChildItems"))
                    drItem("vcInclusionDetail") = CCommon.ToString(dr("InclusionDetail"))
                    drItem("numSelectedPromotionID") = CCommon.ToLong(dr("numSelectedPromotionID"))
                    drItem("bitDisablePromotion") = CCommon.ToBool(dr("bitDisablePromotion"))

                    If Not CCommon.ToBool(dr("bitDisablePromotion")) Then
                        dt.Rows.Add(drItem)
                    End If
                Next
            End If

            If isFromNewItem Then
                drItem = dt.NewRow()
                drItem("numOppItemID") = -999
                drItem("numItemCode") = CCommon.ToLong(hdnCurrentSelectedItem.Value)
                drItem("numUnits") = CCommon.ToDouble(txtUnits.Text) * CCommon.ToDouble(txtUOMConversionFactor.Text)
                If gvLocations.Rows.Count > 0 Then
                    For Each itemrow As GridViewRow In gvLocations.Rows
                        If CType(itemrow.FindControl("chkLocations"), CheckBox).Checked = True Then
                            drItem("numWarehouseItemID") = CCommon.ToLong(gvLocations.DataKeys(itemrow.RowIndex)("numWareHouseItemId"))
                            Exit For
                        End If
                    Next
                Else
                    drItem("numWarehouseItemID") = 0
                End If

                drItem("monUnitPrice") = CCommon.ToDouble(txtprice.Text)
                drItem("fltDiscount") = 0
                drItem("bitDiscountType") = 0
                drItem("monTotalAmount") = 0
                drItem("numPromotionID") = 0
                drItem("bitPromotionTriggered") = False
                drItem("bitPromotionDiscount") = 0
                drItem("vcPromotionDescription") = ""
                drItem("vcKitChildItems") = hdnKitChildItems.Value
                drItem("vcInclusionDetail") = ""
                drItem("numSelectedPromotionID") = 0
                drItem("bitDisablePromotion") = False
                dt.Rows.Add(drItem)
            End If

            Dim dsTempItems As New DataSet
            dsTempItems.Tables.Add(dt)

            Dim objPromotionOffer As New PromotionOffer
            objPromotionOffer.DomainID = CCommon.ToLong(Session("DomainID"))
            objPromotionOffer.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
            objPromotionOffer.CurrencyID = CCommon.ToLong(radcmbCurrency.SelectedValue)
            dt = objPromotionOffer.ApplyItemPromotionToOrder(dsTempItems.GetXml(), isBasedOnDiscountCode, vcDiscountCode)

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                If isFromNewItem Then
                    If dt.Select("numOppItemID=-999").Length > 0 Then
                        Dim drR As DataRow = dt.Select("numOppItemID=-999")(0)
                        txtprice.Text = CCommon.ToDecimal(drR("monUnitPrice"))
                        txtItemDiscount.Text = CCommon.ToDouble(drR("fltDiscount"))

                        If CCommon.ToBool(drR("bitDiscountType")) Then
                            radAmt.Checked = True
                            radPer.Checked = False
                        Else
                            radPer.Checked = True
                            radAmt.Checked = False
                        End If

                        ddlMarkupDiscountOption.SelectedValue = 0
                        chkUsePromotion.Checked = True
                        hdnNewItemPromotionID.Value = CCommon.ToLong(drR("numPromotionID"))
                    End If
                Else
                    For Each drItemR As DataRow In dt.Rows
                        If dtItems.Select("numoppitemtCode=" & drItemR("numOppItemID")).Length > 0 Then
                            Dim drR As DataRow = dtItems.Select("numoppitemtCode=" & drItemR("numOppItemID"))(0)
                            drR("monPrice") = CCommon.ToDecimal(drItemR("monUnitPrice"))
                            drR("monTotAmtBefDiscount") = drR("numUnitHour") * drR("UOMConversionFactor") * CCommon.ToDecimal(drItemR("monUnitPrice"))
                            drR("fltDiscount") = CCommon.ToDouble(drItemR("fltDiscount"))
                            drR("bitDiscountType") = CCommon.ToBool(drItemR("bitDiscountType"))
                            drR("numPromotionID") = CCommon.ToLong(drItemR("numPromotionID"))
                            drR("bitPromotionTriggered") = CCommon.ToBool(drItemR("bitPromotionTriggered"))
                            drR("bitPromotionDiscount") = CCommon.ToBool(drItemR("bitPromotionDiscount"))
                            drR("vcPromotionDetail") = CCommon.ToString(drItemR("vcPromotionDescription"))
                            drR("bitMarkupDiscount") = 0

                            If drR("bitDiscountType") = 0 Then
                                If CCommon.ToDouble(drR("fltDiscount")) = 0 Then
                                    drR("monTotAmount") = drR("numUnitHour") * drR("UOMConversionFactor") * drR("monPrice")
                                Else
                                    ' Markup,Discount logic change
                                    If (CCommon.ToString(drR("bitMarkupDiscount")) = "1") Then
                                        drR("monTotAmount") = drR("monTotAmtBefDiscount") + Math.Round(drR("monTotAmtBefDiscount") * drR("fltDiscount") / 100, 4)
                                    Else
                                        drR("monTotAmount") = drR("monTotAmtBefDiscount") - Math.Round(drR("monTotAmtBefDiscount") * drR("fltDiscount") / 100, 4)
                                    End If
                                End If
                            Else
                                If CCommon.ToDouble(drR("fltDiscount")) = 0 Then
                                    drR("monTotAmount") = drR("numUnitHour") * drR("UOMConversionFactor") * drR("monPrice")
                                Else
                                    ' Markup,Discount logic change
                                    If (CCommon.ToString(drR("bitMarkupDiscount")) = "1") Then
                                        drR("monTotAmount") = drR("monTotAmtBefDiscount") + drR("fltDiscount")
                                    Else
                                        drR("monTotAmount") = drR("monTotAmtBefDiscount") - drR("fltDiscount")
                                    End If
                                End If
                            End If
                        End If
                    Next
                End If
            ElseIf isBasedOnDiscountCode Then
                txtApplyCouponCode.Text = ""
                txtApplyCouponCode.Enabled = True
                btnapplyCouponCode.Text = "Apply Code"
                lblCouponMsg.Text = "Coupon code Is Not valid for added item(s)."
                lblErrroCouponMsg.Text = ""
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ClearCouponCodeItemPromotion(ByRef dtItems As DataTable)
        Try
            If Not dtItems Is Nothing AndAlso dtItems.Rows.Count > 0 Then
                Dim dt As New DataTable("Item")
                dt.Columns.Add("numOppItemID", GetType(Long))
                dt.Columns.Add("numItemCode", GetType(Long))
                dt.Columns.Add("numUnits", GetType(Double))
                dt.Columns.Add("numWarehouseItemID", GetType(Long))
                dt.Columns.Add("monUnitPrice", GetType(Double))
                dt.Columns.Add("fltDiscount", GetType(Double))
                dt.Columns.Add("bitDiscountType", GetType(Boolean))
                dt.Columns.Add("monTotalAmount", GetType(Double))
                dt.Columns.Add("numPromotionID", GetType(Long))
                dt.Columns.Add("bitPromotionTriggered", GetType(Boolean))
                dt.Columns.Add("bitPromotionDiscount", GetType(Boolean))
                dt.Columns.Add("vcPromotionDescription", GetType(String))
                dt.Columns.Add("vcKitChildItems", GetType(String))
                dt.Columns.Add("vcInclusionDetail", GetType(String))


                Dim drItem As DataRow

                For Each dr As DataRow In dtItems.Rows
                    drItem = dt.NewRow()
                    drItem("numOppItemID") = CCommon.ToLong(dr("numoppitemtCode"))
                    drItem("numItemCode") = CCommon.ToLong(dr("numItemCode"))
                    drItem("numUnits") = CCommon.ToDouble(dr("numUnitHour")) * CCommon.ToDouble(dr("UOMConversionFactor"))
                    drItem("numWarehouseItemID") = CCommon.ToLong(dr("numWarehouseItmsID"))
                    drItem("monUnitPrice") = CCommon.ToDouble(dr("monPrice"))
                    drItem("fltDiscount") = CCommon.ToDouble(dr("fltDiscount"))
                    drItem("bitDiscountType") = CCommon.ToBool(dr("bitDiscountType"))
                    drItem("monTotalAmount") = CCommon.ToDouble(dr("monTotAmount"))
                    drItem("numPromotionID") = CCommon.ToLong(dr("numPromotionID"))
                    drItem("bitPromotionTriggered") = CCommon.ToBool(dr("bitPromotionTriggered"))
                    drItem("bitPromotionDiscount") = CCommon.ToBool(dr("bitPromotionDiscount"))
                    drItem("vcPromotionDescription") = CCommon.ToString(dr("vcPromotionDetail"))
                    drItem("vcKitChildItems") = CCommon.ToString(dr("KitChildItems"))
                    drItem("vcInclusionDetail") = CCommon.ToString(dr("InclusionDetail"))
                    dt.Rows.Add(drItem)
                Next

                Dim dsTempItems As New DataSet
                dsTempItems.Tables.Add(dt)

                Dim objPromotionOffer As New PromotionOffer
                objPromotionOffer.DomainID = CCommon.ToLong(Session("DomainID"))
                objPromotionOffer.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                objPromotionOffer.PromotionID = CCommon.ToLong(hdnApplyCouponCode.Value)
                objPromotionOffer.CurrencyID = CCommon.ToLong(radcmbCurrency.SelectedValue)
                dt = objPromotionOffer.ClearCouponCodeItemPromotion(dsTempItems.GetXml())

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    For Each drItemR As DataRow In dt.Rows
                        If dtItems.Select("numoppitemtCode=" & drItemR("numOppItemID")).Length > 0 Then
                            Dim drR As DataRow = dtItems.Select("numoppitemtCode=" & drItemR("numOppItemID"))(0)
                            drR("monPrice") = CCommon.ToDecimal(drItemR("monUnitPrice"))
                            drR("monTotAmtBefDiscount") = drR("numUnitHour") * drR("UOMConversionFactor") * CCommon.ToDecimal(drItemR("monUnitPrice"))
                            drR("fltDiscount") = CCommon.ToDouble(drItemR("fltDiscount"))
                            drR("bitDiscountType") = CCommon.ToBool(drItemR("bitDiscountType"))
                            drR("numPromotionID") = CCommon.ToLong(drItemR("numPromotionID"))
                            drR("bitPromotionTriggered") = CCommon.ToBool(drItemR("bitPromotionTriggered"))
                            drR("bitPromotionDiscount") = CCommon.ToBool(drItemR("bitPromotionDiscount"))
                            drR("vcPromotionDetail") = CCommon.ToString(drItemR("vcPromotionDescription"))
                            drR("bitMarkupDiscount") = 0

                            If drR("bitDiscountType") = 0 Then
                                If CCommon.ToDouble(drR("fltDiscount")) = 0 Then
                                    drR("monTotAmount") = drR("numUnitHour") * drR("UOMConversionFactor") * drR("monPrice")
                                Else
                                    ' Markup,Discount logic change
                                    If (CCommon.ToString(drR("bitMarkupDiscount")) = "1") Then
                                        drR("monTotAmount") = drR("monTotAmtBefDiscount") + Math.Round(drR("monTotAmtBefDiscount") * drR("fltDiscount") / 100, 4)
                                    Else
                                        drR("monTotAmount") = drR("monTotAmtBefDiscount") - Math.Round(drR("monTotAmtBefDiscount") * drR("fltDiscount") / 100, 4)
                                    End If
                                End If
                            Else
                                If CCommon.ToDouble(drR("fltDiscount")) = 0 Then
                                    drR("monTotAmount") = drR("numUnitHour") * drR("UOMConversionFactor") * drR("monPrice")
                                Else
                                    ' Markup,Discount logic change
                                    If (CCommon.ToString(drR("bitMarkupDiscount")) = "1") Then
                                        drR("monTotAmount") = drR("monTotAmtBefDiscount") + drR("fltDiscount")
                                    Else
                                        drR("monTotAmount") = drR("monTotAmtBefDiscount") - drR("fltDiscount")
                                    End If

                                End If
                            End If
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub btnAddClose_Click(sender As Object, e As EventArgs)
        AddSimmilarItems()
        clearControls()
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "close", "ClosePreCheckoutWindow();", True)
        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideOtherTopSectionAfter1stItemAdded", "HideOtherTopSectionAfter1stItemAdded();FocuonItem();  $('.itemsArea').css('visibility', 'hidden');$('.itemsArea').css('display', 'none');", True)

    End Sub

    Protected Sub btnReplaceClose_Click(sender As Object, e As EventArgs)

        AddSimmilarItems()
        dsTemp = ViewState("SOItems")
        hdnApprovalActionTaken.Value = ""
        hdnClickedButton.Value = ""
        hdnIsUnitPriceApprovalRequired.Value = ""
        Dim dtItem As DataTable
        dtItem = dsTemp.Tables(0)
        For Each dgGridItem As DataGridItem In dgItems.Items
            Dim _numItemCode As Long = CCommon.ToLong(dgItems.DataKeys(dgGridItem.ItemIndex))
            If hdnCurrentSelectedItem.Value <> "" Then
                If _numItemCode = hdnCurrentSelectedItem.Value Then
                    If Not dtItem.Rows.Find(CType(dgGridItem.FindControl("lblOppItemCode"), Label).Text) Is Nothing Then
                        Dim decQtyShipped As Decimal = Math.Abs(CCommon.ToDecimal(CType(dgGridItem.FindControl("txtnumQtyShipped"), TextBox).Text))

                        If decQtyShipped > 0 Then
                            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ValidationDelete", "alert('You are not allowed to delete this item,since item has shipped qty.')", True)
                            Exit Sub
                        End If

                        'Code to check Child Items and delete along with Parent Item - START - Neelam
                        Dim dataItem As DataGridItem = CType(dgGridItem, DataGridItem)
                        Dim dataKeyNumItemCode As Long = CCommon.ToLong(dgItems.DataKeys(dataItem.ItemIndex))

                        dtItem.Rows.Find(CType(dgGridItem.FindControl("lblOppItemCode"), Label).Text).Delete()
                        RecalculateContainerQty(dtItem)
                        dtItem.AcceptChanges()

                        UseItemPromotionIfAvailable(dtItem, False, "")

                        dgItems.DataSource = dtItem
                        dgItems.DataBind()

                        If dtItem.Rows.Count = 0 Then
                            divShipping.Visible = False
                            radcmbLocation.Enabled = True
                            UpdatePanel1.Update()
                        End If
                        dtItem = dsTemp.Tables(0)
                        'dtItem.DefaultView.Sort = "vcItemName ASC"
                        dtItem = dtItem.DefaultView.ToTable()
                        dtItem.AcceptChanges()
                        Dim i As Long = 1
                        For Each dr As DataRow In dtItem.Rows
                            dr("numSortOrder") = i
                            i = i + 1
                            dr.AcceptChanges()
                        Next
                        Dim perserveChanges As Boolean = True
                        Dim msAction As System.Data.MissingSchemaAction = System.Data.MissingSchemaAction.Ignore

                        Dim changes As System.Data.DataTable = dsTemp.Tables(0).GetChanges()
                        If changes IsNot Nothing Then
                            changes.Merge(dtItem, perserveChanges, msAction)
                            dsTemp.Tables(0).Merge(changes, False, msAction)
                        Else
                            dsTemp.Tables(0).Merge(dtItem, False, msAction)
                        End If
                        litMessage.Text = ""
                        UpdateDetails()
                        clearControls()

                        plhItemDetails.Controls.Clear()
                        hdnCurrentSelectedItem.Value = ""
                        UpdatePanelItemDetials.Update()
                        UpdatePanelItems.Update()
                        If dtItem.Rows.Count <= 0 Then
                            dtRelatedItems = Nothing
                        End If


                        OrderPromotion()
                        ShippingPromotion()

                        clearControls()

                    End If
                    Exit For
                End If
            End If
        Next
        clearControls()
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "close", "ClosePreCheckoutWindow();", True)
        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideOtherTopSectionAfter1stItemAdded", "HideOtherTopSectionAfter1stItemAdded();FocuonItem();  $('.itemsArea').css('visibility', 'hidden');$('.itemsArea').css('display', 'none');", True)

    End Sub

    Protected Sub chkPickUpWillCall_CheckedChanged(sender As Object, e As EventArgs)
        If chkPickUpWillCall.Checked = True Then
            If radShipVia.Items.FindItemByText("Will-call") IsNot Nothing Then
                radShipVia.Items.FindItemByText("Will-call").Selected = True
            End If
            lblShipTo.Text = lblShipFrom.Text
            hdnShipToState.Value = hdnShipFromState.Value
            hdnShipToCountry.Value = hdnShipFromCountry.Value
            hdnShipToPostalCode.Value = hdnShipFromPostalCode.Value

            lblShipTo1.Text = lblShipTo.Text
            lblShipTo2.Text = ""
            hdnDropShpCountryName.Value = hdnShipFromCountry.Value
            hdnDropShpStateName.Value = hdnShipFromState.Value
            hdnDropShpPostal.Value = hdnShipFromPostalCode.Value
            dtLocationsGrid.Rows.Clear()
            Dim dr As DataRow
            dr = dtLocationsGrid.NewRow()
            If radcmbLocation.SelectedItem IsNot Nothing Then
                dr("FullAddress") = radcmbLocation.SelectedItem.Attributes("Address")
                dr("numAddressID") = radcmbLocation.SelectedItem.Attributes("numAddressID")
                hdnShipAddressID.Value = radcmbLocation.SelectedItem.Attributes("numAddressID")
            End If
            dtLocationsGrid.Rows.Add(dr)
            hdnShipVia.Value = radShipVia.SelectedValue
            hdnWillCallWarehouseID.Value = radcmbLocation.SelectedItem.Value
            For Each itemrow As GridViewRow In gvLocations.Rows
                Dim ShipToLocations As RadComboBox = CType(itemrow.FindControl("rdcmbShipToLocation"), RadComboBox)
                ShipToLocations.DataSource = dtLocationsGrid
                ShipToLocations.DataTextField = "FullAddress"
                ShipToLocations.DataValueField = "numAddressID"
                ShipToLocations.DataBind()
            Next
            Dim dtShippingMethod As DataTable
            If ViewState("dtShippingMethod") Is Nothing Then
                Dim objRule As New ShippingRule()
                objRule.DomainID = CCommon.ToLong(Session("DomainID"))
                objRule.SiteID = -1
                objRule.DivisionID = CCommon.ToLong(txtDivID.Text)
                If CCommon.ToLong(hdnCountry.Value) <> 0 AndAlso
                   CCommon.ToLong(hdnState.Value) <> 0 Then
                    objRule.CountryID = CCommon.ToLong(hdnCountry.Value)
                    objRule.StateID = CCommon.ToLong(hdnState.Value)

                End If

                dtShippingMethod = objRule.GetShippingMethodForItem1()
                Dim newColumn As New System.Data.DataColumn("IsShippingRuleValid", GetType(System.Boolean))
                newColumn.DefaultValue = True
                dtShippingMethod.Columns.Add(newColumn)

                'Add this column to store it as a value field in the dropdown of ddlShippingMethod
                CCommon.AddColumnsToDataTable(dtShippingMethod, "vcServiceTypeID1")
                CCommon.AddColumnsToDataTable(dtShippingMethod, "TransitTime")
                CCommon.AddColumnsToDataTable(dtShippingMethod, "AnticipateDelivery")
                CCommon.AddColumnsToDataTable(dtShippingMethod, "ReleaseDate")
                CCommon.AddColumnsToDataTable(dtShippingMethod, "ExpectedDate")
                CCommon.AddColumnsToDataTable(dtShippingMethod, "ShippingAmount")
                CCommon.AddColumnsToDataTable(dtShippingMethod, "ShippingCarrierImage")
                dtShippingMethod.Columns.Add("monRate1", GetType(System.Decimal))
                ViewState("dtShippingMethod") = dtShippingMethod
            End If
            If ViewState("dtShippingMethod") IsNot Nothing Then
                dtShippingMethod = ViewState("dtShippingMethod")
                dtShippingMethod.Rows.Clear()
                Dim drShipping As DataRow
                drShipping = dtShippingMethod.NewRow
                drShipping("vcShippingCompanyName") = "Pickup Will-Call"
                drShipping("vcServiceName") = lblShipTo.Text
                drShipping("ShippingAmount") = 0
                dtShippingMethod.Rows.Add(drShipping)
                ViewState("dtShippingMethod") = dtShippingMethod
                grdShippingInformation.DataSource = dtShippingMethod
                grdShippingInformation.DataBind()
            End If
            UpdatePanel1.Update()
            UPMatrixAndKitAttributes.Update()
        Else
            hdnShipAddressID.Value = "0"
            hdnShipVia.Value = "0"
            hdnWillCallWarehouseID.Value = "0"
            If Not Session("DefaultWarehouse") Is Nothing AndAlso Not radcmbLocation.Items.FindItemByValue(Session("DefaultWarehouse").ToString()) Is Nothing Then
                radcmbLocation.Items.FindItemByValue(Session("DefaultWarehouse")).Selected = True
                lblShipFrom.Text = CCommon.ToString(radcmbLocation.Items.FindItemByValue(Session("DefaultWarehouse")).Attributes("Address"))
                hdnShipFromState.Value = CCommon.ToString(radcmbLocation.Items.FindItemByValue(Session("DefaultWarehouse")).Attributes("State"))
                hdnShipFromCountry.Value = CCommon.ToString(radcmbLocation.Items.FindItemByValue(Session("DefaultWarehouse")).Attributes("Country"))
                hdnShipFromPostalCode.Value = CCommon.ToString(radcmbLocation.Items.FindItemByValue(Session("DefaultWarehouse")).Attributes("PostalCode"))
            ElseIf radcmbLocation.Items.Count > 0 Then
                radcmbLocation.Items(0).Selected = True
                lblShipFrom.Text = CCommon.ToString(radcmbLocation.Items(0).Attributes("Address"))
                hdnShipFromState.Value = CCommon.ToString(radcmbLocation.Items(0).Attributes("State"))
                hdnShipFromCountry.Value = CCommon.ToString(radcmbLocation.Items(0).Attributes("Country"))
                hdnShipFromPostalCode.Value = CCommon.ToString(radcmbLocation.Items(0).Attributes("PostalCode"))
            End If

            UpdatePanel1.Update()
            Dim objContact As New CContacts
            objContact.AddressType = CContacts.enmAddressType.ShipTo

            objContact.DomainID = Session("DomainID")
            If (radCmbCompany.SelectedValue = "") Then
                objContact.RecordID = 0
            Else
                objContact.RecordID = radCmbCompany.SelectedValue
            End If
            objContact.AddresOf = CContacts.enmAddressOf.Organization
            objContact.byteMode = 2
            dtLocationsGrid = objContact.GetAddressDetail()
            If chkPickUpWillCall.Checked = True Then
                dtLocationsGrid.Rows.Clear()
                Dim dr As DataRow
                dr = dtLocationsGrid.NewRow()
                If radcmbLocation.SelectedItem IsNot Nothing Then
                    dr("FullAddress") = radcmbLocation.SelectedItem.Attributes("Address")
                    dr("numAddressID") = radcmbLocation.SelectedItem.Attributes("numAddressID")
                    dtLocationsGrid.Rows.Add(dr)
                End If
            End If
            For Each itemrow As GridViewRow In gvLocations.Rows
                Dim ShipToLocations As RadComboBox = CType(itemrow.FindControl("rdcmbShipToLocation"), RadComboBox)
                ShipToLocations.DataSource = dtLocationsGrid
                ShipToLocations.DataTextField = "FullAddress"
                ShipToLocations.DataValueField = "numAddressID"
                ShipToLocations.DataBind()
            Next
            FillExistingAddress(txtDivID.Text)
            UpdatePanel1.Update()
            UPMatrixAndKitAttributes.Update()
        End If
    End Sub

    Private Sub UpdateGridDataAndFooterFields()
        Try
            Try
                If dsTemp Is Nothing AndAlso ViewState("SOItems") IsNot Nothing Then
                    dsTemp = ViewState("SOItems")
                ElseIf dsTemp Is Nothing AndAlso ViewState("SOItems") Is Nothing Then
                    Exit Sub
                End If

                Dim dtAllItems As DataTable
                dtAllItems = dsTemp.Tables(0)

                If pageType = PageTypeEnum.Sales Then
                    Dim strTax As String = ""
                    Dim strTaxIds As String = ""

                    If divNewCustomer.Visible Then
                        objCommon = New CCommon
                        For Each Item As ListItem In chkTaxItems.Items
                            If Item.Selected = True Then

                                If Session("BaseTaxCalcOn") = 1 Then 'Base tax calculation on Shipping Address(2) or Billing Address(1) 
                                    strTax = strTax & objCommon.TaxPercentage(0, ddlBillCountry.SelectedValue, ddlBillState.SelectedValue, Session("DomainID"), Item.Value, txtBillCity.Text, txtBillPostal.Text, Session("BaseTaxOnArea"), Session("BaseTaxCalcOn")) & ","
                                Else
                                    strTax = strTax & objCommon.TaxPercentage(0, ddlShipCountry.SelectedValue, ddlShipState.SelectedValue, Session("DomainID"), Item.Value, txtShipCity.Text, txtShipPostal.Text, Session("BaseTaxOnArea"), Session("BaseTaxCalcOn")) & ","
                                End If

                            Else
                                strTax = strTax & "0#1,"
                            End If
                            strTaxIds = strTaxIds & Item.Value & ","
                        Next
                        strTax = strTax.TrimEnd(",")
                        strTaxIds = strTaxIds.TrimEnd(",")
                        txtTax.Text = strTax
                        TaxItemsId.Value = strTaxIds

                    Else
                        If radCmbCompany.SelectedValue <> "" Then
                            objCommon = New CCommon
                            Dim dtTaxTypes As DataTable
                            Dim ObjTaxItems As New TaxDetails
                            ObjTaxItems.DomainID = Session("DomainID")
                            dtTaxTypes = ObjTaxItems.GetTaxItems
                            For Each dr As DataRow In dtTaxTypes.Rows

                                'Base tax calculation on Shipping Address(2) or Billing Address(1) 
                                strTax = strTax & objCommon.TaxPercentage(radCmbCompany.SelectedValue, 0, 0, Session("DomainID"), dr("numTaxItemID"), "", "", Session("BaseTaxOnArea"), Session("BaseTaxCalcOn")) & ","
                                strTaxIds = strTaxIds & dr("numTaxItemID") & ","

                            Next
                            strTax = strTax & objCommon.TaxPercentage(radCmbCompany.SelectedValue, 0, 0, Session("DomainID"), 0, "", "", Session("BaseTaxOnArea"), Session("BaseTaxCalcOn"))

                            strTaxIds = strTaxIds & "0"
                            txtTax.Text = strTax
                            TaxItemsId.Value = strTaxIds
                        Else
                            For k As Integer = 0 To chkTaxItems.Items.Count - 1
                                strTax = strTax & "0#1" & ","
                            Next
                            txtTax.Text = strTax
                        End If
                    End If
                End If

                If Not dtAllItems Is Nothing AndAlso dtAllItems.Rows.Count > 0 AndAlso pageType = PageTypeEnum.Sales Then
                    Dim strTaxes(), strTaxApplicable() As String
                    strTaxes = txtTax.Text.Split(",")
                    strTaxApplicable = Taxable.Value.Split(",")

                    For Each drRow As DataRow In dtAllItems.Rows

                        drRow("TotalTax") = 0

                        For k = 0 To chkTaxItems.Items.Count - 1
                            If dtAllItems.Columns.Contains("bitTaxable" & chkTaxItems.Items(k).Value) Then
                                If IIf(IsDBNull(drRow("bitTaxable" & chkTaxItems.Items(k).Value)), False, drRow.Item("bitTaxable" & chkTaxItems.Items(k).Value)) = True Then
                                    Dim decTaxValue As Decimal = CCommon.ToDecimal(strTaxes(k).Split("#")(0))
                                    Dim tintTaxType As Short = CCommon.ToShort(strTaxes(k).Split("#")(1))

                                    If tintTaxType = 2 Then 'FLAT AMOUNT
                                        drRow("Tax" & chkTaxItems.Items(k).Value) = decTaxValue * (drRow("numUnitHour") * drRow("UOMConversionFactor"))
                                    Else 'PERCENTAGE
                                        drRow("Tax" & chkTaxItems.Items(k).Value) = IIf(IsDBNull(drRow("monTotAmount")), 0, drRow("monTotAmount")) * decTaxValue / 100
                                    End If

                                    drRow("TotalTax") = CCommon.ToDecimal(drRow("TotalTax")) + CCommon.ToDecimal(drRow("Tax" & chkTaxItems.Items(k).Value))
                                End If
                            End If
                        Next
                    Next
                End If

                RecalculateContainerQty(dtAllItems)

                dgItems.DataSource = dtAllItems
                dgItems.DataBind()

                If dtAllItems.Rows.Count > 0 Then
                    Dim decGrandTotal As Decimal
                    Dim discount As Decimal
                    If dtAllItems.Columns.Contains("TotalDiscountAmount") Then
                        discount = IIf(IsDBNull(dtAllItems.Compute("sum(TotalDiscountAmount)", "")), 0, dtAllItems.Compute("sum(TotalDiscountAmount)", ""))
                    End If
                    decGrandTotal = IIf(IsDBNull(dtAllItems.Compute("sum(monTotAmount)", "")), 0, dtAllItems.Compute("sum(monTotAmount)", ""))
                    CType(dgItems.Controls(0).Controls(dtAllItems.Rows.Count + 1).FindControl("lblFTotal"), Label).Text = CCommon.GetDecimalFormat(Math.Truncate(10000 * decGrandTotal) / 10000)

                    If (pageType = PageTypeEnum.Sales) Then
                        For Each Item As ListItem In chkTaxItems.Items
                            Dim decTax As Decimal = IIf(IsDBNull(dtAllItems.Compute("sum(Tax" & Item.Value & " )", "")), 0, dtAllItems.Compute("sum(Tax" & Item.Value & " )", ""))
                            decGrandTotal = decGrandTotal + decTax

                        Next
                    End If

                    lblCouponDiscountAmount.Text = String.Format("{0:#,##0.00}", CCommon.ToDecimal(Session("TotalDiscount")))
                    lblShippingCost.Text = String.Format("{0:#,##0.00}", CCommon.ToDecimal(GetShippingCharge()))
                    lblTotal.Text = String.Format("{0:#,##0.00}", decGrandTotal - CCommon.ToDouble(Session("TotalDiscount")))
                    lblDiscountAmount.Text = String.Format("{0:#,##0.00}", discount)
                Else
                    lblTotal.Text = String.Format("{0:#,##0.00}", 0)
                    lblShippingCost.Text = String.Format("{0:#,##0.00}", 0)
                    lblCouponDiscountAmount.Text = String.Format("{0:#,##0.00}", 0)
                    lblDiscountAmount.Text = String.Format("{0:#,##0.00}", 0)
                End If

                If (Session("MultiCurrency") = True) And (pageType <> PageTypeEnum.AddEditOrder) Then
                    lblCurrencyTotal.Text = "Grand Total:"
                    lblCouponDiscount.Text = "Coupon Discount:"
                    lblShippingCharges.Text = "Shipping Charges:"
                    lblDiscountTitle.Text = "Discount:"

                    lblCouponCurrency.Text = IIf(radcmbCurrency.SelectedValue > 0, radcmbCurrency.SelectedItem.Text.Substring(0, 3), "")
                    lblShippingCostCurrency.Text = IIf(radcmbCurrency.SelectedValue > 0, radcmbCurrency.SelectedItem.Text.Substring(0, 3), "")
                    lblTotalCurrency.Text = IIf(radcmbCurrency.SelectedValue > 0, radcmbCurrency.SelectedItem.Text.Substring(0, 3), "")
                    lblDiscountCurrency.Text = IIf(radcmbCurrency.SelectedValue > 0, radcmbCurrency.SelectedItem.Text.Substring(0, 3), "")
                Else
                    lblCurrencyTotal.Text = "Grand Total:"
                    lblCouponDiscount.Text = "Coupon Discount:"
                    lblShippingCharges.Text = "Shipping Charges:"
                    lblDiscountTitle.Text = "Discount:"
                End If

                Dim totalWeight As Double = 0
                For Each dr As DataRow In dtAllItems.Rows
                    totalWeight = totalWeight + (CCommon.ToDouble(dr("fltItemWeight")) * CCommon.ToDouble(dr("numUnitHour")))
                Next

                txtTotalWeight.Text = totalWeight
            Catch ex As Exception
                Throw ex
            End Try
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub LoadItemPromotions(ByVal itemCode As Long)
        Try
            Dim objPromotionOffer As New PromotionOffer
            objPromotionOffer.DomainID = CCommon.ToLong(Session("DomainID"))
            objPromotionOffer.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
            objPromotionOffer.numItemCode = itemCode
            Dim dt As DataTable = objPromotionOffer.GetPromotionApplicableToItem()

            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                If dt.Rows.Count > 1 Then
                    divItemProm.Visible = False

                    rblItemPromo.Items.Clear()
                    rblItemPromo.DataSource = dt
                    rblItemPromo.DataTextField = "vcShortDesc"
                    rblItemPromo.DataValueField = "numProId"
                    rblItemPromo.DataBind()

                    rblItemPromo.Items(0).Selected = True

                    divItemPromMultiple.Visible = True
                Else
                    divItemProm.Visible = True
                    lblItemPromLongDesc.Text = CCommon.ToString(dt.Rows(0)("vcLongDesc"))
                End If
            Else
                RelatedItems(True)
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Protected Sub btnSaveCloseItemPromo_Click(sender As Object, e As EventArgs)
        Try
            hdnSelectedItemPromo.Value = CCommon.ToLong(rblItemPromo.SelectedValue)
            rblItemPromo.Items.Clear()
            divItemPromMultiple.Visible = False
            RelatedItems(True)
            txtUnits.Focus()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub grdShippingInformation_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdShippingInformation.RowCommand
        Try
            If e.CommandName = "AddShippingRate" Then
                If lngShippingItemCode = 0 Then
                    litMessage.Text = "Please first set Default Shipping Item from Administration->Global Settings->Accounting Tab->Shipping Service Item."
                    Exit Sub
                End If

                Dim isAddItem As Boolean = True
                Dim oppItemID As Long = 0

                Dim dsItems As DataSet = If(dsTemp Is Nothing, ViewState("SOItems"), dsTemp)


                Dim MaxRowOrder As Long
                MaxRowOrder = dsTemp.Tables(0).Rows.Count
                hdnCurrentSelectedItem.Value = lngShippingItemCode
                Session("ShippingItem") = lngShippingItemCode

                Dim description As String
                Dim decShipppingAmount As Decimal
                Dim canShippingFromPromotionApplied As Boolean = False
                Dim numPromotionID As Long = 0
                Dim bitTriggerredPromotion As String = "0"
                Dim vcPromotionDetail As String = ""
                Dim itemReleaseDate As Date = GetReleaseDate()

                If Not canShippingFromPromotionApplied Then
                    If chkUseCustomerShippingAccount.Checked = True Then
                        decShipppingAmount = 0
                        hdnShippingService.Value = "0"
                        description = "Shipping will be cost to account # " & CCommon.ToString(hdnShipperAccountNo.Value)
                    ElseIf chkPickUpWillCall.Checked = True Then
                        Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, Control).NamingContainer, GridViewRow)
                        decShipppingAmount = CCommon.ToDouble(DirectCast(row.FindControl("hdnShipRate"), HiddenField).Value)
                        hdnShippingService.Value = "0"
                        description = CCommon.ToString(DirectCast(row.FindControl("hdnShipServiceName"), HiddenField).Value)
                    Else
                        Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, Control).NamingContainer, GridViewRow)
                        decShipppingAmount = CCommon.ToDouble(DirectCast(row.FindControl("hdnShipRate"), HiddenField).Value)
                        hdnShipVia.Value = CCommon.ToLong(DirectCast(row.FindControl("hdnShipViaID"), HiddenField).Value)
                        hdnShippingService.Value = CCommon.ToLong(DirectCast(row.FindControl("hdnShipServiceID"), HiddenField).Value)
                        description = CCommon.ToString(DirectCast(row.FindControl("hdnShipServiceName"), HiddenField).Value)
                        itemReleaseDate = CCommon.ToString(DirectCast(row.FindControl("hdnReleaseDate"), HiddenField).Value)
                    End If
                End If

                If dsItems.Tables.Count > 0 AndAlso dsItems.Tables(0).Rows.Count > 0 Then
                    If dsItems.Tables(0).Select("numItemCode=" & lngShippingItemCode & " AND ItemReleaseDate='" & itemReleaseDate & "'").Length > 0 Then
                        oppItemID = dsItems.Tables(0).Select("numItemCode=" & lngShippingItemCode & " AND ItemReleaseDate='" & itemReleaseDate & "'")(0)("numoppitemtCode")
                        isAddItem = False
                    End If
                End If

                If Not ViewState("UsedPromotion") Is Nothing AndAlso Not DirectCast(ViewState("UsedPromotion"), DataTable).Select("vcShippingDescription <> ''").FirstOrDefault() Is Nothing Then
                    Dim dr As DataRow

                    For Each drPromotion As DataRow In DirectCast(ViewState("UsedPromotion"), DataTable).Rows
                        If Not String.IsNullOrEmpty(drPromotion("vcShippingDescription")) Then
                            dr = drPromotion
                            Exit For
                        End If
                    Next

                    If CCommon.ToBool(dr("bitFreeShiping")) AndAlso CCommon.ToLong(dr("numFreeShippingCountry")) = hdnCountry.Value AndAlso CCommon.ToDecimal(lblTotal.Text) >= CCommon.ToDecimal(dr("monFreeShippingOrderAmount")) Then
                        canShippingFromPromotionApplied = True
                        decShipppingAmount = 0
                        numPromotionID = CCommon.ToLong(dr("numProId"))
                        vcPromotionDetail = CCommon.ToString(dr("vcShippingDescription"))
                        description = "Promotion Shipping"
                        hdnShippingService.Value = "0"
                    ElseIf CCommon.ToBool(dr("bitFixShipping2")) AndAlso CCommon.ToDecimal(lblTotal.Text) >= CCommon.ToDecimal(dr("monFixShipping2OrderAmount")) Then
                        canShippingFromPromotionApplied = True
                        decShipppingAmount = CCommon.ToDecimal(dr("monFixShipping2Charge"))
                        numPromotionID = CCommon.ToLong(dr("numProId"))
                        vcPromotionDetail = CCommon.ToString(dr("vcShippingDescription"))
                        description = "Promotion Shipping"
                        hdnShippingService.Value = "0"
                    ElseIf CCommon.ToBool(dr("bitFixShipping1")) AndAlso CCommon.ToDecimal(lblTotal.Text) >= CCommon.ToDecimal(dr("monFixShipping1OrderAmount")) Then
                        canShippingFromPromotionApplied = True
                        decShipppingAmount = CCommon.ToDecimal(dr("monFixShipping1Charge"))
                        numPromotionID = CCommon.ToLong(dr("numProId"))
                        vcPromotionDetail = CCommon.ToString(dr("vcShippingDescription"))
                        description = "Promotion Shipping"
                        hdnShippingService.Value = "0"
                    End If
                End If



                Dim objItem As New CItems
                objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                objItem.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                objItem.byteMode = CCommon.ToShort(oppType)
                objItem.ItemCode = lngShippingItemCode
                objItem.WareHouseItemID = 0
                objItem.GetItemAndWarehouseDetails()
                MaxRowOrder = MaxRowOrder + 1
                objItem.numSortOrder = MaxRowOrder
                If chkPickUpWillCall.Checked = True Then
                    objItem.ItemName = "Pickup Will-Call : " & description
                End If

                ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon,
                                                                               dsTemp,
                                                                               isAddItem,
                                                                               objItem.ItemType,
                                                                               "",
                                                                               False,
                                                                               objItem.KitParent,
                                                                               objItem.ItemCode,
                                                                               1,
                                                                               decShipppingAmount,
                                                                               description, objItem.WareHouseItemID, objItem.ItemName, "", oppItemID, objItem.ModelID, 0, "-",
                                                                               1,
                                                                               pageType.ToString(), True, 0, "", txtTax.Text, txtCustomerPartNo.Text, chkTaxItems,
                                                                               numProjectID:=CCommon.ToLong(GetQueryStringVal("Source")),
                                                                               numProjectStageID:=CCommon.ToLong(GetQueryStringVal("StageID")),
                                                                               vcBaseUOMName:=hdnBaseUOMName.Value,
                                                                               objItem:=objItem, primaryVendorCost:=0, salePrice:=decShipppingAmount, numPromotionID:=numPromotionID,
                                                                               IsPromotionTriggered:=False,
                                                                               vcPromotionDetail:=vcPromotionDetail, numSortOrder:=objItem.numSortOrder, itemReleaseDate:=itemReleaseDate)

                UpdateDetails()
                UpdatePanelItems.Update()
            End If


        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Function GetReleaseDate() As Date
        Try
            Dim strDueDate As String = ""

            If Not String.IsNullOrEmpty(hdnReleaseDateControlID.Value) Then
                Dim BizCalendar As UserControl = CType(plhOrderDetail.FindControl(hdnReleaseDateControlID.Value), UserControl)

                If Not BizCalendar Is Nothing Then
                    Dim _myControlType As Type = BizCalendar.GetType()
                    Dim _myUC_DueDate As Reflection.PropertyInfo = _myControlType.GetProperty("SelectedDate")
                    If Not _myUC_DueDate.GetValue(BizCalendar, Nothing) Is Nothing Then
                        strDueDate = _myUC_DueDate.GetValue(BizCalendar, Nothing).ToString
                        If strDueDate <> "" Then
                            Return CDate(strDueDate)
                        End If
                    End If
                End If
            End If

            If strDueDate = "" Then
                Return DateTime.UtcNow.AddMinutes(CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")) * -1).Date
            Else
                Return CDate(strDueDate)
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function

    Protected Sub lkbAssemblyDetail_Click(sender As Object, e As EventArgs) Handles lkbAssemblyDetail.Click
        Try
            If CCommon.ToLong(hdnCurrentSelectedItem.Value) > 0 Then
                Dim objItem As New CItems
                objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                objItem.ItemCode = CCommon.ToLong(hdnCurrentSelectedItem.Value)
                objItem.WareHouseItemID = CCommon.ToLong(radWareHouse.SelectedValue)
                Dim ds As DataSet = objItem.GetAssemblyDetail()

                If Not ds Is Nothing Then
                    If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        lblAssemblyBuildQty.Text = CCommon.ToString(ds.Tables(0).Rows(0)("numMaxWOQty"))
                        lblAssemblyListPrice.Text = String.Format(CCommon.GetDataFormatStringWithCurrency(), CCommon.ToDouble(ds.Tables(0).Rows(0)("monPrice")))
                    Else
                        lblAssemblyBuildQty.Text = ""
                        lblAssemblyListPrice.Text = ""
                    End If

                    If ds.Tables.Count > 1 Then
                        rptAssembltParts.DataSource = ds.Tables(1)
                        rptAssembltParts.DataBind()
                    End If
                Else
                    lblAssemblyShipDate.Text = ""
                    lblAssemblyBuildQty.Text = ""
                    lblAssemblyListPrice.Text = ""
                    rptAssembltParts.DataSource = Nothing
                    rptAssembltParts.DataBind()
                End If


                objItem.StartDate = DateTime.UtcNow
                objItem.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objItem.Quantity = CCommon.ToDouble(txtWOQty.Text)
                If gvLocations.Rows.Count > 0 Then
                    objItem.WarehouseID = CCommon.ToLong(DirectCast(gvLocations.Rows(0).FindControl("hdnWareHouseID"), HiddenField).Value)
                Else
                    objItem.WarehouseID = 0
                End If

                Dim dt As DataTable = objItem.GetAssemblyMfgTimeCost()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    lblAssemblyShipDate.Text = Convert.ToDateTime(dt.Rows(0)("dtProjectedFinishDate")).ToString(CCommon.GetValidationDateFormat())
                    lblMaterialCost.Text = String.Format(CCommon.GetDataFormatStringWithCurrency(), CCommon.ToDouble(dt.Rows(0)("monMaterialCost")))
                    lblOverheadCost.Text = String.Format(CCommon.GetDataFormatStringWithCurrency(), CCommon.ToDouble(dt.Rows(0)("monMFGOverhead")))
                    lblLabourCost.Text = String.Format(CCommon.GetDataFormatStringWithCurrency(), CCommon.ToDouble(dt.Rows(0)("monLaborCost")))
                    lblUnitTotalCost.Text = String.Format(CCommon.GetDataFormatStringWithCurrency(), CCommon.ToDouble(dt.Rows(0)("monMaterialCost")) + CCommon.ToDouble(dt.Rows(0)("monMFGOverhead")) + CCommon.ToDouble(dt.Rows(0)("monLaborCost"))) & " / " & String.Format(CCommon.GetDataFormatStringWithCurrency(), (CCommon.ToDouble(dt.Rows(0)("monMaterialCost")) + CCommon.ToDouble(dt.Rows(0)("monMFGOverhead")) + CCommon.ToDouble(dt.Rows(0)("monLaborCost"))) * CCommon.ToDouble(txtUnits.Text))
                Else
                    lblAssemblyShipDate.Text = ""
                    lblMaterialCost.Text = ""
                    lblOverheadCost.Text = ""
                    lblLabourCost.Text = ""
                    lblUnitTotalCost.Text = ""
                End If
            End If

            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "HideItemSearch", "$('[id$=divAssemblyDetail]').show();$('.itemsArea').css('visibility', 'visible');$('.itemsArea').css('display', 'flow-root');", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

End Class