﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/SalesOrder.Master" CodeBehind="frmNewOrder.aspx.vb" Inherits=".frmNewOrder" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Import Namespace="BACRM.BusinessLogic.Common" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script type='text/javascript' src="../JavaScript/select2.js"></script>
    <script src="../JavaScript/Orders.js" type="text/javascript"></script>
    <script src="../JavaScript/Common.js" type="text/javascript"></script>
    <script type="text/javascript" src="../JavaScript/jquery.validate.js"></script>

    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script src="../javascript/slick.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../javascript/CustomFieldValidation.js"></script>
    <script type="text/javascript" src="../javascript/comboClientSide.js"></script>
    <script type='text/javascript' src="../JavaScript/biz.kitswithchildkits.js"></script>
    <script src="../JavaScript/CommonInlineEdit.js" type="text/javascript"></script>
    <script src="../JavaScript/GooglePlace.js" type="text/javascript"></script>
    <script src="../javascript/Validation.js" type="text/javascript"></script>
    <link rel="Stylesheet" href="../CSS/style.css" />
    <link rel="stylesheet" href="../CSS/select2.css" />
    <link rel="stylesheet" href="../CSS/jscal2.css" />
    <link rel="Stylesheet" href="../CSS/font-awesome.min.css" />
    <link rel="Stylesheet" href="../CSS/slick.css" />
    <link rel="Stylesheet" href="../CSS/slick-theme.css" />

    <script language="javascript" src="https://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyDKZ2Kph5NQXTnPc1xEzM4Bq0nhzXjONS0"></script>

    <style type="text/css">
        .resizeDiv{
            width:1000px;
        }
        .btn-xs {
            padding: 4px !important;
            font-size: 13px !important;
        }
        .hyperlink{
            text-decoration:underline !important;
            font-style:italic;
        }
        .btn-warning {
            background-color: #eca12f !important;
        }

        .nav-tabs {
            border-top: 4px solid #<%= BACRM.BusinessLogic.Common.CCommon.ToString(Session("vcThemeClass"))%>;
            background: #<%= BACRM.BusinessLogic.Common.CCommon.ToString(Session("vcThemeClass"))%>;
            border-radius: 4px;
            border-left: 4px solid #<%= BACRM.BusinessLogic.Common.CCommon.ToString(Session("vcThemeClass"))%>;
        }

        .nav-tabs {
            border-bottom: 1px solid #ddd;
        }

        .nav {
            padding-left: 0;
            margin-bottom: 0;
            list-style: none;
        }

        .nav-tabs > li {
            float: left;
            margin-bottom: -1px;
        }

        .nav > li {
            position: relative;
            display: block;
        }

        .nav-tabs > li.active > a {
            color: #<%= BACRM.BusinessLogic.Common.CCommon.ToString(Session("vcThemeClass"))%> !important;
        }

            .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover {
                color: #555;
                cursor: default;
                background-color: #fff;
                border: 1px solid #ddd;
                border-bottom-color: transparent;
            }

        .nav-tabs > li > a {
            color: #fff !important;
        }

        .nav-tabs > li > a {
            margin-right: 2px;
            line-height: 1.42857143;
            border: 1px solid transparent;
            border-radius: 4px 4px 0 0;
        }

        .nav > li > a {
            position: relative;
            display: block;
            padding: 10px 15px;
        }

        .tab-content {
            border: 1px solid #<%= BACRM.BusinessLogic.Common.CCommon.ToString(Session("vcThemeClass"))%>;
            border-top: 0px;
        }

        .table-bordered {
            border: 1px solid #e4e4e4;
        }

        .table-bordered {
            border: 1px solid #ddd;
        }

        .table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 20px;
        }

        table {
            background-color: transparent;
        }

        table {
            border-spacing: 0;
            border-collapse: collapse;
        }

        .table-striped > tbody > tr:nth-of-type(odd) {
            background-color: #f9f9f9;
        }

        .table-bordered > tbody > tr > th {
            border-bottom-width: 2px;
        }

        .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
            border: 1px solid #e4e4e4;
        }

        .table > tbody > tr > th, .table > thead > tr > td {
            background-color: #ebebeb;
            border-bottom: 2px solid #e4e4e4;
        }

        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            border-top: 1px solid #e4e4e4;
        }

        .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
            border: 1px solid #ddd;
        }

        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
            padding: 8px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        .table-bordered th {
            text-align: center;
        }

        .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
            border: 1px solid #e4e4e4;
        }

        .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
            border-top: 1px solid #e4e4e4;
        }

        .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
            border: 1px solid #ddd;
        }

        .table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
            padding: 8px;
            line-height: 1.42857143;
            vertical-align: top;
            border-top: 1px solid #ddd;
        }

        .CheckCoupon {
            background: url('../images/discounts-icon.png') 5px no-repeat, linear-gradient(to bottom, #cbd1e4 0%,#ffffff 2%,#cbd1e4 100%);
        }

        .panel-default > .panel-heading + .panel-collapse > .panel-body {
            padding: 0px !important;
        }

        label.valid {
            width: 24px;
            height: 24px;
            background: url(assets/img/valid.png) center center no-repeat;
            display: inline-block;
            text-indent: -9999px;
        }

        .checkbox label, .radio label {
            padding-left: 1px !important;
            margin-top: 3px;
        }

        /*.itemsArea {
            display: none;
            visibility: hidden;
        }*/

        #divItemGrid {
            /*visibility:hidden;*/
        }

        .cont1 {
            /*border: 0px !important;*/
        }

        label.error {
            font-weight: bold;
            color: red;
            padding: 2px 8px;
            margin-top: 2px;
        }

        .select2popup {
            width: 650px;
            height: 200px;
        }

        .RadComboBox {
            width: 100%;
            margin-top: 8px !important;
            vertical-align: middle;
        }

        .RadComboBox, .rcbInputCell, .rcbInput {
            width: 100%;
            background: none repeat scroll 0% 0% transparent;
            border: 0px none;
            vertical-align: middle;
            padding: 4px 0px 1px;
            outline: 0px none;
            margin: 0px !important;
        }

        .rcbSlide {
            padding-top: -2px;
            /*left:20% !important;*/
        }

        .multipleRowsColumns .rcbItem, .multipleRowsColumns .rcbHovered {
            float: left;
            margin: 0 1px;
            min-height: 13px;
            overflow: hidden;
            padding: 2px 19px 2px 6px;
        }

        .rcbHeader ul, .rcbFooter ul, .rcbItem ul, .rcbHovered ul, .rcbDisabled ul, .ulItems {
            width: 100%;
            display: inline-block;
            margin: 0;
            padding: 0;
            list-style-type: none;
        }

        .rcbList {
            display: inline-block;
            width: 100%;
            list-style-type: none;
        }


        #radWareHouse_DropDown {
            left: 34% !important;
        }

        .col1, .col2, .col3, .col4, .col5, .col6 {
            float: left;
            width: 80px;
            margin: 0;
            padding: 0 5px 0 0;
            line-height: 14px;
            display: block; /*border:0px solid red;*/
        }

        .col1 {
            width: 240px;
        }

        .col2 {
            width: 200px;
        }

        .col3 {
            width: 80px;
        }

        .col4 {
            width: 80px;
        }

        .col5 {
            width: 85px;
        }

        .col6 {
            width: 80px;
        }

        .dialog-header {
            position: relative;
            text-indent: 1em;
            height: 4em;
            line-height: 4em;
            background-color: #f7f7f7;
        }

        .dialog-body {
            padding: .5em;
            word-break: break-all;
            overflow-y: auto;
        }

        .RadComboBoxDropDown .rcbHeader, .RadComboBoxDropDown .rcbFooter {
            padding: 5px 7px 4px;
            border-width: 0;
            border-style: solid;
            background-image: none !important;
        }

        .KitChildItem .rcbItem, .KitChildItem .rcbHovered {
            padding: 4px !important;
            border-bottom: 1px solid #d8d8d8;
        }

            .KitChildItem .rcbItem .rcbCheckBox, .KitChildItem .rcbHovered .rcbCheckBox {
                margin: 4px !important;
            }

        .KitChildItem .rcbHeader {
            font-weight: bold;
            text-align: center;
        }

        .bigdrop {
            width: 600px !important;
        }

        .btn.btn-flat {
            border-radius: 0;
            -webkit-box-shadow: none;
            -moz-box-shadow: none;
            box-shadow: none;
            border-width: 1px;
            color: white;
            font-weight: bold;
        }

        .bg-purple {
            background-color: #605ca8 !important;
        }

        .bg-olive {
            background-color: #3d9970 !important;
        }

        .bg-orange {
            background-color: #ff851b !important;
        }

        .bg-aqua {
            background-color: #00c0ef !important;
        }

        .bg-maroon {
            background-color: #d81b60 !important;
        }

        .bg-silver {
            background-color: silver !important;
        }



        .btn {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
        }

        .gridItem td, .gridAlternateItem td {
            padding-top: 1px;
            padding-bottom: 1px;
            padding-left: 3px;
            padding-right: 3px;
        }

        .hidden-field, .hiddenColumn {
            display: none;
        }

        .td1 {
            width: 10%;
        }

        .td2 {
            vertical-align: middle;
            text-align: right;
            width: 60%;
        }

        .td3 {
            vertical-align: top;
            padding-left: 5px;
            width: 40%;
        }

        .middlealign {
            vertical-align: middle;
        }

        .hidecolumn {
            display: none;
        }

        .pager span {
            color: black;
            text-decoration: underline;
            font-size: 12pt;
        }

        .pager table {
            text-align: right;
            float: right;
        }

        .duplicateitem table td input {
            display: block;
            width: 20px !important;
            padding: 0px !important;
            margin: 0px !important;
            border: 1px solid #ccc;
            font-size: 12px;
            min-width: 20px;
            height: 20px !important;
        }

        .duplicateitem {
            height: 20px !important;
            width: 20px !important;
            border: 0px !important;
        }

        #divItemPromotionStatus .bizpagercustominfo {
            width: auto !important;
            line-height: 30px;
            padding-right: 5px;
            float: right;
        }

        #divItemPromotionStatus .bizgridpager {
            width: auto !important;
            float: right;
        }

        #divItemPromotionStatus .pagination {
            width: auto !important;
            margin: 0px !important;
        }

        .pager-shipping span, .pager-shipping a {
            font-size: 14px;
            font-weight: bold;
            padding: 5px;
        }

        table#grdShippingInformation td {
            padding: 3px;
        }

        #tblKitChild label {
            padding-left:5px;
            font-weight:normal;
        }


        #divKitChild  img.ul-child-kit-item-image{
            margin:0 auto;
        }

        #divKitChild  ul.ul-child-kit-item{
            font-size:16px;
        }

        #divKitChild  ul.ul-child-kit-item{
            font-size:16px;
        }

        #divKitChild  li.ul-child-kit-item-name {
            font-weight:bold;
        }

        #divKitChild .slick-track
        {
            display: flex !important;
        }

        #divKitChild .slick-item {
            background-color: #fff;
            border: 1px solid #dbdbdb;
            margin: 0 17px;
            height: inherit !important;
            outline:none;
        }

        #divKitChild .slick-item:hover, #divKitChild .slick-item:focus {
            background-color: #efefef;
            color: #282828;
            border: 1px solid #000;
        }

         #divKitChild .slick-item.active {
            background-color: #efefef;
            color: #282828;
            border: 1px solid #dbdbdb !important;
        }

        .slick-prev:before, .slick-next:before { font-family: FontAwesome; font-size: 20px; line-height: 1; color: #9e9e9e; opacity: 0.75; -webkit-font-smoothing: antialiased; -moz-osx-font-smoothing: grayscale; }   

        .slick-prev:before { content: "\f053"; }
        [dir="rtl"] .slick-prev:before { content: "\f054"; }

        .slick-next:before { content: "\f054"; }
        [dir="rtl"] .slick-next:before { content: "\f053"; }

        #chkUsePromotion{
            margin-left:10px;
            margin-top:10px;
        }
    </style>

    <script type="text/javascript">
        function getLocation(txt) {
            getAddressInfoByZip(txt.value);
        }

        function AddUpdate() {
            return validateFixedFields();
        }

        function OpenAmtPaid(a, b, c, d, e) {
            //var BalanceAmt;
            //BalanceAmt = 0;
            //if (document.getElementById('lblBalance') != null) {
            //    BalanceAmt = document.getElementById('lblBalance').innerHTML;
            //    BalanceAmt = BalanceAmt.replace(/,/g, "");
            //}
            //if (BalanceAmt == "")
            //    BalanceAmt = 0;

            if (e == "1") {
                opener.location.href = ('../opportunity/frmOpportunities.aspx?opId=' + b + '');
                self.close();
            } else {
                window.close();
                window.open('../opportunity/frmAmtPaid.aspx?Popup=1&OppBizId=' + a + '&OppId=' + b + '&DivId=' + c + '&PaymentId=' + d + '&IsNewOrder=1&IsPOS=' + $("#hdnPosPayment").val() + '&totalAmount=' + $("#lblTotal").text() + '', 'ReceivePayment', 'toolbar=no,titlebar=no,top=10,width=700,height=400,scrollbars=no,resizable=no');
            }
            return false;
        }

        function onRadComboBoxLoad(sender) {
            var div = sender.get_element();

            if (sender.get_id() != "radcmbUOM") {

                $telerik.$(div).bind('mouseenter', function () {
                    if (!sender.get_dropDownVisible()) {
                        sender.showDropDown();
                    }
                });

                $telerik.$(".RadComboBoxDropDown").mouseleave(function (e) {
                    hideDropDown("#" + sender.get_id(), sender, e);
                });

                $telerik.$(div).mouseleave(function (e) {
                    hideDropDown(".RadComboBoxDropDown", sender, e);
                });
            }
        }

        function hideDropDown(selector, combo, e) {
            var tgt = e.relatedTarget;
            var parent = $telerik.$(selector)[0];
            var parents = $telerik.$(tgt).parents(selector);

            if (tgt != parent && parents.length == 0) {
                if (combo.get_dropDownVisible())
                    combo.hideDropDown();
            }
        }

        $(document).ready(function () {
            if ($("#hdnCurrentSelectedItem").val() > 0) {
                $(".itemsArea").css("visibility", "visible");
                $(".itemsArea").css("display", "flow-root");
            } else {
                $(".itemsArea").css("visibility", "hidden");
                $(".itemsArea").css("display", "none");
            }

            $('#form1').validate(
                {
                    ignore: ":not(:visible)",
                    rules:
                    {
                        'ctl00$Content$radCmbCompany': { required: true },
                        'ctl00$Content$radcmbContact': { required: true },
                        'ctl00$Content$txtFirstName': { required: true },
                        'ctl00$Content$txtLastName': { required: true },
                        'ctl00$Content$radcmbRelationship': { required: true },
                        'ctl00$Content$radcmbCreditCardContact': {
                            required: {
                                depends: function (element) {
                                    if ($('#chkSaveClicked').is(":checked")) {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                }
                            }
                        },
                        'ctl00$Content$radcmbCardType': {
                            required: {
                                depends: function (element) {
                                    if ($('#chkSaveClicked').is(":checked")) {
                                        return true;
                                    }
                                    else {
                                        return false;
                                    }
                                }
                            }
                        }
                    },
                    messages:
                    {
                        'ctl00$Content$radCmbCompany': { required: 'Customer is required' },
                        'ctl00$Content$radcmbContact': { required: 'Contact is required' },
                        'ctl00$Content$txtFirstName': { required: 'First Name is required' },
                        'ctl00$Content$txtLastName': { required: 'Last Name is required' },
                        'ctl00$Content$radcmbRelationship': { required: 'Relationship is required' },
                        'ctl00$Content$radcmbCreditCardContact': { required: 'Please select contact to proceed with credit card payment' },
                        'ctl00$Content$radcmbCardType': { required: 'Please select card type' }
                    },
                    highlight: function (element) {
                        $(element).removeClass('success').addClass('error');
                    },
                    success: function (element) {
                        element.removeClass('error').addClass('success');
                    }
                }
            );
            CalculateTotal();
        });

        function showPaymentSection(value) {
            $("#CustomerSection").collapse('hide');
            $("#OrderSection").collapse('hide');
            $("#ItemSection").collapse('hide');
            $("#PaymentsSection").collapse('show');
            $("#hdnPosPayment").val(value);

            return false;

        }

        function showShippingSection(value) {
            $("#CustomerSection").collapse('hide');
            $("#OrderSection").collapse('hide');
            $("#ItemSection").collapse('hide');
            $("#PaymentsSection").collapse('show');

            return Save();

        }

        function hideUnitPriceDialog() {
            $("#dialogUnitPriceApproval").hide();
            return true;
        }

        function HideOtherTopSectionAfter1stItemAdded() {
            var lengthRows = $('table[id$=dgItems] > tbody > tr').not(':first').not(':last').length;

            if (lengthRows >= 1) {
                $("#CustomerSection").collapse('hide');
                $("#OrderSection").collapse('hide');
            }

            $("#divItem").show();
        }

        function HideItemDescription() {
            if ($("#hdnCurrentSelectedItem").val() > 0) {
                $(".itemsArea").css("visibility", "visible");
                $(".itemsArea").css("display", "flow-root");
                //var tmpStr = $("#txtUnits").val();
                $("#txtUnits").val('');
                //$("#txtUnits").val(tmpStr);
                $("#txtUnits").focus();
            } else {
                $(".itemsArea").css("visibility", "hidden");
                $(".itemsArea").css("display", "none");
            }
        }

        var columns;
        var userDefaultPageSize = '<%= Session("PagingRows")%>';
        var userDefaultCharSearch = '<%= Session("ChrForItemSearch") %>';
        var varPageSize = parseInt(userDefaultPageSize, 10);
        var varCharSearch = 1;
        if (parseInt(userDefaultCharSearch, 10) > 0) {
            varCharSearch = parseInt(userDefaultCharSearch, 10);
        }

        $(function () {
            Scroll(true);

            $("#btnCustomerCollapseButton").click(function () {
                $("#CustomerSection").collapse('toggle');
            })
            $("#btnOrderCollapseButton").click(function () {
                $("#OrderSection").collapse('toggle');
            })
            $("#btnItemCollapseButton").click(function () {
                $("#ItemSection").collapse('toggle');
            })
            $("#btnPaymentCollapseButton").click(function () {
                $("#PaymentsSection").collapse('toggle');
            })
            $(".btnBillme").click(function () {
                var totalAmount = $("#lblTotal").text(); lblTotal
                var RemainingCredit = $("#hdnRemaningCredit").val();
                if (parseFloat(totalAmount) > parseFloat(RemainingCredit)) {
                    alert("Your credit limit balance is not sufficient to place the order");
                    return false;
                }
            })
            

            $.ajax({
                type: "POST",
                url: '../common/Common.asmx/GetSearchedItems',
                data: '{ searchText: "abcxyz", pageIndex: 1, pageSize: 10, divisionId: 0, isGetDefaultColumn: true, warehouseID: 0, searchOrdCusHistory: 0, oppType: 1, isTransferTo: false,IsCustomerPartSearch:false,searchType:"1"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    // Replace the div's content with the page method's return.
                    if (data.hasOwnProperty("d"))
                        columns = $.parseJSON(data.d)
                    else
                        columns = $.parseJSON(data);
                },
                results: function (data) {
                    columns = $.parseJSON(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });

            

            $(document).bind('keypress', function (event) {
                if (event.which === 9 && event.ctrlKey) {
                    $("#ItemSection").collapse('show');
                    $("#txtItem").select2("open");
                    $("#txtItem").focus();
                    $("#CustomerSection").collapse('hide');
                    $("#OrderSection").collapse('hide');
                }
                //B
                if (event.which === 2 && event.ctrlKey) {
                    $("#CustomerSection").collapse('show');
                    var comboBox = $find("<%=radCmbCompany.ClientID %>");
                    var input = comboBox.get_inputDomElement();
                    input.focus();
                }
                if (event.which === 13) {
                    var obj = $('#txtItem').select2('data');

                    if (obj != null && $("#txtHidEditOppItem").val() != "") {
                        $("[id$=btnUpdate]").click();
                    }
                }
            });

            $('#txtItem').on("change", function (e) {
                ItemSelectionChanged(e.added);
            })

            $('#txtItem').on("select2-removed", function (e) {
                var isVisible = "<%=plhItemDetails.Visible%>";
                if (isVisible == "True") {
                    $("#hdnCurrentSelectedItem").val("");
                    $("#hdnHasKitAsChild").val("");
                    document.getElementById("lkbItemRemoved").click();
                }
            })

            $('#txtItem').select2(
                {
                    placeholder: 'Select Items',
                    minimumInputLength: varCharSearch,
                    multiple: false,
                    formatResult: formatItem,
                    width: "100%",
                    dropdownCssClass: 'bigdrop',
                    dataType: "json",
                    allowClear: true,
                    ajax: {
                        quietMillis: 500,
                        url: '../common/Common.asmx/GetSearchedItems',
                        type: 'POST',
                        params: {
                            contentType: 'application/json; charset=utf-8'
                        },
                        dataType: 'json',
                        data: function (term, page) {
                            var customerPart = $("#chkAutoCheckCustomerPart").prop("checked");
                            if (customerPart == undefined) {
                                customerPart = false;
                            }
                            return JSON.stringify({
                                searchText: term,
                                pageIndex: page,
                                pageSize: varPageSize,
                                divisionId: getDivisionId(),
                                isGetDefaultColumn: false,
                                warehouseID: getvalue(),
                                searchOrdCusHistory: 0,
                                oppType: 1,
                                isTransferTo: false,
                                IsCustomerPartSearch: customerPart,
                                searchType : $("#ddlSearchType").val()
                            });
                        },
                        results: function (data, page) {

                            if (data.hasOwnProperty("d")) {
                                if (data.d == "Session Expired") {
                                    alert("Session expired.");
                                    window.opener.location.href = window.opener.location.href;
                                    window.close();
                                } else {
                                    data = $.parseJSON(data.d)
                                }
                            }
                            else
                                data = $.parseJSON(data);

                            var more = (page.page * varPageSize) < data.Total;
                            return { results: $.parseJSON(data.results), more: more };
                        }
                    }
                });

            $('#txtChildItem').select2(
                {
                    placeholder: 'Select Item',
                    minimumInputLength: varCharSearch,
                    multiple: false,
                    formatResult: formatItem,
                    width: "200px",
                    dropdownCssClass: "bigdrop",
                    dataType: "json",
                    allowClear: true,
                    ajax: {
                        quietMillis: 500,
                        url: '../common/Common.asmx/GetKitChildsFirstLevel',
                        type: 'POST',
                        params: {
                            contentType: 'application/json; charset=utf-8'
                        },
                        dataType: 'json',
                        data: function (term, page) {
                            return JSON.stringify({
                                searchText: term,
                                pageIndex: page,
                                pageSize: varPageSize,
                                isGetDefaultColumn: false,
                                isAssembly: false,
                                itemCode: ($("[id$=hdnCurrentSelectedItem]").val() || 0)
                            });
                        },
                        results: function (data, page) {

                            if (data.hasOwnProperty("d")) {
                                if (data.d == "Session Expired") {
                                    alert("Session expired.");
                                    window.opener.location.href = window.opener.location.href;
                                    window.close();
                                } else {
                                    data = $.parseJSON(data.d)
                                }
                            }
                            else
                                data = $.parseJSON(data);

                            var more = (page.page * varPageSize) < data.Total;
                            return { results: $.parseJSON(data.results), more: more };
                        }
                    }
                });

            if ($("[id$=hdnSearchType]").val() != "") {
                $("#ddlSearchType").val($("[id$=hdnSearchType]").val());
            }
            $("#ddlSearchType").change(function () {
                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/SavePersistTable',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "isMasterPage": true
                        , "boolOnlyURL": false
                        , "strParam": "frmNewOrderSearchType"
                        , "strPageName": ""
                        , "values": "[{\"key\": \"frmNewOrderSearchType\",\"value\" : \"" + $("#ddlSearchType").val() + "\"}]"
                    }),
                    error: function (jqXHR, textStatus, errorThrown) {
                        alert("Unknown error occurred.");
                    }
                });
            });

            var cancelDropDownClosing = false;

            StopPropagation = function (e) {
                e.cancelBubble = true;
                if (e.stopPropagation) {
                    e.stopPropagation();
                }
            }
            onDropDownClosing = function (sender, args) {
                cancelDropDownClosing = false;
            }
        });

        function ItemSelectionChanged(item) {
            if ($("#divNewCustomer") == null && radCmbCompany.get_value() == null && radCmbCompany.get_value().length == 0) {
                alert("Select customer");
                return false;
            }
            if ($("#gvMultiSelectItems").is(':visible')) {
                if ($("#<%=gvMultiSelectItems.ClientID %> tr").length != 0) {

                    if (confirm('Before you can use the item autocomplete search, you first should add your multi-selected items or remove them from the select grid. Selecting' + "'OK'" + 'will remove your items.')) {
                        $("#gvMultiSelectItems").html("");
                        $("#divMultiSelect").hide();
                        FocuonItem();
                        return true;
                    }
                    else {
                        return false;
                    }
                }
            }
            if (item != null) {
                $("#hdnCurrentSelectedItem").val(item.id.toString().split("-")[0]);
                $("#hdnHasKitAsChild").val(item.bitHasKitAsChild);
                $("#hdnIsSKUMatch").val(Boolean(item.bitSKUMatch));
                $("#txtCustomerPartNo").val(item.CustomerPartNo);
                if (item.bitHasKitAsChild) {
                    Scroll(false);
                } else {
                    Scroll(true);
                }
                document.getElementById("lkbItemSelected").click();

                $("#CustomerSection").collapse('hide');
                $("#OrderSection").collapse('hide');
            } else {
                HideOtherTopSectionAfter1stItemAdded();
            }
        }

        function formatItem(row) {
            var numOfColumns = 0;
            var ui = "<table style=\"width: 100%;font-size:12px;\" cellpadding=\"0\" cellspacing=\"0\">";
            ui = ui + "<tbody>";
            ui = ui + "<tr>";
            ui = ui + "<td style=\"width:auto; padding: 3px; vertical-align:top; text-align:center\">";
            if (row["vcPathForTImage"] != null && row["vcPathForTImage"].indexOf('.') > -1) {
                ui = ui + "<img id=\"Img7\" height=\"45\" width=\"45\" title=\"Item Image\" src=\"" + row["vcPathForTImage"] + "\" alt=\"Item Image\" >";
            } else {
                ui = ui + "<img id=\"Img7\" height=\"45\" width=\"45\" title=\"Item Image\" src=\"" + "../images/icons/cart_large.png" + "\" alt=\"Item Image\" >";
            }

            ui = ui + "</td>";
            ui = ui + "<td style=\"width: 100%; vertical-align:top\">";
            ui = ui + "<table style=\"width: 100%;\" cellpadding=\"0\" cellspacing=\"0\"  >";
            ui = ui + "<tbody>";
            ui = ui + "<tr>";
            $.each(columns, function (index, column) {
                if (numOfColumns == 4) {
                    ui = ui + "</tr><tr>";
                    numOfColumns = 0;
                }

                if (numOfColumns == 0) {
                    ui = ui + "<td style=\"white-space: nowrap; margin-left:10px\"><strong>" + column.vcFieldName + ":</strong></td><td style=\"width:80%\">" + row[column.vcDbColumnName] + "</td>";
                }
                else {
                    ui = ui + "<td style=\"white-space: nowrap; margin-left:10px\"><strong>" + column.vcFieldName + ":</strong></td><td style=\"width:20%\">" + row[column.vcDbColumnName] + "</td>";
                }
                numOfColumns += 2;
            });
            ui = ui + "</tr>";
            ui = ui + "</tbody>";
            ui = ui + "</table>";
            ui = ui + "</td>";
            ui = ui + "</tr>";
            ui = ui + "</tbody>";
            ui = ui + "</table>";

            return ui;
        }

        function getvalue() {
            var combo = $find('<%=radcmbLocation.ClientID%>');
            if (combo != null) {
                return combo.get_selectedItem().get_value();
            }
            else {
                return 0;
            }
        }

        function getDivisionId() {
            var radCmbCompany = $find('radCmbCompany');

            if (radCmbCompany != null) {
                if (radCmbCompany.get_value() != null && radCmbCompany.get_value().length > 0) {
                    return radCmbCompany.get_value();
                }
                else {
                    return 0;
                }
            }
            else {
                return 0;
            }
        }

            function AddChildItem() {
                var obj = $('#txtChildItem').select2('data');

                if (obj == null) {
                    alert("Please Select Child Item")
                    return false;
                }

                $("[id$=hdnSelectedChildItem]").val(obj.id.toString().split("-")[0]);
                $("[id$=btnAddChilItem]").click();


                return false;
            }

            function CheckQuantityAdded() {
                if (parseFloat($("#txtUnits").val() || 0) == 0) {
                    alert("Enter quantity");
                    return false;
                }
                return true;
            }

            function DbClickUnitSalePrice(lblUnitSalePrice) {
                $(lblUnitSalePrice.parentElement).find("[id$='txtUnitSalePrice']").show().focus();
                $(lblUnitSalePrice).hide();
            }

            function HideSalePriceTextBox(txtUnitSalePrice) {
                $(txtUnitSalePrice.parentElement).find("[id$='lblUnitSalePrice']").show();
                $(txtUnitSalePrice).hide();
            }

            function FocuonItem() {
                $("#txtItem").select2("open");
                $("#txtItem").focus();
            }

            function AddItemToOrder(e) {
                var k;
                document.all ? k = e.keyCode : k = e.which;
                if (k === 13) {
                    $("[id$=btnUpdate]").click();
                }
            }

            function FocusPrice() {
                var tmpStr = $("#txtprice").val();
                $("#txtprice").val('');
                $("#txtprice").val(tmpStr);
                $("#txtprice").focus();
            }

            function FocusDiscount() {
                var tmpStr = $("#txtItemDiscount").val();
                $("#txtItemDiscount").val('');
                $("#txtItemDiscount").val(tmpStr);
                $("#txtItemDiscount").focus();
            }

            function UnitSalePriceChanged(txtUnitSalePrice) {

                var salePrice = parseFloat($(txtUnitSalePrice).val() || 0);

                var lblUnitSalePrice = $(txtUnitSalePrice.parentElement).find("[id$='lblUnitSalePrice']");

                if (lblUnitSalePrice.length > 0) {
                    lblUnitSalePrice.val($(txtUnitSalePrice).val());
                }

                var tr = $(txtUnitSalePrice.parentElement.parentElement)

                if (tr.length > 0) {
                    var txtUnits = $(tr).find("[id$=txtUnits]");
                    var txtUnitPrice = $(tr).find("[id$=txtUnitPrice]");
                    var txtDiscountType = $(tr).find("[id$=txtDiscountType]");
                    var txtTotalDiscount = $(tr).find("[id$=txtTotalDiscount]");
                    var hdnDiscountAmount = $(tr).find("[id$=hdnDiscountAmount]");

                    if (txtDiscountType.length > 0) {
                        txtDiscountType.attr("value", "False");
                    }

                    if (txtTotalDiscount.length > 0) {
                        var price = parseFloat(txtUnitPrice.val() || 0);

                        if (salePrice > price) {
                            txtTotalDiscount.attr("value", 0);
                            txtTotalDiscount.attr("value", 0);
                            hdnDiscountAmount.attr("value", 0);
                        } else {
                            var discount = parseFloat(((price - salePrice) / price * 10) * 100 / 10).toString();
                            hdnDiscountAmount.attr("value", ((discount / 100) * price).toPrecision(4) * (txtUnits.val() || 1));
                            txtTotalDiscount.attr("value", discount);
                        }
                    }
                }
                //DO NOT REMOVE FOLLOWING FUNCTION CALL
                CalculateTotal();
            }

            //This method removes the ending comma from a string.
            function removeLastComma(str) {
                return str.replace(/,$/, "");
            }

            function SetItemAttributes(itemcode, numWarehouseItemID, vcAttributes, vcAttributeIDs) {
                var item = $('#txtItem').select2('data');
                item.numItemCode = itemcode;
                item.numWareHouseItemID = numWarehouseItemID;
                item.bitHasKitAsChild = Boolean(false);
                item.kitChildItems = "";
                item.Attributes = vcAttributes;
                item.AttributeIDs = vcAttributeIDs;
                $('#txtItem').select2("data", item);
                $('#hdnTempSelectedItems').val(JSON.stringify($('#txtItem').select2('data')));
            }

            function GetSelectedItems(isFromAddButton) {
                if ($find('radCmbCompany') != null) {
                    if ($find('radCmbCompany').get_value() == "") {
                        alert("Select Customer")
                        return false;
                    }
                    if ($find('ctl00_Content_radcmbContact').get_value() == 0) {
                        alert("Select Contact")
                        return false;
                    }
                }
                else if ($("#divbulksales") == null) {
                    if (document.getElementById("txtFirstName").value == '') {
                        alert("Enter First Name")
                        document.getElementById("txtFirstName").focus();
                        return false;
                    }
                    if (document.getElementById("txtLastName").value == '') {
                        alert("Enter Last Name")
                        document.getElementById("txtLastName").focus();
                        return false;
                    }
                    if ($find('<%= radcmbRelationship.ClientID%>').get_value() == 0) {
                        alert("Select Relationship ")
                        document.getElementById("<%= radcmbRelationship.ClientID%>").focus();
                        return false;
                    }
                }

            if ($("#txtHidEditOppItem").val() == "") {
                var plannedStart = $find("<%=rdpPlannedStart.ClientID%>");
                var requestedFinish = $find("<%=radCalCompliationDate.ClientID%>");

                if ($("[id$=chkWorkOrder]").is(":checked") && plannedStart != null && plannedStart.get_selectedDate() == null) {
                    alert("Planned start date is required.")
                    plannedStart.focus();
                    return false;
                }

                if ($("[id$=chkWorkOrder]").is(":checked") && requestedFinish != null && requestedFinish.get_selectedDate() == null) {
                    alert("Requested finish date is required.")
                    requestedFinish.focus();
                    return false;
                }

                if ($("[id$=chkWorkOrder]").is(":checked") && plannedStart != null && requestedFinish != null && requestedFinish.get_selectedDate() <= plannedStart.get_selectedDate()) {
                    alert("Requested finish date must be atleast one day after planned start date.")
                    requestedFinish.focus();
                    return false;
                }
            }

            var obj = $('#txtItem').select2('data');
            var hdnFlagRelatedItems = $("#hdnFlagRelatedItems").val();
            if (obj == null && $("#txtHidEditOppItem").val() == "" && hdnFlagRelatedItems == "True") {
                alert("Please select item.");
                return false;
            }
            else {
                if (obj == null) {
                    return;
                }
                var item = new Object();
                item.numItemCode = obj.numItemCode;
                item.numWareHouseItemID = obj.numWareHouseItemID;
                item.bitHasKitAsChild = Boolean(obj.bitHasKitAsChild);
                item.kitChildItems = obj.childKitItems;
                item.numQuantity = obj.numQuantity || 1;
                item.vcAttributes = obj.Attributes || "";
                item.vcAttributeIDs = obj.AttributeIDs || "";
                $("#hdnSelectedItems").val(JSON.stringify(item));
                if (isFromAddButton) {
                    $("[id$=btnUpdate]").click();
                } else {
                    return true;
                }
            }
        }

        function UpdateSelectedItems(items) {
            $('#txtItem').select2("data", items);
        }

        function Check() {
            return 'Check method call';
        }

        function openItem(a, b, c, d, e) {
            if ($('#divExistingCustomerColumn1').length > 0) {
                if ($find('radCmbCompany').get_value() == "") {
                    c = 0
                }
                else {
                    c = $find('radCmbCompany').get_value();
                }
            }
            else {
                c = 0;
            }


            if ($ControlFind(b).value == "") {
                b = 0
            }
            else {
                b = $ControlFind(b).value
            }

            var CalPrice = 0;
            if ($ControlFind('hdKit').value == 'True' && ($ControlFind('dgKitItem') != null)) {
                jQuery('ctl00_Content_order1_dgKitItem tr').not(':first,:last').each(function () {
                    var str;
                    str = jQuery(this).find("[id*='lblUnitPrice']").text();
                    CalPrice = CalPrice + (parseFloat(str.split('/')[0]) * jQuery(this).find("input[id*='txtKitUits']").val());
                });
            }

            var WareHouseItemID = 0;
            if ($find('radWareHouse') != null) {
                if ($find('radWareHouse').get_value() == "") {
                    WareHouseItemID = 0
                }
                else {
                    WareHouseItemID = $find('radWareHouse').get_value()
                }
            }
            if (a != 0 && b != 0) {
                window.open('../opportunity/frmItemPriceRecommd.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=' + a + '&Unit=' + b + '&DivID=' + c + '&OppType=' + e + '&CalPrice=' + CalPrice + '&WarehouseItemID=' + WareHouseItemID, '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
            }
        }

        function Save(a) {
            if (!validateCustomFields()) {
                return false;
            }

            if ($("#divCreditCard").is(":visible")) {
                $("#chkSaveClicked").prop('checked', true);
            }

            if ($('#divExistingCustomerColumn1').length > 0) {
                if ($find('radCmbCompany').get_value() == "") {
                    alert("Select Customer")
                    return false;
                }
                if ($find('ctl00_Content_radcmbContact').get_value() == 0) {
                    alert("Select Contact")
                    return false;
                }
            }
            else {
                if (document.getElementById("txtFirstName").value == '') {
                    alert("Enter First Name")
                    document.getElementById("txtFirstName").focus();
                    return false;
                }
                if (document.getElementById("txtLastName").value == '') {
                    alert("Enter Last Name")
                    document.getElementById("txtLastName").focus();
                    return false;
                }
                if ($find('<%= radcmbRelationship.ClientID%>').get_value() == 0) {
                        alert("Select Relationship ")
                        document.getElementById("<%= radcmbRelationship.ClientID%>").focus();
                    return false;
                }
            }

            if ($('#chkSaveClicked').is(":checked")) {

                if ($find('ctl00_Content_radcmbCardType').get_value() == 0) {
                    alert("Please select card type")
                    return false;
                }
            }
        }

        function googleAddress() {
            if (document.getElementById("txtBillStreet"))
                initializeaddress("txtBillStreet", "txtBillStreet", "txtBillCity", "hdnBillState", "txtBillPostal", "ddlBillCountry");

            if (document.getElementById("txtShipStreet"))
                initializeshipaddress("txtShipStreet", "txtShipStreet", "txtShipCity", "hdnShipState", "txtShipPostal", "ddlShipCountry");
        }

        function pageLoad(sender, args) {
            initComboExtensions();
            cbCompany = $find("radCmbCompany");
            if (cbCompany != null) {
                cbCompany.onDataBound = cb_onDataBoundOrganization;
            }

            googleAddress();
        }

        function parseCurrency(num) {
            return parseFloat(num.replace(/,/g, ''));
        }

        function CalculateTotal() {

            var totalWeight;
            totalWeight = 0;
            jQuery('#ctl00_Content_dgItems > tbody > tr').not(':first,:last').each(function () {
                totalWeight = totalWeight + (parseFloat(jQuery(this).find("input[id*='txtUnits']").val() * parseFloat(jQuery(this).find("input[id*='txtUOMConversionFactor']").val())) * parseFloat(jQuery(this).find("input[id*='txtItemWeight']").val()));
            });
            console.log(totalWeight);
            $('#lblTotalWeight').text(totalWeight + ' lbs');
            $('#hdnTotalWeight').val(totalWeight);

            var unitPrice;
            var Total, subTotal, TaxAmount, ShipAmt, IndTaxAmt, discount, totalDiscount = 0;
            var index, findex;
            Total = 0;
            TaxAmount = 0;
            subTotal = 0;
            IndTaxAmt = 0;
            var strtaxItemIDs = "";
            var strTax = "";
            if ($ControlFind('TaxItemsId') != null) {
                strtaxItemIDs = $ControlFind('TaxItemsId').value.split(',')
            }
            if ($ControlFind('txtTax') != null) {
                strTax = $ControlFind('txtTax').value.split(',')
            }
            ShipAmt = 0;
            $('#hdnTotalWeight').val();
            jQuery('#ctl00_Content_dgItems > tbody > tr').not(':first,:last').each(function () {

                if (parseFloat(jQuery(this).find("input[id*='txtUnits']").val()) <= 0) {
                    alert("Units must greater than 0.")
                    jQuery(this).find("input[id*='txtUnits']").focus();
                    return false;
                }

                var unitPrice = parseCurrency(jQuery(this).find("input[id*='txtUnitPrice']").val());
                var uomConversionFactor = parseCurrency(jQuery(this).find("input[id*='txtUOMConversionFactor']").val()) || 1;
                var units = parseCurrency(jQuery(this).find("input[id*='txtUnits']").val());
                var txtDiscountTypeVal = jQuery(this).find("input[id*='txtDiscountType']").val();
                var txtTotalDiscountVal = parseCurrency(jQuery(this).find("input[id*='txtTotalDiscount']").val());
                var hdnMarkupDiscountVal = jQuery(this).find("input[id*='hdnMarkupDiscount']").val();

                unitSalePrice = 0;

                //WHEN Item Level UOM is Enabled we are displying total price in unitprice textbox
                //So for exammple, if 1 6Packs = 1 Can and order uom is 6 Packs then unit price is multiplied by 6.
                //to get unit price of single unit we have to divide by conversion factor
                if ($("#hdnEnabledItemLevelUOM").val() == "True") {
                    unitPrice = parseCurrency(unitPrice / uomConversionFactor);
                }

                jQuery(this).find("input[id*='hdnUnitPrice']").val(unitPrice);

                unitPrice = parseCurrency(jQuery(this).find("input[id*='hdnUnitPrice']").val());
                subTotal = (units * uomConversionFactor) * parseCurrency(jQuery(this).find("input[id*='hdnUnitPrice']").val());

                if (txtDiscountTypeVal == 'True') {

                    if (hdnMarkupDiscountVal == "1") {
                        subTotal = subTotal + txtTotalDiscountVal;
                    }
                    else {
                        subTotal = subTotal - txtTotalDiscountVal;
                    }

                    unitSalePrice = subTotal / parseFloat(units);

                    jQuery(this).find("[id*='lblUnitSalePrice']").text(parseFloat((unitSalePrice || 0).toFixed(4)));
                    jQuery(this).find("[id*='txtUnitSalePrice']").text(parseFloat((unitSalePrice || 0)));

                    //but for saving in database we are storing actuls unit sale price of single unit
                    jQuery(this).find("[id*='hdnUnitSalePrice']").val(parseFloat(unitSalePrice / (uomConversionFactor || 1) || 0));

                    totalDiscount = totalDiscount + txtTotalDiscountVal;
                }
                else {
                    unitSalePrice = unitPrice - (unitPrice * (txtTotalDiscountVal || 0)) / 100;
                    discount = (subTotal * (txtTotalDiscountVal || 0)) / 100;

                    if (hdnMarkupDiscountVal == "1") {
                        subTotal = subTotal + discount;
                    }
                    else {
                        subTotal = subTotal - discount;
                    }

                    totalDiscount = totalDiscount + discount;

                    //While displaying values we are multiplying it by UOM conversion factor
                    if ($("#hdnEnabledItemLevelUOM").val() == "True") {
                        jQuery(this).find("[id*='lblUnitSalePrice']").text(parseFloat((unitSalePrice || 0) * uomConversionFactor).toFixed(4));
                        jQuery(this).find("[id*='txtUnitSalePrice']").text(parseFloat((unitSalePrice || 0) * uomConversionFactor).toFixed(4));
                    } else {
                        jQuery(this).find("[id*='lblUnitSalePrice']").text(parseFloat((unitSalePrice || 0).toFixed(4)));
                        jQuery(this).find("[id*='txtUnitSalePrice']").text(parseFloat((unitSalePrice || 0).toFixed(4)));
                    }

                    //but for saving in database we are storing actuls unit sale price of single unit
                    //DO NOT REMOVE THIS LINE
                    jQuery(this).find("[id*='hdnUnitSalePrice']").val(parseFloat(unitSalePrice || 0));

                    jQuery(this).find("[id*='lblDiscount']").text(CommaFormatted(parseFloat((discount || 0).toFixed(4))) + ' (' + (txtTotalDiscountVal || 0) + '%)');
                }

                jQuery(this).find("[id*='lblTotal']").text(CommaFormatted(parseFloat((subTotal || 0).toFixed(4))));

                Total = parseFloat(Total) + parseFloat(subTotal);
                if (strtaxItemIDs.length > 0) {
                    for (k = 0; k < strtaxItemIDs.length; k++) {
                        if (jQuery(this).find("[id*='lblTaxable" + strtaxItemIDs[k] + "']").text() != 'False') {
                            jQuery(this).find("[id*='lblTaxAmt" + strtaxItemIDs[k] + "']").text(parseFloat((strTax[k] * parseFloat(subTotal) / 100).toFixed(4)));
                            TaxAmount = parseFloat(TaxAmount) + parseFloat(jQuery(this).find("[id*='lblTaxAmt" + strtaxItemIDs[k] + "']").text());
                        }
                    }
                }
            });


            jQuery('#ctl00_Content_dgItems > tbody > tr:last').find("[id*='lblFTotal']").text(CommaFormatted(parseFloat((Total + parseFloat(TaxAmount || 0) + parseFloat(ShipAmt || 0)).toFixed(4))));


            if (jQuery('#ctl00_Content_dgItems > tbody > tr').length > 2) {
                jQuery('#lblTotal').text(CommaFormatted((Total + parseFloat(TaxAmount || 0) + parseFloat(ShipAmt || 0)).toFixed(4)));
            }

            jQuery('#lblDiscountAmount').text(CommaFormatted(parseFloat((totalDiscount || 0).toFixed(4))));
            if (strtaxItemIDs.length > 0) {
                for (k = 0; k < strtaxItemIDs.length; k++) {
                    jQuery('#ctl00_Content_dgItems > tbody > tr').not(':first,:last').each(function () {
                        IndTaxAmt = IndTaxAmt + parseFloat(jQuery(this).find("[id*='lblTaxAmt" + strtaxItemIDs[k] + "']").text())
                    });

                    jQuery('#ctl00_Content_dgItems > tbody > tr:last').find("[id*='lblFTaxAmt" + strtaxItemIDs[k] + "']").text(CommaFormatted(roundNumber(IndTaxAmt, 2)));
                    IndTaxAmt = 0;
                }
            }
        }

        function OpenAddressWindow(sAddressType) {
            var radValue = $find('radCmbCompany').get_value();
            var AddressID = 0;
            if (sAddressType == "BillTo") {
                AddressID = document.getElementById('hdnBillAddressID').value;
            } else {
                AddressID = document.getElementById('hdnShipAddressID').value;
            }

            window.open('frmChangeDefaultAdd.aspx?AddressType=' + sAddressType + '&AddressID=' + AddressID  + '&CompanyId=' + radValue + "&CompanyName=" + $find('radCmbCompany').get_text(), '', 'toolbar=no,titlebar=no,left=200, top=300,width=900,height=400,scrollbars=no,resizable=no');
            return false;
        }

        function OpenChangeProfitWindow() {
            if ($('#hlProfitPerc').attr("disabled") != "disabled") {
                window.open('frmChangeGrossProfit.aspx?Percent=' + document.getElementById('hlProfitPerc').innerHTML, '', 'toolbar=no,titlebar=no,left=200, top=300,height=0,scrollbars=no,resizable=no');
            }
            return false;
        }

        function FillModifiedAddress(AddressType, AddressID, FullAddress, warehouseID, isDropshipClicked) {
            if (AddressType == "BillTo") {
                document.getElementById('hdnBillAddressID').value = AddressID;
            } else {
                document.getElementById('hdnShipAddressID').value = AddressID;
            }

            document.getElementById("btnTemp").click();

            return false;
        }

        function ChangeGrossProfit(profit) {
            document.getElementById('hdnChangedProfitPercent').value = profit;
            document.getElementById("btnChangedProfit").click();
            return false;
        }

        function Add(a) {
            var IsAddCalidate = Save();

            if (IsAddCalidate == false) {
                return false;
            }

            if ($find('radWareHouse') != null) {
                if ($find('radWareHouse').get_value() == "") {
                    alert("Select Warehouse")
                    return false;
                }
            }
            if (document.getElementById("txtUnits").value == "") {
                alert("Enter Units")
                document.getElementById("txtUnits").focus();
                return false;
            }
            if (parseFloat(document.getElementById("txtUnits").value) <= 0) {
                alert("Units must greater than 0.")
                document.getElementById("txtUnits").focus();
                return false;
            }

            if (document.getElementById("txtprice").value == "") {
                alert("Enter Price")
                document.getElementById("txtprice").focus();
                return false;
            }

            //Check for Duplicate Item
            var table = $ControlFind('ctl00_Content_order1_dgItems');
            var WareHouesID;
            if ($ControlFind('radWareHouse') == null) {
                WareHouesID = '';
            }
            else {
                WareHouesID = $find('radWareHouse').get_value();
            }

            if ($ControlFind('chkDropShip').checked == true && $ControlFind('hdnPrimaryVendorID').value == "0") {
                alert("You must add a primary vendor to this item first")
                return false;
            }

            var dropship;
            if ($ControlFind('chkDropShip').checked == true) {
                dropship = 'True';
                WareHouesID = '';
            }
            else {
                dropship = 'False';
            }



            var checkbox = document.getElementById("chkWorkOrder");
            if (checkbox != null && checkbox.checked) {
                if (document.getElementById('txtDate').value == 0) {
                    alert("Enter Expected Completion Date")
                    document.getElementById('txtDate').focus();
                    return false;
                }
            }
        }

        function SetWareHouseId(id) {
            var combo = $find('radWareHouse')
            var node = combo.findItemByValue(id);
            node.select();
        }

        function OpenAdd() {
            if ($find('radCmbCompany').get_value() == "") {
                alert("Select Customer")
                return false;
            }
            window.open("../prospects/frmProspectsAdd.aspx?pwer=dfsdfdsfdsfdsf&rtyWR=" + $find('radCmbCompany').get_value() + "&Reload=1", '', 'toolbar=no,titlebar=no,top=300,width=700,height=300,scrollbars=no,resizable=no')
            return false;
        }

        function reloadPage(type) {
            if (type == 'add') {
                document.getElementById("hdnType").value = type;
                __doPostBack('btnBindOrderGrid', '');

                return false;
            }
            else {
                document.getElementById("hdnType").value = '';
            }
        }

        function OpenInTransit(selectedItemCode, selectedWarehouseItemID) {
            var w = screen.width - 200;
            var h = screen.height - 100;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            window.open('../opportunity/frmOrderTransit.aspx?ItemCode=' + selectedItemCode + '&WarehouseItemID=' + selectedWarehouseItemID, '', 'toolbar=no,titlebar=no,width=' + w + ',height=' + h + ',top=' + top + ',left=' + left + ',scrollbars=yes,resizable=yes')
            return false;
        }
        function openLeadTimes(selectedItemCode) {
            window.open('../opportunity/frmOrderTransitforSO.aspx?ItemCode=' + selectedItemCode, '', 'toolbar=no,titlebar=no,left=200, top=300,width=900,height=300,scrollbars=yes,resizable=yes')
            return false;
        }
        function openVendorCostTable(a) {
            if ($("#hdnCurrentSelectedItem").val() == '') {
                alert("Select Item");
                return false;
            }

            window.open('../opportunity/frmVendorCostTable.aspx?ItemCode=' + $("#hdnCurrentSelectedItem").val() + '&OppType=' + a + '&Vendor=' + document.getElementById('hdnVendorId').value, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=yes,resizable=yes')
            return false;
        }

        function openSalesVendorCostTable(itemCode) {
            window.open('../opportunity/frmVendorCostTable.aspx?ItemCode=' + itemCode + '&OppType=1&Vendor=' + document.getElementById('hdnVendorId').value, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenRelatedItems(a, b, c, d, selectedItemCode) {

            if (d == 0) {
                alert("Select Company");
                return false;
            }

            window.open('../opportunity/frmOpenRelatedItems.aspx?ItemCode=' + selectedItemCode + '&OppType=' + a + '&PageType=' + b + '&opid=' + c + '&DivId=' + d, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=yes,resizable=yes')
            return false;
        }

        function alertbulksales() {
            var r = confirm("This will create a Sales Order for each of the customers you've seldected.Please review items and price carefully before proceeding.\n\nThere is no mass sales order delete process.\n\nIf you make mistake, you need to delete each Sales Order Manually.\n\nWould you like to proceed?");
            if (r == true) {
                return true;
            } else {
                return false;
            }
        }

        function bulksalesmessage() {
            if (!alert('Check after few minutes as orders creation may take time')) {
                window.opener = self;
                window.close();
                return false;
            }
        }

        function BindOrderGrid() {
            document.getElementById("btnBindOrderGrid").click();
            return false;
        }

        function CheckCharacter(e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;

            if (k == 13) {
                if (e.preventDefault) {
                    e.preventDefault();
                }
                else
                    e.returnValue = false;
                return false;
            }
        }

        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16 || k == 9)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                } else if (k === 13) {
                    $("btnUpdate").click();
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16 || k == 9 || k == 46)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                } else if (k === 13) {
                    $("btnUpdate").click();
                }
            }
        }

        function swipedCreditCard() {
            txtSwipe = document.getElementById('txtSwipe');

            txtCHName = document.getElementById('txtCHName');
            txtCCNo = document.getElementById('txtCCNo');
            radcmbMonth = document.getElementById('<%= radcmbMonth.ClientID%>');
            radcmbYear = document.getElementById('<%= radcmbYear.ClientID%>');
            txtCVV2 = document.getElementById('txtCVV2');

            //boolean values to determine which method to parse
            blnCarrotPresent = false;
            blnEqualPresent = false;
            blnBothPresent = false;

            strParse = txtSwipe.value;

            if (strParse.length <= 10) {
                alert("Error Parsing Card.\r\n");
                txtSwipe.value = '';
                return false;
            }

            strCarrotPresent = strParse.indexOf("^");
            strEqualPresent = strParse.indexOf("=");

            if (strCarrotPresent > 0) {
                blnCarrotPresent = true;
            }

            if (strEqualPresent > 0) {
                blnEqualPresent = true;
            }

            if (blnCarrotPresent == true) {
                //contains carrot
                strParse = '' + strParse + ' ';
                arrSwipe = new Array(4);
                arrSwipe = strParse.split("^");

                if (arrSwipe.length > 2) {
                    account = stripAlpha(arrSwipe[0].substring(1, arrSwipe[0].length));
                    account_name = arrSwipe[1];
                    exp_month = arrSwipe[2].substring(2, 4);
                    exp_year = arrSwipe[2].substring(0, 2);
                    namepieces = account_name.split('/');
                }
                else {
                    alert("Error Parsing Card.\r\n");
                    txtSwipe.value = '';
                    return false;
                }
            }
            else if (blnEqualPresent == true) {
                //equal only delimiter
                sCardNumber = strParse.substring(1, strEqualPresent);
                account = stripAlpha(sCardNumber);
                exp_month = strParse.substr(strEqualPresent + 1, 2);
                exp_year = strParse.substr(strEqualPresent + 3, 2);
            }

            if (isValidCreditCard(account) == false) {
                alert("Error Parsing Card.\r\n Invalid credit card. \r\n");
                txtSwipe.value = '';
                return false;
            }

            selectOptionByValue(radcmbMonth, exp_month);
            selectOptionByValue(radcmbYear, exp_year);
            txtCCNo.value = account;
            selectOptionByValue(document.getElementById('<%= radcmbCardType.ClientID%>'), getCreditCardType(account));

            if (blnCarrotPresent == true) {
                var l = namepieces[0];
                var f = namepieces[1];
                txtCHName.value = namepieces[0] + " " + namepieces[1];
            }

            trackData = strParse.split('?');
            document.getElementById('hdnSwipeTrack1').value = trackData[0] + '?';

            if (trackData.length = 2) {
                document.getElementById('hdnSwipeTrack2').value = trackData[1] + '?';
            }

            txtCVV2.style.visibility = "hidden";

            return true;
        }
        function NotAllowMessage(a) {
            if (a == 1)
                alert("You are not allowed to edit selected item, since it is being used in Authoritative BizDoc!");
            else if (a == 2)
                alert("You are not allowed to edit selected item, since it is being used in Project!");
            else if (a == 3)
                alert("You are not allowed to edit selected item, since it is being used in Workorder!");
            return false;
        }
        function ConfirmApplyCoupon() {
            if (confirm('Coupon Discount will be applied to the whole order. Do you wish to proceed ?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function ConfirmShippingItem() {
            alert("hi");
            if (confirm('This will add selected shipping charges to the “Products & Services” section”.Do you want to proceed ?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function ValidateShipperAccount() {
            if (document.getElementById('hdnShipperAccountNo') != null) {
                if (document.getElementById('hdnShipperAccountNo').value.toString() != '') {
                    return true;
                }
                else {
                    if ($find('radCmbCompany').get_value() != "") {
                        var myWidth = 750;
                        var myHeight = 700;
                        var left = (screen.width - myWidth) / 2;
                        var top = (screen.height - myHeight) / 4;
                        window.open('../account/frmConfigParcelShippingAccount.aspx?IsFromNewOrder=1&DivID=' + $find('radCmbCompany').get_value(), '', 'toolbar=no,titlebar=no,left=' + left + ',top=' + top + ',width=' + myWidth + ',height=' + myHeight + ',scrollbars=yes,resizable=yes');
                        document.getElementById('chkUseCustomerShippingAccount').checked = false;
                        return false;
                    }
                }
            }
        }

        function UpdateShipperAccountNo(shipperAccountNo) {
            if (document.getElementById('hdnShipperAccountNo') != null) {
                document.getElementById('hdnShipperAccountNo').value = shipperAccountNo
            }
        }

        function Scroll(isEnabled) {
            if (isEnabled) {
                $('body').css('overflow', '');
            } else {
                $('body').css('overflow', 'hidden');
            }
        }

        function RemovePromotion() {
            if (confirm('All the items where this promotion is applied will be removed. Do you want to proceed?')) {
                $("#btnRemovePromotion").click();
                return true;
            } else {
                return false;
            }
        }

        function DeleteItem(numOppItemID) {
            if (confirm('This items and all other items where this promotion is applied will be removed. Do you want to proceed?')) {
                $("#hdnDeleteItemID").val(numOppItemID);
                $("#btnDeleteItem").click();
            }

            /* Important - Always return false*/
            return false;
        }
        function checkRelatedItems() {
            if ($('#<%=gvRelatedItems.ClientID%> input:checkbox:checked').length == 1) {
                $("#<%=btnReplaceClose.ClientID%>").css("display", "inline");
            } else {
                $("#<%=btnReplaceClose.ClientID%>").css("display", "none");
            }
        }
        function Confirm() {
            if ($("#divPromotionDetailRI").is(':visible') && $("#lblCouponText").is(':visible')) {
                if ($("#txtCouponCodeRI").val().length == 0) {
                    var confirm_value = document.createElement("INPUT");
                    confirm_value.type = "hidden";
                    confirm_value.name = "confirm_value";
                    if (confirm("You’re about to add an item at a regular price. You can cancel this window and add coupon code first, then add the item, or simply click continue and item will be added to the cart.")) {
                        confirm_value.value = "Yes";
                    } else {
                        confirm_value.value = "No";
                    }
                    document.forms[0].appendChild(confirm_value);
                }
            }
            if ($('#<%=gvRelatedItems.ClientID%> input:checkbox:checked').length <= 0) {
                alert('Please select atleast one item.');
                return false;
            }
            else {
                return GetSelectedItems(false);
            }
        }

        function ClosePreCheckoutWindow() {
            var radwindow = $find('<%=RadWinRelatedItems.ClientID %>');
            radwindow.close();
            HideOtherTopSectionAfter1stItemAdded();
            return false;
        }

        function checkAll(objRef) {
            var GridView = objRef.parentNode.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                //Get the Cell To find out ColumnIndex
                var row = inputList[i].parentNode.parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {
                        //If the header checkbox is checked
                        //check all checkboxes
                        //and highlight all rows
                        inputList[i].checked = true;
                    }
                    else {
                        //If the header checkbox is checked
                        //uncheck all checkboxes
                        inputList[i].checked = false;
                    }
                }
            }
        }

        function ValidateOnMultiSelect(btn) {

            if ($find('radCmbCompany').get_value() == "") {
                alert("Select Customer")
                return false;
            }
            if ($find('ctl00_Content_radcmbContact').get_value() == 0) {
                alert("Select Contact")
                return false;
            }
            if (btn.id == "btnMultiSelect") {
                if ($("#gvMultiSelectItems").is(':visible')) {
                    if ($("#<%=gvMultiSelectItems.ClientID %> tr").length != 0) {

                        if (confirm('Please add your multi-selected items or remove them from the select grid. Selecting' + "'OK'" + 'will remove your items.')) {
                            $("#gvMultiSelectItems").html("");
                            $("#divMultiSelect").hide();
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                }
            }

            var radCmbCompany = $find('radCmbCompany').get_value();
            var hdnCustAddr = $("#hdnCustomerAddr").val()
            var combo = $find('<%=radcmbLocation.ClientID%>');
            var WarehouseId;
            if (combo != null) {
                WarehouseId = combo.get_selectedItem().get_value();
            }
            window.opener.close();
            var child = window.open('../Items/frmItemList.aspx?CustId=' + radCmbCompany + '&CustAddrId=' + hdnCustAddr + '&WarehouseId=' + WarehouseId, '');
            window.external.comeback = function () {
                var back = confirm('Your multi Selected Items will be added.');
                if (back) {
                    child.close();
                } else {
                    child.focus();
                }
            }
        }

        function getCheckedItems(radCmbId) {
            var combo = $find(radCmbId);
            var selectedItemsValues = "";
            var items = combo.get_items();
            var itemsCount = items.get_count();

            for (var itemIndex = 0; itemIndex < itemsCount; itemIndex++) {
                var item = items.getItem(itemIndex);
                var checkbox = $telerik.findElement(item.get_element(), "chkSelect");

                if (checkbox.checked) {
                    selectedItemsValues += item.get_value() + ",";
                }
            }

            return selectedItemsValues;
        }

        function ConfirmPromotion(CouponCode) {
            var requireCouponCode = CouponCode;
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (requireCouponCode == 'NoCouponCode') {
                if (confirm("Do you want to apply Promotion?")) {
                    confirm_value.value = "Yes";
                } else {
                    confirm_value.value = "No";
                }
                document.forms[0].appendChild(confirm_value);
                $("#btnConfirmProm").click();
            }
            else if (requireCouponCode == 'YesCouponCode') {
                if (confirm("To Apply Promotion enter Coupon Code or Cancel to proceed.")) {
                    confirm_value.value = "Yes";
                } else {
                    confirm_value.value = "No";
                }
                document.forms[0].appendChild(confirm_value);
                $("#btnConfirmProm").click();
            }
        }

        function HideDivShipExceptions() {
            $("#dialogShipExceptions").hide();
            return false;
        }

        function showShipExceptnDiv() {
            $("#dialogShipExceptions").show();
            return false;
        }

        function showChangeZipDiv() {
            $("#DivChangeZip").show();
            return false;
        }

        function HideChangeZipDiv() {
            $("#DivChangeZip").hide();
            return false;
        }

        function ValidateChangeZip() {
            if (document.getElementById('txtChangeZip').value == "") {
                alert("Enter Zip");
                return false;
            }
        }

        function OpenKitSelectionWindow() {
            $("[id$=divEditKit]").show();
            return false;
        }

        function CloseEditKitChildWindow() {
            $("[id$=divEditKit]").hide();
            return false;
        }

        function OnLocationSelectedIndexChanged(sender, eventArgs) {
            var item = eventArgs.get_item();
            $("[id$=lblShipFrom]").text(item.get_attr("Address"));
            $("[id$=hdnShipFromCountry]").val(item.get_attr("Country"));
            $("[id$=hdnShipFromState]").val(item.get_attr("State"));
            $("[id$=hdnShipFromPostalCode]").val(item.get_attr("PostalCode"));
        }

        function CloseAssemblyDetail() {
            $("[id$=divAssemblyDetail]").hide();
            return false;
        }

        function OpenNewItemPopup(url) {
            if (url != "") {
                OpenPopUp(url + "&isFromNewOrder=true");
            }
        }

        function SelectNewAddedItem(itemCode, itemName) {
            $("#txtItem").select2("search", itemName);
        }

        function OpenPopUp(url) {
            window.open(url, "", "toolbar = no, titlebar = no, left = 100, top = 100, width = 1200, height = 645, scrollbars = yes, resizable = yes");
        }

        function LoadNewKitItem(itemCode) {
            try {
                $("[id$=divKitChild]").show();

                $.ajax({
                    type: "POST",
                    url: '../common/Common.asmx/GetSearchedItems',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({
                        "searchText": ("preselectitemcode:" + itemCode.toString()),
                        "pageIndex": 1,
                        "pageSize": 10,
                        "divisionId": getDivisionId(),
                        "isGetDefaultColumn": false,
                        "warehouseID": getvalue(),
                        "searchOrdCusHistory": 0,
                        "oppType": 1,
                        "isTransferTo": false,
                        "IsCustomerPartSearch": false,
                        "searchType": $("#ddlSearchType").val()
                    }),
                    success: function (data) {
                        try {
                            if (data.hasOwnProperty("d")) {
                                if (data.d == "Session Expired") {
                                    alert("Session expired.");
                                    window.opener.location.href = window.opener.location.href;
                                    window.close();
                                } else {
                                    data = $.parseJSON(data.d)
                                }
                            }
                            else
                                data = $.parseJSON(data);

                            if ($.parseJSON(data.results).length > 0) {
                                $('#txtItem').select2("data", $.parseJSON(data.results)[0]);
                                ItemSelectionChanged($.parseJSON(data.results)[0]);
                            }

                            $("#divLoader").hide();
                        } catch (e) {
                            $("#divLoader").hide();
                            throw e;
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $("#divLoader").hide();
                        alert(textStatus);
                    }
                });
            } catch (e) {
                $("#divLoader").hide();
                alert("Unkown error occurred while selecting new created item")
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanelException" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Label ID="lblException" runat="server"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    New Sales
    <asp:Label ID="lblTitle" runat="server" Text="Order"></asp:Label>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <table width="100%" runat="server" id="tblMultipleOrderMessage" visible="false" style="background-color: #FFFFFF; color: red">
        <tr>
            <td class="normal4" align="center">You can not create multiple orders simultaneously,
                <asp:LinkButton Text="Click here" runat="server" ID="lnkClick" />
                to clear other order and proceed.
            </td>
        </tr>
    </table>
    <div class="container customer-detail">
        <div class="grid grid-pad">
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" id="btnCustomerCollapseButton" href="#CustomerSection">Customer Section</a>
                        </h4>
                    </div>
                    <div id="CustomerSection" class="accordion-body collapse in">
                        <div class="panel-body">
                            <div class="cont1">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="lkbExistingCustomer" />
                                        <asp:AsyncPostBackTrigger ControlID="lkbNewCustomer" />
                                        <asp:AsyncPostBackTrigger ControlID="radCmbCompany" />
                                        <asp:AsyncPostBackTrigger ControlID="chkShipDifferent" />
                                        <asp:AsyncPostBackTrigger ControlID="radcmbSalesTemplate" />
                                        <asp:AsyncPostBackTrigger ControlID="ddlBillCountry" />
                                        <asp:AsyncPostBackTrigger ControlID="ddlShipCountry" />
                                    </Triggers>
                                    <ContentTemplate>
                                        <div class="col-1-1">
                                            <div class="col-1-2">
                                                <div class="content">
                                                    <div id="divExistingCustomerColumn1" class="col-1-1" runat="server" visible="true">
                                                        <div class="form-group" style="margin-bottom: 0px;">
                                                            <label class="col-3-12">
                                                                <div>
                                                                    <div style="float: right">Customer & Contact<span class="required">*</span></div>
                                                                    <div style="float: right">
                                                                        <asp:LinkButton ID="lkbNewCustomer" ToolTip="New Customer" runat="server">
                                                                            <asp:Image ID="Image1" ImageUrl="~/images/AddRecord.png" runat="server" Style="margin-right: 2px" />
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </div>
                                                            </label>
                                                            <div class="col-9-12" id="divselcompany" runat="server">
                                                                <table width="100%">
                                                                    <tr>
                                                                        <td style="width: 50%">
                                                                            <telerik:RadComboBox AccessKey="C" Width="100%" ID="radCmbCompany" DropDownWidth="600px"
                                                                                Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" ShowMoreResultsBox="true" EnableLoadOnDemand="True"
                                                                                OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                                                                ClientIDMode="Static" TabIndex="1" OnClientLoad="onRadComboBoxLoad">
                                                                                <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                                                            </telerik:RadComboBox>
                                                                        </td>
                                                                        <td style="width: 50%">
                                                                            <telerik:RadComboBox ID="radcmbContact" Width="100%" runat="server" AutoPostBack="true" TabIndex="2" OnClientLoad="onRadComboBoxLoad"></telerik:RadComboBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <div class="form-group" id="divselcompany1" runat="server" style="width: 100%; margin-bottom: 0px;">
                                                                <label class="col-3-12">
                                                                    <a href="#" onclick="return OpenAdd()" class="hyperlink">
                                                                        <img id="Img1" src="../images/edit.png" runat="server" alt="AddEdit" /></a>
                                                                    <asp:LinkButton ID="lnkBillTo" runat="server" Text="Bill to:" class="hyperlink" Font-Underline="true"></asp:LinkButton>
                                                                </label>
                                                                <div class="col-9-12" style="padding-top: 9px;">
                                                                    <asp:Label ID="lblBillTo1" runat="server" Text=""></asp:Label>
                                                                    <asp:Label ID="lblBillTo2" runat="server" Text=""></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div id="divbulksales" class="col-9-12" runat="server" visible="false">
                                                                <asp:Label ID="lblbulksales" runat="server"></asp:Label>
                                                            </div>
                                                        </div>
                                                        <div class="form-group" id="divAssignedTo" runat="server">
                                                            <label class="col-3-12">Assign To</label>
                                                            <div class="col-9-12">
                                                                <telerik:RadComboBox ID="radcmbAssignedTo" Width="100%" runat="server" AutoPostBack="true" OnClientLoad="onRadComboBoxLoad" TabIndex="3">
                                                                    <Items>
                                                                        <telerik:RadComboBoxItem Value="0" Text="--Select One--" />
                                                                    </Items>
                                                                </telerik:RadComboBox>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="col-1-2">
                                                <div class="content">
                                                    <div id="divExistingCustomerColumn2" class="col-1-1" runat="server" visible="true">
                                                        <div class="form-group" style="margin-bottom: 0px;">
                                                            <div class="form-group" id="divLocation" style="margin-bottom: 0px;" runat="server">
                                                                <asp:Panel ID="PanelLocation" runat="server">
                                                                    <label class="col-3-12">
                                                                        Location <span title="Selecting All Locations will pull items from all locations (warehouses), 
                                                                                                but if you select a particular location (which you can set as default by user) 
                                                                                                it will only pull items from that specific location">[?]</span></label>
                                                                    <div class="col-9-12">
                                                                        <telerik:RadComboBox ID="radcmbLocation" Width="100%" runat="server" TabIndex="6" OnClientSelectedIndexChanged="OnLocationSelectedIndexChanged" OnClientLoad="onRadComboBoxLoad" />
                                                                    </div>
                                                                </asp:Panel>
                                                            </div>
                                                            <%--  <label class="col-3-12">Ship Via</label>
                                                            <div class="col-9-12" >
                                                               <table width="100%">
                                                                    <tr>
                                                                        <td style="width:50%">
                                                                             <telerik:RadComboBox ID="rdcmbShipVia" Width="100%" DropDownWidth="180px" runat="server" ></telerik:RadComboBox>
                                                                        </td>
                                                                        <td style="width:50%">
                                                                            <telerik:RadComboBox ID="rdcmbShippingService" runat="server" Width="100%" MaxHeight="400px" DropDownWidth="280px"></telerik:RadComboBox>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </div>--%>
                                                            <div class="form-group" style="margin-bottom: 0px;">
                                                                <label class="col-3-12">
                                                                    <a href="#" onclick="return OpenAdd()" class="hyperlink">
                                                                        <img id="Img2" src="../images/edit.png" runat="server" alt="AddEdit" /></a>
                                                                    <asp:LinkButton ID="lnkShipTo" runat="server" Text="Ship to:" class="hyperlink" Font-Underline="true"></asp:LinkButton>
                                                                </label>
                                                                <div class="col-9-12" style="padding-top: 9px;">
                                                                    <asp:Label ID="lblShipTo1" runat="server" Text=""></asp:Label>
                                                                    <asp:Label ID="lblShipTo2" runat="server" Text=""></asp:Label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group" id="divTemplate" runat="server">
                                                        <label class="col-3-12">Item Template</label>
                                                        <div class="col-9-12">
                                                            <telerik:RadComboBox ID="radcmbSalesTemplate" Width="100%" runat="server" AutoPostBack="true" TabIndex="4" OnClientLoad="onRadComboBoxLoad" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="divNewCustomer" class="col-1-1" runat="server" visible="false">
                                            <div class="col-1-2">
                                                <div class="content">
                                                    <div class="form-group">
                                                        <label for="FirstName" class="col-3-12">
                                                            <div>
                                                                <div style="float: right">First Name<span class="required">*</span></div>
                                                                <div style="float: right">
                                                                    <asp:LinkButton ID="lkbExistingCustomer" ToolTip="Existing Customer" runat="server">
                                                                        <asp:Image ID="Image2" ImageUrl="~/images/AddRecord.png" runat="server" Style="margin-right: 2px" />
                                                                    </asp:LinkButton>
                                                                </div>
                                                            </div>
                                                        </label>
                                                        <div class="col-9-12">
                                                            <asp:TextBox ID="txtFirstName" runat="server" MaxLength="50" TabIndex="12"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Phone" class="col-3-12">Phone</label>
                                                        <div class="col-9-12">
                                                            <div style="float: left">
                                                                <asp:TextBox ID="txtPhone" runat="server" MaxLength="50" TabIndex="14"></asp:TextBox>
                                                            </div>
                                                            <div style="float: left; margin-left: 10px;">
                                                                <div class="form-group">
                                                                    <label for="Ext" class="col-3-12">Ext</label>
                                                                </div>
                                                            </div>
                                                            <div style="float: left;">
                                                                <asp:TextBox ID="txtExt" runat="server" Width="30" MaxLength="10" TabIndex="15"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Organization" class="col-3-12">Organization</label>
                                                        <div class="col-9-12">
                                                            <asp:TextBox ID="txtCompany" runat="server" MaxLength="50" TabIndex="17"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Relationship" class="col-3-12">Relationship<span class="required">*</span></label>
                                                        <div class="col-9-12">
                                                            <telerik:RadComboBox ID="radcmbRelationship" Width="100%" runat="server" AutoPostBack="true" TabIndex="19" OnClientLoad="onRadComboBoxLoad"></telerik:RadComboBox>
                                                            <%-- <asp:DropDownList ID="ddlRelationship" runat="server" AutoPostBack="true" TabIndex="19">
                                            </asp:DropDownList>--%>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="TaxDetail" class="col-3-12"></label>
                                                        <div class="col-9-12">
                                                            <asp:CheckBoxList ID="chkTaxItems" CssClass="normal1" runat="server" RepeatDirection="Horizontal"
                                                                RepeatColumns="3" TabIndex="21">
                                                            </asp:CheckBoxList>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-1-2">
                                                <div class="content">
                                                    <div class="form-group">
                                                        <label for="LastName" class="col-3-12">Last Name<span class="required">*</span></label>
                                                        <div class="col-9-12">
                                                            <asp:TextBox ID="txtLastName" runat="server" MaxLength="50" TabIndex="13"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Email" class="col-3-12">Email</label>
                                                        <div class="col-9-12">
                                                            <asp:TextBox ID="txtEmail" runat="server" TabIndex="16"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="AssignedTo" class="col-3-12">Assign To</label>
                                                        <div class="col-9-12">
                                                            <telerik:RadComboBox ID="radcmbNewAssignTo" Width="100%" runat="server" AutoPostBack="true" TabIndex="18" OnClientLoad="onRadComboBoxLoad">
                                                                <Items>
                                                                    <telerik:RadComboBoxItem Text="--Select One--" Value="0" />
                                                                </Items>
                                                            </telerik:RadComboBox>
                                                            <%--<asp:DropDownList ID="ddlNewAssignTo" runat="server" TabIndex="18">
                                                <asp:ListItem Text="--Select One--" Value="0"></asp:ListItem>
                                            </asp:DropDownList>--%>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="Profile" class="col-3-12">Profile</label>
                                                        <div class="col-9-12">
                                                            <telerik:RadComboBox ID="radcmbProfile" Width="100%" runat="server" AutoPostBack="true" TabIndex="20" OnClientLoad="onRadComboBoxLoad"></telerik:RadComboBox>
                                                            <%-- <asp:DropDownList ID="ddlProfile" runat="server" TabIndex="20">
                                            </asp:DropDownList>--%>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-1-1">
                                                <strong>Billing Address</strong>
                                            </div>
                                            <div class="col-1-1">
                                                <div class="col-1-2">
                                                    <div class="content">
                                                        <div class="form-group">
                                                            <label for="BillStreet" class="col-3-12">Street</label>
                                                            <div class="col-9-12">
                                                                <asp:TextBox ID="txtBillStreet" runat="server" TextMode="MultiLine" TabIndex="21"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="BillState" class="col-3-12">State</label>
                                                            <div class="col-9-12">
                                                                <%--<telerik:RadComboBox ID="radcmbBillState" Width="100%" AutoPostBack="true" runat="server" TabIndex="23" OnClientLoad="onRadComboBoxLoad">
                                                    <Items>
                                                        <telerik:RadComboBoxItem Text="--Select One--" Value="0" />
                                                    </Items>
                                                </telerik:RadComboBox>--%>
                                                                <asp:DropDownList ID="ddlBillState" runat="server" TabIndex="23">
                                                                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:HiddenField ID="hdnBillState" runat="server" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="BillPostal" class="col-3-12">Postal</label>
                                                            <div class="col-9-12">
                                                                <asp:TextBox ID="txtBillPostal" runat="server" TabIndex="25"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-1-2">
                                                    <div class="content">
                                                        <div class="form-group">
                                                            <label for="BillCity" class="col-3-12">City</label>
                                                            <div class="col-9-12">
                                                                <asp:TextBox ID="txtBillCity" runat="server" TabIndex="22"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="BillCountry" class="col-3-12">Country</label>
                                                            <div class="col-9-12">
                                                                <%--<telerik:RadComboBox ID="radcmbBillCountry" Width="100%" TabIndex="24" AutoPostBack="True" runat="server" OnClientLoad="onRadComboBoxLoad">
                                                    <Items>
                                                        <telerik:RadComboBoxItem Text="--Select One--" Value="0" />
                                                    </Items>
                                                </telerik:RadComboBox>--%>
                                                                <asp:DropDownList ID="ddlBillCountry" AutoPostBack="True" runat="server" Style="width: 235px !important"
                                                                    CssClass="signup" TabIndex="24">
                                                                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-1-1">
                                                <asp:CheckBox AutoPostBack="true" ID="chkShipDifferent" Text="Click here if Shipping Address is different than Billing Address" runat="server" TabIndex="26" />
                                            </div>
                                            <div id="divShippingAddress" runat="server" visible="false" style="margin-top: 5px;" class="col-1-1">
                                                <div class="col-1-1"><strong>Shipping Address</strong></div>
                                                <div class="col-1-2">
                                                    <div class="content">
                                                        <div class="form-group">
                                                            <label for="ShipStreet" class="col-3-12">Street</label>
                                                            <div class="col-9-12">
                                                                <asp:TextBox ID="txtShipStreet" runat="server" TextMode="MultiLine" TabIndex="27"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="ShipState" class="col-3-12">State</label>
                                                            <div class="col-9-12">
                                                                <%--<telerik:RadComboBox ID="radcmbShipState" Width="100%" runat="server" TabIndex="29" OnClientLoad="onRadComboBoxLoad">
                                                    <Items>
                                                        <telerik:RadComboBoxItem Text="--Select One--" Value="0" />
                                                    </Items>
                                                </telerik:RadComboBox>--%>
                                                                <asp:DropDownList ID="ddlShipState" runat="server" TabIndex="29">
                                                                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                                </asp:DropDownList>
                                                                <asp:HiddenField ID="hdnShipState" runat="server" />
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="ShipPostal" class="col-3-12">Postal</label>
                                                            <div class="col-9-12">
                                                                <asp:TextBox ID="txtShipPostal" runat="server" TabIndex="31"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-1-2">
                                                    <div class="content">
                                                        <div class="form-group">
                                                            <label for="ShipCity" class="col-3-12">City</label>
                                                            <div class="col-9-12">
                                                                <asp:TextBox ID="txtShipCity" runat="server" TabIndex="28"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="ShipCountry" class="col-3-12">Country</label>
                                                            <div class="col-9-12">
                                                                <%-- <telerik:RadComboBox ID="radcmbShipCountry" Width="100%" TabIndex="30" AutoPostBack="True" runat="server" OnClientLoad="onRadComboBoxLoad">
                                                    <Items>
                                                        <telerik:RadComboBoxItem Text="--Select One--" Value="0" />
                                                    </Items>
                                                </telerik:RadComboBox>--%>
                                                                <asp:DropDownList ID="ddlShipCountry" TabIndex="30" AutoPostBack="True" runat="server">
                                                                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-1-1">
                                            <div class="col-1-2" style="float: left">
                                                <div class="form-group" id="divCurrency" runat="server">
                                                    <asp:Panel ID="pnlCurrency" runat="server" Visible="false">
                                                        <label class="col-3-12">Currency</label>
                                                        <div class="col-9-12">
                                                            <telerik:RadComboBox ID="radcmbCurrency" runat="server" TabIndex="5" Width="100%" OnClientLoad="onRadComboBoxLoad" AutoPostBack="true"></telerik:RadComboBox>
                                                        </div>
                                                    </asp:Panel>
                                                </div>
                                            </div>
                                            <div class="col-1-2" style="float: right">
                                                <%--<div class="form-group" id="divLocation" runat="server">
                                                    <asp:Panel ID="PanelLocation" runat="server">
                                                        <label class="col-3-12">
                                                            Location <span title="Selecting All Locations will pull items from all locations (warehouses), 
                                                                                    but if you select a particular location (which you can set as default by user) 
                                                                                    it will only pull items from that specific location">[?]</span></label>
                                                        <div class="col-9-12">
                                                            <telerik:RadComboBox ID="radcmbLocation" Width="100%" runat="server" TabIndex="6" AutoPostBack="true" OnClientLoad="onRadComboBoxLoad" />
                                                        </div>
                                                    </asp:Panel>
                                                </div>--%>
                                            </div>
                                        </div>
                                        <div class="col-1-1">
                                            <div id="divClass" class="col-1-2" style="float: left" runat="server" visible="false">
                                                <label class="col-3-12">Class</label>
                                                <div class="col-9-12">
                                                    <asp:DropDownList ID="ddlClass" runat="server" TabIndex="3"></asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" id="btnOrderCollapseButton" href="#OrderSection">Order Section</a>
                        </h4>
                    </div>
                    <div id="OrderSection" class="accordion-body collapse in">
                        <div class="panel-body">
                            <asp:UpdatePanel ID="UpdatePanelFinancialStamp" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="cont2">
                                        <div class="col-1-1">
                                            <table border="0" width="100%">
                                                <tr>
                                                    <td style="width: 65%; vertical-align: top;">
                                                        <asp:PlaceHolder runat="server" ID="plhOrderDetail">
                                                            <table border="0" width="100%">
                                                                <tr>
                                                                    <td style="width: 50%">
                                                                        <asp:PlaceHolder ID="plhOrderDetail1" runat="server"></asp:PlaceHolder>
                                                                    </td>
                                                                    <td style="vertical-align: top; width: 50%;">
                                                                        <asp:PlaceHolder ID="plhOrderDetail2" runat="server"></asp:PlaceHolder>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <div style="width: 100%">
                                                                            <asp:UpdatePanel ID="UpdatePanelCustomerDetail1" runat="server" UpdateMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <asp:PlaceHolder ID="plhCustomerDetail1" runat="server"></asp:PlaceHolder>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </div>
                                                                    </td>
                                                                    <td>
                                                                        <div style="width: 100%">
                                                                            <asp:UpdatePanel ID="UpdatePanelCustomerDetail2" runat="server" UpdateMode="Conditional">
                                                                                <ContentTemplate>
                                                                                    <asp:PlaceHolder ID="plhCustomerDetail2" runat="server"></asp:PlaceHolder>
                                                                                </ContentTemplate>
                                                                            </asp:UpdatePanel>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                        </asp:PlaceHolder>
                                                    </td>
                                                    <%--  <td class="col-1-3">                                                        
                                                                                                           
                                                    </td>--%>
                                                    <td style="vertical-align: top; width: 35%; padding-left: 15px;">
                                                        <asp:Panel ID="PanelFinancialStamp" runat="server">
                                                            <table border="0" style="width: 100%">
                                                                <tr id="trExpctedOrderDate">
                                                                    <td class="td2">
                                                                        <%--<label id class="col-3-12">Release Date</label>--%>
                                                                        <asp:Label ID="lblOrderExpectedDate" Text="Expected Date:" Font-Bold="true" runat="server"></asp:Label>
                                                                    </td>
                                                                    <td class="td3">
                                                                        <div class="col-9-12">
                                                                            <telerik:RadDatePicker ID="radOrderExpectedDate" DateInput-DisplayDateFormat="MM/dd/yyyy" DateInput-DateFormat="MM/dd/yyyy" runat="server"></telerik:RadDatePicker>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td2">
                                                                        <label>Sales Credit:</label>
                                                                    </td>
                                                                    <td class="td3">
                                                                        <asp:Label ID="lblCreditBalance" Width="120px" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td2">
                                                                        <label>Credit Limit (A):</label>
                                                                    </td>
                                                                    <td class="td3">
                                                                        <asp:Label ID="lblCreditLimit" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td2">
                                                                        <label>Balance Due (B):</label>
                                                                    </td>
                                                                    <td class="td3">
                                                                        <asp:Label ID="lblBalanceDue" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td2">
                                                                        <label style="width: 150px;">Remaining Credit(A-B):</label>
                                                                    </td>
                                                                    <td class="td3">
                                                                        <asp:Label ID="lblRemaningCredit" runat="server"></asp:Label>
                                                                        <asp:HiddenField ID="hdnRemaningCredit" runat="server" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="td2">
                                                                        <label>Amount Past Due:</label>
                                                                    </td>
                                                                    <td class="td3">
                                                                        <asp:Label ID="lblTotalAmtPastDue" runat="server"></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                        </asp:Panel>
                                                    </td>
                                                </tr>
                                            </table>

                                            <asp:HiddenField ID="hdnmonVal1" runat="server" />
                                            <asp:HiddenField ID="hdnmonVal2" runat="server" />
                                        </div>


                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" id="btnItemCollapseButton" href="#ItemSection">Item Section</a>
                        </h4>
                    </div>
                    <div id="ItemSection" class="accordion-body collapse in">
                        <div class="panel-body">
                            <div class="cont1" style="border-bottom: 0px !important; padding-bottom: 0px !important;">
                                <asp:UpdatePanel ID="UpdateOrderPromRule" runat="server" UpdateMode="Conditional" class="col-1-1">
                                    <ContentTemplate>
                                        <div class="form-group" id="divOrderPromotionDetail" runat="server" visible="false">
                                            <div class="col-0-12" style="text-align: right">
                                                <img src="../images/Deal_icon.png" height="40px" width="40px" alt="" />
                                            </div>
                                            <div class="col-12-12" style="line-height: 40px; padding-left: 10px; font-weight: bold;">
                                                <asp:Label ID="lblOrderPromotionDesc" Style="width: 100%" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdatePanel ID="UpdateShipPromRule" runat="server" UpdateMode="Conditional" class="col-1-1">
                                    <ContentTemplate>
                                        <div style="width: 100%; padding-left: 100px; text-align: left;" id="divShippingProm" runat="server" visible="false">
                                            <div class="form-group">
                                                <div class="col-0-12" style="text-align: right">
                                                    <img src="../images/ShippingBox.png" height="40px" width="40px" alt="" />
                                                </div>
                                                <div class="col-12-12" style="line-height: 40px; padding-left: 10px; font-weight: bold;">
                                                    <asp:Label ID="lblShipPromDesc" Style="width: 100%" runat="server"></asp:Label><a runat="server" id="lnkShippingExceptions" visible="false" onclick="return showShipExceptnDiv();" style="text-decoration: underline;"><i>Exceptions apply</i></a>
                                                </div>
                                            </div>
                                            <div class="overlay" id="dialogShipExceptions" style="display: none">
                                                <div class="overlayContent" style="color: #000; background-color: #FFF; width: 400px; padding: 10px; border: 2px solid black;">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="text-align: center">
                                                                <h5>Additional Shipping Charges</h5>
                                                            </td>
                                                            <td style="float: right;">
                                                                <asp:Button ID="btnCloseShipExcpt" OnClientClick="javascript: return HideDivShipExceptions();" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="Close" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <label style="font-weight: normal"><i>Note: Free shipping does not apply if sum-total of all items is 0.00</i></label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%;" colspan="2">
                                                                <div class="col-xs-12">
                                                                    <asp:DataGrid ID="dgShippingExceptions" AllowSorting="True" runat="server" Width="100%"
                                                                        CssClass="table table-striped table-bordered" AutoGenerateColumns="False">
                                                                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                                        <ItemStyle CssClass="is"></ItemStyle>
                                                                        <HeaderStyle HorizontalAlign="Center" Font-Bold="true" Font-Size="10" BackColor="#e8e8e8" BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                                                                        <Columns>
                                                                            <asp:BoundColumn DataField="vcItemName" HeaderStyle-Width="60%" ItemStyle-Width="60%" HeaderText="Item"></asp:BoundColumn>
                                                                            <asp:TemplateColumn HeaderText="Shipping Charge" HeaderStyle-Width="40%" ItemStyle-Width="40%">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblFlatAmt" runat="server" Text='<%# "$" + Eval("FlatAmt").ToString() %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateColumn>
                                                                            <asp:BoundColumn DataField="FlatAmt" Visible="false"></asp:BoundColumn>
                                                                        </Columns>
                                                                    </asp:DataGrid>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <asp:UpdatePanel ID="UpdateItemPromotion" runat="server" UpdateMode="Conditional" class="col-1-1">
                                    <ContentTemplate>
                                        <div class="form-group" id="divItemProm" runat="server" visible="false">
                                            <div class="col-0-12" style="text-align: right">
                                                <img src="../images/ShoppingCart_Deal.png" alt="" />
                                            </div>
                                            <div class="col-12-12" style="line-height: 40px; padding-left: 10px; font-weight: bold;">
                                                <asp:Label ID="lblItemPromLongDesc" Style="width: 100%" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>

                                <asp:UpdatePanel ID="UpdatePanelItem" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">

                                    <ContentTemplate>
                                        <div class="col-1-1" id="divItem" runat="server">
                                            <div class="form-group" style="margin-bottom: 0px !important;">
                                                <label class="col-1-12" style="width: 7.90%; padding-right: 0px;">
                                                    <a id="lkbNewItem" title="New Item" href="javascript:OpenPopUp('../Items/frmAddItem.aspx?isFromNewOrder=true')" style="text-decoration:none">
                                                        <asp:Image ID="Image5" ImageUrl="~/images/AddRecord.png" runat="server" Style="margin-right: 2px" />
                                                    </a>
                                                    Item <span class="required">*</span></label>
                                                <div class="col-8-12">
                                                    <ul class="list-inline">
                                                        <li>
                                                            <select id="ddlSearchType" class="select2-choice">
                                                                <option value="1" selected="selected">Contains</option>
                                                                <option value="2">Starts-with</option>
                                                                <option value="3">Ends-with</option>
                                                            </select>
                                                            <asp:HiddenField runat="server" ID="hdnSearchType" />
                                                        </li>
                                                        <li style="width: 60%">
                                                            <asp:TextBox ID="txtItem" ClientIDMode="Static" runat="server" TabIndex="300"></asp:TextBox>
                                                        </li>
                                                        <li style="vertical-align: top">
                                                            <input type="button" value="Add Item" runat="server" id="btnAddItem" onclick="return GetSelectedItems(true);" style="padding: 4px;" class="btn btn-primary" />
                                                        </li>
                                                        <li style="vertical-align: top">
                                                            <asp:Button ID="btnMultiSelect" Text="Multi-Select" CssClass="btn btn-primary" Style="padding: 4px;" OnClientClick="return ValidateOnMultiSelect(this);" runat="server"></asp:Button>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="col-3-12" style="padding-top: 7px;">
                                                    <asp:UpdatePanel ID="updatePanelItemTransit" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                                        <ContentTemplate>
                                                            <asp:HyperLink ID='hplInTransit' runat="server" CssClass="hyperlink" NavigateUrl="#" TabIndex="505" Visible="false"></asp:HyperLink>
                                                            <asp:LinkButton ID='hplRelatedItems' runat="server" CssClass="hyperlink" Style="padding-left: 20px" Visible="false" Text="Related Items" NavigateUrl="#" TabIndex="512"></asp:LinkButton>
                                                            <asp:LinkButton ID="hplLastPrices" runat="server" CssClass="hyperlink" Style="padding-left: 20px" Visible="false">Price History</asp:LinkButton>
                                                        </ContentTemplate>
                                                    </asp:UpdatePanel>
                                                </div>
                                            </div>
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                            <asp:UpdatePanel runat="server" ID="UpdatePanelItems" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="radWareHouse" />
                                    <asp:AsyncPostBackTrigger ControlID="txtCustomerPartNo" />
                                    <asp:AsyncPostBackTrigger ControlID="txtunits" />
                                    <asp:AsyncPostBackTrigger ControlID="txtPUnitCost" />
                                    <asp:AsyncPostBackTrigger ControlID="txtprice" />
                                    <asp:AsyncPostBackTrigger ControlID="btnUpdate" />
                                    <asp:AsyncPostBackTrigger ControlID="btnCalculateShipping" />
                                    <asp:AsyncPostBackTrigger ControlID="btnEditCancel" />
                                    <%--<asp:AsyncPostBackTrigger ControlID="imgBtnAddShippingItem" />--%>
                                    <asp:AsyncPostBackTrigger ControlID="chkWorkOrder" />
                                    <asp:AsyncPostBackTrigger ControlID="radcmbPriceLevel" />
                                    <asp:AsyncPostBackTrigger ControlID="radcmbUOM" />
                                    <asp:AsyncPostBackTrigger ControlID="radcmbShippingMethod" />
                                    <asp:AsyncPostBackTrigger ControlID="btnChangedProfit" />
                                    <asp:AsyncPostBackTrigger ControlID="lkbPriceLevel" />
                                    <asp:AsyncPostBackTrigger ControlID="lkbPriceRule" />
                                    <asp:AsyncPostBackTrigger ControlID="lkbLastPrice" />
                                    <asp:AsyncPostBackTrigger ControlID="lkbListPrice" />
                                    <asp:AsyncPostBackTrigger ControlID="lkbItemRemoved" />
                                    <asp:AsyncPostBackTrigger ControlID="btnDeleteItem" />
                                    <asp:AsyncPostBackTrigger ControlID="btnRemovePromotion" />
                                    <asp:AsyncPostBackTrigger ControlID="chkDropShip" />
                                    <asp:AsyncPostBackTrigger ControlID="lkbAssemblyDetail" />
                                </Triggers>
                                <ContentTemplate>
                                    <div class="cont1 col-1-1" style="border: 0px !important;padding: 0px;"  id="divCustomerPart" runat="server">
                                        <div class="col-md-12" style="margin-left: 6.5%;">
                                            <table>
                                                <tr>
                                                    <td></td>
                                                    <td colspan="6">
                                                        <table style="width: 450px;">
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="chkAutoCheckCustomerPart" runat="server" Text="Customer Part#" /></td>
                                                                <td>
                                                                    <asp:Label Font-Bold="true" ID="lblCustomerPartNo" Style="padding-left: 23px;" Text="Create Customer Part#" runat="server"></asp:Label></td>
                                                                <td>
                                                                    <asp:TextBox ID="txtCustomerPartNo" runat="server" TabIndex="507" Style="width: 100%"></asp:TextBox></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div id="divMultiSelect" visible="false" runat="server">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gvMultiSelectItems" HeaderStyle-CssClass="gridHeader" FooterStyle-CssClass="gridFooter"
                                                        OnRowDataBound="gvMultiSelectItems_RowDataBound" OnRowDeleting="gvMultiSelectItems_RowDeleting" OnRowCommand="gvMultiSelectItems_RowCommand"
                                                        runat="server" RowStyle-CssClass="gridViewRow" AlternatingRowStyle-CssClass="gridViewAlternateRow" PageSize="10" AutoGenerateColumns="false"
                                                        AllowPaging="true" OnPageIndexChanging="gvMultiSelectItems_PageIndexChanging">
                                                        <Columns>
                                                            <asp:TemplateField HeaderStyle-Width="20%" ItemStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <label>Item Name</label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:HiddenField ID="hdnID" Value='<%# DataBinder.Eval(Container.DataItem, "ID")%>' runat="server" />
                                                                    <asp:HiddenField ID="hdnItemCode" Value='<%# DataBinder.Eval(Container.DataItem, "numItemCode")%>' runat="server" />
                                                                    <asp:HiddenField ID="hdnItemName" Value='<%# DataBinder.Eval(Container.DataItem, "ItemName")%>' runat="server" />
                                                                    <asp:HiddenField ID="hdnAttr" Value='<%# DataBinder.Eval(Container.DataItem, "Attr")%>' runat="server" />
                                                                    <asp:Label ID="lblMultiSelectItemNAttr" runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <label>Units</label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtMultiSelectUnits" AutoPostBack="true" OnTextChanged="txtMultiSelectUnits_TextChanged" Text='<%# DataBinder.Eval(Container, "DataItem.numUnitHour", "{0:#,##0.00}") %>' Width="90%" runat="server"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderStyle-Width="10%" ItemStyle-HorizontalAlign="Center">
                                                                <HeaderTemplate>
                                                                    <label>Unit Price</label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:TextBox ID="txtMultiSelectUnitPrice" AutoPostBack="true" OnTextChanged="txtMultiSelectUnitPrice_TextChanged" Text='<%# DataBinder.Eval(Container, "DataItem.numUnitHour", "{0:#,##0.00}") %>' Width="90%" runat="server"></asp:TextBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="15%">
                                                                <HeaderTemplate>
                                                                    <label>Item Release Date</label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <telerik:RadDatePicker ID="rdMultiSelectItemReleaseDate" AutoPostBack="true" runat="server" Width="90%" OnSelectedDateChanged="rdMultiSelectItemReleaseDate_SelectedDateChanged"></telerik:RadDatePicker>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderStyle-Width="25%">
                                                                <HeaderTemplate>
                                                                    <label>Ship-From</label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <telerik:RadComboBox ID="rdcmbMultiSelectShipFrom" OnItemDataBound="rdcmbMultiSelectShipFrom_ItemDataBound" Width="95%" ExpandDirection="Down" runat="server" DropDownWidth="800px"
                                                                        AccessKey="W" AutoPostBack="true" DropDownCssClass="multipleRowsColumns" OnClientLoad="onRadComboBoxLoad" OnSelectedIndexChanged="rdcmbMultiSelectShipFrom_SelectedIndexChanged">
                                                                        <HeaderTemplate>
                                                                            <ul>
                                                                                <li class="col1">Warehouse</li>
                                                                                <li class="col2">Attributes</li>
                                                                                <li class="col3">On Hand</li>
                                                                                <li class="col4">On Order</li>
                                                                                <li class="col5">On Allocation</li>
                                                                                <li class="col6">BackOrder</li>
                                                                            </ul>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <ul>
                                                                                <li style="display: none">
                                                                                    <%# DataBinder.Eval(Container.DataItem, "numWareHouseItemId")%></li>
                                                                                <li class="col1">
                                                                                    <%# DataBinder.Eval(Container.DataItem, "vcWareHouse")%></li>
                                                                                <li class="col2">
                                                                                    <%# DataBinder.Eval(Container.DataItem, "Attr") %>&nbsp;</li>
                                                                                <li class="col3">
                                                                                    <%# DataBinder.Eval(Container.DataItem, "numOnHand")%>&nbsp;
                                                                            <%# DataBinder.Eval(Container.DataItem, "vcUnitName")%></li>
                                                                                <li class="col4">
                                                                                    <%# DataBinder.Eval(Container.DataItem, "numOnOrder")%>
                                                                            &nbsp;</li>
                                                                                <li class="col5">
                                                                                    <%# DataBinder.Eval(Container.DataItem, "numAllocation")%>&nbsp;</li>
                                                                                <li class="col6">
                                                                                    <%# DataBinder.Eval(Container.DataItem, "numBackOrder")%>&nbsp;
                                                                        <asp:Label runat="server" ID="lblWareHouseID" Text='<%#DataBinder.Eval(Container.DataItem, "numWareHouseID")%>'
                                                                            Style="display: none"></asp:Label>
                                                                                    <asp:Label runat="server" ID="lblWarehouse" Text='<%#DataBinder.Eval(Container.DataItem, "vcWareHouse")%>'
                                                                                        Style="display: none"></asp:Label>

                                                                                    <asp:Label runat="server" ID="lblOnHandAllocation" Style="display: none"></asp:Label>
                                                                                </li>
                                                                            </ul>
                                                                        </ItemTemplate>
                                                                    </telerik:RadComboBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderStyle-Width="20%">
                                                                <HeaderTemplate>
                                                                    <label>Ship-To</label>
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <telerik:RadComboBox ID="rdcmbMultiSelectShipTo" AutoPostBack="true" OnSelectedIndexChanged="rdcmbMultiSelectShipTo_SelectedIndexChanged" Width="95%" ExpandDirection="Down" runat="server"></telerik:RadComboBox>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderStyle-Width="5%">
                                                                <ItemTemplate>
                                                                    <asp:ImageButton ID="imgDeleteMultiSelectItem" CommandName="Delete" CommandArgument="<%# Container.DataItemIndex %>" runat="server" ImageUrl="~/images/delete2.gif" />

                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center">
                                                    <div>
                                                        <asp:Button ID="btnAddtoOpp" OnClick="btnAddtoOpp_Click" CssClass="btn btn-primary" runat="server" Text="Add to Opportunity/Order & Close" />
                                                        <asp:Button ID="btnAddMoreItems" OnClientClick="return ValidateOnMultiSelect(this);" CssClass="btn btn-primary" runat="server" Text="Add more Items" />
                                                        <asp:Button ID="btnClearItems" OnClick="btnClearItems_Click" CssClass="btn btn-primary" runat="server" Text="Clear Items & Start Over" />
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="cont1 itemsArea" style="border: 0px !important; border: 0px; display: none; visibility: hidden;">

                                        <div class="col-1-1">
                                            <table border="0" width="100%">
                                                <tr>
                                                    <td style="text-align: right;" class="middlealign">
                                                        <label>
                                                            Quantity<span class="required">*</span>
                                                        </label>
                                                    </td>
                                                    <td class="middlealign" style="width: 180px">
                                                        <div class="form-group" style="margin-bottom: 0px;">
                                                            <asp:TextBox ID="txtUnits" runat="server" CssClass="col-3-12" Width="50px" Style="margin-right: 3px;" AutoPostBack="true" TabIndex="302"></asp:TextBox>
                                                            <telerik:RadComboBox ID="radcmbUOM" runat="server" CssClass="form-control col-9-12" ClientIDMode="Static" AutoPostBack="true" TabIndex="518" OnClientLoad="onRadComboBoxLoad" Width="120"></telerik:RadComboBox>

                                                            <asp:HiddenField ID="hdnItemSalePrice" runat="server" />
                                                            <asp:TextBox ID="txtOldUnits" Style="display: none" runat="server" />
                                                            <asp:TextBox ID="txtnumUnitHourReceived" runat="server" Style="display: none" />
                                                            <asp:TextBox ID="txtnumQtyShipped" runat="server" Style="display: none" />
                                                            <asp:TextBox ID="txtUOMConversionFactor" runat="server" Style="display: none" />
                                                            <asp:HiddenField ID="hdnNewItemPromotionID" runat="server" />
                                                        </div>
                                                    </td>
                                                    <td style="text-align: left; width: 194px;">
                                                        <div class="form-group" style="text-align: left; margin-bottom: 0px;">
                                                            <label style="float: left; width: 40px;" class="col-1-3">
                                                                Price
                                                            </label>
                                                            <div>
                                                                <asp:TextBox ID="txtprice" runat="server" CssClass="wid100 float-left" MaxLength="38" AutoPostBack="true" TabIndex="303"></asp:TextBox>
                                                                <span class="textdiv">/
                                                            <asp:Label ID="lblBaseUOMName" runat="server" Text="-"></asp:Label></span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="middlealign" style="text-align: left;">
                                                        <div class="col-md-8">
                                                            <asp:Panel ID="pnlDiscount" Style="text-align: left;" runat="server">
                                                                <div class="form-group" style="margin-bottom: 0px;">
                                                                    <div class="col-1-3" style="width: 31%;">
                                                                        <asp:DropDownList Style="float: left; width: 84px; height: 23px;" ID="ddlMarkupDiscountOption" runat="server">
                                                                            <asp:ListItem Text="- Discount" Value="0"></asp:ListItem>
                                                                            <asp:ListItem Text="+ Markup" Value="1"></asp:ListItem>
                                                                        </asp:DropDownList>&nbsp;
                                                                    </div>
                                                                    <div class="col-9-12" style="width: 69%">
                                                                        <asp:TextBox ID="txtItemDiscount" runat="server" CssClass="float-left" Width="50px" TabIndex="304"></asp:TextBox>
                                                                        <label class="radio float-left textdiv" style="width: 34px !important; padding: 0px; margin: 0px; margin-left: 30px; margin-top: 5px;">
                                                                            <asp:RadioButton ID="radPer" runat="server" GroupName="radDiscount" Checked="true" Text="%" TabIndex="305" />
                                                                        </label>
                                                                        <label class="radio float-left textdiv" style="float: left; margin-left: 10px; margin-top: -1px;">
                                                                            <asp:RadioButton ID="radAmt" runat="server" GroupName="radDiscount" Text="Amount Entered" TabIndex="521" />
                                                                        </label>
                                                                        <asp:CheckBox ID="chkUsePromotion" runat="server" Text="Promotion" style="margin-top:10px;margin-left:10px;" />
                                                                    </div>
                                                                </div>
                                                            </asp:Panel>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group" style="text-align: left; margin-bottom: 0px; padding-top: 9px;">
                                                                <asp:CheckBox ID="chkDropShip" Text="Drop Ship" runat="server" Style="padding-left: 20px" AutoPostBack="true" TabIndex="506" />&nbsp;&nbsp;
                                                            <asp:Label ID="lblItemType" Font-Bold="true" Style="font-style: italic" runat="server"></asp:Label>
                                                                <telerik:RadComboBox ID="radcmbType" Width="100%" Visible="false" runat="server" Enabled="False" TabIndex="503" OnClientLoad="onRadComboBoxLoad">
                                                                    <Items>
                                                                        <telerik:RadComboBoxItem Text="Inventory Item" Value="P" />
                                                                        <telerik:RadComboBoxItem Text="Non-Inventory Item" Value="N" />
                                                                        <telerik:RadComboBoxItem Text="Service" Value="S" />
                                                                    </Items>
                                                                </telerik:RadComboBox>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="middlealign"></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: right">
                                                        <label style="padding-right: 5px;">Description</label>
                                                    </td>
                                                    <td colspan="2">
                                                        <asp:TextBox ID="txtdesc" Width="400px" runat="server" TextMode="MultiLine" TabIndex="504"></asp:TextBox>
                                                    </td>
                                                    <td colspan="2" class="middlealign">
                                                        <label style="float: left; padding-right: 5px; width: 40px; margin-left: 13px;" class="col-1-3">Notes</label>
                                                        <asp:TextBox ID="txtNotes" Width="400px" runat="server" TextMode="MultiLine" TabIndex="504"></asp:TextBox>
                                                    </td>
                                                </tr>

                                            </table>
                                        </div>
                                        <div class="col-1-1 " id="divWorkOrder" style="margin-left: 62px;" runat="server" visible="false">
                                            <asp:HiddenField ID="hdnIsCreateWO" runat="server" Value="0" />
                                            <ul class="list-inline">
                                                <li style="vertical-align: top; padding-left: 0px; padding-right: 0px;">
                                                    <asp:LinkButton ID="lkbAssemblyDetail" runat="server" CssClass="btn btn-xs btn-primary btn-flat" Text="Details" />
                                                </li>
                                                <li style="vertical-align: top; padding-left: 0px; padding-right: 0px; margin-top: 2px;">
                                                    <asp:CheckBox ID="chkWorkOrder" runat="server" TabIndex="508" /></li>
                                                <li style="vertical-align: top; padding-left: 0px; padding-right: 0px; margin-top: 5px;">Create WO</li>
                                                <li style="vertical-align: top; padding-left: 0px; padding-right: 0px; margin-top: 5px;">For
                                                </li>
                                                <li style="vertical-align: top; padding-left: 0px; padding-right: 0px">
                                                    <asp:TextBox ID="txtWOQty" runat="server" Width="60"></asp:TextBox>
                                                </li>
                                                <li style="vertical-align: top; padding-left: 0px; padding-right: 0px; margin-top: 5px;">qty</li>
                                                <li style="vertical-align: top; padding-left: 0px; padding-right: 0px; margin-top: 5px; text-align: right">
                                                    <div>Planned Start<span style="color: red"> *</span></div>
                                                    <br />
                                                    <div>Requested Finish<span style="color: red"> *</span></div>
                                                </li>
                                                <li style="vertical-align: top; padding-left: 0px; padding-right: 0px">
                                                    <telerik:RadDatePicker ID="rdpPlannedStart" runat="server" DateInput-DateFormat="MM/dd/yyyy" TabIndex="515">
                                                        <Calendar>
                                                            <SpecialDays>
                                                                <telerik:RadCalendarDay Repeatable="Today" ItemStyle-CssClass="rcToday">
                                                                </telerik:RadCalendarDay>
                                                            </SpecialDays>
                                                        </Calendar>
                                                    </telerik:RadDatePicker>
                                                    <br />
                                                    <telerik:RadDatePicker ID="radCalCompliationDate" runat="server" DateInput-DateFormat="MM/dd/yyyy" TabIndex="515">
                                                        <Calendar>
                                                            <SpecialDays>
                                                                <telerik:RadCalendarDay Repeatable="Today" ItemStyle-CssClass="rcToday">
                                                                </telerik:RadCalendarDay>
                                                            </SpecialDays>
                                                        </Calendar>
                                                    </telerik:RadDatePicker>
                                                </li>

                                                <li style="vertical-align: top; padding-left: 0px; padding-right: 0px;">
                                                    <b>Max WOs:</b>
                                                    <asp:Label ID="lblWOQty" runat="server" Text="" Style="line-height: 27px"></asp:Label>
                                                </li>
                                                <li style="vertical-align: top; padding-left: 0px; padding-right: 0px; margin-top: 5px;"></li>
                                                <li style="vertical-align: top; padding-left: 0px; padding-right: 0px"></li>

                                                <li style="vertical-align: top; padding-left: 0px; padding-right: 0px; margin-top: 5px;">Comments
                                                </li>
                                                <li style="vertical-align: top; padding-left: 0px; padding-right: 0px">
                                                    <asp:TextBox ID="txtInstruction" runat="server" TextMode="MultiLine" Rows="3" Columns="39" MaxLength="1000" TabIndex="514"></asp:TextBox>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-1-1" style="margin-top: 5px;">
                                            <div class="col-1-3">
                                                <div class="form-group" id="divUnitCost" runat="server">
                                                    <label class="col-3-12 unit-cost">
                                                        <asp:HyperLink ID="hplUnitCost" NavigateUrl="#" CssClass="hyperlink" runat="server" TabIndex="524">Unit-Cost</asp:HyperLink></label>
                                                    <div class="col-9-12">
                                                        <asp:TextBox ID="txtPUnitCost" runat="server" TabIndex="525" Width="50" AutoPostBack="true"></asp:TextBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-1-3">
                                                <div class="form-group" id="divProfit" runat="server" visible="false">
                                                    <label class="col-3-12">Profit - Unit/Total</label>
                                                    <div class="col-9-12">
                                                        <div style="float: left">
                                                            <asp:Label ID="lblProfit" class="textdiv" runat="server"></asp:Label>
                                                        </div>
                                                        <div style="float: left" class="textdiv">&nbsp;(&nbsp;</div>
                                                        <div style="float: left" class="textdiv">
                                                            <asp:HyperLink ID="hlProfitPerc" runat="server"></asp:HyperLink>
                                                            <asp:Button ID="btnChangedProfit" runat="server" CssClass="cancel" Style="display: none"></asp:Button>
                                                        </div>
                                                        <div style="float: left" class="textdiv">&nbsp;)</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-1-3">
                                            </div>
                                        </div>

                                        <div class="col-1-1">
                                            <div class="col-1-3">
                                                <div class="form-group" id="divPriceLevel" runat="server" visible="false">
                                                    <label class="col-3-12">Price Level</label>
                                                    <div class="col-9-12">
                                                        <telerik:RadComboBox ID="radcmbPriceLevel" Width="100%" runat="server" AutoPostBack="true" TabIndex="507" OnClientLoad="onRadComboBoxLoad">
                                                            <Items>
                                                                <telerik:RadComboBoxItem Text="--Select One--" Value="0" />
                                                            </Items>
                                                        </telerik:RadComboBox>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-1-1 text-center" style="padding-top: 10px; padding-bottom: 10px;" id="divPricingOption" runat="server" visible="false">
                                            <asp:LinkButton ID="lkbPriceLevel" runat="server" CssClass="btn btn-flat bg-purple" OnClientClick="return CheckQuantityAdded();">Use Price Level</asp:LinkButton>
                                            <asp:LinkButton ID="lkbPriceRule" runat="server" CssClass="btn btn-flat bg-orange" OnClientClick="return CheckQuantityAdded();">Use Price Rule</asp:LinkButton>
                                            <asp:LinkButton ID="lkbLastPrice" runat="server" CssClass="btn btn-flat bg-olive" OnClientClick="return CheckQuantityAdded();">Use Last Price</asp:LinkButton>
                                            <asp:LinkButton ID="lkbListPrice" runat="server" CssClass="btn btn-flat bg-aqua" OnClientClick="return CheckQuantityAdded();">Use List Price</asp:LinkButton>
                                            <asp:LinkButton ID="lkbEditKitSelection" CssClass="btn btn-flat bg-maroon" Visible="false" runat="server" Text="Edit Kit Selection" OnClientClick="return OpenKitSelectionWindow();"></asp:LinkButton>

                                            <asp:HiddenField ID="hdnPricingBasedOn" runat="server" />
                                            <asp:HiddenField ID="hdnListPrice" runat="server" />
                                            <asp:HiddenField ID="hdnLastPrice" runat="server" />
                                            <asp:HiddenField ID="hdnLastOrderDate" runat="server" />
                                        </div>
                                        <div class="col-1-1 text-center">
                                        </div>
                                        <asp:Panel ID="PanelWarehouse" runat="server">
                                            <div id="divWarehouse" style="display: none;" class="form-group" runat="server">
                                                <label class="col-3-12 ">Location<span class="required">*</span></label>
                                                <div class="col-9-12">
                                                    <telerik:RadComboBox ID="radWareHouse" runat="server" Width="100%" DropDownWidth="800px"
                                                        AccessKey="W" AutoPostBack="true" ClientIDMode="Static" DropDownCssClass="multipleRowsColumns" TabIndex="500" OnClientLoad="onRadComboBoxLoad">
                                                        <HeaderTemplate>
                                                            <ul>
                                                                <li class="col1">Warehouse</li>
                                                                <li class="col2">Attributes</li>
                                                                <li class="col3">On Hand</li>
                                                                <li class="col4">On Order</li>
                                                                <li class="col5">On Allocation</li>
                                                                <li class="col6">BackOrder</li>
                                                            </ul>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <ul>
                                                                <li style="display: none">
                                                                    <%# DataBinder.Eval(Container.DataItem, "numWareHouseItemId")%></li>
                                                                <li class="col1">
                                                                    <%# DataBinder.Eval(Container.DataItem, "vcWareHouse")%></li>
                                                                <li class="col2">
                                                                    <%# DataBinder.Eval(Container.DataItem, "Attr") %>&nbsp;</li>
                                                                <li class="col3">
                                                                    <%# DataBinder.Eval(Container.DataItem, "numOnHand")%>&nbsp;
                                                                            <%# DataBinder.Eval(Container.DataItem, "vcUnitName")%></li>
                                                                <li class="col4">
                                                                    <%# DataBinder.Eval(Container.DataItem, "numOnOrder")%>
                                                                            &nbsp;</li>
                                                                <li class="col5">
                                                                    <%# DataBinder.Eval(Container.DataItem, "numAllocation")%>&nbsp;</li>
                                                                <li class="col6">
                                                                    <%# DataBinder.Eval(Container.DataItem, "numBackOrder")%>&nbsp;
                                                                        <asp:Label runat="server" ID="lblWareHouseID" Text='<%#DataBinder.Eval(Container.DataItem, "numWareHouseID")%>'
                                                                            Style="display: none"></asp:Label>
                                                                    <asp:Label runat="server" ID="lblOnHandAllocation" Style="display: none"></asp:Label>
                                                                </li>
                                                            </ul>
                                                        </ItemTemplate>
                                                    </telerik:RadComboBox>
                                                </div>
                                            </div>
                                        </asp:Panel>
                                        <div>
                                            <div id="divLocations" runat="server">
                                                <asp:UpdatePanel ID="UPMatrixAndKitAttributes" runat="server" UpdateMode="Conditional">
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="btnSaveCloseAttributes" />
                                                        <asp:AsyncPostBackTrigger ControlID="btnSaveCloseKit" />
                                                    </Triggers>
                                                    <ContentTemplate>
                                                        <asp:Label ID="lblMatrixAndKitAttributes" Font-Bold="true" runat="server"></asp:Label>
                                                        <div>
                                                            <asp:GridView ID="gvLocations" RowStyle-CssClass="gvItemsrow" CssClass="table table-bordered tblPrimary" AlternatingRowStyle-CssClass="gvItemsAlternaterow" runat="server" Width="100%" HeaderStyle-CssClass="gridHeader" FooterStyle-CssClass="gridFooter" AutoGenerateColumns="false"
                                                                OnPageIndexChanging="gvLocations_PageIndexChanging" OnRowDataBound="gvLocations_RowDataBound" DataKeyNames="numWareHouseItemId,numOnHand,bitWarehouseMapped" AllowPaging="true" PageSize="10">
                                                                <Columns>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="2%">
                                                                        <HeaderTemplate>
                                                                            <asp:CheckBox ID="chkAllLocations" onclick="SelectAll('chkAllLocations','chk')" runat="server" />
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:CheckBox ID="chkLocations" CssClass="chk" runat="server" />
                                                                            <asp:HiddenField ID="hdnOnHand" runat="server" Value='<%# Eval("numOnHand") %>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField  HeaderStyle-Width="7%">
                                                                        <HeaderTemplate>
                                                                            <b>Available</b><i> (On-Hand)</i>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <b style="color:#00a65a;"><%# Eval("numOnHand")%></b> (<i><%# Convert.ToDouble(Eval("numOnHand")) + Convert.ToDouble(Eval("numAllocation"))%></i>)
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField HeaderText="Allocation" HeaderStyle-Width="5%" DataField="numAllocation" />
                                                                    <asp:TemplateField HeaderText="On-Order" HeaderStyle-Width="7%">
                                                                        <ItemTemplate>
                                                                            <a id="hplOnOrder" style="color:blue;text-decoration:underline" onclick='OpenInTransit(<%# Eval("numItemID")%>,<%# Eval("numWareHouseItemId")%>)' ><%# Eval("numOnOrder") %></a>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField HeaderText="Back Order" HeaderStyle-Width="8%" DataField="numBackOrder" />
                                                                    <asp:TemplateField HeaderStyle-Width="8%" ItemStyle-HorizontalAlign="Center">
                                                                        <HeaderTemplate>
                                                                            <label>Qty</label>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:TextBox ID="txtLocationsQty" Width="90%" runat="server"></asp:TextBox>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="12%">
                                                                        <HeaderTemplate>
                                                                            <label>Item Release Date</label>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblReleaseDateLocation" runat="server"></asp:Label>
                                                                            <telerik:RadDatePicker ID="rdLocationItemReleaseDate" runat="server" Width="90%"></telerik:RadDatePicker>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField ItemStyle-CssClass="hidecolumn" HeaderStyle-CssClass="hidecolumn" DataField="vcWareHouse" />
                                                                    <asp:TemplateField HeaderStyle-Width="25%">
                                                                        <HeaderTemplate>
                                                                            <label>Ship From</label>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="lblShipFromAttr" runat="server"></asp:Label>
                                                                            <asp:HiddenField ID="hdnAttr" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "Attr")%>' />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderStyle-Width="26%">
                                                                        <HeaderTemplate>
                                                                            <label>Ship To</label>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <telerik:RadComboBox ID="rdcmbShipToLocation" Width="100%" ExpandDirection="Down" runat="server"></telerik:RadComboBox>
                                                                            <asp:HiddenField ID="hdnWareHouseID" Value='<%# DataBinder.Eval(Container.DataItem, "numWareHouseID")%>' runat="server" />
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <PagerStyle CssClass="pager" HorizontalAlign="Right" />
                                                            </asp:GridView>
                                                        </div>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>

                                        <asp:HiddenField ID="hdnBaseUOMName" runat="server" />
                                        <asp:HiddenField ID="hdnVendorCost" runat="server" />
                                        <asp:HiddenField ID="hdnItemClassification" runat="server" />
                                        <asp:UpdatePanel ID="UpdatePanelItemDetials" runat="server" UpdateMode="Conditional" class="col-1-1">
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="lkbItemSelected" />
                                                <asp:AsyncPostBackTrigger ControlID="lkbItemRemoved" />
                                            </Triggers>
                                            <ContentTemplate>
                                                <asp:PlaceHolder ID="plhItemDetails" runat="server"></asp:PlaceHolder>
                                                <asp:LinkButton ID="lkbItemSelected" runat="server" Style="display: none"></asp:LinkButton>
                                                <asp:LinkButton ID="lkbItemRemoved" runat="server" Style="display: none"></asp:LinkButton>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                        <div class="col-1-1 text-center">
                                            <asp:Button ID="btnUpdate" runat="server" CssClass="OrderButton" Text="Add" TabIndex="526" Style="display: none" OnClientClick="return GetSelectedItems(false);"></asp:Button>
                                            <asp:Button ID="btnEditCancel" runat="server" Text="Cancel" Style="display: none" CssClass="OrderButton" TabIndex="527"></asp:Button>
                                        </div>
                                    </div>




                                    <asp:HiddenField ID="hdnDeleteItemID" runat="server" />
                                    <asp:Button ID="btnDeleteItem" runat="server" Text="" Style="display: none;" />

                                    <div id="divItemGrid" class="cont1 no-pad" style="border-top: 0px; padding-top: 40px !important;">
                                        <div class="col-1-1">
                                            <div class="pull-right">
                                                <a href="javascript:void(0)" class="btn btn-primary" id="btnLeadTimesNeedPO" runat="server" visible="false">Lead-times for items that need POs</a>
                                            </div>
                                            <asp:DataGrid ID="dgItems" runat="server" CssClass="tblPrimary" UseAccessibleHeader="true" FooterStyle-CssClass="gridFooter" ItemStyle-CssClass="gridItem" ShowFooter="true" AutoGenerateColumns="false" AlternatingItemStyle-CssClass="gridAlternateItem"
                                                DataKeyField="numItemCode"
                                                ClientIDMode="AutoID" Width="100%" TabIndex="529" CellPadding="5" ShowHeader="true">
                                                <%--ItemStyle-CssClass="gridItem" --%>
                                                <Columns>
                                                    <asp:TemplateColumn HeaderText="Line" ItemStyle-CssClass="gridItem" ItemStyle-Wrap="false" FooterStyle-Wrap="false">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtRowOrder" onkeypress="CheckNumber(2,event)" AutoPostBack="true" OnTextChanged="txtRowOrder_TextChanged" Width="40" Text='<%# DataBinder.Eval(Container, "DataItem.numSortOrder") %>' runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:LinkButton ID="lnkBtnUpdateOrder" Style="color: #fff;" OnClick="lnkBtnUpdateOrder_Click" runat="server">A-Z</asp:LinkButton>
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn HeaderText="Item ID" DataField="numItemCode" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"></asp:BoundColumn>
                                                    <asp:TemplateColumn ItemStyle-CssClass="duplicateitem" ItemStyle-Width="100%" HeaderStyle-Wrap="false">
                                                        <ItemTemplate>
                                                            <table width="100%">
                                                                <tr>
                                                                    <td style="width: 98%">
                                                                        <asp:TextBox runat="server" ID="txtItemName" Style="width: 250px !important; height: 25px !important;" MaxLength="300" Text='<%# DataBinder.Eval(Container, "DataItem.vcItemName") %>' />
                                                                    </td>
                                                                    <td style="width: 2%; height: 20px !important; width: 20px !important;" class="duplicateitem">
                                                                        <asp:ImageButton ID="imgDuplicateLineItem" OnClick="imgDuplicateLineItem_Click" CssClass="duplicateitem" ImageUrl="~/images/CloneDoc.png" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblItemName" runat="server">Item</asp:Label>
                                                        </HeaderTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn HeaderText="SKU" DataField="vcSKU" ItemStyle-Wrap="false"></asp:BoundColumn>
                                                    <asp:TemplateColumn HeaderText="Units" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtUnits" CssClass="signup" Width="40" runat="server" AutoPostBack="true" OnTextChanged="txtUnitsGrid_TextChanged" Text='<%# DataBinder.Eval(Container, "DataItem.numUnitHour", "{0:#,##0.00}") %>'></asp:TextBox>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="lblSaleUOMName" runat="server" Text='<%# " " & DataBinder.Eval(Container, "DataItem.vcUOMName")%>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <asp:TextBox ID="txtOldUnits" CssClass="signup" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.numUnitHour") %>'></asp:TextBox>
                                                            <asp:TextBox ID="txtUOMConversionFactor" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container,"DataItem.UOMConversionFactor") %>'></asp:TextBox>
                                                            <asp:TextBox ID="txtnumUnitHourReceived" runat="server" Style="display: none" Text='<%#Eval("numUnitHourReceived")%>' />
                                                            <asp:TextBox ID="txtnumQtyShipped" runat="server" Style="display: none" Text='<%#Eval("numQtyShipped")%>' />
                                                            <asp:TextBox ID="txtItemWeight" runat="server" Style="display: none" Text='<%#Eval("fltItemWeight")%>' />
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn ItemStyle-Wrap="false" HeaderStyle-Wrap="false" HeaderText="Unit List Price">
                                                        <HeaderTemplate>
                                                            <asp:Label ID="lblUnitPriceCaption" runat="server">Unit List Price</asp:Label>
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <table border="0" cellpadding="0" cellspacing="0" style="white-space: nowrap;">
                                                                <tr>
                                                                    <td>
                                                                        <asp:TextBox ID="txtUnitPrice" CssClass="signup" Width="40" runat="server"></asp:TextBox>
                                                                        <asp:HiddenField ID="hdnUnitPrice" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.monPrice")%>' />
                                                                    </td>
                                                                    <td>/
                                                        <asp:Label ID="lblUOMName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcBaseUOMName")%>'></asp:Label>
                                                                        <asp:Label ID="lblItemUOM" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcUOMName")%>'></asp:Label>
                                                                    </td>
                                                                    <td>&nbsp;<asp:Label ID="lblUnitPriceApproval" runat="server" ForeColor="Red" Font-Bold="true" Text='<%# IIf(Not IsDBNull(DataBinder.Eval(Container, "DataItem.bitItemPriceApprovalRequired")) AndAlso DataBinder.Eval(Container, "DataItem.bitItemPriceApprovalRequired") = "True", "!", "")%>' />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn ItemStyle-Wrap="false" HeaderStyle-Wrap="false" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="right" HeaderText="Unit Sale Price">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblUnitSalePrice" runat="server" ondblclick="DbClickUnitSalePrice(this)"></asp:Label>
                                                            <asp:TextBox ID="txtUnitSalePrice" runat="server" Width="40" Style="display: none;" onfocusout="HideSalePriceTextBox(this);"></asp:TextBox>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Sales Tax" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" ItemStyle-Wrap="false" HeaderStyle-Wrap="false" FooterStyle-Wrap="False">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblTaxAmt0" runat="server" CssClass="text" Text='<%# DataBinder.Eval(Container, "DataItem.TotalTax", "{0:#,##0.00}") %>'></asp:Label>
                                                            <asp:Label ID="lblTaxable0" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container, "DataItem.bitTaxable0", "{0:#,##0.00}") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lblFTaxAmt0" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Discount" ItemStyle-HorizontalAlign="Right" ItemStyle-Wrap="false" HeaderStyle-Wrap="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDiscount" runat="server" CssClass="text"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="Total" HeaderStyle-Wrap="false" FooterStyle-Wrap="False" FooterStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" ItemStyle-Wrap="false">
                                                        <ItemTemplate>
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label ID="lblTotal" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.monTotAmount", "{0:#,##0.00}") %>'></asp:Label>
                                                                    </td>
                                                                    <td>&nbsp;<asp:Label ID="lblAmountApproval" runat="server" ForeColor="Red" Font-Bold="true" Text='<%# IIf(Not IsDBNull(DataBinder.Eval(Container, "DataItem.bitItemPriceApprovalRequired")) AndAlso DataBinder.Eval(Container, "DataItem.bitItemPriceApprovalRequired") = "True", "!", "")%>' />
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                        <FooterTemplate>
                                                            <asp:Label ID="lblFTotal" runat="server" Text=""></asp:Label>
                                                        </FooterTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:BoundColumn HeaderText="Item Release Date" DataField="ItemReleaseDate" DataFormatString="{0:MM/dd/yyy}"></asp:BoundColumn>
                                                    <asp:BoundColumn HeaderText="Location" DataField="Location"></asp:BoundColumn>
                                                    <asp:BoundColumn HeaderText="Ship-To" DataField="ShipToFullAddress"></asp:BoundColumn>
                                                    <asp:BoundColumn HeaderText="On-Hand / Allocation" DataField="OnHandAllocation"></asp:BoundColumn>
                                                    <asp:BoundColumn HeaderText="Description" DataField="vcItemDesc"></asp:BoundColumn>
                                                    <asp:BoundColumn HeaderText="Notes" DataField="vcVendorNotes"></asp:BoundColumn>
                                                    <asp:BoundColumn HeaderText="Attributes" DataField="Attributes"></asp:BoundColumn>
                                                    <asp:BoundColumn HeaderText="Inclusion Detail" DataField="Attributes"></asp:BoundColumn>
                                                    <asp:BoundColumn HeaderText="Item Classification" DataField="numItemClassification"></asp:BoundColumn>

                                                    <asp:TemplateColumn HeaderText="Item Promotion" HeaderStyle-Wrap="false">
                                                        <ItemTemplate>
                                                            <ul class="list-inline" style="margin: 0px; max-width: 300px; display: inline-flex;" id="ulItemPromo" runat="server">
                                                                <li style="width: 85px">
                                                                    <img alt="" src='../images/ShoppingCart_Deal.png' /></li>
                                                                <li>
                                                                    <asp:LinkButton ID="lnkItemPromoDesc" runat="server"></asp:LinkButton></li>
                                                            </ul>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>

                                                    <asp:TemplateColumn ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20">
                                                        <ItemTemplate>
                                                            <asp:HiddenField ID="hdnPromotionID" runat="server" Value='<%#Eval("numPromotionID")%>' />
                                                            <asp:HiddenField ID="hdnIsPromotionTrigerred" runat="server" Value='<%#Eval("bitPromotionTriggered")%>' />
                                                            <asp:HiddenField ID="hdnUnitSalePrice" runat="server" />
                                                            <asp:HiddenField ID="hdnDiscountAmount" runat="server" />
                                                            <asp:HiddenField ID="hdnMarkupDiscount" runat="server" />
                                                            <asp:TextBox ID="txtDiscountType" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.bitDiscountType") %>'
                                                                Style="display: none"></asp:TextBox>
                                                            <asp:TextBox ID="txtTotalDiscount" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.fltDiscount", "{0:#,##0.00}") %>'
                                                                Style="display: none"></asp:TextBox>
                                                            <asp:LinkButton ID="lnkEdit" CommandName="Edit" CssClass="hyperlink" runat="server">
                                                                <asp:Image ID="Image3" runat="server" ImageUrl="~/images/pencil-24.gif" Height="15" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="lnkDelete" CommandName="Delete" CssClass="hyperlink" runat="server">
                                                                <asp:Image ID="Image4" runat="server" ImageUrl="~/images/delete2.gif" Height="15" />
                                                            </asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                    <asp:TemplateColumn HeaderText="No" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblOppItemCode" runat="server" CssClass="text" Text='<%#Eval("numoppitemtCode")%>'></asp:Label>
                                                            <asp:Label ID="lblID" runat="server" CssClass="text" Text='<%# "!!" + Eval("numItemCode").ToString() + "," + Eval("numWarehouseItmsID").ToString() + "," + Eval("DropShip").ToString() %>'
                                                                Style="display: none"></asp:Label>
                                                            <asp:Label ID="lblcharItemType" runat="server" CssClass="text" Text='<%#Eval("charItemType")%>'
                                                                Style="display: none"></asp:Label>
                                                            <asp:Label ID="lblnumProjectID" runat="server" CssClass="text" Text='<%#Eval("numProjectID")%>'
                                                                Style="display: none"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateColumn>
                                                </Columns>
                                            </asp:DataGrid>
                                        </div>
                                    </div>
                                    <div class="cont1" style="border-bottom: 0px solid black; padding-bottom: 0px;" id="divCouponCode" runat="server">
                                        <div style="float: right;">
                                            <table style="float: right;">
                                                <tr>
                                                    <td style="padding: 10px;">
                                                        <asp:Button ID="btnapplyCouponCode" OnClick="btnapplyCouponCode_Click" CssClass="OrderButton CheckCoupon" Style="width: 149px; float: left; text-align: left; padding-right: 10px; padding-left: 50px;" Text="Apply Code" runat="server" />
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtApplyCouponCode" runat="server"></asp:TextBox>
                                                        <asp:HiddenField ID="hdnApplyCouponCode" runat="server" />
                                                        <asp:Button ID="btnConfirmProm" OnClick="btnConfirmProm_Click" Style="display: none;" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="text-align: center;">
                                                        <asp:Label ForeColor="Red" ID="lblErrroCouponMsg" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="text-align: center;">
                                                        <asp:Label ForeColor="green" ID="lblCouponMsg" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                            </table>
                                            <asp:HiddenField ID="hdnOrderPromId" runat="server" />
                                            <asp:HiddenField ID="hdnShipPromId" runat="server" />
                                        </div>
                                    </div>
                                    <asp:UpdatePanel ID="UpdatePanelButtons" runat="server" style="border-top: 0px solid black; background-color: #fff !important;" class="cont1 text-center">
                                        <ContentTemplate>
                                            <asp:HiddenField ID="hdnKitChildItems" runat="server" />
                                            <div class="col-1-1">
                                                <asp:Button ID="btnSaveNew" CssClass="OrderButton SaveNew" Style="width: 149px; float: left; text-align: left; padding-right: 10px; padding-left: 50px;" runat="server" Text="Create Order & Start New One" TabIndex="549" />
                                                <asp:Button ID="btnSaveBizDoc" CssClass="OrderButton CreateInvoice" Style="width: 140px; float: left; margin-left: 5px; padding-right: 10px;" runat="server" Text="Create & Open Unpaid Invoice" TabIndex="547" />
                                                <asp:Button ID="btnSave" CssClass="OrderButton SaveOpenOrder" Width="140px" runat="server" Text="Save" TabIndex="546" />
                                                <asp:Button ID="btnAddToCart" CssClass="OrderButton AddtoSite" Style="width: 128px; text-align: center; padding-left: 40px; float: left; margin-left: 5px; padding-right: 10px;" runat="server" Text="Create Web Order" Visible="false" OnClientClick="return Save(1);" TabIndex="550" />
                                                <asp:Button ID="btnBulkSave" CssClass="OrderButton CreateInvoice" Style="width: 128px; padding-right: 8px; padding-left: 42px;" OnClientClick="return alertbulksales();" runat="server" Text="Create Mass Sales Orders" TabIndex="552" Visible="false" />
                                                <asp:Button ID="btnOptionsNAccessories" CssClass="OrderButton" runat="server" Text="Customize" Visible="false" TabIndex="528" />
                                                <asp:Button ID="btnRemovePromotion" runat="server" Text="" Style="display: none;" />

                                                <asp:Button ID="btnPay" OnClientClick="return showPaymentSection(0)" CssClass="OrderButton btnPay paybutton" Style="width: 63px; padding-left: 35px; padding-right: 10px; height: 56px;" runat="server" Text="Pay" TabIndex="549" />
                                                <asp:Button ID="btnPOSPay" OnClientClick="return showPaymentSection(1)" CssClass="OrderButton btnPOSPay paybutton" Style="width: 80px; padding-right: 10px; padding-left: 45px;" runat="server" Text="POS Pay" TabIndex="549" />
                                                <asp:HiddenField ID="hdnPosPayment" Value="0" runat="server" />
                                                <asp:Button ID="btnSaveOpenOrder" CssClass="OrderButton SaveOpenOrder" Style="width: 134px; text-align: center; padding-left: 38px; float: right; padding-right: 10px" runat="server" Text="Create & Open Order" TabIndex="548" />

                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </ContentTemplate>
                            </asp:UpdatePanel>

                        </div>
                    </div>
                </div>
            </div>
            <div class="col-1-1" style="color: red">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" id="btnPaymentCollapseButton" href="#PaymentsSection">Payments, Terms, & Shipping Section</a>
                        </h4>
                    </div>

                    <div id="PaymentsSection" class="accordion-body collapse in">
                        <div class="panel-body">
                            <asp:UpdatePanel ID="UpdatePanelShipping" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div id="divShipping" runat="server" class="col-1-1" visible="false">
                                        <div class="col-1-1">
                                            <div class="col-1-2">
                                                <div class="form-group" style="margin-bottom: 5px;">
                                                    <label class="col-2-12">Ship From</label>
                                                    <div class="col-10-12" style="padding-top: 7px">
                                                        <asp:Label ID="lblShipFrom" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                                <asp:HiddenField ID="hdnWillCallWarehouseID" runat="server" />
                                            </div>
                                            <div class="col-1-2">
                                                <div class="form-group" style="margin-bottom: 5px;">
                                                    <ul class="list-inline" style="margin-bottom: 0px;">
                                                        <li style="padding-left: 0px; padding-right: 0px">
                                                            <asp:CheckBox runat="server" ID="chkMarkupShippingCharges" /></li>
                                                        <li><b>Mark up shipping charges</b></li>
                                                        <li>
                                                            <asp:TextBox runat="server" ID="txtMarkupShippingRate" Width="35"></asp:TextBox></li>
                                                        <li>
                                                            <asp:RadioButtonList ID="rblMarkupType" RepeatLayout="Flow" RepeatDirection="Horizontal" runat="server">
                                                                <asp:ListItem Text="%" Selected="True" Value="1" />
                                                                <asp:ListItem Text="Flat Amt" Selected="True" Value="2" />
                                                            </asp:RadioButtonList>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-1-1">
                                            <div class="col-1-2">
                                                <div class="form-group" style="margin-bottom: 5px;">
                                                    <label class="col-2-12">Ship To</label>
                                                    <div class="col-10-12" style="padding-top: 7px">
                                                        <asp:Label ID="lblShipTo" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-1-2">
                                                <div class="form-group" style="margin-bottom: 5px;">
                                                    <ul class="list-inline" style="margin-bottom: 0px;">
                                                        <li style="padding-left: 0px; padding-right: 0px">
                                                            <asp:CheckBox runat="server" ID="chkUseCustomerShippingAccount" onchange="ValidateShipperAccount()" /></li>
                                                        <li><b>Use Customer's shipping account</b></li>
                                                        <li style="padding-right: 0px"><b>Total Weight:</b></li>
                                                        <li style="padding-left: 0px;">
                                                            <asp:TextBox ID="txtTotalWeight" runat="server" Width="50"></asp:TextBox></li>
                                                        <li style="padding-left: 0px; padding-right: 0px">lbs</li>
                                                    </ul>
                                                    <asp:HiddenField runat="server" ID="hdnShipperAccountNo" />
                                                </div>
                                            </div>
                                        </div>
                                        <asp:Panel ID="PanelShipping" runat="server">
                                            <div class="col-md-12">
                                                <ul class="nav nav-tabs" style="border-bottom: 0px; border-radius: 4px 4px 0px 0px;">
                                                    <li class="active"><a data-toggle="tab" href="#ddlParcel" aria-expanded="true">Parcel</a></li>
                                                    <div class="pull-right">
                                                        <div class="pull-left">
                                                            <label style="color: #fff;">Pickup Will-call</label>
                                                            <asp:CheckBox ID="chkPickUpWillCall" AutoPostBack="true" OnCheckedChanged="chkPickUpWillCall_CheckedChanged" TextAlign="Left" runat="server" />&nbsp;&nbsp;&nbsp;
                                                            <label style="color: #fff;">Ship Via</label>
                                                            <telerik:RadComboBox runat="server" ID="radShipVia" AccessKey="I" Skin="Vista" Height="130px" Width="130px" DropDownWidth="290"
                                                                EnableVirtualScrolling="false" CssClass="form-control" AutoPostBack="true" ChangeTextOnKeyBoardNavigation="false" ClientIDMode="Static" OnSelectedIndexChanged="radShipVia_SelectedIndexChanged">
                                                            </telerik:RadComboBox>
                                                        </div>
                                                        <div class="pull-left">
                                                            <label style="color: #fff; padding-left: 10px;">Shipping Estimates</label>
                                                            <telerik:RadComboBox ID="radCmbShippingMethod" CssClass="form-control" Width="300px" runat="server" TabIndex="530"></telerik:RadComboBox>
                                                        </div>
                                                        <div class="pull-left" style="padding-right: 10px">
                                                            <asp:Button ID="btnCalculateShipping" CssClass="btn btn-warning btn-flat btn-xs" Style="margin-top: 1px; margin-left: 2px;"
                                                                runat="server" Text="Get Rates" OnClientClick="javascript:return showShippingSection();" TabIndex="535" />
                                                        </div>
                                                    </div>
                                                </ul>
                                                <div class="tab-content" style="padding: 5px;">
                                                    <div id="ddlParcel" class="tab-pane fade active in">
                                                        <asp:GridView ID="grdShippingInformation" CssClass="table table-bordered" AutoGenerateColumns="false" AllowSorting="true" OnSorting="OnSorting" OnPageIndexChanging="OnPageIndexChanging" AllowPaging="true" PageSize="10" ShowHeaderWhenEmpty="true" EmptyDataText="No record found" runat="server" PagerSettings-Mode="NumericFirstLast" PagerSettings-FirstPageText="<<" PagerSettings-LastPageText=">>" PagerStyle-CssClass="pager-shipping">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-Width="200" HeaderText="Carrier">
                                                                    <ItemTemplate>
                                                                        <asp:Image runat="server" ID="ImgCatStatus" ImageUrl='<%#Eval("ShippingCarrierImage") %>' />
                                                                        <asp:Label ID="lblShippingCompanyName" runat="server" Text='<%#Eval("vcShippingCompanyName") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="vcServiceName" HeaderText="Service" ItemStyle-Width="300" />
                                                                <asp:BoundField DataField="AnticipateDelivery" HeaderText="Anticipated Delivery" ItemStyle-Width="150" />
                                                                <asp:BoundField DataField="ReleaseDate" HeaderText="Release Date" ItemStyle-Width="150" />
                                                                <asp:BoundField DataField="ExpectedDate" HeaderText="Expected Date" ItemStyle-Width="150" />
                                                                <asp:TemplateField HeaderText="Price">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblShippingAmount" runat="server" Text='<%#"$" + Eval("ShippingAmount") %>'></asp:Label>

                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField ItemStyle-Width="100" HeaderText="">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField ID="hdnShipViaID" runat="server" Value='<%#Eval("numShippingCompanyID")%>' />
                                                                        <asp:HiddenField ID="hdnShipServiceID" runat="server" Value='<%#Eval("intNsoftEnum") %>' />
                                                                        <asp:HiddenField ID="hdnShipServiceName" runat="server" Value='<%#Eval("vcServiceName")%>' />
                                                                        <asp:HiddenField ID="hdnShipRate" runat="server" Value='<%#Eval("ShippingAmount")%>' />
                                                                        <asp:HiddenField ID="hdnReleaseDate" runat="server" Value='<%#Eval("ReleaseDate")%>' />
                                                                        <asp:Button ID="btnAddOrder" runat="server" CssClass="btn btn-flat btn-xs btn-primary" CommandName="AddShippingRate" Text="Add to order" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </div>

                                            </div>
                                            <%--<div class="col-1-1" style="display:none">
                                                <div class="col-1-2">

                                                    <div class="form-group" runat="server" id="divComboEstimatedRates">
                                                        <label class="col-3-12">Estimated Shipping</label>
                                                        <div class="col-9-12">
                                                            <telerik:RadComboBox ID="radCmbEstimatedRates" Width="100%" runat="server" TabIndex="530"></telerik:RadComboBox>
                                                        </div>
                                                    </div>
                                                    <div class="form-group" runat="server" id="divLabelEstimatedRates">
                                                        <label class="col-3-12">Estimated Shipping</label>
                                                        <div class="col-9-12" style="margin-top: 7px">
                                                            <asp:Label ID="lblEstimatedShippingRate" Text="0.00" runat="server"></asp:Label>
                                                            <asp:HiddenField ID="hdnCalculatedShippingRate" runat="server" />
                                                            <asp:HiddenField ID="hdnCalculatedShippingService" runat="server" />
                                                        </div>
                                                    </div>
                                                    <div class="form-group" style="margin-top: 5px">
                                                        <label class="col-3-12">Total Weight: </label>
                                                        <div class="col-9-12">
                                                            <asp:Label runat="server" CssClass=" textdiv" ID="lblTotalWeight" Text="0.00"></asp:Label>
                                                            <asp:HiddenField runat="server" ID="hdnTotalWeight" Value="0" />
                                                            <div class="float-right">
                                                                <asp:ImageButton runat="server" Style="margin-top: 5px; margin-left: 5px" class="float-right" ID="imgBtnAddShippingItem" ImageUrl="~/images/AddRecord.png" TabIndex="534" OnClientClick="return AddUpdate();" />
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="col-1-2">
                                                </div>
                                            </div>--%>
                                        </asp:Panel>
                                    </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="radcmbShippingMethod" />
                                    <asp:AsyncPostBackTrigger ControlID="btnCalculateShipping" />
                                    <asp:AsyncPostBackTrigger ControlID="radShipVia" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <div class="col-1-1">
                                <asp:PlaceHolder ID="plhPaymentMethods" runat="server">
                                    <%--<asp:Button runat="server" ID="btnCashPOS" Text="Cash POS" />--%>
                                    <asp:UpdatePanel ID="UpdatePanelPaymentMethods" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                                        <ContentTemplate>
                                            <div class="col-1-1">
                                                <div class="col-1-2" style="display: none">
                                                    <div class="content">
                                                        <div class="form-group">
                                                            <label class="col-3-12" style="font-weight: bold">
                                                                Receive Payment
                                                            </label>
                                                            <div class="col-9-12">

                                                                <%-- <asp:DropDownList ID="ddlPaymentMethods" runat="server" AutoPostBack="true" TabIndex="536">
                                            </asp:DropDownList>--%>
                                                                <input type="checkbox" id="chkSaveClicked" style="display: none" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-1-2">
                                                    <div class="content">
                                                        <div class="form-group">
                                                            <label class="col-3-12" style="font-weight: bold">
                                                                BizDoc Status
                                                            </label>
                                                            <div class="col-9-12">
                                                                <telerik:RadComboBox ID="radcmbBizDocStatus" Width="100%" runat="server" AutoPostBack="true" TabIndex="537" OnClientLoad="onRadComboBoxLoad"></telerik:RadComboBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-1-1" style="margin-left: 15px;">
                                                <asp:Button ID="btnCreditCard" CssClass="OrderButton btnCreditCard" Width="139" runat="server" Text="Credit Card" TabIndex="549" />
                                                <asp:Button ID="btnCash" CssClass="OrderButton btnCash" Width="139" runat="server" Text="Cash" TabIndex="549" />
                                                <asp:Button ID="btnCheck" CssClass="OrderButton btnCheque" Width="139" runat="server" Text="Check" TabIndex="549" />
                                                <asp:Button ID="btnBillMe" CssClass="OrderButton btnBillme" Width="139" runat="server" Text="Bill-Me" TabIndex="549" />
                                                <asp:Button ID="btnOthers" CssClass="OrderButton btnOther" Width="139" runat="server" Text="Other" TabIndex="549" />
                                            </div>
                                            <div id="divCreditCard" runat="server" class="col-1-1" visible="false">
                                                <div class="col-1-2">
                                                    <div class="form-group" style="padding: 4px 0; text-align: right">
                                                        <asp:RadioButton ID="rdbAuthorizeAndCharge" Text="Authorize & Charge" runat="server" GroupName="PayByCreditCard" TabIndex="538" Checked="true" />
                                                        <asp:RadioButton ID="rdbAuthorizeOnly" Text="Authorize Only" runat="server" GroupName="PayByCreditCard" />
                                                        <asp:Button ID="btnAuthorize" OnClick="btnAuthorize_Click" CssClass="button" runat="server" Text="Authorize Credit Card" TabIndex="539" />
                                                        <br />
                                                        <asp:Literal ID="ltrlAutorizeCard" runat="server"></asp:Literal>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-3-12">Credit Card No</label>
                                                        <div class="col-9-12">
                                                            <div>
                                                                <asp:TextBox ID="txtCCNo" CssClass="signup" runat="server" MaxLength="16" TabIndex="540"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-3-12">CVV</label>
                                                        <div class="col-9-12">
                                                            <div>
                                                                <asp:TextBox ID="txtCVV2" CssClass="signup" runat="server" MaxLength="4" TabIndex="542"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-3-12">Valid Up-to</label>
                                                        <div class="col-9-12">

                                                            <div style="float: left">
                                                                <telerik:RadComboBox ID="radcmbMonth" runat="server" Width="40" TabIndex="544" OnClientLoad="onRadComboBoxLoad">
                                                                    <Items>
                                                                        <telerik:RadComboBoxItem Value="01" Text="01"></telerik:RadComboBoxItem>
                                                                        <telerik:RadComboBoxItem Value="02" Text="02"></telerik:RadComboBoxItem>
                                                                        <telerik:RadComboBoxItem Value="03" Text="03"></telerik:RadComboBoxItem>
                                                                        <telerik:RadComboBoxItem Value="04" Text="04"></telerik:RadComboBoxItem>
                                                                        <telerik:RadComboBoxItem Value="05" Text="05"></telerik:RadComboBoxItem>
                                                                        <telerik:RadComboBoxItem Value="06" Text="06"></telerik:RadComboBoxItem>
                                                                        <telerik:RadComboBoxItem Value="07" Text="07"></telerik:RadComboBoxItem>
                                                                        <telerik:RadComboBoxItem Value="08" Text="08"></telerik:RadComboBoxItem>
                                                                        <telerik:RadComboBoxItem Value="09" Text="09"></telerik:RadComboBoxItem>
                                                                        <telerik:RadComboBoxItem Value="10" Text="10"></telerik:RadComboBoxItem>
                                                                        <telerik:RadComboBoxItem Value="11" Text="11"></telerik:RadComboBoxItem>
                                                                        <telerik:RadComboBoxItem Value="12" Text="12"></telerik:RadComboBoxItem>
                                                                    </Items>
                                                                </telerik:RadComboBox>
                                                            </div>
                                                            <div style="float: left; margin-left: 20px">
                                                                <telerik:RadComboBox ID="radcmbYear" runat="server" Width="80" TabIndex="545" OnClientLoad="onRadComboBoxLoad"></telerik:RadComboBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-1-2">
                                                    <div class="form-group">
                                                        <label class="col-3-12">Saved Card #s</label>
                                                        <div class="col-9-12">
                                                            <div style="float: left">
                                                                <telerik:RadComboBox runat="server" ID="radcmbCards" AutoPostBack="true" Width="180" TabIndex="539" OnClientLoad="onRadComboBoxLoad"></telerik:RadComboBox>
                                                                <%--<asp:DropDownList runat="server" ID="ddlCards" CssClass="signup" AutoPostBack="true" Width="180" TabIndex="538">
                                                    </asp:DropDownList>--%>
                                                            </div>
                                                            <div style="float: left; margin-left: 5px;" class="textdiv">
                                                                <small>(optional)</small>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-3-12">Swipe</label>
                                                        <div class="col-9-12">
                                                            <div>
                                                                <asp:TextBox ID="txtSwipe" Width="200" runat="server" CssClass="signup" MaxLength="250" TabIndex="540"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-3-12">Card Type<span class="required">*</span></label>
                                                        <div class="col-9-12">
                                                            <div>
                                                                <telerik:RadComboBox runat="server" ID="radcmbCardType" Width="100%" TabIndex="542" OnClientLoad="onRadComboBoxLoad"></telerik:RadComboBox>
                                                                <%--<asp:DropDownList runat="server" ID="ddlCardType" CssClass="signup" AutoPostBack="false" TabIndex="542"></asp:DropDownList>--%>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="col-3-12">Card Holder Name</label>
                                                        <div class="col-9-12">
                                                            <div>
                                                                <asp:TextBox ID="txtCHName" runat="server" CssClass="signup" MaxLength="100" TabIndex="545"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>

                                    </asp:UpdatePanel>
                                </asp:PlaceHolder>
                            </div>
                            <asp:UpdatePanel ID="UpdatePanelOrderTotal" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <div class="float-left">
                                        <%--<input type="button" class="float-left" value="Save & Create Quotation" tabindex="552" />
                            <img src="../images/tag.png" class="tag" alt="" />--%>
                                    </div>
                                    <div class="float-right total-cont">
                                        <div class="totWid">
                                            <asp:Panel ID="pnlCouponDiscountConfig" runat="server">
                                                <div id="divCoupon" runat="server" visible="false">
                                                    <asp:Label ID="lblCouponDiscount" runat="server" Style="width: 180px !important" Text="Total Coupon Discount :"></asp:Label><asp:Label ID="lblCouponCurrency" runat="server"></asp:Label><asp:Label ID="lblCouponDiscountAmount" runat="server"></asp:Label>
                                                </div>
                                            </asp:Panel>
                                            <div id="divShippingCharges" runat="server">
                                                <asp:Label ID="lblShippingCharges" runat="server" Style="width: 180px !important" Text=""></asp:Label><asp:Label ID="lblShippingCostCurrency" runat="server"></asp:Label><asp:Label ID="lblShippingCost" runat="server" CssClass="text"></asp:Label>
                                            </div>
                                            <div id="divDiscount" runat="server">
                                                <asp:Label ID="lblDiscountTitle" runat="server" Style="width: 180px !important" Text=""></asp:Label><asp:Label ID="lblDiscountCurrency" runat="server"></asp:Label><asp:Label ID="lblDiscountAmount" runat="server" CssClass="text"></asp:Label>
                                            </div>
                                            <div>
                                                <asp:Label ID="lblCurrencyTotal" runat="server" Style="width: 180px !important" Text=""></asp:Label><asp:Label ID="lblTotalCurrency" runat="server"></asp:Label><asp:Label ID="lblTotal" CssClass="text" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanelHiddenField" runat="server" UpdateMode="Always">
        <ContentTemplate>

            <asp:HiddenField ID="hdnCustomerAddr" runat="server" />
            <asp:HiddenField ID="hdnShipVia" runat="server" />

            <asp:HiddenField ID="hdnShipFromState" runat="server" />
            <asp:HiddenField ID="hdnShipFromCountry" runat="server" />
            <asp:HiddenField ID="hdnShipFromPostalCode" runat="server" />

            <asp:HiddenField ID="hdnShipToState" runat="server" />
            <asp:HiddenField ID="hdnShipToCountry" runat="server" />
            <asp:HiddenField ID="hdnShipToPostalCode" runat="server" />
            <asp:HiddenField ID="hdnReleaseDateControlID" runat="server" />
            <asp:HiddenField ID="hdnShippingService" runat="server" />
            <asp:HiddenField ID="hdnSelectedItems" ClientIDMode="Static" runat="server" />
            <asp:TextBox ID="txtSerialize" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtDivID" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtHidValue" runat="server" Style="display: none"></asp:TextBox>
            <asp:TextBox ID="txtTax" runat="server" Style="display: none"></asp:TextBox>
            <input id="Taxable" runat="server" type="hidden" />
            <input id="TaxItemsId" runat="server" type="hidden" />
            <input id="hdKit" runat="server" type="hidden" />
            <asp:HiddenField ID="hdnBillAddressID" runat="server" />
            <asp:HiddenField ID="hdnShipAddressID" runat="server" />
            <asp:HiddenField ID="hdnOrderCreateDate" runat="server" />
            <asp:HiddenField ID="hdnVendorId" runat="server" />
            <asp:HiddenField ID="hdnPrimaryVendorID" runat="server" />
            <asp:HiddenField ID="hdnUnitCost" runat="server" />
            <asp:HiddenField ID="hdnSearchOrderCustomerHistory" runat="server" Value="0" />
            <asp:TextBox ID="txtModelID" runat="server" Style="display: none">
            </asp:TextBox>
            <asp:HiddenField ID="hdvPurchaseUOMConversionFactor" runat="server" Value="1" />
            <asp:HiddenField ID="hdvSalesUOMConversionFactor" runat="server" Value="1" />
            <asp:HiddenField ID="hdnPOS" runat="server" Value="0" />
            <asp:TextBox ID="txtHidEditOppItem" runat="server" Style="display: none"></asp:TextBox>
            <asp:HiddenField ID="hdnCountry" runat="server" Value="0" />
            <asp:HiddenField ID="hdnState" runat="server" Value="0" />
            <asp:HiddenField ID="hdnCity" runat="server" Value="0" />
            <asp:HiddenField ID="hdnPostal" runat="server" Value="0" />
            <asp:HiddenField ID="hdnItemWeight" runat="server" Value="0" />
            <asp:HiddenField ID="hdnFreeShipping" runat="server" Value="0" />
            <asp:HiddenField ID="hdnHeight" runat="server" Value="0" />
            <asp:HiddenField ID="hdnWidth" runat="server" Value="0" />
            <asp:HiddenField ID="hdnLength" runat="server" Value="0" />
            <asp:HiddenField ID="hdnSKU" runat="server" Value="" />
            <asp:HiddenField ID="hdnType" runat="server" />
            <asp:HiddenField ID="hdnSwipeTrack1" runat="server" />
            <asp:HiddenField ID="hdnCurrentSelectedItem" runat="server" />
            <asp:HiddenField ID="hdnSelectedItemPromo" runat="server" />
            <asp:HiddenField ID="hdnIsMatrix" runat="server" />
            <asp:HiddenField ID="hdnHasKitAsChild" runat="server" />
            <asp:HiddenField ID="hdnIsSKUMatch" runat="server" />
            <asp:HiddenField ID="hdnChangedProfitPercent" runat="server" />
            <asp:HiddenField ID="hdnEditUnitPriceRight" runat="server" />
            <asp:HiddenField ID="hdnTempSelectedItems" runat="server" />
            <asp:HiddenField ID="hdnTempSelectedItemWarehouse" runat="server" />
            <asp:HiddenField ID="hdnEnabledItemLevelUOM" runat="server" />
            <asp:HiddenField ID="hdnPromotionName" Value="0" runat="server" />
            <asp:HiddenField ID="hdnCouponNotRequire" Value="0" runat="server" />

            <asp:HiddenField ID="hdnDropShp" runat="server" Value="0" />
            <asp:HiddenField ID="hdnDropShpStreet" runat="server" />
            <asp:HiddenField ID="hdnDropShpCity" runat="server" />
            <asp:HiddenField ID="hdnDropShpStateID" runat="server" />
            <asp:HiddenField ID="hdnDropShpStateName" runat="server" />
            <asp:HiddenField ID="hdnDropShpPostal" runat="server" />
            <asp:HiddenField ID="hdnDropShpCountryId" runat="server" />
            <asp:HiddenField ID="hdnDropShpCountryName" runat="server" />
            <asp:HiddenField ID="hdnDropShipAltShippingcontact" runat="server" />
            <asp:HiddenField ID="hdnstrShipItemCodes" runat="server" />
            <asp:HiddenField ID="hdnStateName" runat="server" />

            <asp:HiddenField ID="hdnIsAutoWarehouseSelection" runat="server" />
            <asp:HiddenField ID="hdnWarehousePriority" runat="server" />
            <asp:HiddenField runat="server" ID="hdnPortalURL"></asp:HiddenField>

            <div class="overlay1" id="dialogUnitPriceApproval" style="display: none">
                <div class="overlayContent" style="color: #000; background-color: #FFF; width: 420px; padding: 10px; border: 2px solid black;">
                    <p style="text-align: center">The unit price you’ve entered for one or more items requires approval.</p>
                    <p style="text-align: center">
                        <asp:Button ID="btnApproveUnitPrice" CssClass="btn btn-primary" ForeColor="White" runat="server" Text="Approve Unit Price" Visible="false" OnClientClick="return hideUnitPriceDialog();" />
                        <asp:Button ID="btnSaveAndProceed" CssClass="btn btn-primary" ForeColor="White" runat="server" Text="Save & Proceed" OnClientClick="return hideUnitPriceDialog();" />
                        <asp:Button ID="btnClear" CssClass="btn btn-primary" ForeColor="White" runat="server" Text="Clear" OnClientClick="return hideUnitPriceDialog();" />
                        <asp:HiddenField ID="hdnIsUnitPriceApprovalRequired" runat="server" />
                        <asp:HiddenField ID="hdnClickedButton" runat="server" />
                        <asp:HiddenField ID="hdnApprovalActionTaken" runat="server" />
                    </p>
                </div>
            </div>
            <div class="overlay1" id="dialogOtherPayment" style="display: none">
                <div class="overlayContent" style="color: #000; background-color: #FFF; width: 420px; padding: 10px; border: 2px solid black;">
                    <p style="text-align: center">Please select payment.</p>
                    <p style="text-align: center">
                        <asp:DropDownList ID="ddlPaymentMethods" runat="server"></asp:DropDownList>
                        <telerik:RadComboBox ID="radcmbPaymentMethods" Style="display: none" Width="100%" runat="server" AutoPostBack="true" TabIndex="536" OnClientLoad="onRadComboBoxLoad"></telerik:RadComboBox>
                        <asp:Button ID="btnOthersPayment" CssClass="OrderButton" Width="139" Style="width: 95px; text-align: center; margin-left: 40%;" runat="server" Text="Proceed" TabIndex="549" />
                    </p>
                </div>
            </div>
            
            <div class="overlay1" id="divItemAttributes" runat="server" style="display: none">
                <div class="overlayContent" style="color: #000; background-color: #FFF; width: 400px; border: 1px solid #ddd;">
                    <div class="dialog-header">
                        <b style="font-size: 14px;">Select Item Attributes?</b>
                        <div style="float: right; padding: 7px;">
                            <asp:Button ID="btnSaveCloseAttributes" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="Save & Close" />
                        </div>
                    </div>
                    <div class="dialog-body">
                        <div style="text-align: center">
                            <asp:Label ID="lblAttributeError" runat="server" Text="" ForeColor="Red"></asp:Label>
                        </div>
                        <asp:Repeater ID="rptAttributes" runat="server">
                            <HeaderTemplate>
                                <table style="width: 100%; padding: 0px; border-spacing: 10px;">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr style="min-height: 30px;">
                                    <td style="white-space: nowrap; text-align: right">
                                        <asp:Label ID="lblAttributeName" runat="server" Text='<%# Eval("Fld_label")%>' Font-Bold="true"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:HiddenField ID="hdnFldID" runat="server" Value='<%# Eval("Fld_id")%>' />
                                        <asp:HiddenField ID="hdnListID" runat="server" Value='<%# Eval("numlistid")%>' />
                                        <asp:DropDownList ID="ddlAttributes" runat="server" Width="150"></asp:DropDownList>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
            <div class="overlay1" id="divItemPromMultiple" runat="server" visible="false">
                <div class="overlayContent" style="color: #000; background-color: #FFF; width: 90%; border: 1px solid #ddd; max-height: 500px; overflow: hidden">
                    <div class="dialog-header">
                        <div style="float: left">
                            <img alt="" src="../images/ShoppingCart_Deal.png" style="vertical-align: middle; padding-right: 5px;" />
                            <b style="font-size: 14px">Item Promotions</b>
                        </div>
                        <div style="float: right; padding: 7px;">
                            <asp:Button ID="btnSaveCloseItemPromo" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="Save & Close" OnClick="btnSaveCloseItemPromo_Click" />
                        </div>
                    </div>
                    <div class="dialog-body" style="width: 100%">
                        <asp:RadioButtonList ID="rblItemPromo" runat="server">
                        </asp:RadioButtonList>
                    </div>
                </div>
            </div>
            <div class="overlay1" id="divAssemblyDetail" runat="server" style="display: none">
                <div class="overlayContent" style="color: #000; background-color: #FFF; width: 90%; border: 1px solid #ddd; max-height: 500px; overflow: hidden; overflow-y: auto">
                    <div class="dialog-header">
                        <div style="float: left">
                            <b style="font-size: 14px">Assembly Detail</b>
                        </div>
                        <div style="float: right; padding: 7px;">
                            <asp:Button ID="btnCloseAssemblyDetail" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="Close" OnClientClick="return CloseAssemblyDetail();" />
                        </div>
                    </div>
                    <div class="dialog-body" style="width: 100%;">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label>Earliest Available Ship Date:</label>
                                            <asp:Label ID="lblAssemblyShipDate" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label>Max Build Qty:</label>
                                            <asp:Label ID="lblAssemblyBuildQty" runat="server"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row padbottom10">
                                <div class="col-xs-12">
                                    <div class="form-inline">
                                        <div class="form-group">
                                            <label>List Price:</label>
                                            <div class="checkbox">
                                                <asp:Label ID="lblAssemblyListPrice" runat="server"></asp:Label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row padbottom10">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                        <label>Mfg Cost</label>
                                        <div>
                                            <ul class="list-unstyled">
                                                <li><i>Materials:<asp:Label runat="server" ID="lblMaterialCost"></asp:Label></i></li>
                                                <li><i>Overhead:<asp:Label runat="server" ID="lblOverheadCost"></asp:Label></i></li>
                                                <li><i>Projected Labour:<asp:Label runat="server" ID="lblLabourCost"></asp:Label></i></li>
                                                <li><i><b>Unit Cost / Total Cost:</b><asp:Label runat="server" ID="lblUnitTotalCost"></asp:Label></i></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <table class="table table-bordered table-striped" id="tblAssembltParts">
                                        <tr>
                                            <th>BOM Item Name (SKU)</th>
                                            <th>Qty</th>
                                            <th>Price</th>
                                        </tr>
                                        <asp:Repeater runat="server" ID="rptAssembltParts">
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Eval("vcItemName") %></td>
                                                    <td><%# Eval("numQuantity") %></td>
                                                    <td><%# String.Format(Ccommon.GetDataFormatStringWithCurrency(), Ccommon.ToDouble(Eval("monListPrice"))) %></td>
                                                </tr>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="overlay1" id="divKitChild" style="display: none" runat="server">
                <div class="overlayContent" style="color: #000; background-color: #FFF; width:97%;height:96%;margin:20px;border: 1px solid #ddd;overflow: hidden">
                    <div class="dialog-header" style="height:70px">
                        <div style="float: left">
                            <img alt="" src="../images/ItemConfig.png" style="vertical-align: middle; padding-right:5px;max-height:50px" id="imgKit" />
                            <b style="font-size: 14px"><label id="lblKitItem"></label></b>
                            &nbsp;&nbsp;
                            <label id="lblKitDescription" style="max-width:400px;line-height:20px"></label>
                            <b>CONFIGURED PRICE:</b>
                            <label id="lblKitCalculaedPrice" style="color:#00b050; font-size:18px">$ 0.00</label>
                        </div>
                        <div style="float: right; padding-right: 7px;">
                            <asp:Button ID="btnSaveCloseKit" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="Add Item" OnClientClick="return GetChildKitItemSelection();" />
                            <input type="button" class="btn btn-primary" value="Create New Kit" onclick="CreateNewItem(false,true,true);" />
                            <input type="button" class="btn btn-primary" value="Create New Assembly" onclick="CreateNewItem(true,false,true);" />
                            <asp:Button ID="btnCloseKit" runat="server" OnClick="btnCloseKit_Click" CssClass="btn btn-primary" style="color: white" Text="Close" />
                            <asp:HiddenField ID="hdnSelectedText" runat="server"></asp:HiddenField>
                            <asp:HiddenField ID="hdnFromItemPromoPopup" runat="server" Value="0" />
                            <asp:HiddenField ID="hdnItemPromokitChildsHiddeFieldID" runat="server" />
                            <asp:HiddenField ID="hdnItemPromoAddbuttonID" runat="server" />
                        </div>
                    </div>
                    <div class="dialog-body" style="overflow-y: auto; max-height:94%; margin-top:1px;">
                        <div style="text-align: center">
                            <asp:Label ID="lblKitChildError" runat="server" ForeColor="Red" Text=""></asp:Label>
                        </div>
                        <div class="panel-group" id="divChildKitContainer">
            
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <div class="overlay1" id="divEditKit" style="display: none">
        <div class="overlayContent" style="color: #000; background-color: #FFF; width: 90%; border: 1px solid #ddd; max-height: 530px; overflow: hidden">
            <div class="dialog-header">
                <div style="float: left">
                    <img alt="" src="../images/ItemConfig.png" style="vertical-align: middle; padding-right: 5px;" />
                    <b style="font-size: 14px">Select Item(s)</b>
                </div>
                <asp:UpdatePanel ID="uplEditKit1" runat="server" style="float: right; padding-right: 7px;" UpdateMode="Conditional" ChildrenAsTriggers="true">
                    <ContentTemplate>
                        <asp:Button ID="btnSaveEditKit" CssClass="btn btn-primary" ForeColor="White" runat="server" Text="Save & Close" OnClick="btnSaveEditKit_Click" />
                        <asp:Button ID="btnCloseEditKit" CssClass="btn btn-primary" ForeColor="White" runat="server" Text="Close" OnClientClick="return CloseEditKitChildWindow();" />
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="btnSaveEditKit" EventName="Click" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
            <div class="dialog-body" style="width: 100%">
                <div class="col-1-1" style="padding-bottom: 10px">
                    <asp:UpdatePanel ID="UpdatePanel4" runat="server" class="pull-left" ChildrenAsTriggers="true" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="form-inline">
                                <div class="form-group">
                                    <label>Add Item:</label>
                                    <input type="text" id="txtChildItem" />
                                    <input type="button" class="btn btn-success" value="Add Item" onclick="return AddChildItem()" style="margin-left: 5px; margin-right: 5px;" />
                                    <input type="button" class="btn btn-danger" value="Remove Item" onclick="$('[id$=btnRemoveChilItem]').click();" />
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="UpdatePanel5" runat="server" class="pull-right" UpdateMode="Always">
                        <ContentTemplate>
                            <div class="form-inline">
                                <ul class="list-unstyled" id="divKitPrice" runat="server" visible="false">
                                    <li>
                                        <div class="form-inline">
                                            <label>Parent kit unit price:</label>
                                            <asp:Label runat="server" ID="lblKitPrice"></asp:Label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="form-inline">
                                            <label>Price set to:</label>
                                            <asp:Label runat="server" ID="lblKitPriceType"></asp:Label>
                                            <asp:HiddenField runat="server" ID="hdnKitPricing" />
                                            <asp:HiddenField runat="server" ID="hdnKitListPrice" />
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
                <asp:UpdatePanel ID="UpdatePanel3" runat="server" class="col-1-1" ChildrenAsTriggers="true" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:HiddenField ID="hdnSelectedChildItem" runat="server" />
                        <asp:Button ID="btnAddChilItem" runat="server" Style="display: none;" OnClick="btnAddChilItem_Click" />
                        <asp:Button ID="btnRemoveChilItem" runat="server" OnClick="btnRemoveChilItem_Click" Style="display: none" />
                        <div class="table-responsive" style="max-height: 400px">
                            <asp:GridView ID="gvChildItems" ShowHeaderWhenEmpty="true" runat="server" AutoGenerateColumns="false" UseAccessibleHeader="true" CssClass="table table-bordered table-striped">
                                <Columns>
                                    <asp:TemplateField HeaderText="Item Code" HeaderStyle-Width="80" ItemStyle-Width="80">
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemCode" runat="server" Text='<%# Eval("numItemCode")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Item">
                                        <ItemTemplate>
                                            <asp:Label ID="lblItemName" runat="server" Text='<%# Eval("vcItemName")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SKU">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSKU" runat="server" Text='<%# Eval("vcSKU")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblType" runat="server" Text='<%# Eval("vcType")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:BoundField HeaderText="Qty" DataField="numQty" HeaderStyle-Width="80" ItemStyle-Width="80" />--%>
                                    <asp:TemplateField HeaderText="Qty" HeaderStyle-Width="80" ItemStyle-Width="80">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtUnits" runat="server" Text='<%# Eval("numQty")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="List Price" HeaderStyle-Width="80" ItemStyle-Width="80">
                                        <ItemTemplate>
                                            <asp:Label ID="lblListPrice" runat="server" Text='<%# Eval("monListPrice")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Cost" HeaderStyle-Width="88" ItemStyle-Width="88">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVendorCost" runat="server" Text='<%# Eval("monVendorCost")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Average Cost" HeaderStyle-Width="94" ItemStyle-Width="94">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAverageCost" runat="server" Text='<%# Eval("monAverageCost")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Price" HeaderStyle-Width="80" ItemStyle-Width="80">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPrice" runat="server" Text='<%# Eval("monPrice")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sequence" HeaderStyle-Width="74" ItemStyle-Width="74">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtSequence" runat="server" Text='<%# Eval("numSequence")%>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-Width="25" ItemStyle-Width="25">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                            <asp:HiddenField runat="server" ID="hdnItemCode" Value='<%# Eval("numItemCode") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    No item(s) added
                                </EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanelActionWindow" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
        <ContentTemplate>
            <telerik:RadWindow RenderMode="Lightweight" runat="server" ID="RadWinRelatedItems" Title="Related Items" RestrictionZoneID="ContentTemplateZone" Modal="true" Behaviors="Move,Resize" Width="1100" Height="300">
                <ContentTemplate>
                    <div class="dialog-header">
                        <div class="col-1-1">
                            <div class="col-8-12" id="divPromotionDetailRI" runat="server" visible="false">
                                <div id="divPromotionDesriptionRI" runat="server">
                                    <div style="float: left; padding-right: 5px; line-height: 1; display: inline;">
                                        <img src="../images/Admin-Price-Control.png" height="30" alt="" /><b>Promotion: </b>
                                        <asp:Label ID="lblPromotionNameRI" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="lblExpirationRI" runat="server" Text=""></asp:Label><br />
                                        <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Details: </b>
                                        <asp:Label ID="lblPromotionDescriptionRI" runat="server" Text=""></asp:Label><br />
                                        <div id="divCouponCodeRI" runat="server" style="display: -webkit-inline-box;" visible="false">
                                            <label runat="server" id="lblCouponText" style="padding-top: 10px;">Coupon Code:&nbsp; </label>
                                            <asp:TextBox runat="server" ID="txtCouponCodeRI" Width="100px"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div style="float: right;">
                                <asp:Button ID="btnReplaceClose" CssClass="btn btn-warning" Style="color: white; display: none" Text="Replace & Close" runat="server" OnClick="btnReplaceClose_Click" OnClientClick="return Confirm()"></asp:Button>
                                <asp:Button ID="btnAddClose" CssClass="btn btn-primary" Style="color: white" Text="Add & Close" runat="server" OnClick="btnAddClose_Click" OnClientClick="return Confirm()"></asp:Button>
                                <asp:Button ID="btnAdd" CssClass="btn btn-primary" Style="color: white" Text="Add" runat="server" OnClick="btnAdd_Click" OnClientClick="return Confirm()"></asp:Button>
                                <input type="button" id="btnCloseRelatedItems" onclick="return ClosePreCheckoutWindow();" class="btn btn-primary" style="color: white" value="Close Window" />
                            </div>
                        </div>
                    </div>
                    <div class="dialog-body">
                        <asp:GridView ID="gvRelatedItems" CssClass="table table-bordered table-striped" runat="server" AutoGenerateColumns="false" Width="1050px" CellPadding="3" CellSpacing="3" BorderColor="#f0f0f0">
                            <RowStyle BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                            <HeaderStyle HorizontalAlign="Center" Font-Bold="true" Font-Size="10" BackColor="#e8e8e8" BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                            <Columns>
                                <asp:TemplateField ItemStyle-Width="100px">
                                    <ItemTemplate>
                                        <asp:Image ID="imgPath" runat="server" ImageUrl='<%# Eval("vcPathForTImage")%>' alt="" Style="width: 80px;" class="img-responsive" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-Width="500px" HeaderText="Item (SKU) & Description">
                                    <ItemTemplate>
                                        <asp:Label ID="lblItemDesc" runat="server" Text='<%# Eval("vcItemName") & "(" & Eval("vcSKU") & ")" & ":" & Eval("txtItemDesc")%>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <%--<asp:BoundField DataField="vcItemName" HeaderText="Item (SKU) & Description" ItemStyle-Width="600" ItemStyle-Wrap="true" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" />--%>
                                <asp:BoundField DataField="numOnHand" HeaderText="On Hand" ItemStyle-Width="100" ItemStyle-Wrap="true" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Wrap="false" />
                                <asp:BoundField DataField="vcRelationship" HeaderText="Relation to selected item(s)" ItemStyle-Width="500" ItemStyle-Wrap="true" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" />
                                <asp:TemplateField HeaderText="Qty" ItemStyle-Wrap="false" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="txtQty" runat="server" Width="30" Text="1"></asp:TextBox></td>
                                                <td>/ </td>
                                                <td><%# Eval("vcUOMName")%></td>
                                            </tr>
                                        </table>
                                        <asp:HiddenField ID="hdnWareHouseItemID" runat="server" Value='<%# Eval("numWareHouseItemID")%>' />
                                        <%--<asp:HiddenField ID="hdnPrice" runat="server" Value='<%# Eval("monListPrice")%>' />--%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="monListPrice" HeaderText="Unit List" ItemStyle-Width="200" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" DataFormatString="USD {0:#,##0.00}" />
                                <asp:TemplateField HeaderText="Total" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Center" ItemStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotal" runat="server" Width="100" Text="1"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="checkAll" Width="100px" runat="server" Text="Select All" onclick="checkAll(this);checkRelatedItems()" />
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkAdd" onchange="checkRelatedItems()" runat="server" />
                                        <%--<asp:Button ID="lnkAdd" CommandName="Add" CssClass="button" Text="Add" runat="server" OnClientClick="Confirm()" CommandArgument='<%# Eval("numItemCode") %>'></asp:Button>--%>
                                        <asp:Label ID="lblOutOfStock" runat="server" Width="100" Text="Out of stock" Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="numOnHand" ItemStyle-CssClass="hidden-field" HeaderStyle-CssClass="hidden-field" />
                                <asp:BoundField DataField="numItemCode" ItemStyle-CssClass="hidden-field" HeaderStyle-CssClass="hidden-field" />
                            </Columns>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
            </telerik:RadWindow>
            <div class="overlay1" id="divItemPriceHistory" runat="server" style="display: none">
                <div class="overlayContent" style="color: #000; background-color: #FFF; width: 1000px; border: 1px solid #ddd; margin: 100px auto !important;">
                    <div class="dialog-header">
                        <b style="font-size: 14px;">Item Price History</b>
                        <div style="float: right; padding-right: 7px;">
                            <asp:CheckBox ID="chkPriceHistoryByCustomer" runat="server" Text="Show price history of selected customer" Font-Bold="true" Style="margin-right: 20px" AutoPostBack="true" />
                            <asp:Button ID="btnClosePriceHistory" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="Close Window" />
                        </div>
                    </div>
                    <div class="dialog-body" style="max-height: 600px; width: 998px;">
                        <asp:GridView ID="gvPriceHistory" runat="server" AutoGenerateColumns="false" Width="98%" CellPadding="3" CellSpacing="3" BorderColor="#f0f0f0">
                            <RowStyle BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                            <HeaderStyle HorizontalAlign="Center" Font-Bold="true" Font-Size="10" BackColor="#e8e8e8" BorderColor="#CACACA" BorderStyle="Solid" BorderWidth="1" />
                            <Columns>
                                <asp:BoundField DataField="vcCreatedDate" HeaderText="DateCreated" />
                                <asp:BoundField DataField="vcPOppName" HeaderText="Source" />
                                <asp:BoundField DataField="numUnitHour" HeaderText="Units" />
                                <asp:BoundField DataField="vcUOMName" HeaderText="UOM" />
                                <asp:BoundField DataField="monPrice" HeaderText="Price" />
                                <asp:BoundField DataField="vcDiscount" HeaderText="Discount" />
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnDate" runat="server" Value='<%# Eval("vcCreatedDate")%>' />
                                        <asp:HiddenField ID="hdnUnits" runat="server" Value='<%# Eval("numUnitHour")%>' />
                                        <asp:HiddenField ID="hdnUOMId" runat="server" Value='<%# Eval("numUOMId")%>' />
                                        <asp:HiddenField ID="hdnUOMConversionFactor" runat="server" Value='<%# Eval("decUOMConversionFactor")%>' />
                                        <asp:HiddenField ID="hdnPrice" runat="server" Value='<%# Eval("monPrice") %>' />
                                        <asp:HiddenField ID="hdnDiscount" runat="server" Value='<%# Eval("fltDiscount")%>' />
                                        <asp:HiddenField ID="hdnDiscountType" runat="server" Value='<%# Eval("bitDiscountType")%>' />
                                        <asp:LinkButton ID="lnkAdd" CommandName="Add" CssClass="hyperlink" runat="server"><img alt="" src="../images/AddRecord.png" /></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>

            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="uplItemPromotionStatus" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="overlay1" id="divItemPromotionStatus" runat="server" visible="false">
                <div class="overlayContent" style="color: #000; background-color: #FFF; width: 1000px; border: 1px solid #ddd; margin: 100px auto !important;">
                    <div class="dialog-header">
                        <b style="font-size: 14px;">Promotion Status</b>
                        <div style="float: right; padding: 7px;">

                            <asp:Button ID="btnClosePromotionStatus" CssClass="btn btn-primary" ForeColor="White" runat="server" Text="Close" OnClick="btnClosePromotionStatus_Click" />
                        </div>
                    </div>
                    <div class="dialog-body">
                        <div class="row" style="margin-left: 0px; margin-right: 0px">
                            <div class="col-xs-12">
                                <table class="table table-bordered table-striped">
                                    <tr>
                                        <th>Promotion on offer
                                        </th>
                                        <th>Status
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblPromotionDescription" runat="server"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="lblPromotionStatus" runat="server"></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 0px; margin-right: 0px" id="divPromotionDiscountItems" runat="server">
                            <div class="col-xs-12" style="padding-bottom: 10px;">
                                <asp:HiddenField runat="server" ID="hdnIPSPromotionID" />
                                <asp:HiddenField runat="server" ID="hdnIPSItemID" />
                                <asp:HiddenField runat="server" ID="hdnIPSWarehouseItemID" />
                                <asp:HiddenField runat="server" ID="hdnIPSOppItemID" />
                                <asp:HiddenField runat="server" ID="hdnIsCouponCodeRequired" />
                                <webdiyer:AspNetPager ID="bizPager" runat="server"
                                    PagingButtonSpacing="0"
                                    CssClass="bizgridpager"
                                    AlwaysShow="true"
                                    CurrentPageButtonClass="active"
                                    PagingButtonUlLayoutClass="pagination"
                                    PagingButtonLayoutType="UnorderedList"
                                    FirstPageText="<<"
                                    LastPageText=">>"
                                    NextPageText=">"
                                    PrevPageText="<"
                                    UrlPaging="false"
                                    NumericButtonCount="8"
                                    ShowCustomInfoSection="Left"
                                    OnPageChanged="bizPager_PageChanged"
                                    ShowPageIndexBox="Never"
                                    CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
                                    CustomInfoClass="bizpagercustominfo">
                                </webdiyer:AspNetPager>
                            </div>
                            <div class="col-xs-12">
                                <asp:GridView ID="gvItemPromotionStatus" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered table-striped" OnRowCommand="gvItemPromotionStatus_RowCommand" UseAccessibleHeader="true" ShowHeaderWhenEmpty="true">
                                    <Columns>
                                        <asp:BoundField HeaderText="Item" DataField="vcItemName" />
                                        <asp:BoundField HeaderText="Description" DataField="txtItemDesc" />
                                        <asp:TemplateField HeaderText="Units" HeaderStyle-Width="100">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtUnits" runat="server" Text="1" Style="margin: 0px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Unit List" HeaderStyle-Width="100">
                                            <ItemTemplate>
                                                <%# Session("Currency")%>
                                                <asp:Label ID="lblListPrice" runat="server" Text='<%# CCommon.GetDecimalFormat(Eval("monListPrice"))%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="" HeaderStyle-Width="68">
                                            <ItemTemplate>
                                                <asp:HiddenField ID="hdnItemCode" runat="server" Value='<%# Eval("numItemCode")%>' />
                                                <asp:HiddenField ID="hdnWarehouseItemID" runat="server" Value='<%# Eval("numWareHouseItemID")%>' />
                                                <asp:HiddenField ID="hdnMatrixItem" runat="server" Value='<%# Eval("bitMatrix")%>' />
                                                <asp:HiddenField ID="hdnHasChildKits" runat="server" ClientIDMode="AutoID" Value='<%# Eval("bitHasChildKits")%>' />
                                                <asp:HiddenField ID="hdnSelectedChildKits" runat="server" />
                                                <asp:HiddenField ID="hdnCalAmtBasedonDepItems" runat="server" Value='<%# Eval("bitCalAmtBasedonDepItems")%>' />
                                                <asp:Button ID="btnAddItem" runat="server" Text="Add" ClientIDMode="AutoID" CssClass="btn btn-primary" Style="line-height: 10px" CommandName="AddDiscountedItem" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <tr>
                                            <td colspan="8">
                                                <b>No Data Available</b>
                                            </td>
                                        </tr>
                                    </EmptyDataTemplate>
                                </asp:GridView>
                                <asp:Label ID="lblItemsWithLessPrice" runat="server" Font-Bold="true" Text="Close this window and search for item and add to cart. Promotion will be applied if it can be applied to selected item."></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <asp:Button ID="btnBindOrderGrid" runat="server" CssClass="cancel" Text="BindOrderGrid"
        Style="display: none" />
    <asp:Button ID="btnGo" runat="server" CssClass="cancel" Visible="false" />
    <asp:Button ID="btnTemp" runat="server" CssClass="cancel" Style="display: none"></asp:Button>
    <asp:HiddenField ID="hdnFlagRelatedItems" runat="server" Value="False" />
    <asp:HiddenField ID="hdnUpdateParentPromotion" runat="server" />
    <asp:HiddenField ID="hdnExitSub" runat="server" />
    <asp:HiddenField ID="hdnOnHandAllocation" runat="server" />
    <asp:Button ID="btn2" runat="server" OnClick="btn2_Click" Style="display: none" />
    <div class="overlay" id="divLoader" style="display:none">
        <div class="overlayContent" style="background-color:#fff; color: #000; text-align: center; width: 280px; padding: 20px">
            <i class="fa fa-2x fa-refresh fa-spin"></i>
            <h3>Processing Request</h3>
        </div>
    </div>
</asp:Content>
