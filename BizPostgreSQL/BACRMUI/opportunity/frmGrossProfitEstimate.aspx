﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmGrossProfitEstimate.aspx.vb"
    Inherits=".frmGrossProfitEstimate" MasterPageFile="~/common/PopUpBootstrap.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Gross Profit Estimate</title>
    <script type="text/javascript">
        function OpenOpp(oppID) {
            var url = "<%# ResolveUrl("~/Opportunity/frmOpportunities.aspx")%>"
            url = url + "?opId=" + oppID;

            if (window.parent != null) {
                window.parent.window.open(url, '_blank');
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-left">
                <ul class="list-unstyled"> 
                    <li style="text-align:center">
                        <ul class="list-inline">
                            <li>
                                <asp:Label ID="lblProfitPercent" runat="server" font-bold="true" Font-Size="20"></asp:Label>
                            </li>
                            <li>
                                <asp:Label ID="lblProfit" runat="server" font-bold="true" Font-Size="20"></asp:Label>
                            </li>
                        </ul>
                    </li>
                    <li style="text-align:center">
                        <i style="font-weight:bold">GROSS PROFIT</i>
                    </li>
                </ul>
            </div>
            <div class="pull-right">
                 <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" OnClientClick="window.close()" Text="Close" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Gross Profit
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <b>Shipping Profit / Loss: </b><asp:Label ID="lblShippingProfit" runat="server"></asp:Label> 
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvGrossProfitEstimate" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered table-striped" UseAccessibleHeader="true">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Label ID="lblItem" runat="server" Text='<%#Eval("vcItemName")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Units" HeaderStyle-Width="120" ItemStyle-Width="120">
                            <ItemTemplate>
                                <%# FormatNumber(Eval("numUnitHour"), 0)%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Unit Price (Profit %)" HeaderStyle-Width="200" ItemStyle-Width="200">
                            <ItemTemplate>
                                <%#ReturnMoney(Eval("monPrice"))%>
                    (<%# ReturnMoney(Eval("ProfitPer"))%>%)
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Item Profit" HeaderStyle-Width="120" ItemStyle-Width="120">
                            <ItemTemplate>
                                <%# FormatNumber(((Eval("monPrice") * (Eval("ProfitPer") / 100)) * Eval("numUnitHour")), 2)%>
                                <asp:HiddenField ID="hdnBitApproval" Value='<%# Eval("bitItemPriceApprovalRequired")%>' runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Purchase Order">
                            <ItemTemplate>
                                <%# Eval("vcPurchaseOrder") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
