'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports System.Text.RegularExpressions
Imports System.Reflection
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Workflow
Imports System.Xml


Namespace BACRM.UserInterface.Opportunities
    Public Class frmAddBizDocs
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents imgBtnSaveClose As System.Web.UI.WebControls.ImageButton
        Protected WithEvents imgbtnCancel As System.Web.UI.WebControls.ImageButton
        Protected WithEvents ddlBizDocs As System.Web.UI.WebControls.DropDownList
        Protected WithEvents btnSaveClose As System.Web.UI.WebControls.LinkButton
        Protected WithEvents btnCancel As System.Web.UI.WebControls.LinkButton
        Protected WithEvents litMessage As System.Web.UI.WebControls.Literal
        Protected WithEvents ddlReleaseDates As System.Web.UI.WebControls.DropDownList

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region


        Dim dtOppBiDocItems As DataTable
        Dim OppBizDocID As Long
        Dim lngOppID As Long
        Dim objOppBizDocs As OppBizDocs
        Dim OppType As Short
        Dim lngPrevSelectedBizDoc As Long
        Dim lngPrevSelectedBizDocTemplate As Long
        Dim mode As String
        Dim grandTotal As Double = 0

        Dim m_aryRightsForAuthoritativ(), m_aryRightsForNonAuthoritativ() As Integer


        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                lngOppID = GetQueryStringVal("OpID")
                OppType = GetQueryStringVal("OppType")
                lngPrevSelectedBizDoc = CCommon.ToLong(GetQueryStringVal("bizDoc"))
                lngPrevSelectedBizDocTemplate = CCommon.ToLong(GetQueryStringVal("bizDocTemplate"))
                mode = GetQueryStringVal("mode")
                If mode = "add" Then
                    divCreated.Visible = False
                Else
                    divCreated.Visible = True
                End If
                If CCommon.ToLong(hdnOppBizDocID.Value) > 0 Then
                    OppBizDocID = CCommon.ToLong(hdnOppBizDocID.Value)
                Else
                    OppBizDocID = CCommon.ToLong(GetQueryStringVal("OppBizId"))
                End If
                Dim objCommon As New CCommon


                'GetUserRightsForPage(10, 7)
                'If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                '    Response.Redirect("../admin/authentication.aspx?mesg=AC")
                'Else
                '    If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                '        btnSave.Visible = False
                '        btnSaveClose.Visible = False
                '    End If
                'End If

                If Session("MultiCurrency") = True Then
                    trCurrency.Visible = True
                Else
                    trCurrency.Visible = False
                End If

                If Not IsPostBack Then
                    lblRefOrderNo.Text = IIf(OppType = 1, "P.O #", "Invoice #")

                    If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                    objOppBizDocs.DomainID = Session("DomainID")
                    objOppBizDocs.OppId = lngOppID
                    hdnAuthorizativeBizDocsId.Value = objOppBizDocs.GetAuthorizativeOpportuntiy()
                    objCommon.sb_FillComboFromDBwithSel(ddlBizDocStatus, 11, Session("DomainID"))
                    Fillcombo()
                    bindARAccounts()
                    EnableControls()

                    ''Dim objCommon As New CCommon
                    ''objCommon.sb_FillComboFromDBwithSel(ddlBizDocs, 27, Session("DomainID"))
                    'If GetQueryStringVal( "OppType") = 2 Then
                    '    chkPartial.Visible = False
                    'End If
                    'objCommon.sb_FillComboFromDB(ddlNetDays, 296, Session("DomainID"))


                    'LoadBizDocs()
                    Dim dtDetails, dtBillingTerms As DataTable
                    Dim objPageLayout As New CPageLayout
                    objPageLayout.OpportunityId = lngOppID
                    objPageLayout.DomainID = Session("DomainID")
                    dtDetails = objPageLayout.OpportunityDetails.Tables(0)
                    hdnReplationship.Value = CCommon.ToLong(dtDetails.Rows(0)("numCompanyType"))
                    hdnProfile.Value = CCommon.ToLong(dtDetails.Rows(0)("vcProfile"))
                    hdnAccountClass.Value = CCommon.ToLong(dtDetails.Rows(0)("numAccountClass"))
                    lblCompany.Text = dtDetails.Rows(0).Item("vcCompanyName")
                    lblContact.Text = dtDetails.Rows(0).Item("numContactIdName")
                    hplOrderName.Text = dtDetails.Rows(0).Item("VcPoppName")
                    hplOrderName.Attributes.Add("onclick", "return OpenOpp('" + lngOppID.ToString + "','" + OppType.ToString + "')")
                    ViewState("DivisionID") = dtDetails.Rows(0).Item("numDivisionID")
                    hftintshipped.Value = dtDetails.Rows(0).Item("tintshipped")
                    lblForeignCurr.Text = "1 " & CCommon.ToString(dtDetails.Rows(0).Item("varCurrSymbol"))
                    txtExchangeRate.Text = CCommon.ToDecimal(dtDetails.Rows(0).Item("fltExchangeRate"))

                    If Session("BaseCurrencyID") = CCommon.ToString(dtDetails.Rows(0).Item("numCurrencyID")) Or CCommon.ToString(dtDetails.Rows(0).Item("numCurrencyID")) = 0 Then
                        trCurrency.Visible = False
                    Else
                        lblBaseCurr.Text = CCommon.ToString(Session("Currency")).Trim()
                        trCurrency.Visible = True
                    End If

                    If OppType = 1 Then
                        trCurrency.Visible = False
                    End If

                    hdnRecurrenceType.Value = dtDetails.Rows(0).Item("vcRecurrenceType")
                    'Fillcombo()

                    If lngPrevSelectedBizDoc > 0 Then
                        ddlBizDocs.SelectedValue = lngPrevSelectedBizDoc
                        ddlBizDocs_SelectedIndexChanged(Nothing, Nothing)
                        ddlBizDocTemplate.SelectedValue = lngPrevSelectedBizDocTemplate
                    End If

                    If OppBizDocID = 0 Then
                        objCommon = New CCommon
                        objCommon.DomainID = Session("DomainID")
                        objCommon.Mode = 34
                        objCommon.Str = lngOppID
                        txtRefOrderNo.Text = objCommon.GetSingleFieldValue()
                        calFrom.SelectedDate = DateTime.UtcNow

                        If GetQueryStringVal("frm") <> "" Then
                            chkPartial.Checked = True
                        End If

                        If CCommon.ToString(hdnRecurrenceType.Value) = "Sales Order" Then
                            tblRecurInsert.Visible = False
                            tblRecurDetail.Visible = False
                        End If

                        If Not radShipCompany.Items.FindItemByValue(dtDetails.Rows(0).Item("intUsedShippingCompany")) Is Nothing Then
                            radShipCompany.Items.FindItemByValue(dtDetails.Rows(0).Item("intUsedShippingCompany")).Selected = True
                        End If

                        LoadOppBizDocItems()
                    Else
                        getBizDocDetails(OppBizDocID)
                    End If

                    If OppType = 2 Then
                        dgBizDocs.Columns(9).Visible = False
                        dgBizDocs.Columns(14).Visible = False 'Serial/Lot #s
                    End If

                    If CCommon.ToLong(hdnAuthorizativeBizDocsId.Value) = ddlBizDocs.SelectedValue Then
                        m_aryRightsForAuthoritativ = GetUserRightsForPage_Other(10, 28)

                        If m_aryRightsForAuthoritativ(RIGHTSTYPE.VIEW) = 0 Then
                            Response.Redirect("../admin/authentication.aspx?mesg=AC")
                        Else
                            If m_aryRightsForAuthoritativ(RIGHTSTYPE.UPDATE) = 0 Then
                                btnSave.Visible = False
                                btnSaveClose.Visible = False
                            Else
                                btnSave.Visible = True
                                btnSaveClose.Visible = True
                            End If
                        End If

                    Else
                        m_aryRightsForNonAuthoritativ = GetUserRightsForPage_Other(10, 29)

                        If m_aryRightsForNonAuthoritativ(RIGHTSTYPE.VIEW) = 0 Then
                            Response.Redirect("../admin/authentication.aspx?mesg=AC")
                        Else
                            If m_aryRightsForNonAuthoritativ(RIGHTSTYPE.UPDATE) = 0 Then
                                btnSave.Visible = False
                                btnSaveClose.Visible = False
                            Else
                                btnSave.Visible = True
                                btnSaveClose.Visible = True
                            End If
                        End If
                    End If



                    If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                    objOppBizDocs.DomainID = CCommon.ToLong(Session("DomainID"))
                    objOppBizDocs.OppId = lngOppID
                    Dim lngDivId As Long = objOppBizDocs.GetDivisionId()

                    'KEEP IT LAST
                    If OppType = 1 Then
                        'GET DOMAIN RELEASE INVENTORY SETTING
                        objCommon.DomainID = Session("DomainID")
                        Dim dtCommitAllocation As DataTable = objCommon.GetDomainSettingValue("tintCommitAllocation")

                        If Not dtCommitAllocation Is Nothing AndAlso dtCommitAllocation.Rows.Count > 0 Then
                            hdnCommitAllocation.Value = CCommon.ToShort(CCommon.ToShort(dtCommitAllocation.Rows(0)("tintCommitAllocation")))

                            If CCommon.ToShort(dtCommitAllocation.Rows(0)("tintCommitAllocation")) = 2 Then
                                'GET CUSTOMER DEFAULT SETTING TO CHECK WHETHER INVENTORY NEEDS TO BE ALLOCATED ON PICK LIST
                                Dim objAccount As New BACRM.BusinessLogic.Account.CAccounts
                                objAccount.DivisionID = lngDivId
                                Dim dtSetting As DataTable = objAccount.GetDefaultSettingValue("bitAllocateInventoryOnPickList")

                                If Not dtSetting Is Nothing AndAlso dtSetting.Rows.Count > 0 Then
                                    hdnAllocateOnPickList.Value = CCommon.ToBool(dtSetting.Rows(0)("bitAllocateInventoryOnPickList"))

                                    If CCommon.ToBool(dtSetting.Rows(0)("bitAllocateInventoryOnPickList")) Then
                                        If Not ddlBizDocs.Items.FindByValue("296") Is Nothing Then
                                            ddlBizDocs.Items.FindByValue("296").Enabled = False
                                        End If
                                    End If
                                Else
                                    Throw New Exception("Not able to fetch customer default settings for Allocate inventory of pick list")
                                    Exit Sub
                                End If
                            End If
                        Else
                            Throw New Exception("Not able to fetch commmit allocation global settings value.")
                            Exit Sub
                        End If
                    End If

                    If mode <> "add" Then
                        If CCommon.ToLong(ddlBizDocs.SelectedValue) = 296 Then 'Fulfillment BizDoc
                            btnSave.Visible = False
                            btnSaveClose.Visible = False
                            btnUpdateBizDocFields.Visible = True
                        ElseIf CCommon.ToLong(ddlBizDocs.SelectedValue) = 29397 Then 'Pick List
                            If CCommon.ToShort(hdnCommitAllocation.Value) = 2 AndAlso CCommon.ToBool(hdnAllocateOnPickList.Value) AndAlso CCommon.ToBool(dtDetails.Rows(0)("tintshipped")) Then
                                btnSave.Visible = False
                                btnSaveClose.Visible = False
                                btnUpdateBizDocFields.Visible = True
                            ElseIf CCommon.ToShort(hdnCommitAllocation.Value) = 2 AndAlso CCommon.ToBool(hdnAllocateOnPickList.Value) AndAlso CCommon.ToBool(hdnFulfillmentGeneratedOnPickList.Value) Then
                                btnSave.Visible = False
                                btnSaveClose.Visible = False
                                btnUpdateBizDocFields.Visible = True
                            End If
                        ElseIf CCommon.ToLong(ddlBizDocs.SelectedValue) = 304 Then
                            If CCommon.ToLong(hdnInvoiceForDiferredBizDoc.Value) > 0 Then
                                btnSave.Visible = False
                                btnSaveClose.Visible = False
                                btnUpdateBizDocFields.Visible = True
                            End If
                        End If
                    End If
                End If

                btnSave.Attributes.Add("onclick", "return Save('" & CCommon.GetValidationDateFormat() & " h:m a')")
                btnSaveClose.Attributes.Add("onclick", "return Save('" & CCommon.GetValidationDateFormat() & " h:m a')")
                btnShippingLabel.Attributes.Add("onclick", "return openShippingLabel(" & lngOppID & "," & OppBizDocID & ")")
                txtBizDocSequenceId.Attributes.Add("onkeypress", "CheckNumber(2,event);")

                If Page.IsPostBack Then
                    If tblRecurInsert.Visible AndAlso chkEnableRecurring.Checked Then
                        trRecurring2.Style.Remove("display")
                    End If
                End If

                If hdnRecurrenceType.Value <> "" Then
                    If hdnRecurrenceType.Value = "Sales Order" Then
                        btnSave.Visible = False
                        btnSaveClose.Visible = False

                    ElseIf hdnRecurrenceType.Value = "Invoice" AndAlso hdnRecConfigID.Value <> "" Then
                        btnSave.Visible = False
                        btnSaveClose.Visible = False
                    End If
                End If

                If CCommon.ToBool(CCommon.ToInteger(hdnRecurredBizDoc.Value)) Or CCommon.ToBool(CCommon.ToInteger(hdnFulFilledItem.Value)) Then
                    btnSave.Visible = False
                    btnSaveClose.Visible = False
                End If

                If ddlBizDocs.SelectedValue = "305" Then
                    divVendorInvName.Visible = True
                Else
                    divVendorInvName.Visible = False
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub getBizDocDetails(ByVal lngOppBizDocID As Long)
            Try
                Dim dtOppBiDocDtl As DataTable
                If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                objOppBizDocs.OppBizDocId = lngOppBizDocID
                objOppBizDocs.OppId = lngOppID
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.UserCntID = Session("UserContactID")
                objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl
                If dtOppBiDocDtl.Rows.Count <> 0 Then
                    txtExchangeRate.Text = CCommon.ToDecimal(dtOppBiDocDtl.Rows(0).Item("fltExchangeRateBizDoc"))

                    If dtOppBiDocDtl.Rows(0).Item("bitPartialFulfilment") = True Then
                        chkPartial.Checked = True
                    End If

                    hdnISRentalBizDoc.Value = dtOppBiDocDtl.Rows(0).Item("bitRentalBizDoc")

                    If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtFromDate")) Then
                        calFrom.SelectedDate = dtOppBiDocDtl.Rows(0).Item("dtFromDate").ToString
                    End If

                    If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("numShipVia")) Then
                        If Not radShipCompany.Items.FindItemByValue(dtOppBiDocDtl.Rows(0).Item("numShipVia")) Is Nothing Then
                            radShipCompany.Items.FindItemByValue(dtOppBiDocDtl.Rows(0).Item("numShipVia")).Selected = True
                        End If
                    End If

                    If Not ddlAR.Items.FindByValue(CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("numARAccountID"))) Is Nothing Then
                        ddlAR.Items.FindByValue(CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("numARAccountID"))).Selected = True
                    End If

                    If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("numBizDocId")) Then
                        If Not ddlBizDocs.Items.FindByValue(dtOppBiDocDtl.Rows(0).Item("numBizDocId")) Is Nothing Then
                            ddlBizDocs.ClearSelection()
                            ddlBizDocs.SelectedValue = dtOppBiDocDtl.Rows(0).Item("numBizDocId").ToString
                        End If
                    End If

                    If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("numBizDocStatus")) Then
                        If Not ddlBizDocStatus.Items.FindByValue(dtOppBiDocDtl.Rows(0).Item("numBizDocStatus")) Is Nothing Then
                            ddlBizDocStatus.Items.FindByValue(dtOppBiDocDtl.Rows(0).Item("numBizDocStatus")).Selected = True
                        End If
                    End If

                    BindBizDocsTemplate()

                    If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("numBizDocTempID")) Then
                        If Not ddlBizDocTemplate.Items.FindByValue(dtOppBiDocDtl.Rows(0).Item("numBizDocTempID")) Is Nothing Then
                            ddlBizDocTemplate.ClearSelection()
                            ddlBizDocTemplate.SelectedValue = dtOppBiDocDtl.Rows(0).Item("numBizDocTempID").ToString
                        End If
                    End If

                    If dtOppBiDocDtl.Rows(0).Item("tintDeferred") = 1 Then
                        chkFromDate.Visible = True
                        chkFromDate.Checked = True
                        chkFromDate.Enabled = False
                    End If

                    lblShipAmount.Text = String.Format("{0:##,#00.00}", CCommon.ToDecimal(dtOppBiDocDtl.Rows(0).Item("monShipCost")))

                    lblBizDocName.Text = dtOppBiDocDtl.Rows(0).Item("vcBizDocName")
                    txtBizDocSequenceId.Text = dtOppBiDocDtl.Rows(0).Item("numSequenceId")
                    hdnBizDocSequenceId.Value = dtOppBiDocDtl.Rows(0).Item("numSequenceId")
                    txtVendorInvName.Text = dtOppBiDocDtl.Rows(0).Item("vcVendorInvoice")

                    If (CInt(dtOppBiDocDtl.Rows(0).Item("ShippingReportCount")) > 0) Then
                        hplShippingReport.NavigateUrl = "#"
                        hplShippingReport.Attributes.Add("onclick", "openReportList(" & lngOppID & "," & lngOppBizDocID & ")")
                        hplShippingReport.Text = "Shipping Report(" + CInt(dtOppBiDocDtl.Rows(0).Item("ShippingReportCount")).ToString() + ")"
                    End If

                    lblLastModifiedBy.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("ModifiedBy")), "", dtOppBiDocDtl.Rows(0).Item("ModifiedBy"))
                    lblCreatedOn.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("CreatedBy")), "", dtOppBiDocDtl.Rows(0).Item("CreatedBy"))
                    hdnComments.Value = dtOppBiDocDtl.Rows(0).Item("vcComments").ToString
                    hdnPONumber.Value = dtOppBiDocDtl.Rows(0).Item("vcRefOrderNo").ToString
                    txtTrackingNo.Text = dtOppBiDocDtl.Rows(0).Item("vcTrackingNo").ToString

                    txtRefOrderNo.Text = dtOppBiDocDtl.Rows(0).Item("vcRefOrderNo").ToString
                    hfnumDiscountAcntType.Value = CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numDiscountAcntType").ToString)

                    If dtOppBiDocDtl.Rows(0).Item("vcBizDocsStatusList").ToString.Length > 0 Then
                        Dim strFulfillmentCompleted As String


                        Dim strBizDocsStatusList() As String = dtOppBiDocDtl.Rows(0).Item("vcBizDocsStatusList").Split(New String() {"$^$"}, StringSplitOptions.None)
                        Dim strIDValue() As String

                        If strBizDocsStatusList.Length > 0 Then
                            For i As Integer = 0 To strBizDocsStatusList.Length - 1
                                strIDValue = strBizDocsStatusList(i).Split(New String() {"#^#"}, StringSplitOptions.None)

                                strFulfillmentCompleted += String.Format("<span class='tip {3}' title='<b>{0}</b> - Created By : {1} {2}'>{0}</span> ,", strIDValue(0), strIDValue(1), strIDValue(2), strIDValue(3))
                            Next
                        End If

                        lblFulfillmentCompleted.Text = strFulfillmentCompleted.Trim(",")
                        lblFulfillmentCompletedCaption.Visible = True
                    End If

                    'If recurrence is already created on sales order or bizdoc is not authorative bizdoc or its nor sales order 
                    'then user will not be able to see create recurrence panel
                    If CCommon.ToString(hdnRecurrenceType.Value) = "Sales Order" Or
                       OppType <> 1 Or
                       Not CCommon.ToBool(dtOppBiDocDtl.Rows(0)("bitAuthoritativeBizDocs")) Or
                       CCommon.ToBool(dtOppBiDocDtl.Rows(0)("bitRecurred")) Or
                       CCommon.ToBool(dtOppBiDocDtl.Rows(0)("bitOrderRecurred")) Then
                        tblRecurInsert.Visible = False
                        tblRecurDetail.Visible = False
                    Else
                        If CCommon.ToString(dtOppBiDocDtl.Rows(0)("numRecConfigID")) <> "0" Then
                            hdnRecurrenceType.Value = "Invoice"
                            tblRecurInsert.Visible = False
                            tblRecurDetail.Visible = True

                            Page.ClientScript.RegisterStartupScript(Me.GetType(), "RefreshParent", "<script language='javascript'>HideSalesOrderRecurrence()</script>")

                            hdnRecConfigID.Value = CCommon.ToString(dtOppBiDocDtl.Rows(0)("numRecConfigID"))
                            lblFrequency.Text = CCommon.ToString(dtOppBiDocDtl.Rows(0)("vcFrequency"))
                            lblStartDate.Text = CCommon.ToString(dtOppBiDocDtl.Rows(0)("dtStartDate"))

                            If CCommon.ToBool(dtOppBiDocDtl.Rows(0)("bitDisabled")) Then
                                ddlRecurStatus.SelectedValue = 2
                                ddlRecurStatus.Enabled = False
                                lblRecurStatus.Visible = True
                            Else
                                ddlRecurStatus.SelectedValue = 1
                                ddlRecurStatus.Enabled = True
                                lblRecurStatus.Visible = False
                            End If
                        Else
                            tblRecurInsert.Visible = True
                            tblRecurDetail.Visible = False
                        End If
                    End If

                    If CCommon.ToBool(dtOppBiDocDtl.Rows(0)("bitRecurred")) Then
                        hdnRecurredBizDoc.Value = "1"
                    Else
                        hdnRecurredBizDoc.Value = "0"
                    End If

                    If CCommon.ToBool(dtOppBiDocDtl.Rows(0)("bitFulfilled")) Then
                        hdnFulFilledItem.Value = "1"
                    Else
                        hdnFulFilledItem.Value = "0"
                    End If

                    hdnDeferredBizDocID.Value = CCommon.ToLong(dtOppBiDocDtl.Rows(0)("numDeferredBizDocID"))

                    If ddlBizDocs.SelectedValue = "287" Or ddlBizDocs.SelectedValue = "296" Then
                        divAR.Visible = True
                    Else
                        divAR.Visible = False
                    End If

                    Dim lngShippingService As Long = CCommon.ToLong(dtOppBiDocDtl.Rows(0)("numShippingService"))
                    hdnShippingService.Value = lngShippingService
                    If lngShippingService >= 10 And lngShippingService <= 28 Then
                        If Not radShipCompany.Items.FindItemByValue("91") Is Nothing Then
                            radShipCompany.Items.FindItemByValue("91").Selected = True
                        End If
                    ElseIf (lngShippingService = 40 Or
                            lngShippingService = 42 Or
                            lngShippingService = 43 Or
                            lngShippingService = 48 Or
                            lngShippingService = 49 Or
                            lngShippingService = 50 Or
                            lngShippingService = 51 Or
                            lngShippingService = 55) Then
                        If Not radShipCompany.Items.FindItemByValue("88") Is Nothing Then
                            radShipCompany.Items.FindItemByValue("88").Selected = True
                        End If
                    ElseIf lngShippingService >= 70 And lngShippingService <= 76 Then
                        If Not radShipCompany.Items.FindItemByValue("90") Is Nothing Then
                            radShipCompany.Items.FindItemByValue("90").Selected = True
                        End If
                    End If

                    hdnFulfillmentGeneratedOnPickList.Value = CCommon.ToBool(dtOppBiDocDtl.Rows(0)("IsFulfillmentGeneratedOnPickList"))
                    hdnInvoiceForDiferredBizDoc.Value = CCommon.ToLong(dtOppBiDocDtl.Rows(0)("intInvoiceForDiferredBizDoc"))
                End If
                '''Added by Anoop Jayaraj 09/05/2009 ++++++++++++++++++++++

                LoadOppBizDocItems()
                ''''-------------------------------------------

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub Fillcombo()
            Try
                Dim objCommon As New CCommon
                Dim dtMasterItem As DataTable
                dtMasterItem = objCommon.GetMasterListItemsForBizDocs(27, Session("DomainID"), lngOppID)

                If objCommon Is Nothing Then objCommon = New CCommon
                radShipCompany.DataSource = objCommon.GetMasterListItems(82, Session("DomainID"))
                radShipCompany.DataTextField = "vcData"
                radShipCompany.DataValueField = "numListItemID"
                radShipCompany.DataBind()

                Dim listItem As Telerik.Web.UI.RadComboBoxItem
                If radShipCompany.Items.FindItemByText("Fedex") IsNot Nothing Then
                    radShipCompany.Items.Remove(radShipCompany.Items.FindItemByText("Fedex"))
                End If
                If radShipCompany.Items.FindItemByText("UPS") IsNot Nothing Then
                    radShipCompany.Items.Remove(radShipCompany.Items.FindItemByText("UPS"))
                End If
                If radShipCompany.Items.FindItemByText("USPS") IsNot Nothing Then
                    radShipCompany.Items.Remove(radShipCompany.Items.FindItemByText("USPS"))
                End If

                listItem = New Telerik.Web.UI.RadComboBoxItem
                listItem.Text = "-- Select One --"
                listItem.Value = "0"
                radShipCompany.Items.Insert(0, listItem)

                listItem = New Telerik.Web.UI.RadComboBoxItem
                listItem.Text = "Fedex"
                listItem.Value = "91"
                listItem.ImageUrl = "../images/FedEx.png"
                radShipCompany.Items.Insert(1, listItem)

                listItem = New Telerik.Web.UI.RadComboBoxItem
                listItem.Text = "UPS"
                listItem.Value = "88"
                listItem.ImageUrl = "../images/UPS.png"
                radShipCompany.Items.Insert(2, listItem)

                listItem = New Telerik.Web.UI.RadComboBoxItem
                listItem.Text = "USPS"
                listItem.Value = "90"
                listItem.ImageUrl = "../images/USPS.png"
                radShipCompany.Items.Insert(3, listItem)

                ddlBizDocs.DataSource = dtMasterItem
                ddlBizDocs.DataTextField = "vcData"
                ddlBizDocs.DataValueField = "numListItemID"
                ddlBizDocs.DataBind()
                ddlBizDocs.Items.Insert(0, "--Select One--")
                ddlBizDocs.Items.FindByText("--Select One--").Value = "0"

                Dim objOpportunity As New MOpportunity
                objOpportunity.OpportunityId = lngOppID
                Dim dt As DataSet = objOpportunity.CheckOrderedAndInvoicedOrBilledQty()
                Dim bitNoQtyLeftForDiferredBizDoc As Boolean = CCommon.ToBool(dt.Tables(0).Select("ID=5")(0)("bitTrue"))

                'DISABLE DEFERRED BIZDOC IF FUNCTIONALITY IS NOT ENABLED FROM GLOBAL SETTINGS --> ACCOUNTING
                If Not CCommon.ToBool(Session("IsEnableDeferredIncome")) Then
                    If Not ddlBizDocs.Items.FindByValue("304") Is Nothing Then
                        ddlBizDocs.Items.FindByValue("304").Attributes.Add("style", "color:lightgray")
                        ddlBizDocs.Items.FindByValue("304").Enabled = False
                    End If
                ElseIf OppType = 1 AndAlso bitNoQtyLeftForDiferredBizDoc Then 'DISABLE DEFERRED BIZDOC IF ALL ITEMS QTY IS ADDED IN INVOICE OR DIFERRED DIZDOC
                    If Not ddlBizDocs.Items.FindByValue("304") Is Nothing Then
                        ddlBizDocs.Items.FindByValue("304").Attributes.Add("style", "color:lightgray")
                        ddlBizDocs.Items.FindByValue("304").Enabled = False
                    End If
                End If


            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub bindARAccounts()
            Try
                Dim objCOA As New ChartOfAccounting
                objCOA.DomainID = CCommon.ToLong(Session("DomainID"))
                Dim dt As DataTable = objCOA.GetDefaultARAndChildAccounts()
                ddlAR.DataSource = dt
                ddlAR.DataTextField = "vcAccountName"
                ddlAR.DataValueField = "numAccountID"
                ddlAR.DataBind()

                ddlAR.Items.Insert(0, New ListItem("--Select One --", "0"))

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    If Not ddlAR.Items.FindByValue(CCommon.ToString(dt.Rows(0)("numDefaultARAccountID"))) Is Nothing Then
                        ddlAR.Items.FindByValue(CCommon.ToString(dt.Rows(0)("numDefaultARAccountID"))).Selected = True
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub btnSaveCloseWindow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveCloseWindow.Click
            Try

                Dim isExitSub As Boolean
                Save(isExitSub, True)
                If isExitSub Then
                    Exit Sub
                End If
                'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "OpenBizDoc", "OpenBizInvoice(" & lngOppID & "," & OppBizDocID & "," & OppType & ");", True)
                Page_Redirect()
            Catch ex As Exception
                If ex.Message = "FY_CLOSED" Then
                    litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Response.Write(ex)
                End If
            End Try
        End Sub
        Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try

                Dim isExitSub As Boolean
                Save(isExitSub, True)
                If isExitSub Then
                    Exit Sub
                End If
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "OpenBizDoc", "OpenBizInvoice(" & lngOppID & "," & OppBizDocID & "," & OppType & ");", True)
                'Page_Redirect()
            Catch ex As Exception
                If ex.Message = "FY_CLOSED" Then
                    litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Response.Write(ex)
                End If
            End Try
        End Sub

        Function Page_Redirect()
            Try
                If GetQueryStringVal("frm") = "SalesFulfillment" Then
                    Response.Redirect("frmSalesFulfillmentOrder.aspx", False)
                ElseIf GetQueryStringVal("frm") = "BizDocs" Then
                    Response.Redirect("frmOpenBizDocs.aspx", False)
                Else
                    Response.Redirect("frmBizDocs.aspx?OpID=" & lngOppID & "&OppType=" & OppType, False)
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub Save(ByRef isExitSub As Boolean, isValidateSequenceID As Boolean)
            Try
                Dim dtStartDate As Date
                Dim errorMessage As String

                If tblRecurDetail.Visible Then
                    litMessage.Text = "You can't edit recurring bizdoc."
                    isExitSub = True : Exit Sub
                End If

                Dim txtStartDate As TextBox = CType(calRecurStart.FindControl("txtDate"), TextBox)

                If OppType = 1 AndAlso tblRecurInsert.Visible Then
                    If chkEnableRecurring.Checked Then
                        If Not String.IsNullOrEmpty(txtStartDate.Text) Then
                            Dim isValidStartDate = Date.TryParse(txtStartDate.Text, dtStartDate)

                            If Not isValidStartDate Then
                                errorMessage = "Enter valid start date."
                            ElseIf dtStartDate <= DateAndTime.Now.Date Then
                                errorMessage = "Start must be after today."
                            End If
                        Else
                            errorMessage = "Recurrence start is required."
                        End If
                    End If
                End If

                If String.IsNullOrEmpty(errorMessage) Then
                    If isValidateSequenceID AndAlso ddlBizDocs.SelectedItem.Value > 0 AndAlso CCommon.ToLong(txtBizDocSequenceId.Text.Trim()) > 0 _
                            AndAlso CCommon.ToLong(txtBizDocSequenceId.Text.Trim()) <> CCommon.ToLong(hdnBizDocSequenceId.Value) Then

                        If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs

                        objOppBizDocs.DomainID = Session("DomainID")
                        objOppBizDocs.OppId = lngOppID
                        objOppBizDocs.OppBizDocId = OppBizDocID
                        objOppBizDocs.BizDocId = ddlBizDocs.SelectedItem.Value
                        objOppBizDocs.SequenceId = CCommon.ToLong(txtBizDocSequenceId.Text.Trim())

                        Dim dtSequenceID As DataTable = objOppBizDocs.ValidateBizDocSequenceId()

                        If dtSequenceID.Rows.Count > 0 Then
                            litMessage.Text = "You must specify a different BizDoc Sequence #. <b>" & txtBizDocSequenceId.Text & "</b> has already been used. Save it anyway?"
                            btnAllowBizDocSequenceId.Visible = True
                            isExitSub = True
                            Exit Sub
                        End If
                    End If

                    Dim isInvalidRemainingPercent As Boolean = False
                    For Each dgBizGridItem As DataGridItem In dgBizDocs.Items
                        If CType(dgBizGridItem.FindControl("chkInclude"), CheckBox).Checked = True Then
                            If CCommon.ToLong(CType(dgBizGridItem.FindControl("txtRemainingPercent"), TextBox).Text) > CCommon.ToLong(CType(dgBizGridItem.FindControl("hdnRemainingPercent"), HiddenField).Value) Then
                                isExitSub = True
                                Exit Sub
                            End If
                        End If
                    Next

                    If isInvalidRemainingPercent Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Percentage entered to include in bizdoc can not be greater than remaining percentage left to be added in BizDoc' );", True)
                        isExitSub = True : Exit Sub
                    End If

                    isExitSub = False
                    Dim JournalId As Long
                    Dim lintAuthorizativeBizDocsId As Integer
                    If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                    Dim lintOpportunityType As Integer
                    Dim objJournal As New JournalEntry
                    Dim EditMode As Boolean
                    If OppBizDocID > 0 Then EditMode = True Else EditMode = False

                    Dim bAddBizDoc As Boolean = True

                    If ddlBizDocs.SelectedItem.Value > 0 Then
                        Dim i As Integer
                        objOppBizDocs.DomainID = Session("DomainID")
                        objOppBizDocs.OppId = lngOppID
                        lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()

                        If lintAuthorizativeBizDocsId = ddlBizDocs.SelectedItem.Value Then 'save bizdoc only if selected company's AR and AP account is mapped
                            If objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), ViewState("DivisionID")) = 0 Then
                                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip""  for " & lblCompany.Text & " To Save BizDoc' );", True)
                                isExitSub = True : Exit Sub
                            End If
                        End If
                        If OppType = 2 Then
                            'Accounting validation->Do not allow to save PO untill Default COGs account is mapped
                            If ChartOfAccounting.GetDefaultAccount("CG", Session("DomainID")) = 0 Then
                                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default COGs account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                                isExitSub = True : Exit Sub
                            End If
                        End If

                        If OppType = 2 Then
                            If ChartOfAccounting.GetDefaultAccount("PC", Session("DomainID")) = 0 Then
                                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Purchase Clearing account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                                isExitSub = True : Exit Sub
                            End If

                            If ChartOfAccounting.GetDefaultAccount("PV", Session("DomainID")) = 0 Then
                                ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Purchase Price Variance account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                                isExitSub = True : Exit Sub
                            End If
                        End If

                        bAddBizDoc = IIf(OppBizDocID = 0, True, False)

                        objOppBizDocs.OppBizDocId = OppBizDocID
                        objOppBizDocs.BizDocId = ddlBizDocs.SelectedItem.Value
                        objOppBizDocs.UserCntID = Session("UserContactID")
                        lintOpportunityType = objOppBizDocs.GetOpportunityType()
                        objOppBizDocs.OppType = lintOpportunityType
                        objOppBizDocs.bitPartialShipment = True 'chkPartial.Checked
                        objOppBizDocs.ShipCompany = radShipCompany.SelectedValue 'ddlShipCompany.SelectedValue
                        objOppBizDocs.FromDate = CDate(calFrom.SelectedDate & " 00:00:00").AddMinutes(Convert.ToInt32(Session("ClientMachineUTCTimeOffset")))
                        objOppBizDocs.BizDocStatus = ddlBizDocStatus.SelectedValue
                        objOppBizDocs.SequenceId = txtBizDocSequenceId.Text
                        objOppBizDocs.vcComments = Replace(hdnComments.Value, "'", "''")
                        objOppBizDocs.vcPONo = hdnPONumber.Value
                        objOppBizDocs.strBizDocItems = GetBizDocItems()
                        objOppBizDocs.Deferred = IIf(chkFromDate.Checked = True, 1, 0)
                        objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                        objOppBizDocs.boolRentalBizDoc = CCommon.ToBool(hdnISRentalBizDoc.Value)
                        objOppBizDocs.BizDocTemplateID = ddlBizDocTemplate.SelectedValue
                        objOppBizDocs.TrackingNo = txtTrackingNo.Text
                        objOppBizDocs.RefOrderNo = txtRefOrderNo.Text
                        objOppBizDocs.fltExchangeRateBizDoc = CCommon.ToDecimal(txtExchangeRate.Text)
                        If ddlBizDocs.SelectedValue = "305" Then
                            objOppBizDocs.VendorInvoiceName = txtVendorInvName.Text
                        End If
                        Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                            Try
                                If OppType = 1 Then
                                    If chkEnableRecurring.Checked AndAlso tblRecurInsert.Visible Then
                                        Dim x As XmlDocument = New XmlDocument()
                                        x.LoadXml(objOppBizDocs.strBizDocItems)

                                        If x.FirstChild Is Nothing Or x.FirstChild.FirstChild Is Nothing Then
                                            litMessage.Text = "You have to select atleast one item because you have selected recur bizdoc option."
                                            isExitSub = True : Exit Sub
                                        End If

                                        objOppBizDocs.Recur = chkEnableRecurring.Checked
                                        objOppBizDocs.Frequency = CCommon.ToShort(ddlFrequency.SelectedValue)
                                        objOppBizDocs.FrequencyName = ddlFrequency.SelectedItem.Text
                                        objOppBizDocs.StartDate = dtStartDate
                                    ElseIf tblRecurDetail.Visible AndAlso ddlRecurStatus.SelectedValue = "2" Then
                                        objOppBizDocs.RecurConfigID = CCommon.ToLong(hdnRecConfigID.Value)
                                        objOppBizDocs.DisableRecur = True
                                    End If
                                End If

                                If divAR.Visible Then
                                    objOppBizDocs.ARAccountID = CCommon.ToLong(ddlAR.SelectedValue)
                                End If

                                OppBizDocID = objOppBizDocs.SaveBizDoc()
                            Catch ex As Exception
                                If ex.Message = "NOT_ALLOWED" Then
                                    litMessage.Text = "To split order items multiple bizdocs you must create all bizdocs with ""partial"" checked!"
                                    isExitSub = True : Exit Sub
                                ElseIf ex.Message = "NOT_ALLOWED_FulfillmentOrder" Then
                                    litMessage.Text = "Only a Sales Order can create a Fulfillment Order"
                                    isExitSub = True : Exit Sub
                                ElseIf ex.Message = "AlreadyInvoice" Then
                                    litMessage.Text = "You can�t create a Invoice against a Fulfillment Order that already contains a Invoice"
                                    isExitSub = True : Exit Sub
                                ElseIf ex.Message = "AlreadyPackingSlip" Then
                                    litMessage.Text = "You can�t create a Packing-Slip against a Fulfillment Order that already contains a Packing-Slip"
                                    isExitSub = True : Exit Sub
                                ElseIf ex.Message = "AlreadyInvoice_NOT_ALLOWED_FulfillmentOrder" Then
                                    litMessage.Text = "You can�t create a fulfillment order against a sales order that already contains an Invoice or Packing-Slip"
                                    isExitSub = True : Exit Sub
                                ElseIf ex.Message = "AlreadyFulfillmentOrder" Then
                                    litMessage.Text = "Manually adding Invoices or Packing Slips to a Sales Order is not allowed after a Fulfillment Order (FO) is added. Your options are to edit the FO status to trigger creation of Invoices and/or Packing Slips, Delete the Fulfillment Order, or Create Invoices and/or Packing Slips from the Sales Fulfillment section"
                                    isExitSub = True : Exit Sub
                                ElseIf ex.Message = "RECURRENCE_ALREADY_CREATED_FOR_ORDER" Then
                                    litMessage.Text = "You can not set recurrence on bizdoc because someone has already created recurrence for this order."
                                    isExitSub = True : Exit Sub
                                ElseIf ex.Message = "RECURRENCE_NOT_ALLOWED_ALL_ITEM_QUANTITY_ADDED_TO_INVOICE" Then
                                    litMessage.Text = "You can not set recurrence on bizdoc because total quantity of selected items are used in authorative bizdocs and no quantity is available now for recurring bizdoc."
                                    isExitSub = True : Exit Sub
                                ElseIf ex.Message.Contains("WORK_ORDER_NOT_COMPLETED") Then
                                    litMessage.Text = "You can't ship at this time because Work Order for this Sales Order is pending."
                                    isExitSub = True : Exit Sub
                                Else
                                    Throw ex
                                End If
                            End Try

                            If OppBizDocID = 0 Then
                                litMessage.Text = "A BizDoc by the same name is already created. Please select another BizDoc !"
                                isExitSub = True : Exit Sub
                            End If
                            hdnOppBizDocID.Value = OppBizDocID

                            'chkFromDate.Checked then Deferred : Not Create Account Entries
                            If (lintAuthorizativeBizDocsId = ddlBizDocs.SelectedItem.Value AndAlso chkFromDate.Checked = False) Or ddlBizDocs.SelectedValue = 304 Then
                                '' Authorizative Accounting BizDocs

                                ''--------------------------------------------------------------------------''
                                Dim ds As New DataSet
                                objOppBizDocs.OppBizDocId = OppBizDocID
                                ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                                dtOppBiDocItems = ds.Tables(0)

                                Dim objCalculateDealAmount As New CalculateDealAmount
                                objCalculateDealAmount.CalculateDealAmount(lngOppID, OppBizDocID, lintOpportunityType, Session("DomainID"), dtOppBiDocItems)

                                If EditMode = False Then
                                    'Create new journal id
                                    JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, Entry_Date:=IIf(calFrom.SelectedDate <> "", CDate(calFrom.SelectedDate), Date.UtcNow), Description:=ds.Tables(1).Rows(0).Item("vcBizDocID"))
                                Else
                                    'Get existing journal id and delete old and insert new journal entries
                                    JournalId = objOppBizDocs.GetJournalIdForAuthorizativeBizDocs
                                    'When From COA someone deleted journals then we need to create new journal header entry
                                    If JournalId = 0 Then
                                        JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, Entry_Date:=IIf(calFrom.SelectedDate <> "", CDate(calFrom.SelectedDate), Date.UtcNow), Description:=ds.Tables(1).Rows(0).Item("vcBizDocID"))
                                    Else
                                        JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, lngJournalId:=JournalId, Entry_Date:=IIf(calFrom.SelectedDate <> "", CDate(calFrom.SelectedDate), Date.UtcNow), Description:=ds.Tables(1).Rows(0).Item("vcBizDocID"))
                                    End If
                                End If

                                Dim objJournalEntries As New JournalEntry
                                If lintOpportunityType = 1 Then
                                    If ddlBizDocs.SelectedValue = 304 AndAlso CCommon.ToBool(Session("IsEnableDeferredIncome")) Then 'Deferred Revenue
                                        objJournalEntries.SaveJournalEntriesDeferredIncome(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, ViewState("DivisionID"), OppBizDocID, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), objCalculateDealAmount.GrandTotal)
                                    Else
                                        If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                                            objJournalEntries.SaveJournalEntriesSales(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ViewState("DivisionID"), ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), DiscAcntType:=hfnumDiscountAcntType.Value, lngShippingMethod:=radShipCompany.SelectedValue, bitInvoiceAgainstDeferredIncomeBizDoc:=CCommon.ToLong(hdnDeferredBizDocID.Value) > 0)
                                        Else
                                            objJournalEntries.SaveJournalEntriesSalesNew(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ViewState("DivisionID"), ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), DiscAcntType:=hfnumDiscountAcntType.Value, lngShippingMethod:=radShipCompany.SelectedValue, bitInvoiceAgainstDeferredIncomeBizDoc:=CCommon.ToLong(hdnDeferredBizDocID.Value) > 0)
                                        End If

                                        If bAddBizDoc AndAlso Session("AutolinkUnappliedPayment") Then
                                            Dim objSalesFulfillment As New SalesFulfillmentWorkflow
                                            objSalesFulfillment.DomainID = Session("DomainID")
                                            objSalesFulfillment.UserCntID = Session("UserContactID")
                                            objSalesFulfillment.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                                            objSalesFulfillment.OppId = lngOppID
                                            objSalesFulfillment.OppBizDocId = OppBizDocID

                                            objSalesFulfillment.AutoLinkUnappliedPayment()
                                        End If
                                    End If
                                Else
                                    If CCommon.ToBool(ds.Tables(1).Rows(0).Item("bitPPVariance")) Then
                                        objJournalEntries.SaveJournalEntriesPurchaseVariance(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, ViewState("DivisionID"), OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), ds.Tables(1).Rows(0).Item("fltExchangeRateBizDoc"), DiscAcntType:=hfnumDiscountAcntType.Value, lngShippingMethod:=radShipCompany.SelectedValue, vcBaseCurrency:=ds.Tables(1).Rows(0).Item("vcBaseCurrency"), vcForeignCurrency:=ds.Tables(1).Rows(0).Item("vcForeignCurrency"))
                                    Else
                                        objJournalEntries.SaveJournalEntriesPurchase(lngOppID, Session("DomainID"), dtOppBiDocItems, JournalId, ViewState("DivisionID"), OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), DiscAcntType:=hfnumDiscountAcntType.Value, lngShippingMethod:=radShipCompany.SelectedValue)
                                    End If
                                End If


                                ''---------------------------------------------------------------------------------
                            ElseIf ddlBizDocs.SelectedValue = 296 Then 'Fulfillment BizDoc
                                'AUTO FULFILLS bizdoc AND MAKED QTY AS SHIPPED AND RELEASES IT FROM ALLOCATION
                                Dim objOppBizDoc As New OppBizDocs
                                objOppBizDoc.DomainID = CCommon.ToLong(Session("DomainID"))
                                objOppBizDoc.UserCntID = CCommon.ToLong(Session("UserContactID"))
                                objOppBizDoc.OppId = lngOppID
                                objOppBizDoc.OppBizDocId = OppBizDocID
                                objOppBizDoc.AutoFulfillBizDoc()


                                'IMPORTANT: WE ARE MAKING ACCOUNTING CHANGES BECAUSE WE ARE CREATING FULFILLMENT ORDER IN BACKGROUP AND MARKIGN IT AS SHIPPED
                                '           OTHERWISE ACCOUNTING IS ONLY IMPACTED WHEN USER SHIPPED IT FROM SALES FULFILLMENT PAGE
                                'CREATE SALES CLEARING ENTERIES
                                'CREATE JOURNALS ONLY FOR FILFILLMENT BIZDOCS
                                '-------------------------------------------------
                                Dim ds As New DataSet
                                Dim dtOppBiDocItems As DataTable

                                objOppBizDocs.OppBizDocId = OppBizDocID
                                ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                                dtOppBiDocItems = ds.Tables(0)

                                Dim objCalculateDealAmount As New CalculateDealAmount
                                objCalculateDealAmount.CalculateDealAmount(lngOppID, OppBizDocID, 1, CCommon.ToLong(Session("DomainID")), dtOppBiDocItems)

                                'WHEN FROM COA SOMEONE DELETED JOURNALS THEN WE NEED TO CREATE NEW JOURNAL HEADER ENTRY
                                JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, Entry_Date:=Date.UtcNow.Date, Description:=CStr(ds.Tables(1).Rows(0).Item("vcBizDocID")))

                                Dim objJournalEntries As New BACRM.BusinessLogic.Accounting.JournalEntry
                                If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                                    objJournalEntries.SaveJournalEnteriesSalesClearing(lngOppID, CCommon.ToLong(Session("DomainID")), dtOppBiDocItems, JournalId, ViewState("DivisionID"), OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                                Else
                                    objJournalEntries.SaveJournalEnteriesSalesClearingNew(lngOppID, CCommon.ToLong(Session("DomainID")), dtOppBiDocItems, JournalId, ViewState("DivisionID"), OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                                End If
                                '---------------------------------------------------------------------------------
                            Else

                                'Distibute Total cost among bizdoc items and remove old cost when no cost specified
                                'For Auth sales bizdoc below code gets excuted after actual jouranl entries are created
                                Dim objOpp As New MOpportunity
                                objOpp.DomainID = Session("DomainID")
                                objOpp.CostCategoryID = 0
                                objOpp.OppBizDocID = OppBizDocID
                                objOpp.Mode = 1
                                objOpp.ManageEmbeddedCost()
                            End If

                            objTransactionScope.Complete()
                        End Using

                        'Added By Sachin Sadhu||Date:29thApril12014
                        'Purpose :To Added Opportunity data in work Flow queue based on created Rules
                        '          Using Change tracking
                        Dim objWfA As New Workflow()
                        objWfA.DomainID = Session("DomainID")
                        objWfA.UserCntID = Session("UserContactID")
                        objWfA.RecordID = OppBizDocID
                        objWfA.SaveWFBizDocQueue()
                        'end of code
                    End If

                    If OppBizDocID > 0 AndAlso ddlBizDocs.SelectedValue = "296" Then
                        Try
                            objOppBizDocs.CreateInvoiceFromFulfillmentOrder(CCommon.ToLong(Session("DomainID")), CCommon.ToLong(Session("UserContactID")), lngOppID, OppBizDocID, CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")))
                        Catch ex As Exception
                            'DO NOT THROW ERROR
                        End Try
                    End If
                Else
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "RecurValidation", "alert('" & errorMessage & "' );", True)
                    isExitSub = True : Exit Sub
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                Page_Redirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub chkPartial_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkPartial.CheckedChanged
            Try
                LoadOppBizDocItems()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlBizDocs_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBizDocs.SelectedIndexChanged
            Try
                Dim lintAuthorizativeBizDocsId As Integer
                If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs

                trRecurring1.Visible = False
                trRecurring2.Style.Add("display", "none")
                chkEnableRecurring.Checked = False
                ddlFrequency.SelectedIndex = 0
                Dim txtDate As TextBox = CType(calRecurStart.FindControl("txtDate"), TextBox)

                If Not txtDate Is Nothing Then
                    txtDate.Text = ""
                End If

                If ddlBizDocs.SelectedItem.Value > 0 Then
                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.Mode = 33
                    objCommon.Str = ddlBizDocs.SelectedItem.Value
                    txtBizDocSequenceId.Text = objCommon.GetSingleFieldValue()

                    Dim i As Integer
                    objOppBizDocs.DomainID = Session("DomainID")
                    objOppBizDocs.OppId = lngOppID
                    lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()

                    If OppType = 1 AndAlso lintAuthorizativeBizDocsId = ddlBizDocs.SelectedItem.Value Then
                        dgBizDocs.Columns(11).Visible = True
                        dgBizDocs.Columns(14).Visible = False 'Serial/Lot #s
                        chkFromDate.Visible = True
                        chkFromDate.Checked = False

                        If tblRecurInsert.Visible = True Then
                            trRecurring1.Visible = True
                        End If
                    Else
                        dgBizDocs.Columns(11).Visible = False
                        chkFromDate.Visible = False
                        chkFromDate.Checked = False

                        dgBizDocs.Columns(14).Visible = False 'Serial/Lot #s
                    End If

                    If ddlBizDocs.SelectedValue = "287" Or ddlBizDocs.SelectedValue = "296" Then
                        divAR.Visible = True
                    Else
                        divAR.Visible = False
                    End If
                Else
                    dgBizDocs.Columns(11).Visible = False
                    chkFromDate.Visible = False
                    chkFromDate.Checked = False
                End If

                chkPartial.Enabled = True
                BindBizDocsTemplate()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindBizDocsTemplate()
            Try
                If ddlBizDocs.SelectedValue <> 0 Then
                    Dim objOppBizDoc As New OppBizDocs
                    objOppBizDoc.DomainID = Session("DomainID")
                    objOppBizDoc.BizDocId = ddlBizDocs.SelectedValue
                    objOppBizDoc.OppType = OppType
                    objOppBizDoc.byteMode = 0

                    Dim dtBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()

                    ddlBizDocTemplate.DataSource = dtBizDocTemplate
                    ddlBizDocTemplate.DataTextField = "vcTemplateName"
                    ddlBizDocTemplate.DataValueField = "numBizDocTempID"
                    ddlBizDocTemplate.DataBind()

                    ddlBizDocTemplate.Items.Insert(0, New ListItem("--Select--", 0))

                    objOppBizDoc.byteMode = 1
                    objOppBizDoc.Relationship = CCommon.ToLong(hdnReplationship.Value)
                    objOppBizDoc.Profile = CCommon.ToLong(hdnProfile.Value)
                    objOppBizDoc.AccountClass = CCommon.ToLong(hdnAccountClass.Value)
                    Dim dtDefaultBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()

                    If Not IsDBNull(dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID")) Then
                        If Not ddlBizDocTemplate.Items.FindByValue(dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID")) Is Nothing Then
                            ddlBizDocTemplate.ClearSelection()
                            ddlBizDocTemplate.SelectedValue = dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID").ToString
                        End If
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadOppBizDocItems()
            Try
                dgBizDocs.Visible = True
                If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                objOppBizDocs.BizDocId = ddlBizDocs.SelectedValue 'CCommon.ToLong(IIf(ddlBizDocs.Enabled, ddlBizDocs.SelectedValue, Request(ddlBizDocs.ClientID)))
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.OppId = lngOppID
                objOppBizDocs.OppBizDocId = OppBizDocID
                objOppBizDocs.boolRentalBizDoc = CCommon.ToBool(hdnISRentalBizDoc.Value)

                Dim dtOppItems As DataTable = objOppBizDocs.GetOppItemsForBizDocs()

                If Not Page.IsPostBack Then
                    Dim viewReleaseDates As New DataView(dtOppItems)
                    Dim dtReleaseDates As DataTable = viewReleaseDates.ToTable(True, "dtReleaseDate")
                    Dim listitem As ListItem
                    Dim releaseDate As Date
                    For Each drRelease As DataRow In dtReleaseDates.Rows
                        If Date.TryParse(CCommon.ToString(drRelease("dtReleaseDate")), releaseDate) Then
                            listitem = New ListItem
                            listitem.Text = FormattedDateFromDate(releaseDate, Session("DateFormat"))
                            listitem.Value = drRelease("dtReleaseDate")
                            ddlReleaseDates.Items.Add(listitem)
                        End If
                       
                    Next

                    ddlReleaseDates.Items.Insert(0, New ListItem("-- Select One --", "0"))
                End If

                If ddlReleaseDates.SelectedValue <> "0" Then
                    dtOppItems = dtOppItems.Select("dtReleaseDate='" & ddlReleaseDates.SelectedValue & "'").CopyToDataTable()
                End If

                Dim dt As DataTable

                If CCommon.ToBool(hdnISRentalBizDoc.Value) = True AndAlso OppType = 1 AndAlso CCommon.ToLong(hdnAuthorizativeBizDocsId.Value) = ddlBizDocs.SelectedValue AndAlso CCommon.ToBool(Session("bitRentalItem")) = True AndAlso CCommon.ToLong(Session("RentalItemClass")) > 0 Then
                    Dim view As DataView = New DataView
                    view.Table = dtOppItems
                    view.RowFilter = "numItemClassification<>" & CCommon.ToLong(Session("RentalItemClass"))
                    dgBizDocs.DataSource = view
                    chkPartial.Checked = True
                    chkPartial.Enabled = False
                Else
                    dgBizDocs.DataSource = dtOppItems
                End If

                dgBizDocs.DataBind()
                If chkPartial.Checked = True AndAlso CCommon.ToBool(hdnISRentalBizDoc.Value) = False Then
                    For Each dgGItem As DataGridItem In dgBizDocs.Items
                        CType(dgGItem.FindControl("txtQuantityFulfilled"), TextBox).Enabled = True
                    Next
                Else
                    For Each dgGItem As DataGridItem In dgBizDocs.Items
                        CType(dgGItem.FindControl("chkInclude"), CheckBox).Enabled = IIf(CCommon.ToBool(hdnISRentalBizDoc.Value) = True, False, True)

                        CType(dgGItem.FindControl("txtQuantityFulfilled"), TextBox).Enabled = False
                        If OppBizDocID = 0 Then
                            CType(dgGItem.FindControl("txtQuantityFulfilled"), TextBox).Text = dgGItem.Cells(5).Text
                        End If
                    Next
                End If
                If OppType = 1 Then
                    dgBizDocs.Columns(8).Visible = False
                    dgBizDocs.Columns(17).Visible = False
                    dgBizDocs.Columns(18).Visible = False
                Else
                    dgBizDocs.Columns(7).Visible = False
                    dgBizDocs.Columns(17).Visible = True
                    dgBizDocs.Columns(18).Visible = True
                    divGrandTotal.Visible = True
                End If

                If CCommon.ToBool(hdnISRentalBizDoc.Value) = True Then
                    dgBizDocs.Columns(15).Visible = True
                    dgBizDocs.Columns(16).Visible = True
                Else
                    dgBizDocs.Columns(15).Visible = False
                    dgBizDocs.Columns(16).Visible = False
                End If

                '' Get Shipping Address
                'GetShippingAddress()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function GetBizDocItems() As String
            Try
                Dim dtBizDocItems As New DataTable
                Dim strOppBizDocItems As String

                dtBizDocItems.TableName = "BizDocItems"
                dtBizDocItems.Columns.Add("OppItemID")
                dtBizDocItems.Columns.Add("Quantity")
                dtBizDocItems.Columns.Add("Notes")
                dtBizDocItems.Columns.Add("monPrice")

                'dtBizDocItems.Columns.Add("TrackingNo")
                'dtBizDocItems.Columns.Add("vcShippingMethod")
                'dtBizDocItems.Columns.Add("monShipCost")
                'dtBizDocItems.Columns.Add("dtDeliveryDate")

                If CCommon.ToBool(hdnISRentalBizDoc.Value) = True Then
                    dtBizDocItems.Columns.Add("dtRentalStartDate")
                    dtBizDocItems.Columns.Add("dtRentalReturnDate")
                    dtBizDocItems.Columns.Add("monTotAmount")
                End If

                Dim dr As DataRow
                'Dim BizCalendar As UserControl
                Dim strDeliveryDate As String

                Dim CalRentalStartDate As UserControl
                Dim CalRentalReturnDate As UserControl

                Dim RentalStartDate, RentalReturnDate As DateTime

                For Each dgBizGridItem As DataGridItem In dgBizDocs.Items
                    If CType(dgBizGridItem.FindControl("chkInclude"), CheckBox).Checked = True Then
                        dr = dtBizDocItems.NewRow
                        dr("OppItemID") = dgBizGridItem.Cells(0).Text
                        dr("Notes") = CType(dgBizGridItem.FindControl("txtNotes"), TextBox).Text

                        If CCommon.ToLong(CType(dgBizGridItem.FindControl("txtRemainingPercent"), TextBox).Text) <> CCommon.ToLong(CType(dgBizGridItem.FindControl("hdnRemainingPercent"), HiddenField).Value) Then
                            If CCommon.ToInteger(CType(dgBizGridItem.FindControl("txtRemainingPercent"), TextBox).Text) > 0 Then
                                dr("Quantity") = CCommon.ToDouble(CType(dgBizGridItem.FindControl("txtRemainingPercent"), TextBox).Text) * CCommon.ToDouble(CType(dgBizGridItem.FindControl("hdnQtyOrdered"), HiddenField).Value) / 100
                            Else
                                Continue For
                            End If
                        Else
                            dr("Quantity") = CType(dgBizGridItem.FindControl("txtQuantityFulfilled"), TextBox).Text
                        End If

                        If CCommon.ToBool(hdnISRentalBizDoc.Value) = True Then
                            CalRentalStartDate = dgBizGridItem.FindControl("CalRentalStartDate")
                            Dim _myControlType1 As Type = CalRentalStartDate.GetType()
                            Dim _myUC_DueDate1 As PropertyInfo = _myControlType1.GetProperty("SelectedDate")
                            RentalStartDate = _myUC_DueDate1.GetValue(CalRentalStartDate, Nothing)

                            CalRentalReturnDate = dgBizGridItem.FindControl("CalRentalReturnDate")
                            Dim _myControlType2 As Type = CalRentalReturnDate.GetType()
                            Dim _myUC_DueDate2 As PropertyInfo = _myControlType2.GetProperty("SelectedDate")
                            RentalReturnDate = _myUC_DueDate2.GetValue(CalRentalReturnDate, Nothing)

                            dr("dtRentalStartDate") = RentalStartDate.ToString("yyyy-MM-dd HH:mm:ss.000")
                            dr("dtRentalReturnDate") = RentalReturnDate.ToString("yyyy-MM-dd HH:mm:ss.000")
                            Dim lngDayHour As Long

                            Dim objItems As New CItems

                            Dim dtUnit As DataTable

                            Dim objCommon As New CCommon

                            Dim decUOMConversionFactor As Decimal = 1
                            'objCommon.DomainID = Session("DomainID")
                            'objCommon.ItemCode = CType(dgBizGridItem.FindControl("hfnumItemCode"), HiddenField).Value
                            'objCommon.UnitId = CType(dgBizGridItem.FindControl("hfnumUOM"), HiddenField).Value
                            'dtUnit = objCommon.GetItemUOMConversion()
                            'decUOMConversionFactor = CCommon.ToDecimal(dtUnit(0)("UOMConversion"))

                            If CCommon.ToLong(CType(dgBizGridItem.FindControl("hfnumUOM"), HiddenField).Value) = CCommon.ToLong(Session("RentalHourlyUOM")) Then
                                lngDayHour = RentalReturnDate.Subtract(RentalStartDate).TotalHours
                                decUOMConversionFactor = IIf(Session("RentalPriceBasedOn") = 1, 1, 0.0416666666666667)
                            ElseIf CCommon.ToLong(CType(dgBizGridItem.FindControl("hfnumUOM"), HiddenField).Value) = CCommon.ToLong(Session("RentalDailyUOM")) Then
                                lngDayHour = RentalReturnDate.Subtract(RentalStartDate).TotalDays
                                decUOMConversionFactor = IIf(Session("RentalPriceBasedOn") = 1, 24, 1)
                            Else
                                lngDayHour = RentalReturnDate.Subtract(RentalStartDate).TotalDays
                                decUOMConversionFactor = IIf(Session("RentalPriceBasedOn") = 1, 24, 1)
                            End If

                            objItems.ItemCode = CType(dgBizGridItem.FindControl("hfnumItemCode"), HiddenField).Value
                            objItems.NoofUnits = lngDayHour * decUOMConversionFactor
                            objItems.OppId = lngOppID
                            objItems.DivisionID = ViewState("DivisionID")
                            objItems.DomainID = Session("DomainID")
                            objItems.byteMode = OppType

                            objItems.Amount = 0
                            objItems.WareHouseItemID = CCommon.ToDecimal(CType(dgBizGridItem.FindControl("hfnumWarehouseItmsID"), HiddenField).Value)

                            Dim ds As DataSet
                            Dim dtItemPricemgmt As DataTable

                            ds = objItems.GetPriceManagementList1
                            dtItemPricemgmt = ds.Tables(0)

                            If dtItemPricemgmt.Rows.Count = 0 Then
                                dtItemPricemgmt = ds.Tables(2)
                            End If

                            dtItemPricemgmt.Merge(ds.Tables(1))

                            If dtItemPricemgmt.Rows.Count > 0 Then
                                dr("monPrice") = CCommon.ToDecimal(dtItemPricemgmt.Rows(0)("ListPrice"))
                                dr("monTotAmount") = Decimal.Round(Math.Abs((dr("Quantity") * lngDayHour * decUOMConversionFactor) * dr("monPrice")), 2).ToString("f2")
                            End If
                        Else
                            'Removed txtUnitPrice as per Carl's suggestion on : 3rd Jun,2014
                            dr("monPrice") = CCommon.ToDecimal(CType(dgBizGridItem.FindControl("txtUnitPrice"), TextBox).Text)
                        End If

                        dtBizDocItems.Rows.Add(dr)
                    End If
                Next

                Dim dsNew As New DataSet
                dsNew.Tables.Add(dtBizDocItems)
                strOppBizDocItems = dsNew.GetXml
                dsNew.Tables.Remove(dsNew.Tables(0))
                Return strOppBizDocItems
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub dgBizDocs_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgBizDocs.ItemDataBound
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim item As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

                Dim txtQtyToFullFill As TextBox
                txtQtyToFullFill = e.Item.FindControl("txtQuantityFulfilled")
                txtQtyToFullFill.Attributes.Add("onkeyup", "return CheckQtyFulfilled('" & IIf(OppBizDocID > 0, CCommon.ToDecimal(txtQtyToFullFill.Text) + CCommon.ToDecimal(item("QtytoFulFill")), item("QtytoFulFill")) & "','" & txtQtyToFullFill.ClientID & "')")

                If (DataBinder.Eval(e.Item.DataItem, "bitSerialized") = True Or DataBinder.Eval(e.Item.DataItem, "bitLotNo") = True) Then
                    CType(e.Item.FindControl("txtQuantityFulfilled"), TextBox).ReadOnly = True
                ElseIf CCommon.ToLong(hdnDeferredBizDocID.Value) > 0 Then
                    CType(e.Item.FindControl("txtQuantityFulfilled"), TextBox).ReadOnly = True
                    CType(e.Item.FindControl("chkInclude"), CheckBox).Enabled = False
                Else
                    CType(e.Item.FindControl("txtQuantityFulfilled"), TextBox).ReadOnly = False
                End If

                grandTotal += CCommon.ToDouble(item("monOppBizDocAmount"))
            ElseIf e.Item.ItemType = ListItemType.Footer Then
                If OppType = 2 Then
                    lblGrandTotalRef.Text = "Grand Total: " & String.Format(CCommon.GetDataFormatString(), grandTotal)
                End If
            End If
        End Sub

        Private Sub EnableControls()
            Try
                If OppBizDocID > 0 Then
                    chkPartial.Enabled = False
                    dgBizDocs.Visible = False
                    ddlBizDocs.Enabled = False
                    btnShippingLabel.Visible = True
                    chkFromDate.Visible = False
                    chkFromDate.Checked = False
                Else
                    OppBizDocID = 0
                    chkPartial.Enabled = True
                    dgBizDocs.Visible = True
                    ddlBizDocs.Enabled = True
                    ddlBizDocs.ClearSelection()
                    ddlBizDocs.SelectedIndex = -1
                    'hplShippingReport.Text = ""
                    'txtShipAmount.Text = ""
                    ddlBizDocStatus.SelectedIndex = -1
                    'txtDiscount.Text = ""
                    'txtInterest.Text = "0"
                    txtBizDocSequenceId.Text = ""
                    btnShippingLabel.Visible = False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub btnAddOnly_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddOnly.Click
            Try

                Dim isExitSub As Boolean
                Save(isExitSub, True)

                If isExitSub Then
                    Exit Sub
                End If

                Response.Redirect("frmAddBizDocs.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=" + lngOppID.ToString + "&OppBizId=" + OppBizDocID.ToString + "&OppType=" + OppType.ToString, False)
            Catch ex As Exception
                If ex.Message = "FY_CLOSED" Then
                    litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Response.Write(ex)
                End If
            End Try
        End Sub
        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try

                Dim isExitSub As Boolean
                Save(isExitSub, True)

                If isExitSub Then
                    Exit Sub
                End If

                Response.Redirect("frmAddBizDocs.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=" + lngOppID.ToString + "&OppBizId=" + OppBizDocID.ToString + "&OppType=" + OppType.ToString, False)
            Catch ex As Exception
                If ex.Message = "FY_CLOSED" Then
                    litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Response.Write(ex)
                End If
            End Try
        End Sub

        Private Sub btnAllowBizDocSequenceId_Click(sender As Object, e As System.EventArgs) Handles btnAllowBizDocSequenceId.Click
            Try
                Dim isExitSub As Boolean
                Save(isExitSub, False)
                If isExitSub Then
                    Exit Sub
                End If
                Response.Redirect("frmAddBizDocs.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=" + lngOppID.ToString + "&OppBizId=" + OppBizDocID.ToString + "&OppType=" + OppType.ToString, False)
            Catch ex As Exception
                If ex.Message = "FY_CLOSED" Then
                    litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Response.Write(ex)
                End If
            End Try
        End Sub

        Private Sub btnDeleteRecurrence_Click(sender As Object, e As EventArgs) Handles btnDeleteRecurrence.Click
            Try
                Dim objBizRecurrence As New BizRecurrence
                objBizRecurrence.Delete(CCommon.ToLong(hdnRecConfigID.Value), CCommon.ToLong(Session("DomainID")), CCommon.ToLong(Session("UserContactID")))
                Page_Redirect()
            Catch ex As Exception
                If ex.Message = "RECURRENCE_TRANSACTIOS_EXISTS" Then
                    litMessage.Text = "You have to first disable recurrence and then have to delete all recurred bizdoc for deleting recurrence. Go to recurrence detail page and delete recurred bizdocs one by one."
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Response.Write(ex)
                End If
            End Try
        End Sub

        Private Sub btnUpdateRecurrence_Click(sender As Object, e As EventArgs) Handles btnUpdateRecurrence.Click
            Try
                If CCommon.ToLong(hdnRecConfigID.Value) > 0 AndAlso ddlRecurStatus.SelectedValue = "2" Then
                    Dim objBizRecurrence As New BizRecurrence
                    objBizRecurrence.SetDisable(CCommon.ToLong(hdnRecConfigID.Value))
                    Page_Redirect()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnUpdateBizDocFields_Click(sender As Object, e As EventArgs) Handles btnUpdateBizDocFields.Click
            Try
                Dim objOppBizDocs As New OppBizDocs
                objOppBizDocs.UserCntID = Session("UserContactID")
                objOppBizDocs.OppBizDocId = OppBizDocID
                objOppBizDocs.BizDocTemplateID = ddlBizDocTemplate.SelectedValue
                objOppBizDocs.FromDate = CDate(calFrom.SelectedDate & " 00:00:00").AddMinutes(Convert.ToInt32(Session("ClientMachineUTCTimeOffset")))
                objOppBizDocs.ShipCompany = radShipCompany.SelectedValue 'ddlShipCompany.SelectedValue
                objOppBizDocs.SequenceId = txtBizDocSequenceId.Text
                objOppBizDocs.TrackingNo = txtTrackingNo.Text
                objOppBizDocs.RefOrderNo = txtRefOrderNo.Text
                objOppBizDocs.BizDocStatus = ddlBizDocStatus.SelectedValue
                objOppBizDocs.UpdateBizDocEditFields()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Protected Sub ddlReleaseDates_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlReleaseDates.SelectedIndexChanged
            Try
                LoadOppBizDocItems()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class
End Namespace
