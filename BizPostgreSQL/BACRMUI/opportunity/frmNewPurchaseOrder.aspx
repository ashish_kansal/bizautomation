﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmNewPurchaseOrder.aspx.vb"
    Inherits=".frmNewPurchaseOrder" MasterPageFile="~/common/SalesOrder.Master" %>

<%@ Register Src="order.ascx" TagName="order" TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Purchase Order</title>
    <style type="text/css">
        .column {
            float: left;
        }

        html > body .RadComboBoxDropDown_Vista .rcbItem, html > body .RadComboBoxDropDown_Vista .rcbHovered, html > body .RadComboBoxDropDown_Vista .rcbDisabled, html > body .RadComboBoxDropDown_Vista .rcbLoading {
            display: inline-block;
        }
    </style>
    <script language="javascript" type="text/javascript">
        $(function () {
            $("#btnCustomerCollapseButton").click(function () {
                $("#CustomerSection").collapse('toggle');
            });
            $("#btnItemCollapseButton").click(function () {
                $("#ItemSection").collapse('toggle');
            });
        });

        function openItem(a, b, c, d, e) {
            if ($("[id$=trExistCust]") != null && $("[id$=trExistCust]").is(":visible")) {
                if ($find('radCmbCompany').get_value() == "") {
                    c = 0
                }
                else {
                    c = $find('radCmbCompany').get_value();
                }
            }
            else {
                c = 0;
            }


            if ($ControlFind(b).value == "") {
                b = 0
            }
            else {
                b = $ControlFind(b).value
            }


            var CalPrice = 0;
            if ($ControlFind('hdKit').value == 'True' && ($ControlFind('dgKitItem') != null)) {
                $('ctl00_Content_order1_dgKitItem tr').not(':first,:last').each(function () {
                    var str;
                    str = $(this).find("[id*='lblUnitPrice']").text();
                    CalPrice = CalPrice + (parseCurrency(str.split('/')[0]) * $(this).find("input[id*='txtKitUits']").val());
                });
            }

            var WareHouseItemID = 0;
            if ($find('radWareHouse') != null) {
                if ($find('radWareHouse').get_value() == "") {
                    WareHouseItemID = 0
                }
                else {
                    WareHouseItemID = $find('radWareHouse').get_value()
                }
            }
            if (a != 0 && b != 0) {
                window.open('../opportunity/frmItemPriceRecommd.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=' + a + '&Unit=' + b + '&DivID=' + c + '&OppType=' + e + '&CalPrice=' + CalPrice + '&WarehouseItemID=' + WareHouseItemID, '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
            }
        }

        function Save(a) {
            var errorMessage = "";

            var radcmbSTFrom = $find('radcmbSTFrom');
            var radcmbSTTo = $find('radcmbSTTo');
            var ddlFromContact = document.getElementById("ddlFromContact");
            var ddlToContact = document.getElementById("ddlToContact");

            if (radcmbSTFrom != null && radcmbSTFrom.get_value() == "") {
                errorMessage += "Select Storck Transfer From Field. \n";
            }

            if (radcmbSTTo != null && radcmbSTTo.get_value() == "") {
                errorMessage += "Select Storck Transfer To Field. \n";
            }

            if (ddlFromContact != null && ddlFromContact.value == 0) {
                errorMessage += "Select From Contact. \n";
            }

            if (ddlToContact != null && ddlToContact.value == 0) {
                errorMessage += "Select To Contact. \n";
            }

            if (errorMessage.length > 0) {
                alert(errorMessage);
                return false;
            }

            if ($("[id$=trExistCust]") != null && $("[id$=trExistCust]").is(":visible")) {
                if ($find('radCmbCompany').get_value() == "") {
                    alert("Select Vendor")
                    return false;
                }
                if (document.getElementById("ddlContact").value == 0) {
                    alert("Select Contact")
                    return false;
                }
            }
            else {
                if (document.getElementById("txtFirstName") != null && document.getElementById("txtFirstName").value == '') {
                    alert("Enter First Name")
                    return false;
                }
                if (document.getElementById("txtLastName") != null && document.getElementById("txtLastName").value == '') {
                    alert("Enter Last Name")
                    return false;
                }
                if (document.getElementById("ddlRelationship") != null && document.getElementById("ddlRelationship").value == 0) {
                    alert("Select Relationship ")
                    return false;
                }
            }
        }

        function parseCurrency(num) {
            return parseFloat(num.replace(/,/g, ''));
        }

        function CalculateTotal() {
            var Total, subTotal, TaxAmount, ShipAmt, IndTaxAmt, discount;
            var index, findex;
            Total = 0;
            TaxAmount = 0;
            subTotal = 0;
            IndTaxAmt = 0;

            ShipAmt = 0;
            $('#hdnTotalWeight').val();
            $('#ctl00_Content_order1_dgItems > tbody > tr').not(':first,:last').each(function () {
                if (parseFloat($(this).find("input[id*='txtUnits']").val()) <= 0) {
                    alert("Units must greater than 0.")
                    $(this).find("input[id*='txtUnits']").focus();
                    return false;
                }

                var unitPrice = parseCurrency($(this).find("input[id*='txtUnitPrice']").val());
                var uomConversionFactor = parseCurrency($(this).find("input[id*='txtUOMConversionFactor']").val()) || 1;
                var units = parseCurrency($(this).find("input[id*='txtUnits']").val());

                //WHEN Item Level UOM is Enabled we are displying total price in unitprice textbox
                //So for exammple, if 1 6Packs = 1 Can and order uom is 6 Packs then unit price is multiplied by 6.
                //to get unit price of single unit we have to divide by conversion factor
                if ($("#hdnEnabledItemLevelUOM").val() == "True") {
                    unitPrice = parseCurrency(unitPrice / uomConversionFactor);
                }

                $(this).find("input[id*='hdnUnitPrice']").val(unitPrice);
                subTotal = (units * uomConversionFactor) * unitPrice;

                $(this).find("[id*='lblTotal']").text(CommaFormatted(roundNumber(subTotal, 2)));

                Total = parseFloat(Total) + parseFloat(subTotal);
            });
            $('#ctl00_Content_order1_dgItems > tbody > tr:last').find("[id*='lblFTotal']").text(CommaFormatted(roundNumber((Total), 2)));
            $('#lblTotal').text(CommaFormatted(roundNumber((Total) + parseFloat(TaxAmount) + parseFloat(ShipAmt), 2)));
        }
        function OpenAddressWindow(sAddressType, isDropshipClicked) {
            var addressID = 0;
            var w = 800;
            var h = 600;
            var left = (screen.width / 2) - (w / 2);
            var top = (screen.height / 2) - (h / 2);
            if (sAddressType == 'ShipTo') {
                addressID = parseInt($("[id$=hdnShipAddressID]").val());
            } else if (sAddressType == 'BillTo') {
                addressID = parseInt($("[id$=hdnBillAddressID]").val());
            }

            window.open('frmChangeDefaultAdd.aspx?AddressType=' + sAddressType + '&CompanyId=<%= Convert.ToInt64(Session("UserDivisionID"))%>&CompanyName=<%= Convert.ToString(Session("CompanyName"))%>' + '&AddressID=' + addressID + '&IsDropshipClicked=' + isDropshipClicked, '', 'toolbar=no,titlebar=no,width=' + w + ',height=' + h + ',top=' + top + ',left=' + left + ',scrollbars=yes,resizable=yes');
            return false;
        }
        function FillModifiedAddress(AddressType, AddressID, FullAddress, warehouseID, isDropshipClicked) {
            if (AddressType == "BillTo") {
                $('#hdnBillAddressID').val(AddressID);
            } else {
                $('#hdnShipAddressID').val(AddressID);
            }

            if (isDropshipClicked == "1") {
                $("[id$=hdnIsDropshipClicked]").val("1")
            } else {
                if (warehouseID != -1 && $("[id$=hdnShipToWarehouseID]").val() != warehouseID) {
                    $("[id$=hdnClearItems").val("1");
                } else {
                    $("[id$=hdnClearItems").val("0");
                }
            }

            document.getElementById("btnTemp").click()
            return false;
        }
        function GetSelectedItem(combobox) {
            combobox.ClientDataString = document.getElementById("txtDivID").value + '~' + 2;
        }
        function Add(a) {

            var IsAddCalidate = Save();

            if (IsAddCalidate == false) {
                return false;
            }

            if ($find('radWareHouse') != null) {
                if ($find('radWareHouse').get_value() == "") {
                    alert("Select Warehouse")
                    $find('radWareHouse').get_value();
                    return false;
                }
            }
            if (document.getElementById("txtUnits").value == "") {
                alert("Enter Units")
                document.getElementById("txtUnits").focus();
                return false;
            }
            if (parseFloat(document.getElementById("txtUnits").value) <= 0) {
                alert("Units must greater than 0.")
                document.getElementById("txtUnits").focus();
                return false;
            }
            if (document.getElementById("txtprice").value == "") {
                alert("Enter Price")
                document.getElementById("txtprice").focus();
                return false;
            }

            //Check for Duplicate Item
            var table = $ControlFind('ctl00_Content_order1_dgItems');
            var WareHouesID;
            //            alert($('radWareHouse'));
            if ($ControlFind('radWareHouse') == null) {
                WareHouesID = '';
            }
            else {
                WareHouesID = $find('radWareHouse').get_value();
            }

            var dropship;
            if ($ControlFind('chkDropShip').checked == true) {
                dropship = 'True';
                WareHouesID = '';
            }
            else {
                dropship = 'False';
            }
        }
        function OpenOpp(a) {

            window.opener.opener.location = '../opportunity/frmOpportunities.aspx?OpID=' + a
            window.opener.window.close();
            Close();
        }
        function OpenOpp1(a) {
            window.opener.location = '../opportunity/frmOpportunities.aspx?OpID=' + a
            Close();
        }
        function SetWareHouseId(id) {
            var combo = $find('radWareHouse')
            var node = combo.findItemByValue(id);
            node.select();
        }
        function OpenAdd() {
            if ($find('radCmbCompany').get_value() == "") {
                alert("Select Vendor")
                return false;
            }
            window.open("../prospects/frmProspectsAdd.aspx?pwer=dfsdfdsfdsfdsf&rtyWR=" + $find('radCmbCompany').get_value() + "&Reload=1", '', 'toolbar=no,titlebar=no,top=300,width=700,height=300,scrollbars=no,resizable=no')
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanelException" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Label ID="lblException" runat="server" Style="color: red;"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label Text="Purchase Order" runat="server" ID="lblTitle" />
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="100%" runat="server" id="tblMultipleOrderMessage" visible="false" style="background-color: #FFFFFF; color: red">
        <tr>
            <td class="normal4" align="center">You can not create multiple orders simultaneously,
                <asp:LinkButton Text="Click here" runat="server" ID="lnkClick" />
                to clear other order and proceed.
            </td>
        </tr>
    </table>

    <div class="container customer-detail">
        <div class="grid grid-pad">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="panel-group">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" id="btnCustomerCollapseButton" href="#CustomerSection">Vendor Section</a>
                                </h4>
                            </div>
                            <div id="CustomerSection" class="accordion-body collapse in">
                                <div class="panel-body">
                                    <asp:Panel ID="pnlPurchaseOpp" runat="server">
                                        <div class="cont1">
                                            <div class="col-1-1" style="float: left">
                                                <img src="../images/Greenman-32.gif" />
                                            </div>
                                            <div class="col-1-1" id="trExistCust" runat="server">
                                                <div class="col-1-1">
                                                    <div class="col-1-2">
                                                        <div class="content">
                                                            <div class="form-group">
                                                                <label class="col-3-12">
                                                                    <div>
                                                                        <div style="float: right">Vendor & Contact<span class="required">*</span></div>
                                                                        <div style="float: right">
                                                                            <asp:LinkButton ID="lkbNewCustomer" ToolTip="New Customer" runat="server">
                                                                                <asp:Image ID="Image1" ImageUrl="~/images/AddRecord.png" runat="server" Style="margin-right: 2px" />
                                                                            </asp:LinkButton>

                                                                        </div>
                                                                    </div>
                                                                </label>
                                                                <div class="col-9-12">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td style="width: 50%">
                                                                                <telerik:RadComboBox AccessKey="V" Width="100%" ID="radCmbCompany" DropDownWidth="500px"
                                                                                    OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                                                                    ClientIDMode="Static"
                                                                                    ShowMoreResultsBox="true"
                                                                                    Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True"
                                                                                    TabIndex="4">
                                                                                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                                                                </telerik:RadComboBox>

                                                                            </td>
                                                                            <td style="width: 50%">
                                                                                <asp:DropDownList ID="ddlContact" Width="100%" runat="server" TabIndex="5">
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-3-12">
                                                                    <a href="#" onclick="return OpenAdd()" class="hyperlink" tabindex="8">
                                                                        <img src="../images/edit.png" runat="server" alt="AddEdit" /></a>
                                                                    <asp:LinkButton ID="lnkBillTo" runat="server" Text="Bill to:" class="hyperlink" TabIndex="8"></asp:LinkButton>
                                                                </label>
                                                                <div class="col-9-12" style="padding-top: 9px;">
                                                                    <asp:Label ID="lblBillTo1" runat="server" Text=""></asp:Label>
                                                                    <asp:Label ID="lblBillTo2" runat="server" Text=""></asp:Label>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-3-12">Assign To</label>
                                                                <div class="col-9-12">
                                                                    <asp:DropDownList ID="ddlAssignTo" runat="server" TabIndex="6">
                                                                        <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-1-2">
                                                        <div class="content">
                                                            <div class="form-group">
                                                                <label class="col-3-12">Ship Via</label>
                                                                <div class="col-9-12" style="margin: 5px 0;">
                                                                    <table width="100%">
                                                                        <tr>
                                                                            <td style="width: 50%">
                                                                                <telerik:RadComboBox ID="rdcmbShipVia" AutoPostBack="true" OnSelectedIndexChanged="rdcmbShipVia_SelectedIndexChanged" DropDownWidth="180px" Width="100%" runat="server"></telerik:RadComboBox>
                                                                            </td>
                                                                            <td style="width: 50%">
                                                                                <telerik:RadComboBox ID="rdcmbShippingService" DropDownWidth="250px" Width="100%" runat="server"></telerik:RadComboBox>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-3-12">
                                                                    <a href="#" onclick="return OpenAdd()" class="hyperlink" tabindex="11">
                                                                        <img src="../images/edit.png" runat="server" alt="AddEdit" /></a>
                                                                    <asp:LinkButton ID="lnkShipTo" runat="server" Text="Ship to:" class="hyperlink" TabIndex="10"></asp:LinkButton>
                                                                </label>
                                                                <div class="col-9-12" style="padding-top: 9px;">
                                                                    <div style="display:flex">
                                                                        <div class="pull-left" style="width:100%">
                                                                            <asp:Label ID="lblShipTo1" runat="server" Text=""></asp:Label>
                                                                            <asp:Label ID="lblShipTo2" runat="server" Text=""></asp:Label>
                                                                            <asp:HiddenField ID="hdnShipToWarehouseID" runat="server" />
                                                                            <asp:HiddenField ID="hdnClearItems" runat="server" Value="0" />
                                                                            <asp:HiddenField ID="hdnIsDropshipClicked" runat="server" Value="0" />
                                                                        </div>
                                                                        <div class="pull-left" style="width:100px">
                                                                            <asp:CheckBox ID="chkDropshipPO" runat="server" OnCheckedChanged="chkDropshipPO_CheckedChanged" AutoPostBack="true" Text="Drop Ship" Font-Bold="true" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-3-12">Vendor’s Ship from</label>
                                                                <div class="col-9-12">
                                                                    <asp:DropDownList ID="ddlVendorAddresses" runat="server" TabIndex="5" AutoPostBack="true">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-1-1" id="pnlPurchase" runat="server" visible="false">
                                                    <div class="col-1-2" style="float: left"></div>
                                                    <div class="col-1-2" style="float: right">
                                                        <div class="content">
                                                            <div class="form-group">
                                                                <label class="col-3-12"></label>
                                                                <div class="col-9-12">
                                                                    <asp:Button ID="btnSelectVendor" CssClass="button" runat="server" Text="Select Vendor" TabIndex="12" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-1-1" id="trNewCust" runat="server" visible="false">
                                                <div class="col-1-1">
                                                    <div class="col-1-2">
                                                        <div class="content">
                                                            <div class="form-group">
                                                                <label class="col-3-12">
                                                                    <div style="float: right">First Name<span class="required">*</span></div>
                                                                    <div style="float: right">
                                                                        <asp:LinkButton ID="lkbExistingCustomer" ToolTip="Existing Customer" runat="server">
                                                                            <asp:Image ID="Image2" ImageUrl="~/images/AddRecord.png" runat="server" Style="margin-right: 2px" />
                                                                        </asp:LinkButton>
                                                                    </div>
                                                                </label>
                                                                <div class="col-9-12">
                                                                    <asp:TextBox ID="txtFirstName" runat="server" MaxLength="50" TabIndex="13"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="col-3-12">Phone</label>
                                                                <div class="col-9-12">
                                                                    <div style="float: left">
                                                                        <asp:TextBox ID="txtPhone" runat="server" MaxLength="50" TabIndex="15"></asp:TextBox>
                                                                    </div>
                                                                    <div style="float: left; margin-left: 10px;">
                                                                        <div class="form-group">
                                                                            <label class="col-3-12">Ext</label>
                                                                        </div>
                                                                    </div>
                                                                    <div style="float: left;">
                                                                        <asp:TextBox ID="txtExt" runat="server" Width="30" MaxLength="10" TabIndex="16"></asp:TextBox>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="Organization" class="col-3-12">Organization</label>
                                                                <div class="col-9-12">
                                                                    <asp:TextBox ID="txtCompany" runat="server" MaxLength="50" TabIndex="18"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="Relationship" class="col-3-12">Relationship<span class="required">*</span></label>
                                                                <div class="col-9-12">
                                                                    <asp:DropDownList ID="ddlRelationship" runat="server" AutoPostBack="true" TabIndex="20">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-1-2">
                                                        <div class="content">
                                                            <div class="form-group">
                                                                <label class="col-3-12">Last Name<span class="required">*</span></label>
                                                                <div class="col-9-12">
                                                                    <asp:TextBox ID="txtLastName" runat="server" MaxLength="50" TabIndex="14"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="Email" class="col-3-12">Email</label>
                                                                <div class="col-9-12">
                                                                    <asp:TextBox ID="txtEmail" runat="server" TabIndex="17"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="AssignedTo" class="col-3-12">Assign To</label>
                                                                <div class="col-9-12">
                                                                    <asp:DropDownList ID="ddlNewAssignTo" runat="server" TabIndex="19">
                                                                        <asp:ListItem Text="--Select One--" Value="0"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="Profile" class="col-3-12">Profile</label>
                                                                <div class="col-9-12">
                                                                    <asp:DropDownList ID="ddlProfile" runat="server" TabIndex="21">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-1-1">
                                                    <strong>Billing Address</strong>
                                                </div>
                                                <div class="col-1-1">
                                                    <div class="col-1-2">
                                                        <div class="content">
                                                            <div class="form-group">
                                                                <label for="BillStreet" class="col-3-12">Street</label>
                                                                <div class="col-9-12">
                                                                    <asp:TextBox ID="txtBillStreet" runat="server" TextMode="MultiLine" TabIndex="22"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="BillState" class="col-3-12">State</label>
                                                                <div class="col-9-12">
                                                                    <asp:DropDownList ID="ddlBillState" runat="server" TabIndex="24">
                                                                        <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="BillPostal" class="col-3-12">Postal</label>
                                                                <div class="col-9-12">
                                                                    <asp:TextBox ID="txtBillPostal" runat="server" TabIndex="26"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-1-2">
                                                        <div class="content">
                                                            <div class="form-group">
                                                                <label for="BillCity" class="col-3-12">City</label>
                                                                <div class="col-9-12">
                                                                    <asp:TextBox ID="txtBillCity" runat="server" TabIndex="23"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="BillCountry" class="col-3-12">Country</label>
                                                                <div class="col-9-12">
                                                                    <asp:DropDownList ID="ddlBillCountry" TabIndex="25" AutoPostBack="True" runat="server">
                                                                        <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-1-1">
                                                    <asp:CheckBox AutoPostBack="true" ID="chkShipDifferent" Text="Click here if Shipping Address is different than Billing Address" runat="server" TabIndex="27" />
                                                </div>
                                                <div id="pnlShipping" runat="server" visible="false" style="margin-top: 5px;" class="col-1-1">
                                                    <div class="col-1-1"><strong>Shipping Address</strong></div>
                                                    <div class="col-1-2">
                                                        <div class="content">
                                                            <div class="form-group">
                                                                <label for="ShipStreet" class="col-3-12">Street</label>
                                                                <div class="col-9-12">
                                                                    <asp:TextBox ID="txtShipStreet" runat="server" TextMode="MultiLine" TabIndex="28"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="ShipState" class="col-3-12">State</label>
                                                                <div class="col-9-12">
                                                                    <asp:DropDownList ID="ddlShipState" runat="server" TabIndex="30">
                                                                        <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="ShipPostal" class="col-3-12">Postal</label>
                                                                <div class="col-9-12">
                                                                    <asp:TextBox ID="txtShipPostal" runat="server" TabIndex="32"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-1-2">
                                                        <div class="content">
                                                            <div class="form-group">
                                                                <label for="ShipCity" class="col-3-12">City</label>
                                                                <div class="col-9-12">
                                                                    <asp:TextBox ID="txtShipCity" runat="server" TabIndex="29"></asp:TextBox>
                                                                </div>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="ShipCountry" class="col-3-12">Country</label>
                                                                <div class="col-9-12">
                                                                    <asp:DropDownList ID="ddlShipCountry" TabIndex="31" AutoPostBack="True" runat="server">
                                                                        <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-1-1">
                                                <div class="col-1-2">
                                                    <div class="content">
                                                        <div class="form-group" id="pnlCurrency" runat="server" visible="false">
                                                            <label class="col-3-12">Currency</label>
                                                            <div class="col-9-12">
                                                                <asp:DropDownList ID="ddlCurrency" runat="server" TabIndex="4"></asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group" runat="server" id="divClass" visible="false">
                                                            <label class="col-3-12">Class</label>
                                                            <div class="col-9-12">
                                                                <asp:DropDownList ID="ddlClass" runat="server" TabIndex="3"></asp:DropDownList>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label class="col-3-12">Item Template</label>
                                                            <div class="col-9-12">
                                                                <asp:DropDownList ID="ddlPurchaseTemplate" runat="server" AutoPostBack="true" TabIndex="7"></asp:DropDownList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-1-2">
                                                    <div class="content">
                                                        <div class="form-group">
                                                            <label class="col-3-12">Comments</label>
                                                            <div class="col-9-12">
                                                                <asp:TextBox ID="txtComments" runat="server" TextMode="MultiLine" Rows="3" CssClass="form-control"></asp:TextBox>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </asp:Panel>
                                    <div class="cont2">
                                        <div class="col-1-2"></div>
                                        <div class="col-1-2 total-cont" style="float: right" id="tdBanlce" runat="server" visible="false">
                                            <div><span>Purchase Credit:</span><asp:Label ID="lblCreditBalance" runat="server"></asp:Label></div>
                                            <div><span>Credit Limit (A):</span><asp:Label ID="lblCreditLimit" runat="server"></asp:Label></div>
                                            <div style="float: left"><span>Balance Due (B):</span><asp:Label ID="lblBalanceDue" runat="server"></asp:Label></div>
                                            <div style="float: left"><span>Remaining Credit (A - B):</span><asp:Label ID="lblRemaningCredit" runat="server"></asp:Label></div>
                                            <div style="float: left"><span>Amount Past Due:</span><asp:Label ID="lblTotalAmtPastDue" runat="server"></asp:Label></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:Button ID="btnTemp" runat="server" Style="display: none"></asp:Button>
                    <asp:HiddenField ID="hdnVendorID" runat="server" />
                    <asp:HiddenField ID="hdnVendorCost" runat="server" />
                    <asp:Button ID="btnSelectedVendor" runat="server" Text="" Style="display: none" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="radCmbCompany" />
                    <asp:AsyncPostBackTrigger ControlID="chkShipDifferent" />
                    <asp:AsyncPostBackTrigger ControlID="ddlRelationship" />
                    <asp:AsyncPostBackTrigger ControlID="ddlProfile" />
                    <asp:AsyncPostBackTrigger ControlID="ddlPurchaseTemplate" />
                    <asp:AsyncPostBackTrigger ControlID="ddlContact" />
                    <asp:AsyncPostBackTrigger ControlID="ddlNewAssignTo" />
                    <asp:AsyncPostBackTrigger ControlID="ddlCurrency" />
                    <asp:AsyncPostBackTrigger ControlID="ddlAssignTo" />
                    <asp:AsyncPostBackTrigger ControlID="btnSelectVendor" />
                    <asp:AsyncPostBackTrigger ControlID="btnTemp" />
                    <asp:AsyncPostBackTrigger ControlID="btnSelectedVendor" />
                    <asp:AsyncPostBackTrigger ControlID="lkbNewCustomer" />
                    <asp:AsyncPostBackTrigger ControlID="lkbExistingCustomer" />
                    <asp:AsyncPostBackTrigger ControlID="chkDropshipPO" />
                </Triggers>
            </asp:UpdatePanel>
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" id="btnItemCollapseButton" href="#ItemSection">Item Section</a>
                        </h4>
                    </div>
                    <div id="ItemSection" class="accordion-body collapse in">
                        <div class="panel-body">
                            <uc1:order ID="order1" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanelHidden" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnCmbDivisionID" runat="server" />
            <asp:HiddenField ID="hdnCmbOppType" runat="server" />
            <table width="100%" style="background-color: #FFFFFF;">
                <tr>
                    <td class="normal4" align="center">
                        <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
