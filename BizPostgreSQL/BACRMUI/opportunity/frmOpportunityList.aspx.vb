'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Admin
Imports Telerik.Web.UI
Imports System.Web.Services

Namespace BACRM.UserInterface.Opportunities
    Public Class frmOpportunityList
        Inherits BACRMPage

        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim strColumn As String
        Dim m_aryRightsForPage1(), m_aryRightsForViewGridConfiguration() As Integer
        Dim m_aryRightsForPage2() As Integer
        Dim RegularSearch As String
        Dim CustomSearch As String

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try

                Dim m_aryRightsForSendAnnounceMent() As Integer = GetUserRightsForPage_Other(43, 140)

                If m_aryRightsForSendAnnounceMent(RIGHTSTYPE.VIEW) <> 0 Then
                    btnSendAnnounceMent.Visible = True
                End If
                Dim m_aryRightsForAddActionItem() As Integer = GetUserRightsForPage_Other(44, 146)

                If m_aryRightsForAddActionItem(RIGHTSTYPE.VIEW) <> 0 Then
                    btnAddActionItem.Visible = True
                End If
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                'CLEAR ALERT MESSAGE
                divMessage.Style.Add("display", "none")
                litMessage.Text = ""

                If GetQueryStringVal("SI") <> "" Then
                    txtSelIndex.Text = GetQueryStringVal("SI")
                End If
                If GetQueryStringVal("SI1") <> "" Then
                    SI1 = GetQueryStringVal("SI1")
                Else : SI1 = 0
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    SI2 = GetQueryStringVal("SI2")
                Else : SI2 = 0
                End If
                If GetQueryStringVal("frm") <> "" Then
                    frm = ""
                    frm = GetQueryStringVal("frm")
                Else : frm = ""
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = ""
                    frm1 = GetQueryStringVal("frm1")
                Else : frm1 = ""
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    frm2 = ""
                    frm2 = GetQueryStringVal("frm2")
                Else : frm2 = ""
                End If

                txtFormID.Text = IIf(txtSelIndex.Text = 0, 38, 40)

                If txtSelIndex.Text = 0 Then
                    GetUserRightsForPage(10, 2)
                Else
                    GetUserRightsForPage(10, 8)
                End If

                If Not IsPostBack Then
                    If txtSelIndex.Text = 0 Then
                        lblOpportunity.Text = "Sales Opportunity"
                        litTitle.Text = "Sales Opportunity"
                        btnAddNewOpportunity.Attributes.Add("onclick", "return OpenPopUp('../opportunity/frmNewOrder.aspx?OppStatus=0&OppType=1');")
                        aHelp.Attributes.Add("onclick", "return OpenHelpPopUp('opportunity/frmopportunitylist.aspx?opptype=1')")
                    Else
                        lblOpportunity.Text = "Purchase Opportunity"
                        litTitle.Text = "Purchase Opportunity"
                        btnAddNewOpportunity.Attributes.Add("onclick", "return OpenPopUp('../opportunity/frmAddOpportunity.aspx?OppType=2');")
                        aHelp.Attributes.Add("onclick", "return OpenHelpPopUp('opportunity/frmopportunitylist.aspx?opptype=2')")
                    End If
                    Session("Data") = Nothing
                    Session("SalesProcessDetails") = Nothing
                    Session("Help") = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "Opportunity"

                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                        txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                        txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                        txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))

                        If txtSelIndex.Text = 0 Then
                            ddlSortSales.ClearSelection()
                            ddlSortSales.Items.FindByValue(CCommon.ToString(PersistTable(ddlSortSales.ID))).Selected = True
                        Else
                            ddlSortPurchase.ClearSelection()
                            ddlSortPurchase.Items.FindByValue(CCommon.ToString(PersistTable(ddlSortPurchase.ID))).Selected = True
                        End If
                        txtGridColumnFilter.Text = CCommon.ToString(PersistTable(PersistKey.GridColumnSearch))

                        Dim strorderstatus As String = CCommon.ToString(PersistTable(PersistKey.OrderStatus))

                        If Not CCommon.ToString(strorderstatus) = "" Then
                            If strorderstatus = "0" Then
                                radOpen.Checked = True
                            ElseIf strorderstatus = "2" Then
                                radLost.Checked = True
                            Else
                                radOpen.Checked = True
                            End If
                        Else
                            radOpen.Checked = True
                        End If
                    End If
                    If CCommon.ToInteger(txtCurrrentPage.Text) = 0 Then
                        txtCurrrentPage.Text = "1"
                    End If

                    'Check if user has rights to edit grid configuration
                    m_aryRightsForViewGridConfiguration = GetUserRightsForPage_Other(10, 33)
                    If m_aryRightsForViewGridConfiguration(RIGHTSTYPE.VIEW) = 0 Then
                        Dim tdGridConfiguration As HtmlAnchor = CType(CCommon.FindControlRecursive(Page.Master, "tdGridConfiguration"), HtmlAnchor)

                        If Not tdGridConfiguration Is Nothing Then
                            tdGridConfiguration.Visible = False
                        End If
                    End If

                    Session("List") = "Opp"

                    'KEEP IT AT LAST ABOVE BindDatagrid
                    If CCommon.ToShort(GetQueryStringVal("IsFormDashboard")) = 1 Then
                        ddlSortSales.SelectedValue = "3"
                        ddlSortPurchase.SelectedValue = "3"
                        txtGridColumnFilter.Text = ""
                    End If
                    BindDatagrid()
                Else
                    BindDatagrid()
                End If

                If txtSelIndex.Text = 0 Then
                    pnlSales.Visible = True
                    pnlPurchase.Visible = False
                ElseIf txtSelIndex.Text = 1 Then
                    pnlSales.Visible = False
                    pnlPurchase.Visible = True
                End If

                InitializeSearchClientSideTemplate()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)

            End Try
        End Sub

        Private Function GetCheckedValues() As Boolean
            Try
                Session("ContIDs") = ""
                Dim isCheckboxchecked As Boolean = False
                Dim gvRow As GridViewRow
                For Each gvRow In gvSearch.Rows
                    If CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = True Then
                        Session("ContIDs") = Session("ContIDs") & CType(gvRow.FindControl("lblContactId"), Label).Text & ","
                        isCheckboxchecked = True
                    End If
                Next
                Session("ContIDs") = Session("ContIDs").ToString.TrimEnd(",")
                Return isCheckboxchecked
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Private Sub btnAddActionItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddActionItem.Click
            Try
                Dim isCheckboxChecked As Boolean
                isCheckboxChecked = GetCheckedValues()
                If isCheckboxChecked Then
                    Response.Redirect("../Admin/ActionItemDetailsOld.aspx?selectedContactId=1", False)
                Else
                    DisplayError("Select atleast one checkbox For Action Item.")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Private Sub btnSendAnnounceMent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendAnnounceMent.Click
            Try
                Dim isCheckboxChecked As Boolean
                isCheckboxChecked = GetCheckedValues()
                If isCheckboxChecked Then
                    Session("EMailCampCondition") = Session("WhereCondition")
                    Response.Redirect("../Marketing/frmEmailBroadCasting.aspx?PC=true&SearchID=0&ID=0&SAll=true", False)
                Else
                    DisplayError("Select atleast one checkbox to broadcast E-mail.")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub BindDatagrid(Optional ByVal CreateCol As Boolean = True)
            Try
                Dim dtOpportunity As DataTable
                Dim objOpportunity As New COpportunities
                objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                With objOpportunity
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")

                    If radOpen.Checked Then
                        .OppStatus = 0
                    ElseIf radLost.Checked Then
                        .OppStatus = 2
                    Else
                        .OppStatus = 0
                    End If

                    If txtSelIndex.Text = 0 Then
                        .SortOrder = ddlSortSales.SelectedItem.Value
                        .intType = 1
                    ElseIf txtSelIndex.Text = 1 Then
                        .SortOrder = ddlSortPurchase.SelectedItem.Value
                        .intType = 2
                    End If
                    .endDate = Now()
                    .SortCharacter = txtSortChar.Text.Trim()

                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()

                    If txtSelIndex.Text = 0 Then
                        .OppType = 1
                    ElseIf txtSelIndex.Text = 1 Then
                        .OppType = 2
                    End If

                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0

                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If

                    GridColumnSearchCriteria()

                    .RegularSearchCriteria = RegularSearch.Replace("numPartenerSource", "numPartner")
                    .CustomSearchCriteria = CustomSearch
                    .SearchText = radCmbSearch.Text.Replace("%", "[%]")
                    .DashboardReminderType = If(CCommon.ToString(GetQueryStringVal("IsFormDashboard")) = "1", CCommon.ToInteger(GetQueryStringVal("ReminderType")), 0)
                End With

                If txtSortColumn.Text <> "" Then
                    objOpportunity.columnName = txtSortColumn.Text
                Else : objOpportunity.columnName = "opp.bintCreatedDate"
                End If
                objOpportunity.UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)

                Dim dsList As DataSet

                dsList = objOpportunity.GetOpportunityList
                dtOpportunity = dsList.Tables(0)

                Dim dtTableInfo As DataTable
                dtTableInfo = dsList.Tables(1)

                Dim strorderstatus As String
                If radOpen.Checked Then
                    strorderstatus = "0"
                ElseIf radLost.Checked Then
                    strorderstatus = "2"
                Else
                    strorderstatus = "0"
                End If
                'Persist Form Settings
                PersistTable.Clear()
                PersistTable.Add(PersistKey.CurrentPage, IIf(dtOpportunity.Rows.Count > 0, txtCurrrentPage.Text, "1"))
                PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())
                PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
                PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text.Trim())
                PersistTable.Add(ddlSortSales.ID, ddlSortSales.SelectedValue)
                PersistTable.Add(ddlSortPurchase.ID, ddlSortPurchase.SelectedValue)
                PersistTable.Add(PersistKey.GridColumnSearch, txtGridColumnFilter.Text)
                PersistTable.Add(PersistKey.OrderStatus, strorderstatus)
                PersistTable.Save()

                If objOpportunity.TotalRecords = 0 Then
                    txtTotalRecordsSales.Text = 0
                Else
                    Dim strTotalPage As String()
                    Dim decTotalPage As Decimal
                    decTotalPage = objOpportunity.TotalRecords / Session("PagingRows")
                    decTotalPage = Math.Round(decTotalPage, 2)
                    strTotalPage = CStr(decTotalPage).Split(".")
                    If (objOpportunity.TotalRecords Mod Session("PagingRows")) = 0 Then
                        txtTotalPageSales.Text = strTotalPage(0)
                    Else
                        txtTotalPageSales.Text = strTotalPage(0) + 1
                    End If
                    txtTotalRecordsSales.Text = objOpportunity.TotalRecords
                End If

                Dim m_aryRightsForInlineEdit() As Integer

                If objOpportunity.OppType = 1 Then
                    m_aryRightsForInlineEdit = GetUserRightsForPage_Other(10, 3)
                Else
                    m_aryRightsForInlineEdit = GetUserRightsForPage_Other(10, 9)
                End If

                Dim i As Integer

                For i = 0 To dtOpportunity.Columns.Count - 1
                    dtOpportunity.Columns(i).ColumnName = dtOpportunity.Columns(i).ColumnName.Replace(".", "")
                Next

                Dim htGridColumnSearch As New Hashtable

                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")
                    Dim strIDValue() As String

                    For i = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")

                        htGridColumnSearch.Add(strIDValue(0), strIDValue(1))
                    Next
                End If

                If CreateCol = True Then
                    Dim bField As BoundField
                    Dim Tfield As TemplateField
                    gvSearch.Columns.Clear()

                    For Each drRow As DataRow In dtTableInfo.Rows
                        Tfield = New TemplateField

                        Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, drRow, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, IIf(txtSelIndex.Text = 0, 38, 40), objOpportunity.columnName, objOpportunity.columnSortOrder)

                        Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, drRow, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, IIf(txtSelIndex.Text = 0, 38, 40), objOpportunity.columnName, objOpportunity.columnSortOrder)
                        gvSearch.Columns.Add(Tfield)
                    Next

                    Dim dr As DataRow
                    dr = dtTableInfo.NewRow()
                    dr("vcAssociatedControlType") = "DeleteCheckBox"
                    dr("intColumnWidth") = "30"

                    Tfield = New TemplateField
                    Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, dr, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, IIf(txtSelIndex.Text = 0, 38, 40))
                    Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, dr, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, IIf(txtSelIndex.Text = 0, 38, 40))
                    gvSearch.Columns.Add(Tfield)
                End If

                bizPager.PageSize = Session("PagingRows")
                bizPager.RecordCount = objOpportunity.TotalRecords
                bizPager.CurrentPageIndex = txtCurrrentPage.Text

                gvSearch.DataSource = dtOpportunity
                gvSearch.DataBind()

                If m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE) <> 0 Then
                    Dim objPageControls As New PageControls
                    Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtTableInfo)
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "InlineEditValidation", strValidation, True)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        <WebMethod(EnableSession:=True)>
        Public Shared Function FillPendingApproval(ByVal OppId As Long) As String
            Try
                Dim objOpp As New COpportunities
                Dim dt As DataTable
                objOpp.DomainID = OppId
                objOpp.OpportID = OppId
                dt = objOpp.ViewApproverItemUnitPrice(OppId, OppId)
                If dt.Rows.Count > 0 Then
                    Return (Convert.ToString(dt.Rows(0)(0)))
                Else
                    Return ""
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        <WebMethod(EnableSession:=True)>
        Public Shared Function FillApproverPromotionApproval(ByVal OppId As Long) As String
            Try
                Dim objOpp As New COpportunities
                Dim dt As DataTable
                objOpp.DomainID = OppId
                objOpp.OpportID = OppId
                dt = objOpp.ViewApproverPromotionApproval(OppId, OppId)
                If dt.Rows.Count > 0 Then
                    Return (Convert.ToString(dt.Rows(0)(0)))
                Else
                    Return ""
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Private Sub dtgAccount_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim CompanyId, DivisionID, ContactID, CRMTYPE As Integer
                    Dim btnDelete As Button
                    Dim lnkDelete As LinkButton
                    lnkDelete = e.Item.FindControl("lnkDelete")
                    btnDelete = e.Item.FindControl("btnDelete")
                    ContactID = CLng(e.Item.Cells(0).Text)
                    CompanyId = CLng(e.Item.Cells(1).Text)
                    DivisionID = CLng(e.Item.Cells(2).Text)
                    If CompanyId = 1 Then
                        btnDelete.Visible = False
                        lnkDelete.Visible = True
                    Else : btnDelete.Attributes.Add("onclick", "return DeleteAccount()")
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function ReturnName(ByVal SDate) As String
            Try
                Dim strDate As String = ""
                If Not IsDBNull(SDate) Then
                    strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                    If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                        strDate = "<font color=red>" & strDate & "</font>"
                    ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                        strDate = "<font color=orange>" & strDate & "</font>"
                    End If
                End If
                Return strDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ReturnDateTime(ByVal CloseDate) As String
            Try
                Dim strTargetResolveDate As String = ""
                If Not IsDBNull(CloseDate) Then strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
                Return strTargetResolveDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ReturnType(ByVal Type)
            Try
                Dim strType As String
                If Type = 1 Then
                    strType = "Sales"
                ElseIf Type = 2 Then
                    strType = "Purchase"
                ElseIf Type = 3 Then
                    strType = "Sales Deals from Portal"
                End If
                Return strType
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ReturnMoney(ByVal Money)
            Try
                If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub ddlSortPurchase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSortPurchase.SelectedIndexChanged
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlSortSales_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSortSales.SelectedIndexChanged
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub txtCurrrentPage_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                radCmbSearch.ClearSelection()
                radCmbSearch.Text = ""
                BindDatagrid(True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
            Try
                If txtDelOppId.Text > "" Then
                    Dim strOppid As String() = txtDelOppId.Text.Split(",")
                    Dim i As Int16 = 0
                    Dim lngOppID As Long
                    Dim lngRecOwnID As Long
                    Dim lngTerrID As Long
                    Dim objOpport As New COpportunities
                    For i = 0 To strOppid.Length - 1
                        lngOppID = strOppid(i).Split("~")(0)
                        lngRecOwnID = strOppid(i).Split("~")(1)
                        lngTerrID = strOppid(i).Split("~")(2)
                        If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 1 Then
                            Try
                                If lngRecOwnID = Session("UserContactID") Then
                                    DeleteOpp(lngOppID, objOpport)
                                End If
                            Catch ex As Exception

                            End Try
                        ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 2 Then
                            Try
                                Dim j As Integer
                                Dim dtTerritory As New DataTable
                                dtTerritory = Session("UserTerritory")
                                Dim chkDelete As Boolean = False
                                If lngTerrID = 0 Then
                                    chkDelete = True
                                Else
                                    For j = 0 To dtTerritory.Rows.Count - 1
                                        If lngTerrID = dtTerritory.Rows(j).Item("numTerritoryId") Then chkDelete = True
                                    Next
                                End If
                                If chkDelete = True Then
                                    DeleteOpp(lngOppID, objOpport)
                                End If
                            Catch ex As Exception

                            End Try
                        ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 3 Then
                            DeleteOpp(lngOppID, objOpport)
                        End If
                    Next

                End If
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub DeleteOpp(ByVal lngOppID As Long, ByVal objOpport As COpportunities)
            Try
                Dim lintCount As Integer
                With objOpport
                    .OpportID = lngOppID
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                End With
                lintCount = objOpport.GetAuthoritativeOpportunityCount()
                If lintCount = 0 Then
                    Dim objAdmin As New CAdmin

                    objAdmin.DomainID = Session("DomainId")
                    objAdmin.ModeType = MileStone1.ModuleType.Opportunity
                    objAdmin.ProjectID = lngOppID

                    objAdmin.RemoveStagePercentageDetails()

                    objOpport.DelOpp()
                Else : ShowMessage("Some  Opportunities could not be deleted as Authoritative BizDocs is present")
                End If
            Catch ex As Exception
                If ex.Message = "DEPENDANT" Then
                    ShowMessage("Some records could not be removed, Your option is to remove Time and Expense associated with all stages from ""milestone & stages"" sub-tab and try again.")
                ElseIf ex.Message = "RECURRING ORDER OR BIZDOC" Then
                    ShowMessage("Some  Opportunities could not be removed because recurrence is set on order or on bizdoc within order.")
                ElseIf ex.Message = "RECURRED ORDER" Then
                    ShowMessage("Some  Opportunities could not be removed because it is recurred from another order")
                ElseIf ex.Message = "CASE DEPENDANT" Then
                    ShowMessage("Some  Opportunities could not be removed because dependent case exists.")
                ElseIf ex.Message = "CreditBalance DEPENDANT" Then
                    ShowMessage("Some  Opportunities could not be removed because credit balance of record is being used. Your option is used to credit balance of Organization from Accunting sub tab.")
                ElseIf ex.Message = "OppItems DEPENDANT" Then
                    ShowMessage("Some  Opportunities could not be removed because items already used.")
                ElseIf ex.Message = "FY_CLOSED" Then
                    ShowMessage("Some  Opportunities could not be removed because transactions date belongs to closed financial year.")
                ElseIf ex.Message.Contains("SERIAL/LOT#_USED") Then
                    ShowMessage("Can not delete record because some serial/lot# is used in any sales order.")
                Else
                    Throw ex
                End If
            End Try
        End Sub
        Sub GridColumnSearchCriteria()
            Try
                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")

                    Dim strIDValue(), strID(), strCustom As String
                    Dim strRegularCondition As New ArrayList
                    Dim strCustomCondition As New ArrayList


                    For i As Integer = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")
                        strID = strIDValue(0).Split("~")

                        If strID(0).Contains("CFW.Cust") Then

                            Select Case strID(3).Trim()
                                Case "TextBox", "TextArea"
                                    strCustomCondition.Add(" EXISTS (select RecId FROM CFW_Fld_Values_Opp CFW where CFW.RecId = Opp.numOppId AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & " ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                Case "CheckBox"
                                    If strIDValue(1).ToLower() = "yes" Then
                                        strCustomCondition.Add(" EXISTS (select RecId FROM CFW_Fld_Values_Opp CFW where CFW.RecId = Opp.numOppId AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and COALESCE(CFW.Fld_Value,'0') " & "='1')")
                                    ElseIf strIDValue(1).ToLower() = "no" Then
                                        strCustomCondition.Add(" NOT EXISTS (select RecId FROM CFW_Fld_Values_Opp CFW where CFW.RecId = Opp.numOppId AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and COALESCE(CFW.Fld_Value,'0') " & "='1')")
                                    End If
                                Case "SelectBox"
                                    strCustomCondition.Add(" EXISTS (select RecId FROM CFW_Fld_Values_Opp CFW where CFW.RecId = Opp.numOppId AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & "=" & strIDValue(1) & "::VARCHAR)")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strCustomCondition.Add(" EXISTS (select RecId FROM CFW_Fld_Values_Opp CFW where CFW.RecId = Opp.numOppId AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " >= '" & fromDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strCustomCondition.Add(" EXISTS (select RecId FROM CFW_Fld_Values_Opp CFW where CFW.RecId = Opp.numOppId AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " <= '" & toDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    End If
                                Case "CheckBoxList"
                                    Dim items As String() = strIDValue(1).Split(",")
                                    Dim searchString As String = ""

                                    For Each item As String In items
                                        searchString = searchString & If(searchString.Length > 0, " OR ", "") & " GetCustFldValueOpp(" & strID(0).Replace("CFW.Cust", "") & ",Opp.numOppID) " & " ilike '%" & item.Replace("'", "''") & "%'"
                                    Next

                                    strCustomCondition.Add(" EXISTS (select RecId FROM CFW_Fld_Values_Opp CFW where CFW.RecId = Opp.numOppId AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND (" & searchString & "))")
                                Case Else
                                    strCustomCondition.Add(" EXISTS (select RecId FROM CFW_Fld_Values_Opp CFW where CFW.RecId = Opp.numOppId AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & strCustom & ")")
                            End Select
                        Else
                            Select Case strID(3).Trim()
                                Case "Website", "Email", "TextBox"
                                    If strID(0) = "ADC.vcCompactContactDetails" Then
                                        strRegularCondition.Add("(ADC.vcFirstName ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR ADC.vcLastName ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR ADC.numPhone ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR ADC.numPhoneExtension ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                    Else
                                        strRegularCondition.Add(strID(0) & " ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                    End If
                                Case "SelectBox"
                                    If strID(0) = "OL.numWebApiId" Then
                                        If strIDValue(1).StartsWith("Sites~") Then
                                            strRegularCondition.Add("OL.numSiteID=" & strIDValue(1).Replace("Sites~", ""))
                                        Else
                                            strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                                        End If
                                    ElseIf strID(0) = "Opp.tintSource" Then
                                        strRegularCondition.Add("Opp.tintSource=" & strIDValue(1).ToString.Split("~")(0) & " and Opp.tintSourceType=" & strIDValue(1).ToString.Split("~")(1))
                                    Else
                                        strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                                    End If
                                Case "TextArea"
                                    strRegularCondition.Add(" Cast(" & strID(0) & " as varchar(5000)) ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strRegularCondition.Add(strID(0) & " >= '" & fromDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strRegularCondition.Add(strID(0) & " <= '" & toDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    End If
                            End Select
                        End If
                    Next
                    RegularSearch = String.Join(" and ", strRegularCondition.ToArray())
                    CustomSearch = String.Join(" and ", strCustomCondition.ToArray())
                Else
                    RegularSearch = ""
                    CustomSearch = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                txtCurrrentPage_TextChanged(Nothing, Nothing)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Private Sub ShowMessage(ByVal message As String)
            Try
                divMessage.Style.Add("display", "")
                litMessage.Text = message
                divMessage.Focus()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnGotoRecord_Click(sender As Object, e As EventArgs) Handles btnGotoRecord.Click
            Try
                Response.Redirect("~/opportunity/frmOpportunities.aspx?frm=deallist&OpID=" & radCmbSearch.SelectedValue, False)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub radCmbSearch_ItemsRequested(sender As Object, e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs)
            Try
                If e.Text.Trim().Length > 0 Then
                    Dim searchCriteria As String
                    Dim itemOffset As Integer = e.NumberOfItems

                    objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                    objCommon.UserCntID = CCommon.ToLong(Session("UserContactID"))

                    Dim tintOppType As Int16
                    If txtSelIndex.Text = 0 Then
                        tintOppType = 1
                    ElseIf txtSelIndex.Text = 1 Then
                        tintOppType = 2
                    End If

                    Dim ds As DataSet = objCommon.SearchOrders(e.Text.Trim().Replace("%", "[%]"), tintOppType, m_aryRightsForPage(RIGHTSTYPE.VIEW), 1, 2, 0)

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                        Dim endOffset As Integer

                        endOffset = Math.Min(itemOffset + radCmbSearch.ItemsPerRequest, ds.Tables(0).Rows.Count)
                        e.EndOfItems = endOffset = ds.Tables(0).Rows.Count
                        e.Message = IIf(ds.Tables(0).Rows.Count <= 0, "No matches", String.Format("Orders <b>1</b>-<b>{0}</b> out of <b>{1}</b>", endOffset, ds.Tables(0).Rows.Count))

                        For Each dr As DataRow In ds.Tables(0).Rows
                            Dim item As New RadComboBoxItem()
                            item.Text = DirectCast(dr("vcPOppName"), String)
                            item.Value = dr("numOppID").ToString()
                            item.Attributes.Add("orderType", tintOppType)

                            item.DataItem = dr
                            radCmbSearch.Items.Add(item)
                            item.DataBind()
                        Next
                    End If
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                e.Message = "<b style=""color:Red"">Error occured while searching.</b>"
            End Try
        End Sub

        Private Sub InitializeSearchClientSideTemplate()
            Try
                objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                objCommon.UserCntID = CCommon.ToLong(Session("UserContactID"))


                Dim tintOppType As Int16
                If txtSelIndex.Text = 0 Then
                    tintOppType = 1
                ElseIf txtSelIndex.Text = 1 Then
                    tintOppType = 2
                End If

                Dim ds As DataSet = objCommon.SearchOrders("", tintOppType, m_aryRightsForPage(RIGHTSTYPE.VIEW), 0, 2, 0)

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim dtTable As New DataTable
                    dtTable = ds.Tables(0)

                    'genrate header template dynamically
                    radCmbSearch.HeaderTemplate = New SearchTemplate(ListItemType.Header, dtTable, DropDownWidth:=radCmbSearch.DropDownWidth.Value)

                    'generate Clientside Template dynamically
                    radCmbSearch.ItemTemplate = New SearchTemplate(ListItemType.Item, dtTable, DropDownWidth:=radCmbSearch.DropDownWidth.Value)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
    End Class
End Namespace