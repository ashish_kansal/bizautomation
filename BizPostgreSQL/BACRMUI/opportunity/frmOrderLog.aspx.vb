Imports System.IO
Imports BACRM.BusinessLogic.Common

Partial Public Class frmOrderLog : Inherits BACRMPage

    Dim lngOppID As Double
    Dim dsMain As DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DomainID = Session("DomainID")
            UserCntID = Session("UserContactID")
            lngOppID = GetQueryStringVal("OppId")

            Dim strFileName As String = ""
            strFileName = CCommon.GetDocumentPhysicalPath(DomainID) & "OrderShippingDetail_" & lngOppID & "_" & CCommon.ToString(DomainID) & ".xml"
            If Not IsPostBack Then
                If File.Exists(strFileName) Then
                    dsMain = New DataSet
                    dsMain.ReadXml(strFileName)

                    If dsMain IsNot Nothing AndAlso dsMain.Tables.Count = 3 Then
                        Dim dtMain As New DataTable
                        Dim dtBox As New DataTable
                        Dim dtItem As New DataTable

                        dtMain = dsMain.Tables("Main")
                        dtBox = dsMain.Tables("Box")
                        dtItem = dsMain.Tables("Item")

                        If dtMain IsNot Nothing AndAlso dtMain.Rows.Count > 0 Then
                            lblAddedBy.Text = CCommon.ToString(dtMain.Rows(0)("AddedBy"))
                            lblAddedOn.Text = FormattedDateFromDate(CCommon.ToSqlDate(dtMain.Rows(0)("AddedOn")), Session("DateFormat"))
                            lblRegularWeight.Text = CCommon.ToDouble(dtMain.Rows(0)("RegularWeight")).ToString("F2")
                            lblDimensionalWeight.Text = CCommon.ToDouble(dtMain.Rows(0)("DimensionalWeight")).ToString("F2")
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub radBoxes_NeedDataSource(sender As Object, e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles radBoxes.NeedDataSource
        Try

            If dsMain IsNot Nothing Then
                Dim ds As DataSet = dsMain.Copy
                ds.Tables("Item").TableName = "BoxItems"
                ds.Tables.Remove("Main")
                ds.AcceptChanges()
                radBoxes.DataSource = ds
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Function TranslateServiceType(ByVal tintServiceType As Integer) As String
        Try
            Dim service As String = ""

            Select Case tintServiceType
                Case 0 : service = "Unspecified"

                Case 10 : service = "Priority Overnight"
                Case 11 : service = "Standard Overnight"
                Case 12 : service = "First Overnight"
                Case 13 : service = "FedEx 2nd Day"
                Case 14 : service = "FedEx Express Saver"
                Case 15 : service = "FedEx Ground"
                Case 16 : service = "FedEx Ground Home Delivery"
                Case 17 : service = "FedEx 1Day Freight"
                Case 18 : service = "FedEx 2Day Freight"
                Case 19 : service = "FedEx 3Day Freight"
                Case 20 : service = "International Priority"
                Case 21 : service = "International Priority Distribution"
                Case 22 : service = "International Economy"
                Case 23 : service = "International Economy Distribution"
                Case 24 : service = "International First"
                Case 25 : service = "International Priority Freight"
                Case 26 : service = "International Economy Freight"
                Case 27 : service = "International Distribution Freight"
                Case 28 : service = "Europe International Priority"

                Case 70 : service = "USPS Express"
                Case 71 : service = "USPS First Class"
                Case 72 : service = "USPS Priority"
                Case 73 : service = "USPS Parcel Post"
                Case 74 : service = "USPS Bound Printed Matter"
                Case 75 : service = "USPS Media"
                Case 76 : service = "USPS Library"

                Case 40 : service = "UPS Next Day Air"
                Case 42 : service = "UPS 2nd Day Air"
                Case 43 : service = "UPS Ground"
                Case 48 : service = "UPS 3Day Select"
                Case 49 : service = "UPS Next Day Air Saver"
                Case 50 : service = "UPS Saver"
                Case 51 : service = "UPS Next Day Air Early A.M."
                Case 55 : service = "UPS 2nd Day Air AM"
            End Select
            Return service
        Catch ex As Exception
            Throw ex
        End Try
    End Function

End Class