﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master"
    CodeBehind="frmFulfillmentBizDocsList.aspx.vb" Inherits=".frmFulfillmentBizDocsList" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Order Fulfillment BizDocs List</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <script language="javascript">
        function Save() {
            if (document.getElementById("ddlBizDocsInventoryItems").selectedIndex == 0) {
                alert("Select Inventory Items BizDoc");
                document.getElementById("ddlBizDocsInventoryItems").focus();
                return false;
            }
            if (document.getElementById("ddlBizDocsNonInventoryItems").selectedIndex == 0) {
                alert("Select Non-Inventory Items BizDoc");
                document.getElementById("ddlBizDocsNonInventoryItems").focus();
                return false;
            }
            if (document.getElementById("ddlBizDocsServiceItems").selectedIndex == 0) {
                alert("Select Service Items BizDoc");
                document.getElementById("ddlBizDocsServiceItems").focus();
                return false;
            }
        }
    </script>
    <table width="100%">
        <tr>
            <td>
                <div class="input-part">
                    <div class="right-input">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" />&nbsp;
                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="button" />&nbsp;&nbsp;
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <ul id="messagebox" class="errorInfo" style="display: none">
                </ul>
                <asp:Label ID="litMessage" EnableViewState="false" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Order Fulfillment BizDocs List
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
     
   <asp:Table ID="table2" CellPadding="0" Visible ="false"  CellSpacing="0" BorderWidth="1" runat="server"
        Width="800px" CssClass="aspTable" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br />
                <asp:GridView ID="gvBizDoc" runat="server" Width="100%" CssClass="tbl" AllowSorting="true"
                    AutoGenerateColumns="False" DataKeyNames="numBizDocTypeID">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is"></RowStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundField HeaderText="BizDoc Type" DataField="vcBizDocType" />
                        <asp:TemplateField HeaderText="Fulfillment Order" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkFulfillmentOrder" runat="server" Checked='<%# Eval("bitIsFulfillmentOrder")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Inventory Items" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkInventoryItems" runat="server" Checked='<%# Eval("bitIsInventory")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Non-Inventory Items" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkNonInventoryItems" runat="server" Checked='<%# Eval("bitIsNonInventory")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Service Items" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkServiceItems" runat="server" Checked='<%# Eval("bitIsService")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

               Inventory Items :
                <asp:DropDownList ID="ddlBizDocsInventoryItems" runat="server" CssClass="signup">
                </asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                Non-Inventory Items :
                <asp:DropDownList ID="ddlBizDocsNonInventoryItems" runat="server" CssClass="signup">
                </asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                Service Items :
                <asp:DropDownList ID="ddlBizDocsServiceItems" runat="server" CssClass="signup">
                </asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>--%>
