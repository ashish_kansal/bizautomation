﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Opportunities

Public Class frmConfNewOrder : Inherits BACRMPage

#Region "Member Variables"
    Private objCommon As CCommon
    Private objSalesOrderConfiguration As SalesOrderConfiguration
#End Region

#Region "Page Event Handlers"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            If Not Page.IsPostBack Then
                'First Load Master Data
                LoadBizDoc()
                LoadPaymentMethods()
                ' LoadItemColumnSetting()
                LoadItemDetailsSetting()
                LoadOrderSetting()
                LoadCustomerSetting()
                'Bind Data
                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
#End Region
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
#Region "Private Methods"

    Private Sub BindData()
        Try
            objSalesOrderConfiguration = New SalesOrderConfiguration
            objSalesOrderConfiguration.DomainID = Session("DomainID")
            objSalesOrderConfiguration.GetSalesOrderConfiguration()

            If objSalesOrderConfiguration.SalesConfigurationID <> -1 Then

                hfSalesConfigurationID.Value = objSalesOrderConfiguration.SalesConfigurationID
                rdbAutoFocusCustomer.Checked = objSalesOrderConfiguration.bitAutoFocusCustomer
                rdbAutoFocusItem.Checked = objSalesOrderConfiguration.bitAutoFocusItem
                ' chkDisplayRootLocation.Checked = objSalesOrderConfiguration.bitDisplayRootLocation
                chkDisplayCurrency.Checked = objSalesOrderConfiguration.bitDisplayCurrency
                chkDisplayAssignedTo.Checked = objSalesOrderConfiguration.bitDisplayAssignTo
                chkDisplaySalesTemplate.Checked = objSalesOrderConfiguration.bitDisplayTemplate
                chkAutoAssignOrder.Checked = objSalesOrderConfiguration.bitAutoAssignOrder
                chkDisplayLocation.Checked = objSalesOrderConfiguration.bitDisplayLocation
                chkDisplayFinancialStamp.Checked = objSalesOrderConfiguration.bitDisplayFinancialStamp
                chkDisplayItemDetails.Checked = objSalesOrderConfiguration.bitDisplayItemDetails
                chkDisplayUnitCost.Checked = objSalesOrderConfiguration.bitDisplayUnitCost
                chkDisplayProfitTotal.Checked = objSalesOrderConfiguration.bitDisplayProfitTotal
                chkDisplayShippingCluster.Checked = objSalesOrderConfiguration.bitDisplayShippingRates
                chkPaymentMethods.Checked = objSalesOrderConfiguration.bitDisplayPaymentMethods
                chkBizDoc.Checked = objSalesOrderConfiguration.bitCreateOpenBizDoc
                ddlBizDoc.SelectedValue = objSalesOrderConfiguration.numListItemID
                chkDisplyCouponDiscount.Checked = objSalesOrderConfiguration.bitDisplayCouponDiscount
                chkDisplayShippingCharges.Checked = objSalesOrderConfiguration.bitDisplayShippingCharges
                chkDisplayShipVia.Checked = objSalesOrderConfiguration.bitDisplayShipVia
                chkDisplayDiscount.Checked = objSalesOrderConfiguration.bitDisplayDiscount
                chkDisplaySaveNew.Checked = objSalesOrderConfiguration.bitDisplaySaveNew
                chkDisplayAddToCart.Checked = objSalesOrderConfiguration.bitDisplayAddToCart
                chkDisplayCreateInvoice.Checked = objSalesOrderConfiguration.bitDisplayCreateInvoice
                chkDisplayCommentsSection.Checked = objSalesOrderConfiguration.bitDisplayComments
                chkDisplayReleaseDate.Checked = objSalesOrderConfiguration.bitDisplayReleaseDate
                chkDisplayCustomerPartEntry.Checked = objSalesOrderConfiguration.bitDisplayCustomerPartEntry
                chkDisplayOrderExpectedDate.Checked = objSalesOrderConfiguration.bitDisplayExpectedDate
                If (objSalesOrderConfiguration.bitDisplayItemGridOrderAZ = True) Then
                    rdoShowOrderAZ.Checked = True
                Else
                    rdoHideOrderAZ.Checked = True
                End If
                If (objSalesOrderConfiguration.bitDisplayItemGridItemID = True) Then
                    rdoShowItemID.Checked = True
                Else
                    rdoHideItemID.Checked = True
                End If
                If (objSalesOrderConfiguration.bitDisplayItemGridSKU = True) Then
                    rdoShowSKU.Checked = True
                Else
                    rdoHideSKU.Checked = True
                End If
                If (objSalesOrderConfiguration.bitDisplayItemGridUnitListPrice = True) Then
                    rdoShowUnitListPrice.Checked = True
                Else
                    rdoHideUnitListPrice.Checked = True
                End If
                If (objSalesOrderConfiguration.bitDisplayItemGridUnitSalePrice = True) Then
                    rdoShowUnitSalePrice.Checked = True
                Else
                    rdoHideUnitSalePrice.Checked = True
                End If
                If (objSalesOrderConfiguration.bitDisplayItemGridDiscount = True) Then
                    rdoShowDiscount.Checked = True
                Else
                    rdoHideDiscount.Checked = True
                End If
                If (objSalesOrderConfiguration.bitDisplayItemGridItemRelaseDate = True) Then
                    rdoShowItemReleaseDate.Checked = True
                Else
                    rdoHideItemReleaseDate.Checked = True
                End If
                If (objSalesOrderConfiguration.bitDisplayItemGridLocation = True) Then
                    rdoShowLocation.Checked = True
                Else
                    rdoHideLocation.Checked = True
                End If
                If (objSalesOrderConfiguration.bitDisplayItemGridShipTo = True) Then
                    rdoShowShipTo.Checked = True
                Else
                    rdoHideShipTo.Checked = True
                End If
                If (objSalesOrderConfiguration.bitDisplayItemGridOnHandAllocation = True) Then
                    rdoShowOnHandAllocation.Checked = True
                Else
                    rdoHideOnHandAllocation.Checked = True
                End If
                If (objSalesOrderConfiguration.bitDisplayItemGridDescription = True) Then
                    rdoShowDescription.Checked = True
                Else
                    rdoHideDescription.Checked = True
                End If
                If (objSalesOrderConfiguration.bitDisplayItemGridNotes = True) Then
                    rdoShowNotes.Checked = True
                Else
                    rdoHideNotes.Checked = True
                End If
                If (objSalesOrderConfiguration.bitDisplayItemGridAttributes = True) Then
                    rdoShowAttributes.Checked = True
                Else
                    rdoHideAttributes.Checked = True
                End If
                If (objSalesOrderConfiguration.bitDisplayItemGridInclusionDetail = True) Then
                    rdoShowInclusionDetail.Checked = True
                Else
                    rdoHideInclusionDetail.Checked = True
                End If
                If (objSalesOrderConfiguration.bitDisplayItemGridItemClassification = True) Then
                    rdoShowItemClassification.Checked = True
                Else
                    rdoHideItemClassification.Checked = True
                End If

                If (objSalesOrderConfiguration.bitDisplayItemGridItemPromotion = True) Then
                    rdShowItemPromotion.Checked = True
                Else
                    rdHideItemPromotion.Checked = True
                End If

                If (objSalesOrderConfiguration.bitDisplayApplyPromotionCode = True) Then
                    rbSHowApplyPromotionCode.Checked = True
                Else
                    rbHideApplyPromotionCode.Checked = True
                End If

                If (objSalesOrderConfiguration.bitDisplayPayButton = True) Then
                    rbShowPay.Checked = True
                Else
                    rbHidePay.Checked = True
                End If

                If (objSalesOrderConfiguration.bitDisplayPOSPay = True) Then
                    rbShowPOSPay.Checked = True
                Else
                    rbHidePOSPay.Checked = True
                End If

                If Not String.IsNullOrEmpty(CCommon.ToString(objSalesOrderConfiguration.vcPOHiddenColumns)) Then
                    objSalesOrderConfiguration.vcPOHiddenColumns = "," & objSalesOrderConfiguration.vcPOHiddenColumns & ","

                    If objSalesOrderConfiguration.vcPOHiddenColumns.Contains(",ItemID,") Then
                        rdoShowItemIDPO.Checked = False
                        rdoHideItemIDPO.Checked = True
                    End If

                    If objSalesOrderConfiguration.vcPOHiddenColumns.Contains(",Item,") Then
                        rdoShowItemPO.Checked = False
                        rdoHideItemPO.Checked = True
                    End If

                    If objSalesOrderConfiguration.vcPOHiddenColumns.Contains(",Description,") Then
                        rdoShowDescriptionPO.Checked = False
                        rdoHideDescriptionPO.Checked = True
                    End If

                    If objSalesOrderConfiguration.vcPOHiddenColumns.Contains(",SKU,") Then
                        rdoShowSKUPO.Checked = False
                        rdoHideSKUPO.Checked = True
                    End If

                    If objSalesOrderConfiguration.vcPOHiddenColumns.Contains(",Warehouse,") Then
                        rdoShowWarehousePO.Checked = False
                        rdoHideWarehousePO.Checked = True
                    End If
                End If

                If Not String.IsNullOrEmpty(objSalesOrderConfiguration.vcPaymentMethodIDs) Then
                    Dim arrPaymentMethodIDs() As String = Split(objSalesOrderConfiguration.vcPaymentMethodIDs, ",")
                    For i As Integer = 0 To arrPaymentMethodIDs.Length - 1
                        If Not chklPaymentMethods.Items.FindByValue(arrPaymentMethodIDs(i)) Is Nothing Then
                            chklPaymentMethods.Items.FindByValue(arrPaymentMethodIDs(i)).Selected = True
                        End If
                    Next
                End If

            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadBizDoc()
        Try
            objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            objCommon.ListID = 27
            Dim dtBizDoc As New DataTable
            dtBizDoc = objCommon.GetBizDocsByOrderType(1, 1)
            ddlBizDoc.DataSource = dtBizDoc
            ddlBizDoc.DataTextField = "vcData"
            ddlBizDoc.DataValueField = "numListItemID"
            ddlBizDoc.DataBind()
            ddlBizDoc.Items.Insert(0, New ListItem("--Select One --", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadPaymentMethods()
        Try
            objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            objCommon.ListID = 31
            Dim dtPaymentMethods As New DataTable
            dtPaymentMethods = objCommon.GetMasterListItemsWithRights()
            chklPaymentMethods.DataSource = dtPaymentMethods
            chklPaymentMethods.DataTextField = "vcData"
            chklPaymentMethods.DataValueField = "numListItemID"
            chklPaymentMethods.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadItemColumnSetting()
        Try
            Dim str As String
            objCommon = New CCommon
            Dim objlayout As New CcustPageLayout
            Dim dttable1 As DataTable
            Dim i, count, z, r As Integer
            Dim pageid As Integer = 0


            str = "<div class='col-md-12'>"
            str = str + "<div class='row'>"
            z = 0
            Dim max As String
            max = 1
            Dim maxHeight As Int16 = 0
            While (z <= max)
                objlayout.DomainID = Session("domainId")
                objlayout.ColumnID = z

                Dim ds As New DataSet
                Dim dttable2 As DataTable = Nothing

                objlayout.PageId = 0 '5
                objlayout.FormId = 92
                objlayout.PageType = 2

                ds = objlayout.getValuesWithddl()
                If (ds.Tables.Count = 2) Then
                    dttable1 = ds.Tables(0)
                    dttable2 = ds.Tables(1)
                Else : dttable1 = ds.Tables(0)
                End If

                If (Not dttable2 Is Nothing) Then
                    If dttable2.Rows.Count > 0 Then
                        dttable1.Merge(dttable2)
                    End If
                End If

                Dim dv As DataView

                dv = New DataView(dttable1)

                If z <> 0 Then
                    dv.Sort = "tintRow"
                ElseIf z = 0 Then
                    dv.Sort = "vcfieldName"
                End If

                dv.Table.AcceptChanges()

                i = 0
                count = 0
                count = dv.Table.Rows.Count
                str = str & "<div class='col-md-4'>"
                If (z = 0) Then
                    str = str + "<span style='text-align:center; font-weight:bold'>Available Fields</span>"
                ElseIf z = 1 Then
                    str = str + "<span style='text-align:center; font-weight:bold'>Selected Fields</span>"
                ElseIf z = 2 Then
                    str = str + "<span style='text-align:center; font-weight:bold'>Fields added to column 2</span>"
                End If
                str = str & "<ul id='z" + z.ToString + "' class='sortable boxy' style='height:auto; min-height:30px' >"
                While i < count
                    str = str & "<li id='" + dv(i).Item("numFieldId").ToString + "/" + dv(i).Item("bitCustomField").ToString + "/" + Convert.ToInt32(dv(i).Item("bitRequired")).ToString + "' >" + dv(i).Item("vcFieldName") + "</li>"
                    i += 1
                End While
                str = str & "</ul>"
                str = str & " </div>"
                z += 1
                dttable1.Rows.Clear()

                If count > maxHeight Then
                    maxHeight = count
                End If
            End While
            str = str + "</div>"
            str = str + "</div>"

            'divItemColumnSetting.InnerHtml = str
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadItemDetailsSetting()
        Try
            Dim str As String
            objCommon = New CCommon
            Dim objlayout As New CcustPageLayout
            Dim dttable1 As DataTable
            Dim i, count, y, r As Integer
            Dim pageid As Integer = 0


            str = "<div class='col-md-12'>"
            str = str + "<div class='row'>"
            y = 0
            Dim max As String
            max = rows.Text
            Dim maxHeight As Int16 = 0
            While (y <= max)
                objlayout.DomainID = Session("domainId")
                objlayout.ColumnID = y

                Dim ds As New DataSet
                Dim dttable2 As DataTable = Nothing

                objlayout.PageId = 5
                objlayout.FormId = 91
                objlayout.PageType = 2

                ds = objlayout.getValuesWithddl()
                If (ds.Tables.Count = 2) Then
                    dttable1 = ds.Tables(0)
                    dttable2 = ds.Tables(1)
                Else : dttable1 = ds.Tables(0)
                End If

                If (Not dttable2 Is Nothing) Then
                    If dttable2.Rows.Count > 0 Then
                        dttable1.Merge(dttable2)
                    End If
                End If

                Dim dv As DataView

                dv = New DataView(dttable1)

                If y <> 0 Then
                    dv.Sort = "tintRow"
                ElseIf y = 0 Then
                    dv.Sort = "vcfieldName"
                End If

                dv.Table.AcceptChanges()

                i = 0
                count = 0
                count = dv.Table.Rows.Count
                str = str & "<div class='col-md-4'>"
                If (y = 0) Then
                    str = str + "<span style='text-align:center; font-weight:bold'>Available Fields</span>"
                ElseIf y = 1 Then
                    str = str + "<span style='text-align:center; font-weight:bold'>Fields added to column 1</span>"
                ElseIf y = 2 Then
                    str = str + "<span style='text-align:center; font-weight:bold'>Fields added to column 2</span>"
                End If
                str = str & "<ul id='y" + y.ToString + "' class='sortable boxy' style='height:auto; min-height:30px' >"
                While i < count
                    str = str & "<li id='" + dv(i).Item("numFieldId").ToString + "/" + dv(i).Item("bitCustomField").ToString + "/" + Convert.ToInt32(dv(i).Item("bitRequired")).ToString + "' >" + dv(i).Item("vcFieldName") + "</li>"
                    i += 1
                End While
                str = str & "</ul>"
                str = str & " </div>"
                y += 1
                dttable1.Rows.Clear()

                If count > maxHeight Then
                    maxHeight = count
                End If
            End While
            str = str + "</div>"
            str = str + "</div>"

            divItemDetail.InnerHtml = str
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadOrderSetting()
        Try

            Dim str As String
            objCommon = New CCommon
            Dim objlayout As New CcustPageLayout
            Dim dttable1 As DataTable
            Dim i, count, x, r As Integer
            Dim pageid As Integer = 0

            str = "<div class='col-md-12'>"
            str = str + "<div class='row'>"
            x = 0
            Dim max As String
            max = 2
            Dim maxHeight As Int16 = 0

            While (x <= max)
                objlayout.DomainID = Session("domainId")
                objlayout.ColumnID = x

                Dim ds As New DataSet
                Dim dttable2 As DataTable = Nothing

                objlayout.PageId = 2
                objlayout.FormId = 90
                objlayout.PageType = 2

                ds = objlayout.getValuesWithddl()
                If (ds.Tables.Count = 2) Then
                    dttable1 = ds.Tables(0)
                    dttable2 = ds.Tables(1)
                Else : dttable1 = ds.Tables(0)
                End If

                If (Not dttable2 Is Nothing) Then
                    If dttable2.Rows.Count > 0 Then
                        dttable1.Merge(dttable2)
                    End If
                End If

                Dim dv As DataView
                dv = New DataView(dttable1)

                If x <> 0 Then
                    dv.Sort = "tintRow"
                ElseIf x = 0 Then
                    dv.Sort = "vcfieldName"
                End If

                dv.Table.AcceptChanges()

                i = 0
                count = 0
                count = dv.Table.Rows.Count
                str = str & "<div class='col-md-4'>"
                If (x = 0) Then
                    str = str + "<span style='text-align:center; font-weight:bold'>Available Fields</span>"
                ElseIf x = 1 Then
                    'str = str + "<span style='text-align:center; font-weight:bold'>Selected Fields</span>"
                    str = str + "<span style='text-align:center; font-weight:bold'>Added to Column 1</span>"

                ElseIf x = 2 Then
                    str = str + "<span style='text-align:center; font-weight:bold'>Added to Column 2</span>"
                End If

                str = str & "<ul id='x" + x.ToString + "' class='sortable boxy' style='height:auto; min-height:30px' >"
                While i < count
                    str = str & "<li id='" + dv(i).Item("numFieldId").ToString + "/" + dv(i).Item("bitCustomField").ToString + "/" + Convert.ToInt32(dv(i).Item("bitRequired")).ToString + "' >" + dv(i).Item("vcFieldName") + "</li>"
                    i += 1
                End While
                str = str & "</ul>"
                str = str & " </div>"
                x += 1
                dttable1.Rows.Clear()

                If count > maxHeight Then
                    maxHeight = count
                End If
            End While
            str = str + "</div>"
            str = str + "</div>"

            divOrderDetail.InnerHtml = str
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

    Private Sub LoadCustomerSetting()
        Try
            Dim str As String
            objCommon = New CCommon
            Dim objlayout As New CcustPageLayout
            Dim dttable1 As DataTable
            Dim i, count, x, r As Integer
            Dim pageid As Integer = 0

            str = "<div class='col-md-12'>"
            str = str + "<div class='row'>"
            x = 0
            Dim max As String
            max = 2
            Dim maxHeight As Int16 = 0
            While (x <= max)
                objlayout.DomainID = Session("domainId")
                objlayout.ColumnID = x

                Dim ds As New DataSet
                Dim dttable2 As DataTable = Nothing

                objlayout.PageId = 1
                objlayout.FormId = 93
                objlayout.PageType = 2

                ds = objlayout.getValuesWithddl()
                If (ds.Tables.Count = 2) Then
                    dttable1 = ds.Tables(0)
                    dttable2 = ds.Tables(1)
                Else : dttable1 = ds.Tables(0)
                End If

                If (Not dttable2 Is Nothing) Then
                    If dttable2.Rows.Count > 0 Then
                        dttable1.Merge(dttable2)
                    End If
                End If

                Dim dv As DataView

                dv = New DataView(dttable1)

                If x <> 0 Then
                    dv.Sort = "tintRow"
                ElseIf x = 0 Then
                    dv.Sort = "vcfieldName"
                End If

                dv.Table.AcceptChanges()

                i = 0
                count = 0
                count = dv.Table.Rows.Count
                str = str & "<div class='col-md-4'>"
                If (x = 0) Then
                    str = str + "<span style='text-align:center; font-weight:bold'>Available Fields</span>"
                ElseIf x = 1 Then
                    str = str + "<span style='text-align:center; font-weight:bold'>Fields added to column 1</span>"
                ElseIf x = 2 Then
                    str = str + "<span style='text-align:center; font-weight:bold'>Fields added to column 2</span>"
                End If

                str = str & "<ul id='w" + x.ToString + "' class='sortable boxy' style='height:auto; min-height:30px' >"
                While i < count
                    str = str & "<li id='" + dv(i).Item("numFieldId").ToString + "/" + dv(i).Item("bitCustomField").ToString + "/" + Convert.ToInt32(dv(i).Item("bitRequired")).ToString + "' >" + dv(i).Item("vcFieldName") + "</li>"
                    i += 1
                End While
                str = str & "</ul>"
                str = str & " </div>"
                x += 1
                dttable1.Rows.Clear()

                If count > maxHeight Then
                    maxHeight = count
                End If
            End While
            str = str + "</div>"
            str = str + "</div>"


            divCustomerDetail.InnerHtml = str
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SaveItemColumnSetting()
        Try

            Dim container(2) As String
            Dim header(10) As String
            Dim headerdata(10) As String
            Dim data1XML As String
            Dim temp(10) As String
            Dim data1(2) As String
            Dim i As Integer = 1
            Dim j As Integer = 0

            Dim objDT As System.Data.DataTable
            Dim objDT1 As System.Data.DataTable
            Dim objDR As System.Data.DataRow
            objDT = New System.Data.DataTable("Table")
            objDT1 = New System.Data.DataTable("Table")

            objDT.Columns.Add("numFieldID", GetType(Integer))
            objDT.Columns.Add("tintRow", GetType(Integer))
            objDT.Columns.Add("intColumn", GetType(Integer))
            objDT.Columns.Add("numUserCntId", GetType(Integer))
            objDT.Columns.Add("numDomainID", GetType(Integer))
            objDT.Columns.Add("bitCustomField", GetType(Integer))
            objDT.Columns.Add("numRelCntType", GetType(Integer))
            objDT.Columns.Add("Ctype", GetType(Char))

            Dim ds As New DataSet
            Dim objlayout As New CcustPageLayout
            Dim data As String = hfItemColumn.Value

            container = data.Split(":")

            Dim max As String
            max = 1
            While (i <= max)
                temp = container(i).Split("(")
                header(i) = temp(0)
                temp = temp(1).Split(")")
                headerdata = temp(0).Split(",")
                j = 0
                While (j < headerdata.Length)
                    If (headerdata(j) <> "") Then
                        objDR = objDT.NewRow
                        data1 = headerdata(j).Split("/")
                        objDR("numFieldID") = data1(0)
                        objDR("tintRow") = j + 1
                        objDR("intColumn") = i
                        objDR("numDomainID") = Session("domainId")
                        objDR("numUserCntId") = 0
                        objDR("numRelCntType") = 0
                        If (data1(1) = "False" Or data1(1) = "0") Then
                            objDR("bitCustomField") = 0
                        ElseIf (data1(1) = "True" Or data1(1) = "1") Then
                            objDR("bitCustomField") = 1
                        End If
                        objDT.Rows.Add(objDR)
                    End If
                    j += 1
                End While

                ds.Tables.Add(objDT)
                data1XML = ds.GetXml()
                objlayout.DomainID = Session("domainId")
                objlayout.FormId = 92
                objlayout.PageType = 2
                objlayout.UserCntID = 0
                objlayout.ColumnID = i
                objlayout.RowString = data1XML

                objlayout.SaveList()
                objDT.Rows.Clear()
                If ds.Tables.Count > 0 Then ds.Tables.Remove(ds.Tables(0))
                i += 1
            End While
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SaveItemDetailsSetting()
        Try

            Dim container(3) As String
            Dim header(10) As String
            Dim headerdata(10) As String
            Dim data1XML As String
            Dim temp(10) As String
            Dim data1(2) As String
            Dim i As Integer = 1
            Dim j As Integer = 0

            Dim objDT As System.Data.DataTable
            Dim objDT1 As System.Data.DataTable
            Dim objDR As System.Data.DataRow
            objDT = New System.Data.DataTable("Table")
            objDT1 = New System.Data.DataTable("Table")

            objDT.Columns.Add("numFieldID", GetType(Integer))
            objDT.Columns.Add("tintRow", GetType(Integer))
            objDT.Columns.Add("intColumn", GetType(Integer))
            objDT.Columns.Add("numUserCntId", GetType(Integer))
            objDT.Columns.Add("numDomainID", GetType(Integer))
            objDT.Columns.Add("bitCustomField", GetType(Integer))
            objDT.Columns.Add("numRelCntType", GetType(Integer))
            objDT.Columns.Add("Ctype", GetType(Char))

            Dim ds As New DataSet
            Dim objlayout As New CcustPageLayout
            Dim data As String = hfItemDetail.Value

            container = data.Split(":")

            Dim max As String
            max = 2
            While (i <= max)
                temp = container(i).Split("(")
                header(i) = temp(0)
                temp = temp(1).Split(")")
                headerdata = temp(0).Split(",")
                j = 0
                While (j < headerdata.Length)
                    If (headerdata(j) <> "") Then
                        objDR = objDT.NewRow
                        data1 = headerdata(j).Split("/")
                        objDR("numFieldID") = data1(0)
                        objDR("tintRow") = j + 1
                        objDR("intColumn") = i
                        objDR("numDomainID") = Session("domainId")
                        objDR("numUserCntId") = 0
                        objDR("numRelCntType") = 0
                        If (data1(1) = "False" Or data1(1) = "0") Then
                            objDR("bitCustomField") = 0
                        ElseIf (data1(1) = "True" Or data1(1) = "1") Then
                            objDR("bitCustomField") = 1
                        End If
                        objDT.Rows.Add(objDR)
                    End If
                    j += 1
                End While

                ds.Tables.Add(objDT)
                data1XML = ds.GetXml()
                objlayout.DomainID = Session("domainId")
                objlayout.FormId = 91
                objlayout.PageType = 2
                objlayout.UserCntID = 0
                objlayout.ColumnID = i
                objlayout.RowString = data1XML

                objlayout.SaveList()
                objDT.Rows.Clear()
                If ds.Tables.Count > 0 Then ds.Tables.Remove(ds.Tables(0))
                i += 1
            End While
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SaveOrderSetting()
        Try

            Dim container(3) As String
            Dim header(10) As String
            Dim headerdata(10) As String
            Dim data1XML As String
            Dim temp(10) As String
            Dim data1(2) As String
            Dim i As Integer = 1
            Dim j As Integer = 0

            Dim objDT As System.Data.DataTable
            Dim objDT1 As System.Data.DataTable
            Dim objDR As System.Data.DataRow
            objDT = New System.Data.DataTable("Table")
            objDT1 = New System.Data.DataTable("Table")

            objDT.Columns.Add("numFieldID", GetType(Integer))
            objDT.Columns.Add("tintRow", GetType(Integer))
            objDT.Columns.Add("intColumn", GetType(Integer))
            objDT.Columns.Add("numUserCntId", GetType(Integer))
            objDT.Columns.Add("numDomainID", GetType(Integer))
            objDT.Columns.Add("bitCustomField", GetType(Integer))
            objDT.Columns.Add("numRelCntType", GetType(Integer))
            objDT.Columns.Add("Ctype", GetType(Char))

            Dim ds As New DataSet
            Dim objlayout As New CcustPageLayout
            Dim data As String = hfOrderDetail.Value

            container = data.Split(":")

            Dim max As String
            max = 2
            While (i <= max)
                temp = container(i).Split("(")
                header(i) = temp(0)
                temp = temp(1).Split(")")
                headerdata = temp(0).Split(",")
                j = 0
                While (j < headerdata.Length)
                    If (headerdata(j) <> "") Then
                        objDR = objDT.NewRow
                        data1 = headerdata(j).Split("/")
                        objDR("numFieldID") = data1(0)
                        objDR("tintRow") = j + 1
                        objDR("intColumn") = i
                        objDR("numDomainID") = Session("domainId")
                        objDR("numUserCntId") = 0
                        objDR("numRelCntType") = 0
                        If (data1(1) = "False" Or data1(1) = "0") Then
                            objDR("bitCustomField") = 0
                        ElseIf (data1(1) = "True" Or data1(1) = "1") Then
                            objDR("bitCustomField") = 1
                        End If
                        objDT.Rows.Add(objDR)
                    End If
                    j += 1
                End While

                ds.Tables.Add(objDT)
                data1XML = ds.GetXml()
                objlayout.DomainID = Session("domainId")
                objlayout.FormId = 90
                objlayout.PageType = 2
                objlayout.UserCntID = 0
                objlayout.ColumnID = i
                objlayout.RowString = data1XML

                objlayout.SaveList()
                objDT.Rows.Clear()
                If ds.Tables.Count > 0 Then ds.Tables.Remove(ds.Tables(0))
                i += 1
            End While
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SaveCustomerSetting()
        Try

            Dim container(3) As String
            Dim header(10) As String
            Dim headerdata(10) As String
            Dim data1XML As String
            Dim temp(10) As String
            Dim data1(2) As String
            Dim i As Integer = 1
            Dim j As Integer = 0

            Dim objDT As System.Data.DataTable
            Dim objDT1 As System.Data.DataTable
            Dim objDR As System.Data.DataRow
            objDT = New System.Data.DataTable("Table")
            objDT1 = New System.Data.DataTable("Table")

            objDT.Columns.Add("numFieldID", GetType(Integer))
            objDT.Columns.Add("tintRow", GetType(Integer))
            objDT.Columns.Add("intColumn", GetType(Integer))
            objDT.Columns.Add("numUserCntId", GetType(Integer))
            objDT.Columns.Add("numDomainID", GetType(Integer))
            objDT.Columns.Add("bitCustomField", GetType(Integer))
            objDT.Columns.Add("numRelCntType", GetType(Integer))
            objDT.Columns.Add("Ctype", GetType(Char))

            Dim ds As New DataSet
            Dim objlayout As New CcustPageLayout
            Dim data As String = hfCustomerDetail.Value

            container = data.Split(":")

            Dim max As String
            max = 2
            While (i <= max)
                temp = container(i).Split("(")
                header(i) = temp(0)
                temp = temp(1).Split(")")
                headerdata = temp(0).Split(",")
                j = 0
                While (j < headerdata.Length)
                    If (headerdata(j) <> "") Then
                        objDR = objDT.NewRow
                        data1 = headerdata(j).Split("/")
                        objDR("numFieldID") = data1(0)
                        objDR("tintRow") = j + 1
                        objDR("intColumn") = i
                        objDR("numDomainID") = Session("domainId")
                        objDR("numUserCntId") = 0
                        objDR("numRelCntType") = 0
                        If (data1(1) = "False" Or data1(1) = "0") Then
                            objDR("bitCustomField") = 0
                        ElseIf (data1(1) = "True" Or data1(1) = "1") Then
                            objDR("bitCustomField") = 1
                        End If
                        objDT.Rows.Add(objDR)
                    End If
                    j += 1
                End While

                ds.Tables.Add(objDT)
                data1XML = ds.GetXml()
                objlayout.DomainID = Session("domainId")
                objlayout.FormId = 93
                objlayout.PageType = 2
                objlayout.UserCntID = 0
                objlayout.ColumnID = i
                objlayout.RowString = data1XML

                objlayout.SaveList()
                objDT.Rows.Clear()
                If ds.Tables.Count > 0 Then ds.Tables.Remove(ds.Tables(0))
                i += 1
            End While
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub SaveSalesConfigurationDetail()
        Try
            objSalesOrderConfiguration = New SalesOrderConfiguration
            objSalesOrderConfiguration.DomainID = Session("DomainID")
            objSalesOrderConfiguration.SalesConfigurationID = CCommon.ToInteger(hfSalesConfigurationID.Value)
            objSalesOrderConfiguration.bitAutoFocusCustomer = rdbAutoFocusCustomer.Checked
            objSalesOrderConfiguration.bitAutoFocusItem = rdbAutoFocusItem.Checked
            objSalesOrderConfiguration.bitDisplayRootLocation = True   'chkDisplayRootLocation.Checked
            objSalesOrderConfiguration.bitDisplayCurrency = chkDisplayCurrency.Checked
            objSalesOrderConfiguration.bitDisplayAssignTo = chkDisplayAssignedTo.Checked
            objSalesOrderConfiguration.bitDisplayTemplate = chkDisplaySalesTemplate.Checked
            objSalesOrderConfiguration.bitAutoAssignOrder = chkAutoAssignOrder.Checked
            objSalesOrderConfiguration.bitDisplayLocation = chkDisplayLocation.Checked
            objSalesOrderConfiguration.bitDisplayFinancialStamp = chkDisplayFinancialStamp.Checked
            objSalesOrderConfiguration.bitDisplayItemDetails = chkDisplayItemDetails.Checked
            objSalesOrderConfiguration.bitDisplayUnitCost = chkDisplayUnitCost.Checked
            objSalesOrderConfiguration.bitDisplayProfitTotal = chkDisplayProfitTotal.Checked
            objSalesOrderConfiguration.bitDisplayShippingRates = chkDisplayShippingCluster.Checked
            objSalesOrderConfiguration.bitDisplayPaymentMethods = chkPaymentMethods.Checked
            objSalesOrderConfiguration.bitDisplayCouponDiscount = chkDisplyCouponDiscount.Checked
            objSalesOrderConfiguration.bitDisplayShippingCharges = chkDisplayShippingCharges.Checked
            objSalesOrderConfiguration.bitDisplayShipVia = chkDisplayShipVia.Checked
            objSalesOrderConfiguration.bitDisplayDiscount = chkDisplayDiscount.Checked
            objSalesOrderConfiguration.bitDisplaySaveNew = chkDisplaySaveNew.Checked
            objSalesOrderConfiguration.bitDisplayAddToCart = chkDisplayAddToCart.Checked
            objSalesOrderConfiguration.bitDisplayCreateInvoice = chkDisplayCreateInvoice.Checked
            objSalesOrderConfiguration.bitDisplayComments = chkDisplayCommentsSection.Checked
            objSalesOrderConfiguration.bitDisplayReleaseDate = chkDisplayReleaseDate.Checked
            objSalesOrderConfiguration.bitDisplayCustomerPartEntry = chkDisplayCustomerPartEntry.Checked
            objSalesOrderConfiguration.bitDisplayExpectedDate = chkDisplayOrderExpectedDate.Checked
            objSalesOrderConfiguration.bitDisplayItemGridOrderAZ = rdoShowOrderAZ.Checked
            objSalesOrderConfiguration.bitDisplayItemGridItemID = rdoShowItemID.Checked
            objSalesOrderConfiguration.bitDisplayItemGridSKU = rdoShowSKU.Checked
            objSalesOrderConfiguration.bitDisplayItemGridUnitListPrice = rdoShowUnitListPrice.Checked
            objSalesOrderConfiguration.bitDisplayItemGridUnitSalePrice = rdoShowUnitSalePrice.Checked
            objSalesOrderConfiguration.bitDisplayItemGridDiscount = rdoShowDiscount.Checked
            objSalesOrderConfiguration.bitDisplayItemGridItemRelaseDate = rdoShowItemReleaseDate.Checked
            objSalesOrderConfiguration.bitDisplayItemGridLocation = rdoShowLocation.Checked
            objSalesOrderConfiguration.bitDisplayItemGridShipTo = rdoShowShipTo.Checked
            objSalesOrderConfiguration.bitDisplayItemGridOnHandAllocation = rdoShowOnHandAllocation.Checked
            objSalesOrderConfiguration.bitDisplayItemGridDescription = rdoShowDescription.Checked
            objSalesOrderConfiguration.bitDisplayItemGridNotes = rdoShowNotes.Checked
            objSalesOrderConfiguration.bitDisplayItemGridAttributes = rdoShowAttributes.Checked
            objSalesOrderConfiguration.bitDisplayItemGridInclusionDetail = rdoShowInclusionDetail.Checked
            objSalesOrderConfiguration.bitDisplayItemGridItemClassification = rdoShowItemClassification.Checked
            objSalesOrderConfiguration.bitDisplayItemGridItemPromotion = rdShowItemPromotion.Checked
            objSalesOrderConfiguration.bitDisplayApplyPromotionCode = rbSHowApplyPromotionCode.Checked
            objSalesOrderConfiguration.bitDisplayPayButton = rbShowPay.Checked
            objSalesOrderConfiguration.bitDisplayPOSPay = rbShowPOSPay.Checked

            Dim vcPOHiddenColumns As String = ""

            If rdoHideItemIDPO.Checked Then
                vcPOHiddenColumns += (IIf(vcPOHiddenColumns.Length > 0, ",", "") & "ItemID")
            End If
            If rdoHideItemPO.Checked Then
                vcPOHiddenColumns += (IIf(vcPOHiddenColumns.Length > 0, ",", "") & "Item")
            End If
            If rdoHideDescriptionPO.Checked Then
                vcPOHiddenColumns += (IIf(vcPOHiddenColumns.Length > 0, ",", "") & "Description")
            End If
            If rdoHideSKUPO.Checked Then
                vcPOHiddenColumns += (IIf(vcPOHiddenColumns.Length > 0, ",", "") & "SKU")
            End If
            If rdoHideWarehousePO.Checked Then
                vcPOHiddenColumns += (IIf(vcPOHiddenColumns.Length > 0, ",", "") & "Warehouse")
            End If

            objSalesOrderConfiguration.vcPOHiddenColumns = vcPOHiddenColumns

            If chkPaymentMethods.Checked Then
                Dim selectedPaymentMethods As String
                For Each li As ListItem In chklPaymentMethods.Items
                    If li.Selected Then
                        selectedPaymentMethods = selectedPaymentMethods + li.Value + ","
                    End If
                Next
                selectedPaymentMethods = CCommon.ToString(selectedPaymentMethods)
                objSalesOrderConfiguration.vcPaymentMethodIDs = selectedPaymentMethods.TrimEnd(",")
            Else
                objSalesOrderConfiguration.vcPaymentMethodIDs = String.Empty
            End If

            objSalesOrderConfiguration.bitCreateOpenBizDoc = chkBizDoc.Checked

            If chkBizDoc.Checked Then
                objSalesOrderConfiguration.numListItemID = ddlBizDoc.SelectedValue
            Else
                objSalesOrderConfiguration.numListItemID = -1
            End If


            objSalesOrderConfiguration.SaveSalesOrderConfiguration()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region "Event Handlers"
    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            SaveSalesConfigurationDetail()
            'SaveItemColumnSetting()
            SaveItemDetailsSetting()
            SaveOrderSetting()
            SaveCustomerSetting()

            ' LoadItemColumnSetting()
            LoadItemDetailsSetting()
            LoadOrderSetting()
            LoadCustomerSetting()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
#End Region

End Class