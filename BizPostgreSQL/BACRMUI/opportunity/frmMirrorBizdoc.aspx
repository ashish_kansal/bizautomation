<%@ Page Language="vb" AutoEventWireup="true" AspCompat="true" EnableEventValidation="false"
    CodeBehind="frmMirrorBizdoc.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmMirrorBizdoc"
    ValidateRequest="false" %>

<%@ Register Src="../Accounting/PaymentHistory.ascx" TagName="PaymentHistory" TagPrefix="uc1" %>

<%--disabled validate request to submit html form client side when clicked on send email--%>
<%--<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="rad" %>--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>BizDocs</title>
    <script language="javascript" type="text/javascript">
        function PrintIt() {
            //if (confirm("To print bizdoc with background color and border you need to check 'Print Background(colors & images)' from page setup options of your browser") == true) {
                document.getElementById('tblButtons').style.display = 'none';
                document.getElementById('tblApproval').style.display = 'none';
                window.print();
            //}
            return false;
        }

        function Close1(lngOppID) {
            //opener.location='dreamweaver.htm';self.close()">
            // window.open('../opportunity/frmQBStatusReport.aspx?BizDocID='+lngOppID,'','toolbar=no,titlebar=no,top=300,width=800,height=500,scrollbars=yes,resizable=yes')
            window.opener.reDirect('../opportunity/frmOpportunities.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylist&OpID=' + lngOppID);
            self.close();
            return false;
        }

        function RefereshParentPage() {
            document.Form1.btnTemp_DontDelete.click()
            return false;
        }
        function OpenAmtPaid(a, b, c) {
            //var BalanceAmt;
            //BalanceAmt = 0;
            //if (document.getElementById('lblBalance') != null) {
            //    BalanceAmt = document.getElementById('lblBalance').innerHTML;
            //    BalanceAmt = BalanceAmt.replace(/,/g, "");
            //}
            //if (BalanceAmt == "")
            //    BalanceAmt = 0;
            window.open('../opportunity/frmAmtPaid.aspx?OppBizId=' + a + '&OppId=' + b + '&DivId=' + c, 'ReceivePayment', 'toolbar=no,titlebar=no,top=300,width=700,height=400,scrollbars=no,resizable=no');
            return false;
        }

        function OpenPaymentAmtPaid(a) {
            window.opener.location.href = '../opportunity/frmAmtPaid.aspx?DepositeID=' + a;
            //self.close();
            return false;
        }

        function OpenRecievedDate(OppID) {
            window.open('../opportunity/frmRecievedDate.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID=' + OppID, '', 'toolbar=no,titlebar=no,top=300,width=400,height=100,scrollbars=yes,resizable=yes');
            return false;
        }
        function ShowWindow(Page, q, att) {

            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                return false;

            }
        }


        function OpenLogoPage() {
            window.open('frmLogoUpload.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=BizDocs', '', 'toolbar=no,titlebar=no,top=300,width=400,height=200,scrollbars=yes,resizable=yes');
            return false;
        }

        function Close() {
            window.close();
        }

        function CheckNumber(cint) {
            if (cint == 1) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode == 44 || window.event.keyCode == 46)) {
                    window.event.keyCode = 0;
                }
            }
            if (cint == 2) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                    window.event.keyCode = 0;
                }
            }

        }
        function openApp(a, b, c) {

            if (document.all) {
                window.open('../Documents/frmDocApprovers.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DocID=' + a + '&DocType=' + b + '&OpID=' + c + "&DocName=" + document.getElementById('lblBizDocIDValue').innerText, '', 'toolbar=no,titlebar=no,left=300,top=450,width=900,height=600,scrollbars=yes,resizable=yes')
            } else {
                window.open('../Documents/frmDocApprovers.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DocID=' + a + '&DocType=' + b + '&OpID=' + c + "&DocName=" + document.getElementById('lblBizDocIDValue').textContent, '', 'toolbar=no,titlebar=no,left=300,top=450,width=900,height=600,scrollbars=yes,resizable=yes')
            }

            return false;
        }
        function SendEmail(a, b) {
            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenAtch(a, b, c, d) {
            window.open("../opportunity/frmBizDocAttachments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&BizDocID=" + document.Form1.txtBizDoc.value + "&TempID=" + document.Form1.ddlBizDocTemplate.value + "&E=2&OpID=" + a + "&OppBizId=" + b + "&DomainID=" + c + "&ConID=" + c, "", "width=800,height=400,status=no,scrollbars=yes,left=155,top=160");
            return false;
        }
        function openeditAddress(a, b, ReturnID) {
            window.open('../opportunity/frmEditOppAddress.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ReturnID=' + ReturnID + '&AddType=' + a + '&OppID=' + b, '', 'toolbar=no,titlebar=no,left=100,top=350,width=500,height=350,scrollbars=yes,resizable=yes')
            return false;
        }
        function Openfooter(a) {
            window.open('../opportunity/frmFooterUpload.aspx?OppType=' + a, '', 'toolbar=no,titlebar=no,top=300,width=400,height=100,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenExport(a, b) {
            window.open('../opportunity/frmBizDocExport.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppBizId=' + a + '&OpID=' + b, '', 'toolbar=no,titlebar=no,top=300,width=400,height=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenEdit(a, b, c) {
            var strURL = '../opportunity/frmAddBizDocs.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b + '&OppType=' + c;
            if (opener.location.href.toLowerCase().indexOf('frmmenu') > 0) {
                opener.top.frames['mainframe'].location.href = strURL
            } else {
                opener.location.href = strURL
            }
            window.close();
            return false;
        }
        function ChangeDate(a, b) {
            window.open('../opportunity/frmChangeDueDate.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppBizId=' + a + '&date=' + b, '', 'toolbar=no,titlebar=no,top=300,width=240,height=250,scrollbars=yes,resizable=yes');
            return false;
        }
        function GrabHTML() {
            document.getElementById('hdnBizDocHTML').value = document.getElementById("pnlBizDoc").innerHTML.replace(/https/gi, "http"); //case in-sensetive replace
            return true;
        }

    </script>
</head>
<body>
    <form id="Form1" method="post" runat="server">
        <br />
        <div id="resizeDiv" style="float: left; margin-top: 5px; margin-left: 5px; margin-right: 5px;">
            <table width="100%" border="0" cellpadding="0" id="tblApproval" runat="server">
                <tr>
                    <td valign="top">
                        <table width="100%">
                            <tr class="normal1">
                                <td>
                                    <font color="#999999"><i>Created By &nbsp;</i></font>
                                </td>
                                <td>
                                    <font color="#999999"><i>
                                        <asp:Label ID="lblcreated" runat="server"></asp:Label></i></font>
                                </td>
                            </tr>
                            <tr class="normal1">
                                <td>
                                    <font color="#999999"><i>Last Modified By &nbsp;</i></font>
                                </td>
                                <td>
                                    <font color="#999999"><i>
                                        <asp:Label ID="lblModifiedby" runat="server"></asp:Label></i></font>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td valign="top"></td>
                </tr>
            </table>
            <asp:Panel ID="pnlBizDoc" runat="server">
                <link rel="stylesheet" href="../CSS/master.css" type="text/css" />
                <asp:Table ID="tblOriginalBizDoc" BorderWidth="1" runat="server" Width="100%" BorderColor="black"
                    GridLines="None">
                    <asp:TableRow>
                        <asp:TableCell>
                            <style type="text/css">
                                .title
                                {
                                    color: #696969; /*padding:10px;*/
                                    font: bold 30px Arial, Helvetica, sans-serif;
                                }

                                .RowHeader
                                {
                                    background-color: #dedede;
                                    color: #333;
                                    font-family: Arial;
                                    font-size: 8pt;
                                }

                                    .RowHeader .hyperlink
                                    {
                                        color: #333;
                                    }

                                .ItemHeader, .ItemStyle, .AltItemStyle
                                {
                                    border-width: 1px;
                                    padding: 8px;
                                    font: normal 12px/17px arial;
                                    border-style: solid;
                                    border-color: #666666;
                                    background-color: #dedede;
                                }

                                .ItemHeader
                                {
                                    border-width: 1px;
                                    padding: 8px;
                                    border-style: solid;
                                    border-color: #666666;
                                }

                                .ItemStyle td
                                {
                                    border-width: 1px;
                                    padding: 8px;
                                    border-style: solid;
                                    border-color: #666666;
                                    background-color: #ffffff;
                                }

                                .AltItemStyle
                                {
                                    background-color: White;
                                    border-color: black;
                                    padding: 8px;
                                }

                                    .AltItemStyle td
                                    {
                                        padding: 8px;
                                    }

                                .ItemHeader td
                                {
                                    border-width: 1px;
                                    padding: 8px;
                                    font-weight: bold;
                                    border-style: solid;
                                    border-color: #666666;
                                    background-color: #dedede;
                                }

                                    .ItemHeader td th
                                    {
                                        border-width: 1px;
                                        padding: 8px;
                                        border-style: solid;
                                        border-color: #666666;
                                        background-color: #dedede;
                                    }

                                #tblBizDocSumm
                                {
                                    font: normal 12px verdana;
                                    color: #333;
                                    border-width: 1px;
                                    border-color: #666666;
                                    border-collapse: collapse;
                                    background-color: #dedede;
                                }

                                    #tblBizDocSumm td
                                    {
                                        border-width: 1px;
                                        padding: 8px;
                                        border-style: solid;
                                        border-color: #666666;
                                    }

                                .WordWrapSerialNo
                                {
                                    width: 30%;
                                    word-break: break-all;
                                }
                                    .TwoColumns
                                {
                                    width: 85%;
                                    word-break: break-all;
                                }
                                   .ThreeColumns
                                {
                                    width: 50%;
                                    word-break: break-all;
                                }
                                 .OneColumns
                                {
                                    width: 99%;
                                    word-break: break-all;
                                }
                            </style>
                            <table width="100%" border="0" cellpadding="0">
                                <tr>
                                    <td valign="top" align="left">
                                        <asp:Image ID="imgLogo" runat="server"></asp:Image>
                                    </td>
                                    <td align="right" nowrap >
                                        <asp:Label ID="lblOrganizationName" runat="server"></asp:Label>&nbsp;&nbsp;<asp:Label
                                            ID="lblOrganizationContactName" runat="server"></asp:Label>
                                    </td>
                                    <td align="right" nowrap class="title">
                                        <asp:Label ID="lblBizDoc" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" border="0" cellpadding="0">
                                <tr>
                                    <td align="right">
                                        <table width="100%">
                                            <tr>
                                                <td class="RowHeader">Discount
                                                </td>
                                                <td class="RowHeader">Billing Terms
                                                </td>
                                                <td class="RowHeader">
                                                    <a id="hplDueDate" runat="server" class="hyperlink" href="#">Due Date</a>
                                                </td>
                                                <td class="RowHeader">Date Created
                                                </td>
                                                <td class="RowHeader">
                                                    <asp:Label runat="server" ID="lblBizDocIDLabel" Text="ID#"></asp:Label>
                                                    <%--<font color="white">Invoice#</font>--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1">
                                                    <asp:Label ID="lblDiscount" runat="server"></asp:Label>
                                                </td>
                                                <td class="normal1">
                                                    <asp:Label ID="lblBillingTerms" runat="server"></asp:Label>
                                                    <asp:Label ID="lblBillingTermsName" runat="server"></asp:Label>
                                                </td>
                                                <td class="normal1">
                                                    <asp:Label ID="lblDuedate" runat="server"></asp:Label>
                                                </td>
                                                <td class="normal1">
                                                    <asp:Label ID="lblDate" runat="server"></asp:Label>
                                                </td>
                                                <td class="normal1">
                                                    <asp:Label ID="lblBizDocIDValue" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="normal1">
                                    <td colspan="5">
                                        <table height="100%" width="100%">
                                            <tr>
                                                <td class="RowHeader">
                                                    <a id="hplBillto" runat="server" class="hyperlink">Bill To</a>
                                                </td>
                                                <td class="RowHeader">
                                                    <a id="hplShipTo" runat="server" class="hyperlink">Ship To</a>
                                                </td>
                                                <td class="RowHeader">Status
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="normal1">
                                                    <asp:Label ID="lblBillToAddressName" runat="server" Visible="false"></asp:Label>
                                                    <asp:Label ID="lblBillToCompanyName" runat="server"></asp:Label>
                                                    <asp:Label ID="lblBillTo" runat="server"></asp:Label>
                                                </td>
                                                <td class="normal1">
                                                    <asp:Label ID="lblShipToAddressName" runat="server" Visible="false"></asp:Label>
                                                    <asp:Label ID="lblShipToCompanyName" runat="server"></asp:Label>
                                                    <asp:Label ID="lblShipTo" runat="server"></asp:Label>
                                                </td>
                                                <td>
                                                    <table height="100%" width="100%">
                                                        <tr>
                                                            <td colspan="2" class="normal1">
                                                                <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1">
                                                                <asp:Label ID="lblAmountPaidTitle" runat="server" Text="Amount Paid:"></asp:Label>
                                                            </td>
                                                            <td class="normal1">
                                                                <asp:Label ID="lblAmountPaidCurrency" runat="server" Text=""></asp:Label>&nbsp;<asp:Label
                                                                    ID="lblAmountPaid" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="normal1">Balance Due:
                                                            </td>
                                                            <td class="normal1">
                                                                <asp:Label ID="lblBalanceDueCurrency" runat="server" Text=""></asp:Label>&nbsp;<asp:Label
                                                                    ID="lblBalance" runat="server"></asp:Label>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                                <tr class="normal1">
                                    <td colspan="5">
                                        <table runat="server" id="vendorAddress" height="100%" width="100%">
                                            <tr>
                                                <td class="RowHeader">
                                                    
                                                    <a id="hplCusVenBillto" runat="server" class="hyperlink"> 
                                                        <asp:Label ID="lblVendorBillAddHeader" runat="server"></asp:Label>

                                                    </a>
                                                </td>
                                                <td class="RowHeader">
                                                    <a id="hplCusVenShipTo" runat="server" class="hyperlink">

                                                         <asp:Label ID="lblVendorShipAddHeader" runat="server"></asp:Label>

                                                    </a>
                                                </td>
                                              
                                            </tr>
                                            <tr>
                                                <td class="normal1">
                                                    <asp:Label ID="lblCusVenBillToAddressName" runat="server"></asp:Label>
                                                    <asp:Label ID="lblCusVenBillToCompanyName" runat="server"></asp:Label>
                                                    <asp:Label ID="lblCusVenBillTo" runat="server"></asp:Label>
                                                </td>
                                                <td class="normal1">
                                                    <asp:Label ID="lblCusVenShipToAddressName" runat="server" Visible="false"></asp:Label>
                                                    <asp:Label ID="lblCusVenShipToCompanyName" runat="server"></asp:Label>
                                                    <asp:Label ID="lblCusVenShipTo" runat="server"></asp:Label>
                                                </td>
                                              
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr class="normal1">
                                    <td colspan="5">
                                        <table height="100%" width="100%">
                                            <tr>
                                                <td class="RowHeader">
                                                    <asp:Label ID="lblPONo" runat="server"></asp:Label>&nbsp;#
                                                </td>
                                                <td class="RowHeader">Deal or Order ID
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lblNo" runat="server"></asp:Label>
                                                </td>
                                                <td class="normal1">
                                                    <asp:HyperLink ID="hplOppID" runat="server" CssClass="hyperlink"> </asp:HyperLink>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RowHeader" colspan="5">Tracking Numbers
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RowHeader" colspan="5">
                                        <asp:Label ID="lblTrackingNumbers" runat="server"></asp:Label>
                                        <%--<asp:HyperLink ID="hplTrackingNumbers" runat="server" Target="_blank" />--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="RowHeader" colspan="5">Comments
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="5" align="left" valign="top">
                                        <asp:Label ID="lblComments" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr class="normal1">
                                    <td colspan="5">
                                        <asp:DataGrid ID="dgBizDocs" runat="server" ForeColor="" Font-Size="10px" Width="100%"
                                            BorderStyle="None" BackColor="White" GridLines="Vertical" HorizontalAlign="Center"
                                            AutoGenerateColumns="False">
                                            <AlternatingItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="AltItemStyle"></AlternatingItemStyle>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="ItemStyle"></ItemStyle>
                                            <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="ItemHeader"></HeaderStyle>
                                            <Columns>
                                            </Columns>
                                        </asp:DataGrid>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%">
                                <tr>
                                    <td align="right">
                                        <table id="tblBizDocSumm" runat="server" border="0">
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <asp:Image ID="imgFooter" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table ID="tblFormattedBizDoc" Visible="false" BorderWidth="1" runat="server"
                    Width="100%" BorderColor="black" GridLines="None">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Literal ID="litBizDocTemplate" runat="server"></asp:Literal>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:Panel>
            <table id="tblButtons" width="100%" runat="server">
                <tr>
                    <td align="left" class="normal1">
                        <asp:ImageButton runat="server" ID="ibtnSendEmail" AlternateText="Email as PDF" ToolTip="Email as PDF"
                            ImageUrl="~/images/Email.png" OnClientClick="GrabHTML();" />&nbsp;
                    <asp:ImageButton runat="server" ID="ibtnExport" AlternateText="Export to Excel" ToolTip="Export to Excel"
                        ImageUrl="~/images/Excel.png" />&nbsp;
                    <asp:ImageButton runat="server" ID="ibtnExportPDF" AlternateText="Export to PDF"
                        ToolTip="Export to PDF" ImageUrl="~/images/pdf.png" OnClientClick="GrabHTML();" />&nbsp;
                    <asp:ImageButton runat="server" ID="ibtnPrint" AlternateText="Print" ToolTip="Print"
                        ImageUrl="~/images/Print.png" />&nbsp;
                    <asp:ImageButton runat="server" ID="ibtnLogo" AlternateText="Add Logo to be shown on BizDoc"
                        ToolTip="Add Logo to be shown on BizDoc" ImageUrl="~/images/image_add.png"  Visible="false"/>&nbsp;
                    <asp:ImageButton runat="server" ID="ibtnFooter" AlternateText="Add footer image"
                        ToolTip="Add footer image" ImageUrl="~/images/image_add.png" Visible="false"/>&nbsp;
                    </td>
                    <td align="right" class="normal1"></td>
                </tr>
                <tr>
                    <td colspan="2" width="100%">
                        <fieldset>
                            <table>
                                <tr>
                                    <td class="normal1" align="right">BizDoc Template
                                    </td>
                                    <td colspan="2">
                                        <asp:DropDownList ID="ddlBizDocTemplate" runat="server" CssClass="signup" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <uc1:PaymentHistory ID="PaymentHistory1" runat="server" />
                    </td>
                </tr>
                <%-- <tr id="trPaymentsHistory" runat="server">
                <td colspan="2" width="100%">
                    <fieldset>
                        <table width="100%">
                            <tr>
                                <td>
                                    <b>Payments History</b>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:DataGrid ID="dgPaymentDetails" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False">
                                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                        <ItemStyle CssClass="is"></ItemStyle>
                                        <HeaderStyle CssClass="hs"></HeaderStyle>
                                        <Columns>
                                            <asp:TemplateColumn HeaderText="Date">
                                                <ItemTemplate>
                                                    <a href="javascript:void(0)" onclick="return OpenPaymentAmtPaid('<%# Eval("numDepositID") %>');">
                                                        <%# Eval("dtCreatedDate")%>
                                                    </a>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Payment Amount">
                                                <ItemTemplate>
                                                    <%# ReturnMoney(Eval("monDepositAmount"))%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                            <asp:TemplateColumn HeaderText="Amount Applied">
                                                <ItemTemplate>
                                                    <%# ReturnMoney(Eval("monAmountPaidInDeposite"))%>
                                                </ItemTemplate>
                                            </asp:TemplateColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                        </table>
                    </fieldset>
                </td>
            </tr>--%>
            </table>
            <br />
            <asp:Panel ID="pnlApprove" runat="server">
                <table width="100%">
                    <tr>
                        <td class="normal1" align="right">
                            <%--  Comments&nbsp;&nbsp;--%>
                        </td>
                        <td class="normal1" align="left">
                            <%--  <asp:TextBox runat="server" ID="txtComment" CssClass="signup" Width="340" TextMode="MultiLine"></asp:TextBox>--%>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal4" align="right">
                            <%--  Document is Ready for Approval&nbsp;&nbsp;--%>
                        </td>
                        <td class="normal4" align="left">
                            <%--  <asp:Button ID="btnApprove" runat="server" CssClass="button" Text="Approve" />
                        <asp:Button ID="btnDecline" runat="server" CssClass="button" Text="Decline" />--%>
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <table width="100%">
                <tr>
                    <td class="normal4" align="center">
                        <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                    </td>
                </tr>
            </table>
            <asp:TextBox ID="txtBizDoc" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtConEmail" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtOppOwner" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtCompName" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtConID" Style="display: none" runat="server"></asp:TextBox>
            <asp:TextBox ID="txtBizDocRecOwner" Style="display: none" runat="server"></asp:TextBox>
            <input id="hdSubTotal" runat="server" type="hidden" />
            <input id="hdShipAmt" runat="server" type="hidden" />
            <input id="hdTaxAmt" runat="server" type="hidden" />
            <input id="hdCRVTxtAmt" runat="server" type="hidden" />
            <input id="hdDisc" runat="server" type="hidden" />
            <input id="hdLateCharge" runat="server" type="hidden" />
            <input id="hdGrandTotal" runat="server" type="hidden" />
            <input id="hdnBalance" runat="server" type="hidden" />
            <input id="hdnCreditAmount" runat="server" type="hidden" />
            <asp:Button ID="btnTemp_DontDelete" runat="server" Style="display: none" />
            <asp:HiddenField runat="server" ID="hdnBizDocHTML" />
            <input id="hdnOppType" runat="server" type="hidden" />
            <input id="hdnBizDocId" runat="server" type="hidden" />
            <input id="hdnDivID" runat="server" type="hidden" />
        </div>
    </form>
    <script type="text/javascript">
        windowResize();

        function windowResize() {
            var width = document.getElementById("resizeDiv").offsetWidth;
            var height = document.getElementById("resizeDiv").offsetHeight;
            window.resizeTo(width + 45, height + 150);
        }
    </script>
</body>
</html>
