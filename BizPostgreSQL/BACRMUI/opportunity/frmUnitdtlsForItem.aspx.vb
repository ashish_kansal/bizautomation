Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Opportunities
    Public Class frmUnitdtlsForItem
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents dgUnits As System.Web.UI.WebControls.DataGrid

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                Session("Help") = Request.Url.Segments(Request.Url.Segments.Length - 1)
                Dim objItems As New CItems
                Dim dtItemDetails As DataTable
                objItems.ItemCode = GetQueryStringVal(Request.QueryString("enc"), "ItemCode")
                dtItemDetails = objItems.ItemDetails
                dgUnits.DataSource = dtItemDetails
                dgUnits.DataBind()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
