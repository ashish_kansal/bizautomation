﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSalesTemplate.aspx.vb"
    Inherits=".frmSalesTemplate" MasterPageFile="~/common/Popup.Master" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Create Item Templates</title>
    <script type='text/javascript' src="../JavaScript/select2.js"></script>
    <link rel="stylesheet" href="../CSS/select2.css" />
    <style type="text/css">
        .hidden {
            display: none;
        }

        .select2-drop {
            width: 700px !important;
        }
    </style>
    <script type="text/javascript">
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }

        /*Function added by Neelam on 03/14/2018 - Added functionality to open Edit window*/
        //function OpenEditWindow(numDivisionID, templatename, numTemplateId) {
        <%--function OpenEditWindow() {
            debugger;
            //$("#hdnDivisionID").val(numDivisionID);
            //$("#hdnTemplateId").val(numTemplateId);
            //$('#' + '<%=txtEditTemplateName.ClientID %>').val(templatename);
            $find("<%= rwEditTemplate.ClientID%>").show();
            //InitialiseItemTextBox();
            //debugger;
            //__doPostBack('btnBindItems', '');
        }--%>

        /*Function added by Neelam on 03/14/2018 - Added functionality to close Edit window*/
        function CloseEditWindow() {
            var radwindow = $find('<%=rwEditTemplate.ClientID %>');
            radwindow.close();
        }

        //$(function () {
        function InitialiseItemTextBox() {
            //var prm = Sys.WebForms.PageRequestManager.getInstance();
            //prm.add_pageLoaded(pageLoaded);

            /*$("#btnCustomerCollapseButton").click(function () {
                $("#CustomerSection").removeAttr("style");
            })
            $("#btnOrderCollapseButton").click(function () {
                $("#OrderSection").removeAttr("style");
            })
            $("#btnItemCollapseButton").click(function () {
                $("#ItemSection").removeAttr("style");
            })
            $("#btnPaymentCollapseButton").click(function () {
                $("#PaymentsSection").removeAttr("style");
            })
            $(".btnBillme").click(function () {
                var totalAmount = $("#lblTotal").text(); lblTotal
                var RemainingCredit = $("#hdnRemaningCredit").val();
                if (parseFloat(totalAmount) > parseFloat(RemainingCredit)) {
                    alert("Your credit limit balance is not sufficient to place the order");
                    return false;
                }
            })*/
            var columns;
            var userDefaultPageSize = '<%= Session("PagingRows")%>';
            var userDefaultCharSearch = '<%= Session("ChrForItemSearch") %>';
            var varPageSize = parseInt(userDefaultPageSize, 10);
            var varCharSearch = 1;
            if (parseInt(userDefaultCharSearch, 10) > 0) {
                varCharSearch = parseInt(userDefaultCharSearch, 10);
            }

            $.ajax({
                type: "POST",
                url: '../common/Common.asmx/GetSearchedItems',
                data: '{ searchText: "abcxyz", pageIndex: 1, pageSize: 10, divisionId: 0, isGetDefaultColumn: true, warehouseID: 0, searchOrdCusHistory: 0, oppType: 1, isTransferTo: false,IsCustomerPartSearch:false,searchType:"1"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    // Replace the div's content with the page method's return.
                    if (data.hasOwnProperty("d"))
                        columns = $.parseJSON(data.d)
                    else
                        columns = $.parseJSON(data);
                },
                results: function (data) {
                    columns = $.parseJSON(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert(textStatus);
                }
            });

            function formatItem(row) {
                var numOfColumns = 0;
                var ui = "<table style=\"width: 100%;font-size:12px;\" cellpadding=\"0\" cellspacing=\"0\">";
                ui = ui + "<tbody>";
                ui = ui + "<tr>";
                ui = ui + "<td style=\"width:auto; padding: 3px; vertical-align:top; text-align:center\">";
                if (row["vcPathForTImage"] != null && row["vcPathForTImage"].indexOf('.') > -1) {
                    ui = ui + "<img id=\"Img7\" height=\"45\" width=\"45\" title=\"Item Image\" src=\"" + row["vcPathForTImage"] + "\" alt=\"Item Image\" >";
                } else {
                    ui = ui + "<img id=\"Img7\" height=\"45\" width=\"45\" title=\"Item Image\" src=\"" + "../images/icons/cart_large.png" + "\" alt=\"Item Image\" >";
                }

                ui = ui + "</td>";
                ui = ui + "<td style=\"width: 100%; vertical-align:top\">";
                ui = ui + "<table style=\"width: 100%;\" cellpadding=\"0\" cellspacing=\"0\"  >";
                ui = ui + "<tbody>";
                ui = ui + "<tr>";
                $.each(columns, function (index, column) {
                    if (numOfColumns == 4) {
                        ui = ui + "</tr><tr>";
                        numOfColumns = 0;
                    }

                    if (numOfColumns == 0) {
                        ui = ui + "<td style=\"white-space: nowrap; margin-left:10px\"><strong>" + column.vcFieldName + ":</strong></td><td style=\"width:80%\">" + row[column.vcDbColumnName] + "</td>";
                    }
                    else {
                        ui = ui + "<td style=\"white-space: nowrap; margin-left:10px\"><strong>" + column.vcFieldName + ":</strong></td><td style=\"width:20%\">" + row[column.vcDbColumnName] + "</td>";
                    }
                    numOfColumns += 2;
                });
                ui = ui + "</tr>";
                ui = ui + "</tbody>";
                ui = ui + "</table>";
                ui = ui + "</td>";
                ui = ui + "</tr>";
                ui = ui + "</tbody>";
                ui = ui + "</table>";

                return ui;
            }

            function getDivisionId() {
                //$("#hdnDivisionID").val(numDivisionID);
                var divisionID = $("#hdnDivisionID").val();
                if (divisionID != null && divisionID.length > 0) {
                    return divisionID;
                }
                else {
                    return 0;
                }

                /*if (radCmbCompany != null) {
                    if (radCmbCompany.get_value() != null && radCmbCompany.get_value().length > 0) {
                        return radCmbCompany.get_value();
                    }
                    else {
                        return 0;
                    }
                }
                else {
                    return 0;
                }*/
            }

            function getvalue() {
                var combo = $find('<%=radcmbLocation.ClientID%>');
                if (combo != null) {
                    return combo.get_selectedItem().get_value();
                }
                else {
                    return 0;
                }
            }

            $(document).bind('keypress', function (event) {

                if (event.which === 9 && event.ctrlKey) {
                    //$("#ItemSection").css("display", "block");
                    $("#txtItem").select2("open");
                    $("#txtItem").focus();
                }
                //B
                /*if (event.which === 2 && event.ctrlKey) {
                    $("#CustomerSection").css("display", "block");*/
                    <%--var comboBox = $find("<%=radCmbCompany.ClientID %>");--%>
                /*var input = comboBox.get_inputDomElement();
                input.focus();
            }*/
                if (event.which === 13) {
                    var obj = $('#txtItem').select2('data');

                    //if (obj != null && $("#txtHidEditOppItem").val() != "") {
                    //    //$("[id$=btnUpdate]").click();
                    //}
                }
            });

            $('#txtItem').on("change", function (e) {
                /*if ($("#divNewCustomer") == null && radCmbCompany.get_value() == null && radCmbCompany.get_value().length == 0) {
                    alert("Select customer");
                    return false;
                }*/
                if (e.added != null) {
                    $("#hdnCurrentSelectedItem").val(e.added.id.toString().split("-")[0]);
                    $("#hdnHasKitAsChild").val(e.added.bitHasKitAsChild);
                    $("#hdnIsSKUMatch").val(Boolean(e.added.bitSKUMatch));
                    //if (e.added.bitHasKitAsChild) {
                    //    Scroll(false);
                    //}
                    //document.getElementById("lkbItemSelected").click();
                }
            })

            $('#txtItem').on("select2-removed", function (e) {
                <%--var isVisible = "<%=plhItemDetails.Visible%>";
                if (isVisible == "True") {--%>
                $("#hdnCurrentSelectedItem").val("");
                $("#hdnHasKitAsChild").val("");
                //    document.getElementById("lkbItemRemoved").click();
                //}
            })

            $('#txtItem').select2(
                {
                    placeholder: 'Select Items',
                    minimumInputLength: varCharSearch,
                    multiple: false,
                    formatResult: formatItem,
                    width: "90%",
                    dropdownCssClass: 'bigdrop',
                    dataType: "json",
                    allowClear: true,
                    ajax: {
                        quietMillis: 500,
                        url: '../common/Common.asmx/GetSearchedItems',
                        type: 'POST',
                        params: {
                            contentType: 'application/json; charset=utf-8'
                        },
                        dataType: 'json',
                        data: function (term, page) {
                            return JSON.stringify({
                                searchText: term,
                                pageIndex: page,
                                pageSize: varPageSize,
                                divisionId: getDivisionId(),
                                isGetDefaultColumn: false,
                                warehouseID: getvalue(),
                                searchOrdCusHistory: 0,
                                oppType: 1,
                                isTransferTo: false,
                                IsCustomerPartSearch: false,
                                searchType: "1"
                            });
                        },
                        results: function (data, page) {

                            if (data.hasOwnProperty("d")) {
                                if (data.d == "Session Expired") {
                                    alert("Session expired.");
                                    window.opener.location.href = window.opener.location.href;
                                    window.close();
                                } else {
                                    data = $.parseJSON(data.d)
                                }
                            }
                            else
                                data = $.parseJSON(data);

                            var more = (page.page * varPageSize) < data.Total;
                            return { results: $.parseJSON(data.results), more: more };
                        }
                    }
                });

            var cancelDropDownClosing = false;
            /*$('[id$=raditems] .rcbList li.rcbTemplate').click(function (e) {
                e.cancelBubble = true;
                if (e.stopPropagation) {
                    e.stopPropagation();
                }
            });*/
            StopPropagation = function (e) {
                e.cancelBubble = true;
                if (e.stopPropagation) {
                    e.stopPropagation();
                }
            }
            onDropDownClosing = function (sender, args) {
                cancelDropDownClosing = false;
            }
            onCheckBoxClick = function (chk, comboBoxID) {
                //Prevent second RadComboBox from closing.
                cancelDropDownClosing = true;
                var text = "";
                var values = "";
                var combo = $find(comboBoxID);
                var items = combo.get_items();
                for (var i = 0; i < items.get_count(); i++) {
                    var item = items.getItem(i);
                    var chk1 = $get(combo.get_id() + "_i" + i + "_chkSelect");
                    if (chk1.checked) {
                        text += item.get_text() + ",";
                    }
                }
                text = removeLastComma(text);
                if (text.length > 0) {
                    combo.set_text(text);
                }
                else {
                    combo.set_text("");
                }
            }
        }
        //});

        function SetItemAttributes(itemcode, numWarehouseItemID, vcAttributes, vcAttributeIDs) {
            var item = $('#txtItem').select2('data');
            item.numItemCode = itemcode;
            item.numWareHouseItemID = numWarehouseItemID;
            item.bitHasKitAsChild = Boolean(false);
            item.kitChildItems = "";
            item.Attributes = vcAttributes;
            item.AttributeIDs = vcAttributeIDs;
            $('#txtItem').select2("data", item);
            $('#hdnSelectedItems').val(JSON.stringify($('#txtItem').select2('data')));
        }

        function GetSelectedItems() {

            if ($('#' + '<%=txtEditTemplateName.ClientID %>').val == '') {
                alert("Enter template name.")
                $('#' + '<%=txtEditTemplateName.ClientID %>').focus();
                return false;
            }
            var obj = $('#txtItem').select2('data');
            if (obj == null) {
                alert("Please select item.");
                return false;
            }
            else {
                if (obj == null) {
                    return;
                }
                var item = new Object();
                item.numItemCode = obj.numItemCode;
                item.numWareHouseItemID = obj.numWareHouseItemID;
                item.bitHasKitAsChild = Boolean(obj.bitHasKitAsChild);
                item.kitChildItems = obj.childKitItems;
                item.numQuantity = obj.numQuantity || 1;
                item.vcAttributes = obj.Attributes || "";
                item.vcAttributeIDs = obj.AttributeIDs || "";
                $("#hdnSelectedItems").val(JSON.stringify(item));
                $("[id$=btnSave]").click();
                /*if (isFromAddButton) {
                    $("[id$=btnUpdate]").click();
                } else {
                    return true;
                }*/
            }
        }

        function Validate() {
            if (document.getElementById("txtTemplateName").value == '') {
                alert("Enter template name.")
                document.getElementById("txtTemplateName").focus();
                return false;
            }
            else { return true; }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table>
                <tr>
                    <td colspan="2" align="right">
                        <%--Commented by Neelam - Mar 13,2017 - Obsolete
                        <asp:Button ID="btnShowGeneric" runat="server" CssClass="button" Text="Show Generic Templates" />--%>
                        <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="Close();" Text="Close" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Item Templates
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table align="center" width="1000px" border="0">
        <tr>
            <td colspan="3">
                <table width="50%" align="right" border="0">
                    <tr align="right">
                        <td class="normal1" align="right">Template Name
                        </td>
                        <td align="left">
                            <asp:TextBox runat="server" ID="txtTemplateName" CssClass="signup" Width="200px"></asp:TextBox>
                        </td>
                        <td>
                            <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-primary" Text="Add" OnClientClick="return Validate();" />
                            <asp:Label ID="Label1" Text="[?]" CssClass="tip" runat="server" ToolTip="Item Templates (Sometimes called “Wishlists” in e-commerce) are used to reduce the time it takes to build new Opportunities & Orders, since adding items is often the most time consuming portion of the process. 

To create one, simply populate any opportunity or order with items, and click the “Items Templates” function button. 

They can be “Generic” or “Specific”. Generic item templates mean that items appear within the Template drop down in the new Sales / Purchase Opportunity / Order screen, regardless of which organization it belongs to. “Specific” templates however, only appear for the Organization from the Sales / Purchase Opportunity / Order from which the template was created.
"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1" align="right">Where should template be applied ?
                        </td>
                        <td align="left" colspan="2">
                            <asp:DropDownList ID="ddlTemplateApplied" runat="server" CssClass="form-control" Width="200px">
                                <asp:ListItem Text="All Orders & Opportunities" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Sales Opportunities" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Sales Orders" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Sales Opportunities & Orders" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Purchase Opportunities" Value="4"></asp:ListItem>
                                <asp:ListItem Text="Purchase Orders" Value="5"></asp:ListItem>
                                <asp:ListItem Text="Purchase Opportunities & Orders" Value="6"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <%--<td class="normal1" align="right" width="200">
                <label for="chkMakeGeneric">
                    Make this a Generic Template</label><br />
                <small>(only shows up BEFORE an Organization is selected within the New Order form)</small>
            </td>--%>
                        <td colspan="2" align="right">
                            <asp:CheckBox runat="server" ID="chkMakeGeneric" CssClass="signup" Text="Make this template generic" Width="200px"></asp:CheckBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr runat="server" id="trLabelSpecificTemplates" visible="false">
            <td><b>Specific Templates</b></td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:DataGrid runat="server" ID="dgSpecificTemplates" CssClass="dg" Width="100%" AutoGenerateColumns="false">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="numSalesTemplateID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Template Name" DataField="vcTemplateName"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Created By" DataField="CreatedBy"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Organization" DataField="OrganizationName"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Applies to" DataField="tintAppliesTo"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderStyle-Width="30">
                            <ItemTemplate>
                                <%--<asp:LinkButton ID="btnEdit" OnClientClick=<%#String.Format("OpenEditWindow('{0}','{1}','{2}');", DataBinder.Eval(Container.DataItem, "numDivisionID"), DataBinder.Eval(Container.DataItem, "vcTemplateName"), DataBinder.Eval(Container.DataItem, "numSalesTemplateID")) %> CssClass="hyperlink" runat="server">--%>
                                <asp:LinkButton ID="btnEdit" CommandName="Edit" CssClass="hyperlink" runat="server">
                                    <asp:Image ID="Image3" runat="server" ImageUrl="~/images/pencil-24.gif" Height="15" />
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnDelete" CommandName="Delete" CssClass="hyperlink" runat="server">
                                    <asp:Image ID="Image4" runat="server" ImageUrl="~/images/delete2.gif" Height="15" />
                                </asp:LinkButton>
                                <asp:HiddenField ID="hdnNumDivisionID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.numDivisionID")%>' />
                                <asp:HiddenField ID="hdnNumSalesTemplateID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.numSalesTemplateID")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
        <tr runat="server" id="trLabelGenericTemplates" visible="false">
            <td><b>Generic Templates</b></td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:DataGrid ID="dgGenericTemplates" runat="server" CssClass="dg" Width="100%"
                    AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="numSalesTemplateID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Template Name" DataField="vcTemplateName"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Created By" DataField="CreatedBy"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Applies to" DataField="tintAppliesTo"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderStyle-Width="30">
                            <ItemTemplate>
                                <%--<asp:LinkButton ID="btnEdit" OnClientClick=<%#String.Format("OpenEditWindow('{0}','{1}','{2}');return false; ", DataBinder.Eval(Container.DataItem, "numDivisionID"), DataBinder.Eval(Container.DataItem, "vcTemplateName"), DataBinder.Eval(Container.DataItem, "numSalesTemplateID")) %> CssClass="hyperlink" runat="server">--%>
                                <asp:LinkButton ID="btnEditGT" CommandName="Edit" CssClass="hyperlink" runat="server">
                                    <asp:Image ID="Image3" runat="server" ImageUrl="~/images/pencil-24.gif" Height="15" />
                                </asp:LinkButton>
                                <asp:LinkButton ID="btnDelete" CommandName="Delete" CssClass="hyperlink" runat="server">
                                    <asp:Image ID="Image4" runat="server" ImageUrl="~/images/delete2.gif" Height="15" />
                                </asp:LinkButton>
                                <asp:HiddenField ID="hdnGTNumDivisionID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.numDivisionID")%>' />
                                <asp:HiddenField ID="hdnGTNumSalesTemplateID" runat="server" Value='<%# DataBinder.Eval(Container, "DataItem.numSalesTemplateID")%>' />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
        <%--<tr>
            <td colspan="2">
                <br />
                <asp:DataGrid ID="dgSalesTemplate" runat="server" CssClass="dg" Width="100%"
                    AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="numSalesTemplateID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Template Name" DataField="vcTemplateName"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Type" DataField="TemplateType"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Organization" DataField="OrganizationName"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
        <tr runat="server" id="trGeneric" visible="false">
            <td colspan="2">
                <b>Generic Sales Template</b>
                <br />
               <asp:DataGrid ID="dgGenericST" runat="server" CssClass="dg" Width="100%"
                    AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="numSalesTemplateID" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Template Name" DataField="vcTemplateName"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Type" DataField="TemplateType"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Organization" DataField="OrganizationName"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderStyle-Width="10" FooterStyle-Width="10" ItemStyle-Width="10">
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>--%>
    </table>
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>

    <telerik:RadWindow rendermode="Lightweight" runat="server" ID="rwEditTemplate" Title="Edit" RestrictionZoneID="ContentTemplateZone" Modal="true" Behaviors="Resize,Close,Move" Width="800" Height="300">
        <ContentTemplate>
            <asp:UpdatePanel ID="Updatepanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <table align="center" width="100%" border="0">
                        <tr>
                            <td class="normal1" align="right" style="width: 100px;">Template Name
                            </td>
                            <td style="width: 100px;">
                                <asp:TextBox runat="server" ID="txtEditTemplateName" CssClass="signup"></asp:TextBox>
                            </td>
                            <td class="normal1" align="right" style="width: 50px;">Item
                            </td>
                            <td>
                                <asp:TextBox ID="txtItem" ClientIDMode="Static" runat="server" Width="100%"></asp:TextBox>
                            </td>
                            <td style="width: 150px;">
                                <input type="button" runat="server" id="btnAddItem" onclick="return GetSelectedItems();" style="width: 30px;" class="OrderButton AddItem" />
                                <asp:Button ID="btnSaveClose" runat="server" CssClass="btn btn-primary" Text="Save & Close" OnClick="btnSaveClose_Click" />
                                <asp:Button ID="btnSave" runat="server" CssClass="hidden" OnClick="btnSave_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="5">
                                <asp:DataGrid ID="dgSalesTemplateItems" CssClass="dg" Width="100%" AutoGenerateColumns="False" runat="server">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn HeaderText="Item ID" DataField="numItemCode" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderText="Item Name" DataField="vcItemName" ItemStyle-Wrap="false" HeaderStyle-Wrap="false"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderText="SKU" DataField="vcSKU" ItemStyle-Wrap="false"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderText="Description" DataField="txtItemDesc" ItemStyle-Wrap="false"></asp:BoundColumn>
                                        <asp:TemplateColumn Visible="false">
                                            <ItemTemplate>
                                                <asp:Label Text='<%# Eval("numSalesTemplateItemID") %>' runat="server" ID="lblSalesTemplateItemID" />
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn ItemStyle-HorizontalAlign="Center" ItemStyle-Width="20">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkDelete" CommandName="Delete" CssClass="hyperlink" runat="server">
                                                    <asp:Image ID="Image4" runat="server" ImageUrl="~/images/delete2.gif" Height="15" />
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </telerik:RadWindow>

    <asp:UpdatePanel ID="UpdatePanelHiddenField" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:HiddenField ID="hdnSelectedItems" runat="server" />
            <asp:HiddenField ID="hdnCurrentSelectedItem" runat="server" />
            <asp:HiddenField ID="hdnDivisionID" runat="server" />
            <asp:HiddenField ID="hdnHasKitAsChild" runat="server" />
            <asp:HiddenField ID="hdnIsSKUMatch" runat="server" />
            <asp:HiddenField ID="hdnTemplateId" runat="server" />
            <telerik:RadComboBox ID="radcmbLocation" Width="100%" runat="server" AutoPostBack="true" Visible="false" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
