Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports Infragistics.WebUI.UltraWebGrid

Partial Public Class frmSelectSrlItemFrmInv
    Inherits System.Web.UI.Page

    Dim objCommon As New CCommon

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Session("Help") = Request.Url.Segments(Request.Url.Segments.Length - 1)
                Dim ds As DataSet
                Dim objItems As New CItems
                objItems.ItemCode = GetQueryStringVal(Request.QueryString("enc"), "ItemCode")
                objItems.WareHouseItemID = GetQueryStringVal(Request.QueryString("enc"), "WItemID")
                objItems.str = GetQueryStringVal(Request.QueryString("enc"), "Attr").TrimEnd
                ds = objItems.GetItemsOnAttributesAndWarehouse
                Dim dtAttributes As DataTable
                dtAttributes = ds.Tables(2)
                Dim dr As DataRow

                uwItemSel.DataSource = ds.Tables(1)
                uwItemSel.DataBind()
                uwItemSel.Columns(1).Hidden = True
                uwItemSel.Columns(2).Hidden = True
                uwItemSel.Columns(4).Hidden = True
                For Each dr In dtAttributes.Rows
                    If dr("fld_type") = "Drop Down List Box" Then
                        uwItemSel.Bands(0).Columns.FromKey(dr("Fld_label")).Type = ColumnType.DropDownList
                        Dim attVlist As New Infragistics.WebUI.UltraWebGrid.ValueList
                        Dim dtValues As DataTable
                        dtValues = objCommon.GetMasterListItems(dr("numlistid"), Session("DomainID"))
                        attVlist.ValueListItems.Add("0", "-")
                        Dim drValueList As DataRow
                        For Each drValueList In dtValues.Rows
                            attVlist.ValueListItems.Add(CStr(drValueList("numListItemID")), drValueList("vcData"))
                        Next
                        uwItemSel.Bands(0).Columns.FromKey(dr("Fld_label")).ValueList = attVlist
                    End If
                Next

                'For Each ugRow In uwItemSel.Rows
                '    If ugRow.Cells.FromKey("numWareHouseItemID").Value = e.Cell.Tag.Split("~")(1) Then
                '        If ugRow.HasChildRows = True Then
                '            ugRow.Expanded = True
                '        End If
                '        For Each ugRow1 In ugRow.Rows
                '            Dim dv1 As DataView
                '            dv1 = New DataView(dtSerItem)
                '            dv1.RowFilter = "numoppitemtCode=" & dv(i).Item("numoppitemtCode")
                '            For k = 0 To dv1.Count - 1
                '                If ugRow1.Cells.FromKey("numWareHouseItmsDTLID").Value = dv1(k).Item("numWarehouseItmsDTLID") Then
                '                    ugRow1.Cells.FromKey("Select").Value = True

                '                End If
                '            Next
                '        Next
                '    Else
                '        ugRow.Hidden = True
                '    End If
                'Next
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class