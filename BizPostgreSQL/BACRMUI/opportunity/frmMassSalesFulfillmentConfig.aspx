﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmMassSalesFulfillmentConfig.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmMassSalesFulfillmentConfig" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #min-grid-controls {
            margin: 0px;
        }

            #min-grid-controls ul.pagination {
                margin: 0px;
            }

            #min-grid-controls > li {
                padding: 0px;
                vertical-align: top;
            }

        #liRecordDisplay {
            line-height: 34px;
        }

        .modal-header {
            padding: 8px;
            background-color: #0d8ed2;
            color: #fff;
        }
    </style>
    <script type="text/javascript" src="../JavaScript/jquery.twbsPagination.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#divInvoicing").hide();

            $("[id$=ddlWhen]").change(function () {
                if ($(this).val() == "1") {
                } else {
                }
            });

            $("[id$=ddlShipMode]").change(function () {
                $("[id$=ddlOrderStatus]").val("0");
                $("[id$=ddlOrderStatus]").val($("[id$=hdnOrderStatusPacked" + $(this).val() + "]").val());
            });

            $("[id$=ddlOrderStatus]").change(function () {
                if ($("[id$=ddlWhen]").val() === "1") {
                    $("[id$=hdnOrderStatusPicked]").val($(this).val());
                } else if ($("[id$=ddlWhen]").val() === "2") {
                    $("[id$=hdnOrderStatusPacked" + $("[id$=ddlShipMode]").val() + "]").val($(this).val());
                } else if ($("[id$=ddlWhen]").val() === "3") {
                    $("[id$=hdnOrderStatusInvoiced]").val($(this).val());
                } else if ($("[id$=ddlWhen]").val() === "4") {
                    $("[id$=hdnOrderStatusPaid]").val($(this).val());
                } else if ($("[id$=ddlWhen]").val() === "5") {
                    $("[id$=hdnOrderStatusClosed]").val($(this).val());
                }
            });

            $("[id$=chkGroupByOrder]").change(function () {
                if ($(this).is(":checked")) {
                    if ($("[id$=ddlWhen]").val() == "1") {
                        $("[id$=hdnGroupByOrderForPick]").val("True");
                    } else if ($("[id$=ddlWhen]").val() == "2") {
                        $("[id$=hdnGroupByOrderForShip]").val("True");
                    } else if ($("[id$=ddlWhen]").val() == "3") {
                        $("[id$=hdnGroupByOrderForInvoice]").val("True");
                    }
                } else {
                    if ($("[id$=ddlWhen]").val() == "1") {
                        $("[id$=hdnGroupByOrderForPick]").val("False");
                    } else if ($("[id$=ddlWhen]").val() == "2") {
                        $("[id$=hdnGroupByOrderForShip]").val("False");
                    } else if ($("[id$=ddlWhen]").val() == "3") {
                        $("[id$=hdnGroupByOrderForInvoice]").val("False");
                    }
                }

                $("[id$=hdnGroupByOrderForPay]").val("True");
                $("[id$=hdnGroupByOrderForClose]").val("True");
                $("[id$=hdnGroupByOrderForPrintBizDoc]").val("True");
            });

            $("[id$=btnAddLeft]").click(function () {
                if ($("[id$=lbAvailableFieldLeft]").val() != null) {
                    var selectedItems = [];

                    if ($("[id$=ddlWhen]").val() == "1" && $("[id$=hdnReadytoPickLeft]").val() != "") {
                        selectedItems = $("[id$=hdnReadytoPickLeft]").val().split(",");
                    } else if ($("[id$=ddlWhen]").val() == "2" && $("[id$=hdnReadytoShip]").val() != "") {
                        selectedItems = $("[id$=hdnReadytoShip]").val().split(",");
                    } else if ($("[id$=ddlWhen]").val() == "3" && $("[id$=hdnReadytoInvoice]").val() != "") {
                        selectedItems = $("[id$=hdnReadytoInvoice]").val().split(",");
                    } else if ($("[id$=ddlWhen]").val() == "4" && $("[id$=hdnReadytoPay]").val() != "") {
                        selectedItems = $("[id$=hdnReadytoPay]").val().split(",");
                    } else if ($("[id$=ddlWhen]").val() == "5" && $("[id$=hdnPendingClose]").val() != "") {
                        selectedItems = $("[id$=hdnPendingClose]").val().split(",");
                    } else if ($("[id$=ddlWhen]").val() == "6" && $("[id$=hdnPrintBizDocs]").val() != "") {
                        selectedItems = $("[id$=hdnPrintBizDocs]").val().split(",");
                    }

                    if (jQuery.inArray($("[id$=lbAvailableFieldLeft]").val(), selectedItems) === -1) {
                        var $options = $("[id$=lbAvailableFieldLeft] > option:selected").clone();
                        $('[id$=lbSelectedFieldLeft]').append($options);

                        var selected = $("[id$=lbAvailableFieldLeft]").find(":selected");
                        var next = selected.next();
                        if (next.length > 0)
                            next.prop("selected", true)

                        var ddlvalues = $('[id$=lbSelectedFieldLeft] option').map(function () {
                            return $(this).val();
                        });

                        if ($("[id$=ddlWhen]").val() == "1") {
                            $("[id$=hdnReadytoPickLeft]").val(ddlvalues.get());
                        } else if ($("[id$=ddlWhen]").val() == "2") {
                            $("[id$=hdnReadytoShip]").val(ddlvalues.get());
                        } else if ($("[id$=ddlWhen]").val() == "3") {
                            $("[id$=hdnReadytoInvoice]").val(ddlvalues.get());
                        } else if ($("[id$=ddlWhen]").val() == "4") {
                            $("[id$=hdnReadytoPay]").val(ddlvalues.get());
                        } else if ($("[id$=ddlWhen]").val() == "5") {
                            $("[id$=hdnPendingClose]").val(ddlvalues.get());
                        } else if ($("[id$=ddlWhen]").val() == "6") {
                            $("[id$=hdnPrintBizDocs]").val(ddlvalues.get());
                        }
                    } else {
                        alert("Feild is alreay added");
                    }

                } else {
                    alert("Select atleast one field");
                }
            });

            $("[id$=btnRemoveLeft]").click(function () {
                if ($("[id$=lbSelectedFieldLeft]").val() != null) {
                    var selected = $("[id$=lbSelectedFieldLeft]").find(":selected");
                    var next = selected.next();
                    $("[id$=lbSelectedFieldLeft] > option:selected").remove();

                    if (next.length > 0)
                        next.prop("selected", true);

                    var ddlvalues = $('[id$=lbSelectedFieldLeft] option').map(function () {
                        return $(this).val();
                    });

                    if ($("[id$=ddlWhen]").val() == "1") {
                        $("[id$=hdnReadytoPickLeft]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "2") {
                        $("[id$=hdnReadytoShip]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "3") {
                        $("[id$=hdnReadytoInvoice]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "4") {
                        $("[id$=hdnReadytoPay]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "5") {
                        $("[id$=hdnPendingClose]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "6") {
                        $("[id$=hdnPrintBizDocs]").val(ddlvalues.get());
                    }
                } else {
                    alert("Select atleast one field");
                }
            });

            $("[id$=btnUpLeft]").click(function () {
                if ($("[id$=lbSelectedFieldLeft]").val() != null) {
                    var selected = $("[id$=lbSelectedFieldLeft]").find(":selected");
                    var before = selected.prev();
                    if (before.length > 0)
                        selected.detach().insertBefore(before);

                    var ddlvalues = $('[id$=lbSelectedFieldLeft] option').map(function () {
                        return $(this).val();
                    });

                    if ($("[id$=ddlWhen]").val() == "1") {
                        $("[id$=hdnReadytoPickLeft]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "2") {
                        $("[id$=hdnReadytoShip]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "3") {
                        $("[id$=hdnReadytoInvoice]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "4") {
                        $("[id$=hdnReadytoPay]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "5") {
                        $("[id$=hdnPendingClose]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "6") {
                        $("[id$=hdnPrintBizDocs]").val(ddlvalues.get());
                    }
                } else {
                    alert("Select atleast one field");
                }
            });

            $("[id$=btnDownLeft]").click(function () {
                if ($("[id$=lbSelectedFieldLeft]").val() != null) {
                    var selected = $("[id$=lbSelectedFieldLeft]").find(":selected");
                    var next = selected.next();
                    if (next.length > 0)
                        selected.detach().insertAfter(next);

                    var ddlvalues = $('[id$=lbSelectedFieldLeft] option').map(function () {
                        return $(this).val();
                    });

                    if ($("[id$=ddlWhen]").val() == "1") {
                        $("[id$=hdnReadytoPickLeft]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "2") {
                        $("[id$=hdnReadytoShip]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "3") {
                        $("[id$=hdnReadytoInvoice]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "4") {
                        $("[id$=hdnReadytoPay]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "5") {
                        $("[id$=hdnPendingClose]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "6") {
                        $("[id$=hdnPrintBizDocs]").val(ddlvalues.get());
                    }
                } else {
                    alert("Select atleast one field");
                }
            });

            $("[id$=btnAddRight]").click(function () {
                if ($("[id$=lbAvailableFieldRight]").val() != null) {
                    var selectedItems = [];

                    if ($("[id$=hdnReadytoPickRight]").val() != "") {
                        selectedItems = $("[id$=hdnReadytoPickRight]").val().split(",");
                    }

                    if (jQuery.inArray($("[id$=lbAvailableFieldRight]").val(), selectedItems) === -1) {
                        var $options = $("[id$=lbAvailableFieldRight] > option:selected").clone();
                        $('[id$=lbSelectedFieldRight]').append($options);

                        var selected = $("[id$=lbAvailableFieldRight]").find(":selected");
                        var next = selected.next();
                        if (next.length > 0)
                            next.prop("selected", true)

                        var ddlvalues = $('[id$=lbSelectedFieldRight] option').map(function () {
                            return $(this).val();
                        });

                        $("[id$=hdnReadytoPickRight]").val(ddlvalues.get());
                    } else {
                        alert("Feild is alreay added");
                    }

                } else {
                    alert("Select atleast one field");
                }
            });

            $("[id$=btnRemoveRight]").click(function () {
                if ($("[id$=lbSelectedFieldRight]").val() != null) {
                    var selected = $("[id$=lbSelectedFieldRight]").find(":selected");
                    var next = selected.next();
                    $("[id$=lbSelectedFieldRight] > option:selected").remove();

                    if (next.length > 0)
                        next.prop("selected", true);

                    var ddlvalues = $('[id$=lbSelectedFieldRight] option').map(function () {
                        return $(this).val();
                    });

                    $("[id$=hdnReadytoPickRight]").val(ddlvalues.get());
                } else {
                    alert("Select atleast one field");
                }
            });

            $("[id$=btnUpRight]").click(function () {
                if ($("[id$=lbSelectedFieldRight]").val() != null) {
                    var selected = $("[id$=lbSelectedFieldRight]").find(":selected");
                    var before = selected.prev();
                    if (before.length > 0)
                        selected.detach().insertBefore(before);

                    var ddlvalues = $('[id$=lbSelectedFieldRight] option').map(function () {
                        return $(this).val();
                    });

                    $("[id$=hdnReadytoPickRight]").val(ddlvalues.get());
                } else {
                    alert("Select atleast one field");
                }
            });

            $("[id$=btnDownRight]").click(function () {
                if ($("[id$=lbSelectedFieldRight]").val() != null) {
                    var selected = $("[id$=lbSelectedFieldRight]").find(":selected");
                    var next = selected.next();
                    if (next.length > 0)
                        selected.detach().insertAfter(next);

                    var ddlvalues = $('[id$=lbSelectedFieldRight] option').map(function () {
                        return $(this).val();
                    });

                    $("[id$=hdnReadytoPickRight]").val(ddlvalues.get());
                } else {
                    alert("Select atleast one field");
                }
            });

            $("[id$=btnAddWP]").click(function () {
                if ($("[id$=lbAvailableWP]").val() != null) {
                    var selectedItems = [];

                    $("[id$=lbSelectedWP]").children("option").each(function () {
                        selectedItems.push($(this).val());
                    });

                    if (jQuery.inArray($("[id$=lbAvailableWP]").val(), selectedItems) === -1) {
                        var $options = $("[id$=lbAvailableWP] > option:selected").clone();
                        $('[id$=lbSelectedWP]').append($options);

                        var selected = $("[id$=lbAvailableWP]").find(":selected");
                        var next = selected.next();
                        if (next.length > 0)
                            next.prop("selected", true)
                    } else {
                        alert("Feild is alreay added");
                    }
                } else {
                    alert("Select atleast one field");
                }
            });

            $("[id$=btnRemoveWP]").click(function () {
                if ($("[id$=lbSelectedWP]").val() != null) {
                    var selected = $("[id$=lbSelectedWP]").find(":selected");
                    var next = selected.next();
                    $("[id$=lbSelectedWP] > option:selected").remove();

                    if (next.length > 0)
                        next.prop("selected", true);
                } else {
                    alert("Select atleast one field");
                }
            });

            $("[id$=btnUPWP]").click(function () {
                if ($("[id$=lbSelectedWP]").val() != null) {
                    var selected = $("[id$=lbSelectedWP]").find(":selected");
                    var before = selected.prev();
                    if (before.length > 0)
                        selected.detach().insertBefore(before);
                } else {
                    alert("Select atleast one field");
                }
            });

            $("[id$=btnDownWP]").click(function () {
                if ($("[id$=lbSelectedWP]").val() != null) {
                    var selected = $("[id$=lbSelectedWP]").find(":selected");
                    var next = selected.next();
                    if (next.length > 0)
                        selected.detach().insertAfter(next);
                } else {
                    alert("Select atleast one field");
                }
            });

            $("[id$=ddlCountry]").change(function () {
                var combo = $find("<%= rcbState.ClientID%>");
                LoadStates(combo, parseInt($(this).val()), true);
            });

            $("#ms-pagination").pagination({
                items: 0,
                itemsOnPage: recordsPerPage,
                displayedPages: 3,
                hrefTextPrefix: "#"
            });

            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                var target = $(e.target).attr("href") // activated tab

                if (target === "#divWarehouseMapping") {
                    var combo = $find("<%= rcbState.ClientID%>");
                    LoadStates(combo, parseInt($(this).val()), true);
                    LoadWarehouseMappingWithPagination();
                }
            });

            $("[id$=ddlShipVia]").change(function () {
                try {
                    $("[id$=ddlShipService]").find("option").remove();
                    var options = "<option value='0'>Slect Ship-service</option>";

                    if (shipViaID == 91) {
                        options += "<option value='10'>FedEx Priority Overnight</option>";
                        options += "<option value='11'>FedEx Standard Overnight</option>";
                        options += "<option value='12'>FedEx Overnight</option>";
                        options += "<option value='13'>FedEx 2nd Day</option>";
                        options += "<option value='14'>FedEx Express Saver</option>";
                        options += "<option value='15' selected='selected'>FedEx Ground</option>";
                        options += "<option value='16'>FedEx Ground Home Delivery</option>";
                        options += "<option value='17'>FedEx 1 Day Freight</option>";
                        options += "<option value='18'>FedEx 2 Day Freight</option>";
                        options += "<option value='19'>FedEx 3 Day Freight</option>";
                        options += "<option value='20'>FedEx International Priority</option>";
                        options += "<option value='21'>FedEx International Priority Distribution</option>";
                        options += "<option value='22'>FedEx International Economy</option>";
                        options += "<option value='23'>FedEx International Economy Distribution</option>";
                        options += "<option value='24'>FedEx International First</option>";
                        options += "<option value='25'>FedEx International Priority Freight</option>";
                        options += "<option value='26'>FedEx International Economy Freight</option>";
                        options += "<option value='27'>FedEx International Distribution Freight</option>";
                        options += "<option value='28'>FedEx Europe International Priority</option>";
                    } else if (shipViaID == 88) {
                        options += "<option value='40'>UPS Next Day Air</option>";
                        options += "<option value='42'>UPS 2nd Day Air</option>";
                        options += "<option value='43' selected='selected'>UPS Ground</option>";
                        options += "<option value='48'>UPS 3Day Select</option>";
                        options += "<option value='49'>UPS Next Day Air Saver</option>";
                        options += "<option value='50'>UPS Saver</option>";
                        options += "<option value='51'>UPS Next Day Air Early A.M.</option>";
                        options += "<option value='55'>UPS 2nd Day Air AM</option>";
                    } else if (shipViaID == 90) {
                        options += "<option value='70'>USPS Express</option>";
                        options += "<option value='71'>USPS First Class</option>";
                        options += "<option value='72' selected='selected'>USPS Priority</option>";
                        options += "<option value='73'>USPS Parcel Post</option>";
                        options += "<option value='74'>USPS Bound Printed Matter</option>";
                        options += "<option value='75'>USPS Media</option>";
                        options += "<option value='76'>USPS Library</option>";
                    }

                    $(ddlShipService).append(options);
                } catch (e) {
                    alert('Unknown error occurred while loading Ship-Service');
                }
            });
        });

        var recordsPerPage = 20;
        var totalRecords = 0;

        $(document).ajaxStart(function () {
            $("#divLoader").show();
        }).ajaxStop(function () {
            $("#divLoader").hide();
        });

        function MoveUp(controlID) {
            if ($(controlID).val() != null) {
                var selected = $(controlID).find(":selected");
                var before = selected.prev();
                if (before.length > 0)
                    selected.detach().insertBefore(before);
            } else {
                alert("Select atleast one field");
            }
        }

        function MoveDown(controlID) {
            if ($(controlID).val() != null) {
                var selected = $(controlID).find(":selected");
                var next = selected.next();
                if (next.length > 0)
                    selected.detach().insertAfter(next);
            } else {
                alert("Select atleast one field");
            }
        }

        function LoadStates(combo, countryID, clearItems) {
            try {
                if (clearItems) {
                    combo.clearItems();
                }

                if (countryID > 0) {
                    $.ajax({
                        type: "POST",
                        url: '../opportunity/MassSalesFulfillmentService.svc/GetStates',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "countryID": countryID
                        }),
                        success: function (data) {
                            try {
                                var obj = $.parseJSON(data.GetStatesResult);
                                if (obj != null && obj.length > 0) {
                                    obj.forEach(function (e) {
                                        var comboItem = new Telerik.Web.UI.RadComboBoxItem();
                                        comboItem.set_text(replaceNull(e.vcState));
                                        comboItem.set_value(e.numStateID.toString());
                                        combo.get_items().add(comboItem);
                                    });

                                    combo.commitChanges();
                                }
                            } catch (err) {
                                alert("Unknown error occurred.");
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (IsJsonString(jqXHR.responseText)) {
                                var objError = $.parseJSON(jqXHR.responseText)
                                alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                            } else {
                                alert("Unknown error ocurred");
                            }
                        }
                    });
                }
            } catch (e) {
                alert("Unknown error occurred while loading states.");
            }
        }

        function LoadWarehouseMappingWithPagination() {
            $.when(LoadWarehouseMapping()).then(function () {
                if (totalRecords > 0) {
                    $("#liRecordDisplay").html("<b>" + 1 + " to " + (totalRecords > recordsPerPage ? recordsPerPage : totalRecords) + " of " + totalRecords + "</b>");
                } else {
                    $("#liRecordDisplay").html("<b>0 to 0 of 0</b>");
                }

                $("#ms-pagination").pagination('destroy');

                $("#ms-pagination").pagination({
                    items: totalRecords,
                    itemsOnPage: recordsPerPage,
                    displayedPages: 3,
                    hrefTextPrefix: "#",
                    onPageClick: function (page) {
                        $("#liRecordDisplay").html("<b>" + ((page - 1) * recordsPerPage + 1) + " to " + (totalRecords > (page * recordsPerPage) ? (page * recordsPerPage) : totalRecords) + " of " + totalRecords + "</b>");
                        LoadWarehouseMapping(page);
                    }
                });
            });
        }

        function LoadWarehouseMapping(pageIndex) {
            $("#tblWarehouseMapping > tbody").html("");

            var sources = [];
            var countries = [];
            var states = [];

            var checkedItems = $find("<%=rcbSourceHeader.ClientID%>").get_checkedItems();
            for (var i = 0; i < checkedItems.length; i++) {
                sources.push(checkedItems[i].get_value());
            }

            var checkedItems = $find("<%=rcbCountryHeader.ClientID%>").get_checkedItems();
            for (var i = 0; i < checkedItems.length; i++) {
                countries.push(checkedItems[i].get_value());
            }

            var checkedItems = $find("<%=rcbStateHeader.ClientID%>").get_checkedItems();
            for (var i = 0; i < checkedItems.length; i++) {
                states.push(checkedItems[i].get_value());
            }

            return $.ajax({
                type: "POST",
                url: '../WebServices/CommonService.svc/GetWarehouseMapping',
                contentType: "application/json",
                dataType: "json",
                data: JSON.stringify({
                    "pageIndex": (pageIndex || 1)
                    , "pageSize": recordsPerPage
                    , "filterSources": sources.join(",")
                    , "filterCountries": countries.join(",")
                    , "filterStates": states.join(",")
                }),
                success: function (data) {
                    try {
                        var obj = $.parseJSON(data.GetWarehouseMappingResult);
                        var records = $.parseJSON(obj.Records);
                        var data = "";

                        if (records != null && records.length > 0) {
                            records.forEach(function (n) {
                                data += "<tr>";
                                data += "<td>" + replaceNull(n.vcOrderSource) + "</td>";
                                data += "<td>" + replaceNull(n.vcCountry) + "</td>";
                                data += "<td>" + replaceNull(n.vcStates) + "</td>";
                                data += "<td>" + replaceNull(n.vcWarehouses) + "</td>";
                                data += "<td><button class='btn btn-danger btn-xs' onclick='return DeleteWarehouseMapping(" + n.ID + ")'><i class='fa fa-trash-o'></button></td>";
                                data += "</tr>";
                            });

                            $("#tblWarehouseMapping > tbody").html(data);
                        } else {
                            data += "<tr>";
                            data += "<td colspan='5' class='text-center'>No records found</td>";
                            data += "</tr>";
                            $("#tblWarehouseMapping > tbody").html(data);
                        }

                        totalRecords = obj.TotalRecords;
                    } catch (err) {
                        alert("Unknown error occurred.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (IsJsonString(jqXHR.responseText)) {
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                    } else {
                        alert("Unknown error ocurred");
                    }
                }
            });
        }

        function SaveWarehouseMapping() {
            try {
                var rcbState = $find("<%=rcbState.ClientID%>");
                    var states = [];

                    var checkedItems = rcbState.get_checkedItems();
                    for (var i = 0; i < checkedItems.length; i++) {
                        states.push(checkedItems[i].get_value());
                    }

                    var warehousePriorities = [];

                    $("[id$=lbSelectedWP]").children("option").each(function () {
                        warehousePriorities.push($(this).val());
                    });

                    if ($("[id$=ddlSource]").val() === "0") {
                        alert("Select order source");
                        return false;
                    } else if (parseInt($("[id$=ddlCountry]").val()) == 0) {
                        alert("Select Ship-To Country");
                        return false;
                    } else if (states.length == 0) {
                        alert("Select Ship-To State(s)");
                        return false;
                    } else if (warehousePriorities.length == 0) {
                        alert("Select Warehouses & Priorities");
                        return false;
                    } else {
                        $.ajax({
                            type: "POST",
                            url: '../WebServices/CommonService.svc/SaveWarehouseMapping',
                            contentType: "application/json",
                            dataType: "json",
                            data: JSON.stringify({
                                "orderSource": parseInt($("[id$=ddlSource]").val().split("~")[0])
                                , "sourceType": parseInt($("[id$=ddlSource]").val().split("~")[1])
                                , "country": parseInt($("[id$=ddlCountry]").val())
                                , "states": states.join(",")
                                , "warehouses": warehousePriorities.join(",")
                            }),
                            success: function (data) {
                                $("[id$=lbSelectedWP] option").remove();
                                LoadWarehouseMappingWithPagination()
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                if (IsJsonString(jqXHR.responseText)) {
                                    var objError = $.parseJSON(jqXHR.responseText)
                                    alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                                } else {
                                    alert("Unknown error ocurred");
                                }
                            }
                        });
                    }
                } catch (e) {
                    alert("Unknown error occurred while adding warehouse mapping")
                }

                return false;
            }

            function DeleteWarehouseMapping(id) {
                try {
                    $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/DeleteWarehouseMapping',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "id": id
                        }),
                        success: function (data) {
                            LoadWarehouseMappingWithPagination()
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            if (IsJsonString(jqXHR.responseText)) {
                                var objError = $.parseJSON(jqXHR.responseText)
                                alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                            } else {
                                alert("Unknown error ocurred");
                            }
                        }
                    });
                } catch (e) {
                    alert("Unknown error occurred while adding warehouse mapping")
                }

                return false;
            }

            function replaceNull(value) {
                return String(value) === "null" || String(value) === "undefined" ? "" : value.toString().replace(/'/g, "&#39;");
            }

            function IsJsonString(str) {
                try {
                    JSON.parse(str);
                } catch (e) {
                    return false;
                }
                return true;
            }

            function RefreshParentAndClose() {
                if (window.opener != null) {
                    window.opener.LoadRecords();
                }

                window.close();
            }

            function SourceHeaderChanged(sender, eventArgs) {
                var item = eventArgs.get_item();
                var items = sender.get_items();
                if (item.get_text() == "Check All" && item.get_value() == "0") {
                    items.forEach(function (itm) { itm.set_checked(checked); });
                }

                LoadWarehouseMappingWithPagination();
            }

            function CountryHeaderChanged(sender, eventArgs) {
                var combo = $find("<%= rcbStateHeader.ClientID%>");
                combo.clearItems();

                var checkedItems = sender.get_checkedItems();
                for (var i = 0; i < checkedItems.length; i++) {
                    LoadStates(combo, checkedItems[i].get_value(), false)
                }

                LoadWarehouseMappingWithPagination();
            }

            function StateHeaderChanged(sender, eventArgs) {
                LoadWarehouseMappingWithPagination();
            }

            function ClearWMFilters() {
                $find("<%= rcbSourceHeader.ClientID%>").get_items().forEach(function (itm) { itm.set_checked(false); });
                $find("<%= rcbSourceHeader.ClientID%>").clearSelection();
                $find("<%= rcbCountryHeader.ClientID%>").get_items().forEach(function (itm) { itm.set_checked(false); });
                $find("<%= rcbCountryHeader.ClientID%>").clearSelection();
                $find("<%= rcbStateHeader.ClientID%>").clearItems();
                LoadWarehouseMappingWithPagination();
            }

            function ValidateShippingConfiguration() {
                if ($("[id$=ddlOrderStatusShip]").val() == "0") {
                    alert("Select Order Source");
                    $("[id$=ddlOrderStatusShip]").focus();
                    return false;
                } else if ($("[id$=rbPreferredShipVia]").is(":checked") && $("[id$=ddlShipVia]").val() == "0") {
                    alert("Select Ship-via");
                    $("[id$=ddlShipVia]").focus();
                    return false;
                } else if ($("[id$=chkOverrideShipConfig").is(":checked") && (parseFloat($("[id$=txtShipWeight]").val()) == 0 || $("[id$=ddlShipViaOverride]").val() == 0)) {
                    alert("Order weight and Ship-via are required if you want to override all rules.");
                    $("[id$=ddlShipVia]").focus();
                    return false;
                }

                var priority = "";

                $("[id$=lbShipPriority] > option").each(function () {
                    priority += ((priority.length > 0 ? "," : "") + $(this).val());
                });

                $("[id$=hdnShipPriorities]").val(priority);

                return true;
            }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="pull-right">
        <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save & Close" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary" Text="Cancel" OnClientClick="Close();" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Fulfillment Configuration
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager runat="server" ID="scriptmanager"></asp:ScriptManager>
    <div class="container-fluid">
        <div class="overlay" id="divLoader" style="z-index: 10000; display: none">
            <div class="overlayContent" style="color: #000; background-color: #fff; text-align: center; width: 250px; padding: 20px">
                <i class="fa fa-2x fa-refresh fa-spin"></i>
                <h3>Please wait...</h3>
            </div>
        </div>
        <div class="row padbottom10" id="divError" runat="server" style="display: none">
            <div class="col-sm-12">
                <div class="alert alert-danger">
                    <h4><i class="icon fa fa-ban"></i>Error</h4>
                    <p>
                        <asp:Label ID="lblError" runat="server"></asp:Label>
                    </p>
                </div>
            </div>
        </div>
        <div class="row padbottom10">
            <div class="col-xs-12">
                <div class="form-inline">
                    <div class="form-group">
                        <label>When </label>
                        <asp:DropDownList ID="ddlWhen" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlWhen_SelectedIndexChanged">
                            <asp:ListItem Text="Ready to Pick" Value="1" />
                            <asp:ListItem Text="Ready to Ship" Value="2" />
                            <asp:ListItem Text="Ready to Invoice" Value="3" />
                            <asp:ListItem Text="Ready to Pay" Value="4" />
                            <asp:ListItem Text="Pending Close" Value="5" />
                            <asp:ListItem Text="Print BizDocs" Value="6" />
                        </asp:DropDownList>
                    </div>
                    <div class="form-group" id="divOrderStatus1" runat="server">
                        <label>
                            <asp:Literal ID="litOrderStaus" runat="server" Text="After order has been picked"></asp:Literal></label>
                        <asp:DropDownList runat="server" ID="ddlShipMode" Visible="false" CssClass="form-control">
                            <asp:ListItem Text="Ship rate has been added to an order" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Order has been packed" Value="3"></asp:ListItem>
                            <asp:ListItem Text="Order has received shipping Label & Tracking #s" Value="4"></asp:ListItem>
                            <asp:ListItem Text="Order has been shipped" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="form-group" id="divOrderStatus2" runat="server">
                        <label>Change Order Status to </label>
                        <asp:DropDownList runat="server" ID="ddlOrderStatus" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group" id="divPendingClose" runat="server" visible="false">
                        <label>Default Filter </label>
                        <asp:DropDownList ID="ddlPendingCloseFilter" runat="server" CssClass="form-control">
                            <asp:ListItem Text="All Sales Orders" Value="1"></asp:ListItem>
                            <asp:ListItem Text="Fully Shipped and Invoiced Sales Orders" Value="2"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
        </div>
        <div class="row padbottom10" id="divShipSetting" runat="server">
            <div class="col-xs-12">
                <div class="pull-left">
                    <asp:RadioButtonList ID="rblPackingMode" runat="server">
                        <asp:ListItem Text="Item quantities not on fulfillment orders whether they've been picked or not" Value="1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Item quantities that have been picked but are not on fulfillment orders" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Item quantities that have been packed & labeled but are not on fulfillment orders" Value="3"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
                <div class="pull-right">
                    <asp:CheckBox ID="chkAutoGeneratePackingSlip" runat="server" Text="Automatically add a Packing Slip when items are picked" />
                    <asp:CheckBox ID="chkPickListMapping" runat="server" Text="Enable Pick List Mapping" />
                    <br />
                    <a href="#" data-toggle="modal" data-target="#modalShippingConfiguration">Shipping Carrier & Service Selection Automation </a>
                    <br />
                    <asp:DropDownList ID="ddlPachShipButtons" runat="server" CssClass="form-control">
                        <asp:ListItem Text="Both ship buttons" Value="0"></asp:ListItem>
                        <asp:ListItem Text="Ship qty picked not packed" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Ship qty packed not shipped" Value="2"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="panel with-nav-tabs panel-primary">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#divGeneralSettings">General Settings</a></li>
                            <li><a data-toggle="tab" href="#divWarehouseMapping">Warehouse Mapping</a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div id="divGeneralSettings" class="tab-pane fade in active">
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="row padbottom10" id="divInvoicing" runat="server" style="display: none;">
                                            <div class="col-md-12">
                                                <div class="pull-left">
                                                    <div class="form-inline">
                                                        <div class="form-group">
                                                            <label>Invoice Orders:</label>
                                                            <asp:RadioButton ID="rbInvoiceAfterShipping" runat="server" GroupName="Invoicing" Checked="true" />
                                                            Invoice AFTER shipping
                                                    <asp:RadioButton ID="rbInvoiceAnytime" runat="server" GroupName="Invoicing" />
                                                            Invoice anytime
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="pull-right">
                                                    <asp:CheckBox runat="server" ID="chkFulfillmentBizDocMapping" Text="Enable Pick List OR Fulfillment/Packing Slip Maaping" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-6">
                                                <div class="row padbottom10">
                                                    <div class="col-md-12">
                                                        <div class="pull-left" id="divGroupBy" runat="server">
                                                            <asp:CheckBox ID="chkGroupByOrder" runat="server" />
                                                            Group by Order
                                                        </div>

                                                        <div class="pull-right" id="divPickGroup" runat="server" style="display: none">
                                                            <asp:CheckBox ID="chkPickListByOrder" runat="server" />
                                                            Generate Pick List by Order                        
                            &nbsp;&nbsp;<asp:CheckBox runat="server" ID="chkPickWithoutInventoryCheck" />
                                                            Add pick list without inventory validation
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-4">
                                                        <label>Available Fields</label>
                                                        <br />
                                                        <asp:ListBox ID="lbAvailableFieldLeft" runat="server" CssClass="form-control" Height="450"></asp:ListBox>
                                                    </div>
                                                    <div class="col-sm-12 col-md-2" style="height: 450px; margin-top: 25px; vertical-align: middle; text-align: center; padding-left: 0px; padding-right: 0px">
                                                        <div style="padding-top: 105px">
                                                            <button id="btnAddLeft" type="button" class="btn btn-primary">Add&nbsp;<i class='fa fa-chevron-right'></i></button>
                                                            <br />
                                                            <br />
                                                            <button id="btnRemoveLeft" type="button" class="btn btn-primary"><i class='fa fa-chevron-left'></i>&nbsp;Remove</button>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-md-4">
                                                        <label>Selected Fields/Choose Order</label>
                                                        <br />
                                                        <asp:ListBox ID="lbSelectedFieldLeft" runat="server" CssClass="form-control" Height="450"></asp:ListBox>
                                                    </div>
                                                    <div class="col-sm-12 col-md-2" style="height: 450px; margin-top: 25px; vertical-align: middle; text-align: center; padding-left: 0px; padding-right: 0px">
                                                        <div style="padding-top: 105px">
                                                            <button id="btnUpLeft" type="button" class="btn btn-primary"><i class='fa fa-chevron-up'></i></button>
                                                            <br />
                                                            <br />
                                                            <button id="btnDownLeft" type="button" class="btn btn-primary"><i class='fa fa-chevron-down'></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-6" id="divRight" runat="server">
                                                <div class="row padbottom10">
                                                    <div class="col-xs-12">
                                                        <div class="pull-left">
                                                            <div class="form-inline">
                                                                <label>Scan Value:</label>
                                                                <asp:RadioButton ID="rbSKU" runat="server" GroupName="ScanValue" Checked="true" />
                                                                SKU
                                <asp:RadioButton ID="rbUPC" runat="server" GroupName="ScanValue" />
                                                                UPC
                                <asp:RadioButton ID="rbItemName" runat="server" GroupName="ScanValue" />
                                                                Item Name
                                                            </div>
                                                        </div>
                                                        <div class="pull-right">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-4">
                                                        <label>Available Fields</label>
                                                        <br />
                                                        <asp:ListBox ID="lbAvailableFieldRight" runat="server" CssClass="form-control" Height="450" Width="190"></asp:ListBox>
                                                    </div>
                                                    <div class="col-sm-12 col-md-2" style="height: 450px; margin-top: 25px; vertical-align: middle; text-align: center; padding-left: 0px; padding-right: 0px">
                                                        <div style="padding-top: 105px">
                                                            <button id="btnAddRight" type="button" class="btn btn-primary">Add&nbsp;<i class='fa fa-chevron-right'></i></button>
                                                            <br />
                                                            <br />
                                                            <button id="btnRemoveRight" type="button" class="btn btn-primary"><i class='fa fa-chevron-left'></i>&nbsp;Remove</button>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-md-5">
                                                        <label>Selected Fields/Choose Order</label>
                                                        <br />
                                                        <asp:ListBox ID="lbSelectedFieldRight" runat="server" CssClass="form-control" Height="450"></asp:ListBox>
                                                    </div>
                                                    <div class="col-sm-12 col-md-1" style="height: 450px; margin-top: 25px; vertical-align: middle; text-align: center; padding-left: 0px; padding-right: 0px">
                                                        <div style="padding-top: 105px">
                                                            <button id="btnUpRight" type="button" class="btn btn-primary"><i class='fa fa-chevron-up'></i></button>
                                                            <br />
                                                            <br />
                                                            <button id="btnDownRight" type="button" class="btn btn-primary"><i class='fa fa-chevron-down'></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="divWarehouseMapping" class="tab-pane fade">
                                <div class="row padbottom10">
                                    <div class="col-xs-12">
                                        <div class="pull-left">
                                            <div class="form-inline">
                                                <label>Enable automated warehouse selection for items that don’t already have a warehouse selected</label>
                                                <asp:CheckBox ID="chkAutoWarehouseSelection" runat="server" CssClass="checkbox" />
                                            </div>
                                        </div>
                                        <div class="pull-right">
                                            <div class="form-inline">
                                                <label>If item has BO qty don’t map and set Order Status to “Set WH manually”</label>
                                                <asp:CheckBox ID="chkBOOrderStatus" runat="server" CssClass="checkbox" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row padbottom10">
                                    <div class="col-xs-12">
                                        <div class="form-inline">
                                            <div class="form-group">
                                                <label>WHEN Order comes from</label>
                                                <asp:DropDownList ID="ddlSource" runat="server" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                            <div class="form-group" style="padding-left: 10px">
                                                <label>AND its Ship-To Country is</label>
                                                <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control">
                                                </asp:DropDownList>
                                            </div>
                                            <div class="form-group" style="padding-left: 10px">
                                                <label>AND its Ship-To State(s) is</label>
                                                <telerik:RadComboBox ID="rcbState" CheckBoxes="true" runat="server" EmptyMessage="Select State">
                                                </telerik:RadComboBox>
                                            </div>
                                            <div class="form-group" style="padding-left: 10px">
                                                <label>SHIP FROM</label>
                                                <a href="#" data-toggle="modal" data-target="#divWarehousePriority">Warehouses & Priorities</a>
                                                <div id="divWarehousePriority" class="modal fade" role="dialog">
                                                    <div class="modal-dialog">
                                                        <!-- Modal content-->
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                                <h4 class="modal-title">Warehouses & Priorities</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <ul class="list-inline">
                                                                    <li>
                                                                        <label>Available Warehouses</label><br />
                                                                        <asp:ListBox runat="server" ID="lbAvailableWP" CssClass="form-control" Width="200" Height="200"></asp:ListBox>
                                                                    </li>
                                                                    <li style="text-align: center">
                                                                        <button id="btnAddWP" type="button" class="btn btn-primary">Add&nbsp;<i class='fa fa-chevron-right'></i></button>
                                                                        <br />
                                                                        <br />
                                                                        <button id="btnRemoveWP" type="button" class="btn btn-primary"><i class='fa fa-chevron-left'></i>&nbsp;Remove</button>
                                                                    </li>
                                                                    <li>
                                                                        <label>Warehouse Priority</label><br />
                                                                        <asp:ListBox runat="server" ID="lbSelectedWP" CssClass="form-control" Width="200" Height="200"></asp:ListBox>
                                                                    </li>
                                                                    <li style="text-align: center">
                                                                        <button id="btnUPWP" type="button" class="btn btn-primary"><i class='fa fa-chevron-up'></i></button>
                                                                        <br />
                                                                        <br />
                                                                        <button id="btnDownWP" type="button" class="btn btn-primary"><i class='fa fa-chevron-down'></i></button>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Save & Close</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button class="btn btn-primary" onclick="return SaveWarehouseMapping();">Add</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <div class="pull-right">
                                            <ul class="list-inline" id="min-grid-controls">
                                                <li id="liRecordDisplay"></li>
                                                <li>
                                                    <ul id="ms-pagination"></ul>
                                                </li>
                                                <li>
                                                    <a href="javascript:ClearWMFilters();" id="hplClearGridCondition" title="Clear All Filters" class="btn-box-tool"><i class="fa fa-2x fa-filter"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <table class="table table-bordered table-striped" id="tblWarehouseMapping">
                                            <thead>
                                                <tr>
                                                    <th>Order Source<br />
                                                        <telerik:RadComboBox ID="rcbSourceHeader" CheckBoxes="true" runat="server" EmptyMessage="Select Order Sources" OnClientItemChecked="SourceHeaderChanged">
                                                        </telerik:RadComboBox>
                                                    </th>
                                                    <th>Ship-To Country<br />
                                                        <telerik:RadComboBox ID="rcbCountryHeader" CheckBoxes="true" runat="server" EmptyMessage="Select Country" OnClientItemChecked="CountryHeaderChanged">
                                                        </telerik:RadComboBox>
                                                    </th>
                                                    <th>Ship-To State or Province<br />
                                                        <telerik:RadComboBox ID="rcbStateHeader" CheckBoxes="true" runat="server" EmptyMessage="Select State" OnClientItemChecked="StateHeaderChanged">
                                                        </telerik:RadComboBox>
                                                    </th>
                                                    <th style="vertical-align: top">Warehouse Ship-From Priority</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="modalShippingConfiguration" class="modal fade" role="dialog">
                <asp:UpdatePanel ID="upShipConfig" runat="server" ChildrenAsTriggers="true" class="modal-dialog" style="width: 75%">
                    <ContentTemplate>
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Shipping: Rate, Carrier & Service decisions</h4>
                            </div>
                            <div class="modal-body">
                                <div id="divShipMessage" class="row" style="display: none">
                                    <div class="col-xs-12">
                                        <div class="alert alert-warning">
                                            <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                                            <asp:Literal ID="litShipMessage" runat="server"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                                <div class="row padbottom10">
                                    <div class="col-xs-12">
                                        <div class="form-inline">
                                            <label>
                                                Order Source
                                            </label>
                                            <asp:DropDownList ID="ddlOrderStatusShip" runat="server" CssClass="form-control"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                                <div class="row padbottom10">
                                    <div class="col-xs-12">
                                        <asp:RadioButton ID="rbPriorityShipVia" runat="server" GroupName="ShipConfigType" Checked="true" />
                                        Use Priority Sequence to select Ship-Via & Service
                            <ul class="list-inline" style="margin-top: 10px;">
                                <li>
                                    <asp:ListBox ID="lbShipPriority" runat="server" CssClass="form-control" Height="100">
                                        <asp:ListItem Value="1" Text="Deliver by Date"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="Shipping Service"></asp:ListItem>
                                        <asp:ListItem Value="3" Text="Ship-Via (Carrier)"></asp:ListItem>
                                        <asp:ListItem Value="4" Text="Shipping Amount"></asp:ListItem>
                                    </asp:ListBox>
                                </li>
                                <li style="vertical-align: top; margin-top: 10px;">
                                    <button id="btnUPShipConfig" type="button" class="btn btn-primary btn-sm" onclick="MoveUp('[id$=lbShipPriority]');"><i class='fa fa-chevron-up'></i></button>
                                    <br />
                                    <br />
                                    <button id="btnDownShipConfig" type="button" class="btn btn-primary btn-sm" onclick="MoveDown('[id$=lbShipPriority]');"><i class='fa fa-chevron-down'></i></button>
                                </li>
                            </ul>
                                    </div>
                                </div>
                                <div class="row padbottom10">
                                    <div class="col-xs-12">
                                        <asp:RadioButton ID="rbPreferredShipVia" runat="server" GroupName="ShipConfigType" />
                                        Only use this Ship-Via or Ship-Via & Service
                            <ul class="list-inline" style="margin-top: 10px;">
                                <li>
                                    <asp:DropDownList ID="ddlShipVia" runat="server" CssClass="form-control"></asp:DropDownList></li>
                                <li>
                                    <asp:DropDownList ID="ddlShipService" runat="server" CssClass="form-control">
                                        <asp:ListItem Text="-- Select Ship-service --" Value="0"></asp:ListItem>
                                    </asp:DropDownList></li>
                            </ul>
                                    </div>
                                </div>
                                <div class="row padbottom10">
                                    <div class="col-xs-12">
                                        <asp:RadioButton ID="rbCheapestShipVia" runat="server" GroupName="ShipConfigType" />
                                        Always use cheapest Shipping Rate                            
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-xs-12 form-inline" style="line-height: 30px;">
                                        <asp:CheckBox ID="chkOverrideShipConfig" runat="server" />
                                        If total order weight is > 
                            <asp:TextBox runat="server" ID="txtShipWeight" CssClass="form-control" Width="80"></asp:TextBox>
                                        lbs, for all items within a particular ship-from location, override all rules and set Ship-Via to
                            <asp:DropDownList ID="ddlShipViaOverride" runat="server" CssClass="form-control"></asp:DropDownList>
                                        and Ship Service to “Set Manually”
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="modal-footer">
                                <asp:HiddenField ID="hdnShipPriorities" runat="server" />
                                <asp:Button runat="server" CssClass="btn btn-primary" ID="btnSaveShippingConfig" Text="Save" OnClientClick="return ValidateShippingConfiguration();" OnClick="btnSaveShippingConfig_Click" />
                                <asp:Button runat="server" CssClass="btn btn-primary" ID="btnSaveCloseShippingConfig" Text="Save & Close" OnClientClick="return ValidateShippingConfiguration();" OnClick="btnSaveCloseShippingConfig_Click" />
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </ContentTemplate>
                </asp:UpdatePanel>
                <!-- /.modal-dialog -->
            </div>
            <asp:HiddenField ID="hdnReadytoPickLeft" runat="server" Value="-1" />
            <asp:HiddenField ID="hdnReadytoPickRight" runat="server" Value="-1" />
            <asp:HiddenField ID="hdnReadytoShip" runat="server" Value="-1" />
            <asp:HiddenField ID="hdnReadytoInvoice" runat="server" Value="-1" />
            <asp:HiddenField ID="hdnReadytoPay" runat="server" Value="-1" />
            <asp:HiddenField ID="hdnPendingClose" runat="server" Value="-1" />
            <asp:HiddenField ID="hdnPrintBizDocs" runat="server" Value="-1" />


            <asp:HiddenField ID="hdnGroupByOrderForPick" runat="server" />
            <asp:HiddenField ID="hdnGroupByOrderForShip" runat="server" />
            <asp:HiddenField ID="hdnGroupByOrderForInvoice" runat="server" />
            <asp:HiddenField ID="hdnGroupByOrderForPay" runat="server" />
            <asp:HiddenField ID="hdnGroupByOrderForClose" runat="server" />
            <asp:HiddenField ID="hdnGroupByOrderForPrintBizDoc" runat="server" />

            <asp:HiddenField ID="hdnOrderStatusPicked" runat="server" />
            <asp:HiddenField ID="hdnOrderStatusPacked1" runat="server" />
            <asp:HiddenField ID="hdnOrderStatusPacked2" runat="server" />
            <asp:HiddenField ID="hdnOrderStatusPacked3" runat="server" />
            <asp:HiddenField ID="hdnOrderStatusPacked4" runat="server" />
            <asp:HiddenField ID="hdnOrderStatusInvoiced" runat="server" />
            <asp:HiddenField ID="hdnOrderStatusPaid" runat="server" />
            <asp:HiddenField ID="hdnOrderStatusClosed" runat="server" />
</asp:Content>
