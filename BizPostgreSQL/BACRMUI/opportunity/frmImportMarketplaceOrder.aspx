﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmImportMarketplaceOrder.aspx.vb" Inherits=".frmImportMarketplaceOrder"
    MasterPageFile="~/common/Popup.Master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>


<asp:Content ID="Content5" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Import Online Marketplace Order</title>
      <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <style type="text/css">
        .fixpanel
        {
            /*width: auto;
            overflow: hidden;
            display: inline-block;
            white-space: nowrap;
            height: auto;*/
             border-top: 1px solid #CBCBCB;
            border-left: 1px solid #CBCBCB;
            border-right: 1px solid #CBCBCB;
            border-bottom: 1px solid #CBCBCB;
        }

        .displaynone
        {
            display: none;
        }

        .header
        {
            background: #E5E5E5!important;
            border-bottom: 1px solid #CBCBCB;
            border-top: 1px solid #CBCBCB;
            border-right: 1px solid #CBCBCB;
            border-left: 1px solid #CBCBCB;
            font-weight: bold;
            line-height: 30px;
        }



        .fixpanel .is, .ais
        {
            /*background: #E5E5E5!important;*/
            border-bottom: 1px solid #CBCBCB;
            border-top: 1px solid #CBCBCB;
            border-right: 1px solid #fff;
            border-left: 1px solid #fff;
            /*font-weight: bold;*/
            padding: 0 0 0 10px;
            /*line-height: 50px;*/
               }

            .fixpanel .is td:first-child, .ais td:first-child
            {
                border-left: 1px solid #CBCBCB;
               }

            .fixpanel .is td:last-child, .ais td:last-child
            {
                border-right: 1px solid #CBCBCB;
               }

        #tblAssign .NA td:first-child, td:first-child, td:last-child, td:last-child
        {
            border-width: 0 0 0 0;
               }
    </style>
   <script type="text/javascript" language="javascript">


       function ValidateGetOrderList() {
           if (document.getElementById("ddlWebApi").value == 0) {
               alert("First select a Marketplace");
               document.getElementById("ddlWebApi").focus();
               return false;
           }
           if (document.getElementById('ctl00_Content_calOrderFromDate_txtDate').value == 0) {
               alert("please enter From Date and Time to get Orders list")
               document.getElementById('ctl00_Content_calOrderFromDate_txtDate').focus();
                   return false;
               }
           if (document.getElementById('ctl00_Content_calOrderTodate_txtDate').value == 0) {
               alert("please enter To Date and Time to get Orders list")
               document.getElementById('ctl00_Content_calOrderTodate_txtDate').focus();
               return false;
           }

           return true;
       }
               
       function ValidateReImportSelected() {
           var ReImportSelected = false;
           $('#<%=gvMatchingOrders.ClientID%> tr').each(function () {

               var isChecked = $(this).find('#chkSelect').is(':checked');
                    if (isChecked == true) {
                        ReImportSelected = true;
           }
                });
           if (ReImportSelected != true) {
                    alert('Please select atleast one Order to Re-import');
                    return false;
           }
           if (confirm("Are you sure?, do you want to reimport the selected Orders") == false) {
               return false;
           }
           return true;
       }

       function pageLoad() {
           var manager = Sys.WebForms.PageRequestManager.getInstance();
           manager.add_beginRequest(OnBeginRequest);
           manager.add_endRequest(OnEndRequest);
       }
       function OnBeginRequest(sender, args) {
           $get('UpdateProgress1').style.display = "block";
           $("#table-main-part").addClass("Background");
       }
       function OnEndRequest(sender, args) {
           $get('UpdateProgress1').style.display = "none";
           $("#table-main-part").removeClass("Background");
       }
    </script>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">

    </asp:ScriptManager>
    <table cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td align="right"></td>
        </tr>

    </table>
</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">

    
  
              <table style="vertical-align:top;">
        <tr>
            <td colspan="3">
                <table>
                    <tr> <td style="width:90px">Market Place : 
                 <%--<asp:Label runat="server" ID="lblWebAPI" Text="Market Place : "></asp:Label>--%>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlWebApi"></asp:DropDownList>
            </td>
                      <%--  <td > 
                            <div class="RequestType">
                <asp:DropDownList runat="server" ID="ddlRequestType">
                  <asp:ListItem Text="-- Select One --" Value="0"></asp:ListItem>
                     <asp:ListItem Text="Order Id" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Date Range" Value="1"></asp:ListItem>
                   
                </asp:DropDownList></div>
            </td>--%>
                        <td>
                           
                             <div class="OrderDate" style="width:auto;">
                                 <table><tr><td style="width:90px;"> List Orders :</td><td align="right" style="width:40px;"> From </td><td><div style="width:125px;"> <BizCalendar:Calendar ID="calOrderFromDate" runat="server" ClientIDMode="AutoID"
                                ShowTime="12" /></div> </td>

                                   <td  align="right" style="width:30px;"> To </td>  <td><div style="width:125px;"> <BizCalendar:Calendar ID="calOrderTodate" runat="server" ClientIDMode="AutoID"
                                ShowTime="12" /></div></td>
                                        </tr></table>
                                 </div>
                            
                       <%--      <div class="OrderId">
                                   <asp:TextBox ID="txtApiOrderId" runat="server" ></asp:TextBox>
                                 </div>--%>

             
            </td>
                        <td><asp:Button CssClass="button" runat="server" ID="btnGetOrders" Text="Get Orders"></asp:Button></td>

            </tr>
                </table>
            </td>
           
        </tr>
                  <tr><td> <div style="text-align: center;">
        <asp:UpdateProgress ID="UpdateProgress1" runat="server" DynamicLayout="true" AssociatedUpdatePanelID="upnlImportItems">
            <ProgressTemplate>
                <img src="../images/pgBar.gif" runat="server" id="pgBar">
            </ProgressTemplate>
        </asp:UpdateProgress>
    </div></td></tr>
          <tr>
                      <td>
                          <asp:UpdatePanel runat="server" ID="upnlImportItems">
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="btnGetOrders" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnImportOrder" EventName="Click" />
        </Triggers>
        <ContentTemplate>
            <div style="height: 350px; overflow: auto;">
                              <asp:Panel ID="pnlGridView" CssClass="fixpanel" runat="server" ScrollBars="Auto">

                                  <asp:GridView ID="gvMatchingOrders" runat="server" BorderWidth="0" CssClass="fixpanel" AutoGenerateColumns="False"
                                      Style="margin-right: 0px" Width="100%" ClientIDMode="Static" ShowHeaderWhenEmpty="true">
                                      <AlternatingRowStyle CssClass="ais" />
                                      <RowStyle CssClass="is" Height="40px" />
                                      <HeaderStyle CssClass="header" />
                                      <Columns>
                                          <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="5%">
                                              <HeaderTemplate>
                                                  <asp:CheckBox ID="SelectAllCheckBox" runat="server" onclick="SelectAll('SelectAllCheckBox', 'chkSelected');" />
                                              </HeaderTemplate>
                                              <ItemTemplate>
                                                  <asp:CheckBox ID="chkSelect" CssClass="chkSelected" runat="server" />
                                              </ItemTemplate>
                                          </asp:TemplateField>
                                        <%--  <asp:BoundField DataField="numOppId" HeaderText="Biz Order Id" />
                                         --%>
                                          <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Biz Order Id">
                                              <ItemTemplate>
                                                  <asp:Label ID="lblOppID"  runat="server" Text='<%# Eval("numOppId")%>'></asp:Label>
                                              </ItemTemplate>
                                          </asp:TemplateField>
                                          <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Marketplace Order Id">
                                              <ItemTemplate>
                                                  <asp:Label ID="lblMarketplaceOrderId" runat="server" Text='<%# Eval("vcMarketplaceOrderID")%>'></asp:Label>
                                              </ItemTemplate>
                                          </asp:TemplateField>
                                           <asp:BoundField DataField="bintCreatedDate" HeaderText="Order Date & Time" />
                                         <%-- <asp:TemplateField ItemStyle-HorizontalAlign="Left" HeaderText="Order Date">
                                              <ItemTemplate>
                                                  <asp:Label ID="lblOrderPosted" runat="server" Text='<%# Eval("bintCreatedDate")%>'></asp:Label>
                                              </ItemTemplate>
                                          </asp:TemplateField>--%>
                                      </Columns>
                                      <EmptyDataTemplate>
                                          There aren't any Orders matched.
                                      </EmptyDataTemplate>
                                  </asp:GridView>
                              </asp:Panel>
                          </div>

        </ContentTemplate>
    </asp:UpdatePanel>
                          
                      </td>

                  </tr>
                  <tr><td align="right"><asp:Button CssClass="button" runat="server" ID="btnImportOrder" Text="Re Import Order"></asp:Button></td></tr>
          <tr>
            <td colspan="3">
                 <b> Notes :</b>  
            <ol start="1">
                <li>On clicking Import Order button, a request will be sent to respective Online Marketplace to get the Order Details </li>
                <li>Order details will be added into BizAutomation on receiving the Order report from respective Marketplace </li>
                <li>Marketplace order id can be obtained easily from Order details in biz. or you can view those in sales order listing page.if you do not see that field then you will have to configure that field for it to show.</li>
              </ol>
                   
            </td>
        </tr>
         <tr>
            <td>
             
            </td>
        </tr>
         <tr>
            <td>
            </td>
        </tr>
    </table>
    
  
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label runat="server" ID="lblPageTitle"></asp:Label>Import API Specific Order
</asp:Content>
