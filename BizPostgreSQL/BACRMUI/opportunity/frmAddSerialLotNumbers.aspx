﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmAddSerialLotNumbers.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmAddSerialLotNumbers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
        function Add() {

            if ($('#hdnItemType').val() == "Serial") {

                if ($('#lstAvailablefld > option:selected').length > 0) {

                    if ($('#lstSelectedfld > option').length < parseInt($('#hdnQty').val()) || $('#hdnType').val() == "1" || $('#hdnType').val() == "2") {
                        $('#lstAvailablefld > option:selected').remove().appendTo('#lstSelectedfld');
                        $('#lstSelectedfld > option:last').attr('selected', 'selected');
                        $('#lstAvailablefld > option:first').attr('selected', 'selected');
                    } else {
                        alert("You can select maximum " + $('#hdnQty').val() + " item.");
                    }

                }
                else {
                    alert("Select atleast one item.");
                }
            }
            else {
                var qty = parseInt($('#txtQty').val());
                var maxQty = parseInt($('#hdnQty').val())

                if (qty > 0) {

                    var selectedQty = 0;

                    //Get qty from already selected items
                    $("#lstSelectedfld > option").each(function () {
                        selectedQty = selectedQty + parseInt(this.getAttribute("Qty"));
                    });

                    //if Quantity selected for add is greater than Max qty show error message.
                    if ((qty + selectedQty) > maxQty && $('#hdnType').val() != "1" && $('#hdnType').val() != "2") {
                        alert("Quantity can not be greater than Max Quantity.");
                    }
                    else {
                        if ($('#lstAvailablefld > option:selected').length > 0) {
                            var selectedOption = $('#lstAvailablefld > option:selected');

                            //if user has already selected items with required qty restrict user from selecting more items.
                            if (selectedQty == maxQty && $('#hdnType').val() != "1" && $('#hdnType').val() != "2") {
                                alert("You can select maximum " + maxQty + " quantity.");
                            } else {
                                //Get quantity of selected option
                                var selectedOptionQty = parseInt(selectedOption.attr('Qty'));

                                //if selected option have enought qty to add than allow to add other wise show alert
                                if (selectedOptionQty >= qty) {
                                    var selectedOptionToAdd = selectedOption.clone();
                                    var selectedOptionVaule = selectedOption.val();

                                    var objOptionAlreadySelected = $('#lstSelectedfld > option[value="' + selectedOptionVaule + '"]');

                                    //If some quantity is already selected from selected from current selected option then simply
                                    // increase quantity in Selected ListBox.
                                    if (objOptionAlreadySelected.length > 0) {
                                        var alreadyselectedoptionqty = parseInt(objOptionAlreadySelected.attr("Qty"));
                                        objOptionAlreadySelected.attr("Qty", alreadyselectedoptionqty + qty);
                                        objOptionAlreadySelected.text(objOptionAlreadySelected.attr("SerialNo") + "(" + objOptionAlreadySelected.attr("Date") + ")" + "(" + (alreadyselectedoptionqty + qty) + ")");
                                    }
                                    else {
                                        selectedOptionToAdd.attr("Qty", qty);
                                        selectedOptionToAdd.text(selectedOptionToAdd.attr("SerialNo") + "(" + selectedOptionToAdd.attr("Date") + ")" + "(" + qty + ")");
                                        selectedOptionToAdd.appendTo('#lstSelectedfld');
                                    }

                                    //if selected option available qty is same as quantity to add than remove it from available field otherwise
                                    // Decrease it qty with added quantity
                                    if (selectedOptionQty == qty) {
                                        selectedOption.remove();
                                    } else {
                                        $('#lstAvailablefld > option:selected').attr("Qty", selectedOptionQty - qty);
                                        $('#lstAvailablefld > option:selected').text(selectedOption.attr("SerialNo") + "(" + selectedOption.attr("Date") + ")" + "(" + (selectedOptionQty - qty) + ")");
                                    }

                                    $('#lstSelectedfld > option:last').attr('selected', 'selected');
                                    $('#lstAvailablefld > option:first').attr('selected', 'selected');

                                    var selectListAvailable = $('#lstAvailablefld option');
                                    var selectListSelected = $('#lstSelectedfld option');

                                    selectListAvailable.sort(function (a, b) {
                                        arel = parseInt($(a).attr("Order"));
                                        brel = parseInt($(b).attr("Order"));
                                        return arel == brel ? 0 : arel < brel ? -1 : 1
                                    });
                                    $('#lstAvailablefld').html(selectListAvailable);

                                    selectListSelected.sort(function (a, b) {
                                        arel = parseInt($(a).attr("Order"));
                                        brel = parseInt($(b).attr("Order"));
                                        return arel == brel ? 0 : arel < brel ? -1 : 1
                                    });
                                    $('#lstSelectedfld').html(selectListSelected);

                                } else {
                                    alert("Selected option do not have enough qty to add");
                                }
                            }
                        }
                        else {
                            alert("Select atleast one item.");
                        }
                    }

                }
                else {
                    alert("Enter value for Quantity.");
                }
            }

            return false;
        }

        function Remove() {
            if ($('#hdnItemType').val() == "Serial") {

                if ($('#lstSelectedfld > option:selected').length > 0) {
                    var selectedOption = $('#lstSelectedfld > option:selected');

                    if (parseInt(selectedOption.attr("numOppBizDocsId")) > 0) {
                        alert("You cna't remove serial because it is used in fulfillment order.");
                    } else {
                        $('#lstSelectedfld > option:selected').remove().appendTo('#lstAvailablefld');
                        $('#lstSelectedfld > option:first').attr('selected', 'selected');
                        $('#lstAvailablefld > option:last').attr('selected', 'selected');
                    }
                }
                else {
                    alert("Select atleast one item.");
                }
            }
            else {
                if ($('#lstSelectedfld > option:selected').length > 0) {
                    var selectedOption = $('#lstSelectedfld > option:selected');
                    var selectedOptionValue = selectedOption.val();

                    if (parseInt(selectedOption.attr("numOppBizDocsId")) > 0) {
                        alert("You cna't remove lot no because it is used in fulfillment order.");
                    } else {

                        //check if option is aready available in selectedoption with value
                        var objOptionAvailable = $('#lstAvailablefld > option[value="' + selectedOptionValue + '"]');

                        if (objOptionAvailable.length == 0) {
                            $('#lstSelectedfld > option:selected').remove().appendTo('#lstAvailablefld');
                            $('#lstSelectedfld > option:first').attr('selected', 'selected');
                            $('#lstAvailablefld > option:last').attr('selected', 'selected');
                        }
                        else {
                            //Get quantity of selected option;

                            var selectedOptionQty = parseInt(selectedOption.attr("Qty"));
                            var availableOptionQty = parseInt(objOptionAvailable.attr("Qty"));

                            objOptionAvailable.attr("Qty", availableOptionQty + selectedOptionQty);
                            objOptionAvailable.text(objOptionAvailable.attr("SerialNo") + "(" + objOptionAvailable.attr("Date") + ")" + "(" + (availableOptionQty + selectedOptionQty) + ")");

                            $('#lstSelectedfld > option:selected').remove();
                        }

                        var selectListAvailable = $('#lstAvailablefld option');
                        var selectListSelected = $('#lstSelectedfld option');

                        selectListAvailable.sort(function (a, b) {
                            arel = parseInt($(a).attr("Order"));
                            brel = parseInt($(b).attr("Order"));
                            return arel == brel ? 0 : arel < brel ? -1 : 1
                        });
                        $('#lstAvailablefld').html(selectListAvailable);

                        selectListSelected.sort(function (a, b) {
                            arel = parseInt($(a).attr("Order"));
                            brel = parseInt($(b).attr("Order"));
                            return arel == brel ? 0 : arel < brel ? -1 : 1
                        });
                        $('#lstSelectedfld').html(selectListSelected);
                    }
                }
                else {
                    alert("Select atleast one item.");
                }
            }

            return false;
        }

        function Save() {
            //if ($('#lstSelectedfld > option:selected').length > 0) {
                try
                {
                    var selectedItems = "";

                    if ($('#hdnItemType').val() == "Serial") {
                        $('#lstSelectedfld > option').each(function () {
                            selectedItems = selectedItems + $(this).val() + "-1" + ",";
                        });
                    } else {
                        $('#lstSelectedfld > option').each(function () {
                            selectedItems = selectedItems + $(this).val() + "-" + $(this).attr("Qty") + ",";
                        });
                    }

                    $("#hdnSelectedItems").val(selectedItems);

                    return true;
                }
                catch (e) {
                    alert("Error occured.");
                    return false;
                }
               
            //} else {
            //    alert("Select atleast one item.");
            //    return false;
            //}
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="input-part">
        <div class="right-input">
            <asp:Button runat="server" ID="btnSave" Text="Save & Close" CssClass="button" OnClientClick="return Save()" />
            <asp:Button runat="server" ID="btnClose" Text="Close" CssClass="button" Width="50" OnClientClick="javascript:window.close(); return false;" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Serial / Lot #s
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <table>
        <tr id="trLotQty" runat="server" visible="false">
            <td colspan="3" style="float: none" align="center">
                <table width="auto">
                    <tr>
                        <td>
                            <asp:Label ID="lblQty" runat="server" Font-Bold="true" Text="Quantity"></asp:Label></td>
                        <td>
                            <asp:TextBox ID="txtQty" runat="server" Width="50"></asp:TextBox></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; font-size: 12px" align="center">Available Serial / Lot #s</td>
            <td></td>
            <td style="font-weight: bold; font-size: 12px" align="center">Selected Serial / Lot #s</td>
        </tr>
        <tr>
            <td>
                <asp:ListBox ID="lstAvailablefld" runat="server" Style="min-width: 180px; min-height: 200px;"></asp:ListBox>
            </td>
            <td valign="middle" align="center">
                <input type="button" id="btnAdd" class="button" value="Add >" onclick="return Add();" />
                <br />
                <br />
                <input type="button" id="btnRemove" class="button" value="< Remove" onclick="return Remove();" />
            </td>
            <td>
                <asp:ListBox ID="lstSelectedfld" runat="server" Style="min-width: 180px; min-height: 200px;"></asp:ListBox>
            </td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td align="center">
                <asp:Label ID="lblMaxQty" Font-Bold="true" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnQty" runat="server" />
    <asp:HiddenField ID="hdnOppID" runat="server" />
    <asp:HiddenField ID="hdnOppItemID" runat="server" />
    <asp:HiddenField ID="hdnItemID" runat="server" />
    <asp:HiddenField ID="hdnWarehouseItemID" runat="server" />
    <asp:HiddenField ID="hdnItemType" runat="server" />
    <asp:HiddenField ID="hdnSelectedItems" runat="server" />
    <asp:HiddenField ID="hdnType" runat="server" />
    <asp:HiddenField ID="hdnRowID" runat="server" />
</asp:Content>
