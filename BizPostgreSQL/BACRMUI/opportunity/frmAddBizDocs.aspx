<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmAddBizDocs.aspx.vb"
    Inherits="BACRM.UserInterface.Opportunities.frmAddBizDocs" %>

<%@ Import Namespace="BACRM.BusinessLogic.Common" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="~/CSS/biz.css" type="text/css" />
    <link rel="stylesheet" href="~/CSS/biz-theme.css" type="text/css" />
    <link rel="stylesheet" href="~/CSS/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <link href="../CSS/GridColorScheme.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script src="../JavaScript/jquery.min.js" type="text/javascript"></script>
    <script src="../JavaScript/jquery-ui.min.js" type="text/javascript"></script>
    <script src="../JavaScript/jquery.Tooltip.js" type="text/javascript"></script>
    <script type="text/javascript" src="../JavaScript/dateFormat.js"></script>
    <title>BizDocs</title>
    <script type="text/javascript">
        function Close() {
            window.close();
        }

        var IsAddClicked = false;

        function ToggleRecurringVisibility(chk) {
            if (chk.checked) {
                $("[id$=trRecurring2]").show();
            } else {
                $("[id$=trRecurring2]").hide();
            }

            return false;
        }

        function Save(format) {

            if (IsAddClicked == true) {
                return false;
            }

            if (document.Form1.ddlBizDocs.value == 0) {
                alert("Select BizDoc")
                document.Form1.ddlBizDocs.focus();
                return false;
            }

            for (i = 1; i < document.getElementById('dgBizDocs').rows.length; i++) {
                if (i <= 8) {
                    index = '0' + (i + 1)
                }
                else {
                    index = (i + 1);
                }
                if (document.getElementById('dgBizDocs_ctl' + index + '_CalRentalStartDate_txtDate') != null && document.getElementById('dgBizDocs_ctl' + index + '_CalRentalReturnDate_txtDate') != null) {
                    if (document.getElementById('dgBizDocs_ctl' + index + '_CalRentalStartDate_txtDate').value == 0) {
                        alert("Enter Rental Start Date")
                        document.getElementById('dgBizDocs_ctl' + index + '_CalRentalStartDate_txtDate').focus();
                        return false;
                    }

                    if (document.getElementById('dgBizDocs_ctl' + index + '_CalRentalReturnDate_txtDate').value == 0) {
                        alert("Enter Rental Return Date")
                        document.getElementById('dgBizDocs_ctl' + index + '_CalRentalReturnDate_txtDate').focus();
                        return false;
                    }

                    if (!isDate(document.getElementById('dgBizDocs_ctl' + index + '_CalRentalStartDate_txtDate').value, format)) {
                        alert("Enter Valid Rental Start Date");
                        document.getElementById('dgBizDocs_ctl' + index + '_CalRentalStartDate_txtDate').focus();
                        return false;
                    }

                    if (!isDate(document.getElementById('dgBizDocs_ctl' + index + '_CalRentalReturnDate_txtDate').value, format)) {
                        alert("Enter Valid Rental Return Date");
                        document.getElementById('dgBizDocs_ctl' + index + '_CalRentalReturnDate_txtDate').focus();
                        return false;
                    }

                    if (compareDates(document.getElementById('dgBizDocs_ctl' + index + '_CalRentalStartDate_txtDate').value, format, document.getElementById('dgBizDocs_ctl' + index + '_CalRentalReturnDate_txtDate').value, format) == 1) {
                        alert("Rental Return Date must be greater than or equal to Rental Start Date");
                        return false;
                    }
                }
            }
            //Added By :Sachin Sadhu||Date:10thMarch2014
            //Bug:To prevent from multiple clicks of button-Save
            document.getElementById('btnSave').style.visibility = "hidden";
            //End
            return true;
        }
        function CheckQtyFulfilled(a, b) {

            if (isNaN(document.getElementById(b).value) == false) {
                if (parseInt(document.getElementById(b).value) > parseInt(a)) {
                    alert("Please enter a value less than or equal to the quantity needs to be fulfilled")
                    document.getElementById(b).value = a
                }
            }
            else {
                if (document.getElementById(b).value != '') {
                    alert("Please enter only numeric values")
                    document.getElementById(b).value = a
                }
            }
        }

        function openReportList(a, b) {
            window.open('../opportunity/frmShippingReportList.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=' + a + '&OppBizDocId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=680,height=250,scrollbars=yes,resizable=yes');
        }
        function openShippingLabel(a, b) {
            var text = $find('radShipCompany').get_text();
            window.open('../opportunity/frmShippingBox.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=' + a + '&OppBizDocId=' + b + '&ShipCompID=' + $("#hdnShippingService").val() + '&ShipCompName=' + text, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=1000,height=350,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenOpp(a, b) {
            var str;
            str = "../opportunity/frmOpportunities.aspx?frm=OpenBizDoc&OpID=" + a + "&OppType=" + b;
            parent.top.frames[0].frames[1].location.href = str;
            return false;
        }
        function openeditAddress(a, b) {
            window.open('../opportunity/frmEditOppAddress.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&AddType=' + a + '&OppID=' + b, '', 'toolbar=no,titlebar=no,left=100,top=350,width=500,height=350,scrollbars=yes,resizable=yes')
            return false;
        }
        function RefereshParentPage() {
            document.location.reload(true);
            return false;
        }
        function HideSalesOrderRecurrence() {

            if (window.parent != null && window.parent.document != null) {
                $("[id$=tblRecurInsert]", window.parent.document).hide();
                $("[id$=tblRecurDetail]", window.parent.document).hide();
                $("[id$=hdnRecurrenceType]", window.parent.document).val("Invoice");
            }

            return false;
        }

        function OpenBizDoc(a) {
            if (parseInt(document.getElementById('ddlOrderBizDocs').value) > 0) {

                window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + document.getElementById('ddlOrderBizDocs').value, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            }
            return false;
        }
        function CheckBoxClicked(operation) {
            if (operation == 'Add') {
                document.getElementById('chkAddSalesTax').checked = true;
                document.getElementById('chkRemoveTax').checked = false;
            }

            if (operation == 'Remove') {
                document.getElementById('chkAddSalesTax').checked = false;
                document.getElementById('chkRemoveTax').checked = true;
            }
        }
        function PostEmbdCost(a, b) {
            window.open('../opportunity/frmEmbeddedCost.aspx?OppID=' + b + '&OppBizDocId=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=1000,height=450,scrollbars=yes,resizable=yes');
        }
        function CopyEmbdCost(a, b) {
            window.open('../opportunity/frmEmbeddedCostCopy.aspx?OppID=' + b + '&OppBizDocId=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=800,height=250,scrollbars=yes,resizable=yes');
        }
        function OpenConfSerItem(a, b, c, d, e, f) {
            window.open('../opportunity/frmAddSerializedItem.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID=' + a + '&ItemCode=' + b + '&OppType=' + c + '&OppBizDocId=' + d + '&Qty=' + document.getElementById(e).value + '&LotId=' + f, '', 'toolbar=no,titlebar=no,left=100,top=100,width=530,height=400,scrollbars=yes,resizable=yes')
            return false;
        }
        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }

        function OpenRecurrenceDetail() {
            window.open('../opportunity/frmRecurrenceDetail.aspx?RecurConfigID=' + $("#hdnRecConfigID").val(), '', 'toolbar=no,titlebar=no,left=300,top=100,width=700,height=350,scrollbars=yes,resizable=no')
            
            return false;
        }
        function OpenBizInvoice(a, b,c) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            window.location.href = "../opportunity/frmAddBizDocs.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=" + a + "&OppBizId=" + b + "&OppType=" + c;
            //location.reload();
            return false;
        }
    </script>
    <style type="text/css">
        .tblBizDocDetail td {
            width: auto;
            white-space: nowrap;
        }

        .tblBizDocDetailtr {
            height: 30px;
        }

        .tblBizDocDetail td:nth-child(1) {
            font-weight: bold;
        }

        .tblBizDocDetail td:nth-child(3) {
            font-weight: bold;
        }

        .tblBizDocDetail td:nth-child(2) {
            padding-right: 50px;
        }

        input[type=checkbox] {
            vertical-align: middle;
            position: relative;
            bottom: 1px;
        }

        .tblRecurDetail {
            margin: 0px;
            padding: 0px;
            border-spacing: 0px;
            border-collapse: collapse;
        }

            .tblRecurDetail th {
                background: #e5e5e5;
                border-bottom: 1px solid #cbcbcb;
                border-top: 1px solid #cbcbcb;
                border-right: 1px solid #cbcbcb;
                border-left: 1px solid #cbcbcb;
                font-weight: bold;
                height: 25px;
            }

            .tblRecurDetail td {
                border: 1px solid #cbcbcb;
                text-align: center;
            }
    </style>
</head>
<body class="_<%=BACRM.BusinessLogic.Common.CCommon.ToString(Session("vcThemeClass")) %>">
    <form id="Form1" method="post" runat="server">
        <asp:ScriptManager ID="scrBizdocs" runat="server">
        </asp:ScriptManager>
        <div class="alert alert-warning" id="divAlert" runat="server" visible="false">
            <h4><i class="icon fa fa-warning"></i>Alert!</h4>
            <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            <asp:LinkButton ID="btnAllowBizDocSequenceId" runat="server" Visible="false">Yes</asp:LinkButton>
        </div>
        <div class="row padbottom10">
            <div class="col-xs-12 col-md-8">
                <div class="col-xs-12" runat="server" id="divCreated">
                    <div class="form-group col-xs-12 col-md-6">
                        <div>
                            <b>Created On:</b>&nbsp;&nbsp;<asp:Label ID="lblCreatedOn" runat="server"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group col-xs-12 col-md-6">
                        <div>
                            <b>Last Modified By:</b>&nbsp;&nbsp;<asp:Label ID="lblLastModifiedBy" runat="server" ForeColor="Black"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="pull-right">
                    <asp:LinkButton ID="btnUpdateBizDocFields" runat="server" CssClass="btn btn-primary" Visible="false"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                    <asp:LinkButton ID="btnAddOnly" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Add</asp:LinkButton>
                    <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Add & Open</asp:LinkButton>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:LinkButton ID="btnSaveCloseWindow" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close</asp:LinkButton>
                    <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                    <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-primary"><i class="fa fa-times"></i>&nbsp;&nbsp;Close</asp:LinkButton>
                </div>
            </div>
        </div>
        <div class="row">
               <div class="col-xs-12 col-sm-6 col-md-2">
                <div class="form-group">
                    <label>BizDoc ID</label>
                    <div>
                        <asp:Label ID="lblBizDocName" runat="server"></asp:Label>
                        <asp:TextBox ID="txtBizDocSequenceId" runat="server" CssClass="form-control" MaxLength="10"></asp:TextBox>
                        <asp:HiddenField ID="hdnBizDocSequenceId" runat="server" />
            </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-2">
                <div class="form-group">
                    <label>BizDoc Template</label>
                        <asp:DropDownList ID="ddlBizDocTemplate" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>
           <div class="col-xs-12 col-sm-6 col-md-2">
                    <div class="form-group">
                        <label>BizDoc Status</label>
                        <asp:DropDownList ID="ddlBizDocStatus" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>
            <div class="col-xs-12 col-sm-6 col-md-2">
                <div class="form-group">
                    <label>Billing Date</label>
                    <div class="form-inline">
                        <BizCalendar:Calendar ID="calFrom" runat="server" />
                        <asp:CheckBox runat="server" ID="chkFromDate" CssClass="signup" Font-Bold="true" Style="line-height: 20px;" Text="Create on Billing date" Visible="false" />
                    </div>
                </div>
            </div>
            
            <div class="col-xs-12 col-sm-6 col-md-2">
                <div class="form-group">
                    <label>BizDoc Type</label>
                        <asp:DropDownList ID="ddlBizDocs" runat="server" AutoPostBack="true" CssClass="form-control">
                        </asp:DropDownList>
                </div>
            </div>
            
   
            <asp:Label ID="lblCompany" runat="server" CssClass="text" Visible="false"></asp:Label>
            <asp:Label ID="lblContact" runat="server" Visible="false" CssClass="text"></asp:Label>
            <asp:HyperLink ID="hplOrderName" runat="server" NavigateUrl="#" Visible="false"></asp:HyperLink>
        </div>
        <div class="row">
            
            <div class="col-xs-12 col-sm-6 col-md-2">
                <div class="form-group">
                    <label>Shipping Company</label>
                    <div>
                        <telerik:RadComboBox runat="server" ID="radShipCompany" Skin="Default" AccessKey="I" Width="100%"
                            EnableVirtualScrolling="false" ChangeTextOnKeyBoardNavigation="false" ClientIDMode="Static">
                        </telerik:RadComboBox>
                        <asp:Button ID="btnShippingLabel" runat="server" CssClass="btn btn-primary" Text="Generate Shipping Labels & Tracking #s" />
                    </div>
                </div>
            </div>
                <div class="col-xs-12 col-sm-6 col-md-2">
                    <div class="form-group">
                        <label>Tracking No</label>
                        <div>
                            <asp:TextBox ID="txtTrackingNo" runat="server" CssClass="form-control" MaxLength="500"></asp:TextBox>
                            <asp:HyperLink ID="hplShippingReport" runat="server"></asp:HyperLink>
                            <asp:Label ID="lblShipAmount" Width="40" runat="server"></asp:Label>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-2">
                    <div class="form-group">
                        <label>
                            <asp:Label Text="P.O #" runat="server" ID="lblRefOrderNo" /></label>
                        <asp:TextBox ID="txtRefOrderNo" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                    </div>
                </div>
              
            <div class="col-xs-12 col-sm-6 col-md-2">
                    <div class="form-group">
                        <label>Include single release date</label>
                        <asp:DropDownList ID="ddlReleaseDates" runat="server" CssClass="form-control" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
            <div class="col-xs-12 col-sm-6 col-md-2">
                <div class="form-group" id="divAR" runat="server" visible="false">
                    <label>A/R</label>
                    <asp:DropDownList ID="ddlAR" runat="server" CssClass="form-control"></asp:DropDownList>
                </div>
            </div>
                <div class="col-xs-12 col-sm-6 col-md-2">
                    <div class="form-group  text-center" runat="server" id="trCurrency">
                        <label>Exchange Rate</label>
                        <div class="form-inline">
                            <asp:Label runat="server" ID="lblForeignCurr"></asp:Label>&nbsp;=&nbsp;
                        <asp:TextBox ID="txtExchangeRate" runat="server" CssClass="form-control" Width="60px"></asp:TextBox>&nbsp;&nbsp;<asp:Label runat="server" ID="lblBaseCurr" Text="USD"></asp:Label>
                        </div>
                    </div>
                </div>
                <div id="Div1" class="col-xs-12 col-sm-6 col-md-2" runat="server">
                    <div class="form-group" runat="server" id="divVendorInvName">
                        <label>Vendor Invoice Name</label>
                        <div class="form-inline">
                            <asp:TextBox ID="txtVendorInvName" runat="server" CssClass="form-control" MaxLength="64"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <asp:Label ID="lblFulfillmentCompletedCaption" runat="server" Text="Fulfillment Completed :" Visible="false"></asp:Label>
                <asp:CheckBox ID="chkPartial" runat="server" Text="Partial" AutoPostBack="true" Visible="false" Checked="true" />
                <asp:Label ID="lblFulfillmentCompleted" runat="server" Visible="false"></asp:Label>
            </div>

        <asp:Panel ID="tblRecurInsert" runat="server" CssClass="row">
            <div class="col-xs-12">
                <div class="row" id="trRecurring1" runat="server">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label>
                                Recur
                            </label>
                            <div>
                                <asp:CheckBox ID="chkEnableRecurring" onClick="ToggleRecurringVisibility(this);" runat="server" />
                                <asp:Label ID="Label1" Text="[?]" CssClass="tip" runat="server" ToolTip="Note that each line item will be added to each new invoice with a qty of item selected in recurring invoice until qty left to invoice reaches 0 for item." />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row" id="trRecurring2" runat="server" style="display: none;">
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label>Frequency</label>
                            <asp:DropDownList ID="ddlFrequency" runat="server" CssClass="form-control">
                                <asp:ListItem Text="Daily" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Weekly" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Monthly" Value="3"></asp:ListItem>
                                <asp:ListItem Text="Quarterly" Value="4"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label>Start Date</label>
                            <BizCalendar:Calendar ID="calRecurStart" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div class="row" runat="server" visible="false" id="tblRecurDetail">
            <div class="table-responsive">
                <table class="table table-bordered table-striped" width="100%">
                    <tr>
                        <th colspan="4">
                            <a href="javascript:OpenRecurrenceDetail()">Recurrence Detail</a>
                            <asp:Button ID="btnUpdateRecurrence" Style="float: right" CssClass="button" runat="server" Text="Update" />
                        </th>
                    </tr>
                    <tr>
                        <th>Frequency</th>
                        <th>Start Date</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblFrequency" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:Label ID="lblStartDate" runat="server"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlRecurStatus" runat="server" Width="120">
                                <asp:ListItem Text="Enabled" Value="1" />
                                <asp:ListItem Text="Disabled" Value="2" />
                            </asp:DropDownList>
                            <asp:Label ID="lblRecurStatus" Text="[?]" CssClass="tip" runat="server" ToolTip="You have to delete this recurrence and create new one to enable recurrence again." Visible="false" />
                        </td>
                        <td>
                            <asp:Button ID="btnDeleteRecurrence" runat="server" CssClass="btn btn-danger" Text="X"></asp:Button>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12" id="divGrandTotal" runat="server" visible="false">
                <table class="table table-bordered table-striped" style="margin-bottom: 0px; text-align: right;">
                    <tbody>
                        <tr>
                            <td>
                                <asp:Label ID="lblGrandTotalRef" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-xs-12">
                <div class="table-responsive">
                    <asp:DataGrid ID="dgBizDocs" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered table-striped" UseAccessibleHeader="true" Visible="false">
                        <Columns>
                            <asp:BoundColumn DataField="numOppItemtCode" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numItemCode" HeaderText="Item ID"></asp:BoundColumn>
                            <asp:BoundColumn DataField="vcItemName" HeaderText="Item"></asp:BoundColumn>
                            <asp:BoundColumn DataField="vcModelID" HeaderText="Model ID" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn DataField="QtyOrdered" HeaderText="Qty Ordered"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Qty to Add">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtQuantityFulfilled" runat="server" Width="60" CssClass="signup"
                                        Text='<%#String.Format("{0:0.################}", DataBinder.Eval(Container.DataItem, "QtytoFulFilled"))%>'></asp:TextBox>
                                    <asp:HiddenField ID="hfItemType" runat="server" Value='<%# Eval("charItemType") %>' />
                                    <asp:HiddenField ID="hdnQtyShipped" runat="server" Value='<%# Eval("numQtyShipped") %>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="QtytoFulFill" HeaderText="Qty Remaining" DataFormatString="{0:0.################}"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numQtyShipped" HeaderText="Qty Shipped"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numQtyReceived" HeaderText="Qty Received"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numAllocation" HeaderText="Warehouse / Allocation"></asp:BoundColumn>
                            <asp:BoundColumn DataField="numBackOrder" HeaderText="Qty on Backorder"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Notes">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtNotes" TextMode="MultiLine" runat="server" Width="150" CssClass="signup" Text='<%#DataBinder.Eval(Container.DataItem, "vcNotes")%>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Include in BizDoc">
                                <HeaderTemplate>
                                    <ul class="list-inline" style="margin-bottom: 0px">
                                        <li>Include
                                        </li>
                                        <li>[%]
                                        </li>
                                        <li>Included
                                        </li>
                                    </ul>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <ul class="list-inline" style="margin-bottom: 0px">
                                        <li style="width: 60px;padding-left: 35px;"><asp:CheckBox ID="chkInclude" Checked='<%#DataBinder.Eval(Container.DataItem, "Included")%>' runat="server" /></li>
                                        <li><asp:TextBox runat="server" ID="txtRemainingPercent" CssClass="signup" Width="40" Text='<%# Eval("numRemainingPercent")%>'></asp:TextBox>
                                            <asp:HiddenField runat="server" ID="hdnRemainingPercent" Value='<%# Eval("numRemainingPercent")%>' />
                                            <asp:HiddenField runat="server" ID="hdnQtyOrdered" Value='<%# Eval("QtyOrdered")%>' />
                                        </li>
                                        <li><asp:Label runat="server" ID="lblIncludedPercent" Text='<%# Eval("numIncludedPercent") & "%"%>'></asp:Label></li>
                                    </ul>
                                    
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="charItemType" HeaderText="ItemType" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Serial/Lot #s" Visible="false">
                                <ItemTemplate>
                                    <asp:TextBox ID="txLot" runat="server" CssClass="signup" Text='<%# Eval("SerialLotNo") %>'></asp:TextBox>&nbsp;
                                <asp:HyperLink ID="hlSerialized" runat="server" CssClass="BizLink">Add</asp:HyperLink>
                                    <asp:HiddenField ID="hfbitSerialized" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "bitSerialized")%>' />
                                    <asp:HiddenField ID="hfbitLotNo" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "bitLotNo")%>' />
                                    <asp:HiddenField ID="hfnumWarehouseItmsID" runat="server" Value='<%# Eval("numWarehouseItmsID") %>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Rental Start Date">
                                <ItemTemplate>
                                    <BizCalendar:Calendar ID="CalRentalStartDate" runat="server" ShowTime="12" SelectedDate='<%#DataBinder.Eval(Container.DataItem, "dtRentalStartDate")%>' />
                                    <asp:HiddenField ID="hfnumItemCode" runat="server" Value='<%# Eval("numItemCode") %>' />
                                    <asp:HiddenField ID="hfnumUOM" runat="server" Value='<%# Eval("numUOM") %>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Rental Return Date">
                                <ItemTemplate>
                                    <BizCalendar:Calendar ID="CalRentalReturnDate" runat="server" ShowTime="12" SelectedDate='<%#DataBinder.Eval(Container.DataItem, "dtRentalReturnDate")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Unit Price" ItemStyle-Wrap="false" HeaderStyle-Width="150">
                                <ItemTemplate>
                                    <asp:Label ID="lblUnitPrice" runat="server" CssClass="signup" Text='<%#String.Format(CCommon.GetDataFormatString(), Eval("monOppItemPrice"))%>'></asp:Label>
                                    <asp:TextBox ID="txtUnitPrice" Style="visibility: hidden" runat="server" CssClass="signup" Text='<%#Eval("monOppBizDocPrice")%>'></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Total">
                                <ItemTemplate>
                                    <asp:Label ID="lblTotal" runat="server" CssClass="signup" Text='<%#String.Format(CCommon.GetDataFormatString(), Eval("monOppBizDocAmount"))%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                </div>
            </div>

        </div>
        <asp:HiddenField ID="hdnComments" runat="server" />
        <asp:HiddenField ID="hdnPONumber" runat="server" />
        <asp:HiddenField ID="hdnOppBizDocID" runat="server" />
        <asp:HiddenField ID="hdnAuthorizativeBizDocsId" runat="server" />
        <asp:HiddenField ID="hdnISRentalBizDoc" runat="server" />
        <asp:HiddenField ID="hftintshipped" runat="server" />
        <asp:HiddenField ID="hfnumDiscountAcntType" runat="server" Value="0" />
        <asp:HiddenField ID="hdnRecurrenceType" runat="server" />
        <asp:HiddenField ID="hdnRecConfigID" runat="server" />
        <asp:HiddenField ID="hdnRecurredBizDoc" runat="server" />
        <asp:HiddenField ID="hdnFulFilledItem" runat="server" />
        <asp:HiddenField ID="hdnDeferredBizDocID" runat="server" />
        <asp:HiddenField runat="server" ID="hdnReplationship" />
        <asp:HiddenField runat="server" ID="hdnProfile" />
        <asp:HiddenField runat="server" ID="hdnAccountClass" />
        <asp:HiddenField runat="server" ID="hdnShippingService" />
        <asp:HiddenField runat="server" ID="hdnCommitAllocation" />
        <asp:HiddenField runat="server" ID="hdnAllocateOnPickList" />
        <asp:HiddenField runat="server" ID="hdnFulfillmentGeneratedOnPickList" />
        <asp:HiddenField runat="server" ID="hdnInvoiceForDiferredBizDoc" />
    </form>
</body>
</html>
