﻿Imports nsoftware.InShip
Imports BACRM.BusinessLogic.Common

Public Class frmShippingStatus
    Inherits BACRMPage
    Dim _FedexTrack As nsoftware.InShip.Fedextrack = New Fedextrack()
    Dim _UpsTrack As nsoftware.InShip.Upstrack = New Upstrack()
    Dim lngShippingCompanyId As Long = 0
    Dim trackingno As String = ""
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ErrorMessage As String = ""
        lngShippingCompanyId = GetQueryStringVal("companyId")
        trackingno = GetQueryStringVal("trackingno")
        _UpsTrack.IdentifierType = UpstrackIdentifierTypes.uitMasterTrackingNumber
        _UpsTrack.ShipDateStart = "20010219"
        'yyyMMdd
        _UpsTrack.ShipDateEnd = "20190228"
        'yyyMMdd
        NSAPIDETAILS(lngShippingCompanyId, _FedexTrack, _UpsTrack)

        '_FedexTrack.TrackShipment("956473415129602");
        If lngShippingCompanyId = 88 Then
            Try
                _UpsTrack.TrackShipment(trackingno)
                Dim CarrierCodeDescription As String = _UpsTrack.PackageReferences
                Dim ShipDate As String = _UpsTrack.ShipDate
                Dim PackageTrackingNumber As String = _UpsTrack.PackageTrackingNumber
                Dim PackageServiceTypeDescription As String = _UpsTrack.ServiceTypeDescription
                Dim PackagePackagingType As String = _UpsTrack.ServiceTypeDescription
                Dim PackageDeliveryStatus As String = "-"
                Dim PackageDeliveryEstimate As String = "-"
                Dim PackageDeliveryDate As String = "-"
                Dim PackageDeliveryLocation As String = "-"
                If _UpsTrack.TrackEvents.Count > 0 Then
                    Dim count As Integer = 0
                    PackageDeliveryStatus = _UpsTrack.TrackEvents(count).Status
                    Dim [date] As String = Convert.ToString(_UpsTrack.TrackEvents(count).[Date])
                    'if (date != "")
                    '{
                    '    date = ReturnDateTime(Convert.ToDateTime(date));
                    '}
                    PackageDeliveryEstimate = Convert.ToString(([date] & Convert.ToString(" ")) + Convert.ToString(_UpsTrack.TrackEvents(count).Time) + " ") & PackageDeliveryStatus
                    PackageDeliveryDate = _UpsTrack.TrackEvents(count).[Date]

                    PackageDeliveryLocation = _UpsTrack.TrackEvents(count).Location + " " + _UpsTrack.TrackEvents(count).City + " " + _UpsTrack.TrackEvents(count).State + " " + _UpsTrack.TrackEvents(count).CountryCode
                End If
                Dim PackageSignedBy As String = _UpsTrack.PackageSignedBy
                lblStatus.Text = PackageDeliveryStatus
                lblLocation.Text = PackageDeliveryLocation
                lblShipCarrier.Text = PackagePackagingType
                lblTrackingId.Text = PackageTrackingNumber
                lblLatestEvent.Text = PackageDeliveryEstimate
                lblShipmentText.Text = PackageDeliveryStatus
            Catch
                ErrorMessage = "Tracking no is Invalid"
                lblStatus.Text = "-"
                lblLocation.Text = "-"
                lblShipCarrier.Text = "-"
                lblTrackingId.Text = "-"
                lblLatestEvent.Text = "-"
                lblShipmentText.Text = "-"
            End Try
        Else
            Try
                _FedexTrack.TrackShipment(trackingno)
                Dim CarrierCodeDescription As String = _FedexTrack.CarrierCodeDescription
                Dim ShipDate As String = _FedexTrack.ShipDate
                Dim PackageTrackingNumber As String = _FedexTrack.PackageTrackingNumber
                Dim PackageServiceTypeDescription As String = _FedexTrack.PackageServiceTypeDescription

                Dim PackageDeliveryStatus As String = _FedexTrack.PackageDeliveryStatus
                Dim PackageDeliveryEstimate As String = _FedexTrack.PackageDeliveryEstimate
                Dim PackageDeliveryDate As String = _FedexTrack.PackageDeliveryDate
                Dim PackageDeliveryLocation As String = "-"
                Dim PackagePackagingType As String = "-"
                Dim PackageSignedBy As String = _FedexTrack.PackageSignedBy
                Dim latestUpdateOn As String = ""
                If _FedexTrack.TrackEvents.Count > 0 Then
                    Dim count As Integer = 0
                    PackageDeliveryLocation = _FedexTrack.TrackEvents(count).City + " " + _FedexTrack.TrackEvents(count).CountryCode
                    PackagePackagingType = _FedexTrack.TrackEvents(count).Status
                    latestUpdateOn = _FedexTrack.TrackEvents(count).[Date]
                End If
                If PackageDeliveryEstimate <> "" Then
                    PackageDeliveryEstimate = ReturnDateTime(Convert.ToDateTime(PackageDeliveryEstimate))
                End If
                If PackageDeliveryDate <> "" Then
                    PackageDeliveryDate = ReturnDateTime(Convert.ToDateTime(PackageDeliveryDate))
                End If
                lblStatus.Text = PackageDeliveryStatus
                lblLocation.Text = PackageDeliveryLocation
                lblShipCarrier.Text = PackageServiceTypeDescription
                lblTrackingId.Text = PackageTrackingNumber
                lblLatestEvent.Text = PackagePackagingType
                lblShipmentText.Text = Convert.ToString((Convert.ToString(latestUpdateOn & Convert.ToString(",")) & PackagePackagingType) + ",") & PackageDeliveryLocation
            Catch
                ErrorMessage = "Tracking no is Invalid"
                lblStatus.Text = "-"
                lblLocation.Text = "-"
                lblShipCarrier.Text = "-"
                lblTrackingId.Text = "-"
                lblLatestEvent.Text = "-"
                lblShipmentText.Text = "-"
            End Try
        End If
    End Sub


    Public Function ReturnDateTime(CloseDate As [Object]) As String
        Try
            Dim strTargetResolveDate As String = ""
            Dim temp As String = ""

            If Not (CloseDate = System.DBNull.Value) Then
                Dim dtCloseDate As DateTime = DateTime.Parse(CloseDate.ToString())
                strTargetResolveDate = FormattedDateFromDate(dtCloseDate, Session("DateFormat"))

                Dim timePart As String = dtCloseDate.ToShortTimeString().Substring(0, dtCloseDate.ToShortTimeString().Length - 1)
                ' remove gaps
                If timePart.Split(" "c).Length >= 2 Then
                    timePart = timePart.Split(" "c).GetValue(0).ToString() + timePart.Split(" "c).GetValue(1).ToString()

                    ' check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    ' if both are same it is today
                    'if ((dtCloseDate.Date == DateTime.Now.Date & dtCloseDate.Month == DateTime.Now.Month & dtCloseDate.Year == DateTime.Now.Year))
                    '{
                    '    strTargetResolveDate = "<font color=red><b>Today</b></font>";

                    '    return strTargetResolveDate;
                    '}
                    ' check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter dtCloseDate components [ Date , Month , Year ] 
                    ' if both are same it was Yesterday
                    'else if ((dtCloseDate.Date.AddDays(1).Date == DateTime.Now.Date & dtCloseDate.AddDays(1).Month == DateTime.Now.Month & dtCloseDate.AddDays(1).Year == DateTime.Now.Year))
                    '{
                    '    strTargetResolveDate = "<font color=purple><b>Yesterday</b></font>";

                    '    return strTargetResolveDate;
                    '}
                    ' check TodayDate .... components [ Date , Month , Year ] with Parameter [ dtCloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                    ' if both are same it will Tomorrow
                    'else if ((dtCloseDate.Date == DateTime.Now.AddDays(1).Date & dtCloseDate.Month == DateTime.Now.AddDays(1).Month & dtCloseDate.Year == DateTime.Now.AddDays(1).Year))
                    '{
                    '    temp = dtCloseDate.Hour.ToString() + ":" + dtCloseDate.Minute.ToString();
                    '    strTargetResolveDate = "<font color=orange><b>Tomorrow</b></font>";

                    '    return strTargetResolveDate;
                    '}
                    ' display day name for next 4 days from DateTime.Now

                    'else if (dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(2).ToString("yyyyMMdd") | dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(3).ToString("yyyyMMdd") | dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(4).ToString("yyyyMMdd") | dtCloseDate.ToString("yyyyMMdd") == DateTime.Now.AddDays(5).ToString("yyyyMMdd"))
                    '{
                    '    strTargetResolveDate = "<b>" + dtCloseDate.DayOfWeek.ToString() + "</b>";
                    '    return strTargetResolveDate;
                    '}
                    'else
                    '{
                    '    //strTargetResolveDate = strTargetResolveDate;
                    '    return strTargetResolveDate;
                    '}
                End If
            End If
            Return strTargetResolveDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub NSAPIDETAILS(lngShippingCompanyId As Long, _FedexTrack As nsoftware.InShip.Fedextrack, _UpsTrack As nsoftware.InShip.Upstrack)
        Try
            Dim objItem As New BACRM.BusinessLogic.Item.CItems()
            Dim dsShippingDtl As DataSet = Nothing
            objItem.DomainID = Session("DomainID")
            objItem.ShippingCMPID = lngShippingCompanyId
            dsShippingDtl = objItem.ShippingDtls()
            For Each dr As DataRow In dsShippingDtl.Tables(0).Rows
                Select Case dr("vcFieldName").ToString()

                    'Case "Rate Type"
                    '    If Rates1.Provider = EzratesProviders.pFedEx Then
                    '        Rates1.Config("RateType=" & dr("vcShipFieldValue").ToString())
                    '    End If


                    Case "Developer Key"
                        _FedexTrack.FedExAccount.DeveloperKey = dr("vcShipFieldValue").ToString()

                        Exit Select
                    Case "Account Number"
                        _FedexTrack.FedExAccount.AccountNumber = dr("vcShipFieldValue").ToString()

                        Exit Select
                    Case "Meter Number"
                        _FedexTrack.FedExAccount.MeterNumber = dr("vcShipFieldValue").ToString()
                        Exit Select
                        'case "Weight Unit":
                        '    if (dr["vcShipFieldValue"].ToString().ToLower().Equals("lbs"))
                        '    {
                        '        Rates1.Config("WeightUnit=LB");
                        '    }
                        '    else
                        '    {
                        '        Rates1.Config("WeightUnit=KG");
                        '    }

                        Exit Select
                        'Common fields
                    Case "Password"
                        _FedexTrack.FedExAccount.Password = dr("vcShipFieldValue").ToString()
                        _UpsTrack.UPSAccount.Password = dr("vcShipFieldValue").ToString()
                        Exit Select
                    Case "Server URL"
                        _FedexTrack.FedExAccount.Server = dr("vcShipFieldValue").ToString()

                        Exit Select
                    Case "Tracking URL"
                        _UpsTrack.UPSAccount.Server = dr("vcShipFieldValue").ToString()
                        Exit Select
                        'Fields specific to UPS
                    Case "AccessKey"
                        '_FedexTrack.FedExAccount.AccessKey = dr["vcShipFieldValue"].ToString();
                        _UpsTrack.UPSAccount.AccessKey = dr("vcShipFieldValue").ToString()
                        Exit Select
                    Case "UserID"
                        '_FedexTrack.FedExAccount.UserId = dr["vcShipFieldValue"].ToString();
                        _UpsTrack.UPSAccount.UserId = dr("vcShipFieldValue").ToString()
                        Exit Select
                    Case "ShipperNo"
                        _FedexTrack.FedExAccount.AccountNumber = dr("vcShipFieldValue").ToString()
                        _UpsTrack.UPSAccount.AccountNumber = dr("vcShipFieldValue").ToString()
                        Exit Select
                End Select
            Next
        Catch ex As Exception
            Throw ex
        End Try

    End Sub

End Class