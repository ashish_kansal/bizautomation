<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmBizDocs.aspx.vb"
    Inherits="BACRM.UserInterface.Opportunities.frmBizDocs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/bootstrap.min.css" type="text/css" />
    <link rel="stylesheet" href="~/CSS/biz.css" type="text/css" />
    <link rel="stylesheet" href="~/CSS/biz-theme.css" type="text/css" />
    <link rel="stylesheet" href="~/CSS/font-awesome.min.css" type="text/css" />
    <script src="../JavaScript/jquery.min.js" type="text/javascript"></script>
    <script src="../JavaScript/SalesFulfillment.js"></script>
    <title>BizDocs</title>
    <script type="text/javascript">
        function Close() {
            window.close();
            return false;
        }
        function AddBizDocs(a, b, c, d) {
            var URL = "frmAddBizDocs.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=" + a + '&OppType=' + b + '&bizDoc=' + c + '&bizDocTemplate=' + d + '&mode=add';
            window.location = URL;
            return false;
        }
        function OpenBizInvoice(a, b, c) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b + '&Print=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=850,height=700,scrollbars=yes,resizable=yes');
            return false;
        }

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function DeleteMessage1() {
            alert("Deleting authoritative bizdoc is not allowed once Deal/Order is completed!");
            return false;
        }

        function DeleteBizDoc(bizdocname) {
            alert("Deleting " + bizdocname  + " bizdoc is not allowed once Deal/Order is completed!");
            return false;
        }

        function InvoiceAgainstDiferredBizDoc() {
            alert("Deleting deferred income bizdoc is not allowed when invoice is generated against it. You have to first delete invoice generated.");
            return false;
        }

        function EditDeferredBizDocNotAllowed() {
            alert("Editing deferred income bizdoc is not allowed when invoice is generated against it.");
            return false;
        }

        function EditFulfillentBizDocNotAllowed() {
            alert("Editing fulfillment bizdoc is not allowed.");
            return false;
        }

        function DeleteRecurringBizDoc() {
            alert("Deleting bizdoc is not allowed when \n 1. BizDoc is recurring \n 2. BizDoc is recurred from another BizDoc!");
            return false;
        }

        function reDirect(a) {
            parent.location.href = a;
        }
        function OpenEdit(a, b, c, d, e) {
            window.location.href = '../opportunity/frmAddBizDocs.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b + '&OppType=' + c + '&bizDoc=' + d + '&bizDocTemplate=' + e + '&mode=edit';

            return false;
        }

        function Save() {
            if (document.Form1.ddlBizDocs.value == 0) {
                alert("Select BizDoc")
                document.Form1.ddlBizDocs.focus();
                return false;
            }
        }

        function OpenAmtPaid(a, b, c) {
            var ifrm = window.frameElement; // reference to iframe element container
            //console.log(ifrm);
            var doc = ifrm.ownerDocument; // reference to container's document
            //console.log(doc);

            doc.RPPopup = window.open('../opportunity/frmAmtPaid.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Popup=1&OppId=' + a + '&OppBizId=' + b + '&DivId=' + c, 'ReceivePayment', 'toolbar=no,titlebar=no,top=300,width=700,height=400,scrollbars=no,resizable=no');
            console.log(doc.RPPopup);
            return false;
        }

        function OpenRMAInvoice(a, b) {
            window.open('../opportunity/frmMirrorBizdoc.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&RefID=' + a + '&RefType=' + b + '&Print=0', '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }

        function openReportList(a, b, c) {
            var h = screen.height;
            var w = screen.width;

            if (c > 0) {
                window.open('../opportunity/frmShippingReport.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppBizDocId=' + b + '&ShipReportId=' + c + '&OppId=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
                return false;
            }
            else {
                window.open('../opportunity/frmShippingReportList.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=' + a + '&OppBizDocId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=680,height=250,scrollbars=yes,resizable=yes');
                return false;
            }
        }
        function openShippingLabel(a, b, c, d, e) {
            //var index = document.getElementById('ddlShipCompany').selectedIndex;
            if (c > 0) {
                window.open('../opportunity/frmShippingReport.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppBizDocId=' + b + '&ShipReportId=' + c + '&OppId=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=800,height=300,scrollbars=yes,resizable=yes');
                return false;
            }
            else {
                window.open('../opportunity/frmShippingBox.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=' + a + '&OppBizDocId=' + b + '&ShipCompID=' + d + '&ShipServiceID=' + e + '', '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=1000,height=350,scrollbars=yes,resizable=yes');
                return false;
            }
        }

        function CloneBizdoc(OppID, OppBizDocID, DivID, BizDocType, OppType) {
            window.open('../opportunity/frmCloneBizDoc.aspx?Popup=1&OppType=' + OppType + '&OppId=' + OppID + '&OppBizDocId=' + OppBizDocID + '&DivId=' + DivID + '&BizDocType=' + BizDocType, '', 'toolbar=no,titlebar=no,top=300,width=700,height=400,scrollbars=no,resizable=no');
            return false;
        }

        function HideSalesOrderRecurrence() {

            if (window.parent != null && window.parent.document != null) {
                $("[id$=tblRecurInsert]", window.parent.document).hide();
                $("[id$=tblRecurDetail]", window.parent.document).hide();
                $("[id$=hdnRecurrenceType]", window.parent.document).val("Invoice");
            }

            return false;
        }
        function OpenPickListSelectionWindow(oppID) {
            window.open('../opportunity/frmSelectPickList.aspx?OppID=' + oppID, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=850,height=500,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
</head>
<body class="_<%=BACRM.BusinessLogic.Common.CCommon.ToString(Session("vcThemeClass")) %>">
    <form id="Form1" method="post" runat="server">
        <div class="row">
            <div class="col-xs-12" style="margin-bottom: 10px;">
                <asp:Label ID="lblAuthorizedCard" class="btn btn-default btn-sm" runat="server" Text=""></asp:Label>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="pull-left">
                    <label>
                        <asp:Label ID="lblPendingItemQty" runat="server" Text=""></asp:Label></label>
                </div>
                <div class="pull-right">
                    <div class="form-inline">
                        <div class="form-group">
                            <label>BizDoc Type</label>
                            <asp:DropDownList ID="ddlBizDocs" runat="server" Width="200" AutoPostBack="true" CssClass="form-control"></asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <label>Template</label>
                            <asp:DropDownList ID="ddlBizDocTemplate" runat="server" CssClass="form-control" Width="200"></asp:DropDownList>
                        </div>
                        <div class="form-group" id="divAR" runat="server" visible="false">
                            <label>A/R</label>
                            <asp:DropDownList ID="ddlAR" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                        <div class="form-group" id="pnlInvoiceDeferredIncome" runat="server" visible="false">
                            <label>
                            </label>
                            <asp:CheckBox ID="chkCreateInvoiceForDeferred" runat="server" Text="Invoice for deferred income" Font-Bold="true" />
                            <asp:Label ID="Label18" Text="[?]" CssClass="tip" runat="server" ToolTip="Checking this will create invoice with same line items and qty as added in deferred income bizdoc otherwise it will created invoice with remaining items and qty which are left to invoiced. (e.g. say you have an SO with 1 line item with 10 units, and  you invoice it for 6 units and then added deferred income bizdoc of 3 units. Now if you check this checkbox it will create an invoice of 3 units otherwise it will create an invoice of 1 units)." />
                        </div>

                        <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Quick Add"></asp:Button>
                        <asp:Button ID="btnAdd" runat="server" Text="Add New BizDoc" CssClass="btn btn-primary"></asp:Button>
                    </div>
                </div>
                <%-- <div class="pull-right">
                    <asp:Button ID="btnAdd" runat="server" Text="Add New BizDoc" CssClass="btn btn-primary"></asp:Button>
                </div>--%>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
                <asp:Label ID="lblmsg" runat="server" Width="750px"></asp:Label>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <div class="table-responsive">
                    <asp:DataGrid ID="dgBizDocs" runat="server" Width="100%" CssClass="table table-bordered table-striped" AutoGenerateColumns="False" UseAccessibleHeader="true">
                        <Columns>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:Image ID="editImage" runat="server" ImageUrl="~/images/edit.png" ImageAlign="Middle" onclick='' CssClass="btn" />
                                    <asp:LinkButton ID="lnkEdit" runat="server" Visible="false"><font color="#730000">*</font></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="BizDoc Type / ID">
                                <ItemTemplate>
                                    <asp:HyperLink ID="hplBizdcoc" runat="server" NavigateUrl="#" Text='<%# DataBinder.Eval(Container.DataItem, "BizDoc") %>'>
                                    </asp:HyperLink>
                                    <asp:Label ID="lblBizDocID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numBizDocId") %>'>
                                    </asp:Label>
                                    (<%#Eval("BizDocType").ToString()%>) /
                                <asp:Label ID="lblBizDocName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vcBizDocID") %>'>

                                </asp:Label>
                                    (Source - <%#Eval("vcSourceBizDocID").ToString()%>)
                                    <br />
                                    <asp:HyperLink ID="hplParentBizDoc" runat="server" ForeColor="#6600CC" Font-Bold="true" Visible="false"></asp:HyperLink>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="BizDoc Template">
                                <ItemTemplate>
                                    <asp:Label ID="lblBizDocTemplateName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vcTemplateName") %>'>
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Billing Date / Due Date">
                                <ItemTemplate>
                                    <%# ReturnName(DataBinder.Eval(Container.DataItem, "BillingDate"))%>
                                /
                                <asp:Label ID="lblDuedate" runat="server">
                                </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Total Amt / Paid Amt / Due Amt" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <%#Eval("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", Eval("monPAmount"))%>
                                    <%# IIf(Convert.ToString(Eval("BaseCurrencySymbol")) <> Convert.ToString(Eval("varCurrSymbol")), "<span class='gray'>(" + Eval("BaseCurrencySymbol") + " " + String.Format("{0:###00.00}", Eval("monPAmount") * Eval("fltExchangeRateBizDoc")) + ")</span>", "")%>
                                &nbsp;/&nbsp;
                                <%#Eval("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", Eval("monAmountPaid"))%>
                                    <%# IIf(Convert.ToString(Eval("BaseCurrencySymbol")) <> Convert.ToString(Eval("varCurrSymbol")), "<span class='gray'>(" + Eval("BaseCurrencySymbol") + " " + String.Format("{0:###00.00}", Eval("monAmountPaid") * Eval("fltExchangeRateBizDoc")) + ")</span>", "")%>
                                &nbsp;/&nbsp;
                                <%#Eval("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", Eval("BalDue"))%>
                                    <%# IIf(Convert.ToString(Eval("BaseCurrencySymbol")) <> Convert.ToString(Eval("varCurrSymbol")), "<span class='gray'>(" + Eval("BaseCurrencySymbol") + " " + String.Format("{0:###00.00}", Eval("BalDue") * Eval("fltExchangeRateBizDoc")) + ")</span>", "")%>
                                &nbsp;-&nbsp;
                                    <%# IIf(hdnAuthorizeCreditCard.Value="1" AndAlso Convert.ToDecimal(Eval("BalDue"))>0,"Authorized","")  %>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <HeaderTemplate>
                                    <asp:Label Text="P.O #" runat="server" ID="lblRefOrderNo" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("vcRefOrderNo").ToString()%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <HeaderTemplate>
                                    Warehouse Location
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("vcLocation").ToString()%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="BizDoc Status" ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlBizDocStatus" runat="server" Width="200" CssClass="BizDocStatus">
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Actions" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnShippingVisible" Value='<%# DataBinder.Eval(Container.DataItem, "bitShippingGenerated") %>' runat="server" />
                                    <asp:TextBox ID="txtConEmail" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "vcEmail") %>' />
                                    <asp:TextBox ID="txtConID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numContactid") %>' />
                                    <asp:TextBox ID="txtBizDocTemplate" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numBizDocTempID") %>' />
                                    <asp:HyperLink ID="hypShippingBox" runat="server" Style="text-decoration: none">
                                        <img runat="server" id="imgBox" onclick='<%# "openShippingLabel(" + Convert.ToString(Eval("numOppId")) + "," + Convert.ToString(Eval("numoppbizdocsid")) + "," + Convert.ToString(Eval("numShippingReportId")) + "," + Convert.ToString(Eval("intUsedShippingCompany")) + "," + Convert.ToString(Eval("numShippingService")) + ");"%>' style="vertical-align: middle" src='<%#If(Eval("IsShippingReportGenerated") = 1, "../images/box_generated_22x22.png", "../images/box_not_generated_22x22.png")%>' border="0" />
                                    </asp:HyperLink>
                                    <asp:HyperLink ID="hypShippingReport" runat="server" Style="text-decoration: none" href="javascript:void(0);">
                                        <img runat="server" id="imgBarcode" style="vertical-align: middle" onclick='<%# "openReportList(" +Convert.ToString(Eval("numOppId")) + "," + Convert.ToString(Eval("numoppbizdocsid")) + "," + Convert.ToString(Eval("numShippingLabelReportId"))+ ");"%>' src='<%#If(Eval("ISTrackingNumGenerated") = 1, "../images/barcode_generated_22x22.png", "../images/barcode_not_generated_22x22.png")%>' border="0" />
                                    </asp:HyperLink>
                                    <%--<a style="text-decoration: none" href="javascript:void(0);" onclick="openReportList('<%# Eval("numOppId") %>','<%# Eval("numoppbizdocsid")%>','<%# Eval("numShippingLabelReportId") %>');">
                                        <img runat="server" id="imgBarcode" style="vertical-align: middle" src='<%#If(Eval("ISTrackingNumGenerated") = 1, "../images/barcode_generated_22x22.png", "../images/barcode_not_generated_22x22.png")%>' border="0" />
                                    </a>--%>
                                    <asp:ImageButton runat="server" Style="vertical-align: bottom" ID="ibtnPayments" AlternateText="Receive payment" ToolTip="Receive payment" ImageUrl="~/images/moneyup-24.gif" />&nbsp;
                                    <asp:ImageButton runat="server" Style="vertical-align: bottom; margin-bottom: -3px" ID="ibtnSendEmail" AlternateText="Email as PDF" ToolTip="Email as PDF" ImageUrl="~/images/Email_22x22.png" CommandName="Email" />&nbsp;
                                    <asp:ImageButton runat="server" Style="vertical-align: middle" ID="ibtnExportPDF" AlternateText="Export to PDF" ToolTip="Export to PDF" ImageUrl="~/images/pdf_22x22.png" CommandName="PDF" />&nbsp;
                                    <asp:ImageButton runat="server" Style="vertical-align: middle" ID="ibtnPrint" AlternateText="Print" ToolTip="Print" ImageUrl="~/images/Print_22x22.png" />&nbsp;
                                    <asp:ImageButton runat="server" Style="vertical-align: middle" ID="ibtnCloneBizdoc" AlternateText="Clone Bizdoc" ToolTip="Clone Bizdoc" ImageUrl="~/images/clone_docs.png" />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn ItemStyle-HorizontalAlign="Center">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtOppID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numOppId") %>'>
                                    </asp:TextBox>
                                    <asp:TextBox ID="txtBizDocStatus" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numBizDocStatus")%>'>
                                    </asp:TextBox>
                                    <asp:TextBox ID="txtBizDocID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numOppBizDocsId") %>'>
                                    </asp:TextBox>
                                    <asp:TextBox ID="txtOrientation" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numOrientation") %>'>
                                    </asp:TextBox>
                                    <asp:TextBox ID="txtKeepFooterBottom" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "bitKeepFooterBottom") %>'>
                                    </asp:TextBox>
                                    <asp:TextBox ID="txtIsAuthBizDoc" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "bitAuthoritativeBizDocs") %>'>
                                    </asp:TextBox>
                                    <asp:TextBox ID="txtRecurringBizDoc" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "bitRecur")%>'>
                                    </asp:TextBox>
                                    <asp:TextBox ID="txtRecurredBizDoc" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "bitRecurred")%>'>
                                    </asp:TextBox>
                                    <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-xs btn-danger" CommandName="Delete"><i class="fa fa-trash"></i></asp:LinkButton>
                                    <asp:LinkButton ID="lnkDelete" runat="server" Visible="false">
														<font color="#730000">*</font></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid>
                    <asp:HiddenField runat="server" ID="hdnReturnMessage" />
                    <asp:HiddenField runat="server" ID="hdnRecurringOrder" />
                    <asp:HiddenField runat="server" ID="hdnReplationship" />
                    <asp:HiddenField runat="server" ID="hdnProfile" />
                    <asp:HiddenField runat="server" ID="hdnAccountClass" />
                </div>
            </div>
        </div>
        <input type="hidden" id="hdRPopup" />
        <asp:HiddenField ID="hdnIsDeferredBizDocAdded" runat="server" />
        <asp:HiddenField ID="hdnAuthorizeCreditCard" runat="server" />
        <asp:HiddenField ID="hdnCommitAllocation" runat="server" />
        <asp:HiddenField ID="hdnAllocateOnPickList" runat="server" />
    </form>
</body>
</html>
