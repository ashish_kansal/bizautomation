﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Opportunities

Namespace BACRM.UserInterface.Opportunities

    Public Class frmMassSalesFulfillmentConfig
        Inherits BACRMPage

#Region "Member Variable"

        Dim objContact As CContacts
        Dim objMassSalesFulfillmentConfiguration As MassSalesFulfillmentConfiguration

#End Region

#Region "Page Events"

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not Page.IsPostBack Then
                    ddlOrderStatus.DataSource = objCommon.GetMasterListItems(176, Session("DomainID"), 1, 2)
                    ddlOrderStatus.DataTextField = "vcData"
                    ddlOrderStatus.DataValueField = "numListItemID"
                    ddlOrderStatus.DataBind()
                    ddlOrderStatus.Items.Insert(0, New ListItem("--Select One--", "0"))

                    BindConfiguration()
                    BindData()

                    Dim objItem As New BusinessLogic.Item.CItems
                    objItem.DomainID = Session("DomainID")
                    lbAvailableWP.DataSource = objItem.GetWareHouses
                    lbAvailableWP.DataTextField = "vcWareHouse"
                    lbAvailableWP.DataValueField = "numWareHouseID"
                    lbAvailableWP.DataBind()


                    objCommon.DomainID = Session("DomainID")
                    objCommon.ListID = 9
                    Dim dtOrderSource As DataTable = objCommon.GetOpportunitySource

                    rcbSourceHeader.DataSource = dtOrderSource.Select("tintSourceType=1 OR tintSourceType=2 OR tintSourceType=4").CopyToDataTable()
                    rcbSourceHeader.DataTextField = "vcData"
                    rcbSourceHeader.DataValueField = "numItemID"
                    rcbSourceHeader.DataBind()

                    ddlSource.DataSource = dtOrderSource.Select("tintSourceType=1 OR tintSourceType=2 OR tintSourceType=4").CopyToDataTable()
                    ddlSource.DataTextField = "vcData"
                    ddlSource.DataValueField = "numItemID"
                    ddlSource.DataBind()
                    ddlSource.Items.Insert(0, New ListItem("-- Select One --", "0"))

                    ddlOrderStatusShip.DataSource = dtOrderSource.Select("tintSourceType=1 OR tintSourceType=2 OR tintSourceType=4").CopyToDataTable()
                    ddlOrderStatusShip.DataTextField = "vcData"
                    ddlOrderStatusShip.DataValueField = "numItemID"
                    ddlOrderStatusShip.DataBind()
                    ddlOrderStatusShip.Items.Insert(0, New ListItem("-- Select One --", "0"))

                    Dim dt As DataTable = objCommon.GetMasterListItems(40, CCommon.ToLong(Session("DomainID")))
                    ddlCountry.DataSource = dt
                    ddlCountry.DataTextField = "vcData"
                    ddlCountry.DataValueField = "numListItemID"
                    ddlCountry.DataBind()
                    ddlCountry.Items.Insert(0, New ListItem("-- Select One --", "0"))

                    rcbCountryHeader.DataSource = dt
                    rcbCountryHeader.DataTextField = "vcData"
                    rcbCountryHeader.DataValueField = "numListItemID"
                    rcbCountryHeader.DataBind()

                    objCommon.sb_FillComboFromDB(ddlShipVia, 82, CCommon.ToLong(Session("numDomainID")))
                    objCommon.sb_FillComboFromDB(ddlShipViaOverride, 82, CCommon.ToLong(Session("numDomainID")))

                    ddlShipVia.Items.Insert(0, New ListItem("Select Ship-via", "0"))
                    ddlShipViaOverride.Items.Insert(0, New ListItem("Select Ship-via", "0"))
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

#End Region

#Region "Private Methods"

        Private Sub BindConfiguration()
            Try
                objMassSalesFulfillmentConfiguration = New MassSalesFulfillmentConfiguration
                objMassSalesFulfillmentConfiguration.DomainID = CCommon.ToLong(Session("DomainID"))
                objMassSalesFulfillmentConfiguration.UserCntID = CCommon.ToLong(Session("UserContactID"))
                Dim dt As DataTable = objMassSalesFulfillmentConfiguration.GetByDomain()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    hdnGroupByOrderForPick.Value = CCommon.ToBool(dt.Rows(0)("bitGroupByOrderForPick"))
                    hdnGroupByOrderForShip.Value = CCommon.ToBool(dt.Rows(0)("bitGroupByOrderForShip"))
                    hdnGroupByOrderForInvoice.Value = CCommon.ToBool(dt.Rows(0)("bitGroupByOrderForInvoice"))
                    hdnGroupByOrderForPay.Value = CCommon.ToBool(dt.Rows(0)("bitGroupByOrderForPay"))
                    hdnGroupByOrderForClose.Value = CCommon.ToBool(dt.Rows(0)("bitGroupByOrderForClose"))
                    ddlPendingCloseFilter.SelectedValue = CCommon.ToShort(dt.Rows(0)("tintPendingCloseFilter"))
                    chkPickListByOrder.Checked = CCommon.ToBool(dt.Rows(0)("bitGeneratePickListByOrder"))
                    hdnOrderStatusPicked.Value = CCommon.ToLong(dt.Rows(0)("numOrderStatusPicked"))
                    hdnOrderStatusPacked1.Value = CCommon.ToLong(dt.Rows(0)("numOrderStatusPacked1"))
                    hdnOrderStatusPacked2.Value = CCommon.ToLong(dt.Rows(0)("numOrderStatusPacked2"))
                    hdnOrderStatusPacked3.Value = CCommon.ToLong(dt.Rows(0)("numOrderStatusPacked3"))
                    hdnOrderStatusPacked4.Value = CCommon.ToLong(dt.Rows(0)("numOrderStatusPacked4"))
                    hdnOrderStatusInvoiced.Value = CCommon.ToLong(dt.Rows(0)("numOrderStatusInvoiced"))
                    hdnOrderStatusPaid.Value = CCommon.ToLong(dt.Rows(0)("numOrderStatusPaid"))
                    hdnOrderStatusClosed.Value = CCommon.ToLong(dt.Rows(0)("numOrderStatusClosed"))
                    chkPickWithoutInventoryCheck.Checked = CCommon.ToBool(dt.Rows(0)("bitPickWithoutInventoryCheck"))
                    chkPickListMapping.Checked = CCommon.ToBool(dt.Rows(0)("bitEnablePickListMapping"))
                    chkAutoGeneratePackingSlip.Checked = CCommon.ToBool(dt.Rows(0)("bitAutoGeneratePackingSlip"))
                    chkFulfillmentBizDocMapping.Checked = CCommon.ToBool(dt.Rows(0)("bitEnableFulfillmentBizDocMapping"))
                    chkAutoWarehouseSelection.Checked = CCommon.ToBool(dt.Rows(0)("bitAutoWarehouseSelection"))
                    chkBOOrderStatus.Checked = CCommon.ToBool(dt.Rows(0)("bitBOOrderStatus"))
                    ddlPachShipButtons.SelectedValue = CCommon.ToShort(dt.Rows(0)("tintPackShipButtons"))

                    If CCommon.ToShort(dt.Rows(0)("tintInvoicingType")) = 2 Then
                        rbInvoiceAnytime.Checked = True
                    Else
                        rbInvoiceAfterShipping.Checked = True
                    End If

                    If CCommon.ToShort(dt.Rows(0)("tintScanValue")) = 3 Then
                        rbItemName.Checked = True
                    ElseIf CCommon.ToShort(dt.Rows(0)("tintScanValue")) = 2 Then
                        rbUPC.Checked = True
                    Else
                        rbSKU.Checked = True
                    End If

                    If CCommon.ToShort(dt.Rows(0)("tintPackingMode")) = 3 Then
                        rblPackingMode.SelectedValue = "3"
                    ElseIf CCommon.ToShort(dt.Rows(0)("tintPackingMode")) = 2 Then
                        rblPackingMode.SelectedValue = "2"
                    Else
                        rblPackingMode.SelectedValue = "1"
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub BindData()
            Try
                chkGroupByOrder.Checked = False
                divPendingClose.Visible = False
                lbAvailableFieldLeft.Items.Clear()
                lbSelectedFieldLeft.Items.Clear()

                lbAvailableFieldRight.Items.Clear()
                lbSelectedFieldRight.Items.Clear()

                Dim ds As DataSet
                objContact = New CContacts
                objContact.DomainID = Session("DomainId")
                objContact.UserCntID = Session("UserContactId")
                objContact.ViewID = CCommon.ToShort(ddlWhen.SelectedValue)

                objContact.FormId = 141
                ds = objContact.GetColumnConfiguration()

                lbAvailableFieldLeft.DataSource = ds.Tables(0)
                lbAvailableFieldLeft.DataTextField = "vcFieldName"
                lbAvailableFieldLeft.DataValueField = "numFieldID"
                lbAvailableFieldLeft.DataBind()

                lbSelectedFieldLeft.DataSource = ds.Tables(1)
                lbSelectedFieldLeft.DataValueField = "numFieldID"
                lbSelectedFieldLeft.DataTextField = "vcFieldName"
                lbSelectedFieldLeft.DataBind()

                If ddlWhen.SelectedValue = "1" Then
                    objContact.FormId = 142
                    objContact.ViewID = 1
                    ds = objContact.GetColumnConfiguration()

                    lbAvailableFieldRight.DataSource = ds.Tables(0)
                    lbAvailableFieldRight.DataTextField = "vcFieldName"
                    lbAvailableFieldRight.DataValueField = "numFieldID"
                    lbAvailableFieldRight.DataBind()

                    lbSelectedFieldRight.DataSource = ds.Tables(1)
                    lbSelectedFieldRight.DataValueField = "numFieldID"
                    lbSelectedFieldRight.DataTextField = "vcFieldName"
                    lbSelectedFieldRight.DataBind()
                End If

                If ddlWhen.SelectedValue = "1" Then
                    chkGroupByOrder.Checked = CCommon.ToBool(hdnGroupByOrderForPick.Value)
                    divRight.Style.Remove("display")
                    divPickGroup.Style.Remove("display")
                    divInvoicing.Style.Add("display", "none")
                    divGroupBy.Style.Remove("display")
                    divShipSetting.Visible = False

                    divOrderStatus1.Visible = True
                    divOrderStatus2.Visible = True
                    litOrderStaus.Text = "After order has been picked"
                    ddlShipMode.Visible = False

                    ddlOrderStatus.SelectedValue = "0"
                    If Not ddlOrderStatus.Items.FindByValue(hdnOrderStatusPicked.Value) Is Nothing Then
                        ddlOrderStatus.Items.FindByValue(hdnOrderStatusPicked.Value).Selected = True
                    End If
                ElseIf ddlWhen.SelectedValue = "2" Then
                    chkGroupByOrder.Checked = CCommon.ToBool(hdnGroupByOrderForShip.Value)
                    divRight.Style.Add("display", "none")
                    divPickGroup.Style.Add("display", "none")
                    divInvoicing.Style.Add("display", "none")
                    divGroupBy.Style.Remove("display")
                    divShipSetting.Visible = True

                    divOrderStatus1.Visible = True
                    divOrderStatus2.Visible = True
                    litOrderStaus.Text = "After"
                    ddlShipMode.Visible = True

                    ddlShipMode.SelectedValue = 1
                    ddlOrderStatus.SelectedValue = "0"
                    If Not ddlOrderStatus.Items.FindByValue(hdnOrderStatusPacked1.Value) Is Nothing Then
                        ddlOrderStatus.Items.FindByValue(hdnOrderStatusPacked1.Value).Selected = True
                    End If
                ElseIf ddlWhen.SelectedValue = "3" Then
                    chkGroupByOrder.Checked = CCommon.ToBool(hdnGroupByOrderForInvoice.Value)
                    divRight.Style.Add("display", "none")
                    divPickGroup.Style.Add("display", "none")
                    divInvoicing.Style.Remove("display")
                    divGroupBy.Style.Remove("display")
                    divShipSetting.Visible = False

                    divOrderStatus1.Visible = True
                    divOrderStatus2.Visible = True
                    litOrderStaus.Text = "After order is invoiced"
                    ddlShipMode.Visible = False

                    ddlOrderStatus.SelectedValue = "0"
                    If Not ddlOrderStatus.Items.FindByValue(hdnOrderStatusInvoiced.Value) Is Nothing Then
                        ddlOrderStatus.Items.FindByValue(hdnOrderStatusInvoiced.Value).Selected = True
                    End If
                ElseIf ddlWhen.SelectedValue = "4" Then
                    chkGroupByOrder.Checked = True
                    divRight.Style.Add("display", "none")
                    divPickGroup.Style.Add("display", "none")
                    divInvoicing.Style.Add("display", "none")
                    divGroupBy.Style.Add("display", "none")
                    divShipSetting.Visible = False

                    divOrderStatus1.Visible = True
                    divOrderStatus2.Visible = True
                    litOrderStaus.Text = "After order is paid"
                    ddlShipMode.Visible = False

                    ddlOrderStatus.SelectedValue = "0"
                    If Not ddlOrderStatus.Items.FindByValue(hdnOrderStatusPaid.Value) Is Nothing Then
                        ddlOrderStatus.Items.FindByValue(hdnOrderStatusPaid.Value).Selected = True
                    End If
                ElseIf ddlWhen.SelectedValue = "5" Then
                    chkGroupByOrder.Checked = True
                    divRight.Style.Add("display", "none")
                    divPickGroup.Style.Add("display", "none")
                    divInvoicing.Style.Add("display", "none")
                    divGroupBy.Style.Add("display", "none")
                    divShipSetting.Visible = False
                    divPendingClose.Visible = True

                    divOrderStatus1.Visible = True
                    divOrderStatus2.Visible = True
                    litOrderStaus.Text = "After order is closed"
                    ddlShipMode.Visible = False

                    ddlOrderStatus.SelectedValue = "0"
                    If Not ddlOrderStatus.Items.FindByValue(hdnOrderStatusClosed.Value) Is Nothing Then
                        ddlOrderStatus.Items.FindByValue(hdnOrderStatusClosed.Value).Selected = True
                    End If
                ElseIf ddlWhen.SelectedValue = "6" Then
                    chkGroupByOrder.Checked = True
                    divRight.Style.Add("display", "none")
                    divPickGroup.Style.Add("display", "none")
                    divInvoicing.Style.Add("display", "none")
                    divGroupBy.Style.Add("display", "none")
                    divShipSetting.Visible = False
                    divPendingClose.Visible = False

                    divOrderStatus1.Visible = False
                    divOrderStatus2.Visible = False
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub LoadShippingConfiguration()
            Try
                Dim objMassSalesFulfillmentSC As New MassSalesFulfillmentShippingConfig
                objMassSalesFulfillmentSC.DomainID = CCommon.ToLong(Session("DomainID"))
                objMassSalesFulfillmentSC.OrderSource = CCommon.ToLong(ddlOrderStatusShip.SelectedValue.Split("~")(0))
                objMassSalesFulfillmentSC.SourceType = CCommon.ToLong(ddlOrderStatusShip.SelectedValue.Split("~")(1))
                Dim dt As DataTable = objMassSalesFulfillmentSC.GetByOrderSource()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    If CCommon.ToShort(dt.Rows(0)("tintType")) = 1 Then
                        rbPriorityShipVia.Checked = True
                        rbPreferredShipVia.Checked = False
                        rbCheapestShipVia.Checked = False

                        If Not String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcPriorities"))) Then
                            lbShipPriority.Items.Clear()

                            For Each item As Short In CCommon.ToString(dt.Rows(0)("vcPriorities")).Split(",")
                                If item = 1 Then
                                    lbShipPriority.Items.Add(New ListItem("Ship by Date", 1))
                                ElseIf item = 2 Then
                                    lbShipPriority.Items.Add(New ListItem("Deliver by Date", 2))
                                ElseIf item = 3 Then
                                    lbShipPriority.Items.Add(New ListItem("Shipping Service", 3))
                                ElseIf item = 4 Then
                                    lbShipPriority.Items.Add(New ListItem("Ship-Via (Carrier)", 4))
                                ElseIf item = 5 Then
                                    lbShipPriority.Items.Add(New ListItem("Shipping Amount", 5))
                                End If
                            Next
                        End If
                    ElseIf CCommon.ToShort(dt.Rows(0)("tintType")) = 2 Then
                        rbPriorityShipVia.Checked = False
                        rbPreferredShipVia.Checked = True
                        rbCheapestShipVia.Checked = False

                        ddlShipService.Items.Clear()
                        ddlShipService.Items.Add(New ListItem("Select Ship-service", 0))
                        If Not ddlShipVia.Items.FindByValue(CCommon.ToLong(dt.Rows(0)("numShipVia"))) Is Nothing Then
                            ddlShipVia.Items.FindByValue(CCommon.ToLong(dt.Rows(0)("numShipVia"))).Selected = True

                            If ddlShipVia.SelectedValue = 91 Then
                                ddlShipService.Items.Add(New ListItem("FedEx Priority Overnight", 10))
                                ddlShipService.Items.Add(New ListItem("FedEx Standard Overnight", 11))
                                ddlShipService.Items.Add(New ListItem("FedEx Overnight", 12))
                                ddlShipService.Items.Add(New ListItem("FedEx 2nd Day", 13))
                                ddlShipService.Items.Add(New ListItem("FedEx Express Saver", 14))
                                ddlShipService.Items.Add(New ListItem("FedEx Ground", 15))
                                ddlShipService.Items.Add(New ListItem("FedEx Ground Home Delivery", 16))
                                ddlShipService.Items.Add(New ListItem("FedEx 1 Day Freight", 17))
                                ddlShipService.Items.Add(New ListItem("FedEx 2 Day Freight", 18))
                                ddlShipService.Items.Add(New ListItem("FedEx 3 Day Freight", 19))
                                ddlShipService.Items.Add(New ListItem("FedEx International Priority", 20))
                                ddlShipService.Items.Add(New ListItem("FedEx International Priority Distribution", 21))
                                ddlShipService.Items.Add(New ListItem("FedEx International Economy", 22))
                                ddlShipService.Items.Add(New ListItem("FedEx International Economy Distribution", 23))
                                ddlShipService.Items.Add(New ListItem("FedEx International First", 24))
                                ddlShipService.Items.Add(New ListItem("FedEx International Priority Freight", 25))
                                ddlShipService.Items.Add(New ListItem("FedEx International Economy Freight", 26))
                                ddlShipService.Items.Add(New ListItem("FedEx International Distribution Freight", 26))
                                ddlShipService.Items.Add(New ListItem("FedEx Europe International Priority", 28))

                                If Not ddlShipService.Items.FindByValue(CCommon.ToLong(dt.Rows(0)("numShipService"))) Is Nothing Then
                                    ddlShipService.Items.FindByValue(CCommon.ToLong(dt.Rows(0)("numShipService"))).Selected = True
                                Else
                                    ddlShipService.SelectedValue = "15"
                                End If
                            ElseIf ddlShipVia.SelectedValue = 88 Then
                                ddlShipService.Items.Add(New ListItem("UPS Next Day Air", 40))
                                ddlShipService.Items.Add(New ListItem("UPS 2nd Day Air", 42))
                                ddlShipService.Items.Add(New ListItem("UPS Ground", 43))
                                ddlShipService.Items.Add(New ListItem("UPS 3Day Select", 48))
                                ddlShipService.Items.Add(New ListItem("UPS Next Day Air Saver", 49))
                                ddlShipService.Items.Add(New ListItem("UPS Saver", 50))
                                ddlShipService.Items.Add(New ListItem("UPS Next Day Air Early A.M.", 51))
                                ddlShipService.Items.Add(New ListItem("UPS 2nd Day Air AM", 55))

                                If Not ddlShipService.Items.FindByValue(CCommon.ToLong(dt.Rows(0)("numShipService"))) Is Nothing Then
                                    ddlShipService.Items.FindByValue(CCommon.ToLong(dt.Rows(0)("numShipService"))).Selected = True
                                Else
                                    ddlShipService.SelectedValue = "43"
                                End If
                            ElseIf ddlShipVia.SelectedValue = 90 Then
                                ddlShipService.Items.Add(New ListItem("USPS Express", 70))
                                ddlShipService.Items.Add(New ListItem("USPS First Class", 71))
                                ddlShipService.Items.Add(New ListItem("USPS Priority", 72))
                                ddlShipService.Items.Add(New ListItem("USPS Parcel Post", 73))
                                ddlShipService.Items.Add(New ListItem("USPS Bound Printed Matter", 74))
                                ddlShipService.Items.Add(New ListItem("USPS Media", 75))
                                ddlShipService.Items.Add(New ListItem("USPS Library", 76))

                                If Not ddlShipService.Items.FindByValue(CCommon.ToLong(dt.Rows(0)("numShipService"))) Is Nothing Then
                                    ddlShipService.Items.FindByValue(CCommon.ToLong(dt.Rows(0)("numShipService"))).Selected = True
                                Else
                                    ddlShipService.SelectedValue = "72"
                                End If
                            End If
                        Else
                            ddlShipVia.SelectedValue = "0"
                        End If
                    ElseIf CCommon.ToShort(dt.Rows(0)("tintType")) = 3 Then
                        rbPriorityShipVia.Checked = False
                        rbPreferredShipVia.Checked = False
                        rbCheapestShipVia.Checked = True
                    End If

                    chkOverrideShipConfig.Checked = CCommon.ToBool(dt.Rows(0)("bitOverride"))
                    txtShipWeight.Text = CCommon.ToDouble(dt.Rows(0)("fltWeight"))
                    If Not ddlShipViaOverride.Items.FindByValue(CCommon.ToLong(dt.Rows(0)("numShipViaOverride"))) Is Nothing Then
                        ddlShipViaOverride.Items.FindByValue(CCommon.ToLong(dt.Rows(0)("numShipViaOverride"))).Selected = True
                    Else
                        ddlShipViaOverride.SelectedValue = "0"
                    End If
                Else
                    rbPriorityShipVia.Checked = True
                    ddlShipVia.SelectedValue = "0"
                    ddlShipService.Items.Clear()
                    ddlShipService.Items.Insert(0, New ListItem("Select Ship-service", "0"))
                    chkOverrideShipConfig.Checked = False
                    txtShipWeight.Text = ""
                    ddlShipViaOverride.SelectedValue = "0"
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub SaveShippingConfiguration()
            Try
                If ddlOrderStatusShip.SelectedValue <> "0" Then
                    Dim objMassSalesFulfillmentSC As New MassSalesFulfillmentShippingConfig
                    objMassSalesFulfillmentSC.DomainID = CCommon.ToLong(Session("DomainID"))
                    objMassSalesFulfillmentSC.OrderSource = CCommon.ToLong(ddlOrderStatusShip.SelectedValue.Split("~")(0))
                    objMassSalesFulfillmentSC.SourceType = CCommon.ToLong(ddlOrderStatusShip.SelectedValue.Split("~")(1))
                    objMassSalesFulfillmentSC.Priorities = ""
                    objMassSalesFulfillmentSC.ShipVia = 0
                    objMassSalesFulfillmentSC.ShipService = 0
                    objMassSalesFulfillmentSC.Weight = 0
                    objMassSalesFulfillmentSC.ShipViaOverride = 0

                    If rbPriorityShipVia.Checked Then
                        objMassSalesFulfillmentSC.Type = 1
                        objMassSalesFulfillmentSC.Priorities = hdnShipPriorities.Value
                    ElseIf rbPreferredShipVia.Checked Then
                        objMassSalesFulfillmentSC.Type = 2
                        objMassSalesFulfillmentSC.ShipVia = CCommon.ToLong(ddlShipVia.SelectedValue)
                        objMassSalesFulfillmentSC.ShipService = CCommon.ToLong(ddlShipService.SelectedValue)
                    ElseIf rbCheapestShipVia.Checked Then
                        objMassSalesFulfillmentSC.Type = 3
                    End If

                    objMassSalesFulfillmentSC.IsOverride = chkOverrideShipConfig.Checked
                    objMassSalesFulfillmentSC.Weight = CCommon.ToDouble(txtShipWeight.Text)
                    objMassSalesFulfillmentSC.ShipViaOverride = CCommon.ToLong(ddlShipViaOverride.SelectedValue)
                    objMassSalesFulfillmentSC.Save()
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub DisplayError(ByVal errorMessage As String)
            Try
                lblError.Text = errorMessage
                divError.Style.Add("display", "")
            Catch ex As Exception
                'DO NOT THROW ERROR
            End Try
        End Sub

#End Region

#Region "Event Handlers"

        Protected Sub btnSave_Click(sender As Object, e As EventArgs)
            Try
                objContact = New CContacts
                objContact.DomainID = Session("DomainId")
                objContact.UserCntID = Session("UserContactId")
                objContact.FormId = 141

                Dim i As Integer
                Dim str As String()
                Dim dsNew As DataSet
                Dim dtTable As DataTable
                Dim dr As DataRow

                If hdnReadytoPickLeft.Value <> "-1" Then
                    dsNew = New DataSet()
                    dtTable = New DataTable()
                    dtTable.Columns.Add("numFieldID")
                    dtTable.Columns.Add("bitCustom")
                    dtTable.Columns.Add("tintOrder")
                    dtTable.TableName = "Table"

                    str = hdnReadytoPickLeft.Value.Split(",")
                    For i = 0 To str.Length - 1
                        dr = dtTable.NewRow
                        dr("numFieldID") = str(i).Split("~")(0)
                        dr("bitCustom") = str(i).Split("~")(1)
                        dr("tintOrder") = i
                        dtTable.Rows.Add(dr)
                    Next
                    dsNew.Tables.Add(dtTable)

                    objContact.ViewID = 1
                    objContact.strXml = dsNew.GetXml
                    objContact.SaveContactColumnConfiguration()
                End If

                If hdnReadytoShip.Value <> "-1" Then
                    dsNew = New DataSet()
                    dtTable = New DataTable()
                    dtTable.Columns.Add("numFieldID")
                    dtTable.Columns.Add("bitCustom")
                    dtTable.Columns.Add("tintOrder")
                    dtTable.TableName = "Table"

                    str = hdnReadytoShip.Value.Split(",")
                    For i = 0 To str.Length - 1
                        dr = dtTable.NewRow
                        dr("numFieldID") = str(i).Split("~")(0)
                        dr("bitCustom") = str(i).Split("~")(1)
                        dr("tintOrder") = i
                        dtTable.Rows.Add(dr)
                    Next
                    dsNew.Tables.Add(dtTable)

                    objContact.ViewID = 2
                    objContact.strXml = dsNew.GetXml
                    objContact.SaveContactColumnConfiguration()
                End If

                If hdnReadytoInvoice.Value <> "-1" Then
                    dsNew = New DataSet()
                    dtTable = New DataTable()
                    dtTable.Columns.Add("numFieldID")
                    dtTable.Columns.Add("bitCustom")
                    dtTable.Columns.Add("tintOrder")
                    dtTable.TableName = "Table"

                    str = hdnReadytoInvoice.Value.Split(",")
                    For i = 0 To str.Length - 1
                        dr = dtTable.NewRow
                        dr("numFieldID") = str(i).Split("~")(0)
                        dr("bitCustom") = str(i).Split("~")(1)
                        dr("tintOrder") = i
                        dtTable.Rows.Add(dr)
                    Next
                    dsNew.Tables.Add(dtTable)

                    objContact.ViewID = 3
                    objContact.strXml = dsNew.GetXml
                    objContact.SaveContactColumnConfiguration()
                End If

                If hdnReadytoPay.Value <> "-1" Then
                    dsNew = New DataSet()
                    dtTable = New DataTable()
                    dtTable.Columns.Add("numFieldID")
                    dtTable.Columns.Add("bitCustom")
                    dtTable.Columns.Add("tintOrder")
                    dtTable.TableName = "Table"

                    str = hdnReadytoPay.Value.Split(",")
                    For i = 0 To str.Length - 1
                        dr = dtTable.NewRow
                        dr("numFieldID") = str(i).Split("~")(0)
                        dr("bitCustom") = str(i).Split("~")(1)
                        dr("tintOrder") = i
                        dtTable.Rows.Add(dr)
                    Next
                    dsNew.Tables.Add(dtTable)

                    objContact.ViewID = 4
                    objContact.strXml = dsNew.GetXml
                    objContact.SaveContactColumnConfiguration()
                End If

                If hdnPendingClose.Value <> "-1" Then
                    dsNew = New DataSet()
                    dtTable = New DataTable()
                    dtTable.Columns.Add("numFieldID")
                    dtTable.Columns.Add("bitCustom")
                    dtTable.Columns.Add("tintOrder")
                    dtTable.TableName = "Table"

                    str = hdnPendingClose.Value.Split(",")
                    For i = 0 To str.Length - 1
                        dr = dtTable.NewRow
                        dr("numFieldID") = str(i).Split("~")(0)
                        dr("bitCustom") = str(i).Split("~")(1)
                        dr("tintOrder") = i
                        dtTable.Rows.Add(dr)
                    Next
                    dsNew.Tables.Add(dtTable)

                    objContact.ViewID = 5
                    objContact.strXml = dsNew.GetXml
                    objContact.SaveContactColumnConfiguration()
                End If

                If hdnPrintBizDocs.Value <> "-1" Then
                    dsNew = New DataSet()
                    dtTable = New DataTable()
                    dtTable.Columns.Add("numFieldID")
                    dtTable.Columns.Add("bitCustom")
                    dtTable.Columns.Add("tintOrder")
                    dtTable.TableName = "Table"

                    str = hdnPrintBizDocs.Value.Split(",")
                    For i = 0 To str.Length - 1
                        dr = dtTable.NewRow
                        dr("numFieldID") = str(i).Split("~")(0)
                        dr("bitCustom") = str(i).Split("~")(1)
                        dr("tintOrder") = i
                        dtTable.Rows.Add(dr)
                    Next
                    dsNew.Tables.Add(dtTable)

                    objContact.ViewID = 6
                    objContact.strXml = dsNew.GetXml
                    objContact.SaveContactColumnConfiguration()
                End If

                If hdnReadytoPickRight.Value <> "-1" Then
                    dsNew = New DataSet()
                    dtTable = New DataTable()
                    dtTable.Columns.Add("numFieldID")
                    dtTable.Columns.Add("bitCustom")
                    dtTable.Columns.Add("tintOrder")
                    dtTable.TableName = "Table"

                    str = hdnReadytoPickRight.Value.Split(",")
                    For i = 0 To str.Length - 1
                        dr = dtTable.NewRow
                        dr("numFieldID") = str(i).Split("~")(0)
                        dr("bitCustom") = str(i).Split("~")(1)
                        dr("tintOrder") = i
                        dtTable.Rows.Add(dr)
                    Next
                    dsNew.Tables.Add(dtTable)

                    objContact.FormId = 142
                    objContact.ViewID = 1
                    objContact.strXml = dsNew.GetXml
                    objContact.SaveContactColumnConfiguration()
                End If

                objMassSalesFulfillmentConfiguration = New MassSalesFulfillmentConfiguration
                objMassSalesFulfillmentConfiguration.DomainID = CCommon.ToLong(Session("DomainID"))
                objMassSalesFulfillmentConfiguration.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objMassSalesFulfillmentConfiguration.IsGroupByOrderForPick = If(ddlWhen.SelectedValue = "1", chkGroupByOrder.Checked, CCommon.ToBool(hdnGroupByOrderForPick.Value))
                objMassSalesFulfillmentConfiguration.IsGroupByOrderForShip = If(ddlWhen.SelectedValue = "2", chkGroupByOrder.Checked, CCommon.ToBool(hdnGroupByOrderForShip.Value))
                objMassSalesFulfillmentConfiguration.IsGroupByOrderForInvoice = If(ddlWhen.SelectedValue = "3", chkGroupByOrder.Checked, CCommon.ToBool(hdnGroupByOrderForInvoice.Value))
                objMassSalesFulfillmentConfiguration.IsGroupByOrderForPay = False
                objMassSalesFulfillmentConfiguration.IsGroupByOrderForClose = False
                objMassSalesFulfillmentConfiguration.PackingMode = CCommon.ToShort(rblPackingMode.SelectedValue)
                objMassSalesFulfillmentConfiguration.PendingCloseFilter = ddlPendingCloseFilter.SelectedValue
                objMassSalesFulfillmentConfiguration.IsGeneratePickListByOrder = chkPickListByOrder.Checked
                objMassSalesFulfillmentConfiguration.OrderStatusPicked = CCommon.ToLong(hdnOrderStatusPicked.Value)
                objMassSalesFulfillmentConfiguration.OrderStatusPacked1 = CCommon.ToLong(hdnOrderStatusPacked1.Value)
                objMassSalesFulfillmentConfiguration.OrderStatusPacked2 = CCommon.ToLong(hdnOrderStatusPacked2.Value)
                objMassSalesFulfillmentConfiguration.OrderStatusPacked3 = CCommon.ToLong(hdnOrderStatusPacked3.Value)
                objMassSalesFulfillmentConfiguration.OrderStatusPacked4 = CCommon.ToLong(hdnOrderStatusPacked4.Value)
                objMassSalesFulfillmentConfiguration.OrderStatusInvoiced = CCommon.ToLong(hdnOrderStatusInvoiced.Value)
                objMassSalesFulfillmentConfiguration.OrderStatusPaid = CCommon.ToLong(hdnOrderStatusPaid.Value)
                objMassSalesFulfillmentConfiguration.OrderStatusClosed = CCommon.ToLong(hdnOrderStatusClosed.Value)
                objMassSalesFulfillmentConfiguration.IsPickWithoutInventoryCheck = chkPickWithoutInventoryCheck.Checked
                objMassSalesFulfillmentConfiguration.IsEnablePickListMapping = chkPickListMapping.Checked
                objMassSalesFulfillmentConfiguration.IsAutoGeneratePackingSlip = chkAutoGeneratePackingSlip.Checked
                objMassSalesFulfillmentConfiguration.IsEnableFulfillmentBizDocMapping = chkFulfillmentBizDocMapping.Checked
                objMassSalesFulfillmentConfiguration.IsAutoWarehouseSelection = chkAutoWarehouseSelection.Checked
                objMassSalesFulfillmentConfiguration.IsBOOrderStatus = chkBOOrderStatus.Checked
                objMassSalesFulfillmentConfiguration.PackShipButtons = CCommon.ToShort(ddlPachShipButtons.SelectedValue)

                If rbInvoiceAnytime.Checked Then
                    objMassSalesFulfillmentConfiguration.InvoicingType = 2
                Else
                    objMassSalesFulfillmentConfiguration.InvoicingType = 1
                End If

                If rbItemName.Checked Then
                    objMassSalesFulfillmentConfiguration.ScanValue = 3
                ElseIf rbUPC.Checked Then
                    objMassSalesFulfillmentConfiguration.ScanValue = 2
                Else
                    objMassSalesFulfillmentConfiguration.ScanValue = 1
                End If

                objMassSalesFulfillmentConfiguration.Save()

                scriptmanager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "RefreshParentAndClose", "RefreshParentAndClose();", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub ddlWhen_SelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                BindData()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub ddlOrderStatusShip_SelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                If ddlOrderStatusShip.SelectedValue <> "0" Then
                    LoadShippingConfiguration()
                End If
            Catch ex As Exception
                litShipMessage.Text = "Unknown error occurred."
                scriptmanager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "OpenShipConfigDialog", "$('#divShipMessage h4').closest('div').removeClass('alert-success').addClass('alert-warning'); $('#divShipMessage h4').html('<i class=""icon fa fa-warning""></i>Alert!'); $('#divShipMessage').show(); $('#modalShippingConfiguration').modal('show');", True)

                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub btnSaveShippingConfig_Click(sender As Object, e As EventArgs)
            Try
                SaveShippingConfiguration()

                litShipMessage.Text = "Record saved successfully"
                scriptmanager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "OpenShipConfigDialog", "$('#divShipMessage h4').closest('div').removeClass('alert-warning').addClass('alert-success'); $('#divShipMessage h4').html('<i class=""icon fa fa-success""></i>Success'); $('#divShipMessage').show();", True)
            Catch ex As Exception
                litShipMessage.Text = "Unknown error occurred while saving shipping configuration"
                scriptmanager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "OpenShipConfigDialog", "$('#divShipMessage h4').closest('div').removeClass('alert-success').addClass('alert-warning'); $('#divShipMessage h4').html('<i class=""icon fa fa-warning""></i>Alert!'); $('#divShipMessage').show(); $('#modalShippingConfiguration').modal('show');", True)

                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub btnSaveCloseShippingConfig_Click(sender As Object, e As EventArgs)
            Try
                SaveShippingConfiguration()
                scriptmanager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "OpenShipConfigDialog", "$('#modalShippingConfiguration').modal('hide');", True)
            Catch ex As Exception
                litShipMessage.Text = "Unknown error occurred while saving shipping configuration"
                scriptmanager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "OpenShipConfigDialog", "$('#divShipMessage h4').closest('div').removeClass('alert-success').addClass('alert-warning'); $('#divShipMessage h4').html('<i class=""icon fa fa-warning""></i>Alert!'); $('#divShipMessage').show(); $('#modalShippingConfiguration').modal('show');", True)

                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

#End Region

    End Class
End Namespace