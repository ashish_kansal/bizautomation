﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Opportunities


Public Class frmSalesBizDocWorkflowSettings
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindData()
            Page.ClientScript.RegisterStartupScript(Me.GetType(), "resize", "windowResize();", True)

        End If
    End Sub

    Sub BindData()
        Try
            Dim dtBizDocStatus As DataTable = objCommon.GetMasterListItems(11, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlBizDocs, 27, Session("DomainID"))
            'Changed by Sachin Sadhu||Date:17thJune2014
            'Purpose :Filter Status Bizdoc Type wise
            objCommon.sb_FillComboFromDBwithSel(ddlBizDocStatus14, 11, Session("DomainID"), CCommon.ToString(ddlBizDocs.SelectedValue))
            objCommon.sb_FillComboFromDBwithSel(ddlBizDocStatus15, 11, Session("DomainID"), CCommon.ToString(ddlBizDocs.SelectedValue))
            'End of Code
            Dim objCampaign As New Campaign
            Dim dtTable As DataTable
         
            objCampaign.UserCntID = Session("UserContactID")
            objCampaign.DomainID = Session("DomainID")
            dtTable = objCampaign.GetUserEmailTemplates
            FillEmailTemplate(ddlEmailTemplate14, dtTable)
            FillEmailTemplate(ddlEmailTemplate15, dtTable)

            objCommon.DomainID = Session("DomainID")
            Dim dtOrderSource As DataTable = objCommon.GetOpportunitySource()
            ddlOrderSource.DataSource = dtOrderSource
            ddlOrderSource.DataTextField = "vcItemName"
            ddlOrderSource.DataValueField = "numItemID"
            ddlOrderSource.DataBind()


        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub FillEmailTemplate(ByRef ddlEmailTemplate As DropDownList, ByVal dtEmailTemplate As DataTable)
        Try
            ddlEmailTemplate.DataSource = dtEmailTemplate
            ddlEmailTemplate.DataTextField = "VcDocName"
            ddlEmailTemplate.DataValueField = "numGenericDocID"
            ddlEmailTemplate.DataBind()
            ddlEmailTemplate.Items.Insert(0, "--Select One--")
            ddlEmailTemplate.Items.FindByText("--Select One--").Value = "0"
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlBizDocs_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBizDocs.SelectedIndexChanged
        Try
            BindBizDocsTemplate()
            'Changed by Sachin Sadhu||Date:17thJune2014
            'Purpose :Filter Status Bizdoc Type wise
            objCommon.sb_FillComboFromDBwithSel(ddlBizDocStatus14, 11, Session("DomainID"), CCommon.ToString(ddlBizDocs.SelectedValue))
            objCommon.sb_FillComboFromDBwithSel(ddlBizDocStatus15, 11, Session("DomainID"), CCommon.ToString(ddlBizDocs.SelectedValue))
            'End of Code
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindBizDocsTemplate()
        Try
            Dim objOppBizDoc As New OppBizDocs
            objOppBizDoc.DomainID = Session("DomainID")
            objOppBizDoc.BizDocId = ddlBizDocs.SelectedValue
            objOppBizDoc.OppType = 1
            objOppBizDoc.byteMode = 0

            Dim dtBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()

            ddlBizDocTemplate.DataSource = dtBizDocTemplate
            ddlBizDocTemplate.DataTextField = "vcTemplateName"
            ddlBizDocTemplate.DataValueField = "numBizDocTempID"
            ddlBizDocTemplate.DataBind()

            ddlBizDocTemplate.Items.Insert(0, New ListItem("--Select--", 0))

            objOppBizDoc.byteMode = 1

            Dim dtDefaultBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()

            If Not IsDBNull(dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID")) Then
                If Not ddlBizDocTemplate.Items.FindByValue(dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID")) Is Nothing Then
                    ddlBizDocTemplate.ClearSelection()
                    ddlBizDocTemplate.SelectedValue = dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID").ToString
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            Dim objAdmin As New CAdmin
            Dim OrderSource As String
            OrderSource = ddlOrderSource.SelectedValue
            Dim arrOrderSource As String()

            If OrderSource <> "0" Then
                arrOrderSource = OrderSource.Split("~")

            End If

            objAdmin.DomainID = Session("DomainID")
            objAdmin.UserCntID = Session("UserContactID")
            objAdmin.BizDocCreatedFrom = ddlRules.SelectedValue
            objAdmin.byteMode = 1
            objAdmin.BizDocTypeId = ddlBizDocs.SelectedValue
            objAdmin.OppType = 1
            objAdmin.OrderStatus = 0
            objAdmin.BizDocStatus2 = 0
            objAdmin.OppSource = arrOrderSource(0)
            objAdmin.UpdatedTrackingNo = IIf(chkUpdatedTrackingNo.Checked = True, 1, 0)

            objAdmin.BizDocTemplate = ddlBizDocTemplate.SelectedValue

            If (rdbRule14.Checked = True) Then
                objAdmin.RuleID = 14
                objAdmin.BizDocStatus1 = ddlBizDocStatus14.SelectedValue
                objAdmin.OpenRecievePayment = IIf(chkOpenReceivePayment14.Checked = True, 1, 0)
                objAdmin.CreditCardOption = 0
                objAdmin.EmailTemplate = ddlEmailTemplate14.SelectedValue

            ElseIf (rdbRule15.Checked = True) Then
                objAdmin.RuleID = 15
                objAdmin.BizDocStatus1 = ddlBizDocStatus15.SelectedValue
                objAdmin.CreditCardOption = IIf(chkCreditCard15.Checked = True, 1, 0)
                objAdmin.OpenRecievePayment = 0
                objAdmin.EmailTemplate = ddlEmailTemplate15.SelectedValue

            End If
            objAdmin.ManageWorkflowAutomation()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)

        End Try

    End Sub


End Class