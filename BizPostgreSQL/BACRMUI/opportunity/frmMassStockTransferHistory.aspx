﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmMassStockTransferHistory.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmMassStockTransferHistory" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function OpenItem(numItemCode) {
            if (window.opener.opener != null) {
                window.opener.opener.location.href = "../Items/frmKitDetails.aspx?ItemCode=" + numItemCode + "&frm=All Items";
                window.opener.close();
                self.close();
            }

            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
     <div class="pull-right">
        <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary" Text="Cancel" OnClientClick="Close();" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Last 20 Stock Transfer
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <div class="row padbottom10" id="divError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>
                <p>
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvTransfer" runat="server" UseAccessibleHeader="true" CssClass="table table-bordered table-striped" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true">
                    <Columns>
                        <asp:BoundField HeaderText="Date" DataField="dtTransferredDate" />
                        <asp:TemplateField HeaderText="Item Name">
                            <ItemTemplate>
                                <asp:HyperLink ID="hplItem" runat="server" NavigateUrl="#" onclick='<%# "return OpenItem(" & Eval("numItemCode") & ");"%>' Text='<%# Eval("vcItemName")%>'></asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText="Transferred Qty" DataField="numQty" />
                        <asp:TemplateField HeaderText="From Location (On Hand Before / After)">
                            <ItemTemplate>
                                <%# Eval("vcFrom")%> (<%# String.Format("{0:##0.##}",Eval("numFromQtyBefore"))%> / <%# String.Format("{0:##0.##}",Eval("numFromQtyAfter"))%>)
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="To Location (On Hand Before / After)">
                            <ItemTemplate>
                                <%# Eval("vcTo")%> (<%# String.Format("{0:##0.##}", Eval("numToQtyBefore"))%> / <%# String.Format("{0:##0.##}", Eval("numToQtyAfter"))%>)
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
