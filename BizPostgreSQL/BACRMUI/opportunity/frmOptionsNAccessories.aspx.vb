﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item

Public Class frmOptionsNAccessories
    Inherits BACRMPage
    Dim objItems As New CItems
    Dim objCommon As CCommon
    Dim ds As DataSet

    Dim lngItemCode As Long
    Dim tblrow As TableRow = Nothing
    Dim tblcell As TableCell = Nothing
    Dim rblKitItem As RadioButtonList = Nothing
    Dim lbl As Label = Nothing
    Dim img As WebControls.Image = Nothing

    Dim dsTemp As DataSet
    Private m_dynamicRadioButtonList As RadioButtonList()

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngItemCode = GetQueryStringVal( "ItemCode")
            If lngItemCode <> 0 Then
                loadDetails()
            End If
            If Not IsPostBack Then
                If lngItemCode > 0 AndAlso Session("SOItems") IsNot Nothing Then
                    dsTemp = Session("SOItems")
                    Dim dtitem As DataTable
                    dtitem = dsTemp.Tables(2)

                    If dtitem.Rows.Count > 0 Then
                        Dim i As Int32 = 0
                        For Each ctrl As RadioButtonList In m_dynamicRadioButtonList
                            For Each lt As ListItem In ctrl.Items
                                If lt.Value.Contains(dtitem(i)("numItemCode").ToString()) Then
                                    ctrl.SelectedValue = lt.Value
                                    Exit For
                                End If
                            Next


                            i += 1

                            If dtitem.Rows.Count = i Then
                                Exit For
                            End If
                        Next

                    End If
                End If
            End If
            'If Not IsPostBack Then
            '    If GetQueryStringVal( "ItemGroupID") > 0 Then LoadDetails()
            '    
            'End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub loadDetails()
        Try
            Dim dt As DataTable
            objItems.ItemCode = lngItemCode
            'dt = objItems.GetChildItems
            objItems.ItemGroupID = 0
            objItems.DomainID = Session("DomainID")
            ds = objItems.GetItemGroups
            dt = ds.Tables(0)
            dt.TableName = "ChildItems"

            If dt.Rows.Count > 0 Then
                Dim dcItemText As DataColumn
                dcItemText = New DataColumn("ItemText", System.Type.GetType("System.String"))
                dcItemText.Expression = "vcItemName + '- Price is ' + UnitPrice"
                dt.Columns.Add(dcItemText)
                Dim i As Int32 = 0
                m_dynamicRadioButtonList = New RadioButtonList(dt.Rows.Count - 1) {}

                For Each drow As DataRow In dt.Rows

                    Dim dtKitItem As DataTable
                    objItems.ItemCode = drow("numItemCode")
                    dtKitItem = objItems.GetChildItems
                    dtKitItem.TableName = "ChildItems"

                    tblrow = New TableRow()
                    tblcell = New TableCell()
                    lbl = New Label()
                    lbl.CssClass = "normal1"
                    lbl.Text = drow("vcItemName")
                    lbl.Font.Bold = True
                    tblcell.Controls.Add(lbl)
                    tblrow.Cells.Add(tblcell)
                    tblMain.Rows.Add(tblrow)

                    If Not IsDBNull(drow("vcPathForImage")) Then
                        If drow("vcPathForImage") <> "" Then
                            tblrow = New TableRow()
                            tblcell = New TableCell()
                            img = New WebControls.Image()
                            img.BorderWidth = 1
                            img.Width = 100
                            img.Height = 100
                            img.BorderColor = System.Drawing.Color.Black

                            img.Visible = True
                            img.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & drow("vcPathForImage")
                            img.Font.Bold = True
                            tblcell.Controls.Add(img)
                            tblrow.Cells.Add(tblcell)
                            tblMain.Rows.Add(tblrow)
                        End If
                    End If



                    If dtKitItem.Rows.Count > 0 Then
                        tblrow = New TableRow()
                        tblcell = New TableCell()

                        rblKitItem = New RadioButtonList()
                        rblKitItem.CssClass = "signup"
                        rblKitItem.ID = "rblKitItems" + drow("numItemCode").ToString()

                        Dim dcItemText1 As DataColumn
                        dcItemText1 = New DataColumn("ItemText", System.Type.GetType("System.String"))
                        dcItemText1.Expression = "vcItemName + '- Price is ' + UnitPrice"
                        dtKitItem.Columns.Add(dcItemText1)

                        Dim dcItemValue1 As DataColumn
                        dcItemValue1 = New DataColumn("ItemValue", System.Type.GetType("System.String"))
                        dcItemValue1.Expression = "numItemCode + '~' + numQtyItemsReq + '~' + vcItemName + '~' + monListPrice + '~' + UnitPrice + '~'+  charItemType + '~' + ItemType + '~' + txtItemDesc  + '~' + numWarehouseItmsID"
                        dtKitItem.Columns.Add(dcItemValue1)


                        rblKitItem.DataSource = dtKitItem
                        rblKitItem.DataTextField = "ItemText"
                        rblKitItem.DataValueField = "ItemValue"
                        rblKitItem.DataBind()

                        rblKitItem.Items.Insert(0, New ListItem("None", "0~0~0~0~0~0~0~0~0"))


                        For Each lt As ListItem In rblKitItem.Items
                            If lt.Value.Contains(drow("numDefaultSelect")) Then
                                lt.Selected = True
                                Exit For
                            End If
                        Next

                        If rblKitItem.SelectedIndex = -1 Then
                            rblKitItem.SelectedIndex = 0
                        End If

                        tblcell.Controls.Add(rblKitItem)
                        tblrow.Cells.Add(tblcell)
                        m_dynamicRadioButtonList(i) = rblKitItem

                        tblMain.Rows.Add(tblrow)
                    Else
                        tblrow = New TableRow()
                        tblcell = New TableCell()

                        rblKitItem = New RadioButtonList()
                        rblKitItem.CssClass = "signup"
                        rblKitItem.ID = "rblKitItems" + drow("numItemCode").ToString()

                        Dim str As String = drow("numItemCode") & "~" & drow("numQtyItemsReq") & "~" & drow("vcItemName") & "~" & drow("monListPrice") & "~" & drow("UnitPrice") & "~" & drow("charItemType") & "~" & drow("ItemType") & "~" & drow("txtItemDesc") & "~" & drow("numWarehouseItmsID")
                        rblKitItem.Items.Add(New ListItem(drow("ItemText"), str))
                        rblKitItem.Font.Bold = True

                        rblKitItem.SelectedIndex = 0
                        tblcell.Controls.Add(rblKitItem)
                        tblrow.Cells.Add(tblcell)
                        m_dynamicRadioButtonList(i) = rblKitItem

                        tblMain.Rows.Add(tblrow)
                    End If
                    i += 1
                Next

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveClose.Click
        Try
            save()
            Response.Write("<script language='javascript'>self.close();</script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub save()
        Dim dr As DataRow
        If lngItemCode > 0 Then

            dsTemp = Session("SOItems")
            Dim dtitem As DataTable
            dtitem = dsTemp.Tables(2)
            dtitem.Rows.Clear()
            Dim dr2 As DataRow
            For Each ctrl As RadioButtonList In m_dynamicRadioButtonList
                Dim sColumnsArrayForSearch As String()
                sColumnsArrayForSearch = ctrl.SelectedValue.ToString.Split("~")

                If sColumnsArrayForSearch(0).ToString <> "0" Then
                    dr2 = dtitem.NewRow
                    dr2("numOppChildItemID") = CType(IIf(IsDBNull(dtitem.Compute("MAX(numOppChildItemID)", "")), 0, dtitem.Compute("MAX(numOppChildItemID)", "")), Integer) + 1 'dtChildItems.Rows.Count + 1
                    dr2("numoppitemtCode") = 0
                    dr2("numItemCode") = sColumnsArrayForSearch(0)
                    dr2("numQtyItemsReq") = sColumnsArrayForSearch(1)
                    dr2("vcItemName") = sColumnsArrayForSearch(2)
                    dr2("monListPrice") = sColumnsArrayForSearch(3)
                    dr2("UnitPrice") = sColumnsArrayForSearch(4).Split("/")(0)
                    dr2("charItemType") = sColumnsArrayForSearch(5)
                    dr2("txtItemDesc") = sColumnsArrayForSearch(7)
                    dr2("Op_Flag") = 1
                    dr2("ItemType") = sColumnsArrayForSearch(6)
                    dr2("numWarehouseItmsID") = sColumnsArrayForSearch(8)
                    'If sColumnsArrayForSearch(6) = "P" And chkDropShip.Checked = False Then dr2("numWarehouseItmsID") = IIf(CType(dgGridItem.FindControl("radKitWareHouse"), Telerik.Web.UI.RadComboBox).SelectedValue = "", 0, CType(dgGridItem.FindControl("radKitWareHouse"), Telerik.Web.UI.RadComboBox).SelectedValue)
                    dtitem.Rows.Add(dr2)
                    'Response.Write(ctrl.SelectedValue)
                End If
            Next
            dsTemp.AcceptChanges()
            Session("SOItems") = dsTemp
        End If
    End Sub


End Class