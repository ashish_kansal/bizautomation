<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmLogoUpload.aspx.vb"
    Inherits="BACRM.UserInterface.Opportunities.frmLogoUpload" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Logo Upload</title>
    <script language="javascript">
        function Close() {
            window.close();
        }
        function Save() {
            if (document.form1.txtAmount.value == "") {
                alert("Enter Amount")
                document.form1.txtAmount.focus();
                return false;
            }
        }
    </script>
    <%--<style>
        .info
        {
            color: #00529B;
            background-color: #BDE5F8;
            background-image: url('../images/info.png');
            border: 1px solid;
            margin: 10px 5px 0px 5px;
            padding: 15px 10px 15px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
        }
    </style>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table width="100%">
                <tr>
                    <td align="right" colspan="3">
                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save &amp; Close">
                        </asp:Button>
                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button><br>
                        <br />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Logo Upload
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal1" align="right">
                Select Image
            </td>
            <td>
                <input id="txtMagFile" type="file" name="txtMagFile" runat="server" style="width: 200px" />
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="pnlLogoTip" CssClass="info" Visible="false">
        Tip: BizAutomation.com recommends resizing your brand logo to maximum dimension
        of 285px � 130px, any logo smaller than 285px � 130px shall work!
    </asp:Panel>
</asp:Content>
