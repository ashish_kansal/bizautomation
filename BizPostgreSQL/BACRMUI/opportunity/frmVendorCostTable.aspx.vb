﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.VendorCost
    Public Class frmVendorCostTable
        Inherits BACRMPage
        Dim lngItemCode As Long, lngVendor As Long
        Dim OppType As Short

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lngItemCode = GetQueryStringVal( "ItemCode")
                OppType = GetQueryStringVal( "OppType")
                lngVendor = CCommon.ToLong(GetQueryStringVal( "Vendor"))
                If OppType = 2 Then
                    btnUpdate.Visible = False
                End If
                If Not IsPostBack Then
                    If lngItemCode > 0 Then
                        bindGrid()
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub bindGrid()
            Try
                Dim objOpportunity As New MOpportunity
                objOpportunity.ItemCode = lngItemCode
                objOpportunity.DomainID = Session("DomainID")

                Dim dtTableInfo As DataTable
                dtTableInfo = objOpportunity.GetVendorCostTable()

                Dim i As Integer
                For i = 0 To dtTableInfo.Columns.Count - 1
                    dtTableInfo.Columns(i).ColumnName = dtTableInfo.Columns(i).ColumnName.Replace(".", "")
                Next

                Dim Tfield As TemplateField
                gvVendorCost.Columns.Clear()

                For i = 0 To dtTableInfo.Columns.Count - 1

                    Dim str As String()
                    str = dtTableInfo.Columns(i).ColumnName.Split("~")

                    If str.Length > 1 Then
                        Tfield = New TemplateField

                        Tfield.HeaderTemplate = New MyTemp(ListItemType.Header, dtTableInfo.Columns(i).ColumnName, OppType, lngVendor)

                        Tfield.ItemTemplate = New MyTemp(ListItemType.Item, dtTableInfo.Columns(i).ColumnName, OppType, lngVendor)
                        gvVendorCost.Columns.Add(Tfield)
                    End If
                Next

                gvVendorCost.DataSource = dtTableInfo
                gvVendorCost.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
    End Class


    Public Class MyTemp
        Implements ITemplate

        Dim TemplateType As ListItemType
        Dim VendorId, ColumnName, dbColumnName As String
        Dim Custom As Boolean
        Dim EditPermission As Integer, OppType As Short, Vendor As Long

        Sub New(ByVal type As ListItemType, ByVal fld1 As String, ByVal iOppType As Short, ByVal lngVendor As Long)
            Try
                TemplateType = type
                dbColumnName = fld1

                Dim str As String()
                str = dbColumnName.Split("~")

                VendorId = str(0)
                ColumnName = str(1)
                OppType = iOppType
                Vendor = lngVendor
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub InstantiateIn(ByVal Container As Control) Implements ITemplate.InstantiateIn
            Try
                Dim lbl1 As Label = New Label()
                Dim lbl2 As Label = New Label()
                Dim lbl3 As Label = New Label()
                Dim lnkButton As New LinkButton
                Dim lnk As New HyperLink
                Select Case TemplateType
                    Case ListItemType.Header
                        If dbColumnName = "0~Tier" Or OppType = 2 Then
                            lbl1.ID = dbColumnName
                            lbl1.Text = ColumnName
                            'lbl1.ForeColor = Color.White
                            Container.Controls.Add(lbl1)
                        Else
                            Dim rdb As New RadioButton
                            rdb.GroupName = "gnVendorName"
                            rdb.Text = ColumnName
                            rdb.ID = dbColumnName

                            If dbColumnName.Split("~")(0) = Vendor Then
                                rdb.Checked = True
                            End If

                            Container.Controls.Add(rdb)
                        End If
                    Case ListItemType.Item
                        AddHandler lbl1.DataBinding, AddressOf BindStringColumn
                        Container.Controls.Add(lbl1)
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindStringColumn(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim ControlClass As String = ""

                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
                lbl1.Text = CCommon.ToString(Container.DataItem(dbColumnName))

                'Changed the implementation to avoid error when dbColumnName has value like "Application Equipment, Inc (V"
                'lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, dbColumnName)), "", DataBinder.Eval(Container.DataItem, dbColumnName))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace