﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMaster.Master"
    CodeBehind="frmOpenOrdersList.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmOpenOrdersList" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script language="javascript" type="text/javascript">
        function OpenSetting() {
            window.open('../Items/frmConfItemList.aspx?FormID=57', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenShippingReport(a, b, c) {
            window.open('../opportunity/frmShippingReport.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppBizDocId=' + a + '&ShipReportId=' + b + '&OppId=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=800,height=300,scrollbars=yes,resizable=yes');
        }

        function OpenPrintedShippingLabels(a) {
            window.open('../opportunity/frmShippingLabelImages.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&BoxID=0&ServiceType=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=800,height=300,scrollbars=yes,resizable=yes');
            //window.open('../opportunity/frmShippingLabelImages.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ServiceType=88&toolbar=no,menubar=yes,titlebar=no,left=100,width=800,height=300,scrollbars=yes,resizable=yes');
            //window.open('../opportunity/frmShippingLabelImages.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ServiceType=90&toolbar=no,menubar=yes,titlebar=no,left=100,width=800,height=300,scrollbars=yes,resizable=yes');
        }

        function OpenWindow(a, b, c) {
            var str;
            if (b == 0) {
                str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&DivID=" + a;
            }
            else if (b == 1) {
                str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&DivID=" + a;
            }
            else if (b == 2) {
                str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&klds+7kldf=fjk-las&DivId=" + a;
            }
            document.location.href = str;
        }

        function OpenContact(a, b) {
            var str;
            str = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=deallist&ft6ty=oiuy&CntId=" + a;
            document.location.href = str;
        }

        function OpenOpp(a, b) {
            var str;
            str = "../opportunity/frmOpportunities.aspx?frm=deallist&OpID=" + a;
            document.location.href = str;
        }

        function PrintLabels() {
            var OppIds = '';
            var PickOppIds = '';
            $("#gvSearch tr").each(function () {
                if ($(this).find("#chkSelect").is(':checked')) {
                    OppIds = OppIds + $(this).find("#lbl1").text() + ',';
                    PickOppIds = PickOppIds + $(this).find("#lbl1").text() + '~' + $(this).find("#lbl4").text() + ',';
                }
            });
            OppIds = OppIds.substring(0, OppIds.length - 1);
            document.getElementById('txtOppIDs').value = OppIds;

            OppIds = OppIds.substring(0, OppIds.length - 1);
            document.getElementById('txtPickOppIDs').value = PickOppIds;

            if (OppIds.length > 0 || PickOppIds.length > 0)
                return true;
            else {
                alert('Please select atleast one record!!');
                return false;
            }
        }

        //        function PrintPickList() {
        //            var OppIds = '';
        //            $("#gvSearch tr").each(function () {
        //                if ($(this).find("#chkSelect").is(':checked')) {
        //                    
        //                }
        //            });
        //            OppIds = OppIds.substring(0, OppIds.length - 1);
        //            document.getElementById('txtPickOppIDs').value = OppIds;

        //            if (OppIds.length > 0)
        //                return true;
        //            else {
        //                alert('Please select atleast one record!!');
        //                return false;
        //            }
        //        }

        function ChangeOrderStatus() {
            if (document.getElementById("ddlOrderStatus").value == 0) {
                alert("Select Order Status")
                return false;
            }
            var RecordIDs = '';

            $("#gvSearch tr").each(function () {
                if ($(this).find("#chkSelect").is(':checked')) {
                    RecordIDs = RecordIDs + $(this).find("#lbl1").text() + '~' + $(this).find("#lbl2").text() + '~' + $(this).find("#lbl3").text() + ',';
                }
            });
            RecordIDs = RecordIDs.substring(0, RecordIDs.length - 1);
            document.getElementById('txtDelOppId').value = RecordIDs;

            if (RecordIDs.length > 0)
                return true;
            else {
                alert('Please select atleast one record!!');
                return false;
            }
        }

        function OpenEditBizDocs(a, b) {

            var str;
            str = "../opportunity/frmAddBizDocs.aspx?frm=OpenOrder&OppType=1&OpID=" + a + "&OppBizId=" + b;
            document.location.href = str;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <div class="input-part">
        <div class="view-cases" style="width: 250px;">
            <label>
                View</label>
            <asp:DropDownList ID="ddlView" runat="server" CssClass="signup" AutoPostBack="True">
                <asp:ListItem Value="1">Orders view</asp:ListItem>
                <asp:ListItem Value="2">Items View</asp:ListItem>
                <asp:ListItem Value="3">View 1</asp:ListItem>
                <asp:ListItem Value="4">View 2</asp:ListItem>
                <asp:ListItem Value="5">View 3</asp:ListItem>
            </asp:DropDownList>
            &nbsp;<a href="#" onclick="return OpenSetting()" title="Show Grid Settings.">Settings</a>
        </div>
        <div class="right-input">
            <table align="right">
                <tr>
                    <td>
                        <label>
                            Actions
                        </label>
                        <asp:DropDownList runat="server" ID="ddlActions">
                            <%--Do not change Value of this items as permission is coded based on list value--%>
                            <asp:ListItem Text="Print Shipping Label" Value="1" />
                            <asp:ListItem Text="Print Pick List" Value="2" />
                            <asp:ListItem Text="Print Packing Slip" Value="3" />
                            <%--<asp:ListItem Text="Save Qty Shipped / Close Sales Order" Value="4" /><asp:ListItem Text="" Value="5" />--%>
                        </asp:DropDownList>
                        <asp:Button ID="btnActions" CssClass="button" runat="server" Text="Go" OnClientClick="return PrintLabels();">
                        </asp:Button>
                    </td>
                    <td>
                        <label>
                            Change Order Status to</label>
                    </td>
                    <td align="right">
                        <asp:DropDownList ID="ddlOrderStatus" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="btnChangeStatus" OnClientClick="return ChangeOrderStatus()" CssClass="button"
                            Text="Go" runat="server"></asp:Button>&nbsp;&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <center>
        <span style="color: Red">
            <asp:Literal ID="litMessage" runat="server"></asp:Literal></span></center>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Shipping Fulfillment List
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
    <webdiyer:AspNetPager ID="bizPager" runat="server" Direction="RightToLeft" HorizontalAlign="Right"
        LayoutType="div" UrlPaging="false" CssClass="pagn" ShowMoreButtons="true" ShowPageIndexBox="Never"
        Width="" AlwaysShow="true" ShowCustomInfoSection="Left" CustomInfoHTML="Showing records %startrecordindex% to %endrecordindex% of %recordcount% "
        CustomInfoSectionWidth="300px" CustomInfoStyle="line-height:20px;margin-right:3px;text-align:right !important;">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <asp:Panel ID="pnlDefault" runat="server" DefaultButton="btnGo1">
        <asp:GridView ID="gvSearch" runat="server" EnableViewState="true" AutoGenerateColumns="false"
            CssClass="tbl" Width="100%" ShowHeaderWhenEmpty="true">
            <Columns>
            </Columns>
            <EmptyDataTemplate>
                No records Found.
            </EmptyDataTemplate>
        </asp:GridView>
    </asp:Panel>
    <asp:DataGrid ID="dg" runat="server" Style="display: none">
    </asp:DataGrid>
    <asp:TextBox ID="txtDelOppId" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtOppIDs" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtPickOppIDs" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPageDeals" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecordsDeals" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
