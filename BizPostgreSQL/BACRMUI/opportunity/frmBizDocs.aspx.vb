'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Workflow

Namespace BACRM.UserInterface.Opportunities
    Public Class frmBizDocs
        Inherits BACRMPage

        Dim IsOrderClosed As Boolean = False
        Dim OppType As Short
        Dim lngOppId As Long
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub



        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                OppType = CCommon.ToInteger(GetQueryStringVal("OppType"))
                GetUserRightsForPage(10, 6)
                lngOppId = CCommon.ToLong(GetQueryStringVal("OpID"))

                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    litMessage.Text = "You Are Not Authorized to View This Page"
                    btnAdd.Visible = False
                    Exit Sub
                Else : If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                        btnAdd.Visible = False
                        btnSave.Visible = False
                    End If
                End If
                If Not IsPostBack Then
                    If OppType = 1 Then
                        'GET DOMAIN RELEASE INVENTORY SETTING
                        objCommon.DomainID = Session("DomainID")
                        Dim dtCommitAllocation As DataTable = objCommon.GetDomainSettingValue("tintCommitAllocation")

                        If Not dtCommitAllocation Is Nothing AndAlso dtCommitAllocation.Rows.Count > 0 Then
                            hdnCommitAllocation.Value = CCommon.ToShort(CCommon.ToShort(dtCommitAllocation.Rows(0)("tintCommitAllocation")))
                            If CCommon.ToShort(dtCommitAllocation.Rows(0)("tintCommitAllocation")) = 2 Then
                                Dim lngDivId As Long
                                Dim objOppBizDocs As New BACRM.BusinessLogic.Opportunities.OppBizDocs
                                objOppBizDocs.DomainID = CCommon.ToLong(Session("DomainID"))
                                objOppBizDocs.OppId = lngOppId
                                lngDivId = objOppBizDocs.GetDivisionId()

                                'GET CUSTOMER DEFAULT SETTING TO CHECK WHETHER INVENTORY NEEDS TO BE ALLOCATED ON PICK LIST
                                Dim objAccount As New BACRM.BusinessLogic.Account.CAccounts
                                objAccount.DivisionID = lngDivId
                                Dim dtSetting As DataTable = objAccount.GetDefaultSettingValue("bitAllocateInventoryOnPickList")

                                If Not dtSetting Is Nothing AndAlso dtSetting.Rows.Count > 0 Then
                                    hdnAllocateOnPickList.Value = CCommon.ToBool(dtSetting.Rows(0)("bitAllocateInventoryOnPickList"))
                                Else
                                    Throw New Exception("Not able to fetch customer default settings for Allocate inventory of pick list")
                                    Exit Sub
                                End If
                            End If
                        Else
                            Throw New Exception("Not able to fetch commmit allocation global settings value.")
                            Exit Sub
                        End If
                    End If

                    CheckRecurring()
                    getDetails()
                    bindBizDocs()
                    bindARAccounts()
                    VerifyCreditCardStatus()
                    btnSave.Attributes.Add("onclick", "return Save()")
                    'bindBizDocSFList()
                    litMessage.Text = CCommon.ToString(Session("CloneResult"))
                    Session("CloneResult") = Nothing

                End If

                If IsPostBack Then
                    Dim eventTarget As String = If((Me.Request("__EVENTTARGET") Is Nothing), String.Empty, Me.Request("__EVENTTARGET"))

                    If eventTarget = "btnReloadGrid" Then
                        getDetails()
                    End If
                    'bindBizDocSFList()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Private Sub bindARAccounts()
            Try
                Dim objCOA As New ChartOfAccounting
                objCOA.DomainID = CCommon.ToLong(Session("DomainID"))
                Dim dt As DataTable = objCOA.GetDefaultARAndChildAccounts()
                ddlAR.DataSource = dt
                ddlAR.DataTextField = "vcAccountName"
                ddlAR.DataValueField = "numAccountID"
                ddlAR.DataBind()

                ddlAR.Items.Insert(0, New ListItem("--Select One --", "0"))

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    If Not ddlAR.Items.FindByValue(CCommon.ToString(dt.Rows(0)("numDefaultARAccountID"))) Is Nothing Then
                        ddlAR.Items.FindByValue(CCommon.ToString(dt.Rows(0)("numDefaultARAccountID"))).Selected = True
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub VerifyCreditCardStatus()
            Dim dtDetails As DataTable
            Dim objOpportunity As New MOpportunity
            objOpportunity.OpportunityId = lngOppId
            objOpportunity.DomainID = Session("DomainID")
            objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            objOpportunity.ItemCode = 0

            dtDetails = objOpportunity.OpportunityDTL.Tables(0)
            If dtDetails.Rows.Count > 0 Then
                Dim objOppInvoice As New OppInvoice
                Dim dsCCInfo As DataSet
                objOppInvoice.DomainID = Session("DomainID")
                objOppInvoice.DivisionID = CCommon.ToLong(dtDetails.Rows(0).Item("numDivisionID"))
                objOppInvoice.numCCInfoID = 0
                objOppInvoice.IsDefault = 1
                objOppInvoice.bitflag = 1
                dsCCInfo = objOppInvoice.GetCustomerCreditCardInfo()
                If dsCCInfo.Tables(0).Rows.Count > 0 Then
                    hdnAuthorizeCreditCard.Value = 1
                    lblAuthorizedCard.Visible = True
                    lblAuthorizedCard.Text = "Authorized On " + Convert.ToString(dsCCInfo.Tables(0).Rows(0)("dtCreated"))
                Else
                    hdnAuthorizeCreditCard.Value = 0
                    lblAuthorizedCard.Visible = False
                End If
            End If
        End Sub

        Sub bindBizDocs()
            Try
                Dim objCommon As New CCommon
                Dim dtMasterItem As DataTable
                dtMasterItem = objCommon.GetMasterListItemsForBizDocs(27, Session("DomainID"), CCommon.ToLong(GetQueryStringVal("OpID")))

                ddlBizDocs.DataSource = dtMasterItem
                ddlBizDocs.DataTextField = "vcData"
                ddlBizDocs.DataValueField = "numListItemID"
                ddlBizDocs.DataBind()
                'ddlBizDocs.Items.Insert(0, New ListItem("--Select One--", "0"))


                Dim objOpportunity As New MOpportunity
                objOpportunity.OpportunityId = lngOppId
                Dim dt As DataSet = objOpportunity.CheckOrderedAndInvoicedOrBilledQty()
                Dim bitNoQtyLeftForDiferredBizDoc As Boolean = CCommon.ToBool(dt.Tables(0).Select("ID=5")(0)("bitTrue"))

                'DISABLE DEFERRED BIZDOC IF FUNCTIONALITY IS NOT ENABLED FROM GLOBAL SETTINGS --> ACCOUNTING
                If Not CCommon.ToBool(Session("IsEnableDeferredIncome")) Then
                    If Not ddlBizDocs.Items.FindByValue("304") Is Nothing Then
                        ddlBizDocs.Items.FindByValue("304").Attributes.Add("style", "color:lightgray")
                        ddlBizDocs.Items.FindByValue("304").Enabled = False
                    End If
                ElseIf OppType = 1 AndAlso bitNoQtyLeftForDiferredBizDoc Then 'DISABLE DEFERRED BIZDOC IF ALL ITEMS QTY IS ADDED IN INVOICE OR DIFERRED DIZDOC
                    If Not ddlBizDocs.Items.FindByValue("304") Is Nothing Then
                        ddlBizDocs.Items.FindByValue("304").Attributes.Add("style", "color:lightgray")
                        ddlBizDocs.Items.FindByValue("304").Enabled = False
                    End If
                End If

                BindBizDocsTemplate()

                Dim objOppBizDocs As New OppBizDocs()
                objOppBizDocs.BizDocId = ddlBizDocs.SelectedValue 'CCommon.ToLong(IIf(ddlBizDocs.Enabled, ddlBizDocs.SelectedValue, Request(ddlBizDocs.ClientID)))
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.OppId = lngOppId
                objOppBizDocs.OppBizDocId = 0
                objOppBizDocs.boolRentalBizDoc = 0

                Dim dtOppItems As DataTable = objOppBizDocs.GetOppItemsForBizDocs()
                Dim dr As DataRow() = dtOppItems.Select("numRemainingPercent>1")
                If (dr.Length > 0) Then
                    lblPendingItemQty.Visible = True
                    lblPendingItemQty.Text = "% / Qty pending on " & dr.Length & " of " & dtOppItems.Rows.Count & " items"
                ElseIf dtOppItems.Select("numRemainingPercent>1").Length <> dtOppItems.Rows.Count Then
                    lblPendingItemQty.Visible = True
                    lblPendingItemQty.Text = "% / Qty pending on " & dtOppItems.Select("numRemainingPercent>1").Length & " of " & dtOppItems.Rows.Count & " items"
                Else
                    lblPendingItemQty.Visible = False
                End If


            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Function GenerateFulfillmentRulesScript(ByVal dtRules As DataTable) As String
        '    Try
        '        Dim sb As New System.Text.StringBuilder
        '        Dim iRowIndex As Integer = 0

        '        sb.Append(vbCrLf & "var arrFulfillmentRulesFieldConfig = new Array();" & vbCrLf)     'Create a array to hold the field information in javascript

        '        For Each dr As DataRow In dtRules.Rows
        '            sb.Append("arrFulfillmentRulesFieldConfig[" & iRowIndex & "] = new FulfillmentRules('" & dr("numRuleID").ToString & "','296','" & dr("numBizDocStatus1").ToString() & "');" & vbCrLf)
        '            iRowIndex += 1
        '        Next

        '        Return sb.ToString
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

        Private Sub ddlBizDocs_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBizDocs.SelectedIndexChanged
            Try
                BindBizDocsTemplate()

                If ddlBizDocs.SelectedValue = "287" Or ddlBizDocs.SelectedValue = "296" Then
                    divAR.Visible = True
                Else
                    divAR.Visible = False
                End If

                If CCommon.ToBool(Session("IsEnableDeferredIncome")) AndAlso ddlBizDocs.SelectedValue = "287" AndAlso hdnIsDeferredBizDocAdded.Value = "1" Then
                    pnlInvoiceDeferredIncome.Visible = True
                Else
                    pnlInvoiceDeferredIncome.Visible = False
                End If

                If ddlBizDocs.SelectedValue = "296" AndAlso hdnCommitAllocation.Value = "2" AndAlso CCommon.ToBool(hdnAllocateOnPickList.Value) Then
                    btnAdd.Visible = False
                Else
                    btnAdd.Visible = True
                End If
                Dim objOppBizDocs As New OppBizDocs()
                objOppBizDocs.BizDocId = ddlBizDocs.SelectedValue 'CCommon.ToLong(IIf(ddlBizDocs.Enabled, ddlBizDocs.SelectedValue, Request(ddlBizDocs.ClientID)))
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.OppId = lngOppId
                objOppBizDocs.OppBizDocId = 0
                objOppBizDocs.boolRentalBizDoc = 0

                Dim dtOppItems As DataTable = objOppBizDocs.GetOppItemsForBizDocs()
                Dim dr As DataRow() = dtOppItems.Select("numRemainingPercent>1")
                If (dr.Length > 0) Then
                    lblPendingItemQty.Visible = True
                    lblPendingItemQty.Text = "% / Qty pending on " & dr.Length & " of " & dtOppItems.Rows.Count & " items"
                ElseIf dtOppItems.Select("numRemainingPercent>1").Length <> dtOppItems.Rows.Count Then
                    lblPendingItemQty.Visible = True
                    lblPendingItemQty.Text = "% / Qty pending on " & dtOppItems.Select("numRemainingPercent>1").Length & " of " & dtOppItems.Rows.Count & " items"
                Else
                    lblPendingItemQty.Visible = False
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindBizDocsTemplate()
            Try
                Dim objOppBizDoc As New OppBizDocs
                objOppBizDoc.DomainID = Session("DomainID")
                objOppBizDoc.BizDocId = ddlBizDocs.SelectedValue
                objOppBizDoc.OppType = OppType
                objOppBizDoc.byteMode = 0

                Dim dtBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()

                ddlBizDocTemplate.DataSource = dtBizDocTemplate
                ddlBizDocTemplate.DataTextField = "vcTemplateName"
                ddlBizDocTemplate.DataValueField = "numBizDocTempID"
                ddlBizDocTemplate.DataBind()

                ddlBizDocTemplate.Items.Insert(0, New ListItem("--Select--", 0))

                objOppBizDoc.byteMode = 1


                objOppBizDoc.Relationship = CCommon.ToLong(hdnReplationship.Value)
                objOppBizDoc.Profile = CCommon.ToLong(hdnProfile.Value)
                objOppBizDoc.AccountClass = CCommon.ToLong(hdnAccountClass.Value)
                Dim dtDefaultBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()

                If Not IsDBNull(dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID")) Then
                    If Not ddlBizDocTemplate.Items.FindByValue(dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID")) Is Nothing Then
                        ddlBizDocTemplate.ClearSelection()
                        ddlBizDocTemplate.SelectedValue = dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID").ToString
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub getDetails()
            Try
                Dim dtTable As DataTable = Nothing
                Dim dtOppDetails As DataTable = Nothing
                Dim objBizDocs As New OppBizDocs
                objBizDocs.DomainID = Session("DomainID")
                objBizDocs.UserCntID = Session("UserContactID")
                objBizDocs.OppId = CCommon.ToLong(GetQueryStringVal("OpID"))
                objBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                Dim dsBizDocs As DataSet = objBizDocs.GetBizDocsByOOpId

                If Not dsBizDocs Is Nothing AndAlso dsBizDocs.Tables.Count > 1 Then
                    dtOppDetails = dsBizDocs.Tables(0)
                    dtTable = dsBizDocs.Tables(1)
                End If

                If Not dtOppDetails Is Nothing AndAlso dtOppDetails.Rows.Count > 0 Then
                    hdnReplationship.Value = CCommon.ToLong(dtOppDetails.Rows(0)("numCompanyType"))
                    hdnProfile.Value = CCommon.ToLong(dtOppDetails.Rows(0)("vcProfile"))
                    hdnAccountClass.Value = CCommon.ToLong(dtOppDetails.Rows(0)("numAccountClass"))
                End If

                If Not dtTable Is Nothing AndAlso dtTable.Rows.Count > 0 Then
                    IsOrderClosed = CCommon.ToBool(dtTable.Rows(0)("tintShipped"))
                    If dtTable.Select("numBizDocID=304").Count() > 0 Then '304 - Deferred BizDoc
                        hdnIsDeferredBizDocAdded.Value = "1"
                    Else
                        hdnIsDeferredBizDocAdded.Value = "0"
                    End If
                Else
                    hdnIsDeferredBizDocAdded.Value = "0"
                End If

                Dim arrDr As DataRow() = dtTable.Select("bitRecur=1")

                If Not arrDr Is Nothing AndAlso arrDr.Count() > 0 Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "RefreshParent", "<script language='javascript'>HideSalesOrderRecurrence()</script>", False)
                End If


                dgBizDocs.DataSource = dtTable
                dgBizDocs.DataBind()

                Dim objOpportunity As New MOpportunity
                objOpportunity.OpportunityId = lngOppId
                Dim dt As DataSet = objOpportunity.CheckOrderedAndInvoicedOrBilledQty()
                Dim bitNoQtyLeftForDiferredBizDoc As Boolean = CCommon.ToBool(dt.Tables(0).Select("ID=5")(0)("bitTrue"))

                If OppType = 1 AndAlso bitNoQtyLeftForDiferredBizDoc Then 'DISABLE DEFERRED BIZDOC IF ALL ITEMS QTY IS ADDED IN INVOICE OR DIFERRED DIZDOC
                    If Not ddlBizDocs.Items.FindByValue("304") Is Nothing Then
                        ddlBizDocs.Items.FindByValue("304").Attributes.Add("style", "color:lightgray")
                        ddlBizDocs.Items.FindByValue("304").Enabled = False
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgBizDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgBizDocs.ItemCommand
            Try
                Dim objOppBizDocs As New OppBizDocs
                Dim lintAuthorizativeBizDocsId As Integer
                Dim lobjJournalEntry As New JournalEntry
                Dim lintCount As Integer

                If e.CommandName = "Delete" Then
                    Dim txtOppBizId As TextBox
                    Dim txtRecurringBizDoc As TextBox
                    Dim txtRecurredBizDoc As TextBox
                    Dim lintJournalId As Integer
                    Dim lblBizDocID As Label
                    txtOppBizId = e.Item.FindControl("txtBizDocID")
                    lblBizDocID = e.Item.FindControl("lblBizDocID")
                    txtRecurringBizDoc = e.Item.FindControl("txtRecurringBizDoc")
                    txtRecurredBizDoc = e.Item.FindControl("txtRecurredBizDoc")

                    Dim objBizDocs As New OppBizDocs

                    objBizDocs.OppBizDocId = txtOppBizId.Text
                    objBizDocs.OppId = CCommon.ToLong(GetQueryStringVal("OpID"))
                    objBizDocs.DomainID = Session("DomainID")
                    objBizDocs.UserCntID = Session("UserContactID")

                    objOppBizDocs.OppId = CCommon.ToLong(GetQueryStringVal("OpID"))
                    objOppBizDocs.UserCntID = Session("UserContactID")
                    objOppBizDocs.DomainID = Session("DomainID")
                    lintCount = objBizDocs.GetOpportunityBizDocsCount()
                    objOppBizDocs.OppType = OppType
                    lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()
                    objOppBizDocs.OppBizDocId = txtOppBizId.Text
                    lintJournalId = objOppBizDocs.GetJournalIdForAuthorizativeBizDocs
                    If lintAuthorizativeBizDocsId = lblBizDocID.Text Then

                        If CCommon.ToBool(CCommon.ToInteger(txtRecurringBizDoc.Text)) Or
                           CCommon.ToBool(CCommon.ToInteger(txtRecurredBizDoc.Text)) Then
                            litMessage.Text = "BizDoc can not be deleted when order or bizdoc is recurring or bizdoc is recurred from another bizdoc."
                            Exit Sub
                        End If

                        If lintCount = 0 Then
                            ''To Delete Journal Entry
                            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                If lintJournalId <> 0 Then
                                    lobjJournalEntry.JournalId = lintJournalId
                                    lobjJournalEntry.DomainID = Session("DomainId")
                                    lobjJournalEntry.DeleteJournalEntryDetails()
                                End If
                                objBizDocs.DeleteComissionJournal()

                                Dim objAutomatonRule As New AutomatonRule
                                objAutomatonRule.ExecuteAutomationRule(49, objOppBizDocs.OppBizDocId, 4)

                                objBizDocs.DeleteBizDoc()

                                objTransactionScope.Complete()
                            End Using
                            ''Added By Sachin Sadhu||Date:8thMay2014
                            ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                            ''          Using Change tracking
                            Dim objWfA As New Workflow()
                            objWfA.DomainID = Session("DomainID")
                            objWfA.UserCntID = Session("UserContactID")
                            objWfA.RecordID = CCommon.ToLong(txtOppBizId.Text)
                            objWfA.SaveWFBizDocQueue()
                            'end of code
                            Response.Redirect("frmBizDocs.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=" & GetQueryStringVal("OpID") & "&OppType=" & OppType, False)
                        Else
                            litMessage.Text = IIf(OppType = 1, "You cannot delete BizDocs for which amount is deposited", "You cannot delete BizDocs for which amount is paid")
                        End If
                    Else
                        'Added by chintan, cause:bug id 1405
                        Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                            If lintJournalId <> 0 Then
                                lobjJournalEntry.JournalId = lintJournalId
                                lobjJournalEntry.DomainID = Session("DomainId")
                                lobjJournalEntry.DeleteJournalEntryDetails()
                            End If
                            objBizDocs.DeleteComissionJournal()
                            'end

                            Dim objAutomatonRule As New AutomatonRule
                            objAutomatonRule.ExecuteAutomationRule(49, objOppBizDocs.OppBizDocId, 4)

                            objBizDocs.DeleteBizDoc()

                            objTransactionScope.Complete()
                        End Using
                        ''Added By Sachin Sadhu||Date:8thMay2014
                        ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                        ''          Using Change tracking
                        Dim objWfA As New Workflow()
                        objWfA.DomainID = Session("DomainID")
                        objWfA.UserCntID = Session("UserContactID")
                        objWfA.RecordID = CCommon.ToLong(txtOppBizId.Text)
                        objWfA.SaveWFBizDocQueue()
                        'end of code

                        Response.Redirect("frmBizDocs.aspx?OpID=" & GetQueryStringVal("OpID") & "&OppType=" & OppType, False)
                    End If
                ElseIf e.CommandName = "Email" Or e.CommandName = "PDF" Or e.CommandName = "Print" Then
                    CreateHTMLBizDocs(e.CommandName, CCommon.ToLong(GetQueryStringVal("OpID")), CCommon.ToLong(CType(e.Item.FindControl("txtBizDocID"), TextBox).Text), _
                                      CType(e.Item.FindControl("txtConEmail"), TextBox).Text, CCommon.ToLong(CType(e.Item.FindControl("txtConID"), TextBox).Text), _
                                      CCommon.ToLong(CType(e.Item.FindControl("txtBizDocTemplate"), TextBox).Text), _
                                      CCommon.ToString(CType(e.Item.FindControl("lblBizDocName"), Label).Text).Trim.Replace(" ", "_"), _
                                      CCommon.ToLong(CType(e.Item.FindControl("txtOrientation"), TextBox).Text), _
                                      CCommon.ToBool(CCommon.ToLong(CType(e.Item.FindControl("txtKeepFooterBottom"), TextBox).Text)), _
                                      CCommon.ToLong(CType(e.Item.FindControl("lblBizDocID"), Label).Text))
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub CreateHTMLBizDocs(CommandType As String, lngOppID As Long, lngOppBizDocID As Long, strConEmail As String, lngConID As Long, lngBizDocTemplate As Long, strBizDocName As String, numOrientation As Long, keepFooterBottom As Boolean, bizDocID As Long)
            Try
                If CommandType = "Email" Then
                    Session("Attachements") = Nothing 'reset to nothing to avoid duplicate attachments
                    Dim objBizDocs As New OppBizDocs
                    Dim dt As DataTable
                    objBizDocs.OppId = lngOppID
                    objBizDocs.BizDocId = lngOppBizDocID
                    objBizDocs.DomainID = Session("DomainID")
                    objBizDocs.BizDocTemplateID = lngBizDocTemplate
                    objBizDocs.intAddReferenceDocument = 1
                    dt = objBizDocs.GetBizDocAttachments
                    Dim i As Integer
                    Dim strFileName As String = ""
                    Dim strFilePhysicalLocation As String = ""
                    For i = 0 To dt.Rows.Count - 1
                        If dt.Rows(i).Item("vcDocName") = "BizDoc" Then
                            strFileName = "File" & Format(Now, "ddmmyyyyhhmmssfff") & ".pdf"
                            strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName

                            Dim objHTMLToPDF As New HTMLToPDF
                            System.IO.File.WriteAllBytes(strFilePhysicalLocation, objHTMLToPDF.CreateBizDocPDF(lngOppID, lngOppBizDocID, CCommon.ToLong(Session("DomainID")), CCommon.ToLong(Session("UserContactID")), CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")), CCommon.ToString(Session("DateFormat"))))

                            strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
                            objCommon.AddAttchmentToSession(dt.Rows(i).Item("vcFileName") & ".pdf", CCommon.GetDocumentPath(Session("DomainID")) & strFileName, strFilePhysicalLocation)
                        Else
                            strFileName = dt.Rows(i).Item("vcURL")
                            strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
                            objCommon.AddAttchmentToSession(strFileName, CCommon.GetDocumentPath(Session("DomainID")) & strFileName, strFilePhysicalLocation)
                        End If
                    Next

                    If bizDocID = 287 Then
                        Dim objOpp As New MOpportunity
                        objOpp.DomainID = CCommon.ToLong(Session("DomainID"))
                        objOpp.OpportunityId = lngOppID
                        Dim dtContact As DataTable = objOpp.GetARContact()

                        If Not dtContact Is Nothing AndAlso dtContact.Rows.Count > 0 Then
                            strConEmail = CCommon.ToString(dtContact.Rows(0)("vcEmail"))
                            lngConID = CCommon.ToLong(dtContact.Rows(0)("numContactId"))
                        End If
                    End If

                    Dim str As String
                    str = "<script language='javascript'>window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&PickAtch=1&isAttachmentRequired=1&Lsemail=" & HttpUtility.JavaScriptStringEncode(strConEmail) & "&pqwRT=" & lngConID & "&frm=BizInvoice&BizDocID=" & lngOppBizDocID.ToString() & "&OppID=" & lngOppID.ToString() & "&OppType=" & OppType & "','ComposeWindow','toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')</script>"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Email", str, False)
                ElseIf CommandType = "PDF" Then
                    Dim strFileName As String = "File" & Format(Now, "ddmmyyyyhhmmssfff") & ".pdf"
                    Dim strFilePhysicalLocation As String = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName

                    Dim objHTMLToPDF As New HTMLToPDF
                    System.IO.File.WriteAllBytes(strFilePhysicalLocation, objHTMLToPDF.CreateBizDocPDF(lngOppID, lngOppBizDocID, CCommon.ToLong(Session("DomainID")), CCommon.ToLong(Session("UserContactID")), CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")), CCommon.ToString(Session("DateFormat"))))

                    Response.Clear()
                    Response.ClearContent()
                    Response.ContentType = "application/pdf"
                    Response.AddHeader("content-disposition", "attachment; filename=" + IIf(strBizDocName.Length > 0, strBizDocName & ".pdf", strFileName))
                    Response.WriteFile(strFilePhysicalLocation)
                    Response.End()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgBizDocs_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgBizDocs.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Header Then
                    CType(e.Item.FindControl("lblRefOrderNo"), Label).Text = IIf(OppType = 1, "P.O #", "Invoice #")
                End If
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim btnDelete As LinkButton
                    Dim lnkDelete As LinkButton
                    Dim hdnShippingVisible As HiddenField
                    Dim hypShippingBox As HyperLink
                    Dim hypShippingReport As HyperLink

                    lnkDelete = e.Item.FindControl("lnkDelete")
                    btnDelete = e.Item.FindControl("btnDelete")
                    hdnShippingVisible = e.Item.FindControl("hdnShippingVisible")
                    hypShippingBox = e.Item.FindControl("hypShippingBox")
                    hypShippingReport = e.Item.FindControl("hypShippingReport")

                    hypShippingBox.Visible = hdnShippingVisible.Value

                    Dim editImage As System.Web.UI.WebControls.Image
                    editImage = e.Item.FindControl("editImage")

                    Dim lnkEdit As LinkButton
                    lnkEdit = e.Item.FindControl("lnkEdit")

                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                        btnDelete.Visible = False
                        lnkDelete.Visible = True
                        lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    Else : btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If

                    If CType(e.Item.FindControl("txtRecurringBizDoc"), TextBox).Text = "1" Or
                        CType(e.Item.FindControl("txtRecurredBizDoc"), TextBox).Text = "1" Then
                        btnDelete.Attributes.Remove("onclick")
                        btnDelete.Attributes.Add("onclick", "return DeleteRecurringBizDoc()")
                    End If

                    If CType(e.Item.FindControl("txtRecurredBizDoc"), TextBox).Text = "1" Then
                        Dim hplParentBizDoc As HyperLink = CType(e.Item.FindControl("hplParentBizDoc"), HyperLink)

                        If Not hplParentBizDoc Is Nothing Then
                            Dim objBizRecurrence As New BizRecurrence
                            Dim dt As DataTable = objBizRecurrence.GetParentDetail(CCommon.ToLong(CType(e.Item.FindControl("txtBizDocID"), TextBox).Text), 2)

                            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 AndAlso CCommon.ToLong(dt.Rows(0)("RowNo")) > 0 Then
                                hplParentBizDoc.Visible = True
                                hplParentBizDoc.NavigateUrl = "javascript:OpenOpp(" & CCommon.ToLong(dt.Rows(0)("numParentOppID")) & ")"
                                hplParentBizDoc.Text = "(" & CCommon.ToString(dt.Rows(0)("RowNo")) & " recurrence from " & CCommon.ToString(dt.Rows(0)("Name")) & ")"
                                hplParentBizDoc.Attributes.Add("onclick", "return OpenBizInvoice('" & CCommon.ToLong(dt.Rows(0)("numParentOppID")) & "','" & CCommon.ToLong(dt.Rows(0)("numParentBizDocID")) & "',0)")
                            End If

                        End If
                    End If

                    If Not e.Item.FindControl("txtIsAuthBizDoc") Is Nothing Then
                        If IsOrderClosed And CType(e.Item.FindControl("txtIsAuthBizDoc"), TextBox).Text = "1" Then
                            btnDelete.Attributes.Add("style", "Display:none")
                            btnDelete.Text = ""
                            lnkDelete.Visible = True
                            lnkDelete.Attributes.Add("onclick", "return DeleteMessage1()")
                        End If
                    End If

                    If DirectCast(e.Item.DataItem, System.Data.DataRowView).Row("numBizDocID") = 296 Then 'Fulfillment BizDoc
                        If IsOrderClosed Then
                            btnDelete.Attributes.Add("style", "Display:none")
                            btnDelete.Text = ""
                            lnkDelete.Visible = True
                            lnkDelete.Attributes.Add("onclick", "return DeleteBizDoc('fulfillment')")
                        End If
                    ElseIf DirectCast(e.Item.DataItem, System.Data.DataRowView).Row("numBizDocID") = 29397 Then 'Pick List
                        If CCommon.ToShort(hdnCommitAllocation.Value) = 2 AndAlso CCommon.ToBool(hdnAllocateOnPickList.Value) AndAlso IsOrderClosed Then
                            editImage.Attributes.Add("style", "Display:none")
                            lnkEdit.Visible = True
                            lnkEdit.Attributes.Add("onclick", "return EditPickListNotAllowed()")

                            btnDelete.Attributes.Add("style", "Display:none")
                            btnDelete.Text = ""
                            lnkDelete.Visible = True
                            lnkDelete.Attributes.Add("onclick", "return DeleteBizDoc('pick list')")
                        ElseIf CCommon.ToShort(hdnCommitAllocation.Value) = 2 AndAlso CCommon.ToBool(hdnAllocateOnPickList.Value) AndAlso CCommon.ToBool(DirectCast(e.Item.DataItem, System.Data.DataRowView).Row("IsFulfillmentGeneratedOnPickList")) Then
                            editImage.Attributes.Add("style", "Display:none")
                            lnkEdit.Visible = True
                            lnkEdit.Attributes.Add("onclick", "alert('You can't edit pick list bizdoc once fulfillment bizdoc is generated from it.'); return false;")

                            btnDelete.Attributes.Add("style", "Display:none")
                            btnDelete.Text = ""
                            lnkDelete.Visible = True
                            lnkDelete.Attributes.Add("onclick", "alert('You can't delete pick list bizdoc once fulfillment bizdoc is generated from it.'); return false;")
                        End If
                    ElseIf DirectCast(e.Item.DataItem, System.Data.DataRowView).Row("numBizDocID") = 304 Then
                        If IsOrderClosed Then
                            btnDelete.Attributes.Add("style", "Display:none")
                            btnDelete.Text = ""
                            lnkDelete.Visible = True
                            lnkDelete.Attributes.Add("onclick", "return DeleteBizDoc('deferred income')")
                        ElseIf CCommon.ToLong(DirectCast(e.Item.DataItem, System.Data.DataRowView).Row("intInvoiceForDiferredBizDoc")) > 0 Then
                            btnDelete.Attributes.Add("style", "Display:none")
                            btnDelete.Text = ""
                            lnkDelete.Visible = True
                            lnkDelete.Attributes.Add("onclick", "return InvoiceAgainstDiferredBizDoc()")

                            editImage.Attributes.Add("style", "Display:none")
                            lnkEdit.Visible = True
                            lnkEdit.Attributes.Add("onclick", "return EditDeferredBizDocNotAllowed()")
                        End If
                    End If

                    If CCommon.ToBool(hdnRecurringOrder.Value) Then
                        btnDelete.Visible = False
                    End If

                    Dim hpl As HyperLink
                    hpl = e.Item.FindControl("hplBizdcoc")
                    Dim txtOppBizId As TextBox
                    txtOppBizId = e.Item.FindControl("txtBizDocID")
                    hpl.Attributes.Add("onclick", "return OpenBizInvoice('" & GetQueryStringVal("OpID") & "','" & txtOppBizId.Text & "',0)")

                    'hplEditBizdcoc = e.Item.FindControl("hplEditBizdcoc")
                    'hplEditBizdcoc.Attributes.Add("onclick", "return OpenEdit('" & GetQueryStringVal("OpID") & "','" & txtOppBizId.Text & "','" & OppType & "')")

                    'Added by Neelam - To select bizDoc and template in edit mode
                    Dim lblBizDocID As Label
                    lblBizDocID = e.Item.FindControl("lblBizDocID")
                    Dim txtBizDocTemplate As TextBox
                    txtBizDocTemplate = e.Item.FindControl("txtBizDocTemplate")

                    If editImage IsNot Nothing AndAlso lblBizDocID IsNot Nothing AndAlso txtBizDocTemplate IsNot Nothing Then
                        'editImage.Attributes.Add("onclick", "return OpenEdit('" & GetQueryStringVal("OpID") & "','" & txtOppBizId.Text & "','" & OppType & "')")
                        editImage.Attributes.Add("onclick", "return OpenEdit('" & GetQueryStringVal("OpID") & "','" & txtOppBizId.Text & "','" & OppType & "','" & lblBizDocID.Text & "','" & txtBizDocTemplate.Text & "')")
                    End If

                    Dim ibtnPrint As ImageButton
                    ibtnPrint = e.Item.FindControl("ibtnPrint")
                    ibtnPrint.Attributes.Add("onclick", "return OpenBizInvoice('" & GetQueryStringVal("OpID") & "','" & txtOppBizId.Text & "',1)")

                    Dim ibtnPayments As ImageButton
                    ibtnPayments = e.Item.FindControl("ibtnPayments")
                    ibtnPayments.Visible = IIf(OppType = 1 AndAlso CCommon.ToBool(DataBinder.Eval(e.Item.DataItem, "bitAuthoritativeBizDocs")), True, False)
                    ibtnPayments.Attributes.Add("onclick", "return OpenAmtPaid('" & GetQueryStringVal("OpID") & "','" & txtOppBizId.Text & "','" & DataBinder.Eval(e.Item.DataItem, "numDivisionId") & "')")

                    'Dim lblBizDocID As Label
                    'lblBizDocID = e.Item.FindControl("lblBizDocID")
                    If lblBizDocID IsNot Nothing Then
                        Dim ibtnCloneBizdoc As ImageButton
                        ibtnCloneBizdoc = e.Item.FindControl("ibtnCloneBizdoc")
                        ibtnCloneBizdoc.Attributes.Add("onclick", "return CloneBizdoc('" & GetQueryStringVal("OpID") & "','" & txtOppBizId.Text & "','" & DataBinder.Eval(e.Item.DataItem, "numDivisionId") & "','" & CCommon.ToLong(lblBizDocID.Text) & "','" & OppType & "')")

                    End If

                    If Not e.Item.FindControl("txtIsAuthBizDoc") Is Nothing Then
                        If Not CType(e.Item.FindControl("txtIsAuthBizDoc"), TextBox).Text = "1" Then
                            ibtnPayments.Visible = False
                        End If
                    End If
                    Dim ddlBizDocStatus As DropDownList
                    ddlBizDocStatus = e.Item.FindControl("ddlBizDocStatus")
                    'Changed by Sachin Sadhu||Date:16thJune2014
                    'Purpose :Filter Status Bizdoc Type wise
                    objCommon.sb_FillComboFromDBwithSel(ddlBizDocStatus, 11, Session("DomainID"), CCommon.ToString(lblBizDocID.Text))
                    'end of code
                    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "numBizDocStatus")) Then
                        If Not ddlBizDocStatus.Items.FindByValue(DataBinder.Eval(e.Item.DataItem, "numBizDocStatus")) Is Nothing Then
                            ddlBizDocStatus.ClearSelection()
                            ddlBizDocStatus.Items.FindByValue(DataBinder.Eval(e.Item.DataItem, "numBizDocStatus")).Selected = True
                        End If
                    End If

                    Dim strDate As Date

                    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "dtFromDate")) Then
                        strDate = DateAdd(DateInterval.Day, CCommon.ToInteger(DataBinder.Eval(e.Item.DataItem, "numBillingDaysName")), DataBinder.Eval(e.Item.DataItem, "dtFromDate"))

                        Dim lblDuedate As Label
                        lblDuedate = e.Item.FindControl("lblDuedate")
                        lblDuedate.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
                    End If

                    If DataBinder.Eval(e.Item.DataItem, "tintReturnType") <> 0 Then
                        btnDelete.Visible = False
                        lnkDelete.Visible = True
                        lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")

                        ddlBizDocStatus.Visible = False

                        editImage.Visible = False
                        ibtnPrint.Visible = False
                        ibtnPayments.Visible = False
                        CType(e.Item.FindControl("ibtnExportPDF"), ImageButton).Visible = False
                        CType(e.Item.FindControl("ibtnSendEmail"), ImageButton).Visible = False
                        CType(e.Item.FindControl("imgBox"), HtmlImage).Visible = False
                        CType(e.Item.FindControl("imgBarcode"), HtmlImage).Visible = False

                        Select Case CCommon.ToShort(DataBinder.Eval(e.Item.DataItem, "tintReturnType"))
                            Case 1
                                hpl.Attributes.Add("onclick", "return OpenRMAInvoice('" & DataBinder.Eval(e.Item.DataItem, "numReturnHeaderID") & "','" & enmReferenceTypeForMirrorBizDocs.SalesReturns & "',0);")

                            Case 2
                                hpl.Attributes.Add("onclick", "return OpenBizInvoice('" & DataBinder.Eval(e.Item.DataItem, "numReturnHeaderID") & "','" & enmReferenceTypeForMirrorBizDocs.PurchaseReturns & "',0);")

                            Case 3
                                hpl.Attributes.Add("onclick", "return OpenBizInvoice('" & DataBinder.Eval(e.Item.DataItem, "numReturnHeaderID") & "','" & enmReferenceTypeForMirrorBizDocs.CreditMemo & "',0);")

                            Case 4
                                hpl.Attributes.Add("onclick", "return OpenBizInvoice('" & DataBinder.Eval(e.Item.DataItem, "numReturnHeaderID") & "','" & enmReferenceTypeForMirrorBizDocs.RefundReceipt & "',0);")

                        End Select
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function ReturnName(ByVal SDate) As String
            Try
                Dim strDate As String = ""
                If Not IsDBNull(SDate) Then
                    strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                    If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                        strDate = "<font color=red>" & strDate & "</font>"
                    ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                        strDate = "<font color=orange>" & strDate & "</font>"
                    End If
                End If
                Return strDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'Hide Add bizdoc button if recurring bizdoc is on
        Private Sub CheckRecurring()
            Try
                Dim objOpportunity As New OppotunitiesIP
                objOpportunity.DomainID = Session("DomainId")
                objOpportunity.OpportunityId = CCommon.ToLong(GetQueryStringVal("OpID"))
                objOpportunity.GetRecurringDetails()
                btnSave.Visible = objOpportunity.bitVisible
                btnAdd.Visible = objOpportunity.bitVisible
                hdnRecurringOrder.Value = Not objOpportunity.bitVisible
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
            Try
                If CCommon.ToBool(Session("IsMinUnitPriceRule")) Then
                    If CheckIfApprovalRequired() Then
                        litMessage.Text = "You can not add BizDoc because approval of unit price(s) is required."
                        Exit Sub
                    End If
                End If

                Dim objOppBizDocs As New OppBizDocs
                Dim OppBizDocID, lngDivId, JournalId As Long
                Dim lngCntID As Long = 0
                objOppBizDocs.OppId = lngOppId
                objOppBizDocs.OppType = OppType
                objOppBizDocs.UserCntID = Session("UserContactID")
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.vcPONo = "" 'txtPO.Text
                objOppBizDocs.vcComments = "" 'txtComments.Text

                Dim dtDetails, dtBillingTerms As DataTable
                Dim objPageLayout As New CPageLayout
                objPageLayout.OpportunityId = lngOppId
                objPageLayout.DomainID = Session("DomainID")
                dtDetails = objPageLayout.OpportunityDetails.Tables(0)
                lngDivId = dtDetails.Rows(0).Item("numDivisionID")
                lngCntID = dtDetails.Rows(0).Item("numContactID")

                If OppType = 1 And ddlBizDocs.SelectedValue = "296" Then
                    If CCommon.ToShort(hdnCommitAllocation.Value) = 2 AndAlso CCommon.ToBool(hdnAllocateOnPickList.Value) Then
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "openPickListSelection", "OpenPickListSelectionWindow(" & lngOppId & ");", True)
                        Exit Sub
                    End If
                End If

                Dim objAdmin As New CAdmin
                objAdmin.DivisionID = lngDivId

                Dim lintAuthorizativeBizDocsId As Long
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.OppId = lngOppId
                objOppBizDocs.ShipCompany = CCommon.ToLong(dtDetails.Rows(0).Item("intUsedShippingCompany"))
                lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()

                Dim objJournal As New JournalEntry
                If lintAuthorizativeBizDocsId = ddlBizDocs.SelectedItem.Value Then 'save bizdoc only if selected company's AR and AP account is mapped
                    If OppType = 1 AndAlso chkCreateInvoiceForDeferred.Visible AndAlso chkCreateInvoiceForDeferred.Checked Then
                        objOppBizDocs.bitInvoiceForDeferred = True
                    Else
                        objOppBizDocs.bitInvoiceForDeferred = False
                    End If

                    If objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), lngDivId) = 0 Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip"" To Save BizDoc' );", True)
                        Exit Sub
                    End If
                End If
                If OppType = 2 Then
                    'Accounting validation->Do not allow to save PO untill Default COGs account is mapped
                    If ChartOfAccounting.GetDefaultAccount("CG", Session("DomainID")) = 0 Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default COGs account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                        Exit Sub
                    End If
                End If

                If OppType = 2 Then
                    If ChartOfAccounting.GetDefaultAccount("PC", Session("DomainID")) = 0 Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Purchase Clearing account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                        Exit Sub
                    End If

                    If ChartOfAccounting.GetDefaultAccount("PV", Session("DomainID")) = 0 Then
                        ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Purchase Price Variance account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                        Exit Sub
                    End If
                End If
                'dtBillingTerms = objAdmin.GetBillingTerms()
                'If Not IsDBNull(dtBillingTerms.Rows(0).Item("numBillingDays")) Then
                '    If CCommon.ToLong(dtBillingTerms.Rows(0).Item("numBillingDays")) > 0 Then
                '        objOppBizDocs.boolBillingTerms = True
                '        objOppBizDocs.BillingDays = dtBillingTerms.Rows(0).Item("numBillingDays")
                '    End If
                'End If

                objOppBizDocs.FromDate = DateTime.UtcNow
                objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                objOppBizDocs.BizDocId = ddlBizDocs.SelectedValue
                objOppBizDocs.BizDocTemplateID = ddlBizDocTemplate.SelectedValue

                If divAR.Visible Then
                    objOppBizDocs.ARAccountID = CCommon.ToLong(ddlAR.SelectedValue)
                End If

                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")
                objCommon.Mode = 33
                objCommon.Str = ddlBizDocs.SelectedValue
                objOppBizDocs.SequenceId = objCommon.GetSingleFieldValue()

                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")
                objCommon.Mode = 34
                objCommon.Str = lngOppId
                objOppBizDocs.RefOrderNo = objCommon.GetSingleFieldValue()

                objOppBizDocs.bitPartialShipment = True

                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    OppBizDocID = objOppBizDocs.SaveBizDoc()

                    If OppBizDocID = 0 Then
                        litMessage.Text = "A BizDoc by the same name is already created. Please select another BizDoc !"
                        Exit Sub
                    End If

                    'If OppBizDocID > 0 Then
                    '    GetWorkFlowAutomation(OppBizDocID, lngCntID, lngDivId)

                    'End If

                    'Create Journals only for authoritative bizdocs
                    '-------------------------------------------------
                    If lintAuthorizativeBizDocsId = ddlBizDocs.SelectedValue Or ddlBizDocs.SelectedValue = 304 Then ' 304 - Deferred Income
                        Dim ds As New DataSet
                        Dim dtOppBiDocItems As DataTable

                        objOppBizDocs.OppBizDocId = OppBizDocID
                        ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                        dtOppBiDocItems = ds.Tables(0)

                        Dim objCalculateDealAmount As New CalculateDealAmount

                        objCalculateDealAmount.CalculateDealAmount(lngOppId, OppBizDocID, OppType, Session("DomainID"), dtOppBiDocItems)

                        ''---------------------------------------------------------------------------------
                        JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, objOppBizDocs.FromDate, Description:=ds.Tables(1).Rows(0).Item("vcBizDocID"))
                        Dim objJournalEntries As New JournalEntry

                        If OppType = 2 Then
                            If CCommon.ToBool(ds.Tables(1).Rows(0).Item("bitPPVariance")) Then
                                objJournalEntries.SaveJournalEntriesPurchaseVariance(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), ds.Tables(1).Rows(0).Item("fltExchangeRateBizDoc"), vcBaseCurrency:=ds.Tables(1).Rows(0).Item("vcBaseCurrency"), vcForeignCurrency:=ds.Tables(1).Rows(0).Item("vcForeignCurrency"))
                            Else
                                objJournalEntries.SaveJournalEntriesPurchase(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"))
                            End If
                        ElseIf OppType = 1 Then
                            If ddlBizDocs.SelectedValue = 304 AndAlso CCommon.ToBool(Session("IsEnableDeferredIncome")) Then 'Deferred Revenue
                                objJournalEntries.SaveJournalEntriesDeferredIncome(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), objCalculateDealAmount.GrandTotal)
                            Else
                                If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                                    objJournalEntries.SaveJournalEntriesSales(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), 0, bitInvoiceAgainstDeferredIncomeBizDoc:=objOppBizDocs.bitInvoiceForDeferred)
                                Else
                                    objJournalEntries.SaveJournalEntriesSalesNew(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), 0, bitInvoiceAgainstDeferredIncomeBizDoc:=objOppBizDocs.bitInvoiceForDeferred)
                                End If

                                If Session("AutolinkUnappliedPayment") Then
                                    Dim objSalesFulfillment As New SalesFulfillmentWorkflow
                                    objSalesFulfillment.DomainID = Session("DomainID")
                                    objSalesFulfillment.UserCntID = Session("UserContactID")
                                    objSalesFulfillment.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                                    objSalesFulfillment.OppId = lngOppId
                                    objSalesFulfillment.OppBizDocId = OppBizDocID

                                    objSalesFulfillment.AutoLinkUnappliedPayment()
                                End If
                            End If
                        End If
                    ElseIf ddlBizDocs.SelectedValue = 296 Then 'Fulfillment BizDoc
                        'AUTO FULFILLS bizdoc AND MAKED QTY AS SHIPPED AND RELEASES IT FROM ALLOCATION
                        Dim objOppBizDoc As New OppBizDocs
                        objOppBizDoc.DomainID = CCommon.ToLong(Session("DomainID"))
                        objOppBizDoc.UserCntID = CCommon.ToLong(Session("UserContactID"))
                        objOppBizDoc.OppId = lngOppId
                        objOppBizDoc.OppBizDocId = OppBizDocID
                        objOppBizDoc.AutoFulfillBizDoc()


                        'IMPORTANT: WE ARE MAKING ACCOUNTING CHANGES BECAUSE WE ARE CREATING FULFILLMENT ORDER IN BACKGROUP AND MARKIGN IT AS SHIPPED
                        '           OTHERWISE ACCOUNTING IS ONLY IMPACTED WHEN USER SHIPPED IT FROM SALES FULFILLMENT PAGE
                        'CREATE SALES CLEARING ENTERIES
                        'CREATE JOURNALS ONLY FOR FILFILLMENT BIZDOCS
                        '-------------------------------------------------
                        Dim ds As New DataSet
                        Dim dtOppBiDocItems As DataTable

                        objOppBizDocs.OppBizDocId = OppBizDocID
                        ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                        dtOppBiDocItems = ds.Tables(0)

                        Dim objCalculateDealAmount As New CalculateDealAmount
                        objCalculateDealAmount.CalculateDealAmount(lngOppId, OppBizDocID, 1, CCommon.ToLong(Session("DomainID")), dtOppBiDocItems)

                        'WHEN FROM COA SOMEONE DELETED JOURNALS THEN WE NEED TO CREATE NEW JOURNAL HEADER ENTRY
                        JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, Entry_Date:=Date.UtcNow.Date, Description:=CStr(ds.Tables(1).Rows(0).Item("vcBizDocID")))

                        Dim objJournalEntries As New BACRM.BusinessLogic.Accounting.JournalEntry
                        If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                            objJournalEntries.SaveJournalEnteriesSalesClearing(lngOppId, CCommon.ToLong(Session("DomainID")), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                        Else
                            objJournalEntries.SaveJournalEnteriesSalesClearingNew(lngOppId, CCommon.ToLong(Session("DomainID")), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                        End If
                        '---------------------------------------------------------------------------------
                    End If

                    objTransactionScope.Complete()
                End Using

                ''Added By Sachin Sadhu||Date:1stMay2014
                ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                ''          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = OppBizDocID
                objWfA.SaveWFBizDocQueue()
                'end of code

                '------ Send BizDoc Alerts - Notify record owners, their supervisors, and your trading partners when a BizDoc is created, modified, or approved.
                Dim objAlert As New CAlerts
                objAlert.SendBizDocAlerts(lngOppId, OppBizDocID, ddlBizDocs.SelectedValue, Session("DomainID"), CAlerts.enmBizDoc.IsCreated)

                Dim objAutomatonRule As New AutomatonRule
                objAutomatonRule.ExecuteAutomationRule(49, OppBizDocID, 1)

                If OppBizDocID > 0 AndAlso ddlBizDocs.SelectedValue = "296" Then
                    Try
                        objOppBizDocs.CreateInvoiceFromFulfillmentOrder(CCommon.ToLong(Session("DomainID")), CCommon.ToLong(Session("UserContactID")), lngOppId, OppBizDocID, CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")))
                    Catch ex As Exception
                        'DO NOT THROW ERROR
                    End Try
                End If

                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "ReloadPage", "parent.location.href=parent.location.href", True)

                'getDetails()
            Catch ex As Exception
                If ex.Message = "NOT_ALLOWED" Then
                    litMessage.Text = "To split order items multiple bizdocs you must create all bizdocs with ""partial fulfilment"" checked!"
                ElseIf ex.Message = "NOT_ALLOWED_FulfillmentOrder" Then
                    litMessage.Text = "Only a Sales Order can create a Fulfillment Order"
                ElseIf ex.Message = "FY_CLOSED" Then
                    litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
                ElseIf ex.Message = "AlreadyInvoice" Then
                    litMessage.Text = "You can�t create a Invoice against a Fulfillment Order that already contains a Invoice"
                ElseIf ex.Message = "AlreadyPackingSlip" Then
                    litMessage.Text = "You can�t create a Packing-Slip against a Fulfillment Order that already contains a Packing-Slip"
                ElseIf ex.Message = "AlreadyInvoice_NOT_ALLOWED_FulfillmentOrder" Then
                    litMessage.Text = "You can�t create a fulfillment order against a sales order that already contains an Invoice or Packing-Slip"
                ElseIf ex.Message = "AlreadyFulfillmentOrder" Then
                    litMessage.Text = "Manually adding Invoices or Packing Slips to a Sales Order is not allowed after a Fulfillment Order (FO) is added. Your options are to edit the FO status to trigger creation of Invoices and/or Packing Slips, Delete the Fulfillment Order, or Create Invoices and/or Packing Slips from the Sales Fulfillment section"
                ElseIf ex.Message.Contains("WORK_ORDER_NOT_COMPLETED") Then
                    litMessage.Text = "You can't ship at this time because Work Order for this Sales Order is pending."
                ElseIf ex.Message.Contains("NOTSUFFICIENTQTY_ALLOCATION") Then
                    litMessage.Text = "You can�t create a fulfillment order as items(s) are on back order."
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Response.Write(ex)
                End If
            End Try
        End Sub

        Private Function CheckIfApprovalRequired() As Boolean
            Try
                Dim objOpportunity As New MOpportunity
                objOpportunity.OpportunityId = lngOppId
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.UserCntID = Session("UserContactID")
                objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                Dim dsTemp As DataSet = objOpportunity.ItemsByOppId

                If Not dsTemp Is Nothing AndAlso dsTemp.Tables.Count > 0 AndAlso dsTemp.Tables(0).Rows.Count > 0 Then
                    Dim results = From myRow In dsTemp.Tables(0).AsEnumerable()
                                  Where CCommon.ToBool(myRow.Field(Of Boolean)("bitItemPriceApprovalRequired")) = True
                                  Select myRow
                    If results.Count > 0 Then
                        Return True
                    Else
                        Return False
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
            Try
                If CCommon.ToBool(Session("IsMinUnitPriceRule")) Then
                    If CheckIfApprovalRequired() Then
                        litMessage.Text = "You can not add BizDoc because approval of unit price(s) is required."
                        Exit Sub
                    End If
                End If
         
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "AddBizDocs", "AddBizDocs('" & GetQueryStringVal("OpID") & "','" & OppType & "','" & ddlBizDocs.SelectedValue & "','" & ddlBizDocTemplate.SelectedValue & "');", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

#Region "Worflow Automation Functions"

        Private Function GetWorkFlowAutomation(ByVal OppBizDocID As Long, ByVal lngCntID As Long, ByVal lngDivID As Long) As DataTable
            Dim dtRules As New DataTable
            Dim BizDocId As Long = 0
            Dim BizDocTemplateId As Long = 0
            Dim BizDocStatus As Long = 0
            Dim dtBizDocDtl As New DataTable
            ' Dim lngCntID As Long = 0
            Dim OpenReceivePaymentWindow As Boolean = False

            Try
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.UserCntID = Session("UserContactID")
                objAdmin.RuleID = 14
                objAdmin.byteMode = 1
                objAdmin.AutomationID = 0
                dtRules = objAdmin.GetWorkflowAutomationRules()

                Dim objOppBizDocs As New OppBizDocs
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.UserCntID = Session("UserContactID")
                objOppBizDocs.tintMode = 2
                objOppBizDocs.OppBizDocId = OppBizDocID

                dtBizDocDtl = objOppBizDocs.GetOppBizDocDtl()
                BizDocId = dtBizDocDtl.Rows(0)("numBizDocId")
                BizDocTemplateId = dtBizDocDtl.Rows(0)("numBizDocTempID")
                Dim RefType As Integer
                RefType = enmReferenceTypeForMirrorBizDocs.SalesOrder
                Dim objContact As New CContacts

                Dim ToEmailId As String = objContact.GetEmailId(lngCntID)

                For Each dr As DataRow In dtRules.Rows
                    If BizDocId = CCommon.ToLong(dr("numBizDocTypeId")) Then
                        Session("Attachements") = Nothing
                        Dim EmailTemp As Long = CCommon.ToLong(dr("numEmailTemplate"))
                        OpenReceivePaymentWindow = CCommon.ToBool(dr("numOpenRecievePayment"))
                        BizDocStatus = CCommon.ToLong(dr("numBizDocStatus1"))

                        Dim objBizDocs As New OppBizDocs
                        Dim dt As DataTable
                        objBizDocs.BizDocId = OppBizDocID 'lngOppBizDocID
                        objBizDocs.DomainID = Session("DomainID")
                        objBizDocs.UserCntID = Session("UserContactID")
                        objBizDocs.BizDocTemplateID = BizDocTemplateId
                        objBizDocs.OppId = lngOppId
                        objBizDocs.OppBizDocId = OppBizDocID
                        'objBizDocs.GetOppBizDocDtl()
                        dt = objBizDocs.GetBizDocAttachments

                        If BizDocStatus > 0 Then
                            objBizDocs.BizDocStatus = BizDocStatus
                            objBizDocs.OpportunityBizDocStatusChange()

                            ''Added By Sachin Sadhu||Date:5thMay2014
                            ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                            ''          Using Change tracking
                            Dim objWfA As New Workflow()
                            objWfA.DomainID = Session("DomainID")
                            objWfA.UserCntID = Session("UserContactID")
                            objWfA.RecordID = OppBizDocID
                            objWfA.SaveWFBizDocQueue()
                            'end of code
                        End If

                        Dim i As Integer
                        Dim strFileName As String = ""
                        Dim strFilePhysicalLocation As String = ""
                        Session("Attachements") = Nothing 'reset to nothing to avoid duplicate attachments
                        For i = 0 To dt.Rows.Count - 1
                            If dt.Rows(i).Item("vcDocName") = "BizDoc" Then
                                strFileName = "File" & Format(Now, "ddmmyyyyhhmmssfff") & ".pdf"
                                strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName

                                Dim objHTMLToPDF As New HTMLToPDF
                                System.IO.File.WriteAllBytes(strFilePhysicalLocation, objHTMLToPDF.CreateBizDocPDF(lngOppId, OppBizDocID, CCommon.ToLong(Session("DomainID")), CCommon.ToLong(Session("UserContactID")), CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")), CCommon.ToString(Session("DateFormat"))))
                            Else
                                strFileName = dt.Rows(i).Item("vcURL")
                            End If
                            strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
                            objCommon.AddAttchmentToSession(strFileName, CCommon.GetDocumentPath(Session("DomainID")) & strFileName, strFilePhysicalLocation)
                        Next
                        Dim str As String
                        str = "<script language='javascript'>window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&PickAtch=1&isAttachmentRequired=1&Lsemail=" &
                            HttpUtility.JavaScriptStringEncode(ToEmailId) & "&pqwRT=" & lngCntID.ToString & "&BizDocID=" &
                            CCommon.ToShort(1).ToString() &
                            "&OppID=" & CCommon.ToString(OppBizDocID) &
                            "&OppType=" & CCommon.ToString(1) &
                            "&EmailTemp=" & CCommon.ToString(EmailTemp) &
                            "','ComposeWindow','toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')</script>"


                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Email", str, False)
                        If OpenReceivePaymentWindow = True Then
                            str = "<script language='javascript'>window.open('../opportunity/frmAmtPaid.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Popup=1&PickAtch=1&Lsemail=" &
                         HttpUtility.JavaScriptStringEncode(ToEmailId) &
                         "&DivId=" & lngDivID.ToString &
                           "&CntID=" & lngCntID.ToString &
                         "&OppBizId=" & CCommon.ToString(OppBizDocID) &
                         "&OppId=" & CCommon.ToString(lngOppId) &
                         "','','toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')</script>"

                            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "ReceivePayment", str, False)

                        End If

                    End If
                Next

            Catch ex As Exception
                Throw ex

            End Try

            Return dtRules
        End Function

#End Region

    End Class
End Namespace
