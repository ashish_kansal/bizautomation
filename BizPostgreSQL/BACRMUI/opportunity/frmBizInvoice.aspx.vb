'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Alerts
Imports System.IO
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Workflow

Namespace BACRM.UserInterface.Opportunities
    Public Class frmBizInvoice
        Inherits BACRMPage
        Dim dtOppBiDocItems As New DataTable
        Dim lintJournalId As Integer
        Dim lngOppBizDocID, lngOppId, lngDivID, lngReturnID As Long
        Dim objCommon As New CCommon
        Dim m_aryRightsForAuthoritativ(), m_aryRightsForNonAuthoritativ() As Integer
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub




        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lngOppBizDocID = CCommon.ToLong(GetQueryStringVal("OppBizId"))
                lngOppId = CCommon.ToLong(GetQueryStringVal("OpID"))
                lngReturnID = CCommon.ToLong(GetQueryStringVal("ReturnID"))

                SetWebControlAttributes()

                If Not IsPostBack Then
                    AddToRecentlyViewed(RecetlyViewdRecordType.BizDoc, lngOppBizDocID)

                    getAddDetails()
                    getBizDocDetails()

                    PaymentHistory1.OppID = CCommon.ToLong(lngOppId)
                    PaymentHistory1.OppBizDocID = CCommon.ToLong(lngOppBizDocID)
                    PaymentHistory1.BindGrid()

                    If CCommon.ToInteger(GetQueryStringVal("Print")) = 1 Then
                        tblButtons.Visible = False
                        tblApproval.Visible = False

                        Page.ClientScript.RegisterStartupScript(Me.GetType, "PrintInvoice", " window.print();", True)
                    End If

                    If CCommon.ToInteger(GetQueryStringVal("DocGenerate")) = 1 Then
                        tblButtons.Visible = False
                        tblApproval.Visible = False
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub getBizDocDetails()
            Try
                Dim dtOppBiDocDtl As DataTable
                Dim objOppBizDocs As New OppBizDocs
                objOppBizDocs.OppBizDocId = lngOppBizDocID
                objOppBizDocs.OppId = lngOppId
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.UserCntID = Session("UserContactID")
                objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl
                If dtOppBiDocDtl.Rows.Count = 0 Then Exit Sub
                Dim strBizDocId As String = dtOppBiDocDtl.Rows(0).Item("numBizDocId")

                If dtOppBiDocDtl.Rows(0).Item("bitAuthoritativeBizDocs") = 1 Then
                    m_aryRightsForAuthoritativ = GetUserRightsForPage_Other(10, 28)

                    If m_aryRightsForAuthoritativ(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    End If

                    If m_aryRightsForAuthoritativ(RIGHTSTYPE.UPDATE) = 0 Then
                        btnSave.Visible = False
                        btnSaveAndClose.Visible = False
                        'Exit Sub
                    End If
                Else
                    m_aryRightsForNonAuthoritativ = GetUserRightsForPage_Other(10, 29)

                    If m_aryRightsForNonAuthoritativ(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    End If

                    If m_aryRightsForNonAuthoritativ(RIGHTSTYPE.UPDATE) = 0 Then
                        btnSave.Visible = False
                        btnSaveAndClose.Visible = False
                        'Exit Sub
                    End If
                End If

                'lblRefOrderNo.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcRefOrderNo")), "", dtOppBiDocDtl.Rows(0).Item("vcRefOrderNo"))
                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("monAmountPaid")) Then
                    lblAmountPaid.Text = String.Format("{0:#,##0.00}", CCommon.ToDouble(dtOppBiDocDtl.Rows(0).Item("monAmountPaid")))
                Else : lblAmountPaid.Text = "0.00"
                End If
                If dtOppBiDocDtl.Rows(0).Item("AppReq") = 1 Then
                    pnlApprove.Visible = True
                Else : pnlApprove.Visible = False
                End If
                lblPending.Text = dtOppBiDocDtl.Rows(0).Item("Pending")
                lblApproved.Text = dtOppBiDocDtl.Rows(0).Item("Approved")
                lblDeclined.Text = dtOppBiDocDtl.Rows(0).Item("Declined")
                hdnOrientation.Value = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("numOrientation"))
                hdnKeepFooterBottom.Value = CCommon.ToBool(dtOppBiDocDtl.Rows(0).Item("bitKeepFooterBottom"))
                hplBillto.Attributes.Add("onclick", "return openeditAddress('Bill','" & lngOppId & "'," & lngReturnID & ")")
                hplShipTo.Attributes.Add("onclick", "return openeditAddress('Ship','" & lngOppId & "'," & lngReturnID & ")")

                ibtnCloneBizdoc.Attributes.Add("onclick", "return CloneBizdoc('" & lngOppId & "','" & lngOppBizDocID & "','" & lngDivID & "','" & CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numBizDocId")) & "','" & CCommon.ToShort(hdnOppType.Value) & "')")

                If dtOppBiDocDtl.Rows(0).Item("tintDeferred") = 1 Then
                    hplAmountPaid.Text = "Amount Paid: (Deferred)"
                End If

                If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                    If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("BizdocFooter")) Then
                        imgFooter.Visible = True
                        imgFooter.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtOppBiDocDtl.Rows(0).Item("BizdocFooter")
                    Else : imgFooter.Visible = False
                    End If
                    lblPONo.Text = "P.O"

                Else
                    If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("PurBizdocFooter")) Then
                        imgFooter.Visible = True
                        imgFooter.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtOppBiDocDtl.Rows(0).Item("PurBizdocFooter")
                    Else : imgFooter.Visible = False
                    End If
                    lblPONo.Text = "Invoice"

                    hplAmountPaid.Attributes.Add("onclick", "#")
                End If

                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcBizDocImagePath")) Then
                    imgLogo.Visible = True
                    imgLogo.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtOppBiDocDtl.Rows(0).Item("vcBizDocImagePath")
                Else : imgLogo.Visible = False
                End If

                'If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcSignatureFile")) Then
                '    imgSignature.Visible = True
                '    imgSignature.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtOppBiDocDtl.Rows(0).Item("vcSignatureFile")
                'Else : imgSignature.Visible = False
                'End If

                txtComments.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcComments")), "", dtOppBiDocDtl.Rows(0).Item("vcComments"))
                Dim strTrackingNo As String = ""

                Dim trackingNumbers As String() = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcTrackingNo")).Split(New Char() {","c})

                For Each trackingNumber As String In trackingNumbers
                    strTrackingNo = strTrackingNo & If(String.IsNullOrEmpty(strTrackingNo), "", ", ") & String.Format("<a href='{0}{1}' target='_blank'>{1}</a>", IIf(CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcTrackingURL")) = "", "javascript:void(0)", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcTrackingURL"))), trackingNumber)
                Next

                'If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcBizDocName")) AndAlso dtOppBiDocDtl.Rows(0).Item("vcBizDocName").ToString.Trim.Length > 0 Then
                '    lblBizDocIDValue.Text = dtOppBiDocDtl.Rows(0).Item("vcBizDocName")
                'Else
                lblBizDocIDValue.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("vcBizDocID")), "", dtOppBiDocDtl.Rows(0).Item("vcBizDocID"))
                'End If
                lblcreated.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("numCreatedby")), "", dtOppBiDocDtl.Rows(0).Item("numCreatedby")) & "  &nbsp;" & IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtCreatedDate")), "", dtOppBiDocDtl.Rows(0).Item("dtCreatedDate")) & "  &nbsp;" & IIf(CCommon.ToBool(dtOppBiDocDtl.Rows(0).Item("bitAutoCreated")), "WorkFlow", "")
                lblModifiedby.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("numModifiedBy")), "", dtOppBiDocDtl.Rows(0).Item("numModifiedBy")) & "  &nbsp;" & IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtModifiedDate")), "", dtOppBiDocDtl.Rows(0).Item("dtModifiedDate"))
                'lblviewwedby.Text = IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("numViewedBy")), "", dtOppBiDocDtl.Rows(0).Item("numViewedBy")) & "  &nbsp;" & IIf(IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtViewedDate")), "", dtOppBiDocDtl.Rows(0).Item("dtViewedDate"))
                lblDate.Text = FormattedDateFromDate(dtOppBiDocDtl.Rows(0).Item("dtCreatedDate"), Session("DateFormat"))
                lblOrganizationName.Text = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrganizationName"))
                lblOrganizationContactName.Text = CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactName"))
                hdnDivID.Value = CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numDivisionID"))
                SetWebControlAttributes()
                Dim strDate As Date

                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtFromDate")) Then
                    strDate = DateAdd(DateInterval.Day, CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("numBillingDaysName")), dtOppBiDocDtl.Rows(0).Item("dtFromDate"))
                    lblDuedate.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
                End If

                If dtOppBiDocDtl.Rows(0).Item("tintBillingTerms") = True Then
                    lblBillingTerms.Text = "Net " & dtOppBiDocDtl.Rows(0).Item("numBillingDaysName")
                    lblBillingTermsName.Text = dtOppBiDocDtl.Rows(0).Item("vcBillingTermsName")
                    'lblBillingTerms.Text = "Net " & dtOppBiDocDtl.Rows(0).Item("numBillingDaysName") & " , " & IIf(dtOppBiDocDtl.Rows(0).Item("tintInterestType") = False, "-", "+") & dtOppBiDocDtl.Rows(0).Item("fltInterest") & " %"
                    'lblBillingTermsName.Text = dtOppBiDocDtl.Rows(0).Item("vcBillingTermsName") & " , " & IIf(dtOppBiDocDtl.Rows(0).Item("tintInterestType") = False, "-", "+") & dtOppBiDocDtl.Rows(0).Item("fltInterest") & " %"
                Else
                    lblBillingTerms.Text = "-"
                    lblBillingTermsName.Text = "-"

                End If

                If Not strDate = Nothing Then
                    hplDueDate.Attributes.Add("onclick", "return ChangeDate('" & lngOppBizDocID & "','" & strDate.ToString("yyyyMMdd") & "','" & lngOppId & "')")
                End If


                If dtOppBiDocDtl.Rows(0).Item("fltDiscount") > 0 Then
                    If dtOppBiDocDtl.Rows(0).Item("bitDiscountType") = False Then

                        lblDiscount.Text = dtOppBiDocDtl.Rows(0).Item("fltDiscount") & " %"
                    Else
                        lblDiscount.Text = dtOppBiDocDtl.Rows(0).Item("fltDiscount")
                    End If
                End If

                'Changed by Sachin Sadhu||Date:16thJune2014
                'Purpose :Filter Status Bizdoc Type wise
                objCommon.sb_FillComboFromDBwithSel(ddlBizDocStatus, 11, Session("DomainID"), CCommon.ToString(strBizDocId))
                'end of code

                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("numBizDocStatus")) Then
                    If Not ddlBizDocStatus.Items.FindByValue(dtOppBiDocDtl.Rows(0).Item("numBizDocStatus")) Is Nothing Then
                        ddlBizDocStatus.Items.FindByValue(dtOppBiDocDtl.Rows(0).Item("numBizDocStatus")).Selected = True
                    End If
                End If
                lblStatus.Text = dtOppBiDocDtl.Rows(0).Item("BizDocStatus")
                'lblComments.Text = "<pre class='normal1'>" & dtOppBiDocDtl.Rows(0).Item("vcComments") & "</pre>"

                lblComments.Text = "<pre class=""WordWrap"">" & dtOppBiDocDtl.Rows(0).Item("vcComments") & "</pre>"
                lblNo.Text = dtOppBiDocDtl.Rows(0).Item("vcRefOrderNo")
                'txtTrackingURL.Text = dtOppBiDocDtl.Rows(0).Item("vcTrackingURL")
                hdShipAmt.Value = CCommon.GetDecimalFormat(dtOppBiDocDtl.Rows(0).Item("monShipCost"))
                'Show capture amount button
                'If dtOppBiDocDtl.Rows(0).Item("bitAuthoritativeBizDocs") = 1 Then btnCaptureAmount.Visible = IsCardAuthorized()
                Dim ds As New DataSet
                Dim objVendorPayment As New VendorPayment
                Dim dtARAging As New DataTable
                Dim dtARAgingInvoice As New DataTable
                Dim decBalanceDue As Decimal = 0

                Dim dtOppBiDocItems As DataTable
                'If template is of Customer Statement, load details as per Customer's Order list statement & exit 
                If CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numBizDocId")) = enmBizDocTemplate_BizDocID.CustomerStatement Then

                    Dim mobjGeneralLedger As New GeneralLedger
                    mobjGeneralLedger.DomainID = CCommon.ToLong(Session("DomainID"))
                    mobjGeneralLedger.Year = CInt(Now.Year)

                    objVendorPayment.DomainID = Session("DomainID")
                    objVendorPayment.CompanyID = CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numDivisionID"))
                    objVendorPayment.UserCntID = Session("UserContactID")
                    objVendorPayment.Flag = 0 'ddlRange.SelectedValue
                    objVendorPayment.dtFromDate = "1990-01-01" '"mobjGeneralLedger.GetFiscalDate()"
                    objVendorPayment.dtTodate = CDate(DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)) 'DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) ''calTo.SelectedDate
                    objVendorPayment.IsCustomerStatement = True
                    dtARAging = objVendorPayment.GetAccountReceivableAging
                    dtARAgingInvoice = objVendorPayment.GetAccountReceivableAging_Invoice

                    Dim i As Integer
                    Dim dtdgColumns As DataTable
                    Dim objFormWizard As New FormConfigWizard
                    objFormWizard.DomainID = Session("DomainID")
                    objFormWizard.FormID = 7
                    objFormWizard.AuthenticationGroupID = enmBizDocTemplate_BizDocID.CustomerStatement
                    objFormWizard.BizDocTemplateID = CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numBizDocTempID"))




                    dtdgColumns = objFormWizard.getFieldNameListFromDB()

                    If dtdgColumns Is Nothing Then
                        litMessage.Text = "Error occurred while getting Customer Statement Fields. Please contact System Administrator."
                        Exit Sub
                    End If

                    Dim bColumn As BoundColumn
                    For i = 0 To dtdgColumns.Rows.Count - 1
                        bColumn = New BoundColumn
                        bColumn.HeaderText = dtdgColumns.Rows(i).Item("vcFormFieldName")
                        bColumn.DataField = dtdgColumns.Rows(i).Item("vcDbColumnName")

                        If dtdgColumns.Rows(i).Item("vcDbColumnName") = "vcTerms" And dtdgColumns.Rows.Count = 2 Then
                            bColumn.ItemStyle.CssClass = "TwoColumns"
                        ElseIf dtdgColumns.Rows(i).Item("vcDbColumnName") = "vcTerms" And dtdgColumns.Rows.Count = 1 Then
                            bColumn.ItemStyle.CssClass = "OneColumns"
                        ElseIf dtdgColumns.Rows(i).Item("vcDbColumnName") = "vcTerms" And dtdgColumns.Rows.Count = 3 Then
                            bColumn.ItemStyle.CssClass = "ThreeColumns"
                        ElseIf dtdgColumns.Rows(i).Item("vcDbColumnName") = "vcItemName" Then
                            bColumn.ItemStyle.CssClass = "ItemName"
                        ElseIf (CCommon.ToString(dtdgColumns.Rows(i).Item("vcDbColumnName")) = "vcBizDocID" OrElse
                                CCommon.ToString(dtdgColumns.Rows(i).Item("vcDbColumnName")) = "vcPOppName" OrElse
                                CCommon.ToString(dtdgColumns.Rows(i).Item("vcDbColumnName")) = "dtFromDate" OrElse
                                CCommon.ToString(dtdgColumns.Rows(i).Item("vcDbColumnName")) = "DueDate") Then
                            bColumn.HeaderStyle.Width = Unit.Percentage(10)
                        ElseIf CCommon.ToString(dtdgColumns.Rows(i).Item("vcDbColumnName")) = "vcPromotionDetail" Then
                            bColumn.HeaderStyle.Width = Unit.Percentage(25)
                            bColumn.ItemStyle.CssClass = "PromotionDetail"
                        End If

                        If dtdgColumns.Rows(i).Item("vcFieldDataType") = "M" Then bColumn.DataFormatString = CCommon.GetDataFormatString()
                        If dtdgColumns.Rows(i).Item("vcFieldDataType") = "N" Then bColumn.DataFormatString = CCommon.GetDataFormatString()
                        dgBizDocs.Columns.Add(bColumn)
                    Next

                    If dtARAgingInvoice.Rows.Count > 0 Then
                        decBalanceDue = dtARAgingInvoice.Compute("Sum(BalanceDue)", String.Empty)
                    End If

                    dgBizDocs.DataSource = dtARAgingInvoice

                    dgBizDocs.DataBind()
                Else

                    objOppBizDocs.DomainID = Session("DomainID")
                    objOppBizDocs.OppBizDocId = lngOppBizDocID
                    objOppBizDocs.OppId = lngOppId
                    ds = objOppBizDocs.GetBizDocItemsWithKitChilds
                    Dim dtOppBizDocsItemsWithKitCHilds As DataTable = ds.Tables(0)

                    If Not dtOppBizDocsItemsWithKitCHilds Is Nothing AndAlso dtOppBizDocsItemsWithKitCHilds.Rows.Count > 0 Then
                        dtOppBiDocItems = dtOppBizDocsItemsWithKitCHilds.Select("bitChildItem=0").CopyToDataTable()
                    Else
                        dtOppBiDocItems = dtOppBizDocsItemsWithKitCHilds
                    End If

                    If IIf(dtOppBiDocItems.Rows.Count > 0, dtOppBiDocItems.Compute("SUM(DiscAmt)", ""), 0) > 0 Then
                        lblDiscount.Text = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() & " " & String.Format("{0:#,##0.00}", CCommon.ToDouble(dtOppBiDocItems.Compute("SUM(DiscAmt)", "")))
                    Else
                        lblDiscount.Text = ""
                    End If

                    Dim objCalculateDealAmount As New CalculateDealAmount
                    objCalculateDealAmount.CalculateDealAmount(lngOppId, lngOppBizDocID, CShort(dtOppBiDocDtl.Rows(0).Item("tintOppType")), Session("DomainID"), dtOppBiDocItems, FromBizInvoice:=True)

                    hdLateCharge.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalLateCharges)

                    Dim decDiscountServiceItem As Decimal = 0
                    If CCommon.ToDouble(Session("DiscountServiceItem")) > 0 AndAlso dtOppBiDocItems.Select("numItemCode=" & CCommon.ToDouble(Session("DiscountServiceItem"))).Length > 0 Then
                        decDiscountServiceItem = -1 * dtOppBiDocItems.Select("numItemCode=" & CCommon.ToDouble(Session("DiscountServiceItem"))).CopyToDataTable().Compute("SUM(monTotAmount)", "")
                    End If

                    Dim decShippingServiceItem As Decimal = 0
                    If CCommon.ToDouble(Session("ShippingServiceItem")) > 0 AndAlso dtOppBiDocItems.Select("numItemCode=" & CCommon.ToDouble(Session("ShippingServiceItem"))).Length > 0 Then
                        decShippingServiceItem = dtOppBiDocItems.Select("numItemCode=" & CCommon.ToDouble(Session("ShippingServiceItem"))).CopyToDataTable().Compute("SUM(monTotAmount)", "")
                    End If

                    hdShipAmt.Value = CCommon.GetDecimalFormat(dtOppBiDocDtl.Rows(0).Item("monShipCost")) + decShippingServiceItem

                    If CCommon.ToBool(Session("DiscountOnUnitPrice")) Then
                        hdDisc.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalDiscount)
                        hdSubTotal.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalAmount)
                    Else
                        hdDisc.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalDiscount) + decDiscountServiceItem + IIf(dtOppBiDocItems.Rows.Count > 0, dtOppBiDocItems.Compute("SUM(DiscAmt)", ""), 0)
                        hdSubTotal.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalAmount + (objCalculateDealAmount.TotalDiscount + decDiscountServiceItem + IIf(dtOppBiDocItems.Rows.Count > 0, dtOppBiDocItems.Compute("SUM(DiscAmt)", ""), 0)))
                    End If

                    hdGrandTotal.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.GrandTotal - objCalculateDealAmount.CreditAmount)

                    hdTaxAmt.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalTaxAmount)

                    hdCRVTxtAmt.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalCRVTaxAmount)

                    hdnCreditAmount.Value = CCommon.GetDecimalFormat(IIf(objCalculateDealAmount.CreditAmount > 0, objCalculateDealAmount.CreditAmount * -1, 0))

                    Session("Itemslist") = dtOppBizDocsItemsWithKitCHilds

                    '''Add columns to datagrid
                    Dim i As Integer
                    Dim dtdgColumns As DataTable
                    dtdgColumns = ds.Tables(2)
                    Dim bColumn As BoundColumn
                    For i = 0 To dtdgColumns.Rows.Count - 1
                        bColumn = New BoundColumn
                        bColumn.HeaderText = dtdgColumns.Rows(i).Item("vcFormFieldName")
                        bColumn.DataField = dtdgColumns.Rows(i).Item("vcDbColumnName")
                        If dtdgColumns.Rows(i).Item("vcDbColumnName") = "SerialLotNo" Then
                            bColumn.ItemStyle.CssClass = "WordWrapSerialNo"
                        End If
                        'Added By   :Sachin Sadhu||Date:4thJune2014
                        'Description:To resolve Design issue 
                        If dtdgColumns.Rows(i).Item("vcDbColumnName") = "txtItemDesc" And dtdgColumns.Rows.Count = 2 Then
                            bColumn.ItemStyle.CssClass = "BizDocItemDesc TwoColumns"
                        ElseIf dtdgColumns.Rows(i).Item("vcDbColumnName") = "txtItemDesc" And dtdgColumns.Rows.Count = 1 Then
                            bColumn.ItemStyle.CssClass = "BizDocItemDesc OneColumns"
                        ElseIf dtdgColumns.Rows(i).Item("vcDbColumnName") = "txtItemDesc" And dtdgColumns.Rows.Count = 3 Then
                            bColumn.ItemStyle.CssClass = "BizDocItemDesc ThreeColumns"
                        ElseIf dtdgColumns.Rows(i).Item("vcDbColumnName") = "txtItemDesc" Then
                            bColumn.ItemStyle.CssClass = "BizDocItemDesc WordWrapSerialNo"
                        ElseIf dtdgColumns.Rows(i).Item("vcDbColumnName") = "vcItemName" Then
                            bColumn.ItemStyle.CssClass = "ItemName"
                        ElseIf CCommon.ToString(dtdgColumns.Rows(i).Item("vcDbColumnName")) = "vcPromotionDetail" Then
                            bColumn.HeaderStyle.Width = Unit.Percentage(25)
                            bColumn.ItemStyle.CssClass = "PromotionDetail"
                        End If
                        'End of Code

                        If dtdgColumns.Rows(i).Item("vcDbColumnName") = "monUnitSalePrice" Or dtdgColumns.Rows(i).Item("vcDbColumnName") = "monPrice" Or dtdgColumns.Rows(i).Item("vcDbColumnName") = "numCost" Then
                            bColumn.DataFormatString = CCommon.GetPriceDataFormatString()
                        ElseIf dtdgColumns.Rows(i).Item("vcFieldDataType") = "M" Then
                            bColumn.DataFormatString = "{0:#,##0.00}"
                        End If

                        dgBizDocs.Columns.Add(bColumn)
                    Next
                    BindItems()

                End If

                Dim objConfigWizard As New FormConfigWizard
                If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                    ibtnFooter.Attributes.Add("onclick", "return Openfooter(1)")
                    objConfigWizard.FormID = 7
                Else
                    objConfigWizard.FormID = 8
                    ibtnFooter.Attributes.Add("onclick", "return Openfooter(2)")
                End If
                hdnOppType.Value = dtOppBiDocDtl.Rows(0).Item("tintOppType")
                hdnBizDocId.Value = dtOppBiDocDtl.Rows(0).Item("numBizDocId")

                BindBizDocsTemplate()

                If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("numBizDocTempID")) Then
                    If Not ddlBizDocTemplate.Items.FindByValue(dtOppBiDocDtl.Rows(0).Item("numBizDocTempID")) Is Nothing Then
                        ddlBizDocTemplate.ClearSelection()
                        ddlBizDocTemplate.SelectedValue = dtOppBiDocDtl.Rows(0).Item("numBizDocTempID").ToString
                    End If
                End If

                objConfigWizard.DomainID = Session("DomainID")
                objConfigWizard.BizDocID = dtOppBiDocDtl.Rows(0).Item("numBizDocId")
                objConfigWizard.BizDocTemplateID = dtOppBiDocDtl.Rows(0).Item("numBizDocTempID")

                'Added by Neelam Kapila || 09/29/2017 - Added function to show BizDocId 
                lblBizDocId.Text = objConfigWizard.BizDocTemplateID
                If lblBizDocId.Text = "0" And lblBizDocId.Text = "" Then
                    divbizDocId.Visible = False
                Else
                    divbizDocId.Visible = True
                End If
                objOppBizDocs.BizDocId = dtOppBiDocDtl.Rows(0).Item("numBizDocId")
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.BizDocTemplateID = ddlBizDocTemplate.SelectedValue
                hplBizDocAtch.Text = "Attachments(" & IIf(objOppBizDocs.GetBizDocAttachments.Rows.Count = 0, 1, objOppBizDocs.GetBizDocAttachments.Rows.Count) & ")"

                Dim dsNew As DataSet
                Dim dtTable As DataTable
                dsNew = objConfigWizard.GetFieldFormListForBizDocsSumm
                dtTable = dsNew.Tables(1)
                'commented by chintan , reason : bug id 392
                'If dtTable.Rows.Count = 0 Then dtTable = dsNew.Tables(0)
                Dim tblrow As HtmlTableRow
                Dim tblCell As HtmlTableCell

                Dim strLabelCellID As String


                For Each dr As DataRow In dtTable.Rows
                    tblrow = New HtmlTableRow
                    'tblrow.Attributes.Add("style", "page-break-inside: avoid;")
                    tblCell = New HtmlTableCell
                    tblCell.Attributes.Add("class", "normal1 col-1")
                    tblCell.InnerText = dr("vcFormFieldName") & ": "
                    tblrow.Cells.Add(tblCell)

                    tblCell = New HtmlTableCell
                    tblCell.Attributes.Add("class", "normal1 col-2")
                    If dr("vcDbColumnName") = "SubTotal" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", CCommon.ToDouble(hdSubTotal.Value))
                    ElseIf dr("vcDbColumnName") = "ShippingAmount" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", CCommon.ToDouble(hdShipAmt.Value))
                    ElseIf dr("vcDbColumnName") = "TotalSalesTax" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", CCommon.ToDouble(hdTaxAmt.Value) - CCommon.ToDouble(hdCRVTxtAmt.Value))
                    ElseIf dr("vcDbColumnName") = "TotalCRVTax" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", CCommon.ToDouble(hdCRVTxtAmt.Value))
                    ElseIf dr("vcDbColumnName") = "LateCharge" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", CCommon.ToDouble(hdLateCharge.Value))
                    ElseIf dr("vcDbColumnName") = "Discount" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", CCommon.ToDouble(hdDisc.Value))
                    ElseIf dr("vcDbColumnName") = "CreditApplied" Then
                        tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", CCommon.ToDouble(hdnCreditAmount.Value))
                    ElseIf dr("vcDbColumnName") = "GrandTotal" Then
                        tblCell.ID = "tdGradTotal"
                        strLabelCellID = tblCell.ID
                        'tblCell.InnerText = hdGrandTotal.Value
                    Else
                        If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                            Dim taxAmt As Decimal
                            taxAmt = IIf(IsDBNull(dtOppBiDocItems.Compute("SUM([" & dr("vcFormFieldName") & "])", "[" & dr("vcFormFieldName") & "]>0")), 0, dtOppBiDocItems.Compute("SUM([" & dr("vcFormFieldName") & "])", "[" & dr("vcFormFieldName") & "]>0"))
                            tblCell.InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", CCommon.ToDouble(taxAmt))
                        End If
                    End If
                    tblrow.Cells.Add(tblCell)
                    tblBizDocSumm.Rows.Add(tblrow)
                Next
                If strLabelCellID <> "" Then
                    CType(tblBizDocSumm.FindControl(strLabelCellID), HtmlTableCell).InnerText = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + String.Format("{0:#,##0.00}", CCommon.ToDouble(hdGrandTotal.Value))
                End If

                lblBalance.Text = String.Format("{0:#,##0.00}", CCommon.ToDouble(hdGrandTotal.Value) - CCommon.ToDouble(lblAmountPaid.Text))
                hdnBalance.Value = CCommon.ToDecimal(lblBalance.Text)
                lblAmountPaidCurrency.Text = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString()
                lblBalanceDueCurrency.Text = dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString()
                btnEdit.Attributes.Add("onclick", "return OpenEdit(" & lngOppId.ToString & "," & lngOppBizDocID.ToString & "," & dtOppBiDocDtl.Rows(0).Item("tintOppType").ToString & ")")

                If dtOppBiDocDtl.Rows(0)("bitEnabled") = True And dtOppBiDocDtl.Rows(0)("txtBizDocTemplate").ToString().Length > 0 Then

                    'Dim dsAddress As DataSet
                    'objOppBizDocs.OppId = lngOppId
                    'objOppBizDocs.OppBizDocId = lngOppBizDocID
                    'objOppBizDocs.DomainID = Session("DomainID")
                    'dsAddress = objOppBizDocs.GetOPPGetOppAddressDetails

                    'Dim bizDocHtml As String
                    'Dim bizDocCss As String
                    'Dim stingBuilder As New System.Text.StringBuilder

                    ''BizDoc UI modification 
                    'Dim strCss As String = "<style>" & Server.HtmlDecode(dtOppBiDocDtl.Rows(0)("txtCSS").ToString()) & "</style>"
                    'Dim strBizDocUI As String = strCss & HttpUtility.HtmlDecode(dtOppBiDocDtl.Rows(0)("txtBizDocTemplate").ToString())

                    'bizDocHtml = HttpUtility.HtmlDecode(CCommon.ToString(dtOppBiDocDtl.Rows(0)("txtBizDocTemplate")))
                    'bizDocCss = HttpUtility.HtmlDecode(CCommon.ToString(dtOppBiDocDtl.Rows(0)("txtCss")))
                    'stingBuilder.Append("<html><head>")
                    ''stingBuilder.Append("<link rel='stylesheet' href='" & Current.Server.MapPath("~/CSS/master.css") & "' type='text/css' />")
                    'stingBuilder.Append("<style type=""text/css"">")
                    'stingBuilder.Append("pre.WordWrap")
                    'stingBuilder.Append("{")
                    'stingBuilder.Append("height:auto !important;")
                    'stingBuilder.Append("overflow: auto !important;")
                    'stingBuilder.Append("overflow-x: auto !important; /* Use horizontal scroller if needed; for Firefox 2, not */")
                    'stingBuilder.Append(" white-space: pre-wrap !important; ")
                    'stingBuilder.Append("white-space: -moz-pre-wrap !important; /* Mozilla, since 1999 */")
                    'stingBuilder.Append("word-wrap: break-word !important; /* Internet Explorer 5.5+ */")
                    'stingBuilder.Append("font-family:Arial,Helvetica,sans-serif !important; ")
                    'stingBuilder.Append("}")
                    'stingBuilder.Append(".ItemHeader, .ItemStyle, .AltItemStyle {")
                    'stingBuilder.Append("text-align: left;")
                    'stingBuilder.Append("}")
                    'If Not String.IsNullOrEmpty(bizDocCss) Then
                    '    stingBuilder.Append(bizDocCss)
                    'End If
                    'stingBuilder.Append("</style>")
                    'stingBuilder.Append("</head>")
                    'stingBuilder.Append("<body>")
                    'stingBuilder.Append("<table id=""tblFormattedBizDoc"" border=""0"" style=""border-color:Black;border-width:1px;border-style:solid;width:100%;"">")
                    'stingBuilder.Append("<tr>")
                    'stingBuilder.Append("<td>")
                    'Dim objWorkFlowExecution As New BACRM.BusinessLogic.Workflow.WorkFlowExecution
                    'stingBuilder.Append(objWorkFlowExecution.GetBizDocHtmlAfterReplacingTags(bizDocHtml, dtOppBiDocDtl, dsAddress, CCommon.ToLong(Session("DomainID")), lngOppId, lngOppBizDocID, CCommon.ToString(Session("DateFormat"))))
                    'stingBuilder.Append("</td>")
                    'stingBuilder.Append(" </tr>")
                    'stingBuilder.Append("</table>")
                    'stingBuilder.Append("</body>")
                    'stingBuilder.Append("</html>")

                    'litBizDocTemplate.Text = stingBuilder.ToString()
                    'tblOriginalBizDoc.Visible = False
                    'tblFormattedBizDoc.Visible = True
                    'BizDoc UI modification 
                    Dim strCss As String = "<style>" & Server.HtmlDecode(dtOppBiDocDtl.Rows(0)("txtCSS").ToString()) & "</style>"
                    Dim strBizDocUI As String = strCss & HttpUtility.HtmlDecode(dtOppBiDocDtl.Rows(0)("txtBizDocTemplate").ToString())

                    If strBizDocUI.Length > 0 Then
                        strBizDocUI = strBizDocUI.Replace("#Logo#", CCommon.RenderControl(imgLogo))
                        'strBizDocUI = strBizDocUI.Replace("#Signature#", CCommon.RenderControl(imgSignature))
                        'strBizDocUI = strBizDocUI.Replace("#OrganizationComments#", dtOppBiDocDtl.Rows(0)("vcOrganizationComments").ToString())

                        strBizDocUI = strBizDocUI.Replace("#FooterImage#", CCommon.RenderControl(imgFooter))
                        strBizDocUI = strBizDocUI.Replace("#BizDocType#", CCommon.RenderControl(lblBizDoc))
                        'strBizDocUI = strBizDocUI.Replace("#Customer/VendorBill-toAddress#", lblBillTo.Text)
                        'strBizDocUI = strBizDocUI.Replace("#Customer/VendorShip-toAddress#", lblShipTo.Text)

                        Dim dsAddress As DataSet
                        objOppBizDocs.OppId = lngOppId
                        objOppBizDocs.OppBizDocId = lngOppBizDocID
                        objOppBizDocs.DomainID = Session("DomainID")
                        dsAddress = objOppBizDocs.GetOPPGetOppAddressDetails

                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        'Customer/Vendor Information
                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrganizationName")))
                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationPhone#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrganizationPhone")))

                        If dsAddress.Tables(0).Rows.Count = 0 Then
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddressName#", "")
                            strBizDocUI = strBizDocUI.Replace("#BillingContact#", "")
                        Else
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCompanyName#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcCompanyName")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddress#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcFullAddress")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToStreet#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcStreet")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCity#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcCity")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToPostal#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcPostalCode")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToState#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcState")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToCountry#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcCountry")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderBillToAddressName#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcAddressName")))
                            strBizDocUI = strBizDocUI.Replace("#BillingContact#", CCommon.ToString(dsAddress.Tables(0).Rows(0)("vcContact")))
                        End If

                        If Not dsAddress Is Nothing AndAlso dsAddress.Tables.Count > 4 AndAlso dsAddress.Tables(4).Rows.Count > 0 Then
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCompanyName#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcCompanyName")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddress#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcFullAddress")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToStreet#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcStreet")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCity#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcCity")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToPostal#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcPostalCode")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToState#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcState")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCountry#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcCountry")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddressName#", CCommon.ToString(dsAddress.Tables(4).Rows(0)("vcAddressName")))
                        Else
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorBillToAddressName#", "")
                        End If

                        If dsAddress.Tables(1).Rows.Count = 0 Then
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddressName#", "")
                            strBizDocUI = strBizDocUI.Replace("#ShippingContact#", "")
                        Else
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCompanyName#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcCompanyName")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddress#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcFullAddress")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToStreet#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcStreet")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCity#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcCity")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToPostal#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcPostalCode")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToState#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcState")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToCountry#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcCountry")))
                            strBizDocUI = strBizDocUI.Replace("#OppOrderShipToAddressName#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcAddressName")))
                            strBizDocUI = strBizDocUI.Replace("#ShippingContact#", CCommon.ToString(dsAddress.Tables(1).Rows(0)("vcContact")))
                        End If

                        If Not dsAddress Is Nothing AndAlso dsAddress.Tables.Count > 5 AndAlso dsAddress.Tables(5).Rows.Count > 0 Then
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCompanyName#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcCompanyName")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddress#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcFullAddress")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToStreet#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcStreet")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCity#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcCity")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToPostal#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcPostalCode")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToState#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcState")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCountry#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcCountry")))
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddressName#", CCommon.ToString(dsAddress.Tables(5).Rows(0)("vcAddressName")))
                        Else
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#Customer/VendorShipToAddressName#", "")
                        End If

                        If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                            If strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") + "#OppOrderChangeBillToHeader(".Length, strBizDocUI.LastIndexOf(")#") - strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") - "#OppOrderChangeBillToHeader(".Length)
                                hplBillto.InnerHtml = strLink '"<font color=""white"">" + strLink + "</font>"
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeBillToHeader(" & strLink & ")#", CCommon.RenderControl(hplBillto))
                            Else
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeBillToHeader#", CCommon.RenderControl(hplBillto))
                            End If

                            If strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") + "#OppOrderChangeShipToHeader(".Length, strBizDocUI.IndexOf(")#") - strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") - "#OppOrderChangeShipToHeader(".Length)
                                hplShipTo.InnerHtml = strLink '"<font color=""white"">" + strLink + "</font>"
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeShipToHeader(" & strLink & ")#", CCommon.RenderControl(hplShipTo))
                            Else
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeShipToHeader#", CCommon.RenderControl(hplShipTo))
                            End If
                        Else
                            If strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") + "#OppOrderChangeBillToHeader(".Length, strBizDocUI.LastIndexOf(")#") - strBizDocUI.IndexOf("#OppOrderChangeBillToHeader(") - "#OppOrderChangeBillToHeader(".Length)
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeBillToHeader(" & strLink & ")#", strLink)
                            Else
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeBillToHeader#", "Bill To")
                            End If

                            If strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") + "#OppOrderChangeShipToHeader(".Length, strBizDocUI.IndexOf(")#") - strBizDocUI.IndexOf("#OppOrderChangeShipToHeader(") - "#OppOrderChangeShipToHeader(".Length)
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeShipToHeader(" & strLink & ")#", strLink)
                            Else
                                strBizDocUI = strBizDocUI.Replace("#OppOrderChangeShipToHeader#", "Ship To")
                            End If
                        End If

                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationComments#", dtOppBiDocDtl.Rows(0)("vcOrganizationComments").ToString())

                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationContactName#", CCommon.RenderControl(lblOrganizationContactName))
                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationContactEmail#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactEmail")))
                        strBizDocUI = strBizDocUI.Replace("#Customer/VendorOrganizationContactPhone#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactPhone")))
                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                        'Employer Information
                        strBizDocUI = strBizDocUI.Replace("#EmployerOrganizationName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("EmployerOrganizationName")))
                        strBizDocUI = strBizDocUI.Replace("#EmployerOrganizationPhone#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("EmployerOrganizationPhone")))

                        If dsAddress.Tables(2).Rows.Count = 0 Then
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddressName#", "")
                        Else
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCompanyName#", dsAddress.Tables(2).Rows(0)("vcCompanyName"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddress#", dsAddress.Tables(2).Rows(0)("vcFullAddress"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToStreet#", dsAddress.Tables(2).Rows(0)("vcStreet"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCity#", dsAddress.Tables(2).Rows(0)("vcCity"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToPostal#", dsAddress.Tables(2).Rows(0)("vcPostalCode"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToState#", dsAddress.Tables(2).Rows(0)("vcState"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToCountry#", dsAddress.Tables(2).Rows(0)("vcCountry"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerBillToAddressName#", dsAddress.Tables(2).Rows(0)("vcAddressName"))
                        End If

                        If dsAddress.Tables(3).Rows.Count = 0 Then
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCompanyName#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddress#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToStreet#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCity#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToPostal#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToState#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCountry#", "")
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddressName#", "")
                        Else
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCompanyName#", dsAddress.Tables(3).Rows(0)("vcCompanyName"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddress#", dsAddress.Tables(3).Rows(0)("vcFullAddress"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToStreet#", dsAddress.Tables(3).Rows(0)("vcStreet"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCity#", dsAddress.Tables(3).Rows(0)("vcCity"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToPostal#", dsAddress.Tables(3).Rows(0)("vcPostalCode"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToState#", dsAddress.Tables(3).Rows(0)("vcState"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToCountry#", dsAddress.Tables(3).Rows(0)("vcCountry"))
                            strBizDocUI = strBizDocUI.Replace("#EmployerShipToAddressName#", dsAddress.Tables(3).Rows(0)("vcAddressName"))
                        End If

                        If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 2 Then
                            If strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") + "#EmployerChangeBillToHeader(".Length, strBizDocUI.LastIndexOf(")#") - strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") - "#EmployerChangeBillToHeader(".Length)
                                hplBillto.InnerHtml = strLink '"<font color=""white"">" + strLink + "</font>"
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeBillToHeader(" & strLink & ")#", CCommon.RenderControl(hplBillto))
                            Else
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeBillToHeader#", CCommon.RenderControl(hplBillto))
                            End If

                            If strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") + "#EmployerChangeShipToHeader(".Length, strBizDocUI.IndexOf(")#") - strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") - "#EmployerChangeShipToHeader(".Length)
                                hplShipTo.InnerHtml = strLink '"<font color=""white"">" + strLink + "</font>"
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeShipToHeader(" & strLink & ")#", CCommon.RenderControl(hplShipTo))
                            Else
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeShipToHeader#", CCommon.RenderControl(hplShipTo))
                            End If
                        Else
                            If strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") + "#EmployerChangeBillToHeader(".Length, strBizDocUI.LastIndexOf(")#") - strBizDocUI.IndexOf("#EmployerChangeBillToHeader(") - "#EmployerChangeBillToHeader(".Length)
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeBillToHeader(" & strLink & ")#", strBizDocUI)
                            Else
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeBillToHeader#", "Bill To")
                            End If

                            If strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") > 0 Then
                                Dim strLink As String = strBizDocUI.Substring(strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") + "#EmployerChangeShipToHeader(".Length, strBizDocUI.IndexOf(")#") - strBizDocUI.IndexOf("#EmployerChangeShipToHeader(") - "#EmployerChangeShipToHeader(".Length)
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeShipToHeader(" & strLink & ")#", strLink)
                            Else
                                strBizDocUI = strBizDocUI.Replace("#EmployerChangeShipToHeader#", "Ship To")
                            End If
                        End If

                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

                        strBizDocUI = strBizDocUI.Replace("#BizDocStatus#", lblStatus.Text)
                        strBizDocUI = strBizDocUI.Replace("#Currency#", lblBalanceDueCurrency.Text)
                        strBizDocUI = strBizDocUI.Replace("#AmountPaid#", lblAmountPaid.Text)

                        If CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numBizDocId")) <> enmBizDocTemplate_BizDocID.CustomerStatement Then
                            strBizDocUI = strBizDocUI.Replace("#BalanceDue#", CCommon.RenderControl(lblBalance)) 'used by amt paid js
                        Else
                            strBizDocUI = strBizDocUI.Replace("#BalanceDue#", decBalanceDue) 'used by amt paid js
                        End If


                        strBizDocUI = strBizDocUI.Replace("#Discount#", lblDiscount.Text)
                        strBizDocUI = strBizDocUI.Replace("#BillingTerms#", lblBillingTerms.Text)
                        If CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("dtFromDate")).Length > 3 Then
                            strBizDocUI = strBizDocUI.Replace("#BillingTermFromDate#", FormattedDateFromDate(dtOppBiDocDtl.Rows(0).Item("dtFromDate"), Session("DateFormat")))
                        End If
                        strBizDocUI = strBizDocUI.Replace("#BillingTermsName#", lblBillingTermsName.Text)

                        strBizDocUI = strBizDocUI.Replace("#DueDate#", lblDuedate.Text)
                        strBizDocUI = strBizDocUI.Replace("#BizDocID#", CCommon.RenderControl(lblBizDocIDValue))
                        strBizDocUI = strBizDocUI.Replace("#P.O.NO#", lblNo.Text)
                        strBizDocUI = strBizDocUI.Replace("#OrderID#", CCommon.RenderControl(hplOppID))
                        strBizDocUI = strBizDocUI.Replace("#OrderIDBarCode#", CCommon.RenderControl(divBarCode))
                        strBizDocUI = strBizDocUI.Replace("#Comments#", lblComments.Text)
                        strBizDocUI = strBizDocUI.Replace("#BizDocCreatedDate#", lblDate.Text)

                        strBizDocUI = strBizDocUI.Replace("#AmountPaidPopUp#", CCommon.RenderControl(hplAmountPaid))
                        strBizDocUI = strBizDocUI.Replace("#ChangeDueDate#", CCommon.RenderControl(hplDueDate))
                        strBizDocUI = strBizDocUI.Replace("#AssigneeName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("AssigneeName")))
                        strBizDocUI = strBizDocUI.Replace("#AssigneeEmail#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("AssigneeEmail")))
                        strBizDocUI = strBizDocUI.Replace("#AssigneePhoneNo#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("AssigneePhone")))
                        strBizDocUI = strBizDocUI.Replace("#PartnerSource#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcPartner")))
                        strBizDocUI = strBizDocUI.Replace("#DropShip#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcDropShip")))
                        strBizDocUI = strBizDocUI.Replace("#ReleaseDate#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcReleaseDate")))
                        strBizDocUI = strBizDocUI.Replace("#RequiredDate#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcRequiredDate")))
                        strBizDocUI = strBizDocUI.Replace("#InventoryStatus#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcInventoryStatus")))
                        strBizDocUI = strBizDocUI.Replace("#Reference#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcSourceBizDocID")))
                        'strBizDocUI = strBizDocUI.Replace("#OrganizationContactName#", CCommon.RenderControl(lblOrganizationContactName))
                        'strBizDocUI = strBizDocUI.Replace("#OrganizationContactPhone#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactPhone")))
                        'strBizDocUI = strBizDocUI.Replace("#OrganizationContactEmail#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrgContactEmail")))
                        'strBizDocUI = strBizDocUI.Replace("#OrganizationBillingAddress#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("CompanyBillingAddress")))
                        'strBizDocUI = strBizDocUI.Replace("#OrganizationPhone#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrganizationPhone")))
                        strBizDocUI = strBizDocUI.Replace("#OrderRecOwner#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OnlyOrderRecOwner")))

                        'strBizDocUI = strBizDocUI.Replace("#BillToAddressName#", lblBillToAddressName.Text)
                        'strBizDocUI = strBizDocUI.Replace("#ShipToAddressName#", lblShipToAddressName.Text)

                        'strBizDocUI = strBizDocUI.Replace("#BillToCompanyName#", CCommon.RenderControl(lblBillToCompanyName))
                        'strBizDocUI = strBizDocUI.Replace("#ShipToCompanyName#", CCommon.RenderControl(lblShipToCompanyName))

                        strBizDocUI = strBizDocUI.Replace("#ShippingCompany#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("ShipVia")))
                        strBizDocUI = strBizDocUI.Replace("#ShippingService#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcShippingService")))
                        strBizDocUI = strBizDocUI.Replace("#PackingSlipID#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcPackingSlip")))
                        strBizDocUI = strBizDocUI.Replace("#BizDocTemplateName#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcTemplateName")))
                        strBizDocUI = strBizDocUI.Replace("#TrackingNo#", strTrackingNo)
                        strBizDocUI = strBizDocUI.Replace("#CustomerPO##", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcCustomerPO#")))
                        strBizDocUI = strBizDocUI.Replace("#SOComments#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcSOComments")))
                        strBizDocUI = strBizDocUI.Replace("#ParcelShippingAccount#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcShippersAccountNo")))
                        strBizDocUI = strBizDocUI.Replace("#OrderCreatedDate#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("OrderCreatedDate")))

                        strBizDocUI = strBizDocUI.Replace("#VendorInvoice#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcVendorInvoice")))

                        Dim lNumericToWord As New NumericToWord
                        strBizDocUI = strBizDocUI.Replace("#GrandTotalinWords#", lNumericToWord.SpellNumber(Replace(hdGrandTotal.Value, ",", "")))

                        If dtOppBiDocDtl.Columns.Contains("vcTotalQtybyUOM") Then
                            strBizDocUI = strBizDocUI.Replace("#TotalQytbyUOM#", CCommon.ToString(dtOppBiDocDtl.Rows(0).Item("vcTotalQtybyUOM")))
                        Else
                            strBizDocUI = strBizDocUI.Replace("#TotalQytbyUOM#", "")
                        End If

                        'Replace Customer Statement Details
                        If dtARAging.Rows.Count > 0 AndAlso dtARAgingInvoice.Rows.Count > 0 Then

                            strBizDocUI = strBizDocUI.Replace("#Today-30(CurrentAmtDue)#", CCommon.ToDecimal(dtARAging.Rows(0)("numThirtyDays")))
                            strBizDocUI = strBizDocUI.Replace("#31-60(CurrentAmtDue)#", CCommon.ToDecimal(dtARAging.Rows(0)("numSixtyDays")))
                            strBizDocUI = strBizDocUI.Replace("#61-90(CurrentAmtDue)#", CCommon.ToDecimal(dtARAging.Rows(0)("numNinetyDays")))
                            strBizDocUI = strBizDocUI.Replace("#Over91(CurrentAmtDue)#", CCommon.ToDecimal(dtARAging.Rows(0)("numOverNinetyDays")))
                            strBizDocUI = strBizDocUI.Replace("#Today-30(PastDue)#", CCommon.ToDecimal(dtARAging.Rows(0)("numThirtyDaysOverDue")))
                            strBizDocUI = strBizDocUI.Replace("#31-60(PastDue)#", CCommon.ToDecimal(dtARAging.Rows(0)("numSixtyDaysOverDue")))
                            strBizDocUI = strBizDocUI.Replace("#61-90(PastDue)#", CCommon.ToDecimal(dtARAging.Rows(0)("numNinetyDaysOverDue")))
                            strBizDocUI = strBizDocUI.Replace("#Over91(PastDue)#", CCommon.ToDecimal(dtARAging.Rows(0)("numOverNinetyDaysOverDue")))
                            strBizDocUI = strBizDocUI.Replace("#StatementDate#", FormattedDateFromDate(Date.UtcNow, Session("DateFormat")))
                            'strBizDocUI = strBizDocUI.Replace("#BalanceDue#", "")
                            'strBizDocUI = strBizDocUI.Replace("", "")

                        End If

                        If dtOppBiDocItems IsNot Nothing AndAlso dtOppBiDocItems.Rows.Count > 0 Then
                            If dtOppBiDocItems.Select("numItemCode <>" & CCommon.ToDouble(Session("DiscountServiceItem")) & " AND numItemCode <> " & CCommon.ToDouble(Session("ShippingServiceItem"))).Length > 0 Then
                                strBizDocUI = strBizDocUI.Replace("#TotalQuantity#", String.Format("{0:#,##0.##}", CCommon.ToDecimal(dtOppBiDocItems.Select("numItemCode <>" & CCommon.ToDouble(Session("DiscountServiceItem")) & " AND numItemCode <> " & CCommon.ToDouble(Session("ShippingServiceItem"))).CopyToDataTable().Compute("sum(numUnitHour)", ""))))
                            Else
                                strBizDocUI = strBizDocUI.Replace("#TotalQuantity#", String.Format("{0:#,##0.##}", 0))
                            End If
                        Else
                            strBizDocUI = strBizDocUI.Replace("#TotalQuantity#", String.Format("{0:#,##0.##}", 0))
                        End If

                        'Replace custom field values
                        If CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numBizDocId")) <> enmBizDocTemplate_BizDocID.CustomerStatement Then


                            Dim objCustomFields As New CustomFields
                            If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                                objCustomFields.locId = 2
                            Else
                                objCustomFields.locId = 6
                            End If
                            objCustomFields.RelId = 0
                            objCustomFields.DomainID = Session("DomainID")
                            objCustomFields.RecordId = lngOppId
                            Dim dsCust As DataSet = objCustomFields.GetCustFlds
                            For Each drow As DataRow In dsCust.Tables(0).Rows
                                If CCommon.ToLong(drow("numListID")) > 0 Then
                                    strBizDocUI = strBizDocUI.Replace("#" & drow("fld_label").ToString.Trim.Replace(" ", "") & "#", objCommon.GetListItemValue(CCommon.ToString(drow("Value")), Session("DomainID")))
                                Else
                                    strBizDocUI = strBizDocUI.Replace("#" & drow("fld_label").ToString.Trim.Replace(" ", "") & "#", drow("Value"))
                                End If
                            Next
                        End If

                        If CShort(dtOppBiDocDtl.Rows(0).Item("tintOppType")) = 1 AndAlso CShort(dtOppBiDocDtl.Rows(0).Item("tintOppStatus")) = 0 AndAlso dtOppBiDocItems.Select("bitKitParent=1").Length > 0 AndAlso dtOppBiDocItems.Select("numParentOppItemID > 0").Length > 0 Then
                            Dim subTotal As String
                            Dim taxAmt As String
                            Dim crvTaxAmt As String
                            Dim lateCharge As String
                            Dim disc As String
                            Dim creditAmount As String
                            Dim grandTotal As Double
                            Dim amountPaid As Double

                            Dim objCalculateDealAmount As New CalculateDealAmount
                            Dim objWorkFlowExecution As New WorkFlowExecution
                            objCommon = New CCommon
                            Dim dsDateFormat As DataSet = objCommon.GetDomainDateFormat(CCommon.ToLong(Session("DomainID")))
                            Dim strDecimalpoints As String = dsDateFormat.Tables(0).Rows(0)("tintDecimalPoints").ToString()

                            Dim arrKits As DataRow() = dtOppBiDocItems.Select("bitKitParent=1")
                            If Not arrKits Is Nothing AndAlso arrKits.Length > 0 Then
                                Dim tempProducts = ""

                                For Each drKit As DataRow In arrKits
                                    Dim arrKitItemsGroup As DataRow() = dtOppBiDocItems.Select("numoppitemtCode=" & CCommon.ToLong(drKit("numoppitemtCode")) & " OR (numParentOppItemID=0 AND bitKitParent=0) OR numParentOppItemID=" & CCommon.ToLong(drKit("numoppitemtCode")))

                                    If Not arrKitItemsGroup Is Nothing AndAlso arrKitItemsGroup.Length > 0 Then
                                        Dim dataView As New DataView(arrKitItemsGroup.CopyToDataTable())
                                        dataView.Sort = "bitKitParent DESC, numSortOrder ASC"
                                        Dim tempItems As DataTable = dataView.ToTable()

                                        tempProducts = tempProducts & objWorkFlowExecution.GetBizDocItems(dataView.ToTable(), ds.Tables(2), strDecimalpoints, True)

                                        objCalculateDealAmount.CalculateDealAmount(lngOppId, lngOppBizDocID, CShort(dtOppBiDocDtl.Rows(0).Item("tintOppType")), CCommon.ToLong(Session("DomainID")), tempItems, FromBizInvoice:=True)
                                        subTotal = CCommon.GetDecimalFormatService(objCalculateDealAmount.TotalAmount, strDecimalpoints)
                                        taxAmt = CCommon.GetDecimalFormatService(objCalculateDealAmount.TotalTaxAmount - objCalculateDealAmount.TotalCRVTaxAmount, strDecimalpoints)
                                        crvTaxAmt = CCommon.GetDecimalFormatService(objCalculateDealAmount.TotalCRVTaxAmount, strDecimalpoints)
                                        lateCharge = CCommon.GetDecimalFormatService(objCalculateDealAmount.TotalLateCharges, strDecimalpoints)
                                        disc = CCommon.GetDecimalFormatService(objCalculateDealAmount.TotalDiscount, strDecimalpoints)
                                        creditAmount = CCommon.GetDecimalFormatService(IIf(objCalculateDealAmount.CreditAmount > 0, objCalculateDealAmount.CreditAmount * -1, 0), strDecimalpoints)
                                        grandTotal = CCommon.ToDouble(objCalculateDealAmount.GrandTotal) - CCommon.ToDouble(objCalculateDealAmount.CreditAmount)

                                        tempProducts = tempProducts & "<br/>" & objWorkFlowExecution.GetBizDocSummary(dtOppBiDocDtl, tempItems, CCommon.ToLong(Session("DomainID")), subTotal, taxAmt, crvTaxAmt, lateCharge, disc, creditAmount, grandTotal, strDecimalpoints, True) & "<br/>"
                                    End If
                                Next

                                strBizDocUI = strBizDocUI.Replace("#Products#", tempProducts)
                                strBizDocUI = strBizDocUI.Replace("#BizDocSummary#", "")
                            Else
                                strBizDocUI = strBizDocUI.Replace("#Products#", "")
                                strBizDocUI = strBizDocUI.Replace("#BizDocSummary#", "")
                            End If
                        Else
                            strBizDocUI = strBizDocUI.Replace("#Products#", CCommon.RenderControl(dgBizDocs))
                            strBizDocUI = strBizDocUI.Replace("#BizDocSummary#", CCommon.RenderControl(tblBizDocSumm))
                        End If



                        litBizDocTemplate.Text = strBizDocUI
                        tblOriginalBizDoc.Visible = False
                        tblFormattedBizDoc.Visible = True
                    End If
                Else
                    tblOriginalBizDoc.Visible = True
                    tblFormattedBizDoc.Visible = False
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub getAddDetails()
            Try
                Dim dtOppBizAddDtl As DataTable
                Dim objOppBizDocs As New OppBizDocs
                objOppBizDocs.OppBizDocId = lngOppBizDocID
                objOppBizDocs.OppId = lngOppId
                objOppBizDocs.DomainID = Session("DomainID")
                dtOppBizAddDtl = objOppBizDocs.GetOppAddressDetail
                If dtOppBizAddDtl.Rows.Count <> 0 Then

                    'lblAddress.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("CompName")), "", dtOppBizAddDtl.Rows(0).Item("CompName"))
                    lblBillTo.Text = CCommon.ToString(dtOppBizAddDtl.Rows(0).Item("BillAdd"))
                    lblShipTo.Text = CCommon.ToString(dtOppBizAddDtl.Rows(0).Item("ShipAdd"))

                    lblBillToAddressName.Text = CCommon.ToString(dtOppBizAddDtl.Rows(0).Item("BillToAddressName"))
                    lblShipToAddressName.Text = CCommon.ToString(dtOppBizAddDtl.Rows(0).Item("ShipToAddressName"))

                    lblBillToCompanyName.Text = CCommon.ToString(dtOppBizAddDtl.Rows(0).Item("BillToCompanyName"))
                    lblShipToCompanyName.Text = CCommon.ToString(dtOppBizAddDtl.Rows(0).Item("ShipToCompanyName"))

                    hplOppID.Text = CCommon.ToString(dtOppBizAddDtl.Rows(0).Item("OppName"))
                    lblBizDoc.Text = CCommon.ToString(dtOppBizAddDtl.Rows(0).Item("BizDcocName"))
                    lblBizDocIDLabel.Text = lblBizDoc.Text & "#"
                    txtBizDoc.Text = CCommon.ToString(dtOppBizAddDtl.Rows(0).Item("BizDoc"))
                    txtConEmail.Text = CCommon.ToString(dtOppBizAddDtl.Rows(0).Item("vcEmail"))
                    txtOppOwner.Text = CCommon.ToString(dtOppBizAddDtl.Rows(0).Item("Owner"))
                    txtCompName.Text = CCommon.ToString(dtOppBizAddDtl.Rows(0).Item("CompName"))
                    txtConID.Text = CCommon.ToString(dtOppBizAddDtl.Rows(0).Item("numContactID"))
                    txtBizDocRecOwner.Text = CCommon.ToString(dtOppBizAddDtl.Rows(0).Item("BizDocOwner"))
                    lblBarCode.Text = lngOppId
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindItems()
            Try
                Dim dtItems As New DataTable
                dtItems = Session("Itemslist")

                If Not dtItems Is Nothing AndAlso dtItems.Rows.Count > 0 Then
                    For Each dr As DataRow In dtItems.Rows
                        dr("txtItemDesc") = CCommon.ToString(dr("txtItemDesc")).Replace(vbLf, "<br>")
                    Next
                End If

                dgBizDocs.DataSource = dtItems
                dgBizDocs.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub Save()
            Try

                Dim JournalId As Integer
                Dim dtOppBiDocDtl As New DataTable
                Dim lintAuthorizativeBizDocsId As Integer
                Dim objOppBizDocs As New OppBizDocs
                Dim lintOppTypeId As Integer
                Dim i, j As Integer
                Dim lintBizDocId As Integer

                'objOppBizDocs.vcURL = txtTrackingURL.Text
                'objOppBizDocs.vcPONo = lblRefOrderNo.Text
                objOppBizDocs.vcComments = CCommon.ToString(Replace(txtComments.Text, "'", "''"))
                objOppBizDocs.UserCntID = Session("UserContactID")
                objOppBizDocs.OppBizDocId = lngOppBizDocID
                'objOppBizDocs.ShipDoc = ddlShipDoc.SelectedValue
                'objOppBizDocs.ShipCompany = ddlShipCompany.SelectedValue
                objOppBizDocs.BizDocStatus = ddlBizDocStatus.SelectedValue
                objOppBizDocs.SaveInvoiceDtl()

                objOppBizDocs.DomainID = Context.Session("DomainID")
                objOppBizDocs.OppId = lngOppId
                objOppBizDocs.UserCntID = Context.Session("UserContactID")
                objOppBizDocs.BizDocStatus = ddlBizDocStatus.SelectedValue
                objOppBizDocs.OpportunityBizDocStatusChange()

                ''Added By Sachin Sadhu||Date:1stMay2014
                ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                ''          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Context.Session("DomainID")
                objWfA.UserCntID = Context.Session("UserContactID")
                objWfA.RecordID = lngOppBizDocID
                objWfA.SaveWFBizDocQueue()
                'end of code



                ''sending mail
                Dim objAlerts As New CAlerts
                Dim dtAlerts As DataTable
                objAlerts.BizDocs = txtBizDoc.Text
                objAlerts.DomainID = Session("DomainID")
                dtAlerts = objAlerts.BizDocAlertDTLs
                If dtAlerts.Rows.Count > 0 Then
                    j = 0
                    For i = 0 To dtAlerts.Rows.Count - 1
                        If dtAlerts.Rows(i).Item("bitModified") = True Then
                            j = 1
                            Exit For
                        End If
                    Next
                    If j = 1 Then
                        Dim dtEmailTemplate As DataTable
                        Dim objDocuments As New DocumentList
                        objDocuments.GenDocID = dtAlerts.Rows(i).Item("numEmailTemplate")
                        objDocuments.DomainID = Session("DomainID")
                        dtEmailTemplate = objDocuments.GetDocByGenDocID
                        If dtEmailTemplate.Rows.Count > 0 Then
                            Dim objSendMail As New Email
                            Dim dtMergeFields As New DataTable
                            Dim drNew As DataRow
                            dtMergeFields.Columns.Add("BizDocID")
                            dtMergeFields.Columns.Add("DealAmount")
                            dtMergeFields.Columns.Add("vcCompanyName")
                            drNew = dtMergeFields.NewRow
                            drNew("BizDocID") = lblBizDoc.Text
                            drNew("DealAmount") = hdGrandTotal.Value
                            drNew("vcCompanyName") = txtCompName.Text

                            dtMergeFields.Rows.Add(drNew)
                            objCommon.byteMode = 1
                            objCommon.ContactID = txtOppOwner.Text
                            Dim strTo As String
                            strTo = IIf(dtAlerts.Rows(i).Item("bitOppOwner") = 1, objCommon.GetAccountHoldersEmail, "")
                            If strTo <> "" Then
                                strTo = strTo & ";" & IIf(dtAlerts.Rows(i).Item("bitPriConatct") = 1, txtConEmail.Text, "")
                                strTo = strTo.TrimEnd(";")
                            Else : strTo = IIf(dtAlerts.Rows(i).Item("bitPriConatct") = 1, txtConEmail.Text, "")
                            End If
                            objSendMail.SendEmail(dtEmailTemplate.Rows(0).Item("vcSubject"), dtEmailTemplate.Rows(0).Item("vcDocdesc"), IIf(dtAlerts.Rows(i).Item("bitCCManager") = 1, objCommon.GetManagerEmail, ""), Session("UserEmail"), strTo, dtMergeFields)
                        End If

                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub





        Sub Disable()
            Try
                hplAmountPaid.Enabled = False
                btnSave.Enabled = False
                btnSaveAndClose.Enabled = False
                txtComments.ReadOnly = True
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Overloads Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Save()
                Dim objRule As New OrderAutoRules
                If lngOppBizDocID <> 0 Then objRule.GenerateAutoPO(lngOppBizDocID)

                getBizDocDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Overloads Sub btnSaveAndClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveAndClose.Click
            Try
                Save()
                Dim objRule As New OrderAutoRules
                If lngOppBizDocID <> 0 Then objRule.GenerateAutoPO(lngOppBizDocID)
                Response.Write("<script>window.close();</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Function ReturnMoney(ByVal Money)
            Try
                Return String.Format("{0:#,###.##}", Math.Round(Money, 2))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnApprove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnApprove.Click
            Try
                If lngOppBizDocID > 0 Then
                    Dim objDocuments As New DocumentList
                    objDocuments.GenDocID = lngOppBizDocID
                    objDocuments.ContactID = Session("UserContactID")
                    objDocuments.CDocType = "B"
                    objDocuments.byteMode = 3
                    objDocuments.Comments = txtComment.Text
                    objDocuments.ManageApprovers()
                    pnlApprove.Visible = False

                    SendEmail(True)
                    'refresh pending ,Approve,Declined status
                    getBizDocDetails()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnDecline_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDecline.Click
            Try
                If lngOppBizDocID > 0 Then
                    Dim objDocuments As New DocumentList
                    objDocuments.GenDocID = lngOppBizDocID
                    objDocuments.ContactID = Session("UserContactID")
                    objDocuments.CDocType = "B"
                    objDocuments.byteMode = 4
                    objDocuments.Comments = txtComment.Text
                    objDocuments.ManageApprovers()
                    pnlApprove.Visible = False

                    SendEmail(False)

                    'refresh pending ,Approve,Declined status
                    getBizDocDetails()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnTemp_DontDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTemp_DontDelete.Click
            Try
                getAddDetails()
                getBizDocDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ibtnSendEmail_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnSendEmail.Click
            Try
                Dim html As String = "<html><head><link rel='stylesheet' href='" & Server.MapPath("~/CSS/master.css") & "' type='text/css' /><style type=""text/css"">pre.WordWrap{height:auto !important;overflow: auto !important;overflow-x: auto !important; white-space: pre-wrap !important; white-space: -moz-pre-wrap !important; word-wrap: break-word !important; font-family:Arial,Helvetica,sans-serif !important;} .ItemHeader, .ItemStyle, .AltItemStyle {text-align: left;}</style></head><body><table id=""tblFormattedBizDoc"" border=""0"" style=""border-color:Black;border-width:1px;border-style:solid;width:100%;""><tr><td>" + GetHtml() + "</td></tr></table></body></html>"

                Session("Attachements") = Nothing 'reset to nothing to avoid duplicate attachments
                Dim objBizDocs As New OppBizDocs
                Dim dt As DataTable
                objBizDocs.BizDocId = txtBizDoc.Text
                objBizDocs.DomainID = Session("DomainID")
                objBizDocs.BizDocTemplateID = ddlBizDocTemplate.SelectedValue
                dt = objBizDocs.GetBizDocAttachments
                Dim i As Integer
                Dim strFileName As String = ""
                Dim strFilePhysicalLocation As String = ""
                For i = 0 To dt.Rows.Count - 1
                    If dt.Rows(i).Item("vcDocName") = "BizDoc" Then
                        Dim objHTMLToPDF As New HTMLToPDF
                        strFileName = objHTMLToPDF.ConvertHTML2PDF(html, CCommon.ToLong(Session("DomainID")), numOrientation:=IIf(hdnOrientation.Value = "2", 2, 1), keepFooterAtBottom:=CCommon.ToBool(hdnKeepFooterBottom.Value))
                    Else
                        strFileName = dt.Rows(i).Item("vcURL")
                    End If
                    strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
                    objCommon.AddAttchmentToSession(IIf(lblBizDocIDValue.Text.Trim.Length > 0, lblBizDocIDValue.Text.Trim.Replace(" ", "_") + ".pdf", strFileName), CCommon.GetDocumentPath(Session("DomainID")) & strFileName, strFilePhysicalLocation)
                Next
                Dim str As String
                str = "<script language='javascript'>window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&PickAtch=1&isAttachmentRequired=1&Lsemail=" & HttpUtility.JavaScriptStringEncode(txtConEmail.Text) & "&pqwRT=" & txtConID.Text & "&frm=BizInvoice&BizDocID=" & lngOppBizDocID.ToString() & "&OppID=" & lngOppId.ToString() & "&OppType=" & hdnOppType.Value & "','ComposeWindow','toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')</script>"
                'Response.Write(str)
                Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Email", str, False)
                getBizDocDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Function GetHtml() As String
            Try
                Dim content As New System.Text.StringBuilder()
                Dim sWriter As New StringWriter(content)
                Dim htmlWriter As New HtmlTextWriter(sWriter)
                pnlBizDoc.RenderControl(htmlWriter)

                Dim html As String = content.ToString()
                html = html.Replace("<div id=""pnlBizDoc"" style=""border-color:Black;border-width:1px;border-style:solid;"">", "")
                html = html.Substring(0, html.Length - 6)

                Return html
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Sub SetWebControlAttributes()
            Try
                'btnClose.Attributes.Add("onclick", "return Close()")
                ibtnLogo.Attributes.Add("onclick", "return OpenLogoPage();")

                hplAmountPaid.Attributes.Add("onclick", "return OpenAmtPaid(" & lngOppBizDocID & "," & lngOppId.ToString & " ," & hdnDivID.Value & ");")
                btnSave.Attributes.Add("onclick", "return Save()")
                btnSaveAndClose.Attributes.Add("onclick", "return Save()")
                ibtnPrint.Attributes.Add("onclick", "return PrintIt();")
                hplRequestApproval.Attributes.Add("onclick", "return openApp(" & lngOppBizDocID & ",'B'," & lngOppId & ")")
                hplBizDocAtch.Attributes.Add("onclick", "OpenAtch(" & lngOppId.ToString & "," & lngOppBizDocID.ToString & "," & Session("DomainID") & "," & Session("UserContactID") & ")")
                ibtnExport.Attributes.Add("onclick", "OpenExport(" & lngOppBizDocID.ToString & "," & lngOppId.ToString & "); return false;")
                hplOppID.Attributes.Add("onclick", "return Close1(" & lngOppId.ToString & ")")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub ibtnExportPDF_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnExportPDF.Click
            Try
                Dim html As String = "<html><head><link rel='stylesheet' href='" & Server.MapPath("~/CSS/master.css") & "' type='text/css' /><style type=""text/css"">pre.WordWrap{height:auto !important;overflow: auto !important;overflow-x: auto !important; white-space: pre-wrap !important; white-space: -moz-pre-wrap !important; word-wrap: break-word !important; font-family:Arial,Helvetica,sans-serif !important;} .ItemHeader, .ItemStyle, .AltItemStyle {text-align: left;}</style></head><body><table id=""tblFormattedBizDoc"" border=""0"" style=""border-color:Black;border-width:1px;border-style:solid;width:100%;""><tr><td>" + GetHtml() + "</td></tr></table></body></html>"

                Dim objBizDocs As New OppBizDocs
                Dim strFileName As String = ""
                Dim strFilePhysicalLocation As String = ""

                Dim objHTMLToPDF As New HTMLToPDF
                strFileName = objHTMLToPDF.ConvertHTML2PDF(html, CCommon.ToLong(Session("DomainID")), numOrientation:=IIf(hdnOrientation.Value = "2", 2, 1), keepFooterAtBottom:=CCommon.ToBool(hdnKeepFooterBottom.Value))
                strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName

                Response.Clear()
                Response.ClearContent()
                Response.ContentType = "application/pdf"
                Response.AddHeader("content-disposition", "attachment; filename=" + IIf(lblBizDocIDValue.Text.Trim.Length > 0, lblBizDocIDValue.Text.Trim.Replace(" ", "_").Replace(",", " ") + ".pdf", strFileName.Replace(",", " ")))
                'Response.Write(Session("Attachements"))

                Response.WriteFile(strFilePhysicalLocation)

                Response.End()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub SendEmail(ByVal boolApproved As Boolean)
            Try
                Dim objSendEmail As New Email

                objSendEmail.DomainID = Session("DomainID")
                objSendEmail.TemplateCode = "#SYS#DOC_APPROVAL/DECLINED_NOTIFICATION"
                objSendEmail.ModuleID = 1
                objSendEmail.RecordIds = txtBizDocRecOwner.Text
                objSendEmail.AdditionalMergeFields.Add("DocumentApprovalStatus", IIf(boolApproved, "Approved", "Declined"))
                objSendEmail.AdditionalMergeFields.Add("DocumentApprovalComment", txtComment.Text.Trim)
                objSendEmail.AdditionalMergeFields.Add("DocumentName", lblBizDocIDValue.Text.Trim)
                objSendEmail.AdditionalMergeFields.Add("LoggedInUser", Session("ContactName"))
                objSendEmail.FromEmail = Session("UserEmail")
                objSendEmail.ToEmail = "##ContactFirstName## <##ContactEmail##>"
                objSendEmail.SendEmailTemplate()

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindBizDocsTemplate()
            Try
                Dim objOppBizDoc As New OppBizDocs
                objOppBizDoc.DomainID = Session("DomainID")
                objOppBizDoc.BizDocId = hdnBizDocId.Value
                objOppBizDoc.OppType = hdnOppType.Value

                Dim dtBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()

                ddlBizDocTemplate.DataSource = dtBizDocTemplate
                ddlBizDocTemplate.DataTextField = "vcTemplateName"
                ddlBizDocTemplate.DataValueField = "numBizDocTempID"
                ddlBizDocTemplate.DataBind()

                ddlBizDocTemplate.Items.Insert(0, New ListItem("--Select--", 0))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlBizDocTemplate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBizDocTemplate.SelectedIndexChanged
            Try
                objCommon = New CCommon
                objCommon.Mode = 21
                objCommon.UpdateRecordID = lngOppBizDocID
                objCommon.UpdateValueID = ddlBizDocTemplate.SelectedValue
                objCommon.UpdateSingleFieldValue()

                getBizDocDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgBizDocs_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles dgBizDocs.ItemDataBound
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                e.Item.Attributes.Add("style", "page-break-inside : avoid;")

                If e.Item.ItemType = ListItemType.Item Then
                    Select Case DirectCast(e.Item.DataItem, System.Data.DataRowView)("tintLevel")
                        Case 0
                            e.Item.CssClass = "ItemStyle Level0"
                        Case 1
                            e.Item.CssClass = "ItemStyle Level1"
                        Case 2
                            e.Item.CssClass = "ItemStyle Level2"
                    End Select
                ElseIf e.Item.ItemType = ListItemType.AlternatingItem Then
                    Select Case DirectCast(e.Item.DataItem, System.Data.DataRowView)("tintLevel")
                        Case 0
                            e.Item.CssClass = "AltItemStyle Level0"
                        Case 1
                            e.Item.CssClass = "AltItemStyle Level1"
                        Case 2
                            e.Item.CssClass = "AltItemStyle Level2"
                    End Select
                End If
            End If
        End Sub
    End Class
End Namespace
