﻿'Created By Anoop Jayaraj
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports System.Text.RegularExpressions
Imports BACRM.BusinessLogic.Accounting
Imports System.Web.Services
Imports System.Web.Script.Services

Namespace BACRM.UserInterface.Opportunities
    Partial Public Class frmPurchaseFulfillment
        Inherits BACRMPage

        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim strColumn As String
        Dim dtDynamicGridColumns As DataTable


#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                'CLEAR ALERT MESSAGE
                divMessage.Style.Add("display", "none")
                litMessage.Text = ""

                If GetQueryStringVal("SI") <> "" Then
                    txtOpptype.Text = GetQueryStringVal("SI")
                End If
                If GetQueryStringVal("SI1") <> "" Then
                    SI1 = GetQueryStringVal("SI1")
                Else : SI1 = 0
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    SI2 = GetQueryStringVal("SI2")
                Else : SI2 = 0
                End If
                If GetQueryStringVal("frm") <> "" Then
                    frm = ""
                    frm = GetQueryStringVal("frm")
                Else : frm = ""
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = ""
                    frm1 = GetQueryStringVal("frm1")
                Else : frm1 = ""
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    frm2 = ""
                    frm2 = GetQueryStringVal("frm2")
                Else : frm2 = ""
                End If
                GetUserRightsForPage(10, 16)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                    Response.Redirect("../admin/authentication.aspx?mesg=AC")
                Else
                    If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then btnSave.Visible = False
                End If
                If Not IsPostBack Then
                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                        txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                        txtCurrrentPage.Text = "1"
                        txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))
                        If Not ddlSearch.Items.FindByValue(CCommon.ToString(PersistTable(ddlSearch.ID))) Is Nothing Then
                            ddlSearch.ClearSelection()
                            ddlSearch.Items.FindByValue(CCommon.ToString(PersistTable(ddlSearch.ID))).Selected = True
                        End If
                        radcmbSearch.Text = CCommon.ToString(PersistTable("RadSearchText"))
                        radcmbSearch.SelectedValue = CCommon.ToString(PersistTable("RadSearchValue"))
                    End If

                    BindDatagrid()
                    calfrom.SelectedDate = DateTime.Now()

                    FocusSearch()
                Else
                    BindDatagrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub FocusSearch()
            Try
                Dim script As New System.Text.StringBuilder()
                script.Append("Sys.Application.add_load(function(){")
                script.Append("var combo2 = $find('" + radcmbSearch.ClientID + "');")
                script.Append("combo2.get_inputDomElement().focus();")
                script.Append("combo2.selectText(combo2.get_text().length, combo2.get_text().length); });")

                ScriptManager.RegisterStartupScript(Page, Page.GetType(), "comboCursor", script.ToString(), True)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub BindDatagrid(Optional ByVal CreateCol As Boolean = True)
            Try
                Dim dtPOFulfillment As DataTable
                Dim objOpportunity As New COpportunities
                objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                With objOpportunity
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")
                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()
                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0
                    .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    .CompFilter = radcmbSearch.Text
                    .FilterBy = CCommon.ToShort(ddlSearch.SelectedValue)
                    .OpportID = CCommon.ToLong(radcmbSearch.SelectedValue)
                End With

                If CCommon.ToLong(ddlVendorInvoice.SelectedValue) > 0 Then
                    objOpportunity.BizDocId = CCommon.ToLong(ddlVendorInvoice.SelectedValue)
                End If

                If txtSortColumn.Text <> "" Then
                    objOpportunity.columnName = txtSortColumn.Text
                    If txtSortOrder.Text = "D" Then
                        objOpportunity.columnSortOrder = "Desc"
                    Else
                        objOpportunity.columnSortOrder = "Asc"
                    End If
                Else
                    objOpportunity.columnName = "opp.bintCreatedDate"
                    objOpportunity.columnSortOrder = "Desc"
                End If

                Dim ds As DataSet = objOpportunity.GetPOFulfilmentItems

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                    dtPOFulfillment = ds.Tables(0)

                    Dim dtTableInfo As DataTable = ds.Tables(1)
                    dtDynamicGridColumns = dtTableInfo
                    dtDynamicGridColumns.Columns.Add("ColumnUniqueName", GetType(System.String), "'23~'+numFieldId+'~'+bitCustomField")

                    Dim htGridColumnSearch As New Hashtable

                    If txtGridColumnFilter.Text.Trim.Length > 0 Then
                        Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")
                        Dim strIDValue() As String

                        For i = 0 To strValues.Length - 1
                            strIDValue = strValues(i).Split(":")

                            htGridColumnSearch.Add(strIDValue(0), strIDValue(1))
                        Next
                    End If

                    If CreateCol = True Then
                        Dim bField As BoundField
                        Dim Tfield As TemplateField
                        gvSearch.Columns.Clear()

                        For Each drRow As DataRow In dtTableInfo.Rows
                            Tfield = New TemplateField
                            Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, drRow, 3, htGridColumnSearch, 135, objOpportunity.columnName, objOpportunity.columnSortOrder)
                            Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, drRow, 3, htGridColumnSearch, 135, objOpportunity.columnName, objOpportunity.columnSortOrder)
                            gvSearch.Columns.Add(Tfield)
                        Next

                        Dim dr As DataRow
                        dr = dtTableInfo.NewRow()
                        dr("vcAssociatedControlType") = "DeleteCheckBox"
                        dr("intColumnWidth") = "30"

                        Tfield = New TemplateField
                        Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, dr, 3, htGridColumnSearch, 135)
                        Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, dr, 3, htGridColumnSearch, 135)
                        gvSearch.Columns.Add(Tfield)
                    End If

                    bizPager.PageSize = Session("PagingRows")
                    bizPager.CurrentPageIndex = txtCurrrentPage.Text
                    bizPager.RecordCount = objOpportunity.TotalRecords

                    PersistTable.Clear()
                    PersistTable.Add(PersistKey.CurrentPage, IIf(dtPOFulfillment.Rows.Count > 0, txtCurrrentPage.Text, "1"))
                    PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())
                    PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
                    PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text.Trim())
                    PersistTable.Add(ddlSearch.ID, ddlSearch.SelectedValue)
                    PersistTable.Add("RadSearchText", radcmbSearch.Text)
                    PersistTable.Add("RadSearchValue", radcmbSearch.SelectedValue)
                    PersistTable.Save()

                    gvSearch.DataSource = dtPOFulfillment
                    gvSearch.DataBind()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Dim i As Integer = 2
        Private Sub gvSearch_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSearch.RowDataBound
            If e.Row.RowType = DataControlRowType.DataRow Then
                DirectCast(e.Row.FindControl("lblOppItemtCode"), Label).Text = CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numoppitemtCode"))
                DirectCast(e.Row.FindControl("lblOppID"), Label).Text = CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numOppID"))
                DirectCast(e.Row.FindControl("lblWarehouseItmsID"), Label).Text = CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numWarehouseItmsID"))
                DirectCast(e.Row.FindControl("lblQtyOrdered"), Label).Text = DataBinder.Eval(e.Row.DataItem, "numUnitHour")
                DirectCast(e.Row.FindControl("lblQtyReceived"), Label).Text = DataBinder.Eval(e.Row.DataItem, "numUnitHourReceived")
                DirectCast(e.Row.FindControl("lblExchangeRate"), Label).Text = DataBinder.Eval(e.Row.DataItem, "fltExchangeRate")
                DirectCast(e.Row.FindControl("lblCurrencyID"), Label).Text = DataBinder.Eval(e.Row.DataItem, "numCurrencyID")
                DirectCast(e.Row.FindControl("lblDivisionID"), Label).Text = DataBinder.Eval(e.Row.DataItem, "numDivisionID")
                DirectCast(e.Row.FindControl("lblItemIncomeAccount"), Label).Text = DataBinder.Eval(e.Row.DataItem, "itemIncomeAccount")
                DirectCast(e.Row.FindControl("lblItemInventoryAsset"), Label).Text = DataBinder.Eval(e.Row.DataItem, "itemInventoryAsset")
                DirectCast(e.Row.FindControl("lblItemCoGs"), Label).Text = DataBinder.Eval(e.Row.DataItem, "itemCoGs")
                DirectCast(e.Row.FindControl("lblDropShip"), Label).Text = DataBinder.Eval(e.Row.DataItem, "DropShip")
                DirectCast(e.Row.FindControl("lblCharItemType"), Label).Text = DataBinder.Eval(e.Row.DataItem, "charItemType")
                DirectCast(e.Row.FindControl("lblItemType"), Label).Text = DataBinder.Eval(e.Row.DataItem, "ItemType")
                DirectCast(e.Row.FindControl("lblProjectID"), Label).Text = DataBinder.Eval(e.Row.DataItem, "numProjectID")
                DirectCast(e.Row.FindControl("lblClassID"), Label).Text = DataBinder.Eval(e.Row.DataItem, "numClassID")
                DirectCast(e.Row.FindControl("lblItemCode"), Label).Text = DataBinder.Eval(e.Row.DataItem, "numItemCode")
                DirectCast(e.Row.FindControl("lblPrice"), Label).Text = DataBinder.Eval(e.Row.DataItem, "monPrice")
                DirectCast(e.Row.FindControl("lblItemName"), Label).Text = DataBinder.Eval(e.Row.DataItem, "vcItemName")
                DirectCast(e.Row.FindControl("lblPOppName"), Label).Text = DataBinder.Eval(e.Row.DataItem, "vcPOppName")
                DirectCast(e.Row.FindControl("lblVendor"), Label).Text = DataBinder.Eval(e.Row.DataItem, "vcCompanyName")
                DirectCast(e.Row.FindControl("lblPPVariance"), Label).Text = DataBinder.Eval(e.Row.DataItem, "bitPPVariance")
                DirectCast(e.Row.FindControl("lblRemaining"), Label).Text = CCommon.ToDouble(DataBinder.Eval(e.Row.DataItem, "numUnitHour")) - CCommon.ToDouble(DataBinder.Eval(e.Row.DataItem, "numUnitHourReceived"))
                DirectCast(e.Row.FindControl("hfbitLotNo"), HiddenField).Value = DataBinder.Eval(e.Row.DataItem, "bitLotNo")
                DirectCast(e.Row.FindControl("hfbitSerialized"), HiddenField).Value = DataBinder.Eval(e.Row.DataItem, "bitSerialized")
                DirectCast(e.Row.FindControl("hdnIsStockTransfer"), HiddenField).Value = CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "bitStockTransfer"))
                
                Dim chk As CheckBox
                chk = e.Row.FindControl("chkSelect")
                chk.Attributes.Add("onclick", "return CheckAllSameOpp('" & CCommon.ToLong(CType(e.Row.FindControl("lblOppID"), Label).Text) & "','" & chk.ClientID & "');")

                If Not e.Row.FindControl("txtQtyFulfilled") Is Nothing Then
                    Dim txtQtyRec As TextBox = e.Row.FindControl("txtQtyFulfilled")
                    txtQtyRec.Attributes.Add("onkeyup", "keyPressed(this.id,event);return validate('" & i.ToString & "');")
                End If

                If Not e.Row.FindControl("txLot") Is Nothing Then
                    If DataBinder.Eval(e.Row.DataItem, "bitSerialized") = True Or DataBinder.Eval(e.Row.DataItem, "bitLotNo") = True Then
                        CType(e.Row.FindControl("txLot"), TextBox).Visible = True
                    Else
                        CType(e.Row.FindControl("txLot"), TextBox).Visible = False
                    End If
                End If

                If Not e.Row.FindControl("radWarehouse") Is Nothing AndAlso CCommon.ToString(DataBinder.Eval(e.Row.DataItem, "charItemType")) = "P" Then
                    Dim radWarehouse As Telerik.Web.UI.RadComboBox
                    radWarehouse = e.Row.FindControl("radWarehouse")

                    Dim objCommon As New CCommon
                    Dim dt As DataTable = objCommon.GetWarehouseAttrBasedItem(CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numItemCode")))

                    radWarehouse.DataSource = dt.Select("numWareHouseID = " & CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numWarehouseID"))).CopyToDataTable()
                    radWarehouse.DataBind()

                    If Not radWarehouse.Items.FindItemByValue(DataBinder.Eval(e.Row.DataItem, "numWarehouseItmsID")) Is Nothing Then
                        radWarehouse.Items.FindItemByValue(DataBinder.Eval(e.Row.DataItem, "numWarehouseItmsID")).Selected = True
                    End If
                Else
                    If Not e.Row.FindControl("radWarehouse") Is Nothing Then
                        e.Row.FindControl("radWarehouse").Visible = False
                    End If
                End If

                'KEEP AT LAST
                If e.Row.RowIndex = 0 Then
                    hdnSelectedOppID.Value = CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numOppID"))
                    chk.Checked = True
                Else
                    chk.Checked = False
                End If
            End If
        End Sub
        Private Sub gvSearch_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvSearch.Sorting
            Try
                strColumn = e.SortExpression.ToString()
                If txtSortColumn.Text <> strColumn Then
                    txtSortColumn.Text = strColumn
                    txtSortOrder.Text = "A"
                Else
                    If txtSortOrder.Text = "D" Then
                        txtSortOrder.Text = "A"
                    Else : txtSortOrder.Text = "D"
                    End If
                End If
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)

            End Try
        End Sub

        Private Sub btnSaveMain_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveMain.Click
            Dim checkedValue As Integer = 0
            Dim dtPOFulfillmentSetting As DataTable
            Try
                If ChartOfAccounting.GetDefaultAccount("PC", Session("DomainID")) = 0 Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Purchase Clearing account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                    Exit Sub
                End If

                If ChartOfAccounting.GetDefaultAccount("PV", Session("DomainID")) = 0 Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('Please Set Default Purchase Price Variance account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                    Exit Sub
                End If

                Dim isQtyToReceieveIsMoreThanOrdered As Boolean = False
                Dim isWarehouseLocationNeeded As Boolean = False

                Dim strQtyIdMoreThanOrdered As String = "Following items have qty to receieve more than ordered qty:"
                Dim strGlobalWarehouseLocation As String = "Selecte warehouse internal location for following item(s):"
                Dim isMultipleOrderSelected As Boolean = False
                Dim tempOppID As Long = 0
                For Each dgItem As GridViewRow In gvSearch.Rows
                    If CType(dgItem.FindControl("chkSelect"), CheckBox).Checked Then
                        If tempOppID = 0 Then
                            tempOppID = CCommon.ToLong(CType(dgItem.FindControl("lblOppID"), Label).Text)
                        ElseIf tempOppID <> CCommon.ToLong(CType(dgItem.FindControl("lblOppID"), Label).Text) Then
                            isMultipleOrderSelected = True
                        End If


                        Dim lngRemainingQty As Long = CCommon.ToDouble(CType(dgItem.FindControl("lblRemaining"), Label).Text)
                        Dim qtyToReceieve As Double

                        If Not dgItem.FindControl("txtQtyFulfilled") Is Nothing Then
                            qtyToReceieve = CCommon.ToDouble(CType(dgItem.FindControl("txtQtyFulfilled"), TextBox).Text)
                        Else
                            qtyToReceieve = CCommon.ToDouble(CType(dgItem.FindControl("lblRemaining"), Label).Text)
                        End If

                        Dim pOppName As String = CCommon.ToString(CType(dgItem.FindControl("lblPOppName"), Label).Text)
                        Dim itemName As String = CCommon.ToString(CType(dgItem.FindControl("lblItemName"), Label).Text)

                        If qtyToReceieve > lngRemainingQty Then
                            isQtyToReceieveIsMoreThanOrdered = True
                            strQtyIdMoreThanOrdered += "<br/>" + pOppName + " :: " + itemName
                        End If

                        If Not dgItem.FindControl("radWareHouse") Is Nothing AndAlso dgItem.FindControl("radWareHouse").Visible Then
                            Dim radWarehouse As Telerik.Web.UI.RadComboBox = CType(dgItem.FindControl("radWareHouse"), Telerik.Web.UI.RadComboBox)
                            If CCommon.ToLong(radWarehouse.SelectedValue) <= 0 Then
                                isWarehouseLocationNeeded = True
                                strGlobalWarehouseLocation += "<br/>" + pOppName + " :: " + itemName
                            End If
                        End If
                    End If
                Next

                If isMultipleOrderSelected Then
                    DisplayError("Yor can receive item(s) from single purchase order only.")
                    Exit Sub
                End If

                If isWarehouseLocationNeeded Then
                    DisplayError(strGlobalWarehouseLocation)
                    Exit Sub
                End If


                If isQtyToReceieveIsMoreThanOrdered Then
                    DisplayError(strQtyIdMoreThanOrdered)
                    Exit Sub
                End If

                Dim intQtyReceived, intOldQtyReceived As Double

                Dim bitShowSerialLotMessage As Boolean = False
                Dim serialLotRequired As String = "Following items could not be saved (your option is to provide respective no of Lot/Serial #s):"

                Dim oppID As Long
                Dim isStockTransfer As Boolean
                Dim isSerilizedItem As Boolean
                Dim isLotItem As Boolean
                Dim objOpp As New MOpportunity
                objOpp.DomainID = Session("DomainID")
                objOpp.UserCntID = Session("UserContactID")
                objOpp.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                If (ddlSearch.SelectedValue = "3" Or ddlSearch.SelectedValue = "4") AndAlso CCommon.ToLong(ddlVendorInvoice.SelectedValue) > 0 Then
                    'Validate if no qty in received without selecting vendor invoice
                    Dim objBizDoc As New OppBizDocs
                    objBizDoc.OppBizDocId = CCommon.ToLong(ddlVendorInvoice.SelectedValue)
                    If Not objBizDoc.CanReceiveVendorInvoice() Then
                        ShowMessage("This order has already received part of its quantity without a vendor invoice. You’re only option is to finish receiving the balance of the order this way.")
                        ddlVendorInvoice.Items.Clear()
                        ddlVendorInvoice.Items.Insert(0, New ListItem("-- Select One --", "0"))

                        BindDatagrid()
                        Exit Sub
                    End If
                End If

                Using transScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    Dim objOpportunity As New COpportunities

                    For Each dgItem As GridViewRow In gvSearch.Rows
                        If CType(dgItem.FindControl("chkSelect"), CheckBox).Checked Then
                            oppID = CType(dgItem.FindControl("lblOppID"), Label).Text
                            isStockTransfer = CCommon.ToBool(CType(dgItem.FindControl("hdnIsStockTransfer"), HiddenField).Value)
                            isSerilizedItem = CCommon.ToBool(CType(dgItem.FindControl("hfbitSerialized"), HiddenField).Value)
                            isLotItem = CCommon.ToBool(CType(dgItem.FindControl("hfbitLotNo"), HiddenField).Value)

                            If Not dgItem.FindControl("txtQtyFulfilled") Is Nothing Then
                                intQtyReceived = CCommon.ToDouble(CType(dgItem.FindControl("txtQtyFulfilled"), TextBox).Text)
                            Else
                                intQtyReceived = CCommon.ToDouble(CType(dgItem.FindControl("lblRemaining"), Label).Text)
                            End If
                            intOldQtyReceived = CCommon.ToDouble(CType(dgItem.FindControl("lblQtyReceived"), Label).Text)
                            Dim pOppName As String = CCommon.ToString(CType(dgItem.FindControl("lblPOppName"), Label).Text)
                            Dim itemName As String = CCommon.ToString(CType(dgItem.FindControl("lblItemName"), Label).Text)

                            checkedValue = 1

                            If isSerilizedItem Or isLotItem Then
                                If Not dgItem.FindControl("txLot") Is Nothing AndAlso (CType(dgItem.FindControl("txLot"), TextBox).Visible) Then
                                    Dim txLot As TextBox = CType(dgItem.FindControl("txLot"), TextBox)
                                    Dim str As String()

                                    Dim i As Integer
                                    Dim iTotal As Integer = 0

                                    If CType(dgItem.FindControl("hfbitLotNo"), HiddenField).Value = True Then
                                        Dim strLot As String()
                                        strLot = txLot.Text.Split(",")

                                        Dim LotName As String


                                        For i = 0 To strLot.Length - 1
                                            Dim pattern As String = "(?<LotName>(\w*))\s*\((?<LotQty>(\d*))\)"
                                            Dim matches As MatchCollection = Regex.Matches(strLot(i), pattern)

                                            If (matches.Count > 0) Then
                                                iTotal += matches(0).Groups("LotQty").ToString()
                                                LotName = matches(0).Groups("LotName").ToString()
                                            Else
                                                iTotal += 1
                                            End If
                                        Next
                                    Else
                                        iTotal = txLot.Text.Split(",").Length
                                    End If

                                    If (intQtyReceived = iTotal And txLot.Text.Trim.Length > 0) Then
                                        Dim dr As DataRow
                                        Dim ds As New DataSet

                                        Dim dtTable As New DataTable
                                        dtTable.TableName = "SerializedItems"

                                        dtTable.Columns.Add("vcSerialNo")
                                        dtTable.Columns.Add("numWareHouseItemID")
                                        dtTable.Columns.Add("numOppId")
                                        dtTable.Columns.Add("numOppItemId")
                                        dtTable.Columns.Add("numQty")

                                        str = txLot.Text.Split(",")

                                        For i = 0 To str.Length - 1
                                            dr = dtTable.NewRow

                                            If CType(dgItem.FindControl("hfbitLotNo"), HiddenField).Value = True Then

                                                Dim pattern As String = "(?<LotName>(\w*))\s*\((?<LotQty>(\d*))\)"
                                                Dim matches As MatchCollection = Regex.Matches(str(i), pattern)

                                                If (matches.Count > 0) Then
                                                    'StartNo = matches(0).Groups("StartNo").ToString()
                                                    dr("numQty") = matches(0).Groups("LotQty").ToString()
                                                    dr("vcSerialNo") = matches(0).Groups("LotName").ToString()
                                                Else
                                                    dr("vcSerialNo") = str(i)
                                                    dr("numQty") = 1
                                                End If
                                            Else
                                                dr("vcSerialNo") = str(i)
                                                dr("numQty") = 1
                                            End If

                                            If Not dgItem.FindControl("radWareHouse") Is Nothing AndAlso dgItem.FindControl("radWareHouse").Visible Then
                                                dr("numWareHouseItemID") = CCommon.ToLong(CType(dgItem.FindControl("radWareHouse"), Telerik.Web.UI.RadComboBox).SelectedValue)
                                            Else
                                                dr("numWareHouseItemID") = CCommon.ToLong(CType(dgItem.FindControl("lblWarehouseItmsID"), Label).Text)
                                            End If

                                            dr("numOppId") = CCommon.ToLong(CType(dgItem.FindControl("lblOppID"), Label).Text)
                                            dr("numOppItemId") = CCommon.ToLong(CType(dgItem.FindControl("lblOppItemtCode"), Label).Text)

                                            dtTable.Rows.Add(dr)
                                        Next

                                        ds.Tables.Add(dtTable)
                                        objOpp.strItems = ds.GetXml
                                        Try
                                            objOpp.ManageOppItemSerialNo(Nothing)
                                        Catch ex As Exception
                                            If ex.Message.Contains("DUPLICATE_SERIAL_NO") Then
                                                serialLotRequired += "<br/>" + pOppName + " :: " + itemName + "(Same serial(s) already exists for item.)"
                                                bitShowSerialLotMessage = True
                                                Continue For
                                            Else
                                                Throw
                                            End If
                                        End Try
                                    Else
                                        serialLotRequired += "<br/>" + pOppName + " :: " + itemName
                                        bitShowSerialLotMessage = True
                                        Continue For
                                    End If
                                Else
                                    serialLotRequired += "<br/>" + pOppName + " :: " + itemName
                                    bitShowSerialLotMessage = True
                                    Continue For
                                End If
                            End If

                            If intQtyReceived > 0 Then
                                objOpportunity.OppItemCode = CCommon.ToLong(CType(dgItem.FindControl("lblOppItemtCode"), Label).Text)
                                objOpportunity.UnitHour = intQtyReceived
                                objOpportunity.UserCntID = Session("UserContactID")
                                objOpportunity.dtItemReceivedDate = CDate(calfrom.SelectedDate & " 12:00:00")

                                If Not dgItem.FindControl("radWareHouse") Is Nothing AndAlso dgItem.FindControl("radWareHouse").Visible Then
                                    objOpportunity.WarehouseItmsID = CCommon.ToLong(CType(dgItem.FindControl("radWareHouse"), Telerik.Web.UI.RadComboBox).SelectedValue)
                                Else
                                    objOpportunity.WarehouseItmsID = CCommon.ToLong(CType(dgItem.FindControl("lblWarehouseItmsID"), Label).Text)
                                End If

                                If (ddlSearch.SelectedValue = "3" Or ddlSearch.SelectedValue = "4") AndAlso CCommon.ToLong(ddlVendorInvoice.SelectedValue) > 0 Then
                                    objOpportunity.BizDocId = CCommon.ToLong(ddlVendorInvoice.SelectedValue)
                                End If

                                objOpportunity.UpdatePOItemsFulfillment(Nothing)
                            End If

                            If CCommon.ToBool(CType(dgItem.FindControl("lblPPVariance"), Label).Text) Then
                                Dim dtMain As New DataTable
                                CCommon.AddColumnsToDataTable(dtMain, "numDivisionID,vcPOppName,numoppitemtCode,numItemCode,vcItemName,monPrice,numUnitReceived,numCurrencyID,fltExchangeRate,charItemType,ItemType,DropShip,numProjectID,numClassID,ItemInventoryAsset,ItemCoGs,itemIncomeAccount")
                                Dim dr As DataRow
                                dr = dtMain.NewRow

                                dr("numDivisionID") = CCommon.ToLong(CType(dgItem.FindControl("lblDivisionID"), Label).Text)
                                dr("vcPOppName") = CCommon.ToString(CType(dgItem.FindControl("lblPOppName"), Label).Text)
                                dr("numoppitemtCode") = CCommon.ToLong(CType(dgItem.FindControl("lblOppItemtCode"), Label).Text)
                                dr("numItemCode") = CCommon.ToLong(CType(dgItem.FindControl("lblItemCode"), Label).Text)
                                dr("vcItemName") = CCommon.ToLong(CType(dgItem.FindControl("lblItemName"), Label).Text)
                                dr("monPrice") = CCommon.ToDecimal(CType(dgItem.FindControl("lblPrice"), Label).Text)
                                dr("numUnitReceived") = intQtyReceived
                                dr("numCurrencyID") = CCommon.ToLong(CType(dgItem.FindControl("lblCurrencyID"), Label).Text)
                                dr("fltExchangeRate") = CCommon.ToDecimal(CType(dgItem.FindControl("lblExchangeRate"), Label).Text)
                                dr("charItemType") = CCommon.ToString(CType(dgItem.FindControl("lblCharItemType"), Label).Text)
                                dr("ItemType") = CCommon.ToString(CType(dgItem.FindControl("lblItemType"), Label).Text)
                                dr("DropShip") = CCommon.ToBool(CType(dgItem.FindControl("lblDropShip"), Label).Text)
                                dr("numProjectID") = CCommon.ToLong(CType(dgItem.FindControl("lblProjectID"), Label).Text)
                                dr("numClassID") = CCommon.ToLong(CType(dgItem.FindControl("lblClassID"), Label).Text)
                                dr("ItemInventoryAsset") = CCommon.ToLong(CType(dgItem.FindControl("lblItemInventoryAsset"), Label).Text)
                                dr("ItemCoGs") = CCommon.ToLong(CType(dgItem.FindControl("lblItemCoGs"), Label).Text)
                                dr("itemIncomeAccount") = CCommon.ToLong(CType(dgItem.FindControl("lblItemIncomeAccount"), Label).Text)

                                dtMain.Rows.Add(dr)

                                Dim JournalId As Long = 0

                                Dim objOppBizDocs As New OppBizDocs
                                objOppBizDocs.OppId = oppID
                                objOppBizDocs.DomainID = Session("DomainID")
                                objOppBizDocs.UserCntID = Session("UserContactID")

                                JournalId = objOppBizDocs.SaveDataToHeader(dr("numUnitReceived") * dr("monPrice") * dtMain(0)("fltExchangeRate"), CDate(calfrom.SelectedDate & " 12:00:00"), Description:=CCommon.ToString(CType(dgItem.FindControl("lblPOppName"), Label).Text))

                                Dim objJournalEntries As New JournalEntry
                                objJournalEntries.SaveJournalEntriesPurchaseClearing(oppID, Session("DomainID"), dtMain, JournalId)
                                'End If
                            End If
                        End If
                    Next

                    ' Commit the transaction.
                    transScope.Complete()
                End Using

                If bitShowSerialLotMessage Then
                    DisplayError(serialLotRequired)
                End If

                If Not isStockTransfer Then
                    Dim objCommon As New CCommon
                    objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                    dtPOFulfillmentSetting = objCommon.GetPOFulfillmentSettings()


                    If (ddlSearch.SelectedValue = "3" Or ddlSearch.SelectedValue = "4") AndAlso CCommon.ToLong(ddlVendorInvoice.SelectedValue) > 0 Then
                        'IF ALL ITEMS OF VENDOR INVOICE RECIEVED, CREATE BILL AGAINST VENDOR INVOICE IF NO OTHER BILL IS CREATED WHICH USES VENDOR INVOICE QTY
                        Dim bizDocTemplate As Long = 0

                        If Not dtPOFulfillmentSetting Is Nothing AndAlso dtPOFulfillmentSetting.Rows.Count > 0 Then
                            bizDocTemplate = CCommon.ToLong(dtPOFulfillmentSetting.Rows(0)("numBizDocID"))
                        End If

                        Dim objBizDoc As New OppBizDocs
                        objBizDoc.OppBizDocId = CCommon.ToLong(ddlVendorInvoice.SelectedValue)
                        If objBizDoc.CanBillAgainstVendorInvoice() Then
                            CreateBill(oppID, bizDocTemplate, True)
                        End If
                    End If

                    If Not dtPOFulfillmentSetting Is Nothing AndAlso dtPOFulfillmentSetting.Rows.Count > 0 Then
                        objOpp.OpportunityId = oppID
                        Dim dsInvoiceBilled As DataSet = objOpp.CheckOrderedAndInvoicedOrBilledQty()

                        Dim bitOrderedAndShippedBilledQtyNotSame As Boolean = CCommon.ToBool(dsInvoiceBilled.Tables(0).Select("ID=2")(0)("bitTrue"))
                        Dim bitOrderesAndReceivedQtyNotSame As Boolean = CCommon.ToBool(dsInvoiceBilled.Tables(0).Select("ID=6")(0)("bitTrue"))

                        If Not bitOrderesAndReceivedQtyNotSame Then
                            If bitOrderedAndShippedBilledQtyNotSame AndAlso CCommon.ToLong(dtPOFulfillmentSetting.Rows(0)("numBizDocID")) > 0 Then
                                CreateBill(oppID, CCommon.ToLong(dtPOFulfillmentSetting.Rows(0)("numBizDocID")), False)
                            End If

                            If CCommon.ToBool(dtPOFulfillmentSetting.Rows(0)("bitAllQtyOrderStatus")) AndAlso CCommon.ToBool(dtPOFulfillmentSetting.Rows(0)("bitClosePO")) Then
                                objOpp.ShippingOrReceived()
                            End If
                        End If

                        If CCommon.ToBool(dtPOFulfillmentSetting.Rows(0)("bitPartialQtyOrderStatus")) AndAlso bitOrderesAndReceivedQtyNotSame AndAlso CCommon.ToLong(dtPOFulfillmentSetting.Rows(0)("numPartialReceiveOrderStatus")) > 0 Then
                            objOpp.OrderStatus = CCommon.ToLong(dtPOFulfillmentSetting.Rows(0)("numPartialReceiveOrderStatus"))
                            objOpp.ChangeOrderStatus()
                        ElseIf CCommon.ToBool(dtPOFulfillmentSetting.Rows(0)("bitAllQtyOrderStatus")) AndAlso Not bitOrderesAndReceivedQtyNotSame AndAlso CCommon.ToLong(dtPOFulfillmentSetting.Rows(0)("numFullReceiveOrderStatus")) > 0 Then
                            objOpp.OrderStatus = CCommon.ToLong(dtPOFulfillmentSetting.Rows(0)("numFullReceiveOrderStatus"))
                            objOpp.ChangeOrderStatus()
                        End If
                    End If
                End If

                radcmbSearch.Text = ""
                radcmbSearch.ClearSelection()
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub CreateBill(ByVal lngOppId As Long, ByVal bizdocTemplateID As Long, ByVal isVendorInvoiceBill As Boolean)
            Try
                Dim OppBizDocID, lngDivId, JournalId As Long
                Dim lngCntID As Long = 0

                Using objTransaction As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})

                    If CCommon.ToBool(Session("IsMinUnitPriceRule")) Then
                        If CheckIfApprovalRequired(lngOppId) Then
                            ShowMessage("Not able to create bill because approval of unit price(s) is required.")
                            Exit Sub
                        End If
                    End If

                    Dim dtDetails, dtBillingTerms As DataTable

                    Dim objPageLayout As New CPageLayout
                    objPageLayout.OpportunityId = lngOppId
                    objPageLayout.DomainID = Session("DomainID")
                    dtDetails = objPageLayout.OpportunityDetails.Tables(0)
                    lngDivId = dtDetails.Rows(0).Item("numDivisionID")
                    lngCntID = dtDetails.Rows(0).Item("numContactID")


                    Dim objOppBizDocs As New OppBizDocs
                    objOppBizDocs.UserCntID = Session("UserContactID")
                    objOppBizDocs.DomainID = Session("DomainID")

                    If objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), lngDivId) = 0 Then
                        ShowMessage("Not able to create bill. Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip""")
                        Exit Sub
                    End If

                    If ChartOfAccounting.GetDefaultAccount("CG", Session("DomainID")) = 0 Then
                        ShowMessage("Not able to create bill. Please Set Default COGs account from ""Administration->Global Settings->Accounting->Default Accounts""")
                        Exit Sub
                    End If

                    If ChartOfAccounting.GetDefaultAccount("PC", Session("DomainID")) = 0 Then
                        ShowMessage("Not able to create bill. Please Set Default Purchase Clearing account from ""Administration->Global Settings->Accounting->Default Accounts""")
                        Exit Sub
                    End If

                    If ChartOfAccounting.GetDefaultAccount("PV", Session("DomainID")) = 0 Then
                        ShowMessage("Not able to create bill. Please Set Default Purchase Price Variance account from ""Administration->Global Settings->Accounting->Default Accounts""")
                        Exit Sub
                    End If

                    objOppBizDocs.OppId = lngOppId
                    objOppBizDocs.OppType = 2
                    objOppBizDocs.FromDate = DateTime.UtcNow
                    objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")


                    'SETS BIZDOC ID
                    '--------------
                    Dim lintAuthorizativeBizDocsId As Long
                    lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()
                    objOppBizDocs.BizDocId = lintAuthorizativeBizDocsId
                    '--------------

                    If isVendorInvoiceBill Then
                        objOppBizDocs.numSourceBizDocId = CCommon.ToLong(ddlVendorInvoice.SelectedValue)
                        objOppBizDocs.FromOppBizDocsId = CCommon.ToLong(ddlVendorInvoice.SelectedValue)
                    End If

                    'SETS BIZDOC TEMPLETE NEEDS TO BE USED
                    '-------------------------------------
                    If bizdocTemplateID = 0 Then
                        objOppBizDocs.byteMode = 1
                        Dim dtDefaultBizDocTemplate As DataTable = objOppBizDocs.GetBizDocTemplateList()
                        objOppBizDocs.BizDocTemplateID = CCommon.ToLong(dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID"))
                    Else
                        objOppBizDocs.BizDocTemplateID = bizdocTemplateID
                    End If
                    '-------------------------------------

                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.Mode = 33
                    objCommon.Str = lintAuthorizativeBizDocsId 'DEFUALT BIZ DOC ID FOR BILL
                    objOppBizDocs.SequenceId = objCommon.GetSingleFieldValue()

                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.Mode = 34
                    objCommon.Str = lngOppId
                    objOppBizDocs.RefOrderNo = objCommon.GetSingleFieldValue()
                    objOppBizDocs.bitPartialShipment = True

                    OppBizDocID = objOppBizDocs.SaveBizDoc()
                    If OppBizDocID = 0 Then
                        ShowMessage("Not able to create bill. A BizDoc by the same name is already created. Please select another BizDoc !")
                        Exit Sub
                    End If

                    'CREATE JOURNALS
                    '---------------
                    Dim ds As New DataSet
                    Dim dtOppBiDocItems As DataTable

                    objOppBizDocs.OppBizDocId = OppBizDocID
                    ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting

                    dtOppBiDocItems = ds.Tables(0)

                    Dim objCalculateDealAmount As New CalculateDealAmount
                    objCalculateDealAmount.CalculateDealAmount(lngOppId, OppBizDocID, 2, Session("DomainID"), dtOppBiDocItems)

                    JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, objOppBizDocs.FromDate, Description:=ds.Tables(1).Rows(0).Item("vcBizDocID"))

                    Dim objJournalEntries As New JournalEntry
                    If CCommon.ToBool(ds.Tables(1).Rows(0).Item("bitPPVariance")) Then
                        objJournalEntries.SaveJournalEntriesPurchaseVariance(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), ds.Tables(1).Rows(0).Item("fltExchangeRateBizDoc"), vcBaseCurrency:=ds.Tables(1).Rows(0).Item("vcBaseCurrency"), vcForeignCurrency:=ds.Tables(1).Rows(0).Item("vcForeignCurrency"))
                    Else
                        objJournalEntries.SaveJournalEntriesPurchase(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"))
                    End If
                    '---------------

                    '------ SEND BIZDOC ALERTS - NOTIFY RECORD OWNERS, THEIR SUPERVISORS, AND YOUR TRADING PARTNERS WHEN A BIZDOC IS CREATED, MODIFIED, OR APPROVED.
                    Dim objAlert As New BACRM.BusinessLogic.Alerts.CAlerts
                    objAlert.SendBizDocAlerts(lngOppId, OppBizDocID, lintAuthorizativeBizDocsId, Session("DomainID"), BACRM.BusinessLogic.Alerts.CAlerts.enmBizDoc.IsCreated)

                    Dim objAutomatonRule As New BACRM.BusinessLogic.Admin.AutomatonRule
                    objAutomatonRule.ExecuteAutomationRule(49, OppBizDocID, 1)

                    objTransaction.Complete()
                End Using

                ''Added By Sachin Sadhu||Date:1stMay2014
                ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                ''          Using Change tracking
                Dim objWfA As New BACRM.BusinessLogic.Workflow.Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = OppBizDocID
                objWfA.SaveWFBizDocQueue()
                'end of code
            Catch ex As Exception
                If ex.Message = "NOT_ALLOWED" Then
                    ShowMessage("To split order items multiple bizdocs you must create all bizdocs with ""partial fulfilment"" checked!")
                ElseIf ex.Message = "NOT_ALLOWED_FulfillmentOrder" Then
                    ShowMessage("Only a Sales Order can create a Fulfillment Order")
                ElseIf ex.Message = "FY_CLOSED" Then
                    ShowMessage("This transaction can not be posted,Because transactions date belongs to closed financial year.")
                ElseIf ex.Message = "AlreadyInvoice" Then
                    ShowMessage("You can’t create a Invoice against a Fulfillment Order that already contains a Invoice")
                ElseIf ex.Message = "AlreadyPackingSlip" Then
                    ShowMessage("You can’t create a Packing-Slip against a Fulfillment Order that already contains a Packing-Slip")
                ElseIf ex.Message = "AlreadyInvoice_NOT_ALLOWED_FulfillmentOrder" Then
                    ShowMessage("You can’t create a fulfillment order against a sales order that already contains an Invoice or Packing-Slip")
                ElseIf ex.Message = "AlreadyFulfillmentOrder" Then
                    ShowMessage("Manually adding Invoices or Packing Slips to a Sales Order is not allowed after a Fulfillment Order (FO) is added. Your options are to edit the FO status to trigger creation of Invoices and/or Packing Slips, Delete the Fulfillment Order, or Create Invoices and/or Packing Slips from the Sales Fulfillment section")
                Else
                    Throw
                End If
            End Try
        End Sub

        Private Function CheckIfApprovalRequired(ByVal lngOppId As Long) As Boolean
            Try
                Dim objOpportunity As New MOpportunity
                objOpportunity.OpportunityId = lngOppId
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.UserCntID = Session("UserContactID")
                objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                Dim dsTemp As DataSet = objOpportunity.GetOrderItemsProductServiceGrid

                If Not dsTemp Is Nothing AndAlso dsTemp.Tables.Count > 0 AndAlso dsTemp.Tables(0).Rows.Count > 0 Then
                    Dim results = From myRow In dsTemp.Tables(0).AsEnumerable()
                                  Where CCommon.ToBool(myRow.Field(Of Boolean)("bitItemPriceApprovalRequired")) = True
                                  Select myRow
                    If results.Count > 0 Then
                        Return True
                    Else
                        Return False
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub ShowMessage(ByVal message As String)
            Try
                divMessage.Style.Add("display", "")
                litMessage.Text = message
                divMessage.Focus()
            Catch ex As Exception
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        'Added by :Sachin Sadhu||Date:26thJuly2014
        'Purpose   : to save Order Status
        <WebMethod()> _
<ScriptMethod()> _
        Public Shared Sub UpdateOrderStatus(numOrderStatus As String, OrderID As String)
            Dim objOpp As New MOpportunity
            Dim objTransaction As Npgsql.NpgsqlTransaction
            Try
                Dim strOrderID As String() = OrderID.Split(",")
                For k = 0 To strOrderID.Length - 1
                    If strOrderID(k).Length > 0 Then
                        objOpp.OpportunityId = CCommon.ToLong(strOrderID(k))
                        objOpp.UserCntID = HttpContext.Current.Session("UserContactID")
                        objOpp.DomainID = HttpContext.Current.Session("DomainID")
                        objOpp.OrderStatus = CCommon.ToLong(numOrderStatus)
                        objOpp.UpdateOrderStatus()
                    End If


                Next

            Catch ex As Exception
                objOpp.RollbackTransaction(objTransaction)
                ExceptionModule.ExceptionPublish(ex, HttpContext.Current.Session("DomainID"), HttpContext.Current.Session("UserContactID"), HttpContext.Current.Request)
                'DisplayError(ex.Message)
            End Try

        End Sub
        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                BindDatagrid()
                'txtCurrrentPage_TextChanged(Nothing, Nothing)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub

        Protected Sub radcmbSearch_SelectedIndexChanged(sender As Object, e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs)
            Try
                If ddlSearch.SelectedValue = "3" Or ddlSearch.SelectedValue = "4" Then
                    ddlVendorInvoice.Items.Clear()

                    Dim objOppBizDocs As New OppBizDocs
                    objOppBizDocs.DomainID = CCommon.ToLong(Session("DomainID"))
                    objOppBizDocs.OppId = CCommon.ToLong(radcmbSearch.SelectedValue)
                    objOppBizDocs.tintMode = CCommon.ToShort(ddlSearch.SelectedValue)
                    Dim dtVendorInvoices As DataTable = objOppBizDocs.GetVendorInvoices()

                    If Not dtVendorInvoices Is Nothing AndAlso dtVendorInvoices.Rows.Count > 0 Then
                        ddlVendorInvoice.DataSource = dtVendorInvoices
                        ddlVendorInvoice.DataTextField = "vcBizDocID"
                        ddlVendorInvoice.DataValueField = "numOppBizDocsId"
                        ddlVendorInvoice.DataBind()
                    End If

                    ddlVendorInvoice.Items.Insert(0, New ListItem("-- Select One --", "0"))
                    divVendorInvoice.Visible = True
                Else
                    ddlVendorInvoice.Items.Clear()
                    divVendorInvoice.Visible = False
                End If

                txtCurrrentPage.Text = 1
                BindDatagrid()

                FocusSearch()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub radcmbSearch_ItemsRequested(sender As Object, e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs)
            Try
                If Trim(e.Text).Length > 0 Then
                    Dim itemsPerRequest As Integer = 10
                    Dim itemOffset As Integer = e.NumberOfItems
                    Dim endOffset As Integer = itemOffset + itemsPerRequest

                    Dim objOpp As New COpportunities
                    objOpp.DomainID = Session("DomainID")
                    objOpp.byteMode = CCommon.ToShort(e.Context("searchType"))
                    objOpp.CurrentPage = e.NumberOfItems
                    objOpp.PageSize = endOffset
                    objOpp.SearchText = Trim(e.Text)
                    Dim dt As DataTable = objOpp.GetPurchaseOrderForFulfillment()

                    Try
                        If endOffset > objOpp.TotalRecords Then
                            endOffset = objOpp.TotalRecords
                        End If

                        For Each dr As DataRow In dt.Rows
                            radcmbSearch.Items.Add(New Telerik.Web.UI.RadComboBoxItem(dr("RecordValue").ToString(), dr("RecordID").ToString()))
                        Next

                        If dt.Rows.Count > 0 Then
                            e.Message = [String].Format("Items <b>1</b>-<b>{0}</b> out of <b>{1}</b>", endOffset.ToString(), objOpp.TotalRecords.ToString())
                        Else
                            e.Message = "No matches"
                        End If
                    Catch
                        e.Message = "No matches"
                    End Try
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub


        Protected Sub lkbClearFilter_Click(sender As Object, e As EventArgs)
            Try
                txtSortChar.Text = ""
                txtSortColumn.Text = ""
                txtSortOrder.Text = ""
                txtCurrrentPage.Text = 1
                radcmbSearch.Text = ""
                radcmbSearch.ClearSelection()
                ddlVendorInvoice.Items.Clear()
                divVendorInvoice.Visible = False

                PersistTable.Clear()
                PersistTable.Add(PersistKey.SortColumnName, "")
                PersistTable.Add(PersistKey.SortCharacter, "")
                PersistTable.Add(PersistKey.CurrentPage, 1)
                PersistTable.Add(PersistKey.SortOrder, "")
                PersistTable.Add(ddlSearch.ID, "1")
                PersistTable.Add("RadSearchText", "")
                PersistTable.Add("RadSearchValue", "")
                PersistTable.Save()

                BindDatagrid()

                FocusSearch()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlVendorInvoice_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlVendorInvoice.SelectedIndexChanged
            Try
                txtCurrrentPage.Text = "1"
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
    End Class
End Namespace