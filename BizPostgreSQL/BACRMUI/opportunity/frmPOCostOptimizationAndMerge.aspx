﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmPOCostOptimizationAndMerge.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmPOCostOptimizationAndMerge" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ajaxStart(function () {
            $("#divLoader").show();
        }).ajaxStop(function () {
            $("#divLoader").hide();
        });

        $(document).ready(function () {
            $.when(LoadWarehouses()).then(function () {
                LoadRecords();
            });

            $("#chkMergable").change(function () {
                LoadRecords();
            });

            $("#chkExcludeSendPO").change(function () {
                LoadRecords();
            });

            $("#chkDisplayOnlyCostSaving").change(function () {
                LoadRecords();
            });

            $("#ddlWarehouse").change(function () {
                $("[id$=hdnCostType]").val(0);
                LoadRecords();
            });

            $("#hplClearFilters").click(function () {
                $("[id$=hdnCostType]").val(0);
                $("#chkMergable").prop("checked", false);
                $("#chkExcludeSendPO").prop("checked", false);
                LoadRecords();
            });

            $("#btnUpdateCostToPO").click(function () {
                if ($("#divContent tr[costselected='1']").length > 0) {
                    try {
                        var selectedRecords = [];

                        $("#divContent tr[costselected='1']").each(function () {
                            var obj = {}
                            obj.VendorID = parseInt($(this).closest("div[id*=divVendor]").attr("id").replace("divVendor", ""));
                            obj.SelectedVendorID = parseInt($(this).attr("vendorID"));
                            obj.OppID = parseInt($(this).closest("table[id*=tableItems]").attr("id").replace("tableItems", ""));
                            obj.OppItemID = parseInt($(this).find(".hdnOppItemID").val());
                            obj.Units = parseFloat($(this).find(".txtUnits").val().trim().replace(/,/g, ''));
                            obj.UnitCost = parseFloat($(this).find(".lblSelectedCost").text().replace(/,/g, ''));
                            selectedRecords.push(obj);
                        });

                        var found = $.map(selectedRecords, function (obj) {
                            if (obj.VendorID != obj.SelectedVendorID)
                                return obj;
                        });

                        var isApproved = false;

                        if (found.length > 0) {
                            if (confirm("New POs will need to be created for vendors where source unit-costs are derived. If this applies to all line items on existing POs, then these POs will automatically be deleted. Are you sure you want to do this?")) {
                                isApproved = true;
                            } else {
                                isApproved = false;
                            }
                        } else {
                            isApproved = true;
                        }

                        if (isApproved) {
                            $.ajax({
                                type: "POST",
                                url: '../WebServices/CommonService.svc/UpdatePurchaseOrderCost',
                                contentType: "application/json",
                                dataType: "json",
                                data: JSON.stringify({
                                    "selectedRecords": JSON.stringify(selectedRecords)
                                }),
                                success: function (data) {
                                    $("[id$=hdnCostType]").val(0);
                                    $("#btnUpdateCostToPO").hide();
                                    $("#btnUpdateCostToVendor").hide();
                                    LoadRecords();
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    if (IsJsonString(jqXHR.responseText)) {
                                        var objError = $.parseJSON(jqXHR.responseText)
                                        alert(objError);
                                    } else {
                                        alert("Unknown error ocurred");
                                    }
                                }
                            });
                        }
                    } catch (e) {
                        alert("Unknown error occurred while updating cost.");
                    }
                } else {
                    alert("Select cost to update for atleast one record.");
                }
            });

            $("#btnUpdateCostToVendor").click(function () {
                if ($("#divContent tr[costselected='1']").length > 0) {
                    if (confirm("You’re about to update the vendor unit cost on item master records for vendors, based $/Unit values for selected vendors Are you sure you want to do this")) {
                        try {
                            var selectedRecords = [];

                            $("#divContent tr[costselected='1']").each(function () {
                                var obj = {}
                                obj.VendorID = parseInt($(this).closest("div[id*=divVendor]").attr("id").replace("divVendor", ""));
                                obj.ItemCode = parseInt($(this).find(".hdnItemCode").val());
                                obj.UnitCost = parseFloat($(this).find(".lblSelectedCost").text().replace(/,/g, ''));
                                selectedRecords.push(obj);
                            });

                            $.ajax({
                                type: "POST",
                                url: '../WebServices/CommonService.svc/UpdateItemVendorCost',
                                contentType: "application/json",
                                dataType: "json",
                                data: JSON.stringify({
                                    "selectedRecords": JSON.stringify(selectedRecords)
                                }),
                                success: function (data) {
                                    ClearCostSelection();
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    if (IsJsonString(jqXHR.responseText)) {
                                        var objError = $.parseJSON(jqXHR.responseText)
                                        alert(objError);
                                    } else {
                                        alert("Unknown error ocurred");
                                    }
                                }
                            });
                        } catch (e) {
                            alert("Unknown error occurred while updating vendor cost.");
                        }
                    }
                } else {
                    alert("Select cost to update for atleast one record.");
                }
            });
        });

        function LoadWarehouses() {
            return $.ajax({
                type: "GET",
                url: '../opportunity/MassSalesFulfillmentService.svc/GetWarehouses',
                contentType: "application/json",
                success: function (data) {
                    try {
                        var obj = $.parseJSON(data.GetWarehousesResult);
                        if (obj != null && obj.length > 0) {
                            var strWarehouses = "";
                            obj.forEach(function (e) {
                                if (e.numWareHouseID == parseInt($("[id$=hdnMSWarehouseID]").val())) {
                                    strWarehouses += "<option value='" + e.numWareHouseID.toString() + "' selected>" + replaceNull(e.vcWareHouse) + "</option>";
                                } else {
                                    strWarehouses += "<option value='" + e.numWareHouseID.toString() + "'>" + replaceNull(e.vcWareHouse) + "</option>";
                                }
                            });

                            $("#ddlWarehouse").append(strWarehouses);
                        } else {
                            $("#ddlWarehouse").append("<option value='0'>-- Select Warehouse --</option>");
                        }
                    } catch (err) {
                        alert("Unknown error occurred.");
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    if (IsJsonString(jqXHR.responseText)) {
                        var objError = $.parseJSON(jqXHR.responseText)
                        alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                    } else {
                        alert("Unknown error ocurred");
                    }
                }
            });
        }

        function LoadRecords() {
            try {
                $("#divContent").removeClass("text-center");
                $("#divContent").html("");

                $.ajax({
                    type: "POST",
                    url: '../WebServices/CommonService.svc/GetPOForCostOptimizationAndMerge',
                    contentType: "application/json",
                    dataType: "json",
                    data: JSON.stringify({
                        "warehouseID": parseInt($("#ddlWarehouse").val())
                        , "isOnlyMergable": $("#chkMergable").is(":checked")
                        , "isExcludePOSendToVendor": $("#chkExcludeSendPO").is(":checked")
                        , "isDisplayOnlyCostSaving": $("#chkDisplayOnlyCostSaving").is(":checked")
                        , "costType": parseInt($("[id$=hdnCostType]").val())
                    }),
                    success: function (data) {
                        try {
                            var data = $.parseJSON(data.GetPOForCostOptimizationAndMergeResult);
                            var arrVendors = $.parseJSON(data.Vendors);
                            var arrPurchaseOrders = $.parseJSON(data.PurchaseOrders);
                            var arrItems = $.parseJSON(data.Items);
                            if (arrVendors.length > 0) {
                                var arrPOs = [];
                                var found = 0;

                                var html = "";
                                html += "<div class='row padbottom10'><div class='col-xs-12'><div class='pull-right' style='margin-right:10px;'><label>Select all POs on page</label> <input type='checkbox' onchange='return SelectAllPO(this);' /></div></div></div>";
                                arrVendors.forEach(function (objVendor) {
                                    html += "<div class='panel box box-solid box-default box-customer'>";
                                    html += "<div class='box-header with-border' style='padding:8px;background-color:#d2d6de'>";
                                    html += "<h4 class='box-title' style='display:block;font-size:16px;line-height:30px;'>";
                                    html += "<a  class='accordion-toggle aVendor' style='font-weight:bold;color:#000' data-toggle='collapse' data-parent='#accordion' href='#divVendor" + objVendor.numDivisionID + "' aria-expanded='true'>";
                                    html += objVendor.vcCompanyName;
                                    html += "</a>";
                                    html += "  <a target='_blank' href='" + replaceNull(objVendor.vcNavigationURL) + "' style='color:#3c8dbc;background:none;'><i class='fa fa-external-link' aria-hidden='true'></i></a>";
                                    html += "<input type='hidden' class='hdnIncentivePlan' value='" + objVendor.vcPurchaseIncentive + "' />";
                                    html += "<span style='padding-left:20px;'>";
                                    var arrPurchaseIncentive = $.parseJSON(objVendor.vcPurchaseIncentive)

                                    arrPurchaseIncentive.forEach(function (item) {
                                        html += ' <b>Buy</b> ';
                                        if (item.intType == 1) {
                                            html += ('$' + (item.vcbuyingqty || 0) + ' Amount ');
                                        } else if (item.intType == 2) {
                                            html += ((item.vcbuyingqty || 0) + ' Quantity ');
                                        } else {
                                            html += ((item.vcbuyingqty || 0) + ' Lbs ');
                                        }
                                        html += (' <b>Get</b> ' + ((item.vcbuyingqty == 1) ? ("$" + (item.vcIncentives || 0) + " off.") : ((item.vcIncentives || 0) + "% off.")));
                                    });
                                    html += "</span>";
                                    html += " <span style='float:right;margin-right:25px;' class='spnCostSave'></span>";
                                    html += "##ButtonMergePO##";
                                    html += "</h4>";

                                    html += "</div>";
                                    html += "<div id='divVendor" + objVendor.numDivisionID + "' class='panel-collapse collapse in' aria-expanded='true'>";
                                    html += "<div class='box-body'>";

                                    html += "<table class='table tblPrimary'>"
                                    html += "<thead>";
                                    html += "<tr>";
                                    html += "<th>PO Name</th>";
                                    html += "<th>Created on, by</th>";
                                    html += "<th>Estimated Ship</th>";
                                    html += "<th>Total Amt</th>";
                                    html += "<th>PO Sent to Vendor</th>";
                                    html += "<th style='width:25px'>Select Master</th>";
                                    html += "<th style='width:25px'><input type='checkbox' class='chkAll" + objVendor.numDivisionID + "' onchange=\"return SelectAllVendorPO(this,'chk" + objVendor.numDivisionID + "');\" /></th>";
                                    html += "</tr>";
                                    html += "</thead>";
                                    html += "<tbody>";

                                    arrPOs = [];


                                    arrPurchaseOrders.forEach(function (objPO) {
                                        if (parseInt(objPO.numDivisionID) === objVendor.numDivisionID) {
                                            var found = $.map(arrPOs, function (obj) {
                                                if (obj.OppID === parseInt(objPO.numOppId))
                                                    return obj;
                                            });

                                            if (found.length === 0) {
                                                var obj = {};
                                                obj.OppID = parseInt(objPO.numOppId);
                                                arrPOs.push(obj);

                                                html += "<tr style='background-color: #e2e2e2;font-weight: bold;' id='trPO" + objPO.numOppId + "'>";
                                                html += "<td><a target='_blank' href='../opportunity/frmOpportunities.aspx?OpID=" + objPO.numOppId + "'>" + replaceNull(objPO.vcPOppName) + "</a></td>";
                                                html += "<td>" + replaceNull(objPO.bintCreatedDate) + ", " + replaceNull(objPO.vcCreatedBy) + "<input type='hidden' class='hdnPOSent' value='" + objPO.bitPOSendToVendor + "' /></td>";
                                                html += "<td atyle='width:150px;'></td>";
                                                html += "<td>" + objPO.monDealAmount.toLocaleString() + "</td>";
                                                html += "<td>" + replaceNull(objPO.vcPOSend) + "</td>";
                                                html += "<td style='width:105px' class='text-center'><input type='radio' name='rd" + objVendor.numDivisionID + "' oppID='" + objPO.numOppId + "' /></td>";
                                                html += "<td style='width:25px;border-bottom:0px;' class='text-center'><input type='checkbox' class='chk" + objVendor.numDivisionID + " chkPO' oppID='" + objPO.numOppId + "' /></td>";
                                                html += "</tr>";

                                                html += "<tr>";
                                                html += "<td colspan='7' style='padding: 0px;'>"
                                                html += "<table class='table' id='tableItems" + objPO.numOppId + "' style='margin-bottom:0px;'>"
                                                html += "<thead>";
                                                html += "<tr>";
                                                html += "<th style='width:250px;background-color:#f7f7f7 !important;border-bottom: 2px solid #e4e4e4;color:#000;'>Item Name (SKU)</th>";
                                                html += "<th style='width:120px;background-color:#f7f7f7 !important;border-bottom: 2px solid #e4e4e4;color:#000;'>$/Unit</th>";
                                                html += "<th style='width:120px;background-color:#f7f7f7 !important;border-bottom: 2px solid #e4e4e4;color:#000;'>Units</th>";
                                                html += "<th style='width:140px;background-color:#f7f7f7 !important;border-bottom: 2px solid #e4e4e4;color:#000;'>Selected unit-cost</th>";
                                                html += "<th style='width:120px;background-color:#f7f7f7 !important;border-bottom: 2px solid #e4e4e4;color:#000;'>Min qty</th>";
                                                html += "<th style='background-color:#f7f7f7 !important;border-bottom: 2px solid #e4e4e4;color:#000;'>Potential cost-savings found</th>";
                                                html += "</tr>";
                                                html += "</thead>";
                                                html += "<tbody>";

                                                arrItems.forEach(function (objItem) {
                                                    if (objPO.numOppId === parseInt(objItem.numOppId)) {
                                                        html += "<tr>";
                                                        html += "<td><input type='hidden' class='hdnItemCode' value='" + objItem.numItemCode + "' /><input type='hidden' class='hdnOppItemID' value='" + objItem.numoppitemtCode + "' /><input type='hidden' class='hdnUnit' value='" + objItem.numUnitHour + "' /><input type='hidden' class='hdnUnitCost' value='" + objItem.monPrice + "' /><input type='hidden' class='hdnWeight' value='" + objItem.fltWeight + "' /><a target='_blank' href='../Items/frmKitDetails.aspx?ItemCode=" + objItem.numItemCode + "'><img src='../images/tag.png'></a> " + replaceNull(objItem.vcItemName) + ((replaceNull(objItem.vcSKU)).length > 0 ? (" (" + objItem.vcSKU + ")") : "") + "</td>";
                                                        html += "<td><input type='text' class='txtUnitCost form-control' value='" + (objItem.monPrice || 0) + "'/></td>";
                                                        html += "<td><input type='text' class='txtUnits form-control' value='" + (objItem.numUnitHour || 0) + "'/></td>";
                                                        html += "<td><label class='lblSelectedCost'></label></td>";
                                                        html += "<td><label class='lblMinQty'></label></td>";
                                                        html += "<td style='border-bottom:0px;' class='tdItemCostSaving'>" + replaceNull(objItem.vcItemCostSaving) + "</td>";
                                                        html += "</tr>";
                                                    }
                                                });

                                                html += "</tbody>";
                                                html += "</table>"
                                                html += "</td";
                                                html += "</tr>"
                                            }
                                        }
                                    });

                                    if (arrPOs.length > 1) {
                                        html = html.replace("##ButtonMergePO##", "<input type='button' class='btn btn-sm btn-flat bg-blue' style='float:right;margin-right:25px;' value='Merge POs' onclick='return MergePO(" + objVendor.numDivisionID + ");'/>");
                                    } else {
                                        html = html.replace("##ButtonMergePO##", "");
                                    }

                                    html += "</tbody>";
                                    html += "</table>"

                                    html += "</div>";
                                    html += "</div>";
                                    html += "</div>";
                                });

                                $("#divContent").html(html);
                            } else {
                                $("#divContent").html("<div class='row'><div class='col-xs-12 text-center'><label>No records found.</label></div></div>");
                            }
                        } catch (err) {
                            alert("Unknown error occurred.");
                        }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        if (IsJsonString(jqXHR.responseText)) {
                            var objError = $.parseJSON(jqXHR.responseText)
                            alert("Error occurred: " + replaceNull(objError.ErrorMessage));
                        } else {
                            alert("Unknown error ocurred");
                        }
                    }
                });
            } catch (e) {
                alert("Unknown error occurred while loading records.");
            }
        }

        function FindIncentivePlanCost() {
            try {
                $("[id$=hdnCostType]").val(1);

                $("#divContent > .box-customer").each(function (index, boxCustomer) {
                    var arrPurchaseIncentive = $.parseJSON($(boxCustomer).find(".hdnIncentivePlan").val());
                    var totalUnit = 0;
                    var totalAmount = 0;
                    var totalWeight = 0;
                    var costSaving = 0;

                    if (arrPurchaseIncentive.length > 0) {
                        $(boxCustomer).find("tr[id*='trPO']").each(function (index, tr) {
                            if (!JSON.parse($(tr).find(".hdnPOSent").val())) {
                                var itemTableID = $(tr).attr("id").replace("trPO", "tableItems");

                                $(boxCustomer).find("#" + itemTableID).find(".hdnUnit").each(function (index, input) {
                                    totalUnit += parseFloat($(input).val());
                                });

                                $(boxCustomer).find("#" + itemTableID).find(".hdnUnitCost").each(function (index, input) {
                                    totalAmount += (parseFloat($(input).val()) * parseFloat($(input).closest("td").find(".hdnUnit").val()));
                                });

                                $(boxCustomer).find("#" + itemTableID).find(".hdnWeight").each(function (index, input) {
                                    totalWeight += (parseFloat($(input).val()) * parseFloat($(input).closest("td").find(".hdnUnit").val()));
                                });
                            }
                        });


                        arrPurchaseIncentive.forEach(function (objIncentive) {
                            var valueToCompare = 0;

                            if (objIncentive.intType == 1) {
                                valueToCompare = totalAmount;
                            } else if (objIncentive.intType == 2) {
                                valueToCompare = totalUnit;
                            } else if (objIncentive.intType == 3) {
                                valueToCompare = totalWeight;
                            }   

                            if (valueToCompare >= parseFloat((objIncentive.vcbuyingqty || 0))) {
                                if (objIncentive.tintDiscountType === 1) {
                                    if (parseFloat((objIncentive.vcIncentives || 0)) > costSaving) {
                                        costSaving = parseFloat((objIncentive.vcIncentives || 0));
                                    }
                                } else {
                                    if ((totalAmount * (parseFloat((objIncentive.vcIncentives || 0)) / 100)) > costSaving) {
                                        costSaving = (totalAmount * (parseFloat((objIncentive.vcIncentives || 0)) / 100));
                                    }
                                }
                            } 
                        });
                    }

                    if (costSaving > 0) {
                        $(boxCustomer).find(".spnCostSave").html("<label>Savings-Potential: </label>" + "$" + costSaving.toLocaleString());
                    } else {
                        $(boxCustomer).remove();

                        if ($("#divContent > .box-customer").length === 0) {
                            $("#divContent").html("<div class='row'><div class='col-xs-12 text-center'><label>No records found.</label></div></div>");
                        }
                    }
                });
            } catch (e) {
                alert("Unknown error occurred.");
            }

            return false;
        }

        function FindItemCost() {
            $("[id$=hdnCostType]").val(2);
            LoadRecords();
            return false;
        }

        function RecordSelected(input, vendor, price, minorderQty) {
            var tr = $(input).closest("tr");
            $(tr).attr("CostSelected", "1");
            $(tr).attr("vendorID", vendor);
            $(tr).find(".lblSelectedCost").text(price.toLocaleString());
            $(tr).find(".lblMinQty ").text(minorderQty.toLocaleString());
            $(tr).css("background-color", "rgb(240,255,231,1);");
            $("#btnUpdateCostToPO").show();
            $("#btnUpdateCostToVendor").show();
        }

        function ClearCostSelection() {
            $("#divContent tr[costselected='1']").each(function () {
                $(this).removeAttr("costselected");
                $(this).removeAttr("vendorID");
                $(this).find(".lblSelectedCost").text("");
                $(this).find(".lblMinQty").text("");
                $(this).find(".tdItemCostSaving").html("");
                $(this).css("background-color", "#fff");
                $("#btnUpdateCostToPO").hide();
                $("#btnUpdateCostToVendor").hide();
            });  
        }

        function MergePO(vendorID) {
            try {
                if ($("input[name='rd" + vendorID + "']:checked").length > 0) {
                    var masterOppID = parseInt($("input[name='rd" + vendorID + "']:checked").attr("oppID"));

                    var selectedPOs = [];
                    $("input.chk" + vendorID).each(function () {
                        if ($(this).is(":checked") && masterOppID != parseInt($(this).attr("oppID"))) {
                            selectedPOs.push(parseInt($(this).attr("oppID")));
                        }
                    });

                    if (selectedPOs.length > 0) {
                        if (confirm("Selected purchased ordered will be merged with master puchase order and deleted. Do you want to proceed?")) {
                            var arrItems = [];

                            selectedPOs.forEach(function (purchaseOrderID) {
                                $("#tableItems" + purchaseOrderID + " > tbody > tr").each(function () {
                                    var obj = {};
                                    obj.OppID = purchaseOrderID;
                                    obj.OppItemID = parseInt($(this).find(".hdnOppItemID").val());
                                    obj.Units = parseFloat($(this).find(".txtUnits").val().trim().replace(/,/g, ''));
                                    obj.UnitCost = parseFloat($(this).find(".txtUnitCost").val().trim().replace(/,/g, ''));
                                    arrItems.push(obj);
                                });
                            });

                            $.ajax({
                                type: "POST",
                                url: '../WebServices/CommonService.svc/MergePOs',
                                contentType: "application/json",
                                dataType: "json",
                                data: JSON.stringify({
                                    "masterPO": masterOppID
                                    , "itemsToMerge": JSON.stringify(arrItems)
                                }),
                                success: function (data) {
                                    alert("PO(s) are merged successfully.");
                                    LoadRecords();
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    if (IsJsonString(jqXHR.responseText)) {
                                        var objError = $.parseJSON(jqXHR.responseText)
                                        alert(objError);
                                    } else {
                                        alert("Unknown error ocurred");
                                    }
                                }
                            });
                        }
                    } else {
                        alert("Select purchase order(s) needs to be merged with master purchase order.");
                    }
                    
                } else {
                    alert("Select master purchase order.");
                }

            } catch (e) {
                alert("Unknown error occurred.");
            }

            return false;
        }

        function SelectAllVendorPO(chk,chkClass) {
            $("." + chkClass).prop("checked", $(chk).is(":checked"));
        }

        function SelectAllPO(chk) {
            $(".chkPO").prop("checked", $(chk).is(":checked"));
        }

        function IsJsonString(str) {
            try {
                JSON.parse(str);
            } catch (e) {
                return false;
            }
            return true;
        }

        function replaceNull(value) {
            return String(value) === "null" || String(value) === "undefined" ? "" : value.toString().replace(/'/g, "&#39;");
        }
    </script>
    <style type="text/css">
        #divContent .box-header .accordion-toggle:after {
            font-family: 'Glyphicons Halflings';
            content: "\e114";
            float: right;
        }

        #divContent .box-header .accordion-toggle.collapsed:after {
            content: "\e080";
        }

        #divContent .box-customer .box-header {
            background-color: #e8e8e8;
        }

        #divContent .box-customer .box-body {
            padding: 0px;
        }

        #divContent .box-customer .box-header .accordion-toggle:after {
            color: #000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label>Ship-to  Warehouse</label>
                        <select id="ddlWarehouse" class="form-control"></select>
                    </div>
                    &nbsp;&nbsp;
                    <div class="form-group">
                        <label>Show only mergeable opportunities</label>
                        <input type="checkbox" class="checkbox" id="chkMergable" />
                    </div>
                    &nbsp;&nbsp;
                    <div class="form-group">
                        <label>Don’t show POs sent to vendor</label>
                        <input type="checkbox" class="checkbox" id="chkExcludeSendPO" />
                    </div>
                    &nbsp;&nbsp;
                    <div class="form-group">
                        <label>Show only items with lower unit pricing</label>
                        <input type="checkbox" class="checkbox" id="chkDisplayOnlyCostSaving" />
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <button id="btnIncentiveCost" class="btn btn-primary" onclick="return FindIncentivePlanCost();">Find Incentive-plan Cost</button>
                <button id="btnItemCost" class="btn btn-primary" onclick="return FindItemCost();">Find Item Cost</button>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Merging & Cost Optimization&nbsp;<a href="#" onclick="return OpenHelpPopUp('opportunity/frmpocostoptimizationandmerge.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
    <input type="button" id="btnUpdateCostToPO" class="btn btn-flat btn-success" value="Update selected unit cost on POs" style="display:none" />
    &nbsp;&nbsp;<input type="button" id="btnUpdateCostToVendor" class="btn btn-flat btn-warning" value="Update unit-cost on item records" style="display:none" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
    <a href="#" id="hplClearFilters" title="Clear All Filters" class="btn-box-tool"><i class="fa fa-2x fa-filter"></i></a>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <div class="overlay" id="divLoader" style="z-index: 10000">
        <div class="overlayContent" style="color: #000; background-color: #fff; text-align: center; width: 250px; padding: 20px">
            <i class="fa fa-2x fa-refresh fa-spin"></i>
            <h3>Please wait...</h3>
        </div>
    </div>
    <div class="box-group" id="divContent">
    </div>
    <asp:HiddenField ID="hdnMSWarehouseID" runat="server" />
    <asp:HiddenField ID="hdnMSCurrencyID" runat="server" />
    <asp:HiddenField ID="hdnDecimalPoints" runat="server" />
    <asp:HiddenField ID="hdnCostType" runat="server" Value="0" />
</asp:Content>
