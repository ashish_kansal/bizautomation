﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master "
    CodeBehind="frmSalesBizDocWorkflowSettings.aspx.vb" Inherits=".frmSalesBizDocWorkflowSettings" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Sales BizDoc Creation Workflow Settings</title>
    <link href="../CSS/GridColorScheme.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" language="javascript">
        function windowResize() {
            var width = document.getElementById("resizeDiv").offsetWidth;
            var height = document.getElementById("resizeDiv").offsetHeight;
            window.resizeTo(width + 35, height + 150);
        }

        $(document).ready(function () {

            $("#ddlRules").on("change", function () {
                if ($(this).val() != 0) {
                    if ($(this).val() == 2) {
                        $("#rdbRule14").removeAttr("checked");
                        // $("#rdbRule14").disabled = true;
                        $("#rdbRule14").prop('disabled', true);
                    }
                    else {
                        $("#rdbRule14").prop('disabled', false);
                    }
                }
                else {
                    $("#rdbRule14").prop('disabled', false);
                }
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <table width="860px">
        <tr>
            <td>
                <div class="input-part">
                    <div class="right-input">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" />&nbsp;
                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="button" />&nbsp;&nbsp;
                    </div>
                </div>
            </td>
        </tr>
        <tr>
            <td class="normal4" align="center">
                <ul id="messagebox" class="errorInfo" style="display: none">
                </ul>
                <asp:Label ID="litMessage" EnableViewState="false" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Sales BizDoc Creation Workflow Settings
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>


    <table>
        <tr>
            <td>

                WHEN a new 
                <asp:DropDownList runat="server" ID="ddlBizDocs" AutoPostBack="true"></asp:DropDownList>
               BizDoc With the following Template <asp:DropDownList Width="150" runat="server" ID="ddlBizDocTemplate"></asp:DropDownList>
                  is created FROM                                                    
                <asp:DropDownList runat="server" ID="ddlRules">
                    <asp:ListItem Text="--Select One--" Value="0"></asp:ListItem>
                      <asp:ListItem Text="Manually from new SO or BizDoc section of SO" Value="1"></asp:ListItem>
                      <%--<asp:ListItem Text=" BizDoc section of SO" Value="2"></asp:ListItem>--%>
                      <asp:ListItem Text="Sales Fulfillment Workflow" Value="2"></asp:ListItem>
                      <%--<asp:ListItem Text="Sales Transaction Management" Value="4"></asp:ListItem>--%>
                </asp:DropDownList><br />
                where the Opportunity / Order Source is <asp:DropDownList runat="server" Width="150" ID="ddlOrderSource"></asp:DropDownList>
                <asp:CheckBox runat="server" ID="chkUpdatedTrackingNo" /> AFTER Tracking # is added to the BizDoc: 
            </td>

        </tr>
        <tr><td>
            <div>
            <asp:RadioButton runat="server" ID="rdbRule14" GroupName="Rules"/> 
            Automatically create/attach PDF of BizDoc and open new compose message window<br /> with the following email template
            <asp:DropDownList runat="server" ID="ddlEmailTemplate14" Width="220"></asp:DropDownList>
             <div>
                 <asp:CheckBox runat="server" Id="chkSetBizDocStatus14" /> AND set BizDoc status to <asp:DropDownList runat="server" ID="ddlBizDocStatus14"></asp:DropDownList>
                  <asp:CheckBox runat="server" Id="chkOpenReceivePayment14" /> AND open the receive payment window

             </div>

            </div>

             <div>
            <asp:RadioButton runat="server" ID="rdbRule15" GroupName="Rules"/> 
            Automatically create/attach PDF of BizDoc and auto-send within the following email template
            <asp:DropDownList runat="server" ID="ddlEmailTemplate15" Width="220"></asp:DropDownList>
                  <div>
                 <asp:CheckBox runat="server" ID="chkBizDocStatus15" /> AND set BizDoc status to
                       <asp:DropDownList runat="server" ID="ddlBizDocStatus15"></asp:DropDownList>
                  <asp:CheckBox runat="server" ID="chkCreditCard15" /> Credit Card
             </div>
                 </div>
            </td></tr>

    </table>
    </asp:Content>