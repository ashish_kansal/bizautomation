Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts

Namespace BACRM.UserInterface.Opportunities
    Partial Public Class frmConfOppList
        Inherits BACRMPage

        Dim objContact As CContacts

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                btnSave.Attributes.Add("onclick", "Save()")
                If Not IsPostBack Then
                    hfFormId.Value = CCommon.ToLong(GetQueryStringVal( "FormId"))
                    
                    If Not ddlType.Items.FindByValue(GetQueryStringVal( "FormId")) Is Nothing Then
                        ddlType.ClearSelection()
                        ddlType.Items.FindByValue(GetQueryStringVal( "FormId")).Selected = True
                    End If
                    BindLists()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindLists()
            Try
                lstAvailablefld.Items.Clear()
                lstSelectedfld.Items.Clear()
                Dim ds As DataSet
                objContact = New CContacts
                objContact.DomainID = Session("DomainId")

                objContact.FormId = hfFormId.Value

                objContact.UserCntID = Session("UserContactId")
                objContact.ContactType = hfFormId.Value
                ds = objContact.GetColumnConfiguration()

                If objContact.FormId = 39 Or objContact.FormId = 41 Then
                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 AndAlso ds.Tables(0).Select("numFieldID='902'").Length > 0 Then
                        ds.Tables(0).Rows.Remove(ds.Tables(0).Select("numFieldID=902")(0))
                        ds.AcceptChanges()
                    End If
                End If

                lstAvailablefld.DataSource = ds.Tables(0)
                lstAvailablefld.DataTextField = "vcFieldName"
                lstAvailablefld.DataValueField = "numFieldID"
                lstAvailablefld.DataBind()

                lstSelectedfld.DataSource = ds.Tables(1)
                lstSelectedfld.DataValueField = "numFieldID"
                lstSelectedfld.DataTextField = "vcFieldName"
                lstSelectedfld.DataBind()

                If Not lstAvailablefld.Items.FindByText("Opp Name") Is Nothing Then
                    Dim item As ListItem
                    item = lstAvailablefld.Items.FindByText("Opp Name")
                    Select Case ddlType.SelectedValue
                        Case 38
                            item.Text = "Sales Opportunity Name"
                        Case 40
                            item.Text = "Purchase Opportunity Name"
                        Case 39
                            item.Text = "Sales Order Name"
                        Case 41
                            item.Text = "Purchase Order Name"
                    End Select
                End If

                If Not lstSelectedfld.Items.FindByText("Opp Name") Is Nothing Then
                    Dim item As ListItem
                    item = lstSelectedfld.Items.FindByText("Opp Name")
                    Select Case ddlType.SelectedValue
                        Case 38
                            item.Text = "Sales Opportunity Name"
                        Case 40
                            item.Text = "Purchase Opportunity Name"
                        Case 39
                            item.Text = "Sales Order Name"
                        Case 41
                            item.Text = "Purchase Order Name"
                    End Select
                End If

                'If ddlType.SelectedValue = 40 Or ddlType.SelectedValue = 41 Then
                '    If Not lstAvailablefld.Items.FindByText("Purchase Type") Is Nothing Then
                '        lstAvailablefld.Items.Remove(lstAvailablefld.Items.FindByText("Purchase Type"))
                '    End If
                'Else
                '    If Not lstAvailablefld.Items.FindByText("Sales Type") Is Nothing Then
                '        lstAvailablefld.Items.Remove(lstAvailablefld.Items.FindByText("Sales Type"))
                '    End If
                'End If

                'If ddlType.SelectedValue = 1 Or ddlType.SelectedValue = 2 Then 'Do not allow Recurring for OPen Sales/Purchase Opportunity 
                '    If Not lstAvailablefld.Items.FindByText("Next Billing Date") Is Nothing Then
                '        lstAvailablefld.Items.Remove(lstAvailablefld.Items.FindByText("Next Billing Date"))
                '    End If
                'End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
            Try
                hfFormId.Value = ddlType.SelectedValue
                BindLists()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                If objContact Is Nothing Then objContact = New CContacts

                Dim dsNew As New DataSet
                Dim dtTable As New DataTable
                dtTable.Columns.Add("numFieldID")
                dtTable.Columns.Add("bitCustom")
                dtTable.Columns.Add("tintOrder")
                Dim i As Integer
                Dim dr As DataRow
                Dim str As String()
                str = hdnCol.Value.Split(",")
                For i = 0 To str.Length - 2
                    dr = dtTable.NewRow
                    dr("numFieldID") = str(i).Split("~")(0)
                    dr("bitCustom") = str(i).Split("~")(1)
                    dr("tintOrder") = i
                    dtTable.Rows.Add(dr)
                Next

                dtTable.TableName = "Table"
                dsNew.Tables.Add(dtTable.Copy)
                objContact.ContactType = ddlType.SelectedValue
                objContact.DomainID = Session("DomainId")
                objContact.UserCntID = Session("UserContactId")
                objContact.FormId = hfFormId.Value
                objContact.strXml = dsNew.GetXml
                objContact.SaveContactColumnConfiguration()
                dsNew.Tables.Remove(dsNew.Tables(0))
                BindLists()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace