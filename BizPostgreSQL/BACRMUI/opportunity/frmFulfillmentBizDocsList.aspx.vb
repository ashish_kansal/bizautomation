﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common

Public Class frmFulfillmentBizDocsList
    Inherits BACRMPage

    'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
    '    Try
    '        If Not IsPostBack Then
    '            BindData()

    '            btnClose.Attributes.Add("onclick", "return Close()")
    '            btnSave.Attributes.Add("onclick", "return Save()")
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub BindData()
    '    Try
    '        Dim objAdmin As New CAdmin
    '        objAdmin.DomainID = Session("DomainID")
    '        objAdmin.byteMode = 0

    '        Dim dtBizDocs As DataTable = objAdmin.ManageOpportunityFulfillmentBizDocs()

    '        bindBizDocs(ddlBizDocsInventoryItems, dtBizDocs)
    '        bindBizDocs(ddlBizDocsNonInventoryItems, dtBizDocs)
    '        bindBizDocs(ddlBizDocsServiceItems, dtBizDocs)

    '        objAdmin.byteMode = 2
    '        Dim dt As DataTable = objAdmin.ManageOpportunityFulfillmentBizDocs()

    '        Dim foundRows As DataRow() = dt.Select("ItemType='P'")
    '        If foundRows.Length > 0 Then
    '            If Not ddlBizDocsInventoryItems.Items.FindByValue(foundRows(0)("numBizDocTypeID")) Is Nothing Then
    '                ddlBizDocsInventoryItems.Items.FindByValue(foundRows(0)("numBizDocTypeID")).Selected = True
    '            End If
    '        End If

    '        foundRows = dt.Select("ItemType='N'")
    '        If foundRows.Length > 0 Then
    '            If Not ddlBizDocsNonInventoryItems.Items.FindByValue(foundRows(0)("numBizDocTypeID")) Is Nothing Then
    '                ddlBizDocsNonInventoryItems.Items.FindByValue(foundRows(0)("numBizDocTypeID")).Selected = True
    '            End If
    '        End If

    '        foundRows = dt.Select("ItemType='S'")
    '        If foundRows.Length > 0 Then
    '            If Not ddlBizDocsServiceItems.Items.FindByValue(foundRows(0)("numBizDocTypeID")) Is Nothing Then
    '                ddlBizDocsServiceItems.Items.FindByValue(foundRows(0)("numBizDocTypeID")).Selected = True
    '            End If
    '        End If
    '        'gvBizDoc.DataSource = objAdmin.ManageOpportunityFulfillmentBizDocs()
    '        'gvBizDoc.DataBind()
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Sub bindBizDocs(ddlBizDocs As DropDownList, dtBizDocs As DataTable)
    '    Try
    '        ddlBizDocs.DataTextField = "vcBizDocType"
    '        ddlBizDocs.DataValueField = "numBizDocTypeID"
    '        ddlBizDocs.DataSource = dtBizDocs
    '        ddlBizDocs.DataBind()
    '        ddlBizDocs.Items.Insert(0, New ListItem("--Select One--", "0"))
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

    'Private Sub btnSave_Click(sender As Object, e As System.EventArgs) Handles btnSave.Click
    '    Try
    '        Dim dtTable As New DataTable
    '        dtTable.TableName = "Table"

    '        CCommon.AddColumnsToDataTable(dtTable, "numBizDocTypeID,ItemType")

    '        Dim dr As DataRow
    '        'For Each gvr As GridViewRow In gvBizDoc.Rows
    '        '    If (CType(gvr.FindControl("chkFulfillmentOrder"), CheckBox).Checked) Then
    '        '        dr = dtTable.NewRow

    '        '        dr("numBizDocTypeID") = gvBizDoc.DataKeys(gvr.RowIndex).Value
    '        '        dr("bitIsInventory") = CType(gvr.FindControl("chkInventoryItems"), CheckBox).Checked
    '        '        dr("bitIsNonInventory") = CType(gvr.FindControl("chkNonInventoryItems"), CheckBox).Checked
    '        '        dr("bitIsService") = CType(gvr.FindControl("chkServiceItems"), CheckBox).Checked

    '        '        dtTable.Rows.Add(dr)
    '        '    End If
    '        'Next

    '        If ddlBizDocsInventoryItems.SelectedValue > 0 Then
    '            dr = dtTable.NewRow

    '            dr("numBizDocTypeID") = ddlBizDocsInventoryItems.SelectedValue
    '            dr("ItemType") = "P"

    '            dtTable.Rows.Add(dr)
    '        End If

    '        If ddlBizDocsNonInventoryItems.SelectedValue > 0 Then
    '            dr = dtTable.NewRow

    '            dr("numBizDocTypeID") = ddlBizDocsNonInventoryItems.SelectedValue
    '            dr("ItemType") = "N"

    '            dtTable.Rows.Add(dr)
    '        End If

    '        If ddlBizDocsServiceItems.SelectedValue > 0 Then
    '            dr = dtTable.NewRow

    '            dr("numBizDocTypeID") = ddlBizDocsServiceItems.SelectedValue
    '            dr("ItemType") = "S"

    '            dtTable.Rows.Add(dr)
    '        End If

    '        Dim ds As New DataSet
    '        ds.Tables.Add(dtTable)

    '        Dim objAdmin As New CAdmin
    '        objAdmin.DomainID = Session("DomainID")
    '        objAdmin.strItems = ds.GetXml
    '        objAdmin.byteMode = 1
    '        objAdmin.ManageOpportunityFulfillmentBizDocs()

    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    ''Private Sub gvBizDoc_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBizDoc.RowDataBound
    ''    Try
    ''        If e.Row.RowType = DataControlRowType.DataRow Then

    ''            If DataBinder.Eval(e.Row.DataItem, "numBizDocTypeID") = 296 Then
    ''                CType(e.Row.FindControl("chkFulfillmentOrder"), CheckBox).Enabled = False
    ''            End If
    ''        End If
    ''    Catch ex As Exception
    ''        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    ''        Response.Write(ex)
    ''    End Try
    ''End Sub
End Class