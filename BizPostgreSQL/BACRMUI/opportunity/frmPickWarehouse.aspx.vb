﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Opportunities
    Partial Public Class frmPickWarehouse
        Inherits BACRMPage


#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents btnClose As System.Web.UI.WebControls.Button
        Protected WithEvents tblTeams As System.Web.UI.WebControls.Table
        Protected WithEvents ddlTerritory As System.Web.UI.WebControls.ListBox
        Protected WithEvents btnAdd As System.Web.UI.WebControls.Button
        Protected WithEvents btnRemove As System.Web.UI.WebControls.Button
        Protected WithEvents ddlTerritoryAdd As System.Web.UI.WebControls.ListBox
        Protected WithEvents lstTeamAvail As System.Web.UI.WebControls.ListBox
        Protected WithEvents lstTeamAdd As System.Web.UI.WebControls.ListBox
        Protected WithEvents hdnValue As System.Web.UI.WebControls.TextBox

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                    
                    Dim objOpp As New COpportunities
                    Dim ds As DataSet
                    objOpp.DomainID = Session("DomainID")
                    ds = objOpp.GetWarehouseSF
                    lstTeamAvail.DataSource = ds.Tables(0)
                    lstTeamAvail.DataTextField = "vcWarehouse"
                    lstTeamAvail.DataValueField = "numWarehouseID"
                    lstTeamAvail.DataBind()

                    lstTeamAdd.DataSource = ds.Tables(1)
                    lstTeamAdd.DataTextField = "vcWarehouse"
                    lstTeamAdd.DataValueField = "numWarehouseID"
                    lstTeamAdd.DataBind()
                End If
                btnAdd.Attributes.Add("OnClick", "return move(lstTeamAvail,lstTeamAdd)")
                btnRemove.Attributes.Add("OnClick", "return remove1(lstTeamAdd,lstTeamAvail)")
                btnSave.Attributes.Add("onclick", "Save()")
                btnClose.Attributes.Add("onclick", "return Close()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Dim objOpp As New COpportunities
                objOpp.strItemDtls = hdnValue.Text
               objOpp.DomainID = Session("DomainID")
                objOpp.ManageWarehouseSF()
                Response.Write("<script>opener.PopupCheck(); self.close();</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace