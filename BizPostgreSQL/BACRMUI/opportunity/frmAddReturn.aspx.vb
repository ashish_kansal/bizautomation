﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Opportunities
    Partial Public Class frmAddReturn
        Inherits BACRMPage
        Dim lngOppId As Long
        Dim iOppType As Integer
        Dim OppItemIds As String
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lngOppId = GetQueryStringVal("opid")
                iOppType = CCommon.ToInteger(GetQueryStringVal("type"))
                OppItemIds = CCommon.ToString(GetQueryStringVal("OppItemIds"))
                If Not IsPostBack Then

                    'If iOppType = 1 Then
                    '    gvReturns.Columns(7).Visible = False
                    'End If

                    BindGrid()

                End If
                lblMessage.Text = ""
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindGrid()
            Try
                Dim dtReturns As DataTable
                Dim objOpportunity As New MOpportunity
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.Mode = 1
                objOpportunity.OpportunityId = lngOppId
                objOpportunity.strItems = OppItemIds
                dtReturns = objOpportunity.GetOrderItems().Tables(0)
                gvReturns.DataSource = dtReturns
                gvReturns.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Sub BindGrid()

        '    Dim dtReturns As DataTable
        '    dtReturns = If(Session("dtReturns") Is Nothing, Nothing, CType(Session("dtReturns"), DataTable))
        '    gvReturns.DataSource = dtReturns
        '    gvReturns.DataBind()
        'End Sub

        Sub gvReturns_OnRowDataBound(ByVal o As Object, ByVal e As GridViewRowEventArgs) Handles gvReturns.RowDataBound
            Try
                If e.Row.RowType = DataControlRowType.DataRow Then
                    Dim rowView As DataRowView = CType(e.Row.DataItem, DataRowView)
                    Dim ddlReason As DropDownList = CType(e.Row.FindControl("ddlReason"), DropDownList)
                    
                    objCommon.sb_FillComboFromDBwithSel(ddlReason, 48, Session("DomainID"))
                    Dim ddlStatus As DropDownList = CType(e.Row.FindControl("ddlStatus"), DropDownList)
                    objCommon.sb_FillComboFromDBwithSel(ddlStatus, 49, Session("DomainID"))
                    ddlStatus.SelectedIndex = 1
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Private Sub btnAddtoReturnQueue_OnClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddtoReturnQueue.Click
            Try
                Dim boolFlag As Boolean = True
                Dim objOpportunity As New MOpportunity
                For Each gvr As GridViewRow In gvReturns.Rows
                    If CInt(CType(gvr.FindControl("txtQtyReturn"), TextBox).Text.Trim) > 0 Then
                        With objOpportunity
                            .OpportunityId = lngOppId
                            .OppItemCode = gvReturns.DataKeys(gvr.DataItemIndex)("numoppitemtCode").ToString()
                            .ItemCode = CCommon.ToLong(gvReturns.DataKeys(gvr.DataItemIndex)("numItemCode").ToString())
                            .Units = gvr.Cells(3).Text.Trim()
                            .UnitsReturned = CType(gvr.FindControl("txtQtyReturn"), TextBox).Text.Trim
                            .UnitPrice = CCommon.ToDecimal(gvReturns.DataKeys(gvr.DataItemIndex)("monPrice").ToString())
                            .ReasonForReturn = CType(gvr.FindControl("ddlReason"), DropDownList).SelectedValue
                            .ReturnStatus = CType(gvr.FindControl("ddlStatus"), DropDownList).SelectedValue

                            'If iOppType = 2 Then
                            '    .Reference = CType(gvr.FindControl("txtReference"), TextBox).Text.Trim
                            'End If
                            .UserID = Session("UserContactID")
                        End With
                        If Not objOpportunity.InsertReturns() = True Then
                            boolFlag = False
                        End If
                    End If
                Next
                If boolFlag Then
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "close", "<script>Close('" & iOppType & "');</script>")
                End If
            Catch ex As Exception
                If ex.Message.StartsWith("NOT_ALLOWED") = True Then
                    lblMessage.Text = "Total Qty to Return should be less than Total Units"
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Response.Write(ex)
                End If
            End Try
        End Sub
    End Class
End Namespace