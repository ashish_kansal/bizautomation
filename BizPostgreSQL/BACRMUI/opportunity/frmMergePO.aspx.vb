﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports Telerik.Web.UI
Imports System.Collections.Generic
Imports System.Xml.Linq

Public Class frmMergePO
    Inherits BACRMPage

#Region "Member Variables"
    Private objOpp As COpportunities
#End Region

#Region "Constructor"
    Sub New()
        objOpp = New COpportunities

    End Sub
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            litMessage.Text = ""

            If Not Page.IsPostBack Then
                BindGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            litMessage.Text = ex.Message
        End Try
    End Sub
#End Region

#Region "Private Methods"
    Private Sub BindGrid()
        Try
            objOpp.DomainID = CCommon.ToLong(Session("DomainID"))
            RadGridPO.DataSource = objOpp.GetPOAvailableForMerge(0, 0, 0)
            RadGridPO.DataBind()
            UpdatePanelGrid.Update()
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region

#Region "Event Handlers"
    Protected Sub chkSelect_CheckedChanged(sender As Object, e As EventArgs)
        Try
            Dim selectedPOCount As Int32 = 0
            Dim chkSelect As CheckBox = DirectCast(sender, System.Web.UI.WebControls.CheckBox)

            For Each item As GridDataItem In DirectCast(chkSelect.NamingContainer, Telerik.Web.UI.GridDataItem).OwnerTableView.Items()
                If DirectCast(item.FindControl("chkSelect"), System.Web.UI.WebControls.CheckBox).Checked Then
                    selectedPOCount = selectedPOCount + 1
                End If
            Next

            If selectedPOCount < 2 Then
                For Each item As GridDataItem In DirectCast(chkSelect.NamingContainer, Telerik.Web.UI.GridDataItem).OwnerTableView.Items()
                    Dim chkSelectItem As CheckBox = DirectCast(item.FindControl("chkSelect"), System.Web.UI.WebControls.CheckBox)
                    Dim rdbParent As RadioButton = DirectCast(item.FindControl("rdbParent"), System.Web.UI.WebControls.RadioButton)
                    rdbParent.Checked = False
                    rdbParent.Enabled = False
                    chkSelectItem.Enabled = True
                Next
            Else
                For Each item As GridDataItem In DirectCast(chkSelect.NamingContainer, Telerik.Web.UI.GridDataItem).OwnerTableView.Items()
                    Dim rdbParent As RadioButton = DirectCast(item.FindControl("rdbParent"), System.Web.UI.WebControls.RadioButton)
                    If rdbParent.Enabled = False Then
                        rdbParent.Enabled = True
                    End If
                Next
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            litMessage.Text = ex.Message
        End Try
    End Sub
    Protected Sub rdbParent_CheckedChanged(sender As Object, e As EventArgs)
        Try
            For Each item As GridDataItem In DirectCast(DirectCast(sender, RadioButton).NamingContainer, Telerik.Web.UI.GridDataItem).OwnerTableView.Items
                Dim chkSelect As CheckBox = DirectCast(item.FindControl("chkSelect"), CheckBox)
                Dim rdbParent As RadioButton = DirectCast(item.FindControl("rdbParent"), RadioButton)

                If rdbParent.ClientID = DirectCast(sender, RadioButton).ClientID Then
                    chkSelect.Checked = True
                    chkSelect.Enabled = False
                End If

                If rdbParent.ClientID <> DirectCast(sender, RadioButton).ClientID AndAlso rdbParent.Checked Then
                    rdbParent.Checked = False
                    chkSelect.Enabled = True
                End If
            Next
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            litMessage.Text = ex.Message
        End Try
    End Sub
    Protected Sub chkSelectAll_CheckedChanged(sender As Object, e As EventArgs)
        Try
            Dim chkSelectAll As CheckBox = DirectCast(sender, System.Web.UI.WebControls.CheckBox)
            For Each item As GridDataItem In DirectCast(chkSelectAll.NamingContainer, Telerik.Web.UI.GridHeaderItem).OwnerTableView.Items
                Dim chkSelect As CheckBox = DirectCast(item.FindControl("chkSelect"), CheckBox)
                Dim rdbParent As RadioButton = DirectCast(item.FindControl("rdbParent"), RadioButton)
                chkSelect.Enabled = True
                chkSelect.Checked = chkSelectAll.Checked
                rdbParent.Enabled = chkSelectAll.Checked

                If Not chkSelectAll.Checked Then
                    rdbParent.Checked = False
                End If
            Next
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            litMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub btnStop_Click(sender As Object, e As EventArgs) Handles btnStop.Click
        Try
            Threading.Thread.Sleep(5000)
            Dim strSelectedOrders As String = ""
            For Each item As GridDataItem In RadGridPO.Items
                If item.OwnerTableView.Name = "Orders" Then
                    If DirectCast(item.FindControl("chkSelect"), CheckBox).Checked Then
                        strSelectedOrders = strSelectedOrders & item.GetDataKeyValue("POID") & ","
                    End If
                End If
            Next

            If Not String.IsNullOrEmpty(strSelectedOrders) Then
                objOpp.DomainID = CCommon.ToLong(Session("DomainID"))
                objOpp.MarkAsDoNotMerge(strSelectedOrders.Trim(","))
            End If

            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            litMessage.Text = ex.Message
        End Try
    End Sub
    Private Sub btnMerge_Click(sender As Object, e As EventArgs) Handles btnMerge.Click
        Try
            Dim bitValid As Boolean = True
            Dim bitNoRecordsSelected As Boolean = True
            Dim listVendors As New List(Of VendorPO)

            'VENDOR ROWS
            For Each vendorItem As GridDataItem In RadGridPO.MasterTableView.Items
                Dim vendor As New VendorPO
                vendor.VendorID = vendorItem.GetDataKeyValue("VendorID")

                'PURCHASE ORDER ROWS
                For Each orderItem As GridDataItem In vendorItem.ChildItem.NestedTableViews(0).Items
                    Dim poID As Long = orderItem.GetDataKeyValue("POID")
                    Dim chkSelectItem As CheckBox = DirectCast(orderItem.FindControl("chkSelect"), System.Web.UI.WebControls.CheckBox)
                    Dim rdbParent As RadioButton = DirectCast(orderItem.FindControl("rdbParent"), System.Web.UI.WebControls.RadioButton)

                    If chkSelectItem.Checked Then
                        vendor.SelectedPOs.Add(poID)
                        vendor.SelectedPOCount = vendor.SelectedPOCount + 1
                    End If

                    If rdbParent.Checked Then
                        vendor.bitMasterPOSelected = True
                        vendor.MasterPOID = poID
                    End If
                Next

                listVendors.Add(vendor)
            Next

            If listVendors.Count > 0 Then
                'VALIDATE SELECTED DATA
                For Each vendor As VendorPO In listVendors

                    'CHECK IF SOMETHING IS SELECTED OR NOT
                    If vendor.SelectedPOCount > 0 Or vendor.bitMasterPOSelected Then
                        bitNoRecordsSelected = False
                    End If

                    'ATLEAST 2 PO SHOULD BE SELECTED
                    If vendor.SelectedPOCount = 1 Then
                        bitValid = False
                        'IF MORE THAN 1 POs ARE SELECTED THAN MASTER PURCHASE ORDER MUST BE SELECTED
                    ElseIf vendor.SelectedPOCount > 1 AndAlso Not vendor.bitMasterPOSelected Then
                        bitValid = False
                        'IF MASTER PURCHASE ORDER IS SELECTED AND 2 POS ARE NOT SELECTED
                    ElseIf vendor.bitMasterPOSelected AndAlso vendor.SelectedPOCount < 2 Then
                        bitValid = False
                    End If
                Next

                'NO RECORDS ARE SELECTED FROM ANY VENDOR GROUP
                If bitNoRecordsSelected Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "NoVendorAvilable", "alert('Please select purchase orders to merge.')", True)
                Else
                    If bitValid Then
                        'GENERATES XML FROM LIST
                        Dim strXml = New XElement("Vendors", _
                                        From vendor In listVendors.Where(Function(x) x.bitMasterPOSelected AndAlso x.SelectedPOCount > 1) _
                                        Select New XElement("Vendor", _
                                        New XElement("VendorID", vendor.VendorID), _
                                        New XElement("MasterPOID", vendor.MasterPOID), _
                                        New XElement("Orders", _
                                        From orderId In vendor.SelectedPOs _
                                        Select New XElement("OrderID", orderId))
                                        ))


                        'MERGE PO
                        objOpp.DomainID = CCommon.ToLong(Session("DomainID"))
                        objOpp.UserCntID = CCommon.ToLong(Session("UserContactID"))
                        objOpp.MergeVendorPurchaseOrder(strXml.ToString(SaveOptions.DisableFormatting))

                        'REBING GRID
                        BindGrid()
                    Else
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "InvalidSelection", "alert('Records are not properly selected. Please make sure atleast 2 purchase orders and 1 master purchase order are selected in vendor group.')", True)
                    End If
                End If

            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "NoVendorAvilable", "alert('Please select purchase orders to merge.')", True)
            End If
        Catch ex As Exception
            If ex.Message.Contains("CASE DEPENDANT") Then
                litMessage.Text = "Dependent case exists. Cannot be Deleted."
            ElseIf ex.Message.Contains("CreditBalance DEPENDANT") Then
                litMessage.Text = "Credit balance of current order is being used. Your option is used to credit balance of Organization from Accunting sub tab."
            ElseIf ex.Message.Contains("OppItems DEPENDANT") Then
                litMessage.Text = "Items already used. Cannot be Deleted."
            ElseIf ex.Message.Contains("DEPENDANT") Then
                litMessage.Text = "Can not remove record, Your option is to remove Time and Expense associated with all stages from ""milestone & stages"" sub-tab and try again."
            ElseIf ex.Message.Contains("RECURRING ORDER OR BIZDOC") Then
                litMessage.Text = "Order can not be deleted because recurrence is set on order or on bizdoc within order."
            ElseIf ex.Message.Contains("RECURRED ORDER") Then
                litMessage.Text = "Can not remove record because it is recurred from another order"
            ElseIf ex.Message.Contains("FY_CLOSED") Then
                litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                litMessage.Text = ex.Message
            End If
        End Try
    End Sub

    Private Sub RadGridPO_PreRender(sender As Object, e As EventArgs) Handles RadGridPO.PreRender
        Try
            If Not Page.IsPostBack Then
                RadGridPO.MasterTableView.Items(0).Expanded = True
                RadGridPO.MasterTableView.Items(0).ChildItem.NestedTableViews(0).Items(0).Expanded = True
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            litMessage.Text = ex.Message
        End Try
    End Sub
    Private Sub RadGridPO_DetailTableDataBind(sender As Object, e As Telerik.Web.UI.GridDetailTableDataBindEventArgs) Handles RadGridPO.DetailTableDataBind
        Try
            Dim dataItem As GridDataItem = DirectCast(e.DetailTableView.ParentItem, GridDataItem)
            Select Case e.DetailTableView.Name
                Case "Orders"
                    If True Then
                        objOpp.DomainID = CCommon.ToLong(Session("DomainID"))
                        Dim vendorID As String = dataItem.GetDataKeyValue("VendorID").ToString()
                        e.DetailTableView.DataSource = objOpp.GetPOAvailableForMerge(1, CCommon.ToLong(vendorID), 0)
                        Exit Select
                    End If

                Case "Items"
                    If True Then
                        objOpp.DomainID = CCommon.ToLong(Session("DomainID"))
                        Dim poID As String = dataItem.GetDataKeyValue("POID").ToString()
                        e.DetailTableView.DataSource = objOpp.GetPOAvailableForMerge(2, 0, CCommon.ToLong(poID))
                        Exit Select
                    End If
            End Select
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            litMessage.Text = ex.Message
        End Try
    End Sub
#End Region

    Private Class VendorPO
        Public Property VendorID As Long
        Public Property SelectedPOs As New List(Of Long)
        Public Property SelectedPOCount As Int32
        Public Property MasterPOID As Long
        Public Property bitMasterPOSelected As Boolean
    End Class
End Class