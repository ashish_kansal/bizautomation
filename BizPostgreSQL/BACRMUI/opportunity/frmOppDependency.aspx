<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmOppDependency.aspx.vb"
    Inherits="BACRM.UserInterface.Opportunities.frmOppDependency" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Dependency</title>

    <script language="javascript">
        function Close() {
            window.close();
        }
        function Add() {
            if (document.Form1.ddlProcessList.value == 0) {
                alert("Select Process List ")
                document.Form1.ddlProcessList.focus();
                return false;
            }
            if (document.Form1.ddlMilestone.value == 0) {
                alert("Select Milestone ")
                document.Form1.ddlMilestone.focus();
                return false;
            }
            if (document.Form1.ddlStage.value == 0) {
                alert("Select Stage ")
                document.Form1.ddlStage.focus();
                return false;
            }
            for (i = 0; i < document.Form1.elements.length; i++) {
                if (document.getElementById('dgDependency__ctl' + i + '_txtStageId') != null) {
                    if (document.getElementById('dgDependency__ctl' + i + '_txtStageId').value == document.Form1.ddlStage.value) {
                        alert("Dependency Stage is already Added");
                        return false;
                    }
                }
            }
        }
        function Save() {
            if (document.getElementById('dgDependency') != null) {
                if (document.getElementById('dgDependency').rows.length == 1) {
                    alert("Select atleast one Dependency");
                    return false;
                }
            }
            if (document.getElementById('dgDependency') == null) {
                alert("Select atleast one Dependency");
                return false;
            }
        }
    </script>

</head>
<body>
    <form id="Form1" method="post" runat="server">
    <table width="100%">
        <tr>
            <td align="right" colspan="2">
                <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"></asp:Button>
                <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close">
                </asp:Button>
                <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button><br>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Choose Process List
            </td>
            <td>
                <asp:DropDownList ID="ddlProcessList" AutoPostBack="True" runat="server" Width="250"
                    CssClass="signup">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Choose Milestone
            </td>
            <td>
                <asp:DropDownList ID="ddlMilestone" AutoPostBack="True" runat="server" Width="250"
                    CssClass="signup">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Choose Stage
            </td>
            <td>
                <asp:DropDownList ID="ddlStage" runat="server" Width="220" CssClass="signup">
                </asp:DropDownList>
                <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add"></asp:Button>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:DataGrid ID="dgDependency" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
                    >
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn HeaderText="Process List" DataField="vcPOppName"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Milestone" DataField="numstagepercentage"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Stage" DataField="vcstagedetail" ItemStyle-Width="100">
                        </asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Status" DataField="Status"></asp:BoundColumn>
                        <asp:TemplateColumn>
                            <HeaderTemplate>
                                <asp:Button ID="btnHdeletePurchase" runat="server" CssClass="button Delete" Text="X" ></asp:Button>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:TextBox ID="txtStageId" Style="display: none" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "numProcessStageId") %>'>
                                </asp:TextBox>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete">
                                </asp:Button>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
