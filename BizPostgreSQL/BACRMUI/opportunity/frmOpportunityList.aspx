<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmOpportunityList.aspx.vb"
    Inherits="BACRM.UserInterface.Opportunities.frmOpportunityList" MasterPageFile="~/common/GridMaster.Master" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>
        <asp:Literal ID="litTitle" runat="server"></asp:Literal>
    </title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <script type="text/javascript">

        function OpenItemListForPendingApproval(a) {
            $(".imgAUnItem").attr("alt", "");
            $(".imgAUnItem").attr("title", "");
            $.ajax({
                type: "POST",
                url: window.location.pathname + "/FillPendingApproval",
                data: JSON.stringify({ "OppId": '<%#Session("DomainID")%>' }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                error:
          function (XMLHttpRequest, textStatus, errorThrown) {
              console.log("Error");
          },
                success: function (result) {
                    $(".imgAUnItem").attr("alt", "Pending Price Approval from : "+result.d);
                    $(".imgAUnItem").attr("title", "Pending Price Approval from : " + result.d);
                }
            });
        }
        function OpenPromotionApprover(a) {
            $(".imgPromotionApproval").attr("alt", "");
            $(".imgPromotionApproval").attr("title", "");
            $.ajax({
                type: "POST",
                url: window.location.pathname + "/FillApproverPromotionApproval",
                data: JSON.stringify({ "OppId": a }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                error:
          function (XMLHttpRequest, textStatus, errorThrown) {
              console.log("Error");
          },
                success: function (result) {
                    $(".imgPromotionApproval").attr("alt", "Pending Promotion Approval from : "+result.d);
                    $(".imgPromotionApproval").attr("title", "Pending Promotion Approval from : " + result.d);
                }
            });
        }
        
        
        function DeleteRecord() {
            var RecordIDs = GetCheckedRowValues()
            document.getElementById('txtDelOppId').value = RecordIDs;
            if (RecordIDs.length > 0)
                return true
            else {
                alert('Please select atleast one record!!');
                return false
            }
        }

        function OpenWindow(a, b, c) {
            var str;
            if (b == 0) {

                str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylistDivID=" + a;

            }
            else if (b == 1) {

                str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylist&DivID=" + a;

            }
            else if (b == 2) {

                str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylist&klds+7kldf=fjk-las&DivId=" + a;

            }

            document.location.href = str;

        }
        
        function OpenContact(a, b) {

            var str;

            str = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylist&ft6ty=oiuy&CntId=" + a;


            document.location.href = str;

        }

        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function SelWarOpenDeals(a) {
            window.open("../opportunity/frmSelWhouseOpenDls.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Opid=" + a)
            return false;
        }
        function OpenSetting() {
            window.open('../Opportunity/frmConfOppList.aspx?FormId=' + document.getElementById("txtFormID").value, '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=400,scrollbars=yes,resizable=yes')
            return false
        }
        function OpenOpp(a, b) {

            var str;

            str = "../opportunity/frmOpportunities.aspx?frm=opportunitylist&OpID=" + a;

            document.location.href = str;
        }
        //var prg_width = 60;
        function progress(ProgInPercent, Container, progress, prg_width) {
            //Set Total Width
            var OuterDiv = document.getElementById(Container);
            OuterDiv.style.width = prg_width + 'px';
            OuterDiv.style.height = '5px';

            if (ProgInPercent > 100) {
                ProgInPercent = 100;
            }
            //Set Progress Percentage
            var node = document.getElementById(progress);
            node.style.width = parseInt(ProgInPercent) * prg_width / 100 + 'px';
        }

        function OpenBizInvoice(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
        }

        function OpenMirrorBizDoc(a, b) {
            window.open('../opportunity/frmMirrorBizDoc.aspx?RefID=' + a + '&RefType=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=1000,height=600,scrollbars=yes,resizable=yes');
        }

        function OrderSearchClientSelectedIndexChanged(sender, eventArgs) {
            $("[id$=btnGotoRecord]").click();
        }
        function OpenHelp() {
            window.open('../Help/Initial_Grid_Opportunities_Orders_.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div id="divMessage" runat="server" class="row padbottom10" style="display: none">
        <div class="col-xs-12">
            <div class="alert alert-warning">
                <h4><i class="icon fa fa-warning"></i>Alert!</h4>
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-md-4">
            <div class="form-inline">
                <label>
                    View</label>
                <span id="pnlSales" runat="server">
                    <asp:DropDownList ID="ddlSortSales" runat="server" CssClass="form-control" AutoPostBack="True">
                        <asp:ListItem Value="1">My Sales Opportunities</asp:ListItem>
                        <asp:ListItem Value="3">All Sales Opportunities</asp:ListItem>
                        <asp:ListItem Value="2">My Subordinates</asp:ListItem>
                        <asp:ListItem Value="4">Added in Last 7 days</asp:ListItem>
                        <asp:ListItem Value="5">Last 20 Added by me</asp:ListItem>
                        <asp:ListItem Value="6">Last 20 Modified by me</asp:ListItem>
                    </asp:DropDownList>
                </span>
                <span id="pnlPurchase" runat="server">
                    <asp:DropDownList ID="ddlSortPurchase" runat="server" CssClass="form-control" AutoPostBack="True">
                        <asp:ListItem Value="1">My Purchase Opportunities</asp:ListItem>
                        <asp:ListItem Value="3">All Purchase Opportunities</asp:ListItem>
                        <asp:ListItem Value="2">My Subordinates</asp:ListItem>
                        <asp:ListItem Value="4">Added in Last 7 days</asp:ListItem>
                        <asp:ListItem Value="5">Last 20 Added by me</asp:ListItem>
                        <asp:ListItem Value="6">Last 20 Modified by me</asp:ListItem>
                    </asp:DropDownList>
                </span>
            </div>
        </div>
        <div class="col-md-8">
            <div class="pull-right">
                <div class="form-inline">
                    <asp:RadioButton ID="radOpen" GroupName="radOppStatus" AutoPostBack="true" Checked="true" runat="server" Text="Open" />
                    <asp:RadioButton ID="radLost" GroupName="radOppStatus" AutoPostBack="true" runat="server" Text="Lost" />
                    <telerik:radcombobox id="radCmbSearch" runat="server" dropdownwidth="600px" width="250" Style="display:none;"
                        skin="Default" allowcustomtext="false" showmoreresultsbox="true" enableloadondemand="true"
                        enablescreenboundarydetection="true" itemsperrequest="10" enablevirtualscrolling="true"
                        onitemsrequested="radCmbSearch_ItemsRequested" highlighttemplateditems="true" clientidmode="Static"
                        tabindex="503" maxheight="230" onclientselectedindexchanged="OrderSearchClientSelectedIndexChanged">
                    </telerik:radcombobox>
                    <asp:Button ID="btnGo" CssClass="btn btn-primary" runat="server" Text="Go" Style="display:none;"></asp:Button>
                    <asp:LinkButton ID="btnAddActionItem" runat="server" CssClass="btn btn-primary" Visible="false"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Add Action Item</asp:LinkButton>
                    <asp:LinkButton ID="btnSendAnnounceMent" runat="server" CssClass="btn btn-primary" Visible="false"><i class="fa fa-envelope"></i>&nbsp;&nbsp;Send Announcement</asp:LinkButton>
                    <button id="btnAddNewOpportunity" class="btn btn-primary" runat="server" ><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New</button>
                    <asp:LinkButton ID="btnDelete" OnClientClick="return DeleteRecord()" CssClass="btn btn-danger" runat="server"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
                    <asp:Button ID="btnGotoRecord" runat="server" Style="display: none;" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblOpportunity" runat="server"></asp:Label>&nbsp;<a href="#" id="aHelp" runat="server"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvSearch" runat="server" EnableViewState="true" AutoGenerateColumns="false"
                    CssClass="table table-bordered table-striped" Width="99%" ShowHeaderWhenEmpty="true">
                    <Columns>
                    </Columns>
                    <EmptyDataTemplate>
                        No records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </div>
    </div>

    <asp:TextBox ID="txtDelOppId" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPageSales" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecordsSales" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSelIndex" runat="server" Text="0" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtFormID" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
