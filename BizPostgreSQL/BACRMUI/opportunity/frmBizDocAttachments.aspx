<%@ Page Language="vb" AutoEventWireup="false" AspCompat="true" CodeBehind="frmBizDocAttachments.aspx.vb"
    Inherits=".frmBizDocAttachments" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>BizDoc Attachments</title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <script language="javascript">
        function Close() {
            window.close();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSort" runat="server" CssClass="button" Text="Sort Documents">
            </asp:Button>
            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add Documents"></asp:Button>
            <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button><br>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    BizDoc Attachments
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="500px">
        <tr>
            <td class="normal1" align="right" width="10%">
                BizDoc
            </td>
            <td width="15%">
                <asp:DropDownList ID="ddlBizDoc" runat="server" AutoPostBack="true" CssClass="signup">
                </asp:DropDownList>
            </td>
            <td class="normal1" align="right" width="10%">
                Template
            </td>
            <td width="65%">
                <asp:DropDownList ID="ddlBizDocTemplate" runat="server" CssClass="signup" AutoPostBack="true">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <br />
    <asp:DataGrid ID="dgAttachments" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
        >
        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
        <ItemStyle CssClass="is"></ItemStyle>
        <HeaderStyle CssClass="hs"></HeaderStyle>
        <Columns>
            <asp:BoundColumn DataField="numAttachmntID" Visible="false"></asp:BoundColumn>
            <asp:BoundColumn DataField="vcURL" Visible="false"></asp:BoundColumn>
            <asp:BoundColumn DataField="vcDocName" Visible="false"></asp:BoundColumn>
            <asp:TemplateColumn HeaderText="Document Name">
                <ItemTemplate>
                    <asp:HyperLink ID="hplDocument" runat="server" CssClass="hyperlink"></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn>
                <ItemTemplate>
                    <asp:HyperLink ID="hplOpenLink" runat="server" Target="_blank" CssClass="hyperlink"
                        Text="Open Link"></asp:HyperLink>
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn>
                <ItemTemplate>
                    <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete">
                    </asp:Button>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
    </asp:DataGrid>
</asp:Content>
