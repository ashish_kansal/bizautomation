﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Opportunities
    Partial Public Class frmChildOpportunity
        : Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    If GetQueryStringVal("OppId") <> "" Then
                        BindGrid()

                    End If
                End If
                
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Sub BindGrid()
            Try
                Dim objOpportunity As New MOpportunity
                With objOpportunity
                    .DomainID = Session("DomainID")
                    .OpportunityId = GetQueryStringVal( "OppId")
                End With
                gvChildOpps.DataSource = objOpportunity.GetChildOpportunity()
                gvChildOpps.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
    End Class
End Namespace