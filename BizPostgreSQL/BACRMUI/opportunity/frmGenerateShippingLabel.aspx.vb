﻿Imports BACRM.BusinessLogic.Common
Imports Microsoft.Web.UI.WebControls
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contacts
Imports Telerik.WebControls
Imports Telerik.Web.UI

Partial Public Class frmGenerateShippingLabel
    Inherits BACRMPage
    Dim lngOppBizDocID As Long
    Dim lngOppID As Long
    Dim lngShippingService As Long
    Dim lngShippingCompany As Long
    Dim lngShippingReportId As Long
    Dim objCommon As CCommon
    Dim objOppBizDocs As OppBizDocs
    Dim specialService As Long = 0
    Dim sMode As Short
    Dim dtShipService As DataTable
    Dim IsDomestic As Boolean = True
    Dim dsBoxes As DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngOppID = CCommon.ToLong(GetQueryStringVal("OppId"))
            lngOppBizDocID = CCommon.ToLong(GetQueryStringVal("OppBizDocId"))
            lngShippingService = CCommon.ToLong(GetQueryStringVal("ShipServiceID"))
            lngShippingCompany = CCommon.ToLong(GetQueryStringVal("ShipCompID"))

            If lngShippingCompany >= 10 And lngShippingCompany <= 28 Then
                lngShippingCompany = 91
            ElseIf (lngShippingCompany = 40 Or
                    lngShippingCompany = 42 Or
                    lngShippingCompany = 43 Or
                    lngShippingCompany = 48 Or
                    lngShippingCompany = 49 Or
                    lngShippingCompany = 50 Or
                    lngShippingCompany = 51 Or
                    lngShippingCompany = 55) Then
                lngShippingCompany = 88
            ElseIf lngShippingCompany >= 70 And lngShippingCompany <= 76 Then
                lngShippingCompany = 90
            End If

            lngShippingReportId = CCommon.ToLong(GetQueryStringVal("ShipReportId"))
            sMode = CCommon.ToShort(GetQueryStringVal("mode"))

            If Not IsPostBack Then
                ViewState("SS") = 0
                BindData()
                LoadShippingDetails()

                hdnShippingCompany.Value = lngShippingCompany
                dtShipService = Nothing
            End If
            'If Request("__EVENTTARGET") = "btnGetRates" Then
            '    radRateSelection.Visible = True
            '    btnAddToShippingReport.Visible = True
            'End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub BindData()
        Try
            If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
            objOppBizDocs.ShippingReportId = lngShippingReportId
            dsBoxes = objOppBizDocs.GetShippingBoxes()
            ViewState("ShippingBox") = dsBoxes
            dsBoxes.Tables(0).TableName = "Box"
            dsBoxes.Tables(1).TableName = "BoxItems"

            radBoxes.DataSource = dsBoxes
            radBoxes.MasterTableView.HierarchyDefaultExpanded = True


            If lngShippingCompany = 91 Then 'fedex
                chkUseDimensions.Checked = True
                chkUseDimensions.Enabled = False
                trDesc.Visible = False
                trSignType.Visible = True

                chkOther.Items.Clear()
                chkOther.Items.Add(New ListItem("COD", "COD"))
                'chkOther.Items.Add(New ListItem("Dry Ice", "Dry Ice"))
                'chkOther.Items.Add(New ListItem("Hold Saturday", "Hold Saturday"))
                chkOther.Items.Add(New ListItem("Home Delivery Premium", "Home Delivery Premium"))
                chkOther.Items.Add(New ListItem("Inside Delivery", "Inside Delivery"))
                chkOther.Items.Add(New ListItem("Inside Pickup", "Inside Pickup"))
                'chkOther.Items.Add(New ListItem("Return Shipment", "Return Shipment"))
                chkOther.Items.Add(New ListItem("Saturday Delivery", "Saturday Delivery"))
                'chkOther.Items.Add(New ListItem("Saturday Pickup", "Saturday Pickup"))

                chkOther.AutoPostBack = True
                ddlCODType.Items.Clear()
                ddlCODType.Items.Add(New ListItem("-- Select One --", "-- Select One --"))
                ddlCODType.Items.Add(New ListItem("Any", "Any"))
                ddlCODType.Items.Add(New ListItem("Cash", "Cash"))
                ddlCODType.Items.Add(New ListItem("Guaranted Funds", "Guaranted Funds"))

            ElseIf lngShippingCompany = 88 Then 'UPS
                ddlCODType.Items.Clear()
                ddlCODType.Items.Add(New ListItem("-- Select One --", "-- Select One --"))
                ddlCODType.Items.Add(New ListItem("Any Check", "Any Check"))
                ddlCODType.Items.Add(New ListItem("Cashier's Check Or Money Order", "Cashier's Check Or Money Order"))
                trCOD1.Visible = True
                trCOD2.Visible = True

                chkOther.Items.Clear()
                chkOther.Items.Add(New ListItem("Saturday Delivery", "Saturday Delivery"))
                chkOther.Items.Add(New ListItem("Saturday Pickup", "Saturday Pickup"))
                chkOther.Items.Add(New ListItem("Additional Handling", "Additional Handling"))
                chkOther.Items.Add(New ListItem("Large Package", "Large Package"))

                chkOther.AutoPostBack = False

                trDesc.Visible = True
                trSignType.Visible = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadShippingDetails()
        Try
            Dim objOpp As New MOpportunity
            objOpp.OpportunityId = lngOppID
            objOpp.Mode = 1
            Dim objContacts As New CContacts
            If objCommon Is Nothing Then objCommon = New CCommon
            objCommon.sb_FillComboFromDBwithSel(ddlFromCountry, 40, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlToCountry, 40, Session("DomainID"))
            objCommon.sb_FillComboFromDBwithSel(ddlThirdPartyCounty, 40, Session("DomainID"))

            Dim objOppBizDoc As New OppBizDocs
            Dim dtShippingInfo As New DataTable
            objOppBizDoc.DomainID = DomainID
            objOppBizDoc.OppBizDocId = lngOppBizDocID
            objOppBizDoc.ShippingReportId = lngShippingReportId
            dtShippingInfo = objOppBizDoc.GetShippingReport().Tables(0)

            If CCommon.ToShort(GetQueryStringVal("View")) = 1 Then
                btnGenShippingLabel.Visible = False

                If dtShippingInfo.Rows.Count > 0 Then
                    If Not IsDBNull(dtShippingInfo.Rows(0).Item("numToCountry")) Then
                        If Not ddlToCountry.Items.FindByValue(dtShippingInfo.Rows(0).Item("numToCountry")) Is Nothing Then
                            ddlToCountry.Items.FindByValue(dtShippingInfo.Rows(0).Item("numToCountry")).Selected = True
                        End If
                    End If
                    If ddlToCountry.SelectedIndex > 0 Then
                        FillState(ddlToState, ddlToCountry.SelectedItem.Value, Session("DomainID"))
                    End If

                    If Not IsDBNull(dtShippingInfo.Rows(0).Item("numToState")) Then
                        If Not ddlToState.Items.FindByValue(dtShippingInfo.Rows(0).Item("numToState")) Is Nothing Then
                            ddlToState.Items.FindByValue(dtShippingInfo.Rows(0).Item("numToState")).Selected = True
                        End If
                    End If

                    txtToName.Text = CCommon.ToString(dtShippingInfo.Rows(0).Item("vcToName"))
                    txtToCompany.Text = CCommon.ToString(dtShippingInfo.Rows(0).Item("vcToCompany"))
                    txtToPhone.Text = CCommon.ToString(dtShippingInfo.Rows(0).Item("vcToPhone"))

                    txtToZip.Text = IIf(Not IsDBNull(dtShippingInfo.Rows(0).Item("vcToZip")), dtShippingInfo.Rows(0).Item("vcToZip"), "")
                    txtToAddressLine1.Text = dtShippingInfo.Rows(0).Item("vcToAddressLine1").ToString()
                    txtToAddressLine2.Text = dtShippingInfo.Rows(0).Item("vcToAddressLine2").ToString()
                    txtToCity.Text = CCommon.ToString(dtShippingInfo.Rows(0).Item("vcToCity"))
                    'Changed Address format Cause: Bug ID 621
                    'If dtShippingInfo.Rows(0).Item("Street").ToString().Contains(Environment.NewLine) Then
                    '    Dim strAddressLines() As String = dtShippingInfo.Rows(0).Item("Street").ToString().Split(New Char() {CChar(Environment.NewLine)}, StringSplitOptions.RemoveEmptyEntries)
                    '    txtToAddressLine1.Text = IIf(strAddressLines.Length >= 0, strAddressLines(0).ToString, "").ToString 'dtShippingInfo.Rows(0).Item("Street").ToString().Substring(0, dtShippingInfo.Rows(0).Item("Street").ToString().IndexOf(Environment.NewLine)) 
                    '    txtToAddressLine2.Text = IIf(strAddressLines.Length >= 1, strAddressLines(1).ToString, "").ToString 'dtShippingInfo.Rows(0).Item("Street").ToString().Substring(dtShippingInfo.Rows(0).Item("Street").ToString().IndexOf(Environment.NewLine)).Replace(Environment.NewLine, " ") 
                    'End If

                    If Not IsDBNull(dtShippingInfo.Rows(0).Item("numFromCountry")) Then
                        If Not ddlFromCountry.Items.FindByValue(dtShippingInfo.Rows(0).Item("numFromCountry")) Is Nothing Then
                            ddlFromCountry.Items.FindByValue(dtShippingInfo.Rows(0).Item("numFromCountry")).Selected = True
                        End If
                    End If
                    If ddlFromCountry.SelectedIndex > 0 Then
                        FillState(ddlFromState, ddlFromCountry.SelectedItem.Value, Session("DomainID"))
                    End If

                    If Not IsDBNull(dtShippingInfo.Rows(0).Item("numFromState")) Then
                        If Not ddlFromState.Items.FindByValue(dtShippingInfo.Rows(0).Item("numFromState")) Is Nothing Then
                            ddlFromState.Items.FindByValue(dtShippingInfo.Rows(0).Item("numFromState")).Selected = True
                        End If
                    End If

                    txtFromName.Text = CCommon.ToString(dtShippingInfo.Rows(0).Item("vcFromName")) '& " " & CCommon.ToString(dtShippingInfo.Rows(0).Item("vcLastname"))
                    txtFromCompany.Text = CCommon.ToString(dtShippingInfo.Rows(0).Item("vcFromCompany"))
                    txtFromPhone.Text = CCommon.ToString(dtShippingInfo.Rows(0).Item("vcFromPhone"))

                    txtFromZip.Text = CCommon.ToString(dtShippingInfo.Rows(0).Item("vcFromZip"))
                    txtFromCity.Text = CCommon.ToString(dtShippingInfo.Rows(0).Item("vcFromCity"))
                    txtFromAddressLine1.Text = CCommon.ToString(dtShippingInfo.Rows(0).Item("vcFromAddressLine1"))
                    txtFromAddressLine2.Text = CCommon.ToString(dtShippingInfo.Rows(0).Item("vcFromAddressLine2"))

                    txtCODTotalAmount.Text = CCommon.ToString(HttpContext.Current.Session("CurrSymbol")) & " " & [String].Format("{0:#,##0.00}", CCommon.ToDouble(dtShippingInfo.Rows(0).Item("numCODAmount")))

                    chkFromResidential.Checked = CCommon.ToBool(dtShippingInfo.Rows(0).Item("bitFromResidential"))
                    chkToResidential.Checked = CCommon.ToBool(dtShippingInfo.Rows(0).Item("bitToResidential"))
                End If

            Else
                Dim dtTable As DataTable
                Dim dtShipFrom As DataTable
                dtTable = objOpp.GetOpportunityAddress
                objContacts.ContactID = Session("UserContactID")
                objContacts.DomainID = Session("DomainID")
                dtShipFrom = objContacts.GetBillOrgorContAdd

                If (dtTable.Rows.Count > 0) Then
                    If Not IsDBNull(dtTable.Rows(0).Item("Country")) Then
                        If Not ddlToCountry.Items.FindByValue(dtTable.Rows(0).Item("Country")) Is Nothing Then
                            ddlToCountry.Items.FindByValue(dtTable.Rows(0).Item("Country")).Selected = True
                        End If
                    End If
                    If ddlToCountry.SelectedIndex > 0 Then
                        FillState(ddlToState, ddlToCountry.SelectedItem.Value, Session("DomainID"))
                    End If

                    If Not IsDBNull(dtTable.Rows(0).Item("State")) Then
                        If Not ddlToState.Items.FindByValue(dtTable.Rows(0).Item("State")) Is Nothing Then
                            ddlToState.Items.FindByValue(dtTable.Rows(0).Item("State")).Selected = True
                        End If
                    End If

                    txtToName.Text = CCommon.ToString(dtTable.Rows(0).Item("Name"))
                    txtToCompany.Text = CCommon.ToString(dtTable.Rows(0).Item("Company"))
                    txtToPhone.Text = CCommon.ToString(dtTable.Rows(0).Item("Phone"))

                    txtToZip.Text = IIf(Not IsDBNull(dtTable.Rows(0).Item("PostCode")), dtTable.Rows(0).Item("PostCode"), "")
                    txtToAddressLine1.Text = dtTable.Rows(0).Item("Street").ToString()
                    txtToCity.Text = dtTable.Rows(0).Item("City").ToString()
                    'Changed Address format Cause: Bug ID 621
                    If dtTable.Rows(0).Item("Street").ToString().Contains(Environment.NewLine) Then
                        Dim strAddressLines() As String = dtTable.Rows(0).Item("Street").ToString().Split(New Char() {CChar(Environment.NewLine)}, StringSplitOptions.RemoveEmptyEntries)
                        txtToAddressLine1.Text = IIf(strAddressLines.Length >= 0, strAddressLines(0).ToString, "").ToString 'dtTable.Rows(0).Item("Street").ToString().Substring(0, dtTable.Rows(0).Item("Street").ToString().IndexOf(Environment.NewLine)) 
                        txtToAddressLine2.Text = IIf(strAddressLines.Length >= 1, strAddressLines(1).ToString, "").ToString 'dtTable.Rows(0).Item("Street").ToString().Substring(dtTable.Rows(0).Item("Street").ToString().IndexOf(Environment.NewLine)).Replace(Environment.NewLine, " ") 
                    End If
                End If
                If dtShipFrom.Rows.Count > 0 Then
                    If Not IsDBNull(dtShipFrom.Rows(0).Item("vcShipCountry")) Then
                        If Not ddlFromCountry.Items.FindByValue(dtShipFrom.Rows(0).Item("vcShipCountry")) Is Nothing Then
                            ddlFromCountry.Items.FindByValue(dtShipFrom.Rows(0).Item("vcShipCountry")).Selected = True
                        End If
                    End If
                    If ddlFromCountry.SelectedIndex > 0 Then
                        FillState(ddlFromState, ddlFromCountry.SelectedItem.Value, Session("DomainID"))
                    End If

                    If Not IsDBNull(dtShipFrom.Rows(0).Item("vcShipState")) Then
                        If Not ddlFromState.Items.FindByValue(dtShipFrom.Rows(0).Item("vcShipState")) Is Nothing Then
                            ddlFromState.Items.FindByValue(dtShipFrom.Rows(0).Item("vcShipState")).Selected = True
                        End If
                    End If

                    txtFromName.Text = CCommon.ToString(dtShipFrom.Rows(0).Item("vcFirstname")) & " " & CCommon.ToString(dtShipFrom.Rows(0).Item("vcLastname"))
                    txtFromCompany.Text = CCommon.ToString(dtShipFrom.Rows(0).Item("vcCompanyName"))
                    txtFromPhone.Text = CCommon.ToString(dtShipFrom.Rows(0).Item("vcPhone"))

                    txtFromZip.Text = IIf(Not IsDBNull(dtShipFrom.Rows(0).Item("vcShipPostCode")), dtShipFrom.Rows(0).Item("vcShipPostCode"), "")
                    txtFromCity.Text = dtShipFrom.Rows(0).Item("vcShipCity").ToString()
                    txtFromAddressLine1.Text = dtShipFrom.Rows(0).Item("vcShipStreet").ToString()
                    'Changed Address format Cause: Bug ID 621
                    If dtShipFrom.Rows(0).Item("vcShipStreet").ToString().Contains(Environment.NewLine) Then
                        txtFromAddressLine2.Text = dtShipFrom.Rows(0).Item("vcShipStreet").ToString().Substring(0, dtShipFrom.Rows(0).Item("vcShipStreet").ToString().IndexOf(Environment.NewLine))
                        txtFromAddressLine1.Text = dtShipFrom.Rows(0).Item("vcShipStreet").ToString().Substring(dtShipFrom.Rows(0).Item("vcShipStreet").ToString().IndexOf(Environment.NewLine)).Replace(Environment.NewLine, " ")
                    End If
                End If
            End If

            If Not ddlThirdPartyCounty.Items.FindByValue(CCommon.ToString(Session("DefCountry"))) Is Nothing Then
                ddlThirdPartyCounty.Items.FindByValue(CCommon.ToString(Session("DefCountry"))).Selected = True
            End If

            If CCommon.ToBool(Session("UseBizdocAmount")) = True Then

                If dtShippingInfo IsNot Nothing AndAlso dtShippingInfo.Rows.Count > 0 Then
                    txtTotalInsuredValue.Text = CCommon.ToDouble(dtShippingInfo.Rows(0)("monTotAmount"))
                    txtTotalCustomsValue.Text = CCommon.ToDouble(dtShippingInfo.Rows(0)("monTotAmount"))
                Else
                    txtTotalInsuredValue.Text = CCommon.ToDouble(Session("TotalInsuredValue"))
                    txtTotalCustomsValue.Text = CCommon.ToDouble(Session("TotalCustomsValue"))
                End If
            Else
                If dtShippingInfo IsNot Nothing AndAlso dtShippingInfo.Rows.Count > 0 Then
                    txtTotalInsuredValue.Text = CCommon.ToDouble(dtShippingInfo.Rows(0)("numTotalInsuredValue"))
                    txtTotalCustomsValue.Text = CCommon.ToDouble(dtShippingInfo.Rows(0)("numTotalCustomsValue"))
                Else
                    txtTotalInsuredValue.Text = CCommon.ToDouble(Session("TotalInsuredValue"))
                    txtTotalCustomsValue.Text = CCommon.ToDouble(Session("TotalCustomsValue"))
                End If
            End If

            If dtShippingInfo IsNot Nothing AndAlso dtShippingInfo.Rows.Count > 0 Then
                txtReferenceNo.Text = CCommon.ToString(dtShippingInfo.Rows(0).Item("numOppID"))
                txtThirdPartyAccountNo.Text = CCommon.ToString(dtShippingInfo.Rows(0).Item("vcPayorAccountNo"))
                txtThirdPartyZipCode.Text = CCommon.ToString(dtShippingInfo.Rows(0).Item("vcPayorZip"))

                If Not ddlThirdPartyCounty.Items.FindByValue(CCommon.ToLong(dtShippingInfo.Rows(0).Item("numPayorCountry"))) Is Nothing Then
                    ddlThirdPartyCounty.Items.FindByValue(CCommon.ToLong(dtShippingInfo.Rows(0).Item("numPayorCountry"))).Selected = True
                End If

                If Not ddlPayorType.Items.FindByValue(CCommon.ToShort(dtShippingInfo.Rows(0).Item("tintPayorType"))) Is Nothing Then
                    ddlPayorType.Items.FindByValue(CCommon.ToShort(dtShippingInfo.Rows(0).Item("tintPayorType"))).Selected = True

                    If CCommon.ToShort(dtShippingInfo.Rows(0).Item("tintPayorType")) <> 0 Then
                        If lngShippingCompany = 91 Then 'fedex
                            txtThirdPartyZipCode.Visible = False
                            lblPayorlabel.Text = "Account No,Country"
                            tdPaymentOption1.Visible = True
                            tdPaymentOption2.Visible = True
                        ElseIf lngShippingCompany = 88 Then 'UPS
                            'Do nothing
                            tdPaymentOption1.Visible = True
                            tdPaymentOption2.Visible = True
                        ElseIf lngShippingCompany = 90 Then 'USPS
                            tdPaymentOption1.Visible = False
                            tdPaymentOption2.Visible = False
                        Else
                            tdPaymentOption1.Visible = False
                            tdPaymentOption2.Visible = False
                        End If
                    End If
                End If

                If trSignType.Visible AndAlso Not ddlSignatureType.Items.FindByValue(CCommon.ToLong(dtShippingInfo.Rows(0).Item("tintSignatureType"))) Is Nothing Then
                    ddlSignatureType.Items.FindByValue(CCommon.ToLong(dtShippingInfo.Rows(0).Item("tintSignatureType"))).Selected = True
                End If

                If Not ddlCODType.Items.FindByText(CCommon.ToString(dtShippingInfo.Rows(0).Item("vcCODType"))) Is Nothing Then
                    ddlCODType.Items.FindByValue(CCommon.ToString(dtShippingInfo.Rows(0).Item("vcCODType"))).Selected = True
                End If

                If trDesc.Visible Then
                    txtDescription.Text = CCommon.ToString(dtShippingInfo.Rows(0).Item("vcDescription"))
                End If

                If trCOD1.Visible AndAlso Not ddlCODType.Items.FindByValue(dtShippingInfo.Rows(0).Item("vcCODType")) Is Nothing Then
                    ddlCODType.Items.FindByValue(dtShippingInfo.Rows(0).Item("vcCODType")).Selected = True
                End If

                If chkOther.Items.FindByText("COD") IsNot Nothing Then chkOther.Items.FindByText("COD").Selected = CCommon.ToBool(dtShippingInfo.Rows(0).Item("IsCOD"))
                If chkOther.Items.FindByText("Dry Ice") IsNot Nothing Then chkOther.Items.FindByText("Dry Ice").Selected = CCommon.ToBool(dtShippingInfo.Rows(0).Item("IsDryIce"))
                If chkOther.Items.FindByText("Hold Saturday") IsNot Nothing Then chkOther.Items.FindByText("Hold Saturday").Selected = CCommon.ToBool(dtShippingInfo.Rows(0).Item("IsHoldSaturday"))
                If chkOther.Items.FindByText("Home Delivery Premium") IsNot Nothing Then chkOther.Items.FindByText("Home Delivery Premium").Selected = CCommon.ToBool(dtShippingInfo.Rows(0).Item("IsHomeDelivery"))
                If chkOther.Items.FindByText("Inside Delivery") IsNot Nothing Then chkOther.Items.FindByText("Inside Delivery").Selected = CCommon.ToBool(dtShippingInfo.Rows(0).Item("IsInsideDelevery"))
                If chkOther.Items.FindByText("Inside Pickup") IsNot Nothing Then chkOther.Items.FindByText("Inside Pickup").Selected = CCommon.ToBool(dtShippingInfo.Rows(0).Item("IsInsidePickup"))
                If chkOther.Items.FindByText("Return Shipment") IsNot Nothing Then chkOther.Items.FindByText("Return Shipment").Selected = CCommon.ToBool(dtShippingInfo.Rows(0).Item("IsReturnShipment"))
                If chkOther.Items.FindByText("Saturday Delivery") IsNot Nothing Then chkOther.Items.FindByText("Saturday Delivery").Selected = CCommon.ToBool(dtShippingInfo.Rows(0).Item("IsSaturdayDelivery"))
                If chkOther.Items.FindByText("Saturday Pickup") IsNot Nothing Then chkOther.Items.FindByText("Saturday Pickup").Selected = CCommon.ToBool(dtShippingInfo.Rows(0).Item("IsSaturdayPickup"))
                If chkOther.Items.FindByText("Additional Handling") IsNot Nothing Then chkOther.Items.FindByText("Additional Handling").Selected = CCommon.ToBool(dtShippingInfo.Rows(0).Item("IsAdditionalHandling"))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnGenShippingLabel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGenShippingLabel.Click
        Try
            If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
            With objOppBizDocs
                .OppId = lngOppID
                .ShippingReportId = lngShippingReportId
                .DomainID = Session("DomainID")
                .OppBizDocId = lngOppBizDocID
                .ShipCompany = lngShippingCompany
                .FromState = ddlFromState.SelectedValue
                .FromZip = txtFromZip.Text.Trim()
                .FromCountry = ddlFromCountry.SelectedValue
                .ToState = ddlToState.SelectedValue
                .ToZip = txtToZip.Text.Trim()
                .ToCountry = ddlToCountry.SelectedValue

                .UserCntID = Session("UserContactID")
                .strText = ""

                .PayorType = ddlPayorType.SelectedValue
                .PayorAccountNo = txtThirdPartyAccountNo.Text.Trim()
                .PayorCountryCode = ddlThirdPartyCounty.SelectedValue
                .PayorZipCode = txtThirdPartyZipCode.Text.Trim()

                .FromCity = txtFromCity.Text.Trim()
                .FromAddressLine1 = txtFromAddressLine1.Text.Trim()
                .FromAddressLine2 = txtFromAddressLine2.Text.Trim()

                .ToCity = txtToCity.Text.Trim()
                .ToAddressLine1 = txtToAddressLine1.Text.Trim()
                .ToAddressLine2 = txtToAddressLine2.Text.Trim()

                .FromName = txtFromName.Text
                .FromCompany = txtFromCompany.Text
                .FromPhone = txtFromPhone.Text
                .IsFromResidential = chkFromResidential.Checked

                .ToName = txtToName.Text
                .ToCompany = txtToCompany.Text
                .ToPhone = txtToPhone.Text
                .IsToResidential = chkToResidential.Checked

                .IsCOD = If(chkOther.Items.FindByText("COD") Is Nothing, False, chkOther.Items.FindByText("COD").Selected)
                .IsDryIce = If(chkOther.Items.FindByText("Dry Ice") Is Nothing, False, chkOther.Items.FindByText("Dry Ice").Selected)
                .IsHoldSaturday = If(chkOther.Items.FindByText("Hold Saturday") Is Nothing, False, chkOther.Items.FindByText("Hold Saturday").Selected)
                .IsHomeDelivery = If(chkOther.Items.FindByText("Home Delivery Premium") Is Nothing, False, chkOther.Items.FindByText("Home Delivery Premium").Selected)
                .IsInsideDelivery = If(chkOther.Items.FindByText("Inside Delivery") Is Nothing, False, chkOther.Items.FindByText("Inside Delivery").Selected)
                .IsInsidePickup = If(chkOther.Items.FindByText("Inside Pickup") Is Nothing, False, chkOther.Items.FindByText("Inside Pickup").Selected)
                .IsReturnShipment = If(chkOther.Items.FindByText("Return Shipment") Is Nothing, False, chkOther.Items.FindByText("Return Shipment").Selected)
                .IsSaturdayDelivery = If(chkOther.Items.FindByText("Saturday Delivery") Is Nothing, False, chkOther.Items.FindByText("Saturday Delivery").Selected)
                .IsSaturdayPickup = If(chkOther.Items.FindByText("Saturday Pickup") Is Nothing, False, chkOther.Items.FindByText("Saturday Pickup").Selected)
                .IsAdditionalHandling = If(chkOther.Items.FindByText("Additional Handling") Is Nothing, False, chkOther.Items.FindByText("Additional Handling").Selected)
                .CODAmount = txtCODTotalAmount.Text

                If ddlCODType.SelectedItem IsNot Nothing AndAlso ddlCODType.SelectedIndex > 0 Then
                    .CODType = ddlCODType.SelectedItem.Text
                ElseIf Not ddlCODType.Items.FindByValue("Any") Is Nothing Then
                    .CODType = ddlCODType.Items.FindByValue("Any").Value
                End If
                .SignatureType = CCommon.ToLong(ddlSignatureType.SelectedValue)
                .LargePackage = If(chkOther.Items.FindByText("Large Package") Is Nothing, False, chkOther.Items.FindByText("Large Package").Selected)
                .Description = txtDescription.Text

                .TotalInsuredValue = If(CCommon.ToDouble(txtTotalInsuredValue.Text) = 0, CCommon.ToDouble(Session("TotalInsuredValue")), CCommon.ToDouble(txtTotalInsuredValue.Text))
                .TotalCustomsValue = If(CCommon.ToDouble(txtTotalCustomsValue.Text) = 0, CCommon.ToDouble(Session("TotalCustomsValue")), CCommon.ToDouble(txtTotalCustomsValue.Text))

                'Setup IsDomestic as per given address
                If CCommon.ToInteger(ddlFromCountry.SelectedValue) = CCommon.ToInteger(ddlToCountry.SelectedValue) Then
                    IsDomestic = True
                    '.TotalInsuredValue = 0
                    '.TotalCustomsValue = 0
                Else
                    IsDomestic = False
                End If

                .ManageShippingReport() 'Update Shipping report address values and create shipping report items 
            End With

            'Save Box Dimentions and weight and service type
            SaveBoxDimension()

            Dim objShipping As New Shipping
            With objShipping
                .DomainID = Session("DomainID")
                .UserCntID = Session("UserContactID")
                .ShippingReportId = lngShippingReportId
                .OpportunityId = lngOppID
                .OppBizDocId = lngOppBizDocID
                .InvoiceNo = lngOppBizDocID ' Gives error when we pass vcBizDocId ->customerReference value length must not exceed 40 characters for customer reference in requestedPackage 1 
                .PayorType = CType(ddlPayorType.SelectedValue, Short)
                .PayorAccountNo = txtThirdPartyAccountNo.Text.Trim()
                .PayorCountryCode = ddlThirdPartyCounty.SelectedValue
                .PayorZipCode = txtThirdPartyZipCode.Text.Trim()
                .CODAmount = txtCODTotalAmount.Text
                .CODType = If(ddlCODType.SelectedItem IsNot Nothing AndAlso ddlCODType.SelectedIndex > 0, ddlCODType.SelectedItem.Text, If(Not ddlCODType.Items.FindByValue("Any") Is Nothing, ddlCODType.Items.FindByValue("Any").Value, ""))

                .TotalInsuredValue = If(CCommon.ToDouble(txtTotalInsuredValue.Text) = 0, CCommon.ToDouble(Session("TotalInsuredValue")), CCommon.ToDouble(txtTotalInsuredValue.Text))
                .TotalCustomsValue = If(CCommon.ToDouble(txtTotalCustomsValue.Text) = 0, CCommon.ToDouble(Session("TotalCustomsValue")), CCommon.ToDouble(txtTotalCustomsValue.Text))
                .ImageWidth = CCommon.ToInteger(Session("ShippingImageWidth"))
                .ImageHeight = CCommon.ToInteger(Session("ShippingImageHeight"))
                .IsGetRates = False
                If IsDomestic = True Then
                    IsDomestic = True
                End If

                For i As Integer = 0 To radBoxes.MasterTableView.Items.Count - 1

                    objShipping.PackagingType = CCommon.ToShort(DirectCast(radBoxes.MasterTableView.Items(i).FindControl("ddlPackagingType"), DropDownList).SelectedValue)
                    objShipping.ServiceType = CCommon.ToShort(DirectCast(radBoxes.MasterTableView.Items(i).FindControl("ddlServiceType"), DropDownList).SelectedValue)
                    objShipping.PayorType = CCommon.ToShort(DirectCast(radBoxes.MasterTableView.Items(i).FindControl("hdnPayerType"), HiddenField).Value)
                    objShipping.PayorAccountNo = CCommon.ToString(DirectCast(radBoxes.MasterTableView.Items(i).FindControl("hdnPayerAccountNo"), HiddenField).Value)
                    objShipping.PayorCountryCode = CCommon.ToString(DirectCast(radBoxes.MasterTableView.Items(i).FindControl("hdnPayerCountryCode"), HiddenField).Value)
                    objShipping.PayorZipCode = CCommon.ToString(DirectCast(radBoxes.MasterTableView.Items(i).FindControl("hdnPayorZipCode"), HiddenField).Value)
                    objShipping.IsGetRates = False

                    If chkUseDimensions.Checked Then
                        .Height = CCommon.ToDouble(DirectCast(radBoxes.MasterTableView.Items(i).FindControl("txtHeight"), TextBox).Text)
                        .Length = CCommon.ToDouble(DirectCast(radBoxes.MasterTableView.Items(i).FindControl("txtLength"), TextBox).Text)
                        .Width = CCommon.ToDouble(DirectCast(radBoxes.MasterTableView.Items(i).FindControl("txtWidth"), TextBox).Text)
                        .UseDimentions = True
                    Else
                        .UseDimentions = False
                    End If

                    .WeightInLbs = CCommon.ToDouble(DirectCast(radBoxes.MasterTableView.Items(i).FindControl("txtTotalWeight"), TextBox).Text)
                    .WeightInOunce = CCommon.ToDouble(DirectCast(radBoxes.MasterTableView.Items(i).FindControl("txtTotalWeightInOunce"), TextBox).Text)
                    If .WeightInOunce <= 0 Then
                        .WeightInOunce = .WeightInLbs * 16
                    End If

                    If .WeightInLbs = 0 Then
                        litMessage.Text = litMessage.Text & "<p>Please specify weight of box for BoxID :" & CCommon.ToLong(radBoxes.MasterTableView.Items(i).GetDataKeyValue("numBoxID")) & ".</p>"
                        Continue For
                    End If

                    .BoxID = CCommon.ToLong(radBoxes.MasterTableView.Items(i).GetDataKeyValue("numBoxID"))
                    .Provider = OppBizDocs.GetServiceTypes(CCommon.ToString(DirectCast(radBoxes.MasterTableView.Items(i).FindControl("ddlServiceType"), DropDownList).SelectedItem.Text).Trim)
                    .ReferenceNo = txtReferenceNo.Text.Trim
                    .SignatureType = ddlSignatureType.SelectedValue
                    .AddPackage()

                    GetSpecialServices()
                    .ShipperSpecialServices = specialService

                    If dsBoxes Is Nothing Then
                        dsBoxes = DirectCast(ViewState("ShippingBox"), DataSet)
                        Dim drCommodity As DataRow() = dsBoxes.Tables(1).Select("numBoxID=" & .BoxID)
                        If drCommodity.Length > 0 Then
                            .CompoDescription = CCommon.ToString(drCommodity(0)("vcItemName")) & CCommon.ToString(drCommodity(0)("vcItemDesc"))
                            .Quantity = CCommon.ToInteger(drCommodity(0)("intBoxQty"))
                            .UnitName = CCommon.ToString(drCommodity(0)("vcUnitName"))
                            .UnitPrice = CCommon.ToDouble(drCommodity(0)("monUnitPrice"))
                        Else
                            litMessage.Text = litMessage.Text & " Commodity information is required for Box :" & .BoxID & " to ship it."
                        End If
                    Else
                        Dim drCommodity As DataRow() = dsBoxes.Tables(1).Select("numBoxID=" & .BoxID)
                        If drCommodity.Length > 0 Then
                            .CompoDescription = CCommon.ToString(drCommodity(0)("vcItemName")) & CCommon.ToString(drCommodity(0)("vcItemDesc"))
                            .Quantity = CCommon.ToInteger(drCommodity(0)("intBoxQty"))
                            .UnitName = CCommon.ToString(drCommodity(0)("vcUnitName"))
                            .UnitPrice = CCommon.ToDouble(drCommodity(0)("monUnitPrice"))
                        Else
                            litMessage.Text = litMessage.Text & " Commodity information is required for Box :" & .BoxID & " to ship it."
                        End If
                    End If

                    If .CompoDescription = "" Then
                        litMessage.Text = litMessage.Text & " Commodity information (Description) is required for Box :" & .BoxID & " to ship it."
                        Exit Sub

                    ElseIf .Quantity <= 0 Then
                        litMessage.Text = litMessage.Text & " Commodity information (Qauntity) is required for Box :" & .BoxID & " to ship it."
                        Exit Sub

                    ElseIf .UnitName = "" Then
                        litMessage.Text = litMessage.Text & " Commodity information (Unit Name) is required for Box :" & .BoxID & " to ship it."
                        Exit Sub

                    ElseIf .UnitPrice <= 0 Then
                        litMessage.Text = litMessage.Text & " Commodity information (Unit Price) is required for Box :" & .BoxID & " to ship it."
                        Exit Sub

                    End If

                    If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                    With objOppBizDocs
                        .OppId = lngOppID
                        .ShippingReportId = lngShippingReportId
                        .DomainID = Session("DomainID")
                        .OppBizDocId = lngOppBizDocID
                        If objShipping.Provider = 0 Then
                            .ShipCompany = 91
                        ElseIf objShipping.Provider = 1 Then
                            .ShipCompany = 88
                        ElseIf objShipping.Provider = 2 Then
                            .ShipCompany = 90
                        End If
                        '.ShipCompany = lngShippingCompany
                        '.Value1 = ddlCarrierCode.SelectedValue
                        .Value2 = objShipping.ServiceType
                        .BoxServiceTypeID = objShipping.ServiceType
                        '.Value3 = ddlDropoffType.SelectedValue
                        '.Value4 = ddlPackagingType.SelectedValue

                        .FromState = ddlFromState.SelectedValue
                        .FromZip = txtFromZip.Text.Trim()
                        .FromCountry = ddlFromCountry.SelectedValue
                        .ToState = ddlToState.SelectedValue
                        .ToZip = txtToZip.Text.Trim()
                        .ToCountry = ddlToCountry.SelectedValue

                        .UserCntID = Session("UserContactID")
                        .strText = ""

                        .PayorType = ddlPayorType.SelectedValue
                        .PayorAccountNo = txtThirdPartyAccountNo.Text.Trim()
                        .PayorCountryCode = ddlThirdPartyCounty.SelectedValue
                        .PayorZipCode = txtThirdPartyZipCode.Text.Trim()

                        .FromCity = txtFromCity.Text.Trim()
                        .FromAddressLine1 = txtFromAddressLine1.Text.Trim()
                        .FromAddressLine2 = txtFromAddressLine2.Text.Trim()

                        .ToCity = txtToCity.Text.Trim()
                        .ToAddressLine1 = txtToAddressLine1.Text.Trim()
                        .ToAddressLine2 = txtToAddressLine2.Text.Trim()

                        .FromName = txtFromName.Text
                        .FromCompany = txtFromCompany.Text
                        .FromPhone = txtFromPhone.Text
                        .IsFromResidential = chkFromResidential.Checked

                        .ToName = txtToName.Text
                        .ToCompany = txtToCompany.Text
                        .ToPhone = txtToPhone.Text
                        .IsToResidential = chkToResidential.Checked

                        .IsCOD = If(chkOther.Items.FindByText("COD") Is Nothing, False, chkOther.Items.FindByText("COD").Selected)
                        .IsDryIce = If(chkOther.Items.FindByText("Dry Ice") Is Nothing, False, chkOther.Items.FindByText("Dry Ice").Selected)
                        .IsHoldSaturday = If(chkOther.Items.FindByText("Hold Saturday") Is Nothing, False, chkOther.Items.FindByText("Hold Saturday").Selected)
                        .IsHomeDelivery = If(chkOther.Items.FindByText("Home Delivery Premium") Is Nothing, False, chkOther.Items.FindByText("Home Delivery Premium").Selected)
                        .IsInsideDelivery = If(chkOther.Items.FindByText("Inside Delivery") Is Nothing, False, chkOther.Items.FindByText("Inside Delivery").Selected)
                        .IsInsidePickup = If(chkOther.Items.FindByText("Inside Pickup") Is Nothing, False, chkOther.Items.FindByText("Inside Pickup").Selected)
                        .IsReturnShipment = If(chkOther.Items.FindByText("Return Shipment") Is Nothing, False, chkOther.Items.FindByText("Return Shipment").Selected)
                        .IsSaturdayDelivery = If(chkOther.Items.FindByText("Saturday Delivery") Is Nothing, False, chkOther.Items.FindByText("Saturday Delivery").Selected)
                        .IsSaturdayPickup = If(chkOther.Items.FindByText("Saturday Pickup") Is Nothing, False, chkOther.Items.FindByText("Saturday Pickup").Selected)
                        .CODAmount = txtCODTotalAmount.Text
                        .CODType = If(ddlCODType.SelectedItem IsNot Nothing AndAlso ddlCODType.SelectedIndex > 0, ddlCODType.SelectedItem.Text, If(Not ddlCODType.Items.FindByValue("Any") Is Nothing, ddlCODType.Items.FindByValue("Any").Value, ""))
                        .IsAdditionalHandling = If(chkOther.Items.FindByText("Additional Handling") Is Nothing, False, chkOther.Items.FindByText("Additional Handling").Selected)
                        .LargePackage = If(chkOther.Items.FindByText("Large Package") Is Nothing, False, chkOther.Items.FindByText("Large Package").Selected)
                        .Description = txtDescription.Text
                        .TotalInsuredValue = If(CCommon.ToDouble(txtTotalInsuredValue.Text) = 0, CCommon.ToDouble(Session("TotalInsuredValue")), CCommon.ToDouble(txtTotalInsuredValue.Text))
                        .TotalCustomsValue = If(CCommon.ToDouble(txtTotalCustomsValue.Text) = 0, CCommon.ToDouble(Session("TotalCustomsValue")), CCommon.ToDouble(txtTotalCustomsValue.Text))
                        .SignatureType = CCommon.ToLong(ddlSignatureType.SelectedValue)
                        'Setup IsDomestic as per given address
                        If CCommon.ToInteger(ddlFromCountry.SelectedValue) = CCommon.ToInteger(ddlToCountry.SelectedValue) Then
                            IsDomestic = True
                            '.TotalInsuredValue = 0
                            '.TotalCustomsValue = 0
                        Else
                            IsDomestic = False
                        End If

                        .ManageShippingReport() 'Update Shipping report address values and create shipping report items 
                    End With
                Next

                .GetShippingLabel(1, IsDomestic, .Provider)

                If .ErrorMsg <> "" Then
                    If (.ErrorMsg.Contains("6532")) Then
                        litMessage.Text = litMessage.Text & "Recipient's phone number is required, your option is to update phone number of contact."
                    ElseIf (.ErrorMsg.Contains("6541")) Then
                        litMessage.Text = litMessage.Text & "Shipper's phone number is required, your option is to update phone number of primary contact."
                    Else
                        litMessage.Text = litMessage.Text & .ErrorMsg
                    End If
                    Exit Sub
                Else

                    Dim objOppBizDocs As New OppBizDocs
                    objOppBizDocs.ShippingReportId = lngShippingReportId
                    objOppBizDocs.OppBizDocId = lngOppBizDocID
                    objOppBizDocs.UpdateBizDocsShippingFields()
                    litMessage.Text = "Tracking #(s) are updated to bizdoc sucessfully."

                End If
            End With
            Response.Redirect("frmShippingReport.aspx?ShipReportId=" + lngShippingReportId.ToString + "&OppId=" + lngOppID.ToString + "&OppBizDocId=" + lngOppBizDocID.ToString)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub SaveBoxDimension()
        Try
            Dim ds As New DataSet
            Dim dtFields As New DataTable
            If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
            'dtFields.Columns.Add("numItemCode")
            dtFields.Columns.Add("numBoxID")
            dtFields.Columns.Add("tintServiceType")
            'dtFields.Columns.Add("dtDeliveryDate")
            'dtFields.Columns.Add("monShippingRate")
            dtFields.Columns.Add("fltTotalWeight")
            dtFields.Columns.Add("intNoOfBox")
            dtFields.Columns.Add("fltHeight")
            dtFields.Columns.Add("fltWidth")
            dtFields.Columns.Add("fltLength")
            dtFields.Columns.Add("numServiceTypeID")
            dtFields.Columns.Add("fltDimensionalWeight")
            dtFields.Columns.Add("numPackageTypeID")
            dtFields.Columns.Add("numShipCompany")

            'dtFields.Columns.Add("numOppBizDocItemID")
            dtFields.TableName = "Box"

            Dim GridDataItem As Telerik.Web.UI.GridDataItem
            For index As Integer = 0 To radBoxes.MasterTableView.Items.Count - 1
                GridDataItem = radBoxes.MasterTableView.Items(index)

                Dim dtRow As DataRow = dtFields.NewRow
                dtRow("numBoxID") = GridDataItem("numBoxID").Text
                dtRow("tintServiceType") = CCommon.ToShort(DirectCast(GridDataItem.FindControl("ddlServiceType"), DropDownList).SelectedValue)
                dtRow("fltTotalWeight") = CCommon.ToDouble(CType(GridDataItem.FindControl("txtTotalWeight"), TextBox).Text)
                dtRow("intNoOfBox") = CCommon.ToInteger(CType(GridDataItem.FindControl("lblBoxQty"), Label).Text)
                dtRow("fltHeight") = CCommon.ToDouble(CType(GridDataItem.FindControl("txtHeight"), TextBox).Text)
                dtRow("fltWidth") = CCommon.ToDouble(CType(GridDataItem.FindControl("txtWidth"), TextBox).Text)
                dtRow("fltLength") = CCommon.ToDouble(CType(GridDataItem.FindControl("txtLength"), TextBox).Text)
                dtRow("fltDimensionalWeight") = CCommon.ToDouble(CType(GridDataItem.FindControl("hdnDimensionalWeight"), HiddenField).Value)
                dtRow("numServiceTypeID") = CCommon.ToShort(DirectCast(GridDataItem.FindControl("ddlServiceType"), DropDownList).SelectedValue)
                dtRow("numPackageTypeID") = CCommon.ToShort(DirectCast(GridDataItem.FindControl("ddlPackagingType"), DropDownList).SelectedValue)
                dtRow("numShipCompany") = OppBizDocs.GetServiceTypes(CCommon.ToString(DirectCast(GridDataItem.FindControl("ddlServiceType"), DropDownList).SelectedItem.Text))
                dtFields.Rows.Add(dtRow)
            Next
            ds.Tables.Add(dtFields.Copy)
            'Update Box Rate and delivery time
            With objOppBizDocs
                .ShippingReportId = lngShippingReportId
                .strText = ds.GetXml()
                .UserCntID = Session("UserContactID")
                .byteMode = 1
                .ManageShippingBox()
            End With
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlPayorType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPayorType.SelectedIndexChanged
        Try
            If ddlPayorType.SelectedValue = 0 Then
                tdPaymentOption1.Visible = False
                tdPaymentOption2.Visible = False
            Else
                If lngShippingCompany = 91 Then 'fedex
                    txtThirdPartyZipCode.Visible = False
                    lblPayorlabel.Text = "Account No,Country"
                    tdPaymentOption1.Visible = True
                    tdPaymentOption2.Visible = True
                ElseIf lngShippingCompany = 88 Then 'UPS
                    'Do nothing
                    tdPaymentOption1.Visible = True
                    tdPaymentOption2.Visible = True
                ElseIf lngShippingCompany = 90 Then 'USPS
                    tdPaymentOption1.Visible = False
                    tdPaymentOption2.Visible = False
                Else
                    tdPaymentOption1.Visible = False
                    tdPaymentOption2.Visible = False
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub radBoxes_ItemDataBound(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridItemEventArgs) Handles radBoxes.ItemDataBound
        Try
            If e.Item.ItemType = Telerik.Web.UI.GridItemType.AlternatingItem Or e.Item.ItemType = Telerik.Web.UI.GridItemType.Item Then
                If lngShippingCompany = 90 Then 'USPS
                    If Not e.Item.FindControl("lblOunces") Is Nothing Then
                        e.Item.FindControl("lblOunces").Visible = True
                        e.Item.FindControl("txtTotalWeightInOunce").Visible = True
                    End If
                End If

                Dim ddlServiceType As DropDownList
                ddlServiceType = DirectCast(e.Item.FindControl("ddlServiceType"), DropDownList)
                If ddlServiceType IsNot Nothing Then
                    OppBizDocs.LoadServiceTypes(0, ddlServiceType, lngShippingCompany)
                    ddlServiceType.Items.Insert(0, New ListItem("Unspecified", 0))
                    If CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numServiceTypeID")) > 0 Then
                        ddlServiceType.ClearSelection()
                        If ddlServiceType.Items.FindByValue(DataBinder.Eval(e.Item.DataItem, "numServiceTypeID")) IsNot Nothing Then
                            ddlServiceType.Items.FindByValue(DataBinder.Eval(e.Item.DataItem, "numServiceTypeID")).Selected = True
                        End If
                    ElseIf lngShippingService > 0 AndAlso Not ddlServiceType.Items.FindByValue(lngShippingService) Is Nothing Then
                        ddlServiceType.ClearSelection()
                        ddlServiceType.Items.FindByValue(lngShippingService).Selected = True
                    End If
                End If

                Dim ddlPackagingType As DropDownList
                ddlPackagingType = DirectCast(e.Item.FindControl("ddlPackagingType"), DropDownList)
                If ddlPackagingType IsNot Nothing Then
                    OppBizDocs.LoadPackagingType(lngShippingCompany, ddlPackagingType, lngShippingCompany, Session("DomainID"))

                    If CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "vcPackageName")) <> "Unspecified" AndAlso CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "vcPackageName")) <> "" Then
                        ddlPackagingType.ClearSelection()
                        If ddlPackagingType.Items.FindByText(DataBinder.Eval(e.Item.DataItem, "vcPackageName")) IsNot Nothing Then
                            ddlPackagingType.Items.FindByText(DataBinder.Eval(e.Item.DataItem, "vcPackageName")).Selected = True

                            If ddlPackagingType.SelectedValue > 0 Then
                                ddlPackagingType.Enabled = False
                            End If

                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub radBoxes_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles radBoxes.NeedDataSource
        Try
            BindData()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Dim dtRates As DataTable
    Dim ds As New DataSet()
    Private Sub CreateTempTable()
        Try
            Dim dtTemp As DataTable
            'Create Blank Schema of table to merge all Shipping rates
            dtTemp = New DataTable()
            dtTemp.Columns.Add("ID")
            dtTemp.Columns.Add("numBoxID")
            'dtTemp.Columns.Add("numItemCode")
            dtTemp.Columns.Add("GroupName")
            dtTemp.Columns.Add("Date")
            dtTemp.Columns.Add("ServiceType")
            dtTemp.Columns.Add("tintServiceType")
            dtTemp.Columns.Add("Rate")
            dtTemp.Columns.Add("intTotalWeight")
            dtTemp.Columns.Add("intNoOfBox")
            dtTemp.Columns.Add("fltHeight")
            dtTemp.Columns.Add("fltWidth")
            dtTemp.Columns.Add("fltLength")
            dtTemp.Columns.Add("fltDimensionalWeight")
            ds.Tables.Add(dtTemp.Copy)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Dim ID As Integer = 0
    Dim fltHeight, intwidth, fltLength As Integer
    Protected Sub btnGetRates_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnGetRates.Click
        Try
            'Save Box Dimentions and weight and service type
            SaveBoxDimension()

            CreateTempTable()
            If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
            objOppBizDocs.ShipCompany = lngShippingCompany
            objOppBizDocs.ShipFromCountry = ddlFromCountry.SelectedValue
            objOppBizDocs.ShipFromState = ddlFromState.SelectedValue
            objOppBizDocs.ShipToCountry = ddlToCountry.SelectedValue
            objOppBizDocs.ShipToState = ddlToState.SelectedValue
            'set string value
            objOppBizDocs.strShipFromCountry = ddlFromCountry.SelectedItem.Text
            objOppBizDocs.strShipFromState = ddlFromState.SelectedItem.Text
            objOppBizDocs.strShipToCountry = ddlToCountry.SelectedItem.Text
            objOppBizDocs.strShipToState = ddlToState.SelectedItem.Text
            objOppBizDocs.GetShippingAbbreviation()


            'objOppBizDocs.DomainID = Session("DomainID")
            'objOppBizDocs.OppBizDocId = lngOppBizDocID
            'objOppBizDocs.OppId = lngOppID
            'ds = objOppBizDocs.GetBizDocInventoryItems
            'update user entered dimentions and weight pkgs in to dataset and bind
            'For index As Integer = 0 To radBoxes.MasterTableView.Items.Count - 1
            '    For index1 As Integer = 0 To ds.Tables(0).Rows.Count - 1
            '        If ds.Tables(0).Rows(index1)("numItemCode").ToString().Equals(radBoxes.MasterTableView.Items(index).Cells(0).Text) Then
            '            Dim TotalWeight As Double = Convert.ToDouble(CType(radBoxes.MasterTableView.Items(index).FindControl("txtTotalWeight"), TextBox).Text.Trim())
            '            If TotalWeight <= 0 Then
            '                litMessage.Text = "Package must weigh more than zero pounds."
            '                Exit Sub
            '            End If
            '            ds.Tables(0).Rows(index1)("fltWeight") = TotalWeight
            '            intwidth = CInt(CType(radBoxes.MasterTableView.Items(index).FindControl("txtWidth"), TextBox).Text.Trim())
            '            fltHeight = CInt(CType(radBoxes.MasterTableView.Items(index).FindControl("txtHeight"), TextBox).Text.Trim())
            '            fltLength = CInt(CType(radBoxes.MasterTableView.Items(index).FindControl("txtLength"), TextBox).Text.Trim())

            '            If lngShippingCompany = 91 Then 'fedex
            '                If fltLength = 0 Or fltHeight = 0 Or intwidth = 0 Then
            '                    litMessage.Text = "Length, width, and height must be greater than zero."
            '                    Exit Sub
            '                End If
            '            End If
            '            ds.Tables(0).Rows(index1)("fltWidth") = IIf(chkUseDimensions.Checked, intwidth, 0)
            '            ds.Tables(0).Rows(index1)("fltHeight") = IIf(chkUseDimensions.Checked, fltHeight, 0)
            '            ds.Tables(0).Rows(index1)("fltLength") = IIf(chkUseDimensions.Checked, fltLength, 0)
            '            ds.Tables(0).Rows(index1)("intNoOfBox") = 1 'Convert.ToInt32(CType(dgBizDocItems.Items(index).FindControl("txtPackages"), TextBox).Text.Trim())
            '        End If
            '    Next
            'Next
            'dgItems.DataSource = ds
            'dgItems.DataBind()
            'If blVisible = True Then
            '    tblConfig.Visible = False
            '    tblItems.Visible = False
            '    btnAddToShippingReport.Visible = True
            '    tblShipping.Visible = True
            'End If
            btnAddToShippingReport.Visible = True
            radRateSelection.Visible = True
            radRateSelection.Rebind()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Dim dtShipRates As DataTable
    Dim intGroupCount As Integer = 0

    'Private Sub radRateSelection_DetailTableDataBind(sender As Object, e As Telerik.Web.UI.GridDetailTableDataBindEventArgs) Handles radRateSelection.DetailTableDataBind
    '    Try

    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    Private Sub radRateSelection_NeedDataSource(ByVal sender As Object, ByVal e As Telerik.Web.UI.GridNeedDataSourceEventArgs) Handles radRateSelection.NeedDataSource
        Try
            Dim ds As New DataSet
            If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
            objOppBizDocs.ShippingReportId = lngShippingReportId
            ds = objOppBizDocs.GetShippingBoxes()
            ds.Tables(0).TableName = "Box"
            ds.Tables.RemoveAt(1)


            Dim objShipping As New Shipping
            For Each dr As DataRow In ds.Tables(0).Rows
                With objShipping
                    .DomainID = Session("DomainID")
                    .WeightInLbs = CCommon.ToDouble(dr("fltTotalWeight"))
                    .NoOfPackages = 1 'CCommon.ToDouble(e.Item.Cells(5).Text)
                    .UseDimentions = chkUseDimensions.Checked
                    .GroupCount = intGroupCount
                    'If chkUseDimensions.Checked Then
                    .Height = CCommon.ToDouble(dr("fltHeight"))
                    .Width = CCommon.ToDouble(dr("fltWidth"))
                    .Length = CCommon.ToDouble(dr("fltLength"))
                    'End If
                    .SenderState = objOppBizDocs.strShipFromState
                    .SenderZipCode = txtFromZip.Text
                    .SenderCountryCode = objOppBizDocs.strShipFromCountry
                    .RecepientState = objOppBizDocs.strShipToState
                    .RecepientZipCode = txtToZip.Text
                    .RecepientCountryCode = objOppBizDocs.strShipToCountry
                    .ServiceType = CCommon.ToShort(dr("numServiceTypeID"))
                    .ID = ID
                    .BoxID = CCommon.ToLong(dr("numBoxID"))
                    .Provider = lngShippingCompany
                    dtShipRates = .GetRates()

                    If ds.Tables.Count = 1 Then
                        ds.Tables.Add(dtShipRates.Copy())
                    Else
                        For Each dr1 As DataRow In dtShipRates.Rows
                            ds.Tables(1).ImportRow(dr1)
                        Next
                    End If

                    If .ErrorMsg <> "" Then
                        litMessage.Text = .ErrorMsg
                        'blVisible = False
                        btnAddToShippingReport.Visible = False
                        radRateSelection.Visible = False
                        Exit Sub
                    End If
                    ID = .ID
                    intGroupCount = intGroupCount + 1
                End With
            Next
            ds.Tables(1).TableName = "BoxRate"

            radRateSelection.DataSource = ds
            radRateSelection.MasterTableView.HierarchyDefaultExpanded = True
            radBoxes.Visible = False
            Session("dtShipRates") = ds.Tables(1)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnAddToShippingReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddToShippingReport.Click
        Try
            Dim dt As DataTable = CType(Session("dtShipRates"), DataTable)
            Dim strIDs As String() = hdnSelectedIDs.Value.Split(",")
            Dim intID As Integer
            Dim objOppBizDocs As New OppBizDocs
            Dim ds As New DataSet
            Dim dtFields As New DataTable

            'dtFields.Columns.Add("numItemCode")
            dtFields.Columns.Add("numBoxID")
            dtFields.Columns.Add("tintServiceType")
            'dtFields.Columns.Add("dtDeliveryDate")
            'dtFields.Columns.Add("monShippingRate")
            dtFields.Columns.Add("fltTotalWeight")
            dtFields.Columns.Add("intNoOfBox")
            dtFields.Columns.Add("fltHeight")
            dtFields.Columns.Add("fltWidth")
            dtFields.Columns.Add("fltLength")
            dtFields.Columns.Add("fltDimensionalWeight")
            dtFields.TableName = "Box"


            'Create shipping report
            For index As Integer = 0 To strIDs.Length - 1
                If strIDs(index).Length > 0 Then
                    intID = CInt(strIDs(index))
                    Dim dtRow As DataRow = dtFields.NewRow
                    dtRow("numBoxID") = dt.Rows(intID)("numBoxID").ToString()
                    'dtRow("numItemCode") = dt.Rows(intID)("numItemCode").ToString()
                    dtRow("tintServiceType") = dt.Rows(intID)("tintServiceType").ToString()
                    'Try
                    '    dtRow("dtDeliveryDate") = Convert.ToDateTime(dt.Rows(intID)("Date").ToString())
                    'Catch ex As Exception
                    '    dtRow("dtDeliveryDate") = Convert.ToDateTime(Date.Now.ToShortDateString())
                    'End Try
                    'dtRow("monShippingRate") = dt.Rows(intID)("Rate").ToString()
                    dtRow("fltTotalWeight") = dt.Rows(intID)("intTotalWeight").ToString()
                    dtRow("intNoOfBox") = dt.Rows(intID)("intNoOfBox").ToString()
                    dtRow("fltHeight") = dt.Rows(intID)("fltHeight").ToString()
                    dtRow("fltWidth") = dt.Rows(intID)("fltWidth").ToString()
                    dtRow("fltLength") = dt.Rows(intID)("fltLength").ToString()
                    dtRow("fltDimensionalWeight") = dt.Rows(intID)("fltDimensionalWeight")

                    dtFields.Rows.Add(dtRow)
                End If
            Next
            ds.Tables.Add(dtFields.Copy)
            'Update Box reat and delivery time
            With objOppBizDocs
                .ShippingReportId = lngShippingReportId
                .strText = ds.GetXml()
                .UserCntID = Session("UserContactID")
                .byteMode = 1
                .ManageShippingBox()
            End With

            With objOppBizDocs
                .DomainID = Session("DomainID")
                .OppBizDocId = lngOppBizDocID
                .ShipCompany = lngShippingCompany
                .FromState = ddlFromState.SelectedValue
                .FromZip = txtFromZip.Text.Trim()
                .FromCountry = ddlFromCountry.SelectedValue
                .ToState = ddlToState.SelectedValue
                .ToZip = txtToZip.Text.Trim()
                .ToCountry = ddlToCountry.SelectedValue
                .UserCntID = Session("UserContactID")
                .ShippingReportId = .ManageShippingReport
            End With

            radRateSelection.Visible = False
            btnAddToShippingReport.Visible = False
            radBoxes.Visible = True
            radBoxes.Rebind()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub chkOther_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkOther.SelectedIndexChanged
        Try
            For Each strSelectedItem As ListItem In chkOther.Items
                If strSelectedItem.Selected = True AndAlso strSelectedItem.Text = "COD" Then
                    trCOD1.Visible = True
                    trCOD2.Visible = True
                End If

                If strSelectedItem.Selected = True AndAlso strSelectedItem.Text = "Hold Saturday" Then

                End If


                If strSelectedItem.Selected = True AndAlso strSelectedItem.Text = "Inside Delivery" Then

                End If

                If strSelectedItem.Selected = True AndAlso strSelectedItem.Text = "Inside Pickup" Then

                End If

                If strSelectedItem.Selected = True AndAlso strSelectedItem.Text = "Return Shipment" Then

                End If

                If strSelectedItem.Selected = True AndAlso strSelectedItem.Text = "Saturday Delivery" Then

                End If

                If strSelectedItem.Selected = True AndAlso strSelectedItem.Text = "Saturday Pickup" Then

                End If

                If strSelectedItem.Selected = False AndAlso strSelectedItem.Text = "COD" Then
                    trCOD1.Visible = False
                    trCOD2.Visible = False
                    'ddlPackagingType.Enabled = True

                    txtCODTotalAmount.Text = "0.00"
                    ddlCODType.SelectedIndex = 0
                End If
            Next

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub GetSpecialServices()
        Try
            For Each strSelectedItem As ListItem In chkOther.Items
                If strSelectedItem.Selected = True AndAlso strSelectedItem.Text = "COD" Then
                    specialService = specialService Or &H4L '&H4L 'COD 
                End If

                If strSelectedItem.Selected = True AndAlso strSelectedItem.Text = "Dry Ice" Then
                    specialService = specialService Or &H80L '&H80L        'Dry Ice
                End If

                If strSelectedItem.Selected = True AndAlso strSelectedItem.Text = "Hold Saturday" Then
                    specialService = specialService Or &H10000L '&H10000L     'Hold Saturday
                End If

                If strSelectedItem.Selected = True AndAlso strSelectedItem.Text = "Home Delivery Premium" Then
                    specialService = specialService Or &H20000L ' &H20000L     'Home Delivery Premium
                End If

                If strSelectedItem.Selected = True AndAlso strSelectedItem.Text = "Inside Delivery" Then
                    specialService = specialService Or &H40000L '&H40000L     'Inside Delivery
                End If

                If strSelectedItem.Selected = True AndAlso strSelectedItem.Text = "Inside Pickup" Then
                    specialService = specialService Or &H80000L '&H80000L     'Inside Pickup
                End If

                If strSelectedItem.Selected = True AndAlso strSelectedItem.Text = "Return Shipment" Then
                    specialService = specialService Or &H8000000L ' &H8000000L   'Return Shipment
                End If

                If strSelectedItem.Selected = True AndAlso strSelectedItem.Text = "Saturday Delivery" Then
                    specialService = specialService Or &H10000000L '&H20000000L  'Saturday Delivery
                End If

                If strSelectedItem.Selected = True AndAlso strSelectedItem.Text = "Saturday Pickup" Then
                    specialService = specialService Or &H20000000L ' &H40000000L  'Saturday Pickup
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlFromCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFromCountry.SelectedIndexChanged
        Try
            If CCommon.ToLong(ddlFromCountry.SelectedValue) > 0 Then
                FillState(ddlFromState, CCommon.ToLong(ddlFromCountry.SelectedValue), Session("DomainID"))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlToCountry_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlToCountry.SelectedIndexChanged
        Try
            If CCommon.ToLong(ddlToCountry.SelectedValue) > 0 Then
                FillState(ddlToState, CCommon.ToLong(ddlToCountry.SelectedValue), Session("DomainID"))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub


    Protected Sub btnUpdateRates_Click(sender As Object, e As EventArgs)
        Try

            SaveBoxDimension()
            Dim ds As New DataSet
            If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
            objOppBizDocs.ShippingReportId = lngShippingReportId
            ds = objOppBizDocs.GetShippingBoxes()
            ds.Tables(0).TableName = "Box"
            ds.Tables.RemoveAt(1)

            'objOppBizDocs.DomainID = CCommon.ToLong(Session("DomainID"))
            'objOppBizDocs.OppBizDocId = lngOppBizDocID
            'objOppBizDocs.ShippingReportId = lngShippingReportId
            'Dim dsAddress As DataSet
            'dsAddress = objOppBizDocs.GetShippingReport()



            Dim i As Integer = 0
            Dim totalShippingRates As Double = 0
            Dim objShipping As New Shipping
            For Each dr As DataRow In ds.Tables(0).Rows
                objOppBizDocs.ShipCompany = lngShippingCompany
                objOppBizDocs.ShipFromState = ddlFromState.SelectedValue
                objOppBizDocs.ShipFromCountry = ddlFromCountry.SelectedValue
                objOppBizDocs.ShipToState = ddlToState.SelectedValue
                objOppBizDocs.ShipToCountry = ddlToCountry.SelectedValue
                objOppBizDocs.GetShippingAbbreviation()
                With objShipping
                    .DomainID = Session("DomainID")
                    .WeightInLbs = CCommon.ToDouble(dr("fltTotalWeight"))
                    .NoOfPackages = 1 'CCommon.ToDouble(e.Item.Cells(5).Text)
                    .UseDimentions = chkUseDimensions.Checked
                    .GroupCount = intGroupCount
                    'If chkUseDimensions.Checked Then
                    .Height = CCommon.ToDouble(dr("fltHeight"))
                    .Width = CCommon.ToDouble(dr("fltWidth"))
                    .Length = CCommon.ToDouble(dr("fltLength"))
                    'End If
                    .SenderState = objOppBizDocs.strShipFromState
                    .SenderZipCode = txtFromZip.Text
                    .SenderCountryCode = objOppBizDocs.strShipFromCountry
                    .RecepientState = objOppBizDocs.strShipToState
                    .RecepientZipCode = txtToZip.Text
                    .RecepientCountryCode = objOppBizDocs.strShipToCountry

                    .ServiceType = CCommon.ToShort(dr("numServiceTypeID"))
                    '.PackagingType = ddlPackagingType.SelectedValue
                    '.ItemCode = CCommon.ToLong(dr("numItemCode"))
                    '.OppBizDocItemID = CCommon.ToLong(dr("numOppitemtCode"))
                    .ID = ID
                    .BoxID = CCommon.ToLong(dr("numBoxID"))
                    .Provider = lngShippingCompany
                    dtShipRates = .GetRates()
                    Dim IndivRates As Double = 0
                    If (dtShipRates.Rows.Count > 0) Then
                        IndivRates = dtShipRates.Rows(0)("Rate")
                    End If

                    Dim GridDataItem As Telerik.Web.UI.GridDataItem
                    GridDataItem = radBoxes.MasterTableView.Items(i)
                    i = i + 1
                    Dim lblIndivRate As Label
                    lblIndivRate = CType(GridDataItem.FindControl("lblShippingRate"), Label)
                    lblIndivRate.Text = IndivRates
                    totalShippingRates = totalShippingRates + IndivRates

                End With
            Next
            Dim doubleShippingItemId As Double
            doubleShippingItemId = CCommon.ToDouble(Session("ShippingServiceItem"))
            Dim objBizShippingUpdate As New OppBizDocs
            objBizShippingUpdate.OppId = lngOppID
            objBizShippingUpdate.ItemCode = doubleShippingItemId
            objBizShippingUpdate.Price = totalShippingRates
            objBizShippingUpdate.UpdateShippingItemPrice()

        Catch ex As Exception

        End Try
    End Sub
End Class