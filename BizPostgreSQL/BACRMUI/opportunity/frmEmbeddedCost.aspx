﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmEmbeddedCost.aspx.vb"
    Inherits=".frmEmbeddedCost" MasterPageFile="~/common/Popup.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Embedded Cost</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <style type="text/css">
        .hide
        {
            display: none;
        }
    </style>
    <script type="text/javascript">
        function OpenItems(a) {
            window.open('../opportunity/frmEmbeddedCostItems.aspx?EmbeddedCostID=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=400,scrollbars=yes,resizable=yes');
            return false;
        }
        function keyPressed(TB, e) {
            var tblGrid = document.getElementById("dgECost");

            var rowcount = tblGrid.rows.length;
            var TBID = document.getElementById(TB);

            var key;
            if (window.event) { e = window.event; }
            key = e.keyCode;
            if (key == 37 || key == 38 || key == 39 || key == 40) {
                for (Index = 1; Index < rowcount; Index++) {

                    for (childIndex = 0; childIndex <
              tblGrid.rows[Index].cells.length; childIndex++) {

                        if (tblGrid.rows[Index].cells[childIndex].children[0] != null) {
                            if (tblGrid.rows[Index].cells[
                 childIndex].children[0].id == TBID.id) {

                                if (key == 40) {
                                    if (Index + 1 < rowcount) {
                                        if (tblGrid.rows[Index + 1].cells[
                     childIndex].children[0] != null) {
                                            if (tblGrid.rows[Index + 1].cells[
                       childIndex].children[0].type == 'text') {

                                                //downvalue

                                                tblGrid.rows[Index + 1].cells[
                             childIndex].children[0].focus();
                                                return false;
                                            }
                                        }
                                    }
                                }
                                if (key == 38) {
                                    if (tblGrid.rows[Index - 1].cells[
                     childIndex].children[0] != null) {
                                        if (tblGrid.rows[Index - 1].cells[
                       childIndex].children[0].type == 'text') {
                                            //upvalue

                                            tblGrid.rows[Index - 1].cells[
                             childIndex].children[0].focus();
                                            return false;
                                        }
                                    }
                                }

                                if (key == 37 && (childIndex != 0)) {

                                    if ((tblGrid.rows[Index].cells[
                      childIndex - 1].children[0]) != null) {

                                        if (tblGrid.rows[Index].cells[
                       childIndex - 1].children[0].type == 'text') {
                                            //left

                                            if (tblGrid.rows[Index].cells[
                         childIndex - 1].children[0].value != '') {
                                                var cPos =
                           getCaretPos(tblGrid.rows[Index].cells[
                                       childIndex - 1].children[0], 'left');
                                                if (cPos) {
                                                    tblGrid.rows[Index].cells[
                                 childIndex - 1].children[0].focus();
                                                    return false;
                                                }
                                                else {
                                                    return false;
                                                }
                                            }
                                            tblGrid.rows[Index].cells[childIndex - 1].children[0].focus();
                                            return false;
                                        }
                                    }
                                }

                                if (key == 39) {
                                    if (tblGrid.rows[Index].cells[childIndex + 1].children[0] != null) {
                                        if (tblGrid.rows[Index].cells[
                       childIndex + 1].children[0].type == 'text') {
                                            //right

                                            if (tblGrid.rows[Index].cells[
                         childIndex + 1].children[0].value != '') {
                                                var cPosR =
                           getCaretPos(tblGrid.rows[Index].cells[
                                       childIndex + 1].children[0], 'right');
                                                if (cPosR) {
                                                    tblGrid.rows[Index].cells[
                                 childIndex + 1].children[0].focus();
                                                    return false;
                                                }
                                                else {
                                                    return false;
                                                }
                                            }
                                            tblGrid.rows[Index].cells[childIndex + 1].children[0].focus();
                                            return false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        function getCaretPos(control, way) {
            var movement;
            if (way == 'left') {
                movement = -1;
            }
            else {
                movement = 1;
            }
            if (control.createTextRange) {
                control.caretPos = document.selection.createRange().duplicate();
                if (control.caretPos.move("character", movement) != '') {
                    return false;
                }
                else {
                    return true;
                }
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table cellpadding="2" cellspacing="2" width="100%">
                <tr>
                    <td>
                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" />
                        <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Remove" />
                        <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClientClick="return Close();" />&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <table width="100%">
        <tr>
            <td class="normal4" align="center" colspan="2">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Post Embedded Cost
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <table cellpadding="2" cellspacing="2" width="800px" border="0">
        <tr valign="bottom">
            <td align="right">
                Cost Category
            </td>
            <td align="left" class="normal1">
                <asp:DropDownList runat="server" ID="ddlCostCategory" CssClass="signup" AutoPostBack="true">
                </asp:DropDownList>
            </td>
        </tr>
        <tr valign="top">
            <td colspan="2">
                <asp:Table ID="table3" Width="100%" runat="server" Height="100" GridLines="None"
                    BorderColor="black" CssClass="aspTable" BorderWidth="1" CellSpacing="0" CellPadding="0">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:DataGrid ID="dgECost" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False">
                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                <ItemStyle CssClass="is"></ItemStyle>
                                <HeaderStyle CssClass="hs"></HeaderStyle>
                                <Columns>
                                    <asp:BoundColumn DataField="numEmbeddedCostID" Visible="False"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numCostCatID" Visible="False"></asp:BoundColumn>
                                    <asp:TemplateColumn HeaderText="Cost Center Name">
                                        <ItemTemplate>
                                            <asp:DropDownList runat="server" ID="ddlCostCenter" CssClass="signup">
                                            </asp:DropDownList>
                                            <asp:Label runat="server" ID="lblAccountID" Visible="false" Text='<%# Eval("numAccountID")  %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Estimated Cost">
                                        <ItemTemplate>
                                            <asp:HyperLink ID="hplItems" runat="server" NavigateUrl="javascript:"></asp:HyperLink>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Actual Cost">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtActualCost" runat="server" CssClass="signup" Width="75px" onkeyup="keyPressed(this.id,event)"
                                                AUTOCOMPLETE="OFF" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Right" />
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Vendor">
                                        <ItemTemplate>
                                            <telerik:RadComboBox ID="radCmbCompany" Width="195px" DropDownWidth="200px" Skin="Default"
                                                runat="server" AllowCustomText="True" EnableLoadOnDemand="True">
                                                <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                            </telerik:RadComboBox>
                                            <%--<rad:RadComboBox AccessKey="C" ID="radCmbCompany" ExternalCallBackPage="../include/LoadCompany.aspx"
                                                Width="195px" DropDownWidth="200px" Skin="WindowsXP" runat="server" AutoPostBack="True"
                                                AllowCustomText="True" EnableLoadOnDemand="True">
                                            </rad:RadComboBox>--%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Memo">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtMemo" runat="server" CssClass="signup" onkeyup="keyPressed(this.id,event)"
                                                AUTOCOMPLETE="OFF" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Payment Method">
                                        <ItemTemplate>
                                            <asp:DropDownList runat="server" ID="ddlPaymentMethod" CssClass="signup">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="Due Date">
                                        <ItemTemplate>
                                            <BizCalendar:Calendar ID="calDueDate" runat="server" ClientIDMode="AutoID" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="pnlInfo" CssClass="info">
        You need to save respected Bizdoc after posting embedded cost, to reflect entered
        cost in Accounting.
    </asp:Panel>
</asp:Content>
