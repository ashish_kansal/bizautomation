﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmLandedCostBill.aspx.vb" Inherits=".frmLandedCostBill" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Src="~/Accounting/PaymentHistory.ascx" TagName="PaymentHistory" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Landed Cost</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script type="text/javascript" language="javascript">
        $(document).ready(function () {
            InitializeValidation();

            $('#Content_calBillDate_txtDate').blur(function () {
                duedateChage(document.getElementById('hfDateFormat').value);
            });
            $('#Content_calBillDate_txtDate').blur();
            CalculateTotal();

            $("[id$=ddlExpenseAccount]").change(function () {
                var td = $("[id$=ddlExpenseAccount]").parent().parent();
                $("[id$=lblExpenseAccount]", td).text($(this).val().split("~")[2].replace("0", "-"));
            }
            );
        });

        function duedateChage(format) {

            if (!isDate($('#Content_calBillDate_txtDate').val(), format)) {
                alert("Enter Valid Bill date");
                $('#Content_calBillDate_txtDate').focus();
                return false;
            }

            var SDate = new Date(getDateFromFormat($('#Content_calBillDate_txtDate').val(), format));
            //var startDate = formatDate(d, 'dd-MM-yyyy');

            //alert('Bill Date :' + SDate)
            var ddDueDate = $('#ddlTerms option:selected').text();
            var strValue = $('#ddlTerms option:selected').val();
            //var dateFormat = document.getElementById('hfDateFormat').value;

            //startDate = new Date(SDate.getFullYear(), SDate.getMonth(), parseInt(SDate.getDate()) + parseInt(ddDueDate));
            startDate = new Date(SDate.getFullYear(), SDate.getMonth(), parseInt(SDate.getDate()) + parseInt(strValue.split("~")[1]));
            $('#Content_calDueDate_txtDate').val(formatDate(startDate, format));
        }


        var IsAddClicked = false;
        function Save() {
            if (IsAddClicked == true) {
                return false;
            }

            var isValid;
            isValid = true;

            if (document.getElementById('Content_calBillDate_txtDate').value == 0) {
                alert("Enter Bill Date")
                document.getElementById('Content_calBillDate_txtDate').focus();
                return false;

            }

            if (document.getElementById('ddlTerms').value == 0) {
                alert("Please Select Terms");
                document.getElementById('ddlTerms').focus();
                return false;

            }

            if (document.getElementById('Content_calDueDate_txtDate').value == 0) {
                alert("Enter Due Date")
                document.getElementById('Content_calDueDate_txtDate').focus();
                return false;

            }

            //alert(isValid);
            //            if (isValid == true) {
            //                isValid = CalculateTotal(true);
            //            }
            if (CalculateTotal(true) != true) {
                return false;
            }

            return true;

            //return isValid;
        }

        function CalculateTotal(TotalValidate) {
            var itotal = 0.0;
            var idtxtDebit;
            var digits;
            var txtdebitvalue;
            var CrValue;
            var CrTotValue;
            var err = 0;

            $('#gvLandedExpense tr').not(':first').each(function () {
                var txtAmount = $(this).find("input[id*='txtAmount']").val();
                ddlAccounts = $(this).find("[id*='ddlExpenseAccount']");
                ddlVendor = $(this).find("[id*='ddlVendor']");

                if (txtAmount != undefined && txtAmount != '') {
                    if ($(ddlAccounts).val() == "0~0~0") {
                        err = 1;
                        return false;
                    }

                    if ($(ddlVendor).val() == 0) {
                        err = 3;
                        return false;
                    }

                    itotal += parseFloat(txtAmount);

                    if (parseFloat(txtAmount) < 0) {
                        err = 2;
                        return false;
                    }
                }
            });

            if (err == 1) {
                alert('You must select an account for each split line with an amount.');
                return false;
            }
            else if (err == 2) {
                alert("Each amount must be greater than Zero.");
                return false;
            }
            else if (err == 2) {
                alert("You must select an Vendor for each split line with an amount.");
                return false;
            }

            if (TotalValidate == true && itotal == 0) {
                alert("Please enter alteast one line with an amount and account");
                return false;
            }

            formatDigit = formatCurrency(itotal);
            $('#lblTotal').text(formatDigit);
            $('#lblTotal').val(formatDigit);

            return true;
        }

        function formatCurrency(num) {
            num = num.toString().replace(/\$|\,/g, '');
            if (isNaN(num))
                num = "0";
            sign = (num == (num = Math.abs(num)));
            num = Math.floor(num * 100 + 0.50000000001);
            cents = num % 100;
            num = Math.floor(num / 100).toString();
            if (cents < 10)
                cents = "0" + cents;
            for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3) ; i++)
                num = num.substring(0, num.length - (4 * i + 3)) + ',' +
    num.substring(num.length - (4 * i + 3));
            return (((sign) ? '' : '-') + num + '.' + cents);
        }

        function OnCalenderSelect(cal) {
            if (cal.indexOf("calBillDate") > 0) {
                duedateChage(document.getElementById('hfDateFormat').value);
            }
        }

        function NotAllowMessage(a) {
            if (a == 1)
                alert("You are not allowed to edit/delete selected item, since it is being Paid!");
            return false;
        }
    </script>
    <style type="text/css">
        .required
        {
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <table align="right">
        <tr>
            <td>
                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save and Close" OnClientClick="return Save();" />
                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" OnClientClick="window.close()" Text="Close" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Landed Cost
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:Label ID="lblError" runat="server" ForeColor="Red" Visible="false" Text="You can not apply landed cost because some line items do not have enough onHand quatity."></asp:Label>
    <table width="800px" id="tblLandedCost" runat="server">
        <tr>
            <td colspan="4">
                <uc2:paymenthistory id="PaymentHistory1" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="normal4" align="center" colspan="2">
                <asp:Label ID="litMessage" EnableViewState="false" runat="server"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right" width="25%">Bill Date <font color="#ff0000">*</font>
            </td>
            <td class="normal1" width="25%">
                <BizCalendar:Calendar ID="calBillDate" runat="server" IsRequired="false" DateName="Bill Date"
                    ClientIDMode="Predictable" />
                <asp:HiddenField ID="hfDateFormat" runat="server" />
            </td>
            <td class="normal1" align="right">
                <b>Total :</b></td>
            <td class="normal1"><b>
                <asp:Label ID="lblTotal" runat="server" ForeColor="#4E9E19" Font-Size="Large"></asp:Label></b>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">Terms <font color="#ff0000">*</font>
            </td>
            <td>
                <asp:DropDownList ID="ddlTerms" runat="server" Width="160px">
                </asp:DropDownList>
            </td>
            <td class="normal1" align="right">Due Date <font color="#ff0000">*</font>
            </td>
            <td class="normal1">
                <BizCalendar:Calendar ID="calDueDate" runat="server" IsRequired="false" DateName="Due Date"
                    ClientIDMode="Predictable" />
            </td>
        </tr>
        <tr>
            <td align="right">Landed Cost On:                 
            </td>
            <td colspan="3">
                <asp:RadioButtonList runat="server" ID="rblLandedCost" RepeatDirection="Horizontal">
                    <asp:ListItem>Cubic Size</asp:ListItem>
                    <asp:ListItem>Weight</asp:ListItem>
                    <asp:ListItem Selected="True">Amount</asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:GridView ID="gvLandedExpense" runat="server" AutoGenerateColumns="false"
                    CssClass="dg" Width="100%">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <Columns>
                        <asp:TemplateField HeaderText="&nbsp;#&nbsp;" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1%>
                                <asp:HiddenField runat="server" ID="hdnBillID" Value='<%# Eval("numBillID")%>' />
                                <asp:HiddenField runat="server" ID="hdnAmtPaid" Value='<%# Eval("monAmtPaid")%>' />
                                <asp:HiddenField runat="server" ID="hdnBillDetailID" Value='<%# Eval("numBillDetailID")%>' />
                                <asp:HiddenField runat="server" ID="hdnTransactionId_Bill" Value='<%# Eval("numTransactionId_Bill")%>' />
                                <asp:HiddenField runat="server" ID="hdnTransactionId_BillDetail" Value='<%# Eval("numTransactionId_BillDetail")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vendor<font color=#ff0000>*</font>">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlVendor" runat="server" CssClass="{required:false,messages:{required:'Select Vendor!'}}">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Width="400px" HeaderText="Description<font color=#ff0000>*</font>">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlExpenseAccount" style="width:90%" runat="server" CssClass="{required:false,messages:{required:'Select Expense Account!'}}">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount<font color=#ff0000>*</font>">
                            <ItemTemplate>
                                <asp:TextBox ID="txtAmount" runat="server" Width="150px" AutoComplete="OFF" Text='<%# ReturnMoney(Eval("monAmountDue"))%>'
                                    CssClass="required_decimal {required:false,number:true, messages:{required:'Enter Amount! ',number:'provide valid value!'}}"
                                    onchange="return CalculateTotal(true)"></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Expense Account" HeaderStyle-Width="150px">
                            <ItemTemplate>
                                <asp:Label ID="lblExpenseAccount" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnDeleteRow" runat="server" CssClass="button Delete" Text="X" CommandName="DeleteRow"
                                    CommandArgument="<%# Container.DataItemIndex %>" OnClientClick="return confirm('Are you sure to delete selected Vendor Bill?')"></asp:Button>
                                <asp:LinkButton ID="lnkDelete" runat="server" Visible="false">
														<font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="4" style="text-align:right">
                <asp:Button ID="btnAdd" runat="server" CssClass="btn btn-primary" Text="Add New Row" />
            </td>
        </tr>
    </table>
</asp:Content>
