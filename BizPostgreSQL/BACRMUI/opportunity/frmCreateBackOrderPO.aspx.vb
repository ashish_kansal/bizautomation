﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Workflow
Imports BACRM.BusinessLogic.Accounting
Imports Telerik.Web.UI

Public Class frmCreateBackOrderPO
    Inherits BACRMPage

#Region "Memeber Variables"

    Private objOPP As COpportunities

#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblError.Text = ""
            divError.Style.Add("display", "none")

            If Not Page.IsPostBack Then
                If GetQueryStringVal("IsFromWorkOrder") <> "1" Then
                    hdnOppID.Value = GetQueryStringVal("numOppID")

                    If CCommon.ToLong(hdnOppID.Value) = 0 Then
                        DisplayError("Sales order does not exists.")
                        Exit Sub
                    End If
                End If

                BinData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub BinData()
        Try
            Dim objContact As New CContacts
            objContact.AddressType = CContacts.enmAddressType.ShipTo
            objContact.DomainID = CCommon.ToLong(Session("DomainID"))
            objContact.RecordID = CCommon.ToLong(Session("UserDivisionID"))
            objContact.AddresOf = CContacts.enmAddressOf.Organization
            objContact.byteMode = 2
            Dim dtAddress As DataTable = objContact.GetAddressDetail()

            Dim listItem As RadComboBoxItem
            For Each dr As DataRow In dtAddress.Rows
                dr("vcFullAddress") = dr("vcFullAddress").ToString().Replace("<pre>", "").Replace("</pre>", ",").Replace("<br>", "")
                listItem = New RadComboBoxItem
                listItem.Text = CCommon.ToString(dr("vcAddressName"))
                listItem.Value = CCommon.ToString(dr("numAddressID"))
                listItem.Attributes.Add("Address", dr("vcFullAddress"))
                radcmbEmployerShipToAddresses.Items.Add(listItem)
            Next

            If radcmbEmployerShipToAddresses.Items.Count > 0 Then
                lblAddress.Text = radcmbEmployerShipToAddresses.SelectedItem.Attributes("Address")
            End If

            Dim ds As DataSet = Nothing
            objOPP = New COpportunities
            objOPP.DomainID = CCommon.ToLong(Session("DomainID"))

            If GetQueryStringVal("IsFromWorkOrder") = "1" Then
                objOPP.strItemDtls = CCommon.ToString(GetQueryStringVal("Items"))
                ds = objOPP.GetSOBackOrderItems()
            Else
                objOPP.OpportID = CCommon.ToLong(hdnOppID.Value)
                ds = objOPP.GetSOBackOrderItems()
            End If
            

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    hfnReOrderPointOrderStatus.Value = CCommon.ToLong(ds.Tables(0).Rows(0)("numReOrderPointOrderStatus"))
                    hdnOppStautsForAutoPOBackOrder.Value = CCommon.ToShort(ds.Tables(0).Rows(0)("tintOppStautsForAutoPOBackOrder"))
                    hdnUnitsRecommendationForAutoPOBackOrder.Value = CCommon.ToShort(ds.Tables(0).Rows(0)("tintUnitsRecommendationForAutoPOBackOrder"))
                End If

                If ds.Tables.Count > 1 AndAlso ds.Tables(1).Rows.Count > 0 Then
                    gvBackOrderItems.DataSource = ds.Tables(1)
                    gvBackOrderItems.DataBind()
                End If
            Else
                DisplayError("There are back order items in sales order.")
            End If
        Catch ex As Exception
            If ex.Message.Contains("INVALID_OPPID") Then
                DisplayError("Sales order does not exists.")
            Else
                Throw
            End If
        End Try
    End Sub

    Private Sub DisplayError(ByVal errorMessage As String)
        Try
            lblError.Text = errorMessage
            divError.Style.Add("display", "")
        Catch ex As Exception
            'DO NOT THROW ERROR
        End Try
    End Sub

    Private Sub BindVendorAddresses(ByVal vendorID As Long, ByRef ddlVendorWarehouses As DropDownList)
        Try
            ddlVendorWarehouses.Items.Clear()

            Dim objItems As New CItems
            objItems.DomainID = Session("DomainID")
            objItems.VendorID = vendorID
            objItems.byteMode = 2
            Dim dtVendorsWareHouse As DataTable = objItems.GetVendorsWareHouse

            If dtVendorsWareHouse.Rows.Count > 0 Then
                ddlVendorWarehouses.DataSource = dtVendorsWareHouse
                ddlVendorWarehouses.DataTextField = "vcFullAddress"
                ddlVendorWarehouses.DataValueField = "numAddressID"
                ddlVendorWarehouses.DataBind()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BindVendorShipmentMethod(ByVal vendorID As Long, ByVal vendorWarehouse As Long, ByRef ddlVendorShipmentMethod As DropDownList)
        Try
            ddlVendorShipmentMethod.Items.Clear()

            Dim objItems As New CItems
            objItems.DomainID = Session("DomainID")
            objItems.ShipAddressId = vendorWarehouse
            objItems.VendorID = vendorID
            objItems.byteMode = 2
            Dim dtVendorShipmentMethod As DataTable = objItems.GetVendorShipmentMethod

            If Not dtVendorShipmentMethod Is Nothing AndAlso dtVendorShipmentMethod.Rows.Count > 0 Then
                ddlVendorShipmentMethod.DataSource = dtVendorShipmentMethod
                ddlVendorShipmentMethod.DataTextField = "ShipmentMethod"
                ddlVendorShipmentMethod.DataValueField = "vcListValue"
                ddlVendorShipmentMethod.DataBind()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CreateOrder(ByVal DivisionID As Long, ByVal ContactID As Long, ByVal CompanyName As String)
        Try
            Using objTransctionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                Dim i As Integer
                Dim objOpportunity As New MOpportunity
                objOpportunity.OpportunityId = 0
                objOpportunity.OppType = 2
                If hdnOppStautsForAutoPOBackOrder.Value = "1" Then
                    objOpportunity.DealStatus = objOpportunity.EnmDealStatus.DealWon
                Else
                    objOpportunity.DealStatus = objOpportunity.EnmDealStatus.DealOpen
                End If
                objOpportunity.OpportunityName = CompanyName & "-PO-" & Format(Now(), "MMMM")
                objOpportunity.ContactID = ContactID
                objOpportunity.DivisionID = DivisionID
                objOpportunity.UserCntID = Session("UserContactID")
                objOpportunity.EstimatedCloseDate = Now
                objOpportunity.DomainID = Session("DomainId")
                objOpportunity.Active = 1
                objOpportunity.SourceType = 1

                Dim objAdmin As New CAdmin
                Dim dtBillingTerms As DataTable
                objAdmin.DivisionID = DivisionID
                dtBillingTerms = objAdmin.GetBillingTerms()

                objOpportunity.boolBillingTerms = IIf(dtBillingTerms.Rows(0).Item("tintBillingTerms") = 1, True, False)
                objOpportunity.BillingDays = dtBillingTerms.Rows(0).Item("numBillingDays")
                objOpportunity.boolInterestType = IIf(dtBillingTerms.Rows(0).Item("tintInterestType") = 1, True, False)
                objOpportunity.Interest = dtBillingTerms.Rows(0).Item("fltInterest")

                'objOpportunity.strItems = dsTemp.GetXml
                objOpportunity.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                Dim lngOppID As Long = objOpportunity.Save()(0)
                objOpportunity.OpportunityId = lngOppID

                ''Added By Sachin Sadhu||Date:29thApril12014
                ''Purpose :To Added Opportunity data in work Flow queue based on created Rules
                ''          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = lngOppID
                objWfA.SaveWFOrderQueue()
                'end of code

                Dim dg As GridViewRow
                For i = 0 To gvBackOrderItems.Rows.Count - 1
                    dg = gvBackOrderItems.Rows(i)

                    objOpportunity.ItemCode = CCommon.ToLong(DirectCast(dg.FindControl("lblItemCode"), Label).Text.Trim)
                    objOpportunity.Units = CCommon.ToInteger(CType(dg.FindControl("txtUnits"), TextBox).Text)
                    objOpportunity.UnitPrice = CCommon.ToDecimal(CType(dg.FindControl("txtUnitCost"), TextBox).Text)
                    objOpportunity.Desc = ""
                    objOpportunity.ItemName = CCommon.ToString(DirectCast(dg.FindControl("lblItemName"), Label).Text.Trim)
                    objOpportunity.Notes = CCommon.ToString(DirectCast(dg.FindControl("txtNotes"), TextBox).Text.Trim)
                    objOpportunity.ModelName = CCommon.ToString(DirectCast(dg.FindControl("lblModelID"), Label).Text.Trim)
                    objOpportunity.ItemThumbImage = ""
                    objOpportunity.ItemType = CCommon.ToString(DirectCast(dg.FindControl("hdnCharItemType"), HiddenField).Value.Trim)
                    objOpportunity.Dropship = False
                    objOpportunity.WarehouseItemID = CCommon.ToLong(DirectCast(dg.FindControl("hdnWarehouseItemID"), HiddenField).Value)
                    objOpportunity.numUOM = CCommon.ToLong(CType(dg.FindControl("hdnBaseUnit"), HiddenField).Value)
                    objOpportunity.ProjectID = 0
                    objOpportunity.ClassID = 0

                    Dim ddlVendor As DropDownList
                    ddlVendor = dg.FindControl("ddlVendor")

                    Dim strValue As String() = ddlVendor.SelectedValue.Split(CChar("~"))

                    If CCommon.ToLong(strValue(0)) = DivisionID Then
                        objOpportunity.UpdateOpportunityItems()
                    End If
                Next

                If hdnOppStautsForAutoPOBackOrder.Value = "1" Then
                    'Fix of Bug id 638 -by chintan, Update inventory counts
                    Dim objCustomReport As New BACRM.BusinessLogic.Reports.CustomReports
                    objCustomReport.DynamicQuery = " PERFORM USP_UpdatingInventoryonCloseDeal(" & objOpportunity.OpportunityId & ",0," & Session("UserContactID") & ");"
                    objCustomReport.ExecuteDynamicSql()
                End If

                Dim objContact As New CContacts
                objContact.AddressType = CContacts.enmAddressType.BillTo
                objContact.DomainID = CCommon.ToLong(Session("DomainID"))
                objContact.RecordID = CCommon.ToLong(Session("UserDivisionID"))
                objContact.AddresOf = CContacts.enmAddressOf.Organization
                objContact.byteMode = 2
                Dim dtAddress As DataTable = objContact.GetAddressDetail()


                objOpportunity.BillToType = 0
                If Not dtAddress Is Nothing AndAlso dtAddress.Rows.Count > 0 Then
                    objOpportunity.BillToAddressID = CCommon.ToLong(dtAddress.Rows(0)("numAddressID"))
                End If

                objOpportunity.ShipToType = 0
                objOpportunity.ParentOppID = CCommon.ToLong(hdnOppID.Value)
                objOpportunity.ShipToAddressID = CCommon.ToLong(radcmbEmployerShipToAddresses.SelectedValue)
                objOpportunity.UpdateOpportunityLinkingDtls()

                objTransctionScope.Complete()
            End Using
        Catch ex As Exception
            If ex.Message = "NOT_ALLOWED" Then
                Exit Sub
            Else
                Throw ex
            End If
        End Try
    End Sub

#End Region

#Region "Event Handlers"

    Protected Sub ddlVendor_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            Dim ddlVendor As DropDownList = DirectCast(sender, DropDownList)
            Dim grv As GridViewRow = DirectCast(ddlVendor.NamingContainer, GridViewRow)

            If Not grv Is Nothing Then
                Dim ddlVendorWarehouses As DropDownList = DirectCast(grv.FindControl("ddlVendorWarehouses"), DropDownList)
                Dim ddlVendorShipmentMethod As DropDownList = DirectCast(grv.FindControl("ddlVendorShipmentMethod"), DropDownList)
                Dim lblLeadTimeDays As Label = DirectCast(grv.FindControl("lblLeadTimeDays"), Label)
                Dim lblMinOrderQty As Label = DirectCast(grv.FindControl("lblMinOrderQty"), Label)


                BindVendorAddresses(CCommon.ToLong(ddlVendor.SelectedValue.Split("~")(0)), ddlVendorWarehouses:=ddlVendorWarehouses)

                If ddlVendorWarehouses.Items.Count > 0 Then
                    lblMinOrderQty.Text = ddlVendor.SelectedValue.Split("~")(3)
                    BindVendorShipmentMethod(CCommon.ToLong(ddlVendor.SelectedValue.Split("~")(0)), CCommon.ToLong(ddlVendorWarehouses.SelectedValue), ddlVendorShipmentMethod:=ddlVendorShipmentMethod)

                    If ddlVendorShipmentMethod.Items.Count > 0 Then
                        ddlVendorShipmentMethod.Attributes.Add("onchange", "ChangeLeadTimeDays('" & lblLeadTimeDays.ClientID & "','" & ddlVendorShipmentMethod.ClientID & "') ")
                        lblLeadTimeDays.Text = ddlVendorShipmentMethod.SelectedValue.Split("~")(1)
                    Else
                        lblLeadTimeDays.Text = "-"
                    End If
                Else
                    lblMinOrderQty.Text = "-"
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub ddlVendorShipmentMethod_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            Dim ddlVendorShipmentMethod As DropDownList = DirectCast(sender, DropDownList)
            Dim grv As GridViewRow = DirectCast(ddlVendorShipmentMethod.NamingContainer, GridViewRow)

            If Not grv Is Nothing Then
                Dim lblLeadTimeDays As Label = DirectCast(grv.FindControl("lblLeadTimeDays"), Label)

                If ddlVendorShipmentMethod.Items.Count > 0 Then
                    lblLeadTimeDays.Text = ddlVendorShipmentMethod.SelectedValue.Split("~")(1)
                Else
                    lblLeadTimeDays.Text = "-"
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub ddlVendorWarehouses_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            Dim ddlVendorWarehouses As DropDownList = DirectCast(sender, DropDownList)
            Dim grv As GridViewRow = DirectCast(ddlVendorWarehouses.NamingContainer, GridViewRow)

            If Not grv Is Nothing Then
                Dim ddlVendor As DropDownList = DirectCast(grv.FindControl("ddlVendor"), DropDownList)
                Dim lblLeadTimeDays As Label = DirectCast(grv.FindControl("lblLeadTimeDays"), Label)
                Dim ddlVendorShipmentMethod As DropDownList = DirectCast(grv.FindControl("ddlVendorShipmentMethod"), DropDownList)

                BindVendorShipmentMethod(CCommon.ToLong(ddlVendor.SelectedValue.Split("~")(0)), ddlVendorWarehouses.SelectedValue, ddlVendorShipmentMethod)

                If ddlVendorShipmentMethod.Items.Count > 0 Then
                    lblLeadTimeDays.Text = ddlVendorShipmentMethod.SelectedValue.Split("~")(1)
                Else
                    lblLeadTimeDays.Text = "-"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnCreateBackOrderPO_Click(sender As Object, e As EventArgs) Handles btnCreateBackOrderPO.Click
        Try
            If radcmbEmployerShipToAddresses.SelectedItem Is Nothing Then
                DisplayError("Please select Ship-To Location.")
                Exit Sub
            End If

            Dim objJournal As New JournalEntry
            If ChartOfAccounting.GetDefaultAccount("CG", Session("DomainID")) = 0 Then
                DisplayError("Please Set Default COGs account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save.")
                Exit Sub
            End If

            If ChartOfAccounting.GetDefaultAccount("PC", Session("DomainID")) = 0 Then
                DisplayError("Please Set Default Purchase Clearing account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save")
                Exit Sub
            End If

            If ChartOfAccounting.GetDefaultAccount("PV", Session("DomainID")) = 0 Then
                DisplayError("Please Set Default Purchase Price Variance account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save")
                Exit Sub
            End If

            'validate item income,asset , cogs account are mapped based on item type
            Dim objItem As New CItems
            Dim objOppBizDocs As New OppBizDocs
            For Each dgGridItem As GridViewRow In gvBackOrderItems.Rows
                If DirectCast(dgGridItem.FindControl("chkSelect"), CheckBox).Checked Then
                    If Not objItem.ValidateItemAccount(DirectCast(dgGridItem.FindControl("lblItemCode"), Label).Text, Session("DomainID"), 2) = True Then
                        DisplayError("Please Set Income,Asset,COGs(Expense) Account for " & DirectCast(dgGridItem.FindControl("lblItemName"), Label).Text & " from Administration->Inventory->Item Details")
                        Exit Sub
                    End If

                    Dim ddlVendor As DropDownList
                    ddlVendor = dgGridItem.FindControl("ddlVendor")

                    If ddlVendor.SelectedItem Is Nothing Then
                        DisplayError("Please select vendor for all selected item(s).")
                        Exit Sub
                    End If

                    Dim strValue As String() = ddlVendor.SelectedValue.Split(CChar("~"))
                    If objOppBizDocs.ValidateARAP(CCommon.ToLong(strValue(0)), 0, Session("DomainID")) = False Then
                        DisplayError("Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting->Accounts for RelationShip"" To Save")
                        Exit Sub
                    End If

                    Dim txtUnits As TextBox = DirectCast(dgGridItem.FindControl("txtUnits"), TextBox)
                    Dim units As Double = 0
                    If Not Double.TryParse(txtUnits.Text, units) Then
                        DisplayError("Enter units to purchase for all selected item(s).")
                        Exit Sub
                    ElseIf units = 0 Then
                        DisplayError("Enter units to purchase for all selected item(s).")
                        Exit Sub
                    End If

                    'CHECK ITEM HAS WAREHOUSE MAPPED WITH SELECTED ADDRESS
                    objItem.ItemCode = CCommon.ToLong(DirectCast(dgGridItem.FindControl("lblItemCode"), Label).Text)
                    objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                    objItem.ShipAddressId = CCommon.ToLong(radcmbEmployerShipToAddresses.SelectedValue)
                    objItem.WareHouseItemID = CCommon.ToLong(DirectCast(dgGridItem.FindControl("hdnWarehouseItemID"), HiddenField).Value)
                    Dim dtWarehouse As DataTable = objItem.GetItemWarehouseBasedOnAddress()

                    If Not dtWarehouse Is Nothing AndAlso dtWarehouse.Rows.Count > 0 Then
                        If CCommon.ToLong(dtWarehouse.Rows(0)("numWarehouseItemID")) > 0 Then
                            DirectCast(dgGridItem.FindControl("hdnWarehouseItemID"), HiddenField).Value = CCommon.ToLong(dtWarehouse.Rows(0)("numWarehouseItemID"))
                        Else
                            DisplayError("Some item(s) do not have warehouse(s) mapped to selected ship to address. Please make sure there is atleast one warehouse which is mapped to selected ship to address in Items -> Utilities -> Warehouses and also item have this warehouse available in inventory.")
                            Exit Sub
                        End If
                    Else
                        DisplayError("Some item(s) do not have warehouse(s) mapped to selected ship to address. Please make sure there is atleast one warehouse which is mapped to selected ship to address in Items -> Utilities -> Warehouses and also item have this warehouse available in inventory.")
                        Exit Sub
                    End If
                End If
            Next

            Dim dr As DataRow
            Dim gridRow As GridViewRow

            'Create table to group item by selected vendor
            Dim dtItem As New DataTable
            dtItem.Columns.Add("ItemCode")
            dtItem.Columns.Add("Units")
            dtItem.Columns.Add("UnitPrice")
            dtItem.Columns.Add("Desc")
            dtItem.Columns.Add("ItemName")
            dtItem.Columns.Add("ModelName")
            dtItem.Columns.Add("ItemThumbImage")
            dtItem.Columns.Add("ItemType")
            dtItem.Columns.Add("Dropship")
            dtItem.Columns.Add("WarehouseItemID")
            dtItem.Columns.Add("numVendorID")
            dtItem.Columns.Add("numVendorContactId")
            dtItem.Columns.Add("VendorCompanyName")
            dtItem.Columns.Add("numUOM")
            dtItem.Columns.Add("txtNotes")


            Dim ErrorMessage As String = ""
            For i = 0 To gvBackOrderItems.Rows.Count - 1
                gridRow = gvBackOrderItems.Rows(i)

                If DirectCast(gridRow.FindControl("chkSelect"), CheckBox).Checked Then
                    Dim ddlVendor As DropDownList
                    ddlVendor = gridRow.FindControl("ddlVendor")

                    If ddlVendor.Items.Count > 0 Then
                        dr = dtItem.NewRow

                        dr("ItemCode") = CCommon.ToLong(DirectCast(gridRow.FindControl("lblItemCode"), Label).Text.Trim)
                        dr("Units") = CCommon.ToDouble(CType(gridRow.FindControl("txtUnits"), TextBox).Text)
                        dr("UnitPrice") = CCommon.ToDecimal(CType(gridRow.FindControl("txtUnitCost"), TextBox).Text)
                        dr("Desc") = ""
                        dr("ItemName") = CCommon.ToString(DirectCast(gridRow.FindControl("lblItemName"), Label).Text.Trim)
                        dr("ModelName") = CCommon.ToString(DirectCast(gridRow.FindControl("lblModelID"), Label).Text.Trim)
                        dr("ItemThumbImage") = ""
                        dr("ItemType") = CCommon.ToString(DirectCast(gridRow.FindControl("hdnCharItemType"), HiddenField).Value.Trim)
                        dr("Dropship") = False
                        dr("WarehouseItemID") = CCommon.ToLong(DirectCast(gridRow.FindControl("hdnWarehouseItemID"), HiddenField).Value)
                        dr("txtNotes") = CCommon.ToDecimal(CType(gridRow.FindControl("txtNotes"), TextBox).Text)
                        dr("numUOM") = CCommon.ToLong(CType(gridRow.FindControl("hdnBaseUnit"), HiddenField).Value)

                        Dim strValue As String() = ddlVendor.SelectedValue.Split(CChar("~"))

                        dr("numVendorID") = CCommon.ToLong(strValue(0))
                        dr("numVendorContactId") = CCommon.ToLong(strValue(1))
                        dr("VendorCompanyName") = ddlVendor.SelectedItem.Text

                        dtItem.Rows.Add(dr)
                    Else
                        If String.IsNullOrEmpty(ErrorMessage) Then
                            ErrorMessage = "PO can not be created, Your option is to select a vender for following item(s):"
                        Else
                            ErrorMessage += "<br/>=>" & CCommon.ToString(DirectCast(gridRow.FindControl("lblItemName"), Label).Text.Trim)
                        End If
                    End If
                End If
            Next

            If Not String.IsNullOrEmpty(ErrorMessage) Then
                DisplayError(ErrorMessage)
                Exit Sub
            ElseIf dtItem.Rows.Count = 0 Then
                DisplayError("Select atleast one item.")
                Exit Sub
            End If

            Dim dtVendor As DataTable
            dtVendor = dtItem.DefaultView.ToTable(True, "numVendorID")

            For Each drRow As DataRow In dtVendor.Select()

                Dim drItemColl() As DataRow
                'Create single order for all item which are referenced to same vendor
                drItemColl = dtItem.Select("(numVendorID = '" & drRow.Item("numVendorID") & "')")

                'Create Order against primary vendors
                CreateOrder(drItemColl(0).Item("numVendorID"), drItemColl(0).Item("numVendorContactId"), drItemColl(0).Item("VendorCompanyName"))
            Next

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "CloseWindow", "window.opener.location.href=window.opener.location.href; Close();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub gvBackOrderItems_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvBackOrderItems.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                Dim chkSelectAll As CheckBox = DirectCast(e.Row.FindControl("chkSelectAll"), CheckBox)
                chkSelectAll.Checked = True
            End If

            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim drv As DataRowView = DirectCast(e.Row.DataItem, DataRowView)
                Dim ddlVendor As DropDownList = DirectCast(e.Row.FindControl("ddlVendor"), DropDownList)
                Dim ddlVendorWarehouses As DropDownList = DirectCast(e.Row.FindControl("ddlVendorWarehouses"), DropDownList)
                Dim ddlVendorShipmentMethod As DropDownList = DirectCast(e.Row.FindControl("ddlVendorShipmentMethod"), DropDownList)
                Dim txtUnitCost As TextBox = DirectCast(e.Row.FindControl("txtUnitCost"), TextBox)
                Dim txtUnits As TextBox = DirectCast(e.Row.FindControl("txtUnits"), TextBox)
                Dim lblLeadTimeDays As Label = DirectCast(e.Row.FindControl("lblLeadTimeDays"), Label)
                Dim imgThumb As System.Web.UI.WebControls.Image = DirectCast(e.Row.FindControl("imgThumb"), System.Web.UI.WebControls.Image)
                Dim chkSelect As CheckBox = DirectCast(e.Row.FindControl("chkSelect"), CheckBox)

                chkSelect.Checked = True
                txtUnits.Text = String.Format("{0:#,##0}", CCommon.ToInteger(drv("numUnitHour")))
                txtUnitCost.Text = CCommon.GetDecimalFormat(CCommon.ToDouble(drv("monCost")))
                imgThumb.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & If(IsDBNull(drv("vcPathForTImage")), "", drv("vcPathForTImage"))

                If Not ddlVendor Is Nothing Then
                    Dim objItems As New CItems
                    objItems.DomainID = Session("DomainID")
                    objItems.ItemCode = CCommon.ToLong(drv("numItemCode"))
                    Dim dtVendors As DataTable = objItems.GetVendors


                    For Each drVendor As DataRow In dtVendors.Rows
                        ddlVendor.Items.Add(New ListItem(drVendor.Item("Vendor").ToString(), drVendor.Item("numVendorID").ToString() & "~" & drVendor.Item("numContactId").ToString() & "~" & String.Format("{0:#,##0.00}", drVendor.Item("monCost")) & "~" & drVendor.Item("intMinQty").ToString()))
                    Next

                    ddlVendor.Attributes.Add("onchange", "ChangeVendorCost(" & CCommon.ToShort(drv("tintDefaultCost")) & ",'" & txtUnitCost.ClientID & "','" & ddlVendor.ClientID & "'," & CCommon.ToDouble(drv("fltUOMConversionFactorForPurchase")) & "," & CCommon.ToInteger(drv("numBackOrder")) & ",'" & txtUnits.ClientID & "')")

                    If Not ddlVendor.Items.FindByValue(CCommon.ToLong(drv("numVendorID"))) Is Nothing Then
                        ddlVendor.Items.FindByValue(CCommon.ToLong(drv("numVendorID"))).Selected = True
                    End If

                    If Not ddlVendor.SelectedItem Is Nothing Then
                        BindVendorAddresses(CCommon.ToLong(ddlVendor.SelectedValue.Split("~")(0)), ddlVendorWarehouses:=ddlVendorWarehouses)

                        If ddlVendorWarehouses.Items.Count > 0 Then
                            BindVendorShipmentMethod(CCommon.ToLong(ddlVendor.SelectedValue.Split("~")(0)), CCommon.ToLong(ddlVendorWarehouses.SelectedValue), ddlVendorShipmentMethod:=ddlVendorShipmentMethod)

                            If ddlVendorShipmentMethod.Items.Count > 0 Then
                                lblLeadTimeDays.Text = ddlVendorShipmentMethod.SelectedValue.Split("~")(1)
                            Else
                                lblLeadTimeDays.Text = "-"
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub radcmbEmployerShipToAddresses_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles radcmbEmployerShipToAddresses.SelectedIndexChanged
        Try
            If Not radcmbEmployerShipToAddresses.SelectedItem Is Nothing Then
                lblAddress.Text = radcmbEmployerShipToAddresses.SelectedItem.Attributes("Address")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

    
End Class