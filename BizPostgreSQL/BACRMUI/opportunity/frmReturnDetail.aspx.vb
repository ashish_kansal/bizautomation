﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic
Imports System.Web.Services
Imports System.Text.RegularExpressions


Namespace BACRM.UserInterface.Opportunities
    Public Class frmReturnDetail
        Inherits BACRMPage

#Region "PUBLIC/PRIVATE DECLARATION"

        Dim objReturn As ReturnHeader
        Dim objOpportunity As New OppotunitiesIP
        Dim dtReturnDetails As New DataTable
        Dim lngReturnHeaderID As Long
        Dim sMode As Short = 0
        Dim dtTableInfo As DataTable
        Dim objPageControls As New PageControls
        Dim boolIntermediatoryPage As Boolean = False
        Dim lngAccountID As Long
        Dim dtWarehouse As DataTable

#End Region

#Region "PAGE EVENTS"
        <WebMethod()> _
        Public Shared Function PersistTab(ByVal strKey As String, ByVal strValue As String) As String

            Dim PersistTable As New Hashtable
            PersistTable.Clear()
            PersistTable.Add(strKey, strValue)
            PersistTable.Save(strPageName:="frmReturnDetails.aspx")

        End Function

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'HIDE ERROR DIV
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                'HIDE ALERT DIV
                divAlert.Visible = False
                litMessage.Text = ""

                GetUserRightsForPage(10, 14)

                If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                    btnSaveCreditMemo.Visible = False
                    btnSaveRefund.Visible = False

                    btnEdit.Visible = False
                    btnReceive.Visible = False
                End If

                If Not IsPostBack Then
                    If Session("EnableIntMedPage") = 1 Then
                        ViewState("IntermediatoryPage") = True
                    Else
                        ViewState("IntermediatoryPage") = False
                    End If
                End If
                boolIntermediatoryPage = ViewState("IntermediatoryPage")
                ControlSettings()

                lngReturnHeaderID = CCommon.ToLong(GetQueryStringVal("ReturnID"))

                If Not IsPostBack Then
                    'FillDepositTocombo()
                    LoadServiceItem()
                    LoadReturnDetails()
                    LoadTabDetails()
                    AddToRecentlyViewed(RecetlyViewdRecordType.ReturnMA, lngReturnHeaderID)

                    If CCommon.ToShort(hfReturnType.Value) = 1 Or CCommon.ToShort(hfReturnType.Value) = 2 Then
                        Dim toolTip As String
                        toolTip = "Note - Skip step 1,2 and 3 if items are already " & IIf(CCommon.ToShort(hfReturnType.Value) = 1, "received", "shipped") & ".<br /><br />"
                        toolTip += "1. Select Products & Services sub-tab and enter the qty in the received column that you want to " & IIf(CCommon.ToShort(hfReturnType.Value) = 1, "recevie in", "ship") & ".<br />"
                        toolTip += "2. Click on the """ & IIf(CCommon.ToShort(hfReturnType.Value) = 1, "Receive", "Shipped") & """ button. This will trigger the display of the " & IIf(CCommon.ToShort(hfReturnType.Value) = 1, "Save Received Qty", "Ship Qty") & " button & Warehouse selection drop-down.<br />"
                        If CCommon.ToShort(hfReturnType.Value) = 1 Then
                            toolTip += "3. Select the warehouse where items are to be recieved and click ""Save Received Qty"" .<br />"
                        ElseIf CCommon.ToShort(hfReturnType.Value) = 2 Then
                            toolTip += "3. Select the warehouse from where items are to be shipped and click ""Ship Qty"" .<br />"
                        End If

                        toolTip += "4. Go to " & radOppTab.FindTabByValue("IssueAndCredit").Text & " tab,"
                        If CCommon.ToShort(hfReturnType.Value) = 1 Then
                            toolTip += " Here you can either issue Refund or Credit Memo by selecting any option. If you have selected to issue refund than select ""Refund Account"" and check ""Create Refund Receipt"" checkbox and save to issue refund. If you have selected to issue Credit Memo then Check the ""Credit Memo"" checkbox and save to create the credit memo."
                        ElseIf CCommon.ToShort(hfReturnType.Value) = 2 Then
                            toolTip += " Check the ""Credit Memo"" checkbox and save to create the credit memo."
                        End If

                        radToolTip.Text = "<div style=""color:#000000; font-size:12px;"">" & toolTip & "</div>"
                        lblToolTip.Visible = True
                    Else
                        lblToolTip.Visible = False
                    End If


                End If
                LoadControls()

                ibtnDelCrditMemo.Attributes.Add("onclick", "return DeleteRecord()")
                ibtnDelRefunct.Attributes.Add("onclick", "return DeleteRecord()")

                PersistTable.Load(strPageName:="frmReturnDetails.aspx")
                If PersistTable.Count > 0 Then
                    If radOppTab.Tabs.FindTabByValue(PersistTable("index")) IsNot Nothing Then
                        radOppTab.Tabs.FindTabByValue(PersistTable("index")).Selected = True
                    Else
                        radOppTab.SelectedIndex = 1
                    End If
                End If

                If radOppTab.SelectedTab IsNot Nothing Then
                    If radOppTab.SelectedTab.Visible = False Then
                        radOppTab.SelectedIndex = 0
                        radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                    Else
                        radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                    End If
                Else
                    radOppTab.SelectedIndex = 0
                    radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainId"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub LoadTabDetails()
            Try
                If CCommon.ToShort(hfReturnType.Value) = 1 Then
                    radOppTab.FindTabByValue("Details").Text = "Sales Return Details"
                    radOppTab.FindTabByValue("IssueAndCredit").Text = "Issue Refund / Issue Credit Memo"
                    trCreditAccounts.Visible = False
                    trItem.Visible = False
                    ddlCreditAccounts.Enabled = False
                    ddlServiceItem.Enabled = False
                    rbtnItem.Visible = False
                    rbtnAccount.Visible = False
                ElseIf CCommon.ToShort(hfReturnType.Value) = 2 Then
                    radOppTab.FindTabByValue("Details").Text = "Purchase Return Details"
                    radOppTab.FindTabByValue("IssueAndCredit").Text = "Issue Credit Memo"
                    pnlApplyRefund.Visible = False
                    btnReceive.Text = "Shipped"
                    ddlCreditAccounts.Enabled = False
                    ddlServiceItem.Enabled = False
                    trCreditAccounts.Visible = False
                    trItem.Visible = False
                    rbtnItem.Visible = False
                    rbtnAccount.Visible = False
                ElseIf CCommon.ToShort(hfReturnType.Value) = 3 Then
                    rbtnItem.Visible = True
                    radOppTab.FindTabByValue("Details").Text = "Credit Memo Details"
                    radOppTab.FindTabByValue("IssueAndCredit").Text = "Issue Credit Memo"

                    radOppTab.FindTabByValue("ProductsServices").Visible = False
                    pnlApplyRefund.Visible = False
                    rbtnIssueRefund.Checked = False
                    rbtnCreditMemo.Checked = True
                    rbtnAccount.Visible = True
                    rbtnItem.Visible = True
                    ddlCreditAccounts.Enabled = True
                    ddlServiceItem.Enabled = True
                    trCreditAccounts.Visible = True
                    trItem.Visible = True
                    'LoadServiceItem()

                ElseIf CCommon.ToShort(hfReturnType.Value) = 4 Then
                    radOppTab.FindTabByValue("Details").Text = "Refund Details"
                    radOppTab.FindTabByValue("IssueAndCredit").Text = "Issue Refund"

                    radOppTab.FindTabByValue("ProductsServices").Visible = False
                    pnlApplyCredit.Visible = False
                    rbtnIssueRefund.Checked = True
                    rbtnCreditMemo.Checked = False
                    ddlCreditAccounts.Enabled = True
                    ddlServiceItem.Enabled = True
                    rbtnAccount.Visible = True
                    rbtnItem.Visible = True
                    trCreditAccounts.Visible = True
                    trItem.Visible = True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
#End Region

#Region "BUTTON EVENTS"

        Private Sub btnReceive_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReceive.Click
            Try
                sMode = 1
                LoadControls()
                LoadReturnDetails()
                btnReceive.Visible = False
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainId"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnSaveRefund_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveRefund.Click
            Try
                'If ddlRefundAccounts.SelectedIndex > 0 Then
                If CCommon.ToDouble(hdnUnitHourReceived.Value) > 0 Then
                    If hfReturnType.Value = 1 AndAlso ddlRefundAccounts.SelectedIndex = 0 Then
                        DisplayAlert("Select Refund account first.")
                        Exit Sub
                    End If
                    SaveIssueBizDocs(1)

                ElseIf CCommon.ToInteger(hfReturnType.Value) = 3 OrElse CCommon.ToInteger(hfReturnType.Value) = 4 Then
                    If ddlRefundAccounts.SelectedIndex = 0 Then
                        DisplayAlert("Select Refund account first.")
                        Exit Sub
                    End If
                    SaveIssueBizDocs(1)

                Else
                    If CCommon.ToShort(hfReturnType.Value) = 1 Then
                        DisplayAlert("You have to receive quantity in warehouse first.")
                    ElseIf CCommon.ToShort(hfReturnType.Value) = 2 Then
                        DisplayAlert("You have to ship quantity warehouse first.")
                    End If

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainId"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnSaveCreditMemo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveCreditMemo.Click
            Try
                If rbtnAccount.Checked = True Then
                    If CCommon.ToDouble(hdnUnitHourReceived.Value) > 0 Then
                        If hfReturnType.Value = 2 AndAlso ddlCreditAccounts.Enabled AndAlso ddlCreditAccounts.Visible AndAlso ddlCreditAccounts.SelectedIndex = 0 Then
                            DisplayAlert("Select Credit account first.")
                            Exit Sub
                        End If
                        SaveIssueBizDocs(2)

                    ElseIf CCommon.ToInteger(hfReturnType.Value) = 3 OrElse CCommon.ToInteger(hfReturnType.Value) = 4 Then
                        If ddlCreditAccounts.Enabled AndAlso ddlCreditAccounts.Visible AndAlso ddlCreditAccounts.SelectedIndex = 0 Then
                            DisplayAlert("Select Credit account first.")
                            Exit Sub
                        End If
                        SaveIssueBizDocs(2)

                    Else
                        DisplayAlert("You have to receive quantity in warehouse first.")
                    End If

                Else

                    If CCommon.ToDouble(hdnUnitHourReceived.Value) > 0 Then
                        If hfReturnType.Value = 2 AndAlso ddlServiceItem.SelectedIndex = 0 Then
                            DisplayAlert("Select item first.")
                            Exit Sub
                        End If
                        SaveIssueBizDocs(2)

                    ElseIf CCommon.ToInteger(hfReturnType.Value) = 3 OrElse CCommon.ToInteger(hfReturnType.Value) = 4 Then
                        If ddlServiceItem.SelectedIndex = 0 Then
                            DisplayAlert("Select item first.")
                            Exit Sub
                        End If
                        SaveIssueBizDocs(2)
                    Else
                        DisplayAlert("You have to receive quantity in warehouse first.")
                    End If

                End If

                'SaveIssueBizDocs(2)
            Catch ex As Exception
                If ex.Message.Contains("SERIAL/LOT#_USED") Then
                    DisplayAlert("Purchased serial item is used in any sales order or purchase Lot# do not have enough qty left becasuse of used in any sales order.")
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainId"), Session("UserContactID"), Request)
                    DisplayError(CCommon.ToString(ex))
                End If
            End Try
        End Sub

        Private Sub rbtnCreditMemo_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnCreditMemo.CheckedChanged
            Try
                LoadReturn(2)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainId"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub rbtnIssueRefund_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtnIssueRefund.CheckedChanged
            Try
                LoadReturn(1)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainId"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub rbtnIssueDirectRefund_CheckedChanged(sender As Object, e As EventArgs) Handles rbtnIssueDirectRefund.CheckedChanged
            Try
                LoadReturn(3)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainId"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub LoadTransactionHistory()
            Try
                If OppID.Value > 0 Then
                    Dim dtTransHistory As New DataTable
                    Dim objTrans As New TransactionHistory
                    objTrans.TransHistoryID = 0
                    objTrans.OppID = CCommon.ToLong(OppID.Value)
                    objTrans.DivisionID = CCommon.ToLong(hdnDivID.Value)
                    objTrans.DomainID = Session("DomainID")
                    dtTransHistory = objTrans.GetTransactionHistory()

                    pnlApplyDirectRefund.Visible = False

                    If dtTransHistory IsNot Nothing AndAlso dtTransHistory.Rows.Count > 0 Then
                        Dim drFindTransHistory() As DataRow
                        drFindTransHistory = dtTransHistory.Select("tintTransactionStatus in (2,3,5)") '1 = Authorized/Pending Capture 2 = Captured 3 = Void 4=Failed(with any error) 5=Credited
                        If drFindTransHistory.Length > 0 Then
                            dgPaymentDetails.DataSource = drFindTransHistory.CopyToDataTable
                            dgPaymentDetails.DataBind()
                            pnlApplyDirectRefund.Visible = True
                        End If
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ibtnDelCrditMemo_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnDelCrditMemo.Click
            Try
                DeleteReturn(2)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainId"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub ibtnDelRefunct_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ibtnDelRefunct.Click
            Try
                DeleteReturn(1)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainId"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
            Try
                ViewState("IntermediatoryPage") = False
                boolIntermediatoryPage = False
                plhControls.Controls.Clear()
                LoadControls()
                LoadReturnDetails()
                ControlSettings()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                Response.Redirect("../opportunity/frmSalesReturns.aspx?type=" & CCommon.ToShort(hfReturnType.Value))
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Save()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                Save()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub ibtnPrint_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ibtnPrint.Click
            Try
                CreateHTMLBizDocs("Print")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try

        End Sub

        Private Sub ibtnSendEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ibtnSendEmail.Click
            Try
                CreateHTMLBizDocs("Email")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub ibtnExportPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles ibtnExportPDF.Click
            Try
                CreateHTMLBizDocs("PDF")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

#End Region

#Region "METHODS/FUNCTIONS"

        Private Sub LoadReturnDetails()
            Try
                objReturn = New ReturnHeader
                With objReturn
                    .ReturnHeaderID = lngReturnHeaderID
                    .DomainID = Session("DomainID")
                End With

                dtReturnDetails = objReturn.GetReturnHeader()
                If dtReturnDetails IsNot Nothing AndAlso dtReturnDetails.Rows.Count > 0 Then
                    If CCommon.ToLong(Session("UserID")) = 0 AndAlso CCommon.ToLong(Session("UserDivisionID")) <> CCommon.ToLong(dtReturnDetails.Rows(0).Item("numDivisionID")) Then
                        Dim strModuleName, strPermissionName As String
                        m_aryRightsForPage = GetUserRightsForPage_Other(10, 14, strModuleName, strPermissionName)

                        Response.Redirect("~/admin/authentication.aspx?mesg=AC&Module=" & strModuleName & "&Permission=" & strPermissionName)
                    End If

                    If CCommon.ToLong(dtReturnDetails.Rows(0)("numItemCodeForAccount")) > 0 Then
                        rbtnItem.Checked = True
                        If ddlServiceItem.Items.FindByValue(CCommon.ToLong(dtReturnDetails.Rows(0)("numItemCodeForAccount"))) IsNot Nothing Then
                            ddlServiceItem.Items.FindByValue(CCommon.ToLong(dtReturnDetails.Rows(0)("numItemCodeForAccount"))).Selected = True
                        End If
                        ddlCreditAccounts.ClearSelection()
                        ddlCreditAccounts.Items.FindByValue(0).Selected = True

                    Else
                        rbtnAccount.Checked = True
                        For Each li As ListItem In ddlCreditAccounts.Items
                            If li.Value.Contains(dtReturnDetails.Rows(0)("numAccountID") & "~") Then
                                li.Selected = True
                                Exit For
                            End If
                        Next

                        'If ddlCreditAccounts.Items.FindByValue(dtReturnDetails.Rows(0)("numAccountID")) IsNot Nothing Then
                        '    ddlCreditAccounts.Items.FindByValue(dtReturnDetails.Rows(0)("numAccountID")).Selected = True
                        'End If
                        ddlServiceItem.ClearSelection()
                        ddlServiceItem.Items.FindByValue(0).Selected = True

                    End If

                    lblStatus.Text = CCommon.ToString(dtReturnDetails.Rows(0)("vcStatus"))
                    hdnStatus.Value = CCommon.ToLong(dtReturnDetails.Rows(0)("numReturnStatus"))

                    lblWarrantyDaysLeft.Text = dtReturnDetails.Rows(0).Item("numWarrantyDaysLeft")
                    lblWarrantyTooltip.Attributes.Add("title", dtReturnDetails.Rows(0).Item("vcWarrantyNotes"))
                    If CCommon.ToBool(HttpContext.Current.Session("bitDisplayContractElement")) = True AndAlso CheckUserExist(Convert.ToString(HttpContext.Current.Session("UserContactID")), Convert.ToString(HttpContext.Current.Session("vcEmployeeForContractTimeElement"))) Then
                        divContractsDetails.Visible = True
                    Else
                        divContractsDetails.Visible = False
                    End If
                    If CCommon.ToString(hdnStatus.Value) = enmReturnStatus.Returned Then
                        lblReturnDate.Text = If(dtReturnDetails.Rows(0)("dtModifiedDate") Is DBNull.Value, "", FormattedDateFromDate(dtReturnDetails.Rows(0)("dtModifiedDate"), Session("DateFormat")))
                    End If

                    hplSource.Text = CCommon.ToString(dtReturnDetails.Rows(0)("Source"))
                    hplSource.NavigateUrl = "../opportunity/frmOpportunities.aspx?frm=deallist&OpID=" & CCommon.ToLong(dtReturnDetails.Rows(0)("numOppID"))
                    OppID.Value = CCommon.ToLong(dtReturnDetails.Rows(0)("numOppID"))

                    hplCustomer.Text = "<b>Customer :</b> " & dtReturnDetails.Rows(0).Item("vcCompanyName")
                    hdnCRMType.Value = dtReturnDetails.Rows(0).Item("tintCRMType")
                    If dtReturnDetails.Rows(0).Item("tintCRMType") = 0 Then
                        hplCustomer.NavigateUrl = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=oppdetail&DivID=" & dtReturnDetails.Rows(0).Item("numDivisionID") & "&frm1=" & GetQueryStringVal("frm")

                    ElseIf dtReturnDetails.Rows(0).Item("tintCRMType") = 1 Then
                        hplCustomer.NavigateUrl = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=oppdetail&DivID=" & dtReturnDetails.Rows(0).Item("numDivisionID") & "&frm1=" & GetQueryStringVal("frm")

                    ElseIf dtReturnDetails.Rows(0).Item("tintCRMType") = 2 Then
                        hplCustomer.NavigateUrl = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=oppdetail&klds+7kldf=fjk-las&DivId=" & dtReturnDetails.Rows(0).Item("numDivisionID") & "&frm1=" & GetQueryStringVal("frm")
                    End If

                    lblLastModifiedBy.Text = IIf(IsDBNull(dtReturnDetails.Rows(0).Item("ModifiedBy")), "", dtReturnDetails.Rows(0).Item("ModifiedBy"))
                    lblCreatedBy.Text = IIf(IsDBNull(dtReturnDetails.Rows(0).Item("CreatedBy")), "", dtReturnDetails.Rows(0).Item("CreatedBy"))

                    Select Case CCommon.ToShort(dtReturnDetails.Rows(0)("tintReturnType"))
                        Case 1
                            ibtnPrint.Attributes.Add("onclick", "return OpenBizInvoice('" & lngReturnHeaderID & "','" & enmReferenceTypeForMirrorBizDocs.SalesReturns & "',1)")
                            btnReceive.Visible = True

                        Case 2
                            ibtnPrint.Attributes.Add("onclick", "return OpenBizInvoice('" & lngReturnHeaderID & "','" & enmReferenceTypeForMirrorBizDocs.PurchaseReturns & "',1)")
                            btnReceive.Visible = True

                        Case 3
                            ibtnPrint.Attributes.Add("onclick", "return OpenBizInvoice('" & lngReturnHeaderID & "','" & enmReferenceTypeForMirrorBizDocs.CreditMemo & "',1)")
                            btnReceive.Visible = False

                        Case 4
                            ibtnPrint.Attributes.Add("onclick", "return OpenBizInvoice('" & lngReturnHeaderID & "','" & enmReferenceTypeForMirrorBizDocs.RefundReceipt & "',1)")
                            btnReceive.Visible = False

                    End Select

                    hfReturnType.Value = CCommon.ToShort(dtReturnDetails.Rows(0)("tintReturnType"))
                    hfReceiveType.Value = CCommon.ToShort(dtReturnDetails.Rows(0)("tintReceiveType"))
                    hdnOppBizDocID.Value = CCommon.ToLong(dtReturnDetails.Rows(0)("numOppBizDocID"))

                    FillDepositTocombo()

                    If CCommon.ToShort(dtReturnDetails.Rows(0)("tintReceiveType")) = 2 Then
                        PaymentHistory1.ReturnID = CCommon.ToLong(lngReturnHeaderID)
                        PaymentHistory1.BindGrid()
                    End If

                    If CCommon.ToShort(dtReturnDetails.Rows(0)("tintReceiveType")) = 1 Then
                        hrfRefundReceipt.Text = CCommon.ToString(dtReturnDetails.Rows(0)("vcBizDocName"))
                        hrfCreditMemo.Text = ""
                        hrfRefundReceipt.Attributes.Add("onclick", "return OpenBizInvoice(" & lngReturnHeaderID & "," & enmReferenceTypeForMirrorBizDocs.RefundReceipt & ",0);")
                        'rbtnIssueRefund.Checked = True
                        If Not String.IsNullOrEmpty(CCommon.ToString(dtReturnDetails.Rows(0)("vcBizDocName"))) Then
                            tdRefund.Visible = True
                        Else
                            tdRefund.Visible = False
                        End If

                        tdCreditMemo.Visible = False
                        chkRefundReceipt1.Checked = CCommon.ToBool(dtReturnDetails.Rows(0)("IsCreateRefundReceipt"))

                        pnlCredit.Enabled = False
                        pnlRefund.Enabled = True
                        pnlDirectRefund.Enabled = False

                        'If ddlRefundAccounts.Items.FindByValue(dtReturnDetails.Rows(0)("numAccountID")) IsNot Nothing Then
                        '    ddlRefundAccounts.Items.FindByValue(dtReturnDetails.Rows(0)("numAccountID")).Selected = True
                        'End If
                        For Each li As ListItem In ddlRefundAccounts.Items
                            If li.Value.Contains(dtReturnDetails.Rows(0)("numAccountID") & "~") Then
                                li.Selected = True
                                Exit For
                            End If
                        Next
                        rbtnIssueRefund.Checked = True
                        rbtnCreditMemo.Checked = False
                    ElseIf CCommon.ToShort(dtReturnDetails.Rows(0)("tintReceiveType")) = 2 Then
                        hrfCreditMemo.Text = CCommon.ToString(dtReturnDetails.Rows(0)("vcBizDocName"))
                        hrfRefundReceipt.Text = ""
                        Select Case CCommon.ToShort(hfReturnType.Value)
                            Case 1
                                hrfCreditMemo.Attributes.Add("onclick", "return OpenBizInvoice(" & lngReturnHeaderID & "," & enmReferenceTypeForMirrorBizDocs.SalesCreditMemo & ",0);")
                            Case 2
                                hrfCreditMemo.Attributes.Add("onclick", "return OpenBizInvoice(" & lngReturnHeaderID & "," & enmReferenceTypeForMirrorBizDocs.PurchaseCreditMemo & ",0);")
                            Case 3
                                hrfCreditMemo.Attributes.Add("onclick", "return OpenBizInvoice(" & lngReturnHeaderID & "," & enmReferenceTypeForMirrorBizDocs.CreditMemo & ",0);")
                        End Select
                        'rbtnCreditMemo.Checked = True
                        tdRefund.Visible = False
                        If Not String.IsNullOrEmpty(CCommon.ToString(dtReturnDetails.Rows(0)("vcBizDocName"))) Then
                            tdCreditMemo.Visible = True
                        Else
                            tdCreditMemo.Visible = False
                        End If

                        chkRefundReceipt2.Checked = CCommon.ToBool(dtReturnDetails.Rows(0)("IsCreateRefundReceipt"))

                        pnlCredit.Enabled = True
                        pnlRefund.Enabled = False
                        pnlDirectRefund.Enabled = False
                        rbtnIssueRefund.Checked = False
                        rbtnCreditMemo.Checked = True
                    ElseIf CCommon.ToShort(dtReturnDetails.Rows(0)("tintReceiveType")) = 3 Then
                        hrfCreditMemo.Text = ""
                        hrfRefundReceipt.Text = ""

                        tdRefund.Visible = False
                        tdCreditMemo.Visible = False

                        pnlCredit.Enabled = False
                        pnlRefund.Enabled = False
                        pnlDirectRefund.Enabled = True
                        rbtnIssueDirectRefund.Checked = True
                    Else
                        tdRefund.Visible = False
                        tdCreditMemo.Visible = False
                        'btnReceive.Visible = False
                    End If

                    hfEstimatedBizDocAmount.Value = CCommon.ToDecimal(dtReturnDetails.Rows(0)("monAmount"))
                    lblEstimatedBizDocAmount.Text = ReturnMoney(CCommon.ToDecimal(dtReturnDetails.Rows(0)("monAmount")))

                    txtCheckNo.Text = CCommon.ToString(dtReturnDetails.Rows(0)("vcCheckNumber"))
                    hdnBozDocName.Value = CCommon.ToString(dtReturnDetails.Rows(0)("vcBizDocName"))
                    hdnDivID.Value = CCommon.ToLong(dtReturnDetails.Rows(0)("numDivisionId"))
                    hdnContactId.Value = CCommon.ToLong(dtReturnDetails.Rows(0)("numContactId"))
                    hdnRecieveType.Value = CCommon.ToShort(dtReturnDetails.Rows(0)("tintReceiveType"))
                    'hdnBillAddressID.Value = CCommon.ToLong(dtReturnDetails.Rows(0)("vcCheckNumber"))
                    'hdnShipAddressID.Value = CCommon.ToLong(dtReturnDetails.Rows(0)("vcCheckNumber"))
                    txtConEmail.Text = CCommon.ToString(dtReturnDetails.Rows(0)("vcEmail"))
                    txtBizDocTemplate.Text = CCommon.ToLong(dtReturnDetails.Rows(0)("numBizDocTempID"))
                    txtRefType.Text = CCommon.ToLong(dtReturnDetails.Rows(0)("tintReturnType"))
                    hdnUnitHourReceived.Value = CCommon.ToDouble(dtReturnDetails.Rows(0)("numUnitHourReceived"))
                End If

                If CCommon.ToShort(hfReturnType.Value) = 1 Or CCommon.ToShort(hfReturnType.Value) = 2 Then
                    If CCommon.ToShort(hfReturnType.Value = 2) Then
                        For Each dgCol As DataGridColumn In dgItems.Columns
                            If dgCol.HeaderText.Contains("Received") = True Then
                                dgCol.HeaderText = "Shipped Qty"
                            End If

                            If dgCol.HeaderText.Contains("Return Warehouse") = True Then
                                'If CCommon.ToLong(OppID.Value) > 0 And CCommon.ToShort(hfReturnType.Value) = 2 Then

                                dgCol.HeaderText = "Shipped From Warehouse"
                                'End If
                            End If

                        Next
                    End If


                    dgItems.DataSource = dtReturnDetails
                    dgItems.DataBind()
                End If

                If lngReturnHeaderID > 0 Then
                    TransactionInfo1.ReferenceType = enmReferenceType.RMA
                    TransactionInfo1.ReferenceID = CCommon.ToLong(lngReturnHeaderID)
                    TransactionInfo1.getTransactionInfo()
                End If

                If CCommon.ToShort(hfReturnType.Value) = 1 AndAlso OppID.Value > 0 Then
                    LoadTransactionHistory()
                End If

                If CCommon.ToLong(hdnStatus.Value) = CCommon.ToLong(enmReturnStatus.Pending) Then
                    btnReceive.Visible = True
                Else
                    btnReceive.Visible = False
                End If
                ' radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Function CheckUserExist(ByVal UserContactID As String, ByVal SelectedContactIds As String) As Boolean
            Dim strArr As String()
            Dim result As Boolean
            result = False
            strArr = SelectedContactIds.Split(",")
            For Each s As String In strArr
                If UserContactID = s Then
                    result = True
                End If
            Next
            Return result
        End Function
        Sub FillDepositTocombo()
            Try
                Dim dt, dt1 As DataTable
                Dim objCOA As New ChartOfAccounting
                objCOA.DomainID = Session("DomainId")
                'objCOA.AccountCode = "" 'All Accounts
                'dt = objCOA.GetParentCategory()

                If CCommon.ToShort(hfReturnType.Value) = 4 Then 'stand alone refund
                    objCOA.AccountCode = "01010101" 'Bank Accounts
                    'Else 'Sales retunr refund
                    '    objCOA.AccountCode = "0103" 'Income
                    '    dt = objCOA.GetParentCategory()

                    '    objCOA.AccountCode = "0104" 'Expense
                    '    dt1 = objCOA.GetParentCategory()
                    '    dt.Merge(dt1)
                Else
                    objCOA.AccountCode = "" 'All Accounts
                End If
                dt = objCOA.GetParentCategory()


                Dim item, item1 As ListItem

                If ddlRefundAccounts.Items.Count = 0 Then
                    For Each dr As DataRow In dt.Rows
                        item = New ListItem()
                        item.Text = dr("vcAccountName")
                        item.Value = dr("numAccountID") & "~" & dr("vcStartingCheckNumber")
                        item.Attributes("OptionGroup") = dr("vcAccountType")
                        ddlRefundAccounts.Items.Add(item)
                    Next
                    ddlRefundAccounts.Items.Insert(0, New ListItem("--Select One --", "0"))
                End If

                'objCOA.AccountCode = "01010101" 'Bank Accounts
                'dt = objCOA.GetParentCategory()
                If ddlCreditAccounts.Items.Count = 0 Then
                    For Each dr As DataRow In dt.Rows
                        item1 = New ListItem()
                        item1.Text = dr("vcAccountName")
                        item1.Value = dr("numAccountID") & "~" & dr("vcStartingCheckNumber")
                        item1.Attributes("OptionGroup") = dr("vcAccountType")
                        ddlCreditAccounts.Items.Add(item1)
                    Next
                    ddlCreditAccounts.Items.Insert(0, New ListItem("--Select One --", "0"))
                End If

                PersistTable.Load()
                If PersistTable.Count > 0 Then
                    For Each item In ddlRefundAccounts.Items
                        If item.Value.StartsWith(CCommon.ToString(PersistTable(ddlRefundAccounts.ID))) Then
                            ddlRefundAccounts.ClearSelection()
                            item.Selected = True
                            ChangeDepositTo(1)
                            Exit For
                        End If
                    Next

                    For Each item In ddlCreditAccounts.Items
                        If item.Value.StartsWith(CCommon.ToString(PersistTable(ddlCreditAccounts.ID))) Then
                            ddlCreditAccounts.ClearSelection()
                            item.Selected = True
                            ChangeDepositTo(2)
                            Exit For
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadServiceItem()
            Try
                Dim objItem As New CItems
                objItem.DomainID = Session("DomainID")
                objItem.Filter = "S"
                objItem.type = 2
                ddlServiceItem.DataSource = objItem.getItemList()
                ddlServiceItem.DataTextField = "vcItemName"
                ddlServiceItem.DataValueField = "numItemCode"
                ddlServiceItem.DataBind()
                ddlServiceItem.Items.Insert(0, "--Select One--")
                ddlServiceItem.Items.FindByText("--Select One--").Value = "0"
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindWarehouse()
            Try
                Dim objItem As New CItems
                objItem.DomainID = Session("DomainID")
                dtWarehouse = objItem.GetWareHouses()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub ChangeDepositTo(ByVal ReceiveType As Short)
            Try
                Dim lobjCashCreditCard As New CashCreditCard
                Dim ldecOpeningBalance As Decimal = 0

                If ReceiveType = 1 Then
                    If ddlRefundAccounts.SelectedItem.Value <> "0" Then
                        lobjCashCreditCard.AccountId = ddlRefundAccounts.SelectedValue.Split("~")(0)
                        lobjCashCreditCard.DomainID = Session("DomainId")
                        ldecOpeningBalance = lobjCashCreditCard.GetCheckOpeningBalanceForCashCredit()

                        lblRefundBalance.Text = String.Format("<font color={0}>{1} {2}</font>",
                                                                IIf(ldecOpeningBalance < 0, Color.Red, Color.Green),
                                                                Session("Currency").ToString.Trim,
                                                                ReturnMoney(ldecOpeningBalance))

                        txtCheckNo.Text = CCommon.ToLong(ddlRefundAccounts.SelectedValue.Split("~")(1))
                        'hfCheckNo.Value = CCommon.ToLong(ddlRefundAccounts.SelectedValue.Split("~")(1))

                        PersistTable.Clear()
                        PersistTable.Add(ddlRefundAccounts.ID, ddlRefundAccounts.SelectedValue.Split("~")(0))
                        PersistTable.Save()
                    Else
                        lblRefundBalance.Text = String.Format("<font color={0}>{1} {2}</font>",
                                                                IIf(ldecOpeningBalance < 0, Color.Red, Color.Green),
                                                                Session("Currency").ToString.Trim,
                                                                ReturnMoney(0))
                    End If
                Else
                    If ddlCreditAccounts.SelectedItem.Value <> "0" Then
                        lobjCashCreditCard.AccountId = ddlCreditAccounts.SelectedValue.Split("~")(0)
                        lobjCashCreditCard.DomainID = Session("DomainId")
                        ldecOpeningBalance = lobjCashCreditCard.GetCheckOpeningBalanceForCashCredit()

                        lblCreditBalance.Text = String.Format("<font color={0}>{1} {2}</font>",
                                                                IIf(ldecOpeningBalance < 0, Color.Red, Color.Green),
                                                                Session("Currency").ToString.Trim,
                                                                ReturnMoney(ldecOpeningBalance))

                        PersistTable.Clear()
                        PersistTable.Add(ddlCreditAccounts.ID, ddlCreditAccounts.SelectedValue.Split("~")(0))
                        PersistTable.Save()
                    Else
                        lblCreditBalance.Text = String.Format("<font color={0}>{1} {2}</font>",
                                                                IIf(ldecOpeningBalance < 0, Color.Red, Color.Green),
                                                                Session("Currency").ToString.Trim,
                                                                ReturnMoney(0))
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function ReturnMoney(ByVal Money As Object) As String
            Try
                If Not IsDBNull(Money) Then Return String.Format("{0:###0.00}", Money)

                Return ""
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub SaveIssueBizDocs(ByVal ReceiveType As Short)
            Try
                If CCommon.ToShort(hfReceiveType.Value) = 2 Then
                    DisplayAlert("Credit Memo already issued.")
                    Exit Sub
                ElseIf CCommon.ToShort(hfReceiveType.Value) = 1 Then
                    DisplayAlert("Refund Receipt already issued.")
                    Exit Sub
                ElseIf CCommon.ToShort(hfReceiveType.Value) = 3 Then
                    DisplayAlert("Refund already issued.")
                    Exit Sub
                End If

                Dim lngItemCode As Long = 0
                If rbtnItem.Checked = True Then
                    Dim dtItemDetails As DataTable
                    Dim dtAssetItem As New DataTable
                    Dim objItem As New CItems

                    If Not objItem.ValidateItemAccount(CCommon.ToLong(ddlServiceItem.SelectedValue), Session("DomainID"), 1) = True Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "AccountValidation", "alert('Please Set Income,Asset,COGs(Expense) Account for selected Item from Administration->Inventory->Item Details')", True)
                        Return
                    End If

                    objItem.ItemCode = CCommon.ToLong(ddlServiceItem.SelectedValue)
                    objItem.ClientZoneOffsetTime = Session("ClientMachineUTCTimeOffset")
                    dtItemDetails = objItem.ItemDetails

                    lngAccountID = CCommon.ToLong(dtItemDetails.Rows(0)("numIncomeChartAcntId"))
                    lngItemCode = CCommon.ToLong(ddlServiceItem.SelectedValue)

                ElseIf rbtnAccount.Checked = True Then
                    lngAccountID = CCommon.ToLong(ddlCreditAccounts.SelectedValue.Split("~")(0))

                End If

                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    objReturn = New ReturnHeader
                    With objReturn
                        .ReturnHeaderID = lngReturnHeaderID
                        .ReceiveType = ReceiveType
                        .ReturnStatus = enmReturnStatus.Returned
                        .AccountID = IIf(ReceiveType = 1, CCommon.ToLong(ddlRefundAccounts.SelectedValue.Split("~")(0)), lngAccountID)
                        .ItemCode = lngItemCode
                        .CheckNumber = txtCheckNo.Text
                        .IsCreateRefundReceipt = If(ReceiveType = 1, chkRefundReceipt1.Checked, chkRefundReceipt2.Checked)
                        .DomainID = Session("DomainID")
                        .UserCntID = Session("UserContactID")
                        .ManageReturnBizDocs()

                        hfReceiveType.Value = ReceiveType
                        SaveJournalEntries()
                    End With

                    objTransactionScope.Complete()
                End Using

                If ReceiveType = 1 Then
                    DisplayAlert("Refund Receipt issued successfully.")
                ElseIf ReceiveType = 2 Then
                    DisplayAlert("Credit Memo issued successfully.")
                End If

                LoadControls()
                LoadReturnDetails()
            Catch ex As Exception
                If CCommon.ToString(ex.Message).Contains("PurchaseReturn_Qty") Then
                    DisplayAlert("Quantity not sufficient on OnHand.")
                ElseIf CCommon.ToString(ex.Message).Contains("REFUND_RECEIPT_ALREADY_ISSUED") Then
                    DisplayAlert("Refund Receipt already issued.")
                ElseIf CCommon.ToString(ex.Message).Contains("CREDIT_MEMO_ALREADY_ISSUED") Then
                    DisplayAlert("Credit Memo already issued.")
                ElseIf CCommon.ToString(ex.Message).Contains("REFUND_ALREADY_ISSUED") Then
                    DisplayAlert("Refund already issued.")
                ElseIf ex.Message.Contains("MISSING_WAREHOUSE") Then
                    DisplayAlert("Warehouse selected for kit item return is not available for child item(s).")
                Else
                    Throw ex
                End If
            End Try
        End Sub

        Private Sub DeleteReturn(ByVal ReceiveType As Integer)
            Try
                objReturn = New ReturnHeader

                With objReturn
                    .ReturnHeaderID = lngReturnHeaderID
                    .ReturnStatus = enmReturnStatus.Received
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")

                    If .DeleteReturnHeader() = True Then
                        If ReceiveType = 1 Then
                            LoadReturn(1)
                            LoadReturnDetails()
                            rbtnIssueRefund.Checked = True
                            rbtnCreditMemo.Checked = False
                            DisplayAlert("Refund Receipt deleted successfully.")

                        ElseIf ReceiveType = 2 Then
                            LoadReturn(2)
                            LoadReturnDetails()
                            rbtnIssueRefund.Checked = False
                            rbtnCreditMemo.Checked = True
                            DisplayAlert("Credit Memo deleted successfully.")

                        End If

                    Else
                        DisplayAlert("Error occurred while deleting records.")

                    End If
                End With

                hfReceiveType.Value = 0

                'rbtnIssueRefund.AutoPostBack = True
                'rbtnCreditMemo.AutoPostBack = True

            Catch ex As Exception
                If ex.Message = "CreditMemo_PAID" Then
                    DisplayAlert("Credit Memo applied.")
                ElseIf ex.Message = "Serial_LotNo_Used" Then
                    DisplayAlert("Returned serial is used in any sales order or returned Lot# do not have enough qty becasuse of used in any sales order.")
                Else
                    Throw ex
                End If
            End Try
        End Sub

        Private Sub LoadReturn(ByVal ReceiveType As Integer)
            Try
                If CCommon.ToShort(hfReceiveType.Value) = 2 Then
                    DisplayAlert("Credit Memo already issued.")
                    rbtnCreditMemo.Checked = True
                    rbtnIssueRefund.Checked = False
                    rbtnIssueDirectRefund.Checked = False

                    Exit Sub
                ElseIf CCommon.ToShort(hfReceiveType.Value) = 1 Then
                    DisplayAlert("Refund Receipt already issued.")
                    rbtnCreditMemo.Checked = False
                    rbtnIssueRefund.Checked = True
                    rbtnIssueDirectRefund.Checked = False

                    Exit Sub
                ElseIf CCommon.ToShort(hfReceiveType.Value) = 3 Then
                    DisplayAlert("Refund already issued.")
                    rbtnCreditMemo.Checked = False
                    rbtnIssueRefund.Checked = False
                    rbtnIssueDirectRefund.Checked = True

                    Exit Sub
                End If

                objReturn = New ReturnHeader
                Dim dtDetails As New DataTable
                With objReturn

                    .ReturnHeaderID = lngReturnHeaderID
                    .DomainID = Session("DomainID")
                    .ReceiveType = ReceiveType

                    dtDetails = .GetReturnHeader()

                    If dtDetails IsNot Nothing AndAlso dtDetails.Rows.Count > 0 Then

                        If String.IsNullOrEmpty(CCommon.ToString(dtDetails.Rows(0)("vcBizDocName"))) Then

                            hfEstimatedBizDocAmount.Value = CCommon.ToDecimal(dtDetails.Rows(0)("monEstimatedBizDocAmount"))
                            lblEstimatedBizDocAmount.Text = ReturnMoney(CCommon.ToDecimal(dtDetails.Rows(0)("monEstimatedBizDocAmount")))

                            If ReceiveType = 1 Then
                                pnlCredit.Enabled = False
                                pnlRefund.Enabled = True
                                pnlDirectRefund.Enabled = False
                            ElseIf ReceiveType = 2 Then
                                pnlCredit.Enabled = True
                                pnlRefund.Enabled = False
                                pnlDirectRefund.Enabled = False
                            ElseIf ReceiveType = 3 Then
                                pnlCredit.Enabled = False
                                pnlRefund.Enabled = False
                                pnlDirectRefund.Enabled = True
                            End If

                        Else
                            If ReceiveType = 1 AndAlso String.IsNullOrEmpty(hrfRefundReceipt.Text) = True Then
                                DisplayAlert("To issue Refund Receipt, you need to delete issued Credit Memo first.")
                            Else
                                If ReceiveType = 2 AndAlso String.IsNullOrEmpty(hrfCreditMemo.Text) = True Then
                                    DisplayAlert("To issue Credit Memo, you need to delete issued Refund Receipt first.")
                                Else
                                    DisplayAlert("")
                                End If
                            End If

                        End If

                    End If

                End With
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadControls()
            Try
                plhControls.Controls.Clear()
                Dim ds As DataSet
                Dim objPageLayout As New CPageLayout
                Dim Type As Char
                Dim numFormId As Integer

                '1:SalesReturn  
                '2:PurchaseReturn 
                '3:StandAlone CreditMemo 
                '4:UnApplied Amount(Credit) 
                '5:StandAlone Refund 
                '6:Purchase  Credit

                If CCommon.ToShort(hfReturnType.Value) = 1 Then
                    Type = "R"
                    numFormId = 60
                    objPageLayout.PageId = 7
                ElseIf CCommon.ToShort(hfReturnType.Value) = 2 Then
                    Type = "R"
                    numFormId = 61
                    objPageLayout.PageId = 8
                ElseIf CCommon.ToShort(hfReturnType.Value) = 3 Then
                    Type = "R"
                    numFormId = 62
                    objPageLayout.PageId = 7
                ElseIf CCommon.ToShort(hfReturnType.Value) = 4 Then
                    Type = "R"
                    numFormId = 63
                    objPageLayout.PageId = 7
                End If

                btnLayout.Attributes.Add("onclick", "return ShowLayout('" & Type & "','" & lngReturnHeaderID & "','" & numFormId & "');")

                objPageLayout.CoType = Type
                objPageLayout.UserCntID = Session("UserContactId")
                objPageLayout.RecordId = lngReturnHeaderID
                objPageLayout.DomainID = Session("DomainID")

                objPageLayout.PageType = 3
                objPageLayout.FormId = numFormId

                ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
                dtTableInfo = ds.Tables(0)

                objReturn = New ReturnHeader
                objReturn.ReturnHeaderID = lngReturnHeaderID
                objReturn.DomainID = Session("DomainID")
                objReturn.GetReturnDetails()
                lblReturns.Text = "(" & objReturn.Rma & ")"
                Dim intAddedColumns As Int16 = 0

                Dim divRow As HtmlGenericControl
                Dim divMain1 As HtmlGenericControl
                Dim divMain2 As HtmlGenericControl
                Dim divForm1 As HtmlGenericControl
                Dim divForm2 As HtmlGenericControl

                Dim isFirstColumnUsed As Boolean = False

                For Each dr As DataRow In dtTableInfo.Rows
                    If intAddedColumns = 0 Then
                        divRow = New HtmlGenericControl("div")
                        divRow.Attributes.Add("class", "row")

                        divMain1 = New HtmlGenericControl("div")
                        divMain1.Attributes.Add("class", "col-xs-12 col-sm-6")

                        divForm1 = New HtmlGenericControl("div")
                        divForm1.Attributes.Add("class", "form-group")

                        divMain2 = New HtmlGenericControl("div")
                        divMain2.Attributes.Add("class", "col-xs-12 col-sm-6")

                        divForm2 = New HtmlGenericControl("div")
                        divForm2.Attributes.Add("class", "form-group")

                        isFirstColumnUsed = False
                    End If


                    If boolIntermediatoryPage = True Then
                        If Not IsDBNull(dr("vcPropertyName")) And (dr("fld_type") = "SelectBox" Or dr("fld_type") = "SelectBox") And dr("bitCustomField") = False Then
                            dr("vcPropertyName") = dr("vcPropertyName") & "Name"
                        End If
                    End If

                    If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False Then
                        If dr("vcPropertyName") = "CalcAmount" Or dr("vcPropertyName") = "InvoiceGrandTotal" Or dr("vcPropertyName") = "TotalAmountPaid" Or (dr("vcPropertyName") = "Amount" And boolIntermediatoryPage = True) Then
                            'dr("vcValue") = objReturn.GetType.GetProperty("CurrSymbol").GetValue(objReturn, Nothing) & " " & objReturn.GetType.GetProperty(dr("vcPropertyName")).GetValue(objReturn, Nothing)
                            dr("vcValue") = objReturn.GetType.GetProperty(dr("vcPropertyName")).GetValue(objReturn, Nothing)
                        Else
                            dr("vcValue") = objReturn.GetType.GetProperty(dr("vcPropertyName")).GetValue(objReturn, Nothing)

                        End If
                    End If

                    If dr("vcPropertyName") = "Amount" And boolIntermediatoryPage = False Then
                        dr("vcFieldName") = dr("vcFieldName") '& " (" & objReturn.GetType.GetProperty("CurrSymbol").GetValue(objReturn, Nothing) & ")"
                    End If

                    If (dr("fld_type") = "ListBox" Or dr("fld_type") = "SelectBox") Then
                        Dim dtData As DataTable

                        If dr("vcPropertyName") = "AssignedTo" Then ' If dr("numFieldId") = 106 Or dr("numFieldId") = 262 Then
                            If boolIntermediatoryPage = False Then
                                If Session("PopulateUserCriteria") = 1 Then

                                    dtData = objCommon.ConEmpListFromTerritories(Session("DomainID"), 0, 0, objOpportunity.TerritoryID)

                                ElseIf Session("PopulateUserCriteria") = 2 Then
                                    dtData = objCommon.ConEmpList(Session("DomainID"), Session("UserContactID"))
                                Else
                                    dtData = objCommon.ConEmpList(Session("DomainID"), 0, 0)
                                End If

                                'Commissionable Contacts
                                Dim objUser As New UserAccess
                                objUser.DomainID = Context.Session("DomainID")
                                objUser.byteMode = 1

                                Dim dt As DataTable = objUser.GetCommissionsContacts

                                Dim item As ListItem
                                Dim dr1 As DataRow

                                For Each dr2 As DataRow In dt.Rows
                                    dr1 = dtData.NewRow
                                    dr1(0) = dr2("numContactID")
                                    dr1(1) = dr2("vcUserName")
                                    dtData.Rows.Add(dr1)
                                Next
                            End If

                        ElseIf Not IsDBNull(dr("numListID")) And dr("numListID") <> 0 Then
                            If boolIntermediatoryPage = False Then
                                If dr("ListRelID") > 0 Then
                                    objCommon.Mode = 3
                                    objCommon.DomainID = Session("DomainID")
                                    objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objOpportunity)
                                    objCommon.SecondaryListID = dr("numListId")
                                    dtData = objCommon.GetFieldRelationships.Tables(0)
                                Else
                                    dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                End If
                            End If
                        End If


                        If boolIntermediatoryPage Then
                            objPageControls.CreateCells(GetParentDIV(CCommon.ToInteger(dr("intCoulmn")), isFirstColumnUsed:=isFirstColumnUsed, divForm1:=divForm1, divForm2:=divForm2), dr, boolIntermediatoryPage, dtData)
                        Else
                            Dim ddl As DropDownList
                            ddl = objPageControls.CreateCells(GetParentDIV(CCommon.ToInteger(dr("intCoulmn")), isFirstColumnUsed:=isFirstColumnUsed, divForm1:=divForm1, divForm2:=divForm2), dr, boolIntermediatoryPage, dtData)
                            If CLng(dr("DependentFields")) > 0 And Not boolIntermediatoryPage Then
                                ddl.AutoPostBack = True
                                'AddHandler ddl.SelectedIndexChanged, AddressOf PopulateDependentDropdown
                            End If
                        End If
                    Else
                        objPageControls.CreateCells(GetParentDIV(CCommon.ToInteger(dr("intCoulmn")), isFirstColumnUsed:=isFirstColumnUsed, divForm1:=divForm1, divForm2:=divForm2), dr, boolIntermediatoryPage, RecordID:=lngReturnHeaderID)
                    End If

                    intAddedColumns += 1

                    If intAddedColumns = 2 Then
                        intAddedColumns = 0

                        divMain1.Controls.Add(divForm1)
                        divMain2.Controls.Add(divForm2)

                        divRow.Controls.Add(divMain1)
                        divRow.Controls.Add(divMain2)

                        plhControls.Controls.Add(divRow)

                        'CREATES NEW ROW
                        divMain1 = New HtmlGenericControl("div")
                        divMain1.Attributes.Add("class", "col-xs-12 col-sm-6")

                        divForm1 = New HtmlGenericControl("div")
                        divForm1.Attributes.Add("class", "form-group")

                        divMain2 = New HtmlGenericControl("div")
                        divMain2.Attributes.Add("class", "col-xs-12 col-sm-6")

                        divForm2 = New HtmlGenericControl("div")
                        divForm2.Attributes.Add("class", "form-group")

                        isFirstColumnUsed = False
                    ElseIf dtTableInfo.Rows.IndexOf(dr) = dtTableInfo.Rows.Count - 1 Then
                        divMain1.Controls.Add(divForm1)
                        divMain2.Controls.Add(divForm2)

                        divRow.Controls.Add(divMain1)
                        divRow.Controls.Add(divMain2)

                        plhControls.Controls.Add(divRow)
                    End If
                Next

                'Add Client Side validation for custom fields
                If Not boolIntermediatoryPage Then
                    Dim strValidation As String = objPageControls.GenerateValidationScript(dtTableInfo)
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "CFvalidation", strValidation, True)
                ElseIf Session("InlineEdit") = True And m_aryRightsForPage(RIGHTSTYPE.UPDATE) <> 0 Then
                    Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtTableInfo)
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "InlineEditValidation", strValidation, True)
                End If
                If Session("InlineEdit") = True And m_aryRightsForPage(RIGHTSTYPE.UPDATE) <> 0 Then
                    Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtTableInfo)
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "InlineEditValidation", strValidation, True)
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Function GetParentDIV(ByVal intColumn As Int16, ByRef isFirstColumnUsed As Boolean, ByRef divForm1 As HtmlGenericControl, ByRef divForm2 As HtmlGenericControl) As HtmlGenericControl
            If intColumn = 1 Then
                Return divForm1
            ElseIf intColumn = 2 Then
                Return divForm2
            ElseIf isFirstColumnUsed Then
                isFirstColumnUsed = False
                Return divForm2
            Else
                isFirstColumnUsed = True
                Return divForm1
            End If
        End Function

        Private Sub Save()
            Try
                objReturn = New ReturnHeader
                objReturn.ReturnHeaderID = lngReturnHeaderID
                objReturn.UserCntID = Session("UserContactID")
                objReturn.DomainID = Session("DomainId")

                For Each dr As DataRow In dtTableInfo.Rows
                    If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False And dr("bitCanBeUpdated") = True Then
                        objPageControls.SetValueForStaticFields(dr, objReturn, radOppTab.MultiPage)
                    End If
                Next

                If objReturn.ReturnStatus = CCommon.ToLong(enmReturnStatus.Pending) Or objReturn.ReturnStatus = CCommon.ToLong(enmReturnStatus.Received) Then
                    If CCommon.ToShort(hfReceiveType.Value) = 2 Then
                        DisplayAlert("You are not allowed change Return Status, your option is to delete credit memo(bizdoc) and then try again.")
                        Exit Sub
                    ElseIf CCommon.ToShort(hfReceiveType.Value) = 1 Then
                        DisplayAlert("You are not allowed change Return Status, your option is to delete Refund Receipt(bizdoc) and then try again.")
                        Exit Sub
                    ElseIf CCommon.ToShort(hfReceiveType.Value) = 3 Then
                        DisplayAlert("You are not allowed change Return Status, your option is to delete Direct Refund(bizdoc) and then try again.")
                        Exit Sub
                    ElseIf CCommon.ToLong(hdnStatus.Value) = CCommon.ToLong(enmReturnStatus.Returned) Then
                        DisplayAlert("Item is already Returned, Not allowed change Return Status.")
                        Exit Sub
                    End If
                End If

                objReturn.BizDocName = hdnBozDocName.Value
                objReturn.ContactId = CCommon.ToLong(hdnContactId.Value)
                objReturn.DivisionId = CCommon.ToLong(hdnDivID.Value)
                objReturn.ReceiveType = CCommon.ToLong(hdnRecieveType.Value)
                objReturn.strItems = ""

                If objReturn.ManageReturnHeader() > 0 Then
                    DisplayAlert("Records saved successfully.")
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Reload", "self.close();", True)
                Else
                    DisplayAlert("Error occurred while saving records.")
                End If

                Page_Redirect()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub ControlSettings()
            If boolIntermediatoryPage = True Then
                btnSave.Visible = False
                btnSaveClose.Visible = False
                btnEdit.Visible = True
            Else
                btnSave.Visible = True
                btnSaveClose.Visible = True
                btnEdit.Visible = False
            End If
        End Sub

        Private Sub Page_Redirect()
            Try
                Response.Redirect("../opportunity/frmReturnDetail.aspx?type=" & CCommon.ToShort(hfReturnType.Value) & "&ReturnID=" & lngReturnHeaderID)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub CreateHTMLBizDocs(ByVal CommandType As String)
            Try
                Dim RefType As Integer
                '1:SalesReturn  2:PurchaseReturn 3:StandAlone CreditMemo 4:UnApplied Amount(Credit) 5:StandAlone Refund 6:Purchase  Credit', 'SCHEMA', N'dbo', 'TABLE', N'ReturnHeader', 'COLUMN', N'tintReturnType'
                If CCommon.ToShort(hfReturnType.Value) = 1 Then
                    RefType = enmReferenceTypeForMirrorBizDocs.SalesReturns

                ElseIf CCommon.ToShort(hfReturnType.Value) = 2 Then
                    RefType = enmReferenceTypeForMirrorBizDocs.PurchaseReturns

                ElseIf CCommon.ToShort(hfReturnType.Value) = 3 Then
                    RefType = enmReferenceTypeForMirrorBizDocs.CreditMemo

                ElseIf CCommon.ToShort(hfReturnType.Value) = 4 Then
                    RefType = enmReferenceTypeForMirrorBizDocs.RefundReceipt

                End If

                Dim sw As New System.IO.StringWriter()
                Server.Execute("frmMirrorBizdoc.aspx" & QueryEncryption.EncryptQueryString("RefID=" & lngReturnHeaderID & "&RefType=" & RefType & "&DocGenerate=1"), sw)
                Dim htmlCodeToConvert As String = sw.GetStringBuilder().ToString()
                sw.Close()

                If CommandType = "Email" Then
                    Session("Attachements") = Nothing 'reset to nothing to avoid duplicate attachments
                    Dim objBizDocs As New OppBizDocs
                    Dim dt As DataTable
                    objBizDocs.BizDocId = lngReturnHeaderID 'lngOppBizDocID
                    objBizDocs.DomainID = Session("DomainID")
                    objBizDocs.BizDocTemplateID = CCommon.ToLong(txtBizDocTemplate.Text)
                    dt = objBizDocs.GetBizDocAttachments
                    Dim i As Integer
                    Dim strFileName As String = ""
                    Dim strFilePhysicalLocation As String = ""
                    For i = 0 To dt.Rows.Count - 1
                        If dt.Rows(i).Item("vcDocName") = "BizDoc" Then
                            Dim objHTMLToPDF As New HTMLToPDF
                            strFileName = objHTMLToPDF.ConvertHTML2PDF(htmlCodeToConvert, CCommon.ToLong(Session("DomainID")))
                        Else
                            strFileName = dt.Rows(i).Item("vcURL")
                        End If
                        strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
                        objCommon.AddAttchmentToSession(strFileName, CCommon.GetDocumentPath(Session("DomainID")) & strFileName, strFilePhysicalLocation)
                    Next
                    Dim str As String
                    str = "<script language='javascript'>window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&PickAtch=1&isAttachmentRequired=1&Lsemail=" & HttpUtility.JavaScriptStringEncode(txtConEmail.Text) & "&pqwRT=" & CCommon.ToLong(hdnContactId.Value) & "&BizDocID=" & CCommon.ToShort(hfReturnType.Value).ToString() & "&OppID=" & lngReturnHeaderID.ToString() & "&OppType=" & CCommon.ToShort(hfReturnType.Value) & "','ComposeWindow','toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')</script>"
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Email", str, False)
                ElseIf CommandType = "PDF" Then
                    Dim objBizDocs As New OppBizDocs
                    Dim strFileName As String = ""
                    Dim strFilePhysicalLocation As String = ""

                    Dim objHTMLToPDF As New HTMLToPDF
                    strFileName = objHTMLToPDF.ConvertHTML2PDF(htmlCodeToConvert, CCommon.ToLong(Session("DomainID")))
                    strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName

                    Response.Clear()
                    Response.ClearContent()
                    Response.ContentType = "application/pdf"
                    Response.AddHeader("content-disposition", "attachment; filename=" + strFileName)
                    Response.WriteFile(strFilePhysicalLocation)

                    Response.End()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

#End Region

#Region "DROPDOWN EVENTS"

        Private Sub ddlRefundAccounts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRefundAccounts.SelectedIndexChanged
            Try
                ChangeDepositTo(1)
                'radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub


        Private Sub ddlCreditAccounts_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCreditAccounts.SelectedIndexChanged
            Try
                ChangeDepositTo(2)
                'radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
#End Region

#Region "GRID EVENTS"


        Function validateLotSerial() As Boolean
            Dim strMessage As String
            Dim decQtyFulfilled As Decimal

            For Each dgBizGridItem As DataGridItem In dgItems.Items
                'iterate through inventory items only
                If CType(dgBizGridItem.FindControl("hdncharItemType"), HiddenField).Value.ToUpper() = "P" And CCommon.ToDecimal(CType(dgBizGridItem.FindControl("lblUnitReceived"), Label).Text) > 0 Then

                    If CType(dgBizGridItem.FindControl("hfbitSerialized"), HiddenField).Value = True Or CType(dgBizGridItem.FindControl("hfbitLotNo"), HiddenField).Value = True Then
                        Dim txLot As TextBox = CType(dgBizGridItem.FindControl("txLot"), TextBox)
                        If (txLot.Text.Trim.Length > 0) Then
                            Dim str As String()

                            Dim i As Integer
                            Dim iTotal As Integer = 0

                            If CType(dgBizGridItem.FindControl("hfbitLotNo"), HiddenField).Value = True Then
                                Dim strLot, strLotSerial As String()
                                strLot = txLot.Text.Split(",")

                                Dim LotName As String


                                For i = 0 To strLot.Length - 1
                                    Dim pattern As String = "(?<LotName>(\w*))\s*\((?<LotQty>(\d*))\)"
                                    Dim matches As MatchCollection = Regex.Matches(strLot(i), pattern)

                                    If (matches.Count > 0) Then
                                        iTotal += matches(0).Groups("LotQty").ToString()
                                        LotName = matches(0).Groups("LotName").ToString()
                                    Else
                                        iTotal += 1
                                    End If
                                Next
                            Else
                                iTotal = txLot.Text.Split(",").Length
                            End If

                            decQtyFulfilled = CCommon.ToDecimal(CType(dgBizGridItem.FindControl("lblUnitReceived"), Label).Text)
                            If (decQtyFulfilled <> iTotal) Then
                                Return False
                            End If
                        Else
                            Return False
                        End If
                    End If
                End If
            Next

            Return True
        End Function

        Private Sub dgItems_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgItems.ItemCommand
            Try
                If e.CommandName = "SaveReceivedQty" Then
                    If CCommon.ToShort(hfReceiveType.Value) = 2 Then
                        DisplayAlert("To received qty, you need to delete issued Credit Memo first.")
                        Exit Sub
                    ElseIf CCommon.ToShort(hfReceiveType.Value) = 1 Then
                        DisplayAlert("To received qty, you need to delete issued Refund Receipt first.")
                        Exit Sub
                    ElseIf CCommon.ToShort(hfReceiveType.Value) = 3 Then
                        DisplayAlert("To received qty, you need to delete issued direct Refund.")
                        Exit Sub
                    End If

                    If validateLotSerial() = False Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Alert", "alert('Please verify Serial/Lot #s' );", True)
                        Exit Sub
                    End If

                    objReturn = New ReturnHeader
                    Dim dtItems As New DataTable
                    Dim dsItems As New DataSet
                    Dim dtWarehouseItems As New DataTable
                    Dim lngWarehouseItemID As Long
                    dtItems.TableName = "Item"
                    dtItems.Columns.Add("numReturnItemID")
                    dtItems.Columns.Add("numItemCode")
                    dtItems.Columns.Add("numUnitHour")
                    dtItems.Columns.Add("numUnitHourReceived")
                    dtItems.Columns.Add("monPrice")
                    dtItems.Columns.Add("monTotAmount")
                    dtItems.Columns.Add("vcItemDesc")
                    dtItems.Columns.Add("numWareHouseItemID")
                    dtItems.Columns.Add("vcModelID")
                    dtItems.Columns.Add("vcManufacturer")
                    dtItems.Columns.Add("numUOMId")


                    Dim drItem As DataRow
                    Dim drArray() As DataRow
                    For i As Integer = 0 To dgItems.Items.Count - 1
                        drItem = dtItems.NewRow
                        drItem("numReturnItemID") = CCommon.ToLong(DirectCast(dgItems.Items(i).FindControl("hdnReturnItemID"), HiddenField).Value)
                        drItem("numUnitHourReceived") = CCommon.ToDouble(DirectCast(dgItems.Items(i).FindControl("lblUnitReceived"), Label).Text)

                        lngWarehouseItemID = 0
                        If (DirectCast(dgItems.Items(i).FindControl("hdncharItemType"), HiddenField)).Value = "P" Then
                            If Not dgItems.Items(i).FindControl("radWareHouse") Is Nothing Then
                                Dim radWarehouse As Telerik.Web.UI.RadComboBox = DirectCast(dgItems.Items(i).FindControl("radWareHouse"), Telerik.Web.UI.RadComboBox)
                                lngWarehouseItemID = CCommon.ToLong(radWarehouse.SelectedValue)
                            End If

                            If lngWarehouseItemID = 0 Then
                                DisplayAlert("You must have to select warehouse.")
                                Exit Sub
                            End If
                        End If

                        drItem("numWareHouseItemID") = lngWarehouseItemID

                        dtItems.Rows.Add(drItem)
                        dtItems.AcceptChanges()
                    Next

                    dsItems.Tables.Add(dtItems)
                    objReturn.strItems = dsItems.GetXml()
                Else
                    objReturn.strItems = ""
                End If

                objReturn.sMode = 1
                objReturn.ReturnStatus = CCommon.ToLong(enmReturnStatus.Received)

                objReturn.ReturnHeaderID = lngReturnHeaderID
                objReturn.UserCntID = Session("UserContactID")
                objReturn.DomainID = Session("DomainId")

                Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                    If objReturn.ManageReturnHeader() > 0 Then
                        DisplayAlert("Records saved successfully. Please proceed to the """ & radOppTab.FindTabByValue("IssueAndCredit").Text & """ sub-tab to create the """ & IIf(CCommon.ToShort(hfReturnType.Value) = 1, "Credit Memo or Refund", "Credit Memo") & """ for your Vendor.")
                        InsertRMALotSerial()
                    Else
                        DisplayAlert("Error occurred while saving records.")
                    End If

                    objTransactionScope.Complete()
                End Using

                LoadControls()
                LoadReturnDetails()
            Catch ex As Exception
                If ex.Message.Contains("KIT_ITEM_WAREHOUSE_NOT_AVAILABLE_IN_ALL_CHILD_ITEMS") Then
                    DisplayAlert("Warehouse selected for kit item return is not available for child item(s).")
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(CCommon.ToString(ex))
                End If
            End Try
        End Sub


        Sub InsertRMALotSerial()
            Try
                Dim strMessage As String
                Dim decQtyFulfilled As Decimal
                Dim drArray() As DataRow

                For Each dgBizGridItem As DataGridItem In dgItems.Items
                    If CType(dgBizGridItem.FindControl("hdncharItemType"), HiddenField).Value.ToUpper() = "P" And CCommon.ToDecimal(CType(dgBizGridItem.FindControl("lblUnitReceived"), Label).Text) > 0 Then
                        If CType(dgBizGridItem.FindControl("hfbitSerialized"), HiddenField).Value = True Or CType(dgBizGridItem.FindControl("hfbitLotNo"), HiddenField).Value = True Then
                            Dim hdnSelectedLotSerial As HiddenField = CType(dgBizGridItem.FindControl("hdnSelectedLotSerial"), HiddenField)
                            Dim radWarehouse As Telerik.Web.UI.RadComboBox = DirectCast(dgBizGridItem.FindControl("radWareHouse"), Telerik.Web.UI.RadComboBox)

                            Dim str As String()
                            objReturn = New ReturnHeader
                            objReturn.DomainID = Session("DomainId")
                            objReturn.ReturnHeaderID = lngReturnHeaderID
                            objReturn.WareHouseItemID = CCommon.ToLong(radWarehouse.SelectedValue)
                            objReturn.ReturnItemID = CCommon.ToLong(CType(dgBizGridItem.FindControl("hdnReturnItemID"), HiddenField).Value)
                            objReturn.Mode = IIf(hfReturnType.Value = 1, 3, 4)
                            objReturn.strItems = hdnSelectedLotSerial.Value.Trim

                            objReturn.ManageRMALotSerial()
                        End If
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function CreateWarehouseForItem(lngItemCode As Long, lngWarehouseID As Long) As Long
            Try
                Dim objItems As New CItems
                objItems.ItemCode = lngItemCode
                objItems.WareHouseItemID = 0
                objItems.WarehouseID = lngWarehouseID
                objItems.WLocation = ""
                objItems.WListPrice = 0
                objItems.OnHand = 0
                objItems.ReOrder = 0
                objItems.WSKU = ""
                objItems.WBarCode = ""
                objItems.DomainID = Session("DomainID")
                objItems.UserCntID = Session("UserContactID")
                objItems.byteMode = 1
                objItems.strFieldList = ""
                objItems.AddUpdateWareHouseForItems()
                Return objItems.WareHouseItemID
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Private Sub dgItems_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgItems.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Header Then

                ElseIf e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
                    'do not show warehouse dropdown for serive and non inventory item
                    If CType(e.Item.FindControl("hdnItemType"), HiddenField).Value.ToUpper() = "S" Or CType(e.Item.FindControl("hdnItemType"), HiddenField).Value.ToUpper() = "N" Then
                        e.Item.FindControl("hlSerialized").Visible = False
                        e.Item.FindControl("txLot").Visible = False
                        If Not e.Item.FindControl("radWareHouse") Is Nothing Then
                            e.Item.FindControl("radWareHouse").Visible = False
                        End If
                        Exit Sub
                    End If

                    If sMode = 0 AndAlso CCommon.ToLong(hdnStatus.Value) = CCommon.ToLong(enmReturnStatus.Pending) Then
                        dgItems.Columns(6).Visible = False
                    Else
                        dgItems.Columns(6).Visible = True
                    End If

                    If Not e.Item.FindControl("radWareHouse") Is Nothing Then
                        Dim radWarehouse As Telerik.Web.UI.RadComboBox = DirectCast(e.Item.FindControl("radWareHouse"), Telerik.Web.UI.RadComboBox)

                        If objCommon Is Nothing Then objCommon = New CCommon
                        Dim dt As DataTable = objCommon.GetWarehousesForSelectedItem(DataBinder.Eval(e.Item.DataItem, "numItemCode"), 0, 0, 0, 0, 0, 0, "")
                        radWarehouse.DataSource = dt
                        radWarehouse.DataTextField = "vcWarehouse"
                        radWarehouse.DataValueField = "numWareHouseItemID"
                        radWarehouse.DataBind()

                        If Not radWarehouse.FindItemByValue(DataBinder.Eval(e.Item.DataItem, "numWareHouseItemID")) Is Nothing Then
                            radWarehouse.FindItemByValue(DataBinder.Eval(e.Item.DataItem, "numWareHouseItemID")).Selected = True
                        End If
                    End If

                    If (DataBinder.Eval(e.Item.DataItem, "bitSerialized") = True Or DataBinder.Eval(e.Item.DataItem, "bitLotNo") = True) Then
                        e.Item.FindControl("txLot").Visible = True

                        CType(e.Item.FindControl("hlSerialized"), HyperLink).Attributes.Add("onclick", "return OpenRMAConfSerItem('" & lngReturnHeaderID & "','" & CCommon.ToShort(hfReturnType.Value) & "','" & DataBinder.Eval(e.Item.DataItem, "numReturnItemID") & "','" & e.Item.FindControl("txLot").ClientID & "' ,'" & e.Item.FindControl("lblUnitReceived").ClientID & "','" & e.Item.FindControl("hdnSelectedLotSerial").ClientID & "')")

                        If (sMode = 0 AndAlso CCommon.ToLong(hdnStatus.Value) = CCommon.ToLong(enmReturnStatus.Pending)) Or CCommon.ToLong(hdnStatus.Value) <> CCommon.ToLong(enmReturnStatus.Pending) Then
                            e.Item.FindControl("hlSerialized").Visible = False
                        Else
                            e.Item.FindControl("hlSerialized").Visible = True
                        End If
                    Else
                        e.Item.FindControl("hlSerialized").Visible = False
                        e.Item.FindControl("txLot").Visible = False
                    End If
                ElseIf e.Item.ItemType = ListItemType.Footer Then

                    If CCommon.ToShort(hfReturnType.Value) = 2 Then
                        DirectCast(e.Item.FindControl("btnSaveReceive"), Button).Text = "Ship qty"
                    End If

                    If sMode = 1 AndAlso CCommon.ToShort(hfReceiveType.Value) = 0 Then
                        DirectCast(e.Item.FindControl("btnSaveReceive"), Button).Visible = True
                        litNote.Visible = True
                    Else
                        DirectCast(e.Item.FindControl("btnSaveReceive"), Button).Visible = False
                        litNote.Visible = False
                    End If

                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

#End Region

        Sub SaveJournalEntries()
            Try
                objReturn = New ReturnHeader
                Dim dtDetails As New DataTable
                objReturn.ReturnHeaderID = lngReturnHeaderID
                objReturn.DomainID = Session("DomainID")

                dtDetails = objReturn.GetReturnHeader()

                If dtDetails IsNot Nothing AndAlso dtDetails.Rows.Count > 0 Then
                    Dim ReturnType As Short = CCommon.ToShort(dtDetails.Rows(0)("tintReturnType"))
                    Dim ReceiveType As Short = CCommon.ToShort(dtDetails.Rows(0)("tintReceiveType"))

                    Dim lngJournalID As Long
                    Dim GrandTotal As Decimal

                    GrandTotal = CCommon.ToDecimal(dtDetails.Rows(0)("monBizDocAmount"))
                    lngJournalID = SaveDataToHeader(GrandTotal)
                    Dim lngDivisionID As Long = dtDetails.Rows(0)("numDivisionId")

                    If ReturnType = 1 And ReceiveType = 1 Then 'Sales Refund
                        SaveJournalSalesRefundReceipt(lngJournalID, GrandTotal)
                        SaveCheckHeader(lngDivisionID, GrandTotal)

                    ElseIf ReturnType = 1 And ReceiveType = 2 Then 'Sales Credit Memo
                        SaveJournalSalesCreditMemo(lngJournalID, GrandTotal)
                        SaveDeposite(lngDivisionID, GrandTotal)

                    ElseIf ReturnType = 2 And ReceiveType = 2 Then 'Purchase Credit Memo
                        SaveJournalPurchaseCreditMemo(dtDetails, lngJournalID, GrandTotal)
                        SaveBillPayment(lngDivisionID, GrandTotal)

                    ElseIf ReturnType = 3 Then 'Credit Memo
                        SaveJournalCreditMemo(dtDetails, lngJournalID, GrandTotal, ReturnType)
                        If GrandTotal < 0 Then
                            SaveBillPayment(lngDivisionID, Math.Abs(GrandTotal))
                        Else
                            SaveDeposite(lngDivisionID, Math.Abs(GrandTotal))
                        End If


                    ElseIf ReturnType = 4 Then 'Refund
                        If CCommon.ToLong(dtDetails.Rows(0)("numDepositIDRef")) > 0 Then
                            SaveJournalRefundCustomerReceipt(dtDetails, lngJournalID, GrandTotal)
                            SaveCheckHeader(lngDivisionID, GrandTotal)
                        ElseIf CCommon.ToLong(dtDetails.Rows(0)("numBillPaymentIDRef")) > 0 Then
                            SaveJournalRefundVendorReceipt(dtDetails, lngJournalID, GrandTotal)
                        End If

                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Function SaveBillPayment(ByVal lngDivisionID As Long, ByVal GrandTotal As Decimal) As Long
            Try
                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")
                objCommon.UserCntID = Session("UserContactID")
                objCommon.DivisionID = lngDivisionID

                Dim objBillPayment As BillPayment = New BillPayment

                With objBillPayment
                    .PaymentDate = CDate(Date.UtcNow.Date & " 12:00:00")
                    .AccountID = 0
                    .PaymentMethod = 0
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")

                    .strXml = ""
                    .BillPaymentID = 0
                    .Mode = 1
                    .PaymentAmount = GrandTotal
                    .DivisionID = lngDivisionID
                    .ReturnHeaderID = lngReturnHeaderID
                    .AccountClass = objCommon.GetAccountingClass()
                    .ManageBillPayment() 'Save Bill Payment Entry
                End With

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Function SaveDeposite(ByVal lngDivisionID As Long, ByVal GrandTotal As Decimal) As Long
            Try
                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")
                objCommon.UserCntID = Session("UserContactID")
                objCommon.DivisionID = lngDivisionID

                Dim objMakeDeposit As New MakeDeposit
                With objMakeDeposit
                    .DivisionId = lngDivisionID
                    .Entry_Date = CDate(Date.UtcNow.Date & " 12:00:00")
                    .Reference = "Credit Memo"
                    .Memo = ""
                    .PaymentMethod = 0
                    .DepositeToType = 0
                    .numAmount = GrandTotal
                    .RecurringId = 0
                    .DepositId = 0
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainId")

                    Dim ds As New DataSet
                    .StrItems = ""
                    .Mode = 3
                    .DepositePage = 3
                    .ReturnHeaderID = lngReturnHeaderID
                    .AccountClass = objCommon.GetAccountingClass()
                    Dim lngDepositeID As Long = .SaveDataToMakeDepositDetails()

                    Return lngDepositeID
                End With
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Sub SaveCheckHeader(ByVal lngDivisionID As Long, ByVal GrandTotal As Decimal)
            Try
                Dim objChecks As New Checks()

                objChecks.DomainID = Session("DomainID")
                objChecks.AccountId = CCommon.ToLong(ddlRefundAccounts.SelectedValue.Split("~")(0))
                objChecks.CheckHeaderID = 0
                objChecks.DivisionId = lngDivisionID
                objChecks.UserCntID = Session("UserContactID")
                objChecks.Amount = GrandTotal
                objChecks.FromDate = CDate(Date.UtcNow.Date & " 12:00:00")
                objChecks.CheckNo = IIf(chkPrintCheck.Checked = False, 0, CCommon.ToLong(txtCheckNo.Text))
                objChecks.Memo = "Check against Refund"

                objChecks.ReferenceID = lngReturnHeaderID
                objChecks.Type = enmReferenceType.RMA
                objChecks.Mode = 2 'Insert/Edit
                objChecks.IsPrint = IIf(chkPrintCheck.Checked, False, True)

                objChecks.ManageCheckHeader()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Function SaveDataToHeader(ByVal GrandTotal As Decimal) As Long
            Try
                Dim lngJournalId As Long

                Dim objJEHeader As New JournalEntryHeader
                With objJEHeader
                    .JournalId = TransactionInfo1.JournalID
                    .RecurringId = 0
                    .EntryDate = Date.UtcNow.ToShortDateString & " 12:00:00"
                    .Description = ""
                    .Amount = GrandTotal
                    .CheckId = 0
                    .CashCreditCardId = 0
                    .ChartAcntId = 0
                    .OppId = 0
                    .OppBizDocsId = 0
                    .DepositId = 0
                    .BizDocsPaymentDetId = 0
                    .IsOpeningBalance = 0
                    .LastRecurringDate = Date.Now
                    .NoTransactions = 0
                    .CategoryHDRID = 0
                    .ReturnID = 0
                    .CheckHeaderID = 0
                    .BillID = 0
                    .BillPaymentID = 0
                    .ReturnID = lngReturnHeaderID
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")
                End With
                lngJournalId = objJEHeader.Save()
                Return lngJournalId
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveJournalSalesCreditMemo(ByVal lngJournalId As Long, ByVal GrandTotal As Decimal) As Boolean
            Try
                objReturn = New ReturnHeader
                Dim dtDetails As New DataTable
                objReturn.ReturnHeaderID = lngReturnHeaderID
                objReturn.DomainID = Session("DomainID")

                Dim dtReturnItems As DataTable = objReturn.GetReturnItemForJournal()

                If Not dtReturnItems Is Nothing AndAlso dtReturnItems.Rows.Count > 0 Then
                    Dim objJEList As New JournalEntryCollection
                    Dim objJE As New JournalEntryNew()

                    Dim lngItemIncomeAccount, lngItemAssetAccount, lngItemCOGsAccount, lngARAccountId As Long
                    Dim lngDivisionID As Long = dtReturnItems.Rows(0)("numDivisionId")
                    Dim decACProductCost, decTaxAmount As Decimal
                    Dim decAmount, decDebitAmt, decCreditAmt As Decimal
                    For i As Integer = 0 To dtReturnItems.Rows.Count - 1

                        If CCommon.ToLong(dtReturnItems.Rows(i).Item("ItemIncomeAccount")) > 0 Then
                            lngItemIncomeAccount = CCommon.ToLong(dtReturnItems.Rows(i).Item("ItemIncomeAccount"))
                        Else
                            lngItemIncomeAccount = ChartOfAccounting.GetDefaultAccount("BE", Session("DomainID"))
                        End If
                        If CCommon.ToLong(dtReturnItems.Rows(i).Item("ItemInventoryAsset")) > 0 Then
                            lngItemAssetAccount = CCommon.ToLong(dtReturnItems.Rows(i).Item("ItemInventoryAsset"))
                        End If
                        If CCommon.ToLong(dtReturnItems.Rows(i).Item("ItemCoGs")) > 0 Then
                            lngItemCOGsAccount = CCommon.ToLong(dtReturnItems.Rows(i).Item("ItemCoGs"))
                        Else
                            lngItemCOGsAccount = ChartOfAccounting.GetDefaultAccount("CG", Session("DomainID"))
                        End If

                        objJE = New JournalEntryNew()

                        decAmount = CDec(dtReturnItems.Rows(i).Item("numUnitHourReceived")) * CDec(dtReturnItems.Rows(i).Item("monUnitSalePrice"))
                        If decAmount > 0.0 Then
                            decDebitAmt = Math.Abs(decAmount)
                            decCreditAmt = 0
                        ElseIf decAmount < 0.0 Then
                            decDebitAmt = 0
                            decCreditAmt = Math.Abs(decAmount)
                        End If

                        If decAmount <> 0 Then
                            objJE.TransactionId = 0
                            objJE.DebitAmt = decDebitAmt
                            objJE.CreditAmt = decCreditAmt
                            objJE.ChartAcntId = lngItemIncomeAccount
                            objJE.Description = "Sales Return"
                            objJE.CustomerId = lngDivisionID
                            objJE.MainDeposit = False
                            objJE.MainCheck = False
                            objJE.MainCashCredit = False
                            objJE.OppitemtCode = CLng(dtReturnItems.Rows(i).Item("numReturnItemID"))
                            objJE.BizDocItems = "NI"
                            objJE.Reference = ""
                            objJE.PaymentMethod = 0
                            objJE.Reconcile = False
                            objJE.CurrencyID = 0
                            objJE.FltExchangeRate = 1
                            objJE.TaxItemID = 0
                            objJE.BizDocsPaymentDetailsId = 0
                            objJE.ContactID = 0
                            objJE.ItemID = CCommon.ToLong(dtReturnItems.Rows(i).Item("numItemCode"))
                            objJE.ProjectID = 0
                            objJE.ClassID = 0
                            objJE.CommissionID = 0
                            objJE.ReconcileID = 0
                            objJE.Cleared = False
                            objJE.ReferenceType = 0
                            objJE.ReferenceID = 0

                            objJEList.Add(objJE)
                        End If

                        If CStr(dtReturnItems.Rows(i).Item("charitemType")) = "P" AndAlso CBool(dtReturnItems.Rows(i).Item("bitKitParent")) = False Then
                            decACProductCost = CDec(dtReturnItems.Rows(i).Item("numUnitHourReceived")) * CDec(dtReturnItems.Rows(i).Item("AverageCost"))
                            If decACProductCost > 0 Then
                                objJE = New JournalEntryNew()

                                objJE.TransactionId = 0
                                objJE.DebitAmt = decACProductCost
                                objJE.CreditAmt = 0
                                objJE.ChartAcntId = lngItemAssetAccount
                                If CCommon.ToString(dtReturnItems.Rows(i).Item("tintChildLevel")) = 0 Then
                                    objJE.Description = "Item Returned"
                                Else
                                    objJE.Description = "Item Returned - Kit/Assembly"
                                End If
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dtReturnItems.Rows(i).Item("numReturnItemID"))
                                objJE.BizDocItems = "IA"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = 0
                                objJE.FltExchangeRate = 1
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dtReturnItems.Rows(i).Item("numItemCode"))
                                objJE.ProjectID = 0
                                objJE.ClassID = 0
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)

                                objJE = New JournalEntryNew()

                                objJE.TransactionId = 0
                                objJE.DebitAmt = 0
                                objJE.CreditAmt = decACProductCost
                                objJE.ChartAcntId = lngItemCOGsAccount
                                If CCommon.ToString(dtReturnItems.Rows(i).Item("tintChildLevel")) = 0 Then
                                    objJE.Description = "Cost of Goods Return"
                                Else
                                    objJE.Description = "Cost of Goods Return - Kit/Assembly"
                                End If
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dtReturnItems.Rows(i).Item("numReturnItemID"))
                                objJE.BizDocItems = "COG"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = 0
                                objJE.FltExchangeRate = 1
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dtReturnItems.Rows(i).Item("numItemCode"))
                                objJE.ProjectID = 0
                                objJE.ClassID = 0
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)
                            End If
                        End If
                    Next

                    Dim objTaxDetails As New TaxDetails
                    objTaxDetails.DomainID = Session("DomainID")
                    objTaxDetails.OppID = lngReturnHeaderID
                    objTaxDetails.DivisionID = lngDivisionID
                    objTaxDetails.OppBizDocID = 0
                    objTaxDetails.TaxItemID = 0
                    objTaxDetails.mode = 7

                    decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                    If decTaxAmount > 0 Then
                        objJE = New JournalEntryNew()

                        objJE.TransactionId = 0
                        objJE.DebitAmt = decTaxAmount
                        objJE.CreditAmt = 0
                        objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("ST", Session("DomainID"))
                        objJE.Description = "Sales" '"Authoritative Accounts"
                        objJE.CustomerId = lngDivisionID
                        objJE.MainDeposit = False
                        objJE.MainCheck = False
                        objJE.MainCashCredit = False
                        objJE.OppitemtCode = 0
                        objJE.BizDocItems = "ST"
                        objJE.Reference = ""
                        objJE.PaymentMethod = 0
                        objJE.Reconcile = False
                        objJE.CurrencyID = 0
                        objJE.FltExchangeRate = 1
                        objJE.TaxItemID = 0
                        objJE.BizDocsPaymentDetailsId = 0
                        objJE.ContactID = 0
                        objJE.ItemID = 0
                        objJE.ProjectID = 0
                        objJE.ClassID = 0
                        objJE.CommissionID = 0
                        objJE.ReconcileID = 0
                        objJE.Cleared = False
                        objJE.ReferenceType = 0
                        objJE.ReferenceID = 0

                        objJEList.Add(objJE)
                    End If

                    'CRV TAX
                    objTaxDetails.TaxItemID = 1

                    decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                    If decTaxAmount > 0 Then
                        objJE = New JournalEntryNew()

                        objJE.TransactionId = 0
                        objJE.DebitAmt = decTaxAmount
                        objJE.CreditAmt = 0
                        objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("ST", Session("DomainID"))
                        objJE.Description = "Sales" '"Authoritative Accounts"
                        objJE.CustomerId = lngDivisionID
                        objJE.MainDeposit = False
                        objJE.MainCheck = False
                        objJE.MainCashCredit = False
                        objJE.OppitemtCode = 0
                        objJE.BizDocItems = "ST"
                        objJE.Reference = ""
                        objJE.PaymentMethod = 0
                        objJE.Reconcile = False
                        objJE.CurrencyID = 0
                        objJE.FltExchangeRate = 1
                        objJE.TaxItemID = 0
                        objJE.BizDocsPaymentDetailsId = 0
                        objJE.ContactID = 0
                        objJE.ItemID = 0
                        objJE.ProjectID = 0
                        objJE.ClassID = 0
                        objJE.CommissionID = 0
                        objJE.ReconcileID = 0
                        objJE.Cleared = False
                        objJE.ReferenceType = 0
                        objJE.ReferenceID = 0

                        objJEList.Add(objJE)
                    End If

                    Dim dtSalesTax As DataTable
                    Dim k As Integer
                    dtSalesTax = objTaxDetails.GetTaxItems()
                    For k = 0 To dtSalesTax.Rows.Count - 1
                        objTaxDetails.TaxItemID = CLng(dtSalesTax.Rows(k).Item("numTaxItemID"))
                        decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                        If decTaxAmount > 0 Then
                            objJE = New JournalEntryNew()

                            objJE.TransactionId = 0
                            objJE.DebitAmt = decTaxAmount
                            objJE.CreditAmt = 0
                            objJE.ChartAcntId = CLng(dtSalesTax.Rows(k).Item("numChartOfAcntID"))
                            objJE.Description = "Sales" ' "Authoritative Accounts"
                            objJE.CustomerId = lngDivisionID
                            objJE.MainDeposit = False
                            objJE.MainCheck = False
                            objJE.MainCashCredit = False
                            objJE.OppitemtCode = 0
                            objJE.BizDocItems = "OT"
                            objJE.Reference = ""
                            objJE.PaymentMethod = 0
                            objJE.Reconcile = False
                            objJE.CurrencyID = 0
                            objJE.FltExchangeRate = 1
                            objJE.TaxItemID = CLng(dtSalesTax.Rows(k).Item("numTaxItemID"))
                            objJE.BizDocsPaymentDetailsId = 0
                            objJE.ContactID = 0
                            objJE.ItemID = 0
                            objJE.ProjectID = 0
                            objJE.ClassID = 0
                            objJE.CommissionID = 0
                            objJE.ReconcileID = 0
                            objJE.Cleared = False
                            objJE.ReferenceType = 0
                            objJE.ReferenceID = 0

                            objJEList.Add(objJE)
                        End If

                    Next


                    Dim decDiscAmt As Decimal = dtReturnItems.Rows(0).Item("monTotalDiscount")

                    If decDiscAmt <> 0 Then
                        objJE = New JournalEntryNew()

                        objJE.TransactionId = 0
                        objJE.DebitAmt = 0
                        objJE.CreditAmt = decDiscAmt
                        objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("DG", Session("DomainID"))
                        objJE.Description = "Sales Return"
                        objJE.CustomerId = lngDivisionID
                        objJE.MainDeposit = False
                        objJE.MainCheck = False
                        objJE.MainCashCredit = False
                        objJE.OppitemtCode = 0
                        objJE.BizDocItems = "DG"
                        objJE.Reference = ""
                        objJE.PaymentMethod = 0
                        objJE.Reconcile = False
                        objJE.CurrencyID = 0
                        objJE.FltExchangeRate = 1
                        objJE.TaxItemID = 0
                        objJE.BizDocsPaymentDetailsId = 0
                        objJE.ContactID = 0
                        objJE.ItemID = 0
                        objJE.ProjectID = 0
                        objJE.ClassID = 0
                        objJE.CommissionID = 0
                        objJE.ReconcileID = 0
                        objJE.Cleared = False
                        objJE.ReferenceType = 0
                        objJE.ReferenceID = 0

                        objJEList.Add(objJE)
                    End If

                    Dim objOppBizDocs As New OppBizDocs
                    objOppBizDocs.OppBizDocId = CCommon.ToLong(hdnOppBizDocID.Value)
                    lngARAccountId = CCommon.ToLong(objOppBizDocs.GetBizDocARAccountID())

                    If lngARAccountId = 0 Then
                        lngARAccountId = objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), lngDivisionID)
                    End If

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = 0
                    objJE.CreditAmt = GrandTotal '+ decDiscAmt - decTotalTaxAmount
                    objJE.ChartAcntId = lngARAccountId
                    objJE.Description = "Sales Return"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "AR"
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = 0
                    objJE.FltExchangeRate = 1
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)

                    objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, Session("DomainID"))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveJournalPurchaseCreditMemo(ByVal dtReturnItems As DataTable, ByVal lngJournalId As Long, ByVal GrandTotal As Decimal) As Boolean
            Try
                Dim objJEList As New JournalEntryCollection
                Dim objJE As New JournalEntryNew()

                Dim lngItemIncomeAccount, lngItemAssetAccount, lngItemCOGsAccount, lngARAccountId As Long
                Dim lngDivisionID As Long = dtReturnItems.Rows(0)("numDivisionId")
                Dim decAmount, decDebitAmt, decCreditAmt As Decimal

                For i As Integer = 0 To dtReturnItems.Rows.Count - 1

                    If CCommon.ToLong(dtReturnItems.Rows(i).Item("ItemIncomeAccount")) > 0 Then
                        lngItemIncomeAccount = CCommon.ToLong(dtReturnItems.Rows(i).Item("ItemIncomeAccount"))
                    Else
                        lngItemIncomeAccount = ChartOfAccounting.GetDefaultAccount("BE", Session("DomainID"))
                    End If
                    If CCommon.ToLong(dtReturnItems.Rows(i).Item("ItemInventoryAsset")) > 0 Then
                        lngItemAssetAccount = CCommon.ToLong(dtReturnItems.Rows(i).Item("ItemInventoryAsset"))
                    End If
                    If CCommon.ToLong(dtReturnItems.Rows(i).Item("ItemCoGs")) > 0 Then
                        lngItemCOGsAccount = CCommon.ToLong(dtReturnItems.Rows(i).Item("ItemCoGs"))
                    Else
                        lngItemCOGsAccount = ChartOfAccounting.GetDefaultAccount("CG", Session("DomainID"))
                    End If

                    decAmount = CDec(dtReturnItems.Rows(i).Item("numUnitHourReceived")) * CDec(dtReturnItems.Rows(i).Item("monUnitSalePrice"))
                    decDebitAmt = decCreditAmt = 0

                    If decAmount > 0.0 Then
                        decDebitAmt = 0
                        decCreditAmt = Math.Abs(decAmount)
                    ElseIf decAmount < 0.0 Then
                        decDebitAmt = Math.Abs(decAmount)
                        decCreditAmt = 0
                    End If

                    If decAmount <> 0 Then
                        If CStr(dtReturnItems.Rows(i).Item("charitemType")) = "P" Then
                            objJE = New JournalEntryNew()

                            objJE.TransactionId = 0
                            objJE.DebitAmt = decDebitAmt
                            objJE.CreditAmt = decCreditAmt
                            objJE.ChartAcntId = lngItemAssetAccount
                            objJE.Description = "Return"
                            objJE.CustomerId = lngDivisionID
                            objJE.MainDeposit = False
                            objJE.MainCheck = False
                            objJE.MainCashCredit = False
                            objJE.OppitemtCode = CLng(dtReturnItems.Rows(i).Item("numReturnItemID"))
                            objJE.BizDocItems = "NI"
                            objJE.Reference = ""
                            objJE.PaymentMethod = 0
                            objJE.Reconcile = False
                            objJE.CurrencyID = 0
                            objJE.FltExchangeRate = 1
                            objJE.TaxItemID = 0
                            objJE.BizDocsPaymentDetailsId = 0
                            objJE.ContactID = 0
                            objJE.ItemID = CCommon.ToLong(dtReturnItems.Rows(i).Item("numItemCode"))
                            objJE.ProjectID = 0
                            objJE.ClassID = 0
                            objJE.CommissionID = 0
                            objJE.ReconcileID = 0
                            objJE.Cleared = False
                            objJE.ReferenceType = 0
                            objJE.ReferenceID = 0

                            objJEList.Add(objJE)
                        Else
                            objJE = New JournalEntryNew()

                            objJE.TransactionId = 0
                            objJE.DebitAmt = decDebitAmt
                            objJE.CreditAmt = decCreditAmt
                            objJE.ChartAcntId = lngItemCOGsAccount
                            objJE.Description = "Cost of Goods Return"
                            objJE.CustomerId = lngDivisionID
                            objJE.MainDeposit = False
                            objJE.MainCheck = False
                            objJE.MainCashCredit = False
                            objJE.OppitemtCode = CLng(dtReturnItems.Rows(i).Item("numReturnItemID"))
                            objJE.BizDocItems = "NI"
                            objJE.Reference = ""
                            objJE.PaymentMethod = 0
                            objJE.Reconcile = False
                            objJE.CurrencyID = 0
                            objJE.FltExchangeRate = 1
                            objJE.TaxItemID = 0
                            objJE.BizDocsPaymentDetailsId = 0
                            objJE.ContactID = 0
                            objJE.ItemID = CCommon.ToLong(dtReturnItems.Rows(i).Item("numItemCode"))
                            objJE.ProjectID = 0
                            objJE.ClassID = 0
                            objJE.CommissionID = 0
                            objJE.ReconcileID = 0
                            objJE.Cleared = False
                            objJE.ReferenceType = 0
                            objJE.ReferenceID = 0

                            objJEList.Add(objJE)
                        End If
                    End If
                Next

                Dim dtTable As DataTable
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = Session("DomainID")
                dtTable = objUserAccess.GetDomainDetails()
                Dim decTaxAmount As Decimal = 0
                Dim decTotalTaxAmount As Decimal = 0

                If dtTable.Rows.Count > 0 AndAlso CCommon.ToBool(dtTable.Rows(0).Item("bitPurchaseTaxCredit")) Then
                    Dim lngPurchaseTaxCredit As Long = ChartOfAccounting.GetDefaultAccount("PT", Session("DomainID"))

                    If lngPurchaseTaxCredit > 0 Then
                        Dim objTaxDetails As New TaxDetails
                        objTaxDetails.DomainID = Session("DomainID")
                        objTaxDetails.OppID = lngReturnHeaderID
                        objTaxDetails.DivisionID = lngDivisionID
                        objTaxDetails.OppBizDocID = 0
                        objTaxDetails.TaxItemID = 0
                        objTaxDetails.mode = 7

                        decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                        If Math.Abs(decTaxAmount) > 0 Then
                            objJE = New JournalEntryNew()
                            decDebitAmt = 0
                            decCreditAmt = 0
                            If decTaxAmount > 0 Then
                                decDebitAmt = 0
                                decCreditAmt = CDec(Math.Abs(decTaxAmount) * 1)
                            ElseIf decTaxAmount < 0 Then
                                decDebitAmt = CDec(Math.Abs(decTaxAmount) * 1)
                                decCreditAmt = 0
                            End If

                            decTotalTaxAmount += Math.Abs(decTaxAmount)

                            objJE.TransactionId = 0
                            objJE.DebitAmt = decDebitAmt
                            objJE.CreditAmt = decCreditAmt
                            objJE.ChartAcntId = lngPurchaseTaxCredit  'ChartOfAccounting.GetDefaultAccount("PT", lngDomainID)
                            objJE.Description = "Purchase" '"Authoritative Accounts"
                            objJE.CustomerId = lngDivisionID
                            objJE.MainDeposit = False
                            objJE.MainCheck = False
                            objJE.MainCashCredit = False
                            objJE.OppitemtCode = 0
                            objJE.BizDocItems = "PT"
                            objJE.Reference = ""
                            objJE.PaymentMethod = 0
                            objJE.Reconcile = False
                            objJE.CurrencyID = 0
                            objJE.FltExchangeRate = 1
                            objJE.TaxItemID = 0
                            objJE.BizDocsPaymentDetailsId = 0
                            objJE.ContactID = 0
                            objJE.ItemID = 0
                            objJE.ProjectID = 0
                            objJE.ClassID = 0
                            objJE.CommissionID = 0
                            objJE.ReconcileID = 0
                            objJE.Cleared = False
                            objJE.ReferenceType = 0
                            objJE.ReferenceID = 0

                            objJEList.Add(objJE)
                        End If

                        'CRV TAX
                        objTaxDetails.TaxItemID = 1
                        decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                        If Math.Abs(decTaxAmount) > 0 Then
                            objJE = New JournalEntryNew()
                            decDebitAmt = 0
                            decCreditAmt = 0
                            If decTaxAmount > 0 Then
                                decDebitAmt = 0
                                decCreditAmt = CDec(Math.Abs(decTaxAmount) * 1)
                            ElseIf decTaxAmount < 0 Then
                                decDebitAmt = CDec(Math.Abs(decTaxAmount) * 1)
                                decCreditAmt = 0
                            End If

                            decTotalTaxAmount += Math.Abs(decTaxAmount)

                            objJE.TransactionId = 0
                            objJE.DebitAmt = decDebitAmt
                            objJE.CreditAmt = decCreditAmt
                            objJE.ChartAcntId = lngPurchaseTaxCredit  'ChartOfAccounting.GetDefaultAccount("PT", lngDomainID)
                            objJE.Description = "Purchase" '"Authoritative Accounts"
                            objJE.CustomerId = lngDivisionID
                            objJE.MainDeposit = False
                            objJE.MainCheck = False
                            objJE.MainCashCredit = False
                            objJE.OppitemtCode = 0
                            objJE.BizDocItems = "PT"
                            objJE.Reference = ""
                            objJE.PaymentMethod = 0
                            objJE.Reconcile = False
                            objJE.CurrencyID = 0
                            objJE.FltExchangeRate = 1
                            objJE.TaxItemID = 0
                            objJE.BizDocsPaymentDetailsId = 0
                            objJE.ContactID = 0
                            objJE.ItemID = 0
                            objJE.ProjectID = 0
                            objJE.ClassID = 0
                            objJE.CommissionID = 0
                            objJE.ReconcileID = 0
                            objJE.Cleared = False
                            objJE.ReferenceType = 0
                            objJE.ReferenceID = 0

                            objJEList.Add(objJE)
                        End If

                        ''End If

                        Dim dtPurchaseTax As DataTable
                        Dim k As Integer
                        dtPurchaseTax = objTaxDetails.GetTaxItems()
                        For k = 0 To dtPurchaseTax.Rows.Count - 1
                            objTaxDetails.TaxItemID = CLng(dtPurchaseTax.Rows(k).Item("numTaxItemID"))
                            decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                            If Math.Abs(decTaxAmount) > 0 Then
                                objJE = New JournalEntryNew()
                                decDebitAmt = 0
                                decCreditAmt = 0
                                If decTaxAmount > 0 Then
                                    decDebitAmt = 0
                                    decCreditAmt = CDec(Math.Abs(decTaxAmount) * 1)
                                ElseIf decTaxAmount < 0 Then
                                    decDebitAmt = CDec(Math.Abs(decTaxAmount) * 1)
                                    decCreditAmt = 0
                                End If

                                decTotalTaxAmount += Math.Abs(decTaxAmount)

                                objJE.TransactionId = 0
                                objJE.DebitAmt = decDebitAmt
                                objJE.CreditAmt = decCreditAmt
                                objJE.ChartAcntId = CLng(dtPurchaseTax.Rows(k).Item("numChartOfAcntID"))
                                objJE.Description = "Purchase" ' "Authoritative Accounts"
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = 0
                                objJE.BizDocItems = "OT"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = 0
                                objJE.FltExchangeRate = 1
                                objJE.TaxItemID = CLng(dtPurchaseTax.Rows(k).Item("numTaxItemID"))
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = 0
                                objJE.ProjectID = 0
                                objJE.ClassID = 0
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)
                            End If
                        Next
                    End If
                End If

                Dim decDiscAmt As Decimal = dtReturnItems.Rows(0).Item("monTotalDiscount")

                If decDiscAmt <> 0 Then
                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = decDiscAmt
                    objJE.CreditAmt = 0
                    objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("DG", Session("DomainID"))
                    objJE.Description = "Return"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "DG"
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = 0
                    objJE.FltExchangeRate = 1
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)
                End If

                Dim objOppBizDocs As New OppBizDocs
                lngARAccountId = objOppBizDocs.ValidateCustomerAR_APAccounts("AP", Session("DomainID"), lngDivisionID)

                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = GrandTotal '+ decDiscAmt
                objJE.CreditAmt = 0
                objJE.ChartAcntId = lngARAccountId
                objJE.Description = "Return"
                objJE.CustomerId = lngDivisionID
                objJE.MainDeposit = False
                objJE.MainCheck = False
                objJE.MainCashCredit = False
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "AP"
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 1
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = False
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)


                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, Session("DomainID"))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveJournalSalesRefundReceipt(ByVal lngJournalId As Long, ByVal GrandTotal As Decimal) As Boolean
            Try
                objReturn = New ReturnHeader
                Dim dtDetails As New DataTable
                objReturn.ReturnHeaderID = lngReturnHeaderID
                objReturn.DomainID = Session("DomainID")

                Dim dtReturnItems As DataTable = objReturn.GetReturnItemForJournal()

                If Not dtReturnItems Is Nothing AndAlso dtReturnItems.Rows.Count > 0 Then
                    Dim objJEList As New JournalEntryCollection
                    Dim objJE As New JournalEntryNew()

                    Dim lngItemIncomeAccount, lngItemAssetAccount, lngItemCOGsAccount, lngARAccountId As Long
                    Dim lngDivisionID As Long = dtReturnItems.Rows(0)("numDivisionId")
                    Dim decACProductCost, decTaxAmount As Decimal
                    Dim decAmount, decDebitAmt, decCreditAmt As Decimal
                    For i As Integer = 0 To dtReturnItems.Rows.Count - 1

                        If CCommon.ToLong(dtReturnItems.Rows(i).Item("ItemIncomeAccount")) > 0 Then
                            lngItemIncomeAccount = CCommon.ToLong(dtReturnItems.Rows(i).Item("ItemIncomeAccount"))
                        Else
                            lngItemIncomeAccount = ChartOfAccounting.GetDefaultAccount("BE", Session("DomainID"))
                        End If
                        If CCommon.ToLong(dtReturnItems.Rows(i).Item("ItemInventoryAsset")) > 0 Then
                            lngItemAssetAccount = CCommon.ToLong(dtReturnItems.Rows(i).Item("ItemInventoryAsset"))
                        End If
                        If CCommon.ToLong(dtReturnItems.Rows(i).Item("ItemCoGs")) > 0 Then
                            lngItemCOGsAccount = CCommon.ToLong(dtReturnItems.Rows(i).Item("ItemCoGs"))
                        Else
                            lngItemCOGsAccount = ChartOfAccounting.GetDefaultAccount("CG", Session("DomainID"))
                        End If

                        objJE = New JournalEntryNew()

                        decAmount = CDec(dtReturnItems.Rows(i).Item("numUnitHourReceived")) * CDec(dtReturnItems.Rows(i).Item("monUnitSalePrice"))
                        If decAmount > 0.0 Then
                            decDebitAmt = Math.Abs(decAmount)
                            decCreditAmt = 0
                        ElseIf decAmount < 0.0 Then
                            decDebitAmt = 0
                            decCreditAmt = Math.Abs(decAmount)
                        End If
                        If decAmount <> 0 Then
                            objJE.TransactionId = 0
                            objJE.DebitAmt = decDebitAmt
                            objJE.CreditAmt = decCreditAmt
                            objJE.ChartAcntId = lngItemIncomeAccount
                            objJE.Description = "Sales Return"
                            objJE.CustomerId = lngDivisionID
                            objJE.MainDeposit = False
                            objJE.MainCheck = False
                            objJE.MainCashCredit = False
                            objJE.OppitemtCode = CLng(dtReturnItems.Rows(i).Item("numReturnItemID"))
                            objJE.BizDocItems = "NI"
                            objJE.Reference = ""
                            objJE.PaymentMethod = 0
                            objJE.Reconcile = False
                            objJE.CurrencyID = 0
                            objJE.FltExchangeRate = 1
                            objJE.TaxItemID = 0
                            objJE.BizDocsPaymentDetailsId = 0
                            objJE.ContactID = 0
                            objJE.ItemID = CCommon.ToLong(dtReturnItems.Rows(i).Item("numItemCode"))
                            objJE.ProjectID = 0
                            objJE.ClassID = 0
                            objJE.CommissionID = 0
                            objJE.ReconcileID = 0
                            objJE.Cleared = False
                            objJE.ReferenceType = 0
                            objJE.ReferenceID = 0

                            objJEList.Add(objJE)
                        End If

                        If CStr(dtReturnItems.Rows(i).Item("charitemType")) = "P" AndAlso CBool(dtReturnItems.Rows(i).Item("bitKitParent")) = False Then
                            decACProductCost = CDec(dtReturnItems.Rows(i).Item("numUnitHourReceived")) * CDec(dtReturnItems.Rows(i).Item("AverageCost"))
                            If decACProductCost > 0 Then
                                objJE = New JournalEntryNew()

                                objJE.TransactionId = 0
                                objJE.DebitAmt = decACProductCost
                                objJE.CreditAmt = 0
                                objJE.ChartAcntId = lngItemAssetAccount
                                If CCommon.ToString(dtReturnItems.Rows(i).Item("tintChildLevel")) = 0 Then
                                    objJE.Description = "Item Returned"
                                Else
                                    objJE.Description = "Item Returned - Kit/Assembly"
                                End If
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dtReturnItems.Rows(i).Item("numReturnItemID"))
                                objJE.BizDocItems = "IA"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = 0
                                objJE.FltExchangeRate = 1
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dtReturnItems.Rows(i).Item("numItemCode"))
                                objJE.ProjectID = 0
                                objJE.ClassID = 0
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)


                                objJE = New JournalEntryNew()

                                objJE.TransactionId = 0
                                objJE.DebitAmt = 0
                                objJE.CreditAmt = decACProductCost
                                objJE.ChartAcntId = lngItemCOGsAccount
                                If CCommon.ToString(dtReturnItems.Rows(i).Item("tintChildLevel")) = 0 Then
                                    objJE.Description = "Cost of Goods Return"
                                Else
                                    objJE.Description = "Cost of Goods Return - Kit/Assembly"
                                End If
                                objJE.CustomerId = lngDivisionID
                                objJE.MainDeposit = False
                                objJE.MainCheck = False
                                objJE.MainCashCredit = False
                                objJE.OppitemtCode = CLng(dtReturnItems.Rows(i).Item("numReturnItemID"))
                                objJE.BizDocItems = "COG"
                                objJE.Reference = ""
                                objJE.PaymentMethod = 0
                                objJE.Reconcile = False
                                objJE.CurrencyID = 0
                                objJE.FltExchangeRate = 1
                                objJE.TaxItemID = 0
                                objJE.BizDocsPaymentDetailsId = 0
                                objJE.ContactID = 0
                                objJE.ItemID = CCommon.ToLong(dtReturnItems.Rows(i).Item("numItemCode"))
                                objJE.ProjectID = 0
                                objJE.ClassID = 0
                                objJE.CommissionID = 0
                                objJE.ReconcileID = 0
                                objJE.Cleared = False
                                objJE.ReferenceType = 0
                                objJE.ReferenceID = 0

                                objJEList.Add(objJE)
                            End If
                        End If
                    Next

                    Dim objTaxDetails As New TaxDetails
                    objTaxDetails.DomainID = Session("DomainID")
                    objTaxDetails.OppID = lngReturnHeaderID
                    objTaxDetails.DivisionID = lngDivisionID
                    objTaxDetails.OppBizDocID = 0
                    objTaxDetails.TaxItemID = 0
                    objTaxDetails.mode = 7

                    decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                    If decTaxAmount > 0 Then
                        objJE = New JournalEntryNew()

                        objJE.TransactionId = 0
                        objJE.DebitAmt = decTaxAmount
                        objJE.CreditAmt = 0
                        objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("ST", Session("DomainID"))
                        objJE.Description = "Sales" '"Authoritative Accounts"
                        objJE.CustomerId = lngDivisionID
                        objJE.MainDeposit = False
                        objJE.MainCheck = False
                        objJE.MainCashCredit = False
                        objJE.OppitemtCode = 0
                        objJE.BizDocItems = "ST"
                        objJE.Reference = ""
                        objJE.PaymentMethod = 0
                        objJE.Reconcile = False
                        objJE.CurrencyID = 0
                        objJE.FltExchangeRate = 1
                        objJE.TaxItemID = 0
                        objJE.BizDocsPaymentDetailsId = 0
                        objJE.ContactID = 0
                        objJE.ItemID = 0
                        objJE.ProjectID = 0
                        objJE.ClassID = 0
                        objJE.CommissionID = 0
                        objJE.ReconcileID = 0
                        objJE.Cleared = False
                        objJE.ReferenceType = 0
                        objJE.ReferenceID = 0

                        objJEList.Add(objJE)
                    End If

                    'CRV TAX
                    objTaxDetails.TaxItemID = 1

                    decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                    If decTaxAmount > 0 Then
                        objJE = New JournalEntryNew()

                        objJE.TransactionId = 0
                        objJE.DebitAmt = decTaxAmount
                        objJE.CreditAmt = 0
                        objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("ST", Session("DomainID"))
                        objJE.Description = "Sales" '"Authoritative Accounts"
                        objJE.CustomerId = lngDivisionID
                        objJE.MainDeposit = False
                        objJE.MainCheck = False
                        objJE.MainCashCredit = False
                        objJE.OppitemtCode = 0
                        objJE.BizDocItems = "ST"
                        objJE.Reference = ""
                        objJE.PaymentMethod = 0
                        objJE.Reconcile = False
                        objJE.CurrencyID = 0
                        objJE.FltExchangeRate = 1
                        objJE.TaxItemID = 0
                        objJE.BizDocsPaymentDetailsId = 0
                        objJE.ContactID = 0
                        objJE.ItemID = 0
                        objJE.ProjectID = 0
                        objJE.ClassID = 0
                        objJE.CommissionID = 0
                        objJE.ReconcileID = 0
                        objJE.Cleared = False
                        objJE.ReferenceType = 0
                        objJE.ReferenceID = 0

                        objJEList.Add(objJE)
                    End If

                    Dim dtSalesTax As DataTable
                    Dim k As Integer
                    dtSalesTax = objTaxDetails.GetTaxItems()
                    For k = 0 To dtSalesTax.Rows.Count - 1
                        objTaxDetails.TaxItemID = CLng(dtSalesTax.Rows(k).Item("numTaxItemID"))
                        decTaxAmount = objTaxDetails.GetTaxAmtOppForTaxItem1
                        If decTaxAmount > 0 Then
                            objJE = New JournalEntryNew()

                            objJE.TransactionId = 0
                            objJE.DebitAmt = decTaxAmount
                            objJE.CreditAmt = 0
                            objJE.ChartAcntId = CLng(dtSalesTax.Rows(k).Item("numChartOfAcntID"))
                            objJE.Description = "Sales" ' "Authoritative Accounts"
                            objJE.CustomerId = lngDivisionID
                            objJE.MainDeposit = False
                            objJE.MainCheck = False
                            objJE.MainCashCredit = False
                            objJE.OppitemtCode = 0
                            objJE.BizDocItems = "OT"
                            objJE.Reference = ""
                            objJE.PaymentMethod = 0
                            objJE.Reconcile = False
                            objJE.CurrencyID = 0
                            objJE.FltExchangeRate = 1
                            objJE.TaxItemID = CLng(dtSalesTax.Rows(k).Item("numTaxItemID"))
                            objJE.BizDocsPaymentDetailsId = 0
                            objJE.ContactID = 0
                            objJE.ItemID = 0
                            objJE.ProjectID = 0
                            objJE.ClassID = 0
                            objJE.CommissionID = 0
                            objJE.ReconcileID = 0
                            objJE.Cleared = False
                            objJE.ReferenceType = 0
                            objJE.ReferenceID = 0

                            objJEList.Add(objJE)
                        End If

                    Next


                    Dim decDiscAmt As Decimal = dtReturnItems.Rows(0).Item("monTotalDiscount")

                    If decDiscAmt <> 0 Then
                        objJE = New JournalEntryNew()

                        objJE.TransactionId = 0
                        objJE.DebitAmt = 0
                        objJE.CreditAmt = decDiscAmt
                        objJE.ChartAcntId = ChartOfAccounting.GetDefaultAccount("DG", Session("DomainID"))
                        objJE.Description = "Return"
                        objJE.CustomerId = lngDivisionID
                        objJE.MainDeposit = False
                        objJE.MainCheck = False
                        objJE.MainCashCredit = False
                        objJE.OppitemtCode = 0
                        objJE.BizDocItems = "DG"
                        objJE.Reference = ""
                        objJE.PaymentMethod = 0
                        objJE.Reconcile = False
                        objJE.CurrencyID = 0
                        objJE.FltExchangeRate = 1
                        objJE.TaxItemID = 0
                        objJE.BizDocsPaymentDetailsId = 0
                        objJE.ContactID = 0
                        objJE.ItemID = 0
                        objJE.ProjectID = 0
                        objJE.ClassID = 0
                        objJE.CommissionID = 0
                        objJE.ReconcileID = 0
                        objJE.Cleared = False
                        objJE.ReferenceType = 0
                        objJE.ReferenceID = 0

                        objJEList.Add(objJE)
                    End If

                    Dim objOppBizDocs As New OppBizDocs
                    objOppBizDocs.OppBizDocId = CCommon.ToLong(hdnOppBizDocID.Value)
                    lngARAccountId = CCommon.ToLong(objOppBizDocs.GetBizDocARAccountID())

                    If lngARAccountId = 0 Then
                        lngARAccountId = objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), lngDivisionID)
                    End If

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = 0
                    objJE.CreditAmt = GrandTotal '+ decDiscAmt - decTotalTaxAmount
                    objJE.ChartAcntId = lngARAccountId
                    objJE.Description = "Sales Return"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "AR"
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = 0
                    objJE.FltExchangeRate = 1
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = 0
                    objJE.CreditAmt = GrandTotal
                    objJE.ChartAcntId = CCommon.ToLong(ddlRefundAccounts.SelectedValue.Split("~")(0))
                    objJE.Description = "Sales Return"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "AP"
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = 0
                    objJE.FltExchangeRate = 1
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)

                    objJE = New JournalEntryNew()

                    objJE.TransactionId = 0
                    objJE.DebitAmt = GrandTotal '+ decDiscAmt
                    objJE.CreditAmt = 0
                    objJE.ChartAcntId = lngARAccountId
                    objJE.Description = "Sales Return"
                    objJE.CustomerId = lngDivisionID
                    objJE.MainDeposit = False
                    objJE.MainCheck = False
                    objJE.MainCashCredit = False
                    objJE.OppitemtCode = 0
                    objJE.BizDocItems = "AP"
                    objJE.Reference = ""
                    objJE.PaymentMethod = 0
                    objJE.Reconcile = False
                    objJE.CurrencyID = 0
                    objJE.FltExchangeRate = 1
                    objJE.TaxItemID = 0
                    objJE.BizDocsPaymentDetailsId = 0
                    objJE.ContactID = 0
                    objJE.ItemID = 0
                    objJE.ProjectID = 0
                    objJE.ClassID = 0
                    objJE.CommissionID = 0
                    objJE.ReconcileID = 0
                    objJE.Cleared = False
                    objJE.ReferenceType = 0
                    objJE.ReferenceID = 0

                    objJEList.Add(objJE)

                    objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, Session("DomainID"))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveJournalRefundCustomerReceipt(ByVal dtReturnItems As DataTable, ByVal lngJournalId As Long, ByVal GrandTotal As Decimal) As Boolean
            Try
                Dim objJEList As New JournalEntryCollection
                Dim objJE As New JournalEntryNew()


                Dim lngDivisionID As Long = dtReturnItems.Rows(0)("numDivisionId")
                Dim objOppBizDocs As New OppBizDocs
                objOppBizDocs.OppBizDocId = CCommon.ToLong(hdnOppBizDocID.Value)
                Dim lngARAccountId As Long = CCommon.ToLong(objOppBizDocs.GetBizDocARAccountID())

                If lngARAccountId = 0 Then
                    lngARAccountId = objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), lngDivisionID)
                End If

                Dim decDiscAmt As Decimal = dtReturnItems.Rows(0).Item("monTotalDiscount")

                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = 0
                objJE.CreditAmt = GrandTotal
                objJE.ChartAcntId = CCommon.ToLong(ddlRefundAccounts.SelectedValue.Split("~")(0))
                objJE.Description = "Return"
                objJE.CustomerId = lngDivisionID
                objJE.MainDeposit = False
                objJE.MainCheck = False
                objJE.MainCashCredit = False
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "AR"
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 1
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = False
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = GrandTotal '+ decDiscAmt
                objJE.CreditAmt = 0
                objJE.ChartAcntId = lngARAccountId
                objJE.Description = "Return"
                objJE.CustomerId = lngDivisionID
                objJE.MainDeposit = False
                objJE.MainCheck = False
                objJE.MainCashCredit = False
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "AR"
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 1
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = False
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, Session("DomainID"))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveJournalRefundVendorReceipt(ByVal dtReturnItems As DataTable, ByVal lngJournalId As Long, ByVal GrandTotal As Decimal) As Boolean
            Try
                Dim objJEList As New JournalEntryCollection
                Dim objJE As New JournalEntryNew()


                Dim lngDivisionID As Long = dtReturnItems.Rows(0)("numDivisionId")
                Dim objOppBizDocs As New OppBizDocs
                Dim lngAPAccountId As Long = objOppBizDocs.ValidateCustomerAR_APAccounts("AP", Session("DomainID"), lngDivisionID)

                Dim decDiscAmt As Decimal = dtReturnItems.Rows(0).Item("monTotalDiscount")

                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = GrandTotal
                objJE.CreditAmt = 0
                objJE.ChartAcntId = CCommon.ToLong(ddlRefundAccounts.SelectedValue.Split("~")(0))
                objJE.Description = "Return"
                objJE.CustomerId = lngDivisionID
                objJE.MainDeposit = False
                objJE.MainCheck = False
                objJE.MainCashCredit = False
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "AP"
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 1
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = False
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                objJE.DebitAmt = 0 '+ decDiscAmt
                objJE.CreditAmt = GrandTotal
                objJE.ChartAcntId = lngAPAccountId
                objJE.Description = "Return"
                objJE.CustomerId = lngDivisionID
                objJE.MainDeposit = False
                objJE.MainCheck = False
                objJE.MainCashCredit = False
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "AP"
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 1
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = False
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, Session("DomainID"))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Public Function SaveJournalCreditMemo(ByVal dtReturnItems As DataTable, ByVal lngJournalId As Long, ByVal GrandTotal As Decimal, Optional ByVal ReturnType As Short = 0) As Boolean
            Try
                Dim objJEList As New JournalEntryCollection
                Dim objJE As New JournalEntryNew()


                Dim lngDivisionID As Long = dtReturnItems.Rows(0)("numDivisionId")
                Dim objOppBizDocs As New OppBizDocs
                objOppBizDocs.OppBizDocId = CCommon.ToLong(hdnOppBizDocID.Value)
                Dim lngTempARAccountId As Long = CCommon.ToLong(objOppBizDocs.GetBizDocARAccountID())

                If lngTempARAccountId = 0 Then
                    lngTempARAccountId = objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), lngDivisionID)
                End If


                Dim lngARAccountId As Long = IIf(GrandTotal < 0, objOppBizDocs.ValidateCustomerAR_APAccounts("AP", Session("DomainID"), lngDivisionID), lngTempARAccountId)

                Dim decDiscAmt As Decimal = dtReturnItems.Rows(0).Item("monTotalDiscount")

                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                If ReturnType = 3 AndAlso GrandTotal < 0 Then
                    objJE.DebitAmt = 0
                    objJE.CreditAmt = Math.Abs(GrandTotal)
                    objJE.Description = "Standalone Credit"
                Else
                    objJE.DebitAmt = Math.Abs(GrandTotal)
                    objJE.CreditAmt = 0
                    objJE.Description = "Standalone Debit"
                End If

                objJE.ChartAcntId = lngAccountID 'CCommon.ToLong(ddlCreditAccounts.SelectedValue.Split("~")(0))
                'objJE.Description = "Return"
                objJE.CustomerId = lngDivisionID
                objJE.MainDeposit = False
                objJE.MainCheck = False
                objJE.MainCashCredit = False
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "AP"
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 1
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = False
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                objJE = New JournalEntryNew()

                objJE.TransactionId = 0
                If ReturnType = 3 AndAlso GrandTotal < 0 Then
                    objJE.DebitAmt = Math.Abs(GrandTotal) '+ decDiscAmt
                    objJE.CreditAmt = 0
                    objJE.Description = "Standalone Debit"
                Else
                    objJE.DebitAmt = 0
                    objJE.CreditAmt = Math.Abs(GrandTotal) '+ decDiscAmt
                    objJE.Description = "Standalone Credit"
                End If

                objJE.ChartAcntId = lngARAccountId
                'objJE.Description = "Return"
                objJE.CustomerId = lngDivisionID
                objJE.MainDeposit = False
                objJE.MainCheck = False
                objJE.MainCashCredit = False
                objJE.OppitemtCode = 0
                objJE.BizDocItems = "AP"
                objJE.Reference = ""
                objJE.PaymentMethod = 0
                objJE.Reconcile = False
                objJE.CurrencyID = 0
                objJE.FltExchangeRate = 1
                objJE.TaxItemID = 0
                objJE.BizDocsPaymentDetailsId = 0
                objJE.ContactID = 0
                objJE.ItemID = 0
                objJE.ProjectID = 0
                objJE.ClassID = 0
                objJE.CommissionID = 0
                objJE.ReconcileID = 0
                objJE.Cleared = False
                objJE.ReferenceType = 0
                objJE.ReferenceID = 0

                objJEList.Add(objJE)

                objJEList.Save(JournalEntryCollection.JournalMode.DeleteUpdateInsert, lngJournalId, Session("DomainID"))

            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub dgPaymentDetails_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPaymentDetails.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Header Then

                ElseIf e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then

                    If DataBinder.Eval(e.Item.DataItem, "vcCreditCardNo") IsNot Nothing Then
                        Dim strCardNo As String = CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "vcCreditCardNo"))
                        Dim intCardCharLength As Integer = 4

                        If Not String.IsNullOrEmpty(strCardNo) AndAlso strCardNo.Length > intCardCharLength Then
                            strCardNo = objCommon.Decrypt(strCardNo)
                            strCardNo = strCardNo.Substring(strCardNo.Length - intCardCharLength, intCardCharLength)

                            Dim lblCardNo As Label
                            lblCardNo = e.Item.FindControl("lblCardNo")
                            If lblCardNo IsNot Nothing Then
                                lblCardNo.Text = "XXXX-" & strCardNo
                                If Not String.IsNullOrEmpty(CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "vcSignatureFile"))) Then
                                    lblCardNo.Text = lblCardNo.Text & "<br/><a href=""" & CCommon.GetDocumentPath(DomainID) & CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "vcSignatureFile")) & """ target=""_blank"">Signature</a>"
                                End If
                            End If
                        End If
                    End If

                    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                    Dim hplBizDoc As New HyperLink
                    hplBizDoc = DirectCast(e.Item.FindControl("hplBizDoc"), HyperLink)
                    If hplBizDoc IsNot Nothing Then
                        'hplBizDoc.Attributes.Add("onclick", "OpenEdit(" & CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numOppID")) & "," & CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numOppBizDocsID")) & "," & CCommon.ToShort(DataBinder.Eval(e.Item.DataItem, "tintOppType")) & ")")
                        hplBizDoc.Attributes.Add("onclick", "OpenBizDoc(" & CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numOppID")) & "," & CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numOppBizDocsID")) & ")")
                    End If

                    '1 = Authorized/Pending Capture 
                    If CCommon.ToShort(DataBinder.Eval(e.Item.DataItem, "tintTransactionStatus")) = 1 Then
                        DirectCast(e.Item.FindControl("txtAmount"), TextBox).Enabled = False
                        DirectCast(e.Item.FindControl("txtAmount"), TextBox).Text = String.Format("{0:#,##0.00}", CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "monAuthorizedAmt")))

                        '2 = Captured
                    ElseIf CCommon.ToShort(DataBinder.Eval(e.Item.DataItem, "tintTransactionStatus")) = 2 Then
                        DirectCast(e.Item.FindControl("txtAmount"), TextBox).Enabled = True
                        DirectCast(e.Item.FindControl("txtAmount"), TextBox).Text = String.Format("{0:#,##0.00}", CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "monCapturedAmt"))) - CCommon.ToDecimal(DataBinder.Eval(e.Item.DataItem, "monRefundAmt"))

                        '3 = Void
                    ElseIf CCommon.ToShort(DataBinder.Eval(e.Item.DataItem, "tintTransactionStatus")) = 3 Then
                        DirectCast(e.Item.FindControl("txtAmount"), TextBox).Enabled = True
                        DirectCast(e.Item.FindControl("txtAmount"), TextBox).Text = String.Format("{0:#,##0.00}", CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "monRefundAmt")))

                        '4=Failed(with any error)    
                    ElseIf CCommon.ToShort(DataBinder.Eval(e.Item.DataItem, "tintTransactionStatus")) = 4 Then
                        e.Item.FindControl("txtAmount").Visible = False

                        '5=Credited
                    ElseIf CCommon.ToShort(DataBinder.Eval(e.Item.DataItem, "tintTransactionStatus")) = 5 Then
                        If CCommon.ToDecimal(DataBinder.Eval(e.Item.DataItem, "monRefundAmt")) < CCommon.ToDecimal(DataBinder.Eval(e.Item.DataItem, "monCapturedAmt")) Then
                            e.Item.FindControl("txtAmount").Visible = True
                            DirectCast(e.Item.FindControl("txtAmount"), TextBox).Text = CCommon.ToDecimal(DataBinder.Eval(e.Item.DataItem, "monRefundAmt"))
                        Else
                            e.Item.FindControl("txtAmount").Visible = False
                        End If
                        DirectCast(e.Item.FindControl("txtAmount"), TextBox).Enabled = False
                    End If

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Public Function ReturnName(ByVal SDate) As String
            Try
                Dim strDate As String = ""
                If Not IsDBNull(SDate) Then
                    strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                    If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                        strDate = "<font color=red>" & strDate & "</font>"
                    ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                        strDate = "<font color=orange>" & strDate & "</font>"
                    End If
                End If
                Return strDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub dgItems_Load(sender As Object, e As EventArgs) Handles dgItems.Load

        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Private Sub DisplayAlert(ByVal message As String)
            Try
                litMessage.Text = message
                divAlert.Visible = True
                divAlert.Focus()
            Catch ex As Exception
                Throw
            End Try
        End Sub
    End Class
End Namespace
