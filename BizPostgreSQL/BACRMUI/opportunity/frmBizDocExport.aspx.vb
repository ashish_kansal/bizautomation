﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Alerts
Imports System.IO

Namespace BACRM.UserInterface.Opportunities

    Partial Public Class frmBizDocExport
        Inherits BACRMPage
        Dim lngOppBizDocID, lngOppId As Long

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    
                    body.Attributes.Add("onLoad", "document.getElementById('" + btnExport.UniqueID + "').click();")

                    lngOppBizDocID = GetQueryStringVal( "OppBizId")
                    lngOppId = GetQueryStringVal( "OpID")

                    Dim dtOppBiDocDtl As DataTable
                    Dim objOppBizDocs As New OppBizDocs
                    objOppBizDocs.OppBizDocId = lngOppBizDocID
                    objOppBizDocs.OppId = lngOppId
                    objOppBizDocs.DomainID = Session("DomainID")
                    objOppBizDocs.UserCntID = Session("UserContactID")
                    objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl
                    If dtOppBiDocDtl.Rows.Count <> 0 Then
                        'Fill up details for excel export
                        If dtOppBiDocDtl.Rows(0).Item("tintBillingTerms") = True Then
                            If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtCreatedDate")) Then
                                Dim strDate As Date
                                strDate = DateAdd(DateInterval.Day, dtOppBiDocDtl.Rows(0).Item("numBillingDaysName"), dtOppBiDocDtl.Rows(0).Item("dtCreatedDate"))
                                lblDueDate1.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
                            End If
                        Else
                            If Not IsDBNull(dtOppBiDocDtl.Rows(0).Item("dtCreatedDate")) Then
                                lblDueDate1.Text = FormattedDateFromDate(dtOppBiDocDtl.Rows(0).Item("dtCreatedDate"), Session("DateFormat"))
                            End If

                        End If

                        Dim dtOppBizAddDtl As DataTable
                        'Dim objOppBizDocs As New OppBizDocs
                        objOppBizDocs.OppBizDocId = lngOppBizDocID
                        objOppBizDocs.OppId = lngOppId
                        objOppBizDocs.DomainID = Session("DomainID")
                        dtOppBizAddDtl = objOppBizDocs.GetOppAddressDetail
                        If dtOppBizAddDtl.Rows.Count <> 0 Then
                            lblBizDoc1.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("BizDcocName")), "", dtOppBizAddDtl.Rows(0).Item("BizDcocName"))
                            lblAddress1.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("BillAdd")), "", dtOppBizAddDtl.Rows(0).Item("BillAdd"))
                            lblAddress2.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("ShipAdd")), "", dtOppBizAddDtl.Rows(0).Item("ShipAdd"))
                            lblOrderNo.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("OppName")), "", dtOppBizAddDtl.Rows(0).Item("OppName"))
                            Session("BizDocName") = lblBizDoc1.Text
                        End If

                        ''lblTelephone.Text = lbl
                        'lblPO.Text = lblPONo.Text

                        lblEmail.Text = dtOppBizAddDtl.Rows(0).Item("vcEmail")
                        lblCustName.Text = dtOppBizAddDtl.Rows(0).Item("numContactID")
                        lblCompName.Text = dtOppBizAddDtl.Rows(0).Item("CompName")
                        ''lblSuite.Text = lbl
                        lblPO.Text = dtOppBiDocDtl.Rows(0).Item("vcRefOrderNo")
                        lblComment.Text = dtOppBiDocDtl.Rows(0).Item("vcComments")




                        ' Get Items
                        Dim ds As New DataSet
                        Dim dtOppBiDocItems As DataTable
                        objOppBizDocs.DomainID = Session("DomainID")
                        objOppBizDocs.OppBizDocId = lngOppBizDocID
                        objOppBizDocs.OppId = lngOppId
                        ds = objOppBizDocs.GetOppInItems
                        dtOppBiDocItems = ds.Tables(0)


                        Dim objCalculateDealAmount As New CalculateDealAmount
                        objCalculateDealAmount.CalculateDealAmount(lngOppId, lngOppBizDocID, CShort(dtOppBiDocDtl.Rows(0).Item("tintOppType")), Session("DomainID"), dtOppBiDocItems, FromBizInvoice:=True)

                        hdLateCharge.Value = String.Format("{0:#,##0.00}", objCalculateDealAmount.TotalLateCharges)

                        'Commented by chintan, reason: line item discount is display purpose only. take aggregate discount from BizDoc edit ->Discount Field
                        hdDisc.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalDiscount) + IIf(dtOppBiDocItems.Rows.Count > 0, dtOppBiDocItems.Compute("SUM(DiscAmt)", ""), 0)

                        hdSubTotal.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalAmount + (objCalculateDealAmount.TotalDiscount + IIf(dtOppBiDocItems.Rows.Count > 0, dtOppBiDocItems.Compute("SUM(DiscAmt)", ""), 0)))

                        hdTaxAmt.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalTaxAmount - objCalculateDealAmount.TotalCRVTaxAmount)

                        hdCRVTaxAmt.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.TotalCRVTaxAmount)

                        hdGrandTotal.Value = CCommon.GetDecimalFormat(objCalculateDealAmount.GrandTotal)

                        hdnCreditAmount.Value = CCommon.GetDecimalFormat(IIf(objCalculateDealAmount.CreditAmount > 0, objCalculateDealAmount.CreditAmount * -1, 0))

                        Session("Itemslist") = dtOppBiDocItems




                        '''Add columns to datagrid
                        Dim i As Integer
                        Dim dtdgColumns As DataTable
                        dtdgColumns = ds.Tables(2)
                        Dim bColumn As BoundColumn
                        For i = 0 To dtdgColumns.Rows.Count - 1
                            bColumn = New BoundColumn
                            bColumn.HeaderText = dtdgColumns.Rows(i).Item("vcFormFieldName")
                            bColumn.DataField = dtdgColumns.Rows(i).Item("vcDbColumnName")
                            If dtdgColumns.Rows(i).Item("vcFieldDataType") = "M" Then bColumn.DataFormatString = "{0:#,##0.00}"
                            dgBizDocs.Columns.Add(bColumn)
                        Next
                        dgBizDocs.DataSource = dtOppBiDocItems
                        dgBizDocs.DataBind()
                        Try
                            dgBizDocs.Columns(2).Visible = False
                            dgBizDocs.Columns(3).Visible = False
                            dgBizDocs.Columns(7).Visible = False
                        Catch ex As Exception

                        End Try




                        Dim objConfigWizard As New FormConfigWizard
                        If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                            objConfigWizard.FormID = 7
                        Else
                            objConfigWizard.FormID = 8
                        End If
                        objConfigWizard.DomainID = Session("DomainID")
                        objConfigWizard.BizDocID = dtOppBiDocDtl.Rows(0).Item("numBizDocId")
                        objConfigWizard.BizDocTemplateID = dtOppBiDocDtl.Rows(0).Item("numBizDocTempID")
                        Dim dsNew As DataSet
                        Dim dtTable As DataTable
                        dsNew = objConfigWizard.GetFieldFormListForBizDocsSumm
                        dtTable = dsNew.Tables(1)
                        If dtTable.Rows.Count = 0 Then dtTable = dsNew.Tables(0)
                        Dim tblrow As HtmlTableRow
                        Dim tblCell As HtmlTableCell

                        Dim strLabelCellID As String


                        For Each dr As DataRow In dtTable.Rows
                            tblrow = New HtmlTableRow
                            tblCell = New HtmlTableCell
                            tblCell.Attributes.Add("class", "normal1")
                            tblCell.InnerText = dr("vcFormFieldName") & ": "
                            tblrow.Cells.Add(tblCell)

                            tblCell = New HtmlTableCell
                            tblCell.Attributes.Add("class", "normal1")
                            If dr("vcDbColumnName") = "SubTotal" Then
                                tblCell.InnerText = String.Format("{0:#,##0.00}", CCommon.ToDouble(hdSubTotal.Value))
                            ElseIf dr("vcDbColumnName") = "ShippingAmount" Then
                                tblCell.InnerText = String.Format("{0:#,##0.00}", CCommon.ToDouble(hdShipAmt.Value))
                            ElseIf dr("vcDbColumnName") = "TotalSalesTax" Then
                                tblCell.InnerText = String.Format("{0:#,##0.00}", CCommon.ToDouble(hdTaxAmt.Value))
                            ElseIf dr("vcDbColumnName") = "TotalCRVTax" Then
                                tblCell.InnerText = String.Format("{0:#,##0.00}", CCommon.ToDouble(hdCRVTaxAmt.Value))
                            ElseIf dr("vcDbColumnName") = "LateCharge" Then
                                tblCell.InnerText = String.Format("{0:#,##0.00}", CCommon.ToDouble(hdLateCharge.Value))
                            ElseIf dr("vcDbColumnName") = "Discount" Then
                                tblCell.InnerText = String.Format("{0:#,##0.00}", CCommon.ToDouble(hdDisc.Value))
                            ElseIf dr("vcDbColumnName") = "CreditApplied" Then
                                tblCell.InnerText = String.Format("{0:#,##0.00}", CCommon.ToDouble(hdnCreditAmount.Value))
                            ElseIf dr("vcDbColumnName") = "GrandTotal" Then
                                tblCell.ID = "tdGradTotal"
                                strLabelCellID = tblCell.ID
                                'tblCell.InnerText = hdGrandTotal.Value
                            Else
                                If dtOppBiDocDtl.Rows(0).Item("tintOppType") = 1 Then
                                    Dim taxAmt As Decimal
                                    taxAmt = IIf(IsDBNull(dtOppBiDocItems.Compute("SUM([" & dr("vcFormFieldName") & "])", "[" & dr("vcFormFieldName") & "]>0")), 0, dtOppBiDocItems.Compute("SUM([" & dr("vcFormFieldName") & "])", "[" & dr("vcFormFieldName") & "]>0"))
                                    tblCell.InnerText = String.Format("{0:###0.00}", taxAmt)
                                End If
                            End If
                            tblrow.Cells.Add(tblCell)
                            tblBizDocSumm.Rows.Add(tblrow)
                        Next
                        If strLabelCellID <> "" Then
                            CType(tblBizDocSumm.FindControl(strLabelCellID), HtmlTableCell).InnerText = String.Format("{0:#,##0.00}", CCommon.ToDouble(hdGrandTotal.Value))
                        End If



                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub Export()
            Try
                Response.Clear()
                Response.ClearContent()
                Response.Charset = "UTF-8"
                Response.ContentType = "application/x-msexcel"
                Response.AddHeader("content-disposition", "attachment; filename=" + Session("BizDocName") + "_" + DateTime.Now.Date.ToShortDateString() + ".xls")
                Response.Write(Session("ExportData").ToString())
                Response.End()
                Session("ExportData") = Nothing
                Session("BizDocName") = Nothing
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExport.Click
            Try
                Export()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Protected Overloads Overrides Sub Render(ByVal writer As HtmlTextWriter)
            Try
                Dim sw As New StringWriter()
                writer.InnerWriter = sw
                MyBase.Render(writer)
                Session("ExportData") = sw.ToString()
                Response.Write(sw.ToString())
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
