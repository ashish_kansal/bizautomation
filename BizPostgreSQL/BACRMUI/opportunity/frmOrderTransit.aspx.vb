﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common

Public Class frmOrderTransit
    Inherits BACRMPage

    Dim lngItemCode As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngItemCode = GetQueryStringVal( "ItemCode")

            If Not IsPostBack Then
                If lngItemCode > 0 Then
                    bindGrid()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub bindGrid()
        Dim objOpportunity As New MOpportunity
        objOpportunity.ItemCode = lngItemCode
        objOpportunity.DomainID = Session("DomainID")
        objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
        objOpportunity.WarehouseItemID = CCommon.ToLong(GetQueryStringVal("WarehouseItemID"))
        gvOrderTransit.DataSource = objOpportunity.GetItemTransit(2)
        gvOrderTransit.DataBind()
    End Sub
End Class