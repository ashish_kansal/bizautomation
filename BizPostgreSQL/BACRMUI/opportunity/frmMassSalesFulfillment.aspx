﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/BizMaster.Master" CodeBehind="frmMassSalesFulfillment.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmMassSalesFulfillment" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../CSS/animate.min.css" />
    <link href='../JavaScript/MultiSelect/bootstrap-multiselect.css' rel="stylesheet" />
    <link href='<%# ResolveUrl("~/JavaScript/daterangepicker-master/daterangepicker.css")%>' rel="stylesheet" />
    <script type="text/javascript" src="../JavaScript/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../JavaScript/MultiSelect/bootstrap-multiselect.js"></script>
    <script type="text/javascript" src="../JavaScript/jquery.twbsPagination.min.js"></script>
    <script type="text/javascript" src="../JavaScript/MassSalesFulfillment.js"></script>
    <script type="text/javascript" src="../JavaScript/colResizable-1.6.min.js"></script>
    <script src='<%# ResolveUrl("~/JavaScript/daterangepicker-master/moment.min.js")%>'></script>
    <script src='<%# ResolveUrl("~/JavaScript/daterangepicker-master/daterangepicker.js")%>'></script>

    <style type="text/css">
        .bg-pay, .bg-pay:hover, .bg-pay:focus {
            background-color: #9900cc;
            color: #fff;
        }

        .bg-pack, .bg-pack:hover, .bg-pack:focus {
            background-color: #0073b7;
            color: #fff;
        }

        .bg-close, .bg-close:hover, .bg-close:focus {
            background-color: #c00000;
            color: #fff;
        }

        .ms-ul > li {
            padding: 5px;
        }

        .ms-sidebar > .row {
            margin: 0px;
            padding: 0px;
        }

        #ms-container .nav-tabs {
            background: none;
        }

        #ms-sidebar {
            min-height: 85vh;
            vertical-align: top;
            background-color: #f9f9f9;
            border-right: 5px solid #00a65a;
            -webkit-transition: -webkit-transform .3s ease-in-out, width .3s ease-in-out;
            -moz-transition: -moz-transform .3s ease-in-out, width .3s ease-in-out;
            -o-transition: -o-transform .3s ease-in-out, width .3s ease-in-out;
            transition: transform .3s ease-in-out, width .3s ease-in-out;
        }

        div.ms-sidebar-collapsed #ms-sidebar {
            -webkit-transform: translate(0, 0);
            -ms-transform: translate(0, 0);
            -o-transform: translate(0, 0);
            transform: translate(0, 0);
            width: 0px !important;
            padding-right: 0px;
            padding-left: 0px;
            border-right: 0px !important;
        }

            div.ms-sidebar-collapsed #ms-sidebar .row {
                display: none;
            }

        #ms-main-grid {
            min-height: 85vh;
            vertical-align: top;
            border-right: 5px solid #d0d0d0;
            -webkit-transition: -webkit-transform .3s ease-in-out, width .3s ease-in-out;
            -moz-transition: -moz-transform .3s ease-in-out, width .3s ease-in-out;
            -o-transition: -o-transform .3s ease-in-out, width .3s ease-in-out;
            transition: transform .3s ease-in-out, width .3s ease-in-out;
        }

        #min-grid-controls {
            margin: 0px;
        }

            #min-grid-controls ul.pagination {
                margin: 0px;
            }

            #min-grid-controls > li {
                padding: 0px;
                vertical-align: top;
            }

        div.ms-maingrid-collapsed #ms-main-grid {
            -webkit-transform: translate(0, 0);
            -ms-transform: translate(0, 0);
            -o-transform: translate(0, 0);
            transform: translate(0, 0);
            width: 0px !important;
            padding-right: 0px;
            padding-left: 0px;
            border-right: 0px !important;
        }

            div.ms-maingrid-collapsed #ms-main-grid .row {
                display: none;
            }

        #ms-selected-grid {
            min-height: 85vh;
            vertical-align: top;
            -webkit-transition: -webkit-transform .3s ease-in-out, width .3s ease-in-out;
            -moz-transition: -moz-transform .3s ease-in-out, width .3s ease-in-out;
            -o-transition: -o-transform .3s ease-in-out, width .3s ease-in-out;
            transition: transform .3s ease-in-out, width .3s ease-in-out;
        }

        div.ms-selectedgrid-collapsed #ms-selected-grid {
            -webkit-transform: translate(0, 0);
            -ms-transform: translate(0, 0);
            -o-transform: translate(0, 0);
            transform: translate(0, 0);
            width: 0px !important;
            padding-right: 0px;
            padding-left: 0px;
        }

            div.ms-selectedgrid-collapsed #ms-selected-grid .row {
                display: none;
            }

        div.ms-selectedgrid-collapsed #ms-main-grid {
            border-right: 0px !important;
        }

        #divOrderStatus div.checkbox, #divFilterValue div.checkbox {
            margin-top: 0px;
            margin-bottom: 0px;
        }

            #divOrderStatus div.checkbox input, #divFilterValue div.checkbox input {
                margin-left: 0px;
            }

            #divOrderStatus div.checkbox label {
                font-weight: 600;
            }

        #ulFilfillmentSteps {
            padding-top: 20px;
        }

            #ulFilfillmentSteps input {
                border: 0px;
                text-align: left;
            }

            #ulFilfillmentSteps li {
                padding-right: 15px;
                -webkit-transition: -webkit-transform .3s ease-in-out, padding-right .3s ease-in-out;
                -moz-transition: -moz-transform .3s ease-in-out, padding-right .3s ease-in-out;
                -o-transition: -o-transform .3s ease-in-out, padding-right .3s ease-in-out;
                transition: transform .3s ease-in-out, padding-right .3s ease-in-out;
            }

                #ulFilfillmentSteps li.active {
                    padding-right: 0px;
                }

        #divFilterValue .dropdown-toggle::after {
            display: inline-block;
            width: 0;
            height: 0;
            margin-left: .255em;
            vertical-align: .255em;
            content: "";
            border-top: .3em solid;
            border-right: .3em solid transparent;
            border-bottom: 0;
            border-left: .3em solid transparent;
            top: 50%;
            position: absolute;
            right: 3%;
            float: right;
        }

        #divFilterValue .dropdown-menu {
            padding: 10px;
        }

        #divMainGridTopFilters div.checkbox {
            margin-top: 0px;
            margin-bottom: 0px;
        }

            #divMainGridTopFilters div.checkbox input {
                margin-left: 0px;
            }

            #divMainGridTopFilters div.checkbox label {
                font-weight: 600;
            }

        #divMainGridTopFilters ul li {
            vertical-align: middle;
        }

            #divMainGridTopFilters ul li:last-child {
                vertical-align: top;
            }

        #divMainGrid {
            height: 72vh;
            overflow-y: auto;
            overflow-x: auto;
        }

            #divMainGrid > table > tbody > tr > th {
                position: sticky;
                top: -1px;
                white-space: nowrap;
                overflow: visible !important;
            }

            #divMainGrid > table > tbody > tr > td.nowrap {
                white-space: nowrap;
            }

        #divBatch {
            width: auto;
        }

            #divBatch .dropdown-menu > li:focus, #divBatch .dropdown-menu > li:hover {
                color: #262626;
                text-decoration: none;
                background-color: #f5f5f5;
            }


        #divGridRightPick {
            height: 71vh;
            overflow-y: auto;
            overflow-x: auto;
        }

            #divGridRightPick > table > thead > tr > th {
                position: sticky;
                top: -1px;
                white-space: nowrap;
            }

            #divGridRightPick > table > tbody > tr > td.nowrap {
                white-space: nowrap;
            }

        #divGridRightPack {
            height: 71vh;
            overflow-y: auto;
        }

        .focusedRow {
            transform: scale(1);
            -webkit-transform: scale(1);
            -moz-transform: scale(1);
            box-shadow: 0px 0px 5px #06a65a;
            -webkit-box-shadow: 0px 0px 5px #06a65a;
            -moz-box-shadow: 0px 0px 5px #06a65a;
        }

        .pickedItem {
            background-color: #DBF9DC !important;
        }

        .animated.slower {
            -webkit-animation-duration: 1s;
            animation-duration: 1s;
        }

        #divPackSubFilters ul.nav li {
            white-space: nowrap;
            margin-right: 5px;
            margin-bottom: -1px;
        }


        #divPackSubFilters ul.nav, #divMobilePick ul.nav, #divPrintBizDocSubFilters ul.nav {
            background: none !important;
            position: relative;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: -moz-flex;
            display: -ms-flex;
            display: flex;
            border-bottom: 0px;
        }

            #divPackSubFilters ul.nav li, #divMobilePick ul.nav li, #divPrintBizDocSubFilters ul.nav li {
                border: 1px solid #<%=BACRM.BusinessLogic.Common.CCommon.ToString(Session("vcThemeClass")) %>;
                -webkit-flex: 1;
                -moz-flex: 1;
                -ms-flex: 1;
                flex: 1;
                text-align: center;
            }

                #divPrintBizDocSubFilters ul.nav li:not(:last-child), #divMobilePick ul.nav li:not(:last-child) {
                    border-right: none;
                }

                #divPackSubFilters ul.nav li.active, #divPrintBizDocSubFilters ul.nav li.active, #divMobilePick ul.nav li.active {
                    border-top-color: #<%=BACRM.BusinessLogic.Common.CCommon.ToString(Session("vcThemeClass")) %>;
                    border-bottom: none;
                    background-color: #<%=BACRM.BusinessLogic.Common.CCommon.ToString(Session("vcThemeClass")) %>;
                }

                #divPackSubFilters ul.nav li a, #divPackSubFilters ul.nav li a:active, #divPackSubFilters ul.nav li a:focus, #divPackSubFilters ul.nav li a:hover {
                    border: none;
                    color: #<%=BACRM.BusinessLogic.Common.CCommon.ToString(Session("vcThemeClass")) %>;
                    border-radius: 0px;
                    margin: 0px;
                    padding: 6px 5px;
                }

                #divPrintBizDocSubFilters ul.nav li a, #divPrintBizDocSubFilters ul.nav li a:active, #divPrintBizDocSubFilters ul.nav li a:focus, #divPrintBizDocSubFilters ul.nav li a:hover {
                    border: none;
                    color: #<%=BACRM.BusinessLogic.Common.CCommon.ToString(Session("vcThemeClass")) %>;
                    border-radius: 0px;
                    margin: 0px;
                }

                #divMobilePick ul.nav li a, #divMobilePick ul.nav li a:active, #divMobilePick ul.nav li a:focus, #divMobilePick ul.nav li a:hover {
                    border: none;
                    color: #000;
                    border-radius: 0px;
                    margin: 0px;
                }

                #divPackSubFilters ul.nav li.active a, #divPrintBizDocSubFilters ul.nav li.active a, #divMobilePick ul.nav li.active a {
                    background: none;
                    color: #FFF;
                    font-weight: bold;
                }

        #divMobilePick .get-rates, .get-rates-success, .ship-orders, .invoice-orders, .close-orders, .pay-orders {
            font-size: 22px;
        }

        #tblRight {
            margin-bottom: 0px;
        }


        .order-box-body .row {
            margin-left: 0px;
            margin-right: 0px;
        }




        #divGridRightPack > .box {
            position: relative;
            border-radius: 0px;
            background: #ffffff;
            border-top: 0px;
            margin-bottom: 0px;
            width: 100%;
            box-shadow: 0 1px 1px rgba(0, 0, 0, 0.1);
        }

        #divGridRightPack > .box-primary > .box-header {
            padding-top: 5px;
            padding-bottom: 5px;
            background-color: #1473B4;
            color: #fff;
        }

        #divGridRightPack .box-body {
            padding: 0px;
        }

        #divGridRightPack .btn-box-tool {
            padding: 0px;
            font-size: 12px;
            background: transparent;
            color: #fff;
        }

        .ulItems, .ulBoxes {
            border: 1px solid #e9e9e9;
            color: #333;
            text-decoration: none;
            line-height: 1.3em;
            background: #f6f6f6;
            margin: 5px;
            padding: 5px;
            min-height: 340px;
            max-height: 340px;
        }

            .ulBoxes li {
                min-width: 320px;
            }

            .ulBoxes .box-header > .box-tools {
                position: initial;
                padding-left: 30px;
            }

            .ulBoxes .box-header > .box-title {
                line-height: 32px;
            }

            .ulBoxes .package-type {
                margin-right: 5px;
            }

        .ulItems {
            overflow-y: scroll;
        }

            .ulItems > li, .ulBoxedItems > li {
                cursor: move;
                margin-bottom: 4px;
                padding: 2px 2px;
                border: 1px solid #A3B5CE;
                text-align: center;
                background: #A3B5CE;
                color: White;
                text-align: left;
            }

            .ulItems > li {
                line-height: 25px;
                z-index: 101;
            }

        .ulBoxedItems {
            margin-top: 5px;
            min-width: 250px;
            min-height: 128px;
            overflow-y: scroll;
            white-space: nowrap;
            z-index: 100;
        }

            .ulBoxedItems > li {
                line-height: 22px;
            }

        #modal-shipping-detail .page-header {
            margin: 0px 0 10px 0;
            font-size: 16px;
        }

        #modal-shipping-detail .form-group {
            margin-bottom: 5px;
        }

        #modal-shipping-detail .modal-header, #modal-shipping-detail .modal-footer {
            padding-left: 15px;
            padding-top: 5px;
            padding-bottom: 5px;
        }

        #modal-shipping-detail .input-group .input-group-addon {
            background-color: #eee;
            font-weight: bold;
        }

        #divMobilePick {
            display: none;
        }

        .FromDateRangePicker, .ToDateRangePicker {
            border: 1px solid #e1e1e1;
            border-bottom: 0px;
            padding: 2px;
            font-style: italic;
            clear: both;
        }

        /* Custom, iPhone Retina */
        @media only screen and (max-width : 320px) {
            .container-fluid {
                padding-left: 15px;
                padding-right: 15px;
            }

            #ms-main-grid .row {
                margin-left: 0px;
                margin-right: 0px;
            }

            .main-header {
                display: none;
            }

            .content-wrapper {
                padding-top: 0px !important;
            }

            #divMobilePick {
                display: block;
            }

            #ms-sidebar {
                display: none;
            }

            #ms-main-grid {
                display: initial;
            }

            #ms-selected-grid {
                display: none;
            }

            #btncollapseselectedgrid {
                display: none;
            }
        }

        /* Extra Small Devices, Phones */
        @media only screen and (max-width : 480px) {
            .container-fluid {
                padding-left: 15px;
                padding-right: 15px;
            }

            #ms-main-grid .row {
                margin-left: 0px;
                margin-right: 0px;
            }

            .main-header {
                display: none;
            }

            .content-wrapper {
                padding-top: 0px !important;
            }

            #divMobilePick {
                display: block;
            }

            #ms-sidebar {
                display: none;
            }

            #ms-main-grid {
                display: initial;
            }

            #ms-selected-grid {
                display: none;
            }

            #btncollapseselectedgrid {
                display: none;
            }
        }

        /* Small Devices, Tablets */
        @media only screen and (max-width : 768px) {
            .container-fluid {
                padding-left: 15px;
                padding-right: 15px;
            }

            #ms-main-grid .row {
                margin-left: 0px;
                margin-right: 0px;
            }

            .main-header {
                display: none;
            }

            .content-wrapper {
                padding-top: 0px !important;
            }

            #divMobilePick {
                display: block;
            }

            #ms-sidebar {
                display: none;
            }

            #ms-main-grid {
                display: initial;
            }

            #ms-selected-grid {
                display: none;
            }

            #btncollapseselectedgrid {
                display: none;
            }
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div class="overlay" id="divLoader" style="z-index: 10000">
        <div class="overlayContent" style="color: #000; background-color: #fff; text-align: center; width: 250px; padding: 20px">
            <i class="fa fa-2x fa-refresh fa-spin"></i>
            <h3>Please wait...</h3>
        </div>
    </div>
    <div class="container-fluid">
        <div id="ms-container" class="row ms-sidebar-expanded ms-maingrid-expanded ms-selectedgrid-expanded" style="background-color: white; border-radius: 5px;">
            <div class="row padbottom10" id="divMobilePick">
                <div class="col-xs-12">
                    <ul class="nav nav-tabs">
                        <li id="liPickView1" class="active"><a href="javascript:PickingViewChanged(1)">Select Item(s)</a></li>
                        <li id="liPickView2"><a href="javascript:PickingViewChanged(2)">Pick Item(s)</a></li>
                    </ul>
                </div>
            </div>
            <div id="ms-sidebar" class="col-sm-12 col-md-2">
                <div class="row" style="margin-top: 5px">
                    <div class="col-xs-12 padbottom10">
                        <select id="ddlShippingZone" class="form-control">
                            <option value="0">-- Select Shipping Zone --</option>
                        </select>
                    </div>
                    <div class="col-xs-12 padbottom10">
                        <div class="pull-left">
                            <ul class="list-inline">
                                <li>
                                    <input type="radio" id="rbInclude" name="filterType" value="1" checked="checked" />
                                    Include</li>
                                <li>
                                    <input type="radio" id="rbExclude" name="filterType" value="2" />
                                    Exclude</li>
                                <li>
                                    <a href="#" onclick="return OpenHelpPopUp('opportunity/frmmasssalesfulfillment.aspx')"><label class="badge bg-yellow">?</label></a>
                                </li>
                            </ul>
                        </div>
                        <div class="pull-right">
                            <a id="hplPrintBizDocs" href="javascript:void">Print BizDocs</a>
                        </div>
                    </div>
                    <div class="col-xs-12 padbottom10">

                        <ul class="list-unstyled" id="divOrderStatus">
                        </ul>
                    </div>
                    <div class="col-xs-12 padbottom10">
                        <select id="ddlFiterBy" class="form-control">
                            <option value="0">-- Filter By --</option>
                            <option value="1">Item Classification</option>
                        </select>
                    </div>
                    <div class="col-xs-12 padbottom10">
                        <div class="dropdown" id="divFilterValue">
                            <button class="btn dropdown-toggle btn-block btn-flat" style="border-radius: 0; box-shadow: none; border-color: #d2d6de; background-color: #fff; text-align: left" type="button" id="dropdownMenuFilter" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                None Selcted
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuFilter" id="divFilterValueOptions">
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 padbottom10">
                        <input type="button" value="Apply Filter(s)" id="btnApplyFilters" class="btn btn-primary btn-block" />
                        <br />
                        <br />
                    </div>
                    <div class="col-xs-12" style="padding-right: 0px;">
                        <ul class="list-unstyled" id="ulFilfillmentSteps">
                            <li style="padding-bottom: 10px" class="liPick active">
                                <input type="button" id="btnPick" value="Ready to Pick" class="btn btn-flat bg-green btn-block" />
                            </li>
                            <li style="padding-bottom: 10px" class="liPackShip">
                                <input type="button" id="btnPackShip" value="Ready to Pack & Ship" class="btn btn-flat bg-pack btn-block" />
                            </li>
                            <li style="padding-bottom: 10px" class="liInvoice">
                                <input type="button" id="btnInvoice" value="Ready to Invoice" class="btn btn-flat bg-orange btn-block" />
                            </li>
                            <li style="padding-bottom: 10px" class="liPay">
                                <input type="button" id="btnPay" value="Ready to Pay" class="btn btn-flat bg-pay btn-block" />
                            </li>
                            <li style="padding-bottom: 10px" class="liClose">
                                <input type="button" id="btnClose" value="Pending Close" class="btn btn-flat bg-close btn-block" />
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="ms-main-grid" class="col-sm-12 col-md-7">
                <div class="row padbottom10" style="padding: 5px;" id="ms-main-grid-top">
                    <div class="pull-left">
                        <div class="btn-group" role="group">
                            <button id="btnmssidebar" type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i></button>
                        </div>
                    </div>
                    <div class="pull-right">
                        <ul class="list-inline" id="min-grid-controls">
                            <li id="liRecordDisplay"></li>
                            <li>
                                <button id="btnClearFilters" type="button" class="btn btn-default" title="Clear Filters"><i class="fa fa-filter"></i></button>
                                <button id="btncolumnconfig" type="button" class="btn btn-default" title="Configure Columns"><i class="fa fa-cog"></i></button>
                                <div class="btn-group" role="group" id="divExpandCollpaseGroup">
                                    <button id="btncollapsemaingrid" type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i></button>
                                    <button id="btnexpandbothgrid" type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i><i class="fa fa-chevron-right"></i></button>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row padbottom10" id="divPackFilters" style="display: none">
                    <div class="col-xs-12">
                        <div class="panel with-nav-tabs panel-primary no-border" style="margin-bottom:0px;">
                            <div class="panel-heading">
                                <ul class="nav nav-tabs">
                                    <li id="liPackView2" class="active"><a href="javascript:PackingViewChanged(2)">Pack, Label, & Ship Orders</a></li>
                                    <li id="liPackView1"><a href="javascript:PackingViewChanged(1)">Orders without Shipping Rate</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row padbottom10" id="divPackSubFilters" style="display: none">
                    <div class="col-xs-12">
                        <button id="btnPackPickedItems" class="btn btn-primary">Add Packing Slip for Qty <br />Picked (Not Packed)</button>
                        <button id="btnShipPackedItems" class="btn" style="background-color:#bf8f00;color:#fff;border-color:#bf8f00;">Ship Qty Packed<br />(Not Shipped)</button>
                        <button id="btnPackAndShipPickedItems" class="btn" style="background-color:#bf8f00;color:#fff;border-color:#bf8f00;">Ship Qty Picked (Not Packed) & <br />Add matching Packing Slip</button>
                    </div>
                </div>
                <div class="row padbottom10">
                    <div class="pull-left" style="padding-left: 5px;">
                        <button id="btnInvoiceOrders" type="button" style="display: none" class="btn btn-flat btn-primary">Invoice Orders</button>
                        <button id="btnCloseOrders" type="button" style="display: none" class="btn btn-flat btn-primary">Close Orders</button>
                        <button id="btnPrintBizDoc" type="button" style="display: none" class="btn btn-flat btn-primary"><i class="fa fa-print"></i>&nbsp;&nbsp;Print BizDoc</button>
                        <button id="btnPrintLabel" type="button" style="display: none" class="btn btn-flat btn-primary"><i class="fa fa-print"></i>&nbsp;&nbsp;Print Shipping Label</button>
                        <button type="button" id="btnRemoveFromBatch" style="display: none" class="btn btn-danger btn-flat"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;Remove from Batch</button>
                        <button id="btnCreatePackingSlip" type="button" style="display: none" class="btn btn-flat btn-primary">Add Packing Slips</button>
                        <button id="btnShipOrders" type="button" style="display: none" class="btn btn-flat btn-primary">Ship Orders</button>
                        <div class="form-inline" id="divPayOptions" style="display: none">
                            <div class="form-group">
                                <label></label>
                                <input type="radio" name="paymenttype" id="rdCreditCard" checked="checked" />
                                Charge credit card on file
                                <%--<input type="radio" name="paymenttype" id="rdEmailTemplate" />
                                Send Email-Template with link to payment form--%>
                            </div>
                            <button id="btnPayOrders" type="button" class="btn btn-flat btn-primary">Pay Orders</button>
                        </div>
                        <div class="form-inline" id="divShippingRates" style="display: none">
                            <button id="btnGetShippingRate" type="button" class="btn btn-flat btn-primary">Get Shipping Rate</button>
                            <button id="btnCommitShippingRate" type="button" class="btn btn-flat btn-primary" style="display: none">Commit Rates to Orders</button>
                            <button id="btnCancelShippingRate" type="button" class="btn btn-flat btn-danger" style="display: none">Cancel</button>
                        </div>
                    </div>
                    <div class="pull-right" id="divMainGridTopFilters">
                        <ul class="list-inline">
                            <li id="divPrintOrderOpenClosed" style="display: none">
                                <ul class="list-inline">
                                    <li>
                                        <input type="radio" value="1" name="rdOpenCloseAll" onchange="OpenCloseAllChanged();" checked="checked" />
                                        Open</li>
                                    <li>
                                        <input type="radio" value="2" name="rdOpenCloseAll" onchange="OpenCloseAllChanged();" />
                                        Closed</li>
                                    <li>
                                        <input type="radio" value="3" name="rdOpenCloseAll" onchange="OpenCloseAllChanged();" />
                                        All</li>
                                </ul>
                            </li>
                            <li id="divCloseOptions" style="display: none">
                                <select id="ddlPendingCloseFilter" class="form-control">
                                    <option value="1">All Sales Orders</option>
                                    <option value="2">Fully Shipped and Invoiced Sales Orders</option>
                                </select>
                            </li>
                            <li id="liGroupBy">
                                <div class="checkbox">
                                    <input type="checkbox" id="chkGroupBy" /><label>Group by Order</label>
                                </div>
                            </li>
                            <li id="liBackOrder">
                                <div class="checkbox">
                                    <input type="checkbox" id="chkBackOrder" /><label>Remove Items with BO</label>
                                </div>
                            </li>
                            <li>
                                <select id="ddlWarehouse" class="form-control">
                                </select>
                            </li>
                            <li>
                                <div class="input-group-btn" id="divBatch">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="btnBatch">Select Batch <span class="caret"></span></button>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li role="separator" class="divider"></li>
                                        <li><a href="javascript:AddToBatch(1);" style="font-weight: 600">Add to Batch</a></li>
                                        <li role="separator" class="divider"></li>
                                        <li><a href="javascript:AddToBatch(2);" style="font-weight: 600">Create New Batch & Add</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                <button type="button" id="btnPrintPickList" class="btn btn-primary btn-flat"><i class="fa fa-print"></i>&nbsp;&nbsp;Print Pick List</button>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row padbottom10" id="divPrintBizDocSubFilters" style="display: none">
                    <ul class="nav nav-tabs">
                        <li id="liPrintBizDocView1" class="active"><a href="javascript:PrintBizDocViewChanged(1)">Pick Lists</a></li>
                        <li id="liPrintBizDocView2"><a href="javascript:PrintBizDocViewChanged(2)">Packing Slips</a></li>
                        <li id="liPrintBizDocView3"><a href="javascript:PrintBizDocViewChanged(3)">Fulfillment BizDocs</a></li>
                        <li id="liPrintBizDocView4"><a href="javascript:PrintBizDocViewChanged(4)">Invoices</a></li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-xs-12" style="padding: 0px;">
                        <div class="table-responsive" id="divMainGrid">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-3" id="ms-selected-grid">
                <div class="row padbottom10" style="padding: 5px;">
                    <div class="pull-left">
                        <div class="btn-group" role="group">
                            <div class="btn-group" role="group" id="div1">
                                <button id="btnexpandbothgridright" type="button" class="btn btn-default"><i class="fa fa-chevron-left"></i><i class="fa fa-chevron-right"></i></button>
                                <button id="btncollapseselectedgrid" type="button" class="btn btn-default"><i class="fa fa-chevron-right"></i></button>
                            </div>
                        </div>
                    </div>
                    <div class="pull-right">
                        <input type="text" id="txtScan" class="form-control" placeholder="Scan Item" onkeydown='return ItemPicked(this);' />
                    </div>
                </div>
                <div class="row padbottom10" style="padding: 5px; margin-bottom: 15px;" id="divPickActionsRight">
                    <div class="pull-left">
                        <select id="ddlBatchRight" class="form-control"></select>
                    </div>
                    <div class="pull-right">
                        <button type="button" id="btnPickRight" class="btn btn-flat btn-success">Pick</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12" style="padding: 0px;">
                        <div class="table-responsive" id="divGridRightPick" style="display: none">
                            <table class="table table-bordered table-striped" id="tblRight">
                            </table>
                        </div>
                        <div id="divGridRightPack" style="display: none">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hdnMSWarehouseID" runat="server" />
    <asp:HiddenField ID="hdnMSImagePath" runat="server" />
    <asp:HiddenField ID="hdnMSCurrencyID" runat="server" />
    <asp:HiddenField ID="hdnDecimalPoints" runat="server" />
    <asp:HiddenField ID="hdnDefaultShippingBizDoc" runat="server" />
    <input type="hidden" id="hdnViewID" value="1" runat="server" clientidmode="Static" />
    <input type="hidden" id="hdnHeaderSearch" value="" />
    <input type="hidden" id="hdnBatchID" value="0" />
    <input type="hidden" id="hdnSortColumn" value="OpportunityMaster.bintCreatedDate" />
    <input type="hidden" id="hdnSortOrder" value="DESC" />
    <input type="hidden" id="hdnPickWithoutInventoryCheck" />
    <input type="hidden" id="hdnGroupByOrderPick" />
    <input type="hidden" id="hdnGroupByOrderShip" />
    <input type="hidden" id="hdnGroupByOrderInvoice" />
    <input type="hidden" id="hdnGroupByOrderPay" />
    <input type="hidden" id="hdnGroupByOrderClose" />
    <input type="hidden" id="hdnInvoicingType" />
    <input type="hidden" id="hdnScanField" />
    <input type="hidden" id="hdnDefaultShippingBizDocType" />
    <input type="hidden" id="hdnSelectedRecordsForRightPanePicking" />
    <input type="hidden" id="hdnContainers" />
    <input type="hidden" id="hdnPickListByOrder" />
    <input type="hidden" id="hdnPackingViewMode" value="1" />
    <input type="hidden" id="hdnPrintBizDocViewMode" value="1" />
    <input type="hidden" id="hdnEnablePickListMapping" />
    <input type="hidden" id="hdnEnableFulfillmentBizDocMapping" />

    <asp:HiddenField runat="server" ID="hdnLastViewMode" Value="0" />
    <asp:HiddenField runat="server" ID="hdnPackLastSubViewMode" Value="0" />
    <asp:HiddenField runat="server" ID="hdnPrintLastSubViewMode" Value="0" />

    <div class="modal fade" id="modal-batch" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Default Modal</h4>
                    <input type="hidden" id="hdnBatchMode" value="" />
                </div>
                <div class="modal-body">
                    <select id="ddlBatch" style="display: none" class="form-control">
                    </select>
                    <div class="form-inline" id="divBatchName" style="display: none">
                        <div class="form-group">
                            <label>Batch Name:</label>
                            <input id="txtBatchName" class="form-control" type="text" />
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="return SaveBatch();">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modal-shipping-detail" style="display: none;">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Shipping Detail</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <h2 class="page-header">Ship From</h2>
                            <div class="row">
                                <div class="col-sm-12 col-md-6 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">Contact</span>
                                        <input type="text" class="form-control" id="txtFromContact" placeholder="Contact" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">Phone</span>
                                        <input type="text" class="form-control" id="txtFromPhone" placeholder="Phone" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">Company</span>
                                        <input type="text" class="form-control" id="txtFromCompany" placeholder="Company" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">Address Line 1</span>
                                        <input type="text" class="form-control" id="txtFromAddress1" placeholder="Address Line 1" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">Address Line 2</span>
                                        <input type="text" class="form-control" id="txtFromAddress2" placeholder="Address Line 2" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-6 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">Country</span>
                                        <select id="ddlFromCountry" class="form-control"></select>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">State</span>
                                        <select id="ddlFromState" class="form-control"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-5 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">City</span>
                                        <input type="text" class="form-control" id="txtFromCity" placeholder="City" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">ZipCode</span>
                                        <input type="text" class="form-control" id="txtFromZip" placeholder="ZipCode" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-3 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">Is Residential</span>
                                        <div class="form-control">
                                            <input type="checkbox" id="chkFromResidential" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <h2 class="page-header">Ship To</h2>
                            <div class="row">
                                <div class="col-sm-12 col-md-6 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">Contact</span>
                                        <input type="text" class="form-control" id="txtToContact" placeholder="Contact" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">Phone</span>
                                        <input type="text" class="form-control" id="txtToPhone" placeholder="Phone" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">Company</span>
                                        <input type="text" class="form-control" id="txtToCompany" placeholder="Company" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">Address Line 1</span>
                                        <input type="text" class="form-control" id="txtToAddress1" placeholder="Address Line 1" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">Address Line 2</span>
                                        <input type="text" class="form-control" id="txtToAddress2" placeholder="Address Line 2" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-6 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">Country</span>
                                        <select id="ddlToCountry" class="form-control"></select>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">State</span>
                                        <select id="ddlToState" class="form-control"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-5 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">City</span>
                                        <input type="text" class="form-control" id="txtToCity" placeholder="City" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-4 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">ZipCode</span>
                                        <input type="text" class="form-control" id="txtToZip" placeholder="ZipCode" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-3 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">Is Residential</span>
                                        <div class="form-control">
                                            <input type="checkbox" id="chkToResidential" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h2 class="page-header">Shipping/Duties Payment Info</h2>
                    <div class="row">
                        <div class="col-sm-12 col-md-6 form-group">
                            <div class="input-group">
                                <span class="input-group-addon">Payment Option</span>
                                <select class="form-control" id="ddlPaymentOption">
                                    <option value="0">Sender</option>
                                    <option value="1">Recipient</option>
                                    <option value="2">Third Party</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6 form-group">
                            <div class="input-group">
                                <span class="input-group-addon">Reference Number</span>
                                <input type="text" class="form-control" id="txtReferenceNo" placeholder="Reference Number" />
                            </div>
                        </div>
                    </div>
                    <div class="row" id="divPaymentOption1" style="display: none">
                        <div class="col-sm-12 col-md-4 form-group">
                            <div class="input-group">
                                <span class="input-group-addon">Account Number</span>
                                <input type="text" class="form-control" id="txtAccountNumber" placeholder="Account Number" />
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4" id="divThirdPartyZipCode">
                            <div class="input-group">
                                <span class="input-group-addon">ZipCode</span>
                                <input type="text" class="form-control" id="txtZipCode" placeholder="ZipCode" />
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <div class="input-group">
                                <span class="input-group-addon">Country</span>
                                <select class="form-control" id="ddlCountry"></select>
                            </div>
                        </div>
                    </div>
                    <h2 class="page-header">Other Info</h2>
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <ul class="list-unstyled">
                                <li id="liAdditionalHandling">
                                    <input type="checkbox" id="chkAdditionalHandling" />
                                    Additional Handling</li>
                                <li id="liCOD">
                                    <input type="checkbox" id="chkCOD" />
                                    COD</li>
                                <li id="liHDP">
                                    <input type="checkbox" id="chkHDP" />
                                    Home Delivery Premium</li>
                                <li id="liInsideDelivery">
                                    <input type="checkbox" id="chkInsideDelivery" />
                                    Inside Delivery</li>
                                <li id="liInsidePickup">
                                    <input type="checkbox" id="chkInsidePickup" />
                                    Inside Pickup</li>
                                <li id="liLargePackage">
                                    <input type="checkbox" id="chkLargePackage" />
                                    Large Package</li>
                                <li id="liSaturdayDelivery">
                                    <input type="checkbox" id="chkSaturdayDelivery" />
                                    Saturday Delivery</li>
                                <li id="liSaturdayPickup">
                                    <input type="checkbox" id="chkSaturdayPickup" />
                                    Saturday Pickup</li>
                            </ul>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="row">
                                <div class="col-xs-12 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">Signature Type</span>
                                        <select class="form-control" id="ddlSignatureType">
                                            <option value="0">Service Default</option>
                                            <option value="1">Adult Signature Required</option>
                                            <option value="2">Direct Signature</option>
                                            <option value="3">InDirect Signature</option>
                                            <option value="4">No Signature Required</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="divShipDescription">
                                <div class="col-xs-12 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">Description</span>
                                        <input type="text" class="form-control" id="txtDescription" placeholder="Description" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-6 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">Total Insured Value (USD)</span>
                                        <input type="text" class="form-control" id="txtInsuredValue" placeholder="Total Insured Value" />
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">Total Customs Value (USD)</span>
                                        <input type="text" class="form-control" id="txtCustomValue" placeholder="Total Customs Value" />
                                    </div>
                                </div>
                            </div>
                            <div class="row" id="divShipCOD" style="display: none">
                                <div class="col-sm-12 col-md-6 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">COD Type</span>
                                        <select class="form-control" id="ddlCODType">
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6 form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">COD Total Amount (USD)</span>
                                        <input type="text" class="form-control" id="txtCODAmount" placeholder="COD Total Amount" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="hdnShipOppID" />
                    <input type="hidden" id="hdnShipOppBizDocID" />
                    <input type="hidden" id="hdnShippingReportID" />
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="return SaveShippingDetail();">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-parent-bizdoc">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Select BizDoc</h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="hdnRecordID" />
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="return SaveParentBizDoc();">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modal-confirmation">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body text-center text-bold">
                </div>
                <div class="modal-footer">
                    <input type="hidden" id="hdnBizDocIDs" />
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="btnSaveConfirmation">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</asp:Content>
