Imports System.IO
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common

Partial Public Class frmBizDocAddAttchmnts
    Inherits BACRMPage

    Dim strFilePath As String
    Dim strFileName As String
    Dim OppType As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            OppType = GetQueryStringVal( "OppType")

            If Not IsPostBack Then
                
                
                objCommon.sb_FillComboFromDBwithSel(ddlBizDoc, 27, Session("DomainID"))
                LoadDTL()
            End If
            btnSave.Attributes.Add("onclick", "return AddAttachmnts()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadDTL()
        Try
            If GetQueryStringVal( "AtchID") <> "" Then
                Dim objBizDoc As New OppBizDocs
                Dim dtTable As DataTable
                objBizDoc.BizDocAtchID = GetQueryStringVal( "AtchID")
                dtTable = objBizDoc.GetBizDocAttachments
                If dtTable.Rows.Count = 1 Then
                    txtDocName.Text = dtTable.Rows(0).Item("vcDocName")
                    If Not IsDBNull(dtTable.Rows(0).Item("numBizDocID")) Then
                        If Not ddlBizDoc.Items.FindByValue(dtTable.Rows(0).Item("numBizDocID")) Is Nothing Then
                            ddlBizDoc.ClearSelection()
                            ddlBizDoc.Items.FindByValue(dtTable.Rows(0).Item("numBizDocID")).Selected = True
                        End If
                    End If

                    BindBizDocsTemplate()

                    If Not IsDBNull(dtTable.Rows(0).Item("numBizDocTempID")) Then
                        If Not ddlBizDocTemplate.Items.FindByValue(dtTable.Rows(0).Item("numBizDocTempID")) Is Nothing Then
                            ddlBizDocTemplate.ClearSelection()
                            ddlBizDocTemplate.Items.FindByValue(dtTable.Rows(0).Item("numBizDocTempID")).Selected = True
                        End If
                    End If

                    txtURL.Text = dtTable.Rows(0).Item("vcURL")
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objBizDoc As New OppBizDocs
            objBizDoc.BizDocAtchID = IIf(GetQueryStringVal("AtchID") = "", 0, GetQueryStringVal("AtchID"))
            If txtMagFile.Value = "" Then
                objBizDoc.vcURL = txtURL.Text
            Else
                MagUpload()
                objBizDoc.vcURL = strFileName
            End If
            objBizDoc.DomainID = Session("DomainID")
            objBizDoc.BizDocId = ddlBizDoc.SelectedValue
            objBizDoc.vcDocName = txtDocName.Text
            objBizDoc.BizDocTemplateID = ddlBizDocTemplate.SelectedValue
            objBizDoc.AddBizDocAttachments()
            Response.Redirect("../opportunity/frmBizDocAttachments.aspx?BizDocID=" & ddlBizDoc.SelectedValue & "&OppType=" & OppType & "&TempID=" & ddlBizDocTemplate.SelectedValue)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub MagUpload()
        Try
            If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
            End If
            Dim ArrFilePath, ArrFileExt
            Dim strFileExt As String
            If txtMagFile.PostedFile.FileName = "" Then Exit Sub

            'If chkConvert.Checked = True Then
            '    Dim objHTMLToPDF As New HTMLToPDF
            '    'strFileName = objPDFCON.Convert(txtMagFile.PostedFile.FileName)
            '    Exit Sub
            'End If

            ArrFileExt = Split(txtMagFile.PostedFile.FileName, ".")
            strFileExt = ArrFileExt(UBound(ArrFileExt))
            strFileName = "File" & Format(Now, "ddmmyyyyhhmmss") & "." & strFileExt

            strFilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID"))
            strFilePath = strFilePath & strFileName
            If Not txtMagFile.PostedFile Is Nothing Then txtMagFile.PostedFile.SaveAs(strFilePath)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        Try
            Response.Redirect("../opportunity/frmBizDocAttachments.aspx?BizDocID=" & ddlBizDoc.SelectedValue & "&OppType=" & OppType & "&TempID=" & ddlBizDocTemplate.SelectedValue)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlBizDoc_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBizDoc.SelectedIndexChanged
        Try
            BindBizDocsTemplate()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindBizDocsTemplate()
        Try
            Dim objOppBizDoc As New OppBizDocs
            objOppBizDoc.DomainID = Session("DomainID")
            objOppBizDoc.BizDocId = ddlBizDoc.SelectedValue
            objOppBizDoc.OppType = OppType

            Dim dtBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()

            ddlBizDocTemplate.DataSource = dtBizDocTemplate
            ddlBizDocTemplate.DataTextField = "vcTemplateName"
            ddlBizDocTemplate.DataValueField = "numBizDocTempID"
            ddlBizDocTemplate.DataBind()

            ddlBizDocTemplate.Items.Insert(0, New ListItem("--Select--", 0))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class