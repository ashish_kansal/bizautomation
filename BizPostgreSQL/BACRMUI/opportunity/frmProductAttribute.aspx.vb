﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Admin

Public Class frmProductAttribute
    Inherits BACRMPage


    Dim objCommon As CCommon
    Dim objItems As CItems
    Dim strValues As String
    Dim dtOppAtributes As DataTable
    Dim ds As DataSet
    Dim dsTemp As DataSet
    Dim lngDivId, lngCntID, lngOppId, OppBizDocID, JournalId As Long
    Dim dtOppBiDocItems As DataTable
    Dim strMessage As String
    Dim ResponseMessage As String
    Dim boolPurchased As Boolean = False
    Dim IsFromCreateBizDoc As Boolean = False
    Dim IsFromSaveAndOpenOrderDetails As Boolean = False
    Dim IsFromSaveAndNew As Boolean = False
    Dim boolFlag As Boolean = True

    Dim tblrow As TableRow = Nothing
    Dim tblcell As TableCell = Nothing
    Dim ddl As DropDownList = Nothing
    Dim lbl As Label = Nothing
    Private m_dynamicDropDownList As DropDownList()

    Private Sub frmProductAttribute_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

    End Sub


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'CreateAttributes()
        If Not Page.IsPostBack Then
            objItems = New CItems
            Dim dtItemDetails As DataTable

            objItems.ItemCode = GetQueryStringVal( "ItemCode")
            dtItemDetails = objItems.ItemDetails

            If dtItemDetails.Rows.Count > 0 Then
                If Not IsDBNull(dtItemDetails.Rows(0).Item("vcPathForImage")) Then
                    If dtItemDetails.Rows(0).Item("vcPathForImage") <> "" Then
                        imgThumb.Visible = True
                        imgThumb.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtItemDetails.Rows(0).Item("vcPathForImage")
                    Else : imgThumb.Visible = False
                    End If
                Else : imgThumb.Visible = False
                End If
            End If
        End If
        CreateDroDown()
    End Sub

    Public Sub CreateDroDown()
        Try
            tblrow = New TableRow()
            tblcell = New TableCell()
            tblcell.CssClass = "normal1"
            tblcell.HorizontalAlign = HorizontalAlign.Right
            lbl = New Label()
            lbl.Text = "Warehouse"
            lbl.Font.Bold = True
            tblcell.Controls.Add(lbl)
            tblrow.Cells.Add(tblcell)

            tblcell = New TableCell()
            ddl = New DropDownList()
            ' new DropDownList();
            ddl.CssClass = "signup"
            ddl.ID = "ddWarehouse"
            tblcell.Controls.Add(ddl)
            ddl.AutoPostBack = True
            tblrow.Cells.Add(tblcell)


            objItems = New CItems
            objItems.ItemCode = GetQueryStringVal( "ItemCode")
            ds = objItems.GetItemWareHouses()
            Session("dsProductAttr") = ds


            ddl.DataSource = ds.Tables(0).DefaultView.ToTable(True, "numWareHouseID", "vcWareHouse")
            ddl.DataTextField = "vcWareHouse"
            ddl.DataValueField = "numWareHouseID"
            ddl.DataBind()
            ddl.Items.Insert(0, New ListItem("--Select One --", "0"))

            tblItemAttr.Rows.Add(tblrow)
            AddHandler ddl.SelectedIndexChanged, AddressOf ddWarehouse_SelectedIndexChanged


            'dtOppAtributes = ds.Tables(2).AsEnumerable().Distinct().CopyToDataTable()
            'dtOppAtributes = ds.Tables(2).DefaultView.ToTable(True, "numWareHouseID")
            dtOppAtributes = ds.Tables(2)

            If dtOppAtributes.Rows.Count > 0 Then
                Dim i As Integer = 0

                'if(AttributeControlType.Contains("DropDown List"))
                m_dynamicDropDownList = New DropDownList(dtOppAtributes.Rows.Count - 1) {}

                For i = 0 To dtOppAtributes.Rows.Count - 1
                    tblrow = New TableRow()
                    tblcell = New TableCell()
                    tblcell.CssClass = "normal1"
                    tblcell.HorizontalAlign = HorizontalAlign.Right
                    lbl = New Label()
                    lbl.Text = dtOppAtributes.Rows(i)("Fld_label")
                    lbl.Font.Bold = True
                    tblcell.Controls.Add(lbl)
                    tblrow.Cells.Add(tblcell)
                    If dtOppAtributes.Rows(i)("fld_type").ToString() = "SelectBox" Then
                        tblcell = New TableCell()
                        ddl = New DropDownList()
                        ' new DropDownList();
                        ddl.CssClass = "signup"
                        ddl.ID = "Attr~" + dtOppAtributes.Rows(i)("numlistid").ToString() + "~" + dtOppAtributes.Rows(i)("Fld_label")
                        AddHandler ddl.SelectedIndexChanged, AddressOf ddAttr_SelectedIndexChanged
                        tblcell.Controls.Add(ddl)
                        ddl.AutoPostBack = True
                        tblrow.Cells.Add(tblcell)
                        m_dynamicDropDownList(i) = ddl
                    End If
                    tblItemAttr.Rows.Add(tblrow)
                Next
            End If
        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
    End Sub

    Private Sub ddWarehouse_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim value As Integer = CType(sender, DropDownList).SelectedValue
        'Do something with value
        'CreateAttributes(value)
        'For Each ctrl As Control In Page.Controls
        '    If TypeOf ctrl Is DropDownList Then
        '        If ctrl.ID.Contains("Attr") Then
        '            ddl = ctrl
        '            objCommon = New CCommon
        '            objCommon.sb_FillAttibuesForEcommFromDB(ddl, ddl.ID.Remove(0, 4).ToString(), 689, "", value)
        '        End If
        '    End If
        'Next ctrl
        btnSelectClose.Visible = False

        For Each ctrl As DropDownList In m_dynamicDropDownList
            ctrl.Items.Clear()
            objCommon = New CCommon
            objCommon.sb_FillAttibuesForEcommFromDB(ctrl, ctrl.ID.Substring(ctrl.ID.IndexOf("~") + 1, ctrl.ID.LastIndexOf("~") - ctrl.ID.IndexOf("~") - 1), GetQueryStringVal( "ItemCode"), "", value)
            'ctrl.Items.Insert(0, New ListItem("--Select One --", "0"))
        Next ctrl
    End Sub

    Private Sub ddAttr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        checkAll()
    End Sub
   Private Sub btnSelectClose_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSelectClose.Click
        'Response.Write("<script language='javascript'>opener.document.__doPostBack('radWareHouse_DropDown',''); self.close();</script>")
        'Response.Write("<script language='javascript'>opener.location.reload(true); self.close();</script>")
        Response.Write("<script language='javascript'>opener.SetWareHouseId(""" & btnSelectClose.CommandArgument & """); self.close();</script>")


        'Response.End()
        'Dim script As String = "<script language='javascript'> var myDropdownList = opener.getElementById(""radWareHouse_DropDown"");alert(myDropdownList);</script>"

        'Dim script As String = "<script language='javascript'> var myDropdownList = window.opener.getElementById('radWareHouse_DropDown');alert(myDropdownList);" & _
        '                        "for (iLoop = 0; iLoop< myDropdownList.options.length; iLoop++)" & _
        '                        "{if (myDropdownList.options[iLoop].value == """ & CType(Page.FindControl("ddWarehouse"), DropDownList).SelectedValue & """){myDropdownList.options[iLoop].selected = true;break;}};self.close();</script>"
        'Response.Write(Script)
    End Sub
    Public Sub checkAll()
        If CType(Master.FindControl("Content").FindControl("ddWarehouse"), DropDownList).SelectedValue <> 0 Then
            Dim str As String = Nothing
            str = "numWareHouseID=" & CType(Master.FindControl("Content").FindControl("ddWarehouse"), DropDownList).SelectedValue
            For Each ctrl As DropDownList In m_dynamicDropDownList
                str += " and [" & ctrl.ID.Remove(0, ctrl.ID.LastIndexOf("~") + 1) & "]=" & ctrl.SelectedValue
            Next ctrl

            'str = str.TrimEnd("and")
            'Response.Write(str)

            ds = New DataSet
            ds = Session("dsProductAttr")
            Dim view As DataView = New DataView(ds.Tables(0))
            view.RowFilter = str

            If view.Count > 0 Then
                'Dim dr As DataRow
                'dr = view(0).Row
                'Response.Write(dr("Price").ToString())
                tblrow = New TableRow()
                tblcell = New TableCell()
                tblcell.CssClass = "normal1"
                tblcell.HorizontalAlign = HorizontalAlign.Left
                tblcell.ColumnSpan = 2
                lbl = New Label()
                lbl.Text = String.Format("<strong>Price:</strong>{0:#,##0.00}", view(0)("Price"))
                tblcell.Controls.Add(lbl)
                tblrow.Cells.Add(tblcell)
                tblItemAttr.Rows.Add(tblrow)

                tblrow = New TableRow()
                tblcell = New TableCell()
                tblcell.CssClass = "normal1"
                tblcell.HorizontalAlign = HorizontalAlign.Left
                tblcell.ColumnSpan = 2
                lbl = New Label()
                lbl.Text = " <strong>On Hand:</strong>" + view(0)("OnHand").ToString() + "&nbsp;&nbsp;<strong>Allocation:</strong>" + view(0)("Allocation").ToString() + "&nbsp;&nbsp;<strong>On Order:</strong>" + view(0)("OnOrder").ToString() + "&nbsp;&nbsp;<strong>On Back Order:</strong>" + view(0)("BackOrder").ToString()
                tblcell.Controls.Add(lbl)
                tblrow.Cells.Add(tblcell)
                tblItemAttr.Rows.Add(tblrow)

                btnSelectClose.CommandArgument = view(0)("numWareHouseItemId")

                btnSelectClose.Visible = True
                'tblrow = New TableRow()
                'tblcell = New TableCell()
                'tblcell.CssClass = "normal1"
                'tblcell.HorizontalAlign = HorizontalAlign.Right
                'tblcell.ColumnSpan = 2
                'Dim btn As Button
                'btn = New Button()
                'btn.ID = "btnSelectClose"
                'btn.Text = "Select & Close"
                'btn.CssClass = "button"
                'AddHandler btn.Click, AddressOf btnSelectClose_Click
                'tblcell.Controls.Add(btn)
                'tblrow.Cells.Add(tblcell)
                'tblItemAttr.Rows.Add(tblrow)

                'Response.Write(" Price:" + view(0)("Price").ToString() + " On Hand:" + view(0)("OnHand").ToString() + " Allocation:" + view(0)("Allocation").ToString() + " On Order:" + view(0)("OnOrder").ToString() + " On Back Order:" + view(0)("BackOrder").ToString())
            End If
        Else
            btnSelectClose.Visible = False
        End If
    End Sub
    'Public Sub CreateAttributes(ByVal id As Integer)
    '    Try
    '        Dim strAttributes As String = ""
    '        strValues = ""
    '        Dim boolLoadNextdropdown As Boolean = False
    '        objItems = New CItems
    '        objItems.ItemCode = 689
    '        objItems.WarehouseID = id
    '        dtOppAtributes = objItems.GetOppItemAttributesForEcomm()
    '        If dtOppAtributes.Rows.Count > 0 Then
    '            Dim i As Integer = 0

    '            'if(AttributeControlType.Contains("DropDown List"))

    '            For i = 0 To dtOppAtributes.Rows.Count - 1
    '                tblrow = New TableRow()
    '                tblcell = New TableCell()
    '                tblcell.CssClass = "normal1"
    '                tblcell.HorizontalAlign = HorizontalAlign.Right
    '                lbl = New Label()
    '                lbl.Text = dtOppAtributes.Rows(i)("Fld_label")
    '                lbl.Font.Bold = True
    '                tblcell.Controls.Add(lbl)
    '                tblrow.Cells.Add(tblcell)
    '                If dtOppAtributes.Rows(i)("fld_type").ToString() = "SelectBox" Then
    '                    tblcell = New TableCell()
    '                    ddl = New DropDownList()
    '                    ' new DropDownList();
    '                    ddl.CssClass = "dropdown"
    '                    ddl.ID = "Attr" + dtOppAtributes.Rows(i)("fld_id").ToString()
    '                    tblcell.Controls.Add(ddl)
    '                    ddl.AutoPostBack = True
    '                    tblrow.Cells.Add(tblcell)
    '                    strAttributes = (strAttributes + ddl.ID & "~") + dtOppAtributes.Rows(i)("Fld_label") & ","
    '                    If ddl.SelectedIndex > 0 Then
    '                        strValues = strValues + ddl.SelectedValue + ","
    '                    End If
    '                    objCommon = New CCommon
    '                    objCommon.sb_FillAttibuesForEcommFromDB(ddl, Convert.ToDouble(dtOppAtributes.Rows(i)("numlistid").ToString()), 689, "", id)

    '                End If
    '                tblItemAttr.Rows.Add(tblrow)
    '            Next

    '        End If
    '        strAttributes = strAttributes.TrimEnd(","c)


    '    Catch ex As Exception
    '        Response.Write(ex.ToString())
    '    End Try
    'End Sub

   
End Class