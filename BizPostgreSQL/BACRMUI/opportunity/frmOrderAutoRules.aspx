﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmOrderAutoRules.aspx.vb"
    Inherits="BACRM.UserInterface.Opportunities.frmOrderAutoRules" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <table id="Table1" height="2" cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td valign="bottom">
                    <table class="TabStyle">
                        <tr>
                            <td>
                                &nbsp;&nbsp;&nbsp;Order Automation Rules&nbsp;&nbsp;
                            </td>
                        </tr>
                    </table>
                </td>
                <td align="right">
                    <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save &amp; Close">
                    </asp:Button>
                    <asp:Button ID="btnDelete" runat="server" CssClass="button" Text="Delete"></asp:Button>
                    <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="50">
                    </asp:Button>
                </td>
            </tr>
        </table>
        <table id="Table2" height="10" cellspacing="0" cellpadding="0" border="0" width="100%">
            <tr>
                <td class="aspTable">
                    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
                        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="103px">
                        <asp:TableRow>
                            <asp:TableCell>
                                <br>
                                <table cellspacing="2" cellpadding="0" border="0" style="width: 874px">
                                    <tr>
                                        <td align="left" colspan="2">
                                            <b>Exhibit #1</b>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="normal1" align="right" colspan="2">
                                            <span style="font-size: 9pt"> If  "drop-ship" is checked for an inventory item when creating
                                                new Authoritative Sales BizDoc then auto-create matching Purchase Orders primary
                                                vendors where</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" class="normal1">
                                            Bill-to is
                                            <asp:DropDownList CssClass="signup" ID="ddlBillTo1" runat="server" Width="200">
                                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                            </asp:DropDownList>
                                            &nbsp;and ship-to is
                                            <asp:DropDownList CssClass="signup" ID="ddlShipTo1" runat="server" Width="200" AutoPostBack="True">
                                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:CheckBox ID="chkAmount1" runat="server" Text="ONLY IF Amount is paid in full." CssClass="signup"/>
                                            <asp:CheckBox ID="chkBizDoc1" runat="server" Text="ONLY when Authoritative Sales BizDoc Status is set to " AutoPostBack="True" CssClass="signup" />
                                            <asp:DropDownList CssClass="signup" ID="ddlBizDocStatus1" runat="server" Width="200">
                                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <b>Exhibit #2</b>
                                        </td>
                                        <td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="normal1" align="right" colspan="2">
                                            If item is a
                                            <asp:DropDownList ID="ddlItemType" runat="server" CssClass="signup" Height="17px"
                                                Width="214px">
                                                <asp:ListItem Text="-Select One--" Value="21"></asp:ListItem>
                                                <asp:ListItem Value="1">Services</asp:ListItem>
                                                <asp:ListItem Value="2">Inventory Items</asp:ListItem>
                                                <asp:ListItem Value="3">Non-Inventory Items</asp:ListItem>
                                                <asp:ListItem Value="4">Serialized Items</asp:ListItem>
                                                <asp:ListItem Value="5">Kits</asp:ListItem>
                                                <asp:ListItem Value="6">Assemblies</asp:ListItem>
                                                <asp:ListItem Value="7">Matrix Items</asp:ListItem>
                                            </asp:DropDownList>
                                            &nbsp;where item classification is
                                            <asp:DropDownList CssClass="signup" ID="ddlItemClass" runat="server" Width="200">
                                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                            </asp:DropDownList>
                                            &nbsp;then WHEN it's added to an Authoritative Sales BizDoc,
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="normal1" align="right" colspan="2">
                                            auto-create matching Purchase Orders for its primary vendor, where Bill-to is
                                            <asp:DropDownList CssClass="signup" ID="ddlBillTo2" runat="server" Width="200">
                                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                            </asp:DropDownList>
                                            &nbsp;and ship-to is
                                            <asp:DropDownList CssClass="signup" ID="ddlShipTo2" runat="server" Width="200">
                                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="normal1" align="right" colspan="2">
                                            <asp:CheckBox ID="chkAmount" runat="server" Text="ONLY IF Amount is paid in full." />
                                            <asp:CheckBox ID="chkBizDoc" runat="server" Text="ONLY when Authoritative Sales BizDoc Status is set to" AutoPostBack="True" />
                                            &nbsp;
                                            <asp:DropDownList CssClass="signup" ID="ddlBizDocStatus" runat="server" Width="200">
                                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                            </asp:DropDownList>
                                            &nbsp;
                                            <asp:Button ID="btnAdd" runat="server" CssClass="button" Text="Add"></asp:Button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="normal1" align="right" colspan="2">
                                            <asp:DataGrid ID="dgReport" AutoGenerateColumns="False" runat="server" Width="100%"
                                                CssClass="dg">
                                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                <ItemStyle CssClass="is"></ItemStyle>
                                                <Columns>
                                                    <asp:BoundColumn DataField="Item Type" HeaderText="Item Type"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Item Classification" HeaderText=" Item Classification">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Bill To" HeaderText="Bill To"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Ship To" HeaderText="Ship To"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Amount Paid Full" HeaderText="Amount Paid Full"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="Biz Doc Status" HeaderText="Biz Doc Status"></asp:BoundColumn>
                                                    <asp:BoundColumn DataField="numItemTypeID" HeaderText="numItemTypeID" Visible="False">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="numItemClassID" HeaderText="numItemClassID" Visible="False">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="numBillToID" HeaderText="numBillToID" Visible="False">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="numShipToID" HeaderText="numShipToID" Visible="False">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="btFullPaid" HeaderText="btFullPaid" Visible="False">
                                                    </asp:BoundColumn>
                                                    <asp:BoundColumn DataField="numBizDocStatus" HeaderText="numBizDocStatus" Visible="False">
                                                    </asp:BoundColumn>
                                                    <asp:ButtonColumn CommandName="Delete" HeaderText="Delete" Text="Delete"></asp:ButtonColumn>
                                                </Columns>
                                                <HeaderStyle CssClass="hs"></HeaderStyle>
                                            </asp:DataGrid>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="normal1" align="right" colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                                <br />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </td>
            </tr>
            <tr align="center">
                <td class="normal4" valign="middle">
                    <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hdRuleID" runat="server" Value="0" />
    </form>
</body>
</html>
