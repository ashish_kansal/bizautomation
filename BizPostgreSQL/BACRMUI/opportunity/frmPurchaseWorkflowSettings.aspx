﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/DetailPage.Master" CodeBehind="frmPurchaseWorkflowSettings.aspx.vb" Inherits=".frmPurchaseWorkflowSettings" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Purchase Order Workflow Settings</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server">
    
        <table cellpadding="2" cellspacing="2" border="0" width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Label ID="litMessage" EnableViewState="false" runat="server"></asp:Label>
            </td>
            <td align="right">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" />&nbsp;
                        <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="button" Visible="false" />&nbsp;&nbsp;
            </td>
        </tr>
    </table>
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server">

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server">
    <asp:Table ID="table7" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="350">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <table width="100%" class="aspTable">
                    <tr class="normal1" align="right">
                        <td align="left" style="color: #808080; font-weight: bold; font-size: 16px;" class="normal1" colspan="2">
                            <div>
                                <div style="float: left; padding-left: 20px;">
                                    <asp:Image ID="Image7" ImageUrl="~/images/box.png" runat="server" />
                                </div>
                                <div style="float: left; padding-left: 5px; padding-top: 5px; height: 20px; text-align: center;">
                                    <span>Pay Bill Workflow
                                    </span>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr class="normal1" align="right">
                        <td align="left" class="normal1" colspan="2">
                            <div style="margin-bottom: 5px; margin-left: 20px; padding-top: 5px; border: solid 1px lightgray; overflow: hidden;">
                                <div style="margin-top: 8px;">
                                    <b>WHEN Bill payment is made:</b>
                                </div>
                                <div id="liRule15" runat="server" style="margin-top: 8px; margin-left: 12px;">
                                    If a balance due is left on the Bill, change Bill status value to
                        <asp:DropDownList ID="ddlBizDocStatus15" runat="server" CssClass="signup">
                        </asp:DropDownList>
                                </div>
                                <div id="liRule16" runat="server" style="margin-top: 8px; margin-left: 12px;">
                                    If no balance due is left (meaning that it’s been paid in full), then change Bill status value to
                        <asp:DropDownList ID="ddlBizDocStatus16" runat="server" CssClass="signup">
                        </asp:DropDownList>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
         Purchase Order Workflow Settings</asp:Content>