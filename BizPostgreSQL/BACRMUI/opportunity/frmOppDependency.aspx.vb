'***************************************************************************************************************************
'     Author Name				 :  Anoop Jayaraj
'     Date Written				 :  18/2/2005
'***************************************************************************************************************************
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Namespace BACRM.UserInterface.Opportunities

    Public Class frmOppDependency
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents ddlProcessList As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlMilestone As System.Web.UI.WebControls.DropDownList
        Protected WithEvents ddlStage As System.Web.UI.WebControls.DropDownList
        Protected WithEvents dgDependency As System.Web.UI.WebControls.DataGrid
        Protected WithEvents btnSave As System.Web.UI.WebControls.Button
        Protected WithEvents btnSaveClose As System.Web.UI.WebControls.Button
        Protected WithEvents btnCancel As System.Web.UI.WebControls.Button
        Protected WithEvents btnAdd As System.Web.UI.WebControls.Button


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                    
                    Dim objDependency As New Dependency()
                    Dim dtProcessList As DataTable
                    objDependency.DomainId = Session("DomainID")
                    dtProcessList = objDependency.GetOPPPro
                    ddlProcessList.DataSource = dtProcessList
                    ddlProcessList.DataValueField = "ID"
                    ddlProcessList.DataTextField = "vcPOppName"
                    ddlProcessList.DataBind()
                    ddlProcessList.Items.Insert(0, "--Select One--")
                    ddlProcessList.Items.FindByText("--Select One--").Value = 0
                    Cache.Remove("Dependency")
                    Getdetails()
                End If
                btnCancel.Attributes.Add("onclick", "return Close();")
                btnAdd.Attributes.Add("onclick", "return Add();")
                btnSave.Attributes.Add("onclick", "return Save();")
                btnSaveClose.Attributes.Add("onclick", "return Save();")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub Getdetails()
            Try
                Dim objDependency As New Dependency()
                Dim dtDependncy As DataTable
                objDependency.OpporunityID = GetQueryStringVal("Opid")
                objDependency.OppStageId = GetQueryStringVal("OPPStageID")
                dtDependncy = objDependency.GetDependencyDtls
                Session("Dependency") = dtDependncy
                BindDependency()
            Catch ex As Exception
                Response.Write(ex)
            End Try
        End Sub

        Sub BindDependency()
            Try
                Dim dtDependncy As New DataTable
                dtDependncy = Session("Dependency")
                dgDependency.DataSource = dtDependncy
                dgDependency.DataBind()
            Catch ex As Exception
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlProcessList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProcessList.SelectedIndexChanged
            Try
                If ddlProcessList.SelectedIndex > 0 Then
                    Dim objDependency As New Dependency()
                    Dim dtMileStone As DataTable
                    Dim str As String()
                    str = ddlProcessList.SelectedItem.Value.Split(",")
                    objDependency.ProcessID = str(0)
                    If str(1) = "O" Then
                        dtMileStone = objDependency.GetMileStone
                    Else : dtMileStone = objDependency.GetProjectMileStone
                    End If
                    ddlMilestone.DataSource = dtMileStone
                    ddlMilestone.DataValueField = "numstagepercentage"
                    ddlMilestone.DataTextField = "numstagepercentageText"
                    ddlMilestone.DataBind()
                    ddlMilestone.Items.Insert(0, "--Select One--")
                    ddlMilestone.Items.FindByText("--Select One--").Value = 0
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlMilestone_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMilestone.SelectedIndexChanged
            Try
                If ddlMilestone.SelectedIndex > 0 Then
                    Dim objDependency As New Dependency()
                    Dim dtStages As DataTable
                    Dim str As String()
                    str = ddlProcessList.SelectedItem.Value.Split(",")
                    objDependency.ProcessID = str(0)
                    objDependency.MileStoneID = ddlMilestone.SelectedItem.Value()
                    If str(1) = "O" Then
                        dtStages = objDependency.GetStages
                    Else : dtStages = objDependency.GetProjectStages
                    End If
                    ddlStage.DataSource = dtStages
                    ddlStage.DataValueField = "numOppStageId"
                    ddlStage.DataTextField = "vcstagedetail"
                    ddlStage.DataBind()
                    ddlStage.Items.Insert(0, "--Select One--")
                    ddlStage.Items.FindByText("--Select One--").Value = 0
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub save()
            Try
                Dim ds As New DataSet
                Dim objDependency As New Dependency()
                Dim dtDependncy As New DataTable
                dtDependncy = Session("Dependency")
                dtDependncy.TableName = "Table"
                objDependency.OpporunityID = GetQueryStringVal("Opid")
                objDependency.OppStageId = GetQueryStringVal("OPPStageID")
                ds.Tables.Add(dtDependncy.Copy)
                objDependency.strDependency = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))
                objDependency.SaveDependency()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgDependency_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgDependency.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    Dim myRow As DataRow
                    Dim dtDependncy As New DataTable
                    dtDependncy = Session("Dependency")
                    Dim Count As Integer
                    For Each myRow In dtDependncy.Rows
                        If Count = e.Item.ItemIndex Then
                            dtDependncy.Rows.Remove(myRow)
                            dtDependncy.AcceptChanges()
                            Exit For
                        End If
                        Count = Count + 1
                    Next
                    dtDependncy.AcceptChanges()
                    Session("Dependency") = dtDependncy
                    BindDependency()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Try
                Dim dtDependncy As New DataTable
                dtDependncy = Session("Dependency")
                Dim ds As DataRow
                ds = dtDependncy.NewRow
                Dim str As String()
                str = ddlProcessList.SelectedItem.Value.Split(",")
                ds("numProcessStageId") = ddlStage.SelectedItem.Value
                ds("vcstagedetail") = ddlStage.SelectedItem.Text
                ds("vcPOppName") = ddlProcessList.SelectedItem.Text
                ds("vcType") = str(1)
                ds("numstagepercentage") = ddlMilestone.SelectedItem.Text
                dtDependncy.Rows.Add(ds)
                Session("Dependency") = dtDependncy
                BindDependency()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                save()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                save()
                Response.Write("<script>window.close();</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace