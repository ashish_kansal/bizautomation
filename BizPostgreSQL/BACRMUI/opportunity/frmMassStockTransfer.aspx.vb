﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Opportunities
Imports Telerik.Web.UI
Imports System.Collections.Generic
Imports System.Linq
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Admin

Namespace BACRM.UserInterface.Opportunities

    Public Class frmMassStockTransfer
        Inherits BACRMPage

#Region "Member Variables"

        Dim dsTemp As DataSet
        Dim objCommon As CCommon
        Dim lngDivId As Long
        Dim lngCntID As Long
        Dim lngOppId As Long

#End Region

#Region "Page Events"

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not Page.IsPostBack Then
                    BindEmployer()
                    createSet()

                    If CCommon.ToLong(GetQueryStringVal("ItemCode")) > 0 AndAlso CCommon.ToLong(GetQueryStringVal("WarehouseItemID")) > 0 Then
                        LoadSelectedItemDetail(CCommon.ToLong(GetQueryStringVal("ItemCode")), False)
                        hdnUserSelectedItem.Value = CCommon.ToLong(GetQueryStringVal("ItemCode"))
                        hdnUserSelectedWarehouseItemID.Value = CCommon.ToLong(GetQueryStringVal("WarehouseItemID"))
                        hdnUserSelectedItemName.Value = CCommon.ToString(GetQueryStringVal("ItemName"))
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub

#End Region

#Region "Private Methods"

        Private Sub createSet()
            Try
                dsTemp = New DataSet
                Dim dtItem As New DataTable
                Dim keys(0) As DataColumn
                Dim keyColumn As New DataColumn()
                keyColumn.ColumnName = "Id"
                keyColumn.DataType = System.Type.GetType("System.String")
                dtItem.Columns.Add(keyColumn)
                keys(0) = keyColumn
                dtItem.PrimaryKey = keys
                dtItem.Columns.Add("numFromItemCode")
                dtItem.Columns.Add("vcFrom")
                dtItem.Columns.Add("numUnitHour", GetType(Decimal))
                dtItem.Columns.Add("numOnHand")
                dtItem.Columns.Add("numFromWarehouseID")
                dtItem.Columns.Add("numFromWarehouseItemID")
                dtItem.Columns.Add("numFromAvailable")
                dtItem.Columns.Add("numFromBackorder")
                dtItem.Columns.Add("vcTo")
                dtItem.Columns.Add("numToItemCode")
                dtItem.Columns.Add("numToWarehouseID")
                dtItem.Columns.Add("numToWarehouseItemID")
                dtItem.Columns.Add("numToAvailable")
                dtItem.Columns.Add("numToBackorder")
                dtItem.Columns.Add("IsSerial")
                dtItem.Columns.Add("IsLot")
                dtItem.Columns.Add("vcSelectedSerialLotNumbers")
                dsTemp.Tables.Add(dtItem)
                dtItem.TableName = "Item"
                ViewState("SOItems") = dsTemp

                gvItems.DataSource = dsTemp.Tables(0)
                gvItems.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub BindEmployer()
            Try
                objCommon = New CCommon
                objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                Dim dt As DataTable = objCommon.GetEmployer()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    For Each dr As DataRow In dt.Rows
                        radcmbSTFrom.Items.Insert(0, New Telerik.Web.UI.RadComboBoxItem(dr("vcCompanyName"), dr("numDivisionID")))
                        radcmbSTTo.Items.Insert(0, New Telerik.Web.UI.RadComboBoxItem(dr("vcCompanyName"), dr("numDivisionID")))
                    Next

                    radcmbSTFrom_SelectedIndexChanged(Nothing, Nothing)
                    radcmbSTTo_SelectedIndexChanged(Nothing, Nothing)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub clearControls()
            hdnCurrentSelectedItem.Value = ""
            hdnSelectedItems.Value = ""
            radFromLocation.Items.Clear()
            radFromLocation.Text = ""
            radFromLocation.DataSource = Nothing
            radFromLocation.DataBind()
            lblSKU.Text = ""
            lblItemID.Text = ""
            lblAttributes.Text = ""
            hplSerialLotToTransfer.Visible = False
            hplSerialLotToTransfer.Attributes.Remove("onclick")
            txtUnitsToTransfer.Style.Add("display", "inline")
            txtUnitsToTransfer.Text = ""
            hdnSerialLotToTransfer.Value = ""

            chkTransferToItem.Checked = False
            radToLocation.Items.Clear()
            radToLocation.Text = ""
            radToLocation.DataSource = Nothing
            radToLocation.DataBind()
            lblToSKU.Text = ""
            lblToItemID.Text = ""
            lblToAttributes.Text = ""

            hdnConfirmClicked.Value = False

            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "ClearSelectedItems", "$('#divItem').show(); $('#txtItem').select2('data', null); $('#s2id_txtItemTo').removeClass('select2-container-enabled'); $('#s2id_txtItemTo').addClass('select2-container-disabled');", True)
        End Sub

        Private Sub DisplayError(ByVal ex As String)
            lblException.Text = ex
            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ScrollTo", "ScrollToError();", True)
        End Sub

        Private Sub LoadSelectedItemDetail(ByVal itemCode As Long, Optional ByVal isToItem As Boolean = False)
            Try
                If CCommon.ToLong(radcmbSTFrom.SelectedValue) = 0 Or CCommon.ToLong(radcmbSTTo.SelectedValue) = 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "CustomerValidation", "$('#txtItem').select2(""data"", """"); alert(""Select From and To customer."")", True)
                    Return
                ElseIf (CCommon.ToLong(ddlFromContact.SelectedValue) = 0 Or CCommon.ToLong(ddlToContact.SelectedValue) = 0) Then
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "ContactValidation", "$('#txtItem').select2(""data"", """"); alert(""Select From and To contact."")", True)
                    Return
                End If

                Dim objItem As New CItems
                If Not objItem.ValidateItemAccount(CCommon.ToLong(itemCode), Session("DomainID"), 1) Then
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "CustomerValidation", "$('#txtItem').select2(""data"", """"); alert(""Item Income,Asset,COGs(Expense) Account are not set. To set Income,Asset,COGs(Expense) Account go to Administration->Inventory->Item Details."")", True)
                    Return
                End If

                objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                objItem.ItemCode = itemCode
                Dim dtItemDetail As DataTable = objItem.GetItemDetailForStockTransfer(0, 0)

                If Not dtItemDetail Is Nothing AndAlso dtItemDetail.Rows.Count > 0 Then
                    If isToItem Then
                        lblToSKU.Text = CCommon.ToString(dtItemDetail.Rows(0)("vcSKU"))
                        lblToItemID.Text = itemCode
                        lblToAttributes.Text = CCommon.ToString(dtItemDetail.Rows(0)("vcAttributes"))
                    Else
                        lblSKU.Text = CCommon.ToString(dtItemDetail.Rows(0)("vcSKU"))
                        lblItemID.Text = itemCode
                        lblAttributes.Text = CCommon.ToString(dtItemDetail.Rows(0)("vcAttributes"))

                        lblToSKU.Text = CCommon.ToString(dtItemDetail.Rows(0)("vcSKU"))
                        lblToItemID.Text = itemCode
                        lblToAttributes.Text = CCommon.ToString(dtItemDetail.Rows(0)("vcAttributes"))
                    End If

                    If objCommon Is Nothing Then objCommon = New CCommon
                    Dim dtWarehouse As DataTable = objCommon.GetWarehouseAttrBasedItem(itemCode)

                    If isToItem Then
                        radToLocation.Items.Clear()
                        radToLocation.DataSource = dtWarehouse
                        radToLocation.DataBind()

                        If radToLocation.Items.Count > 0 Then
                            radcmbSTFrom.Items(0).Selected = True
                        End If
                    Else
                        If dtWarehouse.Select("numOnHand > 0 Or numAllocation > 0").Length > 0 Then
                            radFromLocation.DataSource = dtWarehouse.Select("numOnHand > 0 Or numAllocation > 0").CopyToDataTable()
                            radFromLocation.DataBind()
                        End If

                        If Not Page.IsPostBack AndAlso CCommon.ToLong(GetQueryStringVal("WarehouseItemID")) > 0 AndAlso Not radFromLocation.Items.FindItemByValue(CCommon.ToLong(GetQueryStringVal("WarehouseItemID"))) Is Nothing Then
                            radFromLocation.Items.FindItemByValue(CCommon.ToLong(GetQueryStringVal("WarehouseItemID"))).Selected = True
                        ElseIf radFromLocation.Items.Count > 0 Then
                            radFromLocation.Items(0).Selected = True
                        End If

                        If radFromLocation.Items.Count > 0 AndAlso dtWarehouse.Select("numWarehouseItemId <> " & radFromLocation.SelectedValue).Length > 0 Then
                            radToLocation.Items.Clear()
                            radToLocation.DataSource = dtWarehouse.Select("numWarehouseItemId <> " & radFromLocation.SelectedValue).CopyToDataTable()
                            radToLocation.DataBind()
                        End If

                        If radToLocation.Items.Count > 0 Then
                            radToLocation.Items(0).Selected = True
                        End If

                        If CCommon.ToLong(radFromLocation.SelectedValue) > 0 AndAlso (CCommon.ToBool(dtItemDetail.Rows(0)("bitSerialized")) Or CCommon.ToBool(dtItemDetail.Rows(0)("bitLotNo"))) Then
                            txtUnitsToTransfer.Style.Add("display", "none")
                            hplSerialLotToTransfer.Visible = True
                            hplSerialLotToTransfer.Attributes.Add("onclick", "OpenSertailLot(" & itemCode & ",-1,'" & IIf(CCommon.ToBool(dtItemDetail.Rows(0)("bitLotNo")), "Lot", "Serial") & "','-1')")
                        Else
                            hplSerialLotToTransfer.Visible = False
                            hplSerialLotToTransfer.Attributes.Remove("onclick")
                            txtUnitsToTransfer.Style.Add("display", "inline")
                        End If
                    End If
                Else
                    If isToItem Then
                        lblToSKU.Text = ""
                        lblToItemID.Text = ""
                        lblToAttributes.Text = ""
                    Else
                        lblSKU.Text = ""
                        lblItemID.Text = ""
                        lblAttributes.Text = ""
                    End If
                End If
            Catch ex As Exception
                If ex.Message.Contains("WAREHOUSE_DOES_NOT_EXISTS") Then
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "CustomerValidation", "$('#txtItem').select2(""data"", """"); alert(""No warehouse available for selected item."")", True)
                Else
                    Throw
                End If
            End Try
        End Sub

        Private Sub BindDataGrid()
            Try
                dsTemp = ViewState("SOItems")

                If Not dsTemp Is Nothing AndAlso dsTemp.Tables.Count > 0 Then
                    For Each dr As DataRow In dsTemp.Tables(0).Rows
                        If CCommon.ToLong(dr("numFromItemCode")) <> CCommon.ToLong(dr("numToItemCode")) Then
                            btnTransferOrder.Visible = False
                            Exit For
                        End If
                    Next

                    gvItems.DataSource = dsTemp.Tables(0)
                    gvItems.DataBind()
                Else
                    gvItems.DataSource = Nothing
                    gvItems.DataBind()
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub
#End Region

#Region "Event Handlers"

        Private Sub lkbItemSelected_Click(sender As Object, e As EventArgs) Handles lkbItemSelected.Click
            Try
                LoadSelectedItemDetail(hdnCurrentSelectedItem.Value)
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "FocusItemPanel", "$('#pnlItem').focus();", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub lkbItemToSelected_Click(sender As Object, e As EventArgs) Handles lkbItemToSelected.Click
            Try
                LoadSelectedItemDetail(hdnCurrentSelectedItemTo.Value, True)
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "FocusItemPanel", "$('#pnlItem').focus();", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub lkbItemRemoved_Click(sender As Object, e As EventArgs) Handles lkbItemRemoved.Click
            Try
                clearControls()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub lkbTansferToItemUnchecked_Click(sender As Object, e As EventArgs) Handles lkbTansferToItemUnchecked.Click
            Try
                If objCommon Is Nothing Then objCommon = New CCommon
                Dim dtWarehouse As DataTable = objCommon.GetWarehouseAttrBasedItem(hdnCurrentSelectedItem.Value)

                If Not dtWarehouse Is Nothing AndAlso dtWarehouse.Rows.Count > 0 Then
                    If dtWarehouse.Select("numWarehouseItemId <> " & radFromLocation.SelectedValue).Length > 0 Then
                        radToLocation.Items.Clear()
                        radToLocation.DataSource = dtWarehouse.Select("numWarehouseItemId <> " & radFromLocation.SelectedValue).CopyToDataTable()
                        radToLocation.DataBind()
                    End If

                    If radToLocation.Items.Count > 0 Then
                        radcmbSTFrom.Items(0).Selected = True
                    End If
                End If

                lblToSKU.Text = lblSKU.Text
                lblToItemID.Text = lblItemID.Text
                lblToAttributes.Text = lblAttributes.Text
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
            Try
                If CCommon.ToLong(radFromLocation.SelectedValue) = 0 Or CCommon.ToLong(radToLocation.SelectedValue) = 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Validation", "alert('Select From and To Location.')", True)
                    Exit Sub
                ElseIf CCommon.ToLong(radFromLocation.SelectedValue) = CCommon.ToLong(radToLocation.SelectedValue) Then
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Validation", "alert('Selecting same From and To Location is not allowed.')", True)
                    Exit Sub
                End If

                litMessage.Text = ""

                dsTemp = ViewState("SOItems")
                Dim dtItem As DataTable = dsTemp.Tables(0)

                Dim alert As String
                Dim alertMessage As String
                Dim account As String
                Dim itemWithoutWarehouse As String

                Dim serializer As New System.Web.Script.Serialization.JavaScriptSerializer()
                Dim objSelectedItem As SelectedItems = serializer.Deserialize(Of SelectedItems)(hdnSelectedItems.Value)

                If hdnSelectedItems.Value <> "" AndAlso Not objSelectedItem Is Nothing Then
                    If dtItem.Select("numFromItemCode=" & objSelectedItem.numItemCode & " AND numFromWarehouseItemID=" & radFromLocation.SelectedValue).Length = 0 Then
                        Dim objItem As New CItems
                        objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                        objItem.ItemCode = objSelectedItem.numItemCode
                        Dim dtItemTable As DataTable = objItem.GetItemDetailForStockTransfer(CCommon.ToLong(radFromLocation.SelectedValue), CCommon.ToLong(radToLocation.SelectedValue))

                        If Not dtItemTable Is Nothing AndAlso dtItemTable.Rows.Count > 0 Then
                            If CCommon.ToDouble(dtItemTable.Rows(0)("numFromAvailable")) <= 0 Then
                                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Validation", "alert('Available qty for selected from location is 0.')", True)
                                Exit Sub
                            ElseIf CCommon.ToDouble(txtUnitsToTransfer.Text) > CCommon.ToDouble(dtItemTable.Rows(0)("numFromAvailable")) Then
                                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Validation", "alert('Qty to Transfer can not be greater then Available qty.')", True)
                                Exit Sub
                            ElseIf CCommon.ToDouble(txtUnitsToTransfer.Text) > CCommon.ToDouble(dtItemTable.Rows(0)("numFromOnHand")) AndAlso Not CCommon.ToBool(hdnConfirmClicked.Value) Then
                                lblConfirmation.Text = "Transferring quantities from allocation may cause inventory status for dependent Sales Orders to change from ""Ready to ship"" to ""Back Order"". Sure you want to do this?"
                                btnSaveCloseConfirmation.Text = "Add Item"
                                divConfirmation.Style.Remove("display")
                                Exit Sub
                            Else
                                Dim dr As DataRow = dtItem.NewRow()
                                dr("Id") = Guid.NewGuid().ToString()
                                dr("numFromItemCode") = CCommon.ToLong(dtItemTable.Rows(0)("numFromItemCode"))
                                dr("numToItemCode") = CCommon.ToLong(dtItemTable.Rows(0)("numToItemCode"))
                                dr("IsSerial") = CCommon.ToBool(dtItemTable.Rows(0)("bitSerialized"))
                                dr("IsLot") = CCommon.ToBool(dtItemTable.Rows(0)("bitLotNo"))
                                If CCommon.ToBool(dtItemTable.Rows(0)("bitSerialized")) Or CCommon.ToBool(dtItemTable.Rows(0)("bitLotNo")) Then
                                    dr("numUnitHour") = CCommon.ToDouble(txtUnitsToTransfer.Text)
                                ElseIf CCommon.ToDouble(txtUnitsToTransfer.Text) > 0 Then
                                    dr("numUnitHour") = CCommon.ToDouble(txtUnitsToTransfer.Text)
                                Else
                                    dr("numUnitHour") = 1
                                End If
                                dr("vcFrom") = CCommon.ToString(dtItemTable.Rows(0)("vcFrom"))
                                dr("numFromWarehouseID") = CCommon.ToLong(dtItemTable.Rows(0)("numFromWarehouseID"))
                                dr("numFromWarehouseItemID") = CCommon.ToLong(dtItemTable.Rows(0)("numFromWarehouseItemID"))
                                dr("numFromAvailable") = CCommon.ToDouble(dtItemTable.Rows(0)("numFromAvailable"))
                                dr("numFromBackorder") = CCommon.ToDouble(dtItemTable.Rows(0)("numFromBackorder"))
                                dr("vcTo") = CCommon.ToString(dtItemTable.Rows(0)("vcTo"))
                                dr("numToWarehouseID") = CCommon.ToLong(dtItemTable.Rows(0)("numToWarehouseID"))
                                dr("numToWarehouseItemID") = CCommon.ToLong(dtItemTable.Rows(0)("numToWarehouseItemID"))
                                If CCommon.ToBool(dtItemTable.Rows(0)("bitSerialized")) Or CCommon.ToBool(dtItemTable.Rows(0)("bitLotNo")) Then
                                    dr("numToAvailable") = CCommon.ToDouble(dtItemTable.Rows(0)("numToAvailable"))
                                Else
                                    dr("numToAvailable") = CCommon.ToDouble(dtItemTable.Rows(0)("numToAvailable")) + 1
                                End If
                                dr("numToBackOrder") = CCommon.ToDouble(dtItemTable.Rows(0)("numToBackOrder"))
                                dr("vcSelectedSerialLotNumbers") = hdnSerialLotToTransfer.Value

                                dtItem.Rows.Add(dr)
                                dsTemp.AcceptChanges()
                                ViewState("SOItems") = dsTemp

                                BindDataGrid()
                                clearControls()
                            End If
                        Else
                            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Validation", "alert('No item found with selected details')", True)
                            Exit Sub
                        End If
                    Else
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "ItemAlreadyAdded", "alert('Item with selected from location is already added.');", True)
                    End If
                Else
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "ItemsNotSelected", "alert('Select item.');", True)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub

        Private Sub btnTransferStock_Click(sender As Object, e As EventArgs) Handles btnTransferStock.Click
            Try
                dsTemp = ViewState("SOItems")

                If Not dsTemp Is Nothing AndAlso dsTemp.Tables.Count > 0 AndAlso dsTemp.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In dsTemp.Tables(0).Rows
                        If CCommon.ToDouble(dr("numUnitHour")) > CCommon.ToDouble(dr("numFromAvailable")) Then
                            lblException.Text = "You can’t transfer more qty than you have on-hand"
                            Exit Sub
                        End If
                    Next
                Else
                    lblException.Text = "Add atleast one item for stock transfer"
                    Exit Sub
                End If


                Dim objJournal As New JournalEntry
                If ChartOfAccounting.GetDefaultAccount("CG", Session("DomainID")) = 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Alert", "alert('Please Set Default COGs account from Administration->Global Settings->Accounting->Default Accounts Mapping To Save' );", True)
                    Exit Sub
                End If

                If ChartOfAccounting.GetDefaultAccount("PC", Session("DomainID")) = 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Alert", "alert('Please Set Default Purchase Clearing account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                    Exit Sub
                End If

                If ChartOfAccounting.GetDefaultAccount("PV", Session("DomainID")) = 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Alert", "alert('Please Set Default Purchase Price Variance account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                    Exit Sub
                End If


                For Each dr As DataRow In dsTemp.Tables(0).Rows
                    If CCommon.ToLong(dr("numFromWarehouseItemID")) = 0 Or CCommon.ToLong(dr("numToWarehouseItemID")) = 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Alert", "alert('From and To location Some item(s) have transfer qty more than ' );", True)
                        Exit Sub
                    End If

                    If CCommon.ToBool(dr("IsSerial")) Or CCommon.ToBool(dr("IsLot")) Then
                        If Not String.IsNullOrEmpty(CCommon.ToString(dr("vcSelectedSerialLotNumbers"))) Then
                            Dim qty As Integer = 0
                            Dim arrSerialLot As String() = CCommon.ToString(dr("vcSelectedSerialLotNumbers")).Split(",")

                            If Not String.IsNullOrEmpty(CCommon.ToString(dr("vcSelectedSerialLotNumbers"))) AndAlso Not arrSerialLot Is Nothing AndAlso arrSerialLot.Length > 0 Then
                                For Each objSerialLot As String In arrSerialLot
                                    qty = qty + CCommon.ToInteger(objSerialLot.Split("-")(1))
                                Next
                            Else
                                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Alert", "alert('Please select serial/Lot# for all serial/Lot item(s) added.' );", True)
                                Exit Sub
                            End If
                        Else
                            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Alert", "alert('Please select serial/Lot# for all serial/Lot item(s) added.' );", True)
                            Exit Sub
                        End If
                    End If
                Next

                If Not CCommon.ToBool(hdnConfirmClicked.Value) Then
                    lblConfirmation.Text = "You’re about to transfer quantities to the ""Transfer To"" warehouse(s).  Sure you want to do this?"
                    btnSaveCloseConfirmation.Text = "Transfer stock now"
                    divConfirmation.Style.Remove("display")
                    Exit Sub
                Else
                    Dim objItem As New CItems
                    objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                    objItem.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objItem.strItemCodes = dsTemp.GetXml()
                    objItem.MassStockTransfer()

                    Dim strScript As String = "self.close();"
                    If (Not Page.IsStartupScriptRegistered("clientScript")) Then ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "clientScript", strScript, True)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub

        Private Sub btnCancelConfirmation_Click(sender As Object, e As EventArgs) Handles btnCancelConfirmation.Click
            Try
                divConfirmation.Style.Add("display", "none")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub radcmbSTTo_SelectedIndexChanged(sender As Object, e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radcmbSTTo.SelectedIndexChanged
            Try
                Dim fillCombo As New COpportunities
                With fillCombo
                    .DivisionID = radcmbSTTo.SelectedValue
                    ddlToContact.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                    ddlToContact.DataTextField = "Name"
                    ddlToContact.DataValueField = "numcontactId"
                    ddlToContact.DataBind()
                End With
                ddlToContact.Items.Insert(0, New ListItem("-- Select One --", "0"))
                If ddlToContact.Items.Count >= 2 Then
                    ddlToContact.Items(1).Selected = True
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub

        Private Sub radcmbSTFrom_SelectedIndexChanged(sender As Object, e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radcmbSTFrom.SelectedIndexChanged
            Try
                Dim fillCombo As New COpportunities
                With fillCombo
                    .DivisionID = radcmbSTFrom.SelectedValue
                    ddlFromContact.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                    ddlFromContact.DataTextField = "Name"
                    ddlFromContact.DataValueField = "numcontactId"
                    ddlFromContact.DataBind()
                End With
                ddlFromContact.Items.Insert(0, New ListItem("-- Select One --", "0"))
                If ddlFromContact.Items.Count >= 2 Then
                    ddlFromContact.Items(1).Selected = True
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub

        Private Sub radFromLocation_ItemDataBound(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemEventArgs) Handles radFromLocation.ItemDataBound
            Try
                e.Item.Text = CType(e.Item.DataItem, DataRowView)("vcWareHouse").ToString() & " (" & CType(e.Item.DataItem, DataRowView)("numOnHand").ToString() & ") " & " ---- " & CType(e.Item.DataItem, DataRowView)("Attr").ToString().TrimEnd(",")
                e.Item.Value = CType(e.Item.DataItem, DataRowView)("numWareHouseItemId").ToString()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub radToLocation_ItemDataBound(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemEventArgs) Handles radToLocation.ItemDataBound
            Try
                e.Item.Text = CType(e.Item.DataItem, DataRowView)("vcWareHouse").ToString() & " (" & CType(e.Item.DataItem, DataRowView)("numOnHand").ToString() & ") " & " ---- " & CType(e.Item.DataItem, DataRowView)("Attr").ToString().TrimEnd(",")
                e.Item.Value = CType(e.Item.DataItem, DataRowView)("numWareHouseItemId").ToString()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub gvItems_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvItems.RowCommand
            Try
                dsTemp = ViewState("SOItems")

                If e.CommandName = "Delete" Then

                    Dim ctrl As Control = TryCast(e.CommandSource, Control)
                    Dim row As GridViewRow = TryCast(ctrl.Parent.NamingContainer, GridViewRow)
                    Dim hdnRowId As HiddenField = DirectCast(row.FindControl("hdnRowId"), HiddenField)

                    Dim dtItem As DataTable
                    dtItem = dsTemp.Tables(0)
                    dtItem.Rows.Find(hdnRowId.Value).Delete()
                    dsTemp.AcceptChanges()
                    ViewState("SOItems") = dsTemp

                    litMessage.Text = ""
                    clearControls()

                    

                    BindDataGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub

        Protected Sub txtUnitsGrid_TextChanged(sender As Object, e As EventArgs)
            Try
                Dim dr As DataRow
                dsTemp = ViewState("SOItems")

                Dim txtUnits As TextBox = CType(sender, TextBox)

                Dim dtItem As DataTable = dsTemp.Tables(0)

                Dim row As GridViewRow = TryCast(sender.NamingContainer, GridViewRow)

                Dim dRows As DataRow() = dtItem.Select("Id = '" & DirectCast(row.FindControl("hdnRowId"), HiddenField).Value & "'")
                If dRows.Length > 0 Then
                    dr = dRows(0)

                    If CCommon.ToInteger(txtUnits.Text) > dr("numOnHand") Then
                        BindDataGrid()

                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Validation", "alert('You can’t transfer more qty than you have on-hand.')", True)
                        Exit Sub
                    Else
                        dr("numUnitHour") = CCommon.ToInteger(txtUnits.Text)
                        dr.AcceptChanges()
                        dsTemp.AcceptChanges()

                        ViewState("SOItems") = dsTemp
                        hdnCurrentTextBox.Value = txtUnits.ClientID
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub gvItems_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvItems.RowDataBound
            Try
                If e.Row.RowType = DataControlRowType.DataRow Then
                    Dim txtUnits As TextBox = DirectCast(e.Row.FindControl("txtUnits"), TextBox)
                    Dim hplSerialLot As HyperLink = DirectCast(e.Row.FindControl("hplSerialLot"), HyperLink)

                    Dim rowView As DataRowView = CType(e.Row.DataItem, DataRowView)
                    If CCommon.ToBool(rowView("IsSerial")) Or CCommon.ToBool(rowView("IsLot")) Then
                        hplSerialLot.Visible = True
                        txtUnits.Visible = False
                        hplSerialLot.Attributes.Add("onclick", "OpenSertailLot(" & rowView("numFromItemCode") & "," & rowView("numFromWarehouseItemID") & ",'" & IIf(CCommon.ToBool(rowView("IsLot")), "Lot", "Serial") & "','" & rowView("Id").ToString & "')")

                        Dim qty As Integer = 0
                        Dim arrSerialLot As String() = CCommon.ToString(rowView("vcSelectedSerialLotNumbers")).Split(",")

                        If Not String.IsNullOrEmpty(CCommon.ToString(rowView("vcSelectedSerialLotNumbers"))) AndAlso Not arrSerialLot Is Nothing AndAlso arrSerialLot.Length > 0 Then
                            For Each objSerialLot As String In arrSerialLot
                                qty = qty + CCommon.ToInteger(objSerialLot.Split("-")(1))
                            Next

                            hplSerialLot.Text = "Added (" & qty & ")"
                        Else
                            hplSerialLot.Text = "Added (0)"
                        End If
                    Else
                        hplSerialLot.Visible = False
                        txtUnits.Visible = True
                    End If

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub gvItems_RowDeleting(sender As Object, e As GridViewDeleteEventArgs) Handles gvItems.RowDeleting

        End Sub

        Private Sub lkbSelectedSerialLot_Click(sender As Object, e As EventArgs) Handles lkbSelectedSerialLot.Click
            Try
                If hdnSelectedId.Value = "-1" Then
                    Dim qty As Integer = 0
                    Dim arrSerialLot As String() = CCommon.ToString(hdnSerialLotToTransfer.Value).Split(",")

                    If Not arrSerialLot Is Nothing AndAlso arrSerialLot.Length > 0 Then
                        For Each objSerialLot As String In arrSerialLot
                            qty = qty + CCommon.ToInteger(objSerialLot.Split("-")(1))
                        Next

                        txtUnitsToTransfer.Text = qty
                        hplSerialLotToTransfer.Text = "Added (" & qty & ")"
                    End If
                ElseIf Not String.IsNullOrEmpty(hdnSelectedSerialLot.Value) AndAlso Not String.IsNullOrEmpty(hdnSelectedId.Value) Then
                    Dim id As String = hdnSelectedId.Value

                    dsTemp = ViewState("SOItems")
                    If Not dsTemp Is Nothing AndAlso dsTemp.Tables.Count > 0 AndAlso dsTemp.Tables(0).Rows.Count > 0 Then
                        Dim arrRows As DataRow() = dsTemp.Tables(0).Select("Id='" & id & "'")

                        If Not arrRows Is Nothing AndAlso arrRows.Length > 0 Then
                            Dim qty As Integer = 0
                            Dim arrSerialLot As String() = CCommon.ToString(hdnSelectedSerialLot.Value).Split(",")

                            If Not arrSerialLot Is Nothing AndAlso arrSerialLot.Length > 0 Then
                                For Each objSerialLot As String In arrSerialLot
                                    qty = qty + CCommon.ToInteger(objSerialLot.Split("-")(1))
                                Next

                                If (qty > CCommon.ToInteger(arrRows(0)("numOnHand"))) Then
                                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Validation", "alert('You can’t transfer more qty than you have on-hand.')", True)
                                    Exit Sub
                                Else
                                    arrRows(0)("numUnitHour") = qty
                                    arrRows(0)("vcSelectedSerialLotNumbers") = hdnSelectedSerialLot.Value
                                    dsTemp.AcceptChanges()
                                    ViewState("SOItems") = dsTemp

                                    BindDataGrid()
                                End If
                            End If
                        End If
                    End If
                End If

                hdnSelectedSerialLot.Value = ""
                hdnSelectedId.Value = ""
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnEditCancel_Click(sender As Object, e As EventArgs) Handles btnEditCancel.Click
            Try
                clearControls()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
#End Region


        Private Class SelectedItems
            Public Property numItemCode As Long
            Public Property numWarehouseItemID As String
            Public Property bitHasKitAsChild As Boolean
            Public Property kitChildItems As String
            Public Property numSelectedUOM As Long
            Public Property vcSelectedUOM As String
            Public Property numQuantity As Long
            Public Property numToWarehouseItemID As String
            Public Property vcAttributes As String
            Public Property vcAttributeIDs As String
            Public Property numToItemCode As Long
        End Class

        Private Sub radFromLocation_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs) Handles radFromLocation.SelectedIndexChanged
            Try
                If Not chkTransferToItem.Checked Then
                    If objCommon Is Nothing Then objCommon = New CCommon
                    Dim dtWarehouse As DataTable = objCommon.GetWarehouseAttrBasedItem(hdnCurrentSelectedItem.Value)

                    If Not dtWarehouse Is Nothing AndAlso dtWarehouse.Rows.Count > 0 Then
                        If dtWarehouse.Select("numWarehouseItemId <> " & radFromLocation.SelectedValue).Length > 0 Then
                            radToLocation.Items.Clear()
                            radToLocation.DataSource = dtWarehouse.Select("numWarehouseItemId <> " & radFromLocation.SelectedValue).CopyToDataTable()
                            radToLocation.DataBind()
                        End If

                        If radToLocation.Items.Count > 0 Then
                            radcmbSTFrom.Items(0).Selected = True
                        End If
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub btnSaveCloseConfirmation_Click(sender As Object, e As EventArgs) Handles btnSaveCloseConfirmation.Click
            Try
                divConfirmation.Style.Add("display", "none")
                hdnConfirmClicked.Value = True

                If btnSaveCloseConfirmation.Text = "Transfer stock now" Then
                    btnTransferStock_Click(Nothing, Nothing)
                ElseIf btnSaveCloseConfirmation.Text = "Create Transfer PO" Then
                    btnTransferOrder_Click(Nothing, Nothing)
                Else
                    btnAdd_Click(Nothing, Nothing)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub btnTransferOrder_Click(sender As Object, e As EventArgs)
            Try
                dsTemp = ViewState("SOItems")

                If Not dsTemp Is Nothing AndAlso dsTemp.Tables.Count > 0 AndAlso dsTemp.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In dsTemp.Tables(0).Rows
                        If CCommon.ToLong(dr("numFromItemCode")) <> CCommon.ToDouble(dr("numToItemCode")) Then
                            lblException.Text = "You can’t create transfer orde when items with ""Transfer to another items"" option is used"
                            Exit Sub
                        End If
                    Next
                Else
                    lblException.Text = "Add atleast one item for stock transfer"
                    Exit Sub
                End If

                Dim objJournal As New JournalEntry
                If ChartOfAccounting.GetDefaultAccount("CG", Session("DomainID")) = 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Alert", "alert('Please Set Default COGs account from Administration->Global Settings->Accounting->Default Accounts Mapping To Save' );", True)
                    Exit Sub
                End If

                If ChartOfAccounting.GetDefaultAccount("PC", Session("DomainID")) = 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Alert", "alert('Please Set Default Purchase Clearing account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                    Exit Sub
                End If

                If ChartOfAccounting.GetDefaultAccount("PV", Session("DomainID")) = 0 Then
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Alert", "alert('Please Set Default Purchase Price Variance account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save' );", True)
                    Exit Sub
                End If


                For Each dr As DataRow In dsTemp.Tables(0).Rows
                    If CCommon.ToLong(dr("numFromWarehouseItemID")) = 0 Or CCommon.ToLong(dr("numToWarehouseItemID")) = 0 Then
                        ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Alert", "alert('From and To location Some item(s) have transfer qty more than ' );", True)
                        Exit Sub
                    End If

                    If CCommon.ToBool(dr("IsSerial")) Or CCommon.ToBool(dr("IsLot")) Then
                        If Not String.IsNullOrEmpty(CCommon.ToString(dr("vcSelectedSerialLotNumbers"))) Then
                            Dim qty As Integer = 0
                            Dim arrSerialLot As String() = CCommon.ToString(dr("vcSelectedSerialLotNumbers")).Split(",")

                            If Not String.IsNullOrEmpty(CCommon.ToString(dr("vcSelectedSerialLotNumbers"))) AndAlso Not arrSerialLot Is Nothing AndAlso arrSerialLot.Length > 0 Then
                                For Each objSerialLot As String In arrSerialLot
                                    qty = qty + CCommon.ToInteger(objSerialLot.Split("-")(1))
                                Next
                            Else
                                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Alert", "alert('Please select serial/Lot# for all serial/Lot item(s) added.' );", True)
                                Exit Sub
                            End If
                        Else
                            ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "Alert", "alert('Please select serial/Lot# for all serial/Lot item(s) added.' );", True)
                            Exit Sub
                        End If
                    End If
                Next

                If Not CCommon.ToBool(hdnConfirmClicked.Value) Then
                    lblConfirmation.Text = "This will create a 0.00 amt Purchase Order named ""Transfer Order"". Items will be transferred when they’re received. Sure you want to do this?"
                    btnSaveCloseConfirmation.Text = "Create Transfer PO"
                    divConfirmation.Style.Remove("display")
                    Exit Sub
                Else
                    Dim dsPOTemp As DataSet
                    Dim dtPOItem As DataTable
                    Dim dtSerItem As DataTable
                    Dim dtChildItems As DataTable

                    Dim view As DataView = New DataView(dsTemp.Tables(0))
                    Dim dtFromTo As DataTable = view.ToTable(True, "numFromWarehouseID", "numToWarehouseID")

                    Dim oCOpportunities As New COpportunities
                    oCOpportunities.DomainID = CType((Session("DomainID")), Long)
                    Dim dtAddress As DataTable = oCOpportunities.GetExistingAddress(CInt(Session("UserID")), CInt(lngDivId))

                    For Each drFromTo As DataRow In dtFromTo.Rows
                        dsPOTemp = New DataSet
                        dtPOItem = New DataTable
                        dtSerItem = New DataTable
                        dtChildItems = New DataTable

                        dtPOItem.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
                        dtPOItem.Columns.Add("numItemCode")
                        dtPOItem.Columns.Add("numItemClassification")
                        dtPOItem.Columns.Add("numUnitHour", GetType(Decimal))
                        dtPOItem.Columns.Add("monPrice", GetType(Decimal))
                        dtPOItem.Columns.Add("numUOM")
                        dtPOItem.Columns.Add("vcUOMName")
                        dtPOItem.Columns.Add("UOMConversionFactor")
                        dtPOItem.Columns.Add("monTotAmount", GetType(Decimal))
                        dtPOItem.Columns.Add("numSourceID")
                        dtPOItem.Columns.Add("vcItemDesc")
                        dtPOItem.Columns.Add("vcModelID")
                        dtPOItem.Columns.Add("numWarehouseID")
                        dtPOItem.Columns.Add("vcItemName")
                        dtPOItem.Columns.Add("Warehouse")
                        dtPOItem.Columns.Add("numWarehouseItmsID")
                        dtPOItem.Columns.Add("ItemType")
                        dtPOItem.Columns.Add("Attributes")
                        dtPOItem.Columns.Add("AttributeIDs")
                        dtPOItem.Columns.Add("Op_Flag")
                        dtPOItem.Columns.Add("bitWorkOrder")
                        dtPOItem.Columns.Add("numMaxWorkOrderQty")
                        dtPOItem.Columns.Add("vcInstruction")
                        dtPOItem.Columns.Add("bintCompliationDate")
                        dtPOItem.Columns.Add("numWOAssignedTo")
                        dtPOItem.Columns.Add("DropShip", GetType(Boolean))
                        dtPOItem.Columns.Add("bitDiscountType", GetType(Boolean))
                        dtPOItem.Columns.Add("fltDiscount", GetType(Decimal))
                        dtPOItem.Columns.Add("monTotAmtBefDiscount", GetType(Decimal))
                        dtPOItem.Columns.Add("bitTaxable0")
                        dtPOItem.Columns.Add("Tax0", GetType(Decimal))
                        dtPOItem.Columns.Add("TotalTax", GetType(Decimal))
                        dtPOItem.Columns.Add("numVendorWareHouse")
                        dtPOItem.Columns.Add("numShipmentMethod")
                        dtPOItem.Columns.Add("numSOVendorId")
                        dtPOItem.Columns.Add("numProjectID")
                        dtPOItem.Columns.Add("numProjectStageID")
                        dtPOItem.Columns.Add("charItemType")
                        dtPOItem.Columns.Add("numToWarehouseItemID")
                        dtPOItem.Columns.Add("bitIsAuthBizDoc", GetType(Boolean))
                        dtPOItem.Columns.Add("numUnitHourReceived")
                        dtPOItem.Columns.Add("numQtyShipped")
                        dtPOItem.Columns.Add("vcBaseUOMName")
                        dtPOItem.Columns.Add("numVendorID", System.Type.GetType("System.Int32"))
                        dtPOItem.Columns.Add("fltItemWeight")
                        dtPOItem.Columns.Add("IsFreeShipping")
                        dtPOItem.Columns.Add("fltHeight")
                        dtPOItem.Columns.Add("fltWidth")
                        dtPOItem.Columns.Add("fltLength")
                        dtPOItem.Columns.Add("vcSKU")
                        dtPOItem.Columns.Add("bitItemPriceApprovalRequired")
                        dtPOItem.Columns.Add("bitAddedFulFillmentBizDoc", GetType(Boolean))
                        dtPOItem.Columns.Add("bitHasKitAsChild", GetType(Boolean))
                        dtPOItem.Columns.Add("KitChildItems", GetType(String))
                        dtPOItem.Columns.Add("numPromotionID")
                        dtPOItem.Columns.Add("bitPromotionTriggered")
                        dtPOItem.Columns.Add("vcPromotionDetail")
                        dtPOItem.Columns.Add("numSortOrder")
                        dtPOItem.Columns.Add("numCost", GetType(Decimal))
                        dtPOItem.Columns.Add("vcVendorNotes")
                        dtPOItem.Columns.Add("CustomerPartNo")

                        dtSerItem.Columns.Add("numWarehouseItmsDTLID")
                        dtSerItem.Columns.Add("numWItmsID")
                        dtSerItem.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
                        dtSerItem.Columns.Add("vcSerialNo")
                        dtSerItem.Columns.Add("Comments")
                        dtSerItem.Columns.Add("Attributes")

                        dtChildItems.Columns.Add("numOppChildItemID", System.Type.GetType("System.Int32"))
                        dtChildItems.Columns.Add("numoppitemtCode", System.Type.GetType("System.Int32"))
                        dtChildItems.Columns.Add("numItemCode")
                        dtChildItems.Columns.Add("numQtyItemsReq")
                        dtChildItems.Columns.Add("vcItemName")
                        dtChildItems.Columns.Add("monListPrice")
                        dtChildItems.Columns.Add("UnitPrice")
                        dtChildItems.Columns.Add("charItemType")
                        dtChildItems.Columns.Add("txtItemDesc")
                        dtChildItems.Columns.Add("numWarehouseItmsID")
                        dtChildItems.Columns.Add("Op_Flag")
                        dtChildItems.Columns.Add("ItemType")
                        dtChildItems.Columns.Add("Attr")
                        dtChildItems.Columns.Add("vcWareHouse")

                        dtPOItem.TableName = "Item"
                        dtSerItem.TableName = "SerialNo"
                        dtChildItems.TableName = "ChildItems"


                        dsPOTemp.Tables.Add(dtPOItem)
                        dsPOTemp.Tables.Add(dtSerItem)
                        dsPOTemp.Tables.Add(dtChildItems)

                        Dim MaxRowOrder As Integer = 0
                        Dim objItem As New CItems
                        objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                        objItem.DivisionID = CCommon.ToLong(radcmbSTFrom.SelectedValue)
                        objItem.byteMode = 2

                        For Each dr As DataRow In dsTemp.Tables(0).Select("numFromWarehouseID=" & drFromTo("numFromWarehouseID") & " AND numToWarehouseID=" & drFromTo("numToWarehouseID"))
                            objItem.ItemCode = CCommon.ToLong(dr("numFromItemCode"))
                            objItem.WareHouseItemID = CCommon.ToLong(dr("numFromWarehouseItemID"))
                            objItem.StockTransferToWarehouseItemID = CCommon.ToLong(dr("numToWarehouseItemID"))
                            objItem.GetItemAndWarehouseDetails()
                            objItem.SelectedUOMID = objItem.BaseUnit
                            objItem.SelectedUOM = ""
                            objItem.NoofUnits = CCommon.ToDouble(dr("numUnitHour"))
                            MaxRowOrder = MaxRowOrder + 1
                            objItem.numSortOrder = MaxRowOrder

                            AddItemToSession(objItem, dsPOTemp)
                        Next

                        lngDivId = CCommon.ToLong(radcmbSTFrom.SelectedValue)
                        lngCntID = CCommon.ToLong(ddlFromContact.SelectedValue)

                        Dim arrOutPut() As String
                        Dim objOpportunity As New MOpportunity
                        objOpportunity.OpportunityId = 0
                        objOpportunity.ContactID = lngCntID
                        objOpportunity.DivisionID = lngDivId
                        objOpportunity.OpportunityName = radcmbSTFrom.Text & "-PO-" & Format(Now(), "MMMM")
                        objOpportunity.IsStockTransfer = True
                        objOpportunity.UserCntID = Session("UserContactID")
                        objOpportunity.EstimatedCloseDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                        objOpportunity.PublicFlag = 0
                        objOpportunity.DomainID = Session("DomainId")
                        objOpportunity.CouponCode = ""
                        objOpportunity.Discount = 0
                        objOpportunity.boolDiscType = False
                        objOpportunity.CurrencyID = Session("BaseCurrencyID")
                        objOpportunity.OppType = 2
                        objOpportunity.DealStatus = 1
                        objOpportunity.SourceType = 1

                        'Ship From Address
                        objItem = New CItems
                        Dim dtTable As DataTable
                        objItem.WarehouseID = CCommon.ToLong(drFromTo("numFromWarehouseID"))
                        objItem.DomainID = Session("DomainID")
                        dtTable = objItem.GetWareHouses

                        If Not dtTable Is Nothing AndAlso dtTable.Rows.Count > 0 AndAlso CCommon.ToLong(dtTable.Rows(0)("numAddressID")) > 0 Then
                            objOpportunity.BillToAddressID = CCommon.ToLong(dtTable.Rows(0)("numAddressID"))
                        ElseIf Not dtAddress Is Nothing AndAlso dtAddress.Rows.Count > 0 Then
                            objOpportunity.BillToAddressID = CCommon.ToLong(dtAddress.Rows(0)("numBillAddress"))
                        End If

                        'Ship To Address
                        objItem = New CItems
                        objItem.WarehouseID = CCommon.ToLong(drFromTo("numToWarehouseID"))
                        objItem.DomainID = Session("DomainID")
                        dtTable = objItem.GetWareHouses

                        If Not dtTable Is Nothing AndAlso dtTable.Rows.Count > 0 AndAlso CCommon.ToLong(dtTable.Rows(0)("numAddressID")) > 0 Then
                            objOpportunity.ShipToAddressID = CCommon.ToLong(dtTable.Rows(0)("numAddressID"))
                        ElseIf Not dtAddress Is Nothing AndAlso dtAddress.Rows.Count > 0 Then
                            objOpportunity.BillToAddressID = CCommon.ToLong(dtAddress.Rows(0)("numShipAddress"))
                        End If

                        Dim TotalAmount As Decimal = 0
                        If dsPOTemp.Tables(0).Rows.Count > 0 Then
                            TotalAmount = CType(dsPOTemp.Tables(0).Compute("Sum(monTotAmount)", ""), Decimal)
                            objOpportunity.strItems = "<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & dsPOTemp.GetXml
                        End If

                        Dim objAdmin As New CAdmin
                        Dim dtBillingTerms As DataTable
                        objAdmin.DivisionID = lngDivId
                        dtBillingTerms = objAdmin.GetBillingTerms()

                        objOpportunity.boolBillingTerms = IIf(dtBillingTerms.Rows(0).Item("tintBillingTerms") = 1, True, False)
                        objOpportunity.BillingDays = dtBillingTerms.Rows(0).Item("numBillingDays")
                        objOpportunity.boolInterestType = IIf(dtBillingTerms.Rows(0).Item("tintInterestType") = 1, True, False)
                        objOpportunity.Interest = dtBillingTerms.Rows(0).Item("fltInterest")
                        objOpportunity.intUsedShippingCompany = CCommon.ToLong(dtBillingTerms.Rows(0).Item("intShippingCompany"))
                        objOpportunity.ShippingService = CCommon.ToLong(dtBillingTerms.Rows(0).Item("numDefaultShippingServiceID"))
                        objOpportunity.VendorAddressID = 0

                        Dim objOpp As New MOpportunity
                        objOpp.DivisionID = lngDivId
                        objOpp.DomainID = objOpportunity.DomainID
                        objOpportunity.CampaignID = objOpp.GetCampaignForOrganization()
                        objOpportunity.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                        arrOutPut = objOpportunity.Save
                        lngOppId = arrOutPut(0)

                        'Set Bill To and Ship To based on domain details settings
                        If objOpportunity.BillToAddressID = 0 And objOpportunity.ShipToAddressID = 0 Then
                            objOpportunity.OpportunityId = lngOppId
                            objOpportunity.BillToType = IIf(Session("DefaultBillToForPO") = 1, 0, 0)
                            objOpportunity.ShipToType = IIf(Session("DefaultShipToForPO") = 1, 0, 0)
                            objOpportunity.UpdateOpportunityLinkingDtls()
                        End If

                        'stock transfer,update order name 
                        If objCommon Is Nothing Then objCommon = New CCommon()
                        objCommon.Mode = 19
                        objCommon.UpdateRecordID = lngOppId
                        objCommon.Comments = "Transfer Order - " & lngOppId.ToString()
                        objCommon.UpdateSingleFieldValue()
                    Next

                    Dim strScript As String = ""
                    If dtFromTo.Rows.Count = 1 Then
                        strScript += "window.opener.reDirectPage('../opportunity/frmOpportunities.aspx?opId=" & lngOppId & "'); self.close();"
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "OpenOpportunity", strScript, True)
                    Else
                        strScript += "alert('Stock transfer orders for different ship from and to locations are created successfully.'); self.close();"
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "OpenOpportunity", strScript, True)
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub AddItemToSession(objItem As CItems, ByRef dsPOTemp As DataSet)
            Try
                Dim units As Double
                Dim price As Decimal
                Dim discount As Decimal = 0
                Dim isDiscountInPer As Boolean = False

                units = CCommon.ToDouble(objItem.NoofUnits)
                price = 0

                objCommon = New CCommon
                objCommon.DomainID = CCommon.ToLong(Session("DomainID"))

                If objItem.numNoItemIntoContainer = -1 Then
                    ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon,
                                                                                   dsPOTemp,
                                                                                   True,
                                                                                   objItem.ItemType,
                                                                                   "",
                                                                                   objItem.AllowDropShip,
                                                                                   objItem.KitParent,
                                                                                   objItem.ItemCode,
                                                                                   units,
                                                                                   price,
                                                                                   objItem.ItemDesc, objItem.WareHouseItemID, objItem.ItemName, "", 0, objItem.ModelID, CCommon.ToLong(objItem.SelectedUOMID), If(CCommon.ToLong(objItem.SelectedUOMID) > 0, objItem.SelectedUOM, "-"),
                                                                                   If(CCommon.ToDecimal(0) = 0, 1, CCommon.ToDecimal(0)),
                                                                                   2, isDiscountInPer, discount, "", "", "", Nothing,
                                                                                   0, "", CCommon.ToString(""),
                                                                                   CCommon.ToLong(0),
                                                                                   0,
                                                                                   numShipmentMethod:=0,
                                                                                   numProjectID:=CCommon.ToLong(GetQueryStringVal("Source")),
                                                                                   numProjectStageID:=CCommon.ToLong(GetQueryStringVal("StageID")),
                                                                                   ToWarehouseItemID:=CCommon.ToLong(objItem.StockTransferToWarehouseItemID),
                                                                                   vcBaseUOMName:=objItem.SelectedUOM,
                                                                                   numSOVendorId:=CCommon.ToLong(0), strSKU:=objItem.SKU,
                                                                                   objItem:=objItem, vcAttributes:=objItem.Attributes, vcAttributeIDs:=objItem.AttributeIDs, numContainer:=objItem.numContainer, numContainerQty:=objItem.numNoItemIntoContainer,
                                                                                   numItemClassification:=CCommon.ToLong(0),
                                                                                   numPromotionID:=CCommon.ToLong(0),
                                                                                   IsPromotionTriggered:=CCommon.ToBool(0),
                                                                                   vcPromotionDetail:=CCommon.ToString(0), numSortOrder:=objItem.numSortOrder
                                                                                   )

                Else
                    ViewState("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon,
                                                                                   dsPOTemp,
                                                                                   True,
                                                                                   objItem.ItemType,
                                                                                   "",
                                                                                   False,
                                                                                   objItem.KitParent,
                                                                                   objItem.ItemCode,
                                                                                   units,
                                                                                   price,
                                                                                   objItem.ItemDesc, objItem.WareHouseItemID, objItem.ItemName, "", 0, objItem.ModelID, CCommon.ToLong(objItem.BaseUnit), "-",
                                                                                   1,
                                                                                  2, isDiscountInPer, discount, "", "", "", Nothing,
                                                                                   0, "", "",
                                                                                  0,
                                                                                   0,
                                                                                   numShipmentMethod:=0,
                                                                                   numProjectID:=CCommon.ToLong(GetQueryStringVal("Source")),
                                                                                   numProjectStageID:=CCommon.ToLong(GetQueryStringVal("StageID")),
                                                                                   ToWarehouseItemID:=CCommon.ToLong(objItem.StockTransferToWarehouseItemID),
                                                                                   strSKU:=objItem.SKU,
                                                                                   objItem:=objItem, vcAttributes:=objItem.Attributes, vcAttributeIDs:=objItem.AttributeIDs, numContainer:=objItem.numContainer, numContainerQty:=objItem.numNoItemIntoContainer,
                                                                                  numSortOrder:=objItem.numSortOrder
                                                                                   )

                End If


                objCommon = Nothing

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
    End Class
End Namespace