<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmOppLoadItems.aspx.vb"
    Inherits=".frmOppLoadItems" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<%@ Register Assembly="RadComboBox.Net2" Namespace="Telerik.WebControls" TagPrefix="rad" %>--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <%--   <rad:RadComboBox ID="radCmbtem" OnClientItemsRequesting="GetSelectedItem" Width="195px"
        DropDownWidth="500px" Skin="WindowsXP" runat="server" AutoPostBack="True" AllowCustomText="True"
        EnableLoadOnDemand="True">
    </rad:RadComboBox>
    --%>
    <telerik:RadComboBox runat="server" AccessKey="I" ID="radCmbItem" OnClientItemsRequesting="OnClientItemsRequesting"
        AllowCustomText="true" AutoPostBack="true" EnableLoadOnDemand="true" Skin="Default"
        Width="195px" DropDownWidth="500px">
    </telerik:RadComboBox>
    </form>
</body>
</html>
