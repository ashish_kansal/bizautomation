﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin

Public Class frmOpenRelatedItems
    Inherits BACRMPage
    Dim lngItemCode As Long
    Dim OppType As Integer
    Dim PageType As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngItemCode = GetQueryStringVal( "ItemCode")
            OppType = GetQueryStringVal( "OppType")
            PageType = GetQueryStringVal( "PageType")
            If Not IsPostBack Then
                If lngItemCode > 0 Then
                    bindGrid()

                    If PageType = "Sales" Then
                        Dim strTax As String = ""
                        
                        Dim dtTaxTypes As DataTable
                        Dim ObjTaxItems As New TaxDetails
                        ObjTaxItems.DomainId = Session("DomainID")
                        dtTaxTypes = ObjTaxItems.GetTaxItems
                        Dim dr As DataRow

                        For Each dr In dtTaxTypes.Rows
                            'Base tax calculation on Shipping Address(2) or Billing Address(1) 
                            strTax = strTax & objCommon.TaxPercentage(GetQueryStringVal( "DivId"), 0, 0, Session("DomainID"), dr("numTaxItemID"), "", "", Session("BaseTaxOnArea"), Session("BaseTaxCalcOn")) & ","
                        Next
                        strTax = strTax & objCommon.TaxPercentage(GetQueryStringVal( "DivId"), 0, 0, Session("DomainID"), 0, "", "", Session("BaseTaxOnArea"), Session("BaseTaxCalcOn"))
                        txtTax.Text = strTax

                        dr = dtTaxTypes.NewRow
                        dr("numTaxItemID") = 0
                        dr("vcTaxName") = "Sales Tax(Default)"
                        dtTaxTypes.Rows.Add(dr)

                        chkTaxItems.DataTextField = "vcTaxName"
                        chkTaxItems.DataValueField = "numTaxItemID"
                        chkTaxItems.DataSource = dtTaxTypes
                        chkTaxItems.DataBind()
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub bindGrid()
        Try
            Dim objItems As New CItems
            objItems.DomainID = CCommon.ToLong(Session("DomainID"))
            objItems.ParentItemCode = lngItemCode
            objItems.byteMode = 2
            gvRelatedItems.DataSource = objItems.GetSimilarItem
            gvRelatedItems.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub AddToCart(ByVal ItemCode As Long)
        Try
            Dim dtTable, dtItemTax As DataTable
            Dim numUOMId As Long = 0, vcUOMName As String
            Dim objItems As New CItems
            objItems.ItemCode = ItemCode
            dtTable = objItems.ItemDetails
            Dim lngWareHouse As Long = 0, decUnitCost As Decimal = 0, UOMConversionFactor As Decimal = 0
            Dim vcWareHouse As String = ""

            If dtTable.Rows.Count > 0 Then
                If (OppType = 2) Then
                    numUOMId = dtTable.Rows(0).Item("numPurchaseUnit")
                    vcUOMName = dtTable.Rows(0).Item("vcPurchaseUnit")
                ElseIf (OppType = 1) Then
                    numUOMId = dtTable.Rows(0).Item("numSaleUnit")
                    vcUOMName = dtTable.Rows(0).Item("vcSaleUnit")
                End If



                If dtTable.Rows(0).Item("charItemType") = "P" Then
                    Dim dtWareHouse As DataTable = objCommon.GetWarehouseAttrBasedItem(ItemCode)
                    If dtWareHouse.Rows.Count > 0 Then
                        lngWareHouse = dtWareHouse(0)("numWareHouseItemId")
                        vcWareHouse = dtWareHouse(0)("vcWareHouse")
                    End If
                End If

                'UOM Conversion Factor
                Dim dtUnit As DataTable
                objCommon.DomainID = Session("DomainID")
                objCommon.ItemCode = ItemCode
                objCommon.UnitId = numUOMId
                dtUnit = objCommon.GetItemUOMConversion()
                UOMConversionFactor = dtUnit(0)("BaseUOMConversion")

                'Unit Cost Calculation 
                Dim ds As DataSet
                Dim dtItemPricemgmt As DataTable

                objItems.NoofUnits = 1 * UOMConversionFactor
                objItems.OppId = GetQueryStringVal("opid")
                objItems.DivisionID = GetQueryStringVal("DivId")
                objItems.DomainID = Session("DomainID")
                objItems.byteMode = OppType
                objItems.Amount = 0
                objItems.WareHouseItemID = IIf(lngWareHouse > 0, lngWareHouse, Nothing)

                ds = objItems.GetPriceManagementList1
                dtItemPricemgmt = ds.Tables(0)

                If dtItemPricemgmt.Rows.Count = 0 Then
                    dtItemPricemgmt = ds.Tables(2)
                End If

                dtItemPricemgmt.Merge(ds.Tables(1))

                If dtItemPricemgmt.Rows.Count > 0 Then
                    decUnitCost = CCommon.ToDecimal(dtItemPricemgmt.Rows(0)("ListPrice"))
                End If


                Dim dsTemp As DataSet = Session("SOItems")

                Dim strApplicable As String = ""

                If PageType = "Sales" Then
                    objItems.DomainID = Session("DomainID")
                    dtItemTax = objItems.ItemTax()

                    For Each dr As DataRow In dtItemTax.Rows
                        strApplicable = strApplicable & dr("bitApplicable") & ","
                    Next

                    strApplicable = strApplicable & dtTable.Rows(0).Item("bitTaxable")
                End If

                Session("SOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon, dsTemp, True, dtTable.Rows(0).Item("charItemType"), dtTable.Rows(0).Item("ItemType"),
                                                                             False, "", ItemCode, 1,
                                                                             Math.Abs(decUnitCost), dtTable.Rows(0).Item("txtItemDesc"), lngWareHouse,
                                                                             dtTable.Rows(0).Item("vcItemName"), vcWareHouse, 0, CCommon.ToString(dtTable.Rows(0).Item("vcModelID")), numUOMId, vcUOMName, UOMConversionFactor,
                                                                              PageType, False, 0,
                                                                             strApplicable, txtTax.Text, "", chkTaxItems, bDirectAdd:=True)

                Response.Write("<script language='javascript'>opener.BindOrderGrid();</script>")
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub gvRelatedItems_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvRelatedItems.RowCommand
        Try
            If e.CommandName = "Add" Then
                AddToCart(e.CommandArgument)

                'Response.Write("<script>opener.location.reload(true);window.close();</script>")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class