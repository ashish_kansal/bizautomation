<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false"  CodeBehind="frmSelectVendor.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmSelectVendor" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1"    runat="server">		
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Select Vendor</title>
		<script type="text/javascript" language="javascript" >
		    function Close()
		    {
		        window.close()
		        return false;
		    }
		    function SelectVen(ItemCode,DivID,Cost,ItemName,CompName,txtUnit)
		    {
		        window.opener.SelectVendor(ItemCode,DivID,Cost,ItemName,CompName,document.getElementById('txtUnit').value)
		        window.close();
		        return false;
		    }
		</script>
	</head>
	<body>
		<form id="Form1" method="post" runat="server">
		<br />
		<table width="100%" >
		    <tr>
		        <td align="right">
        		 <asp:Button id="btnClose" runat="server" CssClass ="button" Width="50" Text="Back"/>
		        </td>
		    </tr>
		     <tr>
		        <td>
        		<asp:datagrid id="dgVendor" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
				>
				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
				<Columns>
					<asp:BoundColumn Visible="False" DataField="numVendorID"></asp:BoundColumn>
					<asp:BoundColumn Visible="False" DataField="numCompanyID"></asp:BoundColumn>
					<asp:BoundColumn Visible="False" DataField="numItemCode"></asp:BoundColumn>
					<asp:BoundColumn DataField="Vendor" HeaderText="Vendor Name, Division"></asp:BoundColumn>
					<asp:BoundColumn DataField="vcPartNo" HeaderText="Part #"></asp:BoundColumn>
					<asp:BoundColumn DataField="ConName" HeaderText="Primary Contact First, Last Name"></asp:BoundColumn>
					<asp:BoundColumn DataField="Phone" HeaderText="Phone Number, Ext"></asp:BoundColumn>
					<asp:BoundColumn DataField="vcItemName" HeaderText="Item"></asp:BoundColumn>
					<asp:BoundColumn DataField="monCost" HeaderText="Cost/Unit" DataFormatString="{0:#,##0.00}"></asp:BoundColumn>
					<asp:TemplateColumn HeaderText="Units"> 
						<ItemTemplate><asp:TextBox ID="txtUnits" runat="server" CssClass="signup" Width="60"></asp:TextBox>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:TemplateColumn>
						<ItemTemplate>
							<asp:Button ID="btnSelect" Runat="server" CssClass="button" Text="Select" Width="60"></asp:Button>
						</ItemTemplate>
					</asp:TemplateColumn>
				</Columns>
			</asp:datagrid>
		        </td>
		    </tr>
		</table>
			
		</form>
	</body>
</HTML>
