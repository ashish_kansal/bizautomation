﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Workflow
Imports BACRM.BusinessLogic.DemandForecast
Imports Telerik.Web.UI
Imports System.Collections.Generic


Partial Public Class frmPlangProcurement
    Inherits BACRMPage

#Region "Member Variables"

    Private objOppContact As COpportunities
    Private objDemandForecast As DemandForecast
    Dim RegularSearch As String
    Dim CustomSearch As String

#End Region

#Region "Constructor"

    Sub New()
        objDemandForecast = New DemandForecast
        objOppContact = New COpportunities
    End Sub

#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            If Not Page.IsPostBack Then
                BindAnalysisPattern()

                PersistTable.Load()
                If PersistTable.Count > 0 Then
                    txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                    chkHistoricalValues.Checked = CCommon.ToBool(PersistTable("chkHistoricalValues"))
                    chkIncludeOpportunity.Checked = CCommon.ToBool(PersistTable("chkIncludeOpportunity"))
                    chkBasedOnLastYear.Checked = CCommon.ToLong(PersistTable("chkBasedOnLastYear"))
                    ddlOpportunityPercentComplete.SelectedValue = CCommon.ToLong(PersistTable("ddlOpportunityPercentComplete"))
                    ddlDemandPlanBasedOn.SelectedValue = CCommon.ToLong(PersistTable("ddlDemandPlanBasedOn"))
                End If

                If CCommon.ToLong(GetQueryStringVal("DFID")) > 0 Then
                    hdnDFID.Value = CCommon.ToString(GetQueryStringVal("DFID"))
                ElseIf Not String.IsNullOrEmpty(CCommon.ToString(GetQueryStringVal("ItemIds"))) Then
                    hdnItemIds.Value = CCommon.ToString(GetQueryStringVal("ItemIds"))
                End If

                hplConfigure.Attributes.Add("onclick", "return OpenDemandPlanWindow(" & CCommon.ToString(GetQueryStringVal("DFID")) & ")")

                If CCommon.ToShort(GetQueryStringVal("PlanType")) = 2 Then
                    btnLoadNewOrder.Text = "Load Items to Build"
                    If Not ddlDemandPlanBasedOn.Items.FindByValue("1") Is Nothing Then
                        ddlDemandPlanBasedOn.Items.FindByValue("1").Enabled = False
                    End If
                    If Not ddlDemandPlanBasedOn.Items.FindByValue("2") Is Nothing Then
                        ddlDemandPlanBasedOn.Items.FindByValue("2").Selected = True
                    End If

                    rbBuildPlan.Checked = True
                Else
                    btnLoadNewOrder.Text = "Load Items to Purchase"
                    If Not ddlDemandPlanBasedOn.Items.FindByValue("1") Is Nothing Then
                        ddlDemandPlanBasedOn.Items.FindByValue("1").Enabled = True
                    End If
                    rbPurchasePlan.Checked = True
                End If

                ExecuteDemandPlan()
            Else
                ExecuteDemandPlan()
            End If

            GetUserRightsForPage(10, 17)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                Response.Redirect("../admin/authentication.aspx?mesg=AC")
            Else
                ' If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then btnSave.Visible = False
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub ExecuteDemandPlan()
        Try
            Dim dtDemandForecast As DataTable

            If CCommon.ToLong(hdnDFID.Value) > 0 Then
                objDemandForecast.numDFID = CCommon.ToLong(hdnDFID.Value)
                objDemandForecast.SelectedIDs = ""
            Else
                objDemandForecast.numDFID = 0
                objDemandForecast.SelectedIDs = CCommon.ToString(hdnItemIds.Value)
            End If

            objDemandForecast.UserCntID = CCommon.ToLong(Session("UserContactID"))
            objDemandForecast.PageSize = 50
            objDemandForecast.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            objDemandForecast.CurrentPage = IIf(String.IsNullOrEmpty(txtCurrrentPage.Text.Trim()) Or txtCurrrentPage.Text.Trim() = "0", 1, CCommon.ToInteger(txtCurrrentPage.Text.Trim()))
            objDemandForecast.TotalRecords = 0
            objDemandForecast.DomainID = Session("DomainID")
            objDemandForecast.DemandPlanBasedOn = CCommon.ToShort(ddlDemandPlanBasedOn.SelectedValue)
            objDemandForecast.PlanType = IIf(rbBuildPlan.Checked, 2, 1)
            objDemandForecast.ShowAllItems = False
            objDemandForecast.ShowHistoricalSales = chkHistoricalValues.Checked
            objDemandForecast.AnalysisPattern = ddlAnalysisPatternLastWeek.SelectedValue
            objDemandForecast.IsBasedOnLastYear = chkBasedOnLastYear.Checked
            objDemandForecast.IsIncludeOpportunity = chkIncludeOpportunity.Checked
            objDemandForecast.OpportunityPercentComplete = ddlOpportunityPercentComplete.SelectedValue

            If txtSortColumn.Text <> "" Then
                objDemandForecast.columnName = txtSortColumn.Text
                If txtSortOrder.Text = "D" Then
                    objDemandForecast.columnSortOrder = "Desc"
                Else
                    objDemandForecast.columnSortOrder = "Asc"
                End If
            Else
                objDemandForecast.columnName = ""
                objDemandForecast.columnSortOrder = "Desc"
            End If

            GridColumnSearchCriteria()

            objDemandForecast.RegularSearchCriteria = RegularSearch

            Dim ds As DataSet

            ds = objDemandForecast.Execute()
            dtDemandForecast = ds.Tables(0)

            Dim dtTableInfo As DataTable
            dtTableInfo = ds.Tables(1)

            For i = 0 To dtDemandForecast.Columns.Count - 1
                dtDemandForecast.Columns(i).ColumnName = dtDemandForecast.Columns(i).ColumnName.Replace(".", "")
            Next

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                bizPager.PageSize = 50
                'bizPager.PageSize = CCommon.ToInteger(Session("PagingRows"))
                bizPager.RecordCount = objDemandForecast.TotalRecords

                If CCommon.ToInteger(txtCurrrentPage.Text) > 0 Then
                    bizPager.CurrentPageIndex = txtCurrrentPage.Text
                Else
                    If bizPager.RecordCount = 0 Then
                        bizPager.CustomInfoHTML = "Showing records 0 to 0 of 0"
                    End If
                End If

                PersistTable.Clear()
                PersistTable.Add(PersistKey.CurrentPage, IIf(ds.Tables(0).Rows.Count > 0, txtCurrrentPage.Text, "0"))
                PersistTable.Add("chkHistoricalValues", chkHistoricalValues.Checked)
                PersistTable.Add("chkIncludeOpportunity", chkIncludeOpportunity.Checked)
                PersistTable.Add("chkBasedOnLastYear", chkBasedOnLastYear.Checked)
                PersistTable.Add("ddlOpportunityPercentComplete", ddlOpportunityPercentComplete.SelectedValue)
                PersistTable.Add("ddlDemandPlanBasedOn", ddlDemandPlanBasedOn.SelectedValue)
                PersistTable.Save()

                If Not ds.Tables(0) Is Nothing Then
                    Dim bField As BoundField
                    Dim Tfield As TemplateField

                    Dim htGridColumnSearch As New Hashtable

                    If txtGridColumnFilter.Text.Trim.Length > 0 Then
                        Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")
                        Dim strIDValue() As String

                        For i = 0 To strValues.Length - 1
                            strIDValue = strValues(i).Split(":")

                            htGridColumnSearch.Add(strIDValue(0), strIDValue(1))
                        Next
                    End If

                    gvSearch.Columns.Clear()

                    For Each drRow As DataRow In dtTableInfo.Rows
                        Tfield = New TemplateField

                        Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, drRow, m_aryRightsForPage(RIGHTSTYPE.UPDATE), htGridColumnSearch, 139, objDemandForecast.columnName, objDemandForecast.columnSortOrder)
                        Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, drRow, m_aryRightsForPage(RIGHTSTYPE.UPDATE), htGridColumnSearch, 139, objDemandForecast.columnName, objDemandForecast.columnSortOrder)

                        gvSearch.Columns.Add(Tfield)
                    Next

                    Dim dr As DataRow
                    dr = dtTableInfo.NewRow()
                    dr("vcAssociatedControlType") = "DeleteCheckBox"
                    dr("intColumnWidth") = "30"

                    Tfield = New TemplateField
                    Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, dr, m_aryRightsForPage(RIGHTSTYPE.UPDATE), htGridColumnSearch, 139)
                    Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, dr, m_aryRightsForPage(RIGHTSTYPE.UPDATE), htGridColumnSearch, 139)
                    gvSearch.Columns.Add(Tfield)

                    Dim dvDF As DataView = dtDemandForecast.DefaultView


                    gvSearch.DataSource = dvDF
                    gvSearch.DataBind()
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub GridColumnSearchCriteria()
        Try
            If txtGridColumnFilter.Text.Trim.Length > 0 Then
                Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")

                Dim strIDValue(), strID(), strCustom As String
                Dim strRegularCondition As New ArrayList
                '  Dim strCustomCondition As New ArrayList

                For i As Integer = 0 To strValues.Length - 1
                    strIDValue = strValues(i).Split(":")
                    strID = strIDValue(0).Split("~")

                    If strID(0).Contains("CFW.Cust") Then

                    ElseIf strID(0) = "OBD.vcBizDocsList" Then
                        strRegularCondition.Add("(SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId = Opp.numOppId AND vcBizDocID IN ('" & strIDValue(1).Replace("'", "").Replace(",", "','") & "')) > 0")
                    ElseIf strID(0) = "Opp.tintInvoicingTextbox" Then
                        strRegularCondition.Add("(SELECT COUNT(*) FROM OpportunityBizDocs WHERE numOppId = Opp.numOppId AND vcBizDocID IN ('" & strIDValue(1).Replace("'", "").Replace(",", "','") & "')) > 0")
                    ElseIf strID(0) = "OI.vcInventoryStatus" Then
                        strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                    Else
                        Select Case strID(3).Trim()
                            Case "Website", "Email", "TextBox", "Label"
                                strRegularCondition.Add(strID(0) & " ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                            Case "SelectBox"
                                If strID(0) = "V.numVendorID" Or strID(0) = ".ShipmentMethod" Or strID(0) = "V.monCost" Then
                                    'strRegularCondition.Add("(SELECT vcCompanyName from DivisionMaster INNER JOIN CompanyInfo ON DivisionMaster.numCompanyID = CompanyInfo.numCompanyId WHERE DivisionMaster.numDivisionID = Item.numVendorID " & " AND vcCompanyName Like '%" & strIDValue(1).Replace("'", "''") & "%'" & ")")
                                Else
                                    strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                                End If

                            Case "TextArea"
                                strRegularCondition.Add(" Cast(" & strID(0) & " as varchar(5000)) ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                            Case "DateField"
                                If strID(4) = "From" Then
                                    Dim fromDate As Date
                                    If Date.TryParse(strIDValue(1), fromDate) Then
                                        strRegularCondition.Add(strID(0) & " >= '" & fromDate.ToString("MM/dd/yyyy") & "'")
                                    End If
                                ElseIf strID(4) = "To" Then
                                    Dim toDate As Date
                                    If Date.TryParse(strIDValue(1), toDate) Then
                                        strRegularCondition.Add(strID(0) & " <= '" & toDate.ToString("MM/dd/yyyy") & "'")
                                    End If
                                End If
                        End Select
                    End If
                Next
                RegularSearch = String.Join(" and ", strRegularCondition.ToArray())
                '  CustomSearch = String.Join(" and ", strCustomCondition.ToArray())
            Else
                RegularSearch = ""
                '   CustomSearch = ""
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub BindAnalysisPattern()
        Try
            Dim objDemandForecast As New DemandForecast
            objDemandForecast.DomainID = Session("DomainID")
            Dim dtAnalysisPattern As DataTable = objDemandForecast.GetAnalysisPattern()

            If Not dtAnalysisPattern Is Nothing AndAlso dtAnalysisPattern.Rows.Count > 0 Then
                ddlAnalysisPatternLastWeek.DataSource = dtAnalysisPattern
                ddlAnalysisPatternLastWeek.DataTextField = "vcAnalysisPattern"
                ddlAnalysisPatternLastWeek.DataValueField = "numDFAPID"
                ddlAnalysisPatternLastWeek.DataBind()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch exception As Exception

        End Try
    End Sub

    Private Function GetVendorPrice(ByVal numVendorID As Long, ByVal numItemCode As Long, ByVal numWarehouseItemID As Long, ByVal numQtyToPurchase As Decimal) As DataSet
        Try
            objDemandForecast.DomainID = Session("DomainID")
            Return objDemandForecast.GetVendorDetail(numVendorID, numItemCode, numWarehouseItemID, numQtyToPurchase)
        Catch ex As Exception
            Throw
        End Try
    End Function

#End Region

#Region "Event Handlers"

    Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
        Try
            txtCurrrentPage.Text = bizPager.CurrentPageIndex
            ExecuteDemandPlan()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

    Private Sub btnLoadNewOrder_Click(sender As Object, e As EventArgs) Handles btnLoadNewOrder.Click
        Try
            Dim qty As Double
            Dim listReleaseItems As New List(Of OpportunityItemsReleaseDates)
            Dim isItemSelected As Boolean = False
            Dim i As Int32 = 1
            If hdnSelectedItems.Value <> "" Then
                Dim listItemsToPurchase As List(Of OpportunityItemsReleaseDates) = Newtonsoft.Json.JsonConvert.DeserializeObject(Of List(Of OpportunityItemsReleaseDates))(hdnSelectedItems.Value)

                For Each objOpportunityItemsReleaseDates In listItemsToPurchase
                    qty = objOpportunityItemsReleaseDates.Quantity

                    If Not String.IsNullOrEmpty(objOpportunityItemsReleaseDates.ReleaseDates) Then
                        Dim arrReleasrDateQty As String() = objOpportunityItemsReleaseDates.ReleaseDates.Split(",")

                        Dim listRequiredDates As New List(Of ReleaseDates)
                        Dim objReleaseDates As ReleaseDates

                        For Each vcReleasrDateQty As String In arrReleasrDateQty
                            objReleaseDates = New ReleaseDates()
                            objReleaseDates.ReleaseDate = vcReleasrDateQty.Substring(0, vcReleasrDateQty.IndexOf("(") - 1)
                            objReleaseDates.Quantity = CCommon.ToDouble(vcReleasrDateQty.Substring(vcReleasrDateQty.IndexOf("(") + 1, vcReleasrDateQty.Length - vcReleasrDateQty.IndexOf("(") - 2))
                            objReleaseDates.IsSelected = True
                            listRequiredDates.Add(objReleaseDates)
                        Next

                        Dim js As New Script.Serialization.JavaScriptSerializer()
                        objOpportunityItemsReleaseDates.RequiredDates = js.Serialize(listRequiredDates)
                    End If

                    If listReleaseItems.Where(Function(x) x.ItemCode = objOpportunityItemsReleaseDates.ItemCode AndAlso x.WarehouseItemID = objOpportunityItemsReleaseDates.WarehouseItemID AndAlso x.VendorID = objOpportunityItemsReleaseDates.VendorID AndAlso x.ReleaseDate = objOpportunityItemsReleaseDates.ReleaseDate).Count > 0 Then
                        objOpportunityItemsReleaseDates = listReleaseItems.Where(Function(x) x.ItemCode = objOpportunityItemsReleaseDates.ItemCode AndAlso x.WarehouseItemID = objOpportunityItemsReleaseDates.WarehouseItemID AndAlso x.VendorID = objOpportunityItemsReleaseDates.VendorID AndAlso x.ReleaseDate = objOpportunityItemsReleaseDates.ReleaseDate).First()
                        objOpportunityItemsReleaseDates.Quantity = objOpportunityItemsReleaseDates.Quantity + qty
                    Else
                        listReleaseItems.Add(objOpportunityItemsReleaseDates)
                    End If
                Next

                For Each objItem As OpportunityItemsReleaseDates In listReleaseItems
                    objItem.Quantity = Math.Ceiling(objItem.Quantity)
                Next

                Session("DFItemsToPurchase") = listReleaseItems

                If rbBuildPlan.Checked Then
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "OpenDemandForecastBuildWindow", "OpenDemandForecastBuildWindow();", True)
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "OpenDemandForecastPOWindow", "OpenDemandForecastPOWindow();", True)
                End If
            Else
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "clientScript", "alert('Please select atleast one item with purchase quantity more than 0.');", True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

    Protected Sub gvItems_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dataView As DataRowView = CType(e.Row.DataItem, DataRowView)

                Dim hfUOMName As HiddenField = DirectCast(e.Row.FindControl("hfUOMName"), HiddenField)
                Dim hfItemCode As HiddenField = DirectCast(e.Row.FindControl("hfItemCode"), HiddenField)
                Dim hfItemName As HiddenField = DirectCast(e.Row.FindControl("hfItemName"), HiddenField)
                Dim hfAssetAccountID As HiddenField = DirectCast(e.Row.FindControl("hfAssetAccountID"), HiddenField)
                Dim hfWarehouseID As HiddenField = DirectCast(e.Row.FindControl("hfWarehouseID"), HiddenField)
                Dim hfWarehouseItemID As HiddenField = DirectCast(e.Row.FindControl("hfWarehouseItemID"), HiddenField)
                Dim hfBusinessProcess As HiddenField = DirectCast(e.Row.FindControl("hfBusinessProcess"), HiddenField)
                Dim hfBusinessProcessID As HiddenField = DirectCast(e.Row.FindControl("hfBusinessProcessID"), HiddenField)
                Dim hfBuildManager As HiddenField = DirectCast(e.Row.FindControl("hfBuildManager"), HiddenField)
                Dim hfBuildManagerID As HiddenField = DirectCast(e.Row.FindControl("hfBuildManagerID"), HiddenField)
                Dim hfAverageCost As HiddenField = DirectCast(e.Row.FindControl("hfAverageCost"), HiddenField)
                Dim hfVendorID As HiddenField = DirectCast(e.Row.FindControl("hfVendorID"), HiddenField)
                Dim hfItemType As HiddenField = DirectCast(e.Row.FindControl("hfItemType"), HiddenField)
                Dim hfContactID As HiddenField = DirectCast(e.Row.FindControl("hfContactID"), HiddenField)
                Dim hfUOMID As HiddenField = DirectCast(e.Row.FindControl("hfUOMID"), HiddenField)
                Dim hfQtyToPurchase As HiddenField = DirectCast(e.Row.FindControl("hfQtyToPurchase"), HiddenField)
                Dim hfUOMConversionfactor As HiddenField = DirectCast(e.Row.FindControl("hfUOMConversionfactor"), HiddenField)
                Dim hfSKU As HiddenField = DirectCast(e.Row.FindControl("hfSKU"), HiddenField)
                Dim hfAttributes As HiddenField = DirectCast(e.Row.FindControl("hfAttributes"), HiddenField)
                Dim hfWarehouse As HiddenField = DirectCast(e.Row.FindControl("hfWarehouse"), HiddenField)
                Dim hfReleaseDate As HiddenField = DirectCast(e.Row.FindControl("hfReleaseDate"), HiddenField)
                Dim lblLeadDays As Label = DirectCast(e.Row.FindControl("lblLeadDays"), Label)
                Dim lblExpectedDelivery As Label = DirectCast(e.Row.FindControl("lblExpectedDelivery"), Label)
                Dim txtmonCost As TextBox = DirectCast(e.Row.FindControl("txtmonCost"), TextBox)
                Dim txtnumQtyToPurchase As TextBox = DirectCast(e.Row.FindControl("txtnumQtyToPurchase"), TextBox)
                Dim hdnQtyToBuyBasedOnPurchasePlan As HiddenField = DirectCast(e.Row.FindControl("hdnQtyToBuyBasedOnPurchasePlan"), HiddenField)

                hfUOMName.Value = dataView("vcUOM")
                hfItemCode.Value = dataView("numItemCode")
                hfItemName.Value = dataView("vcItemName")
                hfAssetAccountID.Value = CCommon.ToLong(dataView("numAssetChartAcntId"))
                hfWarehouseID.Value = CCommon.ToLong(dataView("numWareHouseID"))
                hfWarehouseItemID.Value = CCommon.ToLong(dataView("numWarehouseItemID"))
                hfBusinessProcess.Value = CCommon.ToString(dataView("Slp_Name"))
                hfBusinessProcessID.Value = CCommon.ToLong(dataView("numBusinessProcessId"))
                hfBuildManager.Value = CCommon.ToString(dataView("vcBuildManager"))
                hfBuildManagerID.Value = CCommon.ToLong(dataView("numBuildManager"))
                hfAverageCost.Value = CCommon.ToDouble(dataView("monAverageCost"))
                hfVendorID.Value = dataView("numVendorID")
                hfItemType.Value = dataView("vcItemType")
                hfContactID.Value = dataView("numContactID")
                hfUOMID.Value = dataView("numUOM")
                hfQtyToPurchase.Value = dataView("numQtyToPurchase")
                hfUOMConversionfactor.Value = dataView("fltUOMConversionfactor")
                hfSKU.Value = CCommon.ToString(dataView("vcSKU"))
                hfAttributes.Value = CCommon.ToString(dataView("vcAttributes"))
                hfWarehouse.Value = CCommon.ToString(dataView("vcWarehouse"))
                hfReleaseDate.Value = Convert.ToString(dataView("dtReleaseDateHidden"))

                If Convert.ToDouble(dataView("numQtyBasedOnPurchasePlan")) < 0 Then
                    hdnQtyToBuyBasedOnPurchasePlan.Value = 0
                Else
                    hdnQtyToBuyBasedOnPurchasePlan.Value = Convert.ToDouble(dataView("numQtyBasedOnPurchasePlan"))
                End If


                If Not txtmonCost Is Nothing Then
                    txtmonCost.Text = CCommon.ToDouble(dataView("monVendorCost"))
                End If

                If Not txtnumQtyToPurchase Is Nothing Then
                    If ddlDemandPlanBasedOn.SelectedValue = "2" Then
                        If Convert.ToDouble(dataView("numQtyBasedOnPurchasePlan")) < 0 Then
                            txtnumQtyToPurchase.Text = 0
                        Else
                            txtnumQtyToPurchase.Text = Convert.ToDouble(dataView("numQtyBasedOnPurchasePlan"))
                        End If
                    Else
                        txtnumQtyToPurchase.Text = CCommon.ToString(dataView("numQtyToPurchase"))
                    End If

                    'txtnumQtyToPurchase.Attributes.Add("onChange", "return QtyToPurchaseChanged(this);")
                End If

                Dim chkSelect As CheckBox = TryCast(e.Row.FindControl("ChkSelect"), CheckBox)

                Dim ddlShipVia As DropDownList = e.Row.FindControl("ddlShipVia")
                Dim ddlShipService As DropDownList = DirectCast(e.Row.FindControl("ddlShipService"), DropDownList)

                If Not ddlShipVia Is Nothing Then
                    ddlShipVia.Attributes.Add("onchange", "return ChangedShipVia(this);")
                End If
                If Not ddlShipService Is Nothing Then
                    ddlShipService.Attributes.Add("onchange", "return ChangeShippingService(this);")
                End If

                Dim ddlVendor As DropDownList = DirectCast(e.Row.FindControl("ddlVendor"), DropDownList)

                If Not ddlVendor Is Nothing Then
                    ddlVendor.Attributes.Add("onchange", "return ChangedVendor(this," & CCommon.ToLong(dataView("numItemCode")) & "," & CCommon.ToLong(dataView("numWarehouseItemID")) & ");")
                    'AddHandler ddlVendor.SelectedIndexChanged, AddressOf ddlVendor_SelectedIndexChanged
                    'ScriptManager.GetCurrent(Me).RegisterAsyncPostBackControl(ddlVendor)

                    Dim objItems As New CItems
                    Dim dtVendors As DataTable
                    objItems.DomainID = Session("DomainID")
                    objItems.ItemCode = dataView("numItemCode")
                    dtVendors = objItems.GetVendors
                    ddlVendor.DataSource = dtVendors
                    ddlVendor.DataTextField = "Vendor"
                    ddlVendor.DataValueField = "numVendorID"
                    ddlVendor.DataBind()

                    If Not ddlVendor.Items.FindByValue(CCommon.ToLong(dataView("numVendorID"))) Is Nothing Then
                        ddlVendor.Items.FindByValue(dataView("numVendorID")).Selected = True
                    End If

                    If Not lblExpectedDelivery Is Nothing Then
                        lblExpectedDelivery.Text = FormattedDateFromDate(DateTime.UtcNow.AddMinutes(Session("ClientMachineUTCTimeOffset") * -1), Session("DateFormat"))
                    End If

                    If Not lblLeadDays Is Nothing Then
                        lblLeadDays.Text = "-"
                    End If

                    If ddlVendor.Items.Count > 0 Then
                        Dim drVenders() As DataRow = dtVendors.Select(" numVendorID= " & CCommon.ToLong(ddlVendor.SelectedValue))

                        If drVenders.Count > 0 Then
                            Dim drow As DataRow = drVenders(0)
                            Dim dsVendorDetail As DataSet = GetVendorPrice(CCommon.ToLong(ddlVendor.SelectedValue), CCommon.ToLong(dataView("numItemCode")), CCommon.ToLong(dataView("numWarehouseItemID")), CCommon.ToDecimal(txtnumQtyToPurchase.Text))

                            Dim newPrice As Double
                            If Not dsVendorDetail Is Nothing AndAlso dsVendorDetail.Tables.Count > 0 AndAlso dsVendorDetail.Tables(0).Rows.Count > 0 Then
                                newPrice = CCommon.ToDouble(dsVendorDetail.Tables(0).Rows(0)("monCost"))

                                If dsVendorDetail.Tables.Count > 1 AndAlso dsVendorDetail.Tables(1).Rows.Count > 0 Then
                                    Dim isShipViaSelected As Boolean = False

                                    For Each drMethodofShipment In dsVendorDetail.Tables(1).Rows
                                        If Not ddlShipVia Is Nothing Then
                                            Dim listItem As New ListItem()
                                            listItem.Text = CCommon.ToString(drMethodofShipment("vcData"))
                                            listItem.Value = CCommon.ToString(drMethodofShipment("numListItemID")) & "#" & CCommon.ToString(drMethodofShipment("numListValue")) & "#" & CCommon.ToString(drMethodofShipment("vcShippingServices"))
                                            If Not isShipViaSelected AndAlso CCommon.ToInteger(drMethodofShipment("numListValue")) <> -1 Then
                                                listItem.Selected = True
                                                isShipViaSelected = True
                                            End If

                                            ddlShipVia.Items.Add(listItem)

                                            If Not ddlShipVia.SelectedItem Is Nothing Then
                                                Dim leadTimeDays As Integer = CCommon.ToInteger(ddlShipVia.SelectedValue.Split("#")(1))
                                                Dim arrShippingServices() As String = CCommon.ToString(ddlShipVia.SelectedValue.Split("#")(2)).Split(",")

                                                Dim objShippingService As New ShippingService
                                                objShippingService.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                                                objShippingService.ShipVia = CCommon.ToLong(ddlShipVia.SelectedValue)
                                                ddlShipService.DataSource = objShippingService.GetByShipVia()
                                                ddlShipService.DataTextField = "vcShipmentService"
                                                ddlShipService.DataValueField = "numShippingServiceID"
                                                ddlShipService.DataBind()



                                                If ddlShipService.Items.Count > 0 Then
                                                    ddlShipService.Style.Add("display", "")

                                                    For Each listItem In ddlShipService.Items
                                                        If ("," & CCommon.ToString(ddlShipVia.SelectedValue.Split("#")(2)).Replace(" ", "") & ",").Contains("," & listItem.Value & ",") Then
                                                            ddlShipService.Items.FindByValue(listItem.Value).Selected = True
                                                            Exit For
                                                        End If
                                                    Next

                                                    If leadTimeDays <> -1 AndAlso ("," & CCommon.ToString(ddlShipVia.SelectedValue.Split("#")(2)).Replace(" ", "") & ",").Contains("," & ddlShipService.SelectedValue & ",") Then
                                                        If Not lblExpectedDelivery Is Nothing Then
                                                            lblExpectedDelivery.Text = FormattedDateFromDate(DateTime.UtcNow.AddMinutes(Session("ClientMachineUTCTimeOffset") * -1).AddDays(leadTimeDays), Session("DateFormat"))
                                                        End If

                                                        If Not lblLeadDays Is Nothing Then
                                                            lblLeadDays.Text = "(" & leadTimeDays & " Days)"
                                                        End If
                                                    End If
                                                Else
                                                    ddlShipService.Style.Add("display", "none")

                                                    If leadTimeDays <> -1 Then
                                                        If Not lblExpectedDelivery Is Nothing Then
                                                            lblExpectedDelivery.Text = FormattedDateFromDate(DateTime.UtcNow.AddMinutes(Session("ClientMachineUTCTimeOffset") * -1).AddDays(leadTimeDays), Session("DateFormat"))
                                                        End If

                                                        If Not lblLeadDays Is Nothing Then
                                                            lblLeadDays.Text = "(" & leadTimeDays & " Days)"
                                                        End If
                                                    End If
                                                End If
                                            End If
                                        End If
                                    Next
                                End If
                            Else
                                newPrice = 0
                            End If

                            If Not e.Row.FindControl("txtPrice") Is Nothing Then
                                CType(e.Row.FindControl("txtPrice"), RadNumericTextBox).Text = newPrice
                            Else
                                CType(e.Row.FindControl("hfPrice"), HiddenField).Value = newPrice
                            End If
                        Else
                            If Not e.Row.FindControl("txtPrice") Is Nothing Then
                                CType(e.Row.FindControl("txtPrice"), RadNumericTextBox).Text = "0.00"
                            Else
                                CType(e.Row.FindControl("hfPrice"), HiddenField).Value = "0.00"
                            End If
                        End If
                    Else
                        If Not rbBuildPlan.Checked Then
                            chkSelect.Visible = False
                            chkSelect.Enabled = False
                        End If
                    End If
                End If

                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")
                    Dim strIDValue(), strID() As String

                    For i As Integer = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")
                        strID = strIDValue(0).Split("~")

                        If strIDValue(0).Contains("numVendorID") And (Not ddlVendor Is Nothing) Then
                            If (ddlVendor.Items.Count > 0) Then
                                If ddlVendor.SelectedItem.Text.ToUpper().Contains(strIDValue(1).ToUpper().ToString) Then
                                    e.Row.Visible = True
                                Else
                                    e.Row.Visible = False
                                End If
                            Else
                                e.Row.Visible = False
                            End If
                        End If

                        If Not txtmonCost Is Nothing Then
                            If strIDValue(0).Contains("monCost") Then
                                If txtmonCost.Text = (strIDValue(1).ToString) Then
                                    e.Row.Visible = True
                                Else
                                    e.Row.Visible = False
                                End If
                            End If
                        End If
                    Next
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
        Try
            ExecuteDemandPlan()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub chkHistoricalValues_CheckedChanged(sender As Object, e As EventArgs) Handles chkHistoricalValues.CheckedChanged
        Try
            ExecuteDemandPlan()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub chkIncludeOpportunity_CheckedChanged(sender As Object, e As EventArgs) Handles chkIncludeOpportunity.CheckedChanged
        Try
            ExecuteDemandPlan()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub rbPurchasePlan_CheckedChanged(sender As Object, e As EventArgs)
        Try
            btnLoadNewOrder.Text = "Load Items to Purchase"
            If Not ddlDemandPlanBasedOn.Items.FindByValue("1") Is Nothing Then
                ddlDemandPlanBasedOn.Items.FindByValue("1").Enabled = True
            End If
            ExecuteDemandPlan()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub rbBuildPlan_CheckedChanged(sender As Object, e As EventArgs)
        Try
            btnLoadNewOrder.Text = "Load Items to Build"
            If Not ddlDemandPlanBasedOn.Items.FindByValue("1") Is Nothing Then
                ddlDemandPlanBasedOn.Items.FindByValue("1").Enabled = False
            End If
            If Not ddlDemandPlanBasedOn.Items.FindByValue("2") Is Nothing Then
                ddlDemandPlanBasedOn.Items.FindByValue("2").Selected = True
            End If
            ExecuteDemandPlan()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub ddlAnalysisPatternLastWeek_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAnalysisPatternLastWeek.SelectedIndexChanged
        Try
            ExecuteDemandPlan()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub ddlOpportunityPercentComplete_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlOpportunityPercentComplete.SelectedIndexChanged
        Try
            ExecuteDemandPlan()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub chkBasedOnLastYear_CheckedChanged(sender As Object, e As EventArgs) Handles chkBasedOnLastYear.CheckedChanged
        Try
            ExecuteDemandPlan()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub ddlDemandPlanBasedOn_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            ExecuteDemandPlan()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.ToString())
        End Try
    End Sub

#End Region

    Private Class ReleaseDates
        Public Property ReleaseDate As String
        Public Property Quantity As Double
        Public Property IsSelected As Boolean
    End Class

End Class