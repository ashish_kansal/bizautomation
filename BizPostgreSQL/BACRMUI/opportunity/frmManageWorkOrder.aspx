﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmManageWorkOrder.aspx.vb"
    Inherits=".frmManageWorkOrder" MasterPageFile="~/common/GridMasterRegular.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Work Order</title>
    <script language="javascript">
        function pageLoad(sender, args) {
            BindInlineEdit();
            $("[id$=rtlWorkOrder] table:first").attr("class", "table table-bordered rtlLines rtlHBorders rtlVBorders rtlHVBorders");

            $("#hplClearGridCondition").click(function () {
                $('#txtGridColumnFilter').val("");
                $('#txtSortColumn').val("");
                $('#btnGo1').trigger('click');
            });
        }

        function OpenOpp(a) {
            var str;
            str = "../opportunity/frmOpportunities.aspx?frm=opportunitylist&OpID=" + a;
            document.location.href = str;
        }

        function OpenSetting() {
            window.open('../Items/frmConfItemList.aspx?FormID=33', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function PrintWorkOrder(numWOID) {

            var WOId = numWOID;
            //$(":checkbox").each(function () {
            //    if (this.checked) {
            //        alert($(this).parent().get_dataKeyValue("numWOID"));
            //        WOId = WOId + ',' + $(this).parent().attr("alt");
            //    }
            //});
            if (WOId == '')
                alert("Please select atleast one workorder to print.");
            else
                window.open('../opportunity/frmPrintWorkOrder.aspx?WOId=' + WOId, '', 'toolbar=no,titlebar=no,top=200,left=200,width=750,height=350,scrollbars=yes,resizable=yes')
            return false;
        }

        var arrFieldConfig = new Array();

        function ExpandCollapse(id) {
            try {
                var uniqueCode = id.getAttribute("UniqueID")
                var className = id.className;
                var newClassName;

                if (className.indexOf("rtlCollapse") >= 0) {
                    newClassName = className.replace("rtlCollapse", "rtlExpand");
                    id.className = newClassName;
                } else if (className.indexOf("rtlExpand") >= 0) {
                    newClassName = className.replace("rtlExpand", "rtlCollapse");
                    id.className = newClassName;
                }


                if (id != null) {
                    $('[id$=rtlWorkOrder] > table > tbody > tr').each(function () {
                        if ($(this).attr("UniqueID") != null && $(this).attr("UniqueID").indexOf(uniqueCode + "-") >= 0) {
                            var btn = $(this).find("[id*=ExpandCollapseButton]")

                            if (className.indexOf("rtlCollapse") >= 0) {
                                $(this).hide();

                                if (btn.length > 0) {
                                    btn[0].className = btn[0].className.replace("rtlCollapse", "rtlExpand");
                                }

                            } else if (className.indexOf("rtlExpand") >= 0) {
                                $(this).show();

                                if (btn.length > 0) {
                                    btn[0].className = btn[0].className.replace("rtlExpand", "rtlCollapse");
                                }
                            }
                        }
                    });
                }
            }
            catch (ex) {
            }
            finally {
                return false;
            }
        }

        function DeleteWorkOrder() {
            if (confirm('Selected work orders and its child work orders will be deleted and you have to manually delete any purchase orders associated with selected work orders. Do you want to proceed?')) {
                return true;
            } else {
                return false;
            }
        }

        function ChangeAssemblyStatus(dropDown) {
            try {
                if (dropDown != null && dropDown.value == "23184") {
                    var treelist = $find('<%=rtlWorkOrder.ClientID%>');
                    var item = treelist.getContainerItem(dropDown);

                    for (var i = 0; i < item.get_childItems().length; i++) {
                        var childItem = item.get_childItems()[i];

                        if (childItem.get_childItems().length > 0) {
                            var dropDownAssemblyStatus = $('[id*=ddlWOAssemblyStatus]', $(treelist.getCellByColumnUniqueName(childItem, 'AssemblyStatus')));

                            if (dropDownAssemblyStatus != null && dropDownAssemblyStatus.length > 0 && dropDownAssemblyStatus[0].value != "23184") {
                                dropDownAssemblyStatus[0].value = "23184";
                            }

                            SetStatusRecursive(treelist, childItem);
                        }
                    }
                }
            }
            catch (ex) {
            }
        }

        function SetStatusRecursive(treelist, item) {
            for (var i = 0; i < item.get_childItems().length; i++) {
                var childItem = item.get_childItems()[i];

                if (childItem.get_childItems().length > 0) {
                    var dropDownAssemblyStatus = $('[id*=ddlWOAssemblyStatus]', $(treelist.getCellByColumnUniqueName(childItem, 'AssemblyStatus')));

                    if (dropDownAssemblyStatus != null && dropDownAssemblyStatus.length > 0 && dropDownAssemblyStatus[0].value != "23184") {
                        dropDownAssemblyStatus[0].value = "23184";
                    }

                    SetStatusRecursive(childItem);
                }
            }
        }
        function OpenHelp() {
            window.open('../Help/Work_Orders.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function OpenBackOrderPOWindow(items) {
            var h = screen.height;
            var w = screen.width;

            window.open('../opportunity/frmCreateBackOrderPO.aspx?IsFromWorkOrder=1&Items=' + items, '', 'toolbar=no,titlebar=no,top=50,left=50,width=' + (w - 100) + ',height=' + (h - 200) + ',scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
    <style type="text/css">
        .rtlR td input {
            margin-left: 0px !important;
        }

        .rtlA td input {
            margin-left: 0px !important;
        }

        .rtlA {
            background-color: #fff !important;
        }

        .rtlCL {
            padding: 0px !important;
            background-color: #f3f3f3 !important;
        }

        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <label>
                        Assembly Status</label>
                    <asp:DropDownList ID="ddlAssemblyStatus" runat="server" CssClass="form-control" AutoPostBack="True">
                    </asp:DropDownList>
                    <label>
                        Filter</label>
                    <asp:DropDownList ID="ddlFilter" runat="server" AutoPostBack="true" CssClass="form-control">
                        <asp:ListItem Value="0">All Orders</asp:ListItem>
                        <asp:ListItem Value="1" Selected="true">My Work Orders</asp:ListItem>
                        <asp:ListItem Value="2">Buildable Orders</asp:ListItem>
                        <asp:ListItem Value="3">Un-Buildable Orders</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="pull-right">
                <div class="form-inline">
                    <asp:Button ID="btnPurchaseOrder" runat="server" CssClass="btn btn-primary" Text="Start Purchase Order(s)"></asp:Button>&nbsp;
                    <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save"></asp:Button>&nbsp;
                    <asp:Button ID="btnPrint" runat="server" CssClass="btn btn-primary" Text="Print"></asp:Button>&nbsp;
                    <asp:LinkButton ID="lkbDelete" runat="server" CssClass="btn btn-danger" OnClientClick="return DeleteWorkOrder();"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litError" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridSettingPopup" runat="server"
    ClientIDMode="Static">
    <a href="#" id="hplClearGridCondition" title="Clear All Filters" class="btn-box-tool"><i class="fa fa-2x fa-filter"></i></a>
    <a href="#" onclick="return OpenSetting()" title="Show Grid Settings." class="btn-box-tool"><i class="fa fa-2x fa-cog"></i></a>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    Work Orders &nbsp;<a href="#" onclick="return OpenHelp()"><label class="badge bg-yellow">?</label></a>

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true">
        <ContentTemplate>
            <div class="table-responsive">
                <telerik:RadTreeList ID="rtlWorkOrder" runat="server"
                    ParentDataKeyNames="numParentWOID" DataKeyNames="numWOID"
                    AutoGenerateColumns="false" OnItemCommand="rtlWorkOrder_ItemCommand"
                    OnItemDataBound="rtlWorkOrder_ItemDataBound" AllowRecursiveSelection="True">
                    <HeaderStyle Font-Bold="true" HorizontalAlign="Center" />

                    <DetailTemplate>
                        <table style="padding: 0; margin: 0; width: 100%; border-collapse: collapse;">
                            <tr>
                                <td style="text-align: right; border: none !important;">Created By / On:
                                </td>
                                <td style="border: none !important;">
                                    <asp:Label ID="Label4" runat="server" Text='<%# Eval("vcCreated") %>'></asp:Label>
                                </td>
                                <td style="border: none !important;"></td>
                                <td style="text-align: right; border: none !important;">Instruction:
                                </td>
                                <td style="border: none !important;">
                                    <asp:Label ID="lblNotes" runat="server" Text='<%# Eval("vcInstruction") %>'></asp:Label>
                                </td>
                                <td style="border: none !important;"></td>
                                <td style="text-align: right; border: none !important;">Completion Date:
                                </td>
                                <td style="border: none !important; border: none !important;">
                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("bintCompliationDate") %>'></asp:Label>
                                </td>
                                <td style="border: none !important;"></td>
                                <td style="text-align: right; border: none !important;">Assigned To:
                                </td>
                                <td style="border: none !important;">
                                    <asp:Label ID="Label3" runat="server" Text='<%# Eval("vcAssignedTo") %>'></asp:Label>
                                </td>
                        </table>
                    </DetailTemplate>
                    <Columns>
                        <telerik:TreeListBoundColumn DataField="vcItemName" UniqueName="vcItemName" HeaderText="Item" ItemStyle-Font-Bold="true"></telerik:TreeListBoundColumn>
                        <telerik:TreeListBoundColumn DataField="numQty" UniqueName="numQty" HeaderText="Quantity" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign="Right"></telerik:TreeListBoundColumn>
                        <telerik:TreeListBoundColumn DataField="vcWarehouse" UniqueName="vcWarehouse" HeaderText="Warehouse"></telerik:TreeListBoundColumn>
                        <telerik:TreeListBoundColumn DataField="numOnHand" UniqueName="numOnHand" HeaderText="On Hand" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign="Right"></telerik:TreeListBoundColumn>
                        <telerik:TreeListBoundColumn DataField="numOnOrder" UniqueName="numOnOrder" HeaderText="On Order" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign="Right"></telerik:TreeListBoundColumn>
                        <telerik:TreeListBoundColumn DataField="numAllocation" UniqueName="numAllocation" HeaderText="Allocation" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign="Right"></telerik:TreeListBoundColumn>
                        <telerik:TreeListBoundColumn DataField="numBackOrder" UniqueName="numBackOrder" HeaderText="Back Order" HeaderStyle-Width="80px" ItemStyle-HorizontalAlign="Right"></telerik:TreeListBoundColumn>
                        <telerik:TreeListTemplateColumn HeaderText="Assembly Status" HeaderStyle-Width="157px" UniqueName="AssemblyStatus">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlWOAssemblyStatus" runat="server" CssClass="signup" onchange="ChangeAssemblyStatus(this);">
                                </asp:DropDownList>
                            </ItemTemplate>
                        </telerik:TreeListTemplateColumn>
                        <telerik:TreeListTemplateColumn HeaderText="Sales Order" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center"
                            UniqueName="Sales Order">
                            <ItemTemplate>
                                <a onclick='OpenOpp(<%#Eval("numOppID")%>)' href="#">
                                    <%#Eval("vcOppName")%></a>
                            </ItemTemplate>
                        </telerik:TreeListTemplateColumn>
                        <telerik:TreeListTemplateColumn HeaderText="Purchase Order" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center"
                            UniqueName="Purchase Order">
                            <ItemTemplate>
                                <a onclick='OpenOpp(<%#Eval("numPOID")%>)' href="#">
                                    <%#Eval("vcPOName")%></a>
                            </ItemTemplate>
                        </telerik:TreeListTemplateColumn>
                        <telerik:TreeListTemplateColumn ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="25px">
                            <ItemStyle HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:CheckBox ID="chkDelete" runat="server" />
                                <asp:CheckBox ID="chkBackOrder" runat="server" Visible="false" />
                            </ItemTemplate>
                        </telerik:TreeListTemplateColumn>
                        <%-- HIDDEN COLUMNS --%>
                        <telerik:TreeListBoundColumn DataField="numParentWOID" UniqueName="numParentWOID" HeaderText="Parent Work Order" Visible="false"></telerik:TreeListBoundColumn>
                        <telerik:TreeListBoundColumn DataField="numWOID" UniqueName="numWOID" HeaderText="Work Order" Visible="false"></telerik:TreeListBoundColumn>
                        <telerik:TreeListBoundColumn DataField="bitWorkOrder" UniqueName="bitWorkOrder" HeaderText="bitWorkOrder" Visible="false"></telerik:TreeListBoundColumn>
                        <telerik:TreeListBoundColumn DataField="numItemCode" UniqueName="numItemCode" HeaderText="Item Code" Visible="false"></telerik:TreeListBoundColumn>
                        <telerik:TreeListBoundColumn DataField="numWarehouseItemID" UniqueName="numWarehouseItemID" HeaderText="WarehuoseID" Visible="false"></telerik:TreeListBoundColumn>
                        <telerik:TreeListBoundColumn DataField="numWOStatus" UniqueName="numWOStatus" HeaderText="numWOStatus" Visible="false"></telerik:TreeListBoundColumn>
                        <telerik:TreeListBoundColumn DataField="numOppID" UniqueName="numOppID" HeaderText="numOppID" Visible="false"></telerik:TreeListBoundColumn>
                        <telerik:TreeListBoundColumn DataField="numPOID" UniqueName="numPOID" HeaderText="numPOID" Visible="false"></telerik:TreeListBoundColumn>
                    </Columns>
                </telerik:RadTreeList>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--<telerik:RadGrid ID="RadGrid1" runat="server" OnColumnCreated="RadGrid1_ColumnCreated"
        OnItemCreated="RadGrid1_ItemCreated" OnItemDataBound="RadGrid1_ItemDataBound"
        AutoGenerateColumns="false" CssClass="tbl aspTable" Skin="windows" EnableEmbeddedSkins="false"
        EnableLinqExpressions="false">
        <MasterTableView HierarchyDefaultExpanded="false" HierarchyLoadMode="Client" DataKeyNames="numParentId,numChildId,numWOId,numItemKitID,numItemCode,RStageLevel,StageLevel"
            Width="100%" GridLines="Both" AllowSorting="true" CssClass="dg">
            <SelfHierarchySettings ParentKeyName="numParentId" KeyName="numChildId" />
            <Columns>
                <telerik:GridBoundColumn DataField="numItemCode" Visible="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="numWarehouseItmsID" Visible="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="RStageLevel" ItemStyle-Width="3%">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="vcCreatedBy" HeaderText="Created By, On"
                    ItemStyle-Width="10%"  SortExpression="vcCreatedBy"
                    UniqueName="Created By, On" Visible="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="vcItemName" HeaderText="Assembly Hierarchy"
                    ItemStyle-Width="10%"  SortExpression="vcItemName"
                    UniqueName="Assembly Hierarchy">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="vcWareHouse" HeaderText="Warehouse"
                    ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center" SortExpression="vcWareHouse"
                    UniqueName="Warehouse">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="numQtyItemsReq" HeaderText="Qty."
                    ItemStyle-Width="6%" ItemStyle-HorizontalAlign="Center" SortExpression="numQtyItemsReq"
                    UniqueName="Qty">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="vcItemDesc" HeaderText="Desc" ItemStyle-Width="17%"
                    ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" AllowSorting="false"
                    UniqueName="Description">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn HeaderText="Sales Order Name" ItemStyle-Width="10%" ItemStyle-HorizontalAlign="Center"
                    UniqueName="Sales Order Name" Visible="false">
                    <ItemTemplate>
                        <a onclick='OpenOpp(<%#Eval("numOppId") %>)' href="#">
                            <%#Eval("vcPOppName") %></a>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridTemplateColumn HeaderText="Assembly Status" ItemStyle-Width="9%" ItemStyle-HorizontalAlign="Center"
                    Visible="false" UniqueName="Assembly Status">
                    <ItemTemplate>
                        <asp:DropDownList ID="ddlWOAssemblyStatus" runat="server" CssClass="signup">
                        </asp:DropDownList>
                        <asp:Label ID="lblWOAssemblyStatus" runat="server" CssClass="signup"></asp:Label>
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="vcWOAssignedTo" HeaderText="Assign To" ItemStyle-Width="16%"
                    ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" AllowSorting="false"
                    UniqueName="Assign To" Visible="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="vcInstruction" HeaderText="Instruction" ItemStyle-Width="16%"
                    ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" UniqueName="Instruction"
                    AllowSorting="false" Visible="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="bintCompliationDate" HeaderText="Compl. Date"
                    ItemStyle-Width="9%" ItemStyle-HorizontalAlign="Center" SortExpression="bintCompliationDate"
                    UniqueName="Compliation Date" Visible="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="SOComments" HeaderText="SO Comments" ItemStyle-Width="9%"
                    ItemStyle-HorizontalAlign="Center" SortExpression="SOComments" AllowSorting="false"
                    UniqueName="SOComments" Visible="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="UOM" HeaderText="UOM" ItemStyle-Width="9%" ItemStyle-HorizontalAlign="Center"
                    SortExpression="UOM" AllowSorting="false" UniqueName="UOM">
                </telerik:GridBoundColumn>
                <telerik:GridTemplateColumn ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkDelete" runat="server" />
                    </ItemTemplate>
                </telerik:GridTemplateColumn>
                <telerik:GridBoundColumn DataField="numWOId" HeaderText="Work Order ID" ItemStyle-Width="9%"
                    ItemStyle-HorizontalAlign="Center" SortExpression="numWOId" AllowSorting="false"
                    UniqueName="Work Order ID" Visible="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="numOnHand" HeaderText="OnHand" ItemStyle-Width="9%"
                    ItemStyle-HorizontalAlign="Center" SortExpression="numOnHand" AllowSorting="false"
                    UniqueName="OnHand">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="numOnOrder" HeaderText="OnOrder" ItemStyle-Width="9%"
                    ItemStyle-HorizontalAlign="Center" SortExpression="numOnOrder" AllowSorting="false"
                    UniqueName="OnOrder">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="numBackOrder" HeaderText="Back Order" ItemStyle-Width="9%"
                    ItemStyle-HorizontalAlign="Center" SortExpression="numBackOrder" AllowSorting="false"
                    UniqueName="Back Order">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="numAllocation" HeaderText="Allocation" ItemStyle-Width="9%"
                    ItemStyle-HorizontalAlign="Center" SortExpression="numAllocation" AllowSorting="false"
                    UniqueName="Allocation">
                </telerik:GridBoundColumn>
            </Columns>
            <NoRecordsTemplate>
            </NoRecordsTemplate>
        </MasterTableView>
        <ClientSettings AllowExpandCollapse="true" />
    </telerik:RadGrid>--%>
    <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
