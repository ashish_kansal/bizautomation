﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmChangeDueDate.aspx.vb"
    Inherits=".frmChangeDueDate" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Select due date</title>
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />

    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>

    <script type="text/javascript" src="../JavaScript/en.js"></script>

    <script>
        function $(a) {
            return document.getElementById(a);
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <table width="100%" border="0">
        <tr>
            <td class="normal1">
                Select Due Date
                <asp:Button ID="btnSave" runat="server" Text="Save & Close" CssClass="button" Style="display: none;">
                </asp:Button>
            </td>
        </tr>
        <tr>
            <td>
                <table style="float: left; margin: 0 1em 1em 0">
                    <tr>
                        <td>
                            <!-- element that will contain the calendar -->
                            <div id="calendar-container">
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnDate" runat="server" />
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
    </form>
</body>
</html>
