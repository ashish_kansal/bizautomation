Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Opportunities

Partial Public Class frmPaymentDetails
    Inherits BACRMPage

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            If GetQueryStringVal("RType") <> "" Then
                If CCommon.ToInteger(GetQueryStringVal("RType")) = 1 Then
                    GetUserRightsForPage(35, 110)
                ElseIf CCommon.ToInteger(GetQueryStringVal("RType")) = 2 Then
                    GetUserRightsForPage(35, 111)
                End If
            End If

            'GetUserRightsForPage(10, 27)
            'If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
            '    Response.Redirect("../admin/authentication.aspx?mesg=AC")
            'End If

            If Not IsPostBack Then
                Dim lobjGeneralLedger As New GeneralLedger
                lobjGeneralLedger.DomainID = Session("DomainId")
                lobjGeneralLedger.Year = CInt(Now.Year)
                calFrom.SelectedDate = lobjGeneralLedger.GetFiscalDate()
                calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)

                PersistTable.Load(boolOnlyURL:=True)
                If PersistTable.Count > 0 Then
                    Try
                        calFrom.SelectedDate = PersistTable(calFrom.ID)
                        calTo.SelectedDate = PersistTable(calTo.ID)
                    Catch ex As Exception
                        'Do not throw error when date format which is stored in persist table and Current date formats are different
                        calFrom.SelectedDate = lobjGeneralLedger.GetFiscalDate()
                        calTo.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    End Try
                End If
                If GetQueryStringVal("RType") <> "" Then

                    btnClose.Visible = False

                    If CCommon.ToInteger(GetQueryStringVal("RType")) = 1 Then
                        lblHeader.Text = "Accounts Receivable Payment"
                    ElseIf CCommon.ToInteger(GetQueryStringVal("RType")) = 2 Then
                        lblHeader.Text = "Accounts Payable Payment"
                    End If

                    LoadReceivePaymentDetailsGrid()
                    'Else
                    '    tblDate.Visible = False
                    '    LoadReceivePaymentDetailsGrid()
                    '    dgPaymentDetails.Columns(4).Visible = False
                    '    dgPaymentDetails.Columns(5).Visible = False
                End If
                '' FillDepositTocombo()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub

    Private Sub LoadReceivePaymentDetailsGrid()
        Try
            Dim lobjReceivePayment As New ReceivePayment
            Dim dtReceivePayment As DataTable
            lobjReceivePayment.DomainID = Session("DomainId")
            lobjReceivePayment.OppId = CCommon.ToInteger(GetQueryStringVal("b"))
            lobjReceivePayment.OppBIzDocID = CCommon.ToInteger(GetQueryStringVal("a"))
            lobjReceivePayment.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            lobjReceivePayment.CompanyID = IIf(radCmbCompany.SelectedValue = "", 0, radCmbCompany.SelectedValue)

            If GetQueryStringVal("RType") <> "" Then
                lobjReceivePayment.FromDate = calFrom.SelectedDate
                lobjReceivePayment.ToDate = CDate(calTo.SelectedDate) ''calTo.SelectedDate
                lobjReceivePayment.Type = CType(GetQueryStringVal("RType"), Short)
                'Else
                '    lobjReceivePayment.FromDate = Date.UtcNow
                '    lobjReceivePayment.ToDate = Date.UtcNow
                '    lobjReceivePayment.Type = 0
            End If

            dtReceivePayment = lobjReceivePayment.GetReceivePaymentDetailsForBizDocs
            dgPaymentDetails.DataSource = dtReceivePayment
            dgPaymentDetails.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgPaymentDetails_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgPaymentDetails.ItemCommand
        Try
            If e.CommandName = "Delete" Then

                Dim ReferenceID As Long = CCommon.ToLong(CType(e.Item.FindControl("hfReferenceID"), HiddenField).Value)
                Dim Opptype As Short = CCommon.ToShort(CType(e.Item.FindControl("hfOpptype"), HiddenField).Value)

                Dim objJournalEntry As New JournalEntry

                If Opptype = 1 Then
                    objJournalEntry.DepositId = ReferenceID
                ElseIf Opptype = 2 Then
                    objJournalEntry.BillPaymentID = ReferenceID
                End If

                objJournalEntry.DomainID = Session("DomainId")
                Try
                    objJournalEntry.DeleteJournalEntryDetails()
                    LoadReceivePaymentDetailsGrid()
                Catch ex As Exception
                    If ex.Message = "BILL_PAID" Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('This transaction has been paid. If you want to change or delete it, you must edit the bill payment it appears on and remove it first.');", True)
                        Exit Sub
                    ElseIf ex.Message = "Undeposited_Account" Then
                        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "Alert", "alert('This transaction has been deposited. If you want to change or delete it, you must edit the deposit it appears on and remove it first.');", True)
                        Exit Sub
                    Else
                        Throw ex
                    End If
                End Try
                'Dim lobjReceivePayment As New ReceivePayment
                'Dim lintCount As Integer

                'lobjReceivePayment.BizDocsPaymentDetId = e.Item.Cells(0).Text
                'lobjReceivePayment.BizDocsId = e.Item.Cells(1).Text
                'lobjReceivePayment.OppId = e.Item.Cells(2).Text
                ''lobjReceivePayment.ComissionID = CCommon.ToLong(CType(e.Item.FindControl("numComissionID"), HiddenField).Value)

                'lintCount = lobjReceivePayment.GetOpportunityBizDocsIntegratedToAcnt
                '''lobjReceivePayment.Amount = e.Item.Cells(5).Text
                'If lintCount = 0 Then
                '    lobjReceivePayment.DeletePaymentDetails()
                '    LoadReceivePaymentDetailsGrid()
                'Else
                '    'LoadReceivePaymentDetailsGrid()
                '    litMessage.Text = "You cannot delete Payment Details for which amount is deposited"
                'End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub dgPaymentDetails_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPaymentDetails.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                Dim hpl As New HyperLink
                hpl = e.Item.FindControl("hplName")
                hpl.NavigateUrl = "#"

                'If GetQueryStringVal("RType") <> "" Then 'Or CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numComissionID")) > 0 Then
                hpl.Attributes.Add("onclick", " return OpenBizInvoice(" & DataBinder.Eval(e.Item.DataItem, "numOppId") & "," & DataBinder.Eval(e.Item.DataItem, "numBizDocsId") & ")")
                'Else
                '    hpl.Attributes.Add("onclick", "return OpenAmtPaid(" & e.Item.Cells(0).Text & "," & e.Item.Cells(1).Text & "," & e.Item.Cells(2).Text & "," & GetQueryStringVal("c") & ")")
                'End If

                Dim btnDelete As Button
                Dim lnkDelete As LinkButton
                lnkDelete = e.Item.FindControl("lnkDelete")
                btnDelete = e.Item.FindControl("btnDelete")

                'If DataBinder.Eval(e.Item.DataItem, "bitIntegratedToAcnt") = 1 Or CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numComissionID")) > 0 Then
                '    lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")

                '    lnkDelete.Visible = True
                '    btnDelete.Visible = False
                'Else
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")

                btnDelete.Visible = True
                lnkDelete.Visible = False
                'End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Write("<script>opener.location.reload(true); self.close();</script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Function ReturnMoney(ByVal Money)
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            LoadReceivePaymentDetailsGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub Page_PreRender(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.PreRender
        Try
            PersistTable.Clear()
            PersistTable.Add(calFrom.ID, calFrom.SelectedDate)
            PersistTable.Add(calTo.ID, calTo.SelectedDate)
            PersistTable.Save(boolOnlyURL:=True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub radCmbCompany_SelectedIndexChanged(sender As Object, e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
        Try
            LoadReceivePaymentDetailsGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub ibExportExcel_Click(sender As Object, e As System.EventArgs) Handles ibExportExcel.Click
        Dim lobjReceivePayment As New ReceivePayment
        Dim dtReceivePayment As DataTable
        lobjReceivePayment.DomainID = Session("DomainId")
        lobjReceivePayment.OppId = CCommon.ToInteger(GetQueryStringVal("b"))
        lobjReceivePayment.OppBIzDocID = CCommon.ToInteger(GetQueryStringVal("a"))
        lobjReceivePayment.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
        lobjReceivePayment.CompanyID = IIf(radCmbCompany.SelectedValue = "", 0, radCmbCompany.SelectedValue)

        If GetQueryStringVal("RType") <> "" Then
            lobjReceivePayment.FromDate = calFrom.SelectedDate
            lobjReceivePayment.ToDate = CDate(calTo.SelectedDate) ''calTo.SelectedDate
            lobjReceivePayment.Type = CType(GetQueryStringVal("RType"), Short)
        End If

        dtReceivePayment = lobjReceivePayment.GetReceivePaymentDetailsForBizDocs
        Dim dtPayment As New DataTable
        CCommon.AddColumnsToDataTable(dtPayment, "vcCompanyName,vcPOppName,vcBizDocID,Amount,dtPaymentDate,PaymentMethod,Memo,Reference")

        For Each dr As DataRow In dtReceivePayment.Rows
            dtPayment.ImportRow(dr)
            dtPayment.AcceptChanges()
        Next

        dtPayment.Columns("vcCompanyName").ColumnName = "Company Name"
        dtPayment.Columns("vcPOppName").ColumnName = "Order ID"
        dtPayment.Columns("vcBizDocID").ColumnName = "BizDoc ID"
        dtPayment.Columns("Amount").ColumnName = "Amount"
        dtPayment.Columns("dtPaymentDate").ColumnName = "Date"
        dtPayment.Columns("PaymentMethod").ColumnName = "Payment Method"
        dtPayment.Columns("Memo").ColumnName = "Memo"
        dtPayment.Columns("Reference").ColumnName = "Reference #"
        dtPayment.AcceptChanges()

        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=FileName" & Now.Day & Now.Month & Now.Year & Now.Hour & Now.Minute & Now.Second & ".csv")
        Response.Charset = ""
        Response.ContentType = "text/csv"
        Dim stringWrite As New System.IO.StringWriter
        CSVExport.ProduceCSV(dtPayment, stringWrite, True)
        Response.Write(stringWrite.ToString())
        Response.End()

    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
End Class