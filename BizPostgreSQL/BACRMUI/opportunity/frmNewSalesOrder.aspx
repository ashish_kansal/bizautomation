<%@ Page Language="vb" EnableViewState="true" ValidateRequest="false" AutoEventWireup="true"
    CodeBehind="frmNewSalesOrder.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmNewSalesOrder"
    MasterPageFile="~/common/SalesOrder.Master" %>

<%@ Register Src="order.ascx" TagName="order" TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Sales Order</title>
    <style type="text/css">
        .column {
            float: left;
        }

        html > body .RadComboBoxDropDown_Vista .rcbItem, html > body .RadComboBoxDropDown_Vista .rcbHovered, html > body .RadComboBoxDropDown_Vista .rcbDisabled, html > body .RadComboBoxDropDown_Vista .rcbLoading {
            display: inline-block;
        }
    </style>
    <link href="../CSS/style.css" rel="stylesheet" type="text/css" />
    <script src="../JavaScript/Orders.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script src="../JavaScript/Common.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        jQuery.noConflict();
        //function CheckClose()
        //{
        //    alert(1);
        //    opener.parent.parent.frames['topframe'].Popup = null;
        //    alert(1);
        //}
        //window.onunload = CheckClose;
        function Check() {
            return 'Check method call';
        }

        function openItem(a, b, c, d, e) {
            if (document.getElementById("radExistingCustomer").checked == true) {
                if ($find('radCmbCompany').get_value() == "") {
                    c = 0
                }
                else {
                    c = $find('radCmbCompany').get_value();
                }
            }
            else {
                c = 0;
            }


            if ($ControlFind(b).value == "") {
                b = 0
            }
            else {
                b = $ControlFind(b).value
            }

            var CalPrice = 0;
            if ($ControlFind('hdKit').value == 'True' && ($ControlFind('dgKitItem') != null)) {
                jQuery('ctl00_Content_order1_dgKitItem tr').not(':first,:last').each(function () {
                    var str;
                    str = jQuery(this).find("[id*='lblUnitPrice']").text();
                    CalPrice = CalPrice + (parseFloat(str.split('/')[0]) * jQuery(this).find("input[id*='txtKitUits']").val());
                });
            }

            var WareHouseItemID = 0;
            if ($find('radWareHouse') != null) {
                if ($find('radWareHouse').get_value() == "") {
                    WareHouseItemID = 0
                }
                else {
                    WareHouseItemID = $find('radWareHouse').get_value()
                }
            }
            if (a != 0 && b != 0) {
                window.open('../opportunity/frmItemPriceRecommd.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=' + a + '&Unit=' + b + '&DivID=' + c + '&OppType=' + e + '&CalPrice=' + CalPrice + '&WarehouseItemID=' + WareHouseItemID, '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
            }
        }

        function Save(a) {
            if (document.getElementById("radExistingCustomer").checked == true) {
                if ($find('radCmbCompany').get_value() == "") {
                    alert("Select Customer")
                    return false;
                }
                if (document.getElementById("ddlContact").value == 0) {
                    alert("Select Contact")
                    return false;
                }
            }
            else {
                if (document.getElementById("txtFirstName").value == '') {
                    alert("Enter First Name")
                    document.getElementById("txtFirstName").focus();
                    return false;
                }
                if (document.getElementById("txtLastName").value == '') {
                    alert("Enter Last Name")
                    document.getElementById("txtLastName").focus();
                    return false;
                }
                //Added validation to ensure that email is sent when add to cart button is clicked
                //if (document.getElementById("txtEmail").value == '') {
                //    alert("Enter valid Email");
                //    document.getElementById("txtEmail").focus();
                //    return false;
                //}
                if (document.getElementById("ddlRelationship").value == 0) {
                    alert("Select Relationship ")
                    document.getElementById("ddlRelationship").focus();
                    return false;
                }
            }

        }

        function CalculateTotal() {
            var Total, subTotal, TaxAmount, ShipAmt, IndTaxAmt, discount;
            var index, findex;
            Total = 0;
            TaxAmount = 0;
            subTotal = 0;
            IndTaxAmt = 0;
            var strtaxItemIDs = $ControlFind('TaxItemsId').value.split(',')
            var strTax = $ControlFind('txtTax').value.split(',')
            //            if (isNaN($('txtShipCost').value) == false && $('txtShipCost').value != '') {
            //                ShipAmt = parseFloat($('txtShipCost').value);

            //            }
            //            else {
            ShipAmt = 0;
            //            }
            jQuery('#ctl00_Content_order1_dgItems tr').not(':first,:last').each(function () {
                if (parseFloat(jQuery(this).find("input[id*='txtUnits']").val()) <= 0) {
                    alert("Units must greater than 0.")
                    jQuery(this).find("input[id*='txtUnits']").focus();
                    return false;
                }

                subTotal = (parseFloat(jQuery(this).find("input[id*='txtUnits']").val()) * parseFloat(jQuery(this).find("input[id*='txtUOMConversionFactor']").val())) * parseFloat(jQuery(this).find("input[id*='txtUnitPrice']").val());
                //subTotal = parseFloat(jQuery(this).find("input[id*='txtUnits']").val()) * parseFloat(jQuery(this).find("input[id*='txtUnitPrice']").val());

                if (jQuery(this).find("input[id*='txtDiscountType']").val() == 'True') {
                    subTotal = subTotal - parseFloat(jQuery(this).find("input[id*='txtTotalDiscount']").val());
                }
                else {
                    discount = (subTotal * (parseFloat(jQuery(this).find("input[id*='txtTotalDiscount']").val()) || 0)) / 100;
                    subTotal = subTotal - discount;

                    jQuery(this).find("[id*='lblDiscount']").text(CommaFormatted(roundNumber(discount, 2)) + ' (' + (parseFloat(jQuery(this).find("input[id*='txtTotalDiscount']").val()) || 0) + '%)');
                }

                jQuery(this).find("[id*='lblTotal']").text(CommaFormatted(roundNumber(subTotal, 2)));

                Total = parseFloat(Total) + parseFloat(subTotal);

                for (k = 0; k < strtaxItemIDs.length; k++) {
                    if (jQuery(this).find("[id*='lblTaxable" + strtaxItemIDs[k] + "']").text() != 'False') {
                        jQuery(this).find("[id*='lblTaxAmt" + strtaxItemIDs[k] + "']").text(parseFloat(strTax[k] * parseFloat(subTotal) / 100));
                        TaxAmount = parseFloat(TaxAmount) + parseFloat(jQuery(this).find("[id*='lblTaxAmt" + strtaxItemIDs[k] + "']").text());
                    }
                }
            });

            //for (i = 1; i < $('order1_dgItems').rows.length - 1; i++) {
            //                if (i <= 8) {
            //                    index = '0' + (i + 1)
            //                }
            //                else {
            //                    index = (i + 1);
            //                }

            //                if (parseFloat($('order1_dgItems_ctl' + index + '_txtUnits').value) <= 0) {
            //                    alert("Units must greater than 0.")
            //                    $('order1_dgItems_ctl' + index + '_txtUnits').focus();
            //                    return false;
            //                }

            //                subTotal = (parseFloat($('order1_dgItems_ctl' + index + '_txtUnits').value) * parseFloat($('order1_dgItems_ctl' + index + '_txtUOMConversionFactor').value)) * parseFloat($('order1_dgItems_ctl' + index + '_txtUnitPrice').value);

            //                if ($('order1_dgItems_ctl' + index + '_txtDiscountType').value == 'True') {
            //                    subTotal = subTotal - parseFloat($('order1_dgItems_ctl' + index + '_txtTotalDiscount').value);
            //                }
            //                else {
            //                    discount = (subTotal * parseFloat($('order1_dgItems_ctl' + index + '_txtTotalDiscount').value)) / 100;
            //                    subTotal = subTotal - discount;

            //                    if (document.all) {
            //                        $('order1_dgItems_ctl' + index + '_lblDiscount').innerText = CommaFormatted(roundNumber(discount, 2)) + ' (' + parseFloat($('order1_dgItems_ctl' + index + '_txtTotalDiscount').value) + '%)';
            //                    } else {
            //                        $('order1_dgItems_ctl' + index + '_lblDiscount').textContent = CommaFormatted(roundNumber(discount, 2)) + ' (' + parseFloat($('order1_dgItems_ctl' + index + '_txtTotalDiscount').value) + '%)';
            //                    }
            //                }

            //                if (document.all) {
            //                    $('order1_dgItems_ctl' + index + '_lblTotal').innerText = CommaFormatted(roundNumber(subTotal, 2));
            //                } else {
            //                    $('order1_dgItems_ctl' + index + '_lblTotal').textContent = CommaFormatted(roundNumber(subTotal, 2));
            //                }

            //                Total = parseFloat(Total) + parseFloat(subTotal);

            //                for (k = 0; k < strtaxItemIDs.length; k++) {

            //                    if (document.all) {
            //                        if ($('order1_dgItems_ctl' + index + '_lblTaxable' + strtaxItemIDs[k]).innerText != 'False') {
            //                            $('order1_dgItems_ctl' + index + '_lblTaxAmt' + strtaxItemIDs[k]).innerText = parseFloat(strTax[k] * parseFloat(subTotal) / 100)
            //                            TaxAmount = parseFloat(TaxAmount) + parseFloat($('order1_dgItems_ctl' + index + '_lblTaxAmt' + strtaxItemIDs[k]).innerText)
            //                        }
            //                    } else {
            //                        if ($('order1_dgItems_ctl' + index + '_lblTaxable' + strtaxItemIDs[k]).textContent != 'False') {
            //                            $('order1_dgItems_ctl' + index + '_lblTaxAmt' + strtaxItemIDs[k]).textContent = parseFloat(strTax[k] * parseFloat(subTotal) / 100)
            //                            TaxAmount = parseFloat(TaxAmount) + parseFloat($('order1_dgItems_ctl' + index + '_lblTaxAmt' + strtaxItemIDs[k]).textContent)
            //                        }
            //                    }

            //                }

            //            }
            jQuery('#ctl00_Content_order1_dgItems tr:last').find("[id*='lblFTotal']").text(CommaFormatted(roundNumber((Total), 2)));

            //            if ($('order1_dgItems').rows.length <= 8) {
            //                findex = '0' + ($('order1_dgItems').rows.length)
            //            }
            //            else {
            //                findex = ($('order1_dgItems').rows.length);
            //            }
            //            if (document.all) {
            //                $('order1_dgItems_ctl' + findex + '_lblFTotal').innerText = CommaFormatted(roundNumber((Total), 2))
            //            } else {
            //                $('order1_dgItems_ctl' + findex + '_lblFTotal').textContent = CommaFormatted(roundNumber((Total), 2))
            //            }


            jQuery('#lblTotal').text(CommaFormatted(roundNumber((Total) + parseFloat(TaxAmount) + parseFloat(ShipAmt), 2)));

            //            if (document.all) {
            //                $('lblTotal').innerText = CommaFormatted(roundNumber((Total) + parseFloat(TaxAmount) + parseFloat(ShipAmt), 2))
            //            } else {
            //                $('lblTotal').textContent = CommaFormatted(roundNumber((Total) + parseFloat(TaxAmount) + parseFloat(ShipAmt), 2))
            //            }

            for (k = 0; k < strtaxItemIDs.length; k++) {
                jQuery('#ctl00_Content_order1_dgItems tr').not(':first,:last').each(function () {
                    IndTaxAmt = IndTaxAmt + parseFloat(jQuery(this).find("[id*='lblTaxAmt" + strtaxItemIDs[k] + "']").text())
                });

                jQuery('#ctl00_Content_order1_dgItems tr:last').find("[id*='lblFTaxAmt" + strtaxItemIDs[k] + "']").text(CommaFormatted(roundNumber(IndTaxAmt, 2)));
                IndTaxAmt = 0;
            }
            //for (k = 0; k < strtaxItemIDs.length; k++) {

            //                for (i = 1; i < $('order1_dgItems').rows.length - 1; i++) {
            //                    if (i <= 8) {
            //                        index = '0' + (i + 1)
            //                    }
            //                    else {
            //                        index = (i + 1);
            //                    }
            //                    if (document.all) {
            //                        IndTaxAmt = IndTaxAmt + parseFloat($('order1_dgItems_ctl' + index + '_lblTaxAmt' + strtaxItemIDs[k]).innerText)
            //                    } else {
            //                        IndTaxAmt = IndTaxAmt + parseFloat($('order1_dgItems_ctl' + index + '_lblTaxAmt' + strtaxItemIDs[k]).textContent)
            //                    }
            //                }
            //                if (document.all) {
            //                    $('order1_dgItems_ctl' + findex + '_lblFTaxAmt' + strtaxItemIDs[k]).innerText = CommaFormatted(roundNumber(IndTaxAmt, 2))
            //                } else {
            //                    $('order1_dgItems_ctl' + findex + '_lblFTaxAmt' + strtaxItemIDs[k]).textContent = CommaFormatted(roundNumber(IndTaxAmt, 2))
            //                }

            //                IndTaxAmt = 0;
            //            }
            //            CalDue()

        }


        function OpenAddressWindow(sAddressType) {
            var radValue = $find('radCmbCompany').get_value();
            window.open('frmChangeDefaultAdd.aspx?AddressType=' + sAddressType + '&CompanyId=' + radValue + "&CompanyName=" + $find('radCmbCompany').get_text(), '', 'toolbar=no,titlebar=no,left=200, top=300,width=800,height=170,scrollbars=no,resizable=no');
            return false;
        }

        function FillModifiedAddress(AddressType, AddressID, FullAddress, warehouseID, isDropshipClicked) {
            if (AddressType == "BillTo") {
                document.getElementById('hdnBillAddressID').value = AddressID;
            } else {
                document.getElementById('hdnShipAddressID').value = AddressID;
            }

            document.getElementById("btnTemp").click();

            return false;
        }

        function Add(a) {

            var IsAddCalidate = Save();

            if (IsAddCalidate == false) {
                return false;
            }

            if ($find('radWareHouse') != null) {
                if ($find('radWareHouse').get_value() == "") {
                    alert("Select Warehouse")
                    return false;
                }
            }
            if (document.getElementById("txtUnits").value == "") {
                alert("Enter Units")
                document.getElementById("txtUnits").focus();
                return false;
            }
            if (parseFloat(document.getElementById("txtUnits").value) <= 0) {
                alert("Units must greater than 0.")
                document.getElementById("txtUnits").focus();
                return false;
            }

            if (document.getElementById("txtprice").value == "") {
                alert("Enter Price")
                document.getElementById("txtprice").focus();
                return false;
            }

            //Check for Duplicate Item
            var table = $ControlFind('ctl00_Content_order1_dgItems');
            var WareHouesID;
            //            alert($('radWareHouse'));
            if ($ControlFind('radWareHouse') == null) {
                WareHouesID = '';
            }
            else {
                WareHouesID = $find('radWareHouse').get_value();
            }

            if ($ControlFind('chkDropShip').checked == true && $ControlFind('hdnPrimaryVendorID').value == "0") {
                alert("You must add a primary vendor to this item first")
                return false;
            }

            var dropship;
            if ($ControlFind('chkDropShip').checked == true) {
                dropship = 'True';
                WareHouesID = '';
            }
            else {
                dropship = 'False';
            }

            var checkbox = document.getElementById("chkWorkOrder");
            if (checkbox != null && checkbox.checked) {
                if (document.getElementById('txtDate').value == 0) {
                    alert("Enter Expected Completion Date")
                    document.getElementById('txtDate').focus();
                    return false;
                }
            }
        }

        function SetWareHouseId(id) {
            var combo = $find('radWareHouse')
            var node = combo.findItemByValue(id);
            node.select();
        }


        function OpenAdd() {
            if ($find('radCmbCompany').get_value() == "") {
                alert("Select Customer")
                return false;
            }
            window.open("../prospects/frmProspectsAdd.aspx?pwer=dfsdfdsfdsfdsf&rtyWR=" + $find('radCmbCompany').get_value() + "&Reload=1", '', 'toolbar=no,titlebar=no,top=300,width=700,height=300,scrollbars=no,resizable=no')
            return false;
        }

        function AddUpdate() {
            return validateFixedFields();
        }
        function ValidateShipperAccount() {
            if (document.getElementById('hdnShipperAccountNo') != null && document.getElementById('hdnShipperAccountNo').value.toString() != '') {
                return true;
            }
            else {
                alert('Customer shipping account information not present. Go to the accounting sub-tab of this customer and enter the proper shipping account information for the shipping method selected.');
                document.getElementById('chkUseCustomerShippingAccount').checked = false;
                return false;
            }
        }

        function ShippingVisibility(mode) {
            alert(mode);

            if (mode == 0) {
                console.log($('#trShipping #divLabelEstimatedRates').attr('id'));
                console.log($('#trShipping #divComboEstimatedRates').attr('id'));
                console.log($('#trShipping #divShippingEstimates').attr('id'));
            }
            else {

                console.log($('#trShipping #divLabelEstimatedRates').attr('id'));
                console.log($('#trShipping #divComboEstimatedRates').attr('id'));
                console.log($('#trShipping #divShippingEstimates').attr('id'));
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
        <Scripts>
            <asp:ScriptReference Path="~/JavaScript/prototype-1.6.0.2.compressed.js" />
            <asp:ScriptReference Path="~/JavaScript/comboClientSide.js" />
        </Scripts>
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanelException" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Label ID="lblException" runat="server" Style="color: red;"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Sales Order
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="100%" runat="server" id="tblMultipleOrderMessage" visible="false" style="background-color: #FFFFFF; color: red">
        <tr>
            <td class="normal4" align="center">You can not create multiple orders simultaneously,
                <asp:LinkButton Text="Click here" runat="server" ID="lnkClick" />
                to clear other order and proceed.
            </td>
        </tr>
    </table>
    <table width="100%" style="background-color: #FFFFFF;">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>

    <div class="container customer-detail">
        <div class="grid grid-pad">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="cont1">
                        <div class="col-1-1" style="float: left">
                            <img src="../images/Greenman-32.gif" />
                        </div>
                        <div class="col-1-1">
                            <div class="col-1-2">
                                <div class="content">
                                    <div class="form-group">
                                        <label class="col-3-12"></label>
                                        <div class="col-9-12">
                                            <label class="radio-inline">
                                                <asp:RadioButton AutoPostBack="true" Checked="true" ID="radExistingCustomer" runat="server" Text="Existing Vendor" GroupName="rad" TabIndex="1" />
                                            </label>
                                            <label class="radio-inline">
                                                <asp:RadioButton AutoPostBack="true" ID="radNewCustomer" runat="server" Text="New Vendor" GroupName="rad" TabIndex="2" />
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1-2">
                                <div class="content">
                                    <div class="form-group" id="pnlCurrency" runat="server" visible="false">
                                        <label class="col-3-12">Currency</label>
                                        <div class="col-9-12">
                                            <asp:DropDownList ID="ddlCurrency" runat="server" TabIndex="3"></asp:DropDownList>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-1-1" id="trExistCust" runat="server">
                            <div class="col-1-1">
                                <div class="col-1-2">
                                    <div class="content">
                                        <div class="form-group">
                                            <label class="col-3-12">Vendor<span class="required">*</span></label>
                                            <div class="col-9-12">
                                                <telerik:RadComboBox AccessKey="V" ID="radCmbCompany" Width="100%" DropDownWidth="600px"
                                                    OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                                    ClientIDMode="Static"
                                                    ShowMoreResultsBox="true"
                                                    Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True"
                                                    TabIndex="4">
                                                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                                </telerik:RadComboBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-3-12">Assign To</label>
                                            <div class="col-9-12">
                                                <asp:DropDownList ID="ddlAssignTo" runat="server" TabIndex="6">
                                                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                </asp:DropDownList>
                                                <div>
                                                    <div>
                                                        <div class="col-1-2">
                                                            <asp:LinkButton ID="lnkBillTo" runat="server" Text="Bill to:" class="hyperlink" TabIndex="8"></asp:LinkButton>
                                                        </div>
                                                        <div class="col-1-2"><a href="#" onclick="return OpenAdd()" style="float: right" tabindex="8">Add/Edit</a></div>
                                                    </div>
                                                    <div>
                                                        <asp:Label ID="lblBillTo1" runat="server" Text=""></asp:Label>
                                                        <asp:Label ID="lblBillTo2" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-1-2">
                                    <div class="content">
                                        <div class="form-group">
                                            <label class="col-3-12">Contact<span class="required">*</span></label>
                                            <div class="col-9-12">
                                                <asp:DropDownList ID="ddlContact" runat="server" TabIndex="5">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-3-12">Sales Template</label>
                                            <div class="col-9-12">
                                                <asp:DropDownList ID="ddlSalesTemplate" runat="server" AutoPostBack="true" TabIndex="7"></asp:DropDownList>
                                                <div>
                                                    <div>
                                                        <div class="col-1-2">
                                                            <asp:LinkButton ID="lnkShipTo" runat="server" Text="Ship to:" class="hyperlink" TabIndex="10"></asp:LinkButton>
                                                        </div>
                                                        <div class="col-1-2">
                                                            <a href="#" onclick="return OpenAdd()" class="hyperlink" style="float: right" tabindex="11">Add/Edit</a>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <asp:Label ID="lblShipTo1" runat="server" Text=""></asp:Label>
                                                        <asp:Label ID="lblShipTo2" runat="server" Text=""></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1-1" id="pnlPurchase" runat="server" visible="false">
                                <div class="col-1-2" style="float: left"></div>
                                <div class="col-1-2" style="float: right">
                                    <div class="content">
                                        <div class="form-group">
                                            <label class="col-3-12"></label>
                                            <div class="col-9-12">
                                                <asp:Button ID="btnSelectVendor" CssClass="button" runat="server" Text="Select Vendor" TabIndex="12" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-1-1" id="trNewCust" runat="server">
                            <div class="col-1-1">
                                <div class="col-1-2">
                                    <div class="content">
                                        <div class="form-group">
                                            <label class="col-3-12">First Name<span class="required">*</span></label>
                                            <div class="col-9-12">
                                                <asp:TextBox ID="txtFirstName" runat="server" MaxLength="50" TabIndex="13"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-3-12">Phone</label>
                                            <div class="col-9-12">
                                                <div style="float: left">
                                                    <asp:TextBox ID="txtPhone" runat="server" MaxLength="50" TabIndex="15"></asp:TextBox>
                                                </div>
                                                <div style="float: left; margin-left: 10px;">
                                                    <div class="form-group">
                                                        <label class="col-3-12">Ext</label>
                                                    </div>
                                                </div>
                                                <div style="float: left;">
                                                    <asp:TextBox ID="txtExt" runat="server" Width="30" MaxLength="10" TabIndex="16"></asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Organization" class="col-3-12">Organization</label>
                                            <div class="col-9-12">
                                                <asp:TextBox ID="txtCompany" runat="server" MaxLength="50" TabIndex="18"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Relationship" class="col-3-12">Relationship<span class="required">*</span></label>
                                            <div class="col-9-12">
                                                <asp:DropDownList ID="ddlRelationship" runat="server" AutoPostBack="true" TabIndex="20">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-3-12"></label>
                                            <div class="col-9-12">
                                                <asp:CheckBoxList ID="chkTaxItems" CssClass="normal1" runat="server" RepeatDirection="Horizontal"
                                                    RepeatColumns="3">
                                                </asp:CheckBoxList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-1-2">
                                    <div class="content">
                                        <div class="form-group">
                                            <label class="col-3-12">Last Name<span class="required">*</span></label>
                                            <div class="col-9-12">
                                                <asp:TextBox ID="txtLastName" runat="server" MaxLength="50" TabIndex="14"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Email" class="col-3-12">Email</label>
                                            <div class="col-9-12">
                                                <asp:TextBox ID="txtEmail" runat="server" TabIndex="17"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="AssignedTo" class="col-3-12">Assign To</label>
                                            <div class="col-9-12">
                                                <asp:DropDownList ID="ddlNewAssignTo" runat="server" TabIndex="19">
                                                    <asp:ListItem Text="--Select One--" Value="0"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="Profile" class="col-3-12">Profile</label>
                                            <div class="col-9-12">
                                                <asp:DropDownList ID="ddlProfile" runat="server" TabIndex="21">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1-1">
                                <strong>Billing Address</strong>
                            </div>
                            <div class="col-1-1">
                                <div class="col-1-2">
                                    <div class="content">
                                        <div class="form-group">
                                            <label for="BillStreet" class="col-3-12">Street</label>
                                            <div class="col-9-12">
                                                <asp:TextBox ID="txtBillStreet" runat="server" TextMode="MultiLine" TabIndex="22"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="BillState" class="col-3-12">State</label>
                                            <div class="col-9-12">
                                                <asp:DropDownList ID="ddlBillState" runat="server" TabIndex="24">
                                                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="BillPostal" class="col-3-12">Postal</label>
                                            <div class="col-9-12">
                                                <asp:TextBox ID="txtBillPostal" runat="server" TabIndex="26"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-1-2">
                                    <div class="content">
                                        <div class="form-group">
                                            <label for="BillCity" class="col-3-12">City</label>
                                            <div class="col-9-12">
                                                <asp:TextBox ID="txtBillCity" runat="server" TabIndex="23"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="BillCountry" class="col-3-12">Country</label>
                                            <div class="col-9-12">
                                                <asp:DropDownList ID="ddlBillCountry" TabIndex="25" AutoPostBack="True" runat="server">
                                                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1-1">
                                <asp:CheckBox AutoPostBack="true" ID="chkShipDifferent" Text="Click here if Shipping Address is different than Billing Address" runat="server" TabIndex="27" />
                            </div>
                            <div id="pnlShipping" runat="server" visible="false" style="margin-top: 5px;" class="col-1-1">
                                <div class="col-1-1"><strong>Shipping Address</strong></div>
                                <div class="col-1-2">
                                    <div class="content">
                                        <div class="form-group">
                                            <label for="ShipStreet" class="col-3-12">Street</label>
                                            <div class="col-9-12">
                                                <asp:TextBox ID="txtShipStreet" runat="server" TextMode="MultiLine" TabIndex="28"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ShipState" class="col-3-12">State</label>
                                            <div class="col-9-12">
                                                <asp:DropDownList ID="ddlShipState" runat="server" TabIndex="30">
                                                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ShipPostal" class="col-3-12">Postal</label>
                                            <div class="col-9-12">
                                                <asp:TextBox ID="txtShipPostal" runat="server" TabIndex="32"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-1-2">
                                    <div class="content">
                                        <div class="form-group">
                                            <label for="ShipCity" class="col-3-12">City</label>
                                            <div class="col-9-12">
                                                <asp:TextBox ID="txtShipCity" runat="server" TabIndex="29"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="ShipCountry" class="col-3-12">Country</label>
                                            <div class="col-9-12">
                                                <asp:DropDownList ID="ddlShipCountry" TabIndex="31" AutoPostBack="True" runat="server">
                                                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cont2">
                        <div class="col-1-2"></div>
                        <div class="col-1-2 total-cont" style="float: right" id="tdBanlce" runat="server">
                            <div><span>Sales Credit:</span><asp:Label ID="lblCreditBalance" runat="server"></asp:Label></div>
                            <div><span>Credit Limit (A):</span><asp:Label ID="lblCreditLimit" runat="server"></asp:Label></div>
                            <div style="float: left"><span>Balance Due (B):</span><asp:Label ID="lblBalanceDue" runat="server"></asp:Label></div>
                            <div style="float: left"><span>Remaining Credit (A - B):</span><asp:Label ID="lblRemaningCredit" runat="server"></asp:Label></div>
                            <div style="float: left"><span>Amount Past Due:</span><asp:Label ID="lblTotalAmtPastDue" runat="server"></asp:Label></div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="radExistingCustomer" />
                    <asp:AsyncPostBackTrigger ControlID="radNewCustomer" />
                    <asp:AsyncPostBackTrigger ControlID="radCmbCompany" />
                    <asp:AsyncPostBackTrigger ControlID="chkShipDifferent" />
                    <asp:AsyncPostBackTrigger ControlID="ddlRelationship" />
                    <asp:AsyncPostBackTrigger ControlID="ddlProfile" />
                    <asp:AsyncPostBackTrigger ControlID="ddlSalesTemplate" />
                    <asp:AsyncPostBackTrigger ControlID="ddlContact" />
                    <asp:AsyncPostBackTrigger ControlID="ddlNewAssignTo" />
                    <asp:AsyncPostBackTrigger ControlID="ddlCurrency" />
                    <asp:AsyncPostBackTrigger ControlID="ddlAssignTo" />
                    <asp:AsyncPostBackTrigger ControlID="btnSelectVendor" />
                </Triggers>
            </asp:UpdatePanel>
            <uc1:order ID="order1" runat="server" />
        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanelHiddenField" UpdateMode="Always" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnTemp" runat="server" Style="display: none"></asp:Button>
            <asp:HiddenField ID="hdnCmbDivisionID" runat="server" />
            <asp:HiddenField ID="hdnCmbOppType" runat="server" />
            <asp:HiddenField ID="hdnmonVal1" runat="server" />
            <asp:HiddenField ID="hdnmonVal2" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
