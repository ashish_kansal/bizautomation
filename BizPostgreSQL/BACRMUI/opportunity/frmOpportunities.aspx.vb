'***************************************************************************************************************************
'     Author Name				 :  Anoop Jayaraj
'     Date Written				 :  18/2/2005
'***************************************************************************************************************************
Imports System.IO
Imports System.Text
Imports System.Web.Services
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.ShioppingCart
Imports BACRM.BusinessLogic.Workflow
Imports Newtonsoft.Json
Imports nsoftware.InShip
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Forecasting

Namespace BACRM.UserInterface.Opportunities
    Public Class frmOpportunities
        Inherits BACRMPage
        Shared selectContactId As Integer = 0
        Dim objContacts As CContacts
        Dim objOpportunity As New OppotunitiesIP
        Dim objCus As CustomFields
        Dim objItems As CItems
        Shared dsTemp As DataSet
        Dim ds As DataSet
        Dim strValues As String
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim m_aryRightsForAssContacts(), m_aryRightsForCustFlds(), m_aryRightsForItems(), m_aryRightsForClosingDeal(), m_aryRightsForReOpenDeal(), m_aryRightsForViewLayoutButton() As Integer
        Dim myRow As DataRow
        Dim arrOutPut() As String = New String(2) {}
        Dim lngOppId, lngBizDocId As Long
        Dim lngDivId As Long
        Dim OrderCreatedDate As DateTime
        Dim boolIntermediatoryPage As Boolean = False
        Dim dtTableInfo, dtSalesProcess, dtCustomFieldTable, dtOppOptAttributes, dtOppAtributes, dtAllBizDocs As DataTable
        Dim objPageControls As New PageControls
        Dim alSelectedItems As ArrayList
        Dim lngShippingItemCode As Double = 0
        Dim lngEmpAccountID, lngProjAccountID, lngEmpPayrollExpenseAcntID, CatHdrId As Long
        Dim totalAmount, TotalItemWeight, dcShippingCharge As Decimal
        Dim isMinUnitPriceRuleSet As Boolean
        Dim userID, unitPriceApprover() As String
        Dim eventTarget As String
        Dim refData As Integer = 0
        Dim dtUnit, dtPickPackItems, dtFullFilmentItems, dtInvoicedItems As DataTable
        Dim _FedexTrack As nsoftware.InShip.Fedextrack = New nsoftware.InShip.Fedextrack()
        Dim _UpsTrack As nsoftware.InShip.Upstrack = New nsoftware.InShip.Upstrack()
        Dim lngShippingCompanyId As Long
        Shared dsEDICFForOrder As DataSet
        Shared dsEDICFForOppItem As DataSet
        Public Shared numOppItemCodeEDI As Long
        <WebMethod()>
        Public Shared Function bindContact(ByVal domain As String, ByVal oppid As String, ByVal DivisionID As String) As String
            Dim dtData As New DataTable()
            Dim objCommon As New CCommon
            objCommon.DomainID = Sites.ToLong(domain)
            objCommon.RecordId = Sites.ToLong(oppid)
            objCommon.DivisionID = Sites.ToLong(DivisionID)
            dtData = objCommon.GetPartnerContacts()
            Dim jsTargets As [String] = Nothing
            jsTargets = DataTableToJsonObj(dtData)
            Return jsTargets
        End Function

        <WebMethod()>
        Public Shared Function MappingContact(ByVal ContactID As Integer) As String
            selectContactId = ContactID
            Return "1"
        End Function

        Public Shared Function DataTableToJsonObj(dt As DataTable) As String
            Dim ds As New DataSet()
            ds.Merge(dt)
            Dim JsonString As New StringBuilder()
            If ds IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                JsonString.Append("[")
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    JsonString.Append("{")
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1
                        If j < ds.Tables(0).Columns.Count - 1 Then
                            JsonString.Append("""" + ds.Tables(0).Columns(j).ColumnName.ToString() + """:" + """" + ds.Tables(0).Rows(i)(j).ToString() + """,")
                        ElseIf j = ds.Tables(0).Columns.Count - 1 Then
                            JsonString.Append("""" + ds.Tables(0).Columns(j).ColumnName.ToString() + """:" + """" + ds.Tables(0).Rows(i)(j).ToString() + """")
                        End If
                    Next
                    If i = ds.Tables(0).Rows.Count - 1 Then
                        JsonString.Append("}")
                    Else
                        JsonString.Append("},")
                    End If
                Next
                JsonString.Append("]")
                Return JsonString.ToString()
            Else
                Return Nothing
            End If
        End Function
        'Public Shared Function DataSetToJSON(ds As DataTable) As String

        '    Dim dict As New Dictionary(Of String, Object)()
        '    Dim arr As Object() = New Object(ds.Rows.Count) {}

        '    For i As Integer = 0 To ds.Rows.Count - 1
        '        arr(i) = ds.Rows(i).ItemArray
        '    Next

        '    dict.Add(ds.TableName, arr)
        '    Dim json As New JavaScriptSerializer()
        '    Return json.Serialize(dict)
        'End Function
        <WebMethod()>
        Public Shared Function PersistTab(ByVal strKey As String, ByVal strValue As String,
                                          ByVal PageType As String, ByVal PageStatus As String) As String

            Dim PersistTable As New Hashtable
            PersistTable.Clear()
            PersistTable.Add(strKey, strValue)
            PersistTable.Add("Page", PageType)

            If CCommon.ToShort(PageType) = 1 AndAlso CCommon.ToShort(PageStatus) = 0 Then
                PersistTable.Save(strPageName:="frmOpportunitiesSOpp.aspx")
            ElseIf CCommon.ToShort(PageType) = 1 AndAlso CCommon.ToShort(PageStatus) = 1 Then
                PersistTable.Save(strPageName:="frmOpportunitiesSO.aspx")
            ElseIf CCommon.ToShort(PageType) = 2 AndAlso CCommon.ToShort(PageStatus) = 0 Then
                PersistTable.Save(strPageName:="frmOpportunitiesPOpp.aspx")
            ElseIf CCommon.ToShort(PageType) = 2 AndAlso CCommon.ToShort(PageStatus) = 1 Then
                PersistTable.Save(strPageName:="frmOpportunitiesPO.aspx")
            End If
            'PersistTable.Save(strPageName:="frmOpportunities" & If(PageType = 1, "SO", "PO") & ".aspx")

        End Function

        Private Sub DisplayError(ByVal exception As String)
            Try
                lblPageError.Text = exception
                divPageError.Style.Add("display", "")
                divPageError.Focus()
            Catch ex As Exception

            End Try
        End Sub

        Private Sub ShowMessage(ByVal message As String)
            Try
                divMessage.Style.Add("display", "")
                litMessage.Text = message
                divMessage.Focus()
            Catch ex As Exception
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try

                'CLEAR ERROR ON RELOAD
                lblPageError.Text = ""
                divPageError.Style.Add("display", "none")

                'CLEAR ALERT MESSAGE
                divMessage.Style.Add("display", "none")
                litMessage.Text = ""

                divPickPackShipMoreException.Style.Add("display", "none")
                lblPickPackShipMoreException.Text = ""

                If CCommon.ToDouble(Session("ShippingServiceItem")) > 0 Then
                    lngShippingItemCode = CCommon.ToDouble(Session("ShippingServiceItem"))
                Else
                    lngShippingItemCode = 0
                End If

                DomainID = Session("DomainID")
                UserCntID = Session("UserContactID")
                isMinUnitPriceRuleSet = CCommon.ToBool(Session("IsMinUnitPriceRule"))
                userID = CCommon.ToString(Session("UserID"))
                unitPriceApprover = CCommon.ToString(Session("UnitPriceApprover")).Split(",")

                If Not IsPostBack Then
                    If Session("EnableIntMedPage") = 1 Then
                        ViewState("IntermediatoryPage") = True
                    Else
                        ViewState("IntermediatoryPage") = False
                    End If

                    m_aryRightsForViewLayoutButton = GetUserRightsForPage_Other(10, 32)
                    If m_aryRightsForViewLayoutButton(RIGHTSTYPE.VIEW) = 0 Then
                        btnLayout.Visible = False
                        imgGridConfiguration.Visible = False
                    End If

                    Dim ObjCusfld As New CustomFields
                    Dim dtTable As DataTable
                    ObjCusfld.ListId = CInt(12) 'Deal Status
                    dtTable = ObjCusfld.GetMasterListByListId
                    ddlConclusionReason.DataSource = dtTable
                    ddlConclusionReason.DataTextField = "vcData"
                    ddlConclusionReason.DataValueField = "numListItemID"
                    ddlConclusionReason.DataBind()
                    ddlConclusionReason.Items.Insert(0, "--Select One--")
                    ddlConclusionReason.Items.FindByText("--Select One--").Value = 0
                End If

                'If Session("MultiCurrency") = True Then
                '    tblCurrency.Visible = True
                'Else
                '    tblCurrency.Visible = False
                'End If
                btnResourceScheduling.Visible = Session("IsEnableResourceScheduling")

                boolIntermediatoryPage = ViewState("IntermediatoryPage")
                ControlSettings()
                lngOppId = GetQueryStringVal("opid")
                Dim hdnCollabarationLoggedInUser As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnLoggedInUser"), HiddenField)
                Dim hdnCollabarationRecordId As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnRecordId"), HiddenField)
                Dim hdnCollabarationRecordType As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnRecordType"), HiddenField)
                Dim hdnCollabarationDomainId As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnDomainId"), HiddenField)
                Dim hdnCollabarationPortalDocUrl As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnCollabarationPortalDocUrl"), HiddenField)
                Dim hdnClientMachineUTCTimeOffset As HiddenField = DirectCast(frmCollaborationUserControl.FindControl("hdnClientMachineUTCTimeOffset"), HiddenField)
                hdnCollabarationLoggedInUser.Value = CCommon.ToString(Session("UserContactID"))
                hdnCollabarationDomainId.Value = CCommon.ToString(Session("DomainID"))
                hdnClientMachineUTCTimeOffset.Value = CCommon.ToString(Session("ClientMachineUTCTimeOffset"))
                hdnCollabarationRecordType.Value = "3"
                hdnCollabarationRecordId.Value = CCommon.ToString(lngOppId)
                hdnCollabarationPortalDocUrl.Value = CCommon.GetDocumentPath(Convert.ToInt64(Session("DomainID")))
                frmAssociatedContacts1.TypeId = lngOppId

                m_aryRightsForAssContacts = GetUserRightsForPage_Other(10, 4)
                m_aryRightsForItems = GetUserRightsForPage_Other(10, 5)
                m_aryRightsForClosingDeal = GetUserRightsForPage_Other(10, 20)
                m_aryRightsForReOpenDeal = GetUserRightsForPage_Other(10, 30)

                If Not IsPostBack Then
                    hplHistory.Attributes.Add("onclick", "return OpenHistory(" & lngOppId & ")")
                    AddToRecentlyViewed(RecetlyViewdRecordType.Opportunity, lngOppId)

                    FillNetDays()

                    LoadSavedInformation(boolLoadOppInfo:=True)

                    ''''sub-tab management  - added on 14/09/2009 by chintan
                    objCommon.GetAuthorizedSubTabsNew(radOppTab, Session("DomainID"), IIf(OppType.Value = 1, 2, 6), Session("UserGroupID"))
                    '''''
                    hplTransfer.Attributes.Add("onclick", "return OpenTransfer('" & "../admin/transferrecord.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Opp&pluYR=" & lngOppId & "')")
                    btnSave.Attributes.Add("onclick", "return Save()")
                    btnSaveClose.Attributes.Add("onclick", "return Save()")
                    'btnUpdate.Attributes.Add("onclick", "return Save()")
                    btnUpdate.Attributes.Add("onclick", "return ValidateUpdateSettings()")
                    btnActdelete.Attributes.Add("onclick", "DeleteRecord(this); return false;")
                    hplSalesTemplate.Attributes.Add("onclick", "return OpenSalesTemplate('" & lngOppId & "');")
                    btnReOpenDeal.Attributes.Add("onclick", "ReOpenDeal(this); return false;")


                    Correspondence1.lngRecordID = OppID.Value

                    If OppType.Value = "1" Then
                        Correspondence1.Mode = 1
                        imgGridConfiguration.Attributes.Add("onclick", "return OpenSetting(26)")
                    Else
                        Correspondence1.Mode = 2
                        imgGridConfiguration.Attributes.Add("onclick", "return OpenSetting(129)")
                    End If

                    Correspondence1.bPaging = True
                    Correspondence1.CorrespondenceModule = 10
                    Correspondence1.getCorrespondance()

                    If OppType.Value = 1 And OppStatus.Value = 1 Then
                        Dim objOPP As New COpportunities
                        objOPP.DomainID = CCommon.ToLong(Session("DomainID"))
                        objOPP.OpportID = lngOppId

                        objCommon.DomainID = Session("DomainID")
                        Dim dtDoNotShowDropshipPOWindow As DataTable = objCommon.GetDomainSettingValue("bitDoNotShowDropshipPOWindow")

                        If Not dtDoNotShowDropshipPOWindow Is Nothing AndAlso dtDoNotShowDropshipPOWindow.Rows.Count > 0 Then
                            If Not CCommon.ToBool(dtDoNotShowDropshipPOWindow.Rows(0)("bitDoNotShowDropshipPOWindow")) Then
                                Dim ds As DataSet = objOPP.GetSODropshipItems()

                                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "OpenCreateDripshipPOWindow", "OpenCreateDopshipPOWindow(" & lngOppId & ");", True)
                                End If

                                Dim dsBackOrderItems As DataSet = objOPP.GetSOBackOrderItems()
                                If Not dsBackOrderItems Is Nothing AndAlso dsBackOrderItems.Tables.Count > 1 AndAlso dsBackOrderItems.Tables(1).Rows.Count > 0 Then
                                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "OpenCreateBackOrderPOWindow", "OpenCreateBackOrderPOWindow(" & lngOppId & ");", True)
                                End If
                            End If
                        Else
                            Throw New Exception("Not able to fetch do not show drop-ship po global settings value.")
                            Exit Sub
                        End If
                    End If

                    aHelp.Attributes.Add("onclick", "return OpenHelpPopUp('opportunity/frmopportunities.aspx?opptype=" & OppType.Value & "&oppstatus=" & OppStatus.Value & "')")
                Else
                    eventTarget = If((Me.Request("__EVENTTARGET") Is Nothing), String.Empty, Me.Request("__EVENTTARGET"))

                    If eventTarget = "btnRefreshProducsServices" Or eventTarget = "InvoiceAndShipItems" Or eventTarget = "CreateBill" Then
                        LoadSavedInformation(boolLoadOppInfo:=True)
                    Else
                        LoadSavedInformation()
                    End If
                End If

                If OppType.Value = 1 And OppStatus.Value = 0 Then
                    PersistTable.Load(strPageName:="frmOpportunitiesSOpp.aspx")
                ElseIf OppType.Value = 1 And OppStatus.Value = 1 Then
                    PersistTable.Load(strPageName:="frmOpportunitiesSO.aspx")

                ElseIf OppType.Value = 2 And OppStatus.Value = 0 Then
                    PersistTable.Load(strPageName:="frmOpportunitiesPOpp.aspx")

                ElseIf OppType.Value = 2 And OppStatus.Value = 1 Then
                    PersistTable.Load(strPageName:="frmOpportunitiesPO.aspx")

                End If

                If PersistTable.Count > 0 Then
                    If radOppTab.Tabs.FindTabByValue(PersistTable("index")) IsNot Nothing Then
                        radOppTab.Tabs.FindTabByValue(PersistTable("index")).Selected = True
                    Else
                        radOppTab.SelectedIndex = 0
                    End If
                End If

                If radOppTab.SelectedTab IsNot Nothing Then
                    If radOppTab.SelectedTab.Visible = False Then
                        radOppTab.SelectedIndex = 0
                        radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                    Else
                        radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                    End If
                Else
                    radOppTab.SelectedIndex = 0
                    radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                End If

                MileStone1._ModuleType = MileStone1.ModuleType.Opportunity
                MileStone1.OppID = lngOppId
                MileStone1.DivisionID = lngDivId
                If OppType.Value = 1 Then
                    GetUserRightsForPage(10, 3)
                    MileStone1.ProType = 0
                Else
                    GetUserRightsForPage(10, 9)
                    MileStone1.ProType = 2
                End If
                MileStone1.OppStatus = OppStatus.Value
                MileStone1.RightsForPage = m_aryRightsForPage
                lblTitle.Text = IIf(OppStatus.Value = 0, "Opportunity", "Order")
                lblTitleDetails.Text = IIf(CCommon.ToString(hdnOppname.Value) <> "", "(" & hdnOppname.Value & ")", "")

                If radOppTab.Tabs(0).Text = "Deal Details" Then
                    If OppType.Value = 1 And OppStatus.Value = 0 Then
                        radOppTab.FindTabByValue("Details").Text = "Sales Opportunity Details"
                        If radOppTab.FindTabByValue("ShippingAndPackaging") IsNot Nothing Then
                            radOppTab.FindTabByValue("ShippingAndPackaging").Visible = False
                        End If

                    ElseIf OppType.Value = 1 And OppStatus.Value = 1 Then
                        radOppTab.FindTabByValue("Details").Text = "Sales Order Details"
                        SetShippingChargesLabel()
                        If radOppTab.FindTabByValue("ShippingAndPackaging") IsNot Nothing Then
                            radOppTab.FindTabByValue("ShippingAndPackaging").Visible = True
                            If ddlShippingService.Items.Count = 0 Then OppBizDocs.LoadServiceTypes(0, ddlShippingService)
                            BindShippingDataGrid()

                        End If

                    ElseIf OppType.Value = 2 And OppStatus.Value = 0 Then
                        radOppTab.FindTabByValue("Details").Text = "Purchase Opportunity Details"
                        If radOppTab.FindTabByValue("ShippingAndPackaging") IsNot Nothing Then
                            radOppTab.FindTabByValue("ShippingAndPackaging").Visible = False
                        End If

                    ElseIf OppType.Value = 2 And OppStatus.Value = 1 Then
                        radOppTab.FindTabByValue("Details").Text = "Purchase Order Details"
                        If radOppTab.FindTabByValue("ShippingAndPackaging") IsNot Nothing Then
                            radOppTab.FindTabByValue("ShippingAndPackaging").Visible = False
                        End If

                    End If

                    If hdnStockTransfer.Value = "1" Then
                        radOppTab.FindTabByValue("Details").Text = "Stock Transfer"
                        radOppTab.FindTabByValue("Milestone").Visible = False
                        radOppTab.FindTabByValue("AssociatedContacts").Visible = False
                        radOppTab.FindTabByValue("AssociatedContacts").Visible = False
                        radOppTab.FindTabByValue("Correspondence").Visible = False
                        radOppTab.FindTabByValue("Collaboration").Visible = False
                        btnReOpenDeal.Visible = False
                        'lblDealCompletedDate.Visible = False
                    End If
                End If

                'setUserControlProperties()
                DisplayDynamicFlds()

                If OppType.Value = 1 Then
                    Dim m_aryRightsForSalesProfit() As Integer
                    m_aryRightsForSalesProfit = GetUserRightsForPage_Other(10, 21)

                    If m_aryRightsForSalesProfit(RIGHTSTYPE.VIEW) <> 0 Then
                        hplGrossProfitEstimate.Attributes.Add("onclick", "return openGrossProfitEstimate('" & lngOppId & "')")
                        hplGrossProfitEstimate.Visible = True
                    End If
                End If

                If OppType.Value = 1 And OppStatus.Value = 0 Then
                    imgOrder.Attributes.Add("onclick", "return OpenMirrorBizDoc('" & lngOppId & "','3',0)")
                    trShipping.Visible = False
                    HideRecurrence()

                ElseIf OppType.Value = 1 And OppStatus.Value = 1 Then
                    imgOrder.Attributes.Add("onclick", "return OpenMirrorBizDoc('" & lngOppId & "','1',0)")
                    trShipping.Visible = True

                ElseIf OppType.Value = 2 And OppStatus.Value = 0 Then
                    imgOrder.Attributes.Add("onclick", "return OpenMirrorBizDoc('" & lngOppId & "','4',0)")
                    trShipping.Visible = False
                    HideRecurrence()

                ElseIf OppType.Value = 2 And OppStatus.Value = 1 Then
                    imgOrder.Attributes.Add("onclick", "return OpenMirrorBizDoc('" & lngOppId & "','2',0)")
                    trShipping.Visible = False
                    HideRecurrence()

                End If

                hrfTransactionDetail.Attributes.Add("onclick", "return OpenTransactionDetails('" & lngOppId & "')")
                hrfAutomationExecutionLog.Attributes.Add("onclick", "return OpenAutomationExecutionLog('" & lngOppId & "')")

                btnAddEditOrder.Attributes.Add("onclick", "return AddEditOrder(" & lngOppId & " ," & OppType.Value & "," & DivID.Value & ")")

                Dim SValbuilder As New StringBuilder
                Dim strValidation As String = ""

                SValbuilder.AppendLine("function validateFixedFields() {")
                SValbuilder.AppendLine("if(RequiredField('txtShippingRate','txtShippingRate','Please provide shipping rate!')==false) {return false;} ")
                SValbuilder.AppendLine("if(IsNumeric('txtShippingRate','txtShippingRate','Please provide numeric value!')==false) {return false;} ")
                SValbuilder.AppendLine("if(RequiredField('txtMarkupShippingCharge','txtMarkupShippingCharge','Please provide markup shipping charge!')==false) {return false;} ")
                SValbuilder.AppendLine("if(IsNumeric('txtMarkupShippingCharge','txtMarkupShippingCharge','Please provide numeric value!')==false) {return false;} ")
                SValbuilder.AppendLine("if(RangeValidator('txtMarkupShippingCharge','txtMarkupShippingCharge','" & 0.1 & "','" & 99.99 & "','" & "Enter markup shipping rate in between 0.1 to 99.99!" & "')==false) {return false;} ")
                SValbuilder.AppendLine("return true;}")

                strValidation = SValbuilder.ToString()
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "FixedValidation", strValidation, True)

                If tblRecurInsert.Visible AndAlso chkEnableRecurring.Checked Then
                    trRecurring2.Style.Remove("display")
                    trRecurring3.Style.Remove("display")
                End If

                'hide order setting panel if no child rows are visible
                If trShipping.Visible = True Or
                   trLandedCost.Visible = True Or
                   tblRecurInsert.Visible = True Or
                   tblRecurDetail.Visible = True Or
                   tblCurrency.Visible = True Or
                   tblItemReceivedDate.Visible = True Then
                    tdOrderSetting.Visible = True
                Else
                    tdOrderSetting.Visible = False
                End If

                eventTarget = If(Page.Request("__EVENTTARGET") = Nothing, String.Empty, Page.Request("__EVENTTARGET"))

                If eventTarget = "InvoiceAndShipItems" Then
                    CreateInvoiceAndFulfillmentOrder()
                ElseIf eventTarget = "CreateBill" Then
                    CreateBill()
                End If

                If Not Page.IsPostBack Then
                    If GetQueryStringVal("fromEditBizDoc") = "1" Then
                        If Not radOppTab.FindTabByValue("BizDocs") Is Nothing Then
                            radOppTab.FindTabByValue("BizDocs").Selected = True
                            IframeBiz.Src = "../opportunity/frmAddBizDocs.aspx?OpID=" & GetQueryStringVal("opId") & "&OppBizId=" & GetQueryStringVal("OppBizId") & "&OppType=" & OppType.Value
                        End If
                    End If
                End If
                If GetQueryStringVal("MileStoneId").Length > 0 Then
                    If Not radOppTab.FindTabByValue("Milestone") Is Nothing Then
                        radOppTab.FindTabByValue("Milestone").Selected = True
                        radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                    End If
                End If

                If GetQueryStringVal("ForPriceMarginApproval").Length > 0 Then
                    If Not radOppTab.FindTabByValue("ProductsServices") Is Nothing Then
                        radOppTab.FindTabByValue("ProductsServices").Selected = True
                        radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                    End If
                End If

                If Not String.IsNullOrEmpty(GetQueryStringVal("SelectedTab")) Then
                    If Not radOppTab.FindTabByValue(GetQueryStringVal("SelectedTab")) Is Nothing Then
                        radOppTab.FindTabByValue(GetQueryStringVal("SelectedTab")).Selected = True
                        radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                    End If
                End If

                LoadEDIFieldsForOrder()
                If (numOppItemCodeEDI > 0) Then
                    LoadEDIFieldsForOppItem(numOppItemCodeEDI)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub ShowDocuments(ByRef tblDocuments As HtmlTable, ByVal dtDocLinkName As DataTable, ByVal dtDisplayFields As DataTable, ByVal intSection As Integer)
            Try
                tblDocuments.Controls.Clear()
                Dim countId = 0
                Dim tr As HtmlTableRow
                For Each drDocument In dtDocLinkName.Rows
                    If (intSection = 0 AndAlso (CCommon.ToLong(drDocument("numFormFieldGroupId")) = 0 Or dtDisplayFields.Select("numFormFieldGroupId=" & CCommon.ToLong(drDocument("numFormFieldGroupId"))).Length = 0)) Or
                        (intSection <> 0 AndAlso CCommon.ToLong(drDocument("numFormFieldGroupId")) = intSection) Then
                        If countId = 0 Then
                            tr = New HtmlTableRow
                        End If

                        If CCommon.ToString(drDocument("cUrlType")) = "L" Then
                            Dim strOriginalFileName = CCommon.ToString(drDocument("VcDocName")) & CCommon.ToString(drDocument("vcFileType"))
                            Dim strDocName = CCommon.ToString(drDocument("VcFileName"))
                            Dim fileID = CCommon.ToString(drDocument("numGenericDocID"))
                            Dim strFileLogicalPath = String.Empty
                            If File.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName) Then
                                Dim f As FileInfo = New FileInfo(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName)
                                Dim strFileSize As String = f.Length.ToString
                                strFileLogicalPath = CCommon.GetDocumentPath(Session("DomainID")) & HttpUtility.UrlEncode(strDocName).Replace("+", " ")
                            End If

                            Dim docLink As New HyperLink

                            Dim imgDelete As New ImageButton
                            imgDelete.ImageUrl = "../images/Delete24.png"
                            imgDelete.Height = 13
                            imgDelete.ToolTip = "Delete"
                            imgDelete.ID = "imgDelete" + CCommon.ToString(drDocument("numGenericDocID"))
                            imgDelete.Attributes.Add("onclick", "DeleteDocumentOrLink(" & fileID & ");return false;")

                            Dim Space As New LiteralControl
                            Space.Text = "&nbsp;"

                            docLink.Text = strOriginalFileName
                            docLink.NavigateUrl = strFileLogicalPath
                            docLink.ID = "hpl" + CCommon.ToString(drDocument("numGenericDocID"))
                            docLink.Target = "_blank"
                            Dim td As New HtmlTableCell
                            td.Style.Add("border-bottom", "0px !important")
                            td.Style.Add("padding-left", "5px")
                            td.Style.Add("padding-right", "5px")
                            td.Controls.Add(docLink)
                            td.Controls.Add(Space)
                            td.Controls.Add(imgDelete)
                            tr.Controls.Add(td)
                        ElseIf CCommon.ToString(drDocument("cUrlType")) = "U" Then
                            Dim uriBuilder As UriBuilder = New UriBuilder(CCommon.ToString(drDocument("VcFileName")))

                            Dim link As New HyperLink
                            Dim imgLinkDelete As New ImageButton
                            imgLinkDelete.ImageUrl = "../images/Delete24.png"
                            imgLinkDelete.Height = 13
                            imgLinkDelete.ToolTip = "Delete"
                            imgLinkDelete.ID = "imgLinkDelete" + CCommon.ToString(drDocument("numGenericDocID"))
                            imgLinkDelete.Attributes.Add("onclick", "DeleteDocumentOrLink(" & CCommon.ToString(drDocument("numGenericDocID")) & ");return false;")

                            link.Text = CCommon.ToString(drDocument("VcDocName"))
                            link.NavigateUrl = uriBuilder.Uri.AbsoluteUri
                            link.ID = "hplLink" + CCommon.ToString(drDocument("numGenericDocID"))
                            link.Target = "_blank"

                            Dim td As New HtmlTableCell
                            td.Style.Add("border-bottom", "0px !important")
                            td.Style.Add("padding-left", "5px")
                            td.Style.Add("padding-right", "5px")
                            td.Controls.Add(link)
                            td.Controls.Add(imgLinkDelete)
                            tr.Controls.Add(td)
                        End If

                        countId = countId + 1

                        If countId = 3 Then
                            countId = 0
                            tblDocuments.Controls.Add(tr)
                        End If
                    End If
                Next

                If countId <> 0 Then
                    tblDocuments.Controls.Add(tr)
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Protected Sub btnDeleteDocOrLink_Click(sender As Object, e As EventArgs)
            Dim objDocuments As New DocumentList()
            objDocuments.GenDocID = CCommon.ToLong(hdnFileId.Value)
            objDocuments.DeleteDocumentOrLink()
            LoadControls()
        End Sub

        Private Sub HideRecurrence()
            Try
                tblRecurInsert.Visible = False
                tblRecurDetail.Visible = False
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub FillNetDays()
            Try
                Dim objOpp As New BusinessLogic.Opportunities.OppotunitiesIP
                Dim dtTerms As New DataTable
                With objOpp
                    .DomainID = Session("DomainID")
                    .TermsID = 0
                    .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    dtTerms = .GetTerms()
                End With

                If dtTerms IsNot Nothing Then
                    ddlNetDays.DataSource = dtTerms
                    ddlNetDays.DataTextField = "vcTerms"
                    ddlNetDays.DataValueField = "numTermsID"
                    ddlNetDays.DataBind()
                    ddlNetDays.Items.Insert(0, New ListItem("--Select One --", "0"))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Private Sub BindCOA()
        '    Try
        '        Dim objCOA As New ChartOfAccounting

        '        Dim dt, dt1 As DataTable
        '        objCOA.DomainID = Session("DomainId")
        '        objCOA.AccountCode = "0103" 'Income
        '        dt = objCOA.GetParentCategory()

        '        objCOA.AccountCode = "0104" 'Expense
        '        dt1 = objCOA.GetParentCategory()
        '        dt.Merge(dt1)

        '        Dim item As ListItem
        '        For Each dr As DataRow In dt.Rows
        '            item = New ListItem()
        '            item.Text = dr("vcAccountName")
        '            item.Value = dr("numAccountID")
        '            item.Attributes("OptionGroup") = dr("vcAccountType")
        '            ddlDiscActType.Items.Add(item)
        '        Next
        '        ddlDiscActType.Items.Insert(0, New ListItem("--Select One --", "0"))
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Dim IsServiceItemInOrder As Boolean = False
        Sub LoadSavedInformation(Optional ByVal boolLoadOppInfo As Boolean = False, Optional ByVal tempEvntId As Long = 0)
            Try
                If boolLoadOppInfo Then
                    'objCommon.sb_FillComboFromDBwithSel(ddlContactRole, 26, Session("DomainID"))
                    Dim dtDetails As DataTable
                    objOpportunity.OpportunityId = lngOppId
                    objOpportunity.DomainID = Session("DomainID")
                    objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    objOpportunity.ItemCode = lngShippingItemCode

                    dtDetails = objOpportunity.OpportunityDTL.Tables(0)
                    If dtDetails.Rows.Count > 0 Then
                        If dtDetails.Rows(0).Item("tintOppType") = 1 Then
                            GetUserRightsForPage(10, 3)
                            If CCommon.ToLong(dtDetails.Rows(0).Item("numAssignedTo")) <> CCommon.ToLong(Session("UserContactID")) AndAlso CCommon.ToLong(Session("UserDivisionID")) <> CCommon.ToLong(dtDetails.Rows(0).Item("numDivisionID")) Then
                                GetUserRightsForRecord(10, 3, CCommon.ToLong(dtDetails.Rows(0).Item("numRecOwner")), CCommon.ToLong(dtDetails.Rows(0).Item("numTerID")))
                            End If
                            lblCustomerType.Text = "Customer"
                            hplSalesTemplate.Text = "<img src='../images/Icon/item.png' class='btn-icon-template' />&nbsp;&nbsp; Item Templates"
                        ElseIf dtDetails.Rows(0).Item("tintOppType") = 2 Then
                            GetUserRightsForPage(10, 3)
                            If CCommon.ToLong(dtDetails.Rows(0).Item("numAssignedTo")) <> CCommon.ToLong(Session("UserContactID")) Then
                                GetUserRightsForRecord(10, 9, CCommon.ToLong(dtDetails.Rows(0).Item("numRecOwner")), CCommon.ToLong(dtDetails.Rows(0).Item("numTerID")))
                            End If
                            lblCustomerType.Text = "Vendor"
                            hplSalesTemplate.Text = "<img src='../images/Icon/item.png' class='btn-icon-template' />&nbsp;&nbsp; Purchase Templates"
                            hrfTransactionDetail.Visible = False
                            btnBarcode.Visible = True
                        End If
                        MileStone1.BusinesProcessId = CCommon.ToLong(dtDetails.Rows(0).Item("numBusinessProcessID"))
                        MileStone1.BusinesProcessName = CCommon.ToString(dtDetails.Rows(0).Item("vcProcessName"))
                        'imgShippingReport.Attributes.Add("onclick", "openReportList(" & lngOppId & "," & CCommon.ToDouble(dtDetails.Rows(0).Item("numDivisionID")) & ")")
                        OppType.Value = dtDetails.Rows(0).Item("tintOppType")
                        OppStatus.Value = dtDetails.Rows(0).Item("tintOppStatus")
                        OppID.Value = lngOppId
                        DivID.Value = dtDetails.Rows(0).Item("numDivisionID")
                        hdnCurrencyID.Value = CCommon.ToLong(dtDetails.Rows(0).Item("numCurrencyID"))
                        lngDivId = dtDetails.Rows(0).Item("numDivisionID")
                        OrderCreatedDate = dtDetails.Rows(0).Item("bintCreatedDate")

                        Session("OrderCreatedDate") = OrderCreatedDate

                        LoadAssociatedContacts(lngDivId)

                        If OppType.Value = "1" Then
                            'Check if user has right to edit unit price
                            Dim m_aryRightsForEditUnitPrice As Integer() = GetUserRightsForPage_Other(10, 31)
                            If m_aryRightsForEditUnitPrice(RIGHTSTYPE.UPDATE) = 0 Then
                                hdnEditUnitPriceRight.Value = "False"
                            End If
                        End If

                        hdnPPVariance.Value = dtDetails.Rows(0).Item("bitPPVariance")
                        hfPoppName.Value = dtDetails.Rows(0).Item("vcPoppName")

                        hdnCountry.Value = CCommon.ToDouble(dtDetails.Rows(0).Item("numShipCountry"))
                        hdnState.Value = CCommon.ToDouble(dtDetails.Rows(0).Item("numShipState"))
                        hdnPostal.Value = CCommon.ToString(dtDetails.Rows(0).Item("vcPostalCode"))

                        hplCustomer.Text = dtDetails.Rows(0).Item("vcCompanyName")
                        If CCommon.ToString(dtDetails.Rows(0).Item("numContactId")) <> "" Then
                            lblContactName.Text = dtDetails.Rows(0).Item("numContactId")
                        Else
                            lblContactName.Text = ""
                        End If
                        If CCommon.ToString(dtDetails.Rows(0).Item("PhoneExtension")) <> "" Then
                            lblPhone.Text = dtDetails.Rows(0).Item("Phone")
                        Else
                            lblPhone.Text = ""
                        End If
                        If CCommon.ToString(dtDetails.Rows(0).Item("PhoneExtension")) <> "" Then
                            lblPhone.Text = lblPhone.Text & "(" & CCommon.ToString(dtDetails.Rows(0).Item("PhoneExtension")) & ")"
                        End If
                        If CCommon.ToString(dtDetails.Rows(0).Item("vcEmail")) <> "" Then
                            btnSendEmail.Visible = True
                            btnSendEmail.Attributes.Add("onclick", "return OpenEmail('" & CCommon.ToString(dtDetails.Rows(0).Item("vcEmail")) & "','')")
                        Else
                            btnSendEmail.Visible = False
                        End If
                        If CCommon.ToString(dtDetails.Rows(0).Item("vcCompanyType")) <> "" Then
                            lblRelationCustomerType.Text = "(" & dtDetails.Rows(0).Item("vcCompanyType") & ")"
                        Else
                            lblRelationCustomerType.Text = ""
                        End If

                        hdnContactID.Value = dtDetails.Rows(0).Item("ContactID")
                        hdnContactEmail.Value = dtDetails.Rows(0).Item("vcEmail")

                        hdnCRMType.Value = dtDetails.Rows(0).Item("tintCRMType")
                        hdnRecordOwner.Value = CCommon.ToLong(dtDetails.Rows(0).Item("numRecOwner"))
                        hdnTerrID.Value = CCommon.ToLong(dtDetails.Rows(0).Item("numTerID"))
                        chkUseCustomerShippingAccount.Checked = CCommon.ToBool(dtDetails.Rows(0).Item("bitUseShippersAccountNo"))
                        hdnShipperAccountNo.Value = CCommon.ToString(dtDetails.Rows(0).Item("vcShippersAccountNo"))

                        If radOppTab.FindTabByValue("ShippingAndPackaging") IsNot Nothing Then
                            radOppTab.FindTabByValue("ShippingAndPackaging").Visible = True
                            OppBizDocs.LoadServiceTypes(0, ddlShippingService, CCommon.ToLong(Session("DefaultShippingCompany")))
                        End If

                        chkMarkupShippingCharges.Checked = CCommon.ToBool(dtDetails.Rows(0).Item("bitUseMarkupShippingRate"))
                        txtMarkupShippingCharge.Text = CCommon.ToDecimal(dtDetails.Rows(0).Item("numMarkupShippingRate"))
                        If ddlShippingService.Items.FindByValue(CCommon.ToInteger(dtDetails.Rows(0).Item("numShippingService"))) IsNot Nothing Then
                            ddlShippingService.ClearSelection()
                            ddlShippingService.Items.FindByValue(CCommon.ToInteger(dtDetails.Rows(0).Item("numShippingService"))).Selected = True
                        End If

                        If dtDetails.Rows(0).Item("tintCRMType") = 0 Then
                            hplCustomer.NavigateUrl = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=oppdetail&DivID=" & dtDetails.Rows(0).Item("numDivisionID") & "&opId=" & lngOppId & "&frm1=" & GetQueryStringVal("frm")

                        ElseIf dtDetails.Rows(0).Item("tintCRMType") = 1 Then
                            hplCustomer.NavigateUrl = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=oppdetail&DivID=" & dtDetails.Rows(0).Item("numDivisionID") & "&opId=" & lngOppId & "&frm1=" & GetQueryStringVal("frm")

                        ElseIf dtDetails.Rows(0).Item("tintCRMType") = 2 Then
                            hplCustomer.NavigateUrl = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=oppdetail&klds+7kldf=fjk-las&DivId=" & dtDetails.Rows(0).Item("numDivisionID") & "&opId=" & lngOppId & "&frm1=" & GetQueryStringVal("frm")
                        End If


                        'hplPrice.Attributes.Add("onclick", "return openItem('" & radCmbItem.ClientID & "','" & txtunits.ClientID & "','" & dtDetails.Rows(0).Item("numDivisionID") & "','" & txtSerialize.ClientID & "','" & OppType.Value & "')")
                        'LoadBusinessProcess()
                        If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                            Response.Redirect("../admin/authentication.aspx?mesg=AC")
                        Else
                            If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                                btnSave.Visible = False
                                ibChangeCustomer.Visible = False
                                btnSaveClose.Visible = False
                                btnUpdate.Visible = False
                            End If
                            If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnActdelete.Visible = False
                        End If

                        If CCommon.ToBool(dtDetails.Rows(0).Item("bitAuthorativeBizDocAdded")) Then
                            ibChangeCustomer.Visible = False
                        End If

                        lblLastModifiedBy.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("ModifiedBy")), "", dtDetails.Rows(0).Item("ModifiedBy"))
                        lblCreatedBy.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("CreatedBy")), "", dtDetails.Rows(0).Item("CreatedBy"))
                        lblRecordOwner.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("RecordOwner")), "", dtDetails.Rows(0).Item("RecordOwner"))
                        txtrecOwner.Text = CCommon.ToLong(dtDetails.Rows(0).Item("numRecOwner"))
                        ViewState("AssignedTo") = CCommon.ToLong(dtDetails.Rows(0).Item("numAssignedTo"))

                        MileStone1.ProjectRecOwner = dtDetails.Rows(0).Item("numRecOwner")

                        If Not IsDBNull(dtDetails.Rows(0).Item("numParentOppID")) Then
                            hplOppLinked.Text = "<strong>Source :</strong> " & dtDetails.Rows(0).Item("ParentOppName")
                            hplOppLinked.NavigateUrl = "../opportunity/frmOpportunities.aspx?frm=opportunitylist&OpID=" & dtDetails.Rows(0).Item("numParentOppID")
                        End If
                        If Not IsDBNull(dtDetails.Rows(0).Item("numParentProjectID")) Then
                            hplOppLinked.Text = "<strong>Source :</strong> " & dtDetails.Rows(0).Item("vcProjectName")
                            hplOppLinked.NavigateUrl = "../projects/frmProjects.aspx?frm=OPPDetails&ProId=" & dtDetails.Rows(0).Item("numParentProjectID") & "&OpID=" & lngOppId.ToString
                        End If
                        If Not IsDBNull(dtDetails.Rows(0).Item("Source")) Then
                            If (dtDetails.Rows(0).Item("Source").ToString().Trim().Length > 0) Then
                                hplOppLinked.Text = "<strong>Source :</strong> " & dtDetails.Rows(0).Item("Source")
                                hplOppLinked.NavigateUrl = "#"
                            End If
                        End If
                        If Not IsDBNull(dtDetails.Rows(0).Item("ChildCount")) Then
                            Dim intCount As Integer = CInt(dtDetails.Rows(0).Item("ChildCount").ToString()) + CInt(dtDetails.Rows(0).Item("RecurringChildCount").ToString())
                            If (intCount > 0) Then
                                BindChildGrid()
                            End If
                        End If
                        If dtDetails.Rows(0).Item("tintOppStatus") = 1 Then
                            ViewState("Deal") = "True"
                            radOppTab.Tabs(0).Text = "Deal Details"
                            lblDealCompletedDate.Text = IIf(CDate(dtDetails.Rows(0).Item("bintAccountClosingDate")).Year = 1900, "", "<strong>Deal Completed Date :</strong> " & FormattedDateFromDate(dtDetails.Rows(0).Item("bintAccountClosingDate"), Session("DateFormat")))
                            hplCreateOpp.Visible = True
                            If dtDetails.Rows(0).Item("tintOppType") = 1 Then
                                hplCreateOpp.Text = "<img src='../images/Icon/Purchase%20order.png' class='btn-icon-template'>&nbsp;&nbsp;Purchase Orders" & If(CCommon.ToLong(dtDetails.Rows(0).Item("ChildCount")) > 0, "&nbsp;&nbsp;<span style='color:blue;' onclick='return openChild(" & lngOppId.ToString() & ")'>(" & CCommon.ToLong(dtDetails.Rows(0).Item("ChildCount")) & ")", "") & "</span>"
                            Else : hplCreateOpp.Text = "<img src='../images/Icon/Sales%20order.png' class='btn-icon-template'>&nbsp;&nbsp;Sales Orders" & If(CCommon.ToLong(dtDetails.Rows(0).Item("ChildCount")) > 0, "&nbsp;&nbsp;<span style='color:blue;' onclick='return openChild(" & lngOppId.ToString() & ")'>(" & CCommon.ToLong(dtDetails.Rows(0).Item("ChildCount")) & ")", "") & "</span>"
                            End If
                        End If

                        hftintshipped.Value = dtDetails.Rows(0).Item("tintshipped")
                        If dtDetails.Rows(0).Item("tintOppStatus") = 0 Then
                            btnAddEditOrder.Visible = True
                            btnRecalculatePrice.Visible = True
                            btnUpdateLabel.Visible = True
                            lkbDelete.Visible = True
                        End If
                        If dtDetails.Rows(0).Item("tintOppStatus") = 1 And dtDetails.Rows(0).Item("tintshipped") = 0 Then
                            btnAddEditOrder.Visible = True
                            btnRecalculatePrice.Visible = True
                            lkbDelete.Visible = True
                            btnUpdateLabel.Visible = True
                            btnReceivedOrShipped.Visible = True
                            If m_aryRightsForClosingDeal(RIGHTSTYPE.VIEW) = 0 Then btnReceivedOrShipped.Visible = False
                            btnReceivedOrShipped.Attributes.Add("onclick", "return AlertMsg()")

                            If dtDetails.Rows(0).Item("tintOppType") = "2" AndAlso Not CCommon.ToBool(dtDetails.Rows(0).Item("bitStockTransfer")) Then
                                btnUnreceive.Visible = True
                            End If
                        End If
                        If dtDetails.Rows(0).Item("tintOppStatus") = 1 And dtDetails.Rows(0).Item("tintshipped") = 1 Then
                            lblReturnLegend.Visible = True
                            btnSave.Enabled = False
                            btnSaveClose.Enabled = False
                            btnUpdate.Enabled = False
                            btnReOpenDeal.Visible = True

                            If m_aryRightsForReOpenDeal(RIGHTSTYPE.VIEW) = 0 Then btnReOpenDeal.Visible = False

                            btnActdelete.Visible = False
                            lblDealCompletedDate.Visible = True
                        Else
                            btnReOpenDeal.Visible = False
                            lblDealCompletedDate.Visible = False
                        End If

                        'If OppType.Value = 1 AndAlso OppStatus.Value = 1 And Not CCommon.ToString(dtDetails.Rows(0).Item("tintshipped")) Then
                        '    lkbDemoteToOpportunity.Visible = True
                        'End If

                        If dtDetails.Rows(0).Item("bitStockTransfer") = True Then
                            hdnStockTransfer.Value = "1"
                            btnReOpenDeal.Visible = False
                        End If

                        LoadDataToSession()
                        lblTotalWeight.Text = TotalItemWeight

                        hpClone.Attributes.Add("OnClick", "return OpenSalesOrderClone(" & lngOppId & "," & dtDetails.Rows(0).Item("tintOppType") & "," & dtDetails.Rows(0).Item("numDivisionID") & ")")

                        If Not IsDBNull(dtDetails.Rows(0).Item("numBillingDays")) Then
                            If Not ddlNetDays.Items.FindByValue(dtDetails.Rows(0).Item("numBillingDays")) Is Nothing Then
                                ddlNetDays.Items.FindByValue(dtDetails.Rows(0).Item("numBillingDays")).Selected = True
                            End If
                        End If

                        If ddlSalesTax.Items.FindByValue(CCommon.ToShort(dtDetails.Rows(0).Item("tintTaxOperator"))) IsNot Nothing Then
                            ddlSalesTax.Items.FindByValue(CCommon.ToShort(dtDetails.Rows(0).Item("tintTaxOperator"))).Selected = True
                        End If

                        lblForeignCurr.Text = "1 " & CCommon.ToString(dtDetails.Rows(0).Item("varCurrSymbol"))
                        txtExchangeRate.Text = CCommon.ToDecimal(dtDetails.Rows(0).Item("fltExchangeRate"))


                        If Session("MultiCurrency") = True Then
                            If Session("BaseCurrencyID") = CCommon.ToString(dtDetails.Rows(0).Item("numCurrencyID")) Or CCommon.ToString(dtDetails.Rows(0).Item("numCurrencyID")) = 0 Then
                                tblCurrency.Visible = False
                            Else
                                lblBaseCurr.Text = CCommon.ToString(Session("Currency")).Trim()
                                tblCurrency.Visible = True
                            End If
                        Else
                            tblCurrency.Visible = False
                        End If

                        If OppType.Value = 2 AndAlso OppStatus.Value = 1 Then
                            tblItemReceivedDate.Visible = True
                            calItemReceivedDate.SelectedDate = dtDetails.Rows(0)("dtItemReceivedDate").ToString
                        End If

                        If OppType.Value = 2 AndAlso CCommon.ToBool(Session("IsLandedCost")) Then
                            trLandedCost.Visible = True
                            lblLanedCost.Text = String.Format("{0} {1}", ReturnMoney(CCommon.ToDecimal(dtDetails.Rows(0).Item("monLandedCostTotal"))), CCommon.ToString(dtDetails.Rows(0).Item("vcLanedCost")))

                            hplLandedCost.Attributes.Add("onclick", "return openLanedCost(" & lngOppId & ");")
                        End If


                        'If recurrence is already created for authorative invoice in sales order then user can not 
                        'create recurrence for sales order
                        If CCommon.ToString(dtDetails.Rows(0)("vcRecurrenceType")) = "Invoice" Then
                            hdnRecurrenceType.Value = "Invoice"
                            tblRecurInsert.Visible = False
                            tblRecurDetail.Visible = False
                        Else
                            'If recurrence is already created then controls of creating recurrence will be hidden and 
                            'recurrence detail gird is visible from which user can disable recurrence
                            If CCommon.ToString(dtDetails.Rows(0)("numRecConfigID")) <> "0" Then
                                hdnRecurrenceType.Value = "Sales Order"
                                tblRecurInsert.Visible = False
                                tblRecurDetail.Visible = True

                                hdnRecConfigID.Value = CCommon.ToString(dtDetails.Rows(0)("numRecConfigID"))
                                lblFrequency.Text = CCommon.ToString(dtDetails.Rows(0)("vcFrequency"))
                                lblStartDate.Text = CCommon.ToString(dtDetails.Rows(0)("dtStartDate"))
                                lblEndDate.Text = CCommon.ToString(dtDetails.Rows(0)("dtEndDate"))

                                If CCommon.ToBool(dtDetails.Rows(0)("bitDisabled")) Then
                                    ddlRecurStatus.SelectedValue = 2
                                    ddlRecurStatus.Enabled = False
                                    lblRecurStatus.Visible = True
                                Else
                                    ddlRecurStatus.SelectedValue = 1
                                    ddlRecurStatus.Enabled = True
                                    lblRecurStatus.Visible = False
                                End If
                            Else
                                tblRecurInsert.Visible = True
                                tblRecurDetail.Visible = False
                            End If
                        End If

                        If CCommon.ToBool(dtDetails.Rows(0)("bitRecurred")) Then
                            Dim objBizRecurrence As New BizRecurrence
                            Dim dt As DataTable = objBizRecurrence.GetParentDetail(lngOppId, 1)

                            If Not dt Is Nothing AndAlso dt.Rows.Count > 0 AndAlso CCommon.ToLong(dt.Rows(0)("RowNo")) > 0 Then
                                trParentOrder.Visible = True
                                hplParentOrder.NavigateUrl = "javascript:OpenOpp(" & CCommon.ToLong(dt.Rows(0)("numParentOppID")) & ")"
                                hplParentOrder.Text = CCommon.ToString(dt.Rows(0)("RowNo")) & " recurrence from " & CCommon.ToString(dt.Rows(0)("Name"))
                            End If

                            hdnRecurred.Value = "Recurred"
                            HideRecurrence()
                        End If

                        'Commented by Neelam - Obsolete functionality
                        'hplDocumentCount.Attributes.Add("onclick", "return OpenDocuments(" & lngOppId & ")")
                        'hplDocumentCount.Text = "(" & CCommon.ToLong(dtDetails.Rows(0)("DocumentCount")) & ")"
                        'imgOpenDocument.Attributes.Add("onclick", "return OpenDocuments(" & lngOppId & ")")
                        imgOpenDocument.Attributes.Add("onclick", "OpenDocumentFiles(" & lngOppId.ToString() & ");return false;")
                        hplOpenDocument.Attributes.Add("onclick", "OpenDocumentFiles(" & lngOppId.ToString() & ");return false;")

                    End If
                End If
                dsTemp = ViewState("SOPOItems")
                If m_aryRightsForItems(RIGHTSTYPE.VIEW) <> 0 AndAlso dsTemp IsNot Nothing Then BindItems(dsTemp)

                LoadControls(tempEvntId)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        'Modified by Neelam on 02/16/2018 - Added functionality to attach files*/
        Protected Sub btnAttach_Click(sender As Object, e As EventArgs)
            Try
                If (fileupload.PostedFile.ContentLength > 0) Then
                    Dim strFName As String()
                    Dim strFilePath, strFileName, strFileType As String
                    strFileName = Path.GetFileName(fileupload.PostedFile.FileName)
                    If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                        Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
                    End If
                    Dim newFileName As String = Path.GetFileNameWithoutExtension(strFileName) & DateTime.UtcNow.ToString("yyyyMMddhhmmssfff") & Path.GetExtension(strFileName)
                    strFilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & newFileName
                    strFName = Split(strFileName, ".")
                    strFileType = "." & strFName(strFName.Length - 1)                     'Getting the Extension of the File
                    fileupload.PostedFile.SaveAs(strFilePath)
                    UploadFile("L", strFileType, newFileName, strFName(0), "", 0, 0)
                    Dim script As String = "function f(){$find(""" + RadWinDocumentFiles.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, True)
                    Response.Redirect(Request.RawUrl, False)
                Else
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "NoFiles", "alert('Please attach a file to upload.');return false;", True)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub btnLinkClose_Click(sender As Object, e As EventArgs)
            UploadFile("U", "", txtLinkURL.Text, txtLinkName.Text, "", 0, 0)
            Response.Redirect(Request.RawUrl, False)
        End Sub

        'Modified by Neelam on 02/16/2018 - Added function to save File/Link data in the database*/
        Sub UploadFile(ByVal URLType As String, ByVal strFileType As String, ByVal strFileName As String, ByVal DocName As String, ByVal DocDesc As String,
                       ByVal DocumentStatus As Long, DocCategory As Long)
            Try
                If CCommon.ToLong(OppID.Value) > 0 Then
                    Dim arrOutPut As String()
                    Dim objDocuments As New DocumentList()
                    With objDocuments
                        .DomainID = Session("DomainId")
                        .UserCntID = Session("UserContactID")
                        .UrlType = URLType
                        .DocumentStatus = DocumentStatus
                        .DocCategory = DocCategory
                        .FileType = strFileType
                        .DocName = DocName
                        .DocDesc = DocDesc
                        .FileName = strFileName
                        .FormFieldGroupId = IIf(URLType = "L", ddlSectionFile.SelectedValue, ddlSectionLink.SelectedValue)
                        .DocumentSection = "O"
                        .RecID = CCommon.ToLong(OppID.Value)
                        .DocumentType = IIf(lngDivId > 0, 2, 1) '1=generic,2=specific
                        arrOutPut = .SaveDocuments()
                    End With
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub LoadDataToSession()
            Try
                Dim dtItems As DataTable
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.UserCntID = Session("UserContactID")
                objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dsTemp = objOpportunity.GetOrderItemsProductServiceGrid
                dtItems = dsTemp.Tables(0)
                Dim k As Integer

                TotalItemWeight = 0
                For k = 0 To dtItems.Rows.Count - 1
                    If CCommon.ToBool(dtItems.Rows(k).Item("bitDropShip")) = True Then

                    Else
                        TotalItemWeight = TotalItemWeight + CCommon.ToDecimal(dtItems.Rows(k)("fltWeight"))
                    End If

                    If CCommon.ToLong(dtItems.Rows(k)("numItemCode")) = lngShippingItemCode Then
                        dcShippingCharge = CCommon.ToDecimal(dtItems.Rows(k)("monTotAmount"))
                    End If
                Next

                Dim dtItem As DataTable
                'Dim dtSerItem As DataTable
                Dim dtChildItems As DataTable
                dtItem = dsTemp.Tables(0)
                'dtSerItem = dsTemp.Tables(1)
                'dtChildItems = dsTemp.Tables(2)
                dtItem.TableName = "Item"
                'dtSerItem.TableName = "SerialNo"
                'dtChildItems.TableName = "ChildItems"
                dtItem.PrimaryKey = New DataColumn() {dsTemp.Tables(0).Columns("numoppitemtCode")}

                If dtItem.Columns("numUOM") Is Nothing Then
                    dtItem.Columns.Add("numUOM")
                End If

                'Response.Write(dsTemp.GetXml)
                'If dtSerItem.ParentRelations.Count = 0 Then dsTemp.Relations.Add("Item", dsTemp.Tables(0).Columns("numoppitemtCode"), dsTemp.Tables(1).Columns("numoppitemtCode"))
                dsTemp.AcceptChanges()
                ViewState("SOPOItems") = dsTemp
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        'Sub BindRepeaterControl()
        '    setUserControlProperties()
        '    MileStone.BindRepeaterControl()
        'End Sub

        Public Function LoadPickPackShipGrid()
            Try
                Dim objOppBizDocs As New OppBizDocs
                objOppBizDocs.BizDocId = 29397
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.OppId = lngOppId
                dtPickPackItems = objOppBizDocs.GetOppItemsForPickPackShip()

                objOppBizDocs = New OppBizDocs()
                objOppBizDocs.BizDocId = 296
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.OppId = lngOppId
                dtFullFilmentItems = objOppBizDocs.GetOppItemsForPickPackShip()

                objOppBizDocs = New OppBizDocs()
                objOppBizDocs.BizDocId = 287
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.OppId = lngOppId
                dtInvoicedItems = objOppBizDocs.GetOppItemsForPickPackShip()
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Sub LoadControls(Optional ByVal typeEvent As Long = 0)
            Try
                tblMain.Controls.Clear()
                Dim ds As DataSet
                Dim objPageLayout As New CPageLayout

                Dim Type As Char
                If OppType.Value = 1 Then
                    Type = "O"
                ElseIf OppType.Value = 2 Then
                    Type = "u"
                End If


                Dim numFormId As Integer
                Dim numBizFormID As Integer
                If OppType.Value = 1 And (OppStatus.Value = 0 Or OppStatus.Value = 2) Then
                    numFormId = 38
                    numBizFormID = 105
                ElseIf OppType.Value = 1 And OppStatus.Value = 1 Then
                    numFormId = 39
                    numBizFormID = 106
                ElseIf OppType.Value = 2 And (OppStatus.Value = 0 Or OppStatus.Value = 2) Then
                    numFormId = 40
                    numBizFormID = 107
                ElseIf OppType.Value = 2 And OppStatus.Value = 1 Then
                    numFormId = 41
                    numBizFormID = 108
                End If

                If Not Page.IsPostBack Then
                    Dim dsFieldGroups As New DataSet()
                    Dim objBizFormWizardFormConf As New BizFormWizardMasterConfiguration
                    objBizFormWizardFormConf.DomainID = CCommon.ToLong(Session("DomainID"))
                    objBizFormWizardFormConf.numFormID = numBizFormID
                    'numGroupID is always 0 in table so line is commented
                    'objBizFormWizardFormConf.numGroupID = CCommon.ToLong(Session("UserGroupID"))
                    objBizFormWizardFormConf.tintPageType = 4
                    dsFieldGroups = objBizFormWizardFormConf.ManageFormFieldGroup()
                    ddlSectionFile.DataSource = dsFieldGroups.Tables(0)
                    ddlSectionFile.DataValueField = "numFormFieldGroupId"
                    ddlSectionFile.DataTextField = "vcGroupName"
                    ddlSectionFile.DataBind()
                    ddlSectionFile.Items.Insert(0, New ListItem("-- Select One --", "0"))

                    ddlSectionLink.DataSource = dsFieldGroups.Tables(0)
                    ddlSectionLink.DataValueField = "numFormFieldGroupId"
                    ddlSectionLink.DataTextField = "vcGroupName"
                    ddlSectionLink.DataBind()
                    ddlSectionLink.Items.Insert(0, New ListItem("-- Select One --", "0"))
                End If

                btnLayout.Attributes.Add("onclick", "return ShowLayout('" & Type & "','" & lngOppId & "','" & numFormId & "');")

                objPageLayout.CoType = Type
                objPageLayout.UserCntID = Session("UserContactId")
                objPageLayout.RecordId = lngOppId
                objPageLayout.DomainID = Session("DomainID")
                If Type = "O" Then
                    objPageLayout.PageId = 2
                ElseIf Type = "u" Then
                    objPageLayout.PageId = 6
                End If

                objPageLayout.PageType = 3
                objPageLayout.FormId = numFormId

                ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
                dtTableInfo = ds.Tables(0)

                Dim objOpp As New BusinessLogic.Opportunities.OppotunitiesIP
                Dim dtDocLinkName As New DataTable
                With objOpp
                    .DomainID = Session("DomainID")
                    .DivisionID = CCommon.ToLong(OppID.Value)
                    dtDocLinkName = .GetDocumentFilesLink("O")
                End With

                If Not dtDocLinkName Is Nothing AndAlso dtDocLinkName.Rows.Count > 0 Then
                    ShowDocuments(tblDocuments, dtDocLinkName, dtTableInfo, 0)
                End If

                objOpportunity.OpportunityId = lngOppId
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objOpportunity.OpportunityDetails()
                hdnOppname.Value = objOpportunity.OpportunityName
                If objOpportunity.PercentageComplete > 0 And OppType.Value = 1 And (OppStatus.Value = 0 Or OppStatus.Value = 2) Then
                    CType(MileStone1.FindControl("btnAddProcess"), Button).Attributes.Add("onclick", "return confirm('This will disable use of the ""Percentage Complete"" field, because ""Total Progress"" will now be driven by the completion of the Business Process you�re selecting. Do you want to continue ? � Yes / No');")
                End If

                hdnShipToAddress.Value = objOpportunity.ShippingAddress
                btnResourceScheduling.Attributes.Add("onclick", "return openEmpAvailability(" & lngOppId & "," & lngDivId & ");")

                'lblTProgress.Text = objOpportunity.MilestoneProgress

                Dim intCol1Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=1")
                Dim intCol2Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=2")
                Dim intCol1 As Int32 = 0
                Dim intCol2 As Int32 = 0
                Dim tblCell As TableCell

                Dim tblRow As TableRow
                Const NoOfColumns As Short = 2
                Dim ColCount As Int32 = 0
                ''If intermediatory Page is enabled then Add the text "Name" to dropdown fields
                objPageControls.CreateTemplateRow(tblMain)

                objPageControls.DivisionID = objOpportunity.DivisionID
                objPageControls.OpportunityId = objOpportunity.OpportunityId
                objPageControls.TerritoryID = objOpportunity.TerritoryID
                objPageControls.strPageType = "Opp"
                objPageControls.EditPermission = m_aryRightsForPage(RIGHTSTYPE.UPDATE)

                Dim dv As DataView = dtTableInfo.DefaultView
                Dim dvExcutedView As DataView = dtTableInfo.DefaultView
                Dim dvExcutedGroupsView As DataView = dtTableInfo.DefaultView
                'Dim FormFieldGroups = dtTableInfo.AsEnumerable().Select(Function(table) New With {
                '                    .numFormFieldGroupId = table.Field(Of Integer)("numFormFieldGroupId"),
                '                    .vcGroupName = table.Field(Of String)("vcGroupName")
                '                }).Distinct()
                'If FormFieldGroups.Count > 0 Then

                '    tblCell = New TableCell()
                '    tblCell.ColumnSpan = 2
                '    tblCell.Text = "Other"

                '    tblRow = New TableRow
                '    tblCell = New TableCell

                'End If
                Dim fieldGroups = (From dRow In dtTableInfo.Rows
                                    Select New With {Key .groupId = dRow("numFormFieldGroupId"), Key .groupName = dRow("vcGroupName"), Key .order = dRow("numOrder")}).Distinct()

                For Each drGroup In fieldGroups.OrderBy(Function(x) x.order)
                    tblCell = New TableCell()
                    tblRow = New TableRow
                    tblCell.ColumnSpan = 4
                    tblCell.CssClass = "tableGroupHeader"
                    tblCell.Text = CCommon.ToString(drGroup.groupName)
                    tblRow.Cells.Add(tblCell)
                    tblMain.Rows.Add(tblRow)

                    If drGroup.groupId > 0 AndAlso Not dtDocLinkName Is Nothing AndAlso dtDocLinkName.Rows.Count > 0 Then
                        If dtDocLinkName.Select("numFormFieldGroupId=" & drGroup.groupId).Length > 0 Then
                            Dim tableDocs As New HtmlTable
                            tableDocs.Style.Add("float", "right")
                            ShowDocuments(tableDocs, dtDocLinkName, dtTableInfo, drGroup.groupId)

                            tblCell = New TableCell()
                            tblRow = New TableRow
                            tblCell.ColumnSpan = 4
                            tblCell.Controls.Add(tableDocs)
                            tblRow.Cells.Add(tblCell)
                            tblMain.Rows.Add(tblRow)
                        End If
                    End If

                    dvExcutedView.RowFilter = "numFormFieldGroupId = " & drGroup.groupId
                    tblRow = New TableRow
                    tblCell = New TableCell
                    intCol1Count = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=1 AND numFormFieldGroupId='" & drGroup.groupId & "'")
                    intCol2Count = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=2 AND numFormFieldGroupId='" & drGroup.groupId & "'")
                    intCol1 = 0
                    intCol2 = 0
                    For Each drData As DataRowView In dvExcutedView
                        Dim dr As DataRow
                        dr = drData.Row
                        If boolIntermediatoryPage = True Then
                            If Not IsDBNull(dr("vcPropertyName")) And (dr("fld_type") = "SelectBox" Or dr("fld_type") = "SelectBox") And dr("bitCustomField") = False Then
                                If dr("vcPropertyName") <> "numPartner" And dr("vcPropertyName") <> "numPartenerContact" And dr("vcPropertyName") <> "numPartenerSource" And dr("vcPropertyName") <> "intReqPOApproved" Then
                                    dr("vcPropertyName") = dr("vcPropertyName") & "Name"
                                End If
                            End If
                        End If

                        If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False Then
                            If dr("vcPropertyName") = "CalcAmount" Or dr("vcPropertyName") = "InvoiceGrandTotal" Or dr("vcPropertyName") = "TotalAmountPaid" Or (dr("vcPropertyName") = "Amount" Or dr("vcPropertyName") = "TotalDiscount" And boolIntermediatoryPage = True) Then
                                dr("vcValue") = objOpportunity.GetType.GetProperty("CurrSymbol").GetValue(objOpportunity, Nothing) & " " & CCommon.GetDecimalFormat(objOpportunity.GetType.GetProperty(dr("vcPropertyName")).GetValue(objOpportunity, Nothing))

                                If dr("vcPropertyName") = "CalcAmount" Or
                                   (dr("vcPropertyName") = "InvoiceGrandTotal" And CCommon.ToDouble(objOpportunity.GetType.GetProperty(dr("vcPropertyName")).GetValue(objOpportunity, Nothing)) > 0) Or
                                   (dr("vcPropertyName") = "Amount" And boolIntermediatoryPage = True) Then

                                    Dim dblDiscount As Double = 0
                                    If CCommon.ToString(Session("CouponCode")) <> "" AndAlso
                                       CCommon.ToDouble(Session("TotalDiscount")) = 0 Then
                                    End If
                                    dblDiscount = dblDiscount + CCommon.ToDouble(Session("TotalDiscount"))

                                    dr("vcValue") = objOpportunity.GetType.GetProperty("CurrSymbol").GetValue(objOpportunity, Nothing) &
                                                    " " & CCommon.GetDecimalFormat((CCommon.ToDouble(objOpportunity.GetType.GetProperty(dr("vcPropertyName")).GetValue(objOpportunity, Nothing)) + CCommon.ToDouble(GetShippingCharge()) - dblDiscount)) 'CCommon.ToDouble(dr("vcValue")) + GetShippingCharge() + dblDiscount

                                End If



                            ElseIf dr("vcPropertyName") = "InventoryStatusName" Then
                                dr("vcValue") = CCommon.ToString(objOpportunity.InventoryStatus)
                            ElseIf dr("vcPropertyName") = "numPartner" Then
                                If typeEvent = 1 Then
                                    dr("vcValue") = CCommon.ToString(objOpportunity.numPartnerId)
                                Else
                                    dr("vcValue") = objOpportunity.GetType.GetProperty(dr("vcPropertyName")).GetValue(objOpportunity, Nothing)
                                End If
                            ElseIf dr("vcPropertyName") = "numPartenerSource" Then
                                If typeEvent = 1 Then
                                    dr("vcValue") = CCommon.ToString(objOpportunity.numPartnerId)
                                Else
                                    dr("vcValue") = objOpportunity.GetType.GetProperty("numPartner").GetValue(objOpportunity, Nothing)
                                End If
                            ElseIf dr("vcPropertyName") = "intDropShipName" Then
                                dr("vcValue") = CCommon.ToString(objOpportunity.vcDropShip)
                            ElseIf dr("vcPropertyName") = "numPartenerContact" Then
                                If typeEvent = 1 Then
                                    dr("vcValue") = CCommon.ToLong(objOpportunity.numPartenerContactId)
                                Else
                                    dr("vcValue") = objOpportunity.GetType.GetProperty(dr("vcPropertyName")).GetValue(objOpportunity, Nothing)
                                End If
                            ElseIf dr("vcPropertyName") = "intReqPOApproved" Then
                                If typeEvent = 1 Then
                                    dr("vcValue") = CCommon.ToLong(objOpportunity.intReqPOApproved)
                                Else
                                    dr("vcValue") = objOpportunity.GetType.GetProperty("vcReqPOApproved").GetValue(objOpportunity, Nothing)
                                End If

                            ElseIf dr("vcPropertyName") = "Source" Then
                                dr("vcValue") = objOpportunity.Source & "~" & objOpportunity.SourceType
                            ElseIf dr("vcPropertyName") = "vcCompactContactDetails" Then
                                Dim strData As New StringBuilder()
                                strData.Append("<a class='text-yellow' href='javascript:OpenContact(" & objOpportunity.ContactID & ",1)'>" & objOpportunity.ContactIDName & "</a> &nbsp;")
                                strData.Append(objOpportunity.Phone & "&nbsp;")
                                If objOpportunity.PhoneExtension <> "" Then
                                    strData.Append("&nbsp;(" & objOpportunity.PhoneExtension & ")")
                                End If
                                If objOpportunity.vcEmail <> "" Then
                                    strData.Append("<img src='../images/msg_unread_small.gif' email=" & objOpportunity.vcEmail & " id='imgEmailPopupId' onclick='return OpemEmail(" & objOpportunity.OpportunityId & "," & objOpportunity.ContactID & ")' />")
                                End If
                                dr("vcValue") = strData
                            ElseIf dr("vcPropertyName") = "TrackingNo" Then
                                Dim strTrackingNo As String = ""
                                If Not objOpportunity.TrackingNo Is Nothing AndAlso objOpportunity.TrackingNo.Trim.Length > 0 Then
                                    Dim strTrackingNoList() As String = objOpportunity.TrackingNo.Split(New String() {"$^$"}, StringSplitOptions.None)
                                    Dim strIDValue() As String

                                    If strTrackingNoList.Length > 0 Then
                                        For k As Integer = 0 To strTrackingNoList.Length - 1
                                            strIDValue = strTrackingNoList(k).Split(New String() {"#^#"}, StringSplitOptions.None)

                                            If strIDValue(1).Length > 0 Then
                                                'strTrackingNo += String.Format("<a href='{0}' target='_blank'>{1}</a>&nbsp;&nbsp;", IIf(strIDValue(0).Length = 0, "javascript:void(0)", strIDValue(0) & strIDValue(1)), strIDValue(1))
                                                strTrackingNo += String.Format("<a href='{0}' target='_blank'>{1}</a>&nbsp;&nbsp;", IIf(strIDValue(0).Length = 0, "javascript:void(0)", String.Format(strIDValue(0) + strIDValue(1), strIDValue(1))), strIDValue(1))
                                            End If
                                        Next
                                    End If
                                End If
                                dr("vcValue") = "<div style='width: 95%;word-break: break-all;'>" & strTrackingNo & "</div>"
                            ElseIf dr("vcPropertyName") = "EDIFields" Then

                                objCommon.DomainID = Session("DomainID")
                                Dim dtbitEDI As DataTable = objCommon.GetDomainSettingValue("bitEDI")

                                If dtbitEDI Is Nothing Or dtbitEDI.Rows.Count = 0 Then
                                    Throw New Exception("Not able to fetch bit EDI global settings value.")
                                    Exit Sub
                                ElseIf CCommon.ToShort(dtbitEDI.Rows(0)("bitEDI")) = 1 Then
                                    lnkEDIOrder.Visible = True
                                Else
                                    lnkEDIOrder.Visible = False
                                End If

                            Else
                                dr("vcValue") = objOpportunity.GetType.GetProperty(dr("vcPropertyName")).GetValue(objOpportunity, Nothing)
                            End If
                        End If

                        If IsDBNull(dr("vcPropertyName")) Then
                            dr("vcPropertyName") = ""
                        End If


                        If dr("vcPropertyName") = "PercentageCompleteName" AndAlso objOpportunity.SalesProcessId > 0 Then
                            dr("bitCanBeUpdated") = False
                        End If

                        If dr("vcPropertyName") = "Amount" And boolIntermediatoryPage = False Then
                            dr("vcFieldName") = dr("vcFieldName") & " (" & objOpportunity.GetType.GetProperty("CurrSymbol").GetValue(objOpportunity, Nothing) & ")"
                        End If

                        If dr("vcPropertyName") = "vcOrderedShipped" And boolIntermediatoryPage = True Then
                            Dim strOrderedShipped As String = CCommon.GetCommaSeparatedOrderedShipped(objOpportunity.vcOrderedShipped.ToString)
                            dr("vcValue") = strOrderedShipped
                        End If

                        If dr("vcPropertyName") = "vcOrderedReceived" And boolIntermediatoryPage = True Then
                            Dim strOrderedReceived As String = CCommon.GetCommaSeparatedOrderedShipped(objOpportunity.vcOrderedReceived.ToString)
                            dr("vcValue") = strOrderedReceived
                        End If

                        If ColCount = 0 Then
                            tblRow = New TableRow
                        End If

                        If intCol1Count = intCol1 And intCol2Count > intCol1Count Then
                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)
                            ColCount = 1
                        End If

                        If (dr("fld_type") = "ListBox" Or dr("fld_type") = "SelectBox") Then
                            Dim dtData As DataTable


                            If dr("vcPropertyName") = "AssignedTo" Then ' If dr("numFieldId") = 106 Or dr("numFieldId") = 262 Then
                                Dim m_aryRightsForAssignedTo() As Integer = GetUserRightsForPage_Other(10, 128)
                                If m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 0 Then
                                    dr("bitCanBeUpdated") = False
                                ElseIf m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 1 Then
                                    Try
                                        If CCommon.ToLong(hdnRecordOwner.Value) <> Session("UserContactID") Then
                                            dr("bitCanBeUpdated") = False
                                        End If
                                    Catch ex As Exception
                                    End Try
                                ElseIf m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 2 Then
                                    Dim i As Integer
                                    Dim dtTerritory As DataTable
                                    dtTerritory = Session("UserTerritory")
                                    If CCommon.ToLong(hdnTerrID.Value) = 0 Then
                                        dr("bitCanBeUpdated") = False
                                    Else
                                        Dim chkDelete As Boolean = False
                                        For i = 0 To dtTerritory.Rows.Count - 1
                                            If CCommon.ToLong(hdnTerrID.Value) = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                                chkDelete = True
                                            End If
                                        Next
                                        If chkDelete = False Then
                                            dr("bitCanBeUpdated") = False
                                        End If
                                    End If
                                Else
                                    dr("bitCanBeUpdated") = True
                                End If

                                If boolIntermediatoryPage = False Then
                                    If Session("PopulateUserCriteria") = 1 Then

                                        dtData = objCommon.ConEmpListFromTerritories(Session("DomainID"), 0, 0, objOpportunity.TerritoryID)

                                    ElseIf Session("PopulateUserCriteria") = 2 Then
                                        dtData = objCommon.ConEmpList(Session("DomainID"), Session("UserContactID"))
                                    Else
                                        dtData = objCommon.ConEmpList(Session("DomainID"), 0, 0)
                                    End If

                                    'Commissionable Contacts
                                    Dim objUser As New UserAccess
                                    objUser.DomainID = Context.Session("DomainID")
                                    objUser.byteMode = 1

                                    Dim dt As DataTable = objUser.GetCommissionsContacts

                                    Dim item As ListItem
                                    Dim dr1 As DataRow

                                    For Each dr2 As DataRow In dt.Rows
                                        dr1 = dtData.NewRow
                                        dr1(0) = dr2("numContactID")
                                        dr1(1) = dr2("vcUserName")
                                        dtData.Rows.Add(dr1)
                                    Next
                                End If
                            ElseIf dr("vcPropertyName") = "AssignedToName" Then
                                Dim m_aryRightsForAssignedTo() As Integer = GetUserRightsForPage_Other(10, 128)
                                If m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 0 Then
                                    dr("bitCanBeUpdated") = False
                                ElseIf m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 1 Then
                                    Try
                                        If CCommon.ToLong(hdnRecordOwner.Value) <> Session("UserContactID") Then
                                            dr("bitCanBeUpdated") = False
                                        End If
                                    Catch ex As Exception
                                    End Try
                                ElseIf m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 2 Then
                                    Dim i As Integer
                                    Dim dtTerritory As DataTable
                                    dtTerritory = Session("UserTerritory")
                                    If CCommon.ToLong(hdnTerrID.Value) = 0 Then
                                        dr("bitCanBeUpdated") = False
                                    Else
                                        Dim chkDelete As Boolean = False
                                        For i = 0 To dtTerritory.Rows.Count - 1
                                            If CCommon.ToLong(hdnTerrID.Value) = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                                chkDelete = True
                                            End If
                                        Next
                                        If chkDelete = False Then
                                            dr("bitCanBeUpdated") = False
                                        End If
                                    End If
                                Else
                                    dr("bitCanBeUpdated") = True
                                End If
                            ElseIf dr("vcPropertyName") = "ContactID" Then
                                Dim fillCombo As New COpportunities
                                With fillCombo
                                    .DivisionID = objOpportunity.DivisionID
                                    dtData = fillCombo.ListContact().Tables(0)
                                End With
                            ElseIf dr("vcPropertyName") = "RecurringId" Then ' dr("numFieldId") = 274 Or dr("numFieldId") = 285 Then
                                Dim lobjChecks As New Checks
                                lobjChecks.DomainID = Session("DomainID")
                                dtData = lobjChecks.GetRecurringDetails
                            ElseIf dr("vcPropertyName") = "numPartner" Then
                                objCommon.DomainID = Session("DomainID")
                                dtData = objCommon.GetPartnerSource()
                            ElseIf dr("vcPropertyName") = "numPartenerSource" Then
                                objCommon.DomainID = Session("DomainID")
                                dtData = objCommon.GetPartnerSource()
                            ElseIf dr("vcPropertyName") = "numPartenerContact" Then
                                objCommon.DomainID = Session("DomainID")
                                objCommon.RecordId = objOpportunity.OpportunityId
                                dtData = objCommon.GetPartnerContacts()
                            ElseIf dr("vcPropertyName") = "ProjectID" Then
                                dtData = New DataTable
                                dtData.Columns.Add("numListItemID")
                                dtData.Columns.Add("vcData")

                                Dim objProject As New BACRM.BusinessLogic.Projects.Project
                                objProject.DomainID = Context.Session("DomainID")
                                Dim dtProject As DataTable = objProject.GetOpenProject()
                                If Not dtProject Is Nothing Then
                                    Dim dr1 As DataRow

                                    For Each dr2 As DataRow In dtProject.Rows
                                        dr1 = dtData.NewRow
                                        dr1("numListItemID") = dr2("numProId")
                                        dr1("vcData") = dr2("vcProjectName")
                                        dtData.Rows.Add(dr1)
                                    Next
                                End If
                            ElseIf dr("vcPropertyName") = "DealStatus" Then ' dr("numFieldId") = 294 Or dr("numFieldId") = 295 Then
                                dtData = New DataTable
                                dtData.Columns.Add("numListItemID")
                                dtData.Columns.Add("vcData")

                                Dim row As DataRow = dtData.NewRow
                                row("numListItemID") = 1
                                row("vcData") = "Deal Won"
                                dtData.Rows.Add(row)

                                row = dtData.NewRow
                                row("numListItemID") = 2
                                row("vcData") = "Deal Lost"
                                dtData.Rows.Add(row)
                                dtData.AcceptChanges()
                            ElseIf dr("vcPropertyName") = "ReleaseStatus" Then
                                dtData = New DataTable
                                dtData.Columns.Add("numListItemID")
                                dtData.Columns.Add("vcData")

                                Dim row As DataRow = dtData.NewRow
                                row("numListItemID") = 1
                                row("vcData") = "Open"
                                dtData.Rows.Add(row)

                                row = dtData.NewRow
                                row("numListItemID") = 2
                                row("vcData") = "Purchased"
                                dtData.Rows.Add(row)
                                dtData.AcceptChanges()
                            ElseIf dr("vcPropertyName") = "Source" Then
                                objCommon.DomainID = Session("DomainID")
                                dtData = objCommon.GetOpportunitySource()
                            ElseIf dr("vcPropertyName") = "CampaignID" Then ' dr("numFieldId") = 75 Then 'Campaign
                                If boolIntermediatoryPage = False Then
                                    Dim objCampaign As New BusinessLogic.Reports.PredefinedReports
                                    objCampaign.byteMode = 2
                                    objCampaign.DomainID = Session("DomainID")
                                    dtData = objCampaign.GetCampaign()
                                End If
                            ElseIf dr("vcPropertyName") = "intReqPOApproved" Then
                                dtData = New DataTable
                                dtData.Columns.Add("numListItemID")
                                dtData.Columns.Add("vcData")
                                Dim row As DataRow = dtData.NewRow
                                row = dtData.NewRow
                                row("numListItemID") = 0
                                row("vcData") = "Pending Approval"
                                dtData.Rows.Add(row)
                                row = dtData.NewRow
                                row("numListItemID") = 1
                                row("vcData") = "Approved"
                                dtData.Rows.Add(row)
                                row = dtData.NewRow
                                row("numListItemID") = 2
                                row("vcData") = "Rejected"
                                dtData.Rows.Add(row)
                                dtData.AcceptChanges()
                            ElseIf dr("vcPropertyName") = "EDIStatus" Then
                                dtData = New DataTable
                                dtData.Columns.Add("numListItemID")
                                dtData.Columns.Add("vcData")

                                Dim row As DataRow = dtData.NewRow
                                row = dtData.NewRow
                                row("numListItemID") = 11
                                row("vcData") = "850 Partially Created"

                                row = dtData.NewRow
                                row("numListItemID") = 12
                                row("vcData") = "850 SO Created"
                                ''row("numListItemID") = 1
                                ''row("vcData") = "850 Received"
                                ''dtData.Rows.Add(row)

                                row = dtData.NewRow
                                row("numListItemID") = 11
                                row("vcData") = "850 Partially Created"

                                row = dtData.NewRow
                                row("numListItemID") = 12
                                row("vcData") = "850 SO Created"

                                row = dtData.NewRow
                                row("numListItemID") = 2
                                row("vcData") = "850 Acknowledged"
                                dtData.Rows.Add(row)

                                row = dtData.NewRow
                                row("numListItemID") = 3
                                row("vcData") = "940 Sent"
                                dtData.Rows.Add(row)

                                row = dtData.NewRow
                                row("numListItemID") = 4
                                row("vcData") = "940 Acknowledged"
                                dtData.Rows.Add(row)

                                row = dtData.NewRow
                                row("numListItemID") = 5
                                row("vcData") = "856 Received"
                                dtData.Rows.Add(row)

                                row = dtData.NewRow
                                row("numListItemID") = 6
                                row("vcData") = "856 Acknowledged"
                                dtData.Rows.Add(row)

                                row = dtData.NewRow
                                row("numListItemID") = 7
                                row("vcData") = "856 Sent"
                                dtData.Rows.Add(row)

                                dtData.AcceptChanges()
                            ElseIf dr("vcPropertyName") = "ShippingServiceName" Or dr("vcPropertyName") = "ShippingService" Then
                                dtData = objCommon.GetDropDownValue(0, "PSS", "")
                            ElseIf Not IsDBNull(dr("numListID")) And dr("numListID") <> 0 Then
                                If boolIntermediatoryPage = False Then
                                    If dr("ListRelID") > 0 Then
                                        objCommon.Mode = 3
                                        objCommon.DomainID = Session("DomainID")
                                        objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objOpportunity)
                                        objCommon.SecondaryListID = dr("numListId")
                                        dtData = objCommon.GetFieldRelationships.Tables(0)
                                    Else
                                        If CCommon.ToLong(dr("numListID")) = 176 Then
                                            dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"), OppType.Value, If(OppStatus.Value = 1, 2, 1))
                                        Else
                                            dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                        End If
                                    End If
                                End If
                            End If


                            If boolIntermediatoryPage Then
                                objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData, Nothing, objOpportunity.OpportunityId)
                            Else
                                Dim ddl As DropDownList

                                If dr("vcPropertyName") = "AssignedTo" Then
                                    ddl = objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData, boolEnabled:=CCommon.ToBool(dr("bitCanBeUpdated")))
                                Else
                                    ddl = objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData)
                                End If


                                If CLng(dr("DependentFields")) > 0 And Not boolIntermediatoryPage Then
                                    ddl.AutoPostBack = True
                                    AddHandler ddl.SelectedIndexChanged, AddressOf PopulateDependentDropdown
                                End If
                            End If
                        ElseIf dr("fld_type") = "CheckBoxList" Then
                            If Not IsDBNull(dr("vcPropertyName")) Then
                                Dim dtData As DataTable

                                If boolIntermediatoryPage = False Then
                                    If dr("ListRelID") > 0 Then
                                        objCommon.Mode = 3
                                        objCommon.DomainID = Session("DomainID")
                                        objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objOpportunity)
                                        objCommon.SecondaryListID = dr("numListId")
                                        dtData = objCommon.GetFieldRelationships.Tables(0)
                                    Else
                                        dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                    End If
                                End If

                                objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData)
                            End If
                        ElseIf dr("fld_type") = "EDIFields" Then

                        Else
                            objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, RecordID:=lngOppId)
                        End If

                        If intCol2Count = intCol2 And intCol1Count > intCol2Count Then
                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            ColCount = 1
                        End If

                        If dr("intcoulmn") = 2 Then
                            If intCol1 <> intCol1Count Then
                                intCol1 = intCol1 + 1
                            End If

                            If intCol2 <> intCol2Count Then
                                intCol2 = intCol2 + 1
                            End If
                        End If


                        ColCount = ColCount + 1
                        If NoOfColumns = ColCount Then
                            ColCount = 0
                            tblMain.Rows.Add(tblRow)
                        End If
                    Next
                Next




                If ColCount > 0 Then
                    tblMain.Rows.Add(tblRow)
                End If

                'Add Client Side validation for custom fields
                If Not boolIntermediatoryPage Then
                    Dim strValidation As String = objPageControls.GenerateValidationScript(dtTableInfo)
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "CFvalidation", strValidation, True)
                ElseIf Session("InlineEdit") = True And m_aryRightsForPage(RIGHTSTYPE.UPDATE) <> 0 Then
                    Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtTableInfo)
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "InlineEditValidation", strValidation, True)
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Function GetParentDIV(ByVal intColumn As Int16, ByRef isFirstColumnUsed As Boolean, ByRef divForm1 As HtmlGenericControl, ByRef divForm2 As HtmlGenericControl) As HtmlGenericControl
            If intColumn = 1 Then
                Return divForm1
            ElseIf intColumn = 2 Then
                Return divForm2
            ElseIf isFirstColumnUsed Then
                isFirstColumnUsed = False
                Return divForm2
            Else
                isFirstColumnUsed = True
                Return divForm1
            End If
        End Function



        Public Sub FillContact(ByVal ddlCombo As DropDownList, ByVal lngDivision As Long)
            Try
                Dim fillCombo As New COpportunities
                With fillCombo
                    .DivisionID = lngDivision
                    ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                    ddlCombo.DataTextField = "Name"
                    ddlCombo.DataValueField = "numcontactId"
                    ddlCombo.DataBind()
                End With
                ddlCombo.Items.Insert(0, New ListItem("---Select One---", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveOpportunity()
            Try
                objOpportunity.OpportunityId = lngOppId
                objOpportunity.PublicFlag = 0
                objOpportunity.UserCntID = Session("UserContactID")
                objOpportunity.DomainID = Session("DomainId")
                objOpportunity.OppType = OppType.Value
                objOpportunity.CouponCode = CCommon.ToString(Session("CouponCode"))
                If CCommon.ToDouble(Session("TotalDiscount")) > 0 Then
                    objOpportunity.Discount = CCommon.ToDouble(Session("TotalDiscount"))
                    objOpportunity.boolDiscType = True
                Else
                    objOpportunity.Discount = 0
                    objOpportunity.boolDiscType = False
                End If

                For Each dr As DataRow In dtTableInfo.Rows
                    If Not IsDBNull(dr("vcPropertyName")) And dr("vcPropertyName") = "Source" And dr("bitCustomField") = False And dr("bitCanBeUpdated") = True Then
                        Dim ddl As DropDownList = CType(radOppTab.MultiPage.FindControl(dr("numFieldId").ToString & dr("vcFieldName").ToString & "~" & IIf(IsDBNull(dr("numListId")), 0, dr("numListId").ToString).ToString), DropDownList)

                        If ddl.SelectedValue.IndexOf("~") > 0 Then
                            objOpportunity.Source = ddl.SelectedValue.Split("~")(0)
                            objOpportunity.SourceType = ddl.SelectedValue.Split("~")(1)
                        Else
                            objOpportunity.Source = 0
                            objOpportunity.SourceType = 1
                        End If

                    ElseIf Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False And dr("bitCanBeUpdated") = True Then
                        If dr("vcPropertyName") = "numPartner" Then
                            dr("vcPropertyName") = "numPartnerId"
                        End If
                        If dr("vcPropertyName") = "numPartenerSource" Then
                            refData = 1
                            dr("vcPropertyName") = "numPartnerId"
                        End If
                        If dr("vcPropertyName") = "numPartenerContact" Then
                            dr("vcPropertyName") = "numPartenerContactId"
                        End If
                        If dr("vcPropertyName") = "ShippingCompany" Then
                            dr("vcPropertyName") = "intUsedShippingCompany"
                        End If
                        objPageControls.SetValueForStaticFields(dr, objOpportunity, radOppTab.MultiPage)
                        If dr("vcPropertyName") = "numPartnerId" And refData = 0 Then
                            dr("vcPropertyName") = "numPartner"
                        End If
                        If dr("vcPropertyName") = "numPartnerId" And refData = 1 Then
                            dr("vcPropertyName") = "numPartenerSource"
                        End If
                        If dr("vcPropertyName") = "numPartenerSourceId" Then
                            dr("vcPropertyName") = "numPartenerSource"
                        End If
                        If dr("vcPropertyName") = "numPartenerContactId" Then
                            dr("vcPropertyName") = "numPartenerContact"
                        End If
                        If dr("vcPropertyName") = "intUsedShippingCompany" Then
                            dr("vcPropertyName") = "ShippingCompany"
                        End If
                        refData = 0
                    End If
                Next
                'objPageControls.SetValueForComments(objOpportunity.OppComments, radOppTab)

                If Not ViewState("SOPOItems") Is Nothing Then
                    dsTemp = ViewState("SOPOItems")
                    objOpportunity.strItems = "<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & dsTemp.GetXml
                    If objOpportunity.strItems.Contains("<numWarehouseItmsID>0</numWarehouseItmsID>") OrElse
                       objOpportunity.strItems.Contains("<numWarehouseItmsID></numWarehouseItmsID>") Then
                        objOpportunity.strItems = objOpportunity.strItems.Replace("<numWarehouseItmsID>0</numWarehouseItmsID>", "")
                        objOpportunity.strItems = objOpportunity.strItems.Replace("<numWarehouseItmsID></numWarehouseItmsID>", "")
                    End If
                End If
                objOpportunity.UseShippersAccountNo = CCommon.ToBool(chkUseCustomerShippingAccount.Checked)
                objOpportunity.UseMarkupShippingRate = CCommon.ToBool(chkMarkupShippingCharges.Checked)
                objOpportunity.dcMarkupShippingRate = CCommon.ToDecimal(txtMarkupShippingCharge.Text)

                If objOpportunity.numPartenerContactId = 0 Then
                    objOpportunity.numPartenerContactIn = selectContactId
                Else
                    objOpportunity.numPartenerContactIn = objOpportunity.numPartenerContactId
                End If
                objOpportunity.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                arrOutPut = objOpportunity.Save()
                objOpportunity.OpportunityId = arrOutPut(0)
                ''Added By Sachin Sadhu||Date:29thApril12014
                ''Purpose :To Added Opportunity data in work Flow queue based on created Rules
                ''          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = lngOppId
                objWfA.SaveWFOrderQueue()
                'end of code


                If objOpportunity.AssignedTo > 0 And ViewState("AssignedTo") <> objOpportunity.AssignedTo Then
                    CAlerts.SendAlertToAssignee(2, txtrecOwner.Text, objOpportunity.AssignedTo, objOpportunity.OpportunityId, Session("DomainID")) 'bugid 767
                    ViewState("AssignedTo") = objOpportunity.AssignedTo
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub SaveOrderSettings()
            Try
                Dim dtStartDate As Date
                Dim dtEndDate As Date
                Dim errorMessage As String

                If OppType.Value = "1" AndAlso OppStatus.Value = "1" AndAlso tblRecurInsert.Visible Then
                    If chkEnableRecurring.Checked Then
                        If Not String.IsNullOrEmpty(calRecurStart.SelectedDate) AndAlso Not String.IsNullOrEmpty(calRecurEnd.SelectedDate) Then
                            Dim isValidStartDate = Date.TryParse(calRecurStart.SelectedDate, dtStartDate)
                            Dim isValidEndDate = Date.TryParse(calRecurEnd.SelectedDate, dtEndDate)

                            If isValidStartDate AndAlso isValidEndDate Then
                                If dtStartDate <= DateAndTime.Now.Date Then
                                    errorMessage = "Start must be after today."
                                ElseIf dtEndDate <= dtStartDate Then
                                    errorMessage = "End date must be after start date."
                                End If

                                'If ddlFrequency.SelectedValue = "1" AndAlso dtEndDate < dtStartDate.AddDays(1) Then
                                '    errorMessage = "End date must be atleast 1 day after start Date."
                                'ElseIf ddlFrequency.SelectedValue = "2" AndAlso dtEndDate < dtStartDate.AddDays(7) Then
                                '    errorMessage = "End date must be atleast 1 week after start Date."
                                'ElseIf ddlFrequency.SelectedValue = "3" AndAlso dtEndDate < dtStartDate.AddMonths(1) Then
                                '    errorMessage = "End date must be atleast 1 month after start Date."
                                'ElseIf ddlFrequency.SelectedValue = "4" AndAlso dtEndDate < dtStartDate.AddMonths(4) Then
                                '    errorMessage = "End date must be atleast 1 quarter after start Date."
                                'End If
                            Else
                                errorMessage = "Enter valid start and end date."
                            End If
                        Else
                            errorMessage = "Recurrence start date and end date is required."
                        End If
                    End If
                End If

                If String.IsNullOrEmpty(errorMessage) Then
                    If objOpportunity Is Nothing Then objOpportunity = New OppotunitiesIP
                    objOpportunity.DomainID = Session("DomainID")
                    objOpportunity.UserCntID = Session("UserContactID")
                    objOpportunity.boolBillingTerms = IIf(ddlNetDays.SelectedValue > 0, 1, 0) 'chkBillingTerms.Checked
                    objOpportunity.BillingDays = ddlNetDays.SelectedValue 'IIf(IsNumeric(txtDays.Text), txtDays.Text, 0)
                    'objOpportunity.boolInterestType = IIf(radPlus.Checked, True, False)
                    'objOpportunity.Interest = IIf(IsNumeric(txtInterest.Text), txtInterest.Text, 0)
                    'objOpportunity.Discount = CCommon.ToDecimal(txtDiscount.Text)
                    'objOpportunity.boolDiscType = IIf(radPer.Checked, False, True)
                    'objOpportunity.TaxOperator = IIf(chkAddSalesTax.Checked, 1, IIf(chkRemoveTax.Checked, 2, 0)) 'Add/Remove Sales Tax
                    'objOpportunity.DiscountAcntType = ddlDiscActType.SelectedValue
                    objOpportunity.TaxOperator = CCommon.ToShort(ddlSalesTax.SelectedValue)
                    objOpportunity.fltExchageRate = CCommon.ToDecimal(txtExchangeRate.Text)
                    objOpportunity.dtItemReceivedDate = IIf(calItemReceivedDate.SelectedDate = "", DateTime.UtcNow, calItemReceivedDate.SelectedDate)

                    If OppType.Value = "1" AndAlso OppStatus.Value = "1" Then
                        If chkEnableRecurring.Checked AndAlso tblRecurInsert.Visible Then
                            objOpportunity.Recur = chkEnableRecurring.Checked
                            objOpportunity.Frequency = CCommon.ToShort(ddlFrequency.SelectedValue)
                            objOpportunity.FrequencyName = ddlFrequency.SelectedItem.Text
                            objOpportunity.StartDate = dtStartDate
                            objOpportunity.EndDate = dtEndDate
                        ElseIf tblRecurDetail.Visible AndAlso ddlRecurStatus.SelectedValue = "2" Then
                            objOpportunity.RecurConfigID = CCommon.ToLong(hdnRecConfigID.Value)
                            objOpportunity.DisableRecur = True
                        End If
                    End If


                    Try
                        objOpportunity.SaveOrderSettings()
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "ReloadWindow", "RefereshParentPage();", True)

                        If OppType.Value = "1" AndAlso OppStatus.Value = "1" AndAlso tblRecurInsert.Visible Then
                            If chkEnableRecurring.Checked Then
                                tblRecurInsert.Visible = False
                                chkEnableRecurring.Checked = False
                                trRecurring2.Style.Add("display", "none")
                                trRecurring3.Style.Add("display", "none")
                                calRecurStart.SelectedDate = ""
                                calRecurEnd.SelectedDate = ""

                                tblRecurDetail.Visible = True
                                lblFrequency.Text = ddlFrequency.SelectedItem.Text
                                lblStartDate.Text = dtStartDate.ToString()
                                lblEndDate.Text = dtEndDate.ToString()
                                hdnRecurrenceType.Value = "Sales Order"
                            End If
                        End If
                    Catch ex As Exception
                        If ex.Message = "RECURRENCE_ALREADY_CREATED_FOR_ORDER" Then
                            ShowMessage("You can not set recurrence for order because someone has already created recurrence for bizdoc in this order.")
                        Else
                            Throw
                        End If
                    End Try



                    ''Added By Sachin Sadhu||Date:5thAug2014
                    ''Purpose :To Added Opportunity data in work Flow queue based on created Rules
                    ''          Using Change tracking
                    Dim objWfA As New Workflow()
                    objWfA.DomainID = Session("DomainID")
                    objWfA.UserCntID = Session("UserContactID")
                    objWfA.RecordID = CCommon.ToLong(objOpportunity.OpportunityId)
                    objWfA.SaveWFOrderQueue()
                    'end of code
                Else
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "RecurrenceError", "alert('" & errorMessage & "' );", True)
                End If
            Catch ex As Exception
                ShowMessage("Error occurred while saving Order Settings.")
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                SaveOpportunity()
                SaveCusField()

                ViewState("IntermediatoryPage") = True
                boolIntermediatoryPage = True
                'ddlShippingMethod.Items.Clear()
                'txtCouponCode.Text = ""
                'Session("CouponCode") = Nothing
                'Session("TotalDiscount") = 0

                tblMain.Controls.Clear()
                LoadSavedInformation()
                ControlSettings()
                'dtSalesProcess = objOpportunity.BusinessProcessByOpID
                'BindRepeaterControl()
            Catch ex As Exception
                If ex.Message.Contains("COMMISSION_PAID_AGAINST_BIZDOC") Then
                    ShowMessage("Change in assigned to field is not allowed after commission is paid against any bizdoc of current order.")
                ElseIf ex.Message.Contains("ITEM_USED_IN_BIZDOC") Then
                    ShowMessage("Deleted item(s) are used in bizdoc(s).")
                ElseIf ex.Message.Contains("ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY") Then
                    ShowMessage("Ordered qty can no be less than packing slip quantity when customer default settings is ""Allocate inventory from Packing Slip"".")
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(ex.Message)
                End If
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                SaveOpportunity()
                SaveCusField()
                'ddlShippingMethod.Items.Clear()
                'txtCouponCode.Text = ""
                'Session("CouponCode") = Nothing
                'Session("TotalDiscount") = 0
                sb_PageRedirect()
            Catch ex As Exception
                If ex.Message.Contains("COMMISSION_PAID_AGAINST_BIZDOC") Then
                    ShowMessage("Change in assigned to field is not allowed after commission is paid against any bizdoc of current order.")
                ElseIf ex.Message.Contains("ITEM_USED_IN_BIZDOC") Then
                    ShowMessage("Deleted item(s) are used in bizdoc(s).")
                ElseIf ex.Message.Contains("ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY") Then
                    ShowMessage("Ordered qty can no be less than packing slip quantity when customer default settings is ""Allocate inventory from Packing Slip"".")
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(ex.Message)
                End If
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                sb_PageRedirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub sb_PageRedirect()
            Try
                If GetQueryStringVal("frm") = "contactdetails" Then
                    Response.Redirect("../contact/frmContacts.aspx?frm=" & GetQueryStringVal("frm1") & "&CntID=" & GetQueryStringVal("CntID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm2, False)
                ElseIf GetQueryStringVal("frm") = "tickler" Then
                    Response.Redirect("../common/frmticklerdisplay.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2 & "&SelectedIndex=1", False)
                ElseIf GetQueryStringVal("frm") = "accounts" Then
                    objCommon.OppID = lngOppId
                    objCommon.charModule = "O"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../Account/frmAccounts.aspx?klds+7kldf=fjk-las&DivId=" & objCommon.DivisionID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2, False)
                ElseIf GetQueryStringVal("frm") = "prospects" Then
                    objCommon.OppID = lngOppId
                    objCommon.charModule = "O"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../prospects/frmProspects.aspx?DivID=" & objCommon.DivisionID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2, False)
                ElseIf GetQueryStringVal("frm") = "opportunitylist" Then
                    If ViewState("Deal") = "True" Then
                        If OppType.Value = 1 Then
                            Response.Redirect("../opportunity/frmDealList.aspx?type=1&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2, False)
                        ElseIf OppType.Value = 2 Then
                            Response.Redirect("../opportunity/frmDealList.aspx?type=2&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2, False)
                        End If
                    ElseIf OppType.Value = 1 Then
                        Response.Redirect("../opportunity/frmOpportunityList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2, False)
                    ElseIf OppType.Value = 2 Then
                        Response.Redirect("../opportunity/frmOpportunityList.aspx?SI=1&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2, False)
                    End If
                ElseIf GetQueryStringVal("frm") = "OpenBizDoc" Then
                    Response.Redirect("../opportunity/frmOpenBizDocs.aspx" & "?type=" & OppType.Value, False)
                ElseIf GetQueryStringVal("frm") = "ProjectList" Then
                    Response.Redirect("../projects/frmProjectList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2, False)
                ElseIf GetQueryStringVal("frm") = "Leads" Then
                    objCommon.OppID = lngOppId
                    objCommon.charModule = "O"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../Leads/frmLeads.aspx?DivID=" & objCommon.DivisionID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2, False)
                ElseIf GetQueryStringVal("frm") = "ProductsShipped" Then
                    Response.Redirect("../reports/frmProductShipped.aspx?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2, False)
                ElseIf GetQueryStringVal("frm") = "ContactRole" Then
                    Response.Redirect("../reports/frmContactRole.aspx?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2, False)
                ElseIf GetQueryStringVal("frm") = "CampDetails" Then
                    Response.Redirect("../Marketing/frmCampaignDetails.aspx?CampID=" & GetQueryStringVal("CampID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2, False)
                ElseIf GetQueryStringVal("frm") = "OpportunityOpen" Then
                    Response.Redirect("../reports/frmOpenOpportunities.aspx?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2, False)
                ElseIf GetQueryStringVal("frm") = "OpportunityStatus" Then
                    Response.Redirect("../reports/frmOpportunityStatus.aspx?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2, False)
                ElseIf GetQueryStringVal("frm") = "OppSearch" Then
                    Response.Redirect("../Admin/frmItemSearchRes.aspx", False)
                ElseIf GetQueryStringVal("frm") = "RecurringTransaction" Then
                    Response.Redirect("../Accounting/frmRecurringTransaction.aspx", False)
                ElseIf GetQueryStringVal("frm") = "RecurringTransactionReport" Then
                    Response.Redirect("../Accounting/frmRecurringTransactionReport.aspx", False)
                ElseIf GetQueryStringVal("frm") = "RecurringTransactionDetailReport" Then
                    Response.Redirect("../Accounting/frmRecurringTransactionDetailReport.aspx", False)
                ElseIf GetQueryStringVal("frm") = "PurchaseFulfillment" Then
                    Response.Redirect("../Opportunity/frmPurchaseFulfillment.aspx?R=1", False)
                ElseIf GetQueryStringVal("frm") = "SalesReturn" Then
                    Response.Redirect("../Opportunity/frmSalesReturns.aspx?type=" & OppType.Value, False)
                ElseIf GetQueryStringVal("frm") = "frmCases" Then
                    Response.Redirect("../cases/frmCases.aspx?frm=Caselist&CaseID=" & GetQueryStringVal("CaseId"), False)
                ElseIf GetQueryStringVal("frm") = "GeneralLedger" Then
                    Response.Redirect("../Accounting/frmGeneralLedger.aspx", False)
                Else
                    If ViewState("Deal") = "True" Then
                        If OppType.Value = 1 Then
                            Response.Redirect("../opportunity/frmDealList.aspx?type=1", False)
                        ElseIf OppType.Value = 2 Then
                            Response.Redirect("../opportunity/frmDealList.aspx?type=2", False)
                        End If
                        'Response.Redirect("../opportunity/frmOpportunityList.aspx?SI=2&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                    ElseIf OppType.Value = 1 Then
                        Response.Redirect("../opportunity/frmOpportunityList.aspx" & "?SI=" & 0 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2, False)
                    ElseIf OppType.Value = 2 Then
                        Response.Redirect("../opportunity/frmOpportunityList.aspx?SI=1&SI1=" & 1 & "&frm=" & frm1 & "&frm1=" & frm2, False)
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnReceivedOrShipped_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReceivedOrShipped.Click
            Try
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.OpportunityId = lngOppId

                If isMinUnitPriceRuleSet Then
                    If Not dsTemp Is Nothing AndAlso dsTemp.Tables.Count > 0 AndAlso dsTemp.Tables(0).Rows.Count > 0 Then
                        Dim results = From myRow In dsTemp.Tables(0).AsEnumerable()
                                      Where myRow.Field(Of Boolean)("bitItemPriceApprovalRequired") = True
                                      Select myRow

                        If results.Count > 0 Then
                            ShowMessage("This sales order must be approved before it can be closed.")
                            Exit Sub
                        End If
                    End If
                End If

                If Not dsTemp Is Nothing AndAlso dsTemp.Tables.Count > 0 AndAlso dsTemp.Tables(0).Rows.Count > 0 Then
                    Dim drAsset As DataRow

                    For Each drAsset In dsTemp.Tables(0).Rows
                        If (CCommon.ToBool((drAsset("bitAsset")) = True)) Then
                            If CCommon.ToLong(drAsset("numRentalOut")) = (CCommon.ToLong(drAsset("numRentalIN")) + CCommon.ToLong(drAsset("numRentalLost"))) Then
                                'work as per previously coded.
                            Else
                                ShowMessage("All Rental/Asset Items must be received before closing the Order")
                                Exit Sub
                            End If
                        End If
                    Next
                End If

                'CHECK IF ANY ITEM PURCHASED WITH GLOBAL COLLECTION SELECTED
                If OppType.Value = 2 AndAlso OppStatus.Value = 1 AndAlso hdnStockTransfer.Value <> "1" Then
                    Dim isGlobalLocationFulfilled As Boolean = objOpportunity.CheckIfItemPurchasedToGlobalLocation()

                    If Not isGlobalLocationFulfilled Then
                        ShowMessage("Go to purchase fulfillment and receive all items where global internal location is selected.")
                        Exit Sub
                    End If
                End If

                Dim dtTable As DataTable
                dtTable = objOpportunity.ValidateOppSerializLot()

                Dim dr As DataRow

                Dim msg As String

                For Each dr In dtTable.Rows
                    If dr("Error") = 1 Then
                        msg += "<br/>- " + dr("vcItemName")
                    End If
                Next

                If Not String.IsNullOrEmpty(msg) Then
                    ShowMessage("You must provide Lot/Serial #s for following items:" & msg)
                    Exit Sub
                End If

                If objOpportunity.CheckWOStatus > 0 Then
                    ShowMessage("You can't ship at this time because Work Order for this Sales Order is pending.")
                    Exit Sub
                End If

                Dim dtItemList As DataTable
                dtItemList = objOpportunity.CheckCanbeShipped()

                Dim dsInvoiceBilled As DataSet = objOpportunity.CheckOrderedAndInvoicedOrBilledQty()

                Dim bitFulfillmentBizDocPending As Boolean = CCommon.ToBool(dsInvoiceBilled.Tables(0).Select("ID=1")(0)("bitTrue"))
                Dim bitOrderedAndShippedBilledQtyNotSame As Boolean = CCommon.ToBool(dsInvoiceBilled.Tables(0).Select("ID=2")(0)("bitTrue"))
                Dim bitOrderedAndFulfilledQtyNotSame As Boolean = CCommon.ToBool(dsInvoiceBilled.Tables(0).Select("ID=3")(0)("bitTrue"))
                Dim bitDeferredAndInvoicedQtyNotSame As Boolean = CCommon.ToBool(dsInvoiceBilled.Tables(0).Select("ID=4")(0)("bitTrue"))

                If OppType.Value = "1" AndAlso bitFulfillmentBizDocPending Then
                    If dsInvoiceBilled.Tables.Count > 1 AndAlso dsInvoiceBilled.Tables(1).Rows.Count > 0 Then
                        Dim objOppBizDocs As New OppBizDocs
                        objOppBizDocs.DomainID = CCommon.ToLong(Session("DomainID"))
                        objOppBizDocs.UserCntID = CCommon.ToLong(Session("UserContactID"))
                        objOppBizDocs.OppId = lngOppId

                        Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                            For Each drFulfillBizDoc As DataRow In dsInvoiceBilled.Tables(1).Rows
                                objOppBizDocs.OppBizDocId = CCommon.ToLong(drFulfillBizDoc("numOppBizDocsId"))
                                objOppBizDocs.AutoFulfillBizDoc()
                            Next

                            objTransactionScope.Complete()
                        End Using
                    End If
                End If

                If dtItemList.Rows.Count > 0 Then
                    litMessage.Text = "You can't ship at this time because you don't have enough quantity on hand to support your shipment. Your options are to modify your order, or replenish inventory (to check inventory click on the edit link within the line item, then the value in the 'Products/Services' column):"

                    For Each dr In dtItemList.Rows
                        litMessage.Text += "<br/>- " + dr("vcItemName")
                    Next

                    ShowMessage(litMessage.Text)

                    Exit Sub
                ElseIf OppType.Value = "1" AndAlso bitDeferredAndInvoicedQtyNotSame Then
                    litMessage.Text = ""
                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Alert", "InvoiceAgainstDeferredBizDocPending();", True)
                    Exit Sub
                ElseIf (bitOrderedAndShippedBilledQtyNotSame Or bitOrderedAndFulfilledQtyNotSame) AndAlso OppType.Value = "1" AndAlso hdnStockTransfer.Value <> "1" Then
                    'Condition OppType.Value="1" is added because now we are allowing use to close purchase order without creating bill(s).
                    litMessage.Text = ""

                    If OppType.Value = "1" Then
                        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "InvoiceAndShipItems", "InvoiceAndShipItems();", True)
                    End If

                    Exit Sub
                Else
                    Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                        If CCommon.ToBool(hdnPPVariance.Value) Then
                            If OppType.Value = 2 Then
                                If ChartOfAccounting.GetDefaultAccount("PC", Session("DomainID")) = 0 Then
                                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Alert", "PurchaseClearingAccountNotExists();", True)
                                    Exit Sub
                                End If

                                If ChartOfAccounting.GetDefaultAccount("PV", Session("DomainID")) = 0 Then
                                    ScriptManager.RegisterStartupScript(Me, Me.GetType(), "Alert", "PurchaseVarianceAccountNotExists();", True)
                                    Exit Sub
                                End If

                                Dim dtItems As DataTable
                                objOpportunity.OpportunityId = lngOppId
                                objOpportunity.DomainID = Session("DomainID")
                                objOpportunity.UserCntID = Session("UserContactID")
                                objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                                dsTemp = objOpportunity.GetOrderItemsProductServiceGrid
                                dtItems = dsTemp.Tables(0)

                                Dim drFindItems() As DataRow
                                drFindItems = dtItems.Select("numOriginalUnitHour>numUnitHourReceived", Nothing)

                                If drFindItems.Length > 0 Then
                                    Dim JournalId As Long = 0

                                    Dim dt As DataTable = drFindItems.CopyToDataTable
                                    Dim objOppBizDocs As New OppBizDocs
                                    objOppBizDocs.OppId = lngOppId
                                    objOppBizDocs.DomainID = Session("DomainID")
                                    objOppBizDocs.UserCntID = Session("UserContactID")

                                    'dt.Columns("monPrice").DataType = GetType(Decimal)
                                    'dt.Columns("numOriginalUnitHour").DataType = GetType(Decimal)
                                    'dt.Columns("numUnitHourReceived").DataType = GetType(Decimal)

                                    Dim numUnitReceived As DataColumn = New DataColumn
                                    With numUnitReceived
                                        .DataType = System.Type.GetType("System.Decimal")
                                        .ColumnName = "numUnitReceived"
                                        .Expression = "numOriginalUnitHour - numUnitHourReceived"
                                    End With
                                    dt.Columns.Add(numUnitReceived)

                                    Dim monTotalItemAmount As DataColumn = New DataColumn
                                    With monTotalItemAmount
                                        .DataType = System.Type.GetType("System.Decimal")
                                        .ColumnName = "monTotalItemAmount"
                                        .Expression = "monPrice * (numOriginalUnitHour - numUnitHourReceived) * fltExchangeRate"
                                    End With
                                    dt.Columns.Add(monTotalItemAmount)

                                    JournalId = objOppBizDocs.SaveDataToHeader(dt.Compute("sum([monTotalItemAmount])", ""), CDate(calItemReceivedDate.SelectedDate), Description:=CCommon.ToString(hfPoppName.Value))

                                    Dim objJournalEntries As New JournalEntry
                                    objJournalEntries.SaveJournalEntriesPurchaseClearing(lngOppId, Session("DomainID"), dt, JournalId)
                                End If
                            End If
                        End If

                        objOpportunity.UserCntID = Session("UserContactID")
                        objOpportunity.ShippingOrReceived()

                        objTransactionScope.Complete()
                    End Using

                    ShowMessage("The Deal is now Closed")
                    btnReceivedOrShipped.Visible = False
                    btnAddEditOrder.Visible = False
                    btnRecalculatePrice.Visible = False
                    btnUpdateLabel.Visible = False
                    lkbDelete.Visible = False
                    btnSave.Enabled = False
                    btnSaveClose.Enabled = False
                    btnUpdate.Enabled = False
                    'instead of enable=false .. just hide it client side.. so we can delete it from front end..instead of backend
                    btnActdelete.Attributes.Add("style", "Display:none")
                    btnReOpenDeal.Visible = True

                    'lblDealCompletedDate.Visible = True
                    If m_aryRightsForReOpenDeal(RIGHTSTYPE.VIEW) = 0 Then btnReOpenDeal.Visible = False

                    'LoadSavedInformation(boolLoadOppInfo:=True)
                    sb_PageRedirect()
                End If
            Catch ex As Exception
                If CCommon.ToString(ex.Message).Contains("ORDER_CLOSED") Then
                    ShowMessage("This order is already closed. Please reload page.")
                ElseIf CCommon.ToString(ex.Message).Contains("GLOCAL_LOCATION_PENDING_FULFILLMENT") Then
                    ShowMessage("Go to purchase fulfillment and receive all items where global internal location is selected.")
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(ex.Message)
                End If
            End Try
        End Sub

        Sub DisplayDynamicFlds()
            Try
                'If OppType.Value = 1 Then
                '    objPageControls.DisplayDynamicFlds(lngOppId, 0, Session("DomainID"), objPageControls.Location.SalesOppotunity, radOppTab)
                'Else
                '    objPageControls.DisplayDynamicFlds(lngOppId, 0, Session("DomainID"), objPageControls.Location.PurchaseOppotunity, radOppTab)
                'End If

                If OppType.Value = 1 And (OppStatus.Value = 0 Or OppStatus.Value = 2) Then
                    objPageControls.DisplayDynamicFlds(lngOppId, 0, Session("DomainID"), objPageControls.Location.SalesOppotunity, radOppTab, 38)
                ElseIf OppType.Value = 1 And OppStatus.Value = 1 Then
                    objPageControls.DisplayDynamicFlds(lngOppId, 0, Session("DomainID"), objPageControls.Location.SalesOppotunity, radOppTab, 39)
                ElseIf OppType.Value = 2 And (OppStatus.Value = 0 Or OppStatus.Value = 2) Then
                    objPageControls.DisplayDynamicFlds(lngOppId, 0, Session("DomainID"), objPageControls.Location.PurchaseOppotunity, radOppTab, 40)
                ElseIf OppType.Value = 2 And OppStatus.Value = 1 Then
                    objPageControls.DisplayDynamicFlds(lngOppId, 0, Session("DomainID"), objPageControls.Location.PurchaseOppotunity, radOppTab, 41)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveCusField()
            Try
                If OppType.Value = 1 Then
                    objPageControls.SaveCusField(lngOppId, 0, Session("DomainID"), objPageControls.Location.SalesOppotunity, radOppTab.MultiPage)
                Else
                    objPageControls.SaveCusField(lngOppId, 0, Session("DomainID"), objPageControls.Location.PurchaseOppotunity, radOppTab.MultiPage)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActdelete.Click
            Try
                Try
                    If Not String.IsNullOrEmpty(hdnRecurrenceType.Value) Or Not String.IsNullOrEmpty(hdnRecurred.Value) Then
                        ShowMessage("You cannot delete order when: <br/> 1. Recurrence is set on order or on bizdoc within order. <br/> 2. Order is recuured from another order.")
                        Exit Sub
                    End If

                    Dim objOpport As New COpportunities
                    With objOpport
                        .OpportID = lngOppId
                        .DomainID = Session("DomainID")
                        .UserCntID = Session("UserContactID")
                    End With
                    Dim lintCount As Integer
                    lintCount = objOpport.GetAuthoritativeOpportunityCount()
                    If lintCount = 0 Then
                        Dim objAdmin As New CAdmin

                        objAdmin.DomainID = Session("DomainId")
                        objAdmin.ModeType = MileStone1.ModuleType.Opportunity
                        objAdmin.ProjectID = lngOppId

                        objAdmin.RemoveStagePercentageDetails()

                        objOpport.DelOpp()
                        Dim strFileName As String = ""
                        strFileName = CCommon.GetDocumentPhysicalPath(Session("DomainId")) & "OrderShippingDetail_" & lngOppId & "_" & CCommon.ToString(Session("DomainId")) & ".xml"
                        If File.Exists(strFileName) Then
                            File.Delete(strFileName)
                        End If

                        If GetQueryStringVal("frm") = "deallist" Then
                            If OppType.Value = 1 Then
                                Response.Redirect("../opportunity/frmDealList.aspx?type=1")
                            ElseIf OppType.Value = 2 Then
                                Response.Redirect("../opportunity/frmDealList.aspx?type=2")
                            End If
                        Else
                            If OppType.Value = 1 Then
                                Response.Redirect("../opportunity/frmOpportunityList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                            ElseIf OppType.Value = 2 Then
                                Response.Redirect("../opportunity/frmOpportunityList.aspx?SelectIndex=1" & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                            End If
                        End If

                    Else : ShowMessage("You cannot delete Order for which Authoritative BizDocs is present")
                    End If
                Catch ex As Exception
                    If ex.Message = "DEPENDANT" Then
                        ShowMessage("Can not remove record, Your option is to remove Time and Expense associated with all stages from ""milestone & stages"" sub-tab and try again.")
                    ElseIf ex.Message = "RETURN_EXIST" Then
                        ShowMessage("Return exists against order.record can not be deleted.")
                    ElseIf ex.Message = "CASE DEPENDANT" Then
                        ShowMessage("Dependent case exists. Cannot be Deleted.")
                    ElseIf ex.Message = "CreditBalance DEPENDANT" Then
                        ShowMessage("Credit balance of current order is being used. Your option is used to credit balance of Organization from Accunting sub tab.")
                    ElseIf ex.Message = "OppItems DEPENDANT" Then
                        ShowMessage("Items already used. Cannot be Deleted.")
                    ElseIf ex.Message = "FY_CLOSED" Then
                        ShowMessage("This transaction can not be posted,Because transactions date belongs to closed financial year.")
                    ElseIf ex.Message = "RECURRING ORDER OR BIZDOC" Then
                        ShowMessage("Can not remove record because recurrence is set on order or on bizdoc within order.")
                    ElseIf ex.Message = "RECURRED ORDER" Then
                        ShowMessage("Can not remove record because it is recurred from another order")
                    ElseIf ex.Message.Contains("INVENTORY IM-BALANCE") Then
                        ShowMessage("BizAutomation.com has detected a problem. Wait a few seconds and attempt to delete order again")
                    ElseIf ex.Message.Contains("FULFILLED_ITEMS") Then
                        ShowMessage("Can not delete record because contains fulfilled items. Your option is to delete fulfillment order bizdoc.")
                    ElseIf ex.Message.Contains("WORK_ORDER_EXISTS") Then
                        ShowMessage("Work order exists. You have to first delete work order(s).")
                    ElseIf ex.Message.Contains("SERIAL/LOT#_USED") Then
                        ShowMessage("Purchased serial item is used in any sales order or purchase Lot# do not have enough qty left becasuse of used in any sales order.")
                    Else
                        Throw ex
                    End If

                End Try


                ''objOpport.DelOpp()
                ''If OppType.Value = 1 Then
                ''    Response.Redirect("../opportunity/frmOpportunityList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ''ElseIf OppType.Value = 2 Then
                ''    Response.Redirect("../opportunity/frmOpportunityList.aspx?SelectIndex=1" & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ''End If
            Catch ex As Exception
                ShowMessage("Dependent record exists. Cannot be Deleted.")
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
            Try
                If Not String.IsNullOrEmpty(hdnRecurrenceType.Value) Then
                    ShowMessage("You cannot edit order because recurrence is set on order or on bizdoc within order.")
                    Exit Sub
                End If

                ViewState("IntermediatoryPage") = False
                boolIntermediatoryPage = False
                tblMain.Controls.Clear()
                LoadSavedInformation(False, 1)
                ControlSettings()
                'createSet()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try

        End Sub

        Sub ControlSettings()
            If boolIntermediatoryPage = True Then
                btnSave.Visible = False
                btnSaveClose.Visible = False
                btnEdit.Visible = True
                'tblItem.Visible = False
                'tblKits.Visible = False
                'tblOptSelection.Visible = False

            Else
                'objCommon.CheckdirtyForm(Page)

                btnSave.Visible = True
                btnSaveClose.Visible = True
                btnEdit.Visible = False
                'tblItem.Visible = False
                'RadGrid1.Columns.FindByUniqueNameSafe("EditLink").Visible = True

                'Dim objCommon As New CCommon
                ' objCommon.InitializeClientSideTemplate(radCmbItem)
                'CCommon.UpdateItemRadComboValues(OppType.Value, DivID.Value)
            End If
        End Sub

        Sub PopulateDependentDropdown(ByVal sender As Object, ByVal e As EventArgs)
            objPageControls.PopulateDropdowns(CType(sender, DropDownList), dtTableInfo, objCommon, radOppTab.MultiPage, Session("DomainID"))
        End Sub

        Function ReturnMoney(ByVal Money)
            Try
                If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Sub setUserControlProperties()
        '    MileStone.dtBusinessProcess = dtSalesProcess
        '    MileStone.RecordOwner = lblRecordOwner.Text
        '    MileStone.CustomerName = hplCustomer.Text
        '    MileStone.RecordOwnerID = txtrecOwner.Text
        '    MileStone.TargetModuleType = MileStone.ModuleType.Opportunity
        '    MileStone.RecordID = lngOppId
        '    MileStone.DivisionID = DivID.Value
        '    MileStone.OppProName = objOpportunity.OpportunityName
        '    MileStone.OppAmount = objOpportunity.Amount
        '    MileStone.RecordContactID = objOpportunity.ContactID
        '    MileStone.objCommon = objCommon
        'End Sub

        Private Sub btnUpdateLabel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdateLabel.Click
            Try
                Dim objOpportunity As New OppotunitiesIP

                For Each item As GridViewRow In gvProduct.Rows
                    'Update Oppertunity
                    Dim dRows As DataRow() = dsTemp.Tables(0).Select("numoppitemtCode = " & CCommon.ToLong(gvProduct.DataKeys(item.RowIndex)("numoppitemtcode")).ToString())

                    If dRows.Length > 0 Then
                        objOpportunity.OpportunityId = lngOppId
                        objOpportunity.OppItemCode = CCommon.ToLong(CType(item.FindControl("hfOppItemtCode"), HiddenField).Value)

                        If Not item.FindControl("txtvcItemName") Is Nothing Then
                            objOpportunity.ItemName = CType(item.FindControl("txtvcItemName"), TextBox).Text.Trim
                        Else
                            objOpportunity.ItemName = CCommon.ToString(dRows(0)("vcItemName"))
                        End If
                        If Not item.FindControl("txtvcModelID") Is Nothing Then
                            objOpportunity.ModelName = HttpUtility.HtmlDecode(CType(item.FindControl("txtvcModelID"), TextBox).Text).Trim
                        Else
                            objOpportunity.ModelName = CCommon.ToString(dRows(0)("vcModelID"))
                        End If
                        If Not item.FindControl("txtvcItemDesc") Is Nothing Then
                            objOpportunity.ItemDesc = CType(item.FindControl("txtvcItemDesc"), TextBox).Text.Trim
                        Else
                            objOpportunity.ItemDesc = CCommon.ToString(dRows(0)("vcItemDesc"))
                        End If
                        If Not item.FindControl("txtvcNotes") Is Nothing Then
                            objOpportunity.Notes = CType(item.FindControl("txtvcNotes"), TextBox).Text.Trim
                        Else
                            objOpportunity.Notes = CCommon.ToString(dRows(0)("vcNotes"))
                        End If
                        If Not item.FindControl("txtvcManufacturer") Is Nothing Then
                            objOpportunity.Manufacturer = CType(item.FindControl("txtvcManufacturer"), TextBox).Text.Trim
                        Else
                            objOpportunity.Manufacturer = CCommon.ToString(dRows(0)("vcManufacturer"))
                        End If
                        If Not item.FindControl("ddlProject") Is Nothing Then
                            objOpportunity.ProjectID = CType(item.FindControl("ddlProject"), DropDownList).SelectedValue
                        Else
                            objOpportunity.ProjectID = CCommon.ToDouble(dRows(0)("numProjectID"))
                        End If
                        If Not item.FindControl("ddlClass") Is Nothing Then
                            objOpportunity.ClassID = CType(item.FindControl("ddlClass"), DropDownList).SelectedValue
                        Else
                            objOpportunity.ClassID = CCommon.ToDouble(dRows(0)("numClassID"))
                        End If

                        If Not CType(item.FindControl("txtnumCost"), TextBox) Is Nothing Then
                            objOpportunity.numCost = CCommon.ToDouble(CType(item.FindControl("txtnumCost"), TextBox).Text.Trim)
                        Else

                            If dRows.Length > 0 Then
                                objOpportunity.numCost = CCommon.ToDouble(dRows(0)("numCost"))
                            End If
                        End If
                        objOpportunity.UpdateOpportunityItemLabel()
                    End If
                Next

                If Not btnReOpenDeal.Visible Then
                    Dim boolFlag As Boolean = True

                    If dsTemp.Tables(0).Rows.Count > 0 Then
                        For Each dr As DataRow In dsTemp.Tables(0).Rows
                            Dim decUnits As Decimal = CCommon.ToDecimal(dr("numUnitHour")) * CCommon.ToDecimal(dr("UOMConversionFactor"))
                            Dim decUnitHourReceived As Decimal = Math.Abs(CCommon.ToDecimal(dr("numUnitHourReceived")))
                            Dim decQtyShipped As Decimal = Math.Abs(CCommon.ToDecimal(dr("numQtyShipped")))

                            If OppType.Value = 1 Then
                                If decQtyShipped > decUnits Then
                                    boolFlag = False
                                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "ValidationItemDelete", "alert('You are not allowed to edit this item,since item has shipped qty more than order qty.')", True)
                                End If
                            ElseIf OppType.Value = 2 Then
                                If decUnitHourReceived > decUnits Then
                                    boolFlag = False
                                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "ValidationItemDelete", "alert('You are not allowed to edit this item,since item has received qty more than order qty.')", True)
                                End If
                            End If
                        Next
                    End If

                    If boolFlag Then
                        If CCommon.ToBool(Session("IsMinUnitPriceRule")) AndAlso OppType.Value = "1" Then
                            CheckIfUnitPriceApprovalRequired()
                        End If

                        Dim dtItems As DataTable
                        objOpportunity.DomainID = Session("DomainID")
                        objOpportunity.OpportunityId = lngOppId
                        objOpportunity.Mode = 4
                        Dim dsOppItems As DataSet = objOpportunity.GetOrderItems()
                        dtItems = dsOppItems.Tables(0)
                        Dim k As Integer
                        Dim intCheck As Integer = 0
                        For k = 0 To dtItems.Rows.Count - 1
                            If dtItems.Rows(k).Item("charItemType") <> "S" Then intCheck = 1
                            dtItems.Rows(k)("numUnitHour") = Math.Abs(dtItems.Rows(k)("numUnitHour") / dtItems.Rows(k)("UOMConversionFactor"))
                        Next

                        dtItems.Columns.Add("numVendorID", System.Type.GetType("System.Int32"))
                        dtItems.Columns.Add("vcInstruction")
                        dtItems.Columns.Add("bintCompliationDate")
                        dtItems.Columns.Add("numWOAssignedTo")

                        dtItems.Columns("ItemRequiredDate").DateTimeMode = DataSetDateTime.Unspecified   ' Item Required Date, changing datetime mode
                        dtItems.AcceptChanges()

                        Dim dtItem As DataTable
                        dtItem = dsOppItems.Tables(0)
                        dtItem.TableName = "Item"
                        dtItem.PrimaryKey = New DataColumn() {dsOppItems.Tables(0).Columns("numoppitemtCode")}
                        dsOppItems.AcceptChanges()

                        Dim removedItems As New ArrayList
                        Dim units As Decimal = 0
                        Dim price As Decimal = 0
                        For Each drItem As DataRow In dsOppItems.Tables(0).Rows
                            Dim arrRows As DataRow() = dsTemp.Tables(0).Select("numoppitemtCode = " & drItem("numoppitemtCode"))
                            If arrRows.Length > 0 Then
                                units = -1
                                price = -1
                                Dim discount As Decimal = Convert.ToDecimal(arrRows(0)("fltDiscount"))
                                Dim uomConversionFactor As Decimal = Convert.ToDecimal(arrRows(0)("UOMConversionFactor"))

                                For Each item As GridViewRow In gvProduct.Rows
                                    If gvProduct.DataKeys(item.RowIndex)("numoppitemtcode") = drItem("numoppitemtCode") Then
                                        If Not item.FindControl("txtnumUnitHour") Is Nothing Then
                                            units = CCommon.ToDecimal(CType(item.FindControl("txtnumUnitHour"), TextBox).Text)
                                        End If
                                        If Not item.FindControl("txtmonPriceUOM") Is Nothing Then
                                            price = CCommon.ToDecimal(CType(item.FindControl("txtmonPriceUOM"), TextBox).Text)
                                        End If
                                    End If
                                Next

                                If units <> -1 Then
                                    drItem("numUnitHour") = units
                                Else
                                    drItem("numUnitHour") = CCommon.ToDouble(arrRows(0)("numUnitHour"))
                                End If

                                If price <> -1 Then
                                    Dim salePrice As Decimal
                                    If Not CCommon.ToBool(drItem("bitDiscountType")) Then
                                        If (drItem("bitMarkupDiscount").ToString() = "1") Then ' Markup N Discount logic added
                                            salePrice = price + (price * (discount / 100))
                                        Else
                                            salePrice = price - (price * (discount / 100))
                                        End If
                                    Else
                                        If CCommon.ToDecimal(drItem("numUnitHour")) > 0 Then
                                            If (drItem("bitMarkupDiscount").ToString() = "1") Then  ' Markup N Discount logic added
                                                salePrice = ((price * (CCommon.ToDecimal(drItem("numUnitHour")) * uomConversionFactor)) + discount) / (CCommon.ToDecimal(drItem("numUnitHour")) * uomConversionFactor)
                                            Else
                                                salePrice = ((price * (CCommon.ToDecimal(drItem("numUnitHour")) * uomConversionFactor)) - discount) / (CCommon.ToDecimal(drItem("numUnitHour")) * uomConversionFactor)
                                            End If
                                        Else
                                            salePrice = 0
                                        End If
                                    End If
                                    drItem("monPrice") = price / uomConversionFactor
                                    drItem("monTotAmtBefDiscount") = (drItem("numUnitHour") * uomConversionFactor) * price
                                    drItem("monTotAmount") = (drItem("numUnitHour") * uomConversionFactor) * salePrice
                                Else
                                    drItem("monPrice") = CCommon.ToDecimal(arrRows(0)("monPrice"))
                                    price = CCommon.ToDecimal(arrRows(0)("monPrice"))

                                    Dim salePrice As Decimal
                                    If Not CCommon.ToBool(drItem("bitDiscountType")) Then
                                        If (drItem("bitMarkupDiscount").ToString() = "1") Then ' Markup N Discount logic added
                                            salePrice = price + (price * (discount / 100))
                                        Else
                                            salePrice = price - (price * (discount / 100))
                                        End If
                                    Else
                                        If CCommon.ToDecimal(drItem("numUnitHour")) > 0 Then
                                            If (drItem("bitMarkupDiscount").ToString() = "1") Then  ' Markup N Discount logic added
                                                salePrice = ((price * (CCommon.ToDecimal(drItem("numUnitHour")) * uomConversionFactor)) + discount) / (CCommon.ToDecimal(drItem("numUnitHour")) * uomConversionFactor)
                                            Else
                                                salePrice = ((price * (CCommon.ToDecimal(drItem("numUnitHour")) * uomConversionFactor)) - discount) / (CCommon.ToDecimal(drItem("numUnitHour")) * uomConversionFactor)
                                            End If
                                        Else
                                            salePrice = 0
                                        End If
                                    End If
                                    drItem("monPrice") = price / uomConversionFactor
                                    drItem("monTotAmtBefDiscount") = (drItem("numUnitHour") * uomConversionFactor) * price
                                    drItem("monTotAmount") = (drItem("numUnitHour") * uomConversionFactor) * salePrice
                                End If

                                drItem("numUOM") = arrRows(0)("numUOM")

                                drItem("numWarehouseItmsID") = arrRows(0)("numWarehouseItmsID")
                                drItem("bitDiscountType") = arrRows(0)("bitDiscountType")
                                drItem("fltDiscount") = CCommon.ToDecimal(arrRows(0)("fltDiscount"))
                                drItem("bitItemPriceApprovalRequired") = arrRows(0)("bitItemPriceApprovalRequired")
                                drItem("numSOVendorId") = arrRows(0)("numVendorID")
                                drItem("UOMConversionFactor") = arrRows(0)("UOMConversionFactor")
                                drItem("Op_Flag") = 2
                                drItem("ItemRequiredDate") = arrRows(0)("ItemRequiredDate")
                                drItem.AcceptChanges()
                            End If
                        Next
                        dsOppItems.Tables(0).AcceptChanges()
                        dsOppItems.AcceptChanges()

                        SaveOpportunity(dsOppItems)
                    End If
                End If

                'Added By :Sachin Sadhu||Date:18thJul204
                'Purpose : To add/save custom Fields
                SaveCusFieldItems()
                'end of code:sachin
                LoadDataToSession()
                BindItems(dsTemp)
            Catch ex As Exception
                If ex.Message.Contains("EDITED_ITEMS_QTY_CAN_NOT_BE_GREATER_THEN_COMPLETED_WORK_ORDER") Then
                    ShowMessage("Once work order is completed, You can't have qty more than completed work order qty")
                ElseIf ex.Message.Contains("ITEM_USED_IN_BIZDOC") Then
                    ShowMessage("Deleted item(s) are used in bizdoc(s).")
                ElseIf ex.Message.Contains("ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY") Then
                    ShowMessage("Ordered qty can no be less than packing slip quantity when customer default settings is ""Allocate inventory from Packing Slip"".")
                ElseIf ex.Message.Contains("ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_INVOICE/BILL_QTY") Then
                    ShowMessage("Ordered qty can no be less than invoiced/billed qty.")
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(ex.Message)
                End If
            End Try
        End Sub

        Sub SaveOpportunity(ByVal ds As DataSet)
            Try
                Dim arrOutPut() As String
                Dim objOpportunity As New OppotunitiesIP
                objOpportunity.OpportunityId = lngOppId
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objOpportunity.OpportunityDetails()

                objOpportunity.OpportunityId = lngOppId
                objOpportunity.PublicFlag = 0
                objOpportunity.UserCntID = Session("UserContactID")
                objOpportunity.DomainID = Session("DomainId")
                objOpportunity.OppType = OppType.Value

                If Not ds Is Nothing Then
                    objOpportunity.strItems = "<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & ds.GetXml
                End If
                objOpportunity.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objOpportunity.Save()
            Catch ex As Exception
                Throw
            End Try
        End Sub


        Private Sub ValidateEmpAccount()
            Try
                'Validate Employee's Account ID
                Dim ds As New DataSet
                Dim objCOA As New ChartOfAccounting
                With objCOA
                    .MappingID = 0
                    .ContactTypeID = CContacts.GetContactType(Session("UserContactID"))
                    .DomainID = Session("DomainID")
                    ds = .GetContactTypeMapping()
                    If ds.Tables(0).Rows.Count > 0 Then
                        lngEmpAccountID = CCommon.ToLong(ds.Tables(0).Rows(0).Item("numAccountId"))
                    End If
                End With
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Private Sub gvProductdivBizDocsDtl_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles gvProduct.RowCommand
        '    Try
        '        Response.Write("sachu")
        '    Catch ex As Exception

        '    End Try
        'End Sub

        Protected Sub GridButtons_Command(sender As Object, e As CommandEventArgs)
            Try

                Dim objItem As New CItems
                If e.CommandName = "RentalOut" Then

                    Dim row As GridViewRow = DirectCast(DirectCast(sender, Button).NamingContainer, GridViewRow)
                    Dim txtRentalOut As TextBox = TryCast(row.FindControl("txtDYRentdivBizDocsDtlalOut"), TextBox)

                    objItem.ItemCode = CCommon.ToLong(e.CommandArgument)
                    objItem.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    'objItem.OnAllocation = CCommon.ToLong(CType(e.FindControl("txtSerialLotNo"), TextBox).Text)
                    objItem.OnAllocation = CCommon.ToDouble(txtRentalOut.Text)
                    objItem.OppId = CCommon.ToLong(OppID.Value)
                    objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                    ' Response.Write(txtRentalOut.Text)
                    If CCommon.ToLong(txtRentalOut.Text) > 0 Then
                        objItem.UpdateInventoryRentalAsset(0)
                    End If




                ElseIf e.CommandName = "RentalIN" Then
                    Dim row As GridViewRow = DirectCast(DirectCast(sender, Button).NamingContainer, GridViewRow)
                    Dim txtRentalIN As TextBox = TryCast(row.FindControl("txtDYRentalIN"), TextBox)

                    objItem.ItemCode = CCommon.ToLong(e.CommandArgument)
                    objItem.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objItem.OnAllocation = CCommon.ToDouble(txtRentalIN.Text)
                    objItem.OppId = CCommon.ToLong(OppID.Value)
                    objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                    ' Response.Write(txtRentalIN.Text)
                    If CCommon.ToLong(txtRentalIN.Text) > 0 Then
                        objItem.UpdateInventoryRentalAsset(1)
                    End If

                    '  BindDataGrid()
                ElseIf e.CommandName = "RentalLost" Then
                    Dim row As GridViewRow = DirectCast(DirectCast(sender, Button).NamingContainer, GridViewRow)
                    Dim txtRentalIN As TextBox = TryCast(row.FindControl("txtDYRentalLost"), TextBox)

                    objItem.ItemCode = CCommon.ToLong(e.CommandArgument)
                    objItem.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objItem.OnAllocation = CCommon.ToDouble(txtRentalIN.Text)
                    objItem.OppId = CCommon.ToLong(OppID.Value)
                    objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                    ' Response.Write(txtRentalIN.Text)
                    If CCommon.ToLong(txtRentalIN.Text) > 0 Then
                        objItem.UpdateInventoryRentalAsset(2)
                    End If

                    '  BindDataGrid()

                End If
                LoadDataToSession()
                BindItems(dsTemp)

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try




        End Sub

        Private Sub gvProduct_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvProduct.RowDataBound
            Try
                If e.Row.RowType = DataControlRowType.Header Then
                    Dim btnPickPack As Button = DirectCast(e.Row.FindControl("btnPickPack"), Button)
                    Dim btnFulFillmentOrder As Button = DirectCast(e.Row.FindControl("btnFulFillmentOrder"), Button)
                    Dim btnInvoice As Button = DirectCast(e.Row.FindControl("btnInvoice"), Button)
                    Dim btnUnreceive As Button = DirectCast(e.Row.FindControl("btnUnreceive"), Button)

                    If Not btnPickPack Is Nothing Then
                        AddHandler btnPickPack.Click, AddressOf Me.btnPickPack_Click
                    End If

                    If Not btnFulFillmentOrder Is Nothing Then
                        AddHandler btnFulFillmentOrder.Click, AddressOf Me.btnFulFillmentOrder_Click
                    End If

                    If Not btnInvoice Is Nothing Then
                        AddHandler btnInvoice.Click, AddressOf Me.btnInvoice_Click
                    End If

                    If Not btnUnreceive Is Nothing Then
                        AddHandler btnUnreceive.Click, AddressOf btnPutaway_Click
                    End If
                End If
                If e.Row.RowType = DataControlRowType.DataRow Then
                    Dim rdpItemRequiredDate As RadDatePicker = DirectCast(e.Row.FindControl("rdpItemRequiredDate"), RadDatePicker)
                    Dim txtnumUnitHour As TextBox = DirectCast(e.Row.FindControl("txtnumUnitHour"), TextBox)
                    Dim txtmonPriceUOM As TextBox = DirectCast(e.Row.FindControl("txtmonPriceUOM"), TextBox)
                    Dim radWarehouse As RadComboBox = DirectCast(e.Row.FindControl("radWarehouse"), RadComboBox)
                    Dim ddlUOM As DropDownList = DirectCast(e.Row.FindControl("ddlUOM"), DropDownList)
                    Dim txtDiscount As TextBox = DirectCast(e.Row.FindControl("txtDiscount"), TextBox)
                    Dim radPer As RadioButton = DirectCast(e.Row.FindControl("radPer"), RadioButton)
                    Dim radAmt As RadioButton = DirectCast(e.Row.FindControl("radAmt"), RadioButton)
                    Dim ddlVendor As DropDownList = DirectCast(e.Row.FindControl("ddlVendor"), DropDownList)

                    Dim txtFullFIlmentQty As TextBox = DirectCast(e.Row.FindControl("txtFullFIlmentQty"), TextBox)
                    Dim txtInvoicedQty As TextBox = DirectCast(e.Row.FindControl("txtInvoicedQty"), TextBox)

                    Dim lblRPackagingSlipQty As Label = DirectCast(e.Row.FindControl("lblRPackagingSlipQty"), Label)
                    Dim hfRPackagingSlipQty As HiddenField = DirectCast(e.Row.FindControl("hfRPackagingSlipQty"), HiddenField)
                    Dim lblCPackagingSlipQty As Label = DirectCast(e.Row.FindControl("lblCPackagingSlipQty"), Label)
                    Dim litCPackagingSlipQty As Literal = DirectCast(e.Row.FindControl("litCPackagingSlipQty"), Literal)

                    Dim lblRFullFilmentQty As Label = DirectCast(e.Row.FindControl("lblRFullFilmentQty"), Label)
                    Dim hfRFullFilmentQty As HiddenField = DirectCast(e.Row.FindControl("hfRFullFilmentQty"), HiddenField)
                    Dim hfRFullFilmentQtyWithAllocationCheck As HiddenField = DirectCast(e.Row.FindControl("hfRFullFilmentQtyWithAllocationCheck"), HiddenField)
                    Dim lblCFullFilmentqty As Label = DirectCast(e.Row.FindControl("lblCFullFilmentqty"), Label)
                    Dim litCFullFilmentqty As Literal = DirectCast(e.Row.FindControl("litCFullFilmentqty"), Literal)

                    Dim lblRInvoicedQty As Label = DirectCast(e.Row.FindControl("lblRInvoicedQty"), Label)
                    Dim hfRInvoicedQty As HiddenField = DirectCast(e.Row.FindControl("hfRInvoicedQty"), HiddenField)
                    Dim lblCInvoicedqty As Label = DirectCast(e.Row.FindControl("lblCInvoicedqty"), Label)
                    Dim litCInvoicedqty As Literal = DirectCast(e.Row.FindControl("litCInvoicedqty"), Literal)

                    Dim lnkEDIOppItem As LinkButton = DirectCast(e.Row.FindControl("LinkOppItemEDIFields"), LinkButton)

                    If Not lnkEDIOppItem Is Nothing Then
                        AddHandler lnkEDIOppItem.Click, AddressOf lnkEDIOppItem_Click
                    End If

                    Dim objOppBizDocs As New OppBizDocs
                    If (OppType.Value = "1") Then
                        If Not lblRPackagingSlipQty Is Nothing Then
                            Dim dv As New DataView(dtPickPackItems)
                            dv.RowFilter = "numoppitemtCode = " + Convert.ToString(DataBinder.Eval(e.Row.DataItem, "numoppitemtCode")) + ""
                            If dv.Count > 0 Then
                                lblRPackagingSlipQty.Text = CCommon.ToDouble(dv.Item(0)("QtytoFulFill"))
                                hfRPackagingSlipQty.Value = CCommon.ToDouble(dv.Item(0)("QtytoFulFillOriginal"))

                                If CCommon.ToDouble(hfRPackagingSlipQty.Value) = 0 Then
                                    lblRPackagingSlipQty.Text = 0
                                End If

                                lblCPackagingSlipQty.Text = CCommon.ToDouble(dv.Item(0)("QtyOrdered") - dv.Item(0)("QtytoFulFill"))
                                litCPackagingSlipQty.Text = CCommon.ToString(dv.Item(0)("vcPickLists"))
                            Else
                                lblRPackagingSlipQty.Text = 0
                                hfRPackagingSlipQty.Value = 0
                                lblCPackagingSlipQty.Text = 0
                                litCPackagingSlipQty.Text = 0
                            End If

                            objOppBizDocs.BizDocId = 29397
                            objOppBizDocs.OppId = lngOppId
                            objOppBizDocs.OppItemID = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "numoppitemtCode"))
                            Dim dtPickPack As New DataTable
                            dtPickPack = objOppBizDocs.GetTotalReceivedItemByBizDocType()
                            If (dtPickPack.Rows.Count > 0) Then
                                lblCPackagingSlipQty.Text = CCommon.ToDouble(dtPickPack.Rows(0)("totalUnit"))
                            End If
                        End If

                        If Not txtFullFIlmentQty Is Nothing Then
                            Dim dv As New DataView(dtFullFilmentItems)
                            dv.RowFilter = "numoppitemtCode = " + Convert.ToString(DataBinder.Eval(e.Row.DataItem, "numoppitemtCode")) + ""
                            If dv.Count > 0 Then
                                txtFullFIlmentQty.Text = CCommon.ToDouble(dv.Item(0)("QtytoFulFill"))
                                lblRFullFilmentQty.Text = CCommon.ToDouble(dv.Item(0)("QtytoFulFill"))
                                hfRFullFilmentQty.Value = CCommon.ToDouble(dv.Item(0)("QtytoFulFillOriginal"))
                                hfRFullFilmentQtyWithAllocationCheck.Value = CCommon.ToDouble(dv.Item(0)("QtyToFulfillWithoutAllocationCheck"))
                                lblCFullFilmentqty.Text = CCommon.ToDouble(dv.Item(0)("QtyOrdered") - dv.Item(0)("QtytoFulFill"))
                                litCFullFilmentqty.Text = CCommon.ToString(dv.Item(0)("vcFulfillmentBizDocs"))

                                If CCommon.ToDouble(dv.Item(0)("QtytoFulFillOriginal")) <> CCommon.ToDouble(dv.Item(0)("QtyToFulfillWithoutAllocationCheck")) Then
                                    e.Row.Style.Add("background-color", "#FFE1E1")
                                End If
                            Else
                                txtFullFIlmentQty.Text = 0
                                lblRFullFilmentQty.Text = 0
                                hfRFullFilmentQty.Value = 0
                                lblCFullFilmentqty.Text = 0
                                litCFullFilmentqty.Text = ""
                            End If
                            objOppBizDocs = New OppBizDocs()
                            objOppBizDocs.BizDocId = 296
                            objOppBizDocs.OppId = lngOppId
                            objOppBizDocs.OppItemID = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "numoppitemtCode"))
                            Dim dtFullFilment As New DataTable
                            dtFullFilment = objOppBizDocs.GetTotalReceivedItemByBizDocType()
                            If (dtFullFilment.Rows.Count > 0) Then
                                lblCFullFilmentqty.Text = dtFullFilment.Rows(0)("totalUnit")
                            End If
                        Else
                            Dim dv As New DataView(dtFullFilmentItems)
                            dv.RowFilter = "numoppitemtCode = " + Convert.ToString(DataBinder.Eval(e.Row.DataItem, "numoppitemtCode")) + ""
                            If dv.Count > 0 Then
                                If CCommon.ToDouble(dv.Item(0)("QtytoFulFillOriginal")) <> CCommon.ToDouble(dv.Item(0)("QtyToFulfillWithoutAllocationCheck")) Then
                                    e.Row.Style.Add("background-color", "#FFE1E1")
                                End If
                            End If
                        End If

                        If Not txtInvoicedQty Is Nothing Then
                            Dim dv As New DataView(dtInvoicedItems)
                            dv.RowFilter = "numoppitemtCode = " + Convert.ToString(DataBinder.Eval(e.Row.DataItem, "numoppitemtCode")) + ""
                            If dv.Count > 0 Then
                                txtInvoicedQty.Text = CCommon.ToDouble(dv.Item(0)("QtytoFulFill"))
                                lblRInvoicedQty.Text = CCommon.ToDouble(dv.Item(0)("QtytoFulFill"))
                                hfRInvoicedQty.Value = CCommon.ToDouble(dv.Item(0)("QtytoFulFillOriginal"))
                                lblCInvoicedqty.Text = CCommon.ToDouble(dv.Item(0)("QtyOrdered") - dv.Item(0)("QtytoFulFill"))
                                litCInvoicedqty.Text = CCommon.ToString(dv.Item(0)("vcInvoices"))
                            Else
                                txtInvoicedQty.Text = 0
                                lblRInvoicedQty.Text = 0
                                hfRInvoicedQty.Value = 0
                                lblCInvoicedqty.Text = 0
                                litCInvoicedqty.Text = ""
                            End If
                            objOppBizDocs = New OppBizDocs()
                            objOppBizDocs.BizDocId = 287
                            objOppBizDocs.OppId = lngOppId
                            objOppBizDocs.OppItemID = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "numoppitemtCode"))
                            Dim dtInvoice As New DataTable
                            dtInvoice = objOppBizDocs.GetTotalReceivedItemByBizDocType()
                            If (dtInvoice.Rows.Count > 0) Then
                                lblCInvoicedqty.Text = dtInvoice.Rows(0)("totalUnit")
                            End If
                        End If

                        CType(e.Row.FindControl("chkSelect"), CheckBox).Checked = True

                        If Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "bitWorkOrder")) = True Then
                            'If Not e.Row.FindControl("txtnumUnitHour") Is Nothing Then
                            '    CType(e.Row.FindControl("txtnumUnitHour"), TextBox).Enabled = False
                            'End If
                            'CType(e.Row.FindControl("chkSelect"), CheckBox).Enabled = False
                        End If

                        If DirectCast(e.Row.DataItem, System.Data.DataRowView).Row.Table.Columns.Contains("numSerialNoAssigned") Then
                            If (CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "bitSerialized")) = True Or CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "bitLotNo")) = True) AndAlso CCommon.ToInteger(DataBinder.Eval(e.Row.DataItem, "numSerialNoAssigned")) > 0 Then
                                If Not e.Row.FindControl("txtnumUnitHour") Is Nothing Then
                                    CType(e.Row.FindControl("txtnumUnitHour"), TextBox).Enabled = False
                                End If

                                CType(e.Row.FindControl("chkSelect"), CheckBox).Enabled = False
                            End If
                        End If
                    End If


                    If (OppType.Value = 2) Then
                        If DirectCast(e.Row.DataItem, System.Data.DataRowView).Row.Table.Columns.Contains("numSerialNoAssigned") Then
                            If (CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "bitSerialized")) = True Or CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "bitLotNo")) = True) AndAlso CCommon.ToInteger(DataBinder.Eval(e.Row.DataItem, "numSerialNoAssigned")) > 0 Then
                                If Not txtnumUnitHour Is Nothing Then
                                    txtnumUnitHour.Enabled = False
                                End If

                                If Not ddlUOM Is Nothing Then
                                    ddlUOM.Enabled = False
                                End If

                                CType(e.Row.FindControl("chkSelect"), CheckBox).Enabled = False
                            End If
                        End If
                    End If

                    'If Auth BizDocs associated with item then do not allow edit or delete
                    If (Convert.ToBoolean(DataBinder.Eval(e.Row.DataItem, "bitIsAuthBizDoc")) = True) Or
                        (Convert.ToBoolean(CCommon.ToInteger(DataBinder.Eval(e.Row.DataItem, "bitAddedFulFillmentBizDoc"))) = True) Or
                        (OppType.Value = 1 AndAlso CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numPromotionID")) > 0) Then
                        'If Not txtnumUnitHour Is Nothing Then
                        '    txtnumUnitHour.Enabled = False
                        'End If

                        If Not txtmonPriceUOM Is Nothing Then
                            txtmonPriceUOM.Enabled = False
                        End If

                        If Not ddlUOM Is Nothing Then
                            ddlUOM.Enabled = False
                        End If

                        If Not radWarehouse Is Nothing Then
                            radWarehouse.Enabled = False
                        End If

                        If Not txtDiscount Is Nothing Then
                            txtDiscount.Enabled = False
                        End If

                        If Not radPer Is Nothing Then
                            radPer.Enabled = False
                        End If

                        If Not radAmt Is Nothing Then
                            radAmt.Enabled = False
                        End If

                        CType(e.Row.FindControl("chkSelect"), CheckBox).Enabled = False
                        btnRecalculatePrice.Visible = False
                    End If

                    If Not txtnumUnitHour Is Nothing Then
                        txtnumUnitHour.AutoPostBack = True
                        txtnumUnitHour.Attributes.Add("onchange", "return validateQty('" & txtnumUnitHour.ClientID & "'," & OppType.Value & "," & lngOppId & "," & CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numoppitemtcode")) & ");")
                        'Quantity is now updated through jquery inside validateQty
                        'AddHandler txtnumUnitHour.TextChanged, AddressOf txtUnitsGrid_TextChanged
                    End If

                    If Not rdpItemRequiredDate Is Nothing Then
                        rdpItemRequiredDate.AutoPostBack = True
                        AddHandler rdpItemRequiredDate.SelectedDateChanged, AddressOf rdpItemRequiredDate_SelectedDateChanged
                    End If

                    If Not txtmonPriceUOM Is Nothing Then
                        txtmonPriceUOM.AutoPostBack = True
                        txtmonPriceUOM.Attributes.Add("onchange", "return UpdateOrderItemPrice('" & txtmonPriceUOM.ClientID & "'," & OppType.Value & "," & lngOppId & "," & CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numoppitemtcode")) & ");")
                        'Price is now updated through jquery inside validateQty
                        'AddHandler txtmonPriceUOM.TextChanged, AddressOf txtPriceUOM_TextChanged
                    End If

                    If Not e.Row.FindControl("txtvcItemDesc") Is Nothing Then
                        DirectCast(e.Row.FindControl("txtvcItemDesc"), TextBox).Attributes.Add("onchange", "return UpdateOrderItemDescription('" & DirectCast(e.Row.FindControl("txtvcItemDesc"), TextBox).ClientID & "'," & OppType.Value & "," & lngOppId & "," & CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numoppitemtcode")) & ");")
                    End If

                    If Not e.Row.FindControl("txtvcNotes") Is Nothing Then
                        DirectCast(e.Row.FindControl("txtvcNotes"), TextBox).Attributes.Add("onchange", "return UpdateOrderItemNotes('" & DirectCast(e.Row.FindControl("txtvcNotes"), TextBox).ClientID & "'," & OppType.Value & "," & lngOppId & "," & CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numoppitemtcode")) & ");")
                    End If

                    If Not radWarehouse Is Nothing Then
                        radWarehouse.DataSource = objCommon.GetWarehouseAttrBasedItem(DataBinder.Eval(e.Row.DataItem, "numItemCode"))
                        radWarehouse.DataBind()
                        radWarehouse.SelectedValue = If(DataBinder.Eval(e.Row.DataItem, "numWarehouseItmsID") Is DBNull.Value, 0, DataBinder.Eval(e.Row.DataItem, "numWarehouseItmsID"))
                        AddHandler radWarehouse.SelectedIndexChanged, AddressOf radWarehouse_SelectedIndexChanged

                        If OppType.Value = "1" Then
                            radWarehouse.Enabled = False
                        Else
                            Dim objOppBizDocsNew As New OppBizDocs
                            objOppBizDocsNew.OppId = lngOppId
                            objOppBizDocsNew.byteMode = 1
                            Dim dsBizDocs As DataSet = objOppBizDocsNew.GetBizDocsInOpp()

                            If Not dsBizDocs Is Nothing AndAlso dsBizDocs.Tables.Count > 0 AndAlso dsBizDocs.Tables(0).Rows.Count > 0 Then
                                radWarehouse.Enabled = False
                            End If
                        End If
                    End If



                    If Not ddlUOM Is Nothing Then
                        If Not ddlUOM.Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "numUOM")) Is Nothing Then
                            ddlUOM.SelectedValue = DataBinder.Eval(e.Row.DataItem, "numUOM")
                        End If
                        AddHandler ddlUOM.SelectedIndexChanged, AddressOf ddlUOM_SelectedIndexChanged
                    End If

                    If Not txtDiscount Is Nothing Then
                        txtDiscount.AutoPostBack = True
                        CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(DataBinder.Eval(e.Row.DataItem, "fltDiscount")), txtDiscount)
                        AddHandler txtDiscount.TextChanged, AddressOf txtDiscount_TextChanged
                    End If

                    If Not radPer Is Nothing Then
                        radPer.AutoPostBack = True
                        radPer.Checked = Not CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "bitDiscountType"))
                        AddHandler radPer.CheckedChanged, AddressOf radPer_CheckedChanged
                    End If

                    If Not radAmt Is Nothing Then
                        radAmt.AutoPostBack = True
                        radAmt.Checked = CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "bitDiscountType"))
                        AddHandler radAmt.CheckedChanged, AddressOf radAmt_CheckedChanged
                    End If

                    If Not ddlVendor Is Nothing Then

                        Dim objItems As New CItems
                        objItems.DomainID = Session("DomainID")
                        objItems.ItemCode = CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numItemCode"))
                        ddlVendor.DataSource = objItems.GetVendors
                        ddlVendor.DataTextField = "Vendor"
                        ddlVendor.DataValueField = "numVendorID"
                        ddlVendor.DataBind()
                        ddlVendor.Items.Insert(0, New ListItem("-- Select One --", 0))

                        AddHandler ddlVendor.SelectedIndexChanged, AddressOf ddlVendor_SelectedIndexChanged
                    End If

                    Dim ddl As DropDownList = CType(e.Row.FindControl("ddlProject"), DropDownList)
                    If ddl IsNot Nothing Then
                        BindProject(ddl)

                        If Not ddl.Items.FindByValue(CCommon.ToDouble(DataBinder.Eval(e.Row.DataItem, "numProjectID"))) Is Nothing Then
                            ddl.ClearSelection()
                            ddl.Items.FindByValue(CCommon.ToDouble(DataBinder.Eval(e.Row.DataItem, "numProjectID"))).Selected = True
                        End If

                        Dim imgProject As Label = CType(e.Row.FindControl("imgProject"), Label)
                        If e.Row.RowIndex = 0 AndAlso ddl.Enabled = True Then
                            imgProject.Visible = True
                            imgProject.Attributes.Add("onclick", "return dropDownChange('" & ddl.ClientID & "','SelectProject')")
                        Else
                            imgProject.Visible = False
                        End If

                        If ddl.Enabled Then
                            ddl.CssClass = "SelectProject"
                        End If

                        'Do not allow changing of project or class once its already into accounting 
                        If CCommon.ToInteger(DataBinder.Eval(e.Row.DataItem, "bitItemAddedToAuthBizDoc")) = 1 Then
                            ddl.Enabled = False
                        End If

                    End If

                    Dim ddl1 As DropDownList = CType(e.Row.FindControl("ddlClass"), DropDownList)
                    If ddl1 IsNot Nothing Then
                        BindClass(ddl1)

                        Dim classID As Long
                        If CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numClassID")) > 0 Then
                            classID = CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numClassID"))
                        End If
                        If Not ddl1.Items.FindByValue(classID) Is Nothing Then
                            ddl1.ClearSelection()
                            ddl1.Items.FindByValue(classID).Selected = True
                        End If

                        'Do not allow changing of project or class once its already into accounting 
                        If CCommon.ToInteger(DataBinder.Eval(e.Row.DataItem, "bitItemAddedToAuthBizDoc")) = 1 Then
                            ddl1.Enabled = False
                        End If
                    End If
                    'Added By:Sachin Sadhu|| Date:12thSept2014
                    'Purpose : Add Rental Status Fields Dynamically

                    If CCommon.ToInteger(DataBinder.Eval(e.Row.DataItem, "tintOppType")) = 1 Then 'Sales Order

                        If CCommon.ToBool((DataBinder.Eval(e.Row.DataItem, "bitAsset"))) = True Then
                            e.Row.FindControl("txtDYRentalIN").Visible = True
                            e.Row.FindControl("txtDYRentalOut").Visible = True
                            e.Row.FindControl("btnDYRentalIN").Visible = True
                            e.Row.FindControl("btnDYRentalOut").Visible = True

                            e.Row.FindControl("btnDYRentalLost").Visible = True
                            e.Row.FindControl("txtDYRentalLost").Visible = True

                            Dim btnDYRentalLost As Button = CType(e.Row.FindControl("btnDYRentalLost"), Button)
                            Dim btnDYRentalIN As Button = CType(e.Row.FindControl("btnDYRentalIN"), Button)
                            Dim btnDYRentalOut As Button = CType(e.Row.FindControl("btnDYRentalOut"), Button)
                            Dim txtDYRentalOut As TextBox = CType(e.Row.FindControl("txtDYRentalOut"), TextBox)

                            Dim txtDYRentalIN As TextBox = CType(e.Row.FindControl("txtDYRentalIN"), TextBox)
                            Dim txtDYRentalLost As TextBox = CType(e.Row.FindControl("txtDYRentalLost"), TextBox)

                            'e.Row.FindControl("lblPendingRentalOut").Visible = False
                            'e.Row.FindControl("lblPendingRentalIN").Visible = False
                            'e.Row.FindControl("lblPendingRentalLost").Visible = False
                            Dim lblPendingRentalOut As Label = CType(e.Row.FindControl("lblPendingRentalOut"), Label)
                            Dim lblPendingRentalIN As Label = CType(e.Row.FindControl("lblPendingRentalIN"), Label)
                            Dim lblPendingRentalLost As Label = CType(e.Row.FindControl("lblPendingRentalLost"), Label)

                            btnDYRentalLost.CommandArgument = CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numItemCode"))
                            btnDYRentalIN.CommandArgument = CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numItemCode"))
                            btnDYRentalOut.CommandArgument = CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numItemCode"))

                            btnDYRentalIN.CommandName = "RentalIN"
                            btnDYRentalOut.CommandName = "RentalOut"
                            btnDYRentalLost.CommandName = "RentalLost"

                            AddHandler btnDYRentalIN.Command, AddressOf GridButtons_Command
                            AddHandler btnDYRentalOut.Command, AddressOf GridButtons_Command
                            AddHandler btnDYRentalLost.Command, AddressOf GridButtons_Command

                            btnDYRentalIN.Attributes.Add("onclick", "return CheckAllSameOpp();")
                            btnDYRentalOut.Attributes.Add("onclick", "return CheckAllSameOpp();")
                            btnDYRentalLost.Attributes.Add("onclick", "return CheckAllSameOpp();")


                            If (CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numRentalOut"))) = 0 Then 'Allocation =0
                                btnDYRentalOut.Enabled = True
                                txtDYRentalOut.Enabled = True

                                txtDYRentalIN.Enabled = False
                                btnDYRentalIN.Enabled = False

                                txtDYRentalLost.Enabled = False
                                btnDYRentalLost.Enabled = False
                            Else
                                If (CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numRentalOut")) >= CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numOriginalUnitHour"))) Then
                                    btnDYRentalOut.Enabled = False
                                    txtDYRentalOut.Enabled = False
                                Else
                                    btnDYRentalOut.Enabled = True
                                    txtDYRentalOut.Enabled = True
                                End If

                                txtDYRentalIN.Enabled = True
                                btnDYRentalIN.Enabled = True
                                txtDYRentalLost.Enabled = True
                                btnDYRentalLost.Enabled = True
                            End If

                            Dim lngOut = (CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numOriginalUnitHour")) - CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numRentalOut")))
                            Dim lngIN = (CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numRentalOut")) - (CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numRentalIN")) + CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numRentalLost"))))
                            Dim lngLost = (CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numRentalOut")) - (CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numRentalIN")) + CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numRentalLost"))))

                            If (CCommon.ToLong(lngOut) = 0 And CCommon.ToLong(lngIN) = 0 And CCommon.ToLong(lngLost) = 0) Then
                                txtDYRentalIN.Enabled = True
                                btnDYRentalIN.Enabled = True
                                txtDYRentalLost.Enabled = True
                                btnDYRentalLost.Enabled = True
                                btnDYRentalOut.Enabled = True
                                txtDYRentalOut.Enabled = True
                            Else
                                txtDYRentalIN.Text = lngIN
                                txtDYRentalOut.Text = lngOut
                            End If

                        Else
                            If Not e.Row.FindControl("txtDYRentalIN") Is Nothing Then
                                e.Row.FindControl("txtDYRentalIN").Visible = False
                            End If
                            If Not e.Row.FindControl("txtDYRentalOut") Is Nothing Then
                                e.Row.FindControl("txtDYRentalOut").Visible = False
                            End If
                            If Not e.Row.FindControl("btnDYRentalIN") Is Nothing Then
                                e.Row.FindControl("btnDYRentalIN").Visible = False
                            End If
                            If Not e.Row.FindControl("btnDYRentalOut") Is Nothing Then
                                e.Row.FindControl("btnDYRentalOut").Visible = False
                            End If
                            If Not e.Row.FindControl("btnDYRentalLost") Is Nothing Then
                                e.Row.FindControl("btnDYRentalLost").Visible = False
                            End If
                            If Not e.Row.FindControl("txtDYRentalLost") Is Nothing Then
                                e.Row.FindControl("txtDYRentalLost").Visible = False
                            End If
                            If Not e.Row.FindControl("lblPendingRentalOut") Is Nothing Then
                                e.Row.FindControl("lblPendingRentalOut").Visible = False
                            End If
                            If Not e.Row.FindControl("lblPendingRentalIN") Is Nothing Then
                                e.Row.FindControl("lblPendingRentalIN").Visible = False
                            End If
                            If Not e.Row.FindControl("lblPendingRentalLost") Is Nothing Then
                                e.Row.FindControl("lblPendingRentalLost").Visible = False
                            End If
                        End If


                    End If

                    'end of code

                    'Added By :Sachin sadhu ||Date:18thJul2014
                    'Purpose :To Save Custom Fields
                    Dim dtTable As DataTable

                    Dim ObjCus As New CustomFields
                    ObjCus.DomainID = Session("DomainID")
                    dtTable = ObjCus.GetCustFldsOppItems(CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numoppitemtcode")), CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numItemCode"))).Tables(0)
                    If dtTable.Rows.Count > 0 Then
                        For i = 0 To dtTable.Rows.Count - 1
                            If dtTable.Rows(i).Item("fld_type") = "SelectBox" Then
                                Dim ddl2 As DropDownList = CType(e.Row.FindControl(dtTable.Rows(i).Item("fld_id")), DropDownList)
                                If ddl2 IsNot Nothing Then
                                    BindRate(ddl2, CCommon.ToInteger(dtTable.Rows(i).Item("numlistid")))
                                    Dim CustomID As String
                                    If CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numItemCode")) > 0 Then
                                        CustomID = CCommon.ToString(DataBinder.Eval(e.Row.DataItem, dtTable.Rows(i).Item("fld_label")))

                                    End If
                                    If Not ddl2.Items.FindByText(CustomID) Is Nothing Then
                                        ddl2.ClearSelection()
                                        ddl2.Items.FindByText(CustomID).Selected = True
                                    End If
                                End If
                            ElseIf dtTable.Rows(i).Item("fld_type") = "TextBox" Then
                                Dim txt As TextBox = CType(e.Row.FindControl(dtTable.Rows(i).Item("fld_id")), TextBox)
                                If txt IsNot Nothing Then
                                    'BindRate(ddl2, CCommon.ToInteger(dtTable.Rows(i).Item("numlistid")))
                                    Dim CustomID As String
                                    If CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numItemCode")) > 0 Then
                                        CustomID = CCommon.ToString(DataBinder.Eval(e.Row.DataItem, dtTable.Rows(i).Item("fld_label")))

                                    End If
                                    If Not CustomID Is Nothing Then
                                        txt.Text = ""
                                        txt.Text = CustomID
                                    End If
                                End If
                            ElseIf dtTable.Rows(i).Item("fld_type") = "TextArea" Then
                                Dim txtArea As TextBox = CType(e.Row.FindControl(dtTable.Rows(i).Item("fld_id")), TextBox)
                                If txtArea IsNot Nothing Then
                                    'BindRate(ddl2, CCommon.ToInteger(dtTable.Rows(i).Item("numlistid")))
                                    Dim CustomID As String
                                    If CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numItemCode")) > 0 Then
                                        CustomID = CCommon.ToString(DataBinder.Eval(e.Row.DataItem, dtTable.Rows(i).Item("fld_label")))

                                    End If
                                    If Not CustomID Is Nothing Then
                                        txtArea.Text = ""
                                        txtArea.Text = CustomID
                                    End If
                                End If
                            ElseIf dtTable.Rows(i).Item("fld_type") = "CheckBox" Then
                                Dim chk As CheckBox = CType(e.Row.FindControl(dtTable.Rows(i).Item("fld_id")), CheckBox)
                                If chk IsNot Nothing Then
                                    'BindRate(ddl2, CCommon.ToInteger(dtTable.Rows(i).Item("numlistid")))
                                    Dim CustomID As Integer
                                    If CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numItemCode")) > 0 Then
                                        'CustomID = CCommon.ToInteger(DataBinder.Eval(e.Row.DataItem, dtTable.Rows(i).Item("fld_label")))
                                        CustomID = CCommon.ToInteger(dtTable.Rows(i).Item("Value"))

                                    End If
                                    If CustomID = 1 Then
                                        chk.Checked = True
                                    Else
                                        chk.Checked = False
                                    End If
                                End If
                            ElseIf dtTable.Rows(i).Item("fld_type") = "Link" Then
                                Dim hpl As HyperLink = CType(e.Row.FindControl(dtTable.Rows(i).Item("fld_id")), HyperLink)
                                If hpl IsNot Nothing Then
                                    'BindRate(ddl2, CCommon.ToInteger(dtTable.Rows(i).Item("numlistid")))
                                    Dim CustomID As Integer
                                    If CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numItemCode")) > 0 Then
                                        'CustomID = CCommon.ToInteger(DataBinder.Eval(e.Row.DataItem, dtTable.Rows(i).Item("fld_label")))
                                        CustomID = CCommon.ToInteger(dtTable.Rows(i).Item("Value"))
                                    End If
                                End If
                            ElseIf dtTable.Rows(i).Item("fld_type") = "DateField" Then
                                Dim dtPicker As RadDatePicker
                                dtPicker = CType(e.Row.FindControl("rdpCustomDateColumn" & dtTable.Rows(i).Item("fld_id") & DataBinder.Eval(e.Row.DataItem, "numoppitemtcode")), RadDatePicker)
                            End If
                            'end of code:sachin

                        Next
                    End If

                    If isMinUnitPriceRuleSet Then
                        If CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "bitItemPriceApprovalRequired")) Then
                            Dim lblmonPrice As Label = e.Row.FindControl("lblmonPrice")
                            Dim lblmonTotAmount As Label = e.Row.FindControl("lblmonTotAmount")
                            If Not lblmonPrice Is Nothing Then
                                lblmonPrice.Text += "&nbsp;<span style=""color:red;font-weight:bold"">!</span>"
                            End If

                            If Not lblmonTotAmount Is Nothing Then
                                lblmonTotAmount.Text += "&nbsp;<span style=""color:red;font-weight:bold"">!</span>"
                            End If
                        End If
                    End If

                    'KEEP ALL THE BELOW IF CONDITIONS AT LAST
                    If hdnEditUnitPriceRight.Value = "False" AndAlso Not txtmonPriceUOM Is Nothing Then
                        txtmonPriceUOM.Enabled = False
                    End If

                    If (OppType.Value = "1") Then
                        If CCommon.ToDouble(DataBinder.Eval(e.Row.DataItem, "numQtyShipped")) > 0 Then
                            CType(e.Row.FindControl("chkSelect"), CheckBox).Enabled = False

                            'If Not txtnumUnitHour Is Nothing Then
                            '    txtnumUnitHour.Enabled = False
                            'End If

                            If Not txtmonPriceUOM Is Nothing Then
                                txtmonPriceUOM.Enabled = False
                            End If

                            If Not radWarehouse Is Nothing Then
                                radWarehouse.Enabled = False
                            End If

                            If Not txtDiscount Is Nothing Then
                                txtDiscount.Enabled = False
                            End If

                            If Not radPer Is Nothing Then
                                radPer.Enabled = False
                            End If

                            If Not radAmt Is Nothing Then
                                radAmt.Enabled = False
                            End If
                        End If
                    ElseIf (OppType.Value = "2") Then
                        If CCommon.ToDouble(DataBinder.Eval(e.Row.DataItem, "numUnitHourReceived")) > 0 Then
                            CType(e.Row.FindControl("chkSelect"), CheckBox).Enabled = False

                            'If Not txtnumUnitHour Is Nothing Then
                            '    txtnumUnitHour.Enabled = False
                            'End If

                            If Not txtmonPriceUOM Is Nothing Then
                                txtmonPriceUOM.Enabled = False
                            End If

                            If Not radWarehouse Is Nothing Then
                                radWarehouse.Enabled = False
                            End If

                            If Not txtDiscount Is Nothing Then
                                txtDiscount.Enabled = False
                            End If

                            If Not radPer Is Nothing Then
                                radPer.Enabled = False
                            End If

                            If Not radAmt Is Nothing Then
                                radAmt.Enabled = False
                            End If
                        End If
                    End If

                    If (txtnumUnitHour IsNot Nothing) Then
                        txtnumUnitHour.Text = CCommon.GetDecimalFormat(CCommon.ToDouble(txtnumUnitHour.Text))
                    End If
                    If (txtmonPriceUOM IsNot Nothing) Then
                        txtmonPriceUOM.Text = CCommon.GetDecimalFormat(CCommon.ToDouble(txtmonPriceUOM.Text))
                    End If
                    If (txtFullFIlmentQty IsNot Nothing) Then
                        txtFullFIlmentQty.Text = String.Format("{0:#,##0.####################}", CCommon.ToDouble(txtFullFIlmentQty.Text))
                    End If
                    If (txtInvoicedQty IsNot Nothing) Then
                        txtInvoicedQty.Text = String.Format("{0:#,##0.####################}", CCommon.ToDouble(txtInvoicedQty.Text))
                    End If
                    If (lblCPackagingSlipQty IsNot Nothing) Then
                        lblCPackagingSlipQty.Text = String.Format("{0:#,##0.####################}", CCommon.ToDouble(lblCPackagingSlipQty.Text))
                    End If
                    If (lblRPackagingSlipQty IsNot Nothing) Then
                        lblRPackagingSlipQty.Text = String.Format("{0:#,##0.####################}", CCommon.ToDouble(lblRPackagingSlipQty.Text))
                    End If
                    If (lblRFullFilmentQty IsNot Nothing) Then
                        lblRFullFilmentQty.Text = String.Format("{0:#,##0.####################}", CCommon.ToDouble(lblRFullFilmentQty.Text))
                    End If
                    If (lblCFullFilmentqty IsNot Nothing) Then
                        lblCFullFilmentqty.Text = String.Format("{0:#,##0.####################}", CCommon.ToDouble(lblCFullFilmentqty.Text))
                    End If
                    If (lblCInvoicedqty IsNot Nothing) Then
                        lblCInvoicedqty.Text = String.Format("{0:#,##0.####################}", CCommon.ToDouble(lblCInvoicedqty.Text))
                    End If
                    If (lblRInvoicedQty IsNot Nothing) Then
                        lblRInvoicedQty.Text = String.Format("{0:#,##0.####################}", CCommon.ToDouble(lblRInvoicedQty.Text))
                    End If

                    If Not btnReceivedOrShipped.Visible AndAlso txtnumUnitHour IsNot Nothing Then
                        txtnumUnitHour.Enabled = False
                    End If

                    If Not e.Row.FindControl("btnWinningBid") Is Nothing Then
                        DirectCast(e.Row.FindControl("btnWinningBid"), Button).Visible = CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "bitKitParent"))
                        If CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "bitWinningBid")) Then
                            DirectCast(e.Row.FindControl("btnWinningBid"), Button).Style.Add("display", "none")
                        Else
                            DirectCast(e.Row.FindControl("btnWinningBid"), Button).Style.Remove("display")
                        End If
                    End If

                    If Not CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "bitKitParent")) AndAlso Not e.Row.FindControl("ddlParentKit") Is Nothing AndAlso dsTemp.Tables(0).Select("bitKitParent=1").Length > 0 Then
                        Dim arrKits As DataRow() = dsTemp.Tables(0).Select("bitKitParent=1")

                        If Not arrKits Is Nothing AndAlso arrKits.Length > 0 Then
                            DirectCast(e.Row.FindControl("ddlParentKit"), DropDownList).Visible = True

                            DirectCast(e.Row.FindControl("ddlParentKit"), DropDownList).DataSource = arrKits.CopyToDataTable()
                            DirectCast(e.Row.FindControl("ddlParentKit"), DropDownList).DataTextField = "vcItemName"
                            DirectCast(e.Row.FindControl("ddlParentKit"), DropDownList).DataValueField = "numoppitemtCode"
                            DirectCast(e.Row.FindControl("ddlParentKit"), DropDownList).DataBind()
                            DirectCast(e.Row.FindControl("ddlParentKit"), DropDownList).Items.Insert(0, New ListItem("-- Select One --", "0"))

                            If Not DirectCast(e.Row.FindControl("ddlParentKit"), DropDownList).Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "numParentOppItemID")) Is Nothing Then
                                DirectCast(e.Row.FindControl("ddlParentKit"), DropDownList).Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "numParentOppItemID")).Selected = True
                            End If
                        End If
                    End If

                    If CCommon.ToDouble(DataBinder.Eval(e.Row.DataItem, "bitBackOrder")) > 0 Then
                        e.Row.Style.Add("background-color", "#FFE1E1")
                    End If

                    If CCommon.ToBool(DataBinder.Eval(e.Row.DataItem, "bitMappingRequired")) Then
                        If Not e.Row.FindControl("chkPickPackShip") Is Nothing Then
                            DirectCast(e.Row.FindControl("chkPickPackShip"), CheckBox).Checked = False
                            DirectCast(e.Row.FindControl("chkPickPackShip"), CheckBox).Visible = False
                        End If
                        e.Row.Style.Add("background-color", "#ffffcc")
                        DirectCast(e.Row.FindControl("lbCreateOrMapItem"), LinkButton).Visible = True
                        DirectCast(e.Row.FindControl("lbCreateOrMapItem"), LinkButton).Attributes.Add("onclick", "CreateOrMapItem(" & CCommon.ToLong(OppID.Value) & "," & CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numoppitemtCode")) & ")")
                    End If

                    If Not e.Row.FindControl("hplOnOrder129") Is Nothing Then
                        DirectCast(e.Row.FindControl("hplOnOrder129"), HyperLink).Attributes.Add("onclick", "OpenInTransit(" & CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numItemCode")) & "," & CCommon.ToLong(DataBinder.Eval(e.Row.DataItem, "numWarehouseItmsID")) & ")")
                        DirectCast(e.Row.FindControl("hplOnOrder129"), HyperLink).Attributes.Add("Style", "color:blue;text-decoration:underline")
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub


        Dim dtProject As DataTable
        Private Sub BindProject(ByRef ddlProject As DropDownList)
            Try
                If dtProject Is Nothing Then
                    Dim objProject As New Project
                    objProject.DomainID = Session("DomainID")
                    dtProject = objProject.GetOpenProject()
                End If

                ddlProject.DataTextField = "vcProjectName"
                ddlProject.DataValueField = "numProId"
                ddlProject.DataSource = dtProject
                ddlProject.DataBind()
                ddlProject.Items.Insert(0, "--Select One--")
                ddlProject.Items.FindByText("--Select One--").Value = "0"
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Dim dtClass As DataTable
        Private Sub BindClass(ByRef ddlClass As DropDownList)
            Try
                If dtClass Is Nothing Then
                    Dim objAdmin As New CAdmin
                    objAdmin.DomainID = Session("DomainID")
                    objAdmin.Mode = 1
                    dtClass = objAdmin.GetClass()
                End If
                ddlClass.DataTextField = "ClassName"
                ddlClass.DataValueField = "numChildClassID"
                ddlClass.DataSource = dtClass
                ddlClass.DataBind()
                ddlClass.Items.Insert(0, "--Select One--")
                ddlClass.Items.FindByText("--Select One--").Value = "0"
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub BindRate(ByRef ddlRate As DropDownList, ByVal numListID As Integer)
            Try
                'If dtProject Is Nothing Then
                '    Dim objProject As New Project
                '    objProject.DomainID = Session("DomainID")
                '    dtProject = objProject.GetOpenProject()
                'End If

                'ddlProject.DataTextField = "vcProjectName"
                'ddlProject.DataValueField = "numProId"
                'ddlProject.DataSource = dtProject
                'ddlProject.DataBind()
                'ddlProject.Items.Insert(0, "--Select One--")
                'ddlProject.Items.FindByText("--Select One--").Value = "0"

                Dim ObjCusfld As New CustomFields
                Dim dtTable As DataTable
                ObjCusfld.ListId = CInt(numListID)
                dtTable = ObjCusfld.GetMasterListByListId
                ddlRate.DataSource = dtTable
                ddlRate.DataTextField = "vcData"
                ddlRate.DataValueField = "numListItemID"
                ddlRate.DataBind()
                ddlRate.Items.Insert(0, "--Select One--")
                ddlRate.Items.FindByText("--Select One--").Value = 0
                'ddlRate.Width = Unit.Pixel(180)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub btnReOpenDeal_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnReOpenDeal.Click
            Try
                objOpportunity.OpportunityId = lngOppId
                Dim intResult As Integer = objOpportunity.CheckCanbeReOpenOppertunity()
                If intResult = 1 Then
                    ShowMessage("You are not allowed to Re-Open Deal, your option is to ensure ""Warehouse Stock OnHand"" qty of all items within PO are greater  than or equals to Ordered Qty")
                ElseIf intResult = -2 Then
                    ShowMessage("You are not allowed to Re-Open Deal, your option is to delete ""Purchase Return"" against selected PO and try again")
                ElseIf intResult = -1 Then
                    ShowMessage("You are not allowed to Re-Open Deal, your option is to delete ""Sales Return"" against selected SO and try again")
                Else
                    objOpportunity.UserCntID = Session("UserContactID")
                    objOpportunity.DomainID = Session("DomainID")

                    objOpportunity.ReOpenOppertunity()
                    ShowMessage("The Deal is now Re-Open")
                    btnReceivedOrShipped.Visible = True
                    btnAddEditOrder.Visible = True
                    btnUpdateLabel.Visible = True
                    lkbDelete.Visible = True
                    btnSave.Enabled = True
                    btnSaveClose.Enabled = True
                    btnUpdate.Enabled = True
                    btnReOpenDeal.Visible = False
                    btnActdelete.Attributes.Add("style", "")
                    btnActdelete.Visible = True
                    'lblDealCompletedDate.Visible = False

                    BindItems(dsTemp)
                End If
            Catch ex As Exception
                If CCommon.ToString(ex.Message).Contains("ORDER_OPENED") Then
                    ShowMessage("This order is already Re-Opened. Please reload page.")
                ElseIf CCommon.ToString(ex.Message).Contains("SERIAL/LOT#_USED") Then
                    ShowMessage("you can't open order because some serial/lot# are used in some sales order.")
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(ex.Message)
                End If
            End Try
        End Sub
        Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            Try
                CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnMoreFoSelection_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMoreFoSelection.Click
            Try
                If CCommon.ToLong(rdnFOSelection.SelectedValue) = 0 Then
                    lblPickPackShipMoreException.Text = "Please select source bizdoc."
                    divPickPackShipMoreException.Style.Add("display", "")
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "openOtherPaymentDropDown", "$('#dialMoreFO').modal('show');", True)
                Else
                    If hdnSelectedType.Value = "3" AndAlso (ViewState("InvoiceIterationData") Is Nothing Or String.IsNullOrEmpty(hdnInvoiceInterationValue.Value)) Then
                        ShowMessage("Something went wrong.")
                    ElseIf hdnSelectedType.Value <> "3" AndAlso (ViewState("FulfillmentIterationData") Is Nothing Or String.IsNullOrEmpty(hdnFulfillentInterationValue.Value)) Then
                        ShowMessage("Something went wrong.")
                    Else
                        Dim boolIsQtyIsMoreThanParent As Boolean = False
                        Dim objOppBizDocs As New OppBizDocs
                        If hdnSelectedType.Value = "3" Then
                            objOppBizDocs.BizDocId = 296
                        Else
                            objOppBizDocs.BizDocId = 29397
                        End If

                        objOppBizDocs.DomainID = Session("DomainID")
                        objOppBizDocs.OppId = lngOppId
                        objOppBizDocs.OppBizDocId = rdnFOSelection.SelectedValue
                        Dim dtCreatedPickPackItems As DataTable
                        dtCreatedPickPackItems = objOppBizDocs.GetBizDocItemsForPickPackSlip(If(hdnSelectedType.Value = "3", 287, 296))
                        If (dtCreatedPickPackItems.Rows.Count > 0) Then
                            For Each dr As GridViewRow In gvProduct.Rows
                                Dim txtItemQty As TextBox
                                Dim hfRemainingQty As HiddenField
                                If hdnSelectedType.Value = "3" Then
                                    txtItemQty = dr.FindControl("txtInvoicedQty")
                                    hfRemainingQty = dr.FindControl("hfRInvoicedQty")
                                Else
                                    txtItemQty = dr.FindControl("txtFullFIlmentQty")
                                    hfRemainingQty = dr.FindControl("hfRFullFilmentQty")
                                End If

                                If CCommon.ToDouble(txtItemQty.Text) > CCommon.ToDouble(hfRemainingQty.Value) Then
                                    txtItemQty.Text = hfRemainingQty.Value
                                End If

                                Dim dv As New DataView(dtCreatedPickPackItems)
                                dv.RowFilter = "numoppitemtcode = " + Convert.ToString(gvProduct.DataKeys(dr.RowIndex)("numoppitemtcode")) + ""
                                If dv.Count > 0 Then
                                    If CCommon.ToDouble(txtItemQty.Text) > CCommon.ToDouble(dv.Item(0)("RemainingQty")) Then
                                        txtItemQty.Text = dv.Item(0)("RemainingQty")
                                        boolIsQtyIsMoreThanParent = True
                                    End If
                                End If
                            Next
                        End If
                        Dim IsBizDocCreated As Boolean
                        If hdnSelectedType.Value = "3" Then
                            IsBizDocCreated = CreateBizDoc(287, 3, hdnSelectedTempId.Value, CCommon.ToLong(hdnInvoiceInterationValue.Value.Split("-")(0)), CCommon.ToLong(hdnInvoiceInterationValue.Value.Split("-")(1)), CCommon.ToBool(ViewState("InvoiceFirstIteration")), rdnFOSelection.SelectedValue)
                            If (IsBizDocCreated = True) Then
                                btnOpenInvoice_Click(sender, e)
                            End If

                            Dim arrShipFromTo As Hashtable = DirectCast(ViewState("InvoiceIterationData"), Hashtable)
                            arrShipFromTo(hdnInvoiceInterationValue.Value) = 1
                            hdnInvoiceInterationValue.Value = ""

                            If arrShipFromTo.ContainsValue(0) Then
                                btnInvoice_Click(Nothing, Nothing)
                            Else
                                ViewState("InvoiceIterationData") = Nothing
                                ViewState("InvoiceIteration") = False
                                ViewState("InvoiceFirstIteration") = Nothing
                                hdnInvoiceInterationValue.Value = ""

                                LoadDataToSession()
                                BindItems(dsTemp)
                            End If
                        Else
                            IsBizDocCreated = CreateFulfillmentBizDoc(Session("DomainID"), Session("UserContactID"), hdnSelectedTempId.Value, rdnFOSelection.SelectedValue, CCommon.ToLong(hdnFulfillentInterationValue.Value.Split("-")(0)), CCommon.ToLong(hdnFulfillentInterationValue.Value.Split("-")(1)), CCommon.ToBool(ViewState("FulfillmentFirstIteration")))
                            If (IsBizDocCreated = True) Then
                                If (hdnShippingBizDoc.Value = "1" AndAlso hdnInventoryItems.Value = "1" AndAlso CCommon.ToLong(Session("numDefaultSalesShippingDoc")) > 0) Then
                                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, Guid.NewGuid().ToString(), "<script>window.open('../opportunity/frmShippingBox.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=" + Convert.ToString(lngOppId) + "&OppBizDocId=" + Convert.ToString(lngBizDocId) + "&ShipCompID=0', '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=850,height=700,scrollbars=yes,resizable=yes');</script>", False)
                                End If
                            End If

                            Dim arrShipFromTo As Hashtable = DirectCast(ViewState("FulfillmentIterationData"), Hashtable)
                            arrShipFromTo(hdnFulfillentInterationValue.Value) = 1
                            hdnFulfillentInterationValue.Value = ""

                            If arrShipFromTo.ContainsValue(0) Then
                                btnFulFillmentOrder_Click(Nothing, Nothing)
                            Else
                                ViewState("FulfillmentIterationData") = Nothing
                                ViewState("FulfillmentFirstIteration") = Nothing
                                ViewState("FulfillmentIteration") = Nothing
                                hdnFulfillentInterationValue.Value = ""

                                LoadDataToSession()
                                BindItems(dsTemp)
                            End If
                        End If
                    End If
                End If
            Catch ex As Exception
                If ex.Message.Contains("WORK_ORDER_NOT_COMPLETED") Then
                    ShowMessage("Not able to add fulfillment bizdoc because some work orders are still not completed.")
                ElseIf ex.Message.Contains("QTY_MISMATCH") Then
                    ShowMessage("Not able to add fulfillment bizdoc because qty is not same as required qty.<br />")
                ElseIf ex.Message.Contains("REQUIRED_SERIALS_NOT_PROVIDED") Then
                    ShowMessage("Not able to add fulfillment bizdoc because required number of serials are not provided.<br />")
                ElseIf ex.Message.Contains("INVALID_SERIAL_NUMBERS") Then
                    ShowMessage("Not able to add fulfillment bizdoc because invalid serials are provided.<br />")
                ElseIf ex.Message.Contains("REQUIRED_LOTNO_NOT_PROVIDED") Then
                    ShowMessage("Not able to add fulfillment bizdoc because required number of Lots are not provided.<br />")
                ElseIf ex.Message.Contains("INVALID_LOT_NUMBERS") Then
                    ShowMessage("Not able to add fulfillment bizdoc because invalid Lots are provided.<br />")
                ElseIf ex.Message.Contains("SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY") Then
                    ShowMessage("Not able to add fulfillment bizdoc because Lot number do not have enough qty to fulfill.<br />")
                ElseIf ex.Message.Contains("NOTSUFFICIENTQTY_ALLOCATION") Then
                    ShowMessage("Not able to add fulfillment bizdoc because warehouse allocation is invlid.<br />")
                ElseIf ex.Message.Contains("NOTSUFFICIENTQTY_ONHAND") Then
                    ShowMessage("Not able to add fulfillment bizdoc because warehouse does not have enought on hand quantity.<br />")
                ElseIf ex.Message = "NOT_ALLOWED" Then
                    litMessage.Text = "To split order items multiple bizdocs you must create all bizdocs with ""partial fulfilment"" checked!"
                ElseIf ex.Message = "NOT_ALLOWED_FulfillmentOrder" Then
                    litMessage.Text = "Only a Sales Order can create a Fulfillment Order"
                ElseIf ex.Message = "FY_CLOSED" Then
                    litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
                ElseIf ex.Message = "AlreadyInvoice" Then
                    litMessage.Text = "You can�t create a Invoice against a Fulfillment Order that already contains a Invoice"
                ElseIf ex.Message = "AlreadyPackingSlip" Then
                    litMessage.Text = "You can�t create a Packing-Slip against a Fulfillment Order that already contains a Packing-Slip"
                ElseIf ex.Message = "AlreadyInvoice_NOT_ALLOWED_FulfillmentOrder" Then
                    litMessage.Text = "You can�t create a fulfillment order against a sales order that already contains an Invoice or Packing-Slip"
                ElseIf ex.Message = "AlreadyFulfillmentOrder" Then
                    litMessage.Text = "Manually adding Invoices or Packing Slips to a Sales Order is not allowed after a Fulfillment Order (FO) is added. Your options are to edit the FO status to trigger creation of Invoices and/or Packing Slips, Delete the Fulfillment Order, or Create Invoices and/or Packing Slips from the Sales Fulfillment section"
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(CCommon.ToString(ex))
                End If

                ViewState("FulfillmentIterationData") = Nothing
                ViewState("FulfillmentIteration") = Nothing
                ViewState("FulfillmentFirstIteration") = Nothing
                hdnFulfillentInterationValue.Value = ""
                ViewState("InvoiceIteration") = Nothing
                ViewState("InvoiceIterationData") = Nothing
                ViewState("InvoiceFirstIteration") = Nothing
                hdnInvoiceInterationValue.Value = ""
            End Try
        End Sub
        Private Sub btnOpenInvoice_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnOpenInvoice.Click
            Try

                Dim strScript As String = "<script>window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID='" + Convert.ToString(lngOppId) + "'&OppBizId='" + Convert.ToString(lngBizDocId) + "'&Print=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=850,height=700,scrollbars=yes,resizable=yes');</script>"
                'ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, Guid.NewGuid().ToString(), "<script>OpenBizInvoice('" + Convert.ToString(lngOppId) + "','" + Convert.ToString(lngBizDocId) + "',0);</script>", False)
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, Guid.NewGuid().ToString(), "<script>window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=" + Convert.ToString(lngOppId) + "&OppBizId=" + Convert.ToString(lngBizDocId) + "&Print=0', '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=850,height=700,scrollbars=yes,resizable=yes');</script>", False)
                If (hdnShippingBizDoc.Value = "1" AndAlso hdnInventoryItems.Value = "1" AndAlso CCommon.ToLong(Session("numDefaultSalesShippingDoc")) > 0) Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, Guid.NewGuid().ToString(), "<script>window.open('../opportunity/frmShippingBox.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=" + Convert.ToString(lngOppId) + "&OppBizDocId=" + Convert.ToString(lngBizDocId) + "&ShipCompID=0', '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=850,height=700,scrollbars=yes,resizable=yes');</script>", False)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        'Sub BindGridColumns()
        '    Try
        '        Dim ds As DataSet
        '        objContacts = New CContacts
        '        objContacts.DomainID = Session("DomainId")
        '        objContacts.FormId = 26
        '        objContacts.UserCntID = Session("UserContactId")
        '        objContacts.ContactType = 0
        '        ds = objContacts.GetColumnConfiguration

        '        Dim radColumn As Telerik.Web.UI.GridColumn
        '        RadGrid1.Columns.FindByUniqueNameSafe("Image").Visible = False
        '        Dim index As Integer = 3
        '        'hide all columns
        '        For Each dr As DataRow In ds.Tables(0).Rows
        '            radColumn = RadGrid1.Columns.FindByUniqueNameSafe(dr("vcFieldName").ToString())
        '            If Not radColumn Is Nothing Then
        '                radColumn.Display = False
        '                If radColumn.UniqueName = "Image" Then
        '                    radColumn.Visible = True
        '                    index = index + 1
        '                End If
        '            End If

        '        Next

        '        'Show columns which are set to be shown in settings
        '        For Each dr As DataRow In ds.Tables(1).Rows
        '            radColumn = RadGrid1.Columns.FindByUniqueNameSafe(dr("vcFieldName").ToString())
        '            If dr("Custom") = "1" AndAlso radColumn Is Nothing Then 'is Custom Field -then add dynamic column to radgrid
        '                RadGrid1.Columns.Remove(radColumn)
        '                Dim Column As New Telerik.Web.UI.GridBoundColumn
        '                Column.HeaderText = dr("vcFieldName").ToString()
        '                Column.UniqueName = dr("vcFieldName").ToString()
        '                Column.DataField = dr("vcFieldName").ToString()
        '                Column.HeaderButtonType = Telerik.Web.UI.GridHeaderButtonType.TextButton
        '                Column.OrderIndex = CCommon.ToInteger(dr("tintOrder")) + index
        '                RadGrid1.Columns.Add(Column)
        '            ElseIf Not radColumn Is Nothing Then
        '                radColumn.HeaderText = dr("vcFieldName").ToString()
        '                radColumn.Display = True
        '                radColumn.OrderIndex = CCommon.ToInteger(dr("tintOrder")) + index
        '            End If
        '        Next
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub
        Sub BindItems(ByVal dsTemp As DataSet)
            Try
                LoadPickPackShipGrid()
                tblProducts.Visible = True
                'To persist value of checked item in radgrid 
                alSelectedItems = New ArrayList()

                Dim dtTableInfo As DataTable
                dtTableInfo = dsTemp.Tables(2)
                If OppType.Value <> 2 Then
                    Dim drReceived() As DataRow = dtTableInfo.Select("vcDbColumnName = 'numUnitHourReceived' AND vcFieldName = 'Received'")
                    If drReceived.Length > 0 Then dtTableInfo.Rows.Remove(drReceived(0))
                End If

                If OppType.Value = 2 Then
                    Dim drReceived() As DataRow = dtTableInfo.Select("vcDbColumnName = 'DiscAmt' AND vcOrigDbColumnName = 'DiscAmt'")
                    If drReceived.Length > 0 Then dtTableInfo.Rows.Remove(drReceived(0))

                    drReceived = dtTableInfo.Select("vcDbColumnName = 'numCost' Or vcOrigDbColumnName = 'numCost'")
                    If drReceived.Length > 0 Then dtTableInfo.Rows.Remove(drReceived(0))

                    drReceived = dtTableInfo.Select("vcDbColumnName = 'numVendorID'")
                    If drReceived.Length > 0 Then dtTableInfo.Rows.Remove(drReceived(0))

                    drReceived = dtTableInfo.Select("vcDbColumnName = 'vcMargin'")
                    If drReceived.Length > 0 Then dtTableInfo.Rows.Remove(drReceived(0))

                End If

                Dim bField As BoundField
                Dim Tfield As TemplateField
                gvProduct.Columns.Clear()

                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")
                objCommon.UOMAll = True
                dtUnit = objCommon.GetItemUOM()

                If ((OppType.Value = "1" And OppStatus.Value = "1") Or hdnStockTransfer.Value = "1") AndAlso dtTableInfo.Select("vcOrigDbColumnName='SerialLotNo'").Length > 0 Then
                    If btnReOpenDeal.Visible = False AndAlso btnReceivedOrShipped.Visible = True Then
                        dtTableInfo.Select("vcOrigDbColumnName='SerialLotNo'")(0)("vcAssociatedControlType") = "HyperLink"
                    Else
                        dtTableInfo.Select("vcOrigDbColumnName='SerialLotNo'")(0)("vcAssociatedControlType") = "Label"
                    End If
                Else
                    If dtTableInfo.Select("vcOrigDbColumnName='SerialLotNo'").Length > 0 Then
                        dtTableInfo.Rows.Remove(dtTableInfo.Select("vcOrigDbColumnName='SerialLotNo'")(0))
                    End If
                End If

                For Each drRow As DataRow In dtTableInfo.Rows
                    If drRow("intVisible") <> 0 Then
                        If OppType.Value = "1" And OppStatus.Value = "1" And drRow("vcDbColumnName") = "vcGroupNonDbField" Then
                            Continue For
                        Else
                            Tfield = New TemplateField
                            Tfield.HeaderTemplate = New GridProductTemplate(ListItemType.Header, drRow, OppType.Value, OppStatus.Value, IIf(hdnStockTransfer.Value = "1", True, False), Not btnReceivedOrShipped.Visible)
                            Tfield.ItemTemplate = New GridProductTemplate(ListItemType.Item, drRow, OppType.Value, OppStatus.Value, IIf(hdnStockTransfer.Value = "1", True, False), Not btnReceivedOrShipped.Visible, dtUnit)
                            If drRow("vcDbColumnName") = "vcInclusionDetails" Or drRow("vcDbColumnName") = "vcPromotionDetail" Then
                                Tfield.ItemStyle.Wrap = True
                                Tfield.HeaderStyle.Width = Unit.Percentage(25)
                            Else
                                Tfield.ItemStyle.Wrap = False
                            End If
                            Tfield.HeaderStyle.Wrap = False
                            gvProduct.Columns.Add(Tfield)
                        End If

                    End If
                Next

                Dim dr As DataRow



                dr = dtTableInfo.NewRow()
                dr("vcAssociatedControlType") = "chkSelectAll"
                dr("intColumnWidth") = "30"

                Tfield = New TemplateField
                Tfield.HeaderTemplate = New GridProductTemplate(ListItemType.Header, dr, OppType.Value, OppStatus.Value, IIf(hdnStockTransfer.Value = "1", True, False), Not btnReceivedOrShipped.Visible)
                Tfield.ItemTemplate = New GridProductTemplate(ListItemType.Item, dr, OppType.Value, OppStatus.Value, IIf(hdnStockTransfer.Value = "1", True, False), Not btnReceivedOrShipped.Visible)
                gvProduct.Columns.Add(Tfield)

                If Not dsTemp Is Nothing AndAlso dsTemp.Tables.Count > 0 AndAlso dsTemp.Tables(0).Rows.Count > 0 Then
                    Dim results = From myRow In dsTemp.Tables(0).AsEnumerable()
                                  Where myRow.Field(Of Boolean?)("bitItemPriceApprovalRequired") = True
                                  Select myRow

                    If results.Count > 0 AndAlso Not unitPriceApprover Is Nothing AndAlso unitPriceApprover.Contains(userID) AndAlso isMinUnitPriceRuleSet Then
                        btnApproveUnitPrice.Visible = True
                    Else
                        btnApproveUnitPrice.Visible = False
                    End If
                End If

                gvProduct.DataSource = dsTemp.Tables(0)
                gvProduct.DataBind()

                Dim dvInventory() As DataRow = dsTemp.Tables(0).Select("charItemType = 'P'")
                If (dvInventory.Length > 0) Then
                    hdnInventoryItems.Value = "1"
                End If

                Dim drPickPackShip() As DataRow = dtTableInfo.Select("vcOrigDbColumnName = 'PickPackShip'")
                If drPickPackShip.Length > 0 Then
                    Dim objBizDocs As New OppBizDocs
                    objBizDocs.DomainID = Session("DomainID")
                    objBizDocs.UserCntID = Session("UserContactID")
                    objBizDocs.OppId = CCommon.ToLong(GetQueryStringVal("OpID"))
                    objBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                    Dim dsBizDocs As DataSet = objBizDocs.GetBizDocsByOOpId
                    If dsBizDocs.Tables.Count > 0 Then
                        dtAllBizDocs = dsBizDocs.Tables(1)

                        Dim dvFullFilmentOrder As New DataView(dtAllBizDocs)
                        dvFullFilmentOrder.RowFilter = "numBizDocId = 296"

                        Dim dvInvoice As New DataView(dtAllBizDocs)
                        dvInvoice.RowFilter = "numBizDocId = 287"

                        Dim dvPackagingSlip As New DataView(dtAllBizDocs)
                        dvPackagingSlip.RowFilter = "numBizDocId = 29397"

                        Dim DisablePackagingSlip As Integer = 0
                        Dim DisableInvoiceSlip As Integer = 0
                        Dim DisableFullFilmentOrderSlip As Integer = 0



                        For Each drRows As GridViewRow In gvProduct.Rows
                            Dim txtFullFIlmentQty As TextBox = drRows.FindControl("txtFullFIlmentQty")
                            Dim txtInvoicedQty As TextBox = drRows.FindControl("txtInvoicedQty")

                            If Not txtFullFIlmentQty Is Nothing AndAlso (CCommon.ToDecimal(txtFullFIlmentQty.Text) = 0) Then
                                txtFullFIlmentQty.Enabled = False
                            Else
                                DisableFullFilmentOrderSlip = 0
                            End If
                            If Not txtInvoicedQty Is Nothing AndAlso (CCommon.ToDecimal(txtInvoicedQty.Text) = 0) Then
                                txtInvoicedQty.Enabled = False
                            Else
                                DisableInvoiceSlip = 0
                            End If

                            'If (dvInvoice.Count > 0 AndAlso dvFullFilmentOrder.Count = 0 AndAlso dvPackagingSlip.Count = 0) Then
                            '    If Not txtFullFIlmentQty Is Nothing Then
                            '        txtFullFIlmentQty.Enabled = False
                            '    End If
                            'End If
                        Next
                        If (dvInvoice.Count > 0 AndAlso dvFullFilmentOrder.Count = 0 AndAlso dvPackagingSlip.Count = 0) Then
                            DisablePackagingSlip = 1
                            DisableFullFilmentOrderSlip = 1
                        ElseIf (dvFullFilmentOrder.Count > 0 AndAlso dvPackagingSlip.Count = 0) Then
                            DisablePackagingSlip = 1
                        End If


                        For Each headerCell As TableCell In gvProduct.HeaderRow.Cells
                            Dim ddlPackagingSlipFromProdGrid As DropDownList = TryCast(headerCell.FindControl("ddlPackagingSlipFromProdGrid"), DropDownList)
                            Dim ddlFulFillmentOrderFromProdGrid As DropDownList = TryCast(headerCell.FindControl("ddlFulFillmentOrderFromProdGrid"), DropDownList)
                            Dim btnPickPack As Button = TryCast(headerCell.FindControl("btnPickPack"), Button)
                            Dim btnFulFillmentOrder As Button = TryCast(headerCell.FindControl("btnFulFillmentOrder"), Button)
                            'If (DisablePackagingSlip = 1) Then
                            '    If Not ddlPackagingSlipFromProdGrid Is Nothing Then
                            '        ddlPackagingSlipFromProdGrid.Enabled = False
                            '    End If

                            '    If Not btnPickPack Is Nothing Then
                            '        btnPickPack.CssClass = "btn btn-default"
                            '        btnPickPack.Enabled = False
                            '    End If
                            'End If
                            'If (DisableFullFilmentOrderSlip = 1) Then
                            '    If Not ddlFulFillmentOrderFromProdGrid Is Nothing Then
                            '        ddlFulFillmentOrderFromProdGrid.Enabled = False
                            '    End If
                            '    If Not btnFulFillmentOrder Is Nothing Then
                            '        btnFulFillmentOrder.CssClass = "btn btn-default"
                            '    End If
                            'End If
                        Next
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub CreateHTMLBizDocs(CommandType As String)
            Try
                Dim sw As New System.IO.StringWriter()
                Dim RefType As Short = 0

                If OppType.Value = 1 And OppStatus.Value = 0 Then
                    RefType = 3

                ElseIf OppType.Value = 1 And OppStatus.Value = 1 Then
                    RefType = 1

                ElseIf OppType.Value = 2 And OppStatus.Value = 0 Then
                    RefType = 4

                ElseIf OppType.Value = 2 And OppStatus.Value = 1 Then
                    RefType = 2
                End If

                Server.Execute("frmMirrorBizDoc.aspx" & QueryEncryption.EncryptQueryString("RefID=" & lngOppId & "&RefType=" & RefType & "&DocGenerate=1"), sw)
                Dim htmlCodeToConvert As String = sw.GetStringBuilder().ToString()
                sw.Close()

                If CommandType = "Email" Then
                    Session("Attachements") = Nothing 'reset to nothing to avoid duplicate attachments
                    Dim i As Integer
                    Dim strFileName As String = ""
                    Dim strFilePhysicalLocation As String = ""

                    Dim objHTMLToPDF As New HTMLToPDF
                    strFileName = objHTMLToPDF.ConvertHTML2PDF(htmlCodeToConvert, CCommon.ToLong(Session("DomainID")))

                    strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
                    objCommon.AddAttchmentToSession(IIf(hfPoppName.Value.Trim.Length > 0, hfPoppName.Value.Trim.Replace(" ", "_") + ".pdf", strFileName), CCommon.GetDocumentPath(Session("DomainID")) & strFileName, strFilePhysicalLocation)

                    Dim str As String
                    str = String.Format("<script language='javascript'>window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&PickAtch=1&isAttachmentRequired=1&Lsemail={0}&pqwRT={1}&OppID={2}&OppType={3}','ComposeWindow','toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')</script>",
                                       HttpUtility.JavaScriptStringEncode(hdnContactEmail.Value), hdnContactID.Value, lngOppId.ToString(), OppType)

                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "Email", str, False)
                ElseIf CommandType = "PDF" Then
                    Dim objBizDocs As New OppBizDocs
                    Dim strFileName As String = ""
                    Dim strFilePhysicalLocation As String = ""

                    Dim objHTMLToPDF As New HTMLToPDF
                    strFileName = objHTMLToPDF.ConvertHTML2PDF(htmlCodeToConvert, CCommon.ToLong(Session("DomainID")))
                    strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName

                    Response.Clear()
                    Response.ClearContent()
                    Response.ContentType = "application/pdf"
                    Response.AddHeader("content-disposition", "attachment; filename=" + IIf(hfPoppName.Value.Trim.Length > 0, hfPoppName.Value.Trim.Replace(" ", "_") + ".pdf", strFileName))
                    'Response.Write(Session("Attachements"))

                    Response.WriteFile(strFilePhysicalLocation)

                    Response.End()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpdate.Click
            Try
                SaveOrderSettings()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub SetShippingChargesLabel()
            Try
                Dim dsItems As DataSet = Nothing
                dsItems = If(dsTemp Is Nothing, ViewState("SOPOItems"), dsTemp)
                If dsItems IsNot Nothing Then
                    Dim drItem() As DataRow
                    drItem = dsItems.Tables(0).Select("numItemCode=" & lngShippingItemCode)
                    If drItem IsNot Nothing AndAlso drItem.Length > 0 Then
                        Dim strServiceName As String = ""
                        Dim dblServiceCharges As Double = 0
                        dblServiceCharges = CCommon.ToDouble(drItem(0)("monTotAmount"))
                        strServiceName = If(CCommon.ToString(drItem(0)("vcItemDesc")).Contains("-"), CCommon.ToString(drItem(0)("vcItemDesc")).Split("-")(0), CCommon.ToString(drItem(0)("vcItemDesc")))

                        'lblShippingReport.Text = "Shipping Charges " & strServiceName & " - " & _
                        '                          [String].Format("{0:#,##0.00}", dblServiceCharges) & " " & _
                        '                          CCommon.ToString(HttpContext.Current.Session("CurrSymbol")) & " is added."
                        lblCurrentShippingCharge.Text = CCommon.ToString(Session("Currency")).Trim() & " " & [String].Format("{0:#,##0.00}", dblServiceCharges)
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Function GetShippingCharge() As String
            Try

                'If ddlShippingMethod.Items.Count > 0 Then
                '    Dim strShippingCharge As String() = ddlShippingMethod.SelectedValue.Split("~"c)
                '    If strShippingCharge.Length > 1 Then
                '        Return String.Format("{0:#,##0.00}", CCommon.ToDecimal(strShippingCharge(1)))
                '    Else
                '        Return "0.00"
                '    End If
                'End If

                Return "0.00"
            Catch ex As Exception
                Throw ex
            End Try

        End Function

        Private Sub GetShippingMethod(ByVal ShowShippingAmount As Boolean,
                                      ByVal TotalWeight As Decimal,
                                      ByVal TotalAmount As Decimal,
                                      ByVal IsAllCartItemsFreeShipping As Boolean)
            Dim strErrLog As New StringBuilder
            Try

                'To get itemcount for Flat Rate Per Item method ...
                Dim intCartItemCount As Integer = 0
                Dim dsItems As New DataSet()
                dsItems = If(dsTemp Is Nothing, ViewState("SOPOItems"), dsTemp)
                Dim dtShippingMethod As New DataTable()

                Dim strMessage As String = ""
                If Not IsAllCartItemsFreeShipping Then
                    '#Region "Get Shipping Rule"
                    Dim objRule As New ShippingRule()
                    objRule.DomainID = CCommon.ToLong(Session("DomainID"))
                    objRule.SiteID = -1
                    objRule.DivisionID = CCommon.ToDouble(DivID.Value)
                    If CCommon.ToLong(hdnCountry.Value) <> 0 AndAlso
                       CCommon.ToLong(hdnState.Value) <> 0 Then
                        objRule.CountryID = CCommon.ToLong(hdnCountry.Value)
                        objRule.StateID = CCommon.ToLong(hdnState.Value)

                    End If

                    dtShippingMethod = objRule.GetShippingMethodForItem1()
                    '#End Region

                    '#Region "Get Shipping Method only when one Of Items in Cart having Free Shipping = false"
                    If dtShippingMethod.Rows.Count > 0 Then
                        'Default Column Add
                        Dim newColumn As New System.Data.DataColumn("IsShippingRuleValid", GetType(System.Boolean))
                        newColumn.DefaultValue = True
                        dtShippingMethod.Columns.Add(newColumn)

                        'Add this column to store it as a value field in the dropdown of ddlShippingMethod
                        CCommon.AddColumnsToDataTable(dtShippingMethod, "vcServiceTypeID1")

                        ' dtShippingMethod = dtShippingMethod.Select("IsValid=TRUE").CopyToDataTable();
                        '#Region "foreach start"

                        Dim FromCountry As Double = 0
                        Dim FromState As Double = 0
                        Dim strPostCode As String = ""
                        For Each dr As DataRow In dtShippingMethod.Rows
                            Dim decShipingAmount As [Decimal] = 0

                            If Not IsAllCartItemsFreeShipping AndAlso TotalWeight > 0 Then
                                '#Region "If All Items are not free shipping"

                                '#Region "Get Warehouse"
                                Dim objItem As New CItems()
                                Dim dtTable As DataTable = Nothing
                                ''objItem.WarehouseID = CCommon.ToLong(HttpContext.Current.Session("DefaultWareHouseID"))
                                ''objItem.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                                ''dtTable = objItem.GetWareHouses()
                                '#End Region
                                Dim objContacts As New CContacts()
                                objContacts.DivisionID = CCommon.ToDouble(Session("UserDivisionID"))
                                objContacts.ContactID = CCommon.ToDouble(Session("UserContactID"))
                                objContacts.DomainID = CCommon.ToDouble(Session("DomainID"))
                                dtTable = objContacts.GetBillOrgorContAdd()

                                '#Region "Warehouse checking"

                                If dtTable.Rows.Count > 0 Then
                                    'If CCommon.ToLong(dtTable.Rows(0)("numWCountry")) > 0 AndAlso CCommon.ToLong(dtTable.Rows(0)("numWState")) > 0 Then
                                    If CCommon.ToLong(dtTable.Rows(0)("vcShipCountry")) > 0 AndAlso CCommon.ToLong(dtTable.Rows(0)("vcShipState")) > 0 Then
                                        '#Region "Get Shipping Rates when Warehouse is Configured"

                                        '#Region "Get Bizdoc"
                                        Dim objOppBizDocs As New OppBizDocs()
                                        objOppBizDocs.ShipCompany = CCommon.ToInteger(dr("numShippingCompanyID"))
                                        objOppBizDocs.ShipFromCountry = CCommon.ToLong(dtTable.Rows(0)("vcShipCountry")) 'CCommon.ToLong(dtTable.Rows(0)("numWCountry"))
                                        objOppBizDocs.ShipFromState = CCommon.ToLong(dtTable.Rows(0)("vcShipState")) 'CCommon.ToLong(dtTable.Rows(0)("numWState"))
                                        objOppBizDocs.ShipToCountry = CCommon.ToString(hdnCountry.Value)
                                        objOppBizDocs.ShipToState = CCommon.ToString(hdnState.Value)
                                        objOppBizDocs.strShipFromCountry = "" ' CCommon.ToLong(dtTable.Rows(0)("numWCountry"))
                                        objOppBizDocs.strShipFromState = "" 'CCommon.ToLong(dtTable.Rows(0)("numWState"))
                                        objOppBizDocs.strShipToCountry = "" 'CCommon.ToString(hdnCountry.Value)
                                        objOppBizDocs.strShipToState = "" 'CCommon.ToString(hdnState.Value)
                                        objOppBizDocs.GetShippingAbbreviation()
                                        '#End Region

                                        Dim blnValid As Boolean = False
                                        If CCommon.ToInteger(dr("tintFixedShippingQuotesMode")) = 1 Then
                                            '//Ship by order weight
                                            If TotalWeight >= CCommon.ToDecimal(dr("intFrom")) AndAlso TotalWeight <= CCommon.ToDecimal(dr("intTo")) Then
                                                blnValid = True
                                            End If

                                        ElseIf CCommon.ToInteger(dr("tintFixedShippingQuotesMode")) = 2 Then
                                            '//Ship by Order Total
                                            If TotalAmount >= CCommon.ToDecimal(dr("intFrom")) AndAlso TotalAmount <= CCommon.ToDecimal(dr("intTo")) Then
                                                blnValid = True
                                            End If

                                        End If

                                        If blnValid = True Then

                                            '#Region "Get Shipping Rates"
                                            Dim objShipping As New Shipping()
                                            objShipping.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                                            objShipping.WeightInLbs = CCommon.ToDouble(TotalWeight)
                                            objShipping.NoOfPackages = 1
                                            objShipping.UseDimentions = If(CCommon.ToInteger(dr("numShippingCompanyID")) = 91, True, False)
                                            'for fedex dimentions are must
                                            objShipping.GroupCount = 1
                                            If objShipping.UseDimentions Then
                                                objShipping.Height = objRule.Height
                                                objShipping.Width = objRule.Width
                                                objShipping.Length = objRule.Length
                                            End If

                                            objShipping.SenderState = objOppBizDocs.strShipFromState
                                            objShipping.SenderZipCode = CCommon.ToString(dtTable.Rows(0)("vcShipPostCode")) 'CCommon.ToString(dtTable.Rows(0)("vcWPinCode"))
                                            objShipping.SenderCountryCode = objOppBizDocs.strShipFromCountry

                                            objShipping.RecepientState = objOppBizDocs.strShipToState
                                            objShipping.RecepientZipCode = CCommon.ToString(hdnPostal.Value)
                                            objShipping.RecepientCountryCode = objOppBizDocs.strShipToCountry

                                            objShipping.ServiceType = Short.Parse(CCommon.ToString(dr("intNsoftEnum")))
                                            objShipping.PackagingType = 21
                                            'ptYourPackaging 
                                            objShipping.ItemCode = objRule.ItemID
                                            objShipping.OppBizDocItemID = 0
                                            objShipping.ID = 9999
                                            objShipping.Provider = CCommon.ToInteger(dr("numShippingCompanyID"))

                                            Dim dtShipRates As DataTable = objShipping.GetRates()
                                            If Not String.IsNullOrEmpty(objShipping.ErrorMsg) Then
                                                'log for error Message strMessage = objShipping.ErrorMsg;
                                                Try
                                                    '''Throw New CustomException(objShipping.ErrorMsg)
                                                    'strErrLog.Append("<p>" & objShipping.ErrorMsg & "</p>")
                                                Catch ex As Exception
                                                    '''strMessage = GetErrorMessage("ERR068") & "<font color = ""WhiteSmoke"">" & ex.ToString().Replace("red", "WhiteSmoke") & "</font>"
                                                    ExceptionModule.ExceptionPublish(ex, CCommon.ToLong(Session("DomainID")), CCommon.ToLong(Session("UserContactID")), CCommon.ToLong(Session("SiteId")), Request)
                                                End Try
                                            End If
                                            '#End Region

                                            '#Region "Calculation Of Shipping Rates"
                                            If dtShipRates.Rows.Count > 0 Then
                                                If CCommon.ToDecimal(dtShipRates.Rows(0)("Rate")) > 0 Then
                                                    'Currency conversion of shipping rate
                                                    decShipingAmount = CCommon.ToDecimal(dtShipRates.Rows(0)("Rate")) / If(CCommon.ToDecimal(CCommon.ToString(Session("ExchangeRate"))) = 0, 1, CCommon.ToDecimal(CCommon.ToString(Session("ExchangeRate"))))

                                                    If CCommon.ToBool(dr("bitMarkupType")) = True AndAlso CCommon.ToDecimal(dr("fltMarkup")) > 0 Then
                                                        dr("fltMarkup") = decShipingAmount / CCommon.ToDecimal(dr("fltMarkup"))
                                                    End If
                                                    'Percentage
                                                    decShipingAmount = decShipingAmount + CCommon.ToDecimal(dr("fltMarkup"))

                                                    'dr["intNsoftEnum"].ToString()
                                                    If ShowShippingAmount Then
                                                        dr("vcServiceName") = CCommon.ToString(dr("vcServiceName")) & " - " & CCommon.ToString(Session("CurrSymbol")) & " " & String.Format("{0:#,##0.00}", decShipingAmount)
                                                    End If
                                                    dr("vcServiceTypeID1") = dr("numServiceTypeID").ToString() & "~" & CCommon.ToString(decShipingAmount) & "~" & CCommon.ToString(dr("numRuleID")) & "~" & CCommon.ToString(dr("numShippingCompanyID")) & "~" & CCommon.ToString(dr("vcServiceName"))

                                                End If
                                            Else
                                                dr("IsShippingRuleValid") = False
                                            End If
                                        Else
                                            dr("IsShippingRuleValid") = False
                                        End If
                                    Else
                                        Page.MaintainScrollPositionOnPostBack = False
                                    End If
                                Else
                                    Page.MaintainScrollPositionOnPostBack = False
                                End If
                            Else
                                'Set for not including it as rule if weight is 0 in the case of real Time Shipping Quotes .
                                dr("IsShippingRuleValid") = False
                                '#End Region
                            End If
                        Next
                        '#End Region
                        Dim drShippingMethod As DataRow() = dtShippingMethod.[Select]("IsShippingRuleValid=true")
                        If drShippingMethod.Length > 0 Then
                            dtShippingMethod = dtShippingMethod.[Select]("IsShippingRuleValid=true").CopyToDataTable()
                        Else
                            dtShippingMethod = New DataTable()
                        End If

                        '#End Region
                    End If
                End If

                '#Region "Bind Shipping Cost"
                ''If dtShippingMethod.Rows.Count = 0 Then
                ''    ddlShippingMethod.Items.Clear()
                ''    ' It is necessary otherwise it will show dublicate records .
                ''    Dim list As New ListItem()
                ''    list.Selected = True
                ''    list.Text = "NA - $0"
                ''    list.Value = "0~0.00"
                ''    ddlShippingMethod.Items.Add(list)
                ''    ddlShippingMethod.ClearSelection()
                ''Else
                ''    ddlShippingMethod.Items.Clear()

                ''    ddlShippingMethod.Items.Clear()
                ''    dtShippingMethod.Columns.Add("monRate1", GetType(System.Decimal))
                ''    For Each drService As DataRow In dtShippingMethod.Rows
                ''        If CCommon.ToString(drService("vcServiceName")).Contains("-") AndAlso CCommon.ToString(drService("vcServiceName")).Split("-").Length > 0 Then
                ''            drService("monRate1") = CCommon.ToDecimal(CCommon.ToString(drService("vcServiceName")).Split("-")(1).Trim())
                ''        Else
                ''            drService("monRate1") = 0
                ''        End If
                ''        dtShippingMethod.AcceptChanges()
                ''    Next

                ''    Dim dvShippingMethod As DataView
                ''    dvShippingMethod = dtShippingMethod.DefaultView
                ''    dvShippingMethod.Sort = "monRate1 ASC"
                ''    ddlShippingMethod.DataTextField = "vcServiceName"
                ''    ddlShippingMethod.DataValueField = "vcServiceTypeID1"
                ''    ddlShippingMethod.DataSource = dvShippingMethod
                ''    ddlShippingMethod.DataBind()
                ''End If

                'ddlShippingMethod.Items.Insert(0, New ListItem("--Select One--", "0~0.00~0"))
                '#End Region

                '#Region "Select Default Shipping Rule"
                ''If ddlShippingMethod.Items.Count = 2 Then
                ''    ddlShippingMethod.SelectedIndex = 0
                ''Else
                ''    If CCommon.ToString(Session("ShippingMethodValue")) <> "" Then
                ''        ddlShippingMethod.ClearSelection()
                ''        If ddlShippingMethod.Items.FindByValue(CCommon.ToString(Session("ShippingMethodValue"))) IsNot Nothing Then
                ''            ddlShippingMethod.Items.FindByValue(CCommon.ToString(Session("ShippingMethodValue"))).Selected = True
                ''        Else
                ''            ddlShippingMethod.SelectedIndex = 0
                ''        End If
                ''    Else
                ''        ddlShippingMethod.SelectedIndex = 0
                ''    End If
                ''End If
                '#End Region
                If strMessage.Length > 0 Then
                    Page.MaintainScrollPositionOnPostBack = False
                    'ShowMessage(strMessage, 1)
                End If
            Catch ex As Exception
                strErrLog.Append("<p>" & ex.Message.ToString() & "</p>")
                ShowMessage(strErrLog.ToString())
                Throw ex
            End Try

            If strErrLog.ToString.Length > 0 Then
                ShowMessage(strErrLog.ToString())
            End If
        End Sub

        Private Sub AddShippingItem(Optional ByVal IsManual As Boolean = False)
            Try
                If dsTemp Is Nothing Then
                    dsTemp = ViewState("SOPOItems")
                End If

                objCommon = New CCommon
                Dim dtUnit As DataTable
                objCommon.DomainID = Session("DomainID")
                objCommon.ItemCode = CCommon.ToDouble(Session("ShippingServiceItem"))
                objCommon.UOMAll = True
                dtUnit = objCommon.GetItemUOM()

                Dim dtTable, dtItemTax As DataTable
                If objItems Is Nothing Then objItems = New CItems
                Dim strUnit As String
                Dim objOpportunity As New MOpportunity
                objOpportunity.OppItemCode = CCommon.ToLong(txtHidEditOppItem.Text)
                objOpportunity.OpportunityId = CCommon.ToLong(GetQueryStringVal("opid"))
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.Mode = 2
                objItems.ItemCode = CCommon.ToDouble(Session("ShippingServiceItem"))
                dtTable = objItems.ItemDetails

                If (OppType.Value = 2) Then
                    strUnit = "numPurchaseUnit"
                ElseIf (OppType.Value = 1) Then
                    strUnit = "numSaleUnit"
                End If

                Dim strApplicable As String
                If (OppType.Value = 1 And OppStatus.Value = 1) Then
                    objItems.DomainID = Session("DomainID")
                    dtItemTax = objItems.ItemTax()

                    For Each dr As DataRow In dtItemTax.Rows
                        strApplicable = strApplicable & dr("bitApplicable") & ","
                    Next
                End If

                If dtTable.Rows.Count > 0 Then

                    Dim PageType As Integer
                    If (OppType.Value = 1 And OppStatus.Value = 1) Then
                        PageType = 1
                        strApplicable = strApplicable & dtTable.Rows(0).Item("bitTaxable")
                        'objItems.Taxable = strApplicable
                    End If

                    Dim strDescForServiceItem As String = ""
                    If chkUseCustomerShippingAccount.Checked = True Then
                        strDescForServiceItem = "Customer Shipping Account # is " & hdnShipperAccountNo.Value
                    End If
                    ''If ddlShippingMethod.Items.Count > 0 Then
                    ''    Dim strShippingCharge As String() = ddlShippingMethod.SelectedValue.Split("~"c)
                    ''    If strShippingCharge.Length >= 4 Then
                    ''        strDescForServiceItem = strShippingCharge(4)
                    ''    End If
                    ''End If

                    objItems.ItemName = CCommon.ToString(dtTable.Rows(0).Item("vcItemName"))
                    objItems.type = CCommon.ToInteger(dtTable.Rows(0).Item("tintStandardProductIDType"))
                    objItems.ItemDesc = strDescForServiceItem
                    objItems.Description = strDescForServiceItem
                    objItems.ModelID = CCommon.ToString(dtTable.Rows(0).Item("vcModelID"))
                    objItems.PathForImage = CCommon.ToString(dtTable.Rows(0).Item("vcPathForImage"))
                    objItems.ItemType = dtTable.Rows(0).Item("ItemType")

                    objItems.AllowDropShip = CCommon.ToBool(dtTable.Rows(0).Item("bitAllowDropShip"))
                    objItems.VendorID = CCommon.ToLong(dtTable.Rows(0).Item("numVendorID"))
                    objItems.IsPrimaryVendor = True

                    objItems.Weight = CCommon.ToDouble(dtTable.Rows(0).Item("fltWeight"))
                    objItems.FreeShipping = CCommon.ToBool(dtTable.Rows(0).Item("bitFreeShipping"))
                    objItems.Height = CCommon.ToDouble(dtTable.Rows(0).Item("fltHeight"))
                    objItems.Width = CCommon.ToDouble(dtTable.Rows(0).Item("fltWidth"))
                    objItems.Length = CCommon.ToDouble(dtTable.Rows(0).Item("fltLength"))

                    'Dim dtUnit As DataTable
                    If strUnit.Length > 0 Then
                        objItems.UnitofMeasure = dtTable.Rows(0).Item("vcUnitofMeasure")

                        objItems.BaseUnit = dtTable.Rows(0).Item("numBaseUnit")
                        objItems.PurchaseUnit = dtTable.Rows(0).Item("numPurchaseUnit")
                        objItems.SaleUnit = dtTable.Rows(0).Item("numSaleUnit")

                        objCommon.DomainID = Session("DomainID")
                        objCommon.ItemCode = objItems.ItemCode
                        objCommon.UnitId = dtTable.Rows(0).Item("numSaleUnit")
                        dtUnit = objCommon.GetItemUOMConversion()
                    End If

                    objCommon = New CCommon
                    Dim objItem As New CItems
                    If Not objItem.ValidateItemAccount(CCommon.ToDouble(Session("ShippingServiceItem")), Session("DomainID"), OppType.Value) = True Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "AccountValidation", "alert('Please Set Income,Asset,COGs(Expense) Account for selected Item from Administration->Inventory->Item Details')", True)
                        Return
                    End If


                    'Dim dblDiscount As Decimal = 0
                    'dblDiscount = IIf(IsNumeric(txtDiscount.Text), txtDiscount.Text, 0)
                    'If Session("CouponCode") IsNot Nothing Then
                    '    ApplyCouponCode(Session("CouponCode").ToString())
                    'End If
                    'dblDiscount = dblDiscount + CCommon.ToDouble(Session("TotalDiscount"))

                    Dim dtTaxTypes As DataTable
                    Dim ObjTaxItems As New TaxDetails
                    ObjTaxItems.DomainID = Session("DomainID")
                    dtTaxTypes = ObjTaxItems.GetTaxItems

                    For Each drTax As DataRow In dtTaxTypes.Rows
                        dtTable.Columns.Add("Tax" & drTax("numTaxItemID"), GetType(Decimal))
                        dtTable.Columns.Add("bitTaxable" & drTax("numTaxItemID"))
                    Next

                    Dim dr As DataRow
                    dr = dtTaxTypes.NewRow
                    dr("numTaxItemID") = 0
                    dr("vcTaxName") = "Sales Tax(Default)"
                    dtTaxTypes.Rows.Add(dr)

                    Dim chkTaxItemsForService As New CheckBoxList
                    chkTaxItemsForService.DataTextField = "vcTaxName"
                    chkTaxItemsForService.DataValueField = "numTaxItemID"
                    chkTaxItemsForService.DataSource = dtTaxTypes
                    chkTaxItemsForService.DataBind()

                    If Not dsTemp.Tables(0).Columns.Contains("Op_Flag") Then dsTemp.Tables(0).Columns.Add("Op_Flag")
                    If Not dsTemp.Tables(0).Columns.Contains("bitIsAuthBizDoc") Then dsTemp.Tables(0).Columns.Add("bitIsAuthBizDoc")
                    If Not dsTemp.Tables(0).Columns.Contains("numUnitHourReceived") Then dsTemp.Tables(0).Columns.Add("numUnitHourReceived")
                    If Not dsTemp.Tables(0).Columns.Contains("numQtyShipped") Then dsTemp.Tables(0).Columns.Add("numQtyShipped")
                    If Not dsTemp.Tables(0).Columns.Contains("numUOM") Then dsTemp.Tables(0).Columns.Add("numUOM")
                    If Not dsTemp.Tables(0).Columns.Contains("vcUOMName") Then dsTemp.Tables(0).Columns.Add("vcUOMName")
                    If Not dsTemp.Tables(0).Columns.Contains("UOMConversionFactor") Then dsTemp.Tables(0).Columns.Add("UOMConversionFactor")
                    If Not dsTemp.Tables(0).Columns.Contains("monTotAmtBefDiscount") Then dsTemp.Tables(0).Columns.Add("monTotAmtBefDiscount")
                    If Not dsTemp.Tables(0).Columns.Contains("vcBaseUOMName") Then dsTemp.Tables(0).Columns.Add("vcBaseUOMName")
                    If Not dsTemp.Tables(0).Columns.Contains("bitDiscountType") Then dsTemp.Tables(0).Columns.Add("bitDiscountType")
                    If Not dsTemp.Tables(0).Columns.Contains("numVendorWareHouse") Then dsTemp.Tables(0).Columns.Add("numVendorWareHouse")
                    If Not dsTemp.Tables(0).Columns.Contains("numShipmentMethod") Then dsTemp.Tables(0).Columns.Add("numShipmentMethod")
                    If Not dsTemp.Tables(0).Columns.Contains("numSOVendorId") Then dsTemp.Tables(0).Columns.Add("numSOVendorId")
                    If Not dsTemp.Tables(0).Columns.Contains("numWarehouseID") Then dsTemp.Tables(0).Columns.Add("numWarehouseID")
                    If Not dsTemp.Tables(0).Columns.Contains("numWarehouseItmsID") Then dsTemp.Tables(0).Columns.Add("numWarehouseItmsID")
                    If Not dsTemp.Tables(0).Columns.Contains("vcSKU") Then dsTemp.Tables(0).Columns.Add("vcSKU")

                    If IsManual = False Then
                        dcShippingCharge = CCommon.ToDecimal(hdnCalculatedShippingRate.Value)
                    Else
                        dcShippingCharge = CCommon.ToDecimal(txtShippingRate.Text)
                    End If

                    Dim drDuplicateServiceItemRow As DataRow
                    Dim dsItems As DataSet = DirectCast(ViewState("SOPOItems"), DataSet)
                    If dsItems.Tables(0).Select("numItemCode = " & CCommon.ToDouble(Session("ShippingServiceItem"))) IsNot Nothing AndAlso
                       dsItems.Tables(0).Select("numItemCode = " & CCommon.ToDouble(Session("ShippingServiceItem"))).Length > 0 Then

                        drDuplicateServiceItemRow = dsItems.Tables(0).Select("numItemCode = " & CCommon.ToDouble(Session("ShippingServiceItem")))(0)
                        dsItems.Tables(0).Rows.Remove(drDuplicateServiceItemRow)
                    End If

                    ViewState("SOPOItems") = OpportunityMangagement.AddEditItemtoDataTable(objCommon, dsTemp, True, objItems.ItemType, objItems.ItemType, objItems.AllowDropShip,
                                                                                         objItems.KitParent, CCommon.ToDouble(Session("ShippingServiceItem")), 1, dcShippingCharge,
                                                                                         objItems.ItemDesc, 0, objItems.ItemName, "", 0, txtModelID.Text.Trim, 0, "", 1,
                                                                                         PageType.ToString(), False, 0, strApplicable, objItems.Tax, "", chkTaxItemsForService, False, "",
                                                                                         Nothing, 0, 0, 0, 0, 0, 0, True, 0, 0, 0, 0, 0, 0, 0)

                    Dim drRow As DataRow
                    If dsItems.Tables(0).Select("numItemCode = " & CCommon.ToDouble(Session("ShippingServiceItem"))) IsNot Nothing AndAlso
                       dsItems.Tables(0).Select("numItemCode = " & CCommon.ToDouble(Session("ShippingServiceItem"))).Length > 0 Then

                        drRow = dsItems.Tables(0).Select("numItemCode = " & CCommon.ToDouble(Session("ShippingServiceItem")))(0)

                        If drRow IsNot Nothing Then
                            drRow("tintOppType") = 1
                            drRow("numOppId") = lngOppId
                            'drRow("ReturnedQty") = 0
                            drRow("numOriginalUnitHour") = 0
                            drRow("Source") = ""
                            drRow("numSourceID") = 0
                            drRow("bitKitParent") = 0
                            drRow("numShipClass") = 0
                            'drRow("vcPathForImage") = ""
                            drRow("vcItemName") = objItems.ItemName
                            drRow("vcModelId") = objItems.ModelID
                            drRow("vcWarehouse") = ""
                            drRow("numOnHand") = 1
                            drRow("vcLocation") = ""
                            drRow("vcItemDesc") = objItems.ItemDesc
                            drRow("vcAttributes") = ""
                            drRow("bitDropShip") = False
                            drRow("vcSKU") = ""
                            drRow("vcManufacturer") = ""
                            drRow("numItemClassification") = objItems.ItemClassification
                            drRow("numItemGroup") = objItems.ItemGroupID
                            drRow("fltWeight") = 0
                            drRow("numBarCodeId") = 0
                            drRow("DiscAmt") = 0
                            drRow("bitItemAddedToAuthBizDoc") = True
                            drRow("numClassID") = objItems.ItemClass
                            drRow("vcProjectStageName") = ""
                            drRow("numQty") = 1
                            drRow("bitSerialized") = False
                            drRow("bitLotNo") = False
                            drRow("numUOMId") = 0
                            drRow("vcUOMName") = ""
                            dsItems.Tables(0).AcceptChanges()

                            dsItems.Tables(0).Columns.Remove("numWarehouseId")
                            ViewState("SOPOItems") = dsItems
                            tblMain.Controls.Clear()
                            LoadSavedInformation()
                            ControlSettings()

                        End If

                    End If

                End If


            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Dim objOppBizDocs As New OppBizDocs
        Dim objShipping As New Shipping

        ''Private Sub imgShippingReport_Click(sender As Object, e As ImageClickEventArgs) Handles imgShippingReport.Click
        ''    Try
        ''        GetBoxLog()
        ''    Catch ex As Exception
        ''        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        ''        DisplayError(ex.Message)
        ''    End Try
        ''End Sub

        Private Function GetOrderItemShippingRules(ByVal OppId As Long,
                                                  ByVal OppBizDocID As Long,
                                                  ByVal intDomainId As Integer,
                                                  Optional ByVal intMode As Integer = 0) As DataTable
            'Use intMode = 0 for getting bizdoc items
            'Use intMode = 1 for getting opportunity items without using bizdocid
            Dim dtResult As DataTable = Nothing
            Try
                Dim dsItems As New DataSet
                Dim dtOrderDetail As New DataTable
                objOppBizDocs = New OppBizDocs
                objOppBizDocs.OppId = OppId
                objOppBizDocs.OppBizDocId = OppBizDocID
                objOppBizDocs.DomainID = DomainID

                If intMode = 0 Then
                    dsItems = objOppBizDocs.GetOppInItems()
                ElseIf intMode = 1 Then
                    If objOpportunity Is Nothing Then objOpportunity = New OppotunitiesIP
                    objOpportunity.OpportunityId = OppId
                    objOpportunity.DomainID = Session("DomainID")
                    objOpportunity.UserCntID = Session("UserContactID")
                    objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    dsItems = objOpportunity.GetOrderItemsProductServiceGrid
                End If


                'Tracing Part
                '------------------------------------------------------------------------------------------------------
                'Trace.Write("Start Tracing Items detail.")
                'Trace.Write("Is Item Dataset is initialized or not ? : " & IIf(dsItems Is Nothing, "NO", "YES"))
                'Trace.Write("Is Item Dataset has tables or not? : " & IIf(dsItems.Tables.Count > 0, dsItems.Tables.Count, 0))
                'Trace.Write("End Tracing Items detail.")

                '------------------------------------------------------------------------------------------------------

                If dsItems IsNot Nothing AndAlso dsItems.Tables.Count > 0 AndAlso dsItems.Tables(0).Rows.Count > 0 Then
                    dtOrderDetail.Columns.Add("OppId", GetType(Long))
                    dtOrderDetail.Columns.Add("OppBizDocItemID", GetType(Long))
                    dtOrderDetail.Columns.Add("ItemCode", GetType(Long))
                    dtOrderDetail.Columns.Add("ShippingRuleID", GetType(Long))
                    dtOrderDetail.Columns.Add("numShippingCompanyID1", GetType(Long))
                    dtOrderDetail.Columns.Add("numShippingCompanyID2", GetType(Long))
                    dtOrderDetail.Columns.Add("DomesticServiceID", GetType(Integer))
                    dtOrderDetail.Columns.Add("InternationalServiceID", GetType(Integer))
                    dtOrderDetail.Columns.Add("PackageTypeID", GetType(Integer))
                    dtOrderDetail.Columns.Add("numShipClass", GetType(Long))
                    dtOrderDetail.Columns.Add("numUnitHour", GetType(Decimal))
                    dtOrderDetail.Columns.Add("fltWeight", GetType(Double))
                    dtOrderDetail.Columns.Add("fltWidth", GetType(Double))
                    dtOrderDetail.Columns.Add("fltHeight", GetType(Double))
                    dtOrderDetail.Columns.Add("fltLength", GetType(Double))
                    dtOrderDetail.Columns.Add("vcItemName", GetType(String))

                    Trace.Write("Start Tracing Rules detail.")
                    ' GET SHIPPING RULES AS PER ITEM'S NEEDS
                    Dim dtShipInfo As New DataTable
                    For Each drItem As DataRow In dsItems.Tables(0).Rows
                        objOppBizDocs.OppId = lngOppId
                        objOppBizDocs.OppBizDocId = OppBizDocID
                        objOppBizDocs.BizDocItemId = 0 ' CCommon.ToLong(drItem("OppBizDocItemID"))
                        objOppBizDocs.ShipClassID = CCommon.ToLong(drItem("numShipClass"))
                        objOppBizDocs.UnitHour = CCommon.ToDecimal(drItem("numUnitHour"))
                        objOppBizDocs.DomainID = Session("DomainID")
                        dtShipInfo = objOppBizDocs.GetShippingRuleInfo()

                        'Trace.Write("Shipping Rule dataset available or not: " & If(dtShipInfo Is Nothing, "NO", "YES, Then its row count is :" & dtShipInfo.Rows.Count))
                        If dtShipInfo IsNot Nothing AndAlso dtShipInfo.Rows.Count > 0 Then
                            Dim drOrderDetail As DataRow
                            drOrderDetail = dtOrderDetail.NewRow

                            drOrderDetail("OppId") = OppId
                            drOrderDetail("OppBizDocItemID") = 0 'CCommon.ToLong(drItem("OppBizDocItemID"))
                            drOrderDetail("ItemCode") = CCommon.ToLong(drItem("numItemCode"))
                            drOrderDetail("ShippingRuleID") = CCommon.ToLong(dtShipInfo.Rows(0)("numShippingRuleID"))
                            drOrderDetail("numShippingCompanyID1") = CCommon.ToInteger(dtShipInfo.Rows(0)("numShippingCompanyID1"))
                            drOrderDetail("numShippingCompanyID2") = CCommon.ToInteger(dtShipInfo.Rows(0)("numShippingCompanyID2"))

                            drOrderDetail("DomesticServiceID") = CCommon.ToInteger(dtShipInfo.Rows(0)("numDomesticShipID"))
                            drOrderDetail("InternationalServiceID") = CCommon.ToInteger(dtShipInfo.Rows(0)("numInternationalShipID"))
                            drOrderDetail("PackageTypeID") = CCommon.ToInteger(dtShipInfo.Rows(0)("numPackageTypeID"))
                            drOrderDetail("numShipClass") = CCommon.ToLong(drItem("numShipClass"))
                            drOrderDetail("numUnitHour") = CCommon.ToDecimal(drItem("numUnitHour"))

                            drOrderDetail("fltWeight") = CCommon.ToDouble(drItem("fltWeight"))
                            drOrderDetail("fltWidth") = CCommon.ToDouble(drItem("fltWidth"))
                            drOrderDetail("fltHeight") = CCommon.ToDouble(drItem("fltHeight"))
                            drOrderDetail("fltLength") = CCommon.ToDouble(drItem("fltLength"))
                            drOrderDetail("vcItemName") = CCommon.ToString(drItem("vcItemName"))

                            dtOrderDetail.Rows.Add(drOrderDetail)
                            dtOrderDetail.AcceptChanges()
                        End If

                    Next

                Else
                    dtResult = Nothing
                End If

                'Trace.Write("End Tracing Rules Detail.")

                If dtOrderDetail IsNot Nothing Then
                    Dim dtView As DataView = dtOrderDetail.DefaultView
                    dtView.Sort = "numShipClass"
                    dtResult = dtView.ToTable()
                Else
                    dtResult = Nothing
                End If

            Catch ex As Exception
                dtResult = Nothing
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                'DisplayError(ex.Message)
            End Try

            Return dtResult
        End Function

        Public Sub GetBoxLog()
            Try
                Dim dblTotalWeight As Double = 0
                If objShipping Is Nothing Then objShipping = New Shipping()
                Dim ds As New DataSet
                Dim dsFinal As New DataSet
                Dim intBoxCounter As Integer = 0
                Dim blnSameCountry As Boolean = False

                Dim dtBox As New DataTable("Box")
                dtBox.Columns.Add("vcBoxName")
                dtBox.Columns.Add("vcPackageName")
                dtBox.Columns.Add("fltTotalWeight")
                dtBox.Columns.Add("fltHeight")
                dtBox.Columns.Add("fltWidth")
                dtBox.Columns.Add("fltLength")
                dtBox.Columns.Add("numPackageTypeID")
                dtBox.Columns.Add("numServiceTypeID")
                dtBox.Columns.Add("fltDimensionalWeight")


                Dim dtItem As New DataTable("Item")
                dtItem.Columns.Add("vcBoxName")
                dtItem.Columns.Add("numOppBizDocItemID")
                dtItem.Columns.Add("intBoxQty")
                dtItem.Columns.Add("UnitWeight")
                dtItem.Columns.Add("vcItemName")
                dtItem.Columns.Add("fltHeight")
                dtItem.Columns.Add("fltLength")
                dtItem.Columns.Add("fltWidth")

                Dim dsItems As New DataSet
                Dim dtItems As New DataTable
                Dim dtShipDetail As DataTable = GetOrderItemShippingRules(lngOppId, 0, DomainID, 1)
                If dtShipDetail Is Nothing Then
                    ShowMessage(litMessage.Text & "Order ID " & lngOppId & " : " & vbCrLf & " Shipping Rule has not been found." & "<br/>")
                    Exit Sub
                End If

                dsItems.Tables.Add(dtShipDetail.Copy)
                If dsItems IsNot Nothing AndAlso dsItems.Tables.Count > 0 AndAlso dsItems.Tables(0).Rows.Count > 0 Then

                    If lngOppId > 0 Then
                        dtItems = dsItems.Tables(0).Clone
                        Dim objOpportunity As New MOpportunity
                        For i As Integer = 0 To dsItems.Tables(0).Rows.Count - 1

                            objOpportunity.OpportunityId = 0
                            objOpportunity.DomainID = Session("DomainID")
                            objOpportunity.ItemCode = CCommon.ToLong(dsItems.Tables(0).Rows(i)("ItemCode"))
                            If objOpportunity.CheckServiceItemInOrder() = True Then
                                Continue For
                            End If

                            Dim objOpp As New MOpportunity()
                            Dim objCommon As New CCommon()
                            objOpp.OpportunityId = lngOppId
                            objOpp.Mode = 1
                            Dim dtTable As DataTable = Nothing
                            dtTable = objOpp.GetOpportunityAddress()

                            Dim dtShipFrom As DataTable = Nothing
                            Dim objContacts As New CContacts()
                            objContacts.ContactID = Session("UserContactID")
                            objContacts.DomainID = Session("DomainID")
                            dtShipFrom = objContacts.GetBillOrgorContAdd()
                            If objCommon Is Nothing Then
                                objCommon = New CCommon()
                            End If

                            ' GET SHIPPING COMPANY INFORMATION
                            Dim dtShipCompany As DataTable = Nothing
                            objOppBizDocs.OppId = lngOppId
                            objOppBizDocs.ShipClassID = CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShipClass"))
                            objOppBizDocs.BizDocItemId = CCommon.ToLong(dsItems.Tables(0).Rows(i)("OppBizDocItemID"))

                            'IF Shipping Detail is not available continue process with another order detail
                            If dtTable.Rows.Count = 0 OrElse dtShipFrom.Rows.Count = 0 Then
                                Continue For
                            End If

                            If CCommon.ToLong(dtTable.Rows(0)("Country")) = CCommon.ToLong(dtShipFrom.Rows(0)("vcShipCountry")) Then
                                objOppBizDocs.ShipCompany = CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShippingCompanyID1"))
                                objOppBizDocs.BoxServiceTypeID = CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID"))
                            Else
                                objOppBizDocs.ShipCompany = CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShippingCompanyID2"))
                                objOppBizDocs.BoxServiceTypeID = CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID"))
                            End If

                            'Get Order Items and Process Shipping Label Generating Logic
                            If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs

                            objOppBizDocs.ShippingReportId = 0
                            objOppBizDocs.DomainID = DomainID
                            objOppBizDocs.OppBizDocId = 0

                            ' FROM DETAIL
                            objOppBizDocs.FromName = CCommon.ToString(dtShipFrom.Rows(0)("vcFirstname")) & " " & CCommon.ToString(dtShipFrom.Rows(0)("vcLastname"))
                            objOppBizDocs.FromCompany = CCommon.ToString(dtShipFrom.Rows(0)("vcCompanyName"))
                            objOppBizDocs.FromPhone = CCommon.ToString(dtShipFrom.Rows(0)("vcPhone"))
                            If dtShipFrom.Rows(0)("vcShipStreet").ToString().Contains(Environment.NewLine) Then
                                Dim strAddressLines As String() = dtShipFrom.Rows(0)("vcShipStreet").ToString().Split(System.Environment.NewLine) ' dtShipFrom.Rows(0)("vcShipStreet").ToString().Split(New Char() {Convert.ToChar(Environment.NewLine)}, StringSplitOptions.RemoveEmptyEntries)
                                objOppBizDocs.FromAddressLine1 = (If(strAddressLines.Length >= 0, strAddressLines(0).ToString(), "")).ToString()
                                objOppBizDocs.FromAddressLine2 = (If(strAddressLines.Length >= 1, strAddressLines(1).ToString(), "")).ToString()
                            Else
                                objOppBizDocs.FromAddressLine1 = CCommon.ToString(dtShipFrom.Rows(0)("vcShipStreet"))
                                objOppBizDocs.FromAddressLine2 = ""
                            End If

                            'objOppBizDocs.FromAddressLine1 = dtShipFrom.Rows[0]["vcShipStreet"].ToString().Substring(dtShipFrom.Rows[0]["vcShipStreet"].ToString().IndexOf(Environment.NewLine)).Replace(Environment.NewLine, " ");
                            'objOppBizDocs.FromAddressLine2 = dtShipFrom.Rows[0]["vcShipStreet"].ToString().Substring(0, dtShipFrom.Rows[0]["vcShipStreet"].ToString().IndexOf(Environment.NewLine));
                            objOppBizDocs.FromCountry = CCommon.ToString(dtShipFrom.Rows(0)("vcShipCountry"))
                            objOppBizDocs.FromState = CCommon.ToString(dtShipFrom.Rows(0)("vcShipState"))
                            objOppBizDocs.FromCity = CCommon.ToString(dtShipFrom.Rows(0)("vcShipCity"))
                            objOppBizDocs.FromZip = CCommon.ToString(dtShipFrom.Rows(0)("vcShipPostCode"))
                            objOppBizDocs.IsFromResidential = False

                            objOppBizDocs.UserCntID = UserCntID
                            ' Order Record Owner
                            objOppBizDocs.strText = Nothing

                            'TO DETAILS
                            objOppBizDocs.ToName = CCommon.ToString(dtTable.Rows(0)("Name"))
                            objOppBizDocs.ToCompany = CCommon.ToString(dtTable.Rows(0)("Company"))
                            objOppBizDocs.ToPhone = CCommon.ToString(dtTable.Rows(0)("Phone"))

                            If dtTable.Rows(0)("Street").ToString().Contains(Environment.NewLine) Then
                                Dim strAddressLines As String() = dtTable.Rows(0)("Street").ToString().Split(System.Environment.NewLine) 'dtTable.Rows(0)("Street").ToString().Split(New Char() {Convert.ToChar(Environment.NewLine)}, StringSplitOptions.RemoveEmptyEntries)
                                objOppBizDocs.ToAddressLine1 = (If(strAddressLines.Length >= 0, strAddressLines(0).ToString(), "")).ToString()
                                objOppBizDocs.ToAddressLine2 = (If(strAddressLines.Length >= 1, strAddressLines(1).ToString(), "")).ToString()
                            Else
                                objOppBizDocs.ToAddressLine1 = CCommon.ToString(dtTable.Rows(0)("Street"))
                                objOppBizDocs.ToAddressLine2 = ""
                            End If

                            objOppBizDocs.ToCity = CCommon.ToString(dtTable.Rows(0)("City"))
                            objOppBizDocs.ToState = CCommon.ToString(dtTable.Rows(0)("State"))
                            objOppBizDocs.ToCountry = CCommon.ToString(dtTable.Rows(0)("Country"))
                            objOppBizDocs.ToZip = CCommon.ToString(dtTable.Rows(0)("PostCode"))
                            objOppBizDocs.IsToResidential = False

                            ' SPECIAL SERVICES
                            objOppBizDocs.IsCOD = False
                            objOppBizDocs.IsDryIce = False
                            objOppBizDocs.IsHoldSaturday = False
                            objOppBizDocs.IsHomeDelivery = False
                            objOppBizDocs.IsInsideDelivery = False
                            objOppBizDocs.IsInsidePickup = False
                            objOppBizDocs.IsReturnShipment = False
                            objOppBizDocs.IsSaturdayDelivery = False
                            objOppBizDocs.IsSaturdayPickup = False
                            objOppBizDocs.CODAmount = 0
                            objOppBizDocs.CODType = ""
                            objOppBizDocs.TotalInsuredValue = 0
                            objOppBizDocs.IsAdditionalHandling = False
                            objOppBizDocs.LargePackage = False
                            objOppBizDocs.DeliveryConfirmation = ""
                            objOppBizDocs.Description = ""

                            'Update Shipping report address values and create shipping report items 
                            objShipping.DomainID = DomainID
                            objShipping.UserCntID = UserCntID
                            objShipping.ShippingReportId = 0
                            objShipping.OpportunityId = lngOppId
                            objShipping.OppBizDocId = objOppBizDocs.OppBizDocId
                            objShipping.InvoiceNo = CCommon.ToString(objOppBizDocs.OppBizDocId)

                            'If dtShipCompany IsNot Nothing AndAlso dtShipCompany.Rows.Count > 0 Then
                            objShipping.Provider = CCommon.ToInteger(dsItems.Tables(0).Rows(i)("numShippingCompanyID1"))
                            objShipping.PackagingType = CCommon.ToShort(dsItems.Tables(0).Rows(i)("PackageTypeID"))

                            If CCommon.ToLong(dtTable.Rows(0)("Country")) = CCommon.ToLong(dtShipFrom.Rows(0)("vcShipCountry")) Then
                                objShipping.ServiceType = CCommon.ToShort(dsItems.Tables(0).Rows(i)("DomesticServiceID"))
                                blnSameCountry = True
                            Else
                                objShipping.ServiceType = CCommon.ToShort(dsItems.Tables(0).Rows(i)("InternationalServiceID"))
                            End If

                            'FOR FOLLOWING SERVICE TYPES,IN FEDEX THERE IS ONLY ONE "YOUR PACKAGING" IS AVAILABLE
                            If objShipping.Provider = 91 AndAlso
                               (objShipping.ServiceType = 15 OrElse objShipping.ServiceType = 16 OrElse
                                objShipping.ServiceType = 17 OrElse objShipping.ServiceType = 18 OrElse
                                objShipping.ServiceType = 19) Then

                                objShipping.PackagingType = 21

                            End If
                            '------------------------------------------------------------------------------------

                            Dim dtFields As New DataTable
                            'If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                            dtFields.Columns.Add("numBoxID")
                            dtFields.Columns.Add("tintServiceType")
                            dtFields.Columns.Add("dtDeliveryDate")
                            dtFields.Columns.Add("monShippingRate")
                            dtFields.Columns.Add("fltTotalWeight")
                            dtFields.Columns.Add("intNoOfBox")
                            dtFields.Columns.Add("fltHeight")
                            dtFields.Columns.Add("fltWidth")
                            dtFields.Columns.Add("fltLength")
                            dtFields.Columns.Add("fltDimensionalWeight")
                            dtFields.TableName = "Box"

                            Dim lngBoxID As Long = 0
                            Dim strBoxID As String = 0
                            Dim lngTotalQty As Long = 0
                            Dim dblWidth As Double = 0
                            Dim dblHeight As Double = 0
                            Dim dblLength As Double = 0
                            Dim dblWeight As Double = 0

                            Dim lngPackageTypeID As Long = 0
                            Dim lngServiceTypeID As Long = 0

                            If dtItems.Select("numShipClass = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShipClass")) &
                                              " AND PackageTypeID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("PackageTypeID")) &
                                              " AND DomesticServiceID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID")) &
                                              " AND InternationalServiceID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("InternationalServiceID"))).Length > 0 Then
                                Continue For
                            Else
                                dtItems.Rows.Clear()
                            End If

                            lngTotalQty = 0
                            Dim drClass() As DataRow = dsItems.Tables(0).Select("numShipClass = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("numShipClass")) &
                                                                                " AND PackageTypeID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("PackageTypeID")) &
                                                                                " AND DomesticServiceID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("DomesticServiceID")) &
                                                                                " AND InternationalServiceID = " & CCommon.ToLong(dsItems.Tables(0).Rows(i)("InternationalServiceID")))
                            For Each dr As DataRow In drClass
                                dtItems.ImportRow(dr)
                                lngTotalQty = lngTotalQty + CCommon.ToDecimal(dr("numUnitHour"))
                            Next

                            Dim dblBoxQty As Double
                            Dim dblItemQty As Double
                            Dim dblCarryFwdQty As Double = 0
                            Dim dblQtyToBox As Double = 0
                            Dim dblWeightToBox As Double = 0
                            Dim dblDimensionalWeight As Double = 0

                            For iItem As Integer = 0 To dtItems.Rows.Count - 1

                                objOppBizDocs.OppId = lngOppId
                                objOppBizDocs.ShipClassID = CCommon.ToLong(dtItems.Rows(iItem)("numShipClass"))
                                objOppBizDocs.ItemCode = CCommon.ToLong(dtItems.Rows(iItem)("ItemCode"))
                                objOppBizDocs.UnitHour = IIf(lngTotalQty = 0, CCommon.ToDouble(dtItems.Rows(iItem)("numUnitHour")), lngTotalQty)
                                objOppBizDocs.PackageTypeID = CCommon.ToLong(dtItems.Rows(iItem)("PackageTypeID"))
                                objOppBizDocs.DomainID = DomainID
                                lngPackageTypeID = objOppBizDocs.PackageTypeID
                                lngServiceTypeID = objShipping.ServiceType

                                objOpportunity.OpportunityId = 0
                                objOpportunity.DomainID = Session("DomainID")
                                objOpportunity.ItemCode = CCommon.ToLong(dtItems.Rows(iItem)("ItemCode"))
                                If objOpportunity.CheckServiceItemInOrder() = True Then
                                    Continue For
                                End If

                                Dim dtPackages As DataTable = Nothing
                                Dim intBoxCount As Integer = 0
                                If objShipping.PackagingType <> 21 Then
                                    'ADD ITEMS INTO PACKAGE AS PER PACKAGE RULES
                                    dtPackages = objOppBizDocs.GetPackageInfo()
                                    'If package rule not found then set default 
                                    If dtPackages IsNot Nothing AndAlso dtPackages.Rows.Count > 0 Then
                                        intBoxCount = Math.Ceiling(objOppBizDocs.UnitHour / If(dtPackages.Rows(0)("numFromQty") = 0, 1, CCommon.ToDouble(dtPackages.Rows(0)("numFromQty"))))

                                        dblWeight = CCommon.ToDouble(dtPackages.Rows(0)("fltTotalWeight"))
                                        dblWidth = CCommon.ToDouble(dtPackages.Rows(0)("fltWidth"))
                                        dblHeight = CCommon.ToDouble(dtPackages.Rows(0)("fltHeight"))
                                        dblLength = CCommon.ToDouble(dtPackages.Rows(0)("fltLength"))
                                        dblDimensionalWeight = (If(dblWidth = 0, 1, dblWidth) * If(dblHeight = 0, 1, dblHeight) * If(dblLength = 0, 1, dblLength)) / 166

                                    Else
                                        ShowMessage(litMessage.Text & "Order ID " & objOppBizDocs.OppId & " : " & vbCrLf & " Package Rule has not been found." & "<br/>")
                                        Continue For

                                    End If

                                Else
                                    intBoxCount = 1
                                    dblWeight = 1
                                    dblWidth = 1
                                    dblHeight = 1
                                    dblLength = 1
                                    dblDimensionalWeight = (If(dblWidth = 0, 1, dblWidth) * If(dblHeight = 0, 1, dblHeight) * If(dblLength = 0, 1, dblLength)) / 166

                                End If

                                If objOppBizDocs.UnitHour <= 0 Then
                                    ShowMessage(litMessage.Text & "Order ID " & objOppBizDocs.OppId & " : " & "Quantity can not be zero." & "<br/>")
                                    Continue For
                                End If

                                dblBoxQty = CCommon.ToDouble(dtPackages.Rows(0)("numFromQty"))
                                dblItemQty = CCommon.ToDouble(dtItems.Rows(iItem)("numUnitHour"))

                                If lngTotalQty <> CCommon.ToDouble(dtItems.Rows(iItem)("numUnitHour")) Then
                                    dblItemQty = dblCarryFwdQty + CCommon.ToDouble(dtItems.Rows(iItem)("numUnitHour"))
                                End If

                                If intBoxCounter = intBoxCount - 1 AndAlso intBoxCounter > 1 Then
                                    Exit For
                                Else
                                    For intCount As Integer = 0 To intBoxCount - 1

                                        Dim dr As DataRow
                                        If dblItemQty <= 0 Then
                                            If dblItemQty = 0 Then dblCarryFwdQty = 0
                                            intBoxCounter = intBoxCounter - 1
                                            Exit For

                                        ElseIf dblItemQty < dblBoxQty Then
                                            intBoxCounter = intBoxCounter + 1
                                            dblCarryFwdQty = dblItemQty

                                            If dblCarryFwdQty > 0 AndAlso dtItems.Rows.Count <> iItem + 1 Then
                                                dr = dtItem.NewRow
                                                dr("vcBoxName") = "Box" & intBoxCounter ' (intCount + i + 1).ToString()
                                                dr("numOppBizDocItemID") = CCommon.ToLong(dtItems.Rows(iItem)("OppBizDocItemID"))
                                                dr("intBoxQty") = dblCarryFwdQty
                                                dr("UnitWeight") = CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")) ' dblWeight
                                                dr("vcItemName") = CCommon.ToString(dtItems.Rows(iItem)("vcItemName"))
                                                dr("fltHeight") = CCommon.ToDouble(dtItems.Rows(iItem)("fltHeight"))
                                                dr("fltLength") = CCommon.ToDouble(dtItems.Rows(iItem)("fltLength"))
                                                dr("fltWidth") = CCommon.ToDouble(dtItems.Rows(iItem)("fltWidth"))

                                                dtItem.Rows.Add(dr)
                                                dblTotalWeight = dblTotalWeight + CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")) 'dblWeight

                                                dblQtyToBox = dblCarryFwdQty
                                            Else
                                                ''Check for other Shipping Rule / Package Rule available for Remaining Quantity or not.

                                                Dim dtShipInfoNew As DataTable
                                                objOppBizDocs.OppId = lngOppId
                                                objOppBizDocs.OppBizDocId = 0
                                                objOppBizDocs.BizDocItemId = 0
                                                objOppBizDocs.ShipClassID = CCommon.ToLong(dtItems.Rows(iItem)("numShipClass"))
                                                objOppBizDocs.UnitHour = dblCarryFwdQty
                                                dtShipInfoNew = objOppBizDocs.GetShippingRuleInfo()

                                                ''If available then use that package otherwise same package will be used.
                                                If dtShipInfoNew IsNot Nothing AndAlso dtShipInfoNew.Rows.Count > 0 Then
                                                    objOppBizDocs.OppId = lngOppId
                                                    objOppBizDocs.ShipClassID = CCommon.ToLong(dtItems.Rows(iItem)("numShipClass"))
                                                    objOppBizDocs.ItemCode = CCommon.ToLong(dtItems.Rows(iItem)("ItemCode"))
                                                    objOppBizDocs.UnitHour = dblCarryFwdQty 'IIf(lngTotalQty = 0, CCommon.ToLong(dtItems.Rows(iItem)("numUnitHour")), lngTotalQty)
                                                    objOppBizDocs.PackageTypeID = CCommon.ToLong(dtShipInfoNew.Rows(0)("numPackageTypeID")) 'objShipping.PackagingType
                                                    objOppBizDocs.DomainID = DomainID

                                                    'ADD ITEMS INTO PACKAGE AS PER PACKAGE RULES
                                                    Dim dtCarryFPackage As DataTable = Nothing
                                                    dtCarryFPackage = objOppBizDocs.GetPackageInfo()
                                                    'If package rule not found then set default 
                                                    If dtPackages IsNot Nothing AndAlso dtCarryFPackage.Rows.Count > 0 Then
                                                        lngPackageTypeID = CCommon.ToLong(dtCarryFPackage.Rows(0)("numCustomPackageID"))
                                                        lngServiceTypeID = If(blnSameCountry = True, CCommon.ToShort(dtShipInfoNew.Rows(0)("numDomesticShipID")), CCommon.ToShort(dtShipInfoNew.Rows(0)("numInternationalShipID")))

                                                    End If
                                                End If


                                                dr = dtItem.NewRow
                                                dr("vcBoxName") = "Box" & intBoxCounter
                                                dr("numOppBizDocItemID") = CCommon.ToLong(dtItems.Rows(iItem)("OppBizDocItemID"))
                                                dr("vcItemName") = CCommon.ToString(dtItems.Rows(iItem)("vcItemName"))
                                                dr("fltHeight") = CCommon.ToDouble(dtItems.Rows(iItem)("fltHeight"))
                                                dr("fltLength") = CCommon.ToDouble(dtItems.Rows(iItem)("fltLength"))
                                                dr("fltWidth") = CCommon.ToDouble(dtItems.Rows(iItem)("fltWidth"))

                                                If dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'").Length > 0 Then
                                                    Dim intExistBoxQty As Integer
                                                    For Each drQty As DataRow In dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'")
                                                        intExistBoxQty = intExistBoxQty + CCommon.ToDouble(drQty("intBoxQty"))
                                                    Next
                                                    If dblBoxQty <= intExistBoxQty Then
                                                        Continue For
                                                    Else
                                                        dblCarryFwdQty = dblBoxQty - intExistBoxQty
                                                    End If
                                                End If

                                                dr("intBoxQty") = dblCarryFwdQty
                                                dr("UnitWeight") = CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight"))
                                                dtItem.Rows.Add(dr)

                                                dblTotalWeight = dblTotalWeight + CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight"))
                                                dblQtyToBox = dblCarryFwdQty
                                            End If

                                            dblWeightToBox = dblQtyToBox * CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight"))
                                        Else

                                            intBoxCounter = intBoxCounter + 1
                                            If intBoxCounter = intBoxCount - 1 AndAlso iItem = dtItems.Rows.Count - 1 Then
                                            End If

                                            dr = dtItem.NewRow
                                            dr("vcBoxName") = "Box" & intBoxCounter
                                            dr("numOppBizDocItemID") = CCommon.ToLong(dtItems.Rows(iItem)("OppBizDocItemID"))
                                            dr("vcItemName") = CCommon.ToString(dtItems.Rows(iItem)("vcItemName"))
                                            dr("fltHeight") = CCommon.ToDouble(dtItems.Rows(iItem)("fltHeight"))
                                            dr("fltLength") = CCommon.ToDouble(dtItems.Rows(iItem)("fltLength"))
                                            dr("fltWidth") = CCommon.ToDouble(dtItems.Rows(iItem)("fltWidth"))

                                            Dim dblExistBoxQty As Double
                                            If dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'").Length > 0 Then

                                                For Each drQty As DataRow In dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'")
                                                    dblExistBoxQty = dblExistBoxQty + CCommon.ToDouble(drQty("intBoxQty"))
                                                Next
                                                If dblBoxQty <= dblExistBoxQty Then
                                                    Continue For
                                                Else
                                                    dblExistBoxQty = dblBoxQty - dblExistBoxQty
                                                End If
                                            Else
                                                dblExistBoxQty = dblBoxQty - dblCarryFwdQty
                                            End If

                                            dr("intBoxQty") = dblExistBoxQty
                                            dr("UnitWeight") = CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight"))

                                            dtItem.Rows.Add(dr)
                                            dblTotalWeight = dblTotalWeight + CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight"))

                                            dblQtyToBox = dblBoxQty - dblCarryFwdQty

                                            If dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'").Length > 1 Then
                                                For Each drI As DataRow In dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'")
                                                    dblWeightToBox = dblWeightToBox + (drI("intBoxQty") * drI("UnitWeight"))
                                                Next
                                            Else
                                                dblWeightToBox = (dblQtyToBox * CCommon.ToDouble(dtItems.Rows(iItem)("fltWeight")))

                                            End If

                                        End If

                                        ''Creating Box Entry
                                        dr = dtBox.NewRow
                                        dr("vcBoxName") = "Box" & intBoxCounter
                                        dr("vcPackageName") = CCommon.ToString(dtPackages.Rows(0)("vcPackageName"))
                                        dr("fltTotalWeight") = dblWeightToBox
                                        dr("fltLength") = dblLength
                                        dr("fltWidth") = dblWidth
                                        dr("fltHeight") = dblHeight
                                        dr("numPackageTypeID") = lngPackageTypeID
                                        dr("numServiceTypeID") = lngServiceTypeID
                                        dr("fltDimensionalWeight") = dblDimensionalWeight

                                        If dtItem.Select("vcBoxName='" & "Box" & intBoxCounter & "'").Length > 1 Then
                                            dtBox.Rows.Remove(dtBox.Select("vcBoxName='" & "Box" & intBoxCounter & "'")(0))
                                        End If
                                        dtBox.Rows.Add(dr)

                                        dblWeightToBox = 0
                                        dblTotalWeight = 0
                                        dblItemQty = dblItemQty - dblQtyToBox
                                    Next
                                End If
                            Next
                        Next

                        dtBox.TableName = "Box"
                        dtItem.TableName = "Item"
                        ds.Tables.Add(dtBox)
                        ds.Tables.Add(dtItem)

                        Dim dtMain As New DataTable
                        dtMain.Columns.Add("AddedBy")
                        dtMain.Columns.Add("AddedOn")
                        dtMain.Columns.Add("RegularWeight")
                        dtMain.Columns.Add("DimensionalWeight")
                        Dim dblRegularWeight As Double = 0
                        Dim dblDimWeight As Double = 0

                        For Each drBox As DataRow In dtBox.Rows
                            dblRegularWeight = dblRegularWeight + drBox("fltTotalWeight")
                            dblDimWeight = dblDimWeight + drBox("fltDimensionalWeight")
                        Next

                        Dim drMain As DataRow
                        drMain = dtMain.NewRow
                        drMain("AddedBy") = objOppBizDocs.FromName
                        drMain("AddedOn") = CCommon.ToSqlDate(DateTime.Now)
                        drMain("RegularWeight") = dblRegularWeight
                        drMain("DimensionalWeight") = dblDimWeight
                        dtMain.Rows.Add(drMain)
                        dtMain.TableName = "Main"
                        dtMain.AcceptChanges()
                        ds.Tables.Add(dtMain)

                        If Not Directory.Exists(CCommon.GetDocumentPhysicalPath(DomainID)) Then
                            Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(DomainID))
                        End If

                        ds.WriteXml(CCommon.GetDocumentPhysicalPath(DomainID) & "OrderShippingDetail_" & lngOppId & "_" & CCommon.ToString(DomainID) & ".xml")

                        Dim str As String
                        str = String.Format("<script language='javascript'>window.open('../opportunity/frmOrderLog.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID={0}','','toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')</script>",
                                            lngOppId.ToString())

                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "ShippingLog", str, False)
                    Else
                        ShowMessage("Shipping Rules are not defined/found.")
                    End If
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub MileStone1_Load(sender As Object, e As EventArgs) Handles MileStone1.Load
            Try
                If IsPostBack Then
                    LoadControls()
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

#Region "Shipping Enhancement"

#Region "Button Events"

        Private Sub btnAddUpdate1_Click(sender As Object, e As EventArgs) Handles btnAddUpdate1.Click
            Try
                If objOpportunity Is Nothing Then objOpportunity = New OppotunitiesIP
                With objOpportunity
                    .OpportunityId = lngOppId
                    .DomainID = DomainID
                    .ItemCode = lngShippingItemCode
                    .sMode = 1
                    If .CheckServiceItemInOrder() = True Then
                        ShowMessage("Shipping charge can�t be re-calculated and changed at this point because it�s already been added as a line item within an invoice Or invoice is already generated. Delete the invoice and try again.")

                    Else
                        If ddlShippingService.Items.Count > 0 Then
                            If lngShippingItemCode = 0 Then
                                ShowMessage("Please first set Default Shipping Item from Administration->Global Settings->Accounting Tab->Shipping Service Item.")
                                Return
                            End If

                            AddShippingItem(True)
                            litMessage.Text = ""
                            SetShippingChargesLabel()
                            UpdateOpportunity()
                        End If
                    End If

                End With

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnAddUpdate2_Click(sender As Object, e As EventArgs) Handles btnAddUpdate2.Click
            Try
                If objOpportunity Is Nothing Then objOpportunity = New OppotunitiesIP
                With objOpportunity
                    .OpportunityId = lngOppId
                    .DomainID = DomainID
                    .ItemCode = lngShippingItemCode
                    .sMode = 1
                    If .CheckServiceItemInOrder() = True Then
                        ShowMessage("Shipping charge can�t be re-calculated and changed at this point because it�s already been added as a line item within an invoice Or invoice is already generated. Delete the invoice and try again.")

                    Else
                        If ddlShippingService.Items.Count > 0 Then
                            If lngShippingItemCode = 0 Then
                                ShowMessage("Please first set Default Shipping Item from Administration->Global Settings->Accounting Tab->Shipping Service Item.")
                                Return
                            End If

                            AddShippingItem(False)
                            litMessage.Text = ""
                            SetShippingChargesLabel()
                            UpdateOpportunity()
                        End If

                    End If

                End With

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnCalculateShippingRate_Click(sender As Object, e As EventArgs) Handles btnCalculateShippingRate.Click
            Try
                If CCommon.ToDouble(lblTotalWeight.Text) <= 0 Then
                    ShowMessage("Package must weigh more than zero pounds.")
                    Exit Sub
                End If

                Dim intShippingCompany As Integer = 0
                If CCommon.ToInteger(ddlShippingService.SelectedValue) >= 10 AndAlso CCommon.ToInteger(ddlShippingService.SelectedValue) <= 28 Then
                    intShippingCompany = 91

                ElseIf CCommon.ToInteger(ddlShippingService.SelectedValue) >= 40 AndAlso CCommon.ToInteger(ddlShippingService.SelectedValue) <= 55 Then
                    intShippingCompany = 90

                ElseIf CCommon.ToInteger(ddlShippingService.SelectedValue) >= 70 AndAlso CCommon.ToInteger(ddlShippingService.SelectedValue) <= 76 Then
                    intShippingCompany = 88
                End If

                Dim objContacts As New CContacts()
                Dim dtFromAddress As New DataTable
                objContacts.DivisionID = CCommon.ToDouble(Session("UserDivisionID"))
                objContacts.ContactID = CCommon.ToDouble(Session("UserContactID"))
                objContacts.DomainID = CCommon.ToDouble(Session("DomainID"))
                dtFromAddress = objContacts.GetBillOrgorContAdd()

                If objOppBizDocs Is Nothing Then objOppBizDocs = New OppBizDocs
                objOppBizDocs.ShipCompany = intShippingCompany
                objOppBizDocs.ShipFromCountry = CCommon.ToLong(dtFromAddress.Rows(0)("vcShipCountry"))
                objOppBizDocs.ShipFromState = CCommon.ToLong(dtFromAddress.Rows(0)("vcShipState"))
                objOppBizDocs.ShipToCountry = CCommon.ToString(hdnCountry.Value)
                objOppBizDocs.ShipToState = CCommon.ToString(hdnState.Value)
                objOppBizDocs.strShipFromCountry = ""
                objOppBizDocs.strShipFromState = ""
                objOppBizDocs.strShipToCountry = ""
                objOppBizDocs.strShipToState = ""
                objOppBizDocs.GetShippingAbbreviation()

                '#Region "Get Shipping Rates"
                Dim objShipping As New Shipping()
                objShipping.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                objShipping.WeightInLbs = CCommon.ToDouble(lblTotalWeight.Text)
                objShipping.NoOfPackages = 1
                'objShipping.UseDimentions = If(CCommon.ToInteger(dr("numShippingCompanyID")) = 91, True, False)
                objShipping.GroupCount = 1
                objShipping.UseDimentions = False

                objShipping.SenderState = objOppBizDocs.strShipFromState
                objShipping.SenderZipCode = CCommon.ToString(dtFromAddress.Rows(0)("vcShipPostCode")) 'CCommon.ToString(dtTable.Rows(0)("vcWPinCode"))
                objShipping.SenderCountryCode = objOppBizDocs.strShipFromCountry

                objShipping.RecepientState = objOppBizDocs.strShipToState
                objShipping.RecepientZipCode = CCommon.ToString(hdnPostal.Value)
                objShipping.RecepientCountryCode = objOppBizDocs.strShipToCountry

                objShipping.ServiceType = Short.Parse(ddlShippingService.SelectedValue) ' Short.Parse(CCommon.ToString(dr("intNsoftEnum")))
                objShipping.ConfigRateType = Short.Parse(ddlRateType.SelectedValue)
                objShipping.PackagingType = 21

                Dim dtShipRates As DataTable = objShipping.GetRates()
                If dtShipRates IsNot Nothing AndAlso dtShipRates.Rows.Count > 0 Then

                    Dim dcShippingRates As Decimal = 0
                    If chkMarkupShippingCharges.Checked = True Then
                        dcShippingRates = CCommon.ToDecimal(dtShipRates.Rows(0)("Rate")) + ((CCommon.ToDecimal(dtShipRates.Rows(0)("Rate")) * CCommon.ToDecimal(txtMarkupShippingCharge.Text)) / 100)
                    Else
                        dcShippingRates = CCommon.ToDecimal(dtShipRates.Rows(0)("Rate"))
                    End If

                    lblCalculatedShippingRate.Text = CCommon.ToString(Session("Currency")).Trim() & " " & [String].Format("{0:#,##0.00}", dcShippingRates)
                    hdnCalculatedShippingRate.Value = dcShippingRates

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

#End Region

#Region "Private Subroutines"

        Sub UpdateOpportunity()
            Try
                Dim arrOutPut() As String
                Dim objOpportunity As New OppotunitiesIP
                objOpportunity.OpportunityId = CCommon.ToLong(GetQueryStringVal("opid"))
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objOpportunity.OpportunityDetails()

                objOpportunity.OpportunityId = CCommon.ToLong(GetQueryStringVal("opid"))
                objOpportunity.PublicFlag = 0
                objOpportunity.UserCntID = Session("UserContactID")
                objOpportunity.DomainID = Session("DomainId")
                objOpportunity.OppType = CCommon.ToInteger(OppType.Value)
                objOpportunity.CouponCode = ""
                objOpportunity.Discount = 0
                objOpportunity.boolDiscType = False
                objOpportunity.UseShippersAccountNo = CCommon.ToBool(chkUseCustomerShippingAccount.Checked)
                objOpportunity.UseMarkupShippingRate = CCommon.ToBool(chkMarkupShippingCharges.Checked)
                objOpportunity.dcMarkupShippingRate = CCommon.ToDecimal(txtMarkupShippingCharge.Text)
                objOpportunity.ShippingService = CCommon.ToInteger(ddlShippingService.SelectedValue)

                Dim dsTempCopy As New DataSet
                dsTempCopy = dsTemp.Copy()

                dsTempCopy.Tables(2).Rows.Clear()
                dsTempCopy.AcceptChanges()

                If Not ViewState("SOPOItems") Is Nothing Then
                    'dsTempCopy = ViewState("SOPOItems")
                    objOpportunity.strItems = "<?xml version=""1.0"" encoding=""iso-8859-1"" ?>" & dsTempCopy.GetXml
                End If
                objOpportunity.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                arrOutPut = objOpportunity.Save()

                objOpportunity.OpportunityId = arrOutPut(0)
                ''Added By Sachin Sadhu||Date:29thApril12014
                ''Purpose :To Added Opportunity data in work Flow queue based on created Rules
                ''          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = lngOppId
                objWfA.SaveWFOrderQueue()
                'end of code

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub BindShippingDataGrid()
            Try
                Dim objOpp As New MOpportunity
                objOpp.OpportunityId = CCommon.ToLong(GetQueryStringVal("opid"))
                objOpp.DomainID = Session("DomainID")

                Dim dtShipping As New DataTable
                dtShipping = objOpp.GetShippingDetailsForOrder()
                gvShipping.DataSource = dtShipping
                gvShipping.DataBind()
                Dim deliveryDate As String = "-"
                Dim TransitTime As String = "-"
                If (dtShipping.Rows.Count > 0) Then
                    If (Convert.ToString(dtShipping.Rows(0)("vcTrackingNumber")) <> "" Or Convert.ToString(dtShipping.Rows(0)("vcTrackingNumber")) <> ",") Then

                        lngShippingCompanyId = CCommon.ToLong(dtShipping.Rows(0)("numShippingCompany"))
                        hdnShippingCompanyId.Value = lngShippingCompanyId
                        Dim strTrackingNo As String = Convert.ToString(dtShipping.Rows(0)("vcTrackingNumber"))
                        Dim strArray As String() = strTrackingNo.Split(",")
                        If (strArray.Length > 0) Then
                            hdnTrackingNo.Value = strArray(0)
                            deliveryDate = GetDeliveryDate(strArray(0))
                        End If
                        TransitTime = GetDeliveryDateandTransit(CCommon.ToLong(dtShipping.Rows(0)("numShippingReportId")), CCommon.ToLong(dtShipping.Rows(0)("numoppbizdocsid")))
                        If (lngShippingCompanyId = 88) Then
                            If (deliveryDate <> "-" AndAlso deliveryDate <> "" AndAlso TransitTime <> "" AndAlso TransitTime <> "-") Then

                                deliveryDate = Convert.ToDateTime(deliveryDate).AddDays(TransitTime)
                            End If
                        End If
                    End If

                End If
                If (deliveryDate <> "-" AndAlso deliveryDate <> "") Then
                    deliveryDate = FormattedDateFromDate(deliveryDate, Session("DateFormat"))
                    lblDeliveryDate.Text = deliveryDate
                Else
                    lblDeliveryDate.Text = "Delivery Date - Not Available"
                End If


                If (TransitTime = "" AndAlso TransitTime = "-") Then
                    lblDayinTransit.Text = "Days in Transit - Not Available"
                End If
                lblDayinTransit.Text = "Days in Transit : " & TransitTime
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function GetDeliveryDate(ShipmentNo As String) As String
            Dim PackageDeliveryDate As String = "-"
            Try
                _UpsTrack.IdentifierType = UpstrackIdentifierTypes.uitPackageTrackingNumber
                _UpsTrack.ShipDateStart = "20010219"
                'yyyMMdd
                _UpsTrack.ShipDateEnd = "20010228"
                'yyyMMdd.
                NSAPIDETAILS()

                '_FedexTrack.TrackShipment("956473415129602");
                If lngShippingCompanyId = 88 Then
                    Try
                        _UpsTrack.TrackShipment(ShipmentNo)
                        PackageDeliveryDate = _UpsTrack.ShipDate
                    Catch ex As Exception
                        'DO NOT THROW ERROR
                    End Try
                Else
                    Try
                        _FedexTrack.TrackShipment(ShipmentNo)
                        If _FedexTrack.PackageDeliveryDate = "" Then
                            PackageDeliveryDate = _FedexTrack.PackageDeliveryEstimate
                        Else
                            PackageDeliveryDate = _FedexTrack.PackageDeliveryDate
                        End If
                    Catch ex As Exception
                        'DO NOT THROW ERROR
                    End Try
                End If
            Catch
                Throw
            End Try
            Return PackageDeliveryDate
        End Function

        Public Function GetDeliveryDateandTransit(ShippingReportId As Long, ShippingBizDocId As Long) As String
            Dim transitTime As String = "-"
            Dim ds As New DataSet()
            Dim objOppBizDocs As New OppBizDocs()
            Try
                objOppBizDocs.DomainID = CCommon.ToLong(Session("DomainID"))
                objOppBizDocs.OppBizDocId = ShippingBizDocId
                objOppBizDocs.ShippingReportId = ShippingReportId
                ds = objOppBizDocs.GetShippingReport()
                If ds.Tables(0).Rows.Count > 0 Then

                    Dim objShipping As New Shipping()
                    objShipping.DomainID = CCommon.ToLong(HttpContext.Current.Session("DomainID"))
                    objShipping.WeightInLbs = CCommon.ToDouble(ds.Tables(0).Rows(0)("fltTotalDimensionalWeight"))
                    objShipping.NoOfPackages = 1
                    objShipping.UseDimentions = If(CCommon.ToInteger(ds.Tables(0).Rows(0)("numShippingCompany")) = 91, True, False)
                    'for fedex dimentions are must
                    objShipping.GroupCount = 1
                    'if (objShipping.UseDimentions)
                    '{
                    '    objShipping.Height = CCommon.ToDouble(ds.Tables[0].Rows[0]["fltHeight"]);
                    '    objShipping.Width = CCommon.ToDouble(ds.Tables[0].Rows[0]["fltWidth"]);
                    '    objShipping.Length = CCommon.ToDouble(ds.Tables[0].Rows[0]["fltLength"]);
                    '}

                    objShipping.SenderState = CCommon.ToString(ds.Tables(0).Rows(0)("vcFromState"))
                    objShipping.SenderZipCode = CCommon.ToString(ds.Tables(0).Rows(0)("vcFromZip"))
                    'CCommon.ToString(dtTable.Rows(0)("vcWPinCode"))
                    objShipping.SenderCountryCode = CCommon.ToString(ds.Tables(0).Rows(0)("vcFromCountry"))

                    objShipping.RecepientState = CCommon.ToString(ds.Tables(0).Rows(0)("vcToState"))
                    objShipping.RecepientZipCode = CCommon.ToString(ds.Tables(0).Rows(0)("vcToZip"))
                    objShipping.RecepientCountryCode = CCommon.ToString(ds.Tables(0).Rows(0)("vcToCountry"))

                    objShipping.ServiceType = CCommon.ToShort(ds.Tables(0).Rows(0)("numServiceTypeID"))
                    objShipping.PackagingType = 21
                    'ptYourPackaging 
                    objShipping.ItemCode = 1
                    objShipping.OppBizDocItemID = 0
                    objShipping.ID = 9999
                    objShipping.Provider = CCommon.ToInteger(ds.Tables(0).Rows(0)("numShippingCompany"))

                    Dim dtShipRates As DataTable = objShipping.GetRates()
                    If dtShipRates.Rows.Count > 0 Then
                        transitTime = CCommon.ToString(dtShipRates.Rows(0)("TransitTIme"))
                    End If

                End If
            Catch ex As Exception
                Throw
            End Try
            Return transitTime
        End Function

        Sub NSAPIDETAILS()
            Try
                Dim objItem As New BACRM.BusinessLogic.Item.CItems()
                Dim dsShippingDtl As DataSet = Nothing
                objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                objItem.ShippingCMPID = lngShippingCompanyId
                dsShippingDtl = objItem.ShippingDtls()
                For Each dr As DataRow In dsShippingDtl.Tables(0).Rows
                    Select Case dr("vcFieldName").ToString()

                        'Case "Rate Type"
                        '    If Rates1.Provider = EzratesProviders.pFedEx Then
                        '        Rates1.Config("RateType=" & dr("vcShipFieldValue").ToString())
                        '    End If


                        Case "Developer Key"
                            _FedexTrack.FedExAccount.DeveloperKey = dr("vcShipFieldValue").ToString()

                            Exit Select
                        Case "Account Number"
                            _FedexTrack.FedExAccount.AccountNumber = dr("vcShipFieldValue").ToString()

                            Exit Select
                        Case "Meter Number"
                            _FedexTrack.FedExAccount.MeterNumber = dr("vcShipFieldValue").ToString()
                            Exit Select
                            'case "Weight Unit":
                            '    if (dr["vcShipFieldValue"].ToString().ToLower().Equals("lbs"))
                            '    {
                            '        Rates1.Config("WeightUnit=LB");
                            '    }
                            '    else
                            '    {
                            '        Rates1.Config("WeightUnit=KG");
                            '    }

                            Exit Select
                            'Common fields
                        Case "Password"
                            _FedexTrack.FedExAccount.Password = dr("vcShipFieldValue").ToString()
                            _UpsTrack.UPSAccount.Password = dr("vcShipFieldValue").ToString()
                            Exit Select
                        Case "Server URL"
                            _FedexTrack.FedExAccount.Server = dr("vcShipFieldValue").ToString()

                            Exit Select
                        Case "Tracking URL"
                            _UpsTrack.UPSAccount.Server = dr("vcShipFieldValue").ToString()
                            Exit Select
                            'Fields specific to UPS
                        Case "AccessKey"
                            '_FedexTrack.FedExAccount.AccessKey = dr["vcShipFieldValue"].ToString();
                            _UpsTrack.UPSAccount.AccessKey = dr("vcShipFieldValue").ToString()
                            Exit Select
                        Case "UserID"
                            '_FedexTrack.FedExAccount.UserId = dr["vcShipFieldValue"].ToString();
                            _UpsTrack.UPSAccount.UserId = dr("vcShipFieldValue").ToString()
                            Exit Select
                        Case "ShipperNo"
                            _FedexTrack.FedExAccount.AccountNumber = dr("vcShipFieldValue").ToString()
                            _UpsTrack.UPSAccount.AccountNumber = dr("vcShipFieldValue").ToString()
                            Exit Select
                    End Select
                Next
            Catch ex As Exception
                Throw
            End Try
        End Sub

#End Region

#Region "Grid Events"

        Private Sub gvShipping_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvShipping.RowDataBound
            Try
                If e.Row.RowType = DataControlRowType.DataRow Then

                    Dim hpl As HyperLink
                    hpl = e.Row.FindControl("hplBizdcoc")
                    Dim lblBizDocID As Label
                    lblBizDocID = e.Row.FindControl("lblBizDocID")
                    hpl.Attributes.Add("onclick", "return OpenBizInvoice('" & GetQueryStringVal("OpID") & "','" & lblBizDocID.Text & "',0)")

                    Dim hdnShipCompany As HiddenField
                    hdnShipCompany = e.Row.FindControl("hdnShipCompany")
                    'Lead Tracking URL 
                    Dim objItem As New CItems
                    Dim dsShippingDtl As DataSet
                    objItem.DomainID = Session("DomainID")
                    objItem.ShippingCMPID = CCommon.ToLong(hdnShipCompany.Value)
                    dsShippingDtl = objItem.ShippingDtls()
                    For Each drow As DataRow In dsShippingDtl.Tables(0).Rows
                        Select Case drow("vcFieldName").ToString()
                            Case "Tracking URL"
                                ViewState("TrackingURL") = drow("vcShipFieldValue").ToString()
                        End Select
                    Next

                    Dim hplTrackingNums As HyperLink
                    hplTrackingNums = e.Row.FindControl("hplTrackingNums")
                    hplTrackingNums.NavigateUrl = String.Format(CCommon.ToString(ViewState("TrackingURL")), DataBinder.Eval(e.Row.DataItem, "vcTrackingNumber"))

                    Dim lblBoxTip As Label
                    lblBoxTip = e.Row.FindControl("lblBoxTip")
                    If lblBoxTip IsNot Nothing Then

                        Dim strToolTipCollection() As String = CCommon.ToString(DataBinder.Eval(e.Row.DataItem, "WeightPerBox")).Split("~")
                        If strToolTipCollection.Length > 0 Then
                            For Each strToolTip As String In strToolTipCollection
                                lblBoxTip.ToolTip = lblBoxTip.ToolTip & strToolTip & "<br/>"
                            Next

                        End If

                    End If

                    Dim lblClonedBizdocs As Label
                    lblClonedBizdocs = e.Row.FindControl("lblClonedBizdocs")
                    If lblClonedBizdocs IsNot Nothing Then
                        Dim strBizdocs() As String = CCommon.ToString(DataBinder.Eval(e.Row.DataItem, "vcBizDocs")).Split(",")
                        Dim strBizDocLink As String = ""
                        Dim strBizDocID() As String = {""}

                        For Each strBizDocName As String In strBizdocs
                            strBizDocID = strBizDocName.Split("~")
                            If strBizDocID.Length > 0 Then
                                strBizDocLink = strBizDocLink & ", <a href=""#"" onclick=""OpenBizInvoice('" & GetQueryStringVal("OpID") & "','',0)"" >" & CCommon.ToString(strBizDocID(0)) & "</a>"
                            ElseIf strBizDocID.Length > 1 Then
                                strBizDocLink = strBizDocLink & ", <a href=""#"" onclick=""OpenBizInvoice('" & GetQueryStringVal("OpID") & "','" & CCommon.ToString(strBizDocID(1)) & "',0)"" >" & CCommon.ToString(strBizDocID(0)) & "</a>"
                            Else
                                Continue For
                            End If

                        Next

                        strBizDocLink = strBizDocLink.Trim(",").Trim()
                        lblClonedBizdocs.Text = strBizDocLink
                    End If

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

#End Region

#End Region

        'Added By :Sachin sadhu ||Date:18thJul2014
        'Purpose :To Save Custom Fields
        Sub SaveCusFieldItems()
            Try
                For Each dgItem As GridViewRow In gvProduct.Rows
                    Dim ObjCus As New CustomFields
                    ObjCus.locId = 18
                    ObjCus.RecordId = CCommon.ToLong(CType(dgItem.FindControl("hfOppItemtCode"), HiddenField).Value)
                    ObjCus.DomainID = Session("DomainID")
                    Dim dtTable1 As DataTable = ObjCus.GetCustFlds.Tables(0)

                    If Not dtTable1 Is Nothing Then
                        Dim i As Integer
                        Dim dtTable As DataTable = dtTable1

                        Dim listDeleteRows As New System.Collections.Generic.List(Of DataRow)

                        For Each dr As DataRow In dtTable.Rows
                            If Not dgItem.FindControl("rdpCustomDateColumn" & dr("fld_id") & ObjCus.RecordId) Is Nothing Or Not dgItem.FindControl(dr("fld_id")) Is Nothing Then
                                If dr("fld_type") = "SelectBox" Then
                                    Dim ddl As DropDownList
                                    ddl = CType(dgItem.FindControl(dr("fld_id")), DropDownList)
                                    If ddl IsNot Nothing Then
                                        dr("Value") = CStr(ddl.SelectedValue)
                                    End If

                                ElseIf dr("fld_type") = "TextBox" Then
                                    Dim txt As TextBox
                                    txt = CType(dgItem.FindControl(dr("fld_id")), TextBox)
                                    If txt IsNot Nothing AndAlso txt.Text <> "-" Then
                                        dr("Value") = CStr(txt.Text)
                                    End If
                                ElseIf dr("fld_type") = "TextArea" Then
                                    Dim txtArea As TextBox
                                    txtArea = CType(dgItem.FindControl(dr("fld_id")), TextBox)
                                    If txtArea IsNot Nothing Then
                                        dr("Value") = CStr(txtArea.Text)
                                    End If
                                ElseIf dr("fld_type") = "CheckBox" Then
                                    Dim chk As CheckBox
                                    chk = CType(dgItem.FindControl(dr("fld_id")), CheckBox)
                                    If chk IsNot Nothing Then
                                        If chk.Checked = True Then
                                            dr("Value") = "1"
                                        Else : dr("Value") = "0"
                                        End If
                                    End If
                                ElseIf dr("fld_type") = "DateField" Then
                                    Dim dtPicker As RadDatePicker
                                    dtPicker = CType(dgItem.FindControl("rdpCustomDateColumn" & dr("fld_id") & ObjCus.RecordId), RadDatePicker)
                                    If dtPicker IsNot Nothing Then
                                        dr("Value") = CDate(dtPicker.SelectedDate)
                                    End If
                                End If
                            Else
                                listDeleteRows.Add(dr)
                            End If
                        Next

                        For Each dr As DataRow In listDeleteRows
                            dtTable.Rows.Remove(dr)
                        Next

                        Dim ds As New DataSet
                        Dim strdetails As String
                        dtTable.TableName = "Table"
                        ds.Tables.Add(dtTable.Copy)
                        strdetails = ds.GetXml
                        ds.Tables.Remove(ds.Tables(0))

                        Dim ObjCusfld As New CustomFields
                        ObjCusfld.strDetails = strdetails
                        ObjCusfld.locId = 18
                        ObjCusfld.RecordId = CCommon.ToLong(CType(dgItem.FindControl("hfOppItemtCode"), HiddenField).Value)
                        ObjCusfld.SaveCustomFldsByRecId()
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnApproveUnitPrice_Click(sender As Object, e As EventArgs) Handles btnApproveUnitPrice.Click
            Try
                Dim objOpportunity As New MOpportunity
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.OpportunityId = lngOppId
                objOpportunity.ApproveOpportunityItemsPrice()

                For Each dr As DataRow In dsTemp.Tables(0).Rows
                    dr("bitItemPriceApprovalRequired") = False
                Next

                BindItems(dsTemp)
                btnApproveUnitPrice.Visible = False
                Response.Redirect(Request.Url.AbsoluteUri)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        'end of Code

        Private Sub btnDeleteRecurrence_Click(sender As Object, e As EventArgs) Handles btnDeleteRecurrence.Click
            Try
                Dim objBizRecurrence As New BizRecurrence
                objBizRecurrence.Delete(CCommon.ToLong(hdnRecConfigID.Value), CCommon.ToLong(Session("DomainID")), CCommon.ToLong(Session("UserContactID")))
                LoadSavedInformation(boolLoadOppInfo:=True)
            Catch ex As Exception
                If ex.Message = "RECURRENCE_TRANSACTIOS_EXISTS" Then
                    ShowMessage("You have to first disable recurrence and then have to delete all recurred order for deleting recurrence. Go to recurrence detail page and delete recurred orders one by one.")
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(ex.Message)
                End If
            End Try
        End Sub

        Public Sub CreateInvoiceAndFulfillmentOrder()
            Try
                Dim objOpportunity As New MOpportunity
                objOpportunity.OpportunityId = lngOppId
                Dim dt As DataSet = objOpportunity.CheckOrderedAndInvoicedOrBilledQty()

                Dim bitOrderedAndInvoicedBilledQtyNotSame As Boolean = CCommon.ToBool(dt.Tables(0).Select("ID=2")(0)("bitTrue"))
                Dim bitOrderedAndFulfilledQtyNotSame As Boolean = CCommon.ToBool(dt.Tables(0).Select("ID=3")(0)("bitTrue"))

                'CREATE INVOICE FOR REMAINING QTY WHICH NOT YET INVOICED
                If bitOrderedAndInvoicedBilledQtyNotSame Then
                    Try
                        CreateInvoice()
                    Catch ex As Exception
                        If ex.Message = "NOT_ALLOWED" Then
                            ShowMessage("Not able to create invoice. To split order items multiple bizdocs you must create all bizdocs with ""partial fulfilment"" checked!")
                        ElseIf ex.Message = "NOT_ALLOWED_FulfillmentOrder" Then
                            ShowMessage("Not able to create invoice. Only a Sales Order can create a Fulfillment Order")
                        ElseIf ex.Message = "FY_CLOSED" Then
                            ShowMessage("Not able to create invoice. This transaction can not be posted,Because transactions date belongs to closed financial year.")
                        ElseIf ex.Message = "AlreadyInvoice" Then
                            ShowMessage("Not able to create invoice. You can�t create a Invoice against a Fulfillment Order that already contains a Invoice")
                        ElseIf ex.Message = "AlreadyPackingSlip" Then
                            ShowMessage("Not able to create invoice. You can�t create a Packing-Slip against a Fulfillment Order that already contains a Packing-Slip")
                        ElseIf ex.Message = "AlreadyInvoice_NOT_ALLOWED_FulfillmentOrder" Then
                            ShowMessage("Not able to create invoice. You can�t create a fulfillment order against a sales order that already contains an Invoice or Packing-Slip")
                        ElseIf ex.Message = "AlreadyFulfillmentOrder" Then
                            ShowMessage("Not able to create invoice. Manually adding Invoices or Packing Slips to a Sales Order is not allowed after a Fulfillment Order (FO) is added. Your options are to edit the FO status to trigger creation of Invoices and/or Packing Slips, Delete the Fulfillment Order, or Create Invoices and/or Packing Slips from the Sales Fulfillment section")
                        Else
                            ShowMessage("Not able to create invoice. Unknown error ocurred.")
                            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                        End If

                        Exit Sub
                    End Try
                End If

                'CREATE FULFILLMENT BIZ DOC OF REMAINING QTY AND MARK IF AS SHIPPED (RELEASE FROM ALLOCATION)
                If bitOrderedAndFulfilledQtyNotSame Then
                    Try
                        CreateFulfillmentBizDocAndShip()
                    Catch ex As Exception
                        If ex.Message.Contains("WORK_ORDER_NOT_COMPLETED") Then
                            ShowMessage("Not able to add fulfillment bizdoc because some work orders are still not completed.")
                        ElseIf ex.Message.Contains("QTY_MISMATCH") Then
                            ShowMessage("Not able to add fulfillment bizdoc because qty is not same as required qty.<br />")
                        ElseIf ex.Message.Contains("REQUIRED_SERIALS_NOT_PROVIDED") Then
                            ShowMessage("Not able to add fulfillment bizdoc because required number of serials are not provided.<br />")
                        ElseIf ex.Message.Contains("INVALID_SERIAL_NUMBERS") Then
                            ShowMessage("Not able to add fulfillment bizdoc because invalid serials are provided.<br />")
                        ElseIf ex.Message.Contains("REQUIRED_LOTNO_NOT_PROVIDED") Then
                            ShowMessage("Not able to add fulfillment bizdoc because required number of Lots are not provided.<br />")
                        ElseIf ex.Message.Contains("INVALID_LOT_NUMBERS") Then
                            ShowMessage("Not able to add fulfillment bizdoc because invalid Lots are provided.<br />")
                        ElseIf ex.Message.Contains("SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY") Then
                            ShowMessage("Not able to add fulfillment bizdoc because Lot number do not have enough qty to fulfill.<br />")
                        ElseIf ex.Message.Contains("NOTSUFFICIENTQTY_ALLOCATION") Then
                            ShowMessage("Not able to add fulfillment bizdoc because warehouse allocation is invlid.<br />")
                        ElseIf ex.Message.Contains("NOTSUFFICIENTQTY_ONHAND") Then
                            ShowMessage("Not able to add fulfillment bizdoc because warehouse does not have enought on hand quantity.<br />")
                        Else
                            ShowMessage("Not able to add fulfillment order due to unknown error.<br />")
                            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                        End If
                        Exit Sub
                    End Try
                End If

                btnReceivedOrShipped_Click(Nothing, Nothing)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub CreateBill()
            Try
                Dim OppBizDocID, lngDivId, JournalId As Long
                Dim lngCntID As Long = 0

                Using objTransaction As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})

                    If CCommon.ToBool(Session("IsMinUnitPriceRule")) Then
                        If CheckIfApprovalRequired() Then
                            ShowMessage("Not able to create bill because approval of unit price(s) is required.")
                            Exit Sub
                        End If
                    End If

                    Dim dtDetails, dtBillingTerms As DataTable

                    Dim objPageLayout As New CPageLayout
                    objPageLayout.OpportunityId = lngOppId
                    objPageLayout.DomainID = Session("DomainID")
                    dtDetails = objPageLayout.OpportunityDetails.Tables(0)
                    lngDivId = dtDetails.Rows(0).Item("numDivisionID")
                    lngCntID = dtDetails.Rows(0).Item("numContactID")


                    Dim objOppBizDocs As New OppBizDocs
                    objOppBizDocs.UserCntID = Session("UserContactID")
                    objOppBizDocs.DomainID = Session("DomainID")

                    If objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), lngDivId) = 0 Then
                        ShowMessage("Not able to create bill. Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip""")
                        Exit Sub
                    End If

                    If ChartOfAccounting.GetDefaultAccount("CG", Session("DomainID")) = 0 Then
                        ShowMessage("Not able to create bill. Please Set Default COGs account from ""Administration->Global Settings->Accounting->Default Accounts""")
                        Exit Sub
                    End If

                    If ChartOfAccounting.GetDefaultAccount("PC", Session("DomainID")) = 0 Then
                        ShowMessage("Not able to create bill. Please Set Default Purchase Clearing account from ""Administration->Global Settings->Accounting->Default Accounts""")
                        Exit Sub
                    End If

                    If ChartOfAccounting.GetDefaultAccount("PV", Session("DomainID")) = 0 Then
                        ShowMessage("Not able to create bill. Please Set Default Purchase Price Variance account from ""Administration->Global Settings->Accounting->Default Accounts""")
                        Exit Sub
                    End If

                    objOppBizDocs.OppId = lngOppId
                    objOppBizDocs.OppType = OppType.Value
                    objOppBizDocs.FromDate = DateTime.UtcNow
                    objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")


                    'SETS BIZDOC ID
                    '--------------
                    Dim lintAuthorizativeBizDocsId As Long
                    lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()
                    objOppBizDocs.BizDocId = lintAuthorizativeBizDocsId
                    '--------------

                    'SETS BIZDOC TEMPLETE NEEDS TO BE USED
                    '-------------------------------------
                    objOppBizDocs.byteMode = 1
                    Dim dtDefaultBizDocTemplate As DataTable = objOppBizDocs.GetBizDocTemplateList()
                    objOppBizDocs.BizDocTemplateID = CCommon.ToLong(dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID"))
                    '-------------------------------------

                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.Mode = 33
                    objCommon.Str = lintAuthorizativeBizDocsId 'DEFUALT BIZ DOC ID FOR BILL
                    objOppBizDocs.SequenceId = objCommon.GetSingleFieldValue()

                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.Mode = 34
                    objCommon.Str = lngOppId
                    objOppBizDocs.RefOrderNo = objCommon.GetSingleFieldValue()
                    objOppBizDocs.bitPartialShipment = True

                    OppBizDocID = objOppBizDocs.SaveBizDoc()
                    If OppBizDocID = 0 Then
                        ShowMessage("Not able to create bill. A BizDoc by the same name is already created. Please select another BizDoc !")
                        Exit Sub
                    End If

                    'CREATE JOURNALS
                    '---------------
                    Dim ds As New DataSet
                    Dim dtOppBiDocItems As DataTable

                    objOppBizDocs.OppBizDocId = OppBizDocID
                    ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting

                    dtOppBiDocItems = ds.Tables(0)

                    Dim objCalculateDealAmount As New CalculateDealAmount
                    objCalculateDealAmount.CalculateDealAmount(lngOppId, OppBizDocID, OppType.Value, Session("DomainID"), dtOppBiDocItems)

                    JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, objOppBizDocs.FromDate, Description:=ds.Tables(1).Rows(0).Item("vcBizDocID"))

                    Dim objJournalEntries As New JournalEntry
                    If CCommon.ToBool(ds.Tables(1).Rows(0).Item("bitPPVariance")) Then
                        objJournalEntries.SaveJournalEntriesPurchaseVariance(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), ds.Tables(1).Rows(0).Item("fltExchangeRateBizDoc"), vcBaseCurrency:=ds.Tables(1).Rows(0).Item("vcBaseCurrency"), vcForeignCurrency:=ds.Tables(1).Rows(0).Item("vcForeignCurrency"))
                    Else
                        objJournalEntries.SaveJournalEntriesPurchase(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"))
                    End If
                    '---------------

                    '------ SEND BIZDOC ALERTS - NOTIFY RECORD OWNERS, THEIR SUPERVISORS, AND YOUR TRADING PARTNERS WHEN A BIZDOC IS CREATED, MODIFIED, OR APPROVED.
                    Dim objAlert As New CAlerts
                    objAlert.SendBizDocAlerts(lngOppId, OppBizDocID, lintAuthorizativeBizDocsId, Session("DomainID"), CAlerts.enmBizDoc.IsCreated)

                    Dim objAutomatonRule As New AutomatonRule
                    objAutomatonRule.ExecuteAutomationRule(49, OppBizDocID, 1)

                    objTransaction.Complete()
                End Using

                ''Added By Sachin Sadhu||Date:1stMay2014
                ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                ''          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = OppBizDocID
                objWfA.SaveWFBizDocQueue()
                'end of code

                btnReceivedOrShipped_Click(Nothing, Nothing)
            Catch ex As Exception
                If ex.Message = "NOT_ALLOWED" Then
                    ShowMessage("To split order items multiple bizdocs you must create all bizdocs with ""partial fulfilment"" checked!")
                ElseIf ex.Message = "NOT_ALLOWED_FulfillmentOrder" Then
                    ShowMessage("Only a Sales Order can create a Fulfillment Order")
                ElseIf ex.Message = "FY_CLOSED" Then
                    ShowMessage("This transaction can not be posted,Because transactions date belongs to closed financial year.")
                ElseIf ex.Message = "AlreadyInvoice" Then
                    ShowMessage("You can�t create a Invoice against a Fulfillment Order that already contains a Invoice")
                ElseIf ex.Message = "AlreadyPackingSlip" Then
                    ShowMessage("You can�t create a Packing-Slip against a Fulfillment Order that already contains a Packing-Slip")
                ElseIf ex.Message = "AlreadyInvoice_NOT_ALLOWED_FulfillmentOrder" Then
                    ShowMessage("You can�t create a fulfillment order against a sales order that already contains an Invoice or Packing-Slip")
                ElseIf ex.Message = "AlreadyFulfillmentOrder" Then
                    ShowMessage("Manually adding Invoices or Packing Slips to a Sales Order is not allowed after a Fulfillment Order (FO) is added. Your options are to edit the FO status to trigger creation of Invoices and/or Packing Slips, Delete the Fulfillment Order, or Create Invoices and/or Packing Slips from the Sales Fulfillment section")
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(ex.Message)
                End If
            End Try
        End Sub

        Private Sub CreateInvoice()
            Try
                Dim OppBizDocID, lngDivId, JournalId As Long
                Dim lngCntID As Long = 0

                Using objTransaction As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})

                    If CCommon.ToBool(Session("IsMinUnitPriceRule")) Then
                        If CheckIfApprovalRequired() Then
                            ShowMessage("Not able to create invoice because approval of unit price(s) is required.")
                            Exit Sub
                        End If
                    End If

                    Dim dtDetails, dtBillingTerms As DataTable

                    Dim objPageLayout As New CPageLayout
                    objPageLayout.OpportunityId = lngOppId
                    objPageLayout.DomainID = Session("DomainID")
                    dtDetails = objPageLayout.OpportunityDetails.Tables(0)
                    lngDivId = dtDetails.Rows(0).Item("numDivisionID")
                    lngCntID = dtDetails.Rows(0).Item("numContactID")


                    Dim objOppBizDocs As New OppBizDocs
                    objOppBizDocs.UserCntID = Session("UserContactID")
                    objOppBizDocs.DomainID = Session("DomainID")

                    If objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), lngDivId) = 0 Then
                        ShowMessage("Not able to create invoice. Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip""")
                        Exit Sub
                    End If

                    objOppBizDocs.OppId = lngOppId
                    objOppBizDocs.OppType = OppType.Value
                    objOppBizDocs.FromDate = DateTime.UtcNow
                    objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")


                    'SETS BIZDOC ID
                    '--------------
                    Dim lintAuthorizativeBizDocsId As Long
                    lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()
                    objOppBizDocs.BizDocId = lintAuthorizativeBizDocsId
                    '--------------

                    'SETS BIZDOC TEMPLETE NEEDS TO BE USED
                    '-------------------------------------
                    objOppBizDocs.byteMode = 1
                    Dim dtDefaultBizDocTemplate As DataTable = objOppBizDocs.GetBizDocTemplateList()
                    objOppBizDocs.BizDocTemplateID = CCommon.ToLong(dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID"))
                    '-------------------------------------

                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.Mode = 33
                    objCommon.Str = lintAuthorizativeBizDocsId 'DEFUALT BIZ DOC ID FOR BILL
                    objOppBizDocs.SequenceId = objCommon.GetSingleFieldValue()

                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.Mode = 34
                    objCommon.Str = lngOppId
                    objOppBizDocs.RefOrderNo = objCommon.GetSingleFieldValue()
                    objOppBizDocs.bitPartialShipment = True

                    OppBizDocID = objOppBizDocs.SaveBizDoc()
                    If OppBizDocID = 0 Then
                        ShowMessage("Not able to create invoice. A BizDoc by the same name is already created. Please select another BizDoc !")
                        Exit Sub
                    End If

                    'CREATE JOURNALS
                    '---------------
                    Dim ds As New DataSet
                    Dim dtOppBiDocItems As DataTable

                    objOppBizDocs.OppBizDocId = OppBizDocID
                    ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting

                    dtOppBiDocItems = ds.Tables(0)

                    Dim objCalculateDealAmount As New CalculateDealAmount
                    objCalculateDealAmount.CalculateDealAmount(lngOppId, OppBizDocID, OppType.Value, Session("DomainID"), dtOppBiDocItems)

                    JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, objOppBizDocs.FromDate, Description:=ds.Tables(1).Rows(0).Item("vcBizDocID"))

                    Dim objJournalEntries As New JournalEntry
                    If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                        objJournalEntries.SaveJournalEntriesSales(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), 0)
                    Else
                        objJournalEntries.SaveJournalEntriesSalesNew(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, OppBizDocID, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), 0)
                    End If

                    If Session("AutolinkUnappliedPayment") Then
                        Dim objSalesFulfillment As New SalesFulfillmentWorkflow
                        objSalesFulfillment.DomainID = Session("DomainID")
                        objSalesFulfillment.UserCntID = Session("UserContactID")
                        objSalesFulfillment.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                        objSalesFulfillment.OppId = lngOppId
                        objSalesFulfillment.OppBizDocId = OppBizDocID

                        objSalesFulfillment.AutoLinkUnappliedPayment()
                    End If
                    '---------------

                    '------ SEND BIZDOC ALERTS - NOTIFY RECORD OWNERS, THEIR SUPERVISORS, AND YOUR TRADING PARTNERS WHEN A BIZDOC IS CREATED, MODIFIED, OR APPROVED.
                    Dim objAlert As New CAlerts
                    objAlert.SendBizDocAlerts(lngOppId, OppBizDocID, lintAuthorizativeBizDocsId, Session("DomainID"), CAlerts.enmBizDoc.IsCreated)

                    Dim objAutomatonRule As New AutomatonRule
                    objAutomatonRule.ExecuteAutomationRule(49, OppBizDocID, 1)

                    objTransaction.Complete()
                End Using

                ''Added By Sachin Sadhu||Date:1stMay2014
                ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                ''          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = OppBizDocID
                objWfA.SaveWFBizDocQueue()
                'end of code
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub CreateFulfillmentBizDocAndShip()
            Try
                Dim lngCntID As Long = 0
                Dim OppBizDocID, lngDivId, JournalId As Long

                Using objTransaction As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})

                    If CCommon.ToBool(Session("IsMinUnitPriceRule")) Then
                        If CheckIfApprovalRequired() Then
                            ShowMessage("Not able to create fulfillment bizdoc because approval of unit price(s) is required.")
                            Exit Sub
                        End If
                    End If

                    Dim dtDetails, dtBillingTerms As DataTable

                    Dim objPageLayout As New CPageLayout
                    objPageLayout.OpportunityId = lngOppId
                    objPageLayout.DomainID = Session("DomainID")
                    dtDetails = objPageLayout.OpportunityDetails.Tables(0)
                    lngDivId = dtDetails.Rows(0).Item("numDivisionID")
                    lngCntID = dtDetails.Rows(0).Item("numContactID")


                    Dim objOppBizDocs As New OppBizDocs
                    objOppBizDocs.UserCntID = Session("UserContactID")
                    objOppBizDocs.DomainID = Session("DomainID")

                    If objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), lngDivId) = 0 Then
                        ShowMessage("Not able to fulfillment bizdoc. Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip""")
                        Exit Sub
                    End If

                    objOppBizDocs.OppId = lngOppId
                    objOppBizDocs.OppType = OppType.Value
                    objOppBizDocs.FromDate = DateTime.UtcNow
                    objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")


                    'SETS BIZDOC ID
                    '--------------
                    objOppBizDocs.BizDocId = 296 'Fulfillment Bizdoc
                    '--------------

                    'SETS BIZDOC TEMPLETE NEEDS TO BE USED
                    '-------------------------------------
                    objOppBizDocs.byteMode = 1
                    Dim dtDefaultBizDocTemplate As DataTable = objOppBizDocs.GetBizDocTemplateList()
                    objOppBizDocs.BizDocTemplateID = CCommon.ToLong(dtDefaultBizDocTemplate.Rows(0).Item("numBizDocTempID"))
                    '-------------------------------------

                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.Mode = 33
                    objCommon.Str = objOppBizDocs.BizDocId 'DEFUALT BIZ DOC ID
                    objOppBizDocs.SequenceId = objCommon.GetSingleFieldValue()

                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.Mode = 34
                    objCommon.Str = lngOppId
                    objOppBizDocs.RefOrderNo = objCommon.GetSingleFieldValue()
                    objOppBizDocs.bitPartialShipment = True

                    OppBizDocID = objOppBizDocs.SaveBizDoc()

                    If OppBizDocID = 0 Then
                        ShowMessage("Not able to create fulfillment bizdoc. A BizDoc by the same name is already created. Please select another BizDoc !")
                        Exit Sub
                    End If

                    'AUTO FULFILLS bizdoc AND MAKED QTY AS SHIPPED AND RELEASES IT FROM ALLOCATION
                    Dim objOppBizDoc As New OppBizDocs
                    objOppBizDoc.DomainID = CCommon.ToLong(Session("DomainID"))
                    objOppBizDoc.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objOppBizDoc.OppId = lngOppId
                    objOppBizDoc.OppBizDocId = OppBizDocID
                    objOppBizDoc.AutoFulfillBizDoc()

                    'IMPORTANT: WE ARE MAKING ACCOUNTING CHANGES BECAUSE WE ARE CREATING FULFILLMENT ORDER IN BACKGROUP AND MARKIGN IT AS SHIPPED
                    '           OTHERWISE ACCOUNTING IS ONLY IMPACTED WHEN USER SHIPPED IT FROM SALES FULFILLMENT PAGE
                    'CREATE SALES CLEARING ENTERIES
                    'CREATE JOURNALS ONLY FOR FILFILLMENT BIZDOCS
                    '-------------------------------------------------
                    Dim ds As New DataSet
                    Dim dtOppBiDocItems As DataTable

                    objOppBizDocs.OppBizDocId = OppBizDocID
                    ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                    dtOppBiDocItems = ds.Tables(0)

                    Dim objCalculateDealAmount As New CalculateDealAmount
                    objCalculateDealAmount.CalculateDealAmount(lngOppId, OppBizDocID, 1, CCommon.ToLong(Session("DomainID")), dtOppBiDocItems)

                    JournalId = objOppBizDocs.GetJournalIdForAuthorizativeBizDocs

                    'WHEN FROM COA SOMEONE DELETED JOURNALS THEN WE NEED TO CREATE NEW JOURNAL HEADER ENTRY
                    JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, Entry_Date:=Date.UtcNow.Date, Description:=CStr(ds.Tables(1).Rows(0).Item("vcBizDocID")))

                    Dim objJournalEntries As New JournalEntry
                    If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                        objJournalEntries.SaveJournalEnteriesSalesClearing(lngOppId, CCommon.ToLong(Session("DomainID")), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                    Else
                        objJournalEntries.SaveJournalEnteriesSalesClearingNew(lngOppId, CCommon.ToLong(Session("DomainID")), dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                    End If
                    '---------------------------------------------------------------------------------


                    '------ SEND BIZDOC ALERTS - NOTIFY RECORD OWNERS, THEIR SUPERVISORS, AND YOUR TRADING PARTNERS WHEN A BIZDOC IS CREATED, MODIFIED, OR APPROVED.
                    Dim objAlert As New CAlerts
                    objAlert.SendBizDocAlerts(lngOppId, OppBizDocID, objOppBizDocs.BizDocId, Session("DomainID"), CAlerts.enmBizDoc.IsCreated)

                    Dim objAutomatonRule As New AutomatonRule
                    objAutomatonRule.ExecuteAutomationRule(49, OppBizDocID, 1)

                    objTransaction.Complete()
                End Using

                ''Added By Sachin Sadhu||Date:1stMay2014
                ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                ''          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = OppBizDocID
                objWfA.SaveWFBizDocQueue()
                'end of code
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Function CheckIfApprovalRequired() As Boolean
            Try
                Dim objOpportunity As New MOpportunity
                objOpportunity.OpportunityId = lngOppId
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.UserCntID = Session("UserContactID")
                objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                Dim dsTemp As DataSet = objOpportunity.GetOrderItemsProductServiceGrid

                If Not dsTemp Is Nothing AndAlso dsTemp.Tables.Count > 0 AndAlso dsTemp.Tables(0).Rows.Count > 0 Then
                    Dim results = From myRow In dsTemp.Tables(0).AsEnumerable()
                                  Where CCommon.ToBool(myRow.Field(Of Boolean)("bitItemPriceApprovalRequired")) = True
                                  Select myRow
                    If results.Count > 0 Then
                        Return True
                    Else
                        Return False
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Protected Sub txtUnitsGrid_TextChanged(sender As Object, e As EventArgs)
            Try
                Dim dr As DataRow
                Dim dtItem As DataTable
                Dim txtUnits As TextBox = CType(sender, TextBox)
                Dim row As GridViewRow = DirectCast(txtUnits.NamingContainer, GridViewRow)
                dtItem = dsTemp.Tables(0)

                Dim dRows As DataRow() = dtItem.Select("numoppitemtCode = " & CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode")).ToString())
                If dRows.Length > 0 Then
                    dr = dRows(0)
                    dr("numUnitHour") = CCommon.ToDecimal(txtUnits.Text)
                    UpdateValues()

                    Dim price As Decimal = CCommon.ToDecimal(dr("monPrice"))
                    Dim discount As Decimal = CCommon.ToDecimal(dr("fltDiscount"))
                    Dim salePrice As Decimal = price

                    Dim txtmonPriceUOM As TextBox = DirectCast(row.FindControl("txtmonPriceUOM"), TextBox)
                    If Not txtmonPriceUOM Is Nothing Then
                        CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(price * dr("UOMConversionFactor")), txtmonPriceUOM)
                    End If

                    Dim txtDiscount As TextBox = DirectCast(row.FindControl("txtDiscount"), TextBox)
                    If Not txtDiscount Is Nothing Then
                        CCommon.SetValueInDecimalFormat(discount, txtDiscount)
                    End If

                    If Not CCommon.ToBool(dr("bitDiscountType")) Then
                        If (dr("bitMarkupDiscount").ToString() = "1") Then
                            salePrice = price + (price * (discount / 100))
                        Else
                            salePrice = price - (price * (discount / 100))
                        End If
                    Else
                        If CCommon.ToDecimal(txtUnits.Text) > 0 Then
                            If (dr("bitMarkupDiscount").ToString() = "1") Then ' Markup N Discount logic added
                                salePrice = ((price * (CCommon.ToDecimal(txtUnits.Text) * dr("UOMConversionFactor"))) + discount) / (CCommon.ToDecimal(txtUnits.Text) * dr("UOMConversionFactor"))
                            Else
                                salePrice = ((price * (CCommon.ToDecimal(txtUnits.Text) * dr("UOMConversionFactor"))) - discount) / (CCommon.ToDecimal(txtUnits.Text) * dr("UOMConversionFactor"))
                            End If
                        Else
                            salePrice = 0
                        End If
                    End If


                    Dim lblmonTotAmount As Label = DirectCast(row.FindControl("lblmonTotAmount"), Label)
                    If Not lblmonTotAmount Is Nothing Then
                        lblmonTotAmount.Text = CCommon.GetDecimalFormat(CCommon.ToDouble((CCommon.ToDecimal(txtUnits.Text) * dr("UOMConversionFactor")) * salePrice))
                    End If

                    dr("monPrice") = price
                    dr("fltDiscount") = discount
                    dr("monPriceUOM") = price * dr("UOMConversionFactor")
                    dr("monTotAmount") = (dr("numUnitHour") * dr("UOMConversionFactor")) * salePrice
                    dr("monTotAmtBefDiscount") = (dr("numUnitHour") * dr("UOMConversionFactor")) * price
                    dr.AcceptChanges()

                    If OppType.Value = "1" Then
                        RecalculateContainerQty(dtItem)
                    End If

                    dsTemp.AcceptChanges()

                    BindItems(dsTemp)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub rdpItemRequiredDate_SelectedDateChanged(ByVal sender As Object, ByVal e As Telerik.Web.UI.Calendar.SelectedDateChangedEventArgs)
            Try
                Dim rdpItemRequiredDate As RadDatePicker = CType(sender, RadDatePicker)
                Dim row As GridViewRow = DirectCast(rdpItemRequiredDate.NamingContainer, GridViewRow)
                Dim dtItem As DataTable = dsTemp.Tables(0)

                Dim dRows As DataRow() = dtItem.Select("numoppitemtCode = " & CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode")).ToString())
                If dRows.Length > 0 Then
                    Dim dr As DataRow = dRows(0)
                    UpdateValues()
                    dr("ItemRequiredDate") = rdpItemRequiredDate.SelectedDate
                    dr.AcceptChanges()
                    dsTemp.AcceptChanges()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub ddlUOM_SelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                Dim dr As DataRow
                Dim dtItem As DataTable
                Dim ddlUOM As DropDownList = CType(sender, DropDownList)
                Dim row As GridViewRow = DirectCast(ddlUOM.NamingContainer, GridViewRow)
                dtItem = dsTemp.Tables(0)

                Dim dRows As DataRow() = dtItem.Select("numoppitemtCode = " & CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode")).ToString())
                If dRows.Length > 0 Then
                    dr = dRows(0)
                    UpdateValues()
                    Dim objOpportunity As New COpportunities
                    objOpportunity.DomainID = Session("DomainID")

                    objOpportunity.DivisionID = CCommon.ToLong(DivID.Value)
                    objOpportunity.OppType = OppType.Value
                    objOpportunity.ItemCode = CCommon.ToLong(dr("numItemCode"))
                    objOpportunity.UnitHour = CCommon.ToDecimal(dr("numUnitHour"))
                    objOpportunity.WarehouseItmsID = CCommon.ToLong(dr("numWarehouseItmsID"))
                    Dim ds As DataSet = objOpportunity.GetOrderItemDetails(CCommon.ToLong(ddlUOM.SelectedValue), "", CCommon.ToLong(hdnCountry.Value))

                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                        Dim price As Decimal = CCommon.ToDecimal(dr("monPrice"))
                        Dim discount As Decimal = CCommon.ToDecimal(dr("fltDiscount"))
                        Dim salePrice As Decimal = price
                        Dim uomConversionFactor As Decimal = CCommon.ToDecimal(ds.Tables(0).Rows(0)("UOMConversionFactor"))

                        Dim txtmonPriceUOM As TextBox = DirectCast(row.FindControl("txtmonPriceUOM"), TextBox)
                        If Not txtmonPriceUOM Is Nothing Then
                            CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(price * uomConversionFactor), txtmonPriceUOM)
                        End If

                        Dim txtDiscount As TextBox = DirectCast(row.FindControl("txtDiscount"), TextBox)
                        If Not txtDiscount Is Nothing Then
                            RemoveHandler txtDiscount.TextChanged, AddressOf txtDiscount_TextChanged
                            CCommon.SetValueInDecimalFormat(discount, txtDiscount)
                            AddHandler txtDiscount.TextChanged, AddressOf txtDiscount_TextChanged
                        End If

                        If Not CCommon.ToBool(dr("bitDiscountType")) Then
                            If (dr("bitMarkupDiscount").ToString() = "1") Then ' Markup N Discount logic added
                                salePrice = price + (price * (discount / 100))
                            Else
                                salePrice = price - (price * (discount / 100))
                            End If
                        Else
                            If CCommon.ToDecimal(dr("numUnitHour")) > 0 Then
                                If (dr("bitMarkupDiscount").ToString() = "1") Then ' Markup N Discount logic added
                                    salePrice = ((price * (CCommon.ToDecimal(dr("numUnitHour")) * uomConversionFactor)) + discount) / (CCommon.ToDecimal(dr("numUnitHour")) * uomConversionFactor)
                                Else
                                    salePrice = ((price * (CCommon.ToDecimal(dr("numUnitHour")) * uomConversionFactor)) - discount) / (CCommon.ToDecimal(dr("numUnitHour")) * uomConversionFactor)
                                End If
                            Else
                                salePrice = 0
                            End If
                        End If

                        Dim lblmonTotAmount As Label = DirectCast(row.FindControl("lblmonTotAmount"), Label)
                        If Not lblmonTotAmount Is Nothing Then
                            lblmonTotAmount.Text = CCommon.GetDecimalFormat(CCommon.ToDouble((CCommon.ToDecimal(dr("numUnitHour")) * uomConversionFactor) * salePrice))
                        End If

                        dr("monPrice") = price
                        dr("fltDiscount") = discount
                        dr("monPriceUOM") = price * uomConversionFactor
                        dr("monTotAmount") = (dr("numUnitHour") * uomConversionFactor) * salePrice
                        dr("monTotAmtBefDiscount") = (dr("numUnitHour") * uomConversionFactor) * price
                        dr("UOMConversionFactor") = uomConversionFactor
                        dr("numUOM") = ddlUOM.SelectedValue

                        dr.AcceptChanges()

                        If OppType.Value = "1" Then
                            RecalculateContainerQty(dtItem)
                        End If

                        dsTemp.AcceptChanges()

                        BindItems(dsTemp)
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        <WebMethod(EnableSession:=True)>
        Public Shared Function UpdateItemRequiredDate(ByVal oppID As Long, ByVal oppItemID As Long, ByVal ItemRequiredDate As String)
            Try

                Dim dtItem As DataTable = dsTemp.Tables(0)

                For Each dritem As DataRow In dtItem.Rows
                    If CCommon.ToLong(dritem("numoppitemtCode")) = oppItemID Then
                        Dim dRows As DataRow() = dtItem.Select("numoppitemtCode = " & oppItemID)
                        Dim dr As DataRow = dRows(0)
                        dr("ItemRequiredDate") = ItemRequiredDate
                        dr.AcceptChanges()
                    End If
                Next
                dsTemp.AcceptChanges()

                Return Nothing

            Catch ex As Exception
                Return ("Error: " & ex.Message)
            End Try
        End Function

        Protected Sub txtPriceUOM_TextChanged(ByVal sender As Object, ByVal e As EventArgs)
            Try
                Dim txtmonPriceUOM As TextBox = CType(sender, TextBox)
                Dim row As GridViewRow = DirectCast(txtmonPriceUOM.NamingContainer, GridViewRow)
                Dim dtItem As DataTable = dsTemp.Tables(0)

                Dim dRows As DataRow() = dtItem.Select("numoppitemtCode = " & CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode")).ToString())
                If dRows.Length > 0 Then
                    Dim dr As DataRow = dRows(0)
                    UpdateValues()
                    Dim discount As Decimal = dr("fltDiscount")
                    Dim uomConversionFactor As Decimal = CCommon.ToDecimal(dr("UOMConversionFactor"))
                    Dim price As Decimal = CCommon.ToDecimal(txtmonPriceUOM.Text) / uomConversionFactor
                    Dim salePrice As Decimal

                    If Not CCommon.ToBool(dr("bitDiscountType")) Then
                        If (dr("bitMarkupDiscount").ToString() = "1") Then ' Markup N Discount logic added
                            salePrice = price + (price * (discount / 100))
                        Else
                            salePrice = price - (price * (discount / 100))
                        End If
                    Else
                        If CCommon.ToDecimal(dr("numUnitHour")) > 0 Then
                            If (dr("bitMarkupDiscount").ToString() = "1") Then  ' Markup N Discount logic added
                                salePrice = ((price * (CCommon.ToDecimal(dr("numUnitHour")) * uomConversionFactor)) + discount) / (CCommon.ToDecimal(dr("numUnitHour")) * uomConversionFactor)
                            Else
                                salePrice = ((price * (CCommon.ToDecimal(dr("numUnitHour")) * uomConversionFactor)) - discount) / (CCommon.ToDecimal(dr("numUnitHour")) * uomConversionFactor)
                            End If
                        Else
                            salePrice = 0
                        End If
                    End If

                    Dim lblmonTotAmount As Label = DirectCast(row.FindControl("lblmonTotAmount"), Label)
                    If Not lblmonTotAmount Is Nothing Then
                        lblmonTotAmount.Text = CCommon.GetDecimalFormat(CCommon.ToDouble((CCommon.ToDecimal(dr("numUnitHour")) * uomConversionFactor) * salePrice))
                    End If

                    dr("monPrice") = price
                    dr("monPriceUOM") = price * uomConversionFactor
                    dr("monTotAmtBefDiscount") = (dr("numUnitHour") * uomConversionFactor) * price
                    dr("monTotAmount") = (dr("numUnitHour") * uomConversionFactor) * salePrice

                    dr.AcceptChanges()
                    dsTemp.AcceptChanges()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub txtDiscount_TextChanged(sender As Object, e As EventArgs)
            Try
                Dim txtDiscount As TextBox = CType(sender, TextBox)
                Dim row As GridViewRow = DirectCast(txtDiscount.NamingContainer, GridViewRow)
                Dim dtItem As DataTable = dsTemp.Tables(0)

                Dim dRows As DataRow() = dtItem.Select("numoppitemtCode = " & CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode")).ToString())
                If dRows.Length > 0 Then
                    Dim dr As DataRow = dRows(0)
                    UpdateValues()
                    dr("fltDiscount") = CCommon.ToDecimal(txtDiscount.Text)
                    dr.AcceptChanges()
                    dsTemp.AcceptChanges()

                    UpdatePrice(CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode")), row)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

#Region "EDI Fields"

        ''' <summary>
        '''  Start - EDI Fields Populating and Saving Logic For Sales Order (By Priya)
        ''' </summary>
        Protected Sub BindEDICFValuesForOrder()
            dsEDICFForOrder = New DataSet
            Dim dtEDIFields As New DataTable

            dtEDIFields.Columns.Add("FldDTLID", GetType(String))
            dtEDIFields.Columns.Add("Fld_ID", GetType(String))
            dtEDIFields.Columns.Add("Fld_Value", GetType(String))
            dtEDIFields.Columns.Add("numModifiedBy", GetType(Long))
            dtEDIFields.Columns.Add("bintModifiedDate", GetType(String))
            dsEDICFForOrder.Tables.Add(dtEDIFields)
        End Sub

        Protected Sub LoadEDIFieldsForOrder()
            BindEDICFValuesForOrder()
            tblEDIOrder.Controls.Clear()

            objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            Dim dsEdiFields As DataSet = objCommon.GetEDICustomFields(19, lngOppId)

            If (dsEdiFields IsNot Nothing And dsEdiFields.Tables.Count > 0) Then
                Dim dtEDI As DataTable = dsEdiFields.Tables(0)
                If (dtEDI.Rows.Count > 0) Then
                    For Each dr As DataRow In dtEDI.Rows
                        If (CCommon.ToInteger(dr("Grp_id")) = 19) Then
                            Dim tblRow As New TableRow
                            Dim tblCell1 As New TableCell

                            tblCell1.Style.Add("text-align", "right")
                            tblCell1.Style.Add("border-top", "0px")
                            tblCell1.Style.Add("border-bottom", "0px")
                            tblCell1.Width = New Unit(50%)

                            Dim lblCFName As Label
                            lblCFName = New Label
                            lblCFName.Text = dr("Fld_label").ToString
                            tblCell1.Controls.Add(lblCFName)
                            tblRow.Cells.Add(tblCell1)

                            Dim tblCell2 As New TableCell
                            tblCell2.Style.Add("text-align", "right")
                            tblCell2.Style.Add("border-top", "0px")
                            tblCell2.Style.Add("border-bottom", "0px")
                            tblCell2.Width = New Unit(50%)

                            Dim txt As New TextBox
                            txt.CssClass = "signup"
                            txt.ID = "txtEDI" + dr("Fld_id").ToString
                            txt.Text = dr("Fld_Value").ToString
                            txt.ClientIDMode = ClientIDMode.Static

                            txt.Width = Unit.Pixel(180)
                            tblCell2.Controls.Add(txt)
                            tblRow.Cells.Add(tblCell2)

                            tblEDIOrder.Rows.Add(tblRow)

                            Dim dtEDIFields As DataTable
                            dtEDIFields = dsEDICFForOrder.Tables(0)

                            Dim drEDI As DataRow
                            drEDI = dtEDIFields.NewRow

                            drEDI("FldDTLID") = dr("FldDTLID").ToString
                            drEDI("Fld_ID") = dr("Fld_id").ToString
                            drEDI("Fld_Value") = txt.Text
                            drEDI("numModifiedBy") = Session("UserContactID")
                            drEDI("bintModifiedDate") = Date.Today.ToString("MM/dd/yyyy")

                            dtEDIFields.Rows.Add(drEDI)

                            drEDI.AcceptChanges()
                            dsEDICFForOrder.AcceptChanges()

                        End If
                    Next
                End If

            End If
        End Sub

        Protected Sub lnkEDIOrder_Click(sender As Object, e As EventArgs)
            Try
                If (tblEDIOrder.Rows.Count > 0) Then
                    divEDIOppItem.Style.Add("display", "none")
                    divEDIOrder.Style.Add("display", "")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub btnSaveEdiFields_Click(sender As Object, e As EventArgs)
            Try
                If (dsEDICFForOrder IsNot Nothing) Then
                    Dim dtEDICF As DataTable = dsEDICFForOrder.Tables(0)
                    If (dtEDICF.Rows.Count > 0) Then
                        For Each row As DataRow In dtEDICF.Rows
                            Dim GrpID As Integer = 19
                            Dim FldDTLID As Long = CCommon.ToLong(row("FldDTLID"))
                            Dim Fld_ID As Long = CCommon.ToLong(row("Fld_ID"))
                            Dim RecId As Long = lngOppId
                            Dim numModifiedBy As Long = CCommon.ToLong(row("numModifiedBy"))
                            Dim bintModifiedDate As String = row("bintModifiedDate")
                            Dim Fld_Value As String = CType(tblEDIOrder.FindControl("txtEDI" + row("Fld_id").ToString), TextBox).Text

                            objCommon = New CCommon
                            objCommon.UpdateEDICustomFields(GrpID, RecId, Fld_ID, Fld_Value, FldDTLID, numModifiedBy, bintModifiedDate)
                        Next
                    End If

                End If
                divEDIOrder.Style.Add("display", "none")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        ''' <summary>
        '''  End - EDI Fields Populating and Saving Logic For Sales Order (By Priya)
        ''' </summary>

        ''' <summary>
        '''  Start - EDI Fields Populating and Saving Logic For Sales Order Items(By Priya)
        ''' </summary>

        Protected Sub BindEDICFValuesForOppItem()
            dsEDICFForOppItem = New DataSet
            Dim dtEDIOppItem As New DataTable

            dtEDIOppItem.Columns.Add("FldDTLID", GetType(String))
            dtEDIOppItem.Columns.Add("Fld_ID", GetType(String))
            dtEDIOppItem.Columns.Add("Fld_Value", GetType(String))
            dsEDICFForOppItem.Tables.Add(dtEDIOppItem)
        End Sub

        Protected Sub LoadEDIFieldsForOppItem(ByVal numOppItemCode As Long)
            BindEDICFValuesForOppItem()
            tblEDIOppItem.Controls.Clear()

            objCommon = New CCommon
            objCommon.DomainID = Session("DomainID")
            Dim dsEdiOppItem As DataSet = objCommon.GetEDICustomFields(21, numOppItemCode)

            If (dsEdiOppItem IsNot Nothing And dsEdiOppItem.Tables.Count > 0) Then
                Dim dtEDI As DataTable = dsEdiOppItem.Tables(0)
                If (dtEDI.Rows.Count > 0) Then
                    For Each dr As DataRow In dtEDI.Rows
                        If (CCommon.ToInteger(dr("Grp_id")) = 21) Then
                            Dim tblRow As New TableRow
                            Dim tblCell1 As New TableCell

                            tblCell1.Style.Add("text-align", "right")
                            tblCell1.Style.Add("border-top", "0px")
                            tblCell1.Style.Add("border-bottom", "0px")
                            tblCell1.Width = New Unit(50%)

                            Dim lblCFName As Label
                            lblCFName = New Label
                            lblCFName.Text = dr("Fld_label").ToString
                            tblCell1.Controls.Add(lblCFName)
                            tblRow.Cells.Add(tblCell1)

                            Dim tblCell2 As New TableCell
                            tblCell2.Style.Add("text-align", "right")
                            tblCell2.Style.Add("border-top", "0px")
                            tblCell2.Style.Add("border-bottom", "0px")
                            tblCell2.Width = New Unit(50%)

                            Dim txt As New TextBox
                            txt.CssClass = "signup"
                            txt.ID = "txtEDIOppItem" + dr("Fld_id").ToString
                            txt.Text = dr("Fld_Value").ToString
                            txt.ClientIDMode = ClientIDMode.Static

                            txt.Width = Unit.Pixel(180)
                            tblCell2.Controls.Add(txt)
                            tblRow.Cells.Add(tblCell2)

                            tblEDIOppItem.Rows.Add(tblRow)

                            Dim dtEDIFieldsOppItem As DataTable
                            dtEDIFieldsOppItem = dsEDICFForOppItem.Tables(0)

                            Dim drEDI As DataRow
                            drEDI = dtEDIFieldsOppItem.NewRow

                            drEDI("FldDTLID") = dr("FldDTLID").ToString
                            drEDI("Fld_ID") = dr("Fld_id").ToString
                            drEDI("Fld_Value") = txt.Text

                            dtEDIFieldsOppItem.Rows.Add(drEDI)

                            drEDI.AcceptChanges()
                            dsEDICFForOppItem.AcceptChanges()

                        End If
                    Next
                End If

            End If
        End Sub
        Protected Sub lnkEDIOppItem_Click(sender As Object, e As EventArgs)
            Try
                Dim lnkOppItemEDIFields As LinkButton = CType(sender, LinkButton)
                Dim gvProductrow As GridViewRow = DirectCast(lnkOppItemEDIFields.NamingContainer, GridViewRow)
                numOppItemCodeEDI = CCommon.ToLong(gvProduct.DataKeys(gvProductrow.RowIndex)("numoppitemtcode"))

                LoadEDIFieldsForOppItem(numOppItemCodeEDI)
                If (tblEDIOppItem.Rows.Count > 0) Then
                    divEDIOrder.Style.Add("display", "none")
                    divEDIOppItem.Style.Add("display", "")
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try

        End Sub

        Protected Sub btnSaveEdiOppItem_Click(sender As Object, e As EventArgs)
            Try
                If (dsEDICFForOppItem IsNot Nothing) Then
                    Dim dtEDICF As DataTable = dsEDICFForOppItem.Tables(0)
                    If (dtEDICF.Rows.Count > 0) Then
                        For Each row As DataRow In dtEDICF.Rows
                            Dim GrpID As Integer = 21
                            Dim FldDTLID As Long = CCommon.ToLong(row("FldDTLID"))
                            Dim Fld_ID As Long = CCommon.ToLong(row("Fld_ID"))
                            Dim RecId As Long = numOppItemCodeEDI
                            Dim numModifiedBy As Long = Session("UserContactID")
                            Dim bintModifiedDate As String = Date.Today.ToString("MM/dd/yyyy")
                            Dim Fld_Value As String = CType(tblEDIOppItem.FindControl("txtEDIOppItem" + row("Fld_id").ToString), TextBox).Text

                            objCommon = New CCommon
                            objCommon.UpdateEDICustomFields(GrpID, RecId, Fld_ID, Fld_Value, FldDTLID, numModifiedBy, bintModifiedDate)
                        Next
                    End If

                End If
                divEDIOppItem.Style.Add("display", "none")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub btnItemReqDate_Click(sender As Object, e As EventArgs)

        End Sub

        ''' <summary>
        '''  End - EDI Fields Populating and Saving Logic For Sales Order Items(By Priya)
        ''' </summary>
#End Region

        Protected Sub radPer_CheckedChanged(sender As Object, e As EventArgs)
            Try
                Dim radPer As RadioButton = CType(sender, RadioButton)
                Dim row As GridViewRow = DirectCast(radPer.NamingContainer, GridViewRow)
                Dim dtItem As DataTable = dsTemp.Tables(0)

                Dim dRows As DataRow() = dtItem.Select("numoppitemtCode = " & CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode")).ToString())
                If dRows.Length > 0 Then
                    Dim dr As DataRow = dRows(0)
                    UpdateValues()
                    dr("bitDiscountType") = False
                    dr.AcceptChanges()
                    dsTemp.AcceptChanges()

                    UpdatePrice(CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode")), row)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub radAmt_CheckedChanged(sender As Object, e As EventArgs)
            Try
                Dim radAmt As RadioButton = CType(sender, RadioButton)
                Dim row As GridViewRow = DirectCast(radAmt.NamingContainer, GridViewRow)
                Dim dtItem As DataTable = dsTemp.Tables(0)

                Dim dRows As DataRow() = dtItem.Select("numoppitemtCode = " & CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode")).ToString())
                If dRows.Length > 0 Then
                    Dim dr As DataRow = dRows(0)
                    UpdateValues()
                    dr("bitDiscountType") = True
                    dr.AcceptChanges()
                    dsTemp.AcceptChanges()

                    UpdatePrice(CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode")), row)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub UpdatePrice(ByVal numOppItemID As Long, ByVal row As GridViewRow)
            Try
                Dim dtItem As DataTable = dsTemp.Tables(0)
                Dim dRows As DataRow() = dtItem.Select("numoppitemtCode = " & numOppItemID)
                If dRows.Length > 0 Then
                    Dim dr As DataRow = dRows(0)

                    Dim discount As Decimal = dr("fltDiscount")
                    Dim uomConversionFactor As Decimal = CCommon.ToDecimal(dr("UOMConversionFactor"))
                    Dim price As Decimal = CCommon.ToDecimal(dr("monPrice"))
                    Dim salePrice As Decimal = price

                    If Not CCommon.ToBool(dr("bitDiscountType")) Then
                        If (dr("bitMarkupDiscount").ToString() = "1") Then  ' Markup N Discount logic added
                            salePrice = price + (price * (discount / 100))
                        Else
                            salePrice = price - (price * (discount / 100))
                        End If
                    Else
                        If CCommon.ToDecimal(dr("numUnitHour")) > 0 Then
                            If (dr("bitMarkupDiscount").ToString() = "1") Then  ' Markup N Discount logic added
                                salePrice = ((price * (CCommon.ToDecimal(dr("numUnitHour")) * uomConversionFactor)) + discount) / (CCommon.ToDecimal(dr("numUnitHour")) * uomConversionFactor)
                            Else
                                salePrice = ((price * (CCommon.ToDecimal(dr("numUnitHour")) * uomConversionFactor)) - discount) / (CCommon.ToDecimal(dr("numUnitHour")) * uomConversionFactor)
                            End If
                        Else
                            salePrice = 0
                        End If
                    End If


                    Dim lblmonTotAmount As Label = DirectCast(row.FindControl("lblmonTotAmount"), Label)
                    If Not lblmonTotAmount Is Nothing Then
                        lblmonTotAmount.Text = CCommon.GetDecimalFormat(CCommon.ToDouble((CCommon.ToDecimal(dr("numUnitHour")) * uomConversionFactor) * salePrice))
                    End If

                    dr("monPrice") = price
                    dr("fltDiscount") = discount
                    dr("monPriceUOM") = price * uomConversionFactor
                    dr("monTotAmount") = (dr("numUnitHour") * uomConversionFactor) * salePrice
                    dr("monTotAmtBefDiscount") = (dr("numUnitHour") * uomConversionFactor) * price

                    dr.AcceptChanges()
                    dsTemp.AcceptChanges()
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub btnPickPack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                Dim btnPickPack As Button = CType(sender, Button)
                Dim row As GridViewRow = DirectCast(btnPickPack.NamingContainer, GridViewRow)

                Dim ddlPackagingSlipFromProdGrid As DropDownList = row.FindControl("ddlPackagingSlipFromProdGrid")

                hdnShippingBizDoc.Value = 1
                Dim IsBizDocCreated As Boolean

                Dim arrShipFromTo As New ArrayList
                Dim warehouseID As Long
                Dim shipToAddressID As Long

                Dim listShipFromTo As New System.Collections.Generic.List(Of PickPackShipFromTo)
                Dim listShipFromToTemp As New System.Collections.Generic.List(Of PickPackShipFromTo)
                For Each rowItem As GridViewRow In gvProduct.Rows
                    If DirectCast(rowItem.FindControl("chkPickPackShip"), CheckBox).Checked Then
                        warehouseID = CCommon.ToLong(DirectCast(rowItem.FindControl("hfWarehouseID"), HiddenField).Value)
                        shipToAddressID = CCommon.ToLong(DirectCast(rowItem.FindControl("hfShipToAddressID"), HiddenField).Value)

                        If listShipFromTo.Where(Function(x) x.WarehouseID = warehouseID AndAlso x.ShipToAddressID = shipToAddressID).Count() = 0 Then
                            If warehouseID <> 0 Then
                                listShipFromTo.RemoveAll(Function(x) x.WarehouseID = 0 AndAlso x.ShipToAddressID = shipToAddressID)
                                listShipFromTo.Add(New PickPackShipFromTo With {.WarehouseID = warehouseID, .ShipToAddressID = shipToAddressID})
                            ElseIf warehouseID = 0 AndAlso shipToAddressID > 0 AndAlso listShipFromTo.Where(Function(x) x.WarehouseID > 0 AndAlso x.ShipToAddressID = shipToAddressID).Count() = 0 Then
                                listShipFromTo.Add(New PickPackShipFromTo With {.WarehouseID = warehouseID, .ShipToAddressID = shipToAddressID})
                            End If
                        End If
                    End If
                Next

                If listShipFromTo.Count > 0 Then
                    listShipFromToTemp = listShipFromTo

                    For Each pickPackShipFromTo As PickPackShipFromTo In listShipFromTo.OrderByDescending(Function(x) x.ShipToAddressID)
                        If pickPackShipFromTo.ShipToAddressID > 0 Then
                            listShipFromTo.RemoveAll(Function(x) x.WarehouseID = pickPackShipFromTo.WarehouseID AndAlso x.ShipToAddressID = 0)
                        End If
                    Next

                    For Each pickPackShipFromTo As PickPackShipFromTo In listShipFromTo
                        arrShipFromTo.Add(pickPackShipFromTo.WarehouseID & "-" & pickPackShipFromTo.ShipToAddressID)
                    Next
                End If

                If arrShipFromTo.Count = 0 Then
                    arrShipFromTo.Add("0-0")
                End If

                Dim i As Integer = 1
                For Each Item As String In arrShipFromTo
                    IsBizDocCreated = CreateBizDoc(29397, 1, ddlPackagingSlipFromProdGrid.SelectedValue, CCommon.ToLong(Item.Split("-")(0)), CCommon.ToLong(Item.Split("-")(1)), isFirstIteration:=IIf(i = 1, True, False))

                    If (IsBizDocCreated = True) Then
                        btnOpenInvoice_Click(sender, e)
                    End If

                    i += 1
                Next

                LoadDataToSession()
                BindItems(dsTemp)
            Catch ex As Exception
                If ex.Message = "NOT_ALLOWED" Then
                    litMessage.Text = "To split order items multiple bizdocs you must create all bizdocs with ""partial fulfilment"" checked!"
                ElseIf ex.Message = "NOT_ALLOWED_FulfillmentOrder" Then
                    litMessage.Text = "Only a Sales Order can create a Fulfillment Order"
                ElseIf ex.Message = "FY_CLOSED" Then
                    litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
                ElseIf ex.Message = "AlreadyInvoice" Then
                    litMessage.Text = "You can't create a Invoice against a Fulfillment Order that already contains a Invoice"
                ElseIf ex.Message = "AlreadyPackingSlip" Then
                    litMessage.Text = "You can�t create a Packing-Slip against a Fulfillment Order that already contains a Packing-Slip"
                ElseIf ex.Message = "AlreadyInvoice_NOT_ALLOWED_FulfillmentOrder" Then
                    litMessage.Text = "You can�t create a fulfillment order against a sales order that already contains an Invoice or Packing-Slip"
                ElseIf ex.Message = "AlreadyFulfillmentOrder" Then
                    litMessage.Text = "Manually adding Invoices or Packing Slips to a Sales Order is not allowed after a Fulfillment Order (FO) is added. Your options are to edit the FO status to trigger creation of Invoices and/or Packing Slips, Delete the Fulfillment Order, or Create Invoices and/or Packing Slips from the Sales Fulfillment section"
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(CCommon.ToString(ex))
                End If
            End Try
        End Sub


        Function CreateBizDoc(ByVal BizDocTypeId As Integer, ByVal typeId As Integer, ByVal BizDocTemplateId As Integer, ByVal warehouseID As Long, ByVal shipToAddressID As Long, ByVal isFirstIteration As Boolean, Optional ByVal ParentBizDocId As Integer = 0) As Boolean
            Try
                If CCommon.ToBool(Session("IsMinUnitPriceRule")) Then
                    If CheckIfApprovalRequired() Then
                        'Throw New Exception("You can not add BizDoc because approval of unit price(s) is required.")
                        ShowMessage("You can Not add BizDoc because approval of unit price(s) Is required.")
                        Return 0
                    End If
                End If

                Dim lngCntID, JournalId As Long
                Dim objOppBizDocs = New OppBizDocs
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.OppId = lngOppId
                objOppBizDocs.OppBizDocId = 0
                objOppBizDocs.BizDocId = BizDocTypeId
                objOppBizDocs.BizDocTemplateID = BizDocTemplateId
                objOppBizDocs.UserCntID = Session("UserContactID")
                Dim lintOpportunityType As Integer
                lintOpportunityType = objOppBizDocs.GetOpportunityType()

                objOppBizDocs.OppType = lintOpportunityType
                objOppBizDocs.UserCntID = Session("UserContactID")
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.vcPONo = "" 'txtPO.Text
                objOppBizDocs.vcComments = "" 'txtComments.Text
                'objOppBizDocs.ShipCost = 0 'IIf(IsNumeric(txtShipCost.Text), txtShipCost.Text, 0)
                'Add Billing Term from selected SalesTemplate(Sales Order)'s first Autoritative bizdoc's Billing term


                Dim dtDetails, dtBillingTerms As DataTable
                Dim objPageLayout As New CPageLayout
                objPageLayout.OpportunityId = lngOppId
                objPageLayout.DomainID = Session("DomainID")
                dtDetails = objPageLayout.OpportunityDetails.Tables(0)
                lngDivId = dtDetails.Rows(0).Item("numDivisionID")
                lngCntID = dtDetails.Rows(0).Item("numContactID")
                Dim objAdmin As New CAdmin
                objAdmin.DivisionID = lngDivId

                Dim lintAuthorizativeBizDocsId As Long
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.OppId = lngOppId
                objOppBizDocs.ShipCompany = CCommon.ToLong(dtDetails.Rows(0).Item("intUsedShippingCompany"))
                lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()

                Dim objJournal As New JournalEntry
                If lintAuthorizativeBizDocsId = BizDocTypeId Then 'save bizdoc only if selected company's AR and AP account is mapped
                    If objOppBizDocs.ValidateCustomerAR_APAccounts("AR", Session("DomainID"), lngDivId) = 0 Then
                        'Throw New Exception("Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip"" To Save BizDoc.")
                        ShowMessage("Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip"" To Save BizDoc.")
                        Return 0
                    End If
                End If
                If lintOpportunityType = 2 Then
                    'Accounting validation->Do not allow to save PO untill Default COGs account is mapped
                    If ChartOfAccounting.GetDefaultAccount("CG", Session("DomainID")) = 0 Then
                        'Throw New Exception("Please Set Default COGs account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save")
                        ShowMessage("Please Set Default COGs account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save")
                        Return 0
                    End If
                End If

                If lintOpportunityType = 2 Then
                    If ChartOfAccounting.GetDefaultAccount("PC", Session("DomainID")) = 0 Then
                        'Throw New Exception("Please Set Default Purchase Clearing account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save")
                        ShowMessage("Please Set Default Purchase Clearing account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save")
                        Return 0
                    End If

                    If ChartOfAccounting.GetDefaultAccount("PV", Session("DomainID")) = 0 Then
                        'Throw New Exception("Please Set Default Purchase Price Variance account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save")
                        ShowMessage("Please Set Default Purchase Price Variance account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save")
                        Return 0
                    End If
                End If

                objOppBizDocs.FromDate = DateTime.UtcNow
                objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")
                objCommon.Mode = 33
                objCommon.Str = BizDocTypeId
                objOppBizDocs.SequenceId = objCommon.GetSingleFieldValue()

                objCommon = New CCommon
                objCommon.DomainID = Session("DomainID")
                objCommon.Mode = 34
                objCommon.Str = lngOppId
                objOppBizDocs.RefOrderNo = objCommon.GetSingleFieldValue()

                objOppBizDocs.bitPartialShipment = True
                objOppBizDocs.numSourceBizDocId = ParentBizDocId
                objOppBizDocs.bitShippingBizDoc = hdnShippingBizDoc.Value

                Dim dt As New DataTable
                dt = objCommon.GetMasterListItems(11, Session("DomainID"))

                Dim dv As New DataView(dt)
                If (typeId = 2) Then
                    dv.RowFilter = "vcData = 'Pending Shipping'"
                    If (dv.Count > 0) Then
                        objOppBizDocs.BizDocStatus = dv.Item(0)("numListItemID")
                    End If
                End If
                If (typeId = 3) Then
                    dv.RowFilter = "vcData = 'Unpaid'"
                    If (dv.Count > 0) Then
                        objOppBizDocs.BizDocStatus = dv.Item(0)("numListItemID")
                    End If
                End If
                Dim ItemXML As String
                ItemXML = GetBizDocItems(typeId, warehouseID, shipToAddressID, isFirstIteration)
                If (ItemXML <> "") Then

                    objOppBizDocs.strBizDocItems = ItemXML
                    Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                        lngBizDocId = objOppBizDocs.SaveBizDoc()

                        If lngBizDocId = 0 Then
                            'Throw New Exception("A BizDoc by the same name is already created. Please select another BizDoc !")
                            ShowMessage("A BizDoc by the same name is already created. Please select another BizDoc !")
                            Return 0
                        End If


                        'Create Journals only for authoritative bizdocs
                        '-------------------------------------------------
                        If lintAuthorizativeBizDocsId = BizDocTypeId Or BizDocTypeId = 304 Then ' 304 - Deferred Income
                            Dim ds As New DataSet
                            Dim dtOppBiDocItems As DataTable

                            objOppBizDocs.OppBizDocId = lngBizDocId
                            ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                            dtOppBiDocItems = ds.Tables(0)

                            Dim objCalculateDealAmount As New CalculateDealAmount

                            objCalculateDealAmount.CalculateDealAmount(lngOppId, lngBizDocId, lintOpportunityType, Session("DomainID"), dtOppBiDocItems)

                            ''---------------------------------------------------------------------------------
                            JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, objOppBizDocs.FromDate, Description:=ds.Tables(1).Rows(0).Item("vcBizDocID"))
                            Dim objJournalEntries As New JournalEntry

                            If lintOpportunityType = 2 Then
                                If CCommon.ToBool(ds.Tables(1).Rows(0).Item("bitPPVariance")) Then
                                    objJournalEntries.SaveJournalEntriesPurchaseVariance(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, lngBizDocId, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), ds.Tables(1).Rows(0).Item("fltExchangeRateBizDoc"), vcBaseCurrency:=ds.Tables(1).Rows(0).Item("vcBaseCurrency"), vcForeignCurrency:=ds.Tables(1).Rows(0).Item("vcForeignCurrency"))
                                Else
                                    objJournalEntries.SaveJournalEntriesPurchase(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, lngBizDocId, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"))
                                End If
                            ElseIf lintOpportunityType = 1 Then
                                If BizDocTypeId = 304 AndAlso CCommon.ToBool(Session("IsEnableDeferredIncome")) Then 'Deferred Revenue
                                    objJournalEntries.SaveJournalEntriesDeferredIncome(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, lngDivId, lngBizDocId, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), objCalculateDealAmount.GrandTotal)
                                Else
                                    If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                                        objJournalEntries.SaveJournalEntriesSales(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, lngBizDocId, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), 0, bitInvoiceAgainstDeferredIncomeBizDoc:=objOppBizDocs.bitInvoiceForDeferred)
                                    Else
                                        objJournalEntries.SaveJournalEntriesSalesNew(lngOppId, Session("DomainID"), dtOppBiDocItems, JournalId, lngBizDocId, objCalculateDealAmount.GrandTotal, objCalculateDealAmount.TotalDiscount, objCalculateDealAmount.ShippingAmount, objCalculateDealAmount.TotalLateCharges, lngDivId, ds.Tables(1).Rows(0).Item("numCurrencyID"), ds.Tables(1).Rows(0).Item("fltExchangeRate"), 0, bitInvoiceAgainstDeferredIncomeBizDoc:=objOppBizDocs.bitInvoiceForDeferred)
                                    End If

                                    If Session("AutolinkUnappliedPayment") Then
                                        Dim objSalesFulfillment As New SalesFulfillmentWorkflow
                                        objSalesFulfillment.DomainID = Session("DomainID")
                                        objSalesFulfillment.UserCntID = Session("UserContactID")
                                        objSalesFulfillment.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                                        objSalesFulfillment.OppId = lngOppId
                                        objSalesFulfillment.OppBizDocId = lngBizDocId

                                        objSalesFulfillment.AutoLinkUnappliedPayment()
                                    End If
                                End If
                            End If
                        End If

                        objTransactionScope.Complete()
                    End Using

                    ''Added By Sachin Sadhu||Date:1stMay2014
                    ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                    ''          Using Change tracking
                    Dim objWfA As New Workflow()
                    objWfA.DomainID = Session("DomainID")
                    objWfA.UserCntID = Session("UserContactID")
                    objWfA.RecordID = lngBizDocId
                    objWfA.SaveWFBizDocQueue()
                    'end of code

                    '------ Send BizDoc Alerts - Notify record owners, their supervisors, and your trading partners when a BizDoc is created, modified, or approved.
                    Dim objAlert As New CAlerts
                    objAlert.SendBizDocAlerts(lngOppId, lngBizDocId, BizDocTypeId, Session("DomainID"), CAlerts.enmBizDoc.IsCreated)

                    Dim objAutomatonRule As New AutomatonRule
                    objAutomatonRule.ExecuteAutomationRule(49, lngBizDocId, 1)

                    Return True
                Else
                    Return False
                End If
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function CreateFulfillmentBizDoc(DomainID As Long, UserCntID As Long, ByVal BizDocTemplateId As Integer, ByVal ParentBizDocId As Long, ByVal warehouseID As Long, ByVal shipToAddressID As Long, ByVal isFirstIteration As Boolean) As Boolean
            Try
                Dim ItemXML As String
                ItemXML = GetBizDocItems(2, warehouseID, shipToAddressID, isFirstIteration)
                If (ItemXML <> "") Then
                    Dim OppBizDocID, lngDivId, JournalId As Long
                    Dim lngCntID As Long = 0

                    Using objTransaction As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                        Dim dtDetails, dtBillingTerms As DataTable

                        Dim objPageLayout As New BACRM.BusinessLogic.Contacts.CPageLayout
                        objPageLayout.OpportunityId = lngOppId
                        objPageLayout.DomainID = DomainID
                        dtDetails = objPageLayout.OpportunityDetails.Tables(0)
                        lngDivId = CCommon.ToLong(dtDetails.Rows(0).Item("numDivisionID"))
                        lngCntID = CCommon.ToLong(dtDetails.Rows(0).Item("numContactID"))


                        Dim objOppBizDocs As New OppBizDocs
                        objOppBizDocs.UserCntID = UserCntID
                        objOppBizDocs.DomainID = DomainID

                        If objOppBizDocs.ValidateCustomerAR_APAccounts("AR", DomainID, lngDivId) = 0 Then
                            'Throw New Exception("Not able to fulfillment bizdoc. Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip""")
                            ShowMessage("Not able to fulfillment bizdoc. Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting-> Accounts for RelationShip""")
                            Return 0
                        End If

                        objOppBizDocs.OppId = lngOppId
                        objOppBizDocs.OppType = 1
                        objOppBizDocs.FromDate = DateTime.UtcNow
                        objOppBizDocs.ClientTimeZoneOffset = 0
                        objOppBizDocs.BizDocId = 296 'Fulfillment Bizdoc
                        objOppBizDocs.BizDocTemplateID = BizDocTemplateId
                        objOppBizDocs.numSourceBizDocId = ParentBizDocId

                        Dim objCommon As New CCommon
                        objCommon.DomainID = DomainID
                        objCommon.Mode = 33
                        objCommon.Str = CCommon.ToString(objOppBizDocs.BizDocId) 'DEFUALT BIZ DOC ID
                        objOppBizDocs.SequenceId = CCommon.ToString(objCommon.GetSingleFieldValue())

                        objCommon = New CCommon
                        objCommon.DomainID = DomainID
                        objCommon.Mode = 34
                        objCommon.Str = CCommon.ToString(lngOppId)
                        objOppBizDocs.RefOrderNo = CCommon.ToString(objCommon.GetSingleFieldValue())
                        objOppBizDocs.bitPartialShipment = True

                        objOppBizDocs.strBizDocItems = ItemXML
                        Dim dt As New DataTable
                        dt = objCommon.GetMasterListItems(11, Session("DomainID"))

                        Dim dv As New DataView(dt)
                        dv.RowFilter = "vcData = 'Pending Shipping'"
                        If (dv.Count > 0) Then
                            objOppBizDocs.BizDocStatus = dv.Item(0)("numListItemID")
                        End If
                        objOppBizDocs.bitShippingBizDoc = hdnShippingBizDoc.Value
                        OppBizDocID = objOppBizDocs.SaveBizDoc()
                        lngBizDocId = OppBizDocID

                        If OppBizDocID = 0 Then
                            'Throw New Exception("Not able to create fulfillment bizdoc. A BizDoc by the same name is already created. Please select another BizDoc !")
                            ShowMessage("Not able to create fulfillment bizdoc. A BizDoc by the same name is already created. Please select another BizDoc !")
                            Return 0
                        End If

                        'AUTO FULFILLS bizdoc AND MAKED QTY AS SHIPPED AND RELEASES IT FROM ALLOCATION
                        Dim objOppBizDoc As New OppBizDocs
                        objOppBizDoc.DomainID = DomainID
                        objOppBizDoc.UserCntID = UserCntID
                        objOppBizDoc.OppId = lngOppId
                        objOppBizDoc.OppBizDocId = OppBizDocID
                        objOppBizDoc.AutoFulfillBizDoc()


                        'IMPORTANT: WE ARE MAKING ACCOUNTING CHANGES BECAUSE WE ARE CREATING FULFILLMENT ORDER IN BACKGROUP AND MARKIGN IT AS SHIPPED
                        '           OTHERWISE ACCOUNTING IS ONLY IMPACTED WHEN USER SHIPPED IT FROM SALES FULFILLMENT PAGE
                        'CREATE SALES CLEARING ENTERIES
                        'CREATE JOURNALS ONLY FOR FILFILLMENT BIZDOCS
                        '-------------------------------------------------
                        Dim ds As New DataSet
                        Dim dtOppBiDocItems As DataTable

                        objOppBizDocs.OppBizDocId = OppBizDocID
                        ds = objOppBizDocs.GetOppInItemsForAuthorizativeAccounting
                        dtOppBiDocItems = ds.Tables(0)

                        Dim objCalculateDealAmount As New CalculateDealAmount
                        objCalculateDealAmount.CalculateDealAmount(lngOppId, OppBizDocID, 1, DomainID, dtOppBiDocItems)

                        JournalId = objOppBizDocs.GetJournalIdForAuthorizativeBizDocs

                        'WHEN FROM COA SOMEONE DELETED JOURNALS THEN WE NEED TO CREATE NEW JOURNAL HEADER ENTRY
                        JournalId = objOppBizDocs.SaveDataToHeader(objCalculateDealAmount.GrandTotal, Entry_Date:=Date.UtcNow.Date, Description:=CStr(ds.Tables(1).Rows(0).Item("vcBizDocID")))

                        Dim objJournalEntries As New BACRM.BusinessLogic.Accounting.JournalEntry
                        If objOppBizDocs.IsSalesClearingAccountEntryExists() Then
                            objJournalEntries.SaveJournalEnteriesSalesClearing(lngOppId, DomainID, dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                        Else
                            objJournalEntries.SaveJournalEnteriesSalesClearingNew(lngOppId, DomainID, dtOppBiDocItems, JournalId, lngDivId, OppBizDocID, CLng(ds.Tables(1).Rows(0).Item("numCurrencyID")), CDbl(ds.Tables(1).Rows(0).Item("fltExchangeRate")))
                        End If
                        '---------------------------------------------------------------------------------


                        'IF fulfillment order qty is less than pack qty than change packing slip quantity to match fulfillment order qty
                        objOppBizDocs.UserCntID = UserCntID
                        objOppBizDocs.DomainID = DomainID
                        objOppBizDocs.OppId = lngOppId
                        objOppBizDocs.OppBizDocId = ParentBizDocId
                        objOppBizDocs.ChangePackingSlipQty()

                        objTransaction.Complete()
                    End Using

                    ''Added By Sachin Sadhu||Date:1stMay2014
                    ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                    ''          Using Change tracking
                    Dim objWfA As New Workflow()
                    objWfA.DomainID = DomainID
                    objWfA.UserCntID = UserCntID
                    objWfA.RecordID = OppBizDocID
                    objWfA.SaveWFBizDocQueue()
                    'end of code

                    If OppBizDocID > 0 Then
                        Try
                            Dim objOppBizDocs As New OppBizDocs
                            objOppBizDocs.CreateInvoiceFromFulfillmentOrder(CCommon.ToLong(Session("DomainID")), CCommon.ToLong(Session("UserContactID")), lngOppId, OppBizDocID, CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")))
                        Catch ex As Exception
                            'DO NOT THROW ERROR
                        End Try
                    End If

                    Return True
                Else
                    Return False

                End If
            Catch ex As Exception
                Throw
            End Try
        End Function


        Function GetBizDocItems(ByVal typeId As Integer, ByVal warehouseID As Long, ByVal shipToAddressID As Long, Optional ByVal isFirstIteration As Boolean = False) As String
            Try
                Dim dtBizDocItems As New DataTable
                Dim strOppBizDocItems As String

                dtBizDocItems.TableName = "BizDocItems"
                dtBizDocItems.Columns.Add("OppItemID")
                dtBizDocItems.Columns.Add("Quantity")
                dtBizDocItems.Columns.Add("Notes")
                dtBizDocItems.Columns.Add("monPrice")



                Dim dr As DataRow
                'Dim BizCalendar As UserControl
                Dim strDeliveryDate As String

                Dim CalRentalStartDate As UserControl
                Dim CalRentalReturnDate As UserControl

                Dim RentalStartDate, RentalReturnDate As DateTime

                For Each dgBizGridItem As GridViewRow In gvProduct.Rows
                    If CType(dgBizGridItem.FindControl("chkPickPackShip"), CheckBox).Checked = True AndAlso
                        (((DirectCast(dgBizGridItem.FindControl("hfWarehouseID"), HiddenField).Value = warehouseID Or (DirectCast(dgBizGridItem.FindControl("hfWarehouseID"), HiddenField).Value = 0 AndAlso shipToAddressID > 0 AndAlso DirectCast(dgBizGridItem.FindControl("hfShipToAddressID"), HiddenField).Value = shipToAddressID)) AndAlso (DirectCast(dgBizGridItem.FindControl("hfShipToAddressID"), HiddenField).Value = shipToAddressID Or CCommon.ToLong(DirectCast(dgBizGridItem.FindControl("hfShipToAddressID"), HiddenField).Value) = "0")) Or (isFirstIteration = True AndAlso (DirectCast(dgBizGridItem.FindControl("hfWarehouseID"), HiddenField).Value = 0 AndAlso DirectCast(dgBizGridItem.FindControl("hfShipToAddressID"), HiddenField).Value = 0))) AndAlso
                        ((typeId = 1 AndAlso CCommon.ToDouble(CType(dgBizGridItem.FindControl("lblRPackagingSlipQty"), Label).Text) > 0) Or
                         (typeId = 2 AndAlso CCommon.ToDouble(CType(dgBizGridItem.FindControl("txtFullFIlmentQty"), TextBox).Text) > 0) Or
                         (typeId = 3 AndAlso CCommon.ToDouble(CType(dgBizGridItem.FindControl("txtInvoicedQty"), TextBox).Text) > 0)) Then
                        dr = dtBizDocItems.NewRow
                        dr("OppItemID") = CCommon.ToLong(gvProduct.DataKeys(dgBizGridItem.RowIndex)("numoppitemtcode"))
                        dr("Notes") = ""

                        Dim qty As Double
                        If typeId = 1 Then
                            dr("Quantity") = CCommon.ToDouble(CType(dgBizGridItem.FindControl("lblRPackagingSlipQty"), Label).Text)
                            qty = CCommon.ToDouble(CType(dgBizGridItem.FindControl("lblRPackagingSlipQty"), Label).Text)
                        ElseIf typeId = 2 Then
                            dr("Quantity") = CCommon.ToDouble(CType(dgBizGridItem.FindControl("txtFullFIlmentQty"), TextBox).Text)
                            qty = CCommon.ToDouble(CType(dgBizGridItem.FindControl("txtFullFIlmentQty"), TextBox).Text)
                        ElseIf typeId = 3 Then
                            dr("Quantity") = CCommon.ToDouble(CType(dgBizGridItem.FindControl("txtInvoicedQty"), TextBox).Text)
                            qty = CCommon.ToDouble(CType(dgBizGridItem.FindControl("txtInvoicedQty"), TextBox).Text)
                        End If

                        If (qty = "0") Then
                            ShowMessage("Quantity is not available to create bizdoc.")
                            Return ""
                        End If

                        Dim txtmonPriceUOM As TextBox = DirectCast(dgBizGridItem.FindControl("txtmonPriceUOM"), TextBox)
                        If Not txtmonPriceUOM Is Nothing Then
                            dr("monPrice") = CCommon.ToDecimal(CType(dgBizGridItem.FindControl("txtmonPriceUOM"), TextBox).Text)
                        End If

                        dtBizDocItems.Rows.Add(dr)
                    End If
                Next

                If dtBizDocItems.Rows.Count = 0 Then
                    Return ""
                Else
                    Dim dsNew As New DataSet
                    dsNew.Tables.Add(dtBizDocItems)
                    strOppBizDocItems = dsNew.GetXml
                    dsNew.Tables.Remove(dsNew.Tables(0))
                    Return strOppBizDocItems
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnFulFillmentOrder_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                Dim isFirstIteration As Boolean
                Dim arrShipFromTo As New Hashtable

                'IF User has Closed popup window again click fulfillment button than clear viewstate data
                If Not ViewState("FulfillmentIterationData") Is Nothing AndAlso Not DirectCast(ViewState("FulfillmentIterationData"), Hashtable).ContainsValue(1) Then
                    ViewState("FulfillmentIteration") = Nothing
                    ViewState("FulfillmentIterationData") = Nothing
                    ViewState("FulfillmentFirstIteration") = Nothing
                    hdnFulfillentInterationValue.Value = ""
                End If

                If ViewState("FulfillmentIterationData") Is Nothing AndAlso Not CCommon.ToBool(ViewState("FulfillmentIteration")) Then
                    Dim warehouseID As Long
                    Dim shipToAddressID As Long

                    Dim listShipFromTo As New System.Collections.Generic.List(Of PickPackShipFromTo)
                    Dim listShipFromToTemp As New System.Collections.Generic.List(Of PickPackShipFromTo)
                    For Each rowItem As GridViewRow In gvProduct.Rows
                        If DirectCast(rowItem.FindControl("chkPickPackShip"), CheckBox).Checked Then
                            warehouseID = CCommon.ToLong(DirectCast(rowItem.FindControl("hfWarehouseID"), HiddenField).Value)
                            shipToAddressID = CCommon.ToLong(DirectCast(rowItem.FindControl("hfShipToAddressID"), HiddenField).Value)

                            If listShipFromTo.Where(Function(x) x.WarehouseID = warehouseID AndAlso x.ShipToAddressID = shipToAddressID).Count() = 0 Then
                                If warehouseID <> 0 Then
                                    listShipFromTo.RemoveAll(Function(x) x.WarehouseID = 0 AndAlso x.ShipToAddressID = shipToAddressID)
                                    listShipFromTo.Add(New PickPackShipFromTo With {.WarehouseID = warehouseID, .ShipToAddressID = shipToAddressID})
                                ElseIf warehouseID = 0 AndAlso shipToAddressID > 0 AndAlso listShipFromTo.Where(Function(x) x.WarehouseID > 0 AndAlso x.ShipToAddressID = shipToAddressID).Count() = 0 Then
                                    listShipFromTo.Add(New PickPackShipFromTo With {.WarehouseID = warehouseID, .ShipToAddressID = shipToAddressID})
                                End If
                            End If
                        End If
                    Next

                    If listShipFromTo.Count > 0 Then
                        listShipFromToTemp = listShipFromTo

                        For Each pickPackShipFromTo As PickPackShipFromTo In listShipFromTo.OrderByDescending(Function(x) x.ShipToAddressID)
                            If pickPackShipFromTo.ShipToAddressID > 0 Then
                                listShipFromTo.RemoveAll(Function(x) x.WarehouseID = pickPackShipFromTo.WarehouseID AndAlso x.ShipToAddressID = 0)
                            End If
                        Next

                        For Each pickPackShipFromTo As PickPackShipFromTo In listShipFromTo
                            arrShipFromTo.Add(pickPackShipFromTo.WarehouseID & "-" & pickPackShipFromTo.ShipToAddressID, 0)
                        Next
                    End If

                    If arrShipFromTo.Count = 0 Then
                        arrShipFromTo.Add("0-0", 0)
                    End If

                    isFirstIteration = True
                    ViewState("FulfillmentIterationData") = arrShipFromTo
                    ViewState("FulfillmentIteration") = True
                    ViewState("FulfillmentFirstIteration") = True
                Else
                    isFirstIteration = False
                    ViewState("FulfillmentFirstIteration") = False
                    arrShipFromTo = DirectCast(ViewState("FulfillmentIterationData"), Hashtable)
                End If

                Dim vcError As String = ""

                For Each item As DictionaryEntry In arrShipFromTo
                    If item.Value = 0 Then
                        hdnFulfillentInterationValue.Value = item.Key

                        Dim dtMultiplePickPack As DataTable
                        dtMultiplePickPack = GetBizDocListByOppID(29397, CCommon.ToLong(item.Key.ToString.Split("-")(0)), CCommon.ToLong(item.Key.ToString.Split("-")(1)), isFirstIteration)

                        Dim btnFulFillmentOrder As Button = CType(sender, Button)
                        Dim ddlFulFillmentOrderFromProdGrid As DropDownList = gvProduct.HeaderRow.FindControl("ddlFulFillmentOrderFromProdGrid")
                        hdnSelectedTempId.Value = ddlFulFillmentOrderFromProdGrid.SelectedValue
                        hdnSelectedType.Value = ""
                        If (dtMultiplePickPack.Rows.Count > 0) Then
                            hdnShippingBizDoc.Value = 0
                        Else
                            hdnShippingBizDoc.Value = 1
                        End If
                        If Not dtMultiplePickPack Is Nothing AndAlso dtMultiplePickPack.Rows.Count > 1 Then
                            hdnFulfillentInterationValue.Value = item.Key

                            rdnFOSelection.DataSource = dtMultiplePickPack
                            rdnFOSelection.DataTextField = "vcBizDocName"
                            rdnFOSelection.DataValueField = "numOppBizDocsId"
                            rdnFOSelection.DataBind()

                            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "openOtherPaymentDropDown", "$('#dialMoreFO').modal('show');", True)
                            Exit Sub
                        Else
                            item.Value = 1

                            Try
                                Dim boolIsQtyIsMoreThanParent As Boolean = False
                                Dim objBizDocs As New OppBizDocs
                                objBizDocs.DomainID = Session("DomainID")
                                objBizDocs.UserCntID = Session("UserContactID")
                                objBizDocs.OppId = CCommon.ToLong(GetQueryStringVal("OpID"))
                                objBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                                Dim PackagingOrder As Long = 0

                                If Not dtMultiplePickPack Is Nothing AndAlso dtMultiplePickPack.Rows.Count > 0 Then
                                    PackagingOrder = dtMultiplePickPack.Rows(0)("numOppBizDocsId")

                                    Dim objOppBizDocs As New OppBizDocs
                                    objOppBizDocs.BizDocId = 29397
                                    objOppBizDocs.DomainID = Session("DomainID")
                                    objOppBizDocs.OppId = lngOppId
                                    objOppBizDocs.OppBizDocId = PackagingOrder
                                    Dim dtCreatedPickPackItems As DataTable
                                    dtCreatedPickPackItems = objOppBizDocs.GetBizDocItemsForPickPackSlip(296)
                                    If (dtCreatedPickPackItems.Rows.Count > 0) Then
                                        For Each dr As GridViewRow In gvProduct.Rows
                                            Dim txtItemQty As TextBox
                                            Dim hfRFullFilmentQty As HiddenField
                                            txtItemQty = dr.FindControl("txtFullFIlmentQty")
                                            hfRFullFilmentQty = dr.FindControl("hfRFullFilmentQty")
                                            If CCommon.ToDouble(txtItemQty.Text) > CCommon.ToDouble(hfRFullFilmentQty.Value) Then
                                                txtItemQty.Text = hfRFullFilmentQty.Value
                                            End If

                                            Dim dv As New DataView(dtCreatedPickPackItems)
                                            dv.RowFilter = "numoppitemtcode = " + Convert.ToString(gvProduct.DataKeys(dr.RowIndex)("numoppitemtcode")) + ""
                                            If dv.Count > 0 Then
                                                If CCommon.ToDouble(txtItemQty.Text) > CCommon.ToDouble(dv.Item(0)("RemainingQty")) Then
                                                    txtItemQty.Text = dv.Item(0)("RemainingQty")
                                                    boolIsQtyIsMoreThanParent = True
                                                End If
                                            End If
                                        Next
                                    End If

                                    Dim IsBizDocCreated As Boolean = False
                                    IsBizDocCreated = CreateFulfillmentBizDoc(Session("DomainID"), Session("UserContactID"), ddlFulFillmentOrderFromProdGrid.SelectedValue, PackagingOrder, CCommon.ToLong(item.Key.ToString.Split("-")(0)), CCommon.ToLong(item.Key.ToString.Split("-")(1)), isFirstIteration)
                                    isFirstIteration = False

                                    If (IsBizDocCreated = True) Then
                                        If (hdnShippingBizDoc.Value = "1" AndAlso hdnInventoryItems.Value = "1" AndAlso CCommon.ToLong(Session("numDefaultSalesShippingDoc")) > 0) Then
                                            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, Guid.NewGuid().ToString(), "<script>window.open('../opportunity/frmShippingBox.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppId=" + Convert.ToString(lngOppId) + "&OppBizDocId=" + Convert.ToString(lngBizDocId) + "&ShipCompID=0', '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=850,height=700,scrollbars=yes,resizable=yes');</script>", False)
                                        End If
                                        btnOpenInvoice_Click(sender, e)
                                    End If
                                Else
                                    ShowMessage("Parent bizdoc is not available.")
                                End If
                            Catch ex As Exception
                                If ex.Message.Contains("WORK_ORDER_NOT_COMPLETED") Then
                                    vcError += "Not able to add fulfillment bizdoc because some work orders are still not completed.<br />"
                                ElseIf ex.Message.Contains("QTY_MISMATCH") Then
                                    vcError += "Not able to add fulfillment bizdoc because qty is not same as required qty.<br />"
                                ElseIf ex.Message.Contains("REQUIRED_SERIALS_NOT_PROVIDED") Then
                                    vcError += "Not able to add fulfillment bizdoc because required number of serials are not provided.<br />"
                                ElseIf ex.Message.Contains("INVALID_SERIAL_NUMBERS") Then
                                    vcError += "Not able to add fulfillment bizdoc because invalid serials are provided.<br />"
                                ElseIf ex.Message.Contains("REQUIRED_LOTNO_NOT_PROVIDED") Then
                                    vcError += "Not able to add fulfillment bizdoc because required number of Lots are not provided.<br />"
                                ElseIf ex.Message.Contains("INVALID_LOT_NUMBERS") Then
                                    vcError += "Not able to add fulfillment bizdoc because invalid Lots are provided.<br />"
                                ElseIf ex.Message.Contains("SOME_LOTNO_DO_NOT_HAVE_ENOUGH_QTY") Then
                                    vcError += "Not able to add fulfillment bizdoc because Lot number do not have enough qty to fulfill.<br />"
                                ElseIf ex.Message.Contains("NOTSUFFICIENTQTY_ALLOCATION") Then
                                    vcError += "Not able to add fulfillment bizdoc because warehouse allocation is invalid.<br />"
                                ElseIf ex.Message.Contains("NOTSUFFICIENTQTY_ONHAND") Then
                                    vcError += "Not able to add fulfillment bizdoc because warehouse does not have enought on hand quantity.<br />"
                                ElseIf ex.Message = "NOT_ALLOWED" Then
                                    vcError += "To split order items multiple bizdocs you must create all bizdocs with ""partial fulfilment"" checked!<br />"
                                ElseIf ex.Message = "NOT_ALLOWED_FulfillmentOrder" Then
                                    vcError += "Only a Sales Order can create a Fulfillment Order<br />"
                                ElseIf ex.Message = "FY_CLOSED" Then
                                    vcError += "This transaction can not be posted,Because transactions date belongs to closed financial year.<br />"
                                ElseIf ex.Message = "AlreadyInvoice" Then
                                    vcError += "You can�t create a Invoice against a Fulfillment Order that already contains a Invoice<br />"
                                ElseIf ex.Message = "AlreadyPackingSlip" Then
                                    vcError += "You can�t create a Packing-Slip against a Fulfillment Order that already contains a Packing-Slip<br />"
                                ElseIf ex.Message = "AlreadyInvoice_NOT_ALLOWED_FulfillmentOrder" Then
                                    vcError += "You can�t create a fulfillment order against a sales order that already contains an Invoice or Packing-Slip<br />"
                                ElseIf ex.Message = "AlreadyFulfillmentOrder" Then
                                    vcError += "Manually adding Invoices or Packing Slips to a Sales Order is not allowed after a Fulfillment Order (FO) is added. Your options are to edit the FO status to trigger creation of Invoices and/or Packing Slips, Delete the Fulfillment Order, or Create Invoices and/or Packing Slips from the Sales Fulfillment section<br />"
                                Else
                                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                                    vcError += CCommon.ToString(ex) & "<br />"
                                End If
                            End Try
                        End If
                    End If
                Next

                If Not String.IsNullOrEmpty(vcError) Then
                    ShowMessage("Following error occurred while generating BizDoc(s): <br />" & vcError)
                End If

                LoadDataToSession()
                BindItems(dsTemp)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))

                ViewState("FulfillmentIteration") = Nothing
                ViewState("FulfillmentIterationData") = Nothing
                ViewState("FulfillmentFirstIteration") = Nothing
                hdnFulfillentInterationValue.Value = ""
            End Try
        End Sub

        Public Function GetBizDocListByOppID(ByVal BizDocTypeId As Integer, ByVal warehouseID As Long, ByVal shipToAddressID As Long, ByVal isFirstIteration As Boolean) As DataTable
            Try
                objOppBizDocs = New OppBizDocs()
                objOppBizDocs.BizDocId = BizDocTypeId
                objOppBizDocs.OppId = lngOppId
                Return objOppBizDocs.GeBizDocsByOppId(warehouseID, shipToAddressID, isFirstIteration)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Sub btnInvoice_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
            Try
                Dim isFirstIteration As Boolean
                Dim arrShipFromTo As New Hashtable

                'IF User has Closed popup window again click fulfillment button than clear viewstate data
                If Not ViewState("InvoiceIterationData") Is Nothing AndAlso Not DirectCast(ViewState("InvoiceIterationData"), Hashtable).ContainsValue(1) Then
                    ViewState("InvoiceIterationData") = Nothing
                    ViewState("InvoiceIterationData") = Nothing
                    ViewState("InvoiceFirstIteration") = Nothing
                    hdnInvoiceInterationValue.Value = ""
                End If

                If ViewState("InvoiceIterationData") Is Nothing AndAlso Not CCommon.ToBool(ViewState("InvoiceIteration")) Then
                    Dim warehouseID As Long
                    Dim shipToAddressID As Long

                    Dim listShipFromTo As New System.Collections.Generic.List(Of PickPackShipFromTo)
                    Dim listShipFromToTemp As New System.Collections.Generic.List(Of PickPackShipFromTo)
                    For Each rowItem As GridViewRow In gvProduct.Rows
                        If DirectCast(rowItem.FindControl("chkPickPackShip"), CheckBox).Checked Then
                            warehouseID = CCommon.ToLong(DirectCast(rowItem.FindControl("hfWarehouseID"), HiddenField).Value)
                            shipToAddressID = CCommon.ToLong(DirectCast(rowItem.FindControl("hfShipToAddressID"), HiddenField).Value)

                            If listShipFromTo.Where(Function(x) x.WarehouseID = warehouseID AndAlso x.ShipToAddressID = shipToAddressID).Count() = 0 Then
                                If warehouseID <> 0 Then
                                    listShipFromTo.RemoveAll(Function(x) x.WarehouseID = 0 AndAlso x.ShipToAddressID = shipToAddressID)
                                    listShipFromTo.Add(New PickPackShipFromTo With {.WarehouseID = warehouseID, .ShipToAddressID = shipToAddressID})
                                ElseIf warehouseID = 0 AndAlso shipToAddressID > 0 AndAlso listShipFromTo.Where(Function(x) x.WarehouseID > 0 AndAlso x.ShipToAddressID = shipToAddressID).Count() = 0 Then
                                    listShipFromTo.Add(New PickPackShipFromTo With {.WarehouseID = warehouseID, .ShipToAddressID = shipToAddressID})
                                End If
                            End If
                        End If
                    Next

                    If listShipFromTo.Count > 0 Then
                        listShipFromToTemp = listShipFromTo

                        For Each pickPackShipFromTo As PickPackShipFromTo In listShipFromTo.OrderByDescending(Function(x) x.ShipToAddressID)
                            If pickPackShipFromTo.ShipToAddressID > 0 Then
                                listShipFromTo.RemoveAll(Function(x) x.WarehouseID = pickPackShipFromTo.WarehouseID AndAlso x.ShipToAddressID = 0)
                            End If
                        Next

                        For Each pickPackShipFromTo As PickPackShipFromTo In listShipFromTo
                            arrShipFromTo.Add(pickPackShipFromTo.WarehouseID & "-" & pickPackShipFromTo.ShipToAddressID, 0)
                        Next
                    End If

                    If arrShipFromTo.Count = 0 Then
                        arrShipFromTo.Add("0-0", 0)
                    End If

                    isFirstIteration = True
                    ViewState("InvoiceIterationData") = arrShipFromTo
                    ViewState("InvoiceIteration") = True
                    ViewState("InvoiceFirstIteration") = True
                Else
                    isFirstIteration = False
                    ViewState("InvoiceFirstIteration") = False
                    arrShipFromTo = DirectCast(ViewState("InvoiceIterationData"), Hashtable)
                End If

                Dim vcError As String = ""

                For Each item As DictionaryEntry In arrShipFromTo
                    If item.Value = 0 Then
                        hdnInvoiceInterationValue.Value = item.Key

                        Dim dtMultiplePickPack As DataTable
                        dtMultiplePickPack = GetBizDocListByOppID(296, CCommon.ToLong(item.Key.ToString.Split("-")(0)), CCommon.ToLong(item.Key.ToString.Split("-")(1)), isFirstIteration)

                        Dim boolIsQtyIsMoreThanParent As Boolean = False
                        Dim ddlInvoiceFromProdGrid As DropDownList = gvProduct.HeaderRow.FindControl("ddlInvoiceFromProdGrid")
                        hdnSelectedTempId.Value = ddlInvoiceFromProdGrid.SelectedValue
                        hdnSelectedType.Value = "3"
                        If (dtMultiplePickPack.Rows.Count > 0) Then
                            hdnShippingBizDoc.Value = 0
                        Else
                            hdnShippingBizDoc.Value = 1
                        End If
                        If Not dtMultiplePickPack Is Nothing AndAlso dtMultiplePickPack.Rows.Count > 1 Then
                            hdnInvoiceInterationValue.Value = item.Key

                            rdnFOSelection.DataSource = dtMultiplePickPack
                            rdnFOSelection.DataTextField = "vcBizDocName"
                            rdnFOSelection.DataValueField = "numOppBizDocsId"
                            rdnFOSelection.DataBind()

                            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "openOtherPaymentDropDown", "$('#dialMoreFO').modal('show');", True)
                            Exit Sub
                        Else
                            Dim objBizDocs As New OppBizDocs
                            objBizDocs.DomainID = Session("DomainID")
                            objBizDocs.UserCntID = Session("UserContactID")
                            objBizDocs.OppId = CCommon.ToLong(GetQueryStringVal("OpID"))
                            objBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                            Dim PackagingOrder As Long = 0

                            If Not dtMultiplePickPack Is Nothing AndAlso dtMultiplePickPack.Rows.Count > 0 Then
                                PackagingOrder = dtMultiplePickPack.Rows(0)("numOppBizDocsId")
                                Dim objOppBizDocs As New OppBizDocs
                                objOppBizDocs.BizDocId = 296

                                objOppBizDocs.DomainID = Session("DomainID")
                                objOppBizDocs.OppId = lngOppId
                                objOppBizDocs.OppBizDocId = PackagingOrder
                                Dim dtCreatedPickPackItems As DataTable
                                dtCreatedPickPackItems = objOppBizDocs.GetBizDocItemsForPickPackSlip(287)
                                If (dtCreatedPickPackItems.Rows.Count > 0) Then
                                    For Each dr As GridViewRow In gvProduct.Rows
                                        Dim txtItemQty As TextBox
                                        Dim hfRemainingQty As HiddenField
                                        txtItemQty = dr.FindControl("txtInvoicedQty")
                                        hfRemainingQty = dr.FindControl("hfRInvoicedQty")

                                        If CCommon.ToDouble(txtItemQty.Text) > CCommon.ToDouble(hfRemainingQty.Value) Then
                                            txtItemQty.Text = hfRemainingQty.Value
                                        End If

                                        Dim dv As New DataView(dtCreatedPickPackItems)
                                        dv.RowFilter = "numoppitemtcode = " + Convert.ToString(gvProduct.DataKeys(dr.RowIndex)("numoppitemtcode")) + ""
                                        If dv.Count > 0 Then
                                            If CCommon.ToLong(txtItemQty.Text) > CCommon.ToDouble(dv.Item(0)("RemainingQty")) Then
                                                txtItemQty.Text = dv.Item(0)("RemainingQty")
                                                boolIsQtyIsMoreThanParent = True
                                            End If
                                        End If
                                    Next
                                End If

                                Dim IsBizDocCreated As Boolean
                                IsBizDocCreated = CreateBizDoc(287, 3, ddlInvoiceFromProdGrid.SelectedValue, CCommon.ToLong(item.Key.ToString.Split("-")(0)), CCommon.ToLong(item.Key.ToString.Split("-")(1)), isFirstIteration, PackagingOrder)
                                isFirstIteration = False
                                If (IsBizDocCreated = True) Then
                                    If boolIsQtyIsMoreThanParent Then
                                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "openOtherPaymentDropDown", "alert('Please check bizdoc because because bizdoc has changed quantity for some item(s) as you have entered more quantity than what is available in parent bizdoc.');", True)
                                    End If

                                    btnOpenInvoice_Click(sender, e)
                                End If
                            End If
                        End If
                    End If
                Next

                LoadDataToSession()
                BindItems(dsTemp)
            Catch ex As Exception
                If ex.Message = "NOT_ALLOWED" Then
                    litMessage.Text = "To split order items multiple bizdocs you must create all bizdocs with ""partial fulfilment"" checked!"
                ElseIf ex.Message = "NOT_ALLOWED_FulfillmentOrder" Then
                    litMessage.Text = "Only a Sales Order can create a Fulfillment Order"
                ElseIf ex.Message = "FY_CLOSED" Then
                    litMessage.Text = "This transaction can not be posted,Because transactions date belongs to closed financial year."
                ElseIf ex.Message = "AlreadyInvoice" Then
                    litMessage.Text = "You can�t create a Invoice against a Fulfillment Order that already contains a Invoice"
                ElseIf ex.Message = "AlreadyPackingSlip" Then
                    litMessage.Text = "You can�t create a Packing-Slip against a Fulfillment Order that already contains a Packing-Slip"
                ElseIf ex.Message = "AlreadyInvoice_NOT_ALLOWED_FulfillmentOrder" Then
                    litMessage.Text = "You can�t create a fulfillment order against a sales order that already contains an Invoice or Packing-Slip"
                ElseIf ex.Message = "AlreadyFulfillmentOrder" Then
                    litMessage.Text = "Manually adding Invoices or Packing Slips to a Sales Order is not allowed after a Fulfillment Order (FO) is added. Your options are to edit the FO status to trigger creation of Invoices and/or Packing Slips, Delete the Fulfillment Order, or Create Invoices and/or Packing Slips from the Sales Fulfillment section"
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(CCommon.ToString(ex))
                End If

                ViewState("InvoiceIteration") = Nothing
                ViewState("InvoiceIterationData") = Nothing
                ViewState("InvoiceFirstIteration") = Nothing
                hdnInvoiceInterationValue.Value = ""
            End Try
        End Sub

        Private Sub radWarehouse_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
            Try
                Dim radWarehoue As RadComboBox = CType(sender, RadComboBox)
                Dim row As GridViewRow = DirectCast(radWarehoue.NamingContainer, GridViewRow)
                Dim dtItem As DataTable = dsTemp.Tables(0)

                Dim dRows As DataRow() = dtItem.Select("numoppitemtCode = " & CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode")).ToString())
                If dRows.Length > 0 Then
                    Dim dr As DataRow = dRows(0)
                    UpdateValues()
                    dr("numWarehouseItmsID") = radWarehoue.SelectedValue

                    Dim price As Decimal = CCommon.ToDecimal(dr("monPrice"))
                    Dim discount As Decimal = CCommon.ToDecimal(dr("fltDiscount"))
                    Dim salePrice As Decimal = price
                    Dim uomConversionFactor As Decimal = CCommon.ToDecimal(dr("UOMConversionFactor"))

                    Dim txtmonPriceUOM As TextBox = DirectCast(row.FindControl("txtmonPriceUOM"), TextBox)
                    If Not txtmonPriceUOM Is Nothing Then
                        CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(price * uomConversionFactor), txtmonPriceUOM)
                    End If

                    Dim txtDiscount As TextBox = DirectCast(row.FindControl("txtDiscount"), TextBox)
                    If Not txtDiscount Is Nothing Then
                        RemoveHandler txtDiscount.TextChanged, AddressOf txtDiscount_TextChanged
                        CCommon.SetValueInDecimalFormat(discount, txtDiscount)
                        AddHandler txtDiscount.TextChanged, AddressOf txtDiscount_TextChanged
                    End If

                    If Not CCommon.ToBool(dr("bitDiscountType")) Then
                        If (dr("bitMarkupDiscount").ToString() = "1") Then  ' Markup N Discount logic added
                            salePrice = price + (price * (discount / 100))
                        Else
                            salePrice = price - (price * (discount / 100))
                        End If
                    Else
                        If CCommon.ToDecimal(dr("numUnitHour")) > 0 Then
                            If (dr("bitMarkupDiscount").ToString() = "1") Then  ' Markup N Discount logic added
                                salePrice = ((price * (CCommon.ToDecimal(dr("numUnitHour")) * uomConversionFactor)) + discount) / (CCommon.ToDecimal(dr("numUnitHour")) * uomConversionFactor)
                            Else
                                salePrice = ((price * (CCommon.ToDecimal(dr("numUnitHour")) * uomConversionFactor)) - discount) / (CCommon.ToDecimal(dr("numUnitHour")) * uomConversionFactor)
                            End If
                        Else
                            salePrice = 0
                        End If
                    End If

                    Dim lblmonTotAmount As Label = DirectCast(row.FindControl("lblmonTotAmount"), Label)
                    If Not lblmonTotAmount Is Nothing Then
                        lblmonTotAmount.Text = CCommon.GetDecimalFormat(CCommon.ToDouble((CCommon.ToDecimal(dr("numUnitHour")) * uomConversionFactor) * salePrice))
                    End If

                    dr("monPrice") = price
                    dr("fltDiscount") = discount
                    dr("monPriceUOM") = price * uomConversionFactor
                    dr("monTotAmount") = (dr("numUnitHour") * uomConversionFactor) * salePrice
                    dr("UOMConversionFactor") = uomConversionFactor
                    dr("monTotAmtBefDiscount") = (dr("numUnitHour") * uomConversionFactor) * price

                    dr.AcceptChanges()
                    dsTemp.AcceptChanges()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub lkbDelete_Click(sender As Object, e As EventArgs) Handles lkbDelete.Click
            Try
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.OpportunityId = lngOppId
                objOpportunity.Mode = 4
                Dim dsOppItems As DataSet = objOpportunity.GetOrderItems()
                Dim dtItems As DataTable = dsOppItems.Tables(0)
                Dim k As Integer
                Dim intCheck As Integer = 0
                For k = 0 To dtItems.Rows.Count - 1
                    If dtItems.Rows(k).Item("charItemType") <> "S" Then intCheck = 1
                    dtItems.Rows(k)("numUnitHour") = Math.Abs(dtItems.Rows(k)("numUnitHour") / dtItems.Rows(k)("UOMConversionFactor"))
                Next

                dtItems.Columns.Add("numVendorID", System.Type.GetType("System.Int32"))
                dtItems.Columns.Add("vcInstruction")
                dtItems.Columns.Add("bintCompliationDate")
                dtItems.Columns.Add("numWOAssignedTo")
                dtItems.AcceptChanges()

                Dim dtItem As DataTable
                dtItem = dsOppItems.Tables(0)
                dtItem.TableName = "Item"
                dtItem.PrimaryKey = New DataColumn() {dsOppItems.Tables(0).Columns("numoppitemtCode")}
                dsOppItems.AcceptChanges()

                For Each dr As GridViewRow In gvProduct.Rows
                    If DirectCast(dr.FindControl("chkSelect"), CheckBox).Enabled AndAlso DirectCast(dr.FindControl("chkSelect"), CheckBox).Checked Then
                        Dim dRows As DataRow() = dsOppItems.Tables(0).Select("numoppitemtCode = " & CCommon.ToLong(gvProduct.DataKeys(dr.RowIndex)("numoppitemtcode")).ToString())
                        If dRows.Length > 0 Then
                            dsOppItems.Tables(0).Rows.Remove(dRows(0))
                            dsOppItems.AcceptChanges()
                        End If
                    End If
                Next

                SaveOpportunity(dsOppItems)

                LoadDataToSession()
                BindItems(dsTemp)
            Catch ex As Exception
                If ex.Message.Contains("ITEM_USED_IN_BIZDOC") Then
                    ShowMessage("Deleted item(s) are used in bizdoc(s).")
                ElseIf ex.Message.Contains("ORDERED_QUANTITY_CAN_NOT_BE_LESS_THEN_PACKING_SLIP_QTY") Then
                    ShowMessage("Ordered qty can no be less than packing slip quantity when customer default settings is ""Allocate inventory from Packing Slip"".")
                ElseIf ex.Message.Contains("WORK_ORDER_EXISTS") Then
                    ShowMessage("Work order exists. You have to first delete work order(s).")
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(CCommon.ToString(ex))
                End If
            End Try
        End Sub

        Private Sub RecalculateContainerQty(ByRef dtItem As DataTable)
            Try
                'FIRST MAKE ALL CONTAINER QTY 0
                Dim arrContainer As DataRow() = dtItem.Select("numContainer > 0")
                For Each drRowC In arrContainer
                    Dim arrContainers As DataRow() = dtItem.Select("numItemCode=" & drRowC("numContainer"))

                    If Not arrContainers Is Nothing AndAlso arrContainers.Length > 0 Then
                        For Each drCont As DataRow In arrContainers
                            drCont("numUnitHour") = 0
                        Next
                    End If
                Next

                'Now calculate container qty again because item qty can be change from grid
                arrContainer = dtItem.Select("numContainer > 0")
                If Not arrContainer Is Nothing AndAlso arrContainer.Length > 0 Then
                    Dim dtTempContainer As New DataTable
                    dtTempContainer.Columns.Add("numContainer")
                    dtTempContainer.Columns.Add("numContainerQty")
                    dtTempContainer.Columns.Add("numWarehouseID")

                    For Each dr As DataRow In arrContainer
                        If dtTempContainer.Select("numContainer =" & CCommon.ToLong(dr("numContainer")) & " AND numWarehouseID=" & CCommon.ToLong(dr("numWarehouseID")) & " AND numContainerQty=" & dr("numContainerQty")).Length = 0 Then
                            Dim drTemp As DataRow = dtTempContainer.NewRow
                            drTemp("numContainer") = dr("numContainer")
                            drTemp("numContainerQty") = dr("numContainerQty")
                            drTemp("numWarehouseID") = dr("numWarehouseID")
                            dtTempContainer.Rows.Add(drTemp)
                        End If
                    Next

                    For Each drContainer As DataRow In dtTempContainer.Rows
                        'Check if there are items exists which uses same container and has same qty which can be added to container and container qty is not fully used
                        Dim containerQty As Double = 0
                        Dim arrItems As DataRow() = dtItem.Select("numContainer=" & CCommon.ToLong(drContainer("numContainer")) & " AND numWarehouseID=" & CCommon.ToLong(drContainer("numWarehouseID")) & " AND numContainerQty=" & drContainer("numContainerQty"))
                        If Not arrItems Is Nothing AndAlso arrItems.Length > 0 Then
                            Dim qty As Double = arrItems.Sum(Function(x) CCommon.ToDouble(x("numUnitHour") * x("UOMConversionFactor")))
                            containerQty = containerQty + Math.Ceiling(qty / CCommon.ToDouble(drContainer("numContainerQty")))
                        End If

                        Dim drMain As DataRow = dtItem.Select("numItemCode=" & CCommon.ToLong(drContainer("numContainer")) & " AND numWarehouseID=" & CCommon.ToLong(drContainer("numWarehouseID"))).FirstOrDefault()

                        If Not drMain Is Nothing Then
                            drMain("numUnitHour") = drMain("numUnitHour") + containerQty
                        End If
                    Next
                End If


                'Remove containers with qty 0
                Dim arrMainContainer As DataRow() = dtItem.Select("numContainer > 0")
                If Not arrMainContainer Is Nothing AndAlso arrMainContainer.Length > 0 Then
                    For Each dr As DataRow In arrMainContainer
                        Dim arrItems As DataRow() = dtItem.Select("numContainer=" & CCommon.ToLong(dr("numContainer")) & " AND numUnitHour=0")

                        If arrItems.Length > 0 Then
                            For Each drContainer As DataRow In arrItems
                                dtItem.Rows.Remove(drContainer)
                            Next
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Function CheckIfUnitPriceApprovalRequired() As Boolean
            Try
                objOpportunity.DivisionID = CCommon.ToLong(DivID.Value)
                objOpportunity.CurrencyID = CCommon.ToLong(hdnCurrencyID.Value)
                'Checks whether unit price approval is required for any item added in order
                Return objOpportunity.CheckIfUnitPriceApprovalRequired(dsTemp)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Protected Sub ddlVendor_SelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                Dim ddlVendor As DropDownList = CType(sender, DropDownList)
                Dim row As GridViewRow = DirectCast(ddlVendor.NamingContainer, GridViewRow)
                Dim dtItem As DataTable = dsTemp.Tables(0)

                Dim dRows As DataRow() = dtItem.Select("numoppitemtCode = " & CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode")).ToString())
                If dRows.Length > 0 Then
                    Dim dr As DataRow = dRows(0)
                    UpdateValues()
                    dr("numVendorID") = CCommon.ToLong(ddlVendor.SelectedValue)
                    Dim cost As Decimal

                    If Not row.FindControl("txtnumCost") Is Nothing Then
                        cost = CCommon.ToDecimal(DirectCast(row.FindControl("txtnumCost"), TextBox).Text)
                    Else
                        cost = dr("numCost")
                    End If

                    objCommon.DomainID = Session("DomainID")
                    objCommon.OppID = lngOppId
                    Dim dt As DataTable = objCommon.CalculateItemProfit(dr("numUnitHour") * dr("UOMConversionFactor"), dr("monPrice"), cost, CCommon.ToLong(ddlVendor.SelectedValue), dr("numItemCode"))

                    If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                        dr("numCost") = dt.Rows(0)("numCost")
                        dr("vcMargin") = dt.Rows(0)("Profit")
                    Else
                        dr("numCost") = 0
                        dr("vcMargin") = 0
                    End If

                    Dim txtnumCost As TextBox = DirectCast(row.FindControl("txtnumCost"), TextBox)
                    If Not txtnumCost Is Nothing Then
                        txtnumCost.Text = String.Format("{0:#.00}", dr("numCost"))
                    End If

                    Dim lblvcMargin As Label = DirectCast(row.FindControl("lblvcMargin"), Label)
                    If Not lblvcMargin Is Nothing Then
                        lblvcMargin.Text = String.Format("{0:#.00}", dr("vcMargin"))
                    End If

                    dr.AcceptChanges()
                    dsTemp.AcceptChanges()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub UpdateValues()
            Try
                For Each item As GridViewRow In gvProduct.Rows
                    Dim dRows As DataRow() = dsTemp.Tables(0).Select("numoppitemtCode = " & CCommon.ToLong(gvProduct.DataKeys(item.RowIndex)("numoppitemtcode")).ToString())

                    If dRows.Length > 0 Then
                        Dim dr As DataRow = dRows(0)

                        If Not item.FindControl("txtvcItemName") Is Nothing Then
                            dr("vcItemName") = CType(item.FindControl("txtvcItemName"), TextBox).Text.Trim
                        End If
                        If Not item.FindControl("txtvcModelID") Is Nothing Then
                            dr("vcModelID") = HttpUtility.HtmlDecode(CType(item.FindControl("txtvcModelID"), TextBox).Text).Trim
                        End If
                        If Not item.FindControl("txtvcItemDesc") Is Nothing Then
                            dr("vcItemDesc") = CType(item.FindControl("txtvcItemDesc"), TextBox).Text.Trim
                        End If
                        If Not item.FindControl("txtvcNotes") Is Nothing Then
                            dr("vcNotes") = CType(item.FindControl("txtvcNotes"), TextBox).Text.Trim
                        End If
                        If Not item.FindControl("txtvcManufacturer") Is Nothing Then
                            dr("vcManufacturer") = CType(item.FindControl("txtvcManufacturer"), TextBox).Text.Trim
                        End If
                        If Not item.FindControl("ddlProject") Is Nothing Then
                            dr("numProjectID") = CType(item.FindControl("ddlProject"), DropDownList).SelectedValue
                        End If
                        If Not item.FindControl("ddlClass") Is Nothing Then
                            dr("numClassID") = CType(item.FindControl("ddlClass"), DropDownList).SelectedValue
                        End If
                        If Not CType(item.FindControl("txtnumCost"), TextBox) Is Nothing Then
                            dr("numCost") = CCommon.ToDouble(CType(item.FindControl("txtnumCost"), TextBox).Text.Trim)
                        End If
                        'If Not CType(item.FindControl("txtItemRequiredDate"), TextBox) Is Nothing Then
                        '    dr("ItemRequiredDate") = CType(item.FindControl("txtItemRequiredDate" + gvProduct.DataKeys(item.RowIndex)("numoppitemtcode").ToString()), TextBox).Text.Trim
                        'End If
                    End If
                Next
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub btnRecalculatePrice_Click(sender As Object, e As EventArgs) Handles btnRecalculatePrice.Click
            Try
                Dim dr As DataRow
                Dim dtItem As DataTable
                dtItem = dsTemp.Tables(0)

                For Each row As GridViewRow In gvProduct.Rows
                    If CType(row.FindControl("chkSelect"), CheckBox).Enabled Then
                        Dim dRows As DataRow() = dtItem.Select("numoppitemtCode = " & CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode")).ToString())
                        If dRows.Length > 0 Then
                            dr = dRows(0)
                            UpdateValues()

                            Dim objOpportunity As New COpportunities
                            objOpportunity.DomainID = Session("DomainID")
                            objOpportunity.DivisionID = CCommon.ToLong(DivID.Value)
                            objOpportunity.OppType = OppType.Value
                            objOpportunity.ItemCode = CCommon.ToLong(dr("numItemCode"))
                            objOpportunity.UnitHour = CCommon.ToDecimal(dr("numUnitHour"))
                            objOpportunity.WarehouseItmsID = CCommon.ToLong(dr("numWarehouseItmsID"))
                            Dim ds As DataSet = objOpportunity.GetOrderItemDetails(CCommon.ToLong(dr("numUOM")), "", CCommon.ToLong(hdnCountry.Value))

                            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 Then
                                Dim price As Decimal = CCommon.ToDecimal(ds.Tables(0).Rows(0)("monPrice"))
                                Dim discount As Decimal = CCommon.ToDecimal(ds.Tables(0).Rows(0)("Discount"))
                                Dim salePrice As Decimal = price

                                Dim txtmonPriceUOM As TextBox = DirectCast(row.FindControl("txtmonPriceUOM"), TextBox)
                                If Not txtmonPriceUOM Is Nothing Then
                                    CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(price * dr("UOMConversionFactor")), txtmonPriceUOM)
                                End If

                                Dim txtDiscount As TextBox = DirectCast(row.FindControl("txtDiscount"), TextBox)
                                If Not txtDiscount Is Nothing Then
                                    RemoveHandler txtDiscount.TextChanged, AddressOf txtDiscount_TextChanged
                                    CCommon.SetValueInDecimalFormat(discount, txtDiscount)
                                    AddHandler txtDiscount.TextChanged, AddressOf txtDiscount_TextChanged
                                End If

                                If Not CCommon.ToBool(dr("bitDiscountType")) Then
                                    If (dr("bitMarkupDiscount").ToString() = "1") Then  ' Markup N Discount logic added
                                        salePrice = price + (price * (discount / 100))
                                    Else
                                        salePrice = price - (price * (discount / 100))
                                    End If
                                Else
                                    If CCommon.ToDecimal(dr("numUnitHour")) > 0 Then
                                        If (dr("bitMarkupDiscount").ToString() = "1") Then  ' Markup N Discount logic added
                                            salePrice = ((price * (CCommon.ToDecimal(dr("numUnitHour")) * dr("UOMConversionFactor"))) + discount) / (CCommon.ToDecimal(dr("numUnitHour")) * dr("UOMConversionFactor"))
                                        Else
                                            salePrice = ((price * (CCommon.ToDecimal(dr("numUnitHour")) * dr("UOMConversionFactor"))) - discount) / (CCommon.ToDecimal(dr("numUnitHour")) * dr("UOMConversionFactor"))
                                        End If
                                    Else
                                        salePrice = 0
                                    End If
                                End If

                                Dim lblmonTotAmount As Label = DirectCast(row.FindControl("lblmonTotAmount"), Label)
                                If Not lblmonTotAmount Is Nothing Then
                                    lblmonTotAmount.Text = CCommon.GetDecimalFormat(CCommon.ToDouble((CCommon.ToDecimal(dr("numUnitHour")) * dr("UOMConversionFactor")) * salePrice))
                                End If

                                dr("monPrice") = price
                                dr("fltDiscount") = discount
                                dr("monPriceUOM") = price * dr("UOMConversionFactor")
                                dr("monTotAmount") = (dr("numUnitHour") * dr("UOMConversionFactor")) * salePrice
                                dr("monTotAmtBefDiscount") = (dr("numUnitHour") * dr("UOMConversionFactor")) * price
                                dr.AcceptChanges()

                                If OppType.Value = "1" Then
                                    RecalculateContainerQty(dtItem)
                                End If

                                dr.AcceptChanges()
                            End If
                        End If
                    End If
                Next

                dtItem.AcceptChanges()
                dsTemp.AcceptChanges()
                BindItems(dsTemp)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        'Added by Neelam Kapila || 10/07/2017 - Added functionality to Bind Child grid in modal pop up
        Sub BindChildGrid()
            Try
                Dim objOpportunity As New MOpportunity
                With objOpportunity
                    .DomainID = Session("DomainID")
                    .OpportunityId = lngOppId
                End With

                gvChildOpps.DataSource = objOpportunity.GetChildOpportunity()
                gvChildOpps.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Added by Neelam Kapila || 10/07/2017 - Added functionality to show Product image in the child grid
        Private Sub gvChildOpps_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvChildOpps.RowDataBound
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim dataRowView As DataRowView = DirectCast(e.Row.DataItem, DataRowView)
                Dim charType As String = CCommon.ToString(dataRowView("tintOppType"))
                Dim img As System.Web.UI.WebControls.Image = e.Row.FindControl("imgType")

                If (charType = "2") Then
                    img.ImageUrl = "~/images/Icon/Purchase order.png"
                Else
                    img.ImageUrl = "~/images/Icon/Sales order.png"
                End If
            End If
        End Sub
        'Private Sub lkbDemoteToOpportunity_Click(sender As Object, e As EventArgs) Handles lkbDemoteToOpportunity.Click
        '    Try
        '        If Not String.IsNullOrEmpty(hdnRecurrenceType.Value) Or Not String.IsNullOrEmpty(hdnRecurred.Value) Then
        '            ShowMessage("Can not demote record to opportunity: <br/> 1. Recurrence is set on order or on bizdoc within order. <br/> 2. Order is recuured from another order.")
        '            Exit Sub
        '        End If

        '        Dim objOpport As New COpportunities
        '        With objOpport
        '            .OpportID = lngOppId
        '            .DomainID = Session("DomainID")
        '            .UserCntID = Session("UserContactID")
        '        End With
        '        Dim lintCount As Integer
        '        lintCount = objOpport.GetAuthoritativeOpportunityCount()
        '        If lintCount = 0 Then
        '            objOpport.DemoteToOpportunity()
        '            Dim strFileName As String = ""
        '            strFileName = CCommon.GetDocumentPhysicalPath(Session("DomainId")) & "OrderShippingDetail_" & lngOppId & "_" & CCommon.ToString(Session("DomainId")) & ".xml"
        '            If File.Exists(strFileName) Then
        '                File.Delete(strFileName)
        '            End If

        '            Response.Redirect("../opportunity/frmOpportunities.aspx?opId=" & lngOppId, False)
        '        Else
        '            ShowMessage("Can not demote record to opportunity because Authoritative BizDoc(s) are present")
        '        End If
        '    Catch ex As Exception
        '        If ex.Message = "DEPENDANT" Then
        '            ShowMessage("Can not demote record to opportunity, Your option is to remove Time and Expense associated with all stages from ""milestone & stages"" sub-tab and try again.")
        '        ElseIf ex.Message = "RETURN_EXIST" Then
        '            ShowMessage("Can not demote record to opportunity because Return exists.")
        '        ElseIf ex.Message = "CASE DEPENDANT" Then
        '            ShowMessage("Can not demote record to opportunity because Dependent case exists.")
        '        ElseIf ex.Message = "CreditBalance DEPENDANT" Then
        '            ShowMessage("Can not demote record to opportunity because Credit balance of current order is being used. Your option is used to credit balance of Organization from Accunting sub tab.")
        '        ElseIf ex.Message = "OppItems DEPENDANT" Then
        '            ShowMessage("Can not demote record to opportunity because Items already used.")
        '        ElseIf ex.Message = "FY_CLOSED" Then
        '            ShowMessage("Can not set record as deal lost Because transactions date belongs to closed financial year.")
        '        ElseIf ex.Message = "RECURRING ORDER OR BIZDOC" Then
        '            ShowMessage("Can not demote record to opportunity because recurrence is set on order or on bizdoc within order.")
        '        ElseIf ex.Message = "RECURRED ORDER" Then
        '            ShowMessage("Can not demote record to opportunity because it is recurred from another order")
        '        ElseIf ex.Message.Contains("INVENTORY IM-BALANCE") Then
        '            ShowMessage("BizAutomation.com has detected a problem. Wait a few seconds and attempt to delete order again")
        '        ElseIf ex.Message.Contains("FULFILLED_ITEMS") Then
        '            ShowMessage("Can not demote record to opportunity because contains fulfilled items. Your option is to delete fulfillment order bizdoc.")
        '        ElseIf ex.Message.Contains("SERIAL/LOT#_USED") Then
        '            ShowMessage("Purchased serial item is used in any sales order or purchase Lot# do not have enough qty left becasuse of used in any sales order.")
        '        Else
        '            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '            DisplayError(CCommon.ToString(ex))
        '        End If
        '    End Try
        'End Sub
        Protected Sub btnPutaway_Click(sender As Object, e As EventArgs)
            Try
                Dim vcOppItemIds As String = ""
                Dim vcError As String = ""

                objOpportunity.OpportunityId = lngOppId
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.UserCntID = Session("UserContactID")
                objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                Dim ds As DataSet = objOpportunity.GetOrderItemsProductServiceGrid

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim dtFinal As DataTable = ds.Tables(0).Clone()

                    For Each row As GridViewRow In gvProduct.Rows
                        If Not row.FindControl("chkUnreceive") Is Nothing AndAlso DirectCast(row.FindControl("chkUnreceive"), CheckBox).Checked Then
                            If CCommon.ToDouble(DirectCast(row.FindControl("hfUnitHourReceived"), HiddenField).Value) > 0 Then
                                If ds.Tables(0).Select("numoppitemtCode=" & CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode"))).Length > 0 Then
                                    dtFinal.Rows.Clear()

                                    If ds.Tables(0).Select("numoppitemtCode=" & CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode")) & " AND ISNULL(numUnitHourReceived,0) > 0").Length > 0 Then
                                        Dim dr As DataRow = ds.Tables(0).Select("numoppitemtCode=" & CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode")) & " AND ISNULL(numUnitHourReceived,0) > 0")(0)

                                        Try
                                            Using objTransaction As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                                Dim objOpp As New COpportunities
                                                objOpp.DomainID = CCommon.ToLong(Session("DomainID"))
                                                objOpp.UserCntID = CCommon.ToLong(Session("UserContactID"))
                                                objOpp.OpportID = lngOppId
                                                objOpp.OppItemCode = CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode"))
                                                objOpp.byteMode = 2
                                                objOpp.UnreceiveItems(CCommon.ToDouble(dr("numUnitHourReceived")))

                                                dtFinal.ImportRow(dr)

                                                Dim JournalId As Long = 0

                                                Dim objOppBizDocs As New OppBizDocs
                                                objOppBizDocs.OppId = lngOppId
                                                objOppBizDocs.DomainID = Session("DomainID")
                                                objOppBizDocs.UserCntID = Session("UserContactID")


                                                'Reverse purchase clearing entries
                                                JournalId = objOppBizDocs.SaveDataToHeader(dr("numUnitHourReceived") * dr("monPrice") * dr("fltExchangeRate"), Nothing, Description:=CCommon.ToString(hdnOppname.Value) & ": Unreceived Item(s)")

                                                Dim objJournalEntries As New JournalEntry
                                                objJournalEntries.SaveJournalEntriesRevertPurchaseClearing(lngOppId, Session("DomainID"), dtFinal, JournalId)

                                                objTransaction.Complete()
                                            End Using

                                        Catch ex As Exception
                                            If ex.Message.Contains("SERIAL/LOT#_USED") Then
                                                vcError += CCommon.ToString(dr("vcItemName")) & ": " & "Serial/Lot received are used.<br/>"
                                            ElseIf ex.Message.Contains("INSUFFICIENT_ONHAND_QTY") Then
                                                vcError += CCommon.ToString(dr("vcItemName")) & ": " & "Do not have enough OnHand quantity to unreceive.<br/>"
                                            ElseIf ex.Message.Contains("RECEIVED_QTY_IS_CHANGED_BY_OTHER_USER") Then
                                                vcError += CCommon.ToString(dr("vcItemName")) & ": " & "Do not have enough OnHand quantity to unreceive.<br/>"
                                            Else
                                                vcError += CCommon.ToString(dr("vcItemName")) & ": " & "Unknown error occurred.<br/>"
                                            End If
                                        End Try
                                    End If
                                End If
                            End If

                            vcOppItemIds += CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode")) & ","
                        End If
                    Next
                End If

                If String.IsNullOrEmpty(vcOppItemIds) Then
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "UnReceive", "alert('Select atleat on item to unreceive.');", True)
                ElseIf Not String.IsNullOrEmpty(vcError) Then
                    ShowMessage("Not able to unreceive following item(s): <br />" & vcError)
                End If

                LoadDataToSession()
                BindItems(dsTemp)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Protected Sub btnUnreceive_Click(sender As Object, e As EventArgs)
            Try
                Dim vcOppItemIds As String = ""
                Dim vcError As String = ""

                objOpportunity.OpportunityId = lngOppId
                objOpportunity.DomainID = Session("DomainID")
                objOpportunity.UserCntID = Session("UserContactID")
                objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                Dim ds As DataSet = objOpportunity.GetOrderItemsProductServiceGrid

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim dtFinal As DataTable = ds.Tables(0).Clone()

                    For Each row As GridViewRow In gvProduct.Rows
                        If Not row.FindControl("chkUnreceive") Is Nothing AndAlso DirectCast(row.FindControl("chkUnreceive"), CheckBox).Checked Then
                            If CCommon.ToDouble(DirectCast(row.FindControl("hfUnitHourReceived"), HiddenField).Value) > 0 Then
                                If ds.Tables(0).Select("numoppitemtCode=" & CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode"))).Length > 0 Then
                                    dtFinal.Rows.Clear()

                                    If ds.Tables(0).Select("numoppitemtCode=" & CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode")) & " AND ISNULL(numUnitHourReceived,0) > 0").Length > 0 Then
                                        Dim dr As DataRow = ds.Tables(0).Select("numoppitemtCode=" & CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode")) & " AND ISNULL(numUnitHourReceived,0) > 0")(0)

                                        Try
                                            Using objTransaction As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                                Dim objOpp As New COpportunities
                                                objOpp.DomainID = CCommon.ToLong(Session("DomainID"))
                                                objOpp.UserCntID = CCommon.ToLong(Session("UserContactID"))
                                                objOpp.OpportID = lngOppId
                                                objOpp.OppItemCode = CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode"))
                                                objOpp.byteMode = 1
                                                objOpp.UnreceiveItems(CCommon.ToDouble(dr("numUnitHourReceived")))

                                                dtFinal.ImportRow(dr)

                                                Dim JournalId As Long = 0

                                                Dim objOppBizDocs As New OppBizDocs
                                                objOppBizDocs.OppId = lngOppId
                                                objOppBizDocs.DomainID = Session("DomainID")
                                                objOppBizDocs.UserCntID = Session("UserContactID")


                                                'Reverse purchase clearing entries
                                                JournalId = objOppBizDocs.SaveDataToHeader(dr("numUnitHourReceived") * dr("monPrice") * dr("fltExchangeRate"), Nothing, Description:=CCommon.ToString(hdnOppname.Value) & ": Unreceived Item(s)")

                                                Dim objJournalEntries As New JournalEntry
                                                objJournalEntries.SaveJournalEntriesRevertPurchaseClearing(lngOppId, Session("DomainID"), dtFinal, JournalId)

                                                objTransaction.Complete()
                                            End Using

                                        Catch ex As Exception
                                            If ex.Message.Contains("SERIAL/LOT#_USED") Then
                                                vcError += CCommon.ToString(dr("vcItemName")) & ": " & "Serial/Lot received are used.<br/>"
                                            ElseIf ex.Message.Contains("INSUFFICIENT_ONHAND_QTY") Then
                                                vcError += CCommon.ToString(dr("vcItemName")) & ": " & "Do not have enough OnHand quantity to unreceive.<br/>"
                                            ElseIf ex.Message.Contains("RECEIVED_QTY_IS_CHANGED_BY_OTHER_USER") Then
                                                vcError += CCommon.ToString(dr("vcItemName")) & ": " & "Do not have enough OnHand quantity to unreceive.<br/>"
                                            Else
                                                vcError += CCommon.ToString(dr("vcItemName")) & ": " & "Unknown error occurred.<br/>"
                                            End If
                                        End Try
                                    End If
                                End If
                            End If

                            vcOppItemIds += CCommon.ToLong(gvProduct.DataKeys(row.RowIndex)("numoppitemtcode")) & ","
                        End If
                    Next
                End If

                If String.IsNullOrEmpty(vcOppItemIds) Then
                    ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "UnReceive", "alert('Select atleat on item to unreceive.');", True)
                ElseIf Not String.IsNullOrEmpty(vcError) Then
                    ShowMessage("Not able to unreceive following item(s): <br />" & vcError)
                End If

                LoadDataToSession()
                BindItems(dsTemp)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        <WebMethod()>
        Public Shared Function WebMethodUpdateDealStatusConclusion(ByVal DomainID As Long, ByVal OppID As Long, ByVal OppStatus As Long, ByVal ConAnalysis As Long, ByVal UserCntID As Long) As String
            Try
                Dim objOpportunity As New OppotunitiesIP
                Dim output As Boolean = False
                objOpportunity.DomainID = DomainID
                objOpportunity.OpportunityId = OppID
                objOpportunity.OppStatus = OppStatus
                objOpportunity.ConAnalysis = ConAnalysis
                objOpportunity.UserCntID = UserCntID

                output = objOpportunity.UpdateDealStatusConclusion()
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(output, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function

        Protected Sub radCmbCompany_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
            Try
                If CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                    radcmbContact.Items.Clear()

                    Dim fillCombo As New COpportunities
                    With fillCombo
                        .DivisionID = radCmbCompany.SelectedValue
                        radcmbContact.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                        radcmbContact.DataTextField = "Name"
                        radcmbContact.DataValueField = "numcontactId"
                        radcmbContact.DataBind()
                    End With
                    radcmbContact.Items.Insert(0, New RadComboBoxItem("---Select One---", "0"))
                    If radcmbContact.Items.Count >= 2 Then
                        radcmbContact.Items(1).Selected = True
                    End If
                Else
                    radCmbCompany.Items.Clear()
                    radcmbContact.Items.Clear()
                    rptAssociatedContacts.DataSource = Nothing
                    rptAssociatedContacts.DataBind()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub LoadAssociatedContacts(ByVal divisionID As Long)
            Try
                Dim dr As DataRow
                Dim dt As New DataTable
                dt.Columns.Add("numDivisionID", GetType(System.Int64))
                dt.Columns.Add("numContactID", GetType(System.Int64))
                dt.Columns.Add("TargetCompany", GetType(System.String))
                dt.Columns.Add("ContactName", GetType(System.String))
                dt.Columns.Add("vcData", GetType(System.String))
                Dim objProspects As New BACRM.BusinessLogic.Prospects.CProspects
                objProspects.DivisionID = divisionID
                objProspects.bitAssociatedTo = 1
                Dim dsAssocations As DataSet = objProspects.GetAssociationTo()

                If Not dsAssocations Is Nothing AndAlso dsAssocations.Tables.Count > 0 AndAlso dsAssocations.Tables(0).Rows.Count > 0 Then
                    For Each drassociation As DataRow In dsAssocations.Tables(0).Rows
                        dr = dt.NewRow()
                        dr("numDivisionID") = CCommon.ToLong(drassociation("numDivisionID"))
                        dr("numContactID") = CCommon.ToLong(drassociation("numContactID"))
                        dr("TargetCompany") = CCommon.ToString(drassociation("TargetCompany"))
                        dr("ContactName") = CCommon.ToString(drassociation("ContactName"))
                        dr("vcData") = CCommon.ToString(drassociation("vcData"))
                        dt.Rows.Add(dr)
                    Next
                End If

                Dim objType As New OppotunitiesIP
                objType.DomainID = Session("DomainID")
                objType.OpportunityId = CCommon.ToLong(OppID.Value)
                Dim oppContact As DataTable = objType.AssociatedbyOppID

                If Not oppContact Is Nothing AndAlso oppContact.Rows.Count > 0 Then
                    For Each drassociation As DataRow In oppContact.Rows
                        dr = dt.NewRow()
                        dr("numDivisionID") = CCommon.ToLong(drassociation("numDivisionID"))
                        dr("numContactID") = CCommon.ToLong(drassociation("numContactId"))
                        dr("TargetCompany") = CCommon.ToString(drassociation("Company"))
                        dr("ContactName") = CCommon.ToString(drassociation("Name"))
                        dr("vcData") = CCommon.ToString(drassociation("ContactRole"))
                        dt.Rows.Add(dr)
                    Next
                End If

                rptAssociatedContacts.DataSource = dt
                rptAssociatedContacts.DataBind()
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Protected Sub btnSaveConclusion_Click(sender As Object, e As EventArgs)
            Try
                Dim objOpportunity As New OppotunitiesIP
                objOpportunity.DomainID = CCommon.ToLong(Session("DomainID"))
                objOpportunity.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objOpportunity.OpportunityId = lngOppId
                objOpportunity.OppStatus = If(rbDealLost.Checked, 2, 1)
                objOpportunity.ConAnalysis = ddlConclusionReason.SelectedValue
                objOpportunity.UpdateDealStatusConclusion()

                Dim divisionID As Long = 0
                Dim contactID As Long = 0
                Dim isValidData As Boolean = False

                If rbCustomer.Checked AndAlso CCommon.ToLong(radCmbCompany.SelectedValue) > 0 AndAlso CCommon.ToLong(radcmbContact.SelectedValue) > 0 Then
                    divisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                    contactID = CCommon.ToLong(radcmbContact.SelectedValue)
                    isValidData = True
                Else
                    For Each item As RepeaterItem In rptAssociatedContacts.Items
                        If CCommon.ToBool(DirectCast(item.FindControl("rbAssociatedContact"), RadioButton).Checked) AndAlso CCommon.ToLong(DirectCast(item.FindControl("hdnDivisionID"), HiddenField).Value) > 0 AndAlso CCommon.ToLong(DirectCast(item.FindControl("hdnContactID"), HiddenField).Value) > 0 Then
                            divisionID = CCommon.ToLong(DirectCast(item.FindControl("hdnDivisionID"), HiddenField).Value)
                            contactID = CCommon.ToLong(DirectCast(item.FindControl("hdnContactID"), HiddenField).Value)
                            isValidData = True
                        End If
                    Next
                End If

                If isValidData Then
                    Try
                        Dim objOpp As New MOpportunity
                        objOpp.DomainID = CCommon.ToLong(Session("DomainID"))
                        objOpp.UserCntID = CCommon.ToLong(Session("UserContactID"))
                        objOpp.OpportunityId = lngOppId
                        objOpp.ChangeOrganization(divisionID, contactID)
                    Catch ex As Exception
                        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                        DisplayError(CCommon.ToString(ex))
                    End Try
                End If

                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "DataUpdated", "DealStatusChanged();", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
    End Class

    Public Class GridProductTemplate
        Inherits System.Web.UI.Page
        Implements ITemplate


        Dim TemplateType As ListItemType
        Dim ColumnName, FieldName, DBColumnName, OrigDBColumnName, ControlType, LookBackTableName, tableAlias, ListType, SortColumnName, SortColumnOrder, FieldVisible, FieldDataType As String
        Dim Custom, AllowEdit, AllowSorting As Boolean
        Dim ListID, ListRelID, FormID, FormFieldId As Long
        Dim htGridColumnSearch As Hashtable
        Dim objCommon As New CCommon
        Dim dtUnit As DataTable
        Dim OppType As Short
        Dim OppStatus As Short
        Dim IsStockTransfer As Boolean
        Dim IsClosed As Boolean
        Dim ColumnWidth As Integer

        Sub New(ByVal type As ListItemType, ByVal drRow As DataRow, ByVal pOppType As Short, ByVal pOppStatus As Short, ByVal pIsStockTransfer As Boolean, ByVal pIsClosed As Boolean, Optional ByVal dtUOM As DataTable = Nothing)
            Try
                TemplateType = type

                FieldName = drRow("vcFieldName").ToString
                DBColumnName = drRow("vcDbColumnName").ToString
                OrigDBColumnName = drRow("vcOrigDbColumnName").ToString
                AllowSorting = CCommon.ToBool(drRow("bitAllowSorting"))
                FormFieldId = CCommon.ToLong(drRow("numFieldId"))
                AllowEdit = CCommon.ToBool(drRow("bitAllowEdit"))
                Custom = CCommon.ToBool(drRow("bitCustomField"))
                ControlType = drRow("vcAssociatedControlType").ToString
                ListType = drRow("vcListItemType").ToString
                ListID = CCommon.ToLong(drRow("numListID"))
                ListRelID = CCommon.ToLong(drRow("ListRelID"))
                LookBackTableName = drRow("vcLookBackTableName").ToString
                FieldVisible = drRow("intVisible").ToString
                FieldDataType = drRow("vcFieldDataType").ToString
                dtUnit = dtUOM
                OppType = pOppType
                OppStatus = pOppStatus
                IsStockTransfer = pIsStockTransfer
                IsClosed = pIsClosed
                ColumnWidth = CCommon.ToInteger(drRow("intColumnWidth"))
                FormID = 26
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub InstantiateIn(ByVal Container As Control) Implements ITemplate.InstantiateIn
            Try
                Dim lbl1 As Label = New Label()
                Dim lnkButton As New LinkButton
                Dim lnk As New HyperLink
                Select Case TemplateType
                    Case ListItemType.Header
                        Dim cell = CType(Container, DataControlFieldHeaderCell)

                        If ControlType <> "chkSelectAll" Then
                            If OrigDBColumnName = "SerialLotNo" Then
                                lbl1.ID = FormFieldId & DBColumnName
                                lbl1.Text = FieldName & "<span  class='tip' title='Serial/Lot #s entered below can be used in sales order only after closing this purchase order, untill that happens Serial numbers will not be displayed in inventory subtab of item.'>[?]</span>"
                                'lbl1.ForeColor = Color.White
                                Container.Controls.Add(lbl1)
                            ElseIf OrigDBColumnName = "bitBarcodePrint" Then
                                lbl1.ID = FormFieldId & DBColumnName
                                lbl1.Text = FieldName
                                'lbl1.ForeColor = Color.White
                                Container.Controls.Add(lbl1)

                                Container.Controls.Add(New HtmlGenericControl("br"))

                                Dim chk As New CheckBox
                                chk.ID = "chkSelectPrintAll"
                                'chk.Attributes.Add("onclick", "return SelectAll('chkAll','chkSelect')")
                                Container.Controls.Add(chk)

                                cell.Width = New Unit(110, UnitType.Pixel)

                            ElseIf OrigDBColumnName = "PickPackShip" Then
                                Dim ulMain As New HtmlGenericControl("ul")
                                ulMain.Attributes.Add("class", "list-inline")
                                ulMain.Style.Add("float", "left")

                                Dim li1 As New HtmlGenericControl("li")
                                li1.Attributes.Add("class", "psscol1")
                                Dim li2 As New HtmlGenericControl("li")
                                li2.Attributes.Add("class", "psscol2")
                                Dim li3 As New HtmlGenericControl("li")
                                li3.Attributes.Add("class", "psscol3")
                                Dim li4 As New HtmlGenericControl("li")
                                li4.Attributes.Add("class", "psscol4")

                                'COL 1
                                Dim header As New HtmlGenericControl("b")
                                header.InnerHtml = "Packed<br/>"
                                li1.Controls.Add(header)

                                Dim ddl As New DropDownList
                                ddl.ID = "ddlPackagingSlipFromProdGrid"
                                ddl.Width = New Unit(79, UnitType.Pixel)
                                ddl.DataSource = BindSalesTemplate(1, 29397)
                                ddl.DataTextField = "vcTemplateName"
                                ddl.DataValueField = "numBizDocTempID"
                                ddl.CssClass = "form-control"
                                ddl.Visible = False
                                ddl.DataBind()
                                li1.Controls.Add(ddl)

                                Dim btnPickPack As New Button
                                btnPickPack.ID = "btnPickPack"
                                btnPickPack.Text = "Add"
                                btnPickPack.CssClass = "btn btn-primary"
                                btnPickPack.Attributes.Add("style", "margin-Left:5px; padding:3px 10px 3px 10px;")
                                li1.Controls.Add(btnPickPack)

                                'COL 2
                                header = New HtmlGenericControl("b")
                                header.InnerHtml = "Shipped<br/>"
                                li2.Controls.Add(header)

                                Dim ddlFulFillmentOrder As New DropDownList
                                ddlFulFillmentOrder.ID = "ddlFulFillmentOrderFromProdGrid"
                                ddlFulFillmentOrder.DataSource = BindSalesTemplate(1, 296)
                                ddlFulFillmentOrder.Width = New Unit(146, UnitType.Pixel)
                                ddlFulFillmentOrder.DataTextField = "vcTemplateName"
                                ddlFulFillmentOrder.DataValueField = "numBizDocTempID"
                                ddlFulFillmentOrder.CssClass = "form-control"
                                ddlFulFillmentOrder.DataBind()
                                ddlFulFillmentOrder.Visible = False
                                li2.Controls.Add(ddlFulFillmentOrder)

                                Dim btnFulFillmentOrder As New Button
                                btnFulFillmentOrder.ID = "btnFulFillmentOrder"
                                btnFulFillmentOrder.Text = "Add"
                                btnFulFillmentOrder.CssClass = "btn btn-primary"
                                btnFulFillmentOrder.Attributes.Add("style", "margin-Left:5px; padding:3px 10px 3px 10px;")
                                li2.Controls.Add(btnFulFillmentOrder)

                                'COL 3
                                header = New HtmlGenericControl("b")
                                header.InnerHtml = "Invoiced<br/>"
                                li3.Controls.Add(header)

                                Dim ddlInvoice As New DropDownList
                                ddlInvoice.ID = "ddlInvoiceFromProdGrid"
                                ddlInvoice.DataSource = BindSalesTemplate(1, 287)
                                ddlInvoice.Width = New Unit(147, UnitType.Pixel)
                                ddlInvoice.DataTextField = "vcTemplateName"
                                ddlInvoice.DataValueField = "numBizDocTempID"
                                ddlInvoice.CssClass = "form-control"
                                ddlInvoice.DataBind()
                                ddlInvoice.Visible = False
                                li3.Controls.Add(ddlInvoice)

                                Dim btnInvoice As New Button
                                btnInvoice.ID = "btnInvoice"
                                btnInvoice.Text = "Add"
                                btnInvoice.CssClass = "btn btn-primary"
                                btnInvoice.Attributes.Add("style", "margin-Left:5px; padding:3px 10px 3px 10px;")
                                li3.Controls.Add(btnInvoice)


                                Dim chkAllPickPackSlip As New CheckBox
                                chkAllPickPackSlip.ID = "chkAllPickPackSlip"
                                li4.Controls.Add(chkAllPickPackSlip)

                                ulMain.Controls.Add(li1)
                                ulMain.Controls.Add(li2)
                                ulMain.Controls.Add(li3)
                                ulMain.Controls.Add(li4)
                                Container.Controls.Add(ulMain)
                            ElseIf OrigDBColumnName = "EDIFields" Then
                                objCommon.DomainID = Session("DomainID")
                                Dim dtbitEDI As DataTable = objCommon.GetDomainSettingValue("bitEDI")

                                If dtbitEDI Is Nothing Or dtbitEDI.Rows.Count = 0 Then
                                    Throw New Exception("Not able to fetch bit EDI global settings value.")
                                    Exit Sub
                                ElseIf CCommon.ToShort(dtbitEDI.Rows(0)("bitEDI")) = 1 Then
                                    lbl1.ID = FormFieldId & DBColumnName
                                    lbl1.Text = FieldName
                                    Container.Controls.Add(lbl1)
                                End If
                            Else
                                If OrigDBColumnName = "numUnitHour" AndAlso OppType = 2 AndAlso OppStatus = 1 AndAlso Not IsStockTransfer AndAlso Not IsClosed Then
                                    Dim btnUnreceive As New Button
                                    btnUnreceive.ID = "btnUnreceive"
                                    btnUnreceive.CssClass = "btn btn-primary"
                                    btnUnreceive.Text = "Unputaway"
                                    btnUnreceive.Attributes.Add("OnClientClick", "return ConfirmUnreceive();")
                                    btnUnreceive.Style.Add("float", "left")
                                    Container.Controls.Add(btnUnreceive)
                                End If

                                lbl1.ID = FormFieldId & DBColumnName
                                lbl1.Text = FieldName
                                'lbl1.ForeColor = Color.White
                                Container.Controls.Add(lbl1)

                                If OrigDBColumnName = "numUnitHour" AndAlso OppType = 2 AndAlso OppStatus = 1 AndAlso Not IsStockTransfer AndAlso Not IsClosed Then
                                    Dim chkAllPickPackSlip As New CheckBox
                                    chkAllPickPackSlip.ID = "chkAllUnReceive"
                                    chkAllPickPackSlip.Style.Add("float", "right")
                                    Container.Controls.Add(chkAllPickPackSlip)
                                End If
                            End If
                        Else
                            Dim chk As New CheckBox
                            chk.ID = "chkSelectAll"
                            'chk.Attributes.Add("onclick", "return SelectAll('chkAll','chkSelect')")
                            Container.Controls.Add(chk)
                        End If

                        If ColumnWidth > 0 Then
                            cell.Width = New Unit(ColumnWidth, UnitType.Pixel)
                        Else
                            cell.Width = New Unit(10, UnitType.Pixel)
                        End If
                        cell.Attributes.Add("id", FormID & "~" & FormFieldId & "~" & Custom)

                    Case ListItemType.Item
                        Dim cell = CType(Container, DataControlFieldCell)

                        If ControlType <> "chkSelectAll" Then
                            If OrigDBColumnName = "vcManufacturer" Or
                                OrigDBColumnName = "vcModelID" Or
                                OrigDBColumnName = "vcItemDesc" Or
                                OrigDBColumnName = "vcNotes" Or
                                OrigDBColumnName = "numCost" Or
                                OrigDBColumnName = "numUnitHour" Or
                                OrigDBColumnName = "monPriceUOM" Then
                                Dim txt As New TextBox
                                txt.ClientIDMode = UI.ClientIDMode.AutoID
                                txt.Attributes.Add("class", "form-control")
                                AddHandler txt.DataBinding, AddressOf BindTextBox

                                If OrigDBColumnName = "numUnitHour" Then
                                    txt.Style.Add("min-width", "80px !important")
                                    txt.Style.Add("width", "80px")
                                End If

                                If OrigDBColumnName = "numUnitHour" Or OrigDBColumnName = "monPriceUOM" Then
                                    txt.AutoPostBack = True
                                End If

                                If OrigDBColumnName = "numUnitHour" Then
                                    txt.Style.Add("display", "inline")
                                End If

                                Container.Controls.Add(txt)

                                If OrigDBColumnName = "numUnitHour" AndAlso OppType = 2 AndAlso OppStatus = 1 AndAlso Not IsStockTransfer AndAlso Not IsClosed Then
                                    Dim chkAllPickPackSlip As New CheckBox
                                    chkAllPickPackSlip.ID = "chkUnReceive"
                                    chkAllPickPackSlip.Style.Add("float", "right")
                                    Container.Controls.Add(chkAllPickPackSlip)
                                End If
                                If OrigDBColumnName = "monPriceUOM" AndAlso OppType = 2 Then
                                    Dim lit1 As New Literal
                                    lit1.ID = "litVendorCostTable"
                                    AddHandler lit1.DataBinding, AddressOf BindLiteralColumn
                                    Container.Controls.Add(lit1)

                                    Dim lit2 As New Literal
                                    lit2.ID = "litDynamicCostLastUpdated"
                                    AddHandler lit2.DataBinding, AddressOf BindLiteralColumn
                                    Container.Controls.Add(lit2)
                                End If
                            ElseIf OrigDBColumnName = "vcItemName" Then
                                Dim ul As New HtmlGenericControl("ul")
                                ul.Attributes.Add("class", "list-inline")

                                Dim li1 As New HtmlGenericControl("li")
                                Dim li2 As New HtmlGenericControl("li")
                                Dim li3 As New HtmlGenericControl("li")
                                Dim li4 As New HtmlGenericControl("li")

                                AddHandler lbl1.DataBinding, AddressOf BindOpenItem
                                li1.Controls.Add(lbl1)

                                Dim txt As New TextBox
                                txt.ClientIDMode = UI.ClientIDMode.AutoID
                                txt.Attributes.Add("class", "form-control")
                                AddHandler txt.DataBinding, AddressOf BindTextBox
                                li2.Controls.Add(txt)

                                Dim img As New HtmlGenericControl("img")
                                img.Attributes.Add("src", "../images/CloneDoc.png")
                                img.Attributes.Add("title", "Clone item")
                                img.Attributes.Add("onclick", "return CloneOrderLineItem(this);")
                                li3.Controls.Add(img)

                                Dim lbCreateOrMapItem As New LinkButton
                                lbCreateOrMapItem.ID = "lbCreateOrMapItem"
                                lbCreateOrMapItem.ClientIDMode = UI.ClientIDMode.AutoID
                                lbCreateOrMapItem.Attributes.Add("class", "btn btn-sm btn-primary")
                                lbCreateOrMapItem.Attributes.Add("style", "padding:1px 5px;font-size:14px;")
                                lbCreateOrMapItem.Visible = False
                                lbCreateOrMapItem.Text = "<i class='fa fa-plus'></i>"
                                li4.Controls.Add(lbCreateOrMapItem)

                                ul.Controls.Add(li1)
                                ul.Controls.Add(li2)
                                ul.Controls.Add(li3)
                                ul.Controls.Add(li4)

                                Container.Controls.Add(ul)

                                If OppType = 2 AndAlso OppStatus = 1 Then
                                    Dim lit1 As New Literal
                                    lit1.ID = "litQtyAddedToBills"
                                    AddHandler lit1.DataBinding, AddressOf BindLiteralColumn
                                    Container.Controls.Add(lit1)
                                End If
                            ElseIf OrigDBColumnName = "vcItemReleaseDate" Then
                                Dim table As New HtmlGenericControl("table")
                                Dim tr As New HtmlGenericControl("tr")
                                Dim td1 As New HtmlGenericControl("td")
                                Dim td2 As New HtmlGenericControl("td")

                                Dim lbl As New Label
                                AddHandler lbl.DataBinding, AddressOf BindStringColumn
                                td1.Controls.Add(lbl)

                                Dim radDatePicker As New RadDatePicker
                                radDatePicker.CssClass = "ItemReleaseDate"
                                radDatePicker.DateInput.CssClass = "HideDateInput"

                                radDatePicker.Width = New Unit(30, UnitType.Pixel)
                                AddHandler radDatePicker.DataBinding, AddressOf ItemReleaseDateColumn
                                radDatePicker.DateInput.DateFormat = "MM/dd/yyyy"
                                td2.Controls.Add(radDatePicker)

                                tr.Controls.Add(td1)
                                tr.Controls.Add(td2)
                                table.Controls.Add(tr)

                                Container.Controls.Add(table)

                            ElseIf OrigDBColumnName = "ItemRequiredDate" Then

                                Dim radDatePicker As New RadDatePicker
                                radDatePicker.ID = "rdpItemRequiredDate"
                                radDatePicker.DateInput.CssClass = "form-control"
                                radDatePicker.DateInput.Width = New Unit(100, UnitType.Pixel)
                                radDatePicker.Width = New Unit(120, UnitType.Pixel)
                                AddHandler radDatePicker.DataBinding, AddressOf ItemRequiredDateColumn
                                radDatePicker.DateInput.DateFormat = "MM/dd/yyyy"

                                Container.Controls.Add(radDatePicker)
                            ElseIf OrigDBColumnName = "DiscAmt" Then
                                Dim table As New HtmlGenericControl("table")
                                Dim tr As New HtmlGenericControl("tr")
                                Dim td1 As New HtmlGenericControl("td")
                                Dim td2 As New HtmlGenericControl("td")
                                td2.Style.Add("padding-left", "10px")
                                Dim td3 As New HtmlGenericControl("td")
                                td3.Style.Add("padding-left", "10px")



                                Dim txt As New TextBox
                                txt.Attributes.Add("class", "form-control")
                                txt.Style.Add("min-width", "80px !important")
                                txt.Style.Add("width", "80px")
                                txt.ID = "txtDiscount"
                                td1.Controls.Add(txt)

                                Dim radPer As New RadioButton
                                radPer.ID = "radPer"
                                radPer.Text = "%"
                                radPer.GroupName = "radDiscount"
                                td2.Controls.Add(radPer)

                                Dim radAmt As New RadioButton
                                radAmt.ID = "radAmt"
                                radAmt.GroupName = "radDiscount"
                                radAmt.Text = "Amount"
                                td3.Controls.Add(radAmt)

                                tr.Controls.Add(td1)
                                tr.Controls.Add(td2)
                                tr.Controls.Add(td3)

                                table.Controls.Add(tr)

                                Container.Controls.Add(table)
                            ElseIf OrigDBColumnName = "vcLocation" Then
                                Dim radWarehouse As New RadComboBox
                                radWarehouse.AutoPostBack = True
                                radWarehouse.ClientIDMode = UI.ClientIDMode.AutoID
                                radWarehouse.DataTextField = "vcWarehouse"
                                radWarehouse.DataValueField = "numWarehouseItemID"
                                radWarehouse.DropDownCssClass = "multipleRowsColumns"
                                radWarehouse.DropDownWidth = New Unit(840, UnitType.Pixel)
                                radWarehouse.Height = New Unit(100, UnitType.Pixel)
                                radWarehouse.Width = New Unit(100, UnitType.Percentage)
                                radWarehouse.HeaderTemplate = New WarehouseTemplate(ListItemType.Header)
                                radWarehouse.ItemTemplate = New WarehouseTemplate(ListItemType.Item)
                                AddHandler radWarehouse.DataBinding, AddressOf BindWarehouseDropDown
                                Container.Controls.Add(radWarehouse)
                            ElseIf OrigDBColumnName = "numUOMId" Then
                                Dim ddl As New DropDownList
                                ddl.AutoPostBack = True
                                ddl.Attributes.Add("class", "form-control")
                                ddl.Attributes.CssStyle.Add("width", "120px")
                                ddl.DataSource = dtUnit
                                ddl.DataTextField = "vcUnitName"
                                ddl.DataValueField = "numUOMId"
                                ddl.DataBind()
                                AddHandler ddl.DataBinding, AddressOf BindUOMDropdown
                                Container.Controls.Add(ddl)
                            ElseIf OrigDBColumnName = "numVendorID" Then
                                Dim ddl As New DropDownList
                                ddl.ID = "ddlVendor"
                                ddl.AutoPostBack = True
                                ddl.ClientIDMode = UI.ClientIDMode.AutoID
                                ddl.AutoPostBack = True
                                ddl.Attributes.Add("class", "form-control")
                                ddl.Attributes.CssStyle.Add("width", "180px")
                                Container.Controls.Add(ddl)
                            ElseIf OrigDBColumnName = "SerialLotNo" Then
                                If ControlType = "HyperLink" Then
                                    Dim hplAdd As New HyperLink
                                    AddHandler hplAdd.DataBinding, AddressOf BindSerialLotAddLink
                                    hplAdd.Attributes.Add("onclick", "OpenSerialLot()")
                                    Container.Controls.Add(hplAdd)
                                Else
                                    Dim labelSerialLot As New Label
                                    AddHandler labelSerialLot.DataBinding, AddressOf BindSerialLotNo
                                    Container.Controls.Add(labelSerialLot)
                                End If

                            ElseIf OrigDBColumnName = "PickPackShip" Then
                                Dim ulMain As New HtmlGenericControl("ul")
                                ulMain.Attributes.Add("class", "list-inline")

                                Dim li1Main As New HtmlGenericControl("li")
                                li1Main.Attributes.Add("class", "psscol1")
                                Dim li2Main As New HtmlGenericControl("li")
                                li2Main.Attributes.Add("class", "psscol2")
                                Dim li3Main As New HtmlGenericControl("li")
                                li3Main.Attributes.Add("class", "psscol3")
                                Dim li4Main As New HtmlGenericControl("li")
                                li4Main.Attributes.Add("class", "psscol4")

                                'COL 1
                                Dim ul As New HtmlGenericControl("ul")
                                ul.Attributes.Add("class", "list-inline")
                                Dim li1 As New HtmlGenericControl("li")
                                Dim li2 As New HtmlGenericControl("li")

                                Dim hfRPackagingSlipQty As New HiddenField
                                hfRPackagingSlipQty.ID = "hfRPackagingSlipQty"
                                li1.Controls.Add(hfRPackagingSlipQty)

                                Dim lblRPackagingSlipQty As New Label
                                lblRPackagingSlipQty.ID = "lblRPackagingSlipQty"
                                lblRPackagingSlipQty.Visible = False
                                li1.Controls.Add(lblRPackagingSlipQty)

                                Dim lblCPackagingSlipQty As New Label
                                lblCPackagingSlipQty.ID = "lblCPackagingSlipQty"
                                lblCPackagingSlipQty.Attributes.Add("style", "line-height:34px;")
                                li1.Controls.Add(lblCPackagingSlipQty)

                                ul.Controls.Add(li1)
                                li1Main.Controls.Add(ul)

                                Dim litCPackagingSlipQty As New Literal
                                litCPackagingSlipQty.ID = "litCPackagingSlipQty"
                                li1Main.Controls.Add(litCPackagingSlipQty)

                                'COL 2
                                ul = New HtmlGenericControl("ul")
                                ul.Attributes.Add("class", "list-inline")
                                li1 = New HtmlGenericControl("li")
                                li2 = New HtmlGenericControl("li")

                                Dim lblCFullFilmentqty As New Label
                                lblCFullFilmentqty.ID = "lblCFullFilmentqty"
                                li1.Controls.Add(lblCFullFilmentqty)

                                Dim lblRFullFilmentQty As New Label
                                lblRFullFilmentQty.ID = "lblRFullFilmentQty"
                                lblRFullFilmentQty.Visible = False
                                li1.Controls.Add(lblRFullFilmentQty)

                                Dim txtFullFIlmentQty As New TextBox
                                txtFullFIlmentQty.CssClass = "form-control"
                                txtFullFIlmentQty.ID = "txtFullFIlmentQty"
                                txtFullFIlmentQty.Style.Add("min-width", "50px")
                                txtFullFIlmentQty.Style.Add("width", "75px")
                                li2.Controls.Add(txtFullFIlmentQty)

                                Dim hfRFullFilmentQty As New HiddenField
                                hfRFullFilmentQty.ID = "hfRFullFilmentQty"

                                Dim hfRFullFilmentQtyWithAllocationCheck As New HiddenField
                                hfRFullFilmentQtyWithAllocationCheck.ID = "hfRFullFilmentQtyWithAllocationCheck"
                                li2.Controls.Add(hfRFullFilmentQty)
                                li2.Controls.Add(hfRFullFilmentQtyWithAllocationCheck)

                                ul.Controls.Add(li1)
                                ul.Controls.Add(li2)
                                li2Main.Controls.Add(ul)

                                Dim litCFullFilmentqty As New Literal
                                litCFullFilmentqty.ID = "litCFullFilmentqty"
                                li2Main.Controls.Add(litCFullFilmentqty)

                                'COL 3
                                ul = New HtmlGenericControl("ul")
                                ul.Attributes.Add("class", "list-inline")
                                li1 = New HtmlGenericControl("li")
                                li2 = New HtmlGenericControl("li")

                                Dim lblCInvoicedqty As New Label
                                lblCInvoicedqty.ID = "lblCInvoicedqty"
                                li1.Controls.Add(lblCInvoicedqty)

                                Dim lblRInvoicedQty As New Label
                                lblRInvoicedQty.ID = "lblRInvoicedQty"
                                lblRInvoicedQty.Visible = False
                                li1.Controls.Add(lblRInvoicedQty)

                                Dim txtInvoicedQty As New TextBox
                                txtInvoicedQty.CssClass = "form-control"
                                txtInvoicedQty.ID = "txtInvoicedQty"
                                txtInvoicedQty.Style.Add("min-width", "50px")
                                txtInvoicedQty.Style.Add("width", "75px")
                                li2.Controls.Add(txtInvoicedQty)

                                Dim hfRInvoicedQty As New HiddenField
                                hfRInvoicedQty.ID = "hfRInvoicedQty"
                                li2.Controls.Add(hfRInvoicedQty)

                                ul.Controls.Add(li1)
                                ul.Controls.Add(li2)
                                li3Main.Controls.Add(ul)

                                Dim litCInvoicedqty As New Literal
                                litCInvoicedqty.ID = "litCInvoicedqty"
                                li3Main.Controls.Add(litCInvoicedqty)

                                Dim chkPickPackShip As New CheckBox
                                chkPickPackShip.ID = "chkPickPackShip"
                                li4Main.Controls.Add(chkPickPackShip)

                                ulMain.Controls.Add(li1Main)
                                ulMain.Controls.Add(li2Main)
                                ulMain.Controls.Add(li3Main)
                                ulMain.Controls.Add(li4Main)
                                Container.Controls.Add(ulMain)
                            ElseIf OrigDBColumnName = "bitAsset" Then
                                Dim txtDYRentalIN As New TextBox
                                Dim btnDYRentalIN As New Button

                                Dim txtDYRentalOut As New TextBox
                                Dim btnDYRentalOut As New Button

                                Dim txtDYRentalLost As New TextBox
                                Dim btnDYRentalLost As New Button

                                txtDYRentalIN.ID = "txtDYRentalIN"
                                txtDYRentalOut.ID = "txtDYRentalOut"
                                txtDYRentalLost.ID = "txtDYRentalLost"
                                btnDYRentalIN.ID = "btnDYRentalIN"
                                btnDYRentalOut.ID = "btnDYRentalOut"
                                btnDYRentalLost.ID = "btnDYRentalLost"

                                txtDYRentalIN.Width = 45
                                txtDYRentalOut.Width = 45
                                txtDYRentalLost.Width = 45
                                btnDYRentalIN.Width = 45
                                btnDYRentalOut.Width = 45
                                btnDYRentalLost.Width = 45

                                btnDYRentalIN.CssClass = "button"
                                btnDYRentalOut.CssClass = "button"
                                btnDYRentalLost.CssClass = "button"

                                btnDYRentalIN.Text = "IN"
                                btnDYRentalOut.Text = "Out"
                                btnDYRentalLost.Text = "Lost"

                                'btnDYRentalIN.CommandArgument=

                                Dim bf As New ButtonField()
                                bf.Text = "Details"

                                bf.ButtonType = WebControls.ButtonType.Button

                                'Container.Controls.Add(bf.ButtonType.Button)

                                Container.Controls.Add(txtDYRentalIN)
                                Container.Controls.Add(btnDYRentalIN)
                                Container.Controls.Add(txtDYRentalOut)

                                Container.Controls.Add(btnDYRentalOut)
                                Container.Controls.Add(txtDYRentalLost)
                                Container.Controls.Add(btnDYRentalLost)

                                'btnDYRentalLost = CType(e.Row.FindControl("btnDYRentalLost"), Button)

                                Dim lblPendingRentalOut As Label = New Label()
                                AddHandler lblPendingRentalOut.DataBinding, AddressOf BindPendingRentalOut
                                Container.Controls.Add(lblPendingRentalOut)

                                Dim lblPendingRentalIN As Label = New Label()
                                AddHandler lblPendingRentalIN.DataBinding, AddressOf BindPendingRentalIN
                                Container.Controls.Add(lblPendingRentalIN)

                                Dim lblPendingRentalLost As Label = New Label()
                                AddHandler lblPendingRentalLost.DataBinding, AddressOf BindPendingRentalLost
                                Container.Controls.Add(lblPendingRentalLost)


                            ElseIf OrigDBColumnName = "numProjectID" Or OrigDBColumnName = "numClassID" Then
                                Dim ddl As New DropDownList
                                ddl.CssClass = "form-control"
                                AddHandler ddl.DataBinding, AddressOf BindDropDown
                                Container.Controls.Add(ddl)

                                If OrigDBColumnName = "numProjectID" Then
                                    Dim lbl2 As Label = New Label()
                                    lbl2.ID = "imgProject"
                                    lbl2.Text = "&nbsp;<img src='../images/arrow_turn_right.png' align=""BASELINE"" style=""float:none;vertical-align:middle""/>"
                                    Container.Controls.Add(lbl2)
                                End If

                            ElseIf OrigDBColumnName = "IsArchieve" Then
                                Dim chk As New CheckBox
                                chk.ID = "chk" & OrigDBColumnName
                                'chk.Checked = False
                                Container.Controls.Add(chk)
                                AddHandler chk.DataBinding, AddressOf BindIsArchive

                            ElseIf OrigDBColumnName = "EDIFields" Then
                                objCommon.DomainID = Session("DomainID")
                                Dim dtbitEDI As DataTable = objCommon.GetDomainSettingValue("bitEDI")

                                If dtbitEDI Is Nothing Or dtbitEDI.Rows.Count = 0 Then
                                    Throw New Exception("Not able to fetch bit EDI global settings value.")
                                    Exit Sub
                                ElseIf CCommon.ToShort(dtbitEDI.Rows(0)("bitEDI")) = 1 Then
                                    Dim lnkEdi As New LinkButton
                                    lnkEdi.ClientIDMode = UI.ClientIDMode.AutoID
                                    AddHandler lnkEdi.DataBinding, AddressOf BindEDIOppItemLink
                                    Container.Controls.Add(lnkEdi)
                                End If
                            ElseIf OrigDBColumnName = "bitBarcodePrint" Then
                                cell.Style.Add("text-align", "center")

                                Dim chk As New CheckBox
                                chk.ID = "chkSelectPrint"
                                Container.Controls.Add(chk)
                            ElseIf OrigDBColumnName = "vcGroupNonDbField" Then
                                Dim btnWinningBid As New Button
                                btnWinningBid.ID = "btnWinningBid"
                                btnWinningBid.Visible = False
                                btnWinningBid.Text = "Winning Bid"
                                btnWinningBid.CssClass = "btn btn-primary"
                                btnWinningBid.CommandName = "WinningBid"
                                btnWinningBid.Attributes.Add("onclick", "return SetAsWinningBid(this);")
                                Container.Controls.Add(btnWinningBid)

                                Dim ddlParentKit As New DropDownList
                                ddlParentKit.ID = "ddlParentKit"
                                ddlParentKit.CssClass = "form-control"
                                ddlParentKit.AutoPostBack = False
                                ddlParentKit.Visible = False
                                ddlParentKit.Attributes.Add("onChange", "return UpdateParentKit(this);")
                                Container.Controls.Add(ddlParentKit)
                            ElseIf OrigDBColumnName = "numOnOrder" And FormID = 129 Then
                                Dim link As New HyperLink
                                link.ID = "hplOnOrder129"
                                Container.Controls.Add(link)
                            ElseIf ControlType = "DateField" Then
                                Dim table As New HtmlGenericControl("table")
                                Dim tr As New HtmlGenericControl("tr")
                                Dim td1 As New HtmlGenericControl("td")
                                Dim radDatePicker As New RadDatePicker
                                radDatePicker.CssClass = "ItemReleaseDate"
                                radDatePicker.Width = New Unit(100, UnitType.Pixel)
                                AddHandler radDatePicker.DataBinding, AddressOf CustomDateColumn
                                radDatePicker.DateInput.DateFormat = "MM/dd/yyyy"
                                td1.Controls.Add(radDatePicker)
                                tr.Controls.Add(td1)
                                table.Controls.Add(tr)
                                Container.Controls.Add(table)
                                'Added By :Sachin sadhu ||Date:18thJul2014
                                'Purpose :To Save Custom Fields   
                            ElseIf Custom = True Then

                                If ControlType = "SelectBox" Then
                                    Dim ddl As New DropDownList
                                    ddl.CssClass = "form-control"
                                    AddHandler ddl.DataBinding, AddressOf BindCustDropDown
                                    Container.Controls.Add(ddl)
                                ElseIf ControlType = "TextBox" Then
                                    Dim txt As New TextBox
                                    txt.CssClass = "form-control"
                                    AddHandler txt.DataBinding, AddressOf CreateTexBox
                                    Container.Controls.Add(txt)
                                ElseIf ControlType = "TextArea" Then
                                    Dim txtarea As New TextBox
                                    txtarea.CssClass = "form-control"
                                    AddHandler txtarea.DataBinding, AddressOf CreateTextArea
                                    Container.Controls.Add(txtarea)
                                ElseIf ControlType = "CheckBox" Then
                                    Dim chk As New CheckBox
                                    AddHandler chk.DataBinding, AddressOf CreateCheckBox
                                    Container.Controls.Add(chk)
                                ElseIf ControlType = "DateField" Then
                                    Dim radDatePicker As New RadDatePicker
                                    AddHandler radDatePicker.DataBinding, AddressOf CustomDateColumn
                                    radDatePicker.DateInput.DateFormat = "MM/dd/yyyy"
                                    Container.Controls.Add(radDatePicker)
                                ElseIf ControlType = "Link" Then
                                    Dim link As New HyperLink
                                    AddHandler link.DataBinding, AddressOf CreateLink
                                    Container.Controls.Add(link)
                                End If
                                'end of code
                            Else
                                AddHandler lbl1.DataBinding, AddressOf BindStringColumn
                                Container.Controls.Add(lbl1)
                            End If
                        Else
                            Dim chk As New CheckBox
                            chk.ID = "chkSelect"
                            chk.CssClass = "chkSelect"
                            Container.Controls.Add(chk)

                            Dim hfItemCode As HiddenField = New HiddenField()
                            AddHandler hfItemCode.DataBinding, AddressOf BindItemCode
                            Container.Controls.Add(hfItemCode)

                            Dim hfOppItemtCode As HiddenField = New HiddenField()
                            AddHandler hfOppItemtCode.DataBinding, AddressOf BindOppItemtCode
                            Container.Controls.Add(hfOppItemtCode)

                            Dim hfUnitHour As HiddenField = New HiddenField()
                            AddHandler hfUnitHour.DataBinding, AddressOf BindUnitHour
                            Container.Controls.Add(hfUnitHour)

                            Dim hfUOMConversionFactor As HiddenField = New HiddenField()
                            AddHandler hfUOMConversionFactor.DataBinding, AddressOf BindUOMConversionFactor
                            Container.Controls.Add(hfUOMConversionFactor)

                            Dim hfQtyShipped As HiddenField = New HiddenField()
                            AddHandler hfQtyShipped.DataBinding, AddressOf BindQtyShipped
                            Container.Controls.Add(hfQtyShipped)

                            Dim hfUnitHourReceived As HiddenField = New HiddenField()
                            AddHandler hfUnitHourReceived.DataBinding, AddressOf BindUnitHourReceived
                            Container.Controls.Add(hfUnitHourReceived)

                            Dim hfProjectID As HiddenField = New HiddenField()
                            AddHandler hfProjectID.DataBinding, AddressOf BindProjectID
                            Container.Controls.Add(hfProjectID)

                            Dim hfQty As HiddenField = New HiddenField()
                            AddHandler hfQty.DataBinding, AddressOf BindQty
                            Container.Controls.Add(hfQty)

                            Dim hfbitLotNo As HiddenField = New HiddenField()
                            AddHandler hfbitLotNo.DataBinding, AddressOf BindBitLotNo
                            Container.Controls.Add(hfbitLotNo)

                            Dim hfWarehouseItmsID As HiddenField = New HiddenField()
                            AddHandler hfWarehouseItmsID.DataBinding, AddressOf BindWarehouseItmsID
                            Container.Controls.Add(hfWarehouseItmsID)

                            Dim hfWarehouseID As HiddenField = New HiddenField()
                            AddHandler hfWarehouseID.DataBinding, AddressOf BindWarehouseID
                            Container.Controls.Add(hfWarehouseID)

                            Dim hfShipToAddressID As HiddenField = New HiddenField()
                            AddHandler hfShipToAddressID.DataBinding, AddressOf BindShipToAddressID
                            Container.Controls.Add(hfShipToAddressID)

                            'Dim hfIsArchive As HiddenField = New HiddenField()
                            'AddHandler hfIsArchive.DataBinding, AddressOf BindIsArchive
                            'Container.Controls.Add(hfIsArchive)

                            'Added By :Sachin Sadhu ||Date:17thSept2014
                            'Purpose :for Asset/Rental Items task

                            Dim hfRentalIN As HiddenField = New HiddenField()
                            AddHandler hfRentalIN.DataBinding, AddressOf BindRentalIN
                            Container.Controls.Add(hfRentalIN)

                            Dim hfRentalOut As HiddenField = New HiddenField()
                            AddHandler hfRentalOut.DataBinding, AddressOf BindRentalOut
                            Container.Controls.Add(hfRentalOut)

                            Dim hfRentalLost As HiddenField = New HiddenField()
                            AddHandler hfRentalLost.DataBinding, AddressOf BindRentalLost
                            Container.Controls.Add(hfRentalLost)
                        End If
                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Function BindSalesTemplate(ByVal OppType As Integer, ByVal BizDocId As Integer) As DataTable
            Try
                Dim objOppBizDoc As New OppBizDocs
                objOppBizDoc.DomainID = Session("DomainID")
                objOppBizDoc.BizDocId = BizDocId
                objOppBizDoc.OppType = OppType
                objOppBizDoc.byteMode = 0

                Dim dtBizDocTemplate As DataTable = objOppBizDoc.GetBizDocTemplateList()

                Return dtBizDocTemplate

            Catch ex As Exception
                Throw ex
            End Try
        End Function
        'Sub DisplayDynamicFlds(ByVal lngItemCode As Long,)
        '    Try
        '        Dim strDate As String
        '        Dim bizCalendar As UserControl
        '        Dim _myUC_DueDate As PropertyInfo
        '        Dim PreviousRowID As Integer = 0
        '        Dim objRow As HtmlTableRow
        '        Dim objCell As HtmlTableCell
        '        Dim i, k As Integer
        '        Dim dtTable As DataTable
        '        ' Tabstrip3.Items.Clear()
        '        Dim ObjCus As New CustomFields
        '        ObjCus.locId = 5
        '        ObjCus.RecordId = lngItemCode
        '        ObjCus.DomainID = Session("DomainID")
        '        dtTable = ObjCus.GetCustFlds.Tables(0)


        '        If dtTable.Rows.Count > 0 Then
        '            'Main Detail Section
        '            k = 0
        '            objRow = New HtmlTableRow
        '            For i = 0 To dtTable.Rows.Count - 1
        '                If dtTable.Rows(i).Item("TabId") = 0 Then
        '                    If k = 3 Then
        '                        k = 0
        '                        rblCustom.Rows.Add(objRow)
        '                        objRow = New HtmlTableRow
        '                    End If

        '                    objCell = New HtmlTableCell
        '                    objCell.Align = "Right"
        '                    objCell.Attributes.Add("class", "normal1")
        '                    objCell.InnerText = dtTable.Rows(i).Item("fld_label") & " : "
        '                    objCell.Width = "8%"
        '                    objRow.Cells.Add(objCell)
        '                    If dtTable.Rows(i).Item("fld_type") = "TextBox" Then
        '                        objCell = New HtmlTableCell
        '                        objCell.Width = "20%"
        '                        objCell.Align = "left"
        '                        CreateTexBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
        '                    ElseIf dtTable.Rows(i).Item("fld_type") = "SelectBox" Then
        '                        objCell = New HtmlTableCell
        '                        objCell.Width = "20%"
        '                        objCell.Align = "left"
        '                        CreateDropdown(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")), dtTable.Rows(i).Item("numlistid"))
        '                    ElseIf dtTable.Rows(i).Item("fld_type") = "CheckBox" Then
        '                        objCell = New HtmlTableCell
        '                        objCell.Width = "20%"
        '                        objCell.Align = "left"
        '                        CreateChkBox(objRow, objCell, dtTable.Rows(i).Item("fld_id"), CInt(dtTable.Rows(i).Item("Value")))
        '                    ElseIf dtTable.Rows(i).Item("fld_type") = "TextArea" Then
        '                        objCell = New HtmlTableCell
        '                        objCell.Width = "20%"
        '                        objCell.Align = "left"
        '                        CreateTextArea(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("Value"))
        '                    ElseIf dtTable.Rows(i).Item("fld_type") = "DateField" Then
        '                        PreviousRowID = i
        '                        objCell = New HtmlTableCell
        '                        objCell.Width = "20%"
        '                        objCell.Align = "left"
        '                        bizCalendar = LoadControl("../include/calandar.ascx")
        '                        bizCalendar.ID = "cal" & dtTable.Rows(i).Item("fld_id")
        '                        objCell.Controls.Add(bizCalendar)
        '                        objRow.Cells.Add(objCell)
        '                    ElseIf dtTable.Rows(i).Item("fld_type") = "Link" Then
        '                        objCell = New HtmlTableCell
        '                        objCell.Width = "20%"
        '                        objCell.Align = "left"
        '                        CreateLink(objRow, objCell, dtTable.Rows(i).Item("fld_id"), dtTable.Rows(i).Item("vcURL"), lngItemCode, dtTable.Rows(i).Item("fld_label"))
        '                    End If
        '                    k = k + 1
        '                End If

        '            Next
        '            Dim intRowCount As Integer = 0
        '            If k <> 3 Then
        '                For intRowCount = 0 To 2 - k
        '                    '' objRow = New HtmlTableRow
        '                    objCell = New HtmlTableCell
        '                    objCell.Width = "8%"
        '                    objRow.Cells.Add(objCell)
        '                    objCell = New HtmlTableCell
        '                    objCell.Width = "20%"
        '                    objRow.Cells.Add(objCell)
        '                    objCell.Align = "left"
        '                Next
        '            End If
        '            rblCustom.Rows.Add(objRow)

        '        End If
        '        Dim dvCusFields As DataView
        '        dvCusFields = dtTable.DefaultView
        '        dvCusFields.RowFilter = "fld_type='DateField'"

        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub
        Sub BindOpenItem(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim ControlClass As String = ""

                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)

                lbl1.Text = "<a href='javascript:OpenItem(" & DataBinder.Eval(Container.DataItem, "numItemCode").ToString & ")'><img src='../images/tag.png' align=""BASELINE"" style=""float:none;""/></a>"
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindWarehouseItmsID(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim hfWarehouseItmsID As HiddenField = CType(Sender, HiddenField)
                Dim Container As GridViewRow = CType(hfWarehouseItmsID.NamingContainer, GridViewRow)
                hfWarehouseItmsID.ID = "hfWarehouseItmsID"
                hfWarehouseItmsID.Value = If(DataBinder.Eval(Container.DataItem, "numWarehouseItmsID") Is DBNull.Value, 0, DataBinder.Eval(Container.DataItem, "numWarehouseItmsID"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindWarehouseID(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim hfBindWarehouseID As HiddenField = CType(Sender, HiddenField)
                Dim Container As GridViewRow = CType(hfBindWarehouseID.NamingContainer, GridViewRow)
                hfBindWarehouseID.ID = "hfWarehouseID"
                hfBindWarehouseID.Value = If(DataBinder.Eval(Container.DataItem, "numWarehouseID") Is DBNull.Value, 0, DataBinder.Eval(Container.DataItem, "numWarehouseID"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindShipToAddressID(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim hfShipToAddressID As HiddenField = CType(Sender, HiddenField)
                Dim Container As GridViewRow = CType(hfShipToAddressID.NamingContainer, GridViewRow)
                hfShipToAddressID.ID = "hfShipToAddressID"
                hfShipToAddressID.Value = If(DataBinder.Eval(Container.DataItem, "numShipToAddressID") Is DBNull.Value, 0, DataBinder.Eval(Container.DataItem, "numShipToAddressID"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindBitLotNo(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim hfbitLotNo As HiddenField = CType(Sender, HiddenField)
                Dim Container As GridViewRow = CType(hfbitLotNo.NamingContainer, GridViewRow)
                hfbitLotNo.ID = "hfbitLotNo"
                hfbitLotNo.Value = DataBinder.Eval(Container.DataItem, "bitLotNo")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindQty(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim hfQty As HiddenField = CType(Sender, HiddenField)
                Dim Container As GridViewRow = CType(hfQty.NamingContainer, GridViewRow)
                hfQty.ID = "hfQty"
                hfQty.Value = DataBinder.Eval(Container.DataItem, "numQty")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindProjectID(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim hfProjectID As HiddenField = CType(Sender, HiddenField)
                Dim Container As GridViewRow = CType(hfProjectID.NamingContainer, GridViewRow)
                hfProjectID.ID = "hfProjectID"
                hfProjectID.Value = DataBinder.Eval(Container.DataItem, "numProjectID")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindUnitHour(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim hfUnitHour As HiddenField = CType(Sender, HiddenField)
                Dim Container As GridViewRow = CType(hfUnitHour.NamingContainer, GridViewRow)
                hfUnitHour.ID = "hfUnitHour"
                hfUnitHour.Value = DataBinder.Eval(Container.DataItem, "numOriginalUnitHour")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindQtyShipped(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim hfUnitHour As HiddenField = CType(Sender, HiddenField)
                Dim Container As GridViewRow = CType(hfUnitHour.NamingContainer, GridViewRow)
                hfUnitHour.ID = "hfQtyShipped"
                hfUnitHour.Value = DataBinder.Eval(Container.DataItem, "numQtyShipped")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Sub BindUOMConversionFactor(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim hfUnitHour As HiddenField = CType(Sender, HiddenField)
                Dim Container As GridViewRow = CType(hfUnitHour.NamingContainer, GridViewRow)
                hfUnitHour.ID = "hfUOMConversionFactor"
                hfUnitHour.Value = CCommon.ToDouble(DataBinder.Eval(Container.DataItem, "UOMConversionFactor"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Sub BindUnitHourReceived(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim hfUnitHour As HiddenField = CType(Sender, HiddenField)
                Dim Container As GridViewRow = CType(hfUnitHour.NamingContainer, GridViewRow)
                hfUnitHour.ID = "hfUnitHourReceived"
                hfUnitHour.Value = DataBinder.Eval(Container.DataItem, "numUnitHourReceived")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindItemCode(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim hfItemCode As HiddenField = CType(Sender, HiddenField)
                Dim Container As GridViewRow = CType(hfItemCode.NamingContainer, GridViewRow)
                hfItemCode.ID = "hfItemCode"
                hfItemCode.Value = DataBinder.Eval(Container.DataItem, "numItemCode")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindOppItemtCode(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim hfOppItemtCode As HiddenField = CType(Sender, HiddenField)
                Dim Container As GridViewRow = CType(hfOppItemtCode.NamingContainer, GridViewRow)
                hfOppItemtCode.ID = "hfOppItemtCode"
                hfOppItemtCode.Value = DataBinder.Eval(Container.DataItem, "numoppitemtCode")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindTextBox(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim txtBox As TextBox = CType(Sender, TextBox)
                Dim Container As GridViewRow = CType(txtBox.NamingContainer, GridViewRow)
                If (OrigDBColumnName = "ItemRequiredDate") Then
                    txtBox.ID = "txt" & OrigDBColumnName & CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numoppitemtCode"))
                Else
                    txtBox.ID = "txt" & OrigDBColumnName
                End If

                txtBox.CssClass = "form-control"
                'txtBox.Width = New Unit(120)

                If OrigDBColumnName = "vcItemDesc" Or OrigDBColumnName = "vcNotes" Then
                    txtBox.TextMode = TextBoxMode.MultiLine
                    txtBox.Rows = 4
                End If

                If OrigDBColumnName = "numUnitHour" Then
                    txtBox.Text = CCommon.ToDouble(IIf(IsDBNull(DataBinder.Eval(Container.DataItem, OrigDBColumnName)), "", DataBinder.Eval(Container.DataItem, OrigDBColumnName)))
                Else
                    txtBox.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, OrigDBColumnName)), "", DataBinder.Eval(Container.DataItem, OrigDBColumnName))
                End If

                If OrigDBColumnName = "monPriceUOM" Then
                    CCommon.SetValueInDecimalFormat(CCommon.ToDecimal(txtBox.Text), txtBox)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindDropDown(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim ddl As DropDownList = CType(Sender, DropDownList)
                Dim Container As GridViewRow = CType(ddl.NamingContainer, GridViewRow)
                ddl.ID = "ddl" & IIf(OrigDBColumnName = "numProjectID", "Project", "Class")
                ddl.CssClass = "form-control"
                'ddl.Width = New Unit(120)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Added By :sachin sadhu ||Date:17thSept2014
        'Purpose : Assent/Rental Items
        Sub BindRentalIN(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim hfRentalIN As HiddenField = CType(Sender, HiddenField)
                Dim Container As GridViewRow = CType(hfRentalIN.NamingContainer, GridViewRow)
                hfRentalIN.ID = "hfRentalIN"
                hfRentalIN.Value = If(IsDBNull(DataBinder.Eval(Container.DataItem, "numRentalIN")), 0, DataBinder.Eval(Container.DataItem, "numRentalIN"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindRentalOut(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim hfRentalOut As HiddenField = CType(Sender, HiddenField)
                Dim Container As GridViewRow = CType(hfRentalOut.NamingContainer, GridViewRow)
                hfRentalOut.ID = "hfRentalOut"
                'hfRentalOut.Value = CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numRentalOut")) - (CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numRentalLost") + CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numRentalIN"))))
                hfRentalOut.Value = If(IsDBNull(DataBinder.Eval(Container.DataItem, "numRentalOut")), 0, DataBinder.Eval(Container.DataItem, "numRentalOut"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub BindRentalLost(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim hfRentalLost As HiddenField = CType(Sender, HiddenField)
                Dim Container As GridViewRow = CType(hfRentalLost.NamingContainer, GridViewRow)
                hfRentalLost.ID = "hfRentalLost"
                hfRentalLost.Value = If(IsDBNull(DataBinder.Eval(Container.DataItem, "numRentalLost")), 0, DataBinder.Eval(Container.DataItem, "numRentalLost"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindPendingRentalOut(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lblPendingRentalOut As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lblPendingRentalOut.NamingContainer, GridViewRow)
                lblPendingRentalOut.ID = "lblPendingRentalOut"
                lblPendingRentalOut.Text = "Pending to Out-" & (CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numOriginalUnitHour")) - CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numRentalOut")))
                lblPendingRentalOut.ForeColor = Color.Green

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindPendingRentalIN(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lblPendingRentalIN As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lblPendingRentalIN.NamingContainer, GridViewRow)
                lblPendingRentalIN.ID = "lblPendingRentalIN"
                lblPendingRentalIN.Text = ", IN-" & (CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numRentalOut")) - (CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numRentalIN")) + CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numRentalLost"))))
                lblPendingRentalIN.ForeColor = Color.Green
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub BindPendingRentalLost(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lblPendingRentalLost As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lblPendingRentalLost.NamingContainer, GridViewRow)
                lblPendingRentalLost.ID = "lblPendingRentalLost"
                lblPendingRentalLost.Text = ", Lost-" & (CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numRentalOut")) - (CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numRentalIN")) + CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numRentalLost"))))
                lblPendingRentalLost.ForeColor = Color.Green
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        'end of code--Sachin

        'Added By :Sachin sadhu ||Date:18thJul2014
        'Purpose :To Save Custom Fields
        Sub BindCustDropDown(ByVal Sender As Object, ByVal e As EventArgs)
            Try

                Dim dtTableCust As DataTable
                Dim ObjCus As New CustomFields
                ObjCus.locId = 5
                ObjCus.RecordId = 0
                ObjCus.DomainID = HttpContext.Current.Session("DomainID")
                dtTableCust = ObjCus.GetCustFlds.Tables(0)
                If dtTableCust.Rows.Count > 0 Then
                    For i = 0 To dtTableCust.Rows.Count - 1
                        If OrigDBColumnName = dtTableCust.Rows(i).Item("fld_label") Then
                            Dim ddl As DropDownList = CType(Sender, DropDownList)
                            Dim Container As GridViewRow = CType(ddl.NamingContainer, GridViewRow)
                            ddl.ID = dtTableCust.Rows(i).Item("fld_id")
                            ddl.CssClass = "form-control"
                            'ddl.Width = New Unit(120)
                        End If


                    Next
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Sub CreateTexBox(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim dtTableCust As DataTable
                Dim ObjCus As New CustomFields
                ObjCus.locId = 5
                ObjCus.RecordId = 0
                ObjCus.DomainID = HttpContext.Current.Session("DomainID")
                dtTableCust = ObjCus.GetCustFlds.Tables(0)
                If dtTableCust.Rows.Count > 0 Then
                    For i = 0 To dtTableCust.Rows.Count - 1
                        If OrigDBColumnName = dtTableCust.Rows(i).Item("fld_label") Then
                            Dim txt As TextBox = CType(Sender, TextBox)
                            Dim Container As GridViewRow = CType(txt.NamingContainer, GridViewRow)
                            txt.ID = dtTableCust.Rows(i).Item("fld_id")
                            txt.CssClass = "form-control"
                            'txt.Width = New Unit(120)
                        End If
                    Next
                End If


            Catch ex As Exception
                Throw ex
            End Try


        End Sub
        Public Sub CreateTextArea(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim dtTableCust As DataTable
                Dim ObjCus As New CustomFields
                ObjCus.locId = 5
                ObjCus.RecordId = 0
                ObjCus.DomainID = HttpContext.Current.Session("DomainID")
                dtTableCust = ObjCus.GetCustFlds.Tables(0)
                If dtTableCust.Rows.Count > 0 Then
                    For i = 0 To dtTableCust.Rows.Count - 1
                        If OrigDBColumnName = dtTableCust.Rows(i).Item("fld_label") Then
                            Dim txt As TextBox = CType(Sender, TextBox)
                            Dim Container As GridViewRow = CType(txt.NamingContainer, GridViewRow)
                            txt.ID = dtTableCust.Rows(i).Item("fld_id")
                            txt.CssClass = "form-control"
                            'txt.Width = New Unit(200)
                            txt.Height = New Unit(40)
                            txt.TextMode = TextBoxMode.MultiLine
                        End If
                    Next
                End If


            Catch ex As Exception
                Throw ex
            End Try


        End Sub

        Public Sub CreateLabel(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim dtTableCust As DataTable
                Dim ObjCus As New CustomFields
                ObjCus.locId = 5
                ObjCus.RecordId = 0
                ObjCus.DomainID = HttpContext.Current.Session("DomainID")
                dtTableCust = ObjCus.GetCustFlds.Tables(0)
                If dtTableCust.Rows.Count > 0 Then
                    For i = 0 To dtTableCust.Rows.Count - 1
                        If OrigDBColumnName = dtTableCust.Rows(i).Item("fld_label") Then
                            Dim lbl As Label = CType(Sender, Label)
                            Dim Container As GridViewRow = CType(lbl.NamingContainer, GridViewRow)
                            lbl.ID = dtTableCust.Rows(i).Item("fld_id")
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw ex
            End Try


        End Sub

        Public Sub CreateCheckBox(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim dtTableCust As DataTable
                Dim ObjCus As New CustomFields
                ObjCus.locId = 5
                ObjCus.RecordId = 0
                ObjCus.DomainID = HttpContext.Current.Session("DomainID")
                dtTableCust = ObjCus.GetCustFlds.Tables(0)
                If dtTableCust.Rows.Count > 0 Then
                    For i = 0 To dtTableCust.Rows.Count - 1
                        If OrigDBColumnName = dtTableCust.Rows(i).Item("fld_label") Then
                            Dim chk As CheckBox = CType(Sender, CheckBox)
                            Dim Container As GridViewRow = CType(chk.NamingContainer, GridViewRow)
                            chk.ID = dtTableCust.Rows(i).Item("fld_id")

                        End If
                    Next
                End If


            Catch ex As Exception
                Throw ex
            End Try

        End Sub



        Public Function CreateLink(ByVal Sender As Object, ByVal e As EventArgs)

            Try
                Dim dtTableCust As DataTable
                Dim ObjCus As New CustomFields
                ObjCus.locId = 5
                ObjCus.RecordId = 0
                ObjCus.DomainID = HttpContext.Current.Session("DomainID")
                dtTableCust = ObjCus.GetCustFlds.Tables(0)
                If dtTableCust.Rows.Count > 0 Then
                    For i = 0 To dtTableCust.Rows.Count - 1
                        If OrigDBColumnName = dtTableCust.Rows(i).Item("fld_label") Then
                            Dim hpl As HyperLink = CType(Sender, HyperLink)
                            Dim Container As GridViewRow = CType(hpl.NamingContainer, GridViewRow)
                            hpl.ID = dtTableCust.Rows(i).Item("fld_id")
                            hpl.Text = dtTableCust.Rows(i).Item("fld_label")
                            hpl.Target = "_blank"
                            hpl.CssClass = "hyperlink"
                            hpl.NavigateUrl = dtTableCust.Rows(i).Item("vcURL")
                        End If
                    Next
                End If


            Catch ex As Exception
                Throw ex
            End Try
            'Dim hpl As New HyperLink
            'hpl.ID = "hpl" & strId
            'hpl.Text = Label
            'URL = URL.Replace("RecordID", RecID)
            'hpl.NavigateUrl = URL
            'hpl.Target = "_blank"
            'hpl.CssClass = "hyperlink"
            'tblCell.Controls.Add(hpl)
            'tblRow.Cells.Add(tblCell)
        End Function

        Public Sub CreateCalendar(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim dtTableCust As DataTable
                Dim ObjCus As New CustomFields
                ObjCus.locId = 5
                ObjCus.RecordId = 0
                ObjCus.DomainID = HttpContext.Current.Session("DomainID")
                dtTableCust = ObjCus.GetCustFlds.Tables(0)
                If dtTableCust.Rows.Count > 0 Then
                    For i = 0 To dtTableCust.Rows.Count - 1
                        If OrigDBColumnName = dtTableCust.Rows(i).Item("fld_label") Then
                            Dim bizCalendar As New UserControl
                            'Dim chk As CheckBox = CType(Sender, CheckBox)
                            bizCalendar = LoadControl("~/include/calandar.ascx")
                            'bizCalendar.ID = "cal" & dtTable.Rows(i).Item("fld_id")
                            bizCalendar.ID = dtTableCust.Rows(i).Item("fld_id")

                            Dim Container As GridViewRow = CType(bizCalendar.NamingContainer, GridViewRow)
                            'chk.ID = dtTableCust.Rows(i).Item("fld_id")

                        End If
                    Next
                End If


            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        'End of Sachin Code
        Public Function ReturnName(ByVal SDate) As String
            Try
                Dim strDate As String = ""
                If strDate = "" Then
                Else
                    If Not IsDBNull(SDate) Then
                        strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                        If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                            strDate = "<font color=red>" & strDate & "</font>"
                        ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                            strDate = "<font color=orange>" & strDate & "</font>"
                        End If
                    End If
                End If



                Return strDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Sub BindStringColumn(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim ControlClass As String = ""

                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)

                If OrigDBColumnName = "vcItemReleaseDate" Then
                    lbl1.ID = "lblItemReleaseDate" & CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numoppitemtCode"))
                Else
                    lbl1.ID = "lbl" & OrigDBColumnName
                End If

                If ControlType = "CheckBox" Then
                    lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, OrigDBColumnName).ToString), "", IIf(CCommon.ToBool(DataBinder.Eval(Container.DataItem, OrigDBColumnName)), "Yes", "No").ToString).ToString
                ElseIf OrigDBColumnName = "vcPathForTImage" Then
                    lbl1.Text = String.Format("<div onclick='EditImage({0}, {1})' style='cursor: Pointer'>{2}</div>",
                                              DataBinder.Eval(Container.DataItem, "numOppId"), DataBinder.Eval(Container.DataItem, "numoppitemtCode"), CCommon.GetImageHTML(DataBinder.Eval(Container.DataItem, "vcPathForTImage"), 0))
                ElseIf OrigDBColumnName = "vcProjectStageName" Then
                    lbl1.Text = String.Format(" <a href='#' class='hyperlink' style='text-decoration: underline;' onclick='OpenProjectStageDetail({0},{1})'>{2}</a>",
                                              DataBinder.Eval(Container.DataItem, "numProjectID"), DataBinder.Eval(Container.DataItem, "numProjectStageID"), DataBinder.Eval(Container.DataItem, "vcProjectStageName"))
                ElseIf OrigDBColumnName = "vcShippedReceived" Then
                    lbl1.Text = CCommon.GetDecimalFormat(CCommon.ToDouble(DataBinder.Eval(Container.DataItem, OrigDBColumnName)))
                ElseIf FieldDataType = "M" Then
                    lbl1.Text = CCommon.GetDecimalFormat(DataBinder.Eval(Container.DataItem, OrigDBColumnName))
                Else
                    lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, OrigDBColumnName)), "", DataBinder.Eval(Container.DataItem, OrigDBColumnName)).ToString
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function ItemReleaseDateColumn(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim radDatePicker As RadDatePicker = CType(Sender, RadDatePicker)
                Dim Container As GridViewRow = CType(radDatePicker.NamingContainer, GridViewRow)
                radDatePicker.ID = "rdpItemReleaseDate" & CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numoppitemtCode"))
                radDatePicker.ClientEvents.OnDateSelected = "OnItemReleaseDateSelected"
                radDatePicker.Attributes.Add("OppItemID", CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numoppitemtCode")))
                radDatePicker.Attributes.Add("OppID", CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numOppId")))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ItemRequiredDateColumn(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim radDatePicker As RadDatePicker = CType(Sender, RadDatePicker)
                Dim Container As GridViewRow = CType(radDatePicker.NamingContainer, GridViewRow)

                If Not IsDBNull(DataBinder.Eval(Container.DataItem, OrigDBColumnName)) Then
                    radDatePicker.SelectedDate = CCommon.ToSqlDate(DataBinder.Eval(Container.DataItem, OrigDBColumnName))
                End If
            Catch ex As Exception
                Throw
            End Try
        End Function



        Public Function CustomDateColumn(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim radDatePicker As RadDatePicker = CType(Sender, RadDatePicker)
                Dim Container As GridViewRow = CType(radDatePicker.NamingContainer, GridViewRow)
                radDatePicker.ID = "rdpCustomDateColumn" & FormFieldId & CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numoppitemtCode"))
                If (String.IsNullOrWhiteSpace(DataBinder.Eval(Container.DataItem, OrigDBColumnName)) Or DataBinder.Eval(Container.DataItem, OrigDBColumnName) = "0") Then
                    radDatePicker.SelectedDate = CCommon.ToString(Session("OrderCreatedDate"))
                Else
                    radDatePicker.SelectedDate = CCommon.ToSqlDate(DataBinder.Eval(Container.DataItem, OrigDBColumnName))
                End If
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ReturnMoney(ByVal Money)
            Try
                If Not IsDBNull(Money) Then Return String.Format("{0:#,###.0000}", Money)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Sub BindIsArchive(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim chk As CheckBox = CType(Sender, CheckBox)
                Dim Container As GridViewRow = CType(chk.NamingContainer, GridViewRow)
                chk.ID = "chkIsArchive"
                chk.Checked = CCommon.ToBool(DataBinder.Eval(Container.DataItem, "IsArchieve"))

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindEDIOppItemLink(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lnkEDI As LinkButton = CType(Sender, LinkButton)
                Dim Container As GridViewRow = CType(lnkEDI.NamingContainer, GridViewRow)
                lnkEDI.ID = "LinkOppItem" & OrigDBColumnName
                lnkEDI.Text = "EDI Fields"
                lnkEDI.CssClass = "hyperlink"
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Protected Sub GridInventory_Command(sender As Object, e As CommandEventArgs, OppID As String)
            Try

                Dim objItem As New CItems
                If e.CommandName = "RentalOut" Then

                    Dim row As GridViewRow = DirectCast(DirectCast(sender, Button).NamingContainer, GridViewRow)
                    Dim txtRentalOut As TextBox = TryCast(row.FindControl("txtRentalOut"), TextBox)

                    objItem.ItemCode = CCommon.ToLong(e.CommandArgument)
                    objItem.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    'objItem.OnAllocation = CCommon.ToLong(CType(e.FindControl("txtSerialLotNo"), TextBox).Text)
                    objItem.OnAllocation = CCommon.ToDouble(txtRentalOut.Text)
                    objItem.OppId = CCommon.ToLong(OppID)
                    objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                    ' Response.Write(txtRentalOut.Text)
                    objItem.UpdateInventoryRentalAsset(0)

                ElseIf e.CommandName = "RentalIN" Then
                    Dim row As GridViewRow = DirectCast(DirectCast(sender, Button).NamingContainer, GridViewRow)
                    Dim txtRentalIN As TextBox = TryCast(row.FindControl("txtRentalIN"), TextBox)

                    objItem.ItemCode = CCommon.ToLong(e.CommandArgument)
                    objItem.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objItem.OnAllocation = CCommon.ToDouble(txtRentalIN.Text)
                    objItem.OppId = CCommon.ToLong(OppID)
                    objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                    ' Response.Write(txtRentalIN.Text)
                    objItem.UpdateInventoryRentalAsset(1)
                    '  BindDataGrid()
                ElseIf e.CommandName = "RentalLost" Then
                    Dim row As GridViewRow = DirectCast(DirectCast(sender, Button).NamingContainer, GridViewRow)
                    Dim txtRentalIN As TextBox = TryCast(row.FindControl("txtRentalLost"), TextBox)

                    objItem.ItemCode = CCommon.ToLong(e.CommandArgument)
                    objItem.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objItem.OnAllocation = CCommon.ToDouble(txtRentalIN.Text)
                    objItem.OppId = CCommon.ToLong(OppID)
                    objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                    ' Response.Write(txtRentalIN.Text)
                    objItem.UpdateInventoryRentalAsset(2)
                    '  BindDataGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Throw
            End Try




        End Sub
        'sachin

        'end of sachin

        Sub BindSerialLotAddLink(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim hplAdd As HyperLink = CType(Sender, HyperLink)
                Dim Container As GridViewRow = CType(hplAdd.NamingContainer, GridViewRow)

                Dim bitSerialized As Boolean = CCommon.ToBool(DataBinder.Eval(Container.DataItem, "bitSerialized"))
                Dim bitLotNo As Boolean = CCommon.ToBool(DataBinder.Eval(Container.DataItem, "bitLotNo"))

                If bitSerialized Or bitLotNo Then
                    hplAdd.Text = "Added (" & CCommon.ToString(DataBinder.Eval(Container.DataItem, "SerialLotNo")) & ")"
                    Dim oppID As Long = CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numOppID"))
                    Dim oppItemID As Long = CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numoppitemtCode"))
                    Dim itemID As Long = CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numItemCode"))
                    Dim warehouseItemID As Long = CCommon.ToLong(DataBinder.Eval(Container.DataItem, "numWarehouseItmsID"))
                    Dim qty As Integer = CCommon.ToDecimal(DataBinder.Eval(Container.DataItem, "numOriginalUnitHour"))
                    Dim itemType As String

                    If bitSerialized Then
                        itemType = "Serial"
                    ElseIf bitLotNo Then
                        itemType = "Lot"
                    End If

                    hplAdd.Attributes.Add("onclick", "OpenSertailLot(" & oppID & "," & oppItemID & "," & itemID & "," & warehouseItemID & ",'" & itemType & "'," & qty & ")")
                Else
                    hplAdd.Visible = False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindSerialLotNo(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim labelSerialLotNo As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(labelSerialLotNo.NamingContainer, GridViewRow)

                Dim bitSerialized As Boolean = CCommon.ToBool(DataBinder.Eval(Container.DataItem, "bitSerialized"))
                Dim bitLotNo As Boolean = CCommon.ToBool(DataBinder.Eval(Container.DataItem, "bitLotNo"))

                If bitSerialized Or bitLotNo Then
                    labelSerialLotNo.Text = CCommon.ToString(DataBinder.Eval(Container.DataItem, "vcSerialLotNo"))
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub BindWarehouseDropDown(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim radWarehouse As RadComboBox = CType(Sender, RadComboBox)
                radWarehouse.ID = "radWarehouse"
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub BindUOMDropdown(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim dropDown As DropDownList = CType(Sender, DropDownList)
                Dim Container As GridViewRow = CType(dropDown.NamingContainer, GridViewRow)
                dropDown.ID = "ddlUOM"
                dropDown.Attributes.Add("class", "form-control")
                dropDown.ClientIDMode = UI.ClientIDMode.AutoID
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub BindLiteralColumn(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lit As Literal = CType(Sender, Literal)
                Dim Container As GridViewRow = CType(lit.NamingContainer, GridViewRow)

                If OrigDBColumnName = "monPriceUOM" AndAlso lit.ID = "litVendorCostTable" Then
                    lit.Text = CCommon.ToString(DataBinder.Eval(Container.DataItem, "vcCostTableLink"))
                ElseIf OrigDBColumnName = "monPriceUOM" AndAlso lit.ID = "litDynamicCostLastUpdated" Then
                    lit.Text = CCommon.ToString(DataBinder.Eval(Container.DataItem, "vcDynamicCostLastUpdated"))
                ElseIf OrigDBColumnName = "numUnitHour" AndAlso lit.ID = "litQtyAddedToBills" Then
                    If CCommon.ToString(DataBinder.Eval(Container.DataItem, "vcQtyAddedToBills")) <> "" Then
                        lit.Text = "<br/>" & CCommon.ToString(DataBinder.Eval(Container.DataItem, "vcQtyAddedToBills"))
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
    End Class


    Public Class WarehouseTemplate
        Implements ITemplate

        Private templateType As ListItemType

        Sub New(ByVal type As ListItemType)
            Try
                templateType = type
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub InstantiateIn(container As Control) Implements ITemplate.InstantiateIn
            If templateType = ListItemType.Header Then
                Dim ul As New HtmlGenericControl("ul")

                Dim li As HtmlGenericControl

                li = New HtmlGenericControl("li")
                li.Attributes.Add("class", "col1")
                li.InnerText = "Warehouse"
                ul.Controls.Add(li)

                li = New HtmlGenericControl("li")
                li.Attributes.Add("class", "col2")
                li.InnerText = "Attributes"
                ul.Controls.Add(li)

                li = New HtmlGenericControl("li")
                li.Attributes.Add("class", "col3")
                li.InnerText = "On Hand"
                ul.Controls.Add(li)

                li = New HtmlGenericControl("li")
                li.Attributes.Add("class", "col4")
                li.InnerText = "On Order"
                ul.Controls.Add(li)

                li = New HtmlGenericControl("li")
                li.Attributes.Add("class", "col5")
                li.InnerText = "On Allocation"
                ul.Controls.Add(li)

                li = New HtmlGenericControl("li")
                li.Attributes.Add("class", "col6")
                li.InnerText = "BackOrder"
                ul.Controls.Add(li)

                container.Controls.Add(ul)
            ElseIf templateType.Item Or ListItemType.AlternatingItem Then
                Dim ul As New HtmlGenericControl("ul")

                Dim li As HtmlGenericControl

                li = New HtmlGenericControl("li")
                li.Attributes.Add("class", "col1")
                AddHandler li.DataBinding, AddressOf Me.BindWarehouse
                ul.Controls.Add(li)

                li = New HtmlGenericControl("li")
                li.Attributes.Add("class", "col2")
                AddHandler li.DataBinding, AddressOf Me.BindAttribures
                ul.Controls.Add(li)

                li = New HtmlGenericControl("li")
                li.Attributes.Add("class", "col3")
                AddHandler li.DataBinding, AddressOf Me.BindOnHand
                ul.Controls.Add(li)

                li = New HtmlGenericControl("li")
                li.Attributes.Add("class", "col4")
                AddHandler li.DataBinding, AddressOf Me.BindOnOrder
                ul.Controls.Add(li)

                li = New HtmlGenericControl("li")
                li.Attributes.Add("class", "col5")
                AddHandler li.DataBinding, AddressOf Me.BindOnAllocation
                ul.Controls.Add(li)

                li = New HtmlGenericControl("li")
                li.Attributes.Add("class", "col6")
                AddHandler li.DataBinding, AddressOf Me.BindOnBackOrder
                ul.Controls.Add(li)

                container.Controls.Add(ul)
            End If
        End Sub

        Sub BindWarehouse(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim li As HtmlGenericControl = CType(Sender, HtmlGenericControl)
                Dim Container As RadComboBoxItem = CType(li.NamingContainer, RadComboBoxItem)
                li.InnerText = IIf(String.IsNullOrEmpty(CCommon.ToString(DataBinder.Eval(Container.DataItem, "vcWarehouse"))), "-", DataBinder.Eval(Container.DataItem, "vcWarehouse"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindAttribures(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim li As HtmlGenericControl = CType(Sender, HtmlGenericControl)
                Dim Container As RadComboBoxItem = CType(li.NamingContainer, RadComboBoxItem)
                li.InnerText = IIf(String.IsNullOrEmpty(CCommon.ToString(DataBinder.Eval(Container.DataItem, "Attr"))), "-", DataBinder.Eval(Container.DataItem, "Attr"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindOnHand(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim li As HtmlGenericControl = CType(Sender, HtmlGenericControl)
                Dim Container As RadComboBoxItem = CType(li.NamingContainer, RadComboBoxItem)
                li.InnerText = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "numOnHand")), "0", DataBinder.Eval(Container.DataItem, "numOnHand"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindOnOrder(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim li As HtmlGenericControl = CType(Sender, HtmlGenericControl)
                Dim Container As RadComboBoxItem = CType(li.NamingContainer, RadComboBoxItem)
                li.InnerText = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "numOnOrder")), "0", DataBinder.Eval(Container.DataItem, "numOnOrder"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindOnAllocation(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim li As HtmlGenericControl = CType(Sender, HtmlGenericControl)
                Dim Container As RadComboBoxItem = CType(li.NamingContainer, RadComboBoxItem)
                li.InnerText = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "numAllocation")), "0", DataBinder.Eval(Container.DataItem, "numAllocation"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindOnBackOrder(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim li As HtmlGenericControl = CType(Sender, HtmlGenericControl)
                Dim Container As RadComboBoxItem = CType(li.NamingContainer, RadComboBoxItem)
                li.InnerText = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, "numBackOrder")), "0", DataBinder.Eval(Container.DataItem, "numBackOrder"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


    End Class

    Public Class PickPackShipFromTo
        Public Property WarehouseID As Long
        Public Property ShipToAddressID As Long
    End Class

End Namespace
