﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmAddReturn.aspx.vb"
    Inherits="BACRM.UserInterface.Opportunities.frmAddReturn" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <script type="text/javascript">
        function Close(type) {
            window.opener.location = '../Opportunity/frmSalesReturns.aspx?type=' + type;
            window.close()
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table width="100%">
                <tr>
                    <td>
                        <asp:Button ID="btnAddtoReturnQueue" runat="server" CssClass="button" Text="Add to Sales Return Queue & close" />
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="normal4" align="center">
                        <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Return Items
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table cellpadding="0" cellspacing="0" width="500px">
        <tr>
            <td class="normal1">
                <asp:ValidationSummary ID="valSummary" runat="server" DisplayMode="BulletList" ShowMessageBox="true"
                    ShowSummary="false" />
            </td>
        </tr>
        <tr valign="bottom">
            <td>
            </td>
            <td align="right">
            </td>
        </tr>
        <tr valign="top">
            <td colspan="2">
                <asp:Table ID="table3" Width="100%" runat="server" Height="100" GridLines="None"
                    BorderColor="black" CssClass="aspTable" BorderWidth="1" CellSpacing="0" CellPadding="0">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:GridView ID="gvReturns" runat="server" AutoGenerateColumns="false" CssClass="dg"
                                Width="100%" DataKeyNames="numItemCode,monPrice,numoppitemtCode" ClientIDMode="AutoID">
                                <AlternatingRowStyle CssClass="ais" />
                                <RowStyle CssClass="is" />
                                <HeaderStyle CssClass="hs" />
                                <Columns>
                                    <asp:BoundField HeaderText="ItemCode" DataField="numItemCode" Visible="false" />
                                    <asp:BoundField HeaderText="Item to return" DataField="vcItemName" />
                                    <asp:BoundField HeaderText="Item Description" DataField="vcItemDesc" />
                                    <%--<asp:BoundField HeaderText="Serial #" DataField="Serial" />--%>
                                    <asp:BoundField HeaderText="Total Units" DataField="numUnitHour" ItemStyle-HorizontalAlign="Center" />
                                    <asp:TemplateField HeaderText="Qty to Return" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" ID="txtQtyReturnCmp" Text='<%# String.Format("{0:####}", Eval("numUnitHour"))  %>'
                                                Style="display: none"></asp:TextBox>
                                            <asp:TextBox runat="server" ID="txtQtyReturn" Text='<%# String.Format("{0:####}", Eval("numUnitHour")) %>'
                                                Width="50px" CssClass="signup"></asp:TextBox>
                                            <asp:CompareValidator Operator="LessThanEqual" ControlToValidate="txtQtyReturn" ControlToCompare="txtQtyReturnCmp"
                                                Type="Integer" Display="Dynamic" ID="cvQty" runat="server" ErrorMessage="Qty to Return can not be greater that Total Units"
                                                Text="*"></asp:CompareValidator>
                                            <asp:RequiredFieldValidator ID="rfvQty" runat="server" ErrorMessage="Enter Quantity to Return"
                                                Display="Dynamic" Text="*" ControlToValidate="txtQtyReturn"></asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="cvQty1" runat="server" ErrorMessage="Quantity to Return is numeic(1-9)"
                                                Operator="DataTypeCheck" Display="Dynamic" Text="*" ControlToValidate="txtQtyReturn"
                                                Type="Integer"></asp:CompareValidator>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Reason for Return">
                                        <ItemTemplate>
                                            <asp:DropDownList runat="server" ID="ddlReason" CssClass="signup">
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="rfvReason" runat="server" ErrorMessage="Select Reason for Return"
                                                Display="Dynamic" Text="*" ControlToValidate="ddlReason" InitialValue="0"></asp:RequiredFieldValidator>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:DropDownList runat="server" ID="ddlStatus" Enabled="false" CssClass="signup">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--  <asp:TemplateField HeaderText="Reference #">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" ID="txtReference" Width="70px" CssClass="signup"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                            </asp:GridView>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
    </table>
</asp:Content>
