﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Admin

Public Class frmOrderPaymentDetails
    Inherits BACRMPage

    Dim objTrans As New TransactionHistory
    Dim lngOppID As Long
    Dim lngOppBizDocID As Long
    Dim lngDivisionID As Long

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            lngOppID = CCommon.ToLong(GetQueryStringVal("OppID"))
            lngDivisionID = CCommon.ToLong(GetQueryStringVal("DivisionID"))
            'TODO:Add permission Element here 
            ''GetUserRightsForPage(10, 27)

            If Not IsPostBack Then
                LoadTransactionHistory()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub LoadTransactionHistory()
        Try
            Dim dtTransHistory As New DataTable
            If objTrans Is Nothing Then objTrans = New TransactionHistory
            objTrans.TransHistoryID = 0
            objTrans.OppID = lngOppID
            objTrans.DivisionID = lngDivisionID
            objTrans.DomainID = Session("DomainID")
            dtTransHistory = objTrans.GetOrderTransactionHistory()

            If dtTransHistory IsNot Nothing AndAlso dtTransHistory.Rows.Count > 0 Then
                dgPaymentDetails.DataSource = dtTransHistory
                dgPaymentDetails.DataBind()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub dgPaymentDetails_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgPaymentDetails.ItemCommand
        Try
            'TODO:Put actual card number pending to be done by manish
            Dim ResponseMessage As String = String.Empty
            Dim objPaymentGateway As New PaymentGateway
            objPaymentGateway.DomainID = Session("DomainID")

            Dim dblTransHistoryID As Decimal = CCommon.ToDecimal(DirectCast(e.Item.FindControl("hdnTransHistoryID"), HiddenField).Value)
            Dim strTransactionID As String = CCommon.ToString(DirectCast(e.Item.FindControl("hdnTransactionID"), HiddenField).Value)
            Dim dblAuthorizeAmt As Decimal = CCommon.ToDecimal(DirectCast(e.Item.FindControl("lblAuthorizeAmt"), Label).Text)
            Dim dblCaptureAmount As Decimal = CCommon.ToDecimal(DirectCast(e.Item.FindControl("lblCapturedAmt"), Label).Text)
            Dim dblCreditAmount As Decimal = CCommon.ToDecimal(DirectCast(e.Item.FindControl("lblRefundAmt"), Label).Text)
            Dim strCardNo As String = objCommon.Decrypt(CCommon.ToString(DirectCast(e.Item.FindControl("hdnCardNo"), HiddenField).Value))
            Dim strCVVNo As String = objCommon.Decrypt(CCommon.ToString(DirectCast(e.Item.FindControl("hdnCVVNo"), HiddenField).Value))
            Dim intMonth As Integer = CCommon.ToInteger(DirectCast(e.Item.FindControl("hdnMonth"), HiddenField).Value)
            Dim intYear As Integer = CCommon.ToInteger(DirectCast(e.Item.FindControl("hdnYear"), HiddenField).Value)
            Dim Amount As Decimal = CCommon.ToDecimal(DirectCast(e.Item.FindControl("txtAmount"), TextBox).Text)
            Dim paymentGateway As Integer = CCommon.ToInteger(DirectCast(e.Item.FindControl("hdnPaymentGateway"), HiddenField).Value)

            If objTrans Is Nothing Then objTrans = New TransactionHistory

            objTrans.TransHistoryID = dblTransHistoryID

            If e.CommandName = "Credit" Then
                If Amount + dblCreditAmount > dblCaptureAmount Then
                    litMessage.Text = "Amount should not be greater than Captured Amount."
                    Exit Sub
                ElseIf Amount + dblCreditAmount = dblCaptureAmount Then
                    If objPaymentGateway.RefundAmount(strTransactionID, Amount, ResponseMessage, strCardNo, strCVVNo, intMonth, intYear, paymentGateway:=paymentGateway) Then
                        objTrans.RefundAmt = Amount
                        objTrans.TransactionStatus = 5 '5=Credited
                        objTrans.DomainID = Session("DomainID")
                        objTrans.UserCntID = Session("UserContactID")

                        If objTrans.ManageTransactionHistory() >= 0 Then
                            litMessage.Text = "You have been credited full Captured Amount."
                        End If
                    End If
                    LoadTransactionHistory()
                    Exit Sub
                End If

                If objPaymentGateway.RefundAmount(strTransactionID, Amount, ResponseMessage, strCardNo, strCVVNo, intMonth, intYear, paymentGateway:=paymentGateway) Then
                    ''Update status of transaction and credit amount
                    With objTrans
                        .RefundAmt = Amount
                        .TransactionStatus = 2
                        .DomainID = Session("DomainID")
                        .UserCntID = Session("UserContactID")
                        If .ManageTransactionHistory() >= 0 Then
                            litMessage.Text = "Amount has been credited successfully."
                        End If
                    End With
                End If
            End If

            litMessage.Text = ResponseMessage
            LoadTransactionHistory()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    'Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
    '    Try
    '        Response.Write("<script>opener.location.reload(true); self.close();</script>")
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    Function ReturnMoney(ByVal Money) As String
        Try
            If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Function ReturnName(ByVal SDate) As String
        Try
            Dim strDate As String = ""
            If Not IsDBNull(SDate) Then
                strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                    strDate = "<font color=red>" & strDate & "</font>"
                ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                    strDate = "<font color=orange>" & strDate & "</font>"
                End If
            End If
            Return strDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub dgPaymentDetails_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPaymentDetails.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Header Then

            ElseIf e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then

                If DataBinder.Eval(e.Item.DataItem, "vcCreditCardNo") IsNot Nothing Then
                    Dim strCardNo As String = CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "vcCreditCardNo"))
                    Dim intCardCharLength As Integer = 4

                    If Not String.IsNullOrEmpty(strCardNo) AndAlso strCardNo.Length > intCardCharLength Then
                        strCardNo = objCommon.Decrypt(strCardNo)
                        strCardNo = strCardNo.Substring(strCardNo.Length - intCardCharLength, intCardCharLength)

                        Dim lblCardNo As Label
                        lblCardNo = e.Item.FindControl("lblCardNo")
                        If lblCardNo IsNot Nothing Then
                            lblCardNo.Text = "XXXX-" & strCardNo
                            If Not String.IsNullOrEmpty(CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "vcSignatureFile"))) Then
                                lblCardNo.Text = lblCardNo.Text & "<br/><a href=""" & CCommon.GetDocumentPath(Session("DomainID")) & CCommon.ToString(DataBinder.Eval(e.Item.DataItem, "vcSignatureFile")) & """ target=""_blank"">Signature</a>"
                            End If
                        End If
                    End If
                End If

                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                Dim hplBizDoc As New HyperLink
                hplBizDoc = DirectCast(e.Item.FindControl("hplBizDoc"), HyperLink)
                If hplBizDoc IsNot Nothing Then
                    'hplBizDoc.Attributes.Add("onclick", "OpenEdit(" & CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numOppID")) & "," & CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numOppBizDocsID")) & "," & CCommon.ToShort(DataBinder.Eval(e.Item.DataItem, "tintOppType")) & ")")
                    hplBizDoc.Attributes.Add("onclick", "OpenBizDoc(" & CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numOppID")) & "," & CCommon.ToLong(DataBinder.Eval(e.Item.DataItem, "numOppBizDocsID")) & ")")
                End If

                '1 = Authorized/Pending Capture 
                If CCommon.ToShort(DataBinder.Eval(e.Item.DataItem, "tintTransactionStatus")) = 1 Then
                    e.Item.FindControl("btnCredit").Visible = False
                    e.Item.FindControl("txtAmount").Visible = True
                    DirectCast(e.Item.FindControl("txtAmount"), TextBox).Enabled = True
                    DirectCast(e.Item.FindControl("txtAmount"), TextBox).Text = String.Format("{0:#,##0.00}", CCommon.ToDecimal(DataBinder.Eval(e.Item.DataItem, "monAuthorizedAmt")))

                    '2 = Captured
                ElseIf CCommon.ToShort(DataBinder.Eval(e.Item.DataItem, "tintTransactionStatus")) = 2 Then
                    e.Item.FindControl("btnCredit").Visible = True
                    e.Item.FindControl("txtAmount").Visible = True
                    DirectCast(e.Item.FindControl("txtAmount"), TextBox).Enabled = True
                    DirectCast(e.Item.FindControl("txtAmount"), TextBox).Text = String.Format("{0:#,##0.00}", CCommon.ToDecimal(DataBinder.Eval(e.Item.DataItem, "monCapturedAmt"))) - CCommon.ToDecimal(DataBinder.Eval(e.Item.DataItem, "monRefundAmt"))

                    '3 = Void
                ElseIf CCommon.ToShort(DataBinder.Eval(e.Item.DataItem, "tintTransactionStatus")) = 3 Then
                    e.Item.FindControl("btnCredit").Visible = True
                    e.Item.FindControl("txtAmount").Visible = True
                    DirectCast(e.Item.FindControl("txtAmount"), TextBox).Enabled = True
                    DirectCast(e.Item.FindControl("txtAmount"), TextBox).Text = String.Format("{0:#,##0.00}", CCommon.ToDecimal(DataBinder.Eval(e.Item.DataItem, "monRefundAmt")))

                    '4=Failed(with any error)    
                ElseIf CCommon.ToShort(DataBinder.Eval(e.Item.DataItem, "tintTransactionStatus")) = 4 Then
                    e.Item.FindControl("btnCredit").Visible = False
                    e.Item.FindControl("txtAmount").Visible = False

                    '5=Credited
                ElseIf CCommon.ToShort(DataBinder.Eval(e.Item.DataItem, "tintTransactionStatus")) = 5 Then
                    e.Item.FindControl("btnCredit").Visible = False

                    If CCommon.ToDecimal(DataBinder.Eval(e.Item.DataItem, "monRefundAmt")) < CCommon.ToDecimal(DataBinder.Eval(e.Item.DataItem, "monCapturedAmt")) Then
                        e.Item.FindControl("txtAmount").Visible = True
                        DirectCast(e.Item.FindControl("txtAmount"), TextBox).Text = CCommon.ToDecimal(DataBinder.Eval(e.Item.DataItem, "monRefundAmt"))
                    Else
                        e.Item.FindControl("txtAmount").Visible = False
                    End If

                End If

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class