<%@ Page Language="vb" AutoEventWireup="false" ValidateRequest="false" EnableEventValidation="false"
    CodeBehind="frmAddOpportunity.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmAddOpportunity"
    MasterPageFile="~/common/SalesOrder.Master" %>

<%@ Register Src="order.ascx" TagName="order" TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register TagPrefix="menu1" TagName="Menu" Src="../include/webmenu.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Opportunity</title>
    <style type="text/css">
        label.valid {
            width: 24px;
            height: 24px;
            background: url(assets/img/valid.png) center center no-repeat;
            display: inline-block;
            text-indent: -9999px;
        }

        label.error {
            font-weight: bold;
            color: red;
            padding: 2px 8px;
            margin-top: 2px;
        }

        .select2popup {
            width: 650px;
            height: 200px;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function OptDropShip() {

            if (document.getElementById('chkOptDropShip').checked == true) {
                if (document.getElementById('trOptAttributes') != null) {
                    document.getElementById('trOptAttributes').style.display = 'none';
                }
                if (document.getElementById('trOptWareHouse') != null) {
                    document.getElementById('trOptWareHouse').style.display = 'none';
                }
            }
            else {
                if (document.getElementById('trOptAttributes') != null) {
                    document.getElementById('trOptAttributes').style.display = '';
                }
                if (document.getElementById('trOptWareHouse') != null) {
                    document.getElementById('trOptWareHouse').style.display = '';
                }
            }

        }

        function Save(a) {
            if ($find('radCmbCompany').get_value() == '') {
                alert("Select Company")
                return false;
            }
            if ((document.getElementById("ddlContact").selectedIndex == -1) || (document.getElementById("ddlContact").value == 0)) {
                alert("Select Contact")
                document.getElementById("ddlContact").focus();
                return false;
            }
        }

        function Add(a) {

            var IsAddCalidate = Save();

            if (IsAddCalidate == false) {
                return false;
            }

            if ($find('radWareHouse') != null) {
                if ($find('radWareHouse').get_value() == '') {
                    alert("Select Warehouse")
                    return false;
                }
            }

            if ($ControlFind('txtUnits').value == "") {
                alert("Enter Units")
                $ControlFind('txtUnits').focus();
                return false;
            }

            if (parseFloat(document.getElementById("txtUnits").value) <= 0) {
                alert("Units must greater than 0.")
                document.getElementById("txtUnits").focus();
                return false;
            }

            if ($ControlFind('txtprice').value == "") {
                alert("Enter Price")
                $ControlFind('txtprice').focus();
                return false;
            }

            //Check for Duplicate Item
            var table = $ControlFind('ctl00_Content_order1_dgItems');
            var WareHouesID;
            //            alert($('radWareHouse'));
            if ($ControlFind('radWareHouse') == null) {
                WareHouesID = '';
            }
            else {
                WareHouesID = $find('radWareHouse').get_value();
            }

            var dropship;
            if ($ControlFind('chkDropShip').checked == true) {
                dropship = 'True';
                WareHouesID = '';
            }
            else {
                dropship = 'False';
            }
        }
        function AddOption() {
            if (document.getElementById("ddlOptItem").value == 0) {
                alert("Select Item")
                return false;
            }

            if ($find('radOptWareHouse') != null) {
                if ($find('radOptWareHouse').get_value() == '') {
                    alert("Select Warehouse")
                    return false;
                }
            }

            if (document.getElementById("txtOptUnits").value == "") {
                alert("Enter Units")
                document.getElementById("txtOptUnits").focus();
                return false;
            }
            if (parseFloat(document.getElementById("txtUnits").value) <= 0) {
                alert("Units must greater than 0.")
                document.getElementById("txtUnits").focus();
                return false;
            }
            if (document.getElementById("txtOptPrice").value == "") {
                alert("Enter Price")
                document.getElementById("txtOptPrice").focus();
                return false;
            }


        }
        function openItem(a, b, c, d, e) {

            if ($find('radCmbCompany').get_value() == "") {
                c = 0
            }
            else {
                c = $find('radCmbCompany').get_value();
            }
            if (document.getElementById(b).value == "") {
                b = 0
            }
            else {
                b = document.getElementById(b).value
            }

            var CalPrice = 0;
            if ($ControlFind('hdKit').value == 'True' && ($ControlFind('dgKitItem') != null)) {
                $('ctl00_Content_order1_dgKitItem tr').not(':first,:last').each(function () {
                    var str;
                    str = $(this).find("[id*='lblUnitPrice']").text();
                    CalPrice = CalPrice + (parseCurrency(str.split('/')[0]) * $(this).find("input[id*='txtQuantity']").val());
                });
            }

            var WareHouseItemID = 0;
            if ($find('radWareHouse') != null) {
                if ($find('radWareHouse').get_value() == "") {
                    WareHouseItemID = 0
                }
                else {
                    WareHouseItemID = $find('radWareHouse').get_value()
                }
            }
            if (a != 0 && b != 0) {
                window.open('../opportunity/frmItemPriceRecommd.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=' + a + '&Unit=' + b + '&DivID=' + c + '&OppType=' + e + '&CalPrice=' + CalPrice + '&WarehouseItemID=' + WareHouseItemID, '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')

            }
        }
        function openOptItem(a, b, c) {
            if (document.getElementById(a).selectedIndex < 1) {
                a = 0
            }
            else {
                a = document.getElementById(a).value
            }

            if ($find('radCmbCompany').get_value() == "") {
                c = 0
            }
            else {
                c = $find('radCmbCompany').get_value()
            }
            if (document.getElementById(b).value == "") {
                b = 0
            }
            else {
                b = document.getElementById(b).value
            }
            if (a != 0 && b != 0) {
                window.open('../opportunity/frmItemPriceRecommd.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ItemCode=' + a + '&Unit=' + b + '&DivID=' + c + '&OptItem=' + 1 + '&CalPrice=0&OppType=2', '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')

            }
        }

        function ShowWindow(Page, q, att) {

            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;
            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                return false;

            }
        }
        function Focus() {
            document.getElementById("txtCompName").focus();
        }
        function openPurOpp() {
            window.open('../opportunity/frmSelectPurOpp.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OppID=0', '', 'toolbar=no,titlebar=no,left=300,top=450,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function SelectVendor(ItemCode, DivID, Cost, ItemName, CompName, Unit) {
            document.getElementById("txtUnits").value = Unit;
            document.getElementById("txtprice").value = Cost;
            document.getElementById("hdDivID").value = DivID;
            document.getElementById("btnLOadCo").click();
        }

        function SetWareHouseId(id) {
            var combo = $find('radWareHouse')
            var node = combo.findItemByValue(id);
            node.select();
        }

        function parseCurrency( num ) {
            return parseFloat( num.replace( /,/g, '') );
        }

        function CalculateTotal() {
            var Total, subTotal, TaxAmount, ShipAmt, IndTaxAmt;
            var index, findex;
            Total = 0;
            TaxAmount = 0;
            subTotal = 0;
            IndTaxAmt = 0;
            ShipAmt = 0;

            $('#ctl00_Content_order1_dgItems  > tbody > tr').not(':first,:last').each(function () {
                if (parseFloat($(this).find("input[id*='txtUnits']").val()) <= 0) {
                    alert("Units must greater than 0.")
                    $(this).find("input[id*='txtUnits']").focus();
                    return false;
                }

                var unitPrice = parseCurrency($(this).find("input[id*='txtUnitPrice']").val());
                var uomConversionFactor = parseCurrency($(this).find("input[id*='txtUOMConversionFactor']").val()) || 1;
                var units = parseCurrency($(this).find("input[id*='txtUnits']").val());
                var txtDiscountTypeVal = $(this).find("input[id*='txtDiscountType']").val();
                var txtTotalDiscountVal = parseCurrency($(this).find("input[id*='txtTotalDiscount']").val());

                //WHEN Item Level UOM is Enabled we are displying total price in unitprice textbox
                //So for exammple, if 1 6Packs = 1 Can and order uom is 6 Packs then unit price is multiplied by 6.
                //to get unit price of single unit we have to divide by conversion factor
                if ($("#hdnEnabledItemLevelUOM").val() == "True") {
                    unitPrice = parseCurrency(unitPrice / uomConversionFactor);
                }

                $(this).find("input[id*='hdnUnitPrice']").val(unitPrice);

                subTotal = (units * uomConversionFactor) * unitPrice;

                if ($(this).find("input[id*='txtDiscountType']")) {
                    if (txtDiscountTypeVal == 'True') {
                        subTotal = subTotal - txtTotalDiscountVal;
                    }
                    else {
                        discount = (subTotal * (parseFloat($(this).find("input[id*='txtTotalDiscount']").val()) || 0)) / 100;
                        subTotal = subTotal - discount;

                        $(this).find("[id*='lblDiscount']").text(CommaFormatted(roundNumber(discount, 2)) + ' (' + (txtTotalDiscountVal || 0) + '%)');
                    }
                }

                $(this).find("[id*='lblTotal']").text(CommaFormatted(roundNumber(subTotal, 2)));

                Total = parseFloat(Total) + parseFloat(subTotal);
            });

            $('#ctl00_Content_order1_dgItems > tbody > tr:last').find("[id*='lblFTotal']").text(CommaFormatted(roundNumber((Total), 2)));
            $('#lblTotal').text(CommaFormatted(roundNumber((Total) + parseFloat(TaxAmount) + parseFloat(ShipAmt), 2)));
        }
        function AddUpdate() {
            return validateFixedFields();
        }
        function ValidateShipperAccount() {
            if (document.getElementById('hdnShipperAccountNo') != null) {
                if (document.getElementById('hdnShipperAccountNo').value.toString() != '') {
                    return true;
                }
                else {
                    if ($find('radCmbCompany').get_value() != "") {
                        var myWidth = 750;
                        var myHeight = 700;
                        var left = (screen.width - myWidth) / 2;
                        var top = (screen.height - myHeight) / 4;
                        window.open('../account/frmConfigParcelShippingAccount.aspx?IsFromNewOrder=1&DivID=' + $find('radCmbCompany').get_value(), '', 'toolbar=no,titlebar=no,left=' + left + ',top=' + top + ',width=' + myWidth + ',height=' + myHeight + ',scrollbars=yes,resizable=yes');
                        document.getElementById('chkUseCustomerShippingAccount').checked = false;
                        return false;
                    }
                }
            }
        }

        function UpdateShipperAccountNo(shipperAccountNo) {
            if (document.getElementById('hdnShipperAccountNo') != null) {
                document.getElementById('hdnShipperAccountNo').value = shipperAccountNo
            }
        }

        function ShippingVisibility(mode) {
            alert(mode);

            if (mode == 0) {
                console.log($('#trShipping #divLabelEstimatedRates').attr('id'));
                console.log($('#trShipping #divComboEstimatedRates').attr('id'));
                console.log($('#trShipping #divShippingEstimates').attr('id'));
            }
            else {

                console.log($('#trShipping #divLabelEstimatedRates').attr('id'));
                console.log($('#trShipping #divComboEstimatedRates').attr('id'));
                console.log($('#trShipping #divShippingEstimates').attr('id'));
            }
        }
        function FillModifiedAddress(AddressType, AddressID, FullAddress, warehouseID, isDropshipClicked) {
            if (AddressType == "BillTo") {
                $('#hdnBillAddressID').val(AddressID);
            } else {
                $('#hdnShipAddressID').val(AddressID);
            }

            document.getElementById("btnTemp").click()
            return false;
        }
        function OpenAddressWindow(sAddressType) {
            var radValue = $find('radCmbCompany').get_value();
            window.open('frmChangeDefaultAdd.aspx?AddressType=' + sAddressType + '&CompanyId=' + radValue, '', 'toolbar=no,titlebar=no,left=200, top=300,width=800,height=170,scrollbars=no,resizable=no');
            return false;
        }
        function OpenAdd() {
            if ($find('radCmbCompany').get_value() == "") {
                alert("Select Vendor")
                return false;
            }
            window.open("../prospects/frmProspectsAdd.aspx?pwer=dfsdfdsfdsfdsf&rtyWR=" + $find('radCmbCompany').get_value() + "&Reload=1", '', 'toolbar=no,titlebar=no,top=300,width=700,height=300,scrollbars=no,resizable=no')
            return false;
        }
    </script>
    <style type="text/css">
        .column {
            float: left;
        }

        li > span {
            height: 100% !important;
        }

        html > body .RadComboBoxDropDown_Vista .rcbItem, html > body .RadComboBoxDropDown_Vista .rcbHovered, html > body .RadComboBoxDropDown_Vista .rcbDisabled, html > body .RadComboBoxDropDown_Vista .rcbLoading {
            display: inline-block;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <asp:ScriptManager ID="ScriptManager" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanelException" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Label ID="lblException" runat="server" Style="color: red;"></asp:Label>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    New Opportunity
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="100%" runat="server" id="tblMultipleOrderMessage" visible="false" style="background-color: #FFFFFF; color: red">
        <tr>
            <td class="normal4" align="center">You can not create multiple orders simultaneously,
                <asp:LinkButton Text="Click here" runat="server" ID="lnkClick" />
                to clear other order and proceed.
            </td>
        </tr>
    </table>

    <div class="container customer-detail">
        <div class="grid grid-pad">
            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="cont1">
                        <div class="col-1-1">
                            <div class="col-1-2">
                                <div class="form-group">
                                    <label class="col-3-12">Vendor<span class="required">*</span></label>
                                    <div class="col-9-12">
                                        <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="100%" DropDownWidth="600px"
                                            OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                            ClientIDMode="Static"
                                            ShowMoreResultsBox="true"
                                            Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True"
                                            TabIndex="1">
                                            <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                                        </telerik:RadComboBox>
                                        <div>
                                        <div>
                                            <div class="col-1-2">
                                                <asp:LinkButton ID="lnkBillTo" runat="server" Text="Bill to:" class="hyperlink" TabIndex="8"></asp:LinkButton>
                                            </div>
                                            <div class="col-1-2"><a href="#" onclick="return OpenAdd()" style="float: right" tabindex="8">Add/Edit</a></div>
                                        </div>
                                        <div>
                                            <asp:Label ID="lblBillTo1" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblBillTo2" runat="server" Text=""></asp:Label>
                                        </div>
                                    </div>
                                    </div>
                                    
                                </div>
                                <div class="form-group">
                                    <label class="col-3-12">Assign To</label>
                                    <div class="col-9-12">
                                        <asp:DropDownList ID="ddlAssignTo" runat="server" TabIndex="6">
                                            <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group" id="pnlCurrency" runat="server" visible="false">
                                    <label class="col-3-12">Currency</label>
                                    <div class="col-9-12">
                                        <asp:DropDownList ID="ddlCurrency" runat="server" TabIndex="5"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group" style="float: left" runat="server" id="divClass" visible="false">
                                    <label class="col-3-12">Class</label>
                                    <div class="col-9-12">
                                        <asp:DropDownList ID="ddlClass" runat="server" TabIndex="3"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1-2">
                                <div class="form-group">
                                    <label class="col-3-12">Contact<span class="required">*</span></label>
                                    <div class="col-9-12">
                                        <asp:DropDownList ID="ddlContact" runat="server" TabIndex="2">
                                        </asp:DropDownList>
                                        <div>
                                            <div>
                                                <div class="col-1-2">
                                                    <asp:LinkButton ID="lnkShipTo" runat="server" Text="Ship to:" class="hyperlink" TabIndex="10"></asp:LinkButton>
                                                </div>
                                                <div class="col-1-2">
                                                    <a href="#" onclick="return OpenAdd()" class="hyperlink" style="float: right" tabindex="11">Add/Edit</a>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="pull-left" style="width:100%">
                                                    <asp:Label ID="lblShipTo1" runat="server" Text=""></asp:Label>
                                                    <asp:Label ID="lblShipTo2" runat="server" Text=""></asp:Label>
                                                    <asp:HiddenField ID="hdnShipToWarehouseID" runat="server" />
                                                </div>
                                                <div class="pull-left" style="width:100px">
                                                    <asp:CheckBox ID="chkDropshipPO" runat="server" OnCheckedChanged="chkDropshipPO_CheckedChanged" AutoPostBack="true" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-3-12">Source</label>
                                    <div class="col-9-12">
                                        <asp:DropDownList ID="ddlSource" runat="server" TabIndex="4">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-3-12">Vendor�s Ship from</label>
                                    <div class="col-9-12">
                                        <asp:DropDownList ID="ddlVendorAddresses" runat="server" TabIndex="5" AutoPostBack="true" OnSelectedIndexChanged="ddlVendorAddresses_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-3-12">Shipping Method</label>
                                    <div class="col-9-12">
                                        <asp:DropDownList ID="ddlShippingMethod" runat="server" TabIndex="5" AutoPostBack="true" OnSelectedIndexChanged="ddlShippingMethod_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-3-12">Lead Time (Days)</label>
                                    <div class="col-9-12" style="padding-top: 7px;">
                                        <asp:Label ID="lblLeadTimeDays" runat="server" Text="-"></asp:Label>
                                        &nbsp;&nbsp; <b>Expected Delivery</b>&nbsp;<asp:Label ID="lblExpectedDelivery" runat="server" Text="-"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cont2">
                        <div class="col-1-1" id="tdBanlce" runat="server">
                            <div class="col-1-2">
                                <div class="form-group">
                                    <label class="col-3-12">Follow-up Status</label>
                                    <div class="col-9-12 textdiv">
                                        <asp:Label ID="lblFollowup" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top: 3px">
                                    <label class="col-3-12">Employees</label>
                                    <div class="col-9-12 textdiv">
                                        <asp:Label ID="lblEmployees" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top: 3px">
                                    <label class="col-3-12">Rating</label>
                                    <div class="col-9-12 textdiv">
                                        <asp:Label ID="lblRating" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top: 3px">
                                    <label class="col-3-12">Differentiation</label>
                                    <div class="col-9-12 textdiv">
                                        <asp:Label ID="lblDifferentiation" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="form-group" style="margin-top: 3px">
                                    <label class="col-3-12">Credit Hold</label>
                                    <div class="col-9-12 textdiv">
                                        <asp:CheckBox ID="chkCreditHold" runat="server" Enabled="false" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-1-2" style="float: right">
                                <div class="form-group" style="line-height: 20px">
                                    <label class="col-3-12">Sales Credit</label>
                                    <div class="col-9-12 textdiv">
                                        <asp:Label ID="lblCreditBalance" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="form-group" style="line-height: 20px">
                                    <label class="col-3-12">Credit Limit (A)</label>
                                    <div class="col-9-12 textdiv">
                                        <asp:Label ID="lblCreditLimit" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="form-group" style="line-height: 20px">
                                    <label class="col-3-12">Balance Due (B)</label>
                                    <div class="col-9-12 textdiv">
                                        <asp:Label ID="lblBalanceDue" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="form-group" style="line-height: 20px">
                                    <label class="col-3-12">Remaining Credit(A-B)</label>
                                    <div class="col-9-12 textdiv">
                                        <asp:Label ID="lblRemaningCredit" runat="server"></asp:Label>
                                    </div>
                                </div>
                                <div class="form-group" style="line-height: 20px">
                                    <label class="col-3-12">Amount Past Due</label>
                                    <div class="col-9-12 textdiv">
                                        <asp:Label ID="lblTotalAmtPastDue" runat="server"></asp:Label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <asp:Button ID="btnTemp" runat="server" Style="display: none"></asp:Button>
                    <asp:HiddenField ID="hdnVendorID" runat="server" />
                    <asp:HiddenField ID="hdnVendorCost" runat="server" />
                    <asp:Button ID="btnSelectedVendor" runat="server" Text="" Style="display: none" />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="radCmbCompany" />
                    <asp:AsyncPostBackTrigger ControlID="ddlContact" />
                    <asp:AsyncPostBackTrigger ControlID="ddlSource" />
                    <asp:AsyncPostBackTrigger ControlID="ddlCurrency" />
                    <asp:AsyncPostBackTrigger ControlID="ddlAssignTo" />
                    <asp:AsyncPostBackTrigger ControlID="btnSelectedVendor" />
                    <asp:AsyncPostBackTrigger ControlID="btnTemp" />
                </Triggers>
            </asp:UpdatePanel>
            <uc1:order ID="order1" runat="server" />
        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanelHidden" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <input id="hdOptKit" runat="server" type="hidden" />
            <asp:Button ID="btnLOadCo" runat="server" Style="display: none" />
            <asp:HiddenField ID="hdnCmbDivisionID" runat="server" />
            <asp:HiddenField ID="hdnCmbOppType" runat="server" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
