﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Item
Imports System.Configuration
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Contacts

Public Class frmPrintWorkOrder
    Inherits BACRMPage

    Dim objCommon As CCommon
    Dim objItems As New CItems
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            GetUserRightsForPage(35, 106)

            If Not IsPostBack Then
                bindItemsGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub bindItemsGrid()
        Try
            'litError.Text = ""
            Dim objItems As New CItems
            objItems.DomainID = Session("DomainID")
            objItems.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            objItems.numWOStatus = 0
            objItems.PageSize = Convert.ToInt32(Session("PagingRows"))
            objItems.UserCntID = 0
            objItems.vcWOId = GetQueryStringVal("WOId").Trim(",")
            objItems.CurrentPage = 1
            objItems.columnName = "numWOId"

            Dim ds As DataSet
            ds = objItems.GetWorkOrder
            rtlWorkOrder.DataSource = ds.Tables(0)
            rtlWorkOrder.DataBind()
            rtlWorkOrder.ExpandAllItems()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Protected Sub rtlWorkOrder_ItemCommand(sender As Object, e As TreeListCommandEventArgs)
    '    Try
    '        If e.CommandName = RadTreeList.ExpandCollapseCommandName Then
    '            'bindItemsGrid()
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        DisplayError(ex.Message)
    '    End Try
    'End Sub

    Protected Sub rtlWorkOrder_ItemDataBound(sender As Object, e As TreeListItemDataBoundEventArgs)
        Try
            If e.Item.ItemType = TreeListItemType.DetailTemplateItem Then
                If DirectCast(DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDetailTemplateItem).DataItem, Object), System.Data.DataRowView).Row("numParentWOID") = 0 Then
                    e.Item.Visible = True
                Else
                    e.Item.Visible = False
                End If

                Dim uniqueID As String = DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDetailTemplateItem).DataItem, System.Data.DataRowView).Row("ID")
                DirectCast(e.Item, Telerik.Web.UI.TreeListDetailTemplateItem).Attributes.Add("UniqueID", uniqueID)

            ElseIf e.Item.ItemType = TreeListItemType.Item Or e.Item.ItemType = TreeListItemType.AlternatingItem Then

                Dim btn As Button = TryCast(e.Item.FindControl("ExpandCollapseButton"), Button)
                Dim uniqueID As String = DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).DataItem, System.Data.DataRowView).Row("ID")

                If btn IsNot Nothing Then
                    btn.Attributes.Add("UniqueID", uniqueID)
                    btn.Attributes.Add("onclick", "return ExpandCollapse(" + btn.ClientID + ");")
                End If

                DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).Attributes.Add("UniqueID", uniqueID)

                'Dim chkDelete As CheckBox = DirectCast(e.Item.FindControl("chkDelete"), CheckBox)
                'Dim ddlWOAssemblyStatus As DropDownList = DirectCast(e.Item.FindControl("ddlWOAssemblyStatus"), DropDownList)
                'objCommon = New CCommon
                'objCommon.sb_FillComboFromDBwithSel(ddlWOAssemblyStatus, 302, Session("DomainID"))

                Dim numWOStatus As String = CCommon.ToString(DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).DataItem, System.Data.DataRowView).Row("numWOStatus"))

                'If Not ddlWOAssemblyStatus.Items.FindByValue(numWOStatus) Is Nothing Then
                '    ddlWOAssemblyStatus.Items.FindByValue(numWOStatus).Selected = True

                '    If numWOStatus = 23184 Then
                '        ddlWOAssemblyStatus.Enabled = False
                '    End If
                'End If

                'If ddlWOAssemblyStatus.SelectedItem.Value = 0 Or
                '    ddlWOAssemblyStatus.SelectedItem.Text = "Not Ready to Build" Or
                '    ddlWOAssemblyStatus.SelectedItem.Text = "Ready to Build" Then
                '    'If we do have enough on allocation qty in child items to build assembly then set its status to Ready to Build
                '    'else set its status to Not Ready to Build
                '    If CCommon.ToBool(DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).DataItem, System.Data.DataRowView).Row("bitReadyToBuild")) Then
                '        ddlWOAssemblyStatus.Items.FindByText("Ready to Build").Selected = True
                '    Else
                '        ddlWOAssemblyStatus.Items.FindByText("Not Ready to Build").Selected = True
                '    End If
                'End If

                'User should not be able to delete child work order directly
                'If DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).DataItem, System.Data.DataRowView).Row("numParentWOID") > 0 Then
                '    chkDelete.Visible = False
                'End If

                ''Work Order Status drop down should be visible for assembly items only and not for child items
                'If CCommon.ToBool(DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).DataItem, System.Data.DataRowView).Row("bitWorkOrder")) Then
                '    ddlWOAssemblyStatus.Visible = True
                'Else
                '    ddlWOAssemblyStatus.Visible = False
                'End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    'Sub bindItemsGrid()
    '    Dim objItems As New CItems
    '    objItems.DomainID = Session("DomainID")
    '    objItems.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
    '    objItems.numWOStatus = 0
    '    objItems.PageSize = Session("PagingRows")
    '    objItems.UserCntId = 0
    '    objItems.vcWOId = GetQueryStringVal( "WOId").Trim(",")

    '    objItems.CurrentPage = 1

    '    objItems.columnName = "numWOId"

    '    RadGrid1.MasterTableView.FilterExpression = "numParentId = '0'"
    '    Dim ds As DataSet
    '    ds = objItems.GetWorkOrder
    '    RadGrid1.DataSource = ds.Tables(0)
    '    RadGrid1.DataBind()

    '    HideExpandColumnRecursive(RadGrid1.MasterTableView)

    '    Dim ds1 As DataSet
    '    Dim objContacts As CContacts
    '    objContacts = New CContacts
    '    objContacts.DomainID = Session("DomainId")
    '    objContacts.FormId = 33
    '    objContacts.UserCntID = Session("UserContactId")
    '    objContacts.ContactType = 0
    '    ds1 = objContacts.GetColumnConfiguration

    '    Dim radColumn As Telerik.Web.UI.GridColumn
    '    'hide all columns
    '    For Each dr As DataRow In ds1.Tables(0).Rows
    '        radColumn = RadGrid1.Columns.FindByUniqueNameSafe(dr("vcFieldName").ToString())
    '        If Not radColumn Is Nothing Then
    '            radColumn.Visible = False
    '        End If
    '    Next

    '    radColumn = RadGrid1.Columns.FindByUniqueNameSafe("Assembly Status")

    '    radColumn.Visible = True
    '    'Show columns which are set to be shown in settings
    '    For Each dr As DataRow In ds1.Tables(1).Rows
    '        radColumn = RadGrid1.Columns.FindByUniqueNameSafe(dr("vcFieldName").ToString())
    '        If dr("Custom") = "1" Then 'is Custom Field -then add dynamic column to radgrid
    '            RadGrid1.Columns.Remove(radColumn)
    '            Dim Column As New Telerik.Web.UI.GridBoundColumn
    '            Column.HeaderText = dr("vcFieldName").ToString()
    '            Column.UniqueName = dr("vcFieldName").ToString()
    '            Column.DataField = dr("vcFieldName").ToString()
    '            Column.HeaderButtonType = Telerik.Web.UI.GridHeaderButtonType.TextButton
    '            Column.OrderIndex = CCommon.ToInteger(dr("tintOrder")) + 6
    '            If ds.Tables(0).Columns.IndexOf(dr("vcFieldName").ToString()) > 0 Then
    '                RadGrid1.Columns.Add(Column)
    '            End If
    '        ElseIf Not radColumn Is Nothing Then
    '            radColumn.Visible = True
    '            radColumn.OrderIndex = CCommon.ToInteger(dr("tintOrder")) + 6
    '        End If
    '    Next

    '    RadGrid1.Rebind()
    'End Sub

    'Public Sub HideExpandColumnRecursive(ByVal tableView As GridTableView)
    '    Dim nestedViewItems As GridItem() = tableView.GetItems(GridItemType.NestedView)
    '    For Each nestedViewItem As GridNestedViewItem In nestedViewItems
    '        For Each nestedView As GridTableView In nestedViewItem.NestedTableViews
    '            nestedView.Style("border") = "0"

    '            Dim MyExpandCollapseButton As Button = DirectCast(nestedView.ParentItem.FindControl("MyExpandCollapseButton"), Button)
    '            If nestedView.Items.Count = 0 Then
    '                If Not MyExpandCollapseButton Is Nothing Then
    '                    MyExpandCollapseButton.Style("visibility") = "hidden"
    '                End If
    '                nestedViewItem.Visible = False
    '            Else
    '                If Not MyExpandCollapseButton Is Nothing Then
    '                    MyExpandCollapseButton.Style.Remove("visibility")
    '                End If
    '            End If

    '            If nestedView.HasDetailTables Then
    '                HideExpandColumnRecursive(nestedView)
    '            End If
    '        Next
    '    Next
    'End Sub
    'Protected Sub RadGrid1_ColumnCreated(ByVal sender As Object, ByVal e As GridColumnCreatedEventArgs) Handles RadGrid1.ColumnCreated
    '    If TypeOf e.Column Is GridExpandColumn Then
    '        e.Column.Visible = False
    '    ElseIf TypeOf e.Column Is GridBoundColumn Then
    '        e.Column.HeaderStyle.Width = Unit.Pixel(100)
    '    End If
    'End Sub
    'Protected Sub RadGrid1_ItemCreated(ByVal sender As Object, ByVal e As GridItemEventArgs) Handles RadGrid1.ItemCreated
    '    CreateExpandCollapseButton(e.Item, "ItemLevel")

    '    If TypeOf e.Item Is GridHeaderItem AndAlso Not e.Item.OwnerTableView Is RadGrid1.MasterTableView Then
    '        e.Item.Style("display") = "none"
    '    End If

    '    If TypeOf e.Item Is GridHeaderItem AndAlso Not e.Item.OwnerTableView Is RadGrid1.MasterTableView Then
    '        e.Item.Cells(6).Text = "Item"
    '    End If

    '    If TypeOf e.Item Is GridNestedViewItem Then
    '        e.Item.Cells(0).Visible = False
    '    End If
    'End Sub

    'Protected Sub RadGrid1_ItemDataBound(ByVal sender As Object, ByVal e As GridItemEventArgs) Handles RadGrid1.ItemDataBound
    '    CreateExpandCollapseButton(e.Item, "ItemLevel")

    '    If TypeOf e.Item Is GridDataItem Then

    '        If CInt(DataBinder.Eval(e.Item.DataItem, "ItemLevel").ToString()) = 1 Then
    '            e.Item.Cells(4).Text = ""
    '        End If

    '        Dim ddlWOAssemblyStatus As DropDownList
    '        ddlWOAssemblyStatus = e.Item.FindControl("ddlWOAssemblyStatus")

    '        If CInt(DataBinder.Eval(e.Item.DataItem, "ItemLevel").ToString()) = 1 Then

    '            If DataBinder.Eval(e.Item.DataItem, "numWOStatus").ToString() <> 23184 Then
    '                objCommon = New CCommon
    '                objCommon.sb_FillComboFromDBwithSel(ddlWOAssemblyStatus, 302, Session("DomainID"))

    '                If DataBinder.Eval(e.Item.DataItem, "numWOStatus").ToString() > 0 Then
    '                    If Not ddlWOAssemblyStatus.Items.FindByValue(CInt(DataBinder.Eval(e.Item.DataItem, "numWOStatus").ToString())) Is Nothing Then
    '                        ddlWOAssemblyStatus.Items.FindByValue(CInt(DataBinder.Eval(e.Item.DataItem, "numWOStatus").ToString())).Selected = True
    '                    End If
    '                End If
    '            Else
    '                ddlWOAssemblyStatus.Visible = False

    '                CType(e.Item.FindControl("lblWOAssemblyStatus"), Label).Text = "Completed"
    '            End If
    '        Else
    '            ddlWOAssemblyStatus.Visible = False
    '        End If
    '    End If
    'End Sub

    'Public Sub CreateExpandCollapseButton(ByVal item As GridItem, ByVal columnUniqueName As String)
    '    If TypeOf item Is GridDataItem Then
    '        If item.FindControl("MyExpandCollapseButton") Is Nothing Then
    '            Dim button As New Button()
    '            AddHandler button.Click, AddressOf button_Click_Collapse_Expand
    '            button.CommandName = "ExpandCollapse"
    '            button.CssClass = IIf((item.Expanded), "rgCollapse", "rgExpand")
    '            button.ID = "MyExpandCollapseButton"

    '            If item.OwnerTableView.HierarchyLoadMode = GridChildLoadMode.Client Then
    '                Dim script As String = [String].Format("$find(""{0}"")._toggleExpand(this, event); return false;", item.Parent.Parent.ClientID)

    '                button.OnClientClick = script
    '            End If


    '            Dim level As Integer = item.ItemIndexHierarchical.Split(":"c).Length - 1
    '            item.Cells(6).Style("padding-left") = (level * 10) & "px"

    '            'Dim cell As TableCell = item.Cells(4)
    '            Dim cell As TableCell = DirectCast(item, GridDataItem)(columnUniqueName)
    '            cell.Controls.Add(button)
    '            cell.Controls.Add(New LiteralControl("&nbsp;"))

    '            'cell.Controls.Add(New LiteralControl(DataBinder.Eval(item.DataItem, columnUniqueName).ToString()))
    '            cell.Controls.Add(New LiteralControl((DirectCast(item, GridDataItem)).GetDataKeyValue(columnUniqueName).ToString()))

    '            cell.Style("padding-left") = (level * 10) & "px"
    '        End If
    '    End If
    'End Sub

    'Sub button_Click_Collapse_Expand(ByVal sender As Object, ByVal e As EventArgs)
    '    CType(sender, Button).CssClass = IIf((CType(sender, Button).CssClass = "rgExpand"), "rgCollapse", "rgExpand")
    'End Sub

End Class