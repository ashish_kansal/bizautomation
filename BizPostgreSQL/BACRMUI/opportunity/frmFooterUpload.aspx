<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmFooterUpload.aspx.vb"
    Inherits=".frmFooterUpload" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>BizDoc Footer</title>
    <script language="javascript" type="text/javascript">
        function Close() {
            window.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table width="100%">
                <tr>
                    <td align="right" colspan="3">
                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save &amp; Close">
                        </asp:Button>
                        <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Close"></asp:Button><asp:Button
                            ID="btnDelete" runat="server" CssClass="button Delete" Text="X"></asp:Button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
Footer Image Upload
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="400px">
        <tr>
            <td class="normal1" align="right">
                Select Image
            </td>
            <td>
                <input id="txtMagFile" type="file" name="txtMagFile" runat="server" style="width: 200px" />
            </td>
        </tr>
    </table>
</asp:Content>
