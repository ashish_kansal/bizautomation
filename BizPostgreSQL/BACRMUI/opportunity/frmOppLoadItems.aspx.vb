Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI
Imports System.Web.Services

<System.Web.Script.Services.ScriptService()> _
Partial Public Class frmOppLoadItems
    Inherits BACRMPage

    'Dim objCommon As CCommon

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


    End Sub
    'Private Sub GetItemNames(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs) Handles radCmbItem.ItemsRequested
    '    Try
    '        If e.Text <> "" And Len(e.Text) >= IIf(Session("ChrForItemSearch") = 0, 1, Session("ChrForItemSearch")) Then
    '            If objCommon Is Nothing Then objCommon = New CCommon
    '            objCommon.UserCntID = Session("UserContactID")
    '            Dim OppType As Short = e.Context("Division").ToString().Split("~")(1)
    '            Dim strDiv As String = e.Context("Division").ToString().Split("~")(0)
    '            Dim ds As DataSet
    '            If OppType = 1 Then
    '                ds = objCommon.NewOppItems(Session("DomainID"), 0, 1, e.Text)
    '                CreateColums(ds)
    '                radCmbItem.DataSource = ds.Tables(0)
    '                radCmbItem.DataValueField = "numItemCode"
    '                radCmbItem.DataTextField = "vcItemName"
    '                radCmbItem.DataBind()
    '            ElseIf OppType = 2 Then
    '                If strDiv <> "" Then
    '                    ds = objCommon.NewOppItems(Session("DomainID"), strDiv, 2, e.Text)
    '                    CreateColums(ds)
    '                    radCmbItem.DataSource = ds.Tables(0)
    '                    radCmbItem.DataValueField = "numItemCode"
    '                    radCmbItem.DataTextField = "vcItemName"
    '                    radCmbItem.DataBind()
    '                Else
    '                    ds = objCommon.NewOppItems(Session("DomainID"), 0, 2, e.Text) '1 is replaced by 2-reason:Not allowing one to create PO for which vendor is not mapped.
    '                    CreateColums(ds)
    '                    radCmbItem.DataSource = ds.Tables(0)
    '                    radCmbItem.DataValueField = "numItemCode"
    '                    radCmbItem.DataTextField = "vcItemName"
    '                    radCmbItem.DataBind()
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub
    '<WebMethod(EnableSession:=True)> _
    'Public Sub radCmbItem_ItemsRequested(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs) Handles radCmbItem.ItemsRequested
    '    Try
    '        If e.Text <> "" And Len(e.Text) >= IIf(Session("ChrForItemSearch") = 0, 1, Session("ChrForItemSearch")) Then
    '            If objCommon Is Nothing Then objCommon = New CCommon
    '            objCommon.UserCntID = Session("UserContactID")
    '            Dim OppType As Short = e.Context("Division").ToString().Split("~")(1)
    '            Dim strDiv As String = e.Context("Division").ToString().Split("~")(0)
    '            Dim ds As DataSet
    '            If OppType = 1 Then
    '                ds = objCommon.NewOppItems(Session("DomainID"), 0, 1, e.Text)
    '                CreateColums(ds, CType(o, RadComboBox))
    '                CType(o, RadComboBox).DataSource = ds.Tables(0)
    '                CType(o, RadComboBox).DataValueField = "numItemCode"
    '                CType(o, RadComboBox).DataTextField = "vcItemName"
    '                CType(o, RadComboBox).DataBind()
    '            ElseIf OppType = 2 Then
    '                If strDiv <> "" Then
    '                    ds = objCommon.NewOppItems(Session("DomainID"), strDiv, 2, e.Text)
    '                    CreateColums(ds, CType(o, RadComboBox))
    '                    CType(o, RadComboBox).DataSource = ds.Tables(0)
    '                    CType(o, RadComboBox).DataValueField = "numItemCode"
    '                    CType(o, RadComboBox).DataTextField = "vcItemName"
    '                    CType(o, RadComboBox).DataBind()
    '                Else
    '                    ds = objCommon.NewOppItems(Session("DomainID"), 0, 2, e.Text) '1 is replaced by 2-reason:Not allowing one to create PO for which vendor is not mapped.
    '                    CreateColums(ds, CType(o, RadComboBox))
    '                    CType(o, RadComboBox).DataSource = ds.Tables(0)
    '                    CType(o, RadComboBox).DataValueField = "numItemCode"
    '                    CType(o, RadComboBox).DataTextField = "vcItemName"
    '                    CType(o, RadComboBox).DataBind()
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub
    'Private Sub radCmbItem_ItemsRequested(ByVal o As Object, ByVal e As Telerik.WebControls.RadComboBoxItemsRequestedEventArgs) Handles radCmbItem.ItemsRequested
    '    Try
    '        If e.Text <> "" And Len(e.Text) >= IIf(Session("ChrForItemSearch") = 0, 1, Session("ChrForItemSearch")) Then
    '            If objCommon Is Nothing Then objCommon = New CCommon
    '            objCommon.UserCntID = Session("UserContactID")
    '            Dim OppType As Short = e.ClientDataString.Split("~")(1)
    '            Dim strDiv As String = e.ClientDataString.Split("~")(0)
    '            Dim ds As DataSet
    '            If OppType = 1 Then
    '                ds = objCommon.NewOppItems(Session("DomainID"), 0, 1, e.Text)
    '                CreateColums(ds)
    '                radCmbItem.DataSource = ds.Tables(0)
    '                radCmbItem.DataValueField = "numItemCode"
    '                radCmbItem.DataTextField = "vcItemName"
    '                radCmbItem.DataBind()
    '            ElseIf OppType = 2 Then
    '                If strDiv <> "" Then
    '                    ds = objCommon.NewOppItems(Session("DomainID"), strDiv, 2, e.Text)
    '                    CreateColums(ds)
    '                    radCmbItem.DataSource = ds.Tables(0)
    '                    radCmbItem.DataValueField = "numItemCode"
    '                    radCmbItem.DataTextField = "vcItemName"
    '                    radCmbItem.DataBind()
    '                Else
    '                    ds = objCommon.NewOppItems(Session("DomainID"), 0, 2, e.Text) '1 is replaced by 2-reason:Not allowing one to create PO for which vendor is not mapped.
    '                    CreateColums(ds)
    '                    radCmbItem.DataSource = ds.Tables(0)
    '                    radCmbItem.DataValueField = "numItemCode"
    '                    radCmbItem.DataTextField = "vcItemName"
    '                    radCmbItem.DataBind()
    '                End If
    '            End If
    '        End If
    '    Catch ex As Exception
    '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
    '        Response.Write(ex)
    '    End Try
    'End Sub

    'Sub CreateColums(ByVal ds As DataSet)
    '    Dim dtTable As DataTable
    '    dtTable = ds.Tables(1)
    '    radCmbItem.HeaderTemplate = New RadComboBoxTemplate1(ListItemType.Header, dtTable)
    '    radCmbItem.ItemTemplate = New RadComboBoxTemplate1(ListItemType.Item, dtTable)
    'End Sub
    'Sub CreateColums(ByVal ds As DataSet, ByVal obj As Object)
    '    Dim dtTable As DataTable
    '    dtTable = ds.Tables(1)
    '    CType(obj, RadComboBox).HeaderTemplate = New RadComboBoxTemplate(ListItemType.Header, dtTable)
    '    CType(obj, RadComboBox).ItemTemplate = New RadComboBoxTemplate(ListItemType.Item, dtTable)
    'End Sub

End Class

'Class RadComboBoxTemplate1
'    Implements ITemplate

'    Dim TemplateType As ListItemType
'    Dim dtTable1 As DataTable
'    Dim Field1 As String
'    Dim i As Integer = 0


'    Sub New(ByVal type As ListItemType, ByVal dtTable As DataTable)
'        Try
'            TemplateType = type
'            dtTable1 = dtTable

'        Catch ex As Exception
'            Throw ex
'        End Try
'    End Sub

'    Public Sub InstantiateIn(ByVal container As Control) Implements ITemplate.InstantiateIn
'        i = 0
'        Dim label1 As Label
'        Dim table1 As New HtmlTable
'        Dim tblCell As HtmlTableCell
'        Dim tblRow As HtmlTableRow
'        table1.Width = "100%"
'        Select Case TemplateType
'            Case ListItemType.Header
'                tblRow = New HtmlTableRow
'                For Each dr As DataRow In dtTable1.Rows
'                    tblCell = New HtmlTableCell
'                    tblCell.Width = 100 / dtTable1.Rows.Count & "%"
'                    label1 = New Label
'                    label1.Text = dr("vcFormFieldName")
'                    label1.CssClass = "normal1"
'                    'Field1 = dr("vcDbColumnName")
'                    'AddHandler label1.DataBinding, AddressOf label1_DataBinding
'                    tblCell.Controls.Add(label1)
'                    tblRow.Cells.Add(tblCell)
'                Next
'                table1.Rows.Add(tblRow)
'                container.Controls.Add(table1)
'            Case ListItemType.Item
'                tblRow = New HtmlTableRow
'                For Each dr As DataRow In dtTable1.Rows
'                    tblCell = New HtmlTableCell
'                    tblCell.Width = 100 / dtTable1.Rows.Count & "%"
'                    label1 = New Label
'                    label1.CssClass = "normal1"
'                    'label1.Text = dr("vcFormFieldName")
'                    Field1 = dr("vcDbColumnName")
'                    'label1.Text = "<%#DataBinder.Eval(Container.DataItem, """ & dr("vcDbColumnName") & """)%>"
'                    AddHandler label1.DataBinding, AddressOf label1_DataBinding
'                    tblCell.Controls.Add(label1)
'                    tblRow.Cells.Add(tblCell)
'                Next
'                table1.Rows.Add(tblRow)
'                container.Controls.Add(table1)
'        End Select

'    End Sub

'    Private Sub label1_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
'        Dim target As Label = CType(sender, Label)
'        Dim item As RadComboBoxItem = CType(target.NamingContainer, RadComboBoxItem)
'        Dim itemText As String = CType(DataBinder.Eval(item.DataItem, dtTable1.Rows(i).Item("vcDbColumnName")), String)
'        target.Text = itemText
'        i = i + 1
'    End Sub
'End Class
