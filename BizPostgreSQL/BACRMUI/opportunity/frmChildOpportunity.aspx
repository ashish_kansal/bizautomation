﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmChildOpportunity.aspx.vb"
    Inherits="BACRM.UserInterface.Opportunities.frmChildOpportunity" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Child Records</title>
    <script type="text/javascript">
        function OpenOpp(a) {
            var str;
            str = "../opportunity/frmOpportunities.aspx?frm=&OpID=" + a;
            window.opener.document.location.href = str;
        }
        function OpenBizInvoice(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
        </div>
    </div>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Child Records
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:GridView ID="gvChildOpps" runat="server" AutoGenerateColumns="False" CssClass="tbl"
        Width="100%" AllowSorting="false">
        <Columns>
            <asp:TemplateField HeaderText="Child Order ID" SortExpression="vcPOppName" >
                <ItemTemplate>
                    <a href="javascript:OpenOpp('<%# Eval("numOppId") %>')">
                        <%# Eval("vcPOppName") %>
                    </a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Source Auth BizDoc ID" SortExpression="vcbizdocid"
                >
                <ItemTemplate>
                    <a href="javascript:void(0)" onclick="OpenBizInvoice('<%# Eval("SourceOppId") %>','<%# Eval("numoppbizdocsid") %>');">
                        <%#Eval("vcbizdocid")%>
                    </a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Destination Auth BizDoc ID" SortExpression="vcbizdocid"
                >
                <ItemTemplate>
                    <a href="javascript:void(0)" onclick="OpenBizInvoice('<%# Eval("numOppId") %>','<%# Eval("numDestOppBizDocID") %>');">
                        <%#Eval("vcDestBizDocID")%>
                    </a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField HeaderText="Order Type" DataField="OppType" ReadOnly="true" />
            <asp:BoundField DataField="vcItemName" HeaderText="Item"></asp:BoundField>
            <asp:BoundField DataField="vcModelID" HeaderText="ModelID"></asp:BoundField>
            <asp:BoundField DataField="Attributes" HeaderText="Attributes"></asp:BoundField>
            <asp:BoundField DataField="part" HeaderText="Part #"></asp:BoundField>
            <asp:BoundField DataField="charItemType" HeaderText="Type"></asp:BoundField>
            <asp:BoundField DataField="DropShip" HeaderText="Dropship" ItemStyle-HorizontalAlign="Center">
            </asp:BoundField>
            <asp:BoundField DataField="numUnitHour" HeaderText="Units"></asp:BoundField>
            <asp:BoundField DataField="monPrice" HeaderText="Price" DataFormatString="{0:##,#00.00}">
            </asp:BoundField>
            <asp:BoundField DataField="monTotAmount" HeaderText="Amount" DataFormatString="{0:##,#00.00}">
            </asp:BoundField>
        </Columns>
    </asp:GridView>
</asp:Content>
