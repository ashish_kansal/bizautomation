<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmEditOppAddress.aspx.vb"
    Inherits=".frmEditOppAddress" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Change Address</title>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKZ2Kph5NQXTnPc1xEzM4Bq0nhzXjONS0&libraries=places" type="text/javascript"></script>
    <script src="../JavaScript/GooglePlace.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            initializeaddress("txtStreet", "txtStreet", "txtCity", "hdnGoogleState", "txtPostal", "ddlBillCountry");
        });

        function Save() {
            window.opener.location.href = window.opener.location.href;
            self.close();
        }
    </script>
    <style type="text/css">
        #tblAddress > tbody > tr > td:first-child {
            font-weight:bold;
        }

        #tblAddress > tbody > tr {
            height:30px;
        }

         .rcbInput {
            height: 19px !important;
            padding-right:5px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td>
                <asp:Literal ID="lblHistory" runat="server" Visible="false"></asp:Literal>
            </td>
            <td>
                 <div class="input-part">
        <div class="right-input">
             <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save & Close" Width="60" />
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="60" />
        </div>
    </div>
               
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center">
                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>
            </td>
        </tr>
    </table>
    <div class="input-part">
        <div class="left-input">
            
        </div>
        <div class="right-input">
            
        </div>
    </div>
    <div class="input-part">
        
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Address
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <table id="tblAddress" width="800px">
        <tr>
            <td class="text" align="left">
                <asp:RadioButton ID="rbCompany" runat="server" GroupName="Address" CssClass="signup"
                    Text="Use Company Address" AutoPostBack="true" Checked="true" />
            </td>
            <td>
                <telerik:RadComboBox AccessKey="C" ID="radCmbCompany"  style="width:200px !important" DropDownWidth="600px"
                    OnClientItemsRequested="OnClientItemsRequestedOrganization"
                    ClientIDMode="Static" EnableScreenBoundaryDetection="true"
                    ShowMoreResultsBox="true"
                    Skin="Default" runat="server" AutoPostBack="True" AllowCustomText="True" EnableLoadOnDemand="True">
                    <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                </telerik:RadComboBox>
                <%--  <rad:RadComboBox ID="radCmbCompany" ExternalCallBackPage="../include/LoadCompany.aspx"
                    Width="200px" DropDownWidth="200px" Skin="WindowsXP" runat="server" AutoPostBack="True"
                    AllowCustomText="True" EnableLoadOnDemand="True">
                </rad:RadComboBox>--%>
                <asp:DropDownList runat="server" ID="ddlAddressName" CssClass="signup" AutoPostBack="true"  style="width:120px !important">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="text" align="left" Width="160">
                <asp:RadioButton ID="rbContact" runat="server" GroupName="Address" CssClass="signup"
                    Text="Use Contact Address" AutoPostBack="true" />
            </td>
            <td class="text" align="left">
                <asp:DropDownList ID="ddlContact" CssClass="normal1" runat="server" AutoPostBack="true" style="width:200px !important">
                </asp:DropDownList>
                <asp:DropDownList ID="ddlAddressType" CssClass="normal1" runat="server" AutoPostBack="true" style="width:120px !important">
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="trProject" runat="server" visible="false">
            <td class="text" align="left" Width="160">
                <asp:RadioButton ID="rbProject" runat="server" GroupName="Address" CssClass="signup" Text="Project/Job Site" />
            </td>
            <td>
                <asp:DropDownList ID="ddlProject" CssClass="normal1" runat="server" AutoPostBack="true" style="width:200px !important" OnSelectedIndexChanged="ddlProject_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="text" align="left" colspan="2">
                <asp:CheckBox ID="chkTogether" runat="server" CssClass="signup" Text="Show Contact and Customer Together"
                    AutoPostBack="true" />
            </td>
        </tr>
        <tr>
            <td class="text" align="right">
                <asp:Label ID="lblAddressType" runat="server"></asp:Label>
            </td>
            <td>
                <asp:TextBox ID="txtName" runat="server" CssClass="signup" style="width:322px !important"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <table style="border-spacing: 0px;">
                    <tr>
                        <td>
                            <asp:RadioButton ID="rbAddressContact" runat="server" Font-Bold="true" GroupName="BillingContact" Checked="true" Text="Contact" />
                            <br />
                            <asp:DropDownList ID="ddlAddressContact" runat="server" Width="160"></asp:DropDownList>
                        </td>
                        <td>
                            <asp:RadioButton ID="rbAddressContactAlt" runat="server" Font-Bold="true" GroupName="BillingContact" Checked="false" Text="Contact Alt" />
                            <br />
                            <asp:TextBox ID="txtAddressContactAlt" runat="server" Width="160"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="text" align="right">Address Name
            </td>
            <td>
                <asp:TextBox ID="txtAddressName" runat="server" CssClass="signup" style="width:322px !important" MaxLength="50"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="text" align="right">Street
            </td>
            <td>
                <asp:TextBox ID="txtStreet" runat="server" CssClass="signup" TextMode="MultiLine"
                     style="width:322px !important"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="text" align="right">City
            </td>
            <td>
                <asp:TextBox ID="txtCity" runat="server" CssClass="signup" style="width:322px !important"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="text" align="right">State
            </td>
            <td>
                <asp:DropDownList ID="ddlBillState" runat="server" style="width:322px !important" CssClass="signup">
                </asp:DropDownList>
                <asp:HiddenField ID="hdnGoogleState" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="text" align="right">Postal
            </td>
            <td>
                <asp:TextBox ID="txtPostal" runat="server" CssClass="signup" style="width:322px !important"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="text" align="right">Country
            </td>
            <td>
                <asp:DropDownList ID="ddlBillCountry" AutoPostBack="True" runat="server" style="width:322px !important"
                    CssClass="signup">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="text" align="right" colspan="2">&nbsp;<br />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table id="tblDropShipAddress" runat="server" visible="false">
                    <tr>
                        <td class="normal1" align="left">
                            <asp:CheckBox ID="chkDropShip" runat="server" Text="" />
                        </td>
                        <td class="normal1">Use DropShip �Ship-To� Company and/or Name/Address
                        </td>
                        <td valign="middle">
                            <asp:Button ID="btnUpdate" runat="server" CssClass="button" Text="Update" Width="60" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
