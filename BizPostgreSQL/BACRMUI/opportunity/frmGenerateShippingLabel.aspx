﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmGenerateShippingLabel.aspx.vb"
    Inherits=".frmGenerateShippingLabel" MasterPageFile="~/common/Popup.Master" ClientIDMode="Static" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script language="javascript" type="text/javascript">
        $(document).ready(function () {
            InitializeValidation();
        });

        function Save() {
            if (document.form1.ddlFromState.selectedIndex == 0) {
                alert("Please Select From State");
                document.form1.ddlFromState.focus();
                return false;
            }
            if (document.form1.ddlToState.selectedIndex == 0) {
                alert("Please Select To State");
                document.form1.ddlToState.focus();
                return false;
            }
            if (document.getElementById('txtFromZip').value.length < 5) {
                alert("Please enter valid From Zip");
                document.getElementById('txtFromZip').focus();
                return false;
            }
            if (document.getElementById('txtToZip').value.length < 5) {
                alert("Please enter valid To Zip");
                document.getElementById('txtToZip').focus();
                return false;
            }
            if (document.form1.ddlFromCountry.selectedIndex == 0) {
                alert("Please Select From Country");
                document.form1.ddlFromCountry.focus();
                return false;
            }
            if (document.form1.ddlToCountry.selectedIndex == 0) {
                alert("Please Select To Country");
                document.form1.ddlToCountry.focus();
                return false;
            }
            if (document.form1.hdnShippingCompany.value == "90") { //USPS
                if (document.form1.ddlServiceType.selectedIndex == 0) {
                    alert("Please Select Service Type");
                    document.form1.ddlServiceType.focus();
                    return false;
                }
                $("#txtTotalWeightInOunce").each(function () {
                    if (Number($(this).val() <= 0)) {
                        alert("Please enter weight \n Note:The weight must be greater than zero and less than 70 lbs.");
                        $(this).focus();
                        event.preventDefault();
                    }
                });
            }

            return true;
        }
        function getSelectedValues() {

            var i;
            var strSelected = ''
            for (i = 0; i < document.getElementById('form1').elements.length; i++) {
                var elm = document.getElementById('form1').elements[i];
                if (elm.type == "radio" && elm.name.indexOf("G") != -1) {
                    if (elm.checked) {
                        strSelected = strSelected + elm.value + ','
                    }
                }
            }
            document.getElementById('hdnSelectedIDs').value = strSelected;
        }
        function CheckNumber(cint, e) {
            var k;
            document.all ? k = e.keyCode : k = e.which;
            if (cint == 1) {
                if (!(k > 47 && k < 58 || k == 44 || k == 46 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
            if (cint == 2) {
                if (!(k > 47 && k < 58 || k == 8 || k == 37 || k == 39 || k == 16)) {
                    if (e.preventDefault) {
                        e.preventDefault();
                    }
                    else
                        e.returnValue = false;
                    return false;
                }
            }
        }

        function ManageControl() {
            if (document.form1.chkCOD.checked == true) {
                document.getElementById('tblCOD').style.display = 'block';
                //document.getElementById('trCOD2').style.display = 'block';
            }
            else {
                document.getElementById('tblCOD').style.display = 'none';
                //document.getElementById('trCOD2').style.display = 'none';
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table align="center" width="100%">
        <tr>
            <td align="center" class="normal4">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    <div class="input-part">
        <div class="right-input">
            <table>
                <tr>
                    <td class="normal1" align="right">
                        <asp:CheckBox ID="chkUseDimensions" runat="server" Text="Use Dimensions" CssClass="signup" />&nbsp;
                        <asp:Button ID="btnUpdateRates" runat="server" Text="Update Rates" CssClass="btn btn-primary" OnClick="btnUpdateRates_Click" />
                        <asp:Button ID="btnGetRates" runat="server" CssClass="button" Text="Get Rates" Visible="false"></asp:Button>
                        <asp:Button ID="btnGenShippingLabel" runat="server" CssClass="button" Text="Generate Labels, Tracking #s, & Update Ship Amt"
                            OnClientClick="return Save();"></asp:Button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Generate Shipping Label
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager runat="server" ID="sc1" />
    <table width="900px">
        <tr>
            <td valign="top"></td>
        </tr>
        <tr>
            <td class="normal1" valign="top">
                <asp:Table ID="tblItems" BorderWidth="1" CssClass="aspTable" CellPadding="2" CellSpacing="2"
                    Height="" runat="server" Width="800px" BorderColor="black" GridLines="None">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <telerik:RadGrid ID="radBoxes" runat="server" Width="100%" AutoGenerateColumns="False"
                                GridLines="None" ShowFooter="false" Skin="windows" EnableEmbeddedSkins="false"
                                CssClass="dg" AlternatingItemStyle-CssClass="ais" ItemStyle-CssClass="is" HeaderStyle-CssClass="hs"
                                AutoGenerateHierarchy="false">
                                <MasterTableView DataKeyNames="numBoxID" HierarchyLoadMode="Client" DataMember="Box">
                                    <DetailTables>
                                        <telerik:GridTableView Width="100%" runat="server" ItemStyle-HorizontalAlign="Center"
                                            AlternatingItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            ShowFooter="false" DataMember="BoxItems" ClientIDMode="Static">
                                            <ParentTableRelation>
                                                <telerik:GridRelationFields DetailKeyField="numBoxID" MasterKeyField="numBoxID" />
                                            </ParentTableRelation>
                                            <Columns>
                                                <telerik:GridBoundColumn HeaderText="numBoxID" DataField="numBoxID" Visible="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="ShippingReportItemId" DataField="ShippingReportItemId"
                                                    Visible="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="numOppBizDocItemID" DataField="numOppBizDocItemID"
                                                    Visible="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="numItemCode" DataField="numItemCode" Visible="false">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Item" DataField="vcItemName">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Model #" DataField="vcModelId">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Description" DataField="vcItemDesc">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Weight per Unit" DataField="fltTotalWeight">
                                                </telerik:GridBoundColumn>
                                                <%--<telerik:GridTemplateColumn HeaderText="Quantity">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBoxQty" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "intBoxQty")%>'></asp:Label>
                                                        <asp:HiddenField ID="hdnUnitName" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "vcUnitName")%>' />
                                                        <asp:HiddenField ID="hdnUnitPrice" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "monUnitPrice")%>' />
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>--%>
                                                <telerik:GridBoundColumn HeaderText="Quantity" DataField="intBoxQty">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridTemplateColumn HeaderText="Dimensions = ( H x L x W ) ">
                                                    <ItemTemplate>
                                                        <%# DataBinder.Eval(Container.DataItem, "fltHeight")%>
                                                        x
                                                        <%# DataBinder.Eval(Container.DataItem, "fltLength")%>
                                                        x
                                                        <%# DataBinder.Eval(Container.DataItem, "fltWidth")%>
                                                    </ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                        </telerik:GridTableView>
                                    </DetailTables>
                                    <Columns>
                                        <telerik:GridBoundColumn HeaderText="numBoxID" DataField="numBoxID" Visible="false"
                                            UniqueName="numBoxID">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Box" DataField="vcBoxName">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn HeaderText="Dimensions = ( H x L x W )" ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:TextBox CssClass="signup" ID="txtHeight" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"fltHeight") %>'
                                                    onkeypress="CheckNumber(1,event)" Width="25px"></asp:TextBox>&nbsp;x&nbsp;
                                                <asp:TextBox CssClass="signup" ID="txtLength" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"fltLength") %>'
                                                    onkeypress="CheckNumber(1,event)" Width="25px"></asp:TextBox>&nbsp;x&nbsp;
                                                <asp:TextBox CssClass="signup" ID="txtWidth" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"fltWidth") %>'
                                                    onkeypress="CheckNumber(1,event)" Width="25px"></asp:TextBox>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Total weight" ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:TextBox CssClass="signup" ID="txtTotalWeight" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "fltTotalWeightForShipping")%>'
                                                    onkeypress="CheckNumber(1,event)" Width="50px"></asp:TextBox>&nbsp;lbs
                                                <asp:TextBox CssClass="signup" ID="txtTotalWeightInOunce" runat="server" Text=''
                                                    Visible="false" onkeypress="CheckNumber(1,event)" Width="25px" ClientIDMode="Static"></asp:TextBox><asp:Label
                                                        Text="Oz" runat="server" ID="lblOunces" Visible="false" />
                                                <asp:HiddenField ID="hdnBoxWeight" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"fltTotalWeight") %>' />
                                                <asp:HiddenField ID="hdnDimensionalWeight" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"fltDimensionalWeight") %>' />
                                                <asp:HiddenField ID="hdnPackageTypeID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numPackageTypeID")%>' />
                                                <asp:HiddenField ID="hdnServiceTypeID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numServiceTypeID")%>' />
                                                <asp:HiddenField ID="hdnPayerType" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "tintPayorType")%>' />
                                                <asp:HiddenField ID="hdnPayerAccountNo" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "vcPayorAccountNo")%>' />
                                                <asp:HiddenField ID="hdnPayerCountryCode" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numPayorCountry")%>' />
                                                <asp:HiddenField ID="hdnPayorZipCode" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"vcPayorZip") %>' />
                                                <asp:HiddenField ID="hdnShipCompany" runat="server" Value='<%# DataBinder.Eval(Container.DataItem,"numShipCompany") %>' />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Quantity" Visible="false">
                                            <ItemTemplate>
                                                <%--<asp:Label ID="lblBoxQty" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"intBoxQty") %>'></asp:Label>--%>
                                                <asp:Label ID="lblBoxQty" runat="server" Text="1"></asp:Label>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Service Type">
                                            <ItemTemplate>
                                                <asp:DropDownList CssClass="signup" ID="ddlServiceType" runat="server"></asp:DropDownList>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridTemplateColumn HeaderText="Package Type">
                                            <ItemTemplate>
                                                <asp:DropDownList CssClass="signup" ID="ddlPackagingType" runat="server"></asp:DropDownList>
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                        <%--<telerik:GridBoundColumn HeaderText="Delivery Date" DataField="dtDeliveryDate">
                                        </telerik:GridBoundColumn>--%>
                                        <telerik:GridTemplateColumn HeaderText="Rate">
                                            <ItemTemplate>
                                                <asp:Label ID="lblShippingRate" Text='<%# String.Format("{0:#,###.00}",DataBinder.Eval(Container.DataItem,"monShippingRate")) %>'
                                                    runat="server" />
                                            </ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                </MasterTableView>
                                <ClientSettings AllowExpandCollapse="true" />
                            </telerik:RadGrid>
                            <br />
                            <table width="100%" id="tblConfig" runat="server">
                                <tr>
                                    <td colspan="2">
                                        <fieldset>
                                            <legend><b>Ship From Label</b></legend>
                                            <table border="0" cellpadding="2" cellspacing="2" width="100%">
                                                <tr>
                                                    <td class="normal1" align="right" valign="middle" style="width:25%">Contact
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:TextBox CssClass="signup" ID="txtFromName" runat="server" Width="120px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="normal1" align="right" valign="middle">Phone
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:TextBox CssClass="signup" ID="txtFromPhone" runat="server" Width="120px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="normal1" align="right" valign="middle">Company
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:TextBox CssClass="signup" ID="txtFromCompany" runat="server" Width="120px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                    <td colspan="2">
                                        <fieldset>
                                            <legend><b>Ship To Label</b></legend>
                                            <table border="0" cellpadding="2" cellspacing="2" width="100%">
                                                <tr>
                                                    <td class="normal1" align="right" valign="middle" style="width:25%">Contact
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:TextBox CssClass="signup" ID="txtToName" runat="server" Width="120px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="normal1" align="right" valign="middle">Phone
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:TextBox CssClass="signup" ID="txtToPhone" runat="server" Width="120px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="normal1" align="right" valign="middle">Company
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:TextBox CssClass="signup" ID="txtToCompany" runat="server" Width="120px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <fieldset>
                                            <legend><b>Ship From Address</b></legend>
                                            <table border="0" cellpadding="2" cellspacing="2" width="100%">
                                                <tr>
                                                    <td class="normal1" align="right" valign="middle"  style="width:25%">Address Line 1
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:TextBox CssClass="signup" ID="txtFromAddressLine1" runat="server" Width="120px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="normal1" align="right" valign="middle">Address Line 2
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:TextBox CssClass="signup" ID="txtFromAddressLine2" runat="server" Width="120px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="normal1" align="right" valign="middle">From City
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:TextBox CssClass="signup" ID="txtFromCity" runat="server" Width="100px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="normal1" align="right" valign="middle">From State
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:DropDownList CssClass="signup" ID="ddlFromState" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="normal1" align="right" valign="middle">From Zip
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:TextBox CssClass="signup" ID="txtFromZip" runat="server" Width="58px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="normal1" align="right" valign="middle">From Country
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:DropDownList CssClass="signup" ID="ddlFromCountry" AutoPostBack="True" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="normal1" align="left" colspan="2">
                                                        <asp:CheckBox ID="chkFromResidential" runat="server" Text="Is Residential" />
                                                    </td>
                                                </tr>
                                                <%--<tr>
                                                    <td class="normal1" align="right" valign="bottom">
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:CheckBox Text="Update this Address on BizDoc" runat="server" CssClass="signup"
                                                            ID="chkUpdateFromAddress" />
                                                    </td>
                                                </tr>--%>
                                            </table>
                                        </fieldset>
                                    </td>
                                    <td colspan="2">
                                        <fieldset>
                                            <legend><b>Ship To Address</b></legend>
                                            <table border="0" cellpadding="2" cellspacing="2" width="100%">
                                                <tr>
                                                    <td class="normal1" align="right" valign="middle" style="width:25%">Address Line 1
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:TextBox CssClass="signup" ID="txtToAddressLine1" runat="server" Width="120px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="normal1" align="right" valign="middle">Address Line 2
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:TextBox CssClass="signup" ID="txtToAddressLine2" runat="server" Width="120px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="normal1" align="right" valign="middle">To City
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:TextBox CssClass="signup" ID="txtToCity" runat="server" Width="100px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="normal1" align="right" valign="middle">To State
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:DropDownList CssClass="signup" ID="ddlToState" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="normal1" align="right" valign="middle">To Zip
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:TextBox CssClass="signup" ID="txtToZip" runat="server" Width="49px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="normal1" align="right" valign="middle">To Country
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:DropDownList CssClass="signup" ID="ddlToCountry" AutoPostBack="True" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="normal1" align="left" colspan="2">
                                                        <asp:CheckBox ID="chkToResidential" runat="server" Text="Is Residential" />
                                                    </td>
                                                </tr>
                                                <%--  <tr>
                                                    <td class="normal1" align="right" valign="bottom">
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:CheckBox Text="Update this Address on BizDoc" runat="server" CssClass="signup"
                                                            ID="CheckBox1" />
                                                    </td>
                                                </tr>--%>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="left">
                                        <fieldset>
                                            <legend><b>Shipping/Duties Payment Info</b></legend>
                                            <table border="0" cellpadding="2" cellspacing="2" width="100%">
                                                <tr>
                                                    <td class="normal1" align="right" valign="bottom"  style="width:12%">Shipping/Duties Payment Option
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:DropDownList CssClass="signup" ID="ddlPayorType" AutoPostBack="True" runat="server">
                                                            <asp:ListItem Text="Sender" Value="0" Selected="True" />
                                                            <asp:ListItem Text="Recipient" Value="1" />
                                                            <asp:ListItem Text="Third Party" Value="2" />
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td class="normal1" align="right" valign="bottom" runat="server" id="tdPaymentOption1"
                                                        visible="false">
                                                        <asp:Label Text="Account No,ZipCode,Country" runat="server" ID="lblPayorlabel" />
                                                    </td>
                                                    <td class="normal1" align="left" runat="server" id="tdPaymentOption2" visible="false">
                                                        <asp:TextBox runat="server" ID="txtThirdPartyAccountNo" CssClass="signup" MaxLength="20"
                                                            Width="49px" />
                                                        <asp:TextBox runat="server" ID="txtThirdPartyZipCode" CssClass="signup" MaxLength="10"
                                                            Width="49px" Text="" />
                                                        <asp:DropDownList CssClass="signup" ID="ddlThirdPartyCounty" runat="server">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="normal1" align="right" valign="bottom">Reference Number
                                                    </td>
                                                    <td class="normal1" align="left">
                                                        <asp:TextBox runat="server" ID="txtReferenceNo" CssClass="signup" Width="70px" />
                                                    </td>
                                                    <td class="normal1" align="right"></td>
                                                    <td class="normal1" align="left"></td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="left">
                                        <fieldset>
                                            <legend><b>Other Info</b></legend>
                                            <table border="0" cellpadding="2" cellspacing="2" width="100%">
                                                <tr>
                                                    <td class="normal1" align="left" valign="top">
                                                        <asp:CheckBoxList ID="chkOther" runat="server" CellPadding="2" CellSpacing="2" RepeatDirection="Vertical">
 
                                                        </asp:CheckBoxList>
                                                    </td>
                                                    <td class="normal1" runat="server" valign="top">
                                                        <table>
                                                            <tr runat="server" id="trSignType" >
                                                                <td align="right" width="50%">Signature Type
                                                                </td>
                                                                <td width="40%">
                                                                    <asp:DropDownList runat="server" ID="ddlSignatureType">
                                                                        <asp:ListItem Text="Service Default" Value="0"></asp:ListItem>
                                                                        <asp:ListItem Text="Adult Signature Required" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Text="Direct Signature" Value="2"></asp:ListItem>
                                                                        <asp:ListItem Text="InDirect Signature" Value="3"></asp:ListItem>
                                                                        <asp:ListItem Text="No Signature Required" Value="4"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td width="10%"></td>
                                                            </tr>
                                                            <tr runat="server" id="trDesc">
                                                                <td align="right">Description
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">Total Insured Value
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox runat="server" ID="txtTotalInsuredValue" CssClass="required_decimal {required:false ,number:true, messages:{required:'Please provide value!',number:'Provide valid value!'}}" />
                                                                </td>
                                                                <td>USD
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">Total Customs Value
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox runat="server" ID="txtTotalCustomsValue" CssClass="required_decimal {required:false ,number:true, messages:{required:'Please provide value!',number:'Provide valid value!'}}" />
                                                                </td>
                                                                <td>USD
                                                                </td>
                                                            </tr>
                                                            <tr runat="server" visible="false" id="trCOD1">
                                                                <td class="normal1" align="right">COD Type
                                                                </td>
                                                                <td class="normal1">
                                                                    <asp:DropDownList CssClass="signup" ID="ddlCODType" runat="server">
                                                                    </asp:DropDownList>
                                                                </td>
                                                                <td class="normal1"></td>
                                                            </tr>
                                                            <tr runat="server" visible="false" id="trCOD2">
                                                                <td class="normal1" align="right" width="60%">COD Total Amount
                                                                </td>
                                                                <td class="normal1">
                                                                    <asp:TextBox runat="server" ID="txtCODTotalAmount" CssClass="required_decimal {required:false ,number:true, messages:{required:'Please provide value!',number:'Provide valid value!'}}" />
                                                                </td>
                                                                <td class="normal1">USD
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                </tr>
                            </table>
                        </asp:TableCell></asp:TableRow></asp:Table></td></tr></table><asp:HiddenField ID="hdnSelectedIDs" runat="server" />
    <asp:Table ID="tblShipping" Visible="true" BorderWidth="1" CssClass="aspTable" CellPadding="2"
        CellSpacing="2" Height="" runat="server" Width="100%" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <telerik:RadGrid ID="radRateSelection" runat="server" Width="100%" AutoGenerateColumns="False"
                    GridLines="None" ShowFooter="false" Skin="windows" EnableEmbeddedSkins="false"
                    CssClass="dg" AlternatingItemStyle-CssClass="ais" ItemStyle-CssClass="is" HeaderStyle-CssClass="hs"
                    AutoGenerateHierarchy="false" Visible="false">
                    <MasterTableView DataKeyNames="numBoxID" HierarchyLoadMode="Client" DataMember="Box">
                        <DetailTables>
                            <telerik:GridTableView Width="100%" runat="server" ItemStyle-HorizontalAlign="Center"
                                AlternatingItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                ShowFooter="false" DataMember="BoxRate">
                                <ParentTableRelation>
                                    <telerik:GridRelationFields DetailKeyField="numBoxID" MasterKeyField="numBoxID" />
                                </ParentTableRelation>
                                <Columns>
                                    <telerik:GridTemplateColumn HeaderText="Select">
                                        <ItemTemplate>
                                            <input type="radio" value="<%# Eval("ID") %>" name="<%# Eval("GroupName") %>" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn HeaderText="Delivery Day/Date" DataField="Date">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="Service Type" DataField="ServiceType">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="Rate (US$)" DataField="Rate">
                                    </telerik:GridBoundColumn>
                                </Columns>
                            </telerik:GridTableView>
                        </DetailTables>
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="numBoxID" DataField="numBoxID" Visible="false">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Box" DataField="vcBoxName">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Dimensions = ( H x L x W )">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "fltHeight")%>
                                    x
                                    <%# DataBinder.Eval(Container.DataItem, "fltLength")%>
                                    x
                                    <%# DataBinder.Eval(Container.DataItem, "fltWidth")%>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                            <telerik:GridTemplateColumn HeaderText="Total weight">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "fltTotalWeight")%>
                                </ItemTemplate>
                            </telerik:GridTemplateColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings AllowExpandCollapse="true" />
                </telerik:RadGrid>
            </asp:TableCell></asp:TableRow></asp:Table><div style="float: right">
        <asp:Button ID="btnAddToShippingReport" runat="server" Text="Save selection" Visible="false"
            OnClientClick="javascript:getSelectedValues();" CssClass="button" />
        &nbsp; &nbsp; </div><asp:HiddenField ID="hdnShippingCompany" runat="server" />
</asp:Content>

