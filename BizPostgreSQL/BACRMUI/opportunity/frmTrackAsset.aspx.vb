Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Item
Imports Infragistics.WebUI.UltraWebGrid
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Opportunities
    Partial Public Class frmTracksset
        Inherits BACRMPage

        Dim lngDivId As Long = 0
        Dim lngOpId As Long = 0
        Dim lngItemCode As Long = 0

        'Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        '    Try
        '        btnSave.Attributes.Add("onclick", "return Save()")
        '        If GetQueryStringVal( "DivId") <> Nothing Then lngDivId = GetQueryStringVal( "DivId")
        '        If GetQueryStringVal( "OpId") <> Nothing Then lngOpId = GetQueryStringVal( "OpId")
        '        If GetQueryStringVal( "ItemCode") <> Nothing Then lngItemCode = GetQueryStringVal( "ItemCode")

        '        If Not IsPostBack Then
        '            
        '            FillCustomer(ddlcompany, CStr(txtComp.Text))
        '            If ddlcompany.Items.Count > 0 Then
        '                If Not ddlcompany.Items.FindByValue(lngDivId) Is Nothing Then
        '                    ddlcompany.ClearSelection()
        '                    ddlcompany.Items.FindByValue(lngDivId).Selected = True
        '                End If
        '                loadDropDowns()
        '            End If
        '            BindDataGrid()
        '        End If
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub

        'Sub loadDropDowns()
        '    Try
        '        Dim objcompany As New COpportunities
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Sub BindDataGrid()
        '    Try
        '        Dim dtItems As DataTable
        '        Dim objItems As New CItems
        '        objItems.DomainID = Session("DomainId")
        '        objItems.ItemCode = lngItemCode
        '        objItems.OppId = lngOpId
        '        dtItems = objItems.GetAssetItem()
        '        uwItem.DataSource = dtItems
        '        uwItem.DataBind()
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Sub FillCustomer(ByVal ddlCombo As DropDownList, ByVal strName As String)
        '    Try
        '        
        '        With objCommon
        '            .DomainID = Session("DomainID")
        '            .UserCntID = Session("UserContactID")
        '            .Filter = Trim(strName) & "%"
        '            ddlCombo.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
        '            ddlCombo.DataTextField = "vcCompanyname"
        '            ddlCombo.DataValueField = "numDivisionID"
        '            ddlCombo.DataBind()
        '        End With
        '        ddlCombo.Items.Insert(0, New ListItem("--Select One--", "0"))
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        ''Private Sub uwItem_InitializeDataSource(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.UltraGridEventArgs) Handles uwItem.InitializeDataSource
        ''    Try
        ''        BindDataGrid()
        ''    Catch ex As Exception
        ''        Throw ex
        ''    End Try
        ''End Sub

        'Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        '    Try
        '        Save()
        '        Response.Write("<script>window.close()</script>")
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Sub Save()
        '    Try
        '        Dim dtItems As New DataTable
        '        Dim objDR As System.Data.DataRow
        '        dtItems.Columns.Add("numWareHouseItmsDtlId", GetType(Long))
        '        dtItems.Columns.Add("numItemCode", GetType(Long))
        '        dtItems.Columns.Add("type", GetType(Long))
        '        dtItems.Columns.Add("unit", GetType(Long))
        '        dtItems.Columns.Add("numOppId", GetType(Long))

        '        Dim ugRow As UltraGridRow
        '        For Each ugRow In uwItem.Rows

        '            If ugRow.Cells.FromKey("Select").Value = True Then
        '                objDR = dtItems.NewRow
        '                objDR("numWareHouseItmsDtlId") = ugRow.Cells.FromKey("numWareHouseItmsDtlId").Value
        '                objDR("numItemCode") = ugRow.Cells.FromKey("numItemCode").Value
        '                objDR("type") = ugRow.Cells.FromKey("type").Value
        '                objDR("unit") = ugRow.Cells.FromKey("unit").Value
        '                objDR("numOppId") = lngOpId
        '                dtItems.Rows.Add(objDR)
        '            End If
        '        Next
        '        Dim dsNew As New DataSet
        '        Dim strItems As String = ""
        '        If dtItems.Rows.Count > 0 Then
        '            dtItems.TableName = "Table"
        '            dsNew.Tables.Add(dtItems.Copy)
        '            strItems = dsNew.GetXml()

        '            Dim objItems As New CItems
        '            objItems.DivisionID = ddlcompany.SelectedValue
        '            objItems.DomainID = Session("DomainId")
        '            objItems.strItemCodes = strItems
        '            objItems.SaveAssetItem()
        '            dsNew.Tables.Remove(dsNew.Tables(0))
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Private Sub imgCustGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles imgCustGo.Click
        '    Try
        '        FillCustomer(ddlcompany, CStr(txtComp.Text))
        '        BindDataGrid()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub

    End Class
End Namespace