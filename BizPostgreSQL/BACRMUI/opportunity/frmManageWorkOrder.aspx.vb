﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Item
Imports System.Configuration
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Contacts
Imports System.Collections.Generic
Imports System.Linq

Public Class frmManageWorkOrder
    Inherits BACRMPage
    Dim objCommon As CCommon
    Dim objItems As New CItems
    Dim dsTableInfo As DataSet
    Dim listTreeListItems As List(Of TreeListDataItem)
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
            GetUserRightsForPage(10, 24)

            If Not IsPostBack Then
                Dim m_aryRightsForPurchaseOrder() As Integer = GetUserRightsForPage_Other(10, 35)
                If m_aryRightsForPurchaseOrder(RIGHTSTYPE.VIEW) = 0 Then
                    btnPurchaseOrder.Visible = False
                Else
                    'Check if auto create purchase order global settings is enabled
                    objCommon = New CCommon
                    objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                    Dim dt As DataTable = objCommon.GetDomainSettingValue("bitReOrderPoint")

                    If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                        If CCommon.ToBool(dt.Rows(0)("bitReOrderPoint")) Then
                            btnPurchaseOrder.Visible = True
                        Else
                            btnPurchaseOrder.Visible = False
                        End If
                    Else
                        btnPurchaseOrder.Visible = False
                    End If
                End If
       
                objCommon = New CCommon
                objCommon.sb_FillComboFromDBwithSel(ddlAssemblyStatus, 302, Session("DomainID"))
                txtCurrrentPage.Text = 1

                PersistTable.Load()
                If PersistTable.Count > 0 Then
                    If Not ddlAssemblyStatus.Items.FindByValue(CCommon.ToString(PersistTable("ddlAssemblyStatus"))) Is Nothing Then
                        ddlAssemblyStatus.SelectedValue = CCommon.ToString(PersistTable("ddlAssemblyStatus"))
                    End If

                    If Not ddlFilter.Items.FindByValue(CCommon.ToString(PersistTable("ddlFilter"))) Is Nothing Then
                        ddlFilter.SelectedValue = CCommon.ToString(PersistTable("ddlFilter"))
                    End If
                End If

                bindItemsGrid()
            End If

            If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                btnSave.Visible = False
            End If
            If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                lkbDelete.Visible = False
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Sub bindItemsGrid()
        Try
            'litError.Text = ""
            Dim objItems As New CItems
            objItems.DomainID = Session("DomainID")
            objItems.vcWOId = CCommon.ToString(GetQueryStringVal("numWOID"))
            objItems.OppId = CCommon.ToLong(GetQueryStringVal("oppID"))
            objItems.OppItemCode = CCommon.ToLong(GetQueryStringVal("oppItemID"))
            objItems.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            objItems.PageSize = Convert.ToInt32(Session("PagingRows"))

            If CCommon.ToLong(GetQueryStringVal("oppID")) > 0 AndAlso CCommon.ToLong(GetQueryStringVal("oppItemID")) > 0 Then
                objItems.numWOStatus = 0
                objItems.Filter = 0
                objItems.UserCntID = 0
            Else
                objItems.numWOStatus = ddlAssemblyStatus.SelectedValue
                objItems.Filter = ddlFilter.SelectedValue
                objItems.UserCntID = IIf(ddlFilter.SelectedValue = 1, Session("UserContactID"), 0)
            End If

            If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
            objItems.CurrentPage = Convert.ToInt32(txtCurrrentPage.Text.Trim())

            If txtSortColumn.Text <> "" Then
                objItems.columnName = txtSortColumn.Text.Replace("vcCreatedBy", "Dtl.bintCreatedDate")
            Else : objItems.columnName = "numWOId"
            End If

            Dim ds As DataSet
            ds = objItems.GetWorkOrder
            rtlWorkOrder.DataSource = ds.Tables(0)
            rtlWorkOrder.DataBind()
            rtlWorkOrder.ExpandAllItems()

            bizPager.PageSize = Session("PagingRows")
            bizPager.RecordCount = ds.Tables(1).Rows(0).Item("TotalRowCount")
            bizPager.CurrentPageIndex = txtCurrrentPage.Text
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Protected Sub btnPrint_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPrint.Click
        Try
            Dim objItems As New CItems
            Dim listWorkOrder As New List(Of WorkOrderDetail)
            Dim numWOID As String = ""
            For Each dataGridItems As TreeListDataItem In rtlWorkOrder.Items
                If CType(dataGridItems.FindControl("chkDelete"), CheckBox).Checked = True Then
                    numWOID = numWOID + "," + dataGridItems.Item("numWOID").Text

                End If
            Next
            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "PrintOrder", "PrintWorkOrder('" + numWOID + "');", True)
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Try
            'Dim listWorkOrder As New List(Of WorkOrderDetail)
            'Dim objCommon As New CCommon
            'objCommon.DomainID = Session("DomainID")
            'Dim dtCommitAllocation As DataTable = objCommon.GetDomainSettingValue("tintCommitAllocation")

            'If dtCommitAllocation Is Nothing Or dtCommitAllocation.Rows.Count = 0 Then
            '    Throw New Exception("Not able to fetch commmit allocation global settings value.")
            '    Exit Sub
            'End If


            'litError.Text = ""
            'Dim bError As Boolean = False
            'For Each dataGridItems As TreeListDataItem In rtlWorkOrder.Items
            '    If CType(dataGridItems.FindControl("ddlWOAssemblyStatus"), DropDownList).Visible = True AndAlso
            '        CType(dataGridItems.FindControl("ddlWOAssemblyStatus"), DropDownList).Enabled = True Then
            '        Dim objWorkOrderDetail As New WorkOrderDetail
            '        objWorkOrderDetail.numWOID = CCommon.ToLong(dataGridItems.Item("numWOID").Text)
            '        objWorkOrderDetail.numWOStatus = CCommon.ToLong(CType(dataGridItems.FindControl("ddlWOAssemblyStatus"), DropDownList).SelectedValue)
            '        objWorkOrderDetail.numLevel = dataGridItems.HierarchyIndex.NestedLevel
            '        listWorkOrder.Add(objWorkOrderDetail)
            '    End If
            'Next

            'For Each item As WorkOrderDetail In listWorkOrder.OrderByDescending(Function(x) x.numLevel)
            '    Dim objItems As New CItems
            '    objItems.numWOId = item.numWOID
            '    objItems.numWOStatus = item.numWOStatus
            '    objItems.UserCntID = Session("UserContactID")
            '    objItems.DomainID = Session("DomainID")

            '    If item.numWOStatus = 23184 Then
            '        Dim dtTable As DataTable
            '        dtTable = objItems.ValidateeWorkOrderInventory()

            '        If dtTable.Rows.Count > 0 Then
            '            Dim dr As DataRow

            '            For Each dr In dtTable.Rows
            '                If CCommon.ToShort(dtCommitAllocation.Rows(0)("tintCommitAllocation")) = 2 Then
            '                    litError.Text += "<br/>WO " & objItems.numWOId & " Item: " & dr("vcItemName") & " WO-Qty: " & dr("numQtyItemsReq") & " On-Hand: " & dr("numOnHand")
            '                Else
            '                    litError.Text += "<br/>WO " & objItems.numWOId & " Item: " & dr("vcItemName") & " WO-Qty: " & dr("numQtyItemsReq") & " On-Allocation: " & dr("numAllocation")
            '                End If

            '                bError = True
            '            Next
            '        Else
            '            objItems.ManageWorkOrderStatus()
            '            objItems.AdjustInventory(0, 1)
            '        End If
            '    Else
            '        objItems.ManageWorkOrderStatus()
            '    End If
            'Next


            'If bError = True Then
            '    litError.Text = "Some work order could not be completed due to lack of required quantity to fullfill work order:" + litError.Text
            '    Exit Sub
            'End If

            'bindItemsGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub ddlAssemblyStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlAssemblyStatus.SelectedIndexChanged
        Try
            txtCurrrentPage.Text = 1
            bindItemsGrid()

            If (ddlAssemblyStatus.SelectedValue = "23184") Then
                btnSave.Visible = False
                lkbDelete.Visible = False
            Else
                btnSave.Visible = True
                lkbDelete.Visible = True
            End If

            'Persist Form Settings
            PersistTable.Clear()
            PersistTable.Add("ddlFilter", ddlFilter.SelectedValue)
            PersistTable.Add("ddlAssemblyStatus", ddlAssemblyStatus.SelectedValue)
            PersistTable.Save()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub lkbDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lkbDelete.Click
        Try
            Dim objItems As New CItems
            Dim listWorkOrder As New List(Of WorkOrderDetail)

            For Each dataGridItems As TreeListDataItem In rtlWorkOrder.Items
                If CType(dataGridItems.FindControl("ddlWOAssemblyStatus"), DropDownList).Visible = True AndAlso
                    CType(dataGridItems.FindControl("ddlWOAssemblyStatus"), DropDownList).Enabled = True Then
                    'DELETE CHECK BOX IS VISIBLE FOR TOP MOST PARENT ITEM ONLY 
                    'SO IN CHILD ITEMS WE HAVE TO LOOK AT WHERE PARENT IS CHECKED OR NOT
                    If dataGridItems.HierarchyIndex.NestedLevel = 0 Then
                        If CType(dataGridItems.FindControl("chkDelete"), CheckBox).Checked = True Then
                            Dim objWorkOrderDetail As New WorkOrderDetail
                            objWorkOrderDetail.numWOID = CCommon.ToLong(dataGridItems.Item("numWOID").Text)
                            objWorkOrderDetail.numWOStatus = CCommon.ToLong(CType(dataGridItems.FindControl("ddlWOAssemblyStatus"), DropDownList).SelectedValue)
                            objWorkOrderDetail.numLevel = dataGridItems.HierarchyIndex.NestedLevel
                            listWorkOrder.Add(objWorkOrderDetail)
                        End If
                    Else
                        If listWorkOrder.Where(Function(x) x.numWOID = CCommon.ToLong(dataGridItems.ParentItem.Item("numWOID").Text)).Count() > 0 Then
                            Dim objWorkOrderDetail As New WorkOrderDetail
                            objWorkOrderDetail.numWOID = CCommon.ToLong(dataGridItems.Item("numWOID").Text)
                            objWorkOrderDetail.numWOStatus = CCommon.ToLong(CType(dataGridItems.FindControl("ddlWOAssemblyStatus"), DropDownList).SelectedValue)
                            objWorkOrderDetail.numLevel = dataGridItems.HierarchyIndex.NestedLevel
                            listWorkOrder.Add(objWorkOrderDetail)
                        End If
                    End If
                End If
            Next

            Dim isWorkOrerClosedOrTaskStarted As Boolean = False
            Dim objWorkOrder As New BACRM.BusinessLogic.Opportunities.WorkOrder
            objWorkOrder.DomainID = CCommon.ToLong(Session("DomainID"))

            Using objTransactionScope As New Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                For Each item As WorkOrderDetail In listWorkOrder.OrderByDescending(Function(x) x.numLevel)
                   
                    objWorkOrder.WorkOrderID = item.numWOID
                    If objWorkOrder.IsCompleted() Then
                        isWorkOrerClosedOrTaskStarted = True
                    Else
                        If objWorkOrder.IsTaskStarted() Then
                            isWorkOrerClosedOrTaskStarted = True
                        Else
                             objItems.numWOId = item.numWOID
                            objItems.UserCntID = Session("UserContactID")
                            objItems.DomainID = Session("DomainID")
                            objItems.bitOrderDelete = False
                            objItems.DeleteWorkOrder()
                        End If
                    End If
                Next

                objTransactionScope.Complete()
            End Using

            If isWorkOrerClosedOrTaskStarted Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "Delete", "alert('Some work order(s) are not deleted because they are marked as finished or work order tasks are started.')", True)
            End If

            bindItemsGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilter.SelectedIndexChanged
        Try
            txtCurrrentPage.Text = 1
            bindItemsGrid()

            'Persist Form Settings
            PersistTable.Clear()
            PersistTable.Add("ddlFilter", ddlFilter.SelectedValue)
            PersistTable.Add("ddlAssemblyStatus", ddlAssemblyStatus.SelectedValue)
            PersistTable.Save()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub rtlWorkOrder_ItemCommand(sender As Object, e As TreeListCommandEventArgs)
        Try
            If e.CommandName = RadTreeList.ExpandCollapseCommandName Then
                'bindItemsGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub rtlWorkOrder_ItemDataBound(sender As Object, e As TreeListItemDataBoundEventArgs)
        Try
            If e.Item.ItemType = TreeListItemType.DetailTemplateItem Then
                If DirectCast(DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDetailTemplateItem).DataItem, Object), System.Data.DataRowView).Row("numParentWOID") = 0 Then
                    e.Item.Visible = True
                Else
                    e.Item.Visible = False
                End If

                Dim uniqueID As String = DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDetailTemplateItem).DataItem, System.Data.DataRowView).Row("ID")
                DirectCast(e.Item, Telerik.Web.UI.TreeListDetailTemplateItem).Attributes.Add("UniqueID", uniqueID)

            ElseIf e.Item.ItemType = TreeListItemType.Item Or e.Item.ItemType = TreeListItemType.AlternatingItem Then

                Dim btn As Button = TryCast(e.Item.FindControl("ExpandCollapseButton"), Button)
                Dim uniqueID As String = DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).DataItem, System.Data.DataRowView).Row("ID")

                If btn IsNot Nothing Then
                    btn.Attributes.Add("UniqueID", uniqueID)
                    btn.Attributes.Add("onclick", "return ExpandCollapse(" + btn.ClientID + ");")
                End If

                DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).Attributes.Add("UniqueID", uniqueID)

                Dim chkDelete As CheckBox = DirectCast(e.Item.FindControl("chkDelete"), CheckBox)
                Dim ddlWOAssemblyStatus As DropDownList = DirectCast(e.Item.FindControl("ddlWOAssemblyStatus"), DropDownList)
                objCommon = New CCommon
                objCommon.sb_FillComboFromDBwithSel(ddlWOAssemblyStatus, 302, Session("DomainID"))

                Dim numWOStatus As String = CCommon.ToString(DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).DataItem, System.Data.DataRowView).Row("numWOStatus"))

                If Not ddlWOAssemblyStatus.Items.FindByValue(numWOStatus) Is Nothing Then
                    ddlWOAssemblyStatus.Items.FindByValue(numWOStatus).Selected = True

                    If numWOStatus = 23184 Then
                        ddlWOAssemblyStatus.Enabled = False
                    End If
                End If

                'User should not be able to delete child work order directly
                If DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).DataItem, System.Data.DataRowView).Row("numParentWOID") > 0 Then
                    chkDelete.Visible = False
                End If

                'Work Order Status drop down should be visible for assembly items only and not for child items
                If CCommon.ToBool(DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).DataItem, System.Data.DataRowView).Row("bitWorkOrder")) Then
                    ddlWOAssemblyStatus.Visible = True
                Else
                    ddlWOAssemblyStatus.Visible = False
                End If

                If Not CCommon.ToBool(DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).DataItem, System.Data.DataRowView).Row("bitWorkOrder")) AndAlso ddlFilter.SelectedValue <> "2" AndAlso numWOStatus <> "23184" Then
                    If Not CCommon.ToBool(DirectCast(DirectCast(e.Item, Telerik.Web.UI.TreeListDataItem).DataItem, System.Data.DataRowView).Row("bitReadyToBuild")) Then
                        DirectCast(e.Item.FindControl("chkBackOrder"), CheckBox).Visible = True
                        e.Item.Style.Add("background-color", "#FFE1E1 !important")
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub

    Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
        Try
            txtCurrrentPage.Text = "1"
            ddlAssemblyStatus.SelectedValue = "0"
            ddlFilter.SelectedValue = "0"

            'Persist Form Settings
            PersistTable.Clear()
            PersistTable.Add("ddlFilter", ddlFilter.SelectedValue)
            PersistTable.Add("ddlAssemblyStatus", ddlAssemblyStatus.SelectedValue)
            PersistTable.Save()

            bindItemsGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
        Try
            txtCurrrentPage.Text = bizPager.CurrentPageIndex
            bindItemsGrid()

            'txtCurrrentPage_TextChanged(Nothing, Nothing)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
    Private Sub DisplayError(ByVal ex As String)
        DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
        DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
    End Sub
    Private Class WorkOrderDetail
        Private _numWOID As Long
        Private _numWOStatus As Long
        Private _numLevel As Integer

        Public Property numWOID() As Long
            Get
                Return _numWOID
            End Get
            Set(ByVal value As Long)
                _numWOID = value
            End Set
        End Property
        Public Property numWOStatus() As Long
            Get
                Return _numWOStatus
            End Get
            Set(ByVal value As Long)
                _numWOStatus = value
            End Set
        End Property
        Public Property numLevel() As Integer
            Get
                Return _numLevel
            End Get
            Set(ByVal value As Integer)
                _numLevel = value
            End Set
        End Property
    End Class

    Private Sub btnPurchaseOrder_Click(sender As Object, e As EventArgs) Handles btnPurchaseOrder.Click
        Try
            Dim listBackOrderItems As New List(Of String)

            For Each dataGridItems As TreeListDataItem In rtlWorkOrder.Items
                If Not CType(dataGridItems.FindControl("ddlWOAssemblyStatus"), DropDownList).Visible AndAlso CType(dataGridItems.FindControl("chkBackOrder"), CheckBox).Visible AndAlso CType(dataGridItems.FindControl("chkBackOrder"), CheckBox).Checked Then
                    If listBackOrderItems.Where(Function(x) x = dataGridItems("numItemCode").Text & "-" & dataGridItems("numWarehouseItemID").Text).Count() = 0 Then
                        listBackOrderItems.Add(dataGridItems("numItemCode").Text & "-" & dataGridItems("numWarehouseItemID").Text)
                    End If
                End If
            Next

            If listBackOrderItems.Count > 0 Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "SelectBackOrderItems", "OpenBackOrderPOWindow('" & String.Join(",", listBackOrderItems) & "')", True)
            Else
                ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "SelectBackOrderItems", "Please select atleast one back order item", True)
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub
End Class