﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Opportunities
    Partial Public Class frmSalesReturns
        Inherits BACRMPage
        Dim objOpportunity As MOpportunity


        Dim iOppType As Integer

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'HIDE ERROR DIV
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                'HIDE ALERT DIV
                divAlert.Visible = False
                lblMessage.Text = ""

                GetUserRightsForPage(10, 14)
                iOppType = CCommon.ToInteger(GetQueryStringVal("type"))

                If Not IsPostBack Then

                    Select Case iOppType
                        Case 1
                            lblReturns.Text = "Sales Returns"
                            gvSalesReturns.Columns(2).HeaderText = "RMA #"
                            btnAddNewReturn.Attributes.Add("onclick", "return OpenPopUp('../Opportunity/frmNewReturn.aspx?ReturnType=1');")
                        Case 2
                            lblReturns.Text = "Purchase Returns"
                            gvSalesReturns.Columns(2).HeaderText = "RMA #"
                            btnAddNewReturn.Attributes.Add("onclick", "return OpenPopUp('../Opportunity/frmNewReturn.aspx?ReturnType=2');")
                        Case 3
                            lblReturns.Text = "Credit Memo"
                            gvSalesReturns.Columns(2).HeaderText = "Credit Memo #"
                        Case 4
                            lblReturns.Text = "Refund"
                            gvSalesReturns.Columns(2).HeaderText = "Refund #"
                    End Select

                    'Page.Title = lblReturns.Text
                    PageTitle.InnerHtml = lblReturns.Text

                    PersistTable.Load(boolOnlyURL:=True)
                    hdnSortColumn.Value = CCommon.ToString(PersistTable(hdnSortColumn.ID))
                    hdnSortDirection.Value = CCommon.ToString(PersistTable(hdnSortDirection.ID))
                    hdnOrgSearch.Value = CCommon.ToString(PersistTable(hdnOrgSearch.ID))
                    hdnRMASearch.Value = CCommon.ToString(PersistTable(hdnRMASearch.ID))
                    hdnReason.Value = CCommon.ToString(PersistTable(hdnReason.ID))
                    hdnStatus.Value = CCommon.ToString(PersistTable(hdnStatus.ID))

                    ViewState("SortExpression") = CCommon.ToString(PersistTable(hdnSortColumn.ID))
                    ViewState("SortDirection") = CCommon.ToString(PersistTable(hdnSortDirection.ID))

                    'KEEP IT AT LAST ABOVE LoadTicklerDetails
                    If CCommon.ToShort(GetQueryStringVal("IsFormDashboard")) = 1 Then
                        hdnSortColumn.Value = CCommon.ToString(PersistTable(hdnSortColumn.ID))
                        hdnSortDirection.Value = CCommon.ToString(PersistTable(hdnSortDirection.ID))
                        hdnOrgSearch.Value = ""
                        hdnRMASearch.Value = ""
                        hdnReason.Value = ""
                        hdnStatus.Value = "301"
                    End If


                    BindGrid()

                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnDelete.Visible = False
                End If

                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub BindGrid()
            Try
                objOpportunity = New MOpportunity
                With objOpportunity
                    .DomainID = CCommon.ToLong(Session("DomainID"))
                    .UserCntID = CCommon.ToLong(Session("UserContactID"))
                    .ReturnStatus = CCommon.ToLong(hdnStatus.Value)
                    .ReasonForReturn = CCommon.ToLong(hdnReason.Value)
                    .SearchText = CCommon.ToString(hdnOrgSearch.Value)
                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()
                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0
                    .OppType = iOppType
                    .RMASearchText = CCommon.ToString(hdnRMASearch.Value)
                    .SortColumn = CCommon.ToString(hdnSortColumn.Value)
                    .SortDirection = IIf(CCommon.ToInteger(hdnSortDirection.Value) = 0, 2, CCommon.ToInteger(hdnSortDirection.Value))
                End With

                objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                Dim dt As DataTable = objOpportunity.GetReturns
                gvSalesReturns.DataSource = dt
                gvSalesReturns.DataBind()


                bizPager.PageSize = Session("PagingRows")
                bizPager.RecordCount = objOpportunity.TotalRecords
                bizPager.CurrentPageIndex = txtCurrrentPage.Text

                'Persist Form Settings
                PersistTable.Clear()
                PersistTable.Add(PersistKey.CurrentPage, IIf(gvSalesReturns.Rows.Count > 0, txtCurrrentPage.Text, "1"))
                PersistTable.Add(hdnSortColumn.ID, hdnSortColumn.Value.Trim())
                PersistTable.Add(hdnSortDirection.ID, hdnSortDirection.Value.Trim())
                PersistTable.Add(hdnOrgSearch.ID, hdnOrgSearch.Value.Trim())
                PersistTable.Add(hdnRMASearch.ID, hdnRMASearch.Value.Trim())
                PersistTable.Add(hdnReason.ID, hdnReason.Value.Trim())
                PersistTable.Add(hdnStatus.ID, hdnStatus.Value.Trim())
                PersistTable.Save(boolOnlyURL:=True)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                'txtCurrrentPage.Text = 1
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub btnDelete_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnDelete.Click
            Try
                objOpportunity = New MOpportunity()
                For Each row As GridViewRow In gvSalesReturns.Rows
                    If CType(row.FindControl("chk"), CheckBox).Checked = True Then
                        With objOpportunity
                            .ReturnID = gvSalesReturns.DataKeys(row.DataItemIndex)("numReturnHeaderID").ToString
                            .DomainID = Session("DomainID")
                            .DeleteSalesReturn()
                        End With
                    End If
                Next
                BindGrid()
            Catch ex As Exception
                If ex.Message = "CreditMemo_Refund" Then
                    DisplayAlert("One or more of these items can not be deleted because they contain a Credit Memo/Refund Receipt.")
                ElseIf ex.Message = "Deposit_Refund_Payment" Then
                    DisplayAlert("This transaction has been used in Deposit Refund. If you want to change or delete it, you must edit the Refund and remove it first.")
                ElseIf ex.Message = "BillPayment_Refund_Payment" Then
                    DisplayAlert("This transaction has been used in Bill Payment Refund. If you want to change or delete it, you must edit the Refund and remove it first.")
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(CCommon.ToString(ex))
                End If
            End Try
        End Sub

        Private Sub gvSalesReturns_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvSalesReturns.RowDataBound
            Try
                If e.Row.RowType = DataControlRowType.Header Then
                    Dim txtRMA As TextBox = CType(e.Row.FindControl("txtRMA"), TextBox)
                    Dim txtCustomer As TextBox = CType(e.Row.FindControl("txtCustomer"), TextBox)
                    Dim ddlReason As DropDownList = CType(e.Row.FindControl("ddlReason"), DropDownList)
                    Dim ddlStatus As DropDownList = CType(e.Row.FindControl("ddlStatus"), DropDownList)


                    objCommon.sb_FillComboFromDBwithSel(ddlReason, 48, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlStatus, 49, Session("DomainID"))

                    txtRMA.Text = CCommon.ToString(hdnRMASearch.Value)
                    txtCustomer.Text = CCommon.ToString(hdnOrgSearch.Value)
                    ddlStatus.SelectedValue = CCommon.ToLong(hdnStatus.Value)
                    ddlReason.SelectedValue = CCommon.ToLong(hdnReason.Value)



                    If CCommon.ToString(ViewState("SortExpression")) = "dtCreateDate" Then
                        Dim lbCreatedDate As LinkButton = CType(e.Row.FindControl("lbCreatedDate"), LinkButton)
                        If CCommon.ToShort(ViewState("SortDirection")) = 1 Then
                            lbCreatedDate.Text += String.Format("&nbsp;<font color='black'>{0}</font>", "&#9650;")
                        ElseIf CCommon.ToShort(ViewState("SortDirection")) = 2 Then
                            lbCreatedDate.Text += String.Format("&nbsp;<font color='black'>{0}</font>", "&#9660;")
                        End If
                    ElseIf CCommon.ToString(ViewState("SortExpression")) = "vcCompanyName" Then
                        Dim lbCustomer As LinkButton = CType(e.Row.FindControl("lbCustomer"), LinkButton)
                        If CCommon.ToShort(ViewState("SortDirection")) = 1 Then
                            lbCustomer.Text += String.Format("&nbsp;<font color='black'>{0}</font>", "&#9650;")
                        ElseIf CCommon.ToShort(ViewState("SortDirection")) = 2 Then
                            lbCustomer.Text += String.Format("&nbsp;<font color='black'>{0}</font>", "&#9660;")
                        End If
                    ElseIf CCommon.ToString(ViewState("SortExpression")) = "vcRMA" Then
                        Dim lbRMA As LinkButton = CType(e.Row.FindControl("lbRMA"), LinkButton)
                        If CCommon.ToShort(ViewState("SortDirection")) = 1 Then
                            lbRMA.Text += String.Format("&nbsp;<font color='black'>{0}</font>", "&#9650;")
                        ElseIf CCommon.ToShort(ViewState("SortDirection")) = 2 Then
                            lbRMA.Text += String.Format("&nbsp;<font color='black'>{0}</font>", "&#9660;")
                        End If
                    ElseIf CCommon.ToString(ViewState("SortExpression")) = "vcCreditFromAccount" Then
                        Dim lbCreditFromAccount As LinkButton = CType(e.Row.FindControl("lbCreditFromAccount"), LinkButton)
                        If CCommon.ToShort(ViewState("SortDirection")) = 1 Then
                            lbCreditFromAccount.Text += String.Format("&nbsp;<font color='black'>{0}</font>", "&#9650;")
                        ElseIf CCommon.ToShort(ViewState("SortDirection")) = 2 Then
                            lbCreditFromAccount.Text += String.Format("&nbsp;<font color='black'>{0}</font>", "&#9660;")
                        End If
                    ElseIf CCommon.ToString(ViewState("SortExpression")) = "ReasonforReturn" Then
                        Dim lbReturn As LinkButton = CType(e.Row.FindControl("lbReturn"), LinkButton)
                        If CCommon.ToShort(ViewState("SortDirection")) = 1 Then
                            lbReturn.Text += String.Format("&nbsp;<font color='black'>{0}</font>", "&#9650;")
                        ElseIf CCommon.ToShort(ViewState("SortDirection")) = 2 Then
                            lbReturn.Text += String.Format("&nbsp;<font color='black'>{0}</font>", "&#9660;")
                        End If
                    ElseIf CCommon.ToString(ViewState("SortExpression")) = "Status" Then
                        Dim lbStatus As LinkButton = CType(e.Row.FindControl("lbStatus"), LinkButton)
                        If CCommon.ToShort(ViewState("SortDirection")) = 1 Then
                            lbStatus.Text += String.Format("&nbsp;<font color='black'>{0}</font>", "&#9650;")
                        ElseIf CCommon.ToShort(ViewState("SortDirection")) = 2 Then
                            lbStatus.Text += String.Format("&nbsp;<font color='black'>{0}</font>", "&#9660;")
                        End If
                    End If
                ElseIf e.Row.RowType = DataControlRowType.DataRow Then
                    Dim lblSalesCreditMemo As LinkButton = DirectCast(e.Row.FindControl("lblSalesCreditMemo"), LinkButton)

                    If Not lblSalesCreditMemo Is Nothing Then
                        Dim dv As DataRowView = DirectCast(e.Row.DataItem, DataRowView)

                        If iOppType = 3 AndAlso (dv("tintReturnType") = 1 And dv("tintReceiveType") = 2) Then
                            lblSalesCreditMemo.Text = " (" & dv("vcBizDocName") & ")"
                            lblSalesCreditMemo.Attributes.Add("onclick", "return OpenBizInvoice(" & dv("numReturnHeaderID") & "," & enmReferenceTypeForMirrorBizDocs.SalesCreditMemo & ",0);")
                            lblSalesCreditMemo.Visible = True
                        ElseIf iOppType = 4 AndAlso (dv("tintReturnType") = 1 And dv("tintReceiveType") = 1) Then
                            lblSalesCreditMemo.Text = " (" & dv("vcBizDocName") & ")"
                            lblSalesCreditMemo.Attributes.Add("onclick", "return OpenBizInvoice(" & dv("numReturnHeaderID") & "," & enmReferenceTypeForMirrorBizDocs.RefundReceipt & ",0);")
                            lblSalesCreditMemo.Visible = True
                        End If
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub ddlReason_SelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                hdnReason.Value = CType(sender, DropDownList).SelectedValue
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub ddlStatus_SelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                hdnStatus.Value = CType(sender, DropDownList).SelectedValue
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub txtCustomer_TextChanged(sender As Object, e As EventArgs)
            Try
                hdnOrgSearch.Value = CType(sender, TextBox).Text
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub txtRMA_TextChanged(sender As Object, e As EventArgs)
            Try
                hdnRMASearch.Value = CType(sender, TextBox).Text
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub gvSalesReturns_Sorting(sender As Object, e As GridViewSortEventArgs) Handles gvSalesReturns.Sorting
            Try
                ' By default, set the sort direction to 1 = ascending.
                Dim sortDirection = "1"

                ' Retrieve the last column that was sorted.
                Dim sortExpression = TryCast(ViewState("SortExpression"), String)

                If sortExpression IsNot Nothing Then
                    ' Check if the same column is being sorted.
                    ' Otherwise, the default value can be returned.
                    If sortExpression = e.SortExpression Then
                        Dim lastDirection = TryCast(ViewState("SortDirection"), String)
                        If lastDirection IsNot Nothing _
                          AndAlso lastDirection = "1" Then
                            sortDirection = "2"
                        End If
                    End If
                End If

                ' Save new values in ViewState.
                ViewState("SortDirection") = sortDirection
                ViewState("SortExpression") = e.SortExpression

                hdnSortDirection.Value = sortDirection
                hdnSortColumn.Value = e.SortExpression

                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Protected Sub lnkClearFilter_Click(sender As Object, e As EventArgs)
            Try
                hdnRMASearch.Value = ""
                hdnOrgSearch.Value = ""
                hdnStatus.Value = ""
                hdnReason.Value = ""
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub

        Private Sub DisplayAlert(ByVal message As String)
            Try
                lblMessage.Text = message
                divAlert.Visible = True
                divAlert.Focus()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
    End Class
End Namespace
