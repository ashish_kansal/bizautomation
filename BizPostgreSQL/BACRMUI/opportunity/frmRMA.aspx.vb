﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Alerts
Partial Public Class frmRMA
    Inherits BACRMPage
    Dim decTotalAmount As Decimal
    Dim dtOppBiDocItems As New DataTable
    Dim decDiscAmount, decTotalDiscount, decTotalAmtDisc, decShipCost, decTaxAmount, decLateCharges, decdiscount As Decimal
    Dim lintJournalId As Integer
    Dim lngOppBizDocID, lngOppId As Long


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            btnClose.Attributes.Add("onclick", "return Close()")
            btnPrint.Attributes.Add("onclick", "return PrintIt()")
            lblBizDoc.Text = GetQueryStringVal( "Name")
            lngOppId = GetQueryStringVal( "OpID")
            If Not IsPostBack Then
                getAddressDetails()
                getRMADetails()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub getRMADetails()
        Try
            Dim dtReturnsDtl As DataTable
            Dim dtDetails As DataTable
            Dim dsReturns As DataSet
            Dim objOpp As New MOpportunity
            With objOpp
                .OpportunityId = lngOppId
                dsReturns = .GetReturnDetails()
                dtReturnsDtl = dsReturns.Tables(0)
                BindItems(dtReturnsDtl)
                dtDetails = dsReturns.Tables(1)
            End With
            If dtDetails.Rows.Count <> 0 Then
                hplBillto.Attributes.Add("onclick", "return openeditAddress('Bill','" & lngOppId & "')")
                hplShipTo.Attributes.Add("onclick", "return openeditAddress('Ship','" & lngOppId & "')")
                If Not IsDBNull(dtDetails.Rows(0).Item("vcBizDocImagePath")) Then
                    imgLogo.Visible = True
                    imgLogo.ImageUrl = "../../" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName") & "/documents/docs/" & dtDetails.Rows(0).Item("vcBizDocImagePath")
                Else : imgLogo.Visible = False
                End If
                lblComments.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("txtComments")), "", dtDetails.Rows(0).Item("txtComments"))
                lblOppName.Text = IIf(IsDBNull(dtDetails.Rows(0).Item("vcPOppName")), "", dtDetails.Rows(0).Item("vcPOppName"))
                hdnContactId.Value = dtDetails.Rows(0).Item("numContactId").ToString()
                hdnSendMailTo.Value = dtDetails.Rows(0).Item("vcEmail").ToString()
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub getAddressDetails()
        Try
            Dim dtOppBizAddDtl As DataTable
            Dim objOppBizDocs As New OppBizDocs
            objOppBizDocs.OppBizDocId = lngOppBizDocID
            objOppBizDocs.OppId = lngOppId
            objOppBizDocs.DomainID = Session("DomainID")
            dtOppBizAddDtl = objOppBizDocs.GetOppAddressDetail
            If dtOppBizAddDtl.Rows.Count <> 0 Then
                lblBillTo.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("BillAdd")), "", dtOppBizAddDtl.Rows(0).Item("BillAdd"))
                lblShipTo.Text = IIf(IsDBNull(dtOppBizAddDtl.Rows(0).Item("ShipAdd")), "", dtOppBizAddDtl.Rows(0).Item("ShipAdd"))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindItems(ByVal dt As DataTable)
        Try
            If dt.Rows.Count > 0 Then
                dgBizDocs.DataSource = dt
                dgBizDocs.DataBind()
                lblSubTotal.Text = ReturnMoney(dt.Compute("Sum(ReturnAmount)", String.Empty))
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnMoney(ByVal Money) As String
        Try
            Return String.Format("{0:#,###.##}", Math.Round(Money, 2))
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Private Sub btnSendEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendEmail.Click
        Try
            Session("Attachements") = Nothing 'reset to nothing to avoid duplicate attachments
            hdnBizDocHTML.Value = "<body><link rel='stylesheet' href='" + Request.Url.Scheme + Request.Url.SchemeDelimiter + Request.Url.Host + "/" + Request.Url.Segments(1) + "CSS/master.css' type='text/css' />" + hdnBizDocHTML.Value + "</body>"

            Dim strFileName As String
            Dim objHTMLToPDF As New HTMLToPDF
            strFileName = objHTMLToPDF.ConvertHTML2PDF(hdnBizDocHTML.Value, CCommon.ToLong(Session("DomainID")))


            objCommon.AddAttchmentToSession("RMA", CCommon.GetDocumentPath(Session("DomainID")) & strFileName, CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName)

            Dim str As String
            str = "<script language='javascript'>window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&PickAtch=1&isAttachmentRequired=1&Lsemail=" & hdnSendMailTo.Value & "&pqwRT=" & hdnContactId.Value & "','ComposeWindow','toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')</script>"
            Response.Write(str)
            'getRMADetails()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class