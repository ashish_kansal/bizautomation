Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Alerts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports System.Reflection
Imports BACRM.BusinessLogic.Item
Imports Infragistics.WebUI.UltraWebGrid
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Leads
Imports Telerik.Web.UI
Imports System.Text

Namespace BACRM.UserInterface.Opportunities
    Partial Public Class frmAddOpportunity
        Inherits BACRMPage

        Dim objCommon As CCommon
        Dim objItems As CItems
        Dim drowItem As DataRow
        Dim ds As DataSet
        Dim dsTemp As DataSet
        Dim dtOppAtributes As DataTable
        Dim dtOppOptAttributes As DataTable
        Dim strValues As String
        Dim lngOppId, OppBizDocID, JournalId, lngDivId As Long

        Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
            Try
                CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lblException.Text = ""
                order1._PageType = order1.PageType.AddOppertunity
                order1.OppType = 2
                CType(order1.FindControl("chkDropShip"), CheckBox).Visible = False

                If Not IsPostBack Then
                    PersistTable.Load(strParam:="frmNewPurchaseOpportunity", isMasterPage:=True)
                    If PersistTable.Count > 0 Then
                        CType(order1.FindControl("hdnSearchPageName"), HiddenField).Value = "frmNewPurchaseOpportunity"
                        CType(order1.FindControl("hdnSearchType"), HiddenField).Value = CCommon.ToString(PersistTable("frmNewPurchaseOpportunity"))
                    End If

                    lnkBillTo.Attributes.Add("onclick", "return OpenAddressWindow('BillTo')")
                    lnkShipTo.Attributes.Add("onclick", "return OpenAddressWindow('ShipTo')")
                    Me.FillExistingAddress(Session("UserDivisionID"))
                    radCmbCompany.Focus()
                    SetAssignedToEnabled(0, 0)
                    ' = "Opportunity"

                    objCommon = New CCommon
                    objCommon.UserCntID = Session("UserContactID")

                    objCommon.sb_FillComboFromDBwithSel(ddlSource, 9, Session("DomainID"))
                    BindMultiCurrency()
                    FillClass()

                    If CCommon.ToLong(Session("UserContactID")) > 0 AndAlso CCommon.ToLong(Session("UserID")) = 0 Then
                        objCommon.DivisionID = CCommon.ToLong(Session("UserDivisionID"))
                        objCommon.charModule = "D"
                        radCmbCompany.Enabled = False
                        CType(order1.FindControl("txtprice"), TextBox).ReadOnly = True
                        CType(order1.FindControl("txtItemDiscount"), TextBox).ReadOnly = True

                        objCommon.GetCompanySpecificValues1()
                        Dim strCompany As String
                        strCompany = objCommon.GetCompanyName
                        radCmbCompany.Text = strCompany
                        radCmbCompany.SelectedValue = objCommon.DivisionID
                        FillContact(ddlContact)
                        If Not ddlContact.Items.FindByValue(objCommon.ContactID) Is Nothing Then ddlContact.Items.FindByValue(objCommon.ContactID).Selected = True

                        ddlCompSelectedIndexChanged()
                    ElseIf GetQueryStringVal("uihTR") <> "" Or GetQueryStringVal("fghTY") <> "" Or GetQueryStringVal("rtyWR") <> "" Or GetQueryStringVal("tyrCV") <> "" Or GetQueryStringVal("pluYR") <> "" Then
                        If GetQueryStringVal("uihTR") <> "" Then
                            objCommon.ContactID = GetQueryStringVal("uihTR")
                            objCommon.charModule = "C"
                        ElseIf GetQueryStringVal("rtyWR") <> "" Then
                            objCommon.DivisionID = GetQueryStringVal("rtyWR")
                            objCommon.charModule = "D"
                        ElseIf GetQueryStringVal("tyrCV") <> "" Then
                            objCommon.ProID = GetQueryStringVal("tyrCV")
                            objCommon.charModule = "P"
                        ElseIf GetQueryStringVal("pluYR") <> "" Then
                            objCommon.OppID = GetQueryStringVal("pluYR")
                            objCommon.charModule = "O"
                        ElseIf GetQueryStringVal("fghTY") <> "" Then
                            objCommon.CaseID = GetQueryStringVal("fghTY")
                            objCommon.charModule = "S"
                        End If

                        objCommon.GetCompanySpecificValues1()
                        Dim strCompany As String
                        strCompany = objCommon.GetCompanyName
                        radCmbCompany.Text = strCompany
                        radCmbCompany.SelectedValue = objCommon.DivisionID
                        FillContact(ddlContact)
                        If Not ddlContact.Items.FindByValue(objCommon.ContactID) Is Nothing Then ddlContact.Items.FindByValue(objCommon.ContactID).Selected = True

                        ddlCompSelectedIndexChanged()
                    End If

                    ddlOppType_SelectedIndexChangedExtracted()
                End If

                Dim lngDiv As Long
                If radCmbCompany.SelectedValue = "" Then
                    lngDiv = 0
                Else : lngDiv = radCmbCompany.SelectedValue
                End If

                Dim SValbuilder As New StringBuilder
                Dim strValidation As String = ""

                SValbuilder.AppendLine("function validateFixedFields() {")
                SValbuilder.AppendLine("if(RangeValidator('txtMarkupShippingRate','txtMarkupShippingRate','" & 0.1 & "','" & 99.99 & "','" & "Enter markup shipping rate in between 0.1 to 99.99!" & "')==false) {return false;} ")
                SValbuilder.AppendLine("return true;}")

                strValidation = SValbuilder.ToString()
                ClientScript.RegisterClientScriptBlock(Me.GetType, "FixedValidation", strValidation, True)

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub

        Private Sub FillExistingAddress(ByVal sDivisionId As String)
            Try
                lblBillTo1.Text = ""
                lblShipTo1.Text = ""
                CType(order1.FindControl("hdnBillAddressID"), HiddenField).Value = 0
                CType(order1.FindControl("hdnShipAddressID"), HiddenField).Value = 0
                If Not IsNothing(sDivisionId) Then
                    Dim oCOpportunities As New COpportunities
                    oCOpportunities.DomainID = CType((Session("DomainID")), Long)
                    Dim dtAddress As DataTable = oCOpportunities.GetExistingAddress(CInt(Session("UserID")), CInt(sDivisionId))
                    lblBillTo1.Text = CCommon.ToString(dtAddress(0)("vcBillAddress"))
                    lblShipTo1.Text = CCommon.ToString(dtAddress(0)("vcShipAddress"))

                    hdnShipToWarehouseID.Value = CCommon.ToLong(dtAddress(0)("numWarehouseID"))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnTemp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTemp.Click
            Try
                If (CCommon.ToLong(CType(order1.FindControl("hdnBillAddressID"), HiddenField).Value) > 0) Then
                    BindAddressByAddressID(CCommon.ToLong(CType(order1.FindControl("hdnBillAddressID"), HiddenField).Value), "Bill")
                End If
                If (CCommon.ToLong(CType(order1.FindControl("hdnShipAddressID"), HiddenField).Value) > 0) Then
                    BindAddressByAddressID(CCommon.ToLong(CType(order1.FindControl("hdnShipAddressID"), HiddenField).Value), "Ship")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub
        Private Sub BindAddressByAddressID(ByVal lngAddressID As Long, ByVal AddType As String)
            Try
                Dim objContact As New CContacts
                objContact.DomainID = Session("DomainID")
                objContact.AddressID = lngAddressID
                objContact.byteMode = 1
                Dim dtTable As DataTable = objContact.GetAddressDetail

                If AddType = "Bill" Then
                    If dtTable.Rows.Count > 0 Then
                        lblBillTo1.Text = CCommon.ToString(dtTable.Rows(0)("vcFullAddress"))
                        lblBillTo2.Text = ""
                    Else
                        lblBillTo1.Text = ""
                        lblBillTo2.Text = ""
                    End If
                Else
                    If dtTable.Rows.Count > 0 Then
                        lblShipTo1.Text = CCommon.ToString(dtTable.Rows(0)("vcFullAddress"))
                        lblShipTo2.Text = ""
                        hdnShipToWarehouseID.Value = CCommon.ToLong(dtTable.Rows(0)("numWarehouseID"))
                    Else
                        lblShipTo1.Text = ""
                        lblShipTo2.Text = ""
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub BindMultiCurrency()
            Try
                If Session("MultiCurrency") = True Then
                    pnlCurrency.Visible = True
                    Dim objCurrency As New CurrencyRates
                    objCurrency.DomainID = Session("DomainID")
                    objCurrency.GetAll = 0
                    ddlCurrency.DataSource = objCurrency.GetCurrencyWithRates()
                    ddlCurrency.DataTextField = "vcCurrencyDesc"
                    ddlCurrency.DataValueField = "numCurrencyID"
                    ddlCurrency.DataBind()
                    ddlCurrency.Items.Insert(0, "--Select One--")
                    ddlCurrency.Items.FindByText("--Select One--").Value = "0"
                    If Not ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")) Is Nothing Then
                        ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")).Selected = True
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub ddlCompSelectedIndexChanged()
            Try
                If radCmbCompany.SelectedValue <> "" Then
                    ddlContact.Enabled = True
                    FillContact(ddlContact)
                    tdBanlce.Visible = True
                    objItems = New CItems
                    objItems.DivisionID = radCmbCompany.SelectedValue
                    Dim dtTable As DataTable
                    dtTable = objItems.GetAmountDue

                    Dim strBaseCurrency As String = ""
                    If (Session("MultiCurrency") = True) Then
                        strBaseCurrency = Session("Currency") + " "
                    End If

                    lblCreditLimit.Text = strBaseCurrency + String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("CreditLimit"))
                    lblRemaningCredit.Text = strBaseCurrency + String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("RemainingCredit"))

                    lblBalanceDue.Text = strBaseCurrency + String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("AmountDuePO"))
                    lblTotalAmtPastDue.Text = strBaseCurrency + String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("AmountPastDuePO"))
                    lblCreditBalance.Text = strBaseCurrency + String.Format("{0:#,##0.00}", dtTable.Rows(0).Item("PCreditMemo"))

                    objCommon = New CCommon

                    objCommon.DivisionID = radCmbCompany.SelectedValue
                    objCommon.charModule = "D"
                    objCommon.GetCompanySpecificValues1()

                    SetAssignedToEnabled(objCommon.RecordOwner, objCommon.TerittoryID)

                    If objCommon.CurrencyID > 0 Then
                        If Not ddlCurrency.Items.FindByValue(objCommon.CurrencyID) Is Nothing Then
                            ddlCurrency.ClearSelection()
                            ddlCurrency.Items.FindByValue(objCommon.CurrencyID).Selected = True
                        End If
                    Else
                        If Not ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")) Is Nothing Then
                            ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")).Selected = True
                        End If
                    End If

                    If CCommon.ToInteger(Session("DefaultClassType")) = 2 AndAlso Not ddlClass.Items.FindByValue(objCommon.AccountClassID) Is Nothing Then
                        ddlClass.Items.FindByValue(objCommon.AccountClassID).Selected = True
                    End If

                    FillAssignToDropdown(ddlAssignTo)

                    Dim lngOrgAssignedTo As Long
                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.Mode = 14
                    objCommon.Str = radCmbCompany.SelectedValue
                    lngOrgAssignedTo = objCommon.GetSingleFieldValue()

                    If ddlAssignTo.Items.FindByValue(lngOrgAssignedTo) IsNot Nothing Then
                        ddlAssignTo.ClearSelection()
                        ddlAssignTo.Items.FindByValue(lngOrgAssignedTo).Selected = True
                    End If

                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub SetAssignedToEnabled(ByVal numRecordOwner As Long, ByVal numTerritoryID As Long)
            Try
                Dim m_aryRightsForAssignedTo() As Integer = GetUserRightsForPage_Other(10, 128)
                If m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 0 Then
                    ddlAssignTo.Enabled = False
                ElseIf m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 1 AndAlso CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                    Try
                        If numRecordOwner <> Session("UserContactID") Then
                            ddlAssignTo.Enabled = False
                        End If
                    Catch ex As Exception
                    End Try
                ElseIf m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 2 AndAlso CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                    Dim i As Integer
                    Dim dtTerritory As DataTable
                    dtTerritory = Session("UserTerritory")
                    If numTerritoryID = 0 Then
                        ddlAssignTo.Enabled = False
                    Else
                        Dim chkDelete As Boolean = False
                        For i = 0 To dtTerritory.Rows.Count - 1
                            If numTerritoryID = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                chkDelete = True
                            End If
                        Next
                        If chkDelete = False Then
                            ddlAssignTo.Enabled = False
                        End If
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub FillAssignToDropdown(ByRef ddlCombo As DropDownList)
            Try
                If Session("PopulateUserCriteria") = 1 Then
                    objCommon.sb_FillConEmpFromTerritories(ddlCombo, Session("DomainID"), 0, 0, objCommon.TerittoryID)
                ElseIf Session("PopulateUserCriteria") = 2 Then
                    objCommon.sb_FillConEmpFromDBUTeam(ddlCombo, Session("DomainID"), Session("UserContactID"))
                Else : objCommon.sb_FillConEmpFromDBSel(ddlCombo, Session("DomainID"), 0, 0)
                End If

                Dim objUser As New UserAccess
                objUser.DomainID = Session("DomainID")
                objUser.byteMode = 1

                Dim dt As DataTable = objUser.GetCommissionsContacts

                Dim item As ListItem
                Dim dr As DataRow
                For Each dr In dt.Rows
                    item = New ListItem()
                    item.Text = CCommon.ToString(dr("vcUserName"))
                    item.Value = CCommon.ToString(dr("numContactID"))
                    ddlCombo.Items.Add(item)
                Next

            Catch ex As Exception
                Throw ex
            End Try

        End Sub

        Public Function FillContact(ByVal ddlCombo As DropDownList)
            Try
                Dim fillCombo As New COpportunities
                With fillCombo
                    .DivisionID = radCmbCompany.SelectedValue
                    ddlCombo.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                    ddlCombo.DataTextField = "Name"
                    ddlCombo.DataValueField = "numcontactId"
                    ddlCombo.DataBind()
                End With
                ddlCombo.Items.Insert(0, New ListItem("---Select One---", "0"))
                If ddlCombo.Items.Count >= 2 Then
                    ddlCombo.Items(1).Selected = True
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub ddlOppType_SelectedIndexChangedExtracted()
            Try
                tdBanlce.Visible = False
                CType(order1.FindControl("hplPrice"), HyperLink).Text = "Unit Price"
                order1.SOItems = Nothing

                'Set javascript Variable for radCmbItem
                CCommon.UpdateItemRadComboValues(2, radCmbCompany.SelectedValue)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub radCmbCompany_SelectedIndexChanged(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxSelectedIndexChangedEventArgs) Handles radCmbCompany.SelectedIndexChanged
            Try
                ddlCompSelectedIndexChanged()
                If CCommon.ToLong(radCmbCompany.SelectedValue) > 0 Then
                    Dim objOpp As New OppBizDocs
                    If objOpp.ValidateARAP(radCmbCompany.SelectedValue, 0, Session("DomainID")) = False Then
                        ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "alert('Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting->Accounts for RelationShip"" To Save' );", True)
                        radCmbCompany.SelectedValue = ""
                        radCmbCompany.Text = ""
                        Exit Sub
                    End If

                    Dim lngOrgAssignedTo As Long
                    objCommon = New CCommon
                    objCommon.DomainID = Session("DomainID")
                    objCommon.Mode = 14
                    objCommon.Str = radCmbCompany.SelectedValue
                    lngOrgAssignedTo = objCommon.GetSingleFieldValue()

                    If ddlAssignTo.Items.FindByValue(lngOrgAssignedTo) IsNot Nothing Then
                        ddlAssignTo.ClearSelection()
                        ddlAssignTo.Items.FindByValue(lngOrgAssignedTo).Selected = True
                    Else
                        ddlAssignTo.SelectedValue = "0"
                    End If

                    LoadVendorWareHouse()
                End If

                'Set javascript Variable for radCmbItem
                CCommon.UpdateItemRadComboValues(2, radCmbCompany.SelectedValue)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub

        Private Sub btnLOadCo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLOadCo.Click
            Try
                If objCommon Is Nothing Then objCommon = New CCommon
                Dim strCompany As String
                objCommon.DivisionID = CType(order1.FindControl("txtDivID"), TextBox).Text
                strCompany = objCommon.GetCompanyName
                radCmbCompany.Text = strCompany
                radCmbCompany.SelectedValue = CType(order1.FindControl("txtDivID"), TextBox).Text
                FillContact(ddlContact)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub

        Sub LoadVendorWareHouse()
            Try
                Dim dtVendorsWareHouse As DataTable
                objItems.DomainID = Session("DomainID")
                objItems.VendorID = radCmbCompany.SelectedValue
                objItems.byteMode = 2
                dtVendorsWareHouse = objItems.GetVendorsWareHouse

                If dtVendorsWareHouse.Rows.Count > 0 Then
                    ddlVendorAddresses.DataSource = dtVendorsWareHouse
                    ddlVendorAddresses.DataTextField = "vcFullAddress"
                    ddlVendorAddresses.DataValueField = "numAddressID"
                    ddlVendorAddresses.DataBind()

                    BindVendorShipmentMethod()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub BindVendorShipmentMethod()
            Try
                ddlShippingMethod.Items.Clear()

                Dim objItems As New CItems
                objItems.DomainID = Session("DomainID")
                objItems.ShipAddressId = CCommon.ToLong(ddlVendorAddresses.SelectedValue)
                objItems.VendorID = CCommon.ToLong(radCmbCompany.SelectedValue)
                objItems.byteMode = 2
                Dim dtVendorShipmentMethod As DataTable = objItems.GetVendorShipmentMethod

                If Not dtVendorShipmentMethod Is Nothing AndAlso dtVendorShipmentMethod.Rows.Count > 0 Then
                    ddlShippingMethod.DataSource = dtVendorShipmentMethod
                    ddlShippingMethod.DataTextField = "ShipmentMethod"
                    ddlShippingMethod.DataValueField = "vcListValue"
                    ddlShippingMethod.DataBind()
                End If

                If ddlShippingMethod.Items.Count > 0 Then
                    lblLeadTimeDays.Text = CCommon.ToInteger(ddlShippingMethod.SelectedValue.Split("~")(1)) & " Days"
                    lblExpectedDelivery.Text = FormattedDateFromDate(DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow).Date.AddDays(ddlShippingMethod.SelectedValue.Split("~")(1)), Session("DateFormat"))
                Else
                    lblLeadTimeDays.Text = "-"
                    lblExpectedDelivery.Text = "-"
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Protected Sub ddlShippingMethod_SelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                If Not String.IsNullOrEmpty(ddlShippingMethod.SelectedValue) Then
                    lblLeadTimeDays.Text = CCommon.ToInteger(ddlShippingMethod.SelectedValue.Split("~")(1)) & " Days"
                    lblExpectedDelivery.Text = FormattedDateFromDate(DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow).Date.AddDays(ddlShippingMethod.SelectedValue.Split("~")(1)), Session("DateFormat"))
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub

        Protected Sub ddlVendorAddresses_SelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                ddlShippingMethod.Items.Clear()

                If ddlVendorAddresses.SelectedValue = "0" Then
                    lblLeadTimeDays.Text = "-"
                    lblExpectedDelivery.Text = "-"
                Else
                    BindVendorShipmentMethod()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub

        Private Sub lnkClick_Click(sender As Object, e As System.EventArgs) Handles lnkClick.Click
            Try
                tblMultipleOrderMessage.Visible = False
                order1.SOItems = Nothing
                Response.Redirect(Request.Url.ToString())
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub

        Private Sub LoadCompanyDetail()
            Try
                Dim objLeads As New LeadsIP
                objLeads.DivisionID = CCommon.ToLong(radCmbCompany.SelectedValue)
                objLeads.DomainID = Session("DomainID")
                objLeads.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objLeads.GetCompanyDetails()

                lblFollowup.Text = CCommon.ToString(objLeads.FollowUpStatusName)
                lblEmployees.Text = CCommon.ToString(objLeads.FollowUpStatusName)
                lblRating.Text = CCommon.ToString(objLeads.CompanyRatingName)
                lblDifferentiation.Text = CCommon.ToString(objLeads.CompanyDiffName)
                chkCreditHold.Checked = CCommon.ToString(objLeads.OnCreditHold)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub DisplayError(ByVal ex As String)
            lblException.Text = ex
            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ScrollTo", "ScrollToError();", True)
        End Sub

        Private Sub FillClass()
            Try
                If CCommon.ToLong(Session("DefaultClassType")) > 0 Then
                    Dim dtClass As DataTable
                    Dim objAdmin As New CAdmin
                    objAdmin.DomainID = Session("DomainID")
                    objAdmin.Mode = 1
                    dtClass = objAdmin.GetClass()

                    If dtClass.Rows.Count > 0 Then
                        ddlClass.DataTextField = "ClassName"
                        ddlClass.DataValueField = "numChildClassID"
                        ddlClass.DataSource = dtClass
                        ddlClass.DataBind()

                        Dim intDefaultClassType As Int16 = CCommon.ToInteger(Session("DefaultClassType"))
                        If intDefaultClassType = 1 AndAlso ddlClass.Items.FindByValue(Session("DefaultClass")) IsNot Nothing Then 'intDefaultClassType = 1 => User Level Account Class
                            ddlClass.ClearSelection()
                            ddlClass.Items.FindByValue(Session("DefaultClass")).Selected = True
                        End If
                    End If

                    divClass.Visible = True
                End If

                ddlClass.Items.Insert(0, New ListItem("--Select One --", "0"))
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub btnSelectedVendor_Click(sender As Object, e As EventArgs) Handles btnSelectedVendor.Click
            Try
                ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "HidePOVendor", "$('#imgPODistance').hide(); $('#imgPODistance').removeAttr('onclick');", True)

                objCommon = New CCommon
                objCommon.DivisionID = CCommon.ToLong(hdnVendorID.Value)
                objCommon.charModule = "D"
                objCommon.GetCompanySpecificValues1()

                Dim strCompany, strContactID As String
                strContactID = objCommon.ContactID
                strCompany = objCommon.GetCompanyName
                radCmbCompany.Text = strCompany
                radCmbCompany.SelectedValue = objCommon.DivisionID
                CType(order1.FindControl("txtDivID"), TextBox).Text = radCmbCompany.SelectedValue
                CType(order1.FindControl("txtPrice"), TextBox).Text = CCommon.ToDouble(hdnVendorCost.Value)

                ddlCompSelectedIndexChanged()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub

        Protected Sub chkDropshipPO_CheckedChanged(sender As Object, e As EventArgs)
            Try
                If chkDropshipPO.Checked Then
                    ScriptManager.RegisterClientScriptBlock(Me.Page, Me.Page.GetType(), "OpenAdressWindow", "OpenAdd();", True)
                    CType(order1.FindControl("chkDropShip"), CheckBox).Checked = True
                Else
                    Me.FillExistingAddress(Session("UserDivisionID"))
                    CType(order1.FindControl("chkDropShip"), CheckBox).Checked = False
                End If

                order1.SOItems = Nothing
                CType(order1.FindControl("dgItems"), DataGrid).DataSource = Nothing
                CType(order1.FindControl("dgItems"), DataGrid).DataBind()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.ToString())
            End Try
        End Sub
    End Class
End Namespace