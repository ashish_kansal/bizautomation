﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmMassPurchaseFulfillmentConfig.aspx.vb" Inherits="BACRM.UserInterface.Opportunities.frmMassPurchaseFulfillmentConfig" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            $("[id$=btnAddLeft]").click(function () {
                if ($("[id$=lbAvailableFieldLeft]").val() != null) {
                    var selectedItems = [];

                    if ($("[id$=ddlWhen]").val() == "1" && $("[id$=hdnReadytoReceive]").val() != "") {
                        selectedItems = $("[id$=hdnReadytoReceive]").val().split(",");
                    } else if ($("[id$=ddlWhen]").val() == "2" && $("[id$=hdnReadytoPutAway]").val() != "") {
                        selectedItems = $("[id$=hdnReadytoPutAway]").val().split(",");
                    } else if ($("[id$=ddlWhen]").val() == "3" && $("[id$=hdnReadytoBill]").val() != "") {
                        selectedItems = $("[id$=hdnReadytoBill]").val().split(",");
                    } else if ($("[id$=ddlWhen]").val() == "4" && $("[id$=hdnPendingClose]").val() != "") {
                        selectedItems = $("[id$=hdnPendingClose]").val().split(",");
                    }

                    if (jQuery.inArray($("[id$=lbAvailableFieldLeft]").val(), selectedItems) === -1) {
                        var $options = $("[id$=lbAvailableFieldLeft] > option:selected").clone();
                        $('[id$=lbSelectedFieldLeft]').append($options);

                        var selected = $("[id$=lbAvailableFieldLeft]").find(":selected");
                        var next = selected.next();
                        if (next.length > 0)
                            next.prop("selected", true)

                        var ddlvalues = $('[id$=lbSelectedFieldLeft] option').map(function () {
                            return $(this).val();
                        });

                        if ($("[id$=ddlWhen]").val() == "1") {
                            $("[id$=hdnReadytoReceive]").val(ddlvalues.get());
                        } else if ($("[id$=ddlWhen]").val() == "2") {
                            $("[id$=hdnReadytoPutAway]").val(ddlvalues.get());
                        } else if ($("[id$=ddlWhen]").val() == "3") {
                            $("[id$=hdnReadytoBill]").val(ddlvalues.get());
                        } else if ($("[id$=ddlWhen]").val() == "4") {
                            $("[id$=hdnPendingClose]").val(ddlvalues.get());
                        }
                    } else {
                        alert("Feild is alreay added");
                    }

                } else {
                    alert("Select atleast one field");
                }
            });

            $("[id$=btnRemoveLeft]").click(function () {
                if ($("[id$=lbSelectedFieldLeft]").val() != null) {
                    var selected = $("[id$=lbSelectedFieldLeft]").find(":selected");
                    var next = selected.next();
                    $("[id$=lbSelectedFieldLeft] > option:selected").remove();

                    if (next.length > 0)
                        next.prop("selected", true);

                    var ddlvalues = $('[id$=lbSelectedFieldLeft] option').map(function () {
                        return $(this).val();
                    });

                    if ($("[id$=ddlWhen]").val() == "1") {
                        $("[id$=hdnReadytoReceive]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "2") {
                        $("[id$=hdnReadytoPutAway]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "3") {
                        $("[id$=hdnReadytoBill]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "4") {
                        $("[id$=hdnPendingClose]").val(ddlvalues.get());
                    }
                } else {
                    alert("Select atleast one field");
                }
            });

            $("[id$=btnUpLeft]").click(function () {
                if ($("[id$=lbSelectedFieldLeft]").val() != null) {
                    var selected = $("[id$=lbSelectedFieldLeft]").find(":selected");
                    var before = selected.prev();
                    if (before.length > 0)
                        selected.detach().insertBefore(before);

                    var ddlvalues = $('[id$=lbSelectedFieldLeft] option').map(function () {
                        return $(this).val();
                    });

                    if ($("[id$=ddlWhen]").val() == "1") {
                        $("[id$=hdnReadytoReceive]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "2") {
                        $("[id$=hdnReadytoPutAway]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "3") {
                        $("[id$=hdnReadytoBill]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "4") {
                        $("[id$=hdnPendingClose]").val(ddlvalues.get());
                    }
                } else {
                    alert("Select atleast one field");
                }
            });

            $("[id$=btnDownLeft]").click(function () {
                if ($("[id$=lbSelectedFieldLeft]").val() != null) {
                    var selected = $("[id$=lbSelectedFieldLeft]").find(":selected");
                    var next = selected.next();
                    if (next.length > 0)
                        selected.detach().insertAfter(next);

                    var ddlvalues = $('[id$=lbSelectedFieldLeft] option').map(function () {
                        return $(this).val();
                    });

                    if ($("[id$=ddlWhen]").val() == "1") {
                        $("[id$=hdnReadytoReceive]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "2") {
                        $("[id$=hdnReadytoPutAway]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "3") {
                        $("[id$=hdnReadytoBill]").val(ddlvalues.get());
                    } else if ($("[id$=ddlWhen]").val() == "4") {
                        $("[id$=hdnPendingClose]").val(ddlvalues.get());
                    }
                } else {
                    alert("Select atleast one field");
                }
            });

            $("[id$=btnAddRight]").click(function () {
                if ($("[id$=lbAvailableFieldRight]").val() != null) {
                    var selectedItems = [];

                    if ($("[id$=hdnReadytoPickRight]").val() != "") {
                        selectedItems = $("[id$=hdnReadytoPickRight]").val().split(",");
                    }

                    if (jQuery.inArray($("[id$=lbAvailableFieldRight]").val(), selectedItems) === -1) {
                        var $options = $("[id$=lbAvailableFieldRight] > option:selected").clone();
                        $('[id$=lbSelectedFieldRight]').append($options);

                        var selected = $("[id$=lbAvailableFieldRight]").find(":selected");
                        var next = selected.next();
                        if (next.length > 0)
                            next.prop("selected", true)

                        var ddlvalues = $('[id$=lbSelectedFieldRight] option').map(function () {
                            return $(this).val();
                        });

                        $("[id$=hdnReadytoPickRight]").val(ddlvalues.get());
                    } else {
                        alert("Feild is alreay added");
                    }

                } else {
                    alert("Select atleast one field");
                }
            });

            $("[id$=btnRemoveRight]").click(function () {
                if ($("[id$=lbSelectedFieldRight]").val() != null) {
                    var selected = $("[id$=lbSelectedFieldRight]").find(":selected");
                    var next = selected.next();
                    $("[id$=lbSelectedFieldRight] > option:selected").remove();

                    if (next.length > 0)
                        next.prop("selected", true);

                    var ddlvalues = $('[id$=lbSelectedFieldRight] option').map(function () {
                        return $(this).val();
                    });

                    $("[id$=hdnReadytoPickRight]").val(ddlvalues.get());
                } else {
                    alert("Select atleast one field");
                }
            });

            $("[id$=btnUpRight]").click(function () {
                if ($("[id$=lbSelectedFieldRight]").val() != null) {
                    var selected = $("[id$=lbSelectedFieldRight]").find(":selected");
                    var before = selected.prev();
                    if (before.length > 0)
                        selected.detach().insertBefore(before);

                    var ddlvalues = $('[id$=lbSelectedFieldRight] option').map(function () {
                        return $(this).val();
                    });

                    $("[id$=hdnReadytoPickRight]").val(ddlvalues.get());
                } else {
                    alert("Select atleast one field");
                }
            });

            $("[id$=btnDownRight]").click(function () {
                if ($("[id$=lbSelectedFieldRight]").val() != null) {
                    var selected = $("[id$=lbSelectedFieldRight]").find(":selected");
                    var next = selected.next();
                    if (next.length > 0)
                        selected.detach().insertAfter(next);

                    var ddlvalues = $('[id$=lbSelectedFieldRight] option').map(function () {
                        return $(this).val();
                    });

                    $("[id$=hdnReadytoPickRight]").val(ddlvalues.get());
                } else {
                    alert("Select atleast one field");
                }
            });
        });

        function RefreshParentAndClose() {
            if (window.opener != null) {
                window.opener.LoadRecords();
            }

            window.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="pull-right">
        <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save & Close" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary" Text="Cancel" OnClientClick="Close();" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Fulfillment Configuration
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <div class="container-fluid">
        <div class="row padbottom10" id="divError" runat="server" style="display: none">
            <div class="col-sm-12">
                <div class="alert alert-danger">
                    <h4><i class="icon fa fa-ban"></i>Error</h4>
                    <p>
                        <asp:Label ID="lblError" runat="server"></asp:Label>
                    </p>
                </div>
            </div>
        </div>
        <div class="row padbottom10">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label>When </label>
                        <asp:DropDownList ID="ddlWhen" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlWhen_SelectedIndexChanged">
                            <asp:ListItem Text="Ready to Receive" Value="1" />
                            <asp:ListItem Text="Ready to Put-Away" Value="2" />
                            <asp:ListItem Text="Ready to Bill" Value="3" />
                            <asp:ListItem Text="Pending Close" Value="4" />
                        </asp:DropDownList>
                    </div>
                    <ul class="list-unstyled form-group">
                        <li>
                            <div class="form-group" id="divShowOnlyFullyReceived" runat="server" visible="false">
                                <label>Show only fully received order (all items, all quantities)</label>
                                <div class="checkbox">
                                    <asp:CheckBox ID="chkShowOnlyFullyReceived" runat="server" />
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="form-group" id="divReceiveBillOnClose" runat="server" visible="false">
                                <label>When closing orders, automatically receive and bill all quantities</label>
                                <div class="checkbox">
                                    <asp:CheckBox ID="chkReceiveBillOnClose" runat="server" />
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="form-inline" id="divBill" runat="server" visible="false">
                                <label>“Qty to Bill” value should be:</label>
                                <div class="checkbox">
                                    <asp:DropDownList ID="ddlBillType" runat="server" CssClass="form-control">
                                        <asp:ListItem Text="Qty Remaining" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="Qty Received minus Qty Billed" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="Qty Put Away minus Qty Billed" Value="3"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="row padbottom10">
                    <div class="col-md-12">
                        <div class="pull-left" id="divGroupBy" runat="server">
                            <asp:CheckBox ID="chkGroupByOrder" runat="server" />
                            Group by Order
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <label>Available Fields</label>
                        <br />
                        <asp:ListBox ID="lbAvailableFieldLeft" runat="server" CssClass="form-control" Height="450"></asp:ListBox>
                    </div>
                    <div class="col-sm-12 col-md-2" style="height: 450px; margin-top: 25px; vertical-align: middle; text-align: center; padding-left: 0px; padding-right: 0px">
                        <div style="padding-top: 105px">
                            <button id="btnAddLeft" type="button" class="btn btn-primary">Add&nbsp;<i class='fa fa-chevron-right'></i></button>
                            <br />
                            <br />
                            <button id="btnRemoveLeft" type="button" class="btn btn-primary"><i class='fa fa-chevron-left'></i>&nbsp;Remove</button>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <label>Selected Fields/Choose Order</label>
                        <br />
                        <asp:ListBox ID="lbSelectedFieldLeft" runat="server" CssClass="form-control" Height="450"></asp:ListBox>
                    </div>
                    <div class="col-sm-12 col-md-2" style="height: 450px; margin-top: 25px; vertical-align: middle; text-align: center; padding-left: 0px; padding-right: 0px">
                        <div style="padding-top: 105px">
                            <button id="btnUpLeft" type="button" class="btn btn-primary"><i class='fa fa-chevron-up'></i></button>
                            <br />
                            <br />
                            <button id="btnDownLeft" type="button" class="btn btn-primary"><i class='fa fa-chevron-down'></i></button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-6" id="divRight" runat="server">
                <div class="row padbottom10">
                    <div class="col-xs-12">
                        <div class="pull-left">
                            <div class="form-inline">
                                <label>Scan Value:</label>
                                <asp:RadioButton ID="rbSKU" runat="server" GroupName="ScanValue" Checked="true" />
                                SKU
                                <asp:RadioButton ID="rbUPC" runat="server" GroupName="ScanValue" />
                                UPC
                                <asp:RadioButton ID="rbItemName" runat="server" GroupName="ScanValue" />
                                Item Name
                            </div>
                        </div>
                        <div class="pull-right">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <label>Available Fields</label>
                        <br />
                        <asp:ListBox ID="lbAvailableFieldRight" runat="server" CssClass="form-control" Height="450" Width="190"></asp:ListBox>
                    </div>
                    <div class="col-sm-12 col-md-2" style="height: 450px; margin-top: 25px; vertical-align: middle; text-align: center; padding-left: 0px; padding-right: 0px">
                        <div style="padding-top: 105px">
                            <button id="btnAddRight" type="button" class="btn btn-primary">Add&nbsp;<i class='fa fa-chevron-right'></i></button>
                            <br />
                            <br />
                            <button id="btnRemoveRight" type="button" class="btn btn-primary"><i class='fa fa-chevron-left'></i>&nbsp;Remove</button>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <label>Selected Fields/Choose Order</label>
                        <br />
                        <asp:ListBox ID="lbSelectedFieldRight" runat="server" CssClass="form-control" Height="450" Width="190"></asp:ListBox>
                    </div>
                    <div class="col-sm-12 col-md-2" style="height: 450px; margin-top: 25px; vertical-align: middle; text-align: center; padding-left: 0px; padding-right: 0px">
                        <div style="padding-top: 105px">
                            <button id="btnUpRight" type="button" class="btn btn-primary"><i class='fa fa-chevron-up'></i></button>
                            <br />
                            <br />
                            <button id="btnDownRight" type="button" class="btn btn-primary"><i class='fa fa-chevron-down'></i></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hdnReadytoReceive" runat="server" Value="-1" />
    <asp:HiddenField ID="hdnReadytoPutAway" runat="server" Value="-1" />
    <asp:HiddenField ID="hdnReadytoBill" runat="server" Value="-1" />
    <asp:HiddenField ID="hdnPendingClose" runat="server" Value="-1" />
    <asp:HiddenField ID="hdnReadytoPickRight" runat="server" Value="-1" />

    <asp:HiddenField ID="hdnGroupByOrderForReceive" runat="server" />
    <asp:HiddenField ID="hdnGroupByOrderForPutAway" runat="server" />
    <asp:HiddenField ID="hdnGroupByOrderForBill" runat="server" />
    <asp:HiddenField ID="hdnGroupByOrderForClose" runat="server" />
</asp:Content>
