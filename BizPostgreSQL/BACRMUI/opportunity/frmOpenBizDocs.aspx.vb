﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Alerts
Imports Telerik.Web.UI
Imports System.Text
Imports BACRM.BusinessLogic.Workflow

Namespace BACRM.UserInterface.Opportunities
    Partial Public Class frmOpenBizDocs
        Inherits BACRMPage


        Dim lngBizDocID As Long = 0

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                GetUserRightsForPage(10, 18)
                If Not Page.IsPostBack Then

                    If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then
                        Response.Redirect("../admin/authentication.aspx?mesg=AC")
                    Else
                        'If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then btnPay.Visible = False
                    End If

                    BindBizDocStatus()

                    If CCommon.ToLong(GetQueryStringVal("BizDocID")) >= 0 Then
                        lngBizDocID = CCommon.ToLong(GetQueryStringVal("BizDocID"))
                    End If
                    lblOpenBizDocs.Text = CCommon.ToString(GetQueryStringVal("BizDocName"))

                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                        txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                        txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                        txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))
                    Else
                        txtSortColumn.Text = "dtCreatedDate" 'CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                        txtSortChar.Text = "0" 'CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                        txtSortOrder.Text = "desc" 'CCommon.ToString(PersistTable(PersistKey.SortOrder))
                    End If

                    ViewState("BizDocID") = lngBizDocID
                    BindDataGrid()
                End If
                ClientScript.RegisterClientScriptBlock(Me.GetType, "SetTab", "if (parent.parent.frames.length > 0) { parent.parent.SelectTabByValue('1');}else{ parent.SelectTabByValue('1'); } ", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        'Function GenerateFulfillmentRulesScript(ByVal dtRules As DataTable) As String
        '    Try
        '        Dim sb As New System.Text.StringBuilder
        '        Dim iRowIndex As Integer = 0

        '        sb.Append(vbCrLf & "var arrFulfillmentRulesFieldConfig = new Array();" & vbCrLf)     'Create a array to hold the field information in javascript

        '        For Each dr As DataRow In dtRules.Rows
        '            sb.Append("arrFulfillmentRulesFieldConfig[" & iRowIndex & "] = new FulfillmentRules('" & dr("numRuleID").ToString & "','" & dr("numBizDocTypeID").ToString & "','" & dr("numBizDocStatus").ToString() & "');" & vbCrLf)
        '            iRowIndex += 1
        '        Next

        '        Return sb.ToString
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function

        Sub BindDataGrid()
            Try
                Dim dtBizDocs As DataTable
                Dim objOpenBizDocs As New OpenBizDocs

                With objOpenBizDocs
                    .DomainID = Session("DomainID")
                    .DivisionID = 0 'IIf(radCmbCompany.SelectedValue = "", 0, radCmbCompany.SelectedValue)
                    .BizDocID = If(lngBizDocID = 0, ViewState("BizDocID"), lngBizDocID)
                    .BizDocStatus = Nothing
                    'If (ddlBizDocStatus.SelectedValue = 0) Then
                    '    .BizDocStatus = Nothing
                    'Else
                    '    .BizDocStatus = ddlBizDocStatus.SelectedValue
                    'End If
                    .OppType = 1
                    .SortCol = txtSortColumn.Text 'ViewState("SortCol")
                    .SortDirection = txtSortOrder.Text 'ViewState("SortDirection")

                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()
                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0
                    .SalesUserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                    .UserCntID = Session("UserContactID")
                    .FilterType = 0 'ddFilter.SelectedValue

                    Dim strBizDocStatusValue As String = ""
                    For Each item As RadComboBoxItem In radBizDocStatus.CheckedItems
                        'If CCommon.ToLong(item.Value) > 0 Then
                        strBizDocStatusValue = strBizDocStatusValue & item.Value & ","
                        'End If
                    Next
                    .strBizDocStatus = strBizDocStatusValue.TrimEnd(",")

                    Dim dtStartDate As DateTime
                    Dim dtEndDate As DateTime
                    Select Case ddlDateFilter.SelectedValue
                        Case 1 'All Dates
                            dtStartDate = SqlTypes.SqlDateTime.MinValue
                            dtEndDate = SqlTypes.SqlDateTime.MaxValue

                        Case 2 'Today
                            dtStartDate = DateTime.Now.Date & " 00:00:00"
                            dtEndDate = DateTime.Now.Date & " 23:59:59"

                        Case 3 'This Week
                            dtStartDate = DateAdd("d", 0 - DateTime.Now.DayOfWeek, DateTime.Now).Date & " 00:00:00"
                            dtEndDate = DateAdd("d", 6, dtStartDate).Date & " 23:59:59"

                        Case 4 'Last Week
                            dtStartDate = DateAdd("d", 0 - DateTime.Now.DayOfWeek, DateTime.Now.AddDays(-7)).Date & " 00:00:00"
                            dtEndDate = DateAdd("d", 6, dtStartDate).Date & " 23:59:59"

                        Case 5 'This Month
                            dtStartDate = DateAdd("d", 0 - DateTime.Now.Day + 1, DateTime.Now).Date & " 00:00:00"
                            dtEndDate = DateAdd("d", DateTime.DaysInMonth(dtStartDate.Year, dtStartDate.Month) - 1, dtStartDate).Date & " 23:59:59"

                        Case 6 'Last Month
                            dtStartDate = DateAdd("d", 0 - DateTime.Now.Day + 1, DateTime.Now.AddMonths(-1)).Date & " 00:00:00"
                            dtEndDate = DateAdd("d", DateTime.DaysInMonth(dtStartDate.Year, dtStartDate.Month) - 1, dtStartDate).Date & " 23:59:59"

                    End Select

                    objOpenBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    objOpenBizDocs.FromDate = dtStartDate
                    objOpenBizDocs.ToDate = dtEndDate

                End With

                dtBizDocs = objOpenBizDocs.ListBizDocs.Tables(0)
                gvBizDocs.DataSource = dtBizDocs
                gvBizDocs.DataBind()

                PersistTable.Clear()
                PersistTable.Add(PersistKey.CurrentPage, IIf(dtBizDocs.Rows.Count > 0, txtCurrrentPage.Text, "1"))
                PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())
                PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
                PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text.Trim())
                PersistTable.Save()

                bizPager.PageSize = Session("PagingRows")
                bizPager.RecordCount = objOpenBizDocs.TotalRecords
                bizPager.CurrentPageIndex = txtCurrrentPage.Text

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub gvBizDocs_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvBizDocs.RowCommand
            Try
                Dim objOppBizDocs As New OppBizDocs
                Dim lintAuthorizativeBizDocsId As Integer
                Dim lobjJournalEntry As New JournalEntry
                Dim lintCount As Integer

                'Dim index As Integer = CCommon.ToInteger(e.CommandArgument)
                'Dim BizDocsGridView As GridView = CType(e.CommandSource, GridView)

                If e.CommandName = "Delete" Then
                    Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, ImageButton).NamingContainer, GridViewRow)
                    Dim txtOppBizId As TextBox
                    Dim lintJournalId As Integer
                    Dim lblBizDocID As Label
                    Dim hdnOppType As HiddenField
                    Dim txtOppID As TextBox

                    hdnOppType = row.FindControl("hdnOppType")
                    txtOppBizId = row.FindControl("txtBizDocID")
                    lblBizDocID = row.FindControl("lblBizDocID")
                    txtOppID = row.FindControl("txtOppID")
                    Dim objBizDocs As New OppBizDocs

                    objBizDocs.OppBizDocId = txtOppBizId.Text
                    objBizDocs.OppId = CCommon.ToLong(IIf(txtOppID Is Nothing, 0, txtOppID.Text))
                    objBizDocs.DomainID = Session("DomainID")
                    objBizDocs.UserCntID = Session("UserContactID")

                    objOppBizDocs.OppId = CCommon.ToLong(IIf(txtOppID Is Nothing, 0, txtOppID.Text))
                    objOppBizDocs.UserCntID = Session("UserContactID")
                    objOppBizDocs.DomainID = Session("DomainID")
                    lintCount = objBizDocs.GetOpportunityBizDocsCount()
                    'objOppBizDocs.OppType = OppType
                    lintAuthorizativeBizDocsId = objOppBizDocs.GetAuthorizativeOpportuntiy()
                    objOppBizDocs.OppBizDocId = txtOppBizId.Text
                    lintJournalId = objOppBizDocs.GetJournalIdForAuthorizativeBizDocs


                    If lintAuthorizativeBizDocsId = lblBizDocID.Text Then
                        If lintCount = 0 Then
                            Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                                ''To Delete Journal Entry
                                If lintJournalId <> 0 Then
                                    lobjJournalEntry.JournalId = lintJournalId
                                    lobjJournalEntry.DomainID = Session("DomainId")
                                    lobjJournalEntry.DeleteJournalEntryDetails()
                                End If
                                objBizDocs.DeleteComissionJournal()

                                Dim objAutomatonRule As New AutomatonRule
                                objAutomatonRule.ExecuteAutomationRule(49, objOppBizDocs.OppBizDocId, 4)

                                objBizDocs.DeleteBizDoc()
                                objTransactionScope.Complete()
                            End Using

                            ''Added By Sachin Sadhu||Date:8thMay2014
                            ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                            ''          Using Change tracking
                            Dim objWfA As New Workflow()
                            objWfA.DomainID = Session("DomainID")
                            objWfA.UserCntID = Session("UserContactID")
                            objWfA.RecordID = CCommon.ToLong(txtOppBizId.Text)
                            objWfA.SaveWFBizDocQueue()
                            'end of code

                            Response.Redirect("frmBizDocs.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=" & CCommon.ToLong(IIf(txtOppID Is Nothing, 0, txtOppID.Text)) & "&OppType=" & CCommon.ToShort(IIf(hdnOppType Is Nothing, 0, hdnOppType.Value)), False)
                        Else
                            litMessage.Text = IIf(hdnOppType.Value = 1, "You cannot delete BizDocs for which amount is deposited", "You cannot delete BizDocs for which amount is paid")
                        End If
                    Else
                        Using objTransactionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                            If lintJournalId <> 0 Then
                                lobjJournalEntry.JournalId = lintJournalId
                                lobjJournalEntry.DomainID = Session("DomainId")
                                lobjJournalEntry.DeleteJournalEntryDetails()
                            End If
                            objBizDocs.DeleteComissionJournal()

                            Dim objAutomatonRule As New AutomatonRule
                            objAutomatonRule.ExecuteAutomationRule(49, objOppBizDocs.OppBizDocId, 4)

                            objBizDocs.DeleteBizDoc()
                            objTransactionScope.Complete()
                        End Using

                        ''Added By Sachin Sadhu||Date:8thMay2014
                        ''Purpose :To Add BizDoc data in work Flow queue based on created Rules
                        ''          Using Change tracking
                        Dim objWfA As New Workflow()
                        objWfA.DomainID = Session("DomainID")
                        objWfA.UserCntID = Session("UserContactID")
                        objWfA.RecordID = CCommon.ToLong(txtOppBizId.Text)
                        objWfA.SaveWFBizDocQueue()
                        'end of code
                    End If
                ElseIf e.CommandName = "Email" Or e.CommandName = "PDF" Or e.CommandName = "Print" Then
                    Dim row As GridViewRow = DirectCast(DirectCast(e.CommandSource, ImageButton).NamingContainer, GridViewRow)
                    CreateHTMLBizDocs(e.CommandName, CCommon.ToLong(IIf(row.FindControl("txtOppID") Is Nothing, 0, DirectCast(row.FindControl("txtOppID"), TextBox).Text)), CCommon.ToLong(CType(row.FindControl("txtBizDocID"), TextBox).Text), _
                                      CType(row.FindControl("txtConEmail"), TextBox).Text, CCommon.ToLong(CType(row.FindControl("txtConID"), TextBox).Text), _
                                      CCommon.ToLong(CType(row.FindControl("txtBizDocTemplate"), TextBox).Text))

                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub gvBizDocs_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvBizDocs.RowDataBound
            Try
                'If e.Row.RowType = ListItemType.Header Then
                '    'CType(e.Row.FindControl("lblRefOrderNo"), Label).Text = IIf(OppType = 1, "P.O #", "Invoice #")
                'End If

                If e.Row.RowType = ListItemType.AlternatingItem Or e.Row.RowType = ListItemType.Item Then
                    Dim btnDelete As Button
                    Dim lnkDelete As LinkButton
                    Dim txtOppID As TextBox
                    Dim hdnOppType As HiddenField
                    Dim lblBizDocID As Label
                    txtOppID = e.Row.FindControl("txtOppID")
                    hdnOppType = e.Row.FindControl("hdnOppType")
                    lnkDelete = e.Row.FindControl("lnkDelete")
                    btnDelete = e.Row.FindControl("btnDelete")
                    lblBizDocID = e.Row.FindControl("lblBizDocID")
                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                        btnDelete.Visible = False
                        lnkDelete.Visible = True
                        lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    Else : btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If

                    'Dim hplEditBizdcoc As HyperLink
                    'hpl = e.Row.FindControl("hplBizdcoc")
                    Dim txtOppBizId As TextBox
                    txtOppBizId = e.Row.FindControl("txtBizDocID")
                    'hpl.Attributes.Add("onclick", "return OpenBizInvoice('" & CCommon.ToLong(IIf(txtOppID Is Nothing, 0, txtOppID.Text)) & "','" & txtOppBizId.Text & "',0)")

                    Dim lblEditBizdcoc As Label
                    lblEditBizdcoc = e.Row.FindControl("lblEditBizdcoc")
                    If lblEditBizdcoc IsNot Nothing Then
                        lblEditBizdcoc.Text = DataBinder.Eval(e.Row.DataItem, "vcBizDocID") & " (" & DataBinder.Eval(e.Row.DataItem, "vcCompanyName") & ")"
                    End If

                    Dim editImage As System.Web.UI.WebControls.Image
                    editImage = DirectCast(e.Row.FindControl("editImage"), System.Web.UI.WebControls.Image)
                    If editImage IsNot Nothing Then
                        editImage.Attributes.Add("onclick", "return OpenEdit('" & CCommon.ToLong(IIf(txtOppID Is Nothing, 0, txtOppID.Text)) & "','" & txtOppBizId.Text & "','" & CCommon.ToShort(IIf(hdnOppType Is Nothing, 0, hdnOppType.Value)) & "')")
                    End If

                    Dim ibtnPrint As ImageButton
                    ibtnPrint = e.Row.FindControl("ibtnPrint")
                    ibtnPrint.Attributes.Add("onclick", "return OpenBizInvoice('" & CCommon.ToLong(IIf(txtOppID Is Nothing, 0, txtOppID.Text)) & "','" & txtOppBizId.Text & "',1)")

                    Dim ibtnPayments As ImageButton
                    Dim txtIsAuthBizDoc As TextBox
                    ibtnPayments = e.Row.FindControl("ibtnPayments")
                    txtIsAuthBizDoc = e.Row.FindControl("txtIsAuthBizDoc")

                    ibtnPayments.Visible = IIf(CCommon.ToShort(IIf(hdnOppType Is Nothing, 0, hdnOppType.Value)) = 1, True, False)
                    ibtnPayments.Visible = IIf(txtIsAuthBizDoc Is Nothing, False, txtIsAuthBizDoc.Text)
                    ibtnPayments.Attributes.Add("onclick", "return OpenAmtPaid('" & CCommon.ToLong(IIf(txtOppID Is Nothing, 0, txtOppID.Text)) & "','" & txtOppBizId.Text & "','" & DataBinder.Eval(e.Row.DataItem, "numDivisionId") & "')")

                    Dim ddlBizDocStatus As DropDownList
                    ddlBizDocStatus = e.Row.FindControl("ddlBizDocStatus")
                    'Changed by Sachin Sadhu||Date:17thJune2014
                    'Purpose :Filter Status Bizdoc Type wise
                    objCommon.sb_FillComboFromDBwithSel(ddlBizDocStatus, 11, Session("DomainID"), CCommon.ToString(lblBizDocID.Text))

                    'End of code
                    If Not IsDBNull(DataBinder.Eval(e.Row.DataItem, "numBizDocStatus")) Then
                        If Not ddlBizDocStatus.Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "numBizDocStatus")) Is Nothing Then
                            ddlBizDocStatus.ClearSelection()
                            ddlBizDocStatus.Items.FindByValue(DataBinder.Eval(e.Row.DataItem, "numBizDocStatus")).Selected = True
                        End If
                    End If

                    'Dim strDate As Date
                    'If Not IsDBNull(DataBinder.Eval(e.Row.DataItem, "dtFromDate")) Then
                    '    strDate = DateAdd(DateInterval.Day, CCommon.ToInteger(DataBinder.Eval(e.Row.DataItem, "numBillingDaysName")), DataBinder.Eval(e.Row.DataItem, "dtFromDate"))

                    '    'Dim lblDuedate As Label
                    '    'lblDuedate = e.Row.FindControl("lblDuedate")
                    '    'lblDuedate.Text = FormattedDateFromDate(strDate, Session("DateFormat"))
                    'End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub gvBizDocs_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles gvBizDocs.Sorting
            Try
                If (ViewState("PreviousSortExpression") = e.SortExpression) Then
                    'ViewState("SortCol") = e.SortExpression
                    txtSortColumn.Text = e.SortExpression
                    If Not ViewState("PreviousSortDirection") Is Nothing Then
                        If (ViewState("PreviousSortDirection") = "asc") Then
                            'ViewState("SortDirection") = "desc"
                            txtSortOrder.Text = "desc"
                        Else
                            'ViewState("SortDirection") = "asc"
                            txtSortOrder.Text = "asc"
                        End If
                    Else
                        'ViewState("SortDirection") = "desc"
                        txtSortOrder.Text = "desc"
                    End If
                Else
                    txtSortColumn.Text = e.SortExpression
                    txtSortOrder.Text = "asc"

                    'ViewState("SortCol") = e.SortExpression
                    'ViewState("SortDirection") = "asc"
                End If

                ViewState("PreviousSortExpression") = e.SortExpression
                ViewState("PreviousSortDirection") = txtSortOrder.Text 'ViewState("SortDirection")
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub CreateHTMLBizDocs(ByVal CommandType As String, ByVal lngOppID As Long, ByVal lngOppBizDocID As Long, ByVal strConEmail As String, ByVal lngConID As Long, ByVal lngBizDocTemplate As Long)
            Try
                If CommandType = "Email" Then
                    Session("Attachements") = Nothing 'reset to nothing to avoid duplicate attachments
                    Dim objBizDocs As New OppBizDocs
                    Dim dt As DataTable
                    objBizDocs.BizDocId = lngOppBizDocID
                    objBizDocs.DomainID = Session("DomainID")
                    objBizDocs.BizDocTemplateID = lngBizDocTemplate
                    dt = objBizDocs.GetBizDocAttachments
                    Dim i As Integer
                    Dim strFileName As String = ""
                    Dim strFilePhysicalLocation As String = ""
                    For i = 0 To dt.Rows.Count - 1
                        If dt.Rows(i).Item("vcDocName") = "BizDoc" Then
                            strFileName = "File" & Format(Now, "ddmmyyyyhhmmssfff") & ".pdf"
                            strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName

                            Dim objHTMLToPDF As New HTMLToPDF
                            System.IO.File.WriteAllBytes(strFilePhysicalLocation, objHTMLToPDF.CreateBizDocPDF(lngOppID, lngOppBizDocID, CCommon.ToLong(Session("DomainID")), CCommon.ToLong(Session("UserContactID")), CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")), CCommon.ToString(Session("DateFormat"))))
                        Else
                            strFileName = dt.Rows(i).Item("vcURL")
                        End If
                        strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
                        objCommon.AddAttchmentToSession(strFileName, CCommon.GetDocumentPath(Session("DomainID")) & strFileName, strFilePhysicalLocation)
                    Next
                    Dim str As String
                    str = "<script language='javascript'>window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&PickAtch=1&isAttachmentRequired=1&Lsemail=" & HttpUtility.JavaScriptStringEncode(strConEmail) & "&pqwRT=" & lngConID & "&BizDocID=" & lngOppBizDocID.ToString() & "&OppID=" & lngOppID.ToString() & "','ComposeWindow','toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')</script>"
                    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Email", str, False)
                ElseIf CommandType = "PDF" Then
                    Dim objBizDocs As New OppBizDocs
                    Dim strFileName As String = "File" & Format(Now, "ddmmyyyyhhmmssfff") & ".pdf"
                    Dim strFilePhysicalLocation As String = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName

                    Dim objHTMLToPDF As New HTMLToPDF
                    System.IO.File.WriteAllBytes(strFilePhysicalLocation, objHTMLToPDF.CreateBizDocPDF(lngOppID, lngOppBizDocID, CCommon.ToLong(Session("DomainID")), CCommon.ToLong(Session("UserContactID")), CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")), CCommon.ToString(Session("DateFormat"))))

                    Response.Clear()
                    Response.ClearContent()
                    Response.ContentType = "application/pdf"
                    Response.AddHeader("content-disposition", "attachment; filename=" + strFileName)
                    'Response.Write(Session("Attachements"))

                    Response.WriteFile(strFilePhysicalLocation)

                    Response.End()
                    'ElseIf CommandType = "Print" Then
                    '    Dim strPrint As String
                    '    'strPrint = "win = window.open();self.focus();win.document.open();"
                    '    ''strPrint += "win.document.write('" & htmlCodeToConvert & "');"

                    '    'strPrint += "if (navigator.userAgent.toLowerCase().indexOf('chrome') > -1) {"
                    '    'strPrint += "win.print();"
                    '    'strPrint += "}"
                    '    'strPrint += "else {"
                    '    'strPrint += "win.document.close();"
                    '    'strPrint += "win.print();"
                    '    'strPrint += "win.close();"
                    '    'strPrint += "}"

                    '    Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Print", strPrint, True)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Function ReturnName(ByVal SDate) As String
            Try
                Dim strDate As String = ""
                If Not IsDBNull(SDate) Then
                    strDate = FormattedDateFromDate(SDate, Session("DateFormat"))
                    If Format(SDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                        strDate = "<font color=red>" & strDate & "</font>"
                    ElseIf Format(SDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                        strDate = "<font color=orange>" & strDate & "</font>"
                    End If
                End If
                Return strDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnGo_Click(sender As Object, e As EventArgs) Handles btnGo.Click
            Try
                Select Case CCommon.ToShort(ddlAction.SelectedValue)
                    Case 0
                        litMessage.Text = "Select Action first."

                    Case 1
                        GetPDF(txtRecordIds.Text.Trim(","))

                    Case 2
                        SendMassMail(txtRecordIds.Text.Trim(","))

                End Select

                txtRecordIds.Text = ""
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub BindBizDocStatus()
            Try
                'BizDoc Status
                Dim dtdata As New DataTable
                dtdata = objCommon.GetMasterListItems(11, Session("DomainID"))
                radBizDocStatus.DataSource = dtdata
                radBizDocStatus.DataTextField = "vcData"
                radBizDocStatus.DataValueField = "numListItemID"
                radBizDocStatus.DataBind()

                radBizDocStatus.Items.Add(New RadComboBoxItem("--None--", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub btnBizDocStatus_Click(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = ""
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlDateFilter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDateFilter.SelectedIndexChanged
            Try
                txtCurrrentPage.Text = ""
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)

            End Try
        End Sub
        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                'txtCurrrentPage_TextChanged(Nothing, Nothing)
                BindDataGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
#Region "Actions Method"

        Private Sub SendMassMail(ByVal strBizDocsIDs As String)

            If strBizDocsIDs.Length > 0 Then
                Dim lngOppID As Double = 0
                Dim lngOppBizDocID As Double = 0
                Dim strOppBizDocIDs As String = ""

                Dim sw As New System.IO.StringWriter()
                Dim objBizDocs As New OppBizDocs
                Dim strFileName As String = ""
                Dim strFilePhysicalLocation As String = ""
                Dim dtAttachments As New DataTable
                Dim drRow As DataRow

                Dim dcCol(1) As DataColumn
                dcCol(0) = New DataColumn
                dcCol(0).ColumnName = "numOppBizDocsID"
                dcCol(0).DataType = GetType(System.Double)
                dtAttachments.Columns.Add(dcCol(0))
                dtAttachments.PrimaryKey = dcCol
                dtAttachments.Columns.Add("numOppID")
                dtAttachments.Columns.Add("AttachmentFileName")
                dtAttachments.Columns.Add("AttchmentFilePath")

                For Each strItem As String In strBizDocsIDs.Split(",")

                    If strItem.Split(":").Length > 0 Then
                        lngOppID = CCommon.ToDouble(strItem.Split(":")(0))
                    End If

                    If strItem.Split(":").Length > 1 Then
                        lngOppBizDocID = CCommon.ToDouble(strItem.Split(":")(1))
                        strOppBizDocIDs = strOppBizDocIDs & "," & lngOppBizDocID
                    End If

                    If lngOppID = 0 OrElse lngOppBizDocID = 0 Then
                        Continue For
                    End If

                    strFileName = "File" & Format(Now, "ddmmyyyyhhmmssfff") & ".pdf"
                    strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName

                    Dim objHTMLToPDF As New HTMLToPDF
                    System.IO.File.WriteAllBytes(strFilePhysicalLocation, objHTMLToPDF.CreateBizDocPDF(lngOppID, lngOppBizDocID, CCommon.ToLong(Session("DomainID")), CCommon.ToLong(Session("UserContactID")), CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")), CCommon.ToString(Session("DateFormat"))))

                    drRow = dtAttachments.NewRow
                    drRow("numOppID") = lngOppID
                    drRow("numOppBizDocsID") = lngOppBizDocID
                    drRow("AttachmentFileName") = strFileName
                    drRow("AttchmentFilePath") = strFilePhysicalLocation
                    dtAttachments.Rows.Add(drRow)
                    dtAttachments.AcceptChanges()
                Next

                Session("Attachements") = Nothing
                Dim objSendEmail As New Email()
                objSendEmail.DomainID = CCommon.ToLong(Session("DomainID"))
                objSendEmail.TemplateCode = "#SYS#ECOMMERCE_SHOPPING_COMPLETED"

                objSendEmail.ModuleID = 8
                objSendEmail.RecordIds = strOppBizDocIDs.Trim(",") ' CCommon.ToString(Session("OppID"))
                objSendEmail.FromEmail = CCommon.ToString(Session("UserEmail")) 'CCommon.ToString(Session("DomainAdminEmail"))
                objSendEmail.ToEmail = "##ContactFirstName## <##ContactEmail##>" 'CCommon.ToString(Session("UserEmail"))
                objSendEmail.IsAttachmentDelete = True

                'Dim ParamValues(2) As Object
                'ParamValues(0) = ""
                'ParamValues(1) = dtAttachments
                objSendEmail.SendEmailTemplate("", dtAttachments)

            End If
        End Sub

        Private Sub GetPDF(ByVal strBizDocsIDs As String)
            If strBizDocsIDs.Length > 0 Then
                Dim sbFileContent As String = ""
                Dim lngOppID As Double = 0
                Dim lngOppBizDocID As Double = 0
                Dim strOppBizDocIDs As String = ""

                Dim sw As New System.IO.StringWriter()
                Dim objBizDocs As New OppBizDocs
                Dim strFileName As String = ""
                Dim strFilePhysicalLocation As String = ""
                Dim objHTMLToPDF As New HTMLToPDF

                For Each strItem As String In strBizDocsIDs.Split(",")

                    If strItem.Split(":").Length > 0 Then
                        lngOppID = CCommon.ToDouble(strItem.Split(":")(0))
                    End If

                    If strItem.Split(":").Length > 1 Then
                        lngOppBizDocID = CCommon.ToDouble(strItem.Split(":")(1))
                        strOppBizDocIDs = strOppBizDocIDs & "," & lngOppBizDocID
                    End If

                    If lngOppID = 0 OrElse lngOppBizDocID = 0 Then
                        Continue For
                    End If

                    Server.Execute("frmBizInvoice.aspx" & QueryEncryption.EncryptQueryString("OpID=" & lngOppID & "&OppBizId=" & lngOppBizDocID & "&DocGenerate=1"), sw)
                    Dim htmlCodeToConvert As String = sw.GetStringBuilder().ToString()
                    sw.Close()

                    Try
                        HtmlAgilityPack.HtmlNode.ElementsFlags.Remove("form")
                        Dim html As HtmlAgilityPack.HtmlDocument = New HtmlAgilityPack.HtmlDocument
                        html.OptionAutoCloseOnEnd = True
                        html.LoadHtml(htmlCodeToConvert)

                        Dim node As HtmlAgilityPack.HtmlNode = html.DocumentNode.SelectSingleNode("//div[@id='pnlBizDoc']")
                        If Not node Is Nothing Then
                            htmlCodeToConvert = node.InnerHtml
                        End If
                    Catch ex As Exception

                    End Try

                    If sbFileContent.ToString.Length > 0 Then
                        htmlCodeToConvert = "<tr style=""page-break-before: always""><td style=""width:100% !important"">" & htmlCodeToConvert & "</td></tr>"
                    Else
                        htmlCodeToConvert = "<tr style=""page-break-before: avoid""><td style=""width:100% !important"">" & htmlCodeToConvert & "</td></tr>"
                    End If

                    sbFileContent += htmlCodeToConvert
                    strFileName = ""
                    strFilePhysicalLocation = ""
                    sw = New System.IO.StringWriter()
                Next


                sbFileContent = "<html><head><link rel='stylesheet' href='" & Server.MapPath("~/CSS/master.css") & "' type='text/css' /></head><body><table style=""width:100% !important"">" & sbFileContent.ToString() & "</table></body></html>"

                strFileName = objHTMLToPDF.ConvertHTML2PDF(sbFileContent.ToString(), CCommon.ToLong(Session("DomainID")))
                strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
                Response.Clear()
                Response.ClearContent()
                Response.ContentType = "application/pdf"
                Response.AddHeader("content-disposition", "attachment; filename=" + strFileName)
                Response.WriteFile(strFilePhysicalLocation)
                Response.Flush()
                HttpContext.Current.ApplicationInstance.CompleteRequest()
            End If
        End Sub

#End Region

    End Class
End Namespace
