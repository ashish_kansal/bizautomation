﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace BACRM.UserInterface.Opportunities
    
    Partial Public Class frmSalesReturns
        
        '''<summary>
        '''PageTitle control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents PageTitle As Global.System.Web.UI.HtmlControls.HtmlGenericControl
        
        '''<summary>
        '''btnAddNewReturn control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnAddNewReturn As Global.System.Web.UI.HtmlControls.HtmlButton
        
        '''<summary>
        '''btnDelete control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnDelete As Global.System.Web.UI.WebControls.LinkButton
        
        '''<summary>
        '''divAlert control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents divAlert As Global.System.Web.UI.HtmlControls.HtmlGenericControl
        
        '''<summary>
        '''lblMessage control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label
        
        '''<summary>
        '''lblReturns control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lblReturns As Global.System.Web.UI.WebControls.Label
        
        '''<summary>
        '''lnkClearFilter control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents lnkClearFilter As Global.System.Web.UI.WebControls.LinkButton
        
        '''<summary>
        '''bizPager control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents bizPager As Global.Wuqi.Webdiyer.AspNetPager
        
        '''<summary>
        '''UpdateProgress control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents UpdateProgress As Global.System.Web.UI.UpdateProgress
        
        '''<summary>
        '''gvSalesReturns control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents gvSalesReturns As Global.System.Web.UI.WebControls.GridView
        
        '''<summary>
        '''txtTotalPage control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtTotalPage As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''txtTotalRecords control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtTotalRecords As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''txtSortChar control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtSortChar As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''btnGo1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnGo1 As Global.System.Web.UI.WebControls.Button
        
        '''<summary>
        '''valSummary control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents valSummary As Global.System.Web.UI.WebControls.ValidationSummary
        
        '''<summary>
        '''txtCurrrentPage control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtCurrrentPage As Global.System.Web.UI.WebControls.TextBox
        
        '''<summary>
        '''hdnOrgSearch control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdnOrgSearch As Global.System.Web.UI.WebControls.HiddenField
        
        '''<summary>
        '''hdnRMASearch control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdnRMASearch As Global.System.Web.UI.WebControls.HiddenField
        
        '''<summary>
        '''hdnStatus control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdnStatus As Global.System.Web.UI.WebControls.HiddenField
        
        '''<summary>
        '''hdnReason control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdnReason As Global.System.Web.UI.WebControls.HiddenField
        
        '''<summary>
        '''hdnSortColumn control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdnSortColumn As Global.System.Web.UI.WebControls.HiddenField
        
        '''<summary>
        '''hdnSortDirection control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents hdnSortDirection As Global.System.Web.UI.WebControls.HiddenField
    End Class
End Namespace
