﻿Imports BACRM.BusinessLogic.Common

Public Class frmChangeGrossProfit
    Inherits BACRMPage

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Page.IsPostBack Then
            radNumTxtProfitPercent.Text = GetQueryStringVal("Percent").Replace("%", "")
        End If
    End Sub
#End Region

#Region "Event Handlers"
    Private Sub btnSaveClose_Click(sender As Object, e As EventArgs) Handles btnSaveClose.Click
        Try
            Response.Write("<script>opener.ChangeGrossProfit('" & CCommon.ToDecimal(radNumTxtProfitPercent.Text) & "'); self.close();</script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

End Class