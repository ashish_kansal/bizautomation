<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmConfOppList.aspx.vb"
    ClientIDMode="Predictable" Inherits="BACRM.UserInterface.Opportunities.frmConfOppList"
    MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Contacts Column Customization</title>
    <script language="javascript" src="../javascript/AdvSearchScripts.js"></script>
    <script language="javascript" type="text/javascript">
        function Save() {
            var obj = document.getElementById('lstSelectedfld');
            var str = '';
            for (var i = 0; i < obj.options.length; i++) {
                var SelectedValue;
                SelectedValue = obj.options[i].value;
                str = str + SelectedValue + ','
            }
            document.getElementById('hdnCol').value = str;
            //  alert( document.form1.hdnCol.value)
        }
        function OpenSettings(type, FormID) {
            window.open('../Items/frmConfItemList.aspx?FormID=' + FormID + '&type=' + type, '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button" Width="50" />
            <input class="button" id="btnClose" style="width: 50" onclick="javascript:opener.location.reload(true);window.close()"
                type="button" value="Close">&nbsp;&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Opportunities & Orders Layout
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <br />
    <table cellspacing="0" cellpadding="0" width="600">
        <tr>
            <td>
                <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="true" CssClass="signup">
                    <asp:ListItem Text="Sales Opportunities" Value="38"></asp:ListItem>
                    <asp:ListItem Text="Purchase Opportunities" Value="40"></asp:ListItem>
                    <asp:ListItem Text="Sales Order" Value="39"></asp:ListItem>
                    <asp:ListItem Text="Purchase Order" Value="41"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <asp:Table ID="tblAdvSearchFieldCustomization" runat="server" Width="600" GridLines="None"
        CssClass="aspTable" BorderColor="black" BorderWidth="1">
        <asp:TableRow>
            <asp:TableCell>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center" class="normal1" valign="top">
                            Available Fields<br>
                            &nbsp;&nbsp;
                            <asp:ListBox ID="lstAvailablefld" runat="server" Width="150" Height="200" CssClass="signup"
                                EnableViewState="False"></asp:ListBox>
                        </td>
                        <td align="center" class="normal1" valign="middle">
                            <input type="button" id="btnAdd" class="button" value="Add >" onclick="javascript:move(document.getElementById('lstAvailablefld'),document.getElementById('lstSelectedfld'))">
                            <br>
                            <br>
                            <input type="button" id="btnRemove" class="button" value="< Remove" onclick="javascript:remove1(document.getElementById('lstSelectedfld'),document.getElementById('lstAvailablefld'));">
                        </td>
                        <td align="center" class="normal1">
                            Selected Fields/ Choose Order<br>
                            <asp:ListBox ID="lstSelectedfld" runat="server" Width="150" Height="200" CssClass="signup"
                                EnableViewState="False"></asp:ListBox>
                        </td>
                        <td align="center" class="normal1" valign="middle">
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <img id="btnMoveupOne" src="../images/upArrow.gif" onclick="javascript:MoveUp(document.getElementById('lstSelectedfld'));" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <br>
                            <br>
                            <img id="btnMoveDownOne" src="../images/downArrow1.gif" onclick="javascript:MoveDown(document.getElementById('lstSelectedfld')
                                
                                );" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            (Max 15)
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <table width="100%">
                    <%--    <tr>
                        <td colspan="2" class="normal7">
                            When you click selected Opportunity, the list will default based on your settings
                            here:
                        </td>
                    </tr>
                    <tr>
                        <td class="normal1">
                            Filter :
                        </td>
                        <td align="left">
                            <asp:DropDownList ID="ddlSortSales" runat="server" CssClass="signup" Width="210">
                                <asp:ListItem Value="1">My Sales Opportunities</asp:ListItem>
                                <asp:ListItem Value="3">All Sales Opportunities</asp:ListItem>
                                <asp:ListItem Value="2">My Subordinates</asp:ListItem>
                                <asp:ListItem Value="4">Added in Last 7 days</asp:ListItem>
                                <asp:ListItem Value="5">Last 20 Added by me</asp:ListItem>
                                <asp:ListItem Value="6">Last 20 Modified by me</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlSortPurchase" runat="server" CssClass="signup" Width="210">
                                <asp:ListItem Value="1">My Purchase Opportunities</asp:ListItem>
                                <asp:ListItem Value="3">All Purchase Opportunities</asp:ListItem>
                                <asp:ListItem Value="2">My Subordinates</asp:ListItem>
                                <asp:ListItem Value="4">Added in Last 7 days</asp:ListItem>
                                <asp:ListItem Value="5">Last 20 Added by me</asp:ListItem>
                                <asp:ListItem Value="6">Last 20 Modified by me</asp:ListItem>
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlFilterBy" runat="server" CssClass="signup" Width="210">
                                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                                <asp:ListItem Value="1">Partially Fulfilled Orders</asp:ListItem>
                                <asp:ListItem Value="2">Fulfilled Orders</asp:ListItem>
                                <asp:ListItem Value="3">Orders without Authoritative-BizDocs</asp:ListItem>
                                <asp:ListItem Value="4">Orders with child records</asp:ListItem>
                                <asp:ListItem Value="5">Sort Alphabetically</asp:ListItem>
                               
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="normal7">
                            When you click on the Opportunity tab, is this the list you want to see ?
                            <asp:RadioButton ID="radY" GroupName="Group" runat="server" Text="Yes" />
                            <asp:RadioButton ID="radN" GroupName="Group" runat="server" Checked="true" Text="No" />
                        </td>
                    </tr>--%>
                    <tr>
                        <td colspan="2" class="normal7">
                            <br />
                            What fields do you want to see in result when search for item in New Sales Order/Purchase
                            Order ?
                            <asp:HyperLink Text="Configure Columns" CssClass="hyperlink" runat="server" ID="hplSettings"
                                onclick="return OpenSettings('0','22');"></asp:HyperLink>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="normal7">
                            <br />
                            What fields do you want to use to search result for item in New Sales Order/Purchase
                            Order ?
                            <asp:HyperLink Text="Configure Search" CssClass="hyperlink" runat="server" ID="hplSearch"
                                onclick="return OpenSettings('1','22');"></asp:HyperLink>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <input id="hdnCol" type="hidden" name="hdXMLString" runat="server" value="" />
    <input id="hdSave" type="hidden" name="hdSave" runat="server" value="False" />
    <asp:HiddenField ID="hfFormId" runat="server" />
</asp:Content>
