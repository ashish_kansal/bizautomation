﻿Imports BACRM.BusinessLogic.Common
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Opportunities

Public Class frmOpportunityAutomationQueueExecutionLog
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindDatagrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub BindDatagrid()
        Try
            Dim objOppBizDocs As New OppBizDocs
            objOppBizDocs.OppId = GetQueryStringVal("OppID")
            objOppBizDocs.DomainID = Session("DomainID")

            If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
            objOppBizDocs.CurrentPage = txtCurrrentPage.Text.Trim()

            objOppBizDocs.PageSize = Session("PagingRows")
            objOppBizDocs.TotalRecords = 0
            objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

            gvAutomationLog.DataSource = objOppBizDocs.GetOpportunityAutomationQueueExecutionLog()
            gvAutomationLog.DataBind()

            bizPager.PageSize = Session("PagingRows")
            bizPager.RecordCount = objOppBizDocs.TotalRecords
            bizPager.CurrentPageIndex = txtCurrrentPage.Text
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, DomainID, UserCntID, Request)
            Response.Write(ex)
        End Try
    End Sub

End Class