﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmConfNewOrder.aspx.vb" Inherits=".frmConfNewOrder" MasterPageFile="~/common/DetailPage.Master" ClientIDMode="Static" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <%--<link rel="stylesheet" href="../CSS/OrderStyle.css" type="text/css" />--%>
    <link rel="stylesheet" href="../CSS/lists.css" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script language="JavaScript" type="text/javascript" src="../javascript/coordinates.js"></script>
    <script language="JavaScript" type="text/javascript" src="../javascript/drag.js"></script>
    <script language="JavaScript" type="text/javascript" src="../javascript/dragdrop.js"></script>
    <%--<script src="../JavaScript/jquery.min.js" type="text/javascript"></script>--%>
    <script src="../JavaScript/jquery-ui.min.js" type="text/javascript"></script>
    <%--<script src="../JavaScript/jquery.Tooltip.js" type="text/javascript"></script>--%>
    <script type="text/javascript" src="../JavaScript/jquery.validate.custom-script.js"></script>
    <script type="text/javascript" src="../JavaScript/jquery.validate.min.js"></script>
    <%--Used For JQuery Validation Purpose--%>
    <script type="text/javascript" src="../JavaScript/jquery.metadata.min.js"></script>
    <%--Used For JQuery Validation Purpose--%>
    <script type="text/javascript">
        window.onload = function () {
            var max = document.getElementById("rows").value;

            for (var x = 0; x <= max; x++) {
                if (document.getElementById("x" + x) != null) {
                    list = document.getElementById("x" + x);
                    DragDrop.makeListContainer(list, 'g1');
                    list.onDragOver = function () { this.style["background"] = "none"; };
                    list.onDragOut = function () { this.style["background"] = "none"; };
                }
            }

            for (var x = 0; x <= max; x++) {
                if (document.getElementById("y" + x) != null) {
                    list = document.getElementById("y" + x);
                    DragDrop.makeListContainer(list, 'g2');
                    list.onDragOver = function () { this.style["background"] = "none"; };
                    list.onDragOut = function () { this.style["background"] = "none"; };
                }
            }

            for (var x = 0; x <= max; x++) {
                if (document.getElementById("z" + x) != null) {
                    list = document.getElementById("z" + x);
                    DragDrop.makeListContainer(list, 'g3');
                    list.onDragOver = function () { this.style["background"] = "none"; };
                    list.onDragOut = function () { this.style["background"] = "none"; };
                }
            }

            for (var x = 0; x <= max; x++) {
                if (document.getElementById("w" + x) != null) {
                    list = document.getElementById("w" + x);
                    DragDrop.makeListContainer(list, 'g4');
                    list.onDragOver = function () { this.style["background"] = "none"; };
                    list.onDragOut = function () { this.style["background"] = "none"; };
                }
            }
        };
        function show() {
            orderDetail = document.getElementById("hfOrderDetail");
            customerDetail = document.getElementById("hfCustomerDetail");
            itemDetail = document.getElementById("hfItemDetail");
            itemColumn = document.getElementById("hfItemColumn");

            orderDetail.value = DragDrop.serData('g1', null);
            itemDetail.value = DragDrop.serData('g2', null);
            itemColumn.value = DragDrop.serData('g3', null);
            customerDetail.value = DragDrop.serData('g4', null);
        }
        function fnEditEmployeeLocation() {
            strUrl = "../admin/frmConfEmployeeLocation.aspx"
            window.open(strUrl, "", "width=525,height=375,status=no,scrollbars=1,left=155,top=100");
            //strUrl = "../opportunity/frmNewOrder.aspx"
            //window.open(strUrl, "", "width=820,height=550,status=no,scrollbars=1,left=155,top=100");
            return false;
        }
    </script>
    <style type="text/css">
        #table-main {
            background: #fff;
        }
    </style>
     <style>
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
    <style>
        .cont2 {
            background: #fff;
            display: inline-block;
            width: 100%;
            padding: 12px;
            /*border: 1px solid #C7D2E4;*/
        }

    .cont1 {
        display: inline-block;
        background: #E8ECF9;
        padding: 12px;
        /*border: 1px solid #C7D2E4;*/
        width: 100%;
    }

        .line {
        border-bottom: 1px solid black;
}
    </style>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
   New Sales: Opportunity/Order Form Configuration&nbsp;<a href="#" onclick="return OpenHelpPopUp('opportunity/frmConfNewOrder.aspx')"><label class="badge bg-yellow">?</label></a>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
    </asp:Content>
    <asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
       <div class="col-md-1 col-md-offset-11">
        <asp:LinkButton ID="btnSave" runat="server" OnClientClick="show()" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
           </div>
        <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
        </asp:Content>  
    <asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
        </asp:Content>
    <asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
        <div class="row">
            <div class="cont2">
            <div class="col-md-12">
                <span><strong>Field to receive cursor default when opening form:</strong></span>
            </div>
            <div class="col-md-12">
                <asp:RadioButton ID="rdbAutoFocusCustomer" Text="Customer field in Section 1." runat="server" Checked="true" GroupName="CursorDefault" />
            </div>
            <div class="col-md-12">
                <asp:RadioButton ID="rdbAutoFocusItem" Text="Item look ahead field in Section 3." runat="server" GroupName="CursorDefault" />
            </div>
                </div>
            <div class="cont1">
            <%--<div class="col-md-12">
                <strong>Section 1:</strong>
            </div>--%>
            <div class="col-md-12">
                <asp:CheckBox ID="chkDisplayCurrency" runat="server" Checked="true" Text="Display currency drop-down." />
            </div>
           <%-- <div class="col-md-12">
                <asp:CheckBox ID="chkDisplayRootLocation" runat="server" Checked="true" Text="Display root location drop-down, and default item search by location selected in that drop-down." />
            </div>--%>
            <div class="col-md-12">
                <asp:CheckBox ID="chkDisplayAssignedTo" runat="server" Checked="true" Text="Display assign to drop-down." />
            </div>
            <div class="col-md-12">
                <asp:CheckBox ID="chkDisplaySalesTemplate" runat="server" Checked="true" Text="Display template drop-down." />
            </div>
            <div class="col-md-12">
                <asp:CheckBox ID="chkAutoAssignOrder" runat="server" Text="Automatically assign order to user logged in when they open the New SO form." />
            </div>
                 <div class="col-md-12">
                This section is dynamic, and will automatically configure based on a number of triggers happening such as customer, and items selected.
            </div>
            <div class="col-md-12">
                <asp:CheckBox ID="chkDisplayLocation" runat="server" Checked="true" Text="Display location (Not checking this will remove section from the form)" />
            </div>
            <div class="col-md-12">
                <asp:CheckBox ID="chkDisplayUnitCost" runat="server" Text="Display Unit Cost." Checked="true" />
            </div>
            <div class="col-md-12">
                <asp:CheckBox ID="chkDisplayProfitTotal" runat="server" Text="Display Profit/Unit Total." Checked="true" />
            </div>
                 <div class="col-md-12">
                <asp:CheckBox ID="chkDisplaySaveNew" runat="server" Checked="true" Text="Display Save & New button" />
            </div>
             <div class="col-md-12">
               <asp:CheckBox ID="chkDisplayAddToCart" runat="server" Checked="true" Text="Display Add Item to Site button (If the customer has an e-commerce account, the item will be added to the e-commerce site selected from within Administration | Global Settings | Order Mgt.)" />
            </div>
            <div class="col-md-12">
                <asp:CheckBox ID="chkDisplayCreateInvoice" runat="server" Checked="true" Text="Display Create & Open Unpaid Invoice button" />
            </div>
            <div class="col-md-12">
                <a onclick="return fnEditEmployeeLocation();" href="#">Map a default location for employees.</a>
            </div>
                </div>
            <div class="cont2">
            <div class="col-md-12 line">
                <%--<span><strong>Section 2 & 3:</strong></span>--%>
                <span><strong>Order Section</strong></span>
            </div>
           
            <div class="col-md-12">
                <br />
                <label style="font-style: italic;"><strong>Order Record Fields:</strong></label>
                <asp:Label ID="lblOrderReocrdFields" runat="server" Font-Italic="true" Text="These fields will be editable which you can fill in while building the order."></asp:Label>
            </div>

            <div>
                <table width="100%" border="0">
                          <tr>
                        <td style="width: 75%; vertical-align: top;">
                                   <div id="divOrderDetail" runat="server" style="clear: both; padding-top: 10px; height: auto" />
                              </td>
                        <td style="width: 25%; vertical-align: top; padding-top: 10px;">
                                  <table>
                                      <tr>
                                          <td>
                                              <span><strong>Added to Column 3</strong></span>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              <div>
                                                <asp:CheckBox ID="chkDisplayReleaseDate" Checked="true" runat="server" Text="Display Order Release Date." />
                                            </div>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              <div>
                                                <asp:CheckBox ID="chkDisplayOrderExpectedDate" Checked="true" runat="server" Text="Display Order Expected Date." />
                                            </div>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                              <div  >
                                               <%-- <asp:CheckBox ID="chkDisplayFinancialStamp" Checked="true" runat="server" Text="Display Financial Stamp (Stamp will automatically be located on the 1st row of the 2nd column of the section)." />--%>
                                                   <asp:CheckBox ID="chkDisplayFinancialStamp" Checked="true" runat="server" Text="Display Financial Stamp." />
                                            </div>
                                          </td>
                                      </tr>
                                      <tr>
                                          <td>
                                        <div>
                                                <asp:CheckBox ID="chkDisplayCommentsSection" Checked="true" runat="server" Text="Display Comments section." />
                                            </div>
                                          </td>
                                      </tr>
                                      
                                  </table>
                              </td>
                          </tr>
                    </table>
                </div>
          
            
            <div class="col-md-12 ">
                <br />
                <label style="font-style: italic;"><b>Customer Record Fields:</b></label>
                 <asp:Label ID="lblCustRecFields" runat="server" Font-Italic="true" Text="These are not editable (“read-only”) and can be used for reference purposes while building the order. "></asp:Label>
            </div>
            <div id="divCustomerDetail" runat="server" style="clear: both; padding-top: 10px; height: auto" />
                </div>
            <div class="cont1">
            <div class="col-md-12 line">
                <span><strong>Item Detail Section</strong></span>
            </div>
            
            <div class="col-md-12">
                <label style="font-style: italic;"><strong>Required fields:</strong></label>
                <asp:Label ID="lblItemrqfields" runat="server" Font-Italic="true" Text="Item selection controls, Quantity, UOM, Price per UOM and Discount (% / Amt) always display"></asp:Label>
            </div>
            <div class="col-md-12">
                <asp:CheckBox ID="chkDisplayCustomerPartEntry" Text="Display Customer Part # Entry" runat="server" Checked="true" />
            </div>
            
            </div>
            <div class="cont2">
            <div class="col-md-12 line">
                <span><strong>Item Grid Section</strong></span>
            </div>
           <div class="col-md-12">
                <label style="font-style: italic;"><strong>Required Columns: Item, Units, Unit Sale Price, Sales Tax, Total</strong></label>
           </div>
                &nbsp;
            <div class="col-md-12">
                <div class="col-sm-6">
                    <span style='text-align: center; font-style: italic; font-weight: bold;'>Default Column</span> <br />
                <table>
                    <tr>
                            <td>
                                <asp:Label ID="lblOrderAZ" runat="server" Text=" Order (A-z)"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButton ID="rdoShowOrderAZ" GroupName="grporder" Width="60px" runat="server" Text="Show" />
                             <asp:RadioButton ID="rdoHideOrderAZ" GroupName="grporder" runat="server" Text="Hide" />
                        </td>
                    </tr>
                    <tr>
                            <td>
                                <asp:Label ID="lblItemID" runat="server" Text="Item ID"></asp:Label>
                        </td>
                        <td>
                                <asp:RadioButton ID="rdoShowItemID" GroupName="grpItemId" Width="60px" runat="server" Text="Show" />
                             <asp:RadioButton ID="rdoHideItemID" GroupName="grpItemId" runat="server" Text="Hide" />
                        </td>
                    </tr>
                    <tr>
                        <td>    
                                <asp:Label ID="lblSKU" runat="server" Text="SKU"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButton ID="rdoShowSKU" GroupName="grpSKU" Width="60px" runat="server" Text="Show" />
                             <asp:RadioButton ID="rdoHideSKU" GroupName="grpSKU" runat="server" Text="Hide" />
                        </td>
                    </tr>
                    <tr>
                        <td>    
                                <asp:Label ID="lblUnitListPrice" runat="server" Text="Unit List Price"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButton ID="rdoShowUnitListPrice" GroupName="grpUnitListPrice" Width="60px" runat="server" Text="Show" />
                             <asp:RadioButton ID="rdoHideUnitListPrice" GroupName="grpUnitListPrice" runat="server" Text="Hide" />
                        </td>
                    </tr>
                     <tr>
                            <td>
                                <asp:Label ID="lblUnitSalePrice" runat="server" Text="Unit Sale Price"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButton ID="rdoShowUnitSalePrice" GroupName="grpUnitSalePrice" Width="60px" runat="server" Text="Show" />
                             <asp:RadioButton ID="rdoHideUnitSalePrice" GroupName="grpUnitSalePrice" runat="server" Text="Hide" />
                        </td>
                    </tr>
                     <tr>
                            <td>
                                <asp:Label ID="lblDiscount" runat="server" Text="Discount"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButton ID="rdoShowDiscount" GroupName="grpDiscount" Width="60px" runat="server" Text="Show" />
                             <asp:RadioButton ID="rdoHideDiscount" GroupName="grpDiscount" runat="server" Text="Hide" />
                        </td>
                    </tr>
                    <tr>
                            <td>
                                <asp:Label ID="lblItemReleaseDate" runat="server" Text="Item Release Date"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButton ID="rdoShowItemReleaseDate" GroupName="grpItemReleaseDate" Width="60px" runat="server" Text="Show" />
                             <asp:RadioButton ID="rdoHideItemReleaseDate" GroupName="grpItemReleaseDate" runat="server" Text="Hide" />
                        </td>
                    </tr>
                    <tr>
                            <td>
                                <asp:Label ID="lblLocation" runat="server" Text="Location"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButton ID="rdoShowLocation" GroupName="grpLocation" Width="60px" runat="server" Text="Show" />
                             <asp:RadioButton ID="rdoHideLocation" GroupName="grpLocation" runat="server" Text="Hide" />
                        </td>
                    </tr>
                     <tr>
                            <td>
                                <asp:Label ID="lblShipTo" runat="server" Text="Ship-To"></asp:Label>
                        </td>
                        <td>
                                <asp:RadioButton ID="rdoShowShipTo" GroupName="grpShipTo" Width="60px" runat="server" Text="Show" />
                             <asp:RadioButton ID="rdoHideShipTo" GroupName="grpShipTo" runat="server" Text="Hide" />
                        </td>
                    </tr>
                     <tr>
                            <td>
                                <asp:Label ID="lblOnHandAllocation" runat="server" Text="OnHand / Allocation"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButton ID="rdoShowOnHandAllocation" GroupName="grpOnHandAllocation" Width="60px" runat="server" Text="Show" />
                             <asp:RadioButton ID="rdoHideOnHandAllocation" GroupName="grpOnHandAllocation" runat="server" Text="Hide" />
                        </td>
                    </tr>
                    <tr>
                            <td>
                                <asp:Label ID="lblDescription" runat="server" Text="Description"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButton ID="rdoShowDescription" GroupName="grpDescription" Width="60px" runat="server" Text="Show" />
                             <asp:RadioButton ID="rdoHideDescription" GroupName="grpDescription" runat="server" Text="Hide" />
                        </td>
                    </tr>
                    <tr>
                            <td>
                                <asp:Label ID="lblNotes" runat="server" Text="Notes"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButton ID="rdoShowNotes" GroupName="grpNotes" Width="60px" runat="server" Text="Show" />
                             <asp:RadioButton ID="rdoHideNotes" GroupName="grpNotes" runat="server" Text="Hide" />
                        </td>
                    </tr>
                     <tr>
                            <td>
                                <asp:Label ID="lblAttributes" runat="server" Text="Attributes"></asp:Label>
                        </td>
                        <td>
                                <asp:RadioButton ID="rdoShowAttributes" GroupName="grpAttributes" Width="60px" runat="server" Text="Show" />
                             <asp:RadioButton ID="rdoHideAttributes" GroupName="grpAttributes" runat="server" Text="Hide" />
                        </td>
                    </tr>
                     <tr>
                            <td>
                                <asp:Label ID="lblInclusionDetail" runat="server" Text="Inclusion Detail"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButton ID="rdoShowInclusionDetail" GroupName="grpInclusionDetail" Width="60px" runat="server" Text="Show" />
                             <asp:RadioButton ID="rdoHideInclusionDetail" GroupName="grpInclusionDetail" runat="server" Text="Hide" />
                        </td>
                    </tr>
                    <tr>
                            <td>
                                <asp:Label ID="lblItemClassification" runat="server" Text="Item Classification"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButton ID="rdoShowItemClassification" GroupName="grpItemClassification" Width="60px" runat="server" Text="Show" />
                             <asp:RadioButton ID="rdoHideItemClassification" GroupName="grpItemClassification" runat="server" Text="Hide" />
                        </td>
                    </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblItemPromotion" runat="server" Text="Item Promotion"></asp:Label>
                            </td>
                            <td>
                                <asp:RadioButton ID="rdShowItemPromotion" GroupName="grpItemPromotion" Width="60px" runat="server" Text="Show" />
                                <asp:RadioButton ID="rdHideItemPromotion" GroupName="grpItemPromotion" runat="server" Text="Hide" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-sm-6">
                    <table>
                        <tr>
                            <td>
                                <asp:Label ID="lblApplyPromotionCode" runat="server" Text="Apply Promotion Code"></asp:Label>
                            </td>
                            <td>
                                <asp:RadioButton ID="rbSHowApplyPromotionCode" GroupName="grpApplyPromotionCode" Width="60px" runat="server" Text="Show" />
                                <asp:RadioButton ID="rbHideApplyPromotionCode" GroupName="grpApplyPromotionCode" runat="server" Text="Hide" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblPay" runat="server" Text="Pay"></asp:Label>
                            </td>
                            <td>
                                <asp:RadioButton ID="rbShowPay" GroupName="grpPay" Width="60px" runat="server" Text="Show" />
                                <asp:RadioButton ID="rbHidePay" GroupName="grpPay" runat="server" Text="Hide" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblPOSPay" runat="server" Text="POS Pay"></asp:Label>
                            </td>
                            <td>
                                <asp:RadioButton ID="rbShowPOSPay" GroupName="grpPOSPay" Width="60px" runat="server" Text="Show" />
                                <asp:RadioButton ID="rbHidePOSPay" GroupName="grpPOSPay" runat="server" Text="Hide" />
                            </td>
                        </tr>
                </table>
                    <br />
                    <br />
                    <div class="col-md-12 line">
                        <span><strong>New PO form grid section</strong></span>
                    </div>
                  <table>
                      <tr>
                            <td>
                                <asp:Label ID="Label1" runat="server" Text="Item ID"></asp:Label>
                        </td>
                        <td>
                                <asp:RadioButton ID="rdoShowItemIDPO" GroupName="grpItemId" Width="60px" runat="server" Text="Show" Checked="true" />
                             <asp:RadioButton ID="rdoHideItemIDPO" GroupName="grpItemId" runat="server" Text="Hide" />
                        </td>
                    </tr>
                    <tr>
                        <td>    
                            <asp:Label ID="Label6" runat="server" Text="Item"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButton ID="rdoShowItemPO" GroupName="grpItem" Width="60px" runat="server" Text="Show" Checked="true" />
                             <asp:RadioButton ID="rdoHideItemPO" GroupName="grpItem" runat="server" Text="Hide" />
                        </td>
                    </tr>
                    <tr>
                        <td>    
                            <asp:Label ID="Label7" runat="server" Text="Description"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButton ID="rdoShowDescriptionPO" GroupName="grpDescription" Width="60px" runat="server" Text="Show" Checked="true" />
                             <asp:RadioButton ID="rdoHideDescriptionPO" GroupName="grpDescription" runat="server" Text="Hide" />
                        </td>
                    </tr>
                    <tr>
                        <td>    
                            <asp:Label ID="Label2" runat="server" Text="SKU"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButton ID="rdoShowSKUPO" GroupName="grpSKU" Width="60px" runat="server" Text="Show" Checked="true" />
                             <asp:RadioButton ID="rdoHideSKUPO" GroupName="grpSKU" runat="server" Text="Hide" />
                        </td>
                    </tr>
                     <tr>
                            <td>
                                <asp:Label ID="Label5" runat="server" Text="Warehouse"></asp:Label>
                        </td>
                        <td>
                            <asp:RadioButton ID="rdoShowWarehousePO" GroupName="grpWarehouse" Width="60px" runat="server" Text="Show" Checked="true" />
                             <asp:RadioButton ID="rdoHideWarehousePO" GroupName="grpWarehouse" runat="server" Text="Hide" />
                        </td>
                    </tr>
                  </table>
            </div>

            </div>
                </div>
            
            <div class="cont1">
                <div class="col-md-12">
                    <asp:CheckBox ID="chkDisplayItemDetails" Checked="true" runat="server" Text="Display Item Details (Not checking this will remove section from the form)." />
                </div>
            <div id="divItemDetail" runat="server" style="clear: both; padding-top: 10px; height: auto" />
            </div>
           <%-- <div class="cont1">
            <div class="col-md-12">
                <span><strong>Section 6:</strong></span>
            </div>
            <div class="col-md-12">
               Select the columns you want to display for items selected (Max 4)
            </div>
            <div class="col-md-12">
                <div id="divItemColumnSetting" runat="server" style="clear: both; padding-top: 10px; height: auto">
                </div>
            </div>
            </div>--%>
            <div class="cont2">
            <div class="col-md-12 line">
               <span><strong>Payments, Terms, & Shipping Section</strong></span> 
            </div>
            <div class="col-md-12">
               <asp:CheckBox ID="chkDisplayShippingCluster" runat="server" Checked="true" Text="Display shipping rates and coupon code field cluster." />
            </div>
            <div class="col-md-12">
               <asp:CheckBox ID="chkPaymentMethods" runat="server" Text="Display the following methods of payment buttons." />
            </div>
            <div class="col-md-12">
               <asp:CheckBoxList ID="chklPaymentMethods" runat="server" RepeatDirection="Horizontal" RepeatColumns="4">
                            </asp:CheckBoxList>
            </div>
            <div class="col-md-12">
               <asp:CheckBox ID="chkBizDoc" runat="server" Text="After paying in full, automatically create / open the following BizDoc" />
            
               <asp:DropDownList ID="ddlBizDoc" runat="server"></asp:DropDownList>
            </div>
                </div>
            <div class="cont1">
            <%-- <div class="col-md-12">
               <span><strong>Section 8:</strong></span>
            </div>--%>
             <div class="col-md-12">
               <asp:CheckBox ID="chkDisplyCouponDiscount" runat="server" Checked="true" Text="Display Coupon Discount" />
            </div>
             <div class="col-md-12">
                 <ul class="list-inline">
                    <li>
                        <asp:CheckBox ID="chkDisplayShippingCharges" runat="server" Checked="true" Text="Display Shipping Charges" /></li>
                     <li></li>
                    <li>
                        <asp:CheckBox ID="chkDisplayShipVia" runat="server" Checked="true" Text="Display Ship Via" /></li>
                 </ul>
            </div>
             <div class="col-md-12">
               <asp:CheckBox ID="chkDisplayDiscount" runat="server" Checked="true" Text="Display Discount" />
            </div>
            
                </div>
        </div>
        <%--<div class="container customer-detail">
            <div class="grid grid-pad">--%>
               <%-- <div class="cont2">
                    <div style="width: 100%; text-align: left">
                        
                    </div>
                    <div style="width: 100%; text-align: left; margin-top: 10px;">
                        <div>
                            
                        </div>
                        <div>
                            
                        </div>
                    </div>
                </div>--%>
               <%-- <div class="cont1">
                    <div style="width: 100%; text-align: left">
                        
                        <div style="padding-top: 10px">
                            <div>
                                
                            </div>
                            <div>
                                
                            </div>
                            <div>
                                
                            </div>
                            <div>
                                
                            </div>
                            <div>
                                
                            </div>
                            <div style="padding-left: 5px; padding-top: 3px;">
                                
                            </div>
                        </div>
                    </div>
                </div>--%>
               <%-- <div class="cont2">
                    <div style="width: 100%; text-align: left">
                        
                    </div>
                    <div style="width: 100%; text-align: left; margin-top: 10px;">
                        <div>
                            
                        </div>
                        <div style="font-weight: bold; margin-top: 10px">
                            
                        </div>
                        
                        <div style="font-weight: bold; margin-top: 10px">
                            
                        </div>
                        
                    </div>
                </div>--%>
               <%-- <div class="cont1">
                    <div style="width: 100%; text-align: left">
                        
                    </div>
                    <div style="width: 100%; text-align: left; margin-top: 10px;">
                        <div>
                            
                        </div>
                        
                    </div>
                </div>--%>
                <%--<div class="cont2">
                    <div style="width: 100%; text-align: left">
                        
                    </div>
                    <div style="width: 100%; text-align: left; margin-top: 10px;">
                         
                    </div>
                    <div style="width: 100%; text-align: left; margin-top: 10px;">
                        <div>
                            
                        </div>
                        <div>
                            
                        </div>
                        <div>
                            
                        </div>
                    </div>
                </div>--%>
                <%--<div class="cont1">
                    <div style="width: 100%; text-align: left">
                        
                    </div>
                    <div style="width: 100%; text-align: left; padding-top: 10px;">
                        <div>
                            
                        </div>
                        
                    </div>
                </div>--%>
                <%--<div class="cont2">
                    <div style="width: 100%; text-align: left">
                        
                    </div>
                    <div style="width: 100%; text-align: left; margin-top: 10px;">
                        <div>
                            
                        </div>
                        <div>
                            
                        </div>
                        <div style="padding-left: 15px">
                            
                        </div>
                        <div>
                            <div style="float: left">
                                
                            </div>
                            <div style="float: left; padding-left: 10px; vertical-align: bottom">
                                
                            </div>
                        </div>
                    </div>
                </div>--%>
                <%--<div class="cont1">
                    <div style="width: 100%; text-align: left">
                        
                    </div>
                    <div>
                        <div style="width: 200px; text-align: left; margin-top: 10px; float: left">
                            <div>
                                
                            </div>
                            <div>
                                
                            </div>
                            <div>
                                
                            </div>
                        </div>
                        <div style="width: 200px; text-align: left; margin-top: 10px; float: left">
                            <div>
                                
                            </div>
                            <div>
                                
                            </div>
                            
                        </div>
                    </div>
                </div>--%>
            <%--</div>
        </div>--%>
          
        <asp:Literal Text="" runat="server" ID="litScript" />
        <asp:HiddenField ID="hfOrderDetail" runat="server" />
        <asp:HiddenField ID="hfCustomerDetail" runat="server" />
        <asp:HiddenField ID="hfSalesConfigurationID" runat="server" />
        <asp:HiddenField ID="hfItemDetail" runat="server" />
        <asp:HiddenField ID="hfItemColumn" runat="server" />
        <asp:HiddenField ID="hfchkReleaseDate" runat="server" />
        <asp:TextBox runat="server" ID="rows" Style="display: none" Text="2"></asp:TextBox>
        <script type="text/javascript">
            // select all desired input fields and attach tooltips to them
            $("#form1").find("span.tip").tooltip({

                // place tooltip on the right edge
                position: "center right",

                // a little tweaking of the position
                offset: [-2, 10],

                // use the built-in fadeIn/fadeOut effect
                effect: "fade",

                // custom opacity setting
                opacity: 0.8

            });
    </script>
        </asp:Content>
      

