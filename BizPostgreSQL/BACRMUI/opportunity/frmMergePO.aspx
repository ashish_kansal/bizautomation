﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmMergePO.aspx.vb" Inherits=".frmMergePO" %>

<%@ Register Assembly="Telerik.Web.UI, Version=2011.2.712.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
    <script type="text/javascript">
        function ConfirmStopReporting() {
            var selectedItems = 0
            var dataItems = $find('<%=RadGridPO.ClientID%>').get_masterTableView().get_dataItems();
            for (var i = 0; i < dataItems.length; i++) {
                if (dataItems[i].get_nestedViews().length > 0) {
                    for (var j = 0; j < dataItems[i].get_nestedViews().length ; j++) {
                        for (var k = 0; k < dataItems[i].get_nestedViews()[j].get_dataItems().length; k++) {
                            var chkSelect = dataItems[i].get_nestedViews()[j].get_dataItems()[k].findElement("chkSelect");
                            if (chkSelect.checked) {
                                selectedItems = selectedItems + 1
                            }
                        }
                    }
                }
            }

            if (selectedItems > 0) {
                if (confirm("BizAutomation will stop reporting selected POs as available for merging. Do you want to proceed?")) {
                    return true;
                } else {
                    return false;
                }
            } else {
                alert("Select PO");
                return false;
            }
        }

        function validate() {
            var isvalid = true;
            var vendors = [];

            var dataItems = $find('<%=RadGridPO.ClientID%>').get_masterTableView().get_dataItems();
            for (var i = 0; i < dataItems.length; i++) {
                var vendor = { ID: 0, SelectedPOs: [], SelectedPOCount: 0, isMasterPOSelected: false, MasterPOID: 0 };
                vendor.ID = dataItems[i].getDataKeyValue("VendorID");

                if (dataItems[i].get_nestedViews().length > 0) {
                    for (var j = 0; j < dataItems[i].get_nestedViews().length ; j++) {
                        for (var k = 0; k < dataItems[i].get_nestedViews()[j].get_dataItems().length; k++) {
                            var poID = dataItems[i].get_nestedViews()[j].get_dataItems()[k].getDataKeyValue("POID");

                            var chkSelect = dataItems[i].get_nestedViews()[j].get_dataItems()[k].findElement("chkSelect");
                            var rdbParent = dataItems[i].get_nestedViews()[j].get_dataItems()[k].findElement("rdbParent");

                            if (chkSelect.checked) {
                                vendor.SelectedPOs.push(poID);
                                vendor.SelectedPOCount = vendor.SelectedPOCount + 1;
                            }

                            if (rdbParent.checked) {
                                vendor.isMasterPOSelected = true;
                                vendor.MasterPOID = poID;
                            }
                        }
                    }
                }

                vendors.push(vendor);
            }

            if (vendors.length > 0) {

                var noRecordsSelected = true;

                for (var i = 0; i < vendors.length; i++) {
                    if (vendors[i].SelectedPOCount > 0 || vendors[i].isMasterPOSelected) {
                        noRecordsSelected = false;
                    }

                    if (vendors[i].SelectedPOCount == 1) {
                        isvalid = false;
                    } else if (vendors[i].SelectedPOCount > 1 && !vendors[i].isMasterPOSelected) {
                        isvalid = false;
                    } else if (vendors[i].isMasterPOSelected && vendors[i].SelectedPOCount < 2) {
                        isvalid = false;
                    }
                }

                if (noRecordsSelected) {
                    alert("Please select purchase orders to merge.");
                    return false;
                } else {
                    if (isvalid) {
                        if (confirm('The selected POs will merge their line items into their respective Master Records but everything else including those PO records will be deleted. Items with same ID and warehouse (if any) will be merged to the master line item but only the qty will increase (nothing else from source line item will carry over). Do you want to proceed?')) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        alert("Records are not properly selected. Please make sure atleast 2 purchase orders and 1 master purchase order are selected in vendor group.");
                        return false;
                    }
                }

            } else {
                alert("Please select purchase orders to merge.");
                return false;
            }

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; background-color: #FFF; text-align: center; width: 200px; padding: 20px">
                    <img src="../images/PlUploadImages/throbber.gif" alt="" /><br />
                    <strong>Processing Request</strong>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true">
        <ContentTemplate>
            <div style="width: 100%; text-align: right;">

                <asp:Button ID="btnStop" runat="server" Text="Stop Reporting Selected Records" CssClass="ImageButton" Style="padding-left: 5px !important" OnClientClick="return ConfirmStopReporting();" />
                <asp:Button ID="btnMerge" runat="server" Text="Merge Selected Records" CssClass="ImageButton" Style="padding-left: 5px !important" OnClientClick="return validate();" />
                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="ImageButton" Style="padding-left: 5px !important" OnClientClick="Close();" />

            </div>
            <div style="text-align: center; color: red; width: 100%;">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Merge Purchase Order
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <table style="width: 980px; height: 560px;">
        <tr>
            <td style="vertical-align: top;">
                <asp:UpdatePanel ID="UpdatePanelGrid" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true">
                    <ContentTemplate>
                        <telerik:RadGrid ID="RadGridPO" runat="server" AutoGenerateColumns="false" Skin="Simple" Width="100%" AllowMultiRowSelection="false" ClientSettings-Selecting-AllowRowSelect="false">
                            <MasterTableView HierarchyDefaultExpanded="true" HierarchyLoadMode="Client" ClientDataKeyNames="VendorID" DataKeyNames="VendorID" Name="Vendors">
                                <HeaderStyle Font-Bold="true" HorizontalAlign="Center" />
                                <Columns>
                                    <telerik:GridBoundColumn HeaderText="Vendor" DataField="Vendor" ItemStyle-Font-Size="16px"></telerik:GridBoundColumn>
                                </Columns>
                                <DetailTables>
                                    <telerik:GridTableView DataKeyNames="POID" Name="Orders" ClientDataKeyNames="POID">
                                        <ItemStyle Font-Size="14px" Font-Bold="false" />
                                        <AlternatingItemStyle Font-Size="14px" Font-Bold="false" />
                                        <HeaderStyle Font-Bold="true" HorizontalAlign="Center" />
                                        <ParentTableRelation>
                                            <telerik:GridRelationFields DetailKeyField="VendorID" MasterKeyField="VendorID"></telerik:GridRelationFields>
                                        </ParentTableRelation>
                                        <Columns>
                                            <telerik:GridBoundColumn HeaderText="PO Name" DataField="POName"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn HeaderText="PO Status" DataField="POStatus" ItemStyle-Width="100" ItemStyle-HorizontalAlign="Center"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn HeaderText="Date PO was created" DataField="CreatedDate" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="140"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn HeaderText="Estimated Ship Date" DataField="EstimatedCloseDate" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="140"></telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn ItemStyle-Width="115" ItemStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>
                                                    Select POs to merge
                                            <br />
                                                    <asp:CheckBox ID="chkSelectAll" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelectAll_CheckedChanged" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkSelect" runat="server" AutoPostBack="true" OnCheckedChanged="chkSelect_CheckedChanged" />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridTemplateColumn ItemStyle-Width="100" ItemStyle-HorizontalAlign="Center">
                                                <HeaderTemplate>
                                                    Select Master PO 
                                            <br />
                                                    to merge into
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:RadioButton ID="rdbParent" runat="server" AutoPostBack="true" OnCheckedChanged="rdbParent_CheckedChanged" Enabled="false" />
                                                </ItemTemplate>
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <DetailTables>
                                            <telerik:GridTableView DataKeyNames="POID,numItemCode" Name="Items">
                                                <ParentTableRelation>
                                                    <telerik:GridRelationFields DetailKeyField="POID" MasterKeyField="POID"></telerik:GridRelationFields>
                                                </ParentTableRelation>
                                                <HeaderStyle Font-Bold="true" HorizontalAlign="Center" />
                                                <Columns>
                                                    <telerik:GridBoundColumn HeaderText="Item Name" DataField="vcItemName"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn HeaderText="Model ID" DataField="vcModelID" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="295"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn HeaderText="Qty Ordered" DataField="numUnitHour" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="115"></telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn HeaderText="Min Order Qty" DataField="intMinQty" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="99"></telerik:GridBoundColumn>
                                                </Columns>
                                            </telerik:GridTableView>
                                        </DetailTables>
                                    </telerik:GridTableView>
                                </DetailTables>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </td>
        </tr>
    </table>
</asp:Content>
