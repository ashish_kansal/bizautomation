﻿Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Opportunities
    Public Class frmMassPurchaseFulfillment
        Inherits BACRMPage

#Region "Page Events"
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not Page.IsPostBack Then
                    Dim queryViewId As Int16
                    queryViewId = CCommon.ToInteger(GetQueryStringVal("ViewId"))
                    If queryViewId > 0 Then
                        hdnViewID.Value = queryViewId
                    End If
                    hdnMSWarehouseID.Value = CCommon.ToLong(Session("DefaultWarehouse"))
                    hdnMSImagePath.Value = CCommon.GetDocumentPath(Session("DomainID"))
                    hdnDecimalPoints.Value = CCommon.ToShort(Session("DecimalPoints"))
                    If GetQueryStringVal("View") <> "" Then
                        hdnViewID.Value = GetQueryStringVal("View")
                    End If
                    If GetQueryStringVal("ShowBilled") = "True" And GetQueryStringVal("View") = "1" Then
                        chkshowrecords.Checked = GetQueryStringVal("ShowBilled")
                    ElseIf GetQueryStringVal("ShowReceivedButNotBilled") = "2" And GetQueryStringVal("View") = "3" Then
                        hdnFlag.value = GetQueryStringVal("ShowReceivedButNotBilled")
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DirectCast(Page.Master, BizMaster).DisplayError(ex.Message)
            End Try
        End Sub
#End Region

    End Class
End Namespace