﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmShippingBox.aspx.vb"
    Inherits=".frmShippingBox" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../css/lists.css" type="text/css" />
    <style type="text/css">
        .box
        {
            float: left;
            vertical-align: bottom;
            width: 200px;
            height: auto;
            padding: 5px;
            padding-bottom: 0;
            border: 2px solid #333;
            -moz-border-radius: 10px;
            -webkit-border-radius: 10px;
            border-radius: 10px;
            margin: 0 0 10px 10px;
            background: #fff;
        }

        ul
        {
            list-style-type: none;
            padding: 4px 4px 0 4px;
            margin: 0px;
            width: 18em;
            font-size: 11px;
            font-family: Tahoma, Verdana, Arial, Helvetica;
            border: 1px solid green;
        }

        li
        {
            cursor: move;
            margin-bottom: 4px;
            padding: 2px 2px;
            border: 1px solid #A3B5CE;
            text-align: center;
            background: #A3B5CE;
            color: White;
        }

        .positive .dimension
        {
            font-family: Arial;
            font-size: 8pt;
            color: Black;
            vertical-align: bottom;
        }

        .dimension
        {
            float: right;
            vertical-align: middle;
            display: block;
        }

            .dimension input
            {
                font-family: Arial;
                font-size: 8pt;
                color: Black;
            }

        .BoxHolder
        {
            width: 800px;
            border: 1px solid blue;
            display: block;
            min-height: 900px;
            padding: 10px;
            background: url(../images/boxSet.png);
            background-repeat: no-repeat;
            background-position-y: bottom;
            background-position-x: right;
        }

        .boxheader
        {
            display: block;
            width: 200px;
            font-weight: bold;
            color: gray;
            font-size: 12px;
            height: 40px;
        }

        .placeholder
        {
            display: block;
            width: 200px;
            float: left;
            text-align: center;
        }

        .boxitems
        {
            border: 1px dotted gray;
            display: block;
            height: 105px;
            list-style-type: none;
            overflow-y: scroll;
            overflow-x: hidden;
        }

        #AvailItems
        {
            min-height: 400px;
        }

        .delete
        {
            background: url(../images/package_delete.png);
            background-position: right;
            background-repeat: no-repeat;
            float: right;
            display: block;
            width: 30px;
            height: 15px;
            text-align: left;
            cursor: pointer;
        }

        .deleteItem
        {
            background: url(../images/deleted.gif);
            background-position: right;
            background-repeat: no-repeat;
            float: right;
            display: block;
            width: 30px;
            height: 15px;
            text-align: left;
            cursor: pointer;
        }

        .itemQty
        {
            width: 20px;
            height: 12px !important;
            float: right;
            display: block;
            text-align: right;
        }

        .AddedItemQty
        {
            font-weight: bold;
            float: right;
        }

        .BoxWeight
        {
            color: #555;
            text-align: center;
            background-color: #EEE;
            float: left;
            font-weight: bold;
            padding: 4px;
            padding-bottom: 5px;
            margin-right: 3px;
            width: 22px;
            height: 15px;
            text-decoration: none;
            font-size: x-small;
        }
    </style>
    <link rel="stylesheet" href="http://jqueryui.com/demos/demos.css">
    <style>
        .demo ul
        {
            list-style-type: none;
            margin: 0;
            padding: 0;
            margin-bottom: 10px;
            float: left;
        }

        .demo li
        {
            margin: 5px;
            padding: 5px;
            width: 150px;
        }

        .ui-sortable
        {
            border: 1px solid gray;
            width: 189px;
        }
    </style>
    <script src="../JavaScript/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>
    <script language="JavaScript" type="text/javascript" src="../javascript/jquery.numeric.js"></script>
    <script language="JavaScript" type="text/javascript">

        function ItemDimension(numOppBizDocItemID, height, width, length, weight, count) {
            this.numOppBizDocItemID = numOppBizDocItemID;
            this.height = height;
            this.width = width;
            this.length = length;
            this.weight = weight;
            this.count = count;
        }

        function SetItemDimension(numOppBizDocItemID, BoxID) {
            var DropedItemsCount = 0//$('li[itemid="' + numOppBizDocItemID + '"]').length - 1;
            //            //console.log($('li[itemid="' + numOppBizDocItemID + '"]'))
            DropedItemsCount = GetDroppedItemCount(numOppBizDocItemID);
            //console.log('total count');
            //console.log(DropedItemsCount);


            //console.log($('li[itemid="' + numOppBizDocItemID + '"]').length - 1);

            var ItemDim = GetItemDimension(numOppBizDocItemID)
            //console.log(Number(ItemDim.count));
            if (Number(ItemDim.count) <= DropedItemsCount) {
                //console.log('Items over');
                $("#AvailItems").find('li[itemid="' + numOppBizDocItemID + '"]').remove();
            }
            else {
                //console.log('Items not over');
            }

            if ($("#" + BoxID).find("li").length == 1) {
                //                    $(BoxObject).find("input").keypress(function () { CheckNumber(1, event); });
                $("#" + BoxID).find("#length").val(ItemDim.length);
                $("#" + BoxID).find("#width").val(ItemDim.width);
                $("#" + BoxID).find("#height").val(ItemDim.height);
            }
            //console.log(ItemDim.weight);
            //console.log(Number($("#" + BoxID).find(".BoxWeight").text()));
            $("#" + BoxID).find(".BoxWeight").text(Number($("#" + BoxID).find(".BoxWeight").text()) + Number(ItemDim.weight) * DropedItemsCount);

        }

        // Search for given items's dimentions by BizDocItemID, return array item
        function GetItemDimension(numOppBizDocItemID) {
            for (var i = 0; i < arrItemDim.length; i++) {
                var ItemDim = arrItemDim[i];
                if (ItemDim.numOppBizDocItemID == numOppBizDocItemID) {
                    return ItemDim;
                }
            }
        }

        function GetDroppedItemCount(numOppBizDocItemID) {
            var DropedItemsCount = 0;
            $('.BoxHolder li[itemid="' + numOppBizDocItemID + '"] .AddedItemQty').each(function () {
                DropedItemsCount = DropedItemsCount + parseInt($(this).text());
            });
            return DropedItemsCount;
        }

        var ItemID;
        $(document).ready(function () {
            $(".delete").click(function () {
                var box = $(this).parent().parent();
                $(box).remove();
            })
            doSort();

            makeDraggable();

            $("#spnAddBox").click(function () {
                CreateBox(1);
            });

            $('#ddlPackageType').change(function () {
                CreateBox(2);
            });

            // Create One box by default
            //CreateBox(0);

            $(".deleteItem").live("click", function () {
                var Item = $(this).parent();
                //console.log(Item.attr("itemid"));
                ItemID = Item.attr("itemid");
                //item is not avail..add new item element
                if ($("#AvailItems").find('li[itemid="' + ItemID + '"]').length == 0) {
                    $(Item).find(".deleteItem").remove();
                    $(Item).find(".AddedItemQty").remove();

                    //console.log(Number(GetItemDimension(ItemID).count));
                    //console.log(Number(GetDroppedItemCount(ItemID)));
                    $(Item).append("<input class='itemQty' type='text' value='" + (Number(GetItemDimension(ItemID).count) - Number(GetDroppedItemCount(ItemID))) + "'>");
                    $("#AvailItems").append(Item);
                    makeDraggable();
                }
                else if ($("#AvailItems").find('li[itemid="' + ItemID + '"]').length == 1) {
                    //item already exist just update quantity.
                    var ExistingItem = $("#AvailItems").find('li[itemid="' + ItemID + '"]');
                    Item.remove();
                    //console.log(Number(GetItemDimension(ItemID).count));
                    //console.log(Number(GetDroppedItemCount(ItemID)));
                    $(ExistingItem).find(".itemQty").val((Number(GetItemDimension(ItemID).count) - Number(GetDroppedItemCount(ItemID))));
                }
                else
                    Item.remove();
            });

            //            $("ul, li").disableSelection(); //Commented by chintan, Reason: Firefox disables editing of item qty.
            $(".itemQty").live("keyup", function () {
                var CurrentItemID = $(this).parent().attr("itemid");
                var QtyLeftToDrop = GetItemDimension(CurrentItemID).count - GetDroppedItemCount(CurrentItemID);
                //                console.log(QtyLeftToDrop);
                //                console.log($(this).val());

                if (Number($(this).val()) > GetItemDimension(CurrentItemID).count) {
                    alert('can not enter value higher than ordered qty.');
                    $(this).val(QtyLeftToDrop);
                    //console.log('PIN 2');
                } else if (Number($(this).val()) > QtyLeftToDrop) {
                    alert('can not enter value higher than left qty.');
                    $(this).val(QtyLeftToDrop);
                    //console.log('PIN 3');
                }

            });

            $("#btnSave").click(function (event) {
                var strValues = "";
                $(".BoxHolder .box").each(function (index1) {
                    var box = $(this);

                    //if Fed Ex then do validation
                    if ($("#hdnShippingCompanyID").val() == "91") {

                        if (isNaN(Number($(box).find("#length").val())) == true || Number($(box).find("#length").val()) == 0) {
                            alert("Please enter length of box");
                            event.preventDefault();
                            $(box).find("#length").focus();
                            return false;
                        }
                        if (isNaN(Number($(box).find("#width").val())) == true || Number($(box).find("#width").val()) == 0) {
                            alert("Please enter width of box");
                            event.preventDefault();
                            $(box).find("#width").focus();
                            return false;
                        }
                        if (isNaN(Number($(box).find("#height").val())) == true || Number($(box).find("#height").val()) == 0) {
                            alert("Please enter height of box");
                            event.preventDefault();
                            $(box).find("#height").focus();
                            return false;
                        }
                    }

                    //strValues = strValues + $(box).attr("id") + ":" + $(box).find("#length").val() + ":" + $(box).find("#width").val() + ":" + $(box).find("#height").val() + ":";
                    strValues = strValues + $(box).attr("id") + ":" + $(box).find("#length").val() + ":" + $(box).find("#width").val() + ":" + $(box).find("#height").val() + ":" + $(box).find("#lblPackID").text() + ":" + $(box).find(".BoxWeight").text() + ":";
                    $(box).find("li").each(function (index) {
                        strValues = strValues + $(this).find(".AddedItemQty").text() + "~" + $(this).attr("itemid") + ':';
                        ////console.log($(box).attr("id") + ": " + $(this).attr("id") + ': ' + $(this).text());
                    });
                    strValues = strValues + ',';
                });
                //console.log(strValues);
                $("#hdnValues").val(strValues);
                //                event.preventDefault();

            });


        });

        function doSort(mode) {
            //console.log(mode);
            if (mode == 0) {
                $(".ui-sortable").sortable({
                    revert: true,
                    activate: function (event, ui) { $(this).find(".placeholder").remove(); },
                    receive: function (event, ui) { ItemID = $(ui.item).attr("id"); },
                    update: function (event, ui) {
                        $(ui.item).find(".deleteItem").remove();

                        $(ui.item).append("<div class='deleteItem'></div><div class='AddedItemQty'>" + $(ui.item).find(".itemQty").val() + "</div>");

                        //console.log($(ui.item).attr("itemid"));
                        //console.log('$(this).parent().attr("id"):' + $(this).parent().attr("id"));

                        SetItemDimension($(ui.item).attr("itemid"), $(this).parent().attr("id"));
                        ItemID = $(ui.item).attr("itemid");

                        //                    console.log(ItemID);
                        //                    console.log($("#AvailItems>li[itemid='" + ItemID + "'] .itemQty"));
                        //                    console.log(GetItemDimension(ItemID).count);
                        //                    console.log($(ui.item).find(".itemQty").val());
                        //                    console.log($("#AvailItems>li[itemid='" + ItemID + "'] .itemQty").val());
                        //console.log(GetDroppedItemCount(ItemID));

                        $("#AvailItems>li[itemid='" + ItemID + "'] .itemQty").val(Number(GetItemDimension(ItemID).count) - Number(GetDroppedItemCount(ItemID)));
                        $(ui.item).find(".itemQty").remove();
                    }
                });
            }
            else {
                $(".ui-sortable").sortable({
                    revert: true,
                    activate: function (event, ui) { $(this).find(".placeholder").remove(); },
                    receive: function (event, ui) { ItemID = $(ui.item).attr("id"); },
                    update: function (event, ui) {
                        $(ui.item).find(".deleteItem").remove();
                        $(ui.item).append("<div class='deleteItem'></div><div class='AddedItemQty'>" + $(ui.item).find(".itemQty").val() + "</div>");
                        //SetItemDimension($(ui.item).attr("itemid"), $(this).parent().attr("id"));
                        ItemID = $(ui.item).attr("itemid");
                        $("#AvailItems>li[itemid='" + ItemID + "'] .itemQty").val(Number(GetItemDimension(ItemID).count) - Number(GetDroppedItemCount(ItemID)));
                        $(ui.item).find(".itemQty").remove();
                    }
                });
            }

        }

        function makeDraggable() {
            $("#AvailItems>li").draggable({
                connectToSortable: ".ui-sortable",
                helper: "clone",
                revert: "invalid"
            })
        }
        
        function CreateBox(mode) {
            
            if (mode == 0 || mode == 1) {
                    var BoxNo = (Number($("#hdnNoOfBox").val()) + 1);
                    $('<div class="box" id="Box' + BoxNo + '"><div class="boxheader">Box' + BoxNo + ' <div class="delete"></div> <div class="dimension"><span class="BoxWeight"></span> L<input type="text" id="length" class="positive" style="width:30px"> x W<input type="text" id="width" class="positive" style="width:30px"> x H<input type="text" id="height" class="positive" style="width:30px">  </div> </div> <ul class="ui-sortable boxitems"><div class="placeholder">Drop items here &darr;</div> </ul>  </div>').appendTo(".BoxHolder")

                    //console.log('Item mode:' + mode);
                    doSort(mode);
                    $("#hdnNoOfBox").val(Number($("#hdnNoOfBox").val()) + 1)
                    $(".positive").numeric({ negative: false }, function () { alert("No negative values"); this.value = ""; this.focus(); });
                    $(".delete").click(function (event) {
                        var box = $(this).parent().parent();
                        $(box).remove();
                    })
            }
            else if (mode == 2) {
                if ($('#ddlPackageType option:selected').text() != 'None') {
                    var height;
                    var width;
                    var length;
                    var weight;
                    var packageTypeID;
                    var strValue = $('#ddlPackageType option:selected').val().split(",");

                    packageTypeID = strValue[0];
                    width = strValue[1];
                    height = strValue[2];
                    length = strValue[3];
                    weight = strValue[4];

                    var BoxNo = (Number($("#hdnNoOfBox").val()) + 1);
                    $('<div class="box" id="Box' + BoxNo + ' - ' + $('#ddlPackageType option:selected').text() + '"><div class="boxheader" >Box' + BoxNo + ' : ' + $('#ddlPackageType option:selected').text() + '<span id="lblPackID" style="visibility:hidden">' + packageTypeID + '</span>'
                    + ' <div class="delete"></div> <div class="dimension"><span class="BoxWeight">' + parseFloat(weight).toFixed(1) + '</span>'
                    + 'L<input type="text" id="length" value=' + parseFloat(length).toFixed(1) + ' class="positive" style="width:30px"> x '
                    + 'W<input type="text" id="width" value=' + parseFloat(width).toFixed(1) + ' class="positive" style="width:30px"> x '
                    + 'H<input type="text" id="height" value=' + parseFloat(height).toFixed(1) + ' class="positive" style="width:30px">'
                    + '</div> </div> <ul class="ui-sortable boxitems"><div class="placeholder">Drop items here &darr;</div> </ul>  </div>').appendTo(".BoxHolder")

                    doSort(mode);
                    $("#hdnNoOfBox").val(Number($("#hdnNoOfBox").val()) + 1)
                    $(".positive").numeric({ negative: false }, function () { alert("No negative values"); this.value = ""; this.focus(); });
                    $(".delete").click(function (event) {
                        var box = $(this).parent().parent();
                        $(box).remove();
                    })
                }
            }
            else if (mode == 3) {
                var BoxNo = (Number($("#hdnNoOfBox").val()) + 1);
                $('<div class="box" id="Box' + BoxNo + '"><div class="boxheader">Box' + BoxNo + ' <div class="delete"></div> <div class="dimension"><span class="BoxWeight"></span> L<input type="text" id="length" class="positive" style="width:30px"> x W<input type="text" id="width" class="positive" style="width:30px"> x H<input type="text" id="height" class="positive" style="width:30px">  </div> </div> <ul class="ui-sortable boxitems"><div class="placeholder">Drop items here &darr;</div> </ul>  </div>').appendTo(".BoxHolder")

                $("#hdnNoOfBox").val(Number($("#hdnNoOfBox").val()) + 1)
                $(".positive").numeric({ negative: false }, function () { alert("No negative values"); this.value = ""; this.focus(); });
                $(".delete").click(function (event) {
                    var box = $(this).parent().parent();
                    $(box).remove();
                })
            }
        }

        function OpenShippingReport(a, b, c, mode) {
            this.close();
            window.open('../opportunity/frmShippingReport.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&mode=1&OppBizDocId=' + a + '&ShipReportId=' + b + '&OppId=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=1000,height=300,scrollbars=yes,resizable=yes');
            //if (mode == 0) {
            //    window.open('../opportunity/frmShippingReport.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&mode=0&OppBizDocId=' + a + '&ShipReportId=' + b + '&OppId=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=800,height=300,scrollbars=yes,resizable=yes');
            //}
            //else {
            //    window.open('../opportunity/frmShippingReport.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&mode=1&OppBizDocId=' + a + '&ShipReportId=' + b + '&OppId=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=800,height=300,scrollbars=yes,resizable=yes');
            //}

        }

        //$(function () {
        //    //alert(1);
        //    $("#AvailItems li").draggable();
        //    $("#AvailItems li").droppable({
        //        drop: function (event, ui) {
        //            //layerDrop(ui);
        //            $(ui.item).find(".deleteItem").remove();

        //            console.log('$(ui.item).attr("itemid"): ' + $(ui.item).attr("itemid"));
        //            $(ui.item).append("<div class='deleteItem'></div><div class='AddedItemQty'>" + $(ui.item).find(".itemQty").val() + "</div>");

        //            console.log('$(this).parent().attr("id"):' + $(this).parent().attr("id"));

        //            SetItemDimension($(ui.item).attr("itemid"), $(this).parent().attr("id"));
        //            ItemID = $(ui.item).attr("itemid");

        //            $("#AvailItems>li[itemid='" + ItemID + "'] .itemQty").val(Number(GetItemDimension(ItemID).count) - Number(GetDroppedItemCount(ItemID)));
        //            $(ui.item).find(".itemQty").remove();

        //        }
        //    });
        //});

       
    </script>
    <style>
        .badge {
    display: inline-block;
    min-width: 10px;
    padding: 3px 7px;
    font-size: 12px;
    font-weight: 700;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    background-color: #777;
    border-radius: 10px;
}.bg-yellow{
         background-color: #f39c12 !important;
 }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnGenerateShippingLabel" runat="server" Text="Generate Shipping Labels & Tracking#s" CssClass="button"></asp:Button>
            <asp:Button ID="btnSaveClose" runat="server" Text="Save &amp; Close" CssClass="button"></asp:Button>
            <asp:Button ID="btnSave" runat="server" Text="Save &amp; Proceed" CssClass="button"></asp:Button>
            <%--<span class="CloseButton" onclick="return Close();" class="button">Close</span>&nbsp;&nbsp;--%>
            <span class="button" onclick="return Close();">Close</span>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Arrange items into boxes
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table align="center" width="700px">
        <tr>
            <td align="center" class="normal4">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:Table ID="tbl" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="700px" CssClass="aspTable" BorderColor="black" GridLines="None">
        <asp:TableRow>
            <asp:TableCell>
                <br />
                <table border="0">
                    <tr>
                        <td class="normal1" colspan="2" align="right" valign="middle">
                            <%--  <asp:RadioButton ID="rbOption1" runat="server" Text="Each item has its own Box" CssClass="signup"
                                GroupName="option" AutoPostBack="true" Checked="true" />--%>
                            <asp:Button ID="btnAutoSelect" runat="server" Height="60" Style="display:none;word-wrap: break-word; height: 50px;" Text="Auto-Select Packages & Auto-Pack items based on shipping rules" CssClass="button"></asp:Button>
                            &nbsp;
                            <a href="#" data-toggle="tooltip" title="Containers can be added from Administration | Items | Utilities | Containers. You can assign a container to an item, including the qty of units of that item that fill each container, from the item record itself." onclick="return OpenHelp()"><label class="badge bg-yellow">?</label></a>
                            Select Container&nbsp;
                            <asp:DropDownList runat="server" ID="ddlPackageType"></asp:DropDownList>&nbsp;&nbsp;
                            <asp:Button ID="AddPackageBox" runat="server" Text="Add" OnClientClick="if(!CreateBox(2)) { return false;}" CssClass="btn btn-primary" />
                            &nbsp;Manually add a generic box&nbsp;
                            <span id="spnAddBox">
                                <img src="../images/package_add.png" title="Add Box" style="cursor: pointer" />
                            </span>

                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td class='hs' align='center' width='210'>Available Items
                        </td>
                        <td class='hs' align="left">Virtual Staging Area
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            <asp:Literal ID="litContent" runat="server"></asp:Literal>
                        </td>
                        <td valign="top">
                            <div id="BoxHolder" class="BoxHolder" runat="server">
                            </div>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:HiddenField runat="server" ID="hdnShippingReportID" />
    <asp:HiddenField runat="server" ID="hdnValues" />
    <asp:HiddenField runat="server" ID="hdnNoOfBox" Value="0" />
    <asp:HiddenField runat="server" ID="hdnShippingCompanyID" Value="0" />
    <div style="clear:both"></div>
</asp:Content>
