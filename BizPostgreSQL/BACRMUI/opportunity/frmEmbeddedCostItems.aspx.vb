﻿Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Common
Partial Public Class frmEmbeddedCostItems
    Inherits BACRMPage
    'Dim lngOppBizDocID As Long
    'Dim lngOppID As Long
    Dim lngEmbeddedCostID As Long
    'http://localhost/BACRMUI/opportunity/frmEmbeddedCostItems.aspx?OppId=12388&OppBizDocId=11988&EmbeddedCostID=156
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'lngOppID = GetQueryStringVal( "OppId")
            'lngOppBizDocID = GetQueryStringVal( "OppBizDocId")
            lngEmbeddedCostID = CCommon.ToLong(GetQueryStringVal("EmbeddedCostID"))
            If Not IsPostBack Then
                BindItems()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Sub BindItems()
        Try
            Dim dtOppBiDocItems As DataTable
            Dim objOpp As New MOpportunity
            objOpp.DomainID = Session("DomainID")
            objOpp.EmbeddedCostID = lngEmbeddedCostID
            dtOppBiDocItems = objOpp.GetEmbeddedCostItems()
            dgItems.DataSource = dtOppBiDocItems
            dgItems.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try

            Dim objOpp As New MOpportunity
            objOpp.DomainID = Session("DomainID")
            objOpp.EmbeddedCostID = lngEmbeddedCostID
            objOpp.strItems = GetItems()
            objOpp.ManageEmbeddedCostItems()
            BindItems()
            Response.Write("<script>opener.location.reload(true); self.close();</script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Private Function GetItems() As String
        Try
            Dim ds As New DataSet
            Dim dt As New DataTable
            Dim objOppBizDocs As New OppBizDocs

            dt.Columns.Add("numOppBizDocItemID")
            dt.Columns.Add("monAmount")

            Dim dr As DataRow
            For Each item As DataGridItem In dgItems.Items
                Dim txtCost As TextBox = CType(item.FindControl("txtAmount"), TextBox)
                Dim lngOppBizDocItemID As Long = CCommon.ToLong(item.Cells(0).Text)

                If lngOppBizDocItemID > 0 And CCommon.ToDecimal(txtCost.Text.Replace(",", "")) > 0 Then
                    dr = dt.NewRow()
                    dr("monAmount") = CCommon.ToDecimal(txtCost.Text.Replace(",", ""))
                    dr("numOppBizDocItemID") = lngOppBizDocItemID
                    dt.Rows.Add(dr)
                End If

            Next
            ds.Tables.Add(dt)
            Return ds.GetXml()
        Catch ex As Exception
            Throw ex
        End Try
    End Function
    Function ReturnMoney(ByVal Money)
        Try
            Return String.Format("{0:#,###.##}", Math.Round(Money, 2))
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class