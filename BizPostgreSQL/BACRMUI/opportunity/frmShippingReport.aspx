﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmShippingReport.aspx.vb"
    Inherits=".frmShippingReport" MasterPageFile="~/common/PopupBootstrap.Master" %>

<%@ Import Namespace="BACRM.BusinessLogic.Common" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Shipping Report</title>
    <style type="text/css">
        .dialog-header {
            position: relative;
            text-indent: 1em;
            height: 4em;
            line-height: 4em;
            background-color: #f7f7f7;
        }

        .dialog-body {
            padding: .5em;
            word-break: break-all;
            overflow-y: auto;
        }
    </style>
    <script>

        function SelectAll(headerCheckBox, ItemCheckboxClass) {
            
            $("." + ItemCheckboxClass + " input[type = 'checkbox']").each(function () {

                $(this).prop('checked', $('[id$=' + headerCheckBox + ']').is(':checked'))
                var bool = $("#<%=radBoxes.ClientID%>input[id*='" + headerCheckBox + "']:checkbox").is(':checked');
                var chkunchk;

                if (bool == false) {
                    chkunchk = true;
                }
                else {
                    chkunchk = false;
                }
                //check and check the checkboxes on basis of Boolean value 

                $("#<%=radBoxes.ClientID%> input[id*='" + ItemCheckboxClass + "']:checkbox").attr('checked', chkunchk);

            });
        }

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function openLabelList(a) {
            window.open('../opportunity/frmShippingLabelImages.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&BoxID=' + a, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=900,height=600,scrollbars=yes,resizable=yes');
        }

        function openLabelDetails(ShippingReportID, OppId, OppBizDocID) {
            window.open('../opportunity/frmGenerateShippingLabel.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&View=1&ShipReportId=' + ShippingReportID + '&OppId=' + OppId + "&OppBizDocId=" + OppBizDocID, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=1100,height=600,scrollbars=yes,resizable=yes');
        }

        function DownloadXML(strPath) {
            window.open(strPath, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,width=900,height=600,scrollbars=yes,resizable=yes');
        }

        function CloseWindow() {
            this.close();
        }

        function CloseWarehousePopup() {
            $("[id$=hdnShippingLabelFile]").val("");
            $("[id$=divWarehouse]").hide();
            $('#lkbPrint_Click').trigger('click');
            return false;
        }
       

        function SelectWarehouses() {
            var x = document.getElementById("divWarehouse");
            if (x.style.display === "none") {
                x.style.display = "block";
                return false;
            } 
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:LinkButton ID="lkbPrint" runat="server" OnClientClick="if(!SelectWarehouses()) { return false;}" CssClass="btn btn-primary" OnClick="lkbPrint_Click" ToolTip="Print" ><i class="fa fa-print"></i></asp:LinkButton>
                <asp:Button runat="server" ID="btnAutoGenerateLabels" Text="Auto Generate Shipping Labels" CssClass="btn btn-primary" />
                <asp:Button runat="server" ID="btnGenerateShippingLabels" Text="Generate Shipping Labels" CssClass="btn btn-primary" />
                <asp:Button runat="server" ID="btnClose" Text="Close" OnClientClick="CloseWindow()" CssClass="btn btn-primary" />
            </div>
        </div>
    </div>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Shipping Report
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager runat="server" ID="sc1" />
    <div class="row">
        <div class="col-xs-12 text-center">
            <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-3 text-center">
            <div class="form-group">
                <label>Added By,On:</label>
                <asp:Label runat="server" ID="lblAddedDetails"></asp:Label>
            </div>
        </div>
        <div class="col-sm-12 col-md-2 text-center">
            <div class="form-group">
                <label>Regular Weight:</label>
                <asp:Label runat="server" ID="lblRegularWeight"></asp:Label>
            </div>
        </div>
        <div class="col-sm-12 col-md-2 text-center">
            <div class="form-group">
                <label>Dimensional Weight:</label>
                <asp:Label runat="server" ID="lblDimensionalWeight"></asp:Label>
            </div>
        </div>
        <div class="col-sm-12 col-md-2 text-center">
            <div class="form-group">
                <label>BizDoc ID:</label>
                <asp:Label ID="lblBizDocName" runat="server"></asp:Label>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <telerik:RadGrid ID="radBoxes" runat="server" Width="100%" AutoGenerateColumns="False"
                GridLines="None" ShowFooter="false" Skin="windows" EnableEmbeddedSkins="false"
                CssClass="dg" AlternatingItemStyle-CssClass="ais" ItemStyle-CssClass="is" HeaderStyle-CssClass="hs"
                AutoGenerateHierarchy="false">
                <MasterTableView DataKeyNames="numBoxID" HierarchyLoadMode="Client" DataMember="Box">
                    <DetailTables>
                        <telerik:GridTableView Width="100%" runat="server" ItemStyle-HorizontalAlign="Center"
                            AlternatingItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                            ShowFooter="false" DataMember="BoxItems">
                            <ParentTableRelation>
                                <telerik:GridRelationFields DetailKeyField="numBoxID" MasterKeyField="numBoxID" />
                            </ParentTableRelation>
                            <Columns>
                                <telerik:GridBoundColumn HeaderText="numBoxID" DataField="numBoxID" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="ShippingReportItemId" DataField="ShippingReportItemId"
                                    Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="numOppBizDocItemID" DataField="numOppBizDocItemID"
                                    Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="numItemCode" DataField="numItemCode" Visible="false">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Item" DataField="vcItemName">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Model #" DataField="vcModelId">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Description" DataField="vcItemDesc">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Weight per Unit" DataField="fltTotalWeight">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn HeaderText="Qty">
                                    <ItemTemplate>
                                        <%# DataBinder.Eval(Container.DataItem, "intBoxQty")%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn HeaderText="Dimensions = ( H x L x W ) ">
                                    <ItemTemplate>
                                        <%# DataBinder.Eval(Container.DataItem, "fltHeight")%>
                                                        x
                                                        <%# DataBinder.Eval(Container.DataItem, "fltLength")%>
                                                        x
                                                        <%# DataBinder.Eval(Container.DataItem, "fltWidth")%>
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                            </Columns>
                        </telerik:GridTableView>
                    </DetailTables>
                    <Columns>
                        <telerik:GridBoundColumn HeaderText="numBoxID" DataField="numBoxID" Visible="false">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Box" DataField="vcBoxName" ItemStyle-Width="5%">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn HeaderText="Dimensions ( H x L x W )" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "fltHeight")%>
                                                x
                                                <%# DataBinder.Eval(Container.DataItem, "fltLength")%>
                                                x
                                                <%# DataBinder.Eval(Container.DataItem, "fltWidth")%>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Total weight" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <%# DataBinder.Eval(Container.DataItem, "fltTotalWeight")%>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Service" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <%# TranslateServiceType(Eval("numServiceTypeID"))%>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Package Type" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <asp:HiddenField runat="server" ID="hdnPackageTypeID" Value='<%# DataBinder.Eval(Container.DataItem, "numPackageTypeID")%>' />
                                <%# DataBinder.Eval(Container.DataItem, "vcPackageName")%>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Tracking#" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <a target="_blank" href="<%# String.Format(ViewState("TrackingURL"), Eval("vcTrackingNumber"))%>">
                                    <%#Eval("vcTrackingNumber")%></a>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Label" ItemStyle-Width="5%">
                            <ItemTemplate>
                                <asp:HyperLink ID="hplOpen" runat="server" onclick='<%# "openLabelList(" & Eval("numBoxID") & ");"%>' NavigateUrl="#" Visible='<%#Boolean.Parse(IIf(CCommon.ToString(Eval("vcShippingLabelImage")).Contains(".zpl"), False, True)) %>' Text="Open"></asp:HyperLink>
                                <asp:LinkButton ID="hplDownload" runat="server" CommandName="Download" CommandArgument='<%# Eval("vcShippingLabelImage")%>' Visible='<%# Boolean.Parse(IIf(Eval("vcShippingLabelImage").ToString().Contains(".zpl"), True, False))%>'>Download ZPL</asp:LinkButton>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Label Details" ItemStyle-Width="15%">
                            <ItemTemplate>
                                <a onclick="openLabelDetails(<%=lngShippingReportId%>,<%=lngOppID%>,<%=lngOppBizDocID%>);" href="#">Request</a>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="Shipping" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:ImageButton ID="lnkDownloadRequest" Text="Request"
                                    CommandArgument='<%#"ShippingRequest_" & Eval("numOppId") & "_" & Eval("numShippingReportId")%>'
                                    runat="server" OnClick="DownloadFile" ImageUrl="../images/download_request.png" ToolTip="Download Shipping Request"></asp:ImageButton>&nbsp;/&nbsp;
                                                <asp:ImageButton ID="lnkDownloadResponse" Text="Response"
                                                    CommandArgument='<%#"ShippingResponse_" & Eval("numOppId") & "_" & Eval("numShippingReportId")%>'
                                                    runat="server" OnClick="DownloadFile" ImageUrl="../images/download_response.png" ToolTip="Download Shipping Response"></asp:ImageButton>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                        <telerik:GridTemplateColumn HeaderText="" ItemStyle-Wrap="false">
                             <HeaderTemplate>
                                <asp:CheckBox ID="chkAll"  runat="server" onclick="SelectAll('chkAll','chkSelect');" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger" CommandName="Delete" CommandArgument='<%# Eval("numBoxID")%>' OnClientClick="return DeleteRecord();" ToolTip="Delete"><i class="fa fa-trash-o"></i></asp:LinkButton>
                                 &nbsp;<asp:CheckBox ID="chkSelect" CssClass="chkSelect" runat="server"  />
                                <asp:HiddenField ID="hdnShippingLabelImage" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "vcShippingLabelImage")%>' />
                              <%--  <asp:LinkButton ID="lkbPrint" runat="server" CssClass="btn btn-primary" ToolTip="Print" CommandName="Print" CommandArgument='<%# Eval("vcShippingLabelImage")%>'><i class="fa fa-print"></i></asp:LinkButton>--%>
                            </ItemTemplate>
                        </telerik:GridTemplateColumn>
                    </Columns>
                </MasterTableView>
                <ClientSettings AllowExpandCollapse="true" />
            </telerik:RadGrid>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <div class="form-group">
                    <label>Total Shipping Cost:</label>
                    <asp:Label ID="lblTotalShipCost" runat="server" Text=""></asp:Label>
                </div>
            </div>
        </div>
    </div>
    <div class="overlay" id="divWarehouse" runat="server" style="display: none">
        <div class="overlayContent" style="color: #000; background-color: #FFF; width: 350px; border: 1px solid #ddd; margin: 100px auto !important;">
            <div class="dialog-header">
                <b style="font-size: 14px;">Select Warehouse to Print</b>
                <div style="float: right; padding-right: 7px;">
                    <asp:Button ID="btnClosePriceHistory" runat="server" CssClass="btn btn-primary" ForeColor="White" Text="Close Window" OnClientClick="return CloseWarehousePopup();" />
                </div>
            </div>
            <div class="dialog-body" style="max-height: 600px; width: 100%;">
                <div class="form-group">
                    <label>Warehouse:</label>
                    <telerik:RadComboBox runat="server" ID="radcmbWarehouses" Width="200" />
                    <asp:Button ID="btnPrintLabels" runat="server" Text="Print" CssClass="btn btn-primary" />
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnShippingCompanyID" runat="server" />
    <asp:HiddenField ID="hdnShippingCompanyName" runat="server" />
    <asp:HiddenField ID="hdnShippingLabelFile" runat="server" />
    <asp:HiddenField ID="hdnShipFromWarehouse" runat="server" />
</asp:Content>
