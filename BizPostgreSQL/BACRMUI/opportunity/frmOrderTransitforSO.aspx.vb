﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities

Public Class frmOrderTransitforSO
    Inherits BACRMPage

    Dim lngItemCode As String
    Dim dtData As DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        lngItemCode = GetQueryStringVal("ItemCode")
        bindGrid()
    End Sub

    Protected Sub rptItems_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim drv As DataRowView = DirectCast(e.Item.DataItem, DataRowView)

                If Not e.Item.FindControl("rptVendorDetails") Is Nothing Then
                    Dim hdnItemNameData As HiddenField
                    hdnItemNameData = DirectCast(e.Item.FindControl("hdnItemNameData"), HiddenField)
                    Dim dtItemDetails As New DataTable
                    dtItemDetails.Columns.Add("vcCompanyName")
                    dtItemDetails.Columns.Add("vendorShipmentDays")
                    Dim dv As DataView
                    dv = dtData.DefaultView()
                    dv.RowFilter = "ItemName = '" & hdnItemNameData.Value & "'"
                    Dim i As Int16 = 0
                    For Each rowView As DataRowView In dv
                        Dim strCompanyName As String
                        If i = 0 Then
                            strCompanyName = "<b>" & rowView("vcCompanyName") & "</b>" & "<i>(Preferred)</i>"
                            If rowView("vcDropShip") <> "-" Then
                                strCompanyName = strCompanyName & "<i class='text-warning'>Drop-Ship (" & rowView("vcDropShip") & ")</i>"
                            End If
                            strCompanyName = strCompanyName & "<i class='text-success'>Qty of " & rowView("numUnitHour") & " due " & rowView("DueDate") & " (" & rowView("vcDeliverTo") & ")</i>"
                        Else
                            strCompanyName = "<b>" & rowView("vcCompanyName") & "</b>"
                        End If
                        dtItemDetails.Rows.Add(strCompanyName, rowView("vendorShipmentDays"))
                        i = i + 1
                    Next
                    DirectCast(e.Item.FindControl("rptVendorDetails"), Repeater).DataSource = dtItemDetails
                    DirectCast(e.Item.FindControl("rptVendorDetails"), Repeater).DataBind()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        End Try
    End Sub
    Sub bindGrid()
        Dim objOpportunity As New MOpportunity
        objOpportunity.strItems = lngItemCode
        objOpportunity.DomainID = Session("DomainID")
        objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
        dtData = objOpportunity.GetItemTransitForSO()
        Dim dtItemTable As New DataTable
        dtItemTable.Columns.Add("ItemName")
        Dim dv As DataView = dtData.DefaultView()
        dtItemTable = dv.ToTable(True, "ItemName")

        'For Each rowView As DataRowView In dv
        '    dtItemTable.Rows.Add("ItemName")
        'Next
        rptItems.DataSource = dtItemTable
        rptItems.DataBind()
    End Sub
End Class