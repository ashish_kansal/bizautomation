'CXreated By Anoop Jayaraj
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Namespace BACRM.UserInterface.Contacts


    Public Class frmContactList
        Inherits BACRMPage
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim strColumn As String
        Dim RegularSearch As String
        Dim CustomSearch As String
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                txtDateFormatInlineEdit.Text = CCommon.GetValidationDateFormat()
                GetUserRightsForPage(11, 2)
                If GetQueryStringVal("SI") <> "" Then
                    SI = GetQueryStringVal("SI")
                    'radOppTab.SelectedIndex = SI
                End If
                If GetQueryStringVal("SI1") <> "" Then
                    SI1 = GetQueryStringVal("SI1")
                Else : SI1 = 0
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    SI2 = GetQueryStringVal("SI2")
                Else : SI2 = 0
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = GetQueryStringVal("frm1")
                Else : frm1 = ""
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    frm2 = GetQueryStringVal("frm2")
                Else : frm2 = ""
                End If
                If GetQueryStringVal("frm") <> "" Then
                    frm = GetQueryStringVal("frm")
                Else : frm = ""
                End If

                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                If Not IsPostBack Then
                    'Check if user has rights to edit grid configuration
                    Dim m_aryRightsForViewGridConfiguration() As Integer
                    m_aryRightsForViewGridConfiguration = GetUserRightsForPage_Other(4, 11)
                    If m_aryRightsForViewGridConfiguration(RIGHTSTYPE.VIEW) = 0 Then
                        Dim tdGridConfiguration As HtmlAnchor = CType(CCommon.FindControlRecursive(Page.Master, "tdGridConfiguration"), HtmlAnchor)

                        If Not tdGridConfiguration Is Nothing Then
                            tdGridConfiguration.Visible = False
                        End If
                    End If
                    objCommon.sb_FillComboFromDBwithSel(ddlContactType, 8, Session("DomainID"))
                    Dim item As New ListItem("Primary Contact", 101)
                    ddlContactType.Items.Add(item)
                    'ddlContactType.Items.(101, "Primary Contact")
                    'If GetQueryStringVal( "ContactType") <> "" Then
                    '    If Not ddlContactType.Items.FindByValue(GetQueryStringVal( "ContactType")) Is Nothing Then
                    '        ddlContactType.ClearSelection()
                    '        ddlContactType.Items.FindByValue(GetQueryStringVal( "ContactType")).Selected = True
                    '    End If
                    'End If

                    ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' ' = "Contacts"
                    'Dim ListDetails(8) As String
                    'ListDetails = GetFilterData()  'Session("ListDetails")
                    'If ListDetails IsNot Nothing AndAlso ListDetails.Length >= 8 Then
                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                        txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                        txtCustomer.Text = CCommon.ToString(PersistTable(PersistKey.OrgName))
                        txtFirstName.Text = CCommon.ToString(PersistTable(PersistKey.FirstName))
                        txtLastName.Text = CCommon.ToString(PersistTable(PersistKey.LastName))
                        txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                        txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))
                        ddlContactType.ClearSelection()
                        If Not ddlContactType.Items.FindByValue(CCommon.ToString(PersistTable(ddlContactType.ID))) Is Nothing Then
                            ddlContactType.Items.FindByValue(CCommon.ToString(PersistTable(ddlContactType.ID))).Selected = True
                        End If
                        ddlSort.ClearSelection()
                        If Not ddlSort.Items.FindByValue(CCommon.ToString(PersistTable(PersistKey.FilterBy))) Is Nothing Then
                            ddlSort.Items.FindByValue(CCommon.ToString(PersistTable(PersistKey.FilterBy))).Selected = True
                        End If
                        txtGridColumnFilter.Text = CCommon.ToString(PersistTable(PersistKey.GridColumnSearch))
                    End If

                    If GetQueryStringVal("ContactType") <> "" Then
                        ddlContactType.ClearSelection()
                        ddlContactType.Items.FindByValue(IIf(GetQueryStringVal("ContactType") = "", 0, CCommon.ToLong(GetQueryStringVal("ContactType")))).Selected = True
                    End If

                    Dim dtTab As DataTable
                    dtTab = Session("DefaultTab")
                    If dtTab.Rows.Count > 0 Then
                        lbContacts.Text = IIf(IsDBNull(dtTab.Rows(0).Item("vcContact")), "Contacts", dtTab.Rows(0).Item("vcContact").ToString & "s")
                    Else : lbContacts.Text = "Contacts"
                    End If
                    BindDatagrid()
                End If
                Dim m_aryRightsForSendAnnounceMent() As Integer = GetUserRightsForPage_Other(43, 138)

                If m_aryRightsForSendAnnounceMent(RIGHTSTYPE.VIEW) <> 0 Then
                    btnSendAnnounceMent.Visible = True
                End If
                Dim m_aryRightsForAddActionItem() As Integer = GetUserRightsForPage_Other(44, 144)

                If m_aryRightsForAddActionItem(RIGHTSTYPE.VIEW) <> 0 Then
                    btnAddActionItem.Visible = True
                End If
                If Page.IsPostBack Then
                    BindDatagrid()
                End If
                'ClientScript.RegisterClientScriptBlock(Me.GetType, "SetTab", "if (parent.parent.frames.length > 0) { parent.parent.SelectTabByValue('7');}else{ parent.SelectTabByValue('7'); } ", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Function GetCheckedValues() As Boolean
            Try
                Session("ContIDs") = ""
                Dim isCheckboxchecked As Boolean = False
                Dim gvRow As GridViewRow
                For Each gvRow In gvSearch.Rows
                    If CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = True Then
                        Session("ContIDs") = Session("ContIDs") & CType(gvRow.FindControl("lbl1"), Label).Text & ","
                        isCheckboxchecked = True
                    End If
                Next
                Session("ContIDs") = Session("ContIDs").ToString.TrimEnd(",")
                Return isCheckboxchecked
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Private Sub btnAddActionItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddActionItem.Click
            Try
                Dim isCheckboxChecked As Boolean
                isCheckboxChecked = GetCheckedValues()
                If isCheckboxChecked Then
                    Response.Redirect("../Admin/ActionItemDetailsOld.aspx?selectedContactId=1", False)
                Else
                    DisplayError("Select atleast one checkbox For Action Item.")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Private Sub btnSendAnnounceMent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendAnnounceMent.Click
            Try
                Dim isCheckboxChecked As Boolean
                isCheckboxChecked = GetCheckedValues()
                If isCheckboxChecked Then
                    Session("EMailCampCondition") = Session("WhereCondition")
                    Response.Redirect("../Marketing/frmEmailBroadCasting.aspx?PC=true&SearchID=0&ID=0&SAll=true", False)
                Else
                    DisplayError("Select atleast one checkbox to broadcast E-mail.")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        'Private Sub SaveFilter(ByVal ListDetails As String())
        '    Try
        '        'IStart Pinkal Patel Date:03-11-2011
        '        Dim strFilter As New System.Text.StringBuilder
        '        Dim objContact As New CContacts
        '        objContact.DomainID = Session("DomainID")
        '        objContact.UserCntID = Session("UserContactID")
        '        objContact.ContactType = 0 'ddlContactType.SelectedValue
        '        objContact.bitDefault = False
        '        objContact.blnFlag = True
        '        objContact.FormId = 10
        '        objContact.GroupID = IIf(GetQueryStringVal("ContactType") = "", 0, GetQueryStringVal("ContactType"))
        '        objContact.FilterID = ddlSort.SelectedItem.Value
        '        strFilter.Append(ListDetails(0))
        '        strFilter.Append("~")
        '        strFilter.Append(ListDetails(1))
        '        strFilter.Append("~")
        '        strFilter.Append(ListDetails(2))
        '        strFilter.Append("~")
        '        strFilter.Append(ListDetails(3))
        '        strFilter.Append("~")
        '        strFilter.Append(ListDetails(4))
        '        strFilter.Append("~")
        '        strFilter.Append(ListDetails(5))
        '        strFilter.Append("~")
        '        strFilter.Append(ListDetails(6))
        '        strFilter.Append("~")
        '        strFilter.Append(ListDetails(7))
        '        strFilter.Append("~")
        '        strFilter.Append(ListDetails(8))
        '        objContact.strFilter = strFilter.ToString
        '        objContact.SaveDefaultFilter()
        '        'objContact.strFilter = 
        '        'IEnd Pinkal Patel Date:03-11-2011
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub
        'Public Function GetFilterData() As String()
        '    Try
        '        Dim objContact As New CContacts
        '        objContact.DomainID = Session("DomainId")
        '        objContact.UserCntID = Session("UserContactID")
        '        objContact.ContactType = 0
        '        objContact.FormId = 10
        '        objContact.GroupID = IIf(GetQueryStringVal("ContactType") = "", 0, CCommon.ToLong(GetQueryStringVal("ContactType")))
        '        Dim dt As DataTable = objContact.GetDefaultfilter()
        '        Dim ListDetails As String()
        '        If dt.Rows.Count > 0 Then
        '            ListDetails = CCommon.ToString(dt.Rows(0)("vcFilter")).Split("~")
        '            If Not ddlSort.Items.FindByValue(dt.Rows(0).Item("numfilterId")) Is Nothing Then
        '                ddlSort.ClearSelection()
        '                ddlSort.Items.FindByValue(dt.Rows(0).Item("numfilterId")).Selected = True
        '            End If
        '        End If
        '        Return ListDetails
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function
        'Private Sub SetDefaultFilter()
        '    Try
        '        Dim objcontact As New CContacts
        '        Dim dtTable As DataTable
        '        objcontact.DomainID = Session("DomainId")
        '        objcontact.UserCntID = Session("UsercontactId")
        '        objcontact.FormId = 10
        '        objcontact.ContactType = ddlContactType.SelectedValue
        '        objcontact.GroupID = IIf(GetQueryStringVal( "ContactType") Is Nothing, 0, GetQueryStringVal( "ContactType"))
        '        dtTable = objcontact.GetDefaultfilter()
        '        If dtTable.Rows.Count = 1 Then
        '            If Not IsDBNull(dtTable.Rows(0).Item("numfilterId")) Then

        '            End If
        '        End If
        '        If Not ddlSort.Items.FindByValue(dtTable.Rows(0).Item("numfilterId")) Is Nothing Then
        '            ddlSort.ClearSelection()
        '            ddlSort.Items.FindByValue(dtTable.Rows(0).Item("numfilterId")).Selected = True
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub
        Sub BindDatagrid(Optional ByVal CreateCol As Boolean = True)
            Try
                Dim dtContacts As DataTable
                Dim objContacts As New CContacts


                With objContacts
                    .UserCntID = Session("UserContactID")
                    .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                    .SortOrder = ddlSort.SelectedItem.Value
                    .DomainID = Session("DomainID")
                    .FirstName = txtFirstName.Text
                    .LastName = txtLastName.Text
                    .SortCharacter = txtSortChar.Text.Trim()
                    .CustName = txtCustomer.Text
                    .ContactType = ddlContactType.SelectedValue
                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()
                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0
                    If txtSortColumn.Text <> "" Then
                        'Added Validation because procedure was throwing "Ambiguous column name 'numRecOwner'."
                        txtSortColumn.Text = IIf(txtSortColumn.Text.ToLower() = "numRecOwner".ToLower(), "ADC.numRecOwner", txtSortColumn.Text)
                        txtSortColumn.Text = IIf(txtSortColumn.Text.ToLower() = "bintCreatedDate".ToLower(), "ADC.bintCreatedDate", txtSortColumn.Text)
                        .columnName = txtSortColumn.Text
                    Else : .columnName = "ADC.bintcreateddate"
                    End If
                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If


                    GridColumnSearchCriteria()

                    .RegularSearchCriteria = RegularSearch
                    .CustomSearchCriteria = CustomSearch
                End With



                'Session("ListDetails") = SortChar & "," & txtFirstName.Text & "," & txtLastName.Text & "," & txtCustomer.Text & "," & txtCurrrentPage.Text & "," & txtSortColumn.Text & "," & Session("Asc")
                objContacts.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                Dim dsList As DataSet

                dsList = objContacts.GetContactList
                dtContacts = dsList.Tables(0)

                bizPager.PageSize = Session("PagingRows")
                bizPager.RecordCount = objContacts.TotalRecords
                bizPager.CurrentPageIndex = txtCurrrentPage.Text

                'Persist Form Settings
                PersistTable.Clear()
                PersistTable.Add(PersistKey.CurrentPage, IIf(dtContacts.Rows.Count > 0, txtCurrrentPage.Text, "1"))
                PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())
                PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
                PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text.Trim())
                PersistTable.Add(PersistKey.OrgName, txtCustomer.Text.Trim())
                PersistTable.Add(PersistKey.FirstName, txtFirstName.Text.Trim())
                PersistTable.Add(PersistKey.LastName, txtLastName.Text.Trim())
                PersistTable.Add(PersistKey.FilterBy, ddlSort.SelectedValue)
                PersistTable.Add(ddlContactType.ID, ddlContactType.SelectedValue)
                PersistTable.Add(PersistKey.GridColumnSearch, txtGridColumnFilter.Text)
                PersistTable.Save()
                ''Mstart Pinkal Patel Date:26/11/2011
                'Dim ListDetails(8) As String
                'ListDetails(0) = SortChar
                'ListDetails(1) = txtFirstName.Text
                'ListDetails(2) = txtLastName.Text
                'ListDetails(3) = txtCustomer.Text
                'ListDetails(4) = IIf(dtContacts.Rows.Count > 0, txtCurrrentPage.Text, "1")
                'ListDetails(5) = txtSortColumn.Text
                'ListDetails(6) = Session("Asc")
                'ListDetails(7) = .SelectedItem.Value
                'ListDetails(8) = ddlSort.SelectedItem.Value

                'SaveFilter(ListDetails)
                'Session("ListDetails") = ListDetails
                'MEnd Pinkal Patel Date:26/11/2011

                Dim dtTableInfo As DataTable
                dtTableInfo = dsList.Tables(1)

                'If objContacts.TotalRecords = 0 Then
                '    'hidenav.Visible = False
                '    lblRecordCount.Text = 0
                'Else
                '    'hidenav.Visible = True
                '    lblRecordCount.Text = String.Format("{0:#,###}", objContacts.TotalRecords)
                '    Dim strTotalPage As String()
                '    Dim decTotalPage As Decimal
                '    decTotalPage = lblRecordCount.Text / Session("PagingRows")
                '    decTotalPage = Math.Round(decTotalPage, 2)
                '    strTotalPage = CStr(decTotalPage).Split(".")
                '    If (lblRecordCount.Text Mod Session("PagingRows")) = 0 Then
                '        'lblTotal.Text = strTotalPage(0)
                '        txtTotalPage.Text = strTotalPage(0)
                '    Else
                '        'lblTotal.Text = strTotalPage(0) + 1
                '        txtTotalPage.Text = strTotalPage(0) + 1
                '    End If
                '    txtTotalRecords.Text = lblRecordCount.Text
                'End If
                'dgContacts.DataSource = dtContacts
                'dgContacts.DataBind()

                Dim m_aryRightsForInlineEdit() As Integer = GetUserRightsForPage_Other(11, 3)

                Dim i As Integer

                For i = 0 To dtContacts.Columns.Count - 1
                    dtContacts.Columns(i).ColumnName = dtContacts.Columns(i).ColumnName.Replace(".", "")
                Next

                Dim htGridColumnSearch As New Hashtable

                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")
                    Dim strIDValue() As String

                    For i = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")

                        htGridColumnSearch.Add(strIDValue(0), strIDValue(1))
                    Next
                End If

                If CreateCol = True Then
                    Dim bField As BoundField
                    Dim Tfield As TemplateField

                    gvSearch.Columns.Clear()

                    For Each drRow As DataRow In dtTableInfo.Rows
                        Tfield = New TemplateField

                        Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, drRow, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 10, objContacts.columnName, objContacts.columnSortOrder)

                        Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, drRow, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 10, objContacts.columnName, objContacts.columnSortOrder)
                        gvSearch.Columns.Add(Tfield)
                    Next

                    Dim dr As DataRow
                    dr = dtTableInfo.NewRow()
                    dr("vcAssociatedControlType") = "DeleteCheckBox"
                    dr("intColumnWidth") = "30"

                    Tfield = New TemplateField
                    Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, dr, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 10)
                    Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, dr, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 10)
                    gvSearch.Columns.Add(Tfield)
                End If
                gvSearch.DataSource = dtContacts
                gvSearch.DataBind()

                Dim objPageControls As New PageControls
                Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtTableInfo)
                ClientScript.RegisterClientScriptBlock(Me.GetType, "InlineEditValidation", strValidation, True)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlSort_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSort.SelectedIndexChanged
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
                ' Session("SContacts") = ddlSort.SelectedIndex
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub



        Private Sub txtCurrrentPage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub



        Private Sub ddlContactType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContactType.SelectedIndexChanged
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
            Try
                BindDatagrid()
                Dim objContact As New CContacts
                Dim lngCntID As Long
                Dim lngTerrID As Long
                Dim lngOCntID As Long
                Dim strContactId As String() = txtDelContactIds.Text.Split(",")
                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 1 Then
                    For i As Integer = 0 To strContactId.Length - 1
                        lngCntID = strContactId(i).Split("~")(0)
                        lngOCntID = strContactId(i).Split("~")(2)
                        If lngOCntID = Session("UserContactID") Then
                            If ddlSort.SelectedItem.Value = 7 Then
                                objContact.byteMode = 1
                                objContact.UserCntID = Session("UserContactID")
                                objContact.ContactID = lngCntID
                                objContact.ManageFavorites()
                                litMessage.Text = "Deleted from Favorites"
                                BindDatagrid()
                            Else
                                DeleteContact(objContact, lngCntID)
                            End If
                        End If
                    Next
                ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 2 Then
                    Dim dtTerritory As DataTable
                    dtTerritory = Session("UserTerritory")
                    For i As Integer = 0 To strContactId.Length - 1
                        lngCntID = strContactId(i).Split("~")(0)
                        Dim TerrId As Long = strContactId(i).Split("~")(1)
                        Dim chkDelete As Boolean = False
                        If TerrId = 0 Then
                            chkDelete = True
                        Else
                            For j As Integer = 0 To dtTerritory.Rows.Count - 1
                                If TerrId = dtTerritory.Rows(j).Item("numTerritoryId") Then chkDelete = True
                            Next
                        End If

                        If chkDelete = True Then
                            If ddlSort.SelectedItem.Value = 7 Then
                                objContact.byteMode = 1
                                objContact.UserCntID = Session("UserContactID")
                                objContact.ContactID = lngCntID
                                objContact.ManageFavorites()
                                litMessage.Text = "Deleted from Favorites"
                                BindDatagrid()
                            Else
                                DeleteContact(objContact, lngCntID)
                            End If
                        End If
                    Next
                ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 3 Then
                    For i As Integer = 0 To strContactId.Length - 1
                        lngCntID = strContactId(i).Split("~")(0)
                        Dim TerrId As Long = strContactId(i).Split("~")(1)
                        If ddlSort.SelectedItem.Value = 7 Then
                            objContact.byteMode = 1
                            objContact.UserCntID = Session("UserContactID")
                            objContact.ContactID = lngCntID
                            objContact.ManageFavorites()
                            litMessage.Text = "Deleted from Favorites"
                            BindDatagrid()
                        Else
                            DeleteContact(objContact, lngCntID)
                        End If
                    Next
                End If

                txtDelContactIds.Text = ""
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)

            End Try
        End Sub

        Private Sub DeleteContact(ByVal objContact As CContacts, ByVal lngCntID As Long)
            Try
                If lngCntID <> Session("AdminID") Then
                    objContact.ContactID = lngCntID
                    objContact.DomainID = Session("DomainID")
                    If objContact.DelContact().Length > 2 Then litMessage.Text = "At least on or more contacts you're trying to delete are the only contact records within the organization and thus can't be deleted. To delete them you'll need to delete the entire organization."
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub gvSearch_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvSearch.RowDataBound
            Try
                If e.Row.RowType = DataControlRowType.DataRow Then
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub GridColumnSearchCriteria()
            Try
                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")

                    Dim strIDValue(), strID(), strCustom As String
                    Dim strRegularCondition As New ArrayList
                    Dim strCustomCondition As New ArrayList


                    For i As Integer = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")
                        strID = strIDValue(0).Split("~")

                        If strID(0).Contains("CFW.Cust") Then
                            Select Case strID(3).Trim()
                                Case "TextBox", "TextArea"
                                    strCustomCondition.Add(" ADC.numContactID in (select distinct AdditionalContactsInformation.numContactID from AdditionalContactsInformation left join CFW_Fld_Values_Cont CFW ON AdditionalContactsInformation.numContactID=CFW.RecId where AdditionalContactsInformation.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & " ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                Case "CheckBox"
                                    If strIDValue(1) = "Yes" Then
                                        strCustomCondition.Add(" ADC.numContactID in (select distinct AdditionalContactsInformation.numContactID from AdditionalContactsInformation left join CFW_Fld_Values_Cont CFW ON AdditionalContactsInformation.numContactID=CFW.RecId where AdditionalContactsInformation.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and COALESCE(CFW.Fld_Value,0) " & "=1)")
                                    ElseIf strIDValue(1) = "No" Then
                                        strCustomCondition.Add(" ADC.numContactID in (select distinct AdditionalContactsInformation.numContactID from AdditionalContactsInformation left join CFW_Fld_Values_Cont CFW ON AdditionalContactsInformation.numContactID=CFW.RecId where AdditionalContactsInformation.numDomainId=" & Session("DomainID") & " AND 1 = (CASE WHEN (SELECT COUNT(*) FROM CFW_Fld_Values_Cont CFWInner WHERE CFWInner.RecId=AdditionalContactsInformation.numContactID AND CFWInner.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND CFWInner.Fld_Value='1') > 0 THEN 0 ELSE 1 END))")
                                    End If
                                Case "SelectBox"
                                    strCustomCondition.Add(" ADC.numContactID in (select distinct AdditionalContactsInformation.numContactID from AdditionalContactsInformation left join CFW_Fld_Values_Cont CFW ON AdditionalContactsInformation.numContactID=CFW.RecId where AdditionalContactsInformation.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & "=" & strIDValue(1) & ")")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strCustomCondition.Add(" ADC.numContactID in (select distinct AdditionalContactsInformation.numContactID from AdditionalContactsInformation left join CFW_Fld_Values_Cont CFW ON AdditionalContactsInformation.numContactID=CFW.RecId where AdditionalContactsInformation.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " >= '" & fromDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strCustomCondition.Add(" ADC.numContactID in (select distinct AdditionalContactsInformation.numContactID from AdditionalContactsInformation left join CFW_Fld_Values_Cont CFW ON AdditionalContactsInformation.numContactID=CFW.RecId where AdditionalContactsInformation.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " <= '" & toDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    End If
                                Case "CheckBoxList"
                                    Dim items As String() = strIDValue(1).Split(",")
                                    Dim searchString As String = ""

                                    For Each item As String In items
                                        searchString = searchString & If(searchString.Length > 0, " OR ", "") & " GetCustFldValueContact(" & strID(0).Replace("CFW.Cust", "") & ",AdditionalContactsInformation.numContactID) " & " ilike '%" & item.Replace("'", "''") & "%'"
                                    Next

                                    strCustomCondition.Add(" ADC.numContactID in (select distinct AdditionalContactsInformation.numContactID from AdditionalContactsInformation left join CFW_Fld_Values_Cont CFW ON AdditionalContactsInformation.numContactID=CFW.RecId where AdditionalContactsInformation.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND (" & searchString & "))")
                                Case Else
                                    strCustomCondition.Add(" ADC.numContactID in (select distinct AdditionalContactsInformation.numContactID from AdditionalContactsInformation left join CFW_Fld_Values_Cont CFW ON AdditionalContactsInformation.numContactID=CFW.RecId where AdditionalContactsInformation.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & strCustom & ")")
                            End Select
                        Else
                            Select Case strID(3).Trim()
                                Case "Website", "Email", "TextBox"

                                    If strID(0) = "ADC.vcCompactContactDetails" Then
                                        strRegularCondition.Add("(ADC.vcFirstName ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR ADC.vcLastName ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR ADC.numPhone ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR ADC.numPhoneExtension ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                    Else
                                        strRegularCondition.Add(strID(0) & " ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                    End If
                                Case "SelectBox"
                                    If strID(0).Contains("numFollowUpStatus") Then
                                        strRegularCondition.Add(strID(0) & " IN(" & strIDValue(1) & ")")
                                    Else
                                        strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                                    End If
                                Case "TextArea"
                                    strRegularCondition.Add(" Cast(" & strID(0) & " as varchar(5000)) ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strRegularCondition.Add(strID(0) & " >= '" & fromDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strRegularCondition.Add(strID(0) & " <= '" & toDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    End If
                            End Select
                        End If
                    Next
                    RegularSearch = String.Join(" and ", strRegularCondition.ToArray())
                    CustomSearch = String.Join(" and ", strCustomCondition.ToArray())
                Else
                    RegularSearch = ""
                    CustomSearch = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
    End Class

End Namespace