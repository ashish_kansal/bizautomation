<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" MasterPageFile="~/common/GridMaster.Master"
    CodeBehind="frmContactList.aspx.vb" Inherits="BACRM.UserInterface.Contacts.frmContactList" %>

<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Contacts</title>
    <script language="javascript">
        function OpenSetting() {
            window.open('../Contact/frmConfContactList.aspx?FormId=10', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                var RecordIDs = GetCheckedRowValues()
                document.getElementById('txtDelContactIds').value = RecordIDs;
                if (RecordIDs.length > 0)
                    return true;
                else {
                    alert('Please select atleast one record!!');
                    return false;
                }
            }
            else {
                return false;
            }
        }

        function OpenContact(a, b) {

            var ContactType = document.getElementById('ddlContactType').value;
            var str;

            str = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactlist&ft6ty=oiuy&CntId=" + a + '&ContactType=' + ContactType;


            document.location.href = str;

        }

        function OpenWindow(a, b, c) {
            var ContactType = document.getElementById('ddlContactType').value;
            var str;
            if (b == 0) {

                str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactlist&DivID=" + a + '&ContactType=' + ContactType;

            }
            else if (b == 1) {

                str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactlist&DivID=" + a + '&ContactType=' + ContactType;

            }
            else if (b == 2) {

                str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactlist&klds+7kldf=fjk-las&DivId=" + a + '&ContactType=' + ContactType;

            }

            document.location.href = str;
        }

        function openDrip(cmpID, contactID) {
            window.open("../Marketing/frmConECampHstr.aspx?ConECampID=" + cmpID + "&pqwRT=" + contactID, '', 'toolbar=no,titlebar=no,top=250,left=250,width=700,height=300,scrollbars=no,resizable=no')
            return false;
        }

        function OpenEmail(a) {
            window.open('../Leads/frmOpenEmail.aspx?DivID=' + a + '', '', 'toolbar=no,titlebar=no,top=200,left=200,width=650,height=370,scrollbars=yes,resizable=yes');
            return false;
        }
        function OpenHelp() {
            window.open('../Help/Initial_Grid_Organizations_Contacts_.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
    </script>
        <style>
        .tblNoBorder td, .tblNoBorder th {
            border: 0px solid #fff !important;
        }

        @media (max-width:40em) {
            .tblNoBorder table, .tblNoBorder thead, .tblNoBorder tbody, .tblNoBorder tfoot, .tblNoBorder th, .tblNoBorder td, .tblNoBorder tr {
                display: block;
            }

            .tblNoBorder .editable_select {
                min-height: 30px;
            }

            .tblNoBorder td, .tblNoBorder th {
                text-align: left;
            }
        }

        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
     <div class="col-md-12">
            <div class="pull-left">
            <div class="form-inline">
                <label>
                    View</label>
                 <asp:DropDownList ID="ddlSort" runat="server" AutoPostBack="true" CssClass="form-control" style="max-width:150px">
                <asp:ListItem Value="2" Selected="true">My Contacts</asp:ListItem>
                <asp:ListItem Value="1">All Contacts</asp:ListItem>
                <asp:ListItem Value="3">Employees</asp:ListItem>
                <asp:ListItem Value="4">Added in Last 7 days</asp:ListItem>
                <asp:ListItem Value="5">Last 20 Added by me</asp:ListItem>
                <asp:ListItem Value="6">Last 20 Modified by me</asp:ListItem>
                <asp:ListItem Value="7">Favorites</asp:ListItem>
            </asp:DropDownList></div>
                </div>
         <div class="pull-right">
              <div class="form-inline">
                        <div class="form-group" style="display:none;">
                            <label>Type</label>
                             <asp:DropDownList ID="ddlContactType" runat="server" AutoPostBack="true" CssClass="form-control">
                        </asp:DropDownList>
                        </div>
              
                        <div class="form-group" style="display:none;">
                            <label>Organization</label>
                            <asp:TextBox ID="txtCustomer" runat="server"  CssClass="form-control"></asp:TextBox>
                        </div>
                 
                        <div class="form-group" style="display:none;">
                            <label>First Name</label>
                            <asp:TextBox ID="txtFirstName" runat="server"  CssClass="form-control"></asp:TextBox>
                        </div>
                   
                        <div class="form-group" style="display:none;">
                            <label>Last Name</label>
                            <asp:TextBox ID="txtLastName" runat="server"  CssClass="form-control"></asp:TextBox>
                        </div>
                 
                        <div class="form-group">
                            <%--<label>&nbsp;</label>--%>
                                 
                            <div>
                                <button id="btnAddNewContact" onclick="return OpenPopUp('../contact/newcontact.aspx');" class="btn btn-primary" ><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New</button>
                                <asp:LinkButton ID="btnAddActionItem" runat="server" CssClass="btn btn-primary" Visible="false"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Add Action Item</asp:LinkButton>
                                <asp:LinkButton ID="btnSendAnnounceMent" runat="server" CssClass="btn btn-primary" Visible="false"><i class="fa fa-envelope"></i>&nbsp;&nbsp;Send Announcement</asp:LinkButton>
                                <asp:LinkButton ID="btnGo" style="display:none" runat="server" CssClass="btn btn-primary"><i class="fa fa-share-square-o"></i>&nbsp;&nbsp;Go</asp:LinkButton>&nbsp;
                                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
                            </div>
                        </div>
             </div>
         </div>
 </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
 <div class="row">
     <div class="col-md-12">
         <asp:Literal ID="litMessage" runat="server"></asp:Literal>
     </div>
 </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lbContacts" runat="server"></asp:Label>&nbsp;<a href="#" onclick="return OpenHelpPopUp('contact/frmcontactlist.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
       <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
        ShowPageIndexBox="Never"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
    <asp:GridView ID="gvSearch" runat="server" EnableViewState="true" AutoGenerateColumns="false"
        CssClass="tbl table table-responsive table-bordered" Width="100%" ShowHeaderWhenEmpty="true">
        <Columns>
        </Columns>
    </asp:GridView>
        </div>
    <asp:TextBox ID="txtDelContactIds" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtDateFormatInlineEdit" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
      <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
          <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
