Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Namespace BACRM.UserInterface.Contacts

    Partial Public Class frmConfContactList
        Inherits BACRMPage
        Dim objContact As CContacts
        Dim lngFormID As Long

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lngFormID = CCommon.ToLong(GetQueryStringVal("FormId"))

                btnSave.Attributes.Add("onclick", "Save()")
                If Not IsPostBack Then

                    objCommon.sb_FillComboFromDBwithSel(ddlType, 8, Session("DomainID"))
                    BindLists()

                    If lngFormID = 56 Then 'Account/Prospect Contact Settings
                        ddlType.Visible = False
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindLists()
            Try
                Dim ds As DataSet
                objContact = New CContacts
                objContact.DomainID = Session("DomainId")
                objContact.FormId = lngFormID
                objContact.UserCntID = Session("UserContactId")
                objContact.ContactType = ddlType.SelectedValue

                ds = objContact.GetColumnConfiguration()
                lstAvailablefld.DataSource = ds.Tables(0)
                lstAvailablefld.DataTextField = "vcFieldName"
                lstAvailablefld.DataValueField = "numFieldID"
                lstAvailablefld.DataBind()
                lstSelectedfld.DataSource = ds.Tables(1)
                lstSelectedfld.DataValueField = "numFieldID"
                lstSelectedfld.DataTextField = "vcFieldName"
                lstSelectedfld.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlType.SelectedIndexChanged
            Try
                BindLists()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                If objContact Is Nothing Then objContact = New CContacts

                Dim dsNew As New DataSet
                Dim dtTable As New DataTable
                dtTable.Columns.Add("numFieldID")
                dtTable.Columns.Add("bitCustom")
                dtTable.Columns.Add("tintOrder")
                Dim i As Integer

                Dim dr As DataRow
                Dim str As String()
                str = hdnCol.Value.Split(",")
                For i = 0 To str.Length - 2
                    dr = dtTable.NewRow
                    dr("numFieldID") = str(i).Split("~")(0)
                    dr("bitCustom") = str(i).Split("~")(1)
                    dr("tintOrder") = i
                    dtTable.Rows.Add(dr)
                Next

                dtTable.TableName = "Table"
                dsNew.Tables.Add(dtTable.Copy)

                objContact.ContactType = ddlType.SelectedValue
                objContact.DomainID = Session("DomainId")
                objContact.UserCntID = Session("UserContactId")
                objContact.FormId = lngFormID
                objContact.strXml = dsNew.GetXml
                objContact.SaveContactColumnConfiguration()
                dsNew.Tables.Remove(dsNew.Tables(0))
                'If radY.Checked = True Then
                '    objContact.bitDefault = True
                'Else : objContact.bitDefault = True
                'End If
                'objContact.FilterID = ddlSort.SelectedValue
                'objContact.SaveDefaultFilter()
                BindLists()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
