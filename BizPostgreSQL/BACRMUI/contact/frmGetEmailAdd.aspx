<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmGetEmailAdd.aspx.vb"
    Inherits="frmGetEmailAdd" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Email Address</title>
    <script language="javascript">
//        $(document).ready(
//         function () {
//             document.getElementById('txtFind').focus();
//         }
//        );
        function GetEmailaddress(a) {
            
            var strID = '';
            var strCompanyName = "";
            $(".dg tr").each(function () {
                console.log('Loop ke andear');
                if ($(this).find(".chkEmail input[type = 'checkbox']").is(':checked')) {
                    strID = strID + $(this).find('#lblEmail').text() + "~" + $(this).find('#lblContactID').text() + "~" + $(this).find('#lblFirstName').text() + "~" + $(this).find('#lblLastName').text() + "|"
                    strCompanyName = $(this).find('#lblCompanyName').text();
                    
                 
                   
                }

            });
         
            console.log(strID);
            console.log(strCompanyName);
            window.opener.InsertEmail(a, strID);
            //Author:Sachin Sadhu||Date:12Dec2013
            //Purpose:To Get Selected Company Name for Filteration
            window.opener.getvalue(strCompanyName);
            //Ended by Sachin
            window.close();
        }
        //function fn_doPage(pgno) {
        //    $('#txtCurrrentPage').val(pgno);
        //    if ($('#txtCurrrentPage').val() != 0 || $('#txtCurrrentPage').val() > 0) {
        //        $('#btnGo1').trigger('click');
        //    }
        //}
        function fn_doPage(pgno, CurrentPageWebControlClientID, PostbackButtonClientID) {
            var currentPage;
            var postbackButton;

            if (CurrentPageWebControlClientID != '') {
                currentPage = CurrentPageWebControlClientID;
            }
            else {
                currentPage = 'txtCurrentPageCorr';
            }

            if (PostbackButtonClientID != '') {
                postbackButton = PostbackButtonClientID;
            }
            else {
                postbackButton = 'btnCorresGo';
            }

            $('#' + currentPage + '').val(pgno);
            if ($('#' + currentPage + '').val() != 0 || $('#' + currentPage + '').val() > 0) {
                $('#' + postbackButton + '').trigger('click');
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="right-input">
        <div class="input-part">
            <webdiyer:AspNetPager ID="bizPager" runat="server" Direction="RightToLeft" HorizontalAlign="Right"
                LayoutType="div" UrlPaging="false" CssClass="pagn" ShowMoreButtons="true" ShowPageIndexBox="Never"
                Width="" AlwaysShow="true" ShowCustomInfoSection="Left" CustomInfoHTML="Showing records %startrecordindex% to %endrecordindex% of %recordcount% "
                CustomInfoSectionWidth="300px" CustomInfoStyle="line-height:20px;margin-right:3px;text-align:right !important;" CurrentPageWebControlClientID="txtCurrrentPage" PostbackButtonClientID="btnGo1" >
            </webdiyer:AspNetPager>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Email Address
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table width="100%">
        <tr>
            <td align="right">
                <asp:TextBox ID="txtFind" runat="server" CssClass="signup" ToolTip="Find By Contact Name"></asp:TextBox>
            </td>
            
            <td>
                <asp:Button ID="btnFind" runat="server" Text="Find" Width="70" CssClass="button">
                </asp:Button>
            </td>
            <td>
                <asp:Button ID="btnTo" CssClass="button" runat="server" Text="To >>" Width="100">
                </asp:Button>
            </td>
            <td>
                <asp:Button ID="btnCc" CssClass="button" runat="server" Text="Cc >>" Width="100">
                </asp:Button>
            </td>
            <td>
                <asp:Button ID="btnBcc" CssClass="button" runat="server" Text="Bcc >>" Width="100">
                </asp:Button>
            </td>
            <td>
                <asp:Button ID="btnClose" CssClass="button" runat="server" Text="Close" Width="100">
                </asp:Button>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td colspan="2">
                <asp:DataGrid ID="dgContacts" AllowSorting="True" runat="server" Width="100%" CssClass="dg"
                    AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="First Name" SortExpression="vcFirstName">
                            <ItemTemplate>
                                <asp:Label ID="lblFirstName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vcFirstName") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Last Name" SortExpression="vcLastName">
                            <ItemTemplate>
                                <asp:Label ID="lblLastName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "vcLastName") %>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="vcEmail" SortExpression="vcEmail" HeaderText="<font >Email Address">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="CompanyName" SortExpression="CompanyName" HeaderText="<font >Company">
                        </asp:BoundColumn>
                        <asp:TemplateColumn>
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkHEmail" runat="server" onclick="SelectAll('chkHEmail','chkEmail');">
                                </asp:CheckBox>
                            </HeaderTemplate>
                            <HeaderStyle Width="14px" HorizontalAlign="Center" />
                            <ItemTemplate>
                                <asp:Label ID="lblEmail" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "vcEmail") %>'>
                                </asp:Label>
                                <asp:Label ID="lblContactID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numContactID") %>'>
                                </asp:Label>
                                <asp:Label ID="lblCompanyName" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "CompanyName") %>'>
                                </asp:Label>

                                <asp:CheckBox ID="chkEmail" CssClass="chkEmail" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:Button ID="btnGo1" runat="server" Style="display: none" />
    <asp:HiddenField ID="hdfCompanyName" Value="0" runat="server" />
</asp:Content>
