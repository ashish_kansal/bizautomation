'created by anoop jayaraj
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common
Partial Class frmGetEmailAdd
    Inherits BACRMPage
    Dim objCommon As ccommon
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
   
    Dim strColumn As String
    'NOTE: The following placeholder declaration is required by the Web Form Designer.
    'Do not delete or move it.
    Private designerPlaceholderDeclaration As System.Object

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            objCommon = New CCommon
            GetUserRightsForPage(11, 2)
            If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AC")
            If Not IsPostBack Then
                
                Session("Asc") = 1
                txtCurrrentPage.Text = 1
                

                'Author:Sachin Sadhu||Date:12-Dec-2013
                'Purpose: To Fiter EmailIDs by Company 
                '
                If Not (Server.UrlDecode(CCommon.ToString(GetQueryStringVal("Company"))) Is Nothing) Then
                    GetCompanyWiseContacts()
                End If

                'End of Code by Sachin

                BindDatagrid()


            End If

            btnTo.Attributes.Add("onclick", "return GetEmailaddress(1)")
            btnCc.Attributes.Add("onclick", "return GetEmailaddress(2)")
            btnBcc.Attributes.Add("onclick", "return GetEmailaddress(3)")
            btnClose.Attributes.Add("onclick", "return Close()")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindDatagrid()
        Try
            Dim dtContacts As DataTable
            Dim objContacts As New CContacts
            With objContacts
                .UserCntID = Session("UserContactID")
                .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                .DomainID = Session("DomainID")
                .KeyWord = txtFind.Text
                If txtCurrrentPage.Text.Trim <> "" Then
                    .CurrentPage = txtCurrrentPage.Text
                Else : .CurrentPage = 1
                End If
                .PageSize = Session("PagingRows")
                .TotalRecords = 0
                If ViewState("Column") <> "" Then
                    .columnName = ViewState("Column")
                Else : .columnName = "bintcreateddate"
                End If
                If Session("Asc") = 1 Then
                    .columnSortOrder = "Desc"
                Else : .columnSortOrder = "Asc"
                End If
            End With
            objContacts.bitFlag = True
            dtContacts = objContacts.GetContactEmailList

            bizPager.PageSize = Session("PagingRows")
            bizPager.RecordCount = objContacts.TotalRecords
            bizPager.CurrentPageIndex = txtCurrrentPage.Text
            'If objContacts.TotalRecords = 0 Then
            '    lblRecordCount.Text = 0
            'Else
            '    lblRecordCount.Text = String.Format("{0:#,###}", objContacts.TotalRecords)
            '    Dim strTotalPage As String()
            '    Dim decTotalPage As Decimal
            '    decTotalPage = lblRecordCount.Text / Session("PagingRows")
            '    decTotalPage = Math.Round(decTotalPage, 2)
            '    strTotalPage = CStr(decTotalPage).Split(".")
            '    If (lblRecordCount.Text Mod Session("PagingRows")) = 0 Then
            '        ' lblTotal.Text = strTotalPage(0)
            '        txtTotalPage.Text = strTotalPage(0)
            '    Else
            '        ' lblTotal.Text = strTotalPage(0) + 1
            '        txtTotalPage.Text = strTotalPage(0) + 1
            '    End If
            '    txtTotalRecords.Text = lblRecordCount.Text
            'End If
            dgContacts.DataSource = dtContacts
            dgContacts.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Author:Sachin Sadhu||Date:12-Dec-2013
    'Purpose: To Fiter EmailIDs by Company
    Sub GetCompanyWiseContacts()
        Try
            Dim strEmailID As String
            If Server.UrlDecode(CCommon.ToString(GetQueryStringVal("Company"))) = Nothing Then
                strEmailID = Nothing
            Else
                strEmailID = Server.UrlDecode(CCommon.ToString(GetQueryStringVal("Company")))

            End If

            Dim dtContacts As DataTable

            Dim objContacts As New CContacts
            With objContacts
                .UserCntID = Session("UserContactID")
                .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                .DomainID = Session("DomainID")
                .KeyWord = strEmailID
                If txtCurrrentPage.Text.Trim <> "" Then
                    .CurrentPage = txtCurrrentPage.Text
                Else : .CurrentPage = 1
                End If
                .PageSize = Session("PagingRows")
                .TotalRecords = 0
                If ViewState("Column") <> "" Then
                    .columnName = ViewState("Column")
                Else : .columnName = "bintcreateddate"
                End If
                If Session("Asc") = 1 Then
                    .columnSortOrder = "Desc"
                Else : .columnSortOrder = "Asc"
                End If
            End With
            objContacts.bitFlag = True
            dtContacts = objContacts.GetContactEmailList
            If dtContacts.Rows.Count > 0 Then
                txtFind.Text = dtContacts.Rows(0)("CompanyName").ToString()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'End of code by sachin


    Private Sub txtCurrrentPage_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrrentPage.TextChanged
        Try
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnFind_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFind.Click
        Try
            txtCurrrentPage.Text = 1
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub dgContacts_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgContacts.SortCommand
        Try
            strColumn = e.SortExpression.ToString()
            If ViewState("Column") <> strColumn Then
                ViewState("Column") = strColumn
                Session("Asc") = 0
            Else
                If Session("Asc") = 0 Then
                    Session("Asc") = 1
                Else : Session("Asc") = 0
                End If
            End If
            BindDatagrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    'Private Sub dgContacts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgContacts.ItemDataBound
    '    Try
    '        If e.Item.ItemType = ListItemType.Header Then
    '            Dim chk As CheckBox
    '            chk = e.Item.FindControl("chkHEmail")
    '            chk.Attributes.Add("onclick", "return Selectall(dgContacts)")
    '        End If
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

End Class
