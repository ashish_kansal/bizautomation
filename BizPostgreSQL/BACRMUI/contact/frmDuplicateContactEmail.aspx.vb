﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts

Public Class frmDuplicateContactEmail
    Inherits BACRMPage

#Region "Member Variables"

    Private objContact As CContacts

#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblError.Text = ""
            divError.Style.Add("display", "none")

            If Not Page.IsPostBack Then
                Dim contactID As Long = CCommon.ToLong(GetQueryStringVal("contactID"))
                Dim email As String = CCommon.ToString(GetQueryStringVal("email"))
                Dim isFromInlineEdit As Boolean = CCommon.ToBool(GetQueryStringVal("isFromInlineEdit"))

                If Not String.IsNullOrEmpty(email) Then
                    hdnContactBeingEdited.Value = contactID
                    hdnEmailEnteredByUser.Value = email

                    objContact = New CContacts
                    objContact.DomainID = CCommon.ToLong(Session("DomainID"))
                    objContact.ContactID = contactID
                    objContact.Email = email
                    Dim dt As DataTable = objContact.GetContactByEmail()

                    If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                        lblContactName.Text = CCommon.ToString(dt.Rows(0)("vcContact"))
                        lblOrganizationName.Text = CCommon.ToString(dt.Rows(0)("vcCompanyName"))
                        hdnExistingContactWithSameEmail.Value = CCommon.ToLong(dt.Rows(0)("numContactID"))
                        hdnFromInlineEdit.Value = isFromInlineEdit
                        hdnEmailBeingEdited.Value = email
                    Else
                        DisplayError("No records found with same email. Close this window and try to save contact again.")
                        btnSaveClose.Visible = False
                    End If
                Else
                    DisplayError("Email address is required.")
                    btnSaveClose.Visible = False
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub DisplayError(ByVal errorMessage As String)
        Try
            lblError.Text = errorMessage
            divError.Style.Add("display", "")
        Catch ex As Exception
            'DO NOT THROW ERROR
        End Try
    End Sub

#End Region
    

    Private Sub btnSaveClose_Click(sender As Object, e As EventArgs) Handles btnSaveClose.Click
        Try
            If CCommon.ToBool(hdnFromInlineEdit.Value) Then
                If Not String.IsNullOrEmpty(txtChangeExistingAddress.Text) Then
                    objContact = New CContacts
                    objContact.DomainID = CCommon.ToLong(Session("DomainID"))
                    objContact.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objContact.ContactID = CCommon.ToLong(hdnExistingContactWithSameEmail.Value)
                    objContact.Email = txtChangeExistingAddress.Text

                    If objContact.IsDuplicateEmail() Then
                        DisplayError("New email address entered for existing contact already used by another contact(s).")
                    Else
                        objContact.ChangeEmailAddress()

                        objContact = New CContacts
                        objContact.DomainID = CCommon.ToLong(Session("DomainID"))
                        objContact.UserCntID = CCommon.ToLong(Session("UserContactID"))
                        objContact.ContactID = CCommon.ToLong(hdnContactBeingEdited.Value)
                        objContact.Email = hdnEmailBeingEdited.Value
                        objContact.ChangeEmailAddress()

                        Dim objWF As New BACRM.BusinessLogic.Workflow.Workflow()
                        objWF.DomainID = CCommon.ToLong(Session("DomainID"))
                        objWF.UserCntID = CCommon.ToLong(Session("UserContactID"))
                        objWF.RecordID = CCommon.ToLong(hdnContactBeingEdited.Value)
                        objWF.SaveWFContactQueue()

                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "CloseWindowAndRefreshParent", "CloseWindowAndRefreshParent()", True)
                    End If
                ElseIf Not String.IsNullOrEmpty(txtNewEmailAddress.Text) Then
                    objContact = New CContacts
                    objContact.DomainID = CCommon.ToLong(Session("DomainID"))
                    objContact.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objContact.ContactID = CCommon.ToLong(hdnContactBeingEdited.Value)
                    objContact.Email = txtNewEmailAddress.Text

                    If objContact.IsDuplicateEmail() Then
                        DisplayError("New email address entered for existing contact already used by another contact(s).")
                    Else
                        objContact.ChangeEmailAddress()

                        Dim objWF As New BACRM.BusinessLogic.Workflow.Workflow()
                        objWF.DomainID = CCommon.ToLong(Session("DomainID"))
                        objWF.UserCntID = CCommon.ToLong(Session("UserContactID"))
                        objWF.RecordID = CCommon.ToLong(hdnContactBeingEdited.Value)
                        objWF.SaveWFContactQueue()

                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "CloseWindowAndRefreshParent", "CloseWindowAndRefreshParent();", True)
                    End If
                Else
                    DisplayError("Enter new email address")
                End If

            Else
                If Not String.IsNullOrEmpty(txtChangeExistingAddress.Text) Then
                    objContact = New CContacts
                    objContact.DomainID = CCommon.ToLong(Session("DomainID"))
                    objContact.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objContact.ContactID = CCommon.ToLong(hdnExistingContactWithSameEmail.Value)
                    objContact.Email = txtChangeExistingAddress.Text

                    If objContact.IsDuplicateEmail() Then
                        DisplayError("New email address entered for existing contact already used by another contact(s).")
                    Else
                        objContact.ChangeEmailAddress()
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "CloseWindowAndSaveContact", "CloseWindowAndSaveContact()", True)
                    End If
                ElseIf Not String.IsNullOrEmpty(txtNewEmailAddress.Text) Then
                    objContact = New CContacts
                    objContact.DomainID = CCommon.ToLong(Session("DomainID"))
                    objContact.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objContact.ContactID = CCommon.ToLong(hdnContactBeingEdited.Value)
                    objContact.Email = txtNewEmailAddress.Text

                    If objContact.IsDuplicateEmail() Then
                        DisplayError("New email address entered for existing contact already used by another contact(s).")
                    Else
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "CloseWindowAndChangeEmail", "CloseWindowAndChangeEmail('" & txtNewEmailAddress.Text & "')", True)
                    End If
                Else
                    DisplayError("Enter new email address")
                End If
            End If

            
        Catch ex As Exception
            Throw
        End Try
    End Sub
End Class