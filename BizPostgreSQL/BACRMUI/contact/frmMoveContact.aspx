<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmMoveContact.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmMoveContact" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
    <title>Move Contact</title>

    <script language="javascript" src="../javascript/CompanyAssociations.js"></script>

</head>
<body>
    <form id="frmMoveContact" action="" method="post" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%" style="padding-top: 2px">
        <tr>
            <td style="width: 258px" valign="bottom" width="258">
                <table class="TabStyle">
                    <tr>
                        <td>
                            &nbsp;Move Contact&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
            <td class="normal1" align="right">
                <asp:Button ID="btnMergeContacts" CssClass="button" Width="90" Text="Move Contact"
                    runat="server"></asp:Button>&nbsp;
                <asp:Button ID="btnClose" CausesValidation="false" CssClass="button" Width="50" Text="Close"
                    runat="server"></asp:Button>&nbsp;
            </td>
        </tr>
    </table>
    <asp:Table ID="tblMoveContact" Width="100%" runat="server" BorderColor="black" GridLines="None"
        CssClass="aspTable" BorderWidth="1" CellPadding="2" CellSpacing="0" Height="170">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br />
                <table>
                    <tr>
                        <td class="normal1" align="right">
                            Organization
                        </td>
                        <td>
                            <asp:TextBox ID="txtCompany" runat="server" Width="110px" CssClass="signup"></asp:TextBox>&nbsp;
                            <asp:Button ID="btnSearchOrgByKeywords" runat="Server" Text="Go" Width="30" CssClass="button"
                                CausesValidation="false"></asp:Button>&nbsp;
                            <asp:DropDownList ID="ddlCompany" runat="server" Width="200" AutoPostBack="True"
                                CssClass="signup">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
   <%-- <asp:RequiredFieldValidator ID="rqValCompany" runat="server" ErrorMessage="Please select a Organization."
        Display="None" EnableClientScript="True" ControlToValidate="ddlCompany"></asp:RequiredFieldValidator>
    <asp:ValidationSummary ID="ValidationSummary" runat="server" DisplayMode="List" ShowMessageBox="True"
        ShowSummary="False" HeaderText="Please check the following value(s)"></asp:ValidationSummary>--%>
    <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
    <asp:Literal ID="litButtonMesg" runat="server" EnableViewState="False"></asp:Literal>
    </form>
</body>
</html>
