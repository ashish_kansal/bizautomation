Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin

Namespace BACRM.UserInterface.Contacts

    Public Class frmContactAddress
        Inherits BACRMPage
        Dim strAdd As String
        Dim lngContID As Long

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Protected WithEvents btnClose As System.Web.UI.WebControls.Button
        Protected WithEvents btnSaveClose As System.Web.UI.WebControls.Button
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                lngContID = CCommon.ToLong(GetQueryStringVal("pqwRT"))
                If Not IsPostBack Then
                     ' = Request.Url.Segments(Request.Url.Segments.Length - 1) ' = "Contacts"
                    

                    objCommon.sb_FillComboFromDBwithSel(ddlCountry, 40, Session("DomainID"))
                    BindAddressName()
                    Loadaddress()
                End If
                btnClose.Attributes.Add("onclick", "return Close()")

                If Not String.IsNullOrEmpty(hdnGoogleState.Value) AndAlso ddlState.SelectedItem.Text <> hdnGoogleState.Value Then
                    If Not ddlState.Items.FindByText(hdnGoogleState.Value) Is Nothing Then
                        ddlState.Items.FindByText(hdnGoogleState.Value).Selected = True
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub Loadaddress()
            Try
                Dim dtContactAddress As DataTable
                Dim objContact As New CContacts
                objContact.DomainID = Session("DomainID")
                objContact.AddressID = ddlAddressName.SelectedValue
                objContact.byteMode = 1
                objContact.AddresOf = CContacts.enmAddressOf.Organization
                dtContactAddress = objContact.GetAddressDetail

                If dtContactAddress.Rows.Count > 0 Then
                    txtAddressName.Text = CCommon.ToString(dtContactAddress.Rows(0)("vcAddressName"))
                    txtCity.Text = IIf(IsDBNull(dtContactAddress.Rows(0).Item("vcCity")), "", dtContactAddress.Rows(0).Item("vcCity"))
                    txtStreet.Text = IIf(IsDBNull(dtContactAddress.Rows(0).Item("vcStreet")), "", dtContactAddress.Rows(0).Item("vcStreet"))

                    If Not IsDBNull(dtContactAddress.Rows(0).Item("numCountry")) Then
                        ddlCountry.ClearSelection()
                        If dtContactAddress.Rows(0).Item("numCountry") = 0 Then dtContactAddress.Rows(0).Item("numCountry") = Session("DefCountry")
                        If Not ddlCountry.Items.FindByValue(dtContactAddress.Rows(0).Item("numCountry")) Is Nothing Then
                            ddlCountry.Items.FindByValue(dtContactAddress.Rows(0).Item("numCountry")).Selected = True
                        End If
                    End If
                    If ddlCountry.SelectedValue > 0 Then
                        ddlState.ClearSelection()
                        FillState(ddlState, ddlCountry.SelectedItem.Value, Session("DomainID"))
                        If Not IsDBNull(dtContactAddress.Rows(0).Item("numState")) Then
                            If Not ddlState.Items.FindByValue(dtContactAddress.Rows(0).Item("numState")) Is Nothing Then
                                ddlState.Items.FindByValue(dtContactAddress.Rows(0).Item("numState")).Selected = True
                            End If
                        End If
                    End If
                    txtPostal.Text = IIf(IsDBNull(dtContactAddress.Rows(0).Item("vcPostalCode")), "", dtContactAddress.Rows(0).Item("vcPostalCode"))
                    chkIsPrimary.Checked = CCommon.ToBool(dtContactAddress.Rows(0).Item("bitIsPrimary"))
                Else
                    txtAddressName.Text = ""
                    txtStreet.Text = ""
                    txtPostal.Text = ""
                    txtCity.Text = ""
                    ddlState.SelectedValue = 0
                    ddlCountry.SelectedValue = 0
                    chkIsPrimary.Checked = False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Save()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCountry.SelectedIndexChanged
            Try
                FillState(ddlState, ddlCountry.SelectedItem.Value, Session("DomainID"))

                If Not String.IsNullOrEmpty(hdnGoogleState.Value) Then
                    If Not ddlState.Items.FindByText(hdnGoogleState.Value) Is Nothing Then
                        ddlState.Items.FindByText(hdnGoogleState.Value).Selected = True
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub Save()
            Try
                Dim objContacts As New CContacts
                If txtAddressName.Text.Trim.Length > 1 Then
                    objContacts.AddressID = ddlAddressName.SelectedValue
                    objContacts.AddressName = txtAddressName.Text.Trim
                    objContacts.Street = txtStreet.Text.Trim
                    objContacts.City = txtCity.Text.Trim
                    objContacts.Country = ddlCountry.SelectedItem.Value
                    objContacts.PostalCode = txtPostal.Text.Trim
                    objContacts.State = ddlState.SelectedItem.Value
                    objContacts.AddresOf = CContacts.enmAddressOf.Contact
                    objContacts.AddressType = CContacts.enmAddressType.None
                    objContacts.IsPrimaryAddress = chkIsPrimary.Checked
                    objContacts.RecordID = lngContID
                    objContacts.DomainID = Session("DomainID")
                    objContacts.ManageAddress()

                    If CCommon.ToInteger(GetQueryStringVal("Reload")) = 2 Then
                        Dim strScript As String = "<script language='javascript'>window.opener.UpdateAddress(" & objContacts.AddressID & ");</script>"
                        If (Not ClientScript.IsStartupScriptRegistered("UpdateAddressScript")) Then ClientScript.RegisterStartupScript(Me.GetType, "UpdateAddressScript", strScript)
                    End If

                    BindAddressName()
                    Loadaddress()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                Save()
                Dim strScript As String = "<script language=JavaScript>self.close()</script>"
                If (Not ClientScript.IsStartupScriptRegistered("clientScript")) Then ClientScript.RegisterStartupScript(Me.GetType, "clientScript", strScript)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Private Sub BindAddressName()
            Try
                Dim objContact As New CContacts
                objContact.AddressType = CContacts.enmAddressType.None
                objContact.DomainID = Session("DomainID")
                objContact.RecordID = lngContID
                objContact.AddresOf = CContacts.enmAddressOf.Contact
                objContact.byteMode = 2
                ddlAddressName.DataValueField = "numAddressID"
                ddlAddressName.DataTextField = "vcAddressName"
                ddlAddressName.DataSource = objContact.GetAddressDetail()
                ddlAddressName.DataBind()
                ddlAddressName.Items.Add(New ListItem("--Add New--", "0"))

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub lbDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbDelete.Click
            Try
                If ddlAddressName.SelectedValue > 0 Then
                    Dim objContact As New CContacts
                    objContact.DomainID = Session("DomainID")
                    objContact.AddressID = ddlAddressName.SelectedValue
                    Try
                        objContact.DeleteAddress()
                    Catch ex As Exception
                        If ex.Message = "PRIMARY" Then
                            litMessage.Text = "Can not delete Primary Address!!"
                        Else
                            Throw ex
                        End If
                    End Try
                    BindAddressName()
                    Loadaddress()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlAddressName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAddressName.SelectedIndexChanged
            Try
                Loadaddress()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Sub FillState(ByVal ddl As DropDownList, ByVal lngCountry As Long, ByVal lngDomainID As Long)
            Try
                Dim dtTable As DataTable
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = lngDomainID
                objUserAccess.Country = lngCountry
                dtTable = objUserAccess.SelState
                ddl.DataSource = dtTable
                ddl.DataTextField = "vcState"
                ddl.DataValueField = "numStateID"
                ddl.DataBind()
                ddl.Items.Insert(0, "--Select One--")
                ddl.Items.FindByText("--Select One--").Value = "0"
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
    End Class
End Namespace
