''modified by anoop jayaraj
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Workflow

Partial Public Class newcontact
    Inherits BACRMPage
    Dim ContactId As Long
    

    Dim dtTableInfo As DataTable
    Dim objPageControls As New PageControls
    Dim objContacts As New LeadsIP
    Dim strFirstName As String = ""
    Dim strLastName As String = ""
    Dim strEmail As String = ""

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        Try
            CCommon.InitializeOrganizationClientSideTemplate(Session("DomainID"), Session("UserContactID"), radCmbCompany)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim strModuleName, strPermissionName As String
            Dim m_aryRightsForContact() As Integer = GetUserRightsForPage_Other(11, 1, strModuleName, strPermissionName)

            If m_aryRightsForContact(RIGHTSTYPE.ADD) = 0 Then
                Response.Redirect("../admin/authenticationpopup.aspx?mesg=AC&Module=" & strModuleName & "&Permission=" & strPermissionName)
                Exit Sub
            End If

            If Not IsPostBack Then
                radCmbCompany.Focus()

                If Session("FromDetail") IsNot Nothing Then
                    Dim strFrom As String() = Session("FromDetail")

                    If strFrom.Length > 0 Then
                        strFirstName = If(strFrom(0).Split(" ").Length > 0, CCommon.ToString(strFrom(0).Split(" ")(0)), "")
                        strLastName = If(strFrom(0).Split(" ").Length > 1, CCommon.ToString(strFrom(0).Split(" ")(1)), "")
                    End If

                    'If strFrom.Length > 1 Then strOrgName = If(strFrom(1).Split("@").Length > 1, CCommon.ToString(strFrom(1).Split("@")(1)), "")
                    'If strOrgName.Split(".").Length > 0 Then strOrgName = CCommon.ToString(strOrgName.Split(".")(0))
                    If strFrom.Length > 1 Then strEmail = CCommon.ToString(strFrom(1))
                End If

                Dim dtTab As DataTable
                dtTab = Session("DefaultTab")

                If Session("BtoGContact") = True Then
                    'tdGoogleContact.Visible = True
                End If

                If dtTab.Rows.Count > 0 Then
                    lbContacts.Text = "New " & IIf(IsDBNull(dtTab.Rows(0).Item("vcContact")), "Contacts", dtTab.Rows(0).Item("vcContact").ToString & "s")
                Else : lbContacts.Text = "New Contacts"
                End If
    
                If m_aryRightsForContact(RIGHTSTYPE.ADD) = 0 Then
                    btnSave.Visible = False
                    Exit Sub
                Else : btnSave.Visible = True
                End If

                If GetQueryStringVal("uihTR") <> "" Or GetQueryStringVal("fghTY") <> "" Or GetQueryStringVal("rtyWR") <> "" Or GetQueryStringVal("tyrCV") <> "" Or GetQueryStringVal("pluYR") <> "" Then
                    If GetQueryStringVal("uihTR") <> "" Then
                        objCommon.ContactID = CCommon.ToLong(GetQueryStringVal("uihTR"))
                        objCommon.charModule = "C"
                    ElseIf GetQueryStringVal("rtyWR") <> "" Then
                        objCommon.DivisionID = CCommon.ToLong(GetQueryStringVal("rtyWR"))
                        objCommon.charModule = "D"
                    ElseIf GetQueryStringVal("tyrCV") <> "" Then
                        objCommon.ProID = CCommon.ToLong(GetQueryStringVal("tyrCV"))
                        objCommon.charModule = "P"
                    ElseIf GetQueryStringVal("pluYR") <> "" Then
                        objCommon.OppID = CCommon.ToLong(GetQueryStringVal("pluYR"))
                        objCommon.charModule = "O"
                    ElseIf GetQueryStringVal("fghTY") <> "" Then
                        objCommon.CaseID = CCommon.ToLong(GetQueryStringVal("fghTY"))
                        objCommon.charModule = "S"
                    End If

                    objCommon.GetCompanySpecificValues1()
                    Dim strCompany As String
                    strCompany = objCommon.GetCompanyName
                    radCmbCompany.Text = strCompany
                    radCmbCompany.SelectedValue = objCommon.DivisionID
                End If

                LoadControls()
            End If
            btnSave.Attributes.Add("onclick", "return Save()")
            btnSaveNew.Attributes.Add("onclick", "return Save()")
            ' btnClose.Attributes.Add("onclick", "return Close()")

            If IsPostBack Then
                LoadControls()
            End If

            DisplayDynamicFlds()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub DisplayDynamicFlds()
        Try
            objPageControls.DisplayDynamicFlds(0, 0, Session("DomainID"), objPageControls.Location.Contact)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub LoadControls()
        Try
            tblMain.Controls.Clear()
            Dim ds As DataSet
            Dim objPageLayout As New CPageLayout
            Dim fields() As String

            objPageLayout.UserCntID = 0
            objPageLayout.DomainID = Session("DomainID")
            objPageLayout.PageId = 1
            objPageLayout.numRelCntType = 0
            objPageLayout.FormId = 10
            objPageLayout.PageType = 2

            ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
            dtTableInfo = ds.Tables(0)

            Dim intCol1Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=1")
            Dim intCol2Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=2")
            Dim intCol1 As Int32 = 0
            Dim intCol2 As Int32 = 0
            Dim tblCell As TableCell

            Dim tblRow As TableRow
            Const NoOfColumns As Short = 2
            Dim ColCount As Int32 = 0

            ''If intermediatory Page is enabled then Add the text "Name" to dropdown fields
            objPageControls.CreateTemplateRow(tblMain)
            For Each dr As DataRow In dtTableInfo.Rows
                If (dr("fld_type") <> "Label" And dr("fld_type") <> "Website") Then

                    If dr("vcPropertyName") = "ContactPhoneExt" Then
                        dr("vcPropertyName") = "PhoneExt"
                    End If
                    If dr("vcPropertyName") = "CellPhone" Then
                        dr("vcPropertyName") = "Cell"
                    End If
                    If dr("vcPropertyName") = "Sex" Then
                        dr("vcPropertyName") = "Gender"
                    End If

                    If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False Then
                        dr("vcValue") = objContacts.GetType.GetProperty(dr("vcPropertyName")).GetValue(objContacts, Nothing)

                        If Not IsDBNull(dr("vcPropertyName")) AndAlso CCommon.ToString(dr("vcPropertyName")) = "FirstName" Then
                            dr("vcValue") = strFirstName
                        ElseIf Not IsDBNull(dr("vcPropertyName")) AndAlso CCommon.ToString(dr("vcPropertyName")) = "LastName" Then
                            dr("vcValue") = strLastName
                        ElseIf Not IsDBNull(dr("vcPropertyName")) AndAlso CCommon.ToString(dr("vcPropertyName")) = "Email" Then
                            dr("vcValue") = strEmail
                        End If
                    End If

                    If ColCount = 0 Then
                        tblRow = New TableRow
                    End If

                    If intCol1Count = intCol1 And intCol2Count > intCol1Count Then
                        tblCell = New TableCell
                        tblCell.CssClass = "normal1"
                        tblRow.Cells.Add(tblCell)

                        tblCell = New TableCell
                        tblCell.CssClass = "normal1"
                        tblRow.Cells.Add(tblCell)
                        ColCount = 1
                    End If

                    If (dr("fld_type") = "ListBox" Or dr("fld_type") = "SelectBox") Then
                        Dim dtData As DataTable
                        'Dim dtSelOpportunity As DataTable

                        If Not IsDBNull(dr("vcPropertyName")) Then
                            If dr("vcPropertyName") = "Gender" Then ' If dr("numFieldId") = 23 Then
                                dtData = New DataTable
                                dtData.Columns.Add("Value")
                                dtData.Columns.Add("Text")
                                Dim dr1 As DataRow
                                dr1 = dtData.NewRow
                                dr1("Value") = "M"
                                dr1("Text") = "Male"
                                dtData.Rows.Add(dr1)

                                dr1 = dtData.NewRow
                                dr1("Value") = "F"
                                dr1("Text") = "Female"
                                dtData.Rows.Add(dr1)
                                'ElseIf dr("vcPropertyName") = "Manager" Then ' dr("numFieldId") = 6 Then
                                '        objCommon.ContactID = lngCntID
                                '        objCommon.charModule = "C"
                                '        objCommon.GetCompanySpecificValues1()
                                '        dtData = objCommon.GetManagers(Session("DomainID"), lngCntID, objCommon.DivisionID)
                            ElseIf dr("vcPropertyName") = "DripCampaign" Then ' dr("numFieldId") = 290 Then
                                Dim objCampaign As New Campaign
                                With objCampaign
                                    .SortCharacter = "0"
                                    .UserCntID = Session("UserContactID")
                                    .PageSize = 100
                                    .TotalRecords = 0
                                    .DomainID = Session("DomainID")
                                    .columnSortOrder = "Asc"
                                    .CurrentPage = 1
                                    .columnName = "vcECampName"
                                    dtData = objCampaign.ECampaignList
                                    Dim drow As DataRow = dtData.NewRow
                                    drow("numECampaignID") = -1
                                    drow("vcECampName") = "-- Disengaged --"
                                    dtData.Rows.Add(drow)
                                End With
                            ElseIf Not IsDBNull(dr("numListID")) And dr("numListID") <> 0 Then
                                If dr("ListRelID") > 0 Then
                                    objCommon.Mode = 3
                                    objCommon.DomainID = Session("DomainID")
                                    objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objContacts)
                                    objCommon.SecondaryListID = dr("numListId")
                                    dtData = objCommon.GetFieldRelationships.Tables(0)
                                Else
                                    dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                End If
                            End If
                        End If

                        Dim ddl As DropDownList
                        ddl = objPageControls.CreateCells(tblRow, dr, False, dtData, boolAdd:=True)
                        If CLng(dr("DependentFields")) > 0 And Not False Then
                            ddl.AutoPostBack = True
                            AddHandler ddl.SelectedIndexChanged, AddressOf PopulateDependentDropdown
                        End If
                    Else
                        objPageControls.CreateCells(tblRow, dr, False, RecordID:=0, boolAdd:=True)
                    End If

                    If intCol2Count = intCol2 And intCol1Count > intCol2Count Then
                        tblCell = New TableCell
                        tblCell.CssClass = "normal1"
                        tblRow.Cells.Add(tblCell)

                        tblCell = New TableCell
                        tblCell.CssClass = "normal1"
                        tblRow.Cells.Add(tblCell)

                        ColCount = 1
                    End If


                    If dr("intcoulmn") = 2 Then
                        If intCol1 <> intCol1Count Then
                            intCol1 = intCol1 + 1
                        End If

                        If intCol2 <> intCol2Count Then
                            intCol2 = intCol2 + 1
                        End If
                    End If

                    ColCount = ColCount + 1

                    If NoOfColumns = ColCount Then
                        ColCount = 0
                        tblMain.Rows.Add(tblRow)
                    End If
                End If
            Next

            If ColCount > 0 Then
                tblMain.Rows.Add(tblRow)
            End If

            'objPageControls.CreateComments(tblMain, objLeads.Comments, False)

            'Add Client Side validation for custom fields
            Dim strValidation As String = objPageControls.GenerateValidationScript(dtTableInfo)
            ClientScript.RegisterClientScriptBlock(Me.GetType, "CFvalidation", strValidation, True)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub PopulateDependentDropdown(ByVal sender As Object, ByVal e As EventArgs)
        objPageControls.PopulateDropdowns(CType(sender, DropDownList), dtTableInfo, objCommon, tblMain, Session("DomainID"))
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            CreateLead()

            'Added By Sachin Sadhu||Date:23rdJuly2014
            'Purpose :To Added Contact data in work Flow queue based on created Rules
            '         Using Change tracking
            Dim objWF As New Workflow()
            objWF.DomainID = Session("DomainID")
            objWF.UserCntID = Session("UserContactID")
            objWF.RecordID = ContactId
            objWF.SaveWFContactQueue()

            ' ss//end of code
            If ContactId > 0 Then
                SaveCusField()

                Dim strScript As String = "<script>"
                strScript += "opener.reDirectPage('../contact/frmContacts.aspx?frm=contactlist&uio78=09kji&CntId=" & ContactId & "'); self.close();"
                strScript += "</script>"
                If (Not Page.ClientScript.IsStartupScriptRegistered("clientScript")) Then Page.ClientScript.RegisterStartupScript(Me.GetType, "clientScript", strScript)
            End If
        Catch ex As Exception
            If ex.Message.Contains("DUPLICATE_EMAIL") Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "EmailValidation", "OpenEmailChangeWindow(" & objContacts.ContactID & ",'" & objContacts.Email & "');", True)
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Private Sub btnSaveNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveNew.Click
        Try
            CreateLead()
            If ContactId > 0 Then
                SaveCusField()
                Response.Redirect("../contact/newContact.aspx?rtyWR=" & radCmbCompany.SelectedValue)
            End If

        Catch ex As Exception
            If ex.Message.Contains("DUPLICATE_EMAIL") Then
                ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "EmailValidation", "OpenEmailChangeWindow(" & objContacts.ContactID & ",'" & objContacts.Email & "');", True)
            Else
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End If
        End Try
    End Sub

    Sub SaveCusField()
        Try
            objPageControls.SaveCusField(ContactId, 0, Session("DomainID"), objPageControls.Location.Contact, tblMain)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub CreateLead()
        Try
            With objContacts
                .DivisionID = radCmbCompany.SelectedValue
                .DomainID = Session("DomainID")
                .UserCntID = Session("UserContactID")

                For Each dr As DataRow In dtTableInfo.Rows
                    If (dr("fld_type") <> "Popup" And dr("fld_type") <> "Label" And dr("fld_type") <> "Website") Then
                        If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False And dr("bitCanBeUpdated") = True Then

                            If dr("vcPropertyName") = "ContactPhoneExt" Then
                                dr("vcPropertyName") = "PhoneExt"
                            End If
                            If dr("vcPropertyName") = "CellPhone" Then
                                dr("vcPropertyName") = "Cell"
                            End If
                            If dr("vcPropertyName") = "Sex" Then
                                dr("vcPropertyName") = "Gender"
                            End If
                            If dr("vcPropertyName") = "Comments" Then
                                dr("vcPropertyName") = "Notes"
                            End If

                            objPageControls.SetValueForStaticFields(dr, objContacts, tblMain)
                        End If
                    End If
                Next

                If Not String.IsNullOrEmpty(hdnChangedEmail.Value) Then
                    objContacts.Email = hdnChangedEmail.Value
                End If

                Dim objContactIp As New CContacts
                objContactIp.DomainID = objContacts.DomainID
                objContactIp.ContactID = 0
                objContactIp.Email = objContacts.Email

                'Right now we are allowing user to add duplicate contact email but in future we may want to restrict it
                'If objContactIp.IsDuplicateEmail() Then
                '    Throw New Exception("DUPLICATE_EMAIL")
                'End If

                If hdnIsDuplicate.Value = "0" Then
                    Dim strWhere As String = objContacts.GetStrWhere()
                    objContacts.WhereCondition = strWhere
                    If .CheckDuplicate() = True Then
                        hdnIsDuplicate.Value = "1"
                        litMessage.Text = "A Duplicate Contact Found!! Click Save to Continue"
                        Exit Sub
                    End If
                End If
                ContactId = .CreateRecordAddContactInfo
            End With

            If CCommon.ToLong(hdnAddressID.Value) > 0 Then
                Dim objAddressDetails As New AddressDetails
                objAddressDetails.DomainID = CCommon.ToLong(Session("DomainID"))
                objAddressDetails.AddressID = CCommon.ToLong(hdnAddressID.Value)
                objAddressDetails.RecordID = ContactId
                objAddressDetails.UpdateRecordID()
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub radCmbCompany_ItemsRequested(ByVal o As Object, ByVal e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs) Handles radCmbCompany.ItemsRequested
        Try
            If e.Text <> "" And Len(e.Text) >= IIf(Session("ChrForCompSearch") = 0, 1, Session("ChrForCompSearch")) Then
                If objCommon Is Nothing Then objCommon = New CCommon
                With objCommon
                    .DomainID = Session("DomainID")
                    .Filter = Trim(e.Text) & "%"
                    .UserCntID = Session("UserContactID")
                    radCmbCompany.DataSource = objCommon.PopulateOrganization.Tables(0).DefaultView
                    radCmbCompany.DataTextField = "vcCompanyname"
                    radCmbCompany.DataValueField = "numDivisionID"
                    radCmbCompany.DataBind()
                End With
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class
