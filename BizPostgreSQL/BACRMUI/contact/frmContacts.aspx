<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmContacts.aspx.vb"
    Inherits="BACRM.UserInterface.Contacts.frmContacts" MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register Src="../common/frmCorrespondence.ascx" TagName="frmCorrespondence" TagPrefix="uc1" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Src="../account/OpportunitiesTab.ascx" TagName="OpportunitiesTab" TagPrefix="uc1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <%-- <script src="../JavaScript/DirtyForm.js" type="text/javascript"></script>--%>
    <title>Contacts</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <script type="text/javascript">
        function openActionItem(a, b, c, d, e, f) {
            if (e == 'Email') {
                window.open("../outlook/frmMailDtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&numEmailHstrID=" + a, '', 'titlebar=no,top=100,left=250,width=850,height=550,scrollbars=yes,resizable=yes');
                return false;
            }
            else {
                window.location.href = "../admin/ActionItemDetailsOld.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactdetails&CommId=" + a + "&CaseId=" + b + "&CaseTimeId=" + c + "&CaseExpId=" + d;
                return false;
            }

        }

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function openAddress(CntID) {
            window.open("../contact/frmContactAddress.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&pqwRT=" + CntID, '', 'toolbar=no,titlebar=no,top=300,left=300,width=400,height=300,scrollbars=no,resizable=no')
            return false;
        }
        function openDrip(a) {
            window.open("../Marketing/frmConECampHstr.aspx?ConECampID=" + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=300,scrollbars=no,resizable=no')
            return false;
        }
        function OpenECamp(a) {
            window.open("../Marketing/frmConECamDtls.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&pqwRT=" + a, '', 'width=500,height=300,status=no,titlebar=no,scrollbar=yes,top=110,left=150')
            return false;
        }
        function ShowWindow(Page, q, att) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                window.location.reload(true);
                //return false;

            }

        }
        function ShowWindowAddress(Page, q, att) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                for (i = 60; i < tblMain.all.length; i++) {
                    if (tblMain.all[i] != null) {
                        if (tblMain.all[i].type == 'select-one') {
                            tblMain.all[i].style.visibility = "hidden";
                        }
                    }

                }
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                for (i = 60; i < tblMain.all.length; i++) {
                    if (tblMain.all[i] != null) {
                        if (tblMain.all[i].type == 'select-one') {
                            tblMain.all[i].style.visibility = "visible";
                        }
                    }

                }
                return false;

            }

        }
        function ShowWindow1(Page, q, att, a) {
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                if (a == 1) {
                    return true;
                }
                else {
                    return false;
                }


            }

        }
        function fn_SendMail(txtMailAddr, a, b) {
            var field = document.getElementById(txtMailAddr)
            if (field.value != '') {
                if (a == 1) {

                    window.open('mailto:' + field.value);
                }
                else if (a == 2) {
                    window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail=' + field.value + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
                }

            }

            return false;
        }
        /*Commented by Neelam - Obsolete function*/
        function OpenDocuments(a) {
            window.open("../Documents/frmSpecDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=C&yunWE=" + a, '', 'toolbar=no,titlebar=no,top=10,width=1000,height=450,left=10,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenTo(a) {
            window.open("../admin/frmcomAssociationTo.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DivId=" + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenFrom(a) {
            window.open("../admin/frmCompanyAssociationFrom.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&rtyWR=" + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenTransfer(url) {
            window.open(url, '', "width=340,height=150,status=no,top=100,left=150");
            return false;
        }
        function OpenLast10OpenAct(a) {
            window.open("../admin/DisplayActionItem.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&cntid=" + a + "&type=1", '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenLst10ClosedAct(a) {
            window.open("../admin/DisplayActionItem.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&cntid=" + a + "&type=2", '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function Disableddl() {
            if (Tabstrip2.selectedIndex == 0) {
                document.Form1.ddlOppStatus.style.display = "none";
            }
            else {
                document.Form1.ddlOppStatus.style.display = "";
            }
        }

        function EditSurveyResult(numSurId, numRespondentId) {
            var sURL = 'frmContactsSurveyResponses.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&numSurID=' + numSurId + '&numRespondentId=' + numRespondentId,
			hndSurveyResponsePopUpURL = window.open(sURL, '', 'toolbar=no,titlebar=no,left=190, top=350,width=800,height=270,scrollbars=yes,resizable=yes');
            hndSurveyResponsePopUpURL.focus();
        }
        function Save() {
            return validateCustomFields();
        }

        function OpenMergeCopyWindow(numContactId, frmScreen) {
            window.open("frmMoveContact.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=" + frmScreen + '&numContactId=' + numContactId, 'MoveContacts', 'toolbar=no,titlebar=no,top=300,width=500,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenTmeAndExp(a, b) {
            window.location.href = "../TimeAndExpense/frmEmpCal.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&CntID=" + a + "&frm=" + b;
            return false;
        }

        function GetSelectAllCheck() {
            var objCheck = document.getElementById('dgEmail__ctl1_selectAllOptions') // .net generated name is dgEmail__ctl1_selectAllOptions
            if (objCheck.checked == true) {
                SelectAllCheckBox();
                return true;
            }
            else {
                UnSelectAllCheckBox();
                return false;
            }
        }

        function SelectAllCheckBox() {
            var objAllCheck = document.getElementsByTagName('input');
            var totalCount = objAllCheck.length
            var i
            for (i = 0; i < totalCount; i++) {
                if (objAllCheck[i] != null && objAllCheck[i].type == 'checkbox') {
                    //if( objAllCheck[i].id.indexOf("listChecks") > = 0  ) 
                    {
                        objAllCheck[i].checked = true;
                    }
                }
            }
        }

        function UnSelectAllCheckBox() {
            var objAllCheck = document.getElementsByTagName('input');
            var totalCount = objAllCheck.length
            var i
            for (i = 0; i < totalCount; i++) {
                if (objAllCheck[i] != null && objAllCheck[i].type == 'checkbox') {
                    //if( objAllCheck[i].id.indexOf("listChecks") > = 0  ) 
                    {
                        objAllCheck[i].checked = false;
                    }
                }
            }
        }

        // use script below to Fire a pertiular button event just you need to pass 
        // event objet ------ we are using to identify key code 
        // Button Id that we have to click 
        function Click_Button(eventObject, buttonID) {
            var objButton = document.getElementById(buttonID);
            if (objButton != null && event.keyCode == 13) {
                objButton.click();
            }
        } // end function
        function ShowLayout(a, b, c, d) {
            window.open("../pagelayout/frmCustomisePageLayout.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Ctype=" + a + "&type=" + c + "&PType=3&FormId=" + d, '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
            return false;
        }

        $(document).ready(function () {
            $("#btnBrowse").click(function () {
                $("#thumbFile").click();
            });

            $("#imgContactThumb").dblclick(function () {
                $("#pnlUpload").show();
            });
        });


        function PreviewFile() {
            var x = document.getElementById("thumbFile");
            var txt = "";

            if (x.files && x.files[0]) {
                var file = x.files[0];
                var ext = file.name.substr(file.name.lastIndexOf('.') + 1);
                var size = file.size;
                if (ext != 'jpg' && ext != 'jpeg' && ext != 'png') {
                    alert("allowed file types are jpg, jpeg and png.");
                } else if (size > 102400) {
                    alert("Maximun allowed file size is 100Kb.");
                }
                else {

                    var data = new FormData();
                    data.append(file.name, file);
                    data.append("ContactID", '<%= hdnContactId.Value%>');
                    data.append("ActionType", '1');
                    data.append("ImageName", '<%= hdnImageFileName.Value%>');

                    var options = {};
                    options.url = "../common/ContactImage.ashx";
                    options.type = "POST";
                    options.data = data;
                    options.contentType = false;
                    options.processData = false;
                    options.success = function (result) {
                        if (result == "Success") {
                            $('#<%= imgContactThumb.ClientID%>').attr('src', '<%= hdnUploadPath.Value%>' + "Thumb_" + '<%= hdnContactId.Value%>' + '_' + file.name);
                            $('#<%= lnkDelete.ClientID %>').css("visibility", "");
                        } else {
                            alert("error occued while uploading image.");
                        }
                    };
                    options.error = function (err) {
                        alert("error occued while uploading image.");
                    };

                    $.ajax(options);
                }
        }
        else {
            alert("select image.");
        }

        $("#pnlUpload").hide();
        $("#thumbFile").val('');
    }

    function RemoveImage() {
        if (confirm("Image will be removed. Do you want to proceed?")) {
            var data = new FormData();
            data.append("ContactID", '<%= hdnContactId.Value%>');
            data.append("ActionType", '2');
            data.append("ImageName", '<%= hdnImageFileName.Value%>');

        var options = {};
        options.url = "../common/ContactImage.ashx";
        options.type = "POST";
        options.data = data;
        options.contentType = false;
        options.processData = false;
        options.success = function (result) {
            if (result == "Removed") {
                $('#<%= imgContactThumb.ClientID%>').attr('src', '../images/ContactPic.png');
                $('#<%= lnkDelete.ClientID %>').css("visibility", "hidden");
            } else {
                alert("error occued while removing image.");
            }
        };
        options.error = function (err) {
            alert("error occued while removing image.");
        };

        $.ajax(options);
    }
}

function OpenHelp() {
    window.open('../Help/Contact_Detail.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
    return false
}

        /*Function added by Neelam on 10/07/2017 - Added functionality to open Child Record window in modal pop up*/
        function OpenDocumentFiles(divID) {
            $find("<%= RadWinDocumentFiles.ClientID%>").show();
        }

        /*Function added by Neelam on 02/14/2018 - Added functionality to close document modal pop up*/
        function CloseDocumentFiles(mode) {
            debugger;
            var radwindow = $find('<%=RadWinDocumentFiles.ClientID %>');
            radwindow.close();

            if (mode == 'C')
            { return false; }
            else if (mode == 'L') {
                __doPostBack("<%= btnLinkClose.UniqueID%>", "");
            }
        }

        function DeleteDocumentOrLink(fileId) {
            if (confirm('Are you sure, you want to delete the selected item?')) {
                $('#hdnFileId').val(fileId);
                __doPostBack("<%= btnDeleteDocOrLink.UniqueID%>", "");
            }
            else {
                return false;
            }
        }
        $(document).ready(function ($) {
            requestStart = function (target, arguments) {
                if (arguments.get_eventTarget().indexOf("btnAttach") > -1) {
                    arguments.set_enableAjax(false);
                }
                if (arguments.get_eventTarget().indexOf("btnLinkClose") > -1) {
                    arguments.set_enableAjax(false);
                }
            }
        }
        );

    function OpenEmailChangeWindow(contactID, email) {
        window.open('../contact/frmDuplicateContactEmail.aspx?contactID=' + contactID + '&email=' + email, 'ContactChangeEmail', 'toolbar=no,titlebar=no,top=200,left=200,width=800,height=400,scrollbars=no,resizable=no')
        return false
    }

    function SaveAfterExistingContactEmailChange() {
        $('[id$=btnSaveClose]').click();
        return true;
    }

    function ChangeEmailAndSave(newEmail) {
        $('[id$=hdnChangedEmail]').val(newEmail);
        $('[id$=btnSaveClose]').click();
        return true;
    }
    </script>
    <style type="text/css">
        #tdDocument div{
            float:left;
            padding:10px;
        }
        .tblNoBorder td, .tblNoBorder th {
            border: 0px solid #fff !important;
        }
        
         #tblMain tr td{
            border-bottom:1px solid #e1e1e1 !important;
        }#tblMain tbody tr:first-child td{
            border-top:1px solid #fff !important;
            border-left:1px solid #fff !important;
            border-right:1px solid #fff !important;
        }
         .tableGroupHeader{
             background-color:#f5f5f5 !important;
             font-weight:bold;
         }
        @media (max-width:40em) {
            .tblNoBorder table, .tblNoBorder thead, .tblNoBorder tbody, .tblNoBorder tfoot, .tblNoBorder th, .tblNoBorder td, .tblNoBorder tr {
                display: block;
            }

            .tblNoBorder .editable_select {
                min-height: 30px;
            }

            .tblNoBorder td, .tblNoBorder th {
                text-align: left;
            }
        }

        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }

        div.ContactImage {
            position: relative;
            float: left;
            margin: 5px;
        }

            div.ContactImage a {
                position: absolute;
                display: none;
                top: 10px;
                right: 10px;
            }

            div.ContactImage:hover > img {
                opacity: 0.5;
            }

            div.ContactImage:hover a {
                display: block;
            }

            /*div.RadUpload .ruFakeInput {
            visibility: hidden;
            width: 0;
            padding: 0;
        }

        div.RadUpload .ruFileInput {
            width: 1;
        }*/

        .hidden {
            display: none;
        }

        .VerticalAligned {
            vertical-align: top;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
 <%--   <div class="row">
        <div class="col-md-12">
            <div class="col-sm-4">
                <div class="record-small-box bg-green">
                    <div class="inner">
                        <asp:Label ID="lblRecordOwner" runat="server"></asp:Label>
                    </div>
                    <asp:HyperLink ID="hplTransfer" CssClass="small-box-footer" runat="server" Visible="true" Text="Record Owner:"
                        ToolTip="Transfer Ownership">Record Owner <i class="fa fa-user"></i>
                    </asp:HyperLink>
                    <a href="#" id="hplTransferNonVis" runat="server" visible="false" class="small-box-footer">Record Owner <i class="fa fa-user"></i></a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="record-small-box bg-aqua">
                    <div class="inner">
                        <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
                    </div>
                    <a href="#" class="small-box-footer">Created By <i class="fa fa-user"></i></a>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="record-small-box bg-yellow">
                    <div class="inner">
                        <asp:Label ID="lblModifiedBy" runat="server"></asp:Label>
                    </div>
                    <a href="#" class="small-box-footer">Last Modified By <i class="fa fa-user"></i></a>
                </div>
            </div>
        </div>
    </div>--%>
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Button ID="btnDeleteDocOrLink" OnClick="btnDeleteDocOrLink_Click" runat="server" CssClass="hidden"></asp:Button>
    <asp:HiddenField runat="server" ID="hdnFileId" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-3">
                <div class="pull-left callout bg-theme">
                    <%--<strong>Candidates : </strong>--%>
                    <asp:HyperLink ID="hplCustomer" runat="server" CssClass="hyperlink" Font-Size="Larger">
                    </asp:HyperLink>
                </div>
            </div>
            <div class="col-xs-12 col-sm-5">

                <div class="record-small-box record-small-group-box createdBySingleSection">
                     <asp:HyperLink ID="hplTransfer" runat="server" Visible="true" Text="Owner:"
                        ToolTip="Transfer Ownership">Owner
                    </asp:HyperLink>
                    <a href="#" id="hplTransferNonVis" runat="server" visible="false" >Owner</a>
                    <span class="innerCreated">
                        <asp:Label ID="lblRecordOwner" runat="server"></asp:Label>
                    </span>
                    <a href="#">Created</a>
                    <span class="innerCreated">
                        <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
                    </span>
                    <a href="#">Modified</a>
                    <span class="innerCreated">
                        <asp:Label ID="lblModifiedBy" runat="server"></asp:Label>
                    </span>
                </div>

        </div>
            <div class="col-md-4">
            <div class="pull-right">
                <asp:LinkButton ID="btnActionItem" runat="server" CssClass="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New Action Item</asp:LinkButton>
                <asp:LinkButton ID="btnTimeExp" Visible="false" runat="server" CssClass="btn btn-primary"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;Time &amp; Expense</asp:LinkButton>
                <asp:LinkButton ID="btnMerge" Visible="false" runat="server" CssClass="btn btn-primary"><i class="fa fa-compress"></i>&nbsp;&nbsp;Move Contact</asp:LinkButton>
                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" style="font-family:'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif, FontAwesome;" Text="&#xf0c7;&nbsp;&nbsp;Save"></asp:Button>
                <asp:Button ID="btnSaveClose" runat="server" CssClass="btn btn-primary" style="font-family:'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif, FontAwesome;" Text="&#xf0c7;&nbsp;&nbsp;Save & Close"></asp:Button>
                <asp:LinkButton ID="btnEdit" runat="server" CssClass="btn btn-primary"><i class="fa fa-pencil-square-o"></i>&nbsp;&nbsp;Edit</asp:LinkButton>
                <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-primary"><i class="fa fa-times"></i>&nbsp;&nbsp;Cancel</asp:LinkButton>
                <asp:LinkButton ID="btnActDelete" runat="server" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
                <asp:HiddenField ID="hdnChangedEmail" runat="server" />
            </div>
                </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <telerik:RadTabStrip ID="radOppTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
        Skin="Default" ClickSelectedTab="True" AutoPostBack="True" SelectedIndex="0"
        MultiPageID="radMultiPage_OppTab">
        <Tabs>
            <telerik:RadTab Text="&nbsp;&nbsp;Contact Details&nbsp;&nbsp;" Value="Details" PageViewID="radPageView_Details">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Areas of Interest&nbsp;&nbsp;" Value="AreasOfInterest"
                PageViewID="radPageView_AreasOfInterest">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Orders&nbsp;&nbsp;" Value="Opportunities" PageViewID="radPageView_Opportunities">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Survey History&nbsp;&nbsp;" Value="History" PageViewID="radPageView_History">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Correspondence&nbsp;&nbsp;" Value="Correspondence"
                PageViewID="radPageView_Correspondence">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage_OppTab" runat="server" SelectedIndex="0" CssClass="pageView">
        <telerik:RadPageView ID="radPageView_Details" runat="server">
             <div class="table-responsive">
            <asp:Table ID="tblProspects" CellPadding="0" CellSpacing="0" Height="300" runat="server"
                Width="100%" GridLines="None">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <br />
                        <table width="100%">
                            <tr>
                                <td colspan="2" align="right">
                                    <div class="btn btn-default btn-sm" style="float: right" runat="server" id="btnLayout">
                                        <span><i class="fa fa-columns"></i>&nbsp;&nbsp;Layout</span>
                                    </div>
                                    <div style="float: right; text-align: center;">
                                            <table width="100%" border="0" style="padding-right: 5px;">
                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <img id="imgDocument" runat="server" src="../images/icons/AttachSmall.png" alt="" />
                                                    </td>
                                                    <td style="text-align: left;" runat="server" id="tdDocument"></td>
                                                    <td>
                                                        <asp:ImageButton ID="imgOpenDocument" runat="server" ImageUrl="~/images/icons/drawer.png" Style="height: 30px;" />
                                                    </td>
                                                    <td style="text-align: left;">
                                                        <asp:HyperLink ID="hplOpenDocument" runat="server" Font-Bold="true" Font-Underline="true" Text="Attach or Link" Style="display: inline-block; vertical-align: top; line-height: normal;"></asp:HyperLink>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;">
                                                        <img src="../images/icons/LinkSmall.png" id="imgLink" runat="server">
                                                    </td>
                                                    <td style="text-align: left;" runat="server" id="tdLink"></td>
                                                </tr>
                                            </table>
                                            <%--<asp:HyperLink ID="hplDocumentCount" runat="server" Style="display: inline-block; vertical-align: middle; line-height: normal;"></asp:HyperLink>
                                        <asp:ImageButton ID="imgOpenDocument" runat="server" ImageUrl="~/images/icons/drawer.png" />--%>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" style="width: 210px">
                                    <div class="ContactImage">
                                        <asp:Image ID="imgContactThumb" runat="server" Width="170" Height="200" BorderStyle="Solid" BorderColor="Gray" BorderWidth="1" />
                                        <a id="lnkDelete" onclick="RemoveImage();" runat="server" style="visibility: hidden" title="Remove Image">
                                            <img src="../images/delete2.gif" alt="Remove Image" />
                                        </a>
                                    </div>

                                    <asp:Panel ID="pnlUpload" runat="server" Style="text-align: center; display: none">
                                        <br />
                                        <label style="text-align: center; font-weight: bold">(170 X 200)</label>
                                        <br />
                                        <input id="thumbFile" clientidmode="Static" type="file" size="30" onchange="PreviewFile()" style="width: 170px; display: none;" />
                                        <input id="btnBrowse" type="button" value="Browse..." />
                                    </asp:Panel>
                                    <asp:HiddenField ID="hdnImageFileName" runat="server" />
                                    <asp:HiddenField ID="hdnContactId" runat="server" />
                                    <asp:HiddenField ID="hdnUploadPath" runat="server" />
                                </td>
                                <td valign="top" width="100%">
                                    <asp:Table ID="tblMain" CellPadding="3" CellSpacing="3" align="center" Width="100%"
                                        GridLines="none" border="0" runat="server" CssClass="table table-responsive tblNoBorder">
                                    </asp:Table>
                                </td>
                            </tr>
                        </table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
                 </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_AreasOfInterest" runat="server">
             <div class="table-responsive">
            <asp:Table ID="Table2" runat="server" GridLines="None" Height="300" Width="100%">
                <asp:TableRow>
                    <asp:TableCell CssClass="normal1" VerticalAlign="Top">
                        <br>
                            <asp:CheckBoxList ID="chkAOI" runat="server" CellSpacing="20" RepeatColumns="3" RepeatDirection="Vertical">
                            </asp:CheckBoxList>
                        </br>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
                 </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Opportunities" runat="server">
             <div class="table-responsive">
            <asp:Table ID="Table5" CssClass="table table-responsive" runat="server" CellPadding="0" CellSpacing="0" GridLines="None" Height="300" Width="100%">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <uc1:OpportunitiesTab ID="OpportunitiesTab1" runat="server" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
                 </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_History" runat="server">
            <div class="table-responsive">
                <asp:Table ID="Table8" runat="server" CellPadding="0" CellSpacing="0" GridLines="None"
                    Height="300" Width="100%">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right" VerticalAlign="Top">
                            <asp:Literal ID="litClientMessageSurveyHistory" runat="server"></asp:Literal>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblNextSurveyHistory" runat="server" CssClass="Text_bold">Next:</asp:Label>
                                    </td>
                                    <td class="normal1">
                                        <asp:LinkButton ID="lnk2SurveyHistory" runat="server" CausesValidation="False">2</asp:LinkButton>
                                    </td>
                                    <td class="normal1">
                                        <asp:LinkButton ID="lnk3SurveyHistory" runat="server" CausesValidation="False">3</asp:LinkButton>
                                    </td>
                                    <td class="normal1">
                                        <asp:LinkButton ID="lnk4SurveyHistory" runat="server" CausesValidation="False">4</asp:LinkButton>
                                    </td>
                                    <td class="normal1">
                                        <asp:LinkButton ID="lnk5SurveyHistory" runat="server" CausesValidation="False">5</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkFirstSurveyHistory" runat="server" CausesValidation="False">
                                                    <div class="LinkArrow">
                                                        &lt;&lt;</div>
                                        </asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkPreviousSurveyHistory" runat="server" CausesValidation="False">
                                                    <div class="LinkArrow">
                                                        &lt;</div>
                                        </asp:LinkButton>
                                    </td>
                                    <td class="normal1">
                                        <asp:Label ID="lblPageSurveyHistory" runat="server">Page</asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCurrentPageSurveyHistory" runat="server" AutoPostBack="true"
                                            CssClass="signup" MaxLength="5" Text="1" Width="28px">
                                        </asp:TextBox>
                                    </td>
                                    <td class="normal1">
                                        <asp:Label ID="lblOfSurveyHistory" runat="server">of</asp:Label>
                                    </td>
                                    <td class="normal1">
                                        <asp:Label ID="lblTotalSurveyHistory" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkNextSurveyHistory" runat="server" CausesValidation="False"
                                            CssClass="LinkArrow">
                                                    <div class="LinkArrow">
                                                        &gt;</div>
                                        </asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkLastSurveyHistory" runat="server" CausesValidation="False">
                                                    <div class="LinkArrow">
                                                        &gt;&gt;</div>
                                        </asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                            <asp:DataGrid ID="dgSurvey" runat="server" AlternatingItemStyle-CssClass="ais" AutoGenerateColumns="False"
                                BorderWidth="1px" CellPadding="2" CellSpacing="0" CssClass="dg" DataKeyField="numSurID"
                                HeaderStyle-CssClass="hs" ItemStyle-CssClass="is" ShowHeader="true" Width="100%">
                                <Columns>
                                    <asp:BoundColumn DataField="numSurID" HeaderText="Survey ID" ItemStyle-Width="70"
                                        Visible="true"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numRespondantID" HeaderText="Respondent ID" ItemStyle-Width="70"
                                        Visible="False"></asp:BoundColumn>
                                    <%--<asp:BoundColumn Visible="true" HeaderText="Date Created" DataField="dateCreatedOn" ItemStyle-Width="100"></asp:BoundColumn>--%>
                                    <asp:TemplateColumn HeaderText="Date Created" SortExpression="dateCreatedOn">
                                        <ItemTemplate>
                                            <%#ReturnName(DataBinder.Eval(Container.DataItem, "dateCreatedOn"))%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="vcSurName" HeaderText="Survey Name" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="300" Visible="true"></asp:BoundColumn>
                                    <asp:HyperLinkColumn DataNavigateUrlField="numContactSurResponseLink" DataNavigateUrlFormatString="javascript:EditSurveyResult({0});"
                                        HeaderText="Survey Results" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100"
                                        Text="See Results"></asp:HyperLinkColumn>
                                    <asp:BoundColumn DataField="numSurRating" HeaderText="Survey Rating" ItemStyle-Width="100"
                                        SortExpression="numSurRating" Visible="true"></asp:BoundColumn>
                                    <asp:TemplateColumn ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25">
                                        <ItemTemplate>
                                            <asp:Button ID="btnSurveyHistoryDelete" runat="server" CausesValidation="False" CommandName="Delete"
                                                CssClass="button Delete" OnClick="btnSurveyHistoryDeleteAction_Command" Text="X"
                                                Visible="true" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Correspondence" runat="server">
            <div class="table-responsive">
                <asp:Table ID="Table4" runat="server" CellPadding="0" CellSpacing="0" GridLines="None"
                    Height="300" Width="100%">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <uc1:frmCorrespondence ID="Correspondence1" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <table width="100%">
    </table>
    <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="hidEml" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="hidCompName" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtEmailTotalPage" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtEmailTotalRecords" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtCorrTotalPage" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtCorrTotalRecords" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtContactType" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtItemId" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtChangeKey" Style="display: none" runat="server">
    </asp:TextBox>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Contact Details &nbsp;<a href="#" onclick="return OpenHelpPopUp('contact/frmcontacts.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    <asp:LinkButton runat="server" ID="lbtnFav" CssClass="btn btn-default btn-sm"><i class="fa fa-heart"></i>&nbsp;&nbsp;Add to Favorites
    </asp:LinkButton>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                    <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                        <i class="fa fa-2x fa-refresh fa-spin"></i>
                        <h3>Processing Request</h3>
                    </div>
                </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <telerik:RadWindow RenderMode="Lightweight" runat="server" ID="RadWinDocumentFiles" Title="Documents / Files" RestrictionZoneID="ContentTemplateZone" Modal="true" Behaviors="Resize,Close,Move" Width="800" Height="400">
        <ContentTemplate>
            <asp:UpdatePanel ID="Updatepanel2" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div>
                        <table border="0" style="width: 100%; text-align: right; padding-right: 5px;">
                            <tr>
                                <td>
                                    <input type="button" id="btnCloseDocumentFiles" onclick="return CloseDocumentFiles('C');" class="btn btn-primary" style="color: white" value="Close" /></td>
                            </tr>
                        </table>                        
                        <div class="col-xs-12" style="padding-top: 40px;">
                            <div class="form-inline">
                                <table border="0" style="width: 100%;">
                                    <tr>
                                        <td>
                                            <img src="../images/icons/Attach.png"></td>
                                        <td style="font-size: small; font-weight: bold; padding-left: 2px; padding-right: 2px;">Upload a Document from your Desktop</td>
                                        <td colspan="2">
                                            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" ClientEvents-OnRequestStart="requestStart" LoadingPanelID="LoadingPanel1">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <%--<telerik:RadAsyncUpload RenderMode="Lightweight" runat="server" Width="320px" CssClass="async-attachment" ID="rdUploadFile" HideFileInput="true" Localization-Select="Choose File" AutoAddFileInputs="false" />--%>
                                                        <input class="signup" id="fileupload" type="file" name="fileupload" runat="server">
                                                        </td>
                                                        <td>
                                                            <asp:Button ID="btnAttach" OnClick="btnAttach_Click" runat="server" Text="Attach & Close" CssClass="btn btn-primary"></asp:Button>
                                                            <asp:Button ID="btnLinkClose" OnClick="btnLinkClose_Click" runat="server" CssClass="hidden"></asp:Button></td>
                                                    </tr>
                                                </table>
                                            </telerik:RadAjaxPanel>
                                            <telerik:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server" InitialDelayTime="0" Skin="Default">
                                            </telerik:RadAjaxLoadingPanel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <img src="../images/icons/Link.png">&nbsp;</td>
                                        <td style="font-size: small; font-weight: bold; padding-left: 2px; padding-right: 2px;">Link to a Document</td>
                                        <td>
                                            <telerik:RadTextBox runat="server" ID="txtLinkName" CssClass="inlineFormInput" Width="250px" EmptyMessage="Link Name"></telerik:RadTextBox></td>
                                        <td>
                                            <asp:Button ID="btnLink" runat="server" OnClientClick="return CloseDocumentFiles('L');" Text="Link & Close" CssClass="btn btn-primary"></asp:Button></td>
                                        <td>
                                            <asp:Label ID="Label1" Text="[?]" CssClass="tip" runat="server" ToolTip="Linking to a file on Google Drive or any other file repository is a good way to preserve your BizAutomation storage space. 
To avoid login credentials when clicking the  link, just make it public (e.g. On Google Drive you have to make sure that �Link Sharing� is set to �Public on the web�)." /></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;&nbsp;</td>
                                    </tr>
                                    <tr style="text-align: left;">
                                        <td colspan="2"></td>
                                        <td colspan="2">
                                            <telerik:RadTextBox RenderMode="Lightweight" runat="server" ID="txtLinkURL" Width="200px" EmptyMessage="Paste link here" TextMode="MultiLine" Height="100px" Resize="None"></telerik:RadTextBox>
                                        </td>
                                    </tr>
                                </table>                                
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </telerik:RadWindow>
</asp:Content>
