<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" ValidateRequest="false"
    CodeBehind="frmComposeWindow.aspx.vb" Inherits="BACRM.UserInterface.Contacts.frmComposeWindow" MasterPageFile="~/common/PopupBootstrap.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Compose Message</title>
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <%--<script type="text/javascript" src="../JavaScript/jquery.min.js"></script>--%>
    <script src="../JavaScript/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../JavaScript/jquery.tokeninput.js"></script>
    <link href="../Styles/token-input.css" rel="Stylesheet" type="text/css" />
    <link href="../Styles/token-input-facebook.css" rel="Stylesheet" type="text/css" />
    <%--<script type="text/javascript" src="../JavaScript/jquery-ui.min.js"></script>--%>

    <link href="../CSS/GridColorScheme.css" rel="stylesheet" />
    <style>
        .hyperLinkGrid{
            color:#000 !important;
            text-decoration:underline !important;
        }
        .Delete{
    color: #f00 !important;
    font: bold 14px/15px Verdana,Arial,Helvetica,sans-serif !important;
}
        .button {
    -moz-box-shadow: 0 1px 0 0 #c7c9cf;
    -webkit-box-shadow: 0 1px 0 0 #c7c9cf;
    box-shadow: 0 1px 0 0 #c7c9cf;
    background: -webkit-gradient(linear,left top,left bottom,color-stop(0.05,#fdfefe),color-stop(1,#dce3ea));
    background: -moz-linear-gradient(center top,#fdfefe 5%,#dce3ea 100%);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#fdfefe',endColorstr='#dce3ea');
    background-color: #fdfefe;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    border: 1px solid #cadbed;
    display: inline-block;
    padding: 2px 7px;
    text-decoration: none;
    text-shadow: 1px 1px 3px #cadbed;
    cursor: pointer;
}
    </style>
    <script language="javascript" type="text/javascript">

        var saved_tokens = [];
        var CompanyName = "";
        // var saved_tokensCC = [];
        var hdnTo = [];
        var token_count = 0;
        var maruti = 0;


        $(document).ready(function () {
            $('#txtTo').change(
                function () {
                    data = $(this).val();
                    var arr = data.split('~');
                    var numContactId = arr[2];


                    $.ajax({
                        type: "POST",
                        url: "../common/Common.asmx/FetchAutomatedFollowUpDetails",
                        data: "{numContactId:'" + numContactId + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: OnSuccess,
                        error: function (msg) {
                            alert(msg.d);
                        }
                    });

                    function OnSuccess(response) {

                        $('#lblFollowupCampaign').html(response.d[0]);
                        $('#lblLastFollowUp').html(response.d[1]);
                        $('#lblNextFollowUp').html(response.d[2]);
                    }

                });

            $("#aCC").on('click', function (e) {
                $('#divCC').show();
                $('#divACC').hide();
                LoadToContacts(2, "divCCContacts");
                $("#token-input-txtcc").focus();
            });

            $("#aBCC").on('click', function (e) {
                $('#divBCC').show();
                $('#divABCC').hide();
                LoadToContacts(3, "divBCCContacts");
                $("#token-input-txtBcc").focus();
            });

            $('body').on('click', function (e) {
                if (("," + $("#txtcc").val() + ",").indexOf(",cc~") == -1 && $("#divCC").has(e.target).length == 0 && (e.target == null || e.target.id != "aCC") && $("#divCC").is(":visible")) {
                    $("#divACC").show();
                    $("#divCC").hide();
                }

                if (("," + $("#txtBcc").val() + ",").indexOf(",bcc~") == -1 && $("#divBCC").has(e.target).length == 0 && (e.target == null || e.target.id != "aBCC") && $("#divBCC").is(":visible")) {
                    $("#divABCC").show();
                    $("#divBCC").hide();
                }

                if ($("#divCCContactsWrapper").has(e.target).length == 0) {
                    $("#divCCContacts").hide();
                }

                if ($("#divBCCContactsWrapper").has(e.target).length == 0) {
                    $("#divBCCContacts").hide();
                }                
            });
        });

        function LoadToContacts(mode,div) {
            if ($("#txtTo").val() != "") {
                var arrContacts = $("#txtTo").val().split(',');
                var contactIds = "";
                arrContacts.forEach(function (item, index) {
                    if (item.split("~").length >= 3) {
                        if (parseInt(item.split("~")[2]) > 0) {
                            contactIds = contactIds + (contactIds.length > 0 ? "," : "") + item.split("~")[2];
                        }
                    }
                });

                if (contactIds.length > 0) {

                    $.ajax({
                        type: "POST",
                        url: '../WebServices/CommonService.svc/GetOtherContactsFromOrganization',
                        contentType: "application/json",
                        dataType: "json",
                        data: JSON.stringify({
                            "mode":mode
                            , "contactIds": contactIds
                        }),
                        success: function (data) {
                            try {
                                var html = data.GetOtherContactsFromOrganizationResult;
                                if (html != null && html.length > 0) {
                                    $("#" + div).html(html);
                                    $("#" + div).show();
                                }
                            } catch (err) {
                                alert("Unknown error occurred.");
                            }
                        }
                    });
                }
            }
            
        }

        function OtherContactSelectionChanged(mode,chk) {
            if ($(chk).is(":checked")) {
                InsertEmail(mode, $(chk).attr("id"))
            } else {
                var Arr1 = $(chk).attr("id").split("~");

                if (mode == 3) {
                    $("#txtBcc").tokenInput("remove", { id: Arr1[1], first_name: Arr1[2], last_name: Arr1[3], email: Arr1[0] });
                } else {
                    $("#txtcc").tokenInput("remove", { id: Arr1[1], first_name: Arr1[2], last_name: Arr1[3], email: Arr1[0] });
                }
            }

            return false;
        }

        function OpenSignature() {
            window.open('../Marketing/frmAddSignature.aspx', '', 'toolbar=no,titlebar=no,left=300, top=100,width=305,height=280,scrollbars=no,resizable=yes');
            return false;
        }
        function OpenContactList() {
            //Added By Sachin Sadhu||Date:12Dec13
            //Added Company Querystring param to filter comp wise contacts
            var data = $('#txtTo').val();
            var arr = data.split('~');
            var arrValidate = data.split(',');
            var CommpanyID = arr[1];

            if (arrValidate.length == 1) {
                CommpanyID = arr[1];
            }
            else {
                CommpanyID = null;
            }
            window.open('../contact/frmGetEmailAdd.aspx?Company=' + CommpanyID + '', '', 'width=650,height=350,status=no,titlebar=no,scrollbars=1,resizable=true,top=110,left=250')

            return false;
        }
        //Added By Sachin Sadhu||Date:12Dec13
        //Purpose:Pass Company name  to Querystring
        function getvalue(strCompanyName) {

            CompanyName = strCompanyName.toString();
            return false;
        }
        //End of Code By Sachin
        function InsertEmail(a, strID) {
            var ArrEmail = strID.toString().split("|");
            var Arr1;
            for (i = 0; i < ArrEmail.length; ++i) {
                if (ArrEmail[i].length > 0) {
                    Arr1 = ArrEmail[i].split("~");
                    if (echeck(trim(Arr1[0])) == true) {
                        if (a == 1) {
                            $("#txtTo").tokenInput("add", { id: Arr1[1], first_name: Arr1[2], last_name: Arr1[3], email: Arr1[0] });
                        }
                        else if (a == 2) {
                            $("#txtcc").tokenInput("add", { id: Arr1[1], first_name: Arr1[2], last_name: Arr1[3], email: Arr1[0] });
                        }
                        else if (a == 3) {
                            $("#txtBcc").tokenInput("add", { id: Arr1[1], first_name: Arr1[2], last_name: Arr1[3], email: Arr1[0] });
                        }
                    }
                }
            }
        }
        function OpenAttachments(c, a, b, d) {
            var data = $('#txtTo').val();
            if (data.length > 0) {
                //console.log(data);
                var arr = data.split('~');

                if (arr.length > 2 && arr[2] > 0) {
                    a = arr[2];
                }
            }

            window.open('../Marketing/frmAddAttachments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ContId=' + a + '&compId=' + b + '&DivId=' + c + '&OppId=' + d, '', 'toolbar=no,titlebar=no,left=300, top=100,width=750,height=500,scrollbars=yes,resizable=no');
            return false;
        }
        function AttachmentDetails(a) {
            if (document.all) {
                document.getElementById('lblAttachents').innerText = '';
                document.getElementById('lblAttachents').innerText = a;
            } else {
                document.getElementById('lblAttachents').textContent = '';
                document.getElementById('lblAttachents').textContent = a;
            }
            return false;
        }
        function Send() {
            PopulateValuesToHiddenFields();

            if (trim(document.getElementById('hdnMessageTo').value) == '') {
                alert('Please specify at least one recipient');
                return false;
            }

            if (trim(document.getElementById('txtSubject').value) == '') {
                alert('Please specify subject');
                document.getElementById('txtSubject').focus();
                return false;
            }

            if (document.getElementById('hdnIsEmptyAttachment').value == '1') {
                if (confirm("BizDoc pdf is not attached to an email. Do want to proceed?")) {
                    return true;
                }
                else {
                    return false;
                }
            } else {
                return true;
            }
        }

        function PopulateValuesToHiddenFields() {
            //debugger;

            var i = 0;
            var txtType
            var ToElements = document.getElementById('lblTo');
            document.getElementById('hdnTo').value = '';
            document.getElementById('hdnSelectedEmails').value = '';
            document.getElementById('hdnpreData').value = '[';
            document.getElementById('hdnMessageTo').value = ''
            var CCElements = document.getElementById('lblCC');
            document.getElementById('hdnCC').value = '';
            document.getElementById('hdnSelectedEmailsCC').value = '';
            document.getElementById('hdnpreDataCC').value = '[';
            //var Json =[;
            //console.log(token_count);
            for (i = 0; i < token_count; ++i) {
                //alert(ToElements.childNodes[i].name);alert(ToElements.childNodes[i].innerHTML);
                txtType = saved_tokens[i].id.split('~')[0]
                if (txtType == "to") {
                    document.getElementById('hdnMessageTo').value = document.getElementById('hdnMessageTo').value + saved_tokens[i].id.split('~')[3] + "$^$" + saved_tokens[i].id.split('~')[1] + '#^#';
                    document.getElementById('hdnTo').value = document.getElementById('hdnTo').value + saved_tokens[i].id.split('~')[1] + ' , ';  //ToElements.childNodes[i].innerHTML + ' , ';
                    document.getElementById('hdnSelectedEmails').value = document.getElementById('hdnSelectedEmails').value + saved_tokens[i].id + '|' //ToElements.childNodes[i].innerHTML + '~' + ToElements.childNodes[i].name + '|';
                    document.getElementById('hdnpreData').value = document.getElementById('hdnpreData').value + '{id :' + '"' + saved_tokens[i].id.split('~')[2] + '"' + ',first_name:' + '"' + saved_tokens[i].id.split('~')[3] + '"' + ',last_name :' + '"' + saved_tokens[i].id.split('~')[4] + '"' + ',email:' + '"' + saved_tokens[i].id.split('~')[1] + '"' + ',url:' + '"#"},';
                }
                else if (txtType == "cc") {
                    document.getElementById('hdnMessageCC').value = document.getElementById('hdnMessageCC').value + saved_tokens[i].id.split('~')[3] + "$^$" + saved_tokens[i].id.split('~')[1] + '#^#';
                    document.getElementById('hdnCC').value = document.getElementById('hdnCC').value + saved_tokens[i].id.split('~')[1] + ' , '; // document.getElementById('hdnCC').value + CCElements.childNodes[i].innerHTML + ' , ';

                    document.getElementById('hdnSelectedEmailsCC').value = document.getElementById('hdnSelectedEmailsCC').value + saved_tokens[i].id + '|' //CCElements.childNodes[i].innerHTML + '~' + CCElements.childNodes[i].name + '|';
                    document.getElementById('hdnpreDataCC').value = document.getElementById('hdnpreDataCC').value + '{id :' + '"' + saved_tokens[i].id.split('~')[2] + '"' + ',first_name:' + '"' + saved_tokens[i].id.split('~')[3] + '"' + ',last_name :' + '"' + saved_tokens[i].id.split('~')[4] + '"' + ',email:' + '"' + saved_tokens[i].id.split('~')[1] + '"' + ',url:' + '"#"},';
                }

            }
            if (document.getElementById('hdnpreData').value.length > 1) {
                document.getElementById('hdnTo').value = document.getElementById('hdnTo').value.substring(0, document.getElementById('hdnTo').value.length - 1)
                document.getElementById('hdnpreData').value = document.getElementById('hdnpreData').value.substring(0, document.getElementById('hdnpreData').value.length - 1)
                document.getElementById('hdnpreData').value = document.getElementById('hdnpreData').value + ']';
            } else { document.getElementById('hdnpreData').value = '' }


            if (document.getElementById('hdnpreDataCC').value.length > 1) {
                document.getElementById('hdnCC').value = document.getElementById('hdnCC').value.substring(0, document.getElementById('hdnCC').value.length - 1)

                document.getElementById('hdnpreDataCC').value = document.getElementById('hdnpreDataCC').value.substring(0, document.getElementById('hdnpreDataCC').value.length - 1)
                document.getElementById('hdnpreDataCC').value = document.getElementById('hdnpreDataCC').value + ']';
            } else { document.getElementById('hdnpreDataCC').value = '' }


            var BCCElements = document.getElementById('lblBCC');
            document.getElementById('hdnBCC').value = '';
            document.getElementById('hdnSelectedEmailsBCC').value = '';
            document.getElementById('hdnpreDataBCC').value = '[';
            for (i = 0; i < token_count; ++i) {
                txtType = saved_tokens[i].id.split('~')[0]
                if (txtType == "bcc") {
                    document.getElementById('hdnMessageBCC').value = document.getElementById('hdnMessageBCC').value + saved_tokens[i].id.split('~')[3] + '$^$' + saved_tokens[i].id.split('~')[1] + '#^#';
                    document.getElementById('hdnBCC').value = document.getElementById('hdnBCC').value + saved_tokens[i].id.split('~')[1] + ' , '; //BCCElements.childNodes[i].innerHTML + ' , ';
                    document.getElementById('hdnSelectedEmailsBCC').value = document.getElementById('hdnSelectedEmailsBCC').value + saved_tokens[i].id + '|' //BCCElements.childNodes[i].innerHTML + '~' + BCCElements.childNodes[i].name + '|';
                    document.getElementById('hdnpreDataBCC').value = document.getElementById('hdnpreDataBCC').value + '{id :' + '"' + saved_tokens[i].id.split('~')[2] + '"' + ',first_name:' + '"' + saved_tokens[i].id.split('~')[3] + '"' + ',last_name :' + '"' + saved_tokens[i].id.split('~')[4] + '"' + ',email:' + '"' + saved_tokens[i].id.split('~')[1] + '"' + ',url:' + '"#"},';
                }
            }
            if (document.getElementById('hdnpreDataBCC').value.length > 1) {
                document.getElementById('hdnBCC').value = document.getElementById('hdnBCC').value.substring(0, document.getElementById('hdnBCC').value.length - 1)
                document.getElementById('hdnpreDataBCC').value = document.getElementById('hdnpreDataBCC').value.substring(0, document.getElementById('hdnpreDataBCC').value.length - 1)
                document.getElementById('hdnpreDataBCC').value = document.getElementById('hdnpreDataBCC').value + ']';
            } else { document.getElementById('hdnpreDataBCC').value = '' }
            //alert(document.getElementById('hdnBCC').value);
            //alert(document.getElementById('hdnSelectedEmailsBCC').value);
        }


        function FillEmailID(ComboID, hdnContactEmails, DisplayEmailField) {
            // debugger;
            var combo = $find(ComboID);
            if (combo.get_value() != "") {
                var email = trim(combo.get_text());
                if (echeck(email) == true) {

                    //Add Email id with css
                    add(DisplayEmailField, email, combo.get_value());
                    combo.clearSelection();
                    return false;
                }
            }
        }


        function over(e) {
            this.className = 'EmailHover';
        }
        function out(e) {
            this.className = 'Email';
        }

        function RemoveMe(e) {
            this.parentNode.removeChild(this);
        }

        /**
        * DHTML email validation script. Courtesy of SmartWebby.com (http://www.smartwebby.com/dhtml/)
        */
        function echeck(str) {

            var at = "@"
            var dot = "."
            var lat = str.indexOf(at)
            var lstr = str.length
            var ldot = str.indexOf(dot)
            if (str.indexOf(at) == -1) {
                alert("Invalid E-mail ID")
                return false
            }

            if (str.indexOf(at) == -1 || str.indexOf(at) == 0 || str.indexOf(at) == lstr) {
                alert("Invalid E-mail ID")
                return false
            }

            if (str.indexOf(dot) == -1 || str.indexOf(dot) == 0 || str.indexOf(dot) == lstr) {
                alert("Invalid E-mail ID")
                return false
            }

            if (str.indexOf(at, (lat + 1)) != -1) {
                alert("Invalid E-mail ID")
                return false
            }

            if (str.substring(lat - 1, lat) == dot || str.substring(lat + 1, lat + 2) == dot) {
                alert("Invalid E-mail ID")
                return false
            }

            if (str.indexOf(dot, (lat + 2)) == -1) {
                alert("Invalid E-mail ID")
                return false
            }

            if (str.indexOf(" ") != -1) {
                alert("Invalid E-mail ID")
                return false
            }

            return true
        }
        function trim(str, chars) {
            return ltrim(rtrim(str, chars), chars);
        }

        function ltrim(str, chars) {
            chars = chars || "\\s";
            return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
        }

        function rtrim(str, chars) {
            chars = chars || "\\s";
            return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
        }
        function openSMTP(a) {
            window.open('../admin/frmSMTPPopup.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&UserId=' + a + '&Mode=1', '', 'toolbar=no,titlebar=no,left=200,top=250,width=750,height=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function keyHandler(e) {
            var TABKEY = 9;
            if (e.keyCode == TABKEY) {
                SetFocusOnRadEditor();

                if (e.preventDefault) {
                    e.preventDefault();
                }
                else
                    e.returnValue = false;
                return false;
            }
        }
        function SetFocusOnRadEditor() {
            var editor = $find("RadEditor1"); //get a reference to RadEditor client object
            //        if (editor == null)
            //            editor = $get("RadEditor1");
            editor.setFocus(); //set the focus on the the editor
        }
        function openEmpAvailability() {
            window.open("../ActionItems/frmEmpAvailability.aspx?frm=Tickler", '', 'toolbar=no,titlebar=no,top=0,left=100,width=850,height=500,scrollbars=yes,resizable=yes')
            return false;
        }
        //www.telerik.com/help/aspnet-ajax/onclientpastehtml.html
        function OnClientPasteHtml(sender, args) {
            var commandName = args.get_commandName();
            var value = args.get_value();
            if (commandName == "ImageManager") {
                //See if an img has an alt tag set
                var div = document.createElement("DIV");
                //Do not use div.innerHTML as in IE this would cause the image's src or the link's href to be converted to absolute path.
                //This is a severe IE quirk.
                Telerik.Web.UI.Editor.Utils.setElementInnerHtml(div, value);
                //Now check if there is alt attribute
                var img = div.firstChild;
                if (img.src) {
                    img.setAttribute("src", img.src);
                    //Set new content to be pasted into the editor
                    args.set_value(div.innerHTML);
                }
            }
        }
        function OnClientCommandExecuting(editor, args) {
            var name = args.get_name();
            var val = args.get_value();
            if (name == "MergeField") {
                editor.pasteHtml(val);
                //Cancel the further execution of the command as such a command does not exist in the editor command list
                args.set_cancel(true);
            }
        }

        window.onbeforeunload = confirmExit;
        function confirmExit() {
            return " Are you sure you want to exit this page?";
        }
        $(function () {
            $("#lkbSend").click(function () {

                window.onbeforeunload = null;
            });

            $("#ddlEmailtemplate").click(function () {

                window.onbeforeunload = null;
            });
            $("#ddlCategory").click(function () {

                window.onbeforeunload = null;
            });
            $("#btnSaveAttachment").click(function () {

                window.onbeforeunload = null;
            });
            $("#btnSaveCloseAttachment").click(function () {

                window.onbeforeunload = null;
            });
            $("#hplAttachmentsLink").click(function () {

                window.onbeforeunload = null;
            });
            $("#btnOpenFileUpload").click(function () {

                window.onbeforeunload = null;
            });
            
        });

        //Script ended by Sachin:00000000514
        function OnClientLoad(editor, args) {
            var buttonsHolder = $get(editor.get_id() + "Top");
            var buttons = buttonsHolder.getElementsByTagName("A");
            for (var i = 0; i < buttons.length; i++) {
                var a = buttons[i];
                a.tabIndex = -1;
                a.tabStop = false;
            }
        }

        function disableEmail() {
            document.getElementById('disablingDiv').style.display = 'block';
            window.onbeforeunload = null;
        }
    </script>
    <style>
        #updateAddAttachment{
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0;
            left: 0;
            background-color: #0000005c;
            z-index:100 !important;
        }
        #updateAddAttachment .innerPanel{
            top: 30%;
            position: absolute;
            left: 30%;
                padding: 10px;
                background:#fff !important;
        }
          #updateAddAttachment .innerPanel .dg tr td{
              padding:5px;
        }
          #updateAddAttachment .innerPanel .dg tr.hs td{
              padding:5px;
              background-color:#e1e1e1;
        }
        
        ul.token-input-list-facebook {
            width: 100% !important;
            border-left: 0px;
            border-radius: 0 !important;
            box-shadow: none !important;
            border-color: #d2d6de;
            height: 34px !important;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }

        .Holder {
            border: 1px solid #999999;
            height: auto !important;
            margin: 0;
            overflow: hidden;
            padding: 4px 5px 0;
            font-family: Verdana;
            font-size: 11px;
            width: 500px;
        }

        .Email {
            margin: 3px;
        }

        .EmailHover {
            margin: 3px;
            text-decoration: line-through;
        }

        .error {
            color: #D8000C;
            background-color: #FFBABA;
            background-image: url('../images/error.png');
            border: 1px solid;
            margin: 10px 5px 0px 5px;
            padding: 15px 10px 15px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
        }

        #disablingDiv {
            /* Do not display it on entry */
            display: none;
            /* Display it on the layer with index 1001.
               Make sure this is the highest z-index value
               used by layers on that page */
            /*  z-index:1001;*/
            /* make it cover the whole screen */
            /*  position: absolute; 
            top: 0%; 
            left: 0%; 
            width: 100%; 
            height: 100%; */
            /* make it white but fully transparent */
            /*  background-color: grey; 
            opacity:.00; 
            filter: alpha(opacity=00); */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <asp:ScriptManager runat="server" ID="scriptManager">
    </asp:ScriptManager>
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-left">
                <asp:LinkButton ID="lkbSend" runat="server" AccessKey="S" CssClass="btn btn-primary" OnClientClick="return Send();" OnClick="lkbSend_Click">Send&nbsp;&nbsp;<i class="fa fa-paper-plane"></i></asp:LinkButton>&nbsp;&nbsp;&nbsp;
                <a runat="server" onclicl="javascript:return openEmpAvailability()">Availability</a>&nbsp;&nbsp;&nbsp;
                <asp:CheckBox ID="chkTrack" Text="Track This Message" Checked="true" runat="server"></asp:CheckBox>&nbsp;&nbsp;&nbsp;
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Compose Message
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Panel runat="server" ID="pnlSMTPError" CssClass="error" Visible="false">
        please configure SMTP details
        <asp:HyperLink ID="hplImap" runat="server" Text="here" NavigateUrl="#"></asp:HyperLink>
        before attempting to send messages.
    </asp:Panel>
    <div class="row">
        <div class="col-xs-12 col-md-4">
            <div class="form-group">
                <label>Category</label>
                <asp:DropDownList ID="ddlCategory" AutoPostBack="true" CssClass="form-control" runat="server" onchange="PopulateValuesToHiddenFields();"></asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="form-group">
                <label>Email Template</label>
                <asp:DropDownList ID="ddlEmailtemplate" AutoPostBack="True" runat="server" CssClass="form-control" onchange="PopulateValuesToHiddenFields();"></asp:DropDownList>
                <asp:HiddenField ID="hdnFollowupDateUpdateTemplate" Value="0" runat="server" />
            </div>
        </div>
        <div class="col-xs-12 col-md-4">
            <div class="checkbox-inline">
                <asp:CheckBox ID="chkFollowUpStatus" Style="" runat="server" /><label>Change Follow-up Status to</label>
            </div>
            <asp:DropDownList ID="ddlFollowupStatus" CssClass="form-control" runat="server"></asp:DropDownList>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="input-group" style="margin-bottom: 15px;">
                <span class="input-group-btn">
                    <button id="btnTo" class="btn btn-default" onclick="return OpenContactList()" style="font-weight: bold;">To</button></span>
                <input type="text" id="txtTo" name="blah2" runat="server" class="form-control" />
                 <div class="input-group-addon" id="divACC">
                    <a href="#" id="aCC">Add Cc</a>
                 </div>
                <div class="input-group-addon" id="divABCC">
                     <a href="#" id="aBCC">Add Bcc</a>
                </div>
                <asp:Label runat="server" ID="lblTo" Style="border: 1px solid #DFDFDF; color: Navy; font-size: smaller; min-height: 18px; padding-left: 5px; display: none;" Width="500"></asp:Label>
            </div>
        </div>
    </div>
    <div class="row" id="divCC" style="display: none">
        <div class="col-xs-12">
            <div class="input-group" style="margin-bottom: 15px;">
                <span class="input-group-btn">
                    <button id="btnCc" class="btn btn-default" onclick="return OpenContactList()" style="font-weight: bold;">CC</button></span>


                <input type="text" id="txtcc" name="blah2" runat="server" class="form-control" />
                <div class="dropdown" id="divCCContactsWrapper">
                    <ul class="dropdown-menu dropdown-menu-right" id="divCCContacts" style="padding:10px">
                    </ul>
                </div>
                <asp:Label runat="server" ID="lblCC" Style="border: 1px solid #DFDFDF; color: Navy; font-size: smaller; min-height: 18px; padding-left: 5px; display: none;" Width="500"></asp:Label>
            </div>
        </div>
    </div>
    <div class="row" id="divBCC" style="display: none">
        <div class="col-xs-12">
            <div class="input-group" style="margin-bottom: 15px;">
                <span class="input-group-btn">
                    <button id="btnBcc" class="btn btn-default" onclick="return OpenContactList()" style="font-weight: bold;">BCC</button></span>
                <input type="text" id="txtBcc" name="blah2" class="form-control" />
                <div class="dropdown" id="divBCCContactsWrapper">
                    <ul class="dropdown-menu dropdown-menu-right" id="divBCCContacts" style="padding:10px">
                       
                    </ul>
                </div>
                <asp:Label runat="server" ID="lblBCC" Style="border: 1px solid #DFDFDF; color: Navy; font-size: smaller; min-height: 18px; padding-left: 5px; display: none;" Width="500"></asp:Label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="form-group">
                <label>Subject</label>
                <asp:TextBox ID="txtSubject" CssClass="form-control" runat="server"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <ul class="list-inline">
                <li>
                    <asp:LinkButton OnClick="hplAttachmentsLink_Click" CssClass="btn btn-default btn-file" ID="hplAttachmentsLink" runat="server"><i class="fa fa-paperclip"></i>&nbsp;&nbsp;Attachment</asp:LinkButton>
                    <a class="btn btn-default btn-file" href="EditorTools.xml" id="hplAttachments" runat="server" style="display:none"><i class="fa fa-paperclip"></i>&nbsp;&nbsp;Attachment</a>
                </li>
                <li>
                    <asp:Label ID="lblAttachents" runat="server"></asp:Label>
                </li>
            </ul>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-sm-12 col-md-4">
            <asp:Label ID="lblFpTooltip" Text="[?]" CssClass="tip" ToolTip="Following up with new leads, prospects, and sometimes even accounts (heck even fellow employees) is an important part of anyone�s work day. With automated follow-ups you can schedule automated email templates and action items such as calls and tasks, to be created / sent out on a schedule, so you never have to remember to follow-up with a contact again�well, action items do require you look at your action item list, but emails are sent automatically by BizAutomation on your behalf, and you can always set an email alert to ping you from workflow automation.  You can disengage follow-up from the contact record itself, or by opening the compose message window, and clicking on the follow-up campaign �Disengage from Follow-up campaign� button.<\n>To setup an Automation Template, go to Marketing | Automated Follow-up. " runat="server"></asp:Label>
            <asp:Label ID="lblFollowup" Text="Follow-up Campaign:" Font-Bold="true" runat="server"></asp:Label><asp:Label ID="lblFollowupCampaign" runat="server"></asp:Label>
        </div>
        <div class="col-sm-12 col-md-4">
            <asp:Label ID="lblLastFp" Text="Last Follow-up:" Font-Bold="true" runat="server"></asp:Label>
            <asp:Label ID="lblLastFollowUp" runat="server"></asp:Label>
        </div>
        <div class="col-sm-12 col-md-4">
            <asp:Label ID="lblNextFp" Text="Next Follow-up:" Font-Bold="true" runat="server"></asp:Label>
            <asp:Label ID="lblNextFollowUp" runat="server"></asp:Label>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <telerik:RadEditor ID="RadEditor1" runat="server" ToolsFile="EditorTools.xml" Width="100%"
                OnClientLoad="OnClientLoad" Height="255" OnClientPasteHtml="OnClientPasteHtml"
                OnClientCommandExecuting="OnClientCommandExecuting">
                <Content>
                </Content>
                <Tools>
                    <telerik:EditorToolGroup>
                        <telerik:EditorDropDown Name="MergeField" Text="Merge Field">
                        </telerik:EditorDropDown>
                    </telerik:EditorToolGroup>
                </Tools>
            </telerik:RadEditor>
        </div>
    </div>
    <div id="divRecord" runat="server" visible="false">
        <asp:Button ID="btnSignature" runat="server" Text="Signature" CssClass="Credit"></asp:Button>
        Pin To
                <asp:DropDownList ID="ddlAssignTO1" CssClass="signup" runat="server" AutoPostBack="true"
                    onchange="PopulateValuesToHiddenFields();">
                    <asp:ListItem Text="--Select One--" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Sales Opportunity" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Purchase Opportunity" Value="2"></asp:ListItem>
                    <asp:ListItem Text="Sales Order" Value="3"></asp:ListItem>
                    <asp:ListItem Text="Purchase Order" Value="4"></asp:ListItem>
                    <asp:ListItem Text="Project" Value="5"></asp:ListItem>
                    <asp:ListItem Text="Cases" Value="6"></asp:ListItem>
                </asp:DropDownList>
        <asp:DropDownList ID="ddlOpenRecord" CssClass="signup" runat="server">
        </asp:DropDownList>
    </div>
    <asp:HiddenField runat="server" ID="hdnSelectedEmails" Value="" />
    <asp:HiddenField runat="server" ID="hdnTo" Value="" />
    <asp:HiddenField runat="server" ID="hdnToName" Value="" />
    <asp:HiddenField runat="server" ID="hdnSelectedEmailsCC" Value="" />
    <asp:HiddenField runat="server" ID="hdnCC" Value="" />
    <asp:HiddenField runat="server" ID="hdnCCName" Value="" />
    <asp:HiddenField runat="server" ID="hdnSelectedEmailsBCC" Value="" />
    <asp:HiddenField runat="server" ID="hdnBCC" Value="" />
    <asp:HiddenField runat="server" ID="hdnBCCName" Value="" />
    <asp:HiddenField runat="server" ID="hdnUserRights" Value="" />
    <asp:HiddenField runat="server" ID="hdnpreData" Value="" />
    <asp:HiddenField runat="server" ID="hdnpreDataCC" Value="" />
    <asp:HiddenField runat="server" ID="hdnpreDataBCC" Value="" />
    <asp:HiddenField runat="server" ID="hdnMessageTo" Value="" />
    <asp:HiddenField runat="server" ID="hdnMessageCC" Value="" />
    <asp:HiddenField runat="server" ID="hdnMessageBCC" Value="" />
    <asp:HiddenField runat="server" ID="hdntokenCount" Value="" />
    <asp:HiddenField runat="server" ID="hdnIsReplied" Value="" />
    <asp:HiddenField runat="server" ID="hdnDivisionId" Value="" />
    <asp:HiddenField ID="hdfCompanyName" runat="server" Value="0" />
    <asp:HiddenField ID="hdnBizDocTempId" runat="server" Value="0" />
    <asp:HiddenField ID="hdnModuleId" runat="server" Value="45" />
    <asp:HiddenField ID="hdnBizDocHtmlCss" runat="server" Value="" />
    <asp:HiddenField ID="hdnIsEmptyAttachment" runat="server" Value="" />
    <div id="disablingDiv"></div>

     <asp:HiddenField ID="OppType" runat="server" />
    <asp:HiddenField ID="OppStatus" runat="server" />
    <asp:HiddenField ID="hdnAttachContactID" runat="server" />
    <asp:HiddenField ID="hdnAttachCaseID" runat="server" />
    <asp:HiddenField ID="hdnAttachProID" runat="server" />
    <asp:HiddenField ID="hdnAttachCommID" runat="server" />
    <asp:HiddenField ID="hdnAttachOppID" runat="server" />
    <asp:HiddenField ID="hdnAttachDivisionID" runat="server" />
    <div id="updateAddAttachment" runat="server" Visible="false">
        <div class="innerPanel">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                   <table cellspacing="0" cellpadding="0" border="0" align="center">

<tr>
    <td colspan="2" align="right">
        <asp:Button ID="btnSaveCloseAttachment" OnClick="btnSaveCloseAttachment_Click"  CssClass="btn btn-primary" runat="server" Text="Save & Close" />
        <asp:Button ID="btnSaveAttachment" OnClick="btnSaveAttachment_Click"  CssClass="btn btn-primary" runat="server" Text="Close" />
    </td>
</tr>
        <tr>
           <%-- <td valign="top" style="text-align: right;">
                <i>From your local drive&nbsp;</i>
            </td>--%>
            <td valign="top" style="padding: 5px;">
                <button class="btn btn-default" id="btnOpenFileUpload" type="button" style="    width: 100%;
    font-weight: bold;">Attach from local drive</button>
                <span id="fileNames"></span>
                <input class="signup" id="fileupload" onchange="fileSelect(event)" type="file"  style="display:none" name="fileupload"  runat="server" />
            </td>
            
        </tr>
                       <tr>
                           <td valign="top" align="right">
                <asp:Button ID="btnAttachFile"  runat="server" Text="Add" CssClass="btn btn-primary" OnClick="btnAttachFile_Click"></asp:Button>
            </td>
                       </tr>
        <tr runat="server" id="trDocuments" visible="false">
            <%--<td valign="top" style="text-align: right;">
                <i>From the record you�re in&nbsp;</i>
            </td>--%>
            <td valign="top" style="padding: 5px;">
                <%-- <asp:DropDownList ID="ddldocuments" CssClass="signup" runat="server" Width="130">
                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                </asp:DropDownList>--%>
                <telerik:RadComboBox ID="radDocuments" runat="server" EmptyMessage="Attach from the Organization" CheckBoxes="true" Width="350px">
                </telerik:RadComboBox>
            </td>
            <td valign="top">
                <asp:Button ID="btnAttachDocuments" style="display:none" runat="server" Text="Attach" CssClass="btn btn-primary" OnClick="btnAttachDocuments_Click"></asp:Button>
            </td>
        </tr>
        <tr runat="server" id="trParentOrganization" visible="false">
           <%-- <td valign="top" style="text-align: right;" >
                <i>From the parent organization of the record you�re in&nbsp;</i>
            </td>--%>
            <td valign="top" style="padding: 5px;">
                <telerik:RadComboBox ID="radParentRecord" EmptyMessage="Attach from the record you�re in" runat="server" CheckBoxes="true" Width="350px">
                </telerik:RadComboBox>
            </td>
            <td valign="top">
                <asp:Button ID="btnParentRecord" style="display:none" runat="server" Text="Attach" CssClass="btn btn-primary" OnClick="btnParentRecord_Click"></asp:Button>
            </td>
        </tr>
        <tr runat="server" id="trBizDocs" visible="false">
           <%-- <td valign="top" style="text-align: right;">
                <i>BizDocs&nbsp;</i>
            </td>--%>
            <td valign="top" style="padding: 5px;">
                <telerik:RadComboBox ID="radBizDocsDocuments" EmptyMessage="Attach other BizDocs" runat="server" CheckBoxes="true" Width="350px">
                </telerik:RadComboBox>
            </td>
            <td>
                <asp:Button ID="btnBizDocAttach" style="display:none" runat="server" Text="Attach" CssClass="btn btn-primary" OnClick="btnBizDocAttach_Click"></asp:Button>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:DataGrid ID="dgAttachments" runat="server" CssClass="dg" Width="100%"
                    AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn SortExpression="vcfiletype" HeaderText="File">
                            <ItemTemplate>
                                <asp:HyperLink ID="hplLink" CssClass="hyperLinkGrid" Target="_blank" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Filename") %>'
                                    NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "FileLocation") %>'>
                                </asp:HyperLink>
                                <%--<asp:TextBox ID="txtBizDocID" runat="server" Style="display: none" Text='<%# DataBinder.Eval(Container.DataItem, "numOppBizDocsId") %>'>
                                </asp:TextBox>--%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <HeaderTemplate>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
                                <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
																	<font color="#730000">*</font></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger  ControlID="btnSaveCloseAttachment" />
                <asp:PostBackTrigger  ControlID="btnSaveAttachment" />
                <asp:PostBackTrigger  ControlID="btnAttachFile" />
                <asp:PostBackTrigger  ControlID="btnParentRecord" />
                <asp:PostBackTrigger  ControlID="btnBizDocAttach"  />
            </Triggers>
        </asp:UpdatePanel>
         </div>
        <asp:TextBox ID="txtAttachements" runat="server" Style="display: none"></asp:TextBox>
    </div>

    <script type="text/javascript">
        /* Add event to focus on editor when tab key is pressed*/
        var myInput = document.getElementById("txtSubject");
        if (myInput.addEventListener) {
            myInput.addEventListener('keydown', this.keyHandler, false);
        } else if (myInput.attachEvent) {
            myInput.attachEvent('onkeydown', this.keyHandler);
        }

    </script>
    <asp:Literal runat="server" ID="litScript"></asp:Literal>
    <div>
        <script type="text/javascript">
            $("#btnOpenFileUpload").click(function () {
                $("#fileupload").click();
            })
            function fileSelect(e) {
                console.log(e.target.files[0].name);
                $("#fileNames").text(e.target.files[0].name)
            }
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_endRequest(function () {
                // Put your code here
                $("#btnOpenFileUpload").click(function () {
                    $("#fileupload").click();
                })
            });
            $(document).ready(function () {
                //token - input - delete -token - facebook
                $('.token-input-delete-token-facebook').on('click', function () {
                    //console.log('facebook');
                    //console.log($(this).attr('id'));
                    var removeMe;
                    $.each(saved_tokens, function (i, v) {
                        if (v.id == $(this).attr('id')) {
                            removeMe = i;
                        }
                    });
                    saved_tokens.splice(removeMe, 1);

                });

                $("#txtTo").tokenInput("EmailList.ashx", {
                    txtInput: "to",
                    theme: "facebook",
                    tokenDelete: "token-input-delete-token-facebook",
                    prePopulate: eval(document.getElementById("hdnpreData").value),
                    propertyToSearch: "first_name",
                    resultsFormatter: function (item) { return "<li><div style='display: inline-block; padding-left: 10px;'><div class='full_name'>" + item.first_name + " " + item.last_name + "</div><div class='email'>" + item.email + "</div></div></li>" },
                    tokenFormatter: function (item) { return "<li><p title='" + item.email + "'>" + item.first_name + " " + item.last_name + "</p></li>" }
                });

                $("#txtTo").focus();

                $("#txtcc").tokenInput("EmailList.ashx", {
                    txtInput: "cc",
                    theme: "facebook",
                    tokenDelete: "token-input-delete-token-facebook",
                    prePopulate: eval(document.getElementById("hdnpreDataCC").value),
                    propertyToSearch: "first_name",
                    resultsFormatter: function (itemcc) { return "<li><div style='display: inline-block; padding-left: 10px;'><div class='full_name'>" + itemcc.first_name + " " + itemcc.last_name + "</div><div class='email'>" + itemcc.email + "</div></div></li>" },
                    tokenFormatter: function (itemcc) { return "<li><p title='" + itemcc.email + "'>" + itemcc.first_name + " " + itemcc.last_name + "</p></li>" }
                });

                $("#txtBcc").tokenInput("EmailList.ashx", {
                    txtInput: "bcc",
                    theme: "facebook",
                    prePopulate: eval(document.getElementById("hdnpreDataBCC").value),
                    propertyToSearch: "first_name",
                    resultsFormatter: function (item) { return "<li><div style='display: inline-block; padding-left: 10px;'><div class='full_name'>" + item.first_name + " " + item.last_name + "</div><div class='email'>" + item.email + "</div></div></li>" },
                    tokenFormatter: function (item) { return "<li><p title='" + item.email + "'>" + item.first_name + " " + item.last_name + "</p></li>" }
                });

                if (("," + $("#txtcc").val() + ",").indexOf(",cc~") == -1) {
                    $("#divACC").show();
                    $("#divCC").hide();
                } else {
                    $("#divACC").hide();
                    $("#divCC").show();
                }

                if (("," + $("#txtBcc").val() + ",").indexOf(",bcc~") == -1) {
                    $("#divABCC").show();
                    $("#divBCC").hide();
                } else {
                    $("#divABCC").hide();
                    $("#divBCC").show();
                }
            });
        </script>
    </div>
</asp:Content>
