<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmContactAddress.aspx.vb"
    Inherits="BACRM.UserInterface.Contacts.frmContactAddress" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Address</title>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKZ2Kph5NQXTnPc1xEzM4Bq0nhzXjONS0&libraries=places" type="text/javascript"></script>
    <script src="../JavaScript/GooglePlace.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            initializeaddress("txtStreet","txtStreet","txtCity","hdnGoogleState","txtPostal","ddlCountry");
        });
    </script>
    <style type="text/css">
        #tblContact {
            border:0px;
            padding:10px;
        }

            #tblContact td:first-child {
                font-weight:bold;
                width:120px;
                padding-right:5px;
            }

            #tblContact tr {
                height:30px;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" Width="50">
            </asp:Button>
            <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close">
            </asp:Button>
            <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" Width="50">
            </asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Contact Address
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table id="tblContact" width="500px">
        <tr>
            <td class="normal4" align="center" colspan="2">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td class="normal7" align="right">
                Address
            </td>
            <td>
                <asp:DropDownList runat="server" ID="ddlAddressName" CssClass="signup" AutoPostBack="true" style="width:300px !important">
                </asp:DropDownList>
                &nbsp;<asp:LinkButton Text="Delete" ID="lbDelete" runat="server" CssClass="signup" />
            </td>
        </tr>
        <tr>
            <td class="text" align="right">
                Address Name
            </td>
            <td>
                <asp:TextBox ID="txtAddressName" runat="server" CssClass="signup"  style="width:300px !important"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Street
            </td>
            <td>
                <asp:TextBox ID="txtStreet" runat="server" CssClass="signup" TextMode="MultiLine" Rows="3" style="width:295px !important"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                City
            </td>
            <td>
                <asp:TextBox ID="txtCity" runat="server" CssClass="signup" style="width:300px !important"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                State
            </td>
            <td>
                <asp:DropDownList ID="ddlState" runat="server" CssClass="signup" style="width:300px !important">
                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Postal
            </td>
            <td>
                <asp:TextBox ID="txtPostal" runat="server" CssClass="signup" style="width:300px !important"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                Country
            </td>
            <td>
                <asp:DropDownList ID="ddlCountry" AutoPostBack="True" runat="server" style="width:300px !important" CssClass="signup">
                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="text" align="right">
                <label for="chkIsPrimary">
                    Is Primary Address?</label>
            </td>
            <td>
                <asp:CheckBox Text="" runat="server" ID="chkIsPrimary" />
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnGoogleState" runat="server" />
</asp:Content>
