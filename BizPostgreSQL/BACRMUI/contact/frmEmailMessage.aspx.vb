'created by anoop jayaraj
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common
Imports Infragistics.WebUI.WebHtmlEditor
Namespace BACRM.UserInterface.Contacts
    Public Class frmEmailMessage
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents lblTo As System.Web.UI.WebControls.Label
        Protected WithEvents lblFrom As System.Web.UI.WebControls.Label
        Protected WithEvents lblSubject As System.Web.UI.WebControls.Label
        Protected WithEvents lblCc As System.Web.UI.WebControls.Label
        Protected WithEvents lblBcc As System.Web.UI.WebControls.Label
        Protected WithEvents btnReply As System.Web.UI.WebControls.Button
        Protected WithEvents btReplyAll As System.Web.UI.WebControls.Button
        Protected WithEvents btnForward As System.Web.UI.WebControls.Button
        Protected WithEvents tdAttachments As System.Web.UI.HtmlControls.HtmlTableCell

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then
                    
                    Dim objContacts As New CContacts
                    Dim dtEmail As DataTable
                    objContacts.EmailHstrID = CCommon.ToLong(GetQueryStringVal("Email"))
                    objContacts.CreatedOn = Replace(GetQueryStringVal( "Date"), "%20", " ")
                    dtEmail = objContacts.EmailDetails
                    If dtEmail.Rows.Count = 1 Then
                        lblTo.Text = dtEmail.Rows(0).Item("vcMessageTo")
                        lblFrom.Text = dtEmail.Rows(0).Item("vcMessageFrom")
                        lblCc.Text = dtEmail.Rows(0).Item("vcCC")
                        lblBcc.Text = dtEmail.Rows(0).Item("vcBCC")
                        lblSubject.Text = dtEmail.Rows(0).Item("vcSubject")
                        'If dtEmail.Rows(0).Item("chrSource") = "I" Then
                        '    Dim objEmails As New Email
                        '    Dim dt As DataTable
                        '    dt = objEmails.GetEmail(GetQueryStringVal( "Email"), Session("DomainId"))
                        '    oEditHtml.Text = dt.Rows(0).Item("Body")
                        'Else : 
                        oEditHtml.Text = dtEmail.Rows(0).Item("vcBody")
                        'End If

                        Dim dtAttachments As DataTable
                        dtAttachments = objContacts.EmailAttachmentDtls
                        If dtAttachments.Rows.Count > 0 Then
                            Dim i As Integer
                            Dim hpl As HyperLink
                            Dim lbl As Label
                            For i = 0 To dtAttachments.Rows.Count - 1
                                If i <> 0 Then
                                    lbl = New Label
                                    lbl.Text = "&nbsp; ,"
                                    tdAttachments.Controls.Add(lbl)
                                End If
                                hpl = New HyperLink
                                hpl.Text = dtAttachments.Rows(i).Item("vcFileName")
                                hpl.NavigateUrl = CCommon.GetDocumentPath(Session("DomainID")) & dtAttachments.Rows(i).Item("vcFileName")
                                hpl.CssClass = "normal1"
                                hpl.Target = "_blank"
                                tdAttachments.Controls.Add(hpl)
                            Next
                        End If
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnReply_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReply.Click
            Try
                Response.Redirect("../contact/frmComposeWindow.aspx?LsEmail=" & lblFrom.Text & "&Subj=RE: " & lblSubject.Text)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btReplyAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btReplyAll.Click
            Try
                Response.Redirect("../contact/frmComposeWindow.aspx?LsEmail=" & lblFrom.Text & IIf(lblCc.Text = "", "", "," & lblCc.Text) & "&Subj=RE: " & lblSubject.Text)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnForward_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnForward.Click
            Try
                Session("Content") = Nothing
                Response.Redirect("../contact/frmComposeWindow.aspx?LsEmail=" & lblFrom.Text & "&Subj=FWD: " & lblSubject.Text)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
