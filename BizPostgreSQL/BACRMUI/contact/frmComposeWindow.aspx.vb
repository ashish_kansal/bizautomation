'created by anoop jayaraj
Imports System.Text
Imports System.Web.Services
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Opportunities
Imports Telerik.Web.UI
Imports System.IO

Namespace BACRM.UserInterface.Contacts
    Public Class frmComposeWindow
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

#Region "Global Declation"
        Dim strToEMailID As String = ""
        Dim strCcEMailID As String = ""
#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try

                If Not IsPostBack Then
                    BindBizDocsCategory()
                    BindFollowUpStatus()
                    If CCommon.ToInteger(GetQueryStringVal("PickAtch")) <> 1 Then Session("Attachements") = Nothing
                    IsSMTPEnable()
                    RadEditor1.Content = Session("Signature")
                    RadEditor1.DisableFilter(Telerik.Web.UI.EditorFilters.ConvertFontToSpan)

                    If Not Session("Content") Is Nothing Then
                        RadEditor1.Content = Session("Content")
                        Session("Content") = Nothing
                    End If

                    If GetQueryStringVal("reply") IsNot Nothing Then
                        hdnIsReplied.Value = CCommon.ToBool(CCommon.ToInteger(GetQueryStringVal("reply")))
                    End If

                    If GetQueryStringVal("frm") = "outlook" Then
                        Dim objOutlook As New COutlook
                        Dim dttable As DataTable
                        objOutlook.numEmailHstrID = CCommon.ToLong(GetQueryStringVal("EmailHstrId"))
                        objOutlook.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                        dttable = objOutlook.getMail()
                        If dttable.Rows.Count > 0 Then
                            Dim intCountAttachment As Integer = 0
                            Session("Attachements") = Nothing
                            For intCountAttachment = 0 To dttable.Rows.Count - 1
                                If dttable.Rows(intCountAttachment).Item("HasAttachments") = True Then
                                    Dim AttachName As String() = dttable.Rows(intCountAttachment).Item("AttachmentPath").ToString.Split("|")
                                    Dim OriginalAttachName As String() = dttable.Rows(intCountAttachment).Item("vcLocation").ToString.Split("|")

                                    For i As Integer = 0 To AttachName.Length - 1
                                        objCommon.AddAttchmentToSession(AttachName(i), CCommon.GetDocumentPath(Session("DomainID")) & OriginalAttachName(i), CCommon.GetDocumentPhysicalPath(Session("DomainID")) & OriginalAttachName(i))
                                    Next
                                End If
                            Next
                            '    End If
                            'Else
                            Dim strFrom As String
                            If Not IsDBNull(dttable.Rows(0).Item("FromEmail")) Then
                                If CCommon.ToString(dttable.Rows(0).Item("FromEmail")) <> "" Then
                                    Dim fromNameAddress As String() = dttable.Rows(0).Item("FromEmail").ToString.Split(New String() {"$^$"}, StringSplitOptions.None)

                                    If fromNameAddress.Length >= 3 Then
                                        strFrom = "On " & dttable.Rows(0).Item("Sent") & " " & HttpUtility.HtmlEncode(fromNameAddress(1)) & " (" & HttpUtility.HtmlEncode(fromNameAddress(2)) & ") wrote:"
                                    End If
                                    'Else : hypFrom.Text = HttpUtility.HtmlEncode(dtTable.Rows(0).Item("FromName"))
                                End If
                            End If

                            RadEditor1.Content = "<br/><br/><br/>" & Session("Signature") & "<br/><br/><i>" & strFrom & "</i><hr />" & dttable.Rows(0).Item("Body")
                            'End If
                            'ClientScript.RegisterClientScriptBlock(Me.GetType, "SetFocusOnReply", " ", True)
                            'litScript.Text = "<script> SetFocusOnRadEditor(); </script>"
                        End If
                    ElseIf GetQueryStringVal("frm") = "BizInvoice" Then
                        If Session("Attachements") Is Nothing Then
                            hdnIsEmptyAttachment.Value = "1"
                        Else
                            Dim k As Integer
                            Dim strSplitedNames() As String
                            Dim strLocation As String
                            Dim strLocation1 As String
                            Dim dtAttachments As DataTable = CType(Session("Attachements"), DataTable)

                            If Not dtAttachments Is Nothing Then
                                For k = 0 To dtAttachments.Rows.Count - 1
                                    strSplitedNames = Split(dtAttachments.Rows(k).Item("FileLocation").ToString, "/")
                                    dtAttachments.Rows(k).Item("FileLocation") = Replace(dtAttachments.Rows(k).Item("FileLocation").ToString, "..", ConfigurationManager.AppSettings("BACRMLocation"))
                                    'Checking whether the Files is present

                                    strLocation = CCommon.GetDocumentPhysicalPath() & strSplitedNames(strSplitedNames.Length - 1).ToString
                                    strLocation1 = CCommon.GetDocumentPhysicalPath(DomainID) & strSplitedNames(strSplitedNames.Length - 1).ToString 'Check into domain's Folder

                                    If dtAttachments.Columns.Contains("FilePhysicalPath") Then
                                        strLocation = dtAttachments.Rows(k)("FilePhysicalPath").ToString
                                    End If

                                    If Not System.IO.File.Exists(strLocation) AndAlso Not System.IO.File.Exists(strLocation1) AndAlso Not System.IO.File.Exists(CCommon.ToString(dtAttachments.Rows(k).Item("FileLocation"))) Then
                                        hdnIsEmptyAttachment.Value = "1"
                                    End If
                                Next
                            End If

                        End If
                    Else
                        'radCmbTo.Focus()
                    End If
                    If GetQueryStringVal("frm") = "message" Then
                        Dim str As String = ""
                        str = "<br>" & RadEditor1.Content
                        RadEditor1.Content = "<hr />" & Session("MailBody") & str
                        Session("MailBody") = Nothing
                    End If

                    If GetQueryStringVal("pqwRT") <> "" Then   '''ContactID
                        'ViewState("ContID") = GetQueryStringVal( "pqwRT")
                        objCommon.ContactID = CCommon.ToLong(GetQueryStringVal("pqwRT"))
                        objCommon.charModule = "C"
                        objCommon.GetCompanySpecificValues1()
                    ElseIf GetQueryStringVal("fghTY") <> "" Then
                        objCommon.CaseID = CCommon.ToLong(GetQueryStringVal("fghTY"))
                        objCommon.charModule = "S"
                        objCommon.GetCompanySpecificValues1()
                    ElseIf GetQueryStringVal("tyrCV") <> "" Then
                        objCommon.ProID = CCommon.ToLong(GetQueryStringVal("tyrCV"))
                        objCommon.charModule = "P"
                        objCommon.GetCompanySpecificValues1()
                    ElseIf GetQueryStringVal("CommID") <> "" Then
                        objCommon.CommID = CCommon.ToLong(GetQueryStringVal("CommID"))
                        objCommon.charModule = "A"
                        objCommon.GetCompanySpecificValues1()
                    ElseIf GetQueryStringVal("rtyWR") <> "" Then   ''''DivisionID
                        objCommon.DivisionID = CCommon.ToLong(GetQueryStringVal("rtyWR"))
                        objCommon.charModule = "D"
                        objCommon.GetCompanySpecificValues1()
                    ElseIf GetQueryStringVal("pluYR") <> "" Then
                        objCommon.OppID = CCommon.ToLong(GetQueryStringVal("pluYR"))
                        objCommon.charModule = "O"
                        objCommon.GetCompanySpecificValues1()
                    ElseIf (CCommon.ToLong(GetQueryStringVal("uihTR")) > 0) Then         'IF Clicked from ContactDetails screen ' Added by Priya (2 Feb 2018)
                        objCommon.ContactID = CCommon.ToLong(GetQueryStringVal("uihTR"))
                        objCommon.charModule = "C"
                        objCommon.GetCompanySpecificValues1()
                    End If
                    hdnAttachContactID.Value = objCommon.ContactID
                    hdnAttachCaseID.Value = objCommon.CaseID
                    hdnAttachProID.Value = objCommon.ProID
                    hdnAttachCommID.Value = objCommon.CommID
                    hdnAttachOppID.Value = objCommon.OppID
                    hdnAttachDivisionID.Value = objCommon.DivisionID

                    If CCommon.ToLong(GetQueryStringVal("OppID")) > 0 Then
                        objCommon.OppID = CCommon.ToLong(GetQueryStringVal("OppID"))
                        Session("OppID") = CCommon.ToLong(GetQueryStringVal("OppID"))
                    End If


                    Dim contactId As Long = 0
                    contactId = objCommon.ContactID
                    Dim compId As Long = 0
                    compId = objCommon.CompID
                    Dim DivId As Long = 0
                    DivId = objCommon.DivisionID
                    hdnDivisionId.Value = objCommon.DivisionID

                    'hplAttachments.Attributes.Add("onclick", "return OpenAttachments('" & DivId & "','" & contactId & "','" & compId & "','" & objCommon.OppID & "')")
                    LoadTemplates()

                    If Globals.FormatQueryString(GetQueryStringVal("LsEmail")) <> "" Then
                        lblTo.Text = Globals.FormatQueryString(GetQueryStringVal("LsEmail")).Trim()
                    Else
                        GetEmailIDs()
                        lblTo.Text = strToEMailID
                        If (objCommon.EmailID <> "") Then
                            lblTo.Text = Globals.FormatQueryString(objCommon.EmailID).Trim()
                        ElseIf CCommon.ToLong(GetQueryStringVal("tyrCV")) > 0 Then   'IF Clicked from Project Details screen  ' Added by Priya (2 Feb 2018)
                            lblTo.Text = objCommon.GetCustProjectManagerEmailId((CCommon.ToLong(Session("DomainID"))), (CCommon.ToLong(GetQueryStringVal("tyrCV"))))
                        End If
                    End If

                    If CCommon.ToLong(GetQueryStringVal("ContID")) > 0 Then
                        hdnpreData.Value = CreateJSON(lblTo.Text, CCommon.ToLong(GetQueryStringVal("ContID")))
                    Else
                        hdnpreData.Value = CreateJSON(lblTo.Text, contactId)
                    End If

                    If strCcEMailID <> "" Then
                        lblCC.Text = strCcEMailID

                        If CCommon.ToLong(GetQueryStringVal("ContID")) > 0 Then
                            hdnpreDataCC.Value = CreateJSON(lblCC.Text, CCommon.ToLong(GetQueryStringVal("ContID")))
                        Else
                            hdnpreDataCC.Value = CreateJSON(lblCC.Text, contactId)
                        End If

                        Page.ClientScript.RegisterStartupScript(Me.GetType, "ShowCC", "Hide(1);", True)
                    End If


                    If CCommon.ToString(Session("Subj")) <> "" Then
                        txtSubject.Text = Server.UrlDecode(CCommon.ToString(Session("Subj")))
                        Session("Subj") = Nothing
                    ElseIf GetQueryStringVal("Subj") <> "" Then
                        txtSubject.Text = Server.UrlDecode(CCommon.ToString(GetQueryStringVal("Subj")))
                    End If

                    GetUserRightsForPage(11, 2)
                    hdnUserRights.Value = m_aryRightsForPage(RIGHTSTYPE.VIEW)

                    LoadMergeField()

                    If GetQueryStringVal("EmailTemp") <> "" Then
                        If Not ddlEmailtemplate.Items.FindByValue(CCommon.ToString(GetQueryStringVal("EmailTemp"))) Is Nothing Then
                            ddlEmailtemplate.Items.FindByValue(CCommon.ToString(GetQueryStringVal("EmailTemp"))).Selected = True
                            ddlEmailtemplateSelectedIndexChanged()
                        End If
                    End If

                    If CCommon.ToInteger(GetQueryStringVal("PickAtch")) = 1 Then
                        If Not Session("Attachements") Is Nothing Then
                            Dim dtTable As DataTable
                            dtTable = Session("Attachements")
                            Dim k As Integer
                            lblAttachents.Text = ""
                            For k = 0 To dtTable.Rows.Count - 1
                                lblAttachents.Text = lblAttachents.Text & dtTable.Rows(k).Item("Filename") & ","
                            Next
                            lblAttachents.Text.TrimEnd(",")
                        End If
                    Else
                        Session("Attachements") = Nothing
                    End If

                    'Added by Priya (16 JAn 2018)
                    If CCommon.ToLong(GetQueryStringVal("ContID")) > 0 Then
                        FetchAutomatedFollowUpDetails(CCommon.ToLong(Session("DomainID")), CCommon.ToLong(GetQueryStringVal("ContID")))
                    Else
                        FetchAutomatedFollowUpDetails(CCommon.ToLong(Session("DomainID")), contactId)
                    End If

                    'KEEP IT LAST IN OF BLOACK
                    If GetQueryStringVal("frm") = "BizInvoice" AndAlso CCommon.ToLong(GetQueryStringVal("BizDocID")) > 0 Then
                        'Load to, cc and bcc based on past email sent if enabled in global setttings
                        Dim objDivisionMasterBizDocEmail As New BusinessLogic.Account.DivisionMasterBizDocEmail
                        objDivisionMasterBizDocEmail.DomainID = CCommon.ToLong(Session("DomainID"))
                        objDivisionMasterBizDocEmail.OppBizDocID = CCommon.ToLong(GetQueryStringVal("BizDocID"))
                        Dim dtEmails As DataTable = objDivisionMasterBizDocEmail.GetByOppBizDocID()

                        If Not dtEmails Is Nothing AndAlso dtEmails.Rows.Count > 0 Then
                            If Not String.IsNullOrEmpty(CCommon.ToString(dtEmails.Rows(0)("vcTo")).Trim()) Then
                                hdnpreData.Value = CCommon.ToString(dtEmails.Rows(0)("vcTo")).Trim()
                            End If
                            If Not String.IsNullOrEmpty(CCommon.ToString(dtEmails.Rows(0)("vcCC")).Trim()) Then
                                hdnpreDataCC.Value = CCommon.ToString(dtEmails.Rows(0)("vcCC")).Trim()
                            End If
                            If Not String.IsNullOrEmpty(CCommon.ToString(dtEmails.Rows(0)("vcBCC")).Trim()) Then
                                hdnpreDataBCC.Value = CCommon.ToString(dtEmails.Rows(0)("vcBCC")).Trim()
                            End If
                        End If
                    End If
                End If

                Session("SearchCompanyName") = hdfCompanyName.Value.ToString()
                'Set Image Manage and Template Manager
                Campaign.SetRadEditorPath(RadEditor1, CCommon.ToLong(Session("DomainID")), Page)

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Private Sub GetDocumentFilesLink()
            Try
                Dim objOpp As New OppotunitiesIP
                Dim dtDocLinkName As New DataTable
                With objOpp
                    .DomainID = Session("DomainID")
                    If CCommon.ToLong(hdnAttachDivisionID.Value) > 0 Then
                        .DivisionID = CCommon.ToLong(hdnAttachDivisionID.Value)
                    ElseIf Session("DivisionID") IsNot Nothing AndAlso Session("DivisionID") <> "0" Then
                        .DivisionID = CCommon.ToLong(Session("DivisionID"))
                    Else : .DivisionID = 0
                    End If
                    dtDocLinkName = .GetDocumentFilesLink("E")
                End With

                If dtDocLinkName IsNot Nothing AndAlso dtDocLinkName.Rows.Count > 0 Then
                    trDocuments.Visible = True
                    radDocuments.DataSource = dtDocLinkName
                    radDocuments.DataTextField = "vcdocname"
                    radDocuments.DataValueField = "numGenericDocid"
                    radDocuments.DataBind()
                    'radDocuments.Items.Insert(0, "--Select One--")
                    'radDocuments.Items.FindByText("--Select One--").Value = 0
                Else
                    trDocuments.Visible = False
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Private Sub GetParentRecord()
            Try
                Dim dtOppBiDocDtl As DataTable
                Dim objOppBizDocs As New OppBizDocs
                objOppBizDocs.ReferenceType = 1
                If CCommon.ToLong(hdnAttachOppID.Value) Then
                    objOppBizDocs.ReferenceID = CCommon.ToLong(hdnAttachOppID.Value)
                    trParentOrganization.Visible = True
                ElseIf Session("OppID") IsNot Nothing AndAlso Session("OppID") <> "0" Then
                    objOppBizDocs.ReferenceID = CCommon.ToLong(Session("OppID"))
                    trParentOrganization.Visible = True
                Else : objOppBizDocs.ReferenceID = 0
                    trParentOrganization.Visible = False
                    Exit Sub
                End If
                objOppBizDocs.DomainID = Session("DomainID")
                objOppBizDocs.UserCntID = Session("UserContactID")
                objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                Dim ds1 As DataSet = objOppBizDocs.GetMirrorBizDocDtls
                dtOppBiDocDtl = ds1.Tables(0)

                If dtOppBiDocDtl.Rows.Count = 0 Then Exit Sub
                If dtOppBiDocDtl IsNot Nothing AndAlso dtOppBiDocDtl.Rows.Count > 0 Then
                    radParentRecord.DataSource = dtOppBiDocDtl
                    radParentRecord.DataTextField = "OppName"
                    radParentRecord.DataValueField = "numBizDocId"
                    radParentRecord.DataBind()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub GetBizDocs()
            Dim dtTable As DataTable = Nothing
            Dim objBizDocs As New OppBizDocs

            If CCommon.ToLong(hdnAttachOppID.Value) > 0 Then
                objBizDocs.OppId = CCommon.ToLong(hdnAttachOppID.Value)
                trBizDocs.Visible = True
            ElseIf Session("OppID") IsNot Nothing AndAlso Session("OppID") <> "0" Then
                objBizDocs.OppId = CCommon.ToLong(Session("OppID"))
                trBizDocs.Visible = True
            Else : objBizDocs.OppId = 0
                trBizDocs.Visible = False
                Exit Sub
            End If

            objBizDocs.DomainID = Session("DomainID")
            objBizDocs.UserCntID = Session("UserContactID")

            objBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

            Dim dsBizDocs As DataSet = objBizDocs.GetBizDocsByOOpId

            If Not dsBizDocs Is Nothing AndAlso dsBizDocs.Tables.Count > 1 Then
                dtTable = dsBizDocs.Tables(1)
            End If

            If Not dtTable Is Nothing AndAlso dtTable.Rows.Count > 0 Then
                'trBizDocs.Visible = True
                radBizDocsDocuments.DataSource = dtTable
                'radBizDocsDocuments.DataTextField = "BizDoc"
                radBizDocsDocuments.DataTextField = "vcBizDocID"
                'radBizDocsDocuments.DataValueField = "numBizDocId"
                radBizDocsDocuments.DataValueField = "numOppBizDocsId"
                radBizDocsDocuments.DataBind()

                radBizDocsDocuments.ShowDropDownOnTextboxClick = True
                radBizDocsDocuments.CheckBoxes = True
                radBizDocsDocuments.EnableTextSelection = True
            End If
        End Sub
        Private Sub GetEmailIDs()
            Try
                If CCommon.ToString(GetQueryStringVal("MailMode")).ToLower().Trim(" ") <> "forward" Then

                    Dim numEmailHstrID As Decimal
                    numEmailHstrID = CCommon.ToLong(GetQueryStringVal("EmailHstrId"))
                    If numEmailHstrID > 0 Then
                        Dim dtTable As DataTable

                        Dim objOutlook As COutlook
                        objOutlook = New COutlook
                        objOutlook.numEmailHstrID = numEmailHstrID
                        ' hdnEmailId.Value = numEmailHstrID
                        objOutlook.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                        dtTable = objOutlook.getMail()

                        If dtTable.Rows.Count > 0 Then
                            'IEnd Pinkal Patel Date:12-Oct-2011
                            Dim replyMail As String = ""

                            If CCommon.ToString(dtTable.Rows(0).Item("Source")).ToLower() = "i" Then

                                If Not IsDBNull(dtTable.Rows(0).Item("FromEmail")) Then
                                    If CCommon.ToString(dtTable.Rows(0).Item("FromEmail")) <> "" Then
                                        Dim fromNameAddress As String() = dtTable.Rows(0).Item("FromEmail").ToString.Split(New String() {"$^$"}, StringSplitOptions.None)

                                        If fromNameAddress.Length >= 3 Then
                                            If CCommon.ToString(fromNameAddress(2)) <> Session("UserEmail") Then
                                                strToEMailID = strToEMailID & fromNameAddress(2)
                                            End If
                                        End If
                                        'Else : hypFrom.Text = HttpUtility.HtmlEncode(dtTable.Rows(0).Item("FromName"))
                                    End If
                                End If
                                If CCommon.ToString(GetQueryStringVal("MailMode")).ToLower().Trim(" ") = "replyall" Then
                                    If Not IsDBNull(dtTable.Rows(0).Item("CCEmail")) Then
                                        Dim ccName As String() = dtTable.Rows(0).Item("CCEmail").ToString.Split(New String() {"#^#"}, StringSplitOptions.RemoveEmptyEntries)

                                        If ccName.Length > 0 Then
                                            Dim CCLength As Integer = 0

                                            For CCLength = 0 To ccName.Length - 1
                                                Dim ccNameAddress As String() = ccName(CCLength).ToString.Split(New String() {"$^$"}, StringSplitOptions.None)

                                                If ccNameAddress.Length >= 3 Then
                                                    'Author:Sachin Sadhu||Date:3-Dec-2013||Case NO:513   
                                                    'UnCommented below line not to include sender in CC
                                                    If CCommon.ToString(ccNameAddress(2)) <> Session("UserEmail") Then
                                                        strCcEMailID = strCcEMailID & (ccNameAddress(2).ToString) & ","
                                                    End If
                                                    'End of modification by sachin
                                                End If
                                            Next
                                        End If
                                    End If


                                    If Not IsDBNull(dtTable.Rows(0).Item("ToEmail")) Then
                                        Dim toName As String() = dtTable.Rows(0).Item("ToEmail").ToString.Split(New String() {"#^#"}, StringSplitOptions.RemoveEmptyEntries)

                                        If toName.Length > 0 Then
                                            Dim toLength As Integer = 0
                                            For toLength = 0 To toName.Length - 1
                                                Dim toNameAddress As String() = toName(toLength).ToString.Split(New String() {"$^$"}, StringSplitOptions.None)

                                                If (toNameAddress.Length >= 3) Then
                                                    If CCommon.ToString(toNameAddress(2)) <> Session("UserEmail") Then
                                                        strCcEMailID = strCcEMailID & (CCommon.ToString(toNameAddress(2))) & ","
                                                    End If
                                                End If
                                            Next
                                        End If
                                    End If
                                End If
                            Else
                                If Not IsDBNull(dtTable.Rows(0).Item("ToEmail")) Then
                                    Dim toName As String() = dtTable.Rows(0).Item("ToEmail").ToString.Split(New String() {"#^#"}, StringSplitOptions.RemoveEmptyEntries)

                                    If toName.Length > 0 Then
                                        Dim toLength As Integer = 0
                                        For toLength = 0 To toName.Length - 1
                                            Dim toNameAddress As String() = toName(toLength).ToString.Split(New String() {"$^$"}, StringSplitOptions.None)

                                            If (toNameAddress.Length >= 3) Then
                                                If CCommon.ToString(toNameAddress(2)) <> Session("UserEmail") Then
                                                    strToEMailID = strToEMailID & (CCommon.ToString(toNameAddress(2))) & ","
                                                End If
                                            End If
                                        Next
                                    End If
                                End If
                                If CCommon.ToString(GetQueryStringVal("MailMode")).ToLower().Trim(" ") = "replyall" Then
                                    If Not IsDBNull(dtTable.Rows(0).Item("CCEmail")) Then
                                        Dim ccName As String() = dtTable.Rows(0).Item("CCEmail").ToString.Split(New String() {"#^#"}, StringSplitOptions.RemoveEmptyEntries)

                                        If ccName.Length > 0 Then
                                            Dim CCLength As Integer = 0

                                            For CCLength = 0 To ccName.Length - 1
                                                Dim ccNameAddress As String() = ccName(CCLength).ToString.Split(New String() {"$^$"}, StringSplitOptions.None)

                                                If ccNameAddress.Length >= 3 Then
                                                    'Author:Sachin Sadhu||Date:3-Dec-2013||Case NO:513   
                                                    'UnCommented below line not to include sender in CC
                                                    If CCommon.ToString(ccNameAddress(2)) <> Session("UserEmail") Then
                                                        strCcEMailID = strCcEMailID & (ccNameAddress(2).ToString) & ","
                                                    End If
                                                    'End of Modification by Sachin
                                                End If
                                            Next
                                        End If
                                    End If
                                End If
                            End If
                        End If
                        strCcEMailID = strCcEMailID.Trim(",")
                        strToEMailID = strToEMailID.Trim(",")
                    End If

                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        ' modified by tarun
        ' reason : load email templates for this user only
        Sub LoadTemplates()
            Try
                Dim objCampaign As New Campaign
                Dim dtTable As DataTable
                Dim currentUser As Int32 = -1
                Dim currentDomainID As Int32 = -1
                If Not Session("UserID") Is Nothing And Not Session("DomainID") Is Nothing Then
                    currentUser = Convert.ToInt32(Session("UserID").ToString())
                    currentDomainID = Convert.ToInt32(Session("DomainID").ToString())
                End If
                objCampaign.UserCntID = Session("UserContactID")
                objCampaign.DomainID = Session("DomainID")
                objCampaign.numGroupId = Session("UserGroupID")
                objCampaign.numCategoryId = ddlCategory.SelectedValue

                If ((GetQueryStringVal("pluYR") <> "") Or (CCommon.ToLong(GetQueryStringVal("OppID")) > 0)) Then
                    If ((GetQueryStringVal("BizDocID") = "") And GetQueryStringVal("CaseBizDoc").ToString() = "") Then
                        objCampaign.screen = "Opp/Order"
                    ElseIf (((GetQueryStringVal("BizDocID") = "") And (GetQueryStringVal("CaseBizDoc").ToString() = "MirrorBizDoc")) Or ((GetQueryStringVal("BizDocID") <> "") And (GetQueryStringVal("CaseBizDoc").ToString() = ""))) Then
                        objCampaign.screen = "BizDoc"
                    End If
                Else
                    objCampaign.screen = ""

                End If
                '    objCampaign.screen = "Opp/Order"
                'ElseIf (((GetQueryStringVal("pluYR") <> "") Or (CCommon.ToLong(GetQueryStringVal("OppID")) > 0)) And (GetQueryStringVal("BizDocID") <> "")) Then
                '    objCampaign.screen = "BizDoc"
                'Else
                '    objCampaign.screen = ""
                'End If

                dtTable = objCampaign.GetUserEmailTemplates_ComposeEmail
                ddlEmailtemplate.DataSource = dtTable
                ddlEmailtemplate.DataTextField = "VcDocName"
                ddlEmailtemplate.DataValueField = "numGenericDocID"
                ddlEmailtemplate.DataBind()
                ddlEmailtemplate.Items.Insert(0, "-- Select One --")
                ddlEmailtemplate.Items.FindByText("-- Select One --").Value = 0
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub BindBizDocsCategory()
            Try
                objCommon.DomainID = Session("DomainID")
                objCommon.ListID = 29
                ddlCategory.Items.Clear()
                ddlCategory.DataSource = objCommon.GetMasterListItemsWithOutDefaultItemsRights
                ddlCategory.DataTextField = "vcData"
                ddlCategory.DataValueField = "numListItemID"
                ddlCategory.DataBind()
                ddlCategory.Items.Insert(0, New ListItem("-- Select One --", "0"))

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub ddlCategory_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
            Try
                BindFollowUpStatus()
                LoadTemplates()
                'PersistEmailIdOnPostback()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Private Sub ddlEmailtemplate_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlEmailtemplate.SelectedIndexChanged
            Try
                BindFollowUpStatus()
                If ddlEmailtemplate.SelectedIndex > 0 Then
                    Dim dtDocDetails As DataTable
                    Dim objDocuments As New DocumentList
                    With objDocuments
                        .GenDocID = ddlEmailtemplate.SelectedItem.Value
                        .DomainID = Session("DomainID")
                        dtDocDetails = .GetDocByGenDocID
                    End With
                    Dim strDesc As String
                    strDesc = dtDocDetails.Rows(0).Item("vcDocdesc")

                    txtSubject.Text = IIf(IsDBNull(dtDocDetails.Rows(0).Item("vcSubject")), "", dtDocDetails.Rows(0).Item("vcSubject"))
                    hdnFollowupDateUpdateTemplate.Value = IIf(IsDBNull(dtDocDetails.Rows(0).Item("bitLastFollowupUpdate")), 0, dtDocDetails.Rows(0).Item("bitLastFollowupUpdate"))
                    RadEditor1.Content = strDesc
                    If Not ddlFollowupStatus.Items.FindByValue(dtDocDetails.Rows(0).Item("numFollowUpStatusId")) Is Nothing Then
                        ddlFollowupStatus.Items.FindByValue(dtDocDetails.Rows(0).Item("numFollowUpStatusId")).Selected = True
                    End If
                    chkFollowUpStatus.Checked = dtDocDetails.Rows(0).Item("bitUpdateFollowUpStatus")
                    'BizDoc Template (Added by Priya 24 Feb 2018)
                    If (dtDocDetails.Rows(0).Item("numModuleID") = 8) Then
                        If (RadEditor1.Content.Contains("BizDocTemplateFromGlobalSettings")) Then
                            hdnBizDocHtmlCss.Value = "##BizDocTemplateFromGlobalSettings##"
                        ElseIf (Not (RadEditor1.Content.Contains("BizDocTemplateFromGlobalSettings"))) Then
                            hdnBizDocHtmlCss.Value = "##BizDocTemplateWithoutHtmlandCSS##"
                        End If
                        hdnBizDocTempId.Value = CCommon.ToLong(dtDocDetails.Rows(0).Item("BizDocTemplate"))
                    End If

                    hdnModuleId.Value = dtDocDetails.Rows(0).Item("numModuleID").ToString()

                    If CCommon.ToString(dtDocDetails.Rows(0).Item("vcContactPosition")).Trim().Length > 0 AndAlso CCommon.ToLong(hdnDivisionId.Value) > 0 Then
                        Dim dtContactPositionEmail As DataTable
                        With objDocuments
                            .GenDocID = ddlEmailtemplate.SelectedItem.Value
                            .DomainID = Session("DomainID")
                            .DivisionID = CCommon.ToLong(hdnDivisionId.Value)
                            dtContactPositionEmail = .GetEmailTemplateContactPostionEmail
                        End With

                        If dtContactPositionEmail.Rows.Count > 0 Then
                            Dim strJSON As New StringBuilder
                            strJSON.Append("[")

                            For Each dr As DataRow In dtContactPositionEmail.Rows
                                strJSON.Append("{")
                                strJSON.Append("""id"":""" & dr("numContactId").ToString() & """,")
                                strJSON.Append("""first_name"":""" & dr("vcFirstName").ToString() & """,")
                                strJSON.Append("""last_name"":""" & dr("vcLastName").ToString() & """,")
                                strJSON.Append("""email"":""" & dr("vcEmail").ToString() & """,")
                                strJSON.Append("""url"":""#""")
                                strJSON.Append("},")
                            Next

                            strJSON.Remove(strJSON.ToString.Length - 1, 1)
                            strJSON.Append("]")
                            hdnpreData.Value = strJSON.ToString()
                        End If
                    End If

                    '  oEdit1.Content = strDesc
                Else
                    hdnModuleId.Value = 45
                    hdnFollowupDateUpdateTemplate.Value = 0
                End If

                'PersistEmailIdOnPostback()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        Sub BindFollowUpStatus()
            Try
                ddlFollowupStatus.Items.Clear()
                Dim dt As New DataTable()
                objCommon.DomainID = CLng(HttpContext.Current.Session("DomainID"))
                objCommon.ListID = CCommon.ToLong(30)
                dt = objCommon.GetMasterListItemsWithRights()
                Dim item As ListItem = New ListItem("-- All --", "0")
                ddlFollowupStatus.Items.Add(item)
                ddlFollowupStatus.AutoPostBack = False
                For Each dr As DataRow In dt.Rows
                    item = New ListItem(CCommon.ToString(dr("vcData")), CCommon.ToString(dr("numListItemID")))
                    item.Attributes("OptionGroup") = CCommon.ToString(dr("vcListItemGroupName"))
                    item.Attributes("class") = CCommon.ToString(dr("vcColorScheme"))
                    ddlFollowupStatus.Items.Add(item)
                Next
            Catch ex As Exception

            End Try
        End Sub
        Private Sub ddlEmailtemplateSelectedIndexChanged()
            Try
                If ddlEmailtemplate.SelectedIndex > 0 Then
                    Dim dtDocDetails As DataTable
                    Dim objDocuments As New DocumentList
                    With objDocuments
                        .GenDocID = ddlEmailtemplate.SelectedItem.Value
                        .DomainID = Session("DomainID")
                        dtDocDetails = .GetDocByGenDocID
                    End With
                    Dim strDesc As String
                    strDesc = dtDocDetails.Rows(0).Item("vcDocdesc")

                    txtSubject.Text = IIf(IsDBNull(dtDocDetails.Rows(0).Item("vcSubject")), "", dtDocDetails.Rows(0).Item("vcSubject"))
                    RadEditor1.Content = strDesc
                    '  oEdit1.Content = strDesc
                End If
                'PersistEmailIdOnPostback()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        ' Private objUserContactId As Object = Session("UserContactID")
        'Public  objUser
        <WebMethod()>
        <Script.Services.ScriptMethod()>
        Public Shared Function SayHello(ByVal strEMail As String) As String
            Dim objContacts As New CContacts
            Dim dtContacts As DataTable
            ' Dim strEmail As String = tbAuto.Text
            With objContacts
                If strEMail.Contains(",") Then
                    .KeyWord = strEMail.Substring(strEMail.LastIndexOf(",") + 1)
                    If .KeyWord.Length <= 1 Then Return Nothing
                Else
                    .KeyWord = strEMail
                End If
                '.UserCntID = CLng(objUserContactId)
                '.UserRightType = hdnUserRights.Value
                .DomainID = 1 ' Session("DomainID")
                .CurrentPage = 1
                .PageSize = 100
                .TotalRecords = 0
                .columnName = "ADC.bintcreateddate"
                'If Session("Asc") = 1 Then
                .columnSortOrder = "Desc"
                'Else : .columnSortOrder = "Asc"
                'End If
            End With
            objContacts.bitFlag = False
            dtContacts = objContacts.GetContactEmailList
            Dim ListContacts As String = ""
            Dim intCount As Integer = 0
            ListContacts = "["
            For intCount = 0 To dtContacts.Rows.Count - 1
                ListContacts = ListContacts & "{'id':" & dtContacts.Rows(intCount)("vcFirstName") & ",'name'" & dtContacts.Rows(intCount)("vcEmail").ToString
            Next
            ListContacts = ListContacts & "]"
            Return ListContacts




        End Function

        Private Sub IsSMTPEnable()
            Try
                Dim objUserAccess As New UserAccess
                objUserAccess.DomainID = CCommon.ToLong(Session("DomainID"))
                objUserAccess.UserCntID = CCommon.ToLong(Session("UserContactID"))
                Dim dt As DataTable = objUserAccess.GetMailRefreshToken()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    If (CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 1 Or CCommon.ToShort(dt.Rows(0)("tintMailProvider")) = 2) Then
                        If String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailAccessToken"))) Or String.IsNullOrEmpty(CCommon.ToString(dt.Rows(0)("vcMailRefreshToken"))) Then
                            pnlSMTPError.Visible = True
                            Page.ClientScript.RegisterStartupScript(Me.GetType, "disableEmail", "disableEmail();", True)
                        End If

                        Exit Sub
                    End If
                End If

                objUserAccess.UserId = Session("UserID")
                Dim dtUserAccessDetails As DataTable = objUserAccess.GetUserAccessDetails
                If dtUserAccessDetails.Rows.Count > 0 Then
                    If Not CCommon.ToBool(dtUserAccessDetails.Rows(0)("bitSMTPServer")) Then
                        pnlSMTPError.Visible = True
                        Page.ClientScript.RegisterStartupScript(Me.GetType, "disableEmail", "disableEmail();", True)
                        hplImap.Attributes.Add("onclick", "return openSMTP(" & Session("UserID").ToString & ")")
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub LoadMergeField()
            Try
                Dim objDocument As New DocumentList
                objDocument.BindEmailTemplateMergeFields(RadEditor1, 1)
                ''***************************************************
                ''	USING CUSTOM TAG INSERTION FEATURE
                ''***************************************************
                ''This custom tag will be utilised by mail merge only when contact id is present for given email id
                'RadEditor1.Snippets.Clear()
                'RadEditor1.Snippets.Add("<b>Insert</b>", "")
                'RadEditor1.Snippets.Add("First Name", "##ContactFirstName##")
                'RadEditor1.Snippets.Add("Last Name", "##ContactLastName##")
                'RadEditor1.Snippets.Add("Company Name", "##OrganizationName##")
                'RadEditor1.Snippets.Add("Shipping Address", "##Ship##")
                If CCommon.ToLong(GetQueryStringVal("CaseID")) > 0 Then
                    RadEditor1.Snippets.Add("Case Number", "##vcCaseNumber##")
                    RadEditor1.Snippets.Add("Item Name", "##vcItemName##")
                    RadEditor1.Snippets.Add("Model", "##vcModelID##")
                    RadEditor1.Snippets.Add("Item Description", "##vcItemDesc##")
                    RadEditor1.Snippets.Add("Units", "##numUnitHour##")
                    RadEditor1.Snippets.Add("Serial No", "##vcSerialNo##")

                    'Add First Solution added to case into Email Body
                    Dim objSolution As New Solution
                    Dim dtSolution As DataTable
                    objSolution.CaseID = CCommon.ToLong(GetQueryStringVal("CaseID"))
                    dtSolution = objSolution.GetSolutionForCases
                    If dtSolution.Rows.Count > 0 Then
                        RadEditor1.Content = CCommon.ToString(dtSolution.Rows(0)("Solution Descrition"))
                    End If
                End If
                'Commented by chinta Bug id 612
                'If CCommon.ToInteger(GetQueryStringVal("PickAtch")) = 1 And GetQueryStringVal("BizDocID") <> "" Then
                '    Dim ddl As Telerik.Web.UI.EditorDropDown = CType(RadEditor1.FindTool("MergeField"), Telerik.Web.UI.EditorDropDown)
                '    ddl.Items.Add("BizDocID", "##BizDocID##")
                'End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub AddCaseMergeColumns(ByVal lngCaseID As Long, ByRef dtMergeField As DataTable)
            Try
                If dtMergeField.Rows.Count > 0 And lngCaseID > 0 Then
                    Dim dtCase As DataTable
                    Dim objCase As New CCases

                    objCase.CaseID = lngCaseID
                    objCase.DomainID = Session("DomainID")

                    'Add Case Number Field
                    objCase.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    dtCase = objCase.GetCaseDTL()
                    If dtCase.Rows.Count > 0 Then
                        dtMergeField.Columns.Add("vcCaseNumber")
                        For Each dr As DataRow In dtMergeField.Rows
                            dr("vcCaseNumber") = CCommon.ToString(dtCase.Rows(0)("vcCaseNumber"))
                        Next
                    End If
                    dtMergeField.AcceptChanges()

                    'Add Case Item fields
                    objCase.strOppSel = ""
                    dtCase = objCase.GetCaseOpportunities()
                    Dim drCase As DataRow
                    If dtCase.Rows.Count >= 1 Then
                        drCase = dtCase.Rows(0) 'as of now email template will support one item 
                    Else : Exit Sub
                    End If

                    dtMergeField.Columns.Add("vcItemName")
                    dtMergeField.Columns.Add("vcModelID")
                    dtMergeField.Columns.Add("vcItemDesc")
                    dtMergeField.Columns.Add("numUnitHour")
                    dtMergeField.Columns.Add("vcSerialNo")
                    dtMergeField.AcceptChanges()

                    For Each dr As DataRow In dtMergeField.Rows

                        dr("vcItemName") = drCase("vcItemName")
                        dr("vcModelID") = drCase("vcModelID")
                        dr("vcItemDesc") = drCase("vcItemDesc")
                        dr("numUnitHour") = drCase("numUnitHour")
                        dr("vcSerialNo") = drCase("vcSerialNo")
                    Next
                    dtMergeField.AcceptChanges()
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Public Function CreateJSON(ByVal strEmail As String, ByVal lngContactID As Long) As String
            Dim mail As String()
            Dim intMailCount As Integer = 0
            Dim strJSON As New StringBuilder

            If strEmail.ToString <> "" Then
                mail = strEmail.Split(",")
                ''Dim intMailCount As Integer = 0
                ''Dim strJSON As New StringBuilder
                strJSON.Append("[")
                For intMailCount = 0 To mail.Length - 1
                    strJSON.Append("{")
                    strJSON.Append("""id"":""" & lngContactID.ToString() & """,")
                    strJSON.Append("""first_name"":""" & mail(intMailCount) & """,")
                    strJSON.Append("""last_name"":"""",")
                    strJSON.Append("""email"":""" & mail(intMailCount) & """,")
                    strJSON.Append("""url"":""#""")
                    'nameList.Append("""Firstname"":""" & dtContacts.Rows(intCount)("vcFirstName") & """")
                    strJSON.Append("},")
                Next
                strJSON.Remove(strJSON.ToString.Length - 1, 1)
                strJSON.Append("]")
                'strJSON.Remove(0, 1)
                'strJSON.Remove(strJSON.Length - 1, 1)
                Return strJSON.ToString
            Else
                Return strJSON.ToString
            End If
        End Function

        Private Sub FetchAutomatedFollowUpDetails(ByVal numDomainId As Long, ByVal numContactId As Long)
            Try
                Dim dsFollowupdetails As New DataSet
                dsFollowupdetails = objCommon.FetchFollowUpCampaignDetails(numDomainId, numContactId)

                Dim dtFollowupTable As DataTable
                dtFollowupTable = dsFollowupdetails.Tables(0)
                If dtFollowupTable.Rows.Count > 0 Then
                    lblFollowupCampaign.Text = dtFollowupTable.Rows(0).Item("FollowUpCampaign")
                    lblLastFollowUp.Text = dtFollowupTable.Rows(0).Item("LastFollowUp")
                    lblNextFollowUp.Text = dtFollowupTable.Rows(0).Item("NextFollowUp")
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Function GetBizDocItems(ByVal ds As DataSet, ByVal strDecimalPoint As String) As String
            Dim stringBuilder As New StringBuilder
            stringBuilder.Append("<table style=""background-color:White;border-style:None;font-size:10px;width:100%;border-collapse:collapse;"">")
            Dim i As Integer
            Dim j As Integer
            Dim dtdgColumns As DataTable

            If (GetQueryStringVal("CaseBizDoc").ToString() = "MirrorBizDoc") Then
                dtdgColumns = ds.Tables(1)
            Else
                dtdgColumns = ds.Tables(2)
            End If


            If dtdgColumns.Rows.Count > 0 Then
                'Generates Header Row
                stringBuilder.Append("<tr class=""ItemHeader"">")
                For i = 0 To dtdgColumns.Rows.Count - 1

                    If CCommon.ToString(dtdgColumns.Rows(i).Item("vcDbColumnName")) = "txtItemDesc" And dtdgColumns.Rows.Count = 2 Then
                        stringBuilder.Append("<td class=""TwoColumns"">" + CCommon.ToString(dtdgColumns.Rows(i).Item("vcFormFieldName")) + "</td>")
                    ElseIf CCommon.ToString(dtdgColumns.Rows(i).Item("vcDbColumnName")) = "txtItemDesc" And dtdgColumns.Rows.Count = 1 Then
                        stringBuilder.Append("<td class=""OneColumns"">" + CCommon.ToString(dtdgColumns.Rows(i).Item("vcFormFieldName")) + "</td>")
                    ElseIf CCommon.ToString(dtdgColumns.Rows(i).Item("vcDbColumnName")) = "txtItemDesc" And dtdgColumns.Rows.Count = 3 Then
                        stringBuilder.Append("<td class=""ThreeColumns"">" + CCommon.ToString(dtdgColumns.Rows(i).Item("vcFormFieldName")) + "</td>")
                    Else
                        stringBuilder.Append("<td>" + CCommon.ToString(dtdgColumns.Rows(i).Item("vcFormFieldName")) + "</td>")
                    End If

                    'TODO: Implement date format code
                    'If dtdgColumns.Rows(i).Item("vcFieldDataType") = "M" Then bColumn.DataFormatString = CCommon.GetDataFormatString()
                Next
                stringBuilder.Append("</tr>")

                'Generates Data Rows
                Dim dtOppBiDocItems As DataTable = ds.Tables(0)

                For j = 0 To dtOppBiDocItems.Rows.Count - 1
                    If j Mod 2 = 0 Then
                        stringBuilder.Append("<tr class=""ItemStyle"">")
                        'stringBuilder.Append("<tr style=""page-break-inside : avoid"" class=""ItemStyle"">")
                    Else
                        stringBuilder.Append("<tr class=""ItemStyle"">")
                        'stringBuilder.Append("<tr style=""page-break-inside : avoid"" class=""AltItemStyle"">")
                    End If

                    For Each drColumn As DataRow In dtdgColumns.Rows
                        stringBuilder.Append("<td>")

                        If CCommon.ToString(drColumn("vcFieldDataType")) = "M" Then
                            stringBuilder.Append(String.Format(CCommon.GetDataFormatStringService(strDecimalPoint), dtOppBiDocItems.Rows(j)(CCommon.ToString(drColumn("vcDbColumnName")))))
                        Else
                            stringBuilder.Append(dtOppBiDocItems.Rows(j)(CCommon.ToString(drColumn("vcDbColumnName"))))
                        End If

                        stringBuilder.Append("</td>")
                    Next
                    stringBuilder.Append("</tr>")
                Next
            End If

            stringBuilder.Append("</table>")

            Return stringBuilder.ToString()
        End Function

        Private Function GetBizDocSummary(ByVal dtOppBiDocDtl As DataTable,
                                          ByVal ds As DataSet,
                                          ByVal domainId As Long,
                                          ByVal subTotal As String,
                                          ByVal taxAmount As String,
                                          ByVal crvTaxAmount As String,
                                          ByVal lateCharge As String,
                                          ByVal disc As String,
                                          ByVal creditAmount As String,
                                          ByVal grandTotal As Double, ByVal strDecimalPoints As String) As String
            Dim stringBuilder As New StringBuilder

            stringBuilder.Append("<table id=""tblBizDocSumm"" style=""page-break-inside : avoid"">")

            Dim dtOppBiDocItems As DataTable = ds.Tables(0)

            Dim objConfigWizard As New FormConfigWizard

            If CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("tintOppType")) = 1 Then
                objConfigWizard.FormID = 7
            Else
                objConfigWizard.FormID = 8
            End If

            objConfigWizard.DomainID = domainId
            objConfigWizard.BizDocID = CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numBizDocId"))
            objConfigWizard.BizDocTemplateID = CCommon.ToLong(dtOppBiDocDtl.Rows(0).Item("numBizDocTempID"))

            Dim dsNew As DataSet
            Dim dtTable As DataTable
            dsNew = objConfigWizard.GetFieldFormListForBizDocsSumm
            dtTable = dsNew.Tables(1)

            For Each dr As DataRow In dtTable.Rows

                stringBuilder.Append("<tr>")
                stringBuilder.Append("<td class=""normal1"">")
                stringBuilder.Append(CCommon.ToString(dr("vcFormFieldName")) & ": ")
                stringBuilder.Append("</td>")
                stringBuilder.Append("<td class=""normal1"">")
                If CCommon.ToString(dr("vcDbColumnName")) = "SubTotal" Then
                    stringBuilder.Append(dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + subTotal)
                ElseIf CCommon.ToString(dr("vcDbColumnName")) = "ShippingAmount" Then
                    stringBuilder.Append(dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + CCommon.GetDecimalFormatService(dtOppBiDocDtl.Rows(0).Item("monShipCost"), strDecimalPoints))
                ElseIf CCommon.ToString(dr("vcDbColumnName")) = "TotalSalesTax" Then
                    stringBuilder.Append(dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + taxAmount)
                ElseIf CCommon.ToString(dr("vcDbColumnName")) = "TotalCRVTax" Then
                    stringBuilder.Append(dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + crvTaxAmount)
                ElseIf CCommon.ToString(dr("vcDbColumnName")) = "LateCharge" Then
                    stringBuilder.Append(dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + lateCharge)
                ElseIf CCommon.ToString(dr("vcDbColumnName")) = "Discount" Then
                    stringBuilder.Append(dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + disc)
                ElseIf CCommon.ToString(dr("vcDbColumnName")) = "CreditApplied" Then
                    stringBuilder.Append(dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + creditAmount)
                ElseIf CCommon.ToString(dr("vcDbColumnName")) = "GrandTotal" Then
                    stringBuilder.Append(dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + CCommon.GetDecimalFormatService(grandTotal, strDecimalPoints))
                Else
                    If CCommon.ToInteger(dtOppBiDocDtl.Rows(0).Item("tintOppType")) = 1 Then
                        Dim taxAmt As Double
                        taxAmt = CCommon.ToDouble(IIf(IsDBNull(dtOppBiDocItems.Compute("SUM([" & CCommon.ToString(dr("vcFormFieldName")) & "])", "[" & CCommon.ToString(dr("vcFormFieldName")) & "]>0")), 0, dtOppBiDocItems.Compute("SUM([" & CCommon.ToString(dr("vcFormFieldName")) & "])", "[" & CCommon.ToString(dr("vcFormFieldName")) & "]>0")))
                        stringBuilder.Append(dtOppBiDocDtl.Rows(0).Item("varCurrSymbol").ToString() + " " + CCommon.GetDecimalFormatService(taxAmt, strDecimalPoints))
                    End If
                End If
                stringBuilder.Append("</td>")
                stringBuilder.Append("<tr>")
            Next

            stringBuilder.Append("</table>")

            Return stringBuilder.ToString()
        End Function


        Protected Sub lkbSend_Click(sender As Object, e As EventArgs)
            Try
                If CCommon.ToShort(GetQueryStringVal("isAttachmentRequired")) = 1 AndAlso Session("Attachements") Is Nothing Then
                    Response.Write("Attachement(s) are required")
                    Exit Sub
                End If

                Dim strBizDocUI As String
                Dim strCss As String
                Dim strvcale As String = txtcc.Value
                Dim strccale As String = txtTo.Value

                If Session("Trial") = "False" Then
                    Dim i As Integer
                    Dim objContacts As New CContacts
                    Dim objEmail As New Email

                    Dim dtEmailBroadCast As New DataTable

                    Dim strContacts() As String = hdnSelectedEmails.Value.Split(New String() {"|"}, StringSplitOptions.RemoveEmptyEntries) '& hdnSelectedEmailsCC.Value & hdnSelectedEmailsBCC.Value
                    Dim strContactIDs As String = String.Empty
                    Dim alEmail As New ArrayList
                    Dim lngContactID As Long
                    For i = 0 To strContacts.Length - 1
                        lngContactID = strContacts(i).Split("~")(2)
                        If lngContactID > 0 Then
                            strContactIDs = strContactIDs + lngContactID.ToString + ","
                        Else
                            alEmail.Add(strContacts(i).Split("~")(1))
                        End If
                    Next
                    If CCommon.ToBool(hdnFollowupDateUpdateTemplate.Value) = True Then
                        Dim objCampaign As New Campaign()
                        objCampaign.DomainID = Session("DomainID")
                        objCampaign.strBroadCastDtls = strContactIDs
                        objCampaign.bitUpdateFollowUpStatus = chkFollowUpStatus.Checked
                        objCampaign.numFollowUpStatusId = ddlFollowupStatus.SelectedValue
                        objCampaign.UpdateFollowUpDates()
                    End If
                    'Added by Priya 24 Feb 2018(BizDoc Template) 
                    Dim OppID, OppBizDocId As Long
                    If (CCommon.ToLong(hdnBizDocTempId.Value) <> 0) Then

                        Dim dtOppBiDocDtl As DataTable
                        Dim objOppBizDocs As New OppBizDocs
                        OppID = CCommon.ToLong(GetQueryStringVal("OppID"))
                        If (GetQueryStringVal("CaseBizDoc").ToString() = "") Then

                            OppBizDocId = CCommon.ToLong(GetQueryStringVal("BizDocID"))

                            objOppBizDocs.OppBizDocId = OppBizDocId
                            objOppBizDocs.OppId = OppID
                            objOppBizDocs.DomainID = Session("DomainID")
                            objOppBizDocs.UserCntID = Session("UserContactID")
                            objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                            dtOppBiDocDtl = objOppBizDocs.GetOppBizDocDtl

                        ElseIf (GetQueryStringVal("CaseBizDoc").ToString() = "MirrorBizDoc") Then

                            objOppBizDocs.ReferenceType = CCommon.ToLong(GetQueryStringVal("RefType"))
                            objOppBizDocs.ReferenceID = OppID
                            objOppBizDocs.DomainID = Session("DomainID")
                            objOppBizDocs.UserCntID = Session("UserContactID")
                            objOppBizDocs.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                            Dim ds1 As DataSet = objOppBizDocs.GetMirrorBizDocDtls
                            dtOppBiDocDtl = ds1.Tables(0)

                            OppBizDocId = dtOppBiDocDtl.Rows(0)("numBizDocId").ToString()
                        End If

                        Dim objOppBizDoc As New OppBizDocs
                        objOppBizDoc.BizDocTemplateID = CCommon.ToLong(hdnBizDocTempId.Value)

                        Dim dsAddress As DataSet
                        objOppBizDocs.OppId = OppID
                        objOppBizDocs.DomainID = Session("DomainID")
                        objOppBizDocs.OppBizDocId = OppBizDocId
                        dsAddress = objOppBizDocs.GetOPPGetOppAddressDetails

                        '(BizDoc Template With HTML and CSS)
                        If (hdnBizDocHtmlCss.Value.ToString() = "##BizDocTemplateFromGlobalSettings##") Then

                            If dtOppBiDocDtl.Rows(0)("bitEnabled") = True And dtOppBiDocDtl.Rows(0)("txtBizDocTemplate").ToString().Length > 0 Then

                                strCss = "<style>" & Server.HtmlDecode(dtOppBiDocDtl.Rows(0)("txtCSS").ToString()) & "</style>"
                                strBizDocUI = HttpUtility.HtmlDecode(dtOppBiDocDtl.Rows(0)("txtBizDocTemplate").ToString())

                                If strBizDocUI.Length > 0 Then
                                    Dim objWorkflowExecution As New BACRM.BusinessLogic.Workflow.WorkFlowExecution
                                    strBizDocUI = objWorkflowExecution.GetBizDocHtmlAfterReplacingTags(strBizDocUI, dtOppBiDocDtl, dsAddress, objOppBizDocs.DomainID, OppID, OppBizDocId, "MM/DD/YYYY")
                                End If
                            End If
                        ElseIf (hdnBizDocHtmlCss.Value.ToString() = "##BizDocTemplateWithoutHtmlandCSS##") Then
                            Dim objWorkflowExecution As New BACRM.BusinessLogic.Workflow.WorkFlowExecution
                            strBizDocUI = objWorkflowExecution.GetBizDocHtmlAfterReplacingTags(RadEditor1.Content.ToString(), dtOppBiDocDtl, dsAddress, objOppBizDocs.DomainID, OppID, OppBizDocId, "MM/DD/YYYY")
                            RadEditor1.Content = strBizDocUI
                        End If

                        'End If
                    Else
                        'Fetch details of contact with following columns  numDivisionID,numContactID,vcFirstName,vcLastName,Email,vcCompanyName,Ship,Opt-Out
                        'If any other module Template and not BizDoc Template
                        'If strContactIDs.Length > 0 Then

                        objContacts.strContactIDs = strContactIDs.TrimEnd(",")

                        If (CCommon.ToLong(hdnModuleId.Value) = 2) Then
                            If (CCommon.ToLong(GetQueryStringVal("OppID")) > 0) Then
                                strContactIDs = GetQueryStringVal("OppID").ToString()
                            Else
                                strContactIDs = GetQueryStringVal("pluYR").ToString()
                            End If
                            Dim numOppBizDocId As Long = IIf((GetQueryStringVal("BizDocID") <> ""), CCommon.ToLong(GetQueryStringVal("BizDocID")), 0)
                            dtEmailBroadCast = objEmail.GetEmailMergeData(CCommon.ToLong(hdnModuleId.Value), strContactIDs, Session("DomainID"), 0, 0, numOppBizDocId)

                            For Each drrow As DataRow In dtEmailBroadCast.Rows
                                Dim strTrackingNo As String = ""
                                If (Not String.IsNullOrEmpty(CCommon.ToString(drrow("OppOrderTrackingNo")).Trim())) Then
                                    Dim strTrackingNoList() As String = CCommon.ToString(drrow("OppOrderTrackingNo")).Split(New String() {"$^$"}, StringSplitOptions.None)
                                    Dim strIDValue() As String

                                    If strTrackingNoList.Length > 0 Then
                                        For k As Integer = 0 To strTrackingNoList.Length - 1
                                            strIDValue = strTrackingNoList(k).Split(New String() {"#^#"}, StringSplitOptions.None)
                                            If strIDValue(1).Length > 0 Then
                                                strTrackingNo += String.Format("<a href='{0}' target='_blank'>{1}</a>&nbsp;&nbsp;", IIf(strIDValue(0).Length = 0, "javascript:void(0)", String.Format(strIDValue(0) + strIDValue(1), strIDValue(1))), strIDValue(1))
                                            End If
                                        Next
                                    End If
                                End If
                                drrow("OppOrderTrackingNo") = "<div style='width: 95%;word-break: break-all;'>" & strTrackingNo & "</div>"
                            Next
                        Else
                            'Add rows to datatable with only email id, where contact id does not exist
                            Dim iEnum As IEnumerator = alEmail.GetEnumerator()
                            Dim dr As DataRow
                            If dtEmailBroadCast.Columns.Count = 0 Then
                                objContacts.strContactIDs = "0"
                                dtEmailBroadCast = objEmail.GetEmailMergeData(CCommon.ToLong(hdnModuleId.Value), strContactIDs, Session("DomainID"), 0, 0)
                            End If
                            While iEnum.MoveNext()
                                dr = dtEmailBroadCast.NewRow
                                dr("ContactEmail") = iEnum.Current.ToString()
                                dr("ContactFirstName") = ""
                                dtEmailBroadCast.Rows.Add(dr)
                            End While
                            dtEmailBroadCast.AcceptChanges()
                        End If

                        'End If
                    End If

                    'Dim testURL As New Bluepay("100403839821", "GWB7RSPKYCC4284KKMZ5LY/N0/KUK4K7", "TEST")
                    ''SHPF_FORM_ID SHPF_ACCOUNT_ID MODE TPS_HASH_TYPE INVOICE_ID ORDER_ID ORDER_NAME INVOICE_NAME AMOUNT
                    'Dim generatedURL As String = testURL.generateURLForBiz("bizauto01", "http://portal.bizautomation.com/common/BluePayLinkPayment.aspx", "100403839821 TEST HMAC_SHA512 950137 777530 49", "MERCHANT MODE TPS_HASH_TYPE ORDER_ID INVOICE_ID AMOUNT", "HMAC_SHA512", "777530", "950137", "SO-1515", "INV-324", "49", "1/logo.png")

                    'objEmail.AdditionalMergeFields.Add("PaymentURL", generatedURL)

                    'Add BizDoc fields when email has bizdoc as atachment-bug id 612 
                    If (CCommon.ToInteger(GetQueryStringVal("PickAtch")) = 1 And (CCommon.ToLong(hdnModuleId.Value) = 8) And (GetQueryStringVal("BizDocID") <> "" Or GetQueryStringVal("CaseBizDoc").ToString() = "MirrorBizDoc")) Then
                        'Add BizDoc columns into merge table. when user clicks on send email icon in bizdoc, it opens compose window, now it should merge all bizdoc field.
                        Dim dtBizDocMergeData As DataTable
                        If (GetQueryStringVal("CaseBizDoc").ToString() = "MirrorBizDoc") Then
                            dtBizDocMergeData = objEmail.GetEmailMergeDataMirrorBizDoc(Session("UserContactID"), Session("DomainID"), CCommon.ToLong(GetQueryStringVal("RefType")), OppID, Session("ClientMachineUTCTimeOffset"))
                        Else
                            dtBizDocMergeData = objEmail.GetEmailMergeData(8, GetQueryStringVal("BizDocID"), Session("DomainID"), 0, Session("ClientMachineUTCTimeOffset"))
                            If dtBizDocMergeData.Rows.Count > 0 Then
                                For Each col As DataColumn In dtBizDocMergeData.Columns
                                    objEmail.AdditionalMergeFields.Add(col.ColumnName, CCommon.ToString(dtBizDocMergeData.Rows(0)(col.ColumnName)))
                                Next
                                Dim strTrackingNo As String = ""
                                strTrackingNo = String.Format("<a href='{0}' target='_blank'>{1}</a>&nbsp;&nbsp;", IIf(CCommon.ToString(dtBizDocMergeData.Rows(0).Item("vcTrackingURL")) = "", "javascript:void(0)", String.Format(CCommon.ToString(dtBizDocMergeData.Rows(0).Item("vcTrackingURL")), CCommon.ToString(dtBizDocMergeData.Rows(0).Item("vcTrackingNo")))), CCommon.ToString(dtBizDocMergeData.Rows(0).Item("vcTrackingNo")))

                                objEmail.AdditionalMergeFields.Add("TrackingNo", strTrackingNo)
                            End If
                        End If

                        ' (BizDoc HTML & CSS Template) 
                        dtEmailBroadCast = dtBizDocMergeData
                        For Each drrow As DataRow In dtEmailBroadCast.Rows
                            If (drrow("BizDocTemplateFromGlobalSettings").ToString() Is "") Then
                                drrow("BizDocTemplateFromGlobalSettings") = strBizDocUI
                            End If
                        Next
                    End If

                    'Add Case columns into merge table e.g. Case No Item Name, Model ID, Item Description, Item thumbnail image (if possible), and serial # 
                    AddCaseMergeColumns(CCommon.ToLong(GetQueryStringVal("CaseID")), dtEmailBroadCast)

                    Dim lngEmailHstrID As Long
                    If hdnTo.Value.Length > 1 Then
                        hdnTo.Value = hdnTo.Value.TrimEnd(",")
                    End If
                    If hdnCC.Value.Length > 1 Then
                        hdnCC.Value = hdnCC.Value.TrimEnd(",")
                    End If
                    If hdnBCC.Value.Length > 1 Then
                        hdnBCC.Value = hdnBCC.Value.TrimEnd(",")
                    End If

                    objContacts.MessageTo = hdnMessageTo.Value.TrimEnd(New Char() {"#", "^"})
                    objContacts.MessageFrom = Session("ContactName") & "$^$" & Session("UserEmail")
                    objContacts.MessageFromName = Session("ContactName")
                    objContacts.Cc = hdnMessageCC.Value.TrimEnd(New Char() {"#", "^"})
                    objContacts.Bcc = hdnMessageBCC.Value.TrimEnd(New Char() {"#", "^"})
                    objContacts.Subject = txtSubject.Text
                    objContacts.Body = RadEditor1.Content
                    objContacts.ItemId = ""
                    objContacts.ChangeKey = ""
                    objContacts.bitRead = False
                    objContacts.Type = "B"
                    objContacts.tinttype = 2
                    If Session("Attachements") Is Nothing Then
                        objContacts.HasAttachment = False
                    Else : objContacts.HasAttachment = True
                    End If
                    objContacts.CreatedOn = DateTime.UtcNow
                    objContacts.MCategory = "B"
                    objContacts.MessageFromName = ""
                    objContacts.MessageToName = ""
                    objContacts.CCName = ""

                    Dim encoding As New ASCIIEncoding()
                    Dim bytes As Byte() = encoding.GetBytes(RadEditor1.Content)
                    Dim length As Long = bytes.Length

                    If Not Session("Attachements") Is Nothing Then
                        Dim dtTable As DataTable
                        Dim k As Integer
                        dtTable = Session("Attachements")
                        length += CCommon.ToLong(dtTable.Compute("Sum(FileSize)", ""))
                    End If

                    objContacts.Size = length
                    objContacts.AttachmentItemId = ""
                    objContacts.AttachmentType = ""
                    objContacts.FileName = ""
                    objContacts.DomainID = Session("DomainId")
                    objContacts.UserCntID = Session("UserContactId")
                    objContacts.BodyText = objCommon.StripTags(RadEditor1.Content, False)
                    objContacts.IsReplied = CCommon.ToBool(hdnIsReplied.Value)
                    objContacts.RepliedMailID = CCommon.ToLong(GetQueryStringVal("EmailHstrId"))
                    objContacts.bitInvisible = False   'chkInvisible.Checked  'By Priya

                    If GetQueryStringVal("frm") = "BizInvoice" Then
                        objContacts.OppBizDocID = CCommon.ToLong(GetQueryStringVal("BizDocID"))
                    End If

                    lngEmailHstrID = objContacts.InsertIntoEmailHstr()

                    ''Add To Correspondense if Open record is selected
                    If (CCommon.ToLong(ddlOpenRecord.SelectedValue) > 0 And lngEmailHstrID > 0) Or CCommon.ToLong(GetQueryStringVal("OppID")) > 0 Or CCommon.ToLong(GetQueryStringVal("fghTY")) > 0 Or CCommon.ToLong(GetQueryStringVal("tyrCV")) > 0 Or CCommon.ToLong(GetQueryStringVal("CaseID")) > 0 Then
                        Dim objActionItem As New ActionItem
                        objActionItem.CorrespondenceID = 0
                        objActionItem.CommID = 0
                        objActionItem.EmailHistoryID = lngEmailHstrID

                        '-----Automatically Pin this email to Order Correspondance bug id # 2122
                        If CCommon.ToInteger(GetQueryStringVal("PickAtch")) = 1 And GetQueryStringVal("BizDocID") <> "" And GetQueryStringVal("OppID") <> "" Then
                            objActionItem.OpenRecordID = CCommon.ToLong(GetQueryStringVal("OppID"))
                            objActionItem.CorrType = IIf(GetQueryStringVal("OppType") = "1", "3", "4")
                        ElseIf CCommon.ToLong(GetQueryStringVal("CaseID")) > 0 Then
                            objActionItem.CorrType = 6
                            objActionItem.OpenRecordID = CCommon.ToLong(GetQueryStringVal("CaseID"))
                        ElseIf CCommon.ToLong(GetQueryStringVal("fghTY")) > 0 Then
                            objActionItem.CorrType = 6
                            objActionItem.OpenRecordID = CCommon.ToLong(GetQueryStringVal("fghTY"))
                        ElseIf CCommon.ToLong(GetQueryStringVal("tyrCV")) > 0 Then
                            objActionItem.CorrType = 5
                            objActionItem.OpenRecordID = CCommon.ToLong(GetQueryStringVal("tyrCV"))
                        Else
                            objActionItem.CorrType = CCommon.ToShort(ddlAssignTO1.SelectedValue)
                            objActionItem.OpenRecordID = CCommon.ToLong(ddlOpenRecord.SelectedValue)
                        End If
                        objActionItem.DomainID = Session("DomainID")
                        objActionItem.ManageCorrespondence()
                    End If

                    Dim intUnsussesfull As Integer = 0
                    If Not Session("Attachements") Is Nothing Then
                        Dim objDocuments As New DocumentList
                        Dim dtTable As DataTable
                        Dim k As Integer
                        dtTable = Session("Attachements")
                        Dim str As String()
                        For k = 0 To dtTable.Rows.Count - 1
                            str = Split(dtTable.Rows(k).Item("FileLocation"), "/")

                            objDocuments.EmailHstrID = lngEmailHstrID
                            objDocuments.FileName = dtTable.Rows(k)("Filename")
                            objDocuments.FileLocation = str(str.Length - 1)
                            objDocuments.FileType = dtTable.Rows(k)("FileType")
                            objDocuments.FileSize = dtTable.Rows(k)("FileSize")
                            objDocuments.InsertAttachemnt()
                        Next
                    End If

                    Dim strBody As String

                    If chkTrack.Checked = True Then
                        strBody = RadEditor1.Content & "<img width='0px' height='0px' runat='server' src='" & ConfigurationManager.AppSettings("EmailTracking") & "?EmailHstrID=" & lngEmailHstrID & "' />"
                    Else
                        strBody = RadEditor1.Content
                    End If

                    If strBody.ToLower().Contains("##ContactOpt-OutLink##".ToLower()) Then
                        objEmail.AdditionalMergeFields.Add("ContactOpt-OutLink", "")
                    End If

                    objEmail.AdditionalMergeFields.Add("Signature", Session("Signature"))
                    For Each iKey As Object In objEmail.AdditionalMergeFields.Keys
                        If dtEmailBroadCast.Columns.Contains(iKey.ToString) = False Then
                            dtEmailBroadCast.Columns.Add(iKey.ToString)
                        End If
                        'Assign values to rows in merge table
                        For Each row As DataRow In dtEmailBroadCast.Rows
                            If iKey.ToString() <> "ContactEmail" Then
                                row(iKey.ToString) = CCommon.ToString(objEmail.AdditionalMergeFields(iKey))
                            End If
                        Next
                    Next
                    'commented to Debug function
                    'objEmail.SendEmail(txtSubject.Text, strBody, hdnCC.Value, Session("UserEmail"), hdnTo.Value, dtEmailBroadCast, Session("ContactName"), hdnBCC.Value)

                    'Added by Priya 28 Feb 2018(Fetching Custom fields values and replacing with custom merge tags in Email) 
                    Dim numOppId As Long
                    If (GetQueryStringVal("pluYR") <> "") Then
                        numOppId = CCommon.ToLong(GetQueryStringVal("pluYR"))
                    ElseIf (GetQueryStringVal("OppID") <> "") Then
                        numOppId = CCommon.ToLong(GetQueryStringVal("OppID"))
                    End If
                    Dim dtCustomMergeFields As DataTable
                    dtCustomMergeFields = objEmail.GetCustomMergeFieldsData(CCommon.ToLong(hdnModuleId.Value), Session("DomainID"), strContactIDs, numOppId)
                    If (dtCustomMergeFields IsNot Nothing) Then
                        For Each drow As DataRow In dtCustomMergeFields.Rows
                            If strBody.Contains(drow("CustomField_Name").ToString()) Then
                                strBody = strBody.Replace(drow("CustomField_Name").ToString(), drow("CustomField_Value").ToString())
                            End If
                        Next
                    End If
                    'Added by Priya 24 Feb 2018(BizDoc HTML & CSS Template) 
                    If (strBody.Contains("##BizDocTemplateFromGlobalSettings##")) Then
                        strBody = strBody.Replace("##BizDocTemplateFromGlobalSettings##", strBizDocUI)
                    End If


                    objEmail.AddEmailToJob(txtSubject.Text, strBody, hdnCC.Value, Session("UserEmail"), hdnTo.Value, dtEmailBroadCast, Session("ContactName"), hdnBCC.Value, strPageUrl:="/BACRMUI/contact/frmComposeWindow.aspx")
                    Response.Write("<script>self.close();</script>")

                    Session("Attachements") = Nothing
                    lblAttachents.Text = ""
                    hdnSelectedEmails.Value = ""
                    lblTo.Text = ""
                    hdnTo.Value = ""
                    hdnSelectedEmailsCC.Value = ""
                    lblCC.Text = ""
                    hdnCC.Value = ""
                    hdnSelectedEmailsBCC.Value = ""
                    lblBCC.Text = ""
                    hdnBCC.Value = ""

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Protected Sub hplAttachmentsLink_Click(sender As Object, e As EventArgs)
            updateAddAttachment.Visible = True
            bindGrid()
            If CCommon.ToLong(hdnAttachDivisionID.Value) > 0 Or CCommon.ToLong(hdnAttachContactID.Value) > 0 Or CCommon.ToLong(hdnAttachOppID.Value) > 0 Then
                GetDocumentFilesLink()
                GetParentRecord()
                GetBizDocs()
            End If
        End Sub

        Private Sub dgAttachments_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgAttachments.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    Dim dtTable As DataTable
                    Dim intcnt As Integer
                    Dim myRow As DataRow
                    dtTable = Session("Attachements")
                    For Each myRow In dtTable.Rows
                        If intcnt = e.Item.ItemIndex Then
                            dtTable.Rows.Remove(myRow)
                            Exit For
                        End If
                        intcnt = intcnt + 1
                    Next
                    Session("Attachements") = dtTable
                    bindGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgAttachments_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgAttachments.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.Item Then
                    Dim hpl As HyperLink
                    hpl = e.Item.FindControl("hplLink")
                    hpl.NavigateUrl = Replace(hpl.NavigateUrl, "..", "../../" & ConfigurationManager.AppSettings("PortalVirtualDirectoryName"))
                    'Dim hpl1 As HyperLink
                    'hpl1 = e.Item.FindControl("hplBizdcoc")
                    'Dim txtOppBizId As TextBox
                    'txtOppBizId = e.Item.FindControl("txtBizDocID")
                    'hpl1.Attributes.Add("onclick", "return OpenBizInvoice('" & GetQueryStringVal("OpID") & "','" & txtOppBizId.Text & "')")

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Protected Sub btnAttachFile_Click(sender As Object, e As EventArgs)
            If fileupload.PostedFile.ContentLength > 0 Then
                Dim strFName As String()
                Dim strFilePath, strFileName, strFileType As String
                Try
                    If (fileupload.PostedFile.ContentLength > 0) Then
                        strFileName = Path.GetFileName(fileupload.PostedFile.FileName)      'Getting the File Name
                        If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                            Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
                        End If
                        strFilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName

                        strFName = Split(strFileName, ".")
                        strFileType = fileupload.PostedFile.ContentType  'strFName(strFName.Length - 1)                     'Getting the Extension of the File
                        fileupload.PostedFile.SaveAs(strFilePath)
                        'strFileName = CCommon.GetDocumentPath(Session("DomainID")) & strFileName

                        UploadFile(strFileName, strFileType)
                    End If
                Catch ex As Exception
                    Throw ex
                End Try
            End If
            bindGrid()
        End Sub

        Protected Sub btnAttachDocuments_Click(sender As Object, e As EventArgs)
            If radDocuments.CheckedItems().Count > 0 Then
                'Dim lngOppID As Long = CCommon.ToLong(Session("OppID"))
                Dim lngOppID As Long
                If CCommon.ToLong(hdnAttachOppID.Value) > 0 Then
                    lngOppID = CCommon.ToLong(hdnAttachOppID.Value)
                ElseIf Session("OppID") IsNot Nothing AndAlso Session("OppID") <> "0" Then
                    lngOppID = CCommon.ToLong(Session("OppID"))
                Else
                    lngOppID = 0
                End If
                For Each item As RadComboBoxItem In radDocuments.CheckedItems
                    If item.Checked = True Then
                        Dim objDocuments As New BACRM.BusinessLogic.Documents.DocumentList
                        Dim dt As DataTable
                        objDocuments.GenDocID = item.Value 'radDocuments.SelectedItem.Value
                        objDocuments.UserCntID = Session("UserContactID")
                        objDocuments.DomainID = Session("DomainID")
                        dt = objDocuments.GetDocByGenDocID

                        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                            UploadFile(Replace(dt.Rows(0).Item("VcFileName"), "../documents/docs/", ""), "application/octet-stream")
                        End If
                    End If
                Next
            End If
            bindGrid()
        End Sub

        Protected Sub btnParentRecord_Click(sender As Object, e As EventArgs)
            If radParentRecord.CheckedItems().Count > 0 Then
                Dim lngOppID As Long
                If CCommon.ToLong(hdnAttachOppID.Value) Then
                    lngOppID = CCommon.ToLong(hdnAttachOppID.Value)
                ElseIf Session("OppID") IsNot Nothing AndAlso Session("OppID") <> "0" Then
                    lngOppID = CCommon.ToLong(Session("OppID"))
                Else
                    lngOppID = 0
                End If

                Dim sw As New System.IO.StringWriter()
                Dim RefType As Short = 0
                If Session("OppType") IsNot Nothing Then
                    OppType.Value = Session("OppType")
                End If
                If Session("OppStatus") IsNot Nothing Then
                    OppStatus.Value = Session("OppStatus")
                End If

                If OppType.Value = 1 And OppStatus.Value = 0 Then
                    RefType = 3

                ElseIf OppType.Value = 1 And OppStatus.Value = 1 Then
                    RefType = 1

                ElseIf OppType.Value = 2 And OppStatus.Value = 0 Then
                    RefType = 4

                ElseIf OppType.Value = 2 And OppStatus.Value = 1 Then
                    RefType = 2
                End If
                Server.Execute("../opportunity/frmMirrorBizDoc.aspx" & QueryEncryption.EncryptQueryString("RefID=" & lngOppID & "&RefType=" & RefType & "&DocGenerate=1"), sw)
                Dim htmlCodeToConvert As String = sw.GetStringBuilder().ToString()
                sw.Close()

                For Each item As RadComboBoxItem In radParentRecord.CheckedItems
                    If item.Checked = True Then
                        Dim i As Integer
                        Dim strFileName As String = ""
                        Dim strFilePhysicalLocation As String = ""

                        Dim objHTMLToPDF As New HTMLToPDF
                        strFileName = objHTMLToPDF.ConvertHTML2PDF(htmlCodeToConvert, CCommon.ToLong(Session("DomainID")))

                        strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
                        objCommon.AddAttchmentToSession(item.Text & ".pdf", CCommon.GetDocumentPath(Session("DomainID")) & strFileName, strFilePhysicalLocation)
                    End If
                Next
            End If
            bindGrid()
        End Sub

        Protected Sub btnBizDocAttach_Click(sender As Object, e As EventArgs)
            If radBizDocsDocuments.CheckedItems().Count > 0 Then
                'Dim lngOppID As Long = CCommon.ToLong(Session("OppID"))
                Dim lngOppID As Long
                If CCommon.ToLong(hdnAttachOppID.Value) > 0 Then
                    lngOppID = CCommon.ToLong(hdnAttachOppID.Value)
                ElseIf Session("OppID") IsNot Nothing AndAlso Session("OppID") <> "0" Then
                    lngOppID = CCommon.ToLong(Session("OppID"))
                Else
                    lngOppID = 0
                End If
                For Each item As RadComboBoxItem In radBizDocsDocuments.CheckedItems
                    If item.Checked = True Then
                        Dim objBizDocs As New OppBizDocs
                        Dim dt As DataTable
                        objBizDocs.BizDocId = item.Value
                        objBizDocs.DomainID = Session("DomainID")
                        objBizDocs.BizDocTemplateID = 0
                        objBizDocs.intAddReferenceDocument = 1
                        dt = objBizDocs.GetBizDocAttachments
                        Dim i As Integer
                        Dim strFileName As String = ""
                        Dim strFilePhysicalLocation As String = ""
                        If dt IsNot Nothing AndAlso dt.Rows.Count > 0 Then
                            For i = 0 To dt.Rows.Count - 1
                                If dt.Rows(i).Item("vcDocName") = "BizDoc" Then
                                    strFileName = "File" & Format(Now, "ddmmyyyyhhmmssfff") & ".pdf"
                                    strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName

                                    Dim objHTMLToPDF As New HTMLToPDF
                                    System.IO.File.WriteAllBytes(strFilePhysicalLocation, objHTMLToPDF.CreateBizDocPDF(lngOppID, item.Value, CCommon.ToLong(Session("DomainID")), CCommon.ToLong(Session("UserContactID")), CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")), CCommon.ToString(Session("DateFormat"))))

                                    strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
                                    objCommon.AddAttchmentToSession(item.Text & ".pdf", CCommon.GetDocumentPath(Session("DomainID")) & strFileName, strFilePhysicalLocation)
                                Else
                                    strFileName = dt.Rows(i).Item("vcURL")
                                    strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
                                    objCommon.AddAttchmentToSession(strFileName, CCommon.GetDocumentPath(Session("DomainID")) & strFileName, strFilePhysicalLocation)
                                End If

                            Next
                        End If
                    End If
                Next
            End If
            bindGrid()
        End Sub

        Sub UploadFile(ByVal strDocName As String, ByVal strFileType As String)
            Try
                If File.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName) Then

                    Dim f As FileInfo = New FileInfo(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName)
                    Dim strFileSize As String = f.Length.ToString

                    objCommon.AddAttchmentToSession(strDocName, CCommon.GetDocumentPath(Session("DomainID")) & strDocName, CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName, strFileType, strFileSize)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub bindGrid()
            Try
                Dim dtTable As DataTable
                Dim i As Integer
                If Not Session("Attachements") Is Nothing Then
                    dtTable = Session("Attachements")
                    dgAttachments.DataSource = dtTable
                    dgAttachments.DataBind()
                    txtAttachements.Text = ""
                    For i = 0 To dtTable.Rows.Count - 1
                        txtAttachements.Text = txtAttachements.Text + dtTable.Rows(i).Item("Filename") + ","
                    Next
                    txtAttachements.Text = txtAttachements.Text.TrimEnd(",")
                    lblAttachents.Text = txtAttachements.Text.TrimEnd(",")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub btnSaveAttachment_Click(sender As Object, e As EventArgs)
            updateAddAttachment.Visible = False
            'btnAttachFile_Click(Nothing, Nothing)
            'btnAttachDocuments_Click(Nothing, Nothing)
            'btnParentRecord_Click(Nothing, Nothing)
            'btnBizDocAttach_Click(Nothing, Nothing)
        End Sub
        Protected Sub btnSaveCloseAttachment_Click(sender As Object, e As EventArgs)
            btnAttachFile_Click(Nothing, Nothing)
            btnAttachDocuments_Click(Nothing, Nothing)
            btnParentRecord_Click(Nothing, Nothing)
            btnBizDocAttach_Click(Nothing, Nothing)
            updateAddAttachment.Visible = False
            bindGrid()
        End Sub
    End Class



End Namespace
