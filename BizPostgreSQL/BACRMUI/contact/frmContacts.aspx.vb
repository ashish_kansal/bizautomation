Imports System.IO
Imports System.Text
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Survey

Namespace BACRM.UserInterface.Contacts

    Public Class frmContacts
        Inherits BACRMPage
        Dim objContacts As New ContactIP
        Dim objprospects As CProspects

        Dim ObjCus As CustomFields
        Dim strColumn As String
        Dim lngCntID As Long
        Dim m_aryRightsForCusFlds(), m_aryRightsForActItem(), m_aryRightsForEmail(), m_aryRightsForOpp(), m_aryRightsForAOI() As Integer
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim boolIntermediatoryPage As Boolean = False
        Dim dtCustomFieldTable As DataTable
        Dim dtTableInfo As DataTable
        Dim objPageControls As New PageControls
        Dim lngDivId As Long

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object
        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try


                If Not IsPostBack Then
                    DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                    DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                    If Session("EnableIntMedPage") = 1 Then
                        ViewState("IntermediatoryPage") = True
                    Else
                        ViewState("IntermediatoryPage") = False
                    End If

                    hdnUploadPath.Value = CCommon.GetDocumentPath(Session("DomainID"))

                    Dim m_aryRightsForManageInventory As Integer() = GetUserRightsForPage_Other(37, 132)
                    If m_aryRightsForManageInventory(RIGHTSTYPE.VIEW) = 0 Then
                        btnLayout.Visible = False
                    End If
                End If
                boolIntermediatoryPage = ViewState("IntermediatoryPage")
                ControlSettings()

                If GetQueryStringVal("SI") <> "" Then SI = GetQueryStringVal("SI")
                If GetQueryStringVal("SI1") <> "" Then
                    SI1 = GetQueryStringVal("SI1")
                Else : SI1 = 0
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    SI2 = GetQueryStringVal("SI2")
                Else : SI2 = 0
                End If
                If GetQueryStringVal("frm") <> "" Then
                    frm = GetQueryStringVal("frm")
                Else : frm = ""
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = GetQueryStringVal("frm1")
                Else : frm1 = ""
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    frm2 = GetQueryStringVal("frm2")
                Else : frm2 = ""
                End If
                If GetQueryStringVal("frm") = "SurveyRespondents" Or GetQueryStringVal("frm") = "DuplicateSearch" Then    'This special section is introduced because the screen has to open in a new window without any menus
                    'Added by Debasish Nag on 6th Jan 2006
                    Dim objForm As Object                                   'The generic object
                    objForm = Page.FindControl("webmenu1")                  'Get a holder to the web control
                    objForm.visible = False                                 'Hide the web control
                    btnCancel.Attributes.Add("onclick", "javascript: window.close();return false;") 'Write code to close the window
                End If
                lngCntID = CCommon.ToInteger(GetQueryStringVal("CntId"))
                Correspondence1.lngRecordID = lngCntID
                Correspondence1.Mode = 8

                GetUserRightsForPage(11, 3)

                If Not IsPostBack Then

                    Session("Asc") = 1

                    AddToRecentlyViewed(RecetlyViewdRecordType.Contact, lngCntID)
                    Session("Help") = "Contacts"
                    ''''sub-tab management  - added on 16/09/2009 by chintan
                    objCommon.GetAuthorizedSubTabs(radOppTab, Session("DomainID"), 4, Session("UserGroupID"))
                    '''''

                    If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                        btnSave.Visible = False
                        btnSaveClose.Visible = False
                    End If
                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnActDelete.Visible = False


                    m_aryRightsForActItem = GetUserRightsForPage_Other(11, 5)
                    If m_aryRightsForActItem(RIGHTSTYPE.VIEW) = 0 Then btnActionItem.Visible = False

                    PersistTable.Load(boolOnlyURL:=True)
                    If PersistTable.Count > 0 Then
                        radOppTab.SelectedIndex = CCommon.ToLong(PersistTable(PersistKey.SelectedTab))
                    End If

                    LoadInformation()
                    sb_ShowAOIs()
                    LoadTabDetails()

                    If lngCntID = Session("AdminID") Then
                        btnActDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    Else : btnActDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If
                    btnLayout.Attributes.Add("onclick", "return ShowLayout('c','" & lngCntID & "','" & objContacts.ContactType & "','10');")
                    btnSave.Attributes.Add("onclick", "return Save()")
                    btnSaveClose.Attributes.Add("onclick", "return Save()")
                    btnMerge.Attributes.Add("onclick", "return OpenMergeCopyWindow('" & lngCntID & "','" & GetQueryStringVal("frm") & "');")
                    hplTransfer.Attributes.Add("onclick", "return OpenTransfer('" & "../admin/transferrecord.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contacts&uihTR=" & lngCntID & "')")

                    If radOppTab.Tabs.Count > SI AndAlso SI > 0 Then radOppTab.SelectedIndex = SI

                    If Not radOppTab.SelectedTab.Visible Then
                        radOppTab.SelectedIndex = 0
                    End If

                    radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                End If
                If IsPostBack Then
                    LoadInformation()
                End If

                OpportunitiesTab1.ContactID = lngCntID
                DisplayDynamicFlds()

                'Added by Neelam on 02/16/2018 - Fetch and displays document and link name on the page*/
                GetDocumentFilesLink()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        'Added by Neelam on 02/16/2018 - Fetch and displays document and link name on the page*/
        Private Sub GetDocumentFilesLink()
            Try
                tdDocument.Controls.Clear()
                Dim objOpp As New BusinessLogic.Opportunities.OppotunitiesIP
                Dim dtDocLinkName As New DataTable
                With objOpp
                    .DomainID = Session("DomainID")
                    .DivisionID = lngDivId
                    dtDocLinkName = .GetDocumentFilesLink("P")
                End With

                If dtDocLinkName IsNot Nothing AndAlso dtDocLinkName.Rows.Count > 0 Then

                    Dim docRows = (From dRow In dtDocLinkName.Rows
                                   Where CCommon.ToString(dRow("cUrlType")) = "L"
                                   Select New With {Key .fileName = dRow("VcFileName"), .docName = dRow("VcDocName"), .extention = dRow("vcFileType"), .fileId = dRow("numGenericDocID")}).Distinct()
                    If docRows.Count() > 0 Then
                        Dim _docName = String.Empty
                        Dim countId = 0
                        For Each dr In docRows
                            Dim strOriginalFileName = CCommon.ToString(dr.docName) & CCommon.ToString(dr.extention)
                            Dim strDocName = CCommon.ToString(dr.fileName)
                            Dim fileID = CCommon.ToString(dr.fileId)
                            Dim strFileLogicalPath = String.Empty
                            If File.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName) Then
                                Dim f As FileInfo = New FileInfo(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName)
                                Dim strFileSize As String = f.Length.ToString
                                strFileLogicalPath = CCommon.GetDocumentPath(Session("DomainID")) & HttpUtility.UrlEncode(strDocName).Replace("+", " ")
                            End If

                            Dim docLink As New HyperLink

                            Dim imgDelete As New ImageButton
                            imgDelete.ImageUrl = "../images/Delete24.png"
                            imgDelete.Height = 13
                            imgDelete.ToolTip = "Delete"
                            imgDelete.ID = "imgDelete" + CCommon.ToString(countId)
                            imgDelete.Attributes.Add("onclick", "DeleteDocumentOrLink(" & fileID & ");return false;")

                            Dim Space As New LiteralControl
                            Space.Text = "&nbsp;"

                            docLink.Text = strOriginalFileName
                            docLink.NavigateUrl = strFileLogicalPath
                            docLink.ID = "hpl" + CCommon.ToString(countId)
                            docLink.Target = "_blank"
                            Dim createDocDiv As HtmlGenericControl = New HtmlGenericControl("DIV")
                            createDocDiv.Controls.Add(docLink)
                            createDocDiv.Controls.Add(Space)
                            createDocDiv.Controls.Add(imgDelete)
                            tdDocument.Controls.Add(createDocDiv)
                            countId = countId + 1
                        Next
                        imgDocument.Visible = True
                    Else
                        imgDocument.Visible = False
                    End If

                    Dim linkRows = (From dRow In dtDocLinkName.Rows
                                    Where CCommon.ToString(dRow("cUrlType")) = "U"
                                    Select New With {Key .linkURL = dRow("VcFileName"), Key .linkName = dRow("VcDocName"), .fileId = dRow("numGenericDocID")}).Distinct()
                    If linkRows.Count() > 0 Then
                        Dim _linkName = String.Empty
                        Dim _linkURL = String.Empty
                        Dim fileLinkID = String.Empty
                        Dim countLinkId = 0

                        For Each dr In linkRows
                            '_linkName = _linkName + ", " + CCommon.ToString(dr.linkName)
                            '_linkURL = _linkURL + ",<br/> " + CCommon.ToString(dr.linkURL)

                            _linkName = CCommon.ToString(dr.linkName)
                            _linkURL = CCommon.ToString(dr.linkURL)
                            fileLinkID = CCommon.ToString(dr.fileId)
                            Dim uriBuilder As UriBuilder = New UriBuilder(_linkURL)
                            'UriBuilder builder = New UriBuilder(myUrl)
                            'charityNameText.NavigateUrl = builder.Uri.AbsoluteUri

                            Dim link As New HyperLink
                            Dim imgLinkDelete As New ImageButton
                            imgLinkDelete.ImageUrl = "../images/Delete24.png"
                            imgLinkDelete.Height = 13
                            imgLinkDelete.ToolTip = "Delete"
                            imgLinkDelete.ID = "imgLinkDelete" + CCommon.ToString(countLinkId)
                            imgLinkDelete.Attributes.Add("onclick", "DeleteDocumentOrLink(" & fileLinkID & ");return false;")

                            link.Text = _linkName
                            link.NavigateUrl = uriBuilder.Uri.AbsoluteUri
                            'link.NavigateUrl = _linkURL
                            link.ID = "hplLink" + CCommon.ToString(countLinkId)
                            link.Target = "_blank"
                            Dim createLinkDiv As HtmlGenericControl = New HtmlGenericControl("DIV")
                            createLinkDiv.Controls.Add(link)
                            createLinkDiv.Controls.Add(imgLinkDelete)
                            tdLink.Controls.Add(createLinkDiv)
                            countLinkId = countLinkId + 1
                        Next
                        imgLink.Visible = True
                    Else
                        imgLink.Visible = False
                    End If
                Else
                    imgDocument.Visible = False
                    imgLink.Visible = False
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub btnDeleteDocOrLink_Click(sender As Object, e As EventArgs)
            Dim objDocuments As New DocumentList()
            objDocuments.GenDocID = CCommon.ToLong(hdnFileId.Value)
            objDocuments.DeleteDocumentOrLink()
            GetDocumentFilesLink()
        End Sub

        Sub LoadTabDetails()
            Try
                If radOppTab.SelectedIndex = 2 Then
                    'sc_GetOppDetails()
                ElseIf radOppTab.SelectedIndex = 3 Then
                    DisplaySurveyHistoryLists()
                ElseIf radOppTab.SelectedIndex = 4 Then
                    Correspondence1.lngRecordID = lngCntID
                    Correspondence1.Mode = 8
                    Correspondence1.CorrespondenceModule = 11
                    Correspondence1.getCorrespondance()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadInformation()
            Try
                hdnContactId.Value = lngCntID

                Dim dtContactInfo As DataTable
                objContacts.ContactID = lngCntID
                objContacts.DomainID = Session("DomainID")
                objContacts.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtContactInfo = objContacts.GetCntInfoForEdit1

                objContacts.GetContactInfoEditIP()
                ''Time And Expense
                If dtContactInfo.Rows.Count > 0 Then
                    lngDivId = CCommon.ToLong(dtContactInfo.Rows(0).Item("numDivisionID"))
                    Session("DivisionID") = lngDivId

                    If CCommon.ToLong(dtContactInfo.Rows(0).Item("numAssignedTo")) <> CCommon.ToLong(Session("UserContactID")) Then
                        GetUserRightsForRecord(11, 3, CCommon.ToLong(dtContactInfo.Rows(0).Item("numRecOwner")), CCommon.ToLong(dtContactInfo.Rows(0).Item("numTerID")))
                    End If

                    If dtContactInfo.Rows(0).Item("numUserID") > 0 Then
                        Dim m_aryRightsForTimeExp() As Integer
                        m_aryRightsForTimeExp = GetUserRightsForPage_Other(11, 11)
                        If m_aryRightsForTimeExp(RIGHTSTYPE.VIEW) <> 0 Then
                            btnTimeExp.Visible = True
                            btnTimeExp.Attributes.Add("onclick", "return OpenTmeAndExp(" & lngCntID & ",'" & GetQueryStringVal("frm") & "')")
                        End If
                    End If
                    hplCustomer.Text = "<b>Customer :&nbsp;&nbsp;</b>" & dtContactInfo.Rows(0).Item("vcCompanyName")
                    If dtContactInfo.Rows(0).Item("tintCRMType") = 0 Then
                        hplCustomer.NavigateUrl = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactdetail&DivID=" & dtContactInfo.Rows(0).Item("numDivisionID") & "&CntID=" & lngCntID & "&frm1=" & GetQueryStringVal("frm")

                    ElseIf dtContactInfo.Rows(0).Item("tintCRMType") = 1 Then
                        hplCustomer.NavigateUrl = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactdetail&DivID=" & dtContactInfo.Rows(0).Item("numDivisionID") & "&CntID=" & lngCntID & "&frm1=" & GetQueryStringVal("frm")

                    ElseIf dtContactInfo.Rows(0).Item("tintCRMType") = 2 Then
                        hplCustomer.NavigateUrl = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactdetail&klds+7kldf=fjk-las&DivId=" & dtContactInfo.Rows(0).Item("numDivisionID") & "&CntID=" & lngCntID & "&frm1=" & GetQueryStringVal("frm")

                    End If
                    lblRecordOwner.Text = dtContactInfo.Rows(0).Item("RecordOwner")
                    If dtContactInfo.Rows(0).Item("CreatedBy") IsNot Nothing AndAlso dtContactInfo.Rows(0).Item("CreatedBy") IsNot DBNull.Value Then
                        lblCreatedBy.Text = dtContactInfo.Rows(0).Item("CreatedBy")
                    End If
                    If dtContactInfo.Rows(0).Item("ModifiedBy") IsNot Nothing AndAlso dtContactInfo.Rows(0).Item("ModifiedBy") IsNot DBNull.Value Then
                        lblModifiedBy.Text = dtContactInfo.Rows(0).Item("ModifiedBy")
                    End If

                    hidCompName.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcCompanyName")), "", dtContactInfo.Rows(0).Item("vcCompanyName"))
                    hidEml.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcGivenName")), "", dtContactInfo.Rows(0).Item("vcGivenName"))
                    txtContactType.Text = dtContactInfo.Rows(0).Item("numContactType")

                    'Commented by Neelam - Obsolete functionality
                    'hplDocumentCount.Attributes.Add("onclick", "return OpenDocuments(" & lngCntID & ")")
                    'hplDocumentCount.Text = "(" & CCommon.ToLong(dtContactInfo.Rows(0)("DocumentCount")) & ")"
                    'imgOpenDocument.Attributes.Add("onclick", "return OpenDocuments(" & lngCntID & ")")
                    imgOpenDocument.Attributes.Add("onclick", "OpenDocumentFiles(" & lngCntID.ToString() & ");return false;")
                    hplOpenDocument.Attributes.Add("onclick", "OpenDocumentFiles(" & lngCntID.ToString() & ");return false;")

                    LoadControls()



                    txtItemId.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcItemId")), "", dtContactInfo.Rows(0).Item("vcItemId"))
                    txtChangeKey.Text = IIf(IsDBNull(dtContactInfo.Rows(0).Item("vcChangeKey")), "", dtContactInfo.Rows(0).Item("vcChangeKey"))

                    If dtContactInfo.Rows(0).Item("vcCompanyTypeName") = "Employer" Then
                        radOppTab.FindTabByValue("Correspondence").Visible = False
                    End If

                    hdnImageFileName.Value = CCommon.ToString(dtContactInfo.Rows(0).Item("vcImageName"))

                    If Not String.IsNullOrEmpty(hdnImageFileName.Value) Then
                        imgContactThumb.ImageUrl = CCommon.GetDocumentPath(Session("DomainID")) + hdnImageFileName.Value
                        lnkDelete.Style.Add("visibility", "")
                    Else
                        imgContactThumb.ImageUrl = "../images/ContactPic.png"
                        lnkDelete.Style.Add("visibility", "hidden")
                    End If
                End If
                m_aryRightsForAOI = GetUserRightsForPage_Other(11, 4)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadControls()
            Try
                tblMain.Controls.Clear()
                Dim ds As DataSet
                Dim objPageLayout As New CPageLayout
                Dim fields() As String

                objPageLayout.CoType = "C"
                objPageLayout.UserCntID = Session("UserContactId")
                objPageLayout.RecordId = lngCntID
                'objPageLayout.ContactID = lngCntID
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.PageId = 4
                objPageLayout.numRelCntType = objContacts.ContactType

                objPageLayout.FormId = 10
                objPageLayout.PageType = 3

                ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 

                dtTableInfo = ds.Tables(0)

                Dim intCol1Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=1")
                Dim intCol2Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=2")
                Dim intCol1 As Int32 = 0
                Dim intCol2 As Int32 = 0
                Dim tblCell As TableCell

                Dim tblRow As TableRow
                Const NoOfColumns As Short = 2
                Dim ColCount As Int32 = 0
                ''If intermediatory Page is enabled then Add the text "Name" to dropdown fields
                objPageControls.CreateTemplateRow(tblMain)

                objPageControls.ContactID = objContacts.ContactID
                objPageControls.DivisionID = objContacts.DivisionID
                objPageControls.TerritoryID = objContacts.TerritoryID
                objPageControls.strPageType = "Contact"
                objPageControls.EditPermission = m_aryRightsForPage(RIGHTSTYPE.UPDATE)

                Dim dv As DataView = dtTableInfo.DefaultView
                Dim dvExcutedView As DataView = dtTableInfo.DefaultView
                Dim dvExcutedGroupsView As DataView = dtTableInfo.DefaultView
                Dim DataRows = (From row In dtTableInfo.AsEnumerable() Select row)
                Dim distinctNames = DataRows.OrderBy(Function(x) x("numOrder")).Select(Function(x) x("vcGroupName")).Distinct().ToList()
                For Each drGroupName As String In distinctNames
                    If drGroupName Is Nothing Then
                        drGroupName = ""
                    End If
                    tblCell = New TableCell()
                    tblRow = New TableRow
                    tblCell.ColumnSpan = 4
                    tblCell.CssClass = "tableGroupHeader"
                    tblCell.Text = drGroupName
                    tblRow.Cells.Add(tblCell)
                    tblMain.Rows.Add(tblRow)
                    dvExcutedView.RowFilter = "vcGroupName = '" & drGroupName & "'"
                    tblRow = New TableRow
                    tblCell = New TableCell
                    intCol1Count = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=1 AND vcGroupName='" & drGroupName & "'")
                    intCol2Count = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=2 AND vcGroupName='" & drGroupName & "'")
                    intCol1 = 0
                    intCol2 = 0
                    For Each drData As DataRowView In dvExcutedView
                        Dim dr As DataRow
                        dr = drData.Row
                        If boolIntermediatoryPage = True Then
                            If Not IsDBNull(dr("vcPropertyName")) And (dr("fld_type") = "SelectBox") And dr("bitCustomField") = False Then
                                dr("vcPropertyName") = dr("vcPropertyName") & "Name"
                            End If
                        End If

                        If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False Then
                            If CCommon.ToString(dr("vcPropertyName")) = "vcCompactContactDetails" Then
                                Dim strData As New StringBuilder()
                                strData.Append("<a class='text-yellow' href='javascript:OpenContact(" & objContacts.ContactID & ",1)'>" & objContacts.GivenName & "</a> &nbsp;")
                                strData.Append(objContacts.ContactPhone & "&nbsp;")
                                If objContacts.ContactPhoneExt <> "" Then
                                    strData.Append("&nbsp;(" & objContacts.ContactPhoneExt & ")")
                                End If
                                If objContacts.Email <> "" Then
                                    strData.Append("<img src='../images/msg_unread_small.gif' email=" & objContacts.Email & " id='imgEmailPopupId' onclick='return OpemParticularEmail(" & objContacts.ContactID & ")' />")
                                End If
                                dr("vcValue") = strData
                            Else
                                dr("vcValue") = objContacts.GetType.GetProperty(dr("vcPropertyName")).GetValue(objContacts, Nothing)
                            End If
                        End If

                        If ColCount = 0 Then
                            tblRow = New TableRow
                        End If

                        If intCol1Count = intCol1 And intCol2Count > intCol1Count Then
                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)
                            ColCount = 1
                        End If

                        If (dr("fld_type") = "ListBox" Or dr("fld_type") = "SelectBox") Then
                            Dim dtData As DataTable

                            If Not IsDBNull(dr("vcPropertyName")) Then
                                If dr("vcPropertyName") = "Sex" Then ' If dr("numFieldId") = 23 Then
                                    If boolIntermediatoryPage = False Then
                                        dtData = New DataTable
                                        dtData.Columns.Add("Value")
                                        dtData.Columns.Add("Text")
                                        Dim dr1 As DataRow
                                        dr1 = dtData.NewRow
                                        dr1("Value") = "M"
                                        dr1("Text") = "Male"
                                        dtData.Rows.Add(dr1)

                                        dr1 = dtData.NewRow
                                        dr1("Value") = "F"
                                        dr1("Text") = "Female"
                                        dtData.Rows.Add(dr1)
                                    End If
                                ElseIf dr("vcPropertyName") = "Manager" Then ' dr("numFieldId") = 6 Then
                                    If boolIntermediatoryPage = False Then
                                        objCommon.ContactID = lngCntID
                                        objCommon.charModule = "C"
                                        objCommon.GetCompanySpecificValues1()
                                        dtData = objCommon.GetManagers(Session("DomainID"), lngCntID, objCommon.DivisionID)
                                    End If
                                ElseIf dr("vcPropertyName") = "DripCampaign" Then ' dr("numFieldId") = 290 Then
                                    If boolIntermediatoryPage = False Then
                                        Dim objCampaign As New Campaign
                                        With objCampaign
                                            .SortCharacter = "0"
                                            .UserCntID = Session("UserContactID")
                                            .PageSize = 100
                                            .TotalRecords = 0
                                            .DomainID = Session("DomainID")
                                            .columnSortOrder = "Asc"
                                            .CurrentPage = 1
                                            .columnName = "vcECampName"
                                            dtData = objCampaign.ECampaignList
                                            Dim drow As DataRow = dtData.NewRow
                                            drow("numECampaignID") = -1
                                            drow("vcECampName") = "-- Disengaged --"
                                            dtData.Rows.Add(drow)
                                        End With
                                    End If
                                ElseIf Not IsDBNull(dr("numListID")) And dr("numListID") <> 0 Then
                                    If boolIntermediatoryPage = False Then
                                        If dr("ListRelID") > 0 Then
                                            objCommon.Mode = 3
                                            objCommon.DomainID = Session("DomainID")
                                            objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objContacts)
                                            objCommon.SecondaryListID = dr("numListId")
                                            dtData = objCommon.GetFieldRelationships.Tables(0)
                                        Else
                                            dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                        End If
                                    End If
                                End If
                            End If

                            If boolIntermediatoryPage Then
                                'If dr("vcPropertyName") = "ContactType" Then ' dr("numFieldID") = 5 Then
                                'Dim boolEnabled As Boolean = True
                                'If objContacts.ContactType = 70 Then boolEnabled = False
                                'objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData)
                                'Else
                                objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData)
                                'End If
                            Else
                                Dim ddl As DropDownList
                                'If dr("vcPropertyName") = "ContactType" Then ' dr("numFieldID") = 5 Then
                                'Dim boolEnabled As Boolean = True
                                'If objContacts.ContactType = 70 Then boolEnabled = False
                                'ddl = objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData)
                                'Else
                                ddl = objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData)
                                'End If
                                If CLng(dr("DependentFields")) > 0 And Not boolIntermediatoryPage Then
                                    ddl.AutoPostBack = True
                                    AddHandler ddl.SelectedIndexChanged, AddressOf PopulateDependentDropdown
                                End If
                            End If
                        ElseIf dr("fld_type") = "CheckBoxList" Then
                            If Not IsDBNull(dr("vcPropertyName")) Then
                                Dim dtData As DataTable

                                If boolIntermediatoryPage = False Then
                                    If dr("ListRelID") > 0 Then
                                        objCommon.Mode = 3
                                        objCommon.DomainID = Session("DomainID")
                                        objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objContacts)
                                        objCommon.SecondaryListID = dr("numListId")
                                        dtData = objCommon.GetFieldRelationships.Tables(0)
                                    Else
                                        dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                    End If
                                End If

                                objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData)
                            End If
                        ElseIf dr("fld_type") = "Popup" Then
                            If dr("vcDbColumnName") = "numConEmailCampID" Then ' dr("numFieldId") = 293 Then 'drip campaign
                                objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, RecordID:=objContacts.ContactECampaignID)
                            Else
                                objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, RecordID:=lngCntID)
                            End If
                        Else
                            If CCommon.ToString(dr("vcPropertyName")) = "DOB" Then 'Bug id 857
                                If CCommon.ToString(dr("vcValue")).Contains("1753") Then
                                    dr("vcValue") = ""
                                End If
                            End If
                            objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, RecordID:=lngCntID)
                        End If

                        If intCol2Count = intCol2 And intCol1Count > intCol2Count Then
                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            ColCount = 1
                        End If


                        If dr("intcoulmn") = 2 Then
                            If intCol1 <> intCol1Count Then
                                intCol1 = intCol1 + 1
                            End If

                            If intCol2 <> intCol2Count Then
                                intCol2 = intCol2 + 1
                            End If
                        End If

                        ColCount = ColCount + 1
                        If NoOfColumns = ColCount Then
                            ColCount = 0
                            tblMain.Rows.Add(tblRow)
                        End If
                    Next
                Next
                If ColCount > 0 Then
                    tblMain.Rows.Add(tblRow)
                End If

                'objPageControls.CreateComments(tblMain, objContacts.Comments, boolIntermediatoryPage)
                'Add Client Side validation for custom fields
                If Not boolIntermediatoryPage Then
                    Dim strValidation As String = objPageControls.GenerateValidationScript(dtTableInfo)
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "CFvalidation", strValidation, True)
                ElseIf Session("InlineEdit") = True And m_aryRightsForPage(RIGHTSTYPE.UPDATE) <> 0 Then
                    Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtTableInfo)
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType, "InlineEditValidation", strValidation, True)
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Modified by Neelam on 02/16/2018 - Added functionality to attach files*/
        Protected Sub btnAttach_Click(sender As Object, e As EventArgs)
            Try
                If (fileupload.PostedFile.ContentLength > 0) Then
                    Dim strFName As String()
                    Dim strFilePath, strFileName, strFileType As String
                    strFileName = Path.GetFileName(fileupload.PostedFile.FileName)
                    If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                        Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
                    End If
                    Dim newFileName As String = Path.GetFileNameWithoutExtension(strFileName) & DateTime.UtcNow.ToString("yyyyMMddhhmmssfff") & Path.GetExtension(strFileName)
                    strFilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & newFileName
                    strFName = Split(strFileName, ".")
                    strFileType = "." & strFName(strFName.Length - 1)                     'Getting the Extension of the File
                    fileupload.PostedFile.SaveAs(strFilePath)
                    UploadFile("L", strFileType, newFileName, strFName(0), "", 0, 0, "A")
                    Dim script As String = "function f(){$find(""" + RadWinDocumentFiles.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, True)
                    Response.Redirect(Request.RawUrl, False)
                Else
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "NoFiles", "alert('Please attach a file to upload.');return false;", True)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub btnLinkClose_Click(sender As Object, e As EventArgs)
            UploadFile("U", "", txtLinkURL.Text, txtLinkName.Text, "", 0, 0, "A")
            Response.Redirect(Request.RawUrl, False)
        End Sub

        'Modified by Neelam on 02/16/2018 - Added function to save File/Link data in the database*/
        Sub UploadFile(ByVal URLType As String, ByVal strFileType As String, ByVal strFileName As String, ByVal DocName As String, ByVal DocDesc As String,
                       ByVal DocumentStatus As Long, DocCategory As Long, ByVal strType As String)
            Try
                Dim arrOutPut As String()
                Dim objDocuments As New DocumentList()
                With objDocuments
                    .DomainID = Session("DomainId")
                    .UserCntID = Session("UserContactID")
                    .UrlType = URLType
                    .DocumentStatus = DocumentStatus
                    .DocCategory = DocCategory
                    .FileType = strFileType
                    .DocName = DocName
                    .DocDesc = DocDesc
                    .FileName = strFileName
                    .DocumentSection = strType
                    .RecID = lngDivId
                    .DocumentType = IIf(lngCntID > 0, 2, 1) '1=generic,2=specific
                    arrOutPut = .SaveDocuments()
                End With
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                saveContactInfo()
                SaveCusField()
                SaveAOI()
            Catch ex As Exception
                If ex.Message.Contains("DUPLICATE_EMAIL") Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "EmailValidation", "OpenEmailChangeWindow(" & objContacts.ContactID & ",'" & objContacts.Email & "');", True)
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(ex.Message)
                End If
            End Try
        End Sub

        Sub saveContactInfo()
            Try
                With objContacts
                    .ContactID = lngCntID
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                End With

                For Each dr As DataRow In dtTableInfo.Rows
                    If dr("vcDbColumnName") = "charSex" Then ' dr("numFieldID") = 23 Then
                        If Not radOppTab.MultiPage.FindControl(dr("numFieldId") & dr("vcFieldName") & "~0") Is Nothing Then
                            objContacts.Sex = CType(radOppTab.MultiPage.FindControl(dr("numFieldId") & dr("vcFieldName") & "~0"), DropDownList).SelectedValue
                        End If
                    ElseIf Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False And dr("bitCanBeUpdated") = True Then
                        objPageControls.SetValueForStaticFields(dr, objContacts, radOppTab.MultiPage)
                    End If
                Next

                If Not String.IsNullOrEmpty(hdnChangedEmail.Value) Then
                    objContacts.Email = hdnChangedEmail.Value
                End If

                'Right now we are allowing user to add duplicate contact email but in future we may want to restrict it
                'If objContacts.IsDuplicateEmail() Then
                '    Throw New Exception("DUPLICATE_EMAIL")
                'End If

                hdnChangedEmail.Value = ""
                txtContactType.Text = objContacts.ContactType
                objContacts.ManageContactInfo()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                saveContactInfo()
                SaveCusField()
                SaveAOI()
                If GetQueryStringVal("frm") = "SurveyRespondents" Or GetQueryStringVal("frm") = "DuplicateSearch" Then    'This special section is introduced because the screen has to open in a new window without any menus
                    litMessage.Text = "<script language='javascript'>window.close();</script>" 'Write code to close the window
                Else : PageRedirect()
                End If
            Catch ex As Exception
                If ex.Message.Contains("DUPLICATE_EMAIL") Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "EmailValidation", "OpenEmailChangeWindow(" & objContacts.ContactID & ",'" & objContacts.Email & "');", True)
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(ex.Message)
                End If
            End Try
        End Sub

        Sub DisplayDynamicFlds()
            Try
                objPageControls.DisplayDynamicFlds(lngCntID, objContacts.ContactType, Session("DomainID"), objPageControls.Location.Contact, radOppTab, 10)


            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveCusField()
            Try
                objPageControls.SaveCusField(lngCntID, objContacts.ContactType, Session("DomainID"), objPageControls.Location.Contact, radOppTab.MultiPage)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub sb_ShowAOIs()
            Try
                m_aryRightsForAOI = GetUserRightsForPage_Other(11, 4)
                If m_aryRightsForAOI(RIGHTSTYPE.VIEW) = 0 Then chkAOI.Visible = False
                Dim dtAOI As DataTable
                Dim i As Integer
                objContacts.ContactID = lngCntID
                objContacts.DomainID = Session("DomainID")
                dtAOI = objContacts.GetAOIDetails
                chkAOI.DataSource = dtAOI
                chkAOI.DataTextField = "vcAOIName"
                chkAOI.DataValueField = "numAOIId"
                chkAOI.DataBind()
                For i = 0 To dtAOI.Rows.Count - 1
                    If Not IsDBNull(dtAOI.Rows(i).Item("Status")) Then
                        If dtAOI.Rows(i).Item("Status") = 1 Then
                            chkAOI.Items.FindByValue(dtAOI.Rows(i).Item("numAOIId")).Selected = True
                        Else : chkAOI.Items.FindByValue(dtAOI.Rows(i).Item("numAOIId")).Selected = False
                        End If
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveAOI()
            Try
                Dim dtTable As New DataTable
                dtTable.Columns.Add("numAOIId")
                dtTable.Columns.Add("Status")
                Dim dr As DataRow
                Dim i As Integer
                For i = 0 To chkAOI.Items.Count - 1
                    If chkAOI.Items(i).Selected = True Then
                        dr = dtTable.NewRow
                        dr("numAOIId") = chkAOI.Items(i).Value
                        dr("Status") = 1
                        dtTable.Rows.Add(dr)
                    End If
                Next
                Dim ds As New DataSet
                Dim strdetails As String
                dtTable.TableName = "Table"
                ds.Tables.Add(dtTable)
                strdetails = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))

                objContacts.strAOI = strdetails
                objContacts.ContactID = lngCntID
                objContacts.SaveAOI()
                dtTable.Dispose()
                ds.Dispose()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                If GetQueryStringVal("frm") = "SurveyRespondents" Or GetQueryStringVal("frm") = "DuplicateSearch" Then    'This special section is introduced because the screen has to open in a new window without any menus
                    litMessage.Text = "<script language='javascript'>window.close();</script>" 'Write code to close the window
                Else : PageRedirect()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub PageRedirect()
            Try
                If GetQueryStringVal("frm1") = "ActionItem" Then
                    Response.Redirect("../admin/ActionItemDetailsOld.aspx?CommID=" & GetQueryStringVal("CommID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                End If
                If GetQueryStringVal("frm") = "ActItem" Then
                    Response.Redirect("../admin/ActionItemDetailsOld.aspx?CommId=" & GetQueryStringVal("CommID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "prospects" Then
                    objCommon.ContactID = lngCntID
                    objCommon.charModule = "C"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../prospects/frmProspects.aspx?frm=prospects&DivID=" & objCommon.DivisionID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "accounts" Then
                    objCommon.ContactID = lngCntID
                    objCommon.charModule = "C"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../account/frmAccounts.aspx?frm=accounts&klds+7kldf=fjk-las&DivId=" & objCommon.DivisionID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "tickler" Then
                    Response.Redirect("../common/frmticklerdisplay.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "contactlist" Then
                    Response.Redirect("../contact/frmContactList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2 & "&ContactType=" & GetQueryStringVal("ContactType"))
                ElseIf GetQueryStringVal("frm") = "prospectlist" Then
                    Response.Redirect("../prospects/frmProspectList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2 & "&profileId=" & GetQueryStringVal("profileId"))
                ElseIf GetQueryStringVal("frm") = "accountlist" Then
                    Response.Redirect("../account/frmAccountList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2 & "&profileId=" & GetQueryStringVal("profileId"))
                ElseIf GetQueryStringVal("frm") = "LeadsList" Then
                    Response.Redirect("../Leads/frmLeadList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2 & "&profileId=" & GetQueryStringVal("profileId"))
                ElseIf GetQueryStringVal("frm") = "Caselist" Then
                    Response.Redirect("../cases/frmCaseList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "ProjectList" Then
                    Response.Redirect("../projects/frmProjectList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "tickler" Then
                    Response.Redirect("../common/frmTicklerDisplay.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "search" Then
                    Response.Redirect("../common/searchdisplay.aspx?frm=advancedsearch&srchback=true&typ=" & GetQueryStringVal("typ"))
                ElseIf GetQueryStringVal("frm") = "opportunitylist" Then
                    Response.Redirect("../opportunity/frmOpportunityList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "ContactRole" Then
                    Response.Redirect("../reports/frmContactRole.aspx")
                ElseIf GetQueryStringVal("frm") = "DealHistory" Then
                    Response.Redirect("../reports/frmRepDealHistory.aspx")
                ElseIf GetQueryStringVal("frm") = "CompanyList" Then
                    Response.Redirect("../prospects/frmCompanyList.aspx?RelId=" & GetQueryStringVal("RelID") & "&profileId=" & GetQueryStringVal("profileId") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "ECampReport" Then
                    Response.Redirect("../Marketing/frmECampaignReport.aspx")
                ElseIf GetQueryStringVal("frm") = "LeadDetails" Then
                    objCommon.ContactID = lngCntID
                    objCommon.charModule = "C"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../Leads/frmLeads.aspx?DivID=" & objCommon.DivisionID & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "AdvSearchSur" Then
                    Response.Redirect("../Admin/FrmAdvSurveyRes.aspx")
                Else : Response.Redirect("../contact/frmContactList.aspx")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Private Sub dgClosedOpp_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgClosedOpp.ItemCommand
        '    Try
        '        Dim lngOppID As Long
        '        lngOppID = e.Item.Cells(0).Text
        '        If e.CommandName = "Name" Then Response.Redirect("../opportunity/frmOpportunities.aspx?frm=contactdetails&opID=" & lngOppID)
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub dgOpenOpportunty_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgOpenOpportunty.ItemCommand
        '    Try
        '        Dim lngOppID As Long
        '        lngOppID = e.Item.Cells(0).Text
        '        If e.CommandName = "Name" Then Response.Redirect("../opportunity/frmOpportunities.aspx?frm=contactdetails&opID=" & lngOppID)
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to display the surveys history from the database for the selected contact
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/02/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Sub DisplaySurveyHistoryLists()
            Try
                Dim objSurvey As New SurveyAdministration                   'Declare and create a object of SurveyAdministration
                objSurvey.DomainID = Session("DomainID")                    'Set the Doamin Id
                objSurvey.ContactId = lngCntID                              'Set the contact ID
                Dim dtSurveyHistorylist As DataTable                        'Declare a datatable
                dtSurveyHistorylist = GetRowsInPageRange(objSurvey.getSurveyHistoryList())  'call function to get the list of available surveys

                Dim drSurveyList As DataRow                               'Declare a DataRow object
                For Each drSurveyList In dtSurveyHistorylist.Rows                'Loop through the rows on the table containing the survey list
                    drSurveyList.Item("vcSurName") = Server.HtmlDecode(CStr(drSurveyList.Item("vcSurName")))
                Next

                dgSurvey.DataSource = dtSurveyHistorylist                   'set the datasource
                dgSurvey.DataBind()                                 'databind the datgrid
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to delete the Survey History
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/02/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Protected Overridable Sub btnSurveyHistoryDeleteAction_Command(ByVal sender As Object, ByVal e As EventArgs)
            Try
                Dim btnSurveyHistoryDelete As Button = CType(sender, Button)                                'typecast to button
                Dim dgContainer As DataGridItem = CType(btnSurveyHistoryDelete.NamingContainer, DataGridItem) 'get the containeer object
                Dim gridIndex As Integer = dgContainer.ItemIndex                                            'Get the row idnex of datagrid
                Dim numSurId As Integer = CInt(dgSurvey.Items(gridIndex).Cells(0).Text)                     'Get the Survey Id from the column
                Dim numRespondentID As Integer = CInt(dgSurvey.Items(gridIndex).Cells(1).Text)              'Get the Respondent Id from the column

                Dim objSurvey As New SurveyAdministration                                                   'Declare and create a object of SurveyAdministration
                objSurvey.SurveyId = numSurId                                                               'Get the Survey Id
                objSurvey.SurveyExecution.SurveyExecution().RespondentID = numRespondentID                  'Get the Respondent ID
                objSurvey.DeleteSurveyHistoryForContact()                                                  'Call to delete the Survey History
                DisplaySurveyHistoryLists()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
            'Call to display the list of surveys for the contact
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to pick up a selected range of rows
        ''' </summary>
        ''' <remarks> Returns the table with limited rows
        ''' </remarks>
        ''' <param name="dtSurveyRespondentlist">Table which contains the search result to be displayed in the DataGrid</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/02/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function GetRowsInPageRange(ByVal dtContactSurveylist As DataTable) As DataTable
            Try
                Dim numRowIndex As Integer                                                       'Declare an index variable
                Dim dtResultTableCopy As New DataTable                                           'Declare a datatable object and instantiate
                dtResultTableCopy = dtContactSurveylist.Clone()                                  'Clone the results table

                Dim iRowIndex As Integer = (txtCurrentPageSurveyHistory.Text * Session("PagingRows")) - Session("PagingRows") 'Declare a row index and set it to the first row in the table which has ot be displayed
                Dim iLastRowIndex As Integer = (iRowIndex + Session("PagingRows")) - 1 'Nos of records to be traversed
                For numRowIndex = iRowIndex To iLastRowIndex                                     'Loop through the row index from the table
                    If ((numRowIndex >= 0) And (dtContactSurveylist.Rows.Count > numRowIndex)) Then 'Ensure that the row exists
                        dtResultTableCopy.ImportRow(dtContactSurveylist.Rows(numRowIndex))       'import the range of rows
                    End If
                Next
                Return dtResultTableCopy
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to fill the search results for Adv.Search Screen from the XML file (intermediate search) when the page index changes
        ''' </summary>
        ''' <remarks> When the page number is specifically entered in the textbox, it executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub txtCurrentPageSurveyHistory_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrentPageSurveyHistory.TextChanged
            Try
                If IsNumeric(txtCurrentPageSurveyHistory.Text) Then
                    DisplaySurveyHistoryLists()                                                      'Call to execute the common steps to search and display results
                    litClientMessageSurveyHistory.Text = ""                                          'Clean the client side message
                Else : litClientMessageSurveyHistory.Text = "<script language=javascript>alert('Invalid page number entered, please re-enter.');</script>" 'Alert to user about invalid page number
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the last page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnkLastSurveyHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLastSurveyHistory.Click
            Try
                txtCurrentPageSurveyHistory.Text = lblTotalSurveyHistory.Text                                 'Set the Page Index
                DisplaySurveyHistoryLists()                                                      'Call to execute the common steps to search and display results
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the previous page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnkPreviousSurveyHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPreviousSurveyHistory.Click
            Try
                If Convert.ToInt64(txtCurrentPageSurveyHistory.Text) > 1 Then                               'If the first page is already displayed then exit
                    txtCurrentPageSurveyHistory.Text = txtCurrentPageSurveyHistory.Text - 1                               'decrement the page number
                End If
                DisplaySurveyHistoryLists()                                                                 'Call to execute the common steps to search and display results
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the first page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnkFirstSurveyHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFirstSurveyHistory.Click
            Try
                txtCurrentPageSurveyHistory.Text = 1                                         'Set the first page index to 1
                DisplaySurveyHistoryLists()                                                  'Call to execute the common steps to search and display results
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the next page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnkNextSurveyHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkNextSurveyHistory.Click
            Try
                If Convert.ToInt64(txtCurrentPageSurveyHistory.Text) < Convert.ToInt64(lblTotalSurveyHistory.Text) Then 'check if this is not the last page
                    txtCurrentPageSurveyHistory.Text = txtCurrentPageSurveyHistory.Text + 1 'increment the page number
                End If
                DisplaySurveyHistoryLists()                                                'Call to execute the common steps to search and display results
                litClientMessageSurveyHistory.Text = ""                                    'Clean the client side message
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the Next: 2 page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnk2SurveyHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2SurveyHistory.Click
            Try
                If Convert.ToInt64(txtCurrentPageSurveyHistory.Text) + 2 <= Convert.ToInt64(lblTotalSurveyHistory.Text) Then 'check if incrementing the page counter exhausts the page
                    txtCurrentPageSurveyHistory.Text = txtCurrentPageSurveyHistory.Text + 2  'increment the page number by 2
                End If
                DisplaySurveyHistoryLists()                                                  'Call to Display the Results
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the Next: 3 page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnk3SurveyHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3SurveyHistory.Click
            Try
                If Convert.ToInt64(txtCurrentPageSurveyHistory.Text) + 3 <= Convert.ToInt64(lblTotalSurveyHistory.Text) Then 'check if incrementing the page counter exhausts the page
                    txtCurrentPageSurveyHistory.Text = txtCurrentPageSurveyHistory.Text + 3      'increment the page number by 3
                End If
                DisplaySurveyHistoryLists()                                                      'Call to Display the Results
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the Next: 4 page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnk4SurveyHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4SurveyHistory.Click
            Try
                If Convert.ToInt64(txtCurrentPageSurveyHistory.Text) + 4 <= Convert.ToInt64(lblTotalSurveyHistory.Text) Then  'check if incrementing the page counter exhausts the page
                    txtCurrentPageSurveyHistory.Text = txtCurrentPageSurveyHistory.Text + 4      'increment the page number by 4
                End If
                DisplaySurveyHistoryLists()                                                      'Call to Display the Results
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the Next: 5 page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnk5SurveyHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5SurveyHistory.Click
            Try
                If Convert.ToInt64(txtCurrentPageSurveyHistory.Text) + 5 <= Convert.ToInt64(lblTotalSurveyHistory.Text) Then 'check if incrementing the page counter exhausts the page
                    txtCurrentPageSurveyHistory.Text = txtCurrentPageSurveyHistory.Text + 5  'increment the page number by 5
                End If
                DisplaySurveyHistoryLists()                                                  'Call to Display the Results
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnActionItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActionItem.Click
            Try
                Response.Redirect("../Admin/ActionItemDetailsOld.aspx?frm=contactdetails&CntID=" & lngCntID)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Function ReturnName(ByVal CloseDate) As String
            Try
                Dim strTargetResolveDate As String = ""
                If Not IsDBNull(CloseDate) Then strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
                Return strTargetResolveDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function



        Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActDelete.Click
            Try
                objContacts.ContactID = lngCntID
                objContacts.DomainID = Session("DomainID")
                Dim strError As String = objContacts.DelContact()
                If strError = "" Then
                    Response.Redirect("../contact/frmContactList.aspx")
                Else
                    litMessage.Text = strError
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub radOppTab_TabClick(sender As Object, e As Telerik.Web.UI.RadTabStripEventArgs) Handles radOppTab.TabClick
            Try
                PersistTable.Clear()
                PersistTable.Add(PersistKey.SelectedTab, radOppTab.SelectedIndex)
                PersistTable.Save(boolOnlyURL:=True)

                LoadTabDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub


        Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
            Try
                ViewState("IntermediatoryPage") = False
                boolIntermediatoryPage = False
                tblMain.Controls.Clear()
                LoadControls()
                ControlSettings()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub ControlSettings()
            If boolIntermediatoryPage = True Then
                btnSave.Visible = False
                btnSaveClose.Visible = False
                btnEdit.Visible = True
            Else
                'objCommon.CheckdirtyForm(Page)

                btnSave.Visible = True
                btnSaveClose.Visible = True
                btnEdit.Visible = False
            End If
        End Sub


        Sub PopulateDependentDropdown(ByVal sender As Object, ByVal e As EventArgs)
            objPageControls.PopulateDropdowns(CType(sender, DropDownList), dtTableInfo, objCommon, radOppTab.MultiPage, Session("DomainID"))
        End Sub


        Private Sub lbtnFav_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnFav.Click
            Try
                objContacts.byteMode = 0
                objContacts.UserCntID = Session("UserContactID")
                objContacts.ContactID = lngCntID
                objContacts.Type = "C"
                objContacts.ManageFavorites()
                litMessage.Text = "Added to Favorites"
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)

            End Try
        End Sub

        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
    End Class
End Namespace
