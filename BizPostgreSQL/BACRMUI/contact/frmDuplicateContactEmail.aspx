﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmDuplicateContactEmail.aspx.vb" Inherits=".frmDuplicateContactEmail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script>
        function CloseWindowAndSaveContact() {
            window.close();

            if (window.opener != null) {
                window.opener.SaveAfterExistingContactEmailChange();
            }

            return false;
        }

        function CloseWindowAndChangeEmail(newEmail) {
            window.close();

            if (window.opener != null) {
                window.opener.ChangeEmailAndSave(newEmail);
            }

            return false;
        }

        function CloseWindowAndRefreshParent() {
            window.close();

            if (window.opener != null) {
                window.opener.location.href = window.opener.location.href;
            }

            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="pull-right">
        <asp:Button ID="btnSaveClose" runat="server" CssClass="btn btn-primary" Text="Save & Close" />
        <asp:Button ID="btnCancel" runat="server" CssClass="btn btn-primary" Text="Cancel" OnClientClick="Close();" />
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Contact - Change Email 
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <div class="row padbottom10" id="divError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>
                <p>
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
    <div class="alert alert-info alert-dismissible padbotton10">
        <h4><i class="icon fa fa-info"></i> Alert!</h4>
        You can’t use this email address because (Contact) <asp:Label ID="lblContactName" runat="server"></asp:Label> of (Organization) <asp:Label ID="lblOrganizationName" runat="server"></asp:Label> is already using it.
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="form-inline">
                <div class="form-group">
                    <label>
                        You can change the existing address
                    </label>
                    <asp:TextBox ID="txtChangeExistingAddress" runat="server" CssClass="form-control" Width="250"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="form-inline">
                <div class="form-group">
                    <label>
                        You can simply use a different email address (enter it here)
                    </label>
                    <asp:TextBox ID="txtNewEmailAddress" runat="server" CssClass="form-control" Width="250"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hdnFromInlineEdit" runat="server" />
    <asp:HiddenField ID="hdnEmailBeingEdited" runat="server" />
    <asp:HiddenField ID="hdnContactBeingEdited" runat="server" />
    <asp:HiddenField ID="hdnEmailEnteredByUser" runat="server" />
    <asp:HiddenField ID="hdnExistingContactWithSameEmail" runat="server" />
</asp:Content>
