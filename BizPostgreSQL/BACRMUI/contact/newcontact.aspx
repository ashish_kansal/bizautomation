<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="newcontact.aspx.vb" Inherits=".newcontact"
    MasterPageFile="~/common/Popup.Master" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Contact</title>
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script src="../JavaScript/jquery.min.js" type="text/javascript"></script>
    <script src="../JavaScript/jquery.Tooltip.js" type="text/javascript"></script>
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script language="javascript" src="../javascript/CustomFieldValidation.js"></script>
        <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <%--<script type="text/javascript" src="../JavaScript/jquery.min.js"></script>--%>
    <script src="../JavaScript/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../JavaScript/jquery.tokeninput.js"></script>
    <link href="../Styles/token-input.css" rel="Stylesheet" type="text/css" />
    <link href="../Styles/token-input-facebook.css" rel="Stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        var saved_tokens = [];
        var CompanyName = "";
        // var saved_tokensCC = [];
        var hdnTo = [];
        var token_count = 0;
        var maruti = 0;
        function Save() {
            if ($find('radCmbCompany').get_value() == "") {
                alert("Select Company")
                return false;
            }
            var oldEmailIds = $(".txtAutoLookUpEmail").val();
            var arrElement = oldEmailIds.split("~");
            var newEmailId = "";
            if (arrElement.length > 2) {
                newEmailId = arrElement[1];
            }
            $(".txtAutoLookUpEmail").val(newEmailId);
            return validateCustomFields();
        }

        function OpenEmailChangeWindow(contactID, email) {
            window.open('../contact/frmDuplicateContactEmail.aspx?contactID=' + contactID + '&email=' + email, 'ContactChangeEmail', 'toolbar=no,titlebar=no,top=200,left=200,width=800,height=400,scrollbars=no,resizable=no')
            return false
        }

        function SaveAfterExistingContactEmailChange() {
            $('[id$=btnSave]').click();
            return true;
        }

        function ChangeEmailAndSave(newEmail) {
            $('[id$=hdnChangedEmail]').val(newEmail);
            $('[id$=btnSave]').click();
            return true;
        }

        function openAddress(CntID) {
            window.open("../contact/frmContactAddress.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&pqwRT=" + CntID + "&Reload=2", '', 'toolbar=no,titlebar=no,top=300,left=300,width=400,height=300,scrollbars=no,resizable=no')
            return false;
        }

        function UpdateAddress(addressID) {
            $("[id$=hdnAddressID]").val(addressID);
        }
        $(document).ready(function () {
            $(".txtAutoLookUpEmail").attr('autocomplete', 'off');
            $(".txtAutoLookUpEmail").tokenInput("EmailList.ashx", {
                theme: "facebook",
                tokenDelete: "token-input-delete-token-facebook",
                tokenLimit:1,
                propertyToSearch: "email",
                resultsFormatter: function (item) { return "<li><div style='display: inline-block; padding-left: 10px;'><div class='full_name'>" + item.first_name + " " + item.last_name + "</div><div class='email'>" + item.email + "</div></div></li>" },
                tokenFormatter: function (item) { return "<li><p title='" + item.email + "'>" + item.first_name + " " + item.last_name + "</p></li>" }

            });
            $('form[name=form1]').find(':text').filter(":visible:enabled").filter(".signup").first().focus();
        })
    </script>
    <style type="text/css">
        #tblOppr td:nth-child(1), td:nth-child(3) {
            font-weight:bold;
        }
        ul.token-input-list-facebook li input{
            background:none !important;
        }
        .txtAutoLookUpEmail{
            width:200px;
        }
        ul.token-input-list-facebook{
            width: 84%;
    float: left;
    margin-right: 5px;
        }
        .btnGoNewEmail{
            display:none;
        }
        .button{
                margin-left: -1px !important;
    float: left;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSaveNew" runat="server" Text="Save &amp; New" CssClass="button"></asp:Button>
            <asp:Button ID="btnSave" runat="server" Text="Save &amp; Close" CssClass="button"></asp:Button>
            <asp:Button ID="btnClose" runat="server" OnClientClick="javascript:window.close()" CssClass="button" Text="Close" Width="50"></asp:Button>
            <asp:HiddenField ID="hdnChangedEmail" runat="server" />
            <asp:HiddenField ID="hdnAddressID" runat="server" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lbContacts" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="ScriptManager1" EnablePartialRendering="true" runat="server">
    </asp:ScriptManager>
    <table cellspacing="0" cellpadding="0" width="840px" border="0">
        <tr>
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
       <%-- <tr>
            <td id="tdGoogleContact" runat="server" visible="false" class="normal1" align="right"
                valign="top">Add to Google Contact &nbsp;
                <asp:CheckBox ID="chkGoogleApps" runat="server" />
            </td>
        </tr>--%>
    </table>
    <asp:Table ID="tblOppr" BorderWidth="1" runat="server" CssClass="aspTable" Width="840px"
        BorderColor="black" GridLines="None" CellPadding="3" CellSpacing="3" align="center">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <br>
                <table align="center" width="100%" cellpadding="2" style="margin-bottom:-8px;">
                    <tr>
                        <td class="normal1Right" align="right" width="20%">Customer<font color="#ff0000">*</font>
                        </td>
                        <td class="normal1" width="30%">
                            <telerik:RadComboBox AccessKey="C" ID="radCmbCompany" Width="195px" DropDownWidth="600px"
                                OnClientItemsRequested="OnClientItemsRequestedOrganization"
                                ClientIDMode="Static"
                                ShowMoreResultsBox="true"
                                Skin="Default" runat="server" AllowCustomText="True" EnableLoadOnDemand="True">
                                <WebServiceSettings Path="../common/Common.asmx" Method="GetCompanies" />
                            </telerik:RadComboBox>
                        </td>
                        <td width="20%"></td>
                        <td width="30%"></td>
                    </tr>
                </table>
                <asp:Table ID="tblMain" CellPadding="3" CellSpacing="3" align="center" Width="100%"
                    runat="server">
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:HiddenField ID="hdnIsDuplicate" runat="server" Value="0" />
</asp:Content>
