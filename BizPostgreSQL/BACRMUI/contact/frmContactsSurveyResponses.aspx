<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmContactsSurveyResponses.aspx.vb" Inherits="BACRM.UserInterface.Survey.frmContactsSurveyResponses" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
	<head id="Head1" runat="server">
        <link rel="stylesheet" href="~/CSS/master.css" type="text/css" />
		<title>Contacts Survey Responses</title>
	</HEAD>
	<body >
		<form id="frmContactsSurveyResponses" method="post" runat="server">
			<asp:datagrid id="dgContactsSurveyResponses" runat="server" Width="100%" AutoGenerateColumns="False"
				CssClass="dg" AlternatingItemStyle-CssClass="ais" ItemStyle-CssClass="is" HeaderStyle-CssClass="hs"
				  BorderWidth="1px" CellPadding="2" CellSpacing="0"
				ShowHeader="True">
				<Columns>
					<asp:BoundColumn Visible="True" ItemStyle-HorizontalAlign="Left" HeaderText="Question" DataField="vcQuestion" ></asp:BoundColumn>
					<asp:BoundColumn Visible="True" ItemStyle-HorizontalAlign="Left" HeaderText="Answer" DataField="vcAnsLabel" ></asp:BoundColumn>
				</Columns>
			</asp:datagrid>
		</form>
	</body>
</HTML>
