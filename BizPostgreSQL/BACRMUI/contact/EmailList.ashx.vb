﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Script.Serialization
Imports System.Linq
Imports System.Collections.Generic
Imports System.Text
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports System.Web.SessionState
Public Class EmailList1
    Implements System.Web.IHttpHandler, IReadOnlySessionState
    Dim intCount As Integer
    Sub ProcessRequest(ByVal context As HttpContext) Implements IHttpHandler.ProcessRequest

        'context.Response.ContentType = "application/json"
        'Dim strEmailList As String = "[{id: 7, name: 'Ruby'}]"
        'context.Response.Write(strEmailList)
        Dim objContacts As New CContacts
        Dim dtContacts As DataTable
        ' Dim strEmail As String = tbAuto.Text
        With objContacts
            'If strEMail.Contains(",") Then
            '    .KeyWord = strEMail.Substring(strEMail.LastIndexOf(",") + 1)
            '    If .KeyWord.Length <= 1 Then Exit Sub

            'Else
            '    .KeyWord = strEMail
            'End If
            '.UserCntID = CLng(objUserContactId)
            '.UserRightType = hdnUserRights.Value
            .KeyWord = context.Request.QueryString("q").ToString
            .DomainID = context.Session("DomainID")
            .CurrentPage = 1
            .PageSize = 10
            .TotalRecords = 0
            .columnName = "ADC.bintcreateddate"
            'If Session("Asc") = 1 Then
            .columnSortOrder = "Desc"
            'Else : .columnSortOrder = "Asc"
            'End If
        End With
        objContacts.bitFlag = False
        dtContacts = objContacts.GetContactEmailList
        'viewstate("intCount") = intCount
        ' HttpContext.Current.Cur()
        Dim nameList As New StringBuilder

        nameList.Append("[")
        '  Dim intCount As Integer
        For intCount = 0 To dtContacts.Rows.Count - 1
            nameList.Append("{")
            nameList.Append("""id"":""" & dtContacts.Rows(intCount)("numContactID") & """,")
            nameList.Append("""first_name"":""" & dtContacts.Rows(intCount)("vcFirstName") & """,")
            nameList.Append("""last_name"":""" & CCommon.ToString(dtContacts.Rows(intCount)("vcLastName")) & " (" & CCommon.ToString(dtContacts.Rows(intCount)("Company")) & ")" & """,")
            nameList.Append("""last_name_actual"":""" & CCommon.ToString(dtContacts.Rows(intCount)("vcLastName")) & """,")
            nameList.Append("""company_type"":""" & CCommon.ToString(dtContacts.Rows(intCount)("Company")) & """,")
            nameList.Append("""email"":""" & dtContacts.Rows(intCount)("vcEmail") & """,")
            nameList.Append("""url"":""#""")
            'nameList.Append("""Firstname"":""" & dtContacts.Rows(intCount)("vcFirstName") & """")
            nameList.Append("},")
        Next
        nameList.Remove(nameList.ToString.Length - 1, 1)
        nameList.Append("]")
        nameList.Remove(0, 1)
        nameList.Remove(nameList.Length - 1, 1)

        context.Response.ContentType = "application/json"
        context.Response.ContentEncoding = Encoding.UTF8

        'Dim js As New JavaScriptSerializer()
        'Dim strJSON As String = js.Serialize(nameList)
        context.Response.Write("[" & nameList.ToString & "]")


    End Sub


    ReadOnly Property IsReusable() As Boolean Implements IHttpHandler.IsReusable
        Get
            Return False
        End Get
    End Property

End Class