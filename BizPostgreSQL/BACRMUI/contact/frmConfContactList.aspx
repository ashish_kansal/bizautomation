<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmConfContactList.aspx.vb" Inherits="BACRM.UserInterface.Contacts.frmConfContactList" 
MasterPageFile ="~/common/Popup.Master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
  	<title>Contacts Column Customization</title>
		<script language="javascript" src="../javascript/AdvSearchScripts.js"></script>
		<script language="javascript" type="text/javascript" >
		    function Save() {
		        var str = '';
		        for (var i = 0; i < document.getElementById('lstSelectedfld').options.length; i++) {
		            var SelectedValue;
		            SelectedValue = document.getElementById('lstSelectedfld').options[i].value;
		            str = str + SelectedValue + ','
		        }
		        document.getElementById('hdnCol').value = str;
		        //alert(document.getElementById('hdnCol').value);
		    }
		
		</script>
	
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table cellspacing="0" cellpadding="0" width="100%" border="0">
                <tr>
                    <td align="right">
                       <table cellspacing="0" cellpadding="0" width="500">
				<tr>
					<td class="normal1">Contact Type</td>
					<td > <asp:DropDownList ID="ddlType" runat="server" AutoPostBack="true" CssClass="signup" ></asp:DropDownList>
                          &nbsp;
                    </td>
					<td align="right" height="23">
					<asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button" Width="50" />
					&nbsp;
                    <input class="button" id="btnClose" style="width:50" onclick="javascript:window.opener.location.reload(true);window.close()" type="button" value="Close">&nbsp;&nbsp;&nbsp;
					 &nbsp;&nbsp;
                    </td>
				</tr>
			</table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Contacts Layout
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
 	<asp:table id="tblAdvSearchFieldCustomization" Runat="server" Width="800px" GridLines="None" CssClass="aspTable"
				BorderColor="black" BorderWidth="1">
				<asp:TableRow>
					<asp:TableCell>
						<table cellpadding="0" cellspacing="0" width="100%">
							
							<tr>
								<td align="center" class="normal1" valign="top">
									Available Fields<br>
									&nbsp;&nbsp;
									<asp:ListBox ID="lstAvailablefld" Runat="server" Width="150" Height="200" CssClass="signup" EnableViewState="False"></asp:ListBox>
								</td>
								<td align="center" class="normal1" valign="middle">
									<input type="button" id="btnAdd" Class="button" Value="Add >" onclick="javascript:move(document.getElementById('lstAvailablefld'),document.getElementById('lstSelectedfld'))">
									<br>
									<br>
									<input type="button" id="btnRemove" Class="button" Value="< Remove" onclick="javascript:remove1(document.getElementById('lstSelectedfld'),document.getElementById('lstAvailablefld'));"></td>
								<td align="center" class="normal1">
									Selected Fields/ Choose Order<br>
									<asp:ListBox ID="lstSelectedfld" Runat="server" Width="150" Height="200" CssClass="signup" EnableViewState="False"></asp:ListBox>
								</td>
								<td align="center" class="normal1" valign="middle">
									<br>
									<br>
									<br>
									<br>
									<br>
									<img id="btnMoveupOne" src="../images/upArrow.gif"
										onclick="javascript:MoveUp(document.getElementById('lstSelectedfld'));"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<br>
									<br>
									<img id="btnMoveDownOne" src="../images/downArrow1.gif"
										onclick="javascript:MoveDown(document.getElementById('lstSelectedfld'));"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<br>
									<br>
									<br>
									<br>
									<br>
									(Max 15)
								</td>
							</tr>
						</table>
					</asp:TableCell>
				
				</asp:TableRow>
				
			</asp:table><input id="hdnCol" type="hidden" name="hdXMLString" runat="server" value="" />
			<input id="hdSave" type="hidden" name="hdSave" runat="server" value="False" />	
</asp:Content>
