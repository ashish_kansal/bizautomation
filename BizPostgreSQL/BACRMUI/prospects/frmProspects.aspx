﻿<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" MasterPageFile="~/common/DetailPage.Master"
    CodeBehind="frmProspects.aspx.vb" Inherits="BACRM.UserInterface.Prospects.frmProspects" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Src="../common/frmCorrespondence.ascx" TagName="frmCorrespondence" TagPrefix="uc1" %>
<%@ Register Src="../account/OpportunitiesTab.ascx" TagName="OpportunitiesTab" TagPrefix="uc1" %>
<%@ Register Src="../include/AssetList.ascx" TagName="AssetList" TagPrefix="uc1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <script type='text/javascript' src="../JavaScript/biz.VendorLeadTimes.js"></script>
    <script type="text/javascript">

        function openActionItem(a, b, c, d, e, f) {
            if (e == 'Email') {
                window.open("../outlook/frmMailDtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&numEmailHstrID=" + a, '', 'titlebar=no,top=100,left=250,width=850,height=550,scrollbars=yes,resizable=yes');
                return false;
            }
            else {
                window.location.href = "../admin/ActionItemDetailsOld.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospects&CommId=" + a + "&CaseId=" + b + "&CaseTimeId=" + c + "&CaseExpId=" + d;
                return false;
            }

        }
        function OpemParticularEmail(b) {

            var a = $("#imgEmailPopupId").attr("email")
            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenContact(a, b) {
            var str;
            str = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactlist&ft6ty=oiuy&CntId=" + a;
            document.location.href = str;
        }
        function OpenContactSetting() {
            window.open('../Contact/frmConfContactList.aspx?FormId=56', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function OpenContact(a, b) {
            var str;
            str = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospects&SI1=1&CntId=" + a;

            document.location.href = str;
        }

        function OpenNewAction(a) {
            var str;
            str = "../admin/ActionItemDetailsOld.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Prospects&CntID=" + a;

            document.location.href = str;
        }

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function ShowWindow(Page, q, att) {

            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                return false;

            }
        }

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function openFollow(a) {
            window.open("../Leads/frmFollowUpHstr.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&rtyWR=" + a, 'WebLinkLabel', 'width=500,height=200,status=no,scrollbar=yes,top=110,left=150');
            return false;
        }

        function fn_GoToURL(varURL) {
            var url = document.getElementById(varURL).value
            if ((url != '') && (url.substr(0, 7) == 'http://') && (url.length > 7)) {
                var LoWindow = window.open(url, "", "");
                LoWindow.focus();
            }
            return false;
        }
        function fn_EditLabels(URL) {
            window.open(URL, 'WebLinkLabel', 'width=500,height=70,status=no,scrollbar=yes,top=110,left=150');
            return false;
        }
        function OpenTransfer(url) {
            window.open(url, '', "width=340,height=150,status=no,top=100,left=150");
            return false;
        }
        function OpenLst(url) {
            window.open(url, '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,left=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenAdd(a) {
            window.open("../prospects/frmProspectsAdd.aspx?pwer=dfsdfdsfdsfdsf&rtyWR=" + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=300,scrollbars=no,resizable=no')
            return false;
        }
        /*Commented by Neelam - Obsolete function*/
        function OpenDocuments(a) {
            window.open("../Documents/frmSpecDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=A&yunWE=" + a, '', 'toolbar=no,titlebar=no,top=10,width=1000,height=450,left=10,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenTo(a) {
            window.open("../admin/frmcomAssociationTo.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&boolAssociateDiv=true&numDivisionId=" + a, '', 'toolbar=no,left=200,titlebar=no,top=300,width=800,height=500,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenFrom(a) {
            window.open("../admin/frmCompanyAssociationFrom.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&rtyWR=" + a, '', 'toolbar=no,titlebar=no,top=300,left=200,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }
        function Disableddl() {
            if (Tabstrip6.selectedIndex == 0) {
                document.Form1.ddlOppStatus.style.display = "none";
            }
            else {
                document.Form1.ddlOppStatus.style.display = "";
            }
        }
        function CheckNumber(cint) {
            if (cint == 1) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode == 44 || window.event.keyCode == 46)) {
                    window.event.keyCode = 0;
                }
            }
            if (cint == 2) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                    window.event.keyCode = 0;
                }
            }

        }

        /*
        Purpose:	Lead to Organization Details Page
        Created By: Debasish Tapan Nag
        Parameter:	1) numContactId: The Contact Id
        Return		1) None
        */
        function GoOrgDetails(a, b) {
            var str;
            if (b == 0) {

                str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=LeadsList&DivID=" + a;

            }
            else if (b == 1) {

                str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=LeadsList&DivID=" + a;

            }
            else if (b == 2) {

                str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=LeadsList&klds+7kldf=fjk-las&DivId=" + a;

            }
            document.location.href = str;
        }


        function OpemEmail(a, b) {
            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
            return false;
        }

        function ShowLayout(a, b, c, d) {
            window.open("../pagelayout/frmCustomisePageLayout.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Ctype=" + a + "&type=" + c + "&PType=3&FormId=" + d, '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function Save() {
            return validateCustomFields();
        }

        function OpenShareRecord(a) {
            window.open("../common/frmShareRecord.aspx?RecordID=" + a + "&ModuleID=1", '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
            return false;
        }

        /*Function added by Neelam on 10/07/2017 - Added functionality to open Child Record window in modal pop up*/
        function OpenDocumentFiles(divID) {
            $find("<%= RadWinDocumentFiles.ClientID%>").show();
        }

        /*Function added by Neelam on 02/14/2018 - Added functionality to close document modal pop up*/
        function CloseDocumentFiles(mode) {
            debugger;
            var radwindow = $find('<%=RadWinDocumentFiles.ClientID %>');
            radwindow.close();

            if (mode == 'C') { return false; }
            else if (mode == 'L') {
                __doPostBack("<%= btnLinkClose.UniqueID%>", "");
            }
        }

        function DeleteDocumentOrLink(fileId) {
            if (confirm('Are you sure, you want to delete the selected item?')) {
                $('#hdnFileId').val(fileId);
                __doPostBack("<%= btnDeleteDocOrLink.UniqueID%>", "");
            }
            else {
                return false;
            }
        }

        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);

            requestStart = function (target, arguments) {
                if (arguments.get_eventTarget().indexOf("btnAttach") > -1) {
                    arguments.set_enableAjax(false);
                }
                if (arguments.get_eventTarget().indexOf("btnLinkClose") > -1) {
                    arguments.set_enableAjax(false);
                }
            }
        });

        function pageLoaded() {
            var tab = $find("<%= radOppTab.ClientID%>").get_selectedTab()
            if (tab.get_value() == "VendorLeadTimes") {
                LoadVendorLeadTimes();
            }
        }

        function openPurchaseincentivesPopup(DivID) {
            window.open('../account/frmPurchaseIncentives.aspx?DivId=' + DivID, '', 'toolbar=no,titlebar=no,top=200,left=200,width=800,height=500,scrollbars=yes,resizable=yes')
            return false
        }
    </script>
    <style>
        #divLinkUpload:before {
            content: "";
            background-color: #dcdcdc;
            position: absolute;
            width: 1px;
            height: 100%;
            top: 10px;
            left: 0%;
            display: block;
        }

        .tblNoBorder td, .tblNoBorder th {
            border: 0px solid #fff !important;
        }

        #tblMain tr td {
            border-bottom: 1px solid #e1e1e1 !important;
        }

        #tblMain tbody tr:first-child td {
            border-top: 1px solid #fff !important;
            border-left: 1px solid #fff !important;
            border-right: 1px solid #fff !important;
        }

        .tableGroupHeader {
            background-color: #f5f5f5 !important;
            font-weight: bold;
        }

        @media (max-width:40em) {
            .tblNoBorder table, .tblNoBorder thead, .tblNoBorder tbody, .tblNoBorder tfoot, .tblNoBorder th, .tblNoBorder td, .tblNoBorder tr {
                display: block;
            }

            .tblNoBorder .editable_select {
                min-height: 30px;
            }

            .tblNoBorder td, .tblNoBorder th {
                text-align: left;
            }
        }

        /*div.RadUpload .ruFakeInput {
            visibility: hidden;
            width: 0;
            padding: 0;
        }

        div.RadUpload .ruFileInput {
            width: 1;
        }*/

        .hidden {
            display: none;
        }

        .VerticalAligned {
            vertical-align: top;
        }

        /*div span,
        div img {
            display: inline-block;
            vertical-align: middle;
        }​*/
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Button ID="btnDeleteDocOrLink" OnClick="btnDeleteDocOrLink_Click" runat="server" CssClass="hidden"></asp:Button>
    <asp:HiddenField runat="server" ID="hdnFileId" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-3">
                <div class="pull-left callout bg-theme">
                    <%--<strong>Organization ID : </strong>--%>
                    <asp:Label ID="lblCustomerId" runat="server" Font-Size="Larger" Text="Customer :"></asp:Label>
                    <asp:Label ID="lblAssociation" runat="Server" Font-Size="Larger"></asp:Label>
                </div>
            </div>
            <div class="col-xs-12 col-sm-5">

                <div class="record-small-box record-small-group-box createdBySingleSection">
                    <asp:HyperLink ID="hplTransfer" runat="server" Visible="true" Text="Record Owner:"
                        ToolTip="Transfer Ownership">Owner
                    </asp:HyperLink>
                    <a href="#" id="hplTransferNonVis" runat="server" visible="false">Owner <i class="fa fa-user"></i></a>
                    <span class="innerCreated">
                        <asp:Label ID="lblRecordOwner" runat="server"></asp:Label>
                    </span>
                    <a href="#">Created</a>
                    <span class="innerCreated">
                        <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
                    </span>
                    <a href="#">Modified</a>
                    <span class="innerCreated">
                        <asp:Label ID="lblLastModifiedBy" runat="server"></asp:Label>
                    </span>
                </div>

            </div>
            <div class="col-md-4">
                <div class="pull-right">
                    <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                    <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save &amp; Close</asp:LinkButton>
                    <asp:LinkButton ID="btnEdit" runat="server" CssClass="btn btn-primary"><i class="fa fa-pencil-square-o"></i>&nbsp;&nbsp;Edits</asp:LinkButton>
                    <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-primary"><i class="fa fa-times"></i>&nbsp;&nbsp;Cancel</asp:LinkButton>
                    <asp:LinkButton ID="btnActDelete" runat="server" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <center>
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </center>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <telerik:RadTabStrip ID="radOppTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
        Skin="Default" ClickSelectedTab="True" AutoPostBack="True" SelectedIndex="0"
        MultiPageID="radMultiPage_OppTab">
        <Tabs>
            <telerik:RadTab Text="&nbsp;&nbsp;Prospect Details&nbsp;&nbsp;" Value="Prospect Details"
                PageViewID="radPageView_ProspectDetails">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Contacts&nbsp;&nbsp;" Value="Contacts" PageViewID="radPageView_Contacts">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Transactions&nbsp;&nbsp;" Value="Opportunities" PageViewID="radPageView_Opportunities">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Projects&nbsp;&nbsp;" Value="Projects" PageViewID="radPageView_Projects">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Accounting&nbsp;&nbsp;" Value="Accounting" PageViewID="radPageView_Accounting">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Web Analysis&nbsp;&nbsp;" Value="WebAnalysis" PageViewID="radPageView_WebAnalysis">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Correspondence&nbsp;&nbsp;" Value="Correspondence" PageViewID="radPageView_Correspondence">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Assets&nbsp;" Value="Assets" PageViewID="radPageView_Assets" Visible="false">
            </telerik:RadTab>
            <telerik:RadTab Text="Associations" Value="Associations" PageViewID="radPageView_Associations">
            </telerik:RadTab>
            <telerik:RadTab Text="Vendor Lead-Times" Value="VendorLeadTimes" PageViewID="radPageView_VendorLeadTimes">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage_OppTab" runat="server" SelectedIndex="0" CssClass="pageView">
        <telerik:RadPageView ID="radPageView_ProspectDetails" runat="server">
            <div class="row">
                <div class="col-xs-12">
                    <div class="pull-left">
                        <asp:ImageButton ID="imgOrder" runat="server" ImageUrl="~/images/Building-48.gif" />
                    </div>
                    <div class="pull-right">
                        <table border="0" style="width: 100%; padding-right: 5px;">
                            <tr>
                                <td style="text-align: left;">
                                    <table id="tblDocuments" runat="server">
                                    </table>
                                </td>
                                <td>
                                    <asp:ImageButton ID="imgOpenDocument" runat="server" ImageUrl="~/images/icons/drawer.png" Style="height: 30px;" />
                                </td>
                                <td style="text-align: left;">
                                    <asp:HyperLink ID="hplOpenDocument" runat="server" Font-Bold="true" Font-Underline="true" Text="Attach or Link" Style="display: inline-block; vertical-align: top; line-height: normal;"></asp:HyperLink>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <asp:Table ID="tblMain" CellPadding="3" CellSpacing="3" align="center" Width="100%" CssClass="table table-responsive tblNoBorder" GridLines="none" border="0" runat="server">
                        </asp:Table>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Contacts" runat="server">
            <div class="table-responsive">
                <asp:Table ID="Table1" CellPadding="0" CellSpacing="0" Height="300" runat="server"
                    Width="100%" GridLines="None">
                    <%--  <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right" VerticalAlign="Top" Height="20">
                 <a href="#" onclick="return OpenContactSetting()" title="Show Grid Settings.">
                            <img src="../images/NewUiImages/setting-icon.gif" alt="" /></a>
                    </asp:TableCell>
                </asp:TableRow>--%>
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:GridView ID="gvContacts" runat="server" EnableViewState="true" AutoGenerateColumns="false"
                                CssClass="table table-responsive table-bordered" Width="100%" ShowHeaderWhenEmpty="true">
                                <Columns>
                                </Columns>
                            </asp:GridView>
                            <%--  <asp:DataGrid ID="dgContacts" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False">
                            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                            <ItemStyle CssClass="is"></ItemStyle>
                            <HeaderStyle CssClass="hs"></HeaderStyle>
                            <Columns>
                                <asp:BoundColumn DataField="numContactId" Visible="False"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numCreatedBy" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn DataField="numTerid" Visible="False"></asp:BoundColumn>
                                <asp:HyperLinkColumn DataNavigateUrlField="numContactId" DataNavigateUrlFormatString="../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospects&CntId={0}"
                                    DataTextField="Name" HeaderText="First & Last Name"></asp:HyperLinkColumn>
                                <asp:BoundColumn HeaderText="Position,Type" DataField="Pos"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Email">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hplEmail" runat="server" CssClass="hyperlink" Text='<%# DataBinder.Eval(Container.DataItem, "Email") %>'
                                            Target="_blank">
                                        </asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn HeaderText="Phone - Ext" DataField="Phone"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Assistant's Name" DataField="Asst"></asp:BoundColumn>
                                <asp:BoundColumn HeaderText="Assistant's Phone - Ext" DataField="AsstPhone"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="<font >New Action Item</font>">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hplNewActItem" runat="server" CssClass="hyperlink">New Action Item</asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="<font >Last 10 Action Item Opened</font>">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hplLast10Opened" runat="server" CssClass="hyperlink">Last 10 Action Item Opened</asp:HyperLink>/
                                        <asp:HyperLink ID="hplLst10Closed" runat="server" CssClass="hyperlink">Closed</asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <HeaderTemplate>
                                        <asp:Button ID="btnHdelete" runat="server" CssClass="button Delete" Text="X"></asp:Button>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete">
                                        </asp:Button>
                                        <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
																<font color="#730000">*</font></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>--%>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Opportunities" runat="server">
            <div class="table-responsive">
                <asp:Table ID="Table5" runat="server" Height="300" Width="100%" GridLines="None">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <uc1:OpportunitiesTab ID="OpportunitiesTab1" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Projects" runat="server">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-inline">
                        <label>Project Status</label>
                        <asp:DropDownList ID="ddlProjectStatus" runat="server" CssClass="form-control" AutoPostBack="True">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <br />
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <asp:DataGrid ID="dgProjectsOpen" runat="server" Width="100%" CssClass="table table-responsive table-bordered" UseAccessibleHeader="true" AutoGenerateColumns="False">
                            <Columns>
                                <asp:BoundColumn Visible="False" DataField="numProId"></asp:BoundColumn>
                                <asp:BoundColumn Visible="False" DataField="numContactId"></asp:BoundColumn>
                                <asp:ButtonColumn DataTextField="Name" HeaderText="Name" CommandName="Name"></asp:ButtonColumn>
                                <asp:BoundColumn DataField="DealID" HeaderText="Opportunity Or Deal ID"></asp:BoundColumn>
                                <asp:ButtonColumn DataTextField="CustJobContact" HeaderText="Cust. Prime Job Contact"
                                    CommandName="Contact"></asp:ButtonColumn>
                                <asp:BoundColumn DataField="vcResource" HeaderText="Resource(Project Role)"></asp:BoundColumn>
                                <asp:BoundColumn DataField="LstCmtMilestone" HeaderText="Last Completed Milestone"></asp:BoundColumn>
                                <asp:BoundColumn DataField="lstCompStage" HeaderText="Last completed Stage, Completed vs. Due Date"></asp:BoundColumn>
                            </Columns>
                            <PagerStyle Visible="False" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
                        </asp:DataGrid>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Accounting" runat="server">
            <div class="table-responsive">
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>
                                Credit Limit
                            </label>
                            <asp:DropDownList ID="ddlCreditLimit" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Tax Details</label>
                            <div>
                                <asp:CheckBoxList ID="chkTaxItems" CssClass="normal1" runat="server" RepeatDirection="Horizontal">
                                </asp:CheckBoxList>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Net</label>
                            <div class="input-group">
                                <asp:DropDownList ID="ddlNetDays" runat="server" CssClass="form-control">
                                </asp:DropDownList><span class="input-group-addon">Days</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3" style="display: none">
                        <div class="form-group">
                            <label>days</label>
                            <div>
                                <asp:RadioButton ID="radPlus" GroupName="rad" Checked="True" runat="server" Text="Plus"></asp:RadioButton>
                                <asp:RadioButton ID="radMinus" runat="server" GroupName="rad" Text="Minus"></asp:RadioButton>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3" style="display: none">
                        <div class="form-group">
                            <label>&nbsp;</label>
                            <div>
                                <asp:TextBox ID="txtInterest" runat="server" Width="40" CssClass="signup">
                                </asp:TextBox>%
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Default Trading Currency</label>
                            <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <asp:Table ID="Table11" Height="300" runat="server" Width="100%" GridLines="None">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                     
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_WebAnalysis" runat="server">
            <div class="table-responsive">
                <asp:Table ID="Table12" Height="300" runat="server" Width="100%" GridLines="None">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <table width="700">
                                <tr>
                                    <td class="text_bold" align="right">Original Referring Page :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblReferringPage" runat="server" CssClass="normal1"></asp:Label>
                                    </td>
                                    <td class="text_bold" align="right">Original Referring Key Word Used :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblKeyword" runat="server" CssClass="normal1"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text_bold" align="right">First / Last Date visited :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblDatesvisited" runat="server" CssClass="normal1"></asp:Label>
                                    </td>
                                    <td class="text_bold" align="right">Total Number of times visited :
                                    </td>
                                    <td>
                                        <asp:Label ID="lblNoofTimes" runat="server" CssClass="normal1"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <asp:DataList ID="dlWebAnlys" runat="server" Width="100%" CellPadding="0" CellSpacing="2">
                                <ItemTemplate>
                                    <table width="100%" class="hs">
                                        <tr>
                                            <td align="right">Date Visitied:
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCreated" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dtCreated") %>'>
                                                </asp:Label>
                                            </td>
                                            <td align="right">
                                                <asp:Label ID="Label4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.NoOfTimes") %>'>
                                                </asp:Label>&nbsp;Pages Viewed :
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnk" CommandName="Pages" runat="server" Style="text-decoration: none">
													<font color="white">Click Here</font></asp:LinkButton>
                                            </td>
                                            <td align="right">Total Time on site :
                                            </td>
                                            <td>
                                                <asp:Label ID="lblTotalTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcTotalTime") %>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                                <SelectedItemTemplate>
                                    <table width="100%" cellpadding="0" cellspacing="0">
                                        <tr class="hs">
                                            <td align="right">
                                                <asp:Label ID="lblID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.numTrackingID") %>'
                                                    Visible="false">
                                                </asp:Label>
                                                Date Visitied:
                                            </td>
                                            <td>
                                                <asp:Label ID="lblCreated" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.dtCreated") %>'>
                                                </asp:Label>
                                            </td>
                                            <td align="right">
                                                <asp:Label ID="Label4" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.NoOfTimes") %>'>
                                                </asp:Label>&nbsp;Pages Viewed :
                                            </td>
                                            <td align="right">Total Time on site :
                                            </td>
                                            <td>
                                                <asp:Label ID="lblTotalTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.vcTotalTime") %>'>
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <asp:DataGrid ID="dgWebAnlys" runat="server" AllowSorting="True" CssClass="dg table table-responsive table-bordered" Width="100%"
                                                    AutoGenerateColumns="False">
                                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                                    <ItemStyle CssClass="is"></ItemStyle>
                                                    <HeaderStyle CssClass="hs"></HeaderStyle>
                                                    <Columns>
                                                        <asp:BoundColumn Visible="False" DataField="numTracVisitorsHDRID"></asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Page" DataField="vcPageName"></asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Time Spent on the Page" DataField="vcElapsedTime"></asp:BoundColumn>
                                                        <asp:BoundColumn HeaderText="Time Visited" DataField="TimeVisited"></asp:BoundColumn>
                                                    </Columns>
                                                </asp:DataGrid>
                                            </td>
                                        </tr>
                                    </table>
                                </SelectedItemTemplate>
                            </asp:DataList>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Correspondence" runat="server">
            <div class="table-responsive">
                <asp:Table ID="Table4" CellPadding="0" CellSpacing="0" runat="server" Width="100%"
                    GridLines="None" Height="300">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <uc1:frmCorrespondence ID="Correspondence1" runat="server" />
                                    </td>
                                </tr>
                            </table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Assets" runat="server">
            <div class="table-responsive">
                <asp:Table ID="Table2" CellPadding="0" CellSpacing="0" runat="server" Width="100%"
                    GridLines="None" Height="300">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <uc1:AssetList ID="AssetList" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Associations" runat="server">
            <div class="table-responsive">
                <asp:Table ID="tbl5" runat="server" Width="100%" BackColor="white" CellSpacing="0"
                    CellPadding="0" GridLines="None" Height="380">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                        <table width="100%">
                            <tr>
                                <td>
                                    <iframe id="ifAssociation" frameborder="0" width="100%" scrolling="auto"
                                        height="380"></iframe>
                                </td>
                            </tr>
                        </table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_VendorLeadTimes" runat="server">
            <!--#include file="~/common/VendorLeadTimes.html"-->
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <table width="100%">
    </table>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:TextBox ID="txtCorrTotalPage" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtCorrTotalRecords" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtContactType" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtContId" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtHidden" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtROwner" Style="display: none" runat="server">
    </asp:TextBox>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    <asp:Label ID="lblTitle" runat="server">Prospect Details</asp:Label>&nbsp;&nbsp;<asp:Label ID="lblOrganizationID" runat="server" ForeColor="Gray" />&nbsp;<a href="#" onclick="return OpenHelpPopUp('relationshipsdetail')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    <div class="btn btn-default btn-sm" runat="server" id="btnLayout">
        <span><i class="fa fa-columns"></i>&nbsp;&nbsp;Layout</span>
    </div>
    <asp:LinkButton runat="server" ID="lbtnDemote" CssClass="btn btn-primary btn-sm"><i class="fa fa-thumbs-down"></i>&nbsp;&nbsp; Demote to Lead
    </asp:LinkButton>
    <asp:LinkButton runat="server" ID="lbtnPromote" CssClass="btn btn-primary btn-sm"><i class="fa fa-thumbs-up"></i>&nbsp;&nbsp; Promote to Account 
    </asp:LinkButton>

    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:HiddenField ID="hfOwner" runat="server" />
    <asp:HiddenField ID="hfTerritory" runat="server" />
    <asp:HiddenField runat="server" ID="hdnDivisionID" />
    <telerik:RadWindow RenderMode="Lightweight" runat="server" ID="RadWinDocumentFiles" Title="Documents / Files" RestrictionZoneID="ContentTemplateZone" Modal="true" Behaviors="Resize,Close,Move" Width="800" Height="530">
        <ContentTemplate>
            <asp:UpdatePanel ID="Updatepanel3" runat="server" UpdateMode="Conditional" class="row" style="margin-right: 0px; margin-left: 0px">
                <ContentTemplate>
                    <br />
                    <%--<div class="row">--%>
                    <div class="col-xs-12 padbottom10">
                        <div class="pull-right">
                            <input type="button" id="btnCloseDocumentFiles" onclick="return CloseDocumentFiles('C');" class="btn btn-primary" style="color: white" value="Close" />
                        </div>
                    </div>
                    <%-- </div>
                    <div class="row">--%>
                    <div class="col-sm-6">
                        <h4>
                            <img src="../images/icons/Attach.png" height="24">
                            Upload a Document from your Desktop
                        </h4>
                        <hr style="color: #dcdcdc" />
                        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel1" ClientEvents-OnRequestStart="requestStart">
                            <div class="form-group">
                                <div class="custom-file">
                                    <input class="custom-file-input" id="fileupload" type="file" name="fileupload" runat="server">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Section</label>
                                <asp:DropDownList ID="ddlSectionFile" runat="server" CssClass="form-control"></asp:DropDownList>
                            </div>
                            <asp:Button ID="btnAttach" OnClick="btnAttach_Click" runat="server" Text="Attach & Close" CssClass="btn btn-primary"></asp:Button>
                            <asp:Button ID="btnLinkClose" OnClick="btnLinkClose_Click" runat="server" CssClass="hidden"></asp:Button>
                        </telerik:RadAjaxPanel>
                        <telerik:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server" InitialDelayTime="0" Skin="Default">
                        </telerik:RadAjaxLoadingPanel>
                    </div>

                    <div class="col-sm-6" id="divLinkUpload">
                        <h4>
                            <img src="../images/icons/Link.png">
                            Link to a Document</h4>
                        <hr style="color: #dcdcdc" />
                        <div class="form-group">
                            <label>Link Name</label>
                            <asp:TextBox runat="server" ID="txtLinkName" EmptyMessage="Link Name" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Link URL</label>
                            <asp:TextBox runat="server" ID="txtLinkURL" EmptyMessage="Paste link here" TextMode="MultiLine" CssClass="form-control" Rows="3"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Section</label>
                            <asp:DropDownList ID="ddlSectionLink" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                        <asp:Button ID="btnLink" runat="server" OnClientClick="return CloseDocumentFiles('L');" Text="Link & Close" CssClass="btn btn-primary"></asp:Button>
                        <asp:Label ID="Label2" Text="[?]" CssClass="tip" runat="server" ToolTip="Linking to a file on Google Drive or any other file repository is a good way to preserve your BizAutomation storage space. To avoid login credentials when clicking the  link, just make it public (e.g. On Google Drive you have to make sure that “Link Sharing” is set to “Public on the web”)." />
                    </div>
                    <%--</div>--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </telerik:RadWindow>
</asp:Content>


