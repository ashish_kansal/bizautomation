<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" MasterPageFile="~/common/GridMaster.Master"
    CodeBehind="frmProspectList.aspx.vb" Inherits="BACRM.UserInterface.Prospects.frmProspectList" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Prospect</title>
    
    <script language="javascript">
        function OpenSetting() {
            window.open('../Prospects/frmConfCompanyList.aspx?RelId=3&FormId=35', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                var RecordIDs = GetCheckedRowValues()
                document.getElementById('txtDelContactIds').value = RecordIDs;
                if (RecordIDs.length > 0)
                    return true
                else {
                    alert('Please select atleast one record!!');
                    return false
                }
            }
            else {
                return false;
            }
        }

        function OpenContact(a, b) {
            var str;
            str = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospectlist&ft6ty=oiuy&CntId=" + a + '&profileid=0';
            document.location.href = str;
        }

        function OpenWindow(a, b, c) {
            var str;
            if (b == 0) {
                str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospectlist&DivID=" + a + '&profileid=0';
            }
            else if (b == 1) {
                str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospectlist&DivID=" + a + '&profileid=0';
            }
            else if (b == 2) {
                str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospectlist&klds+7kldf=fjk-las&DivId=" + a + '&profileid=0';
            }

            document.location.href = str;
        }

        function GoImport() {
            window.location.href = "../admin/importfromputlook.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospects";
            return false;
        }

        function OpenActionItem(a) {
            window.open('../Leads/frmActionItemList.aspx?DivID=' + a + '', '', 'toolbar=no,titlebar=no,top=200,left=200,width=650,height=370,scrollbars=yes,resizable=yes')
        }

        function OpenEmail(a) {
            window.open('../Leads/frmOpenEmail.aspx?DivID=' + a + '', '', 'toolbar=no,titlebar=no,top=200,left=200,width=650,height=370,scrollbars=yes,resizable=yes')
        }
        function OrderSearchClientSelectedIndexChanged(sender, eventArgs) {
            $("[id$=btnGotoRecord]").click();
        }
        function openDrip(a) {
            window.open("../Marketing/frmConECampHstr.aspx?ConECampID=" + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=300,scrollbars=no,resizable=no')
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label>
                            View:</label>
                        <asp:DropDownList ID="ddlSort" runat="server" Style="width: 132px;" AutoPostBack="True" CssClass="form-control">
                            <asp:ListItem Value="3" Selected="True">My Prospects</asp:ListItem>
                            <asp:ListItem Value="4">All Prospects</asp:ListItem>
                            <asp:ListItem Value="6">Added in Last 7 days</asp:ListItem>
                            <asp:ListItem Value="7">Last 20 Added by me</asp:ListItem>
                            <asp:ListItem Value="8">Last 20 Modified by me</asp:ListItem>
                            <asp:ListItem Value="9">Favorites</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="pull-right">
                <div class="form-inline">
                    <asp:RadioButtonList ID="radActive" AutoPostBack="true" runat="server" style="margin-right:5px;"
                        RepeatDirection="Horizontal" RepeatLayout="Flow">
                        <asp:ListItem Selected="True" Value="True" class="flat-red">Active</asp:ListItem>
                        <asp:ListItem Value="False" class="minimal">In-Active</asp:ListItem>
                    </asp:RadioButtonList>
                    <telerik:RadComboBox ID="radCmbSearch" runat="server" DropDownWidth="600px" Width="250"
                        Skin="Default" AllowCustomText="false" ShowMoreResultsBox="true" EnableLoadOnDemand="true"
                        EnableScreenBoundaryDetection="true" ItemsPerRequest="10" EnableVirtualScrolling="true"
                        OnItemsRequested="radCmbSearch_ItemsRequested" HighlightTemplatedItems="true" ClientIDMode="Static"
                        TabIndex="503" MaxHeight="230" OnClientSelectedIndexChanged="OrderSearchClientSelectedIndexChanged" Style="display: none">
                    </telerik:RadComboBox>
                     <button id="btnAddNewProspect" onclick="return OpenPopUp('../include/frmAddOrganization.aspx?RelID=3&FormID=35');" class="btn btn-primary" ><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New</button>
                    <asp:LinkButton ID="btnAddActionItem" runat="server" CssClass="btn btn-primary" Visible="false"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Add Action Item</asp:LinkButton>
                    <asp:LinkButton ID="btnSendAnnounceMent" runat="server" CssClass="btn btn-primary" Visible="false"><i class="fa fa-envelope"></i>&nbsp;&nbsp;Send Announcement</asp:LinkButton>
                    <asp:LinkButton ID="btnGo" runat="server" CssClass="btn btn-primary" Style="display: none"><i class="fa fa-share-square-o"></i>&nbsp;&nbsp;Go</asp:LinkButton>
                    
                    <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
                    
                    <asp:UpdatePanel ID="updatepanel1" runat="server">
                        <ContentTemplate>
                            <asp:Button ID="Button1" style="display:none" OnClick="Button1_Click" runat="server" Text="Send" />
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="Button1" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:Button ID="btnGotoRecord" runat="server" Style="display: none;" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <asp:Literal ID="litMessage" runat="server"></asp:Literal>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lbProspects" runat="server"></asp:Label>&nbsp;<a href="#" onclick="return OpenHelpPopUp('relationships')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:Table ID="tblReport" runat="server" CssClass="table table-responsive table-bordered" EnableViewState="true"></asp:Table>
                <asp:GridView ID="gvSearch" runat="server" EnableViewState="true" AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered" Width="100%" ShowHeaderWhenEmpty="true" UseAccessibleHeader="true">
                    <Columns>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>

    <asp:TextBox ID="txtDelContactIds" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
    <asp:Button ID="btnGo1" runat="server" Style="display: none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
