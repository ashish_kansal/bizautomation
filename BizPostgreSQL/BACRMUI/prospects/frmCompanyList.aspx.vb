Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Prospects
    Public Class frmCompanyList : Inherits BACRMPage

        Dim strColumn As String
        Dim m_aryRightsForOulook() As Integer
        Dim RegularSearch As String
        Dim CustomSearch As String

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                GetUserRightsForPage(32, CCommon.ToInteger(GetQueryStringVal("RelId")))
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                If Not IsPostBack Then
                     ' = "Organization"
                    '  Session("RelId") = GetQueryStringVal( "RelId")
                    If GetQueryStringVal( "RelId") <> "" Then
                        Session("RelId") = GetQueryStringVal( "RelId")
                        txtRelId.Text = Session("RelId")
                    Else
                        txtRelId.Text = 0
                        Session("RelId") = 0
                    End If

                    Select Case txtRelId.Text
                        Case 1 'Lead
                            txtFormId.Text = "34"
                        Case 2 'Account
                            txtFormId.Text = "36"
                        Case 3 'Prospect
                            txtFormId.Text = "35"
                        Case Else 'Default Account
                            txtFormId.Text = "36"
                    End Select

                    btnAddNewAccount.Attributes.Add("onclick", "return OpenPopUp('../include/frmAddOrganization.aspx?RelID=" & txtRelId.Text & "&FormID=" & txtFormId.Text & "');")

                    objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))
                    If GetQueryStringVal( "FilterId") <> "" Then
                        If Not ddlFilter.Items.FindByValue(GetQueryStringVal( "FilterId")) Is Nothing Then
                            ddlFilter.ClearSelection()
                            ddlFilter.Items.FindByValue(GetQueryStringVal( "FilterId")).Selected = True
                        End If
                    End If
                    If GetQueryStringVal( "RelId") <> "" Then
                        If Not ddlRelationship.Items.FindByValue(GetQueryStringVal( "RelId")) Is Nothing Then
                            ddlRelationship.Items.FindByValue(GetQueryStringVal( "RelId")).Selected = True
                        End If
                    End If
                    LoadProfile()
                    If GetQueryStringVal("profileid") <> "" Then
                        If Not ddlProfile.Items.FindByValue(GetQueryStringVal("profileid")) Is Nothing Then
                            ddlProfile.Items.FindByValue(GetQueryStringVal("profileid")).Selected = True
                        End If
                    End If

                    'Dim ListDetails(8) As String

                    'ListDetails = GetFilterData() 'Session("ListDetails")
                    'If ListDetails IsNot Nothing AndAlso ListDetails.Length >= 8 Then
                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                        txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                        txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                        txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))
                        ddlFilter.ClearSelection()
                        If Not ddlFilter.Items.FindByValue(CCommon.ToString(PersistTable(PersistKey.FilterBy))) Is Nothing Then ddlFilter.Items.FindByValue(CCommon.ToString(PersistTable(PersistKey.FilterBy))).Selected = True
                        ddlProfile.ClearSelection()
                        If Not ddlProfile.Items.FindByValue(CCommon.ToString(PersistTable(ddlProfile.ID))) Is Nothing Then ddlProfile.Items.FindByValue(CCommon.ToString(PersistTable(ddlProfile.ID))).Selected = True
                        txtGridColumnFilter.Text = CCommon.ToString(PersistTable(PersistKey.GridColumnSearch))
                    End If
                    BindDatagrid()
                    'txtSortColumn.Text = "" 'bug ID 1629 #2
                    'txtSortChar.Text = "" 'bug ID 1629 #2
                End If
                ' Session("List") = "Organization"
                Dim m_aryRightsForSendAnnounceMent() As Integer = GetUserRightsForPage_Other(43, 142)

                If m_aryRightsForSendAnnounceMent(RIGHTSTYPE.VIEW) <> 0 Then
                    btnSendAnnounceMent.Visible = True
                End If
                Dim m_aryRightsForAddActionItem() As Integer = GetUserRightsForPage_Other(44, 148)

                If m_aryRightsForAddActionItem(RIGHTSTYPE.VIEW) <> 0 Then
                    btnAddActionItem.Visible = True
                End If
                If Page.IsPostBack Then

                    BindDatagrid()
                End If
                If ddlRelationship.SelectedIndex = 0 Then
                    lblRelationship.Text = "Organization"
                    ddlFilter.Items(1).Text = "My Organization"
                    ddlFilter.Items(2).Text = "All Organization"
                    ddlFilter.Items(3).Text = "Active Organization"
                    ddlFilter.Items(4).Text = "Inactive Organization"
                Else
                    lblRelationship.Text = ddlRelationship.SelectedItem.Text & "s"
                    ddlFilter.Items(1).Text = "My " & ddlRelationship.SelectedItem.Text & "s"
                    ddlFilter.Items(2).Text = "All " & ddlRelationship.SelectedItem.Text & "s"
                    ddlFilter.Items(3).Text = "Active " & ddlRelationship.SelectedItem.Text & "s"
                    ddlFilter.Items(4).Text = "Inactive " & ddlRelationship.SelectedItem.Text & "s"
                End If
                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                    btnDelete.Visible = False
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub


        Private Function GetCheckedValues() As Boolean
            Try
                Session("ContIDs") = ""
                Dim isCheckboxchecked As Boolean = False
                Dim gvRow As GridViewRow
                For Each gvRow In gvSearch.Rows
                    If CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = True Then
                        Session("ContIDs") = Session("ContIDs") & CType(gvRow.FindControl("lblContactId"), Label).Text & ","
                        isCheckboxchecked = True
                    End If
                Next
                Session("ContIDs") = Session("ContIDs").ToString.TrimEnd(",")
                Return isCheckboxchecked
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Private Sub btnAddActionItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddActionItem.Click
            Try
                Dim isCheckboxChecked As Boolean
                isCheckboxChecked = GetCheckedValues()
                If isCheckboxChecked Then
                    Response.Redirect("../Admin/ActionItemDetailsOld.aspx?selectedContactId=1", False)
                Else
                    DisplayError("Select atleast one checkbox For Action Item.")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Private Sub btnSendAnnounceMent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendAnnounceMent.Click
            Try
                Dim isCheckboxChecked As Boolean
                isCheckboxChecked = GetCheckedValues()
                If isCheckboxChecked Then
                    Session("EMailCampCondition") = Session("WhereCondition")
                    Response.Redirect("../Marketing/frmEmailBroadCasting.aspx?PC=true&SearchID=0&ID=0&SAll=true", False)
                Else
                    DisplayError("Select atleast one checkbox to broadcast E-mail.")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Sub LoadProfile()
            Try
                Dim dtTable As DataTable
                Dim objUserAccess As New UserAccess
                objUserAccess.RelID = ddlRelationship.SelectedItem.Value
                objUserAccess.DomainID = Session("DomainID")
                dtTable = objUserAccess.GetRelProfileD
                ddlProfile.DataSource = dtTable
                ddlProfile.DataTextField = "ProName"
                ddlProfile.DataValueField = "numProfileID"
                ddlProfile.DataBind()
                ddlProfile.Items.Insert(0, New ListItem("---Select One---", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Sub BindDatagrid(Optional ByVal CreateCol As Boolean = True)
            Try
                Dim dtCompanyList As DataTable
                Dim objAccounts As New CAccounts


                With objAccounts
                    .RelType = Session("RelId")
                    .UserCntID = Session("UserContactID")
                    .Profile = ddlProfile.SelectedItem.Value
                    .SortOrder = ddlFilter.SelectedItem.Value
                    .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                    .SortCharacter = txtSortChar.Text.Trim()
                    .DomainID = Session("DomainID")
                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()
                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0
                    If txtSortColumn.Text <> "" Then
                        .columnName = txtSortColumn.Text
                    Else : .columnName = "DM.bintcreateddate"
                    End If
                    'If ViewState("Column") <> "" Then
                    '    .columnName = ViewState("Column")
                    'Else : .columnName = "DM.bintcreateddate"
                    'End If
                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If

                    .FormId = txtFormId.Text

                    GridColumnSearchCriteria()

                    .RegularSearchCriteria = RegularSearch
                    .CustomSearchCriteria = CustomSearch
                End With


                ' Session("ListDetails") = ListDetails
                'Session("ListDetails") = SortChar & "," & txtFirstName.Text & "," & txtLastName.Text & "," & txtCustomer.Text & "," & txtCurrrentPage.Text & "," & txtSortColumn.Text & "," & Session("Asc") & "," & ddlFilter.SelectedValue & "," & ddlProfile.SelectedValue

                Dim dsList As DataSet

                dsList = objAccounts.GetCompanyList
                dtCompanyList = dsList.Tables(0)

                Dim dtTableInfo As DataTable
                dtTableInfo = dsList.Tables(1)

                bizPager.PageSize = Session("PagingRows")
                bizPager.RecordCount = objAccounts.TotalRecords
                bizPager.CurrentPageIndex = txtCurrrentPage.Text

                'If objAccounts.TotalRecords = 0 Then
                '    hidenav.Visible = False
                '    lblRecordCount.Text = 0
                'Else
                '    hidenav.Visible = True
                '    lblRecordCount.Text = String.Format("{0:#,###}", objAccounts.TotalRecords)
                '    Dim strTotalPage As String()
                '    Dim decTotalPage As Decimal
                '    decTotalPage = lblRecordCount.Text / Session("PagingRows")
                '    decTotalPage = Math.Round(decTotalPage, 2)
                '    strTotalPage = CStr(decTotalPage).Split(".")
                '    If (lblRecordCount.Text Mod Session("PagingRows")) = 0 Then
                '        lblTotal.Text = strTotalPage(0)
                '        txtTotalPage.Text = strTotalPage(0)
                '    Else
                '        lblTotal.Text = strTotalPage(0) + 1
                '        txtTotalPage.Text = strTotalPage(0) + 1
                '    End If
                '    txtTotalRecords.Text = lblRecordCount.Text
                'End If
                'Persist Form Settingsn
                PersistTable.Clear()
                PersistTable.Add(PersistKey.CurrentPage, IIf(dtCompanyList.Rows.Count > 0, txtCurrrentPage.Text, "1"))
                PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())
                PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
                PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text.Trim())
                PersistTable.Add(PersistKey.FilterBy, ddlFilter.SelectedValue)
                PersistTable.Add(ddlProfile.ID, ddlProfile.SelectedValue)
                PersistTable.Add(PersistKey.GridColumnSearch, txtGridColumnFilter.Text)
                PersistTable.Save()
                ''Mstart Pinkal Patel Date:26/11/2011
                'Dim ListDetails(8) As String
                'ListDetails(0) = SortChar
                'ListDetails(1) = txtFirstName.Text.Trim()
                'ListDetails(2) = txtLastName.Text.Trim()
                'ListDetails(3) = txtCustomer.Text.Trim()
                'ListDetails(4) = IIf(dtCompanyList.Rows.Count > 0, txtCurrrentPage.Text, "1")
                'ListDetails(5) = txtSortColumn.Text.Trim()
                'ListDetails(6) = Session("Asc")
                'ListDetails(7) = ddlFilter.SelectedItem.Value
                'ListDetails(8) = ddlProfile.SelectedItem.Value
                'SaveFilter(ListDetails)
                ''MEnd Pinkal Patel Date:26/11/2011
                Dim m_aryRightsForInlineEdit() As Integer = GetUserRightsForPage_Other(3, 4)

                Dim i As Integer

                For i = 0 To dtCompanyList.Columns.Count - 1
                    dtCompanyList.Columns(i).ColumnName = dtCompanyList.Columns(i).ColumnName.Replace(".", "")
                Next

                Dim htGridColumnSearch As New Hashtable

                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")
                    Dim strIDValue() As String

                    For i = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")

                        htGridColumnSearch.Add(strIDValue(0), strIDValue(1))
                    Next
                End If

                If CreateCol = True Then
                    Dim bField As BoundField
                    Dim Tfield As TemplateField

                    gvSearch.Columns.Clear()

                    For Each drRow As DataRow In dtTableInfo.Rows
                        If drRow("vcDbColumnName") = "numAssignedTo" Then
                            Dim m_aryRightsForAssignedTo() As Integer = GetUserRightsForPage_Other(3, 128)
                            If m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 3 Then
                                drRow("bitAllowEdit") = True
                            Else
                                drRow("bitAllowEdit") = False
                            End If
                        ElseIf drRow("vcDbColumnName") = "numBillState" Then
                            drRow("vcAssociatedControlType") = "TextBox"
                        ElseIf drRow("vcDbColumnName") = "numShipState" Then
                            drRow("vcAssociatedControlType") = "TextBox"
                        End If

                        Tfield = New TemplateField

                        Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, drRow, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, txtFormId.Text, objAccounts.columnName, objAccounts.columnSortOrder)

                        Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, drRow, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, txtFormId.Text, objAccounts.columnName, objAccounts.columnSortOrder)
                        gvSearch.Columns.Add(Tfield)
                    Next

                    Dim dr As DataRow
                    dr = dtTableInfo.NewRow()
                    dr("vcAssociatedControlType") = "DeleteCheckBox"
                    dr("intColumnWidth") = "30"

                    Tfield = New TemplateField
                    Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, dr, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, txtFormId.Text)
                    Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, dr, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, txtFormId.Text)
                    gvSearch.Columns.Add(Tfield)
                End If

                gvSearch.DataSource = dtCompanyList
                gvSearch.DataBind()

                Dim objPageControls As New PageControls
                Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtTableInfo)
                ClientScript.RegisterClientScriptBlock(Me.GetType, "InlineEditValidation", strValidation, True)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        'Public Function GetFilterData() As String()
        '    Try
        '        Dim objContact As New CContacts
        '        objContact.DomainID = Session("DomainId")
        '        objContact.UserCntID = Session("UserContactID")
        '        objContact.FormId = txtFormId.Text
        '        objContact.ContactType = txtRelId.Text
        '        objContact.GroupID = ddlProfile.SelectedValue 'IIf(GetQueryStringVal( "ContactType") Is Nothing, 0, GetQueryStringVal( "ContactType"))
        '        Dim dt As DataTable = objContact.GetDefaultfilter()
        '        Dim ListDetails As String()
        '        If dt.Rows.Count = 1 Then
        '            ListDetails = CCommon.ToString(dt.Rows(0)("vcFilter")).Split("~")
        '            If Not ddlFilter.Items.FindByValue(dt.Rows(0).Item("numfilterId")) Is Nothing Then
        '                ddlFilter.ClearSelection()
        '                ddlFilter.Items.FindByValue(dt.Rows(0).Item("numfilterId")).Selected = True
        '            End If
        '        End If
        '        Return ListDetails
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Function
        'Private Sub SaveFilter(ByVal ListDetails As String())
        '    Try
        '        'IStart Pinkal Patel Date:03-11-2011
        '        Dim strFilter As New System.Text.StringBuilder
        '        Dim objContact As New CContacts
        '        objContact.DomainID = Session("DomainID")
        '        objContact.UserCntID = Session("UserContactID")
        '        objContact.ContactType = txtRelId.Text
        '        objContact.bitDefault = False
        '        objContact.blnFlag = True
        '        objContact.FormId = txtFormId.Text
        '        objContact.GroupID = IIf(GetQueryStringVal("profileid") <> "", GetQueryStringVal("profileid"), 0) 'ddlProfile.SelectedValue
        '        objContact.FilterID = ddlFilter.SelectedItem.Value
        '        strFilter.Append(ListDetails(0))
        '        strFilter.Append("~")
        '        strFilter.Append(ListDetails(1))
        '        strFilter.Append("~")
        '        strFilter.Append(ListDetails(2))
        '        strFilter.Append("~")
        '        strFilter.Append(ListDetails(3))
        '        strFilter.Append("~")
        '        strFilter.Append(ListDetails(4))
        '        strFilter.Append("~")
        '        strFilter.Append(ListDetails(5))
        '        strFilter.Append("~")
        '        strFilter.Append(ListDetails(6))
        '        strFilter.Append("~")
        '        strFilter.Append(ListDetails(7))
        '        strFilter.Append("~")
        '        strFilter.Append(ListDetails(8))
        '        objContact.strFilter = strFilter.ToString
        '        objContact.SaveDefaultFilter()
        '        'objContact.strFilter = 
        '        'IEnd Pinkal Patel Date:03-11-2011
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlRelationship_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRelationship.SelectedIndexChanged
            Try
                Session("RelId") = ddlRelationship.SelectedValue
                LoadProfile()
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlFilter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilter.SelectedIndexChanged
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlProfile_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProfile.SelectedIndexChanged
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
            Try
                Dim strDivid As String() = txtDelContactIds.Text.Split(",")
                Dim i As Int16 = 0
                Dim lngDivID As Long
                Dim lngRecOwnID As Long
                Dim lngTerrID As Long
                Dim objAccount As New CAccounts
                For i = 0 To strDivid.Length - 1
                    lngDivID = strDivid(i).Split("~")(0)
                    lngRecOwnID = strDivid(i).Split("~")(1)
                    lngTerrID = strDivid(i).Split("~")(2)
                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 1 Then
                        Try
                            If lngRecOwnID = Session("UserContactID") Then
                                DeleteOrganization(lngDivID, objAccount)
                            End If
                        Catch ex As Exception
                            litMessage.Text = "Vendors cannot be deleted."
                        End Try
                    ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 2 Then
                        Try
                            Dim j As Integer
                            Dim dtTerritory As New DataTable
                            dtTerritory = Session("UserTerritory")
                            Dim chkDelete As Boolean = False
                            If lngTerrID = 0 Then
                                chkDelete = False
                            Else
                                For j = 0 To dtTerritory.Rows.Count - 1
                                    If lngTerrID = dtTerritory.Rows(j).Item("numTerritoryId") Then chkDelete = True
                                Next
                            End If
                            If chkDelete = True Then
                                DeleteOrganization(lngDivID, objAccount)
                            End If
                        Catch ex As Exception
                            litMessage.Text = "Vendor cannot be deleted."
                        End Try
                    ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 3 Then
                        Try
                            DeleteOrganization(lngDivID, objAccount)
                        Catch ex As Exception
                            litMessage.Text = "Vendors cannot be deleted."
                        End Try
                    End If
                Next
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)

            End Try
        End Sub
        Private Sub DeleteOrganization(ByVal lngDivID As Long, ByVal objAccount As CAccounts)
            Try
                If ddlFilter.SelectedItem.Value = 9 Then
                    Dim objContacts As New CContacts
                    objContacts.byteMode = 1
                    objContacts.UserCntID = Session("UserContactID")
                    objContacts.ContactID = lngDivID '' passing division id instead of contact if for deleting compnay from favorites
                    objContacts.ManageFavorites()
                    litMessage.Text = "Deleted from Favorites"
                Else
                    If lngDivID <> Session("UserDivisionID") Then
                        With objAccount
                            .DivisionID = lngDivID
                            .DomainID = Session("DomainID")
                        End With
                        If objAccount.DeleteOrg.Length > 2 Then litMessage.Text = "Some Companies cannot be deleted."
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub GridColumnSearchCriteria()
            Try
                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")

                    Dim strIDValue(), strID(), strCustom As String
                    Dim strRegularCondition As New ArrayList
                    Dim strCustomCondition As New ArrayList


                    For i As Integer = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")
                        strID = strIDValue(0).Split("~")

                        If strID(0).Contains("CFW.Cust") Then
                            Select Case strID(3).Trim()
                                Case "TextBox", "TextArea"
                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & " ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                Case "CheckBox"
                                    If strIDValue(1).ToLower() = "yes" Then
                                        strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and COALESCE(CFW.Fld_Value,'0') " & "='1')")
                                    ElseIf strIDValue(1).ToLower() = "no" Then
                                        strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND 1 = (CASE WHEN (SELECT COUNT(*) FROM CFW_Fld_Values CFWInner WHERE CFWInner.RecId=DivisionMaster.numDivisionID AND CFWInner.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND CFWInner.Fld_Value='1') > 0 THEN 0 ELSE 1 END))")
                                    End If
                                Case "SelectBox"
                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & "=" & strIDValue(1) & ")")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " >= '" & fromDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " <= '" & toDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    End If
                                Case "CheckBoxList"
                                    Dim items As String() = strIDValue(1).Split(",")
                                    Dim searchString As String = ""

                                    For Each item As String In items
                                        searchString = searchString & If(searchString.Length > 0, " OR ", "") & " fn_GetCustFldStringValue(" & strID(0).Replace("CFW.Cust", "") & ",DivisionMaster.numDivisionID,CFW.Fld_Value) " & " ilike '%" & item.Replace("'", "''") & "%'"
                                    Next

                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND (" & searchString & "))")
                                Case Else
                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & strCustom & ")")
                            End Select
                        Else
                            Select Case strID(3).Trim()
                                Case "Website", "Email", "TextBox"
                                    If strID(0) = "AD.numShipState" Then
                                        strRegularCondition.Add("ShipState.vcState ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                    ElseIf strID(0) = "ADC.vcCompactContactDetails" Then
                                        strRegularCondition.Add("(ADC.vcFirstName ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR ADC.vcLastName ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR ADC.numPhone ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR ADC.numPhoneExtension ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                    ElseIf strID(0) = "AD.numBillState" Then
                                        strRegularCondition.Add("BillState.vcState ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                    Else
                                        strRegularCondition.Add(strID(0) & " ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                    End If
                                Case "SelectBox"
                                    If strID(0).Contains("numFollowUpStatus") Then
                                        strRegularCondition.Add(strID(0) & " IN(" & strIDValue(1) & ")")
                                    Else
                                        strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                                    End If
                                Case "TextArea"
                                    strRegularCondition.Add(" Cast(" & strID(0) & " as varchar(5000)) ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strRegularCondition.Add(strID(0) & " >= '" & fromDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strRegularCondition.Add(strID(0) & " <= '" & toDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    End If
                            End Select
                        End If
                    Next
                    RegularSearch = String.Join(" and ", strRegularCondition.ToArray())
                    CustomSearch = String.Join(" and ", strCustomCondition.ToArray())
                Else
                    RegularSearch = ""
                    CustomSearch = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
    End Class

End Namespace