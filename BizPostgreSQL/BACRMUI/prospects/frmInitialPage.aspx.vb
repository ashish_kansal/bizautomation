Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin

Namespace BACRM.UserInterface.Prospects
    Partial Public Class frmInitialPage : Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            
            Try
                If Not IsPostBack Then


                    Dim objUserGroups As New CShortCutBar
                    objUserGroups.GroupId = Session("UserGroupID")
                    objUserGroups.DomainId = Session("DomainID")
                    objUserGroups.ContactId = Session("UserContactID")
                    objUserGroups.TabId = CCommon.ToLong(GetQueryStringVal( "Tab"))

                    'If GetQueryStringVal( "frm") = "opp" Then
                    '    objAccount.FormId = 14
                    'Else : objAccount.FormId = 0
                    'End If
                    Dim ds As DataSet

                    ds = objUserGroups.GetInitialPageDetails()
                    If ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        Response.Redirect(Replace(ds.Tables(0).Rows(0).Item("Link"), "RecordID", Session("UserContactID")), False)

                        '    Dim FormId As Short = dtTable.Rows(0).Item("numFormId")
                        '    Dim Relationid As Long = dtTable.Rows(0).Item("numRelationId")
                        '    Dim filterId As Long = dtTable.Rows(0).Item("numFilterId")
                        'Dim strRedirect As String = ""

                        '    If FormId = 10 Then
                        '        strRedirect = "../contact/frmContactList.aspx?ContactType=" & Relationid & "&srt=" & filterId
                        '    ElseIf FormId = 34 Or FormId = 35 Or FormId = 36 Then
                        '        If Relationid = 1 Then
                        '            strRedirect = "../Leads/frmLeadList.aspx?FilterId=" & filterId & "&GrpId=" & dtTable.Rows(0).Item("tintGroupId")
                        '        ElseIf Relationid = 3 Then
                        '            strRedirect = "../prospects/frmProspectList.aspx?FilterId=" & filterId
                        '        ElseIf Relationid = 2 Then
                        '            strRedirect = "../Account/frmAccountList.aspx?FilterId=" & filterId
                        '        Else : strRedirect = "../prospects/frmCompanyList.aspx?RelId=" & Relationid & "&FilterId=" & filterId
                        '        End If
                        '    ElseIf FormId = 38 Or FormId = 39 Or FormId = 40 Or FormId = 41 Then
                        '        If Relationid = 38 Then
                        '            strRedirect = "../Opportunity/frmOpportunityList.aspx?SI=0"
                        '        ElseIf Relationid = 40 Then
                        '            strRedirect = "../Opportunity/frmOpportunityList.aspx?SI=1"
                        '        ElseIf Relationid = 39 Then
                        '            strRedirect = "../Opportunity/frmDealList.aspx?type=1"
                        '        ElseIf Relationid = 41 Then
                        '            strRedirect = "../Opportunity/frmDealList.aspx?type=2"
                        '        End If
                        '    End If
                        'If strRedirect <> "" Then Response.Redirect(strRedirect, False)
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
