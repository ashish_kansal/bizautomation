Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common
Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Admin
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.CustomReports
Imports System.Text
Imports System.IO

Namespace BACRM.UserInterface.Prospects
    Public Class frmProspectList
        Inherits BACRMPage

        Dim RegularSearch As String
        Dim CustomSearch As String
        Dim m_aryRightsForViewGridConfiguration() As Integer
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                GetUserRightsForPage(3, 2)
                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnDelete.Visible = False
                If Not IsPostBack Then
                    Dim m_aryRightsForSendAnnounceMent() As Integer = GetUserRightsForPage_Other(43, 141)

                    If m_aryRightsForSendAnnounceMent(RIGHTSTYPE.VIEW) <> 0 Then
                        btnSendAnnounceMent.Visible = True
                    End If
                    Dim m_aryRightsForAddActionItem() As Integer = GetUserRightsForPage_Other(44, 147)

                    If m_aryRightsForAddActionItem(RIGHTSTYPE.VIEW) <> 0 Then
                        btnAddActionItem.Visible = True
                    End If
                    DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                    DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                    Dim dtTab As New DataTable
                    dtTab = Session("DefaultTab")
                    If dtTab.Rows.Count > 0 Then
                        lbProspects.Text = IIf(IsDBNull(dtTab.Rows(0).Item("vcProspect")), "Prospects", dtTab.Rows(0).Item("vcProspect").ToString & "s")
                    Else : lbProspects.Text = "Prospects"
                    End If
                    If GetQueryStringVal("FilterId") <> "" Then
                        If Not ddlSort.Items.FindByValue(GetQueryStringVal("FilterId")) Is Nothing Then
                            ddlSort.ClearSelection()
                            ddlSort.Items.FindByValue(GetQueryStringVal("FilterId")).Selected = True
                        End If
                    End If

                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                        btnDelete.Visible = False
                    End If
                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                        txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                        txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                        txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))
                        If Not ddlSort.Items.FindByValue(CCommon.ToString(PersistTable(PersistKey.FilterBy))) Is Nothing Then
                            ddlSort.ClearSelection()
                            ddlSort.Items.FindByValue(CCommon.ToString(PersistTable(PersistKey.FilterBy))).Selected = True
                            'Else
                            '    SetDefaultFilter()
                        End If
                        txtGridColumnFilter.Text = CCommon.ToString(PersistTable(PersistKey.GridColumnSearch))
                    End If

                    'Check if user has rights to edit grid configuration
                    m_aryRightsForViewGridConfiguration = GetUserRightsForPage_Other(3, 15)
                    If m_aryRightsForViewGridConfiguration(RIGHTSTYPE.VIEW) = 0 Then
                        Dim tdGridConfiguration As HtmlAnchor = CType(CCommon.FindControlRecursive(Page.Master, "tdGridConfiguration"), HtmlAnchor)

                        If Not tdGridConfiguration Is Nothing Then
                            tdGridConfiguration.Visible = False
                        End If
                    End If


                    BindDatagrid()
                End If
                If Page.IsPostBack Then

                    BindDatagrid()
                End If
                'ClientScript.RegisterClientScriptBlock(Me.GetType, "SetTab", "if (parent.parent.frames.length > 0) { parent.parent.SelectTabByValue('7');}else{ parent.SelectTabByValue('7'); } ", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Function GetCheckedValues() As Boolean
            Try
                Session("ContIDs") = ""
                Dim isCheckboxchecked As Boolean = False
                Dim gvRow As GridViewRow
                For Each gvRow In gvSearch.Rows
                    If CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = True Then
                        Session("ContIDs") = Session("ContIDs") & CType(gvRow.FindControl("lblContactId"), Label).Text & ","
                        isCheckboxchecked = True
                    End If
                Next
                Session("ContIDs") = Session("ContIDs").ToString.TrimEnd(",")
                Return isCheckboxchecked
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Private Sub btnAddActionItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddActionItem.Click
            Try
                Dim isCheckboxChecked As Boolean
                isCheckboxChecked = GetCheckedValues()
                If isCheckboxChecked Then
                    Response.Redirect("../Admin/ActionItemDetailsOld.aspx?selectedContactId=1", False)
                Else
                    DisplayError("Select atleast one checkbox For Action Item.")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Private Sub btnSendAnnounceMent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendAnnounceMent.Click
            Try
                Dim isCheckboxChecked As Boolean
                isCheckboxChecked = GetCheckedValues()
                If isCheckboxChecked Then
                    Session("EMailCampCondition") = Session("WhereCondition")
                    Response.Redirect("../Marketing/frmEmailBroadCasting.aspx?PC=true&SearchID=0&ID=0&SAll=true", False)
                Else
                    DisplayError("Select atleast one checkbox to broadcast E-mail.")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Sub BindDatagrid(Optional ByVal CreateCol As Boolean = True)
            Try
                Dim dtAccounts As DataTable
                Dim objAccounts As New CAccounts


                With objAccounts
                    .CRMType = 1
                    .UserCntID = Session("UserContactID")
                    .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                    .SortOrder = ddlSort.SelectedItem.Value
                    '.FirstName = txtFirstName.Text.Trim()
                    '.LastName = txtLastName.Text.Trim()
                    .SortCharacter = txtSortChar.Text.Trim()
                    '.CustName = txtCustomer.Text.Trim()
                    .DomainID = Session("DomainID")
                    .Profile = CCommon.ToLong(GetQueryStringVal("numProfile"))
                    .bitPartner = False
                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()
                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0
                    If txtSortColumn.Text <> "" Then
                        .columnName = IIf(txtSortColumn.Text.ToLower = "bintcreateddate", "DM.bintcreateddate", txtSortColumn.Text)
                    Else : .columnName = "DM.bintcreateddate"
                    End If

                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If

                    .ActiveInActive = radActive.SelectedValue

                    GridColumnSearchCriteria()

                    .RegularSearchCriteria = RegularSearch
                    .SearchTxt = radCmbSearch.Text
                    .CustomSearchCriteria = CustomSearch
                End With


                'Session("ListDetails") = SortChar & "," & txtFirstName.Text & "," & txtLastName.Text & "," & txtCustomer.Text & "," & txtCurrrentPage.Text & "," & txtSortColumn.Text & "," & Session("Asc")
                objAccounts.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                Dim dsList As DataSet

                dsList = objAccounts.GetProspects
                dtAccounts = dsList.Tables(0)

                bizPager.PageSize = Session("PagingRows")
                bizPager.RecordCount = objAccounts.TotalRecords
                bizPager.CurrentPageIndex = txtCurrrentPage.Text

                'Persist Form Settings
                PersistTable.Clear()
                PersistTable.Add(PersistKey.CurrentPage, IIf(dtAccounts.Rows.Count > 0, txtCurrrentPage.Text, "1"))
                PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())
                PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
                PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text.Trim())
                PersistTable.Add(PersistKey.FilterBy, ddlSort.SelectedValue)
                PersistTable.Add(PersistKey.GridColumnSearch, txtGridColumnFilter.Text)
                PersistTable.Save()

                Dim dtTableInfo As DataTable
                dtTableInfo = dsList.Tables(1)

                Dim i As Integer

                For i = 0 To dtAccounts.Columns.Count - 1
                    dtAccounts.Columns(i).ColumnName = dtAccounts.Columns(i).ColumnName.Replace(".", "")
                Next

                Dim htGridColumnSearch As New Hashtable

                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")
                    Dim strIDValue() As String

                    For i = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")

                        htGridColumnSearch.Add(strIDValue(0), strIDValue(1))
                    Next
                End If

                Dim m_aryRightsForInlineEdit() As Integer = GetUserRightsForPage_Other(3, 4)

                If CreateCol = True Then
                    Dim bField As BoundField
                    Dim Tfield As TemplateField

                    gvSearch.Columns.Clear()

                    For Each drRow As DataRow In dtTableInfo.Rows
                        If drRow("vcDbColumnName") = "numAssignedTo" Then
                            Dim m_aryRightsForAssignedTo() As Integer = GetUserRightsForPage_Other(3, 128)
                            If m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 3 Then
                                drRow("bitAllowEdit") = True
                            Else
                                drRow("bitAllowEdit") = False
                            End If
                        ElseIf drRow("vcDbColumnName") = "numBillState" Then
                            drRow("vcAssociatedControlType") = "TextBox"
                        ElseIf drRow("vcDbColumnName") = "numShipState" Then
                            drRow("vcAssociatedControlType") = "TextBox"
                        End If

                        Tfield = New TemplateField

                        Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, drRow, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 35, objAccounts.columnName, objAccounts.columnSortOrder)

                        Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, drRow, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 35, objAccounts.columnName, objAccounts.columnSortOrder)
                        gvSearch.Columns.Add(Tfield)
                    Next

                    Dim dr As DataRow
                    dr = dtTableInfo.NewRow()
                    dr("vcAssociatedControlType") = "DeleteCheckBox"
                    dr("intColumnWidth") = "30"

                    Tfield = New TemplateField
                    Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, dr, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 35)
                    Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, dr, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 35)
                    gvSearch.Columns.Add(Tfield)
                End If
                InitializeSearchClientSideTemplate(dsList.Tables(1).AsEnumerable().Take(3).CopyToDataTable())
                gvSearch.DataSource = dtAccounts
                gvSearch.DataBind()

                Dim objPageControls As New PageControls
                Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtTableInfo)
                ClientScript.RegisterClientScriptBlock(Me.GetType, "InlineEditValidation", strValidation, True)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub InitializeSearchClientSideTemplate(dt As DataTable)
            Try
                'objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                'objCommon.UserCntID = CCommon.ToLong(Session("UserContactID"))
                'Dim ds As DataSet = objCommon.SearchOrders("", ddlSort.SelectedValue, IIf(ddlSort.SelectedValue = "1", m_aryRightsForPage(RIGHTSTYPE.VIEW), m_aryRightsForPage(RIGHTSTYPE.VIEW)), 0, 2)

                'If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                '    Dim dtTable As New DataTable
                '    dtTable = ds.Tables(0)

                'genrate header template dynamically
                radCmbSearch.HeaderTemplate = New SearchTemplate(ListItemType.Header, dt, DropDownWidth:=radCmbSearch.DropDownWidth.Value)

                'generate Clientside Template dynamically
                radCmbSearch.ItemTemplate = New SearchTemplate(ListItemType.Item, dt, DropDownWidth:=radCmbSearch.DropDownWidth.Value)
                'End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub


#Region "RadComboBox Template"
        Public Class SearchTemplate
            Implements ITemplate

            Dim ItemTemplateType As ListItemType
            Dim dtTable1 As DataTable
            Dim i As Integer = 0
            Dim _DropDownWidth As Double

            Sub New(ByVal type As ListItemType, ByVal dtTable As DataTable, Optional ByVal DropDownWidth As Double = 600)
                Try
                    ItemTemplateType = type
                    dtTable1 = dtTable
                    _DropDownWidth = DropDownWidth
                Catch ex As Exception
                    Throw ex
                End Try
            End Sub

            Public Sub InstantiateIn(ByVal container As Control) Implements ITemplate.InstantiateIn
                Try


                    i = 0
                    Dim label1 As Label
                    Dim label2 As Label
                    Dim img As System.Web.UI.WebControls.Image
                    Dim table1 As New HtmlTable
                    Dim tblCell As HtmlTableCell
                    Dim tblRow As HtmlTableRow
                    table1.Width = "100%"
                    Dim ul As New HtmlGenericControl("ul")

                    Select Case ItemTemplateType
                        Case ListItemType.Header
                            For Each dr As DataRow In dtTable1.Rows
                                Dim li As New HtmlGenericControl("li")
                                li.Style.Add("width", Unit.Pixel(CCommon.ToInteger((_DropDownWidth - 50) / dtTable1.Rows.Count)).Value & "px")
                                li.Style.Add("float", "left")
                                li.Style.Add("text-align", "center")
                                li.Style.Add("font-weight", "bold")
                                li.InnerText = dr("vcFieldName").ToString
                                ul.Controls.Add(li)
                            Next

                            container.Controls.Add(ul)
                        Case ListItemType.Item
                            For Each dr As DataRow In dtTable1.Rows
                                Dim li As New HtmlGenericControl("li")
                                Dim width As Integer = CCommon.ToInteger((_DropDownWidth - 50) / dtTable1.Rows.Count)
                                li.Style.Add("width", Unit.Pixel(width).Value & "px")
                                li.Style.Add("float", "left")

                                If dr("bitCustomField") = "1" Then
                                    label2 = New Label
                                    label2.CssClass = "normal1"
                                    label2.Attributes.Add("style", "display:inline-block;width:" & width)
                                    AddHandler label2.DataBinding, AddressOf label2_DataBinding
                                    li.Controls.Add(label2)
                                Else
                                    label1 = New Label
                                    label1.CssClass = "normal1"
                                    label1.Attributes.Add("style", "display:inline-block;width:" & width)
                                    AddHandler label1.DataBinding, AddressOf label1_DataBinding
                                    li.Controls.Add(label1)
                                End If

                                ul.Controls.Add(li)
                            Next

                            container.Controls.Add(ul)
                    End Select
                Catch ex As Exception
                    Throw
                End Try
            End Sub

            Private Sub label1_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
                Dim target As Label = CType(sender, Label)
                Dim item As RadComboBoxItem = CType(target.NamingContainer, RadComboBoxItem)
                Dim itemText As String = Convert.ToString(DirectCast(item.DataItem, DataRow)(dtTable1.Rows(i).Item("vcDbColumnName") & "~" & dtTable1.Rows(i).Item("numFieldId") & "~" & CCommon.ToInteger(dtTable1.Rows(i).Item("bitCustomField"))))
                'Dim itemText As String = CType(DataBinder.Eval(item.DataItem, dtTable1.Rows(i).Item("vcDbColumnName").ToString()), String)
                target.Text = itemText
                i = i + 1
            End Sub

            Private Sub label2_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
                Dim target As Label = CType(sender, Label)
                Dim item As RadComboBoxItem = CType(target.NamingContainer, RadComboBoxItem)
                Dim itemText As String = Convert.ToString(DirectCast(item.DataItem, DataRow)(dtTable1.Rows(i).Item("vcDbColumnName") & "~" & dtTable1.Rows(i).Item("numFieldId") & "~" & CCommon.ToInteger(dtTable1.Rows(i).Item("bitCustomField"))))
                'Dim itemText As String = CType(DataBinder.Eval(item.DataItem, dtTable1.Rows(i).Item("vcFieldName").ToString()), String)
                target.Text = itemText
                i = i + 1
            End Sub

        End Class
#End Region
        Protected Sub radCmbSearch_ItemsRequested(sender As Object, e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs)
            Try
                If e.Text.Trim().Length > 0 Then
                    Dim dtAccounts As DataTable
                    Dim objAccounts As New CAccounts
                    Dim searchCriteria As String
                    Dim itemOffset As Integer = e.NumberOfItems

                    objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                    objCommon.UserCntID = CCommon.ToLong(Session("UserContactID"))


                    With objAccounts
                        .CRMType = 1
                        .UserCntID = Session("UserContactID")
                        .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                        .SortOrder = ddlSort.SelectedItem.Value
                        '.FirstName = txtFirstName.Text.Trim()
                        '.LastName = txtLastName.Text.Trim()
                        .SortCharacter = txtSortChar.Text.Trim()
                        '.CustName = txtCustomer.Text.Trim()
                        .DomainID = Session("DomainID")
                        .bitPartner = False
                        If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                        .CurrentPage = txtCurrrentPage.Text.Trim()
                        .PageSize = Session("PagingRows")
                        .TotalRecords = 0
                        If txtSortColumn.Text <> "" Then
                            .columnName = IIf(txtSortColumn.Text.ToLower = "bintcreateddate", "DM.bintcreateddate", txtSortColumn.Text)
                        Else : .columnName = "DM.bintcreateddate"
                        End If

                        If txtSortOrder.Text = "D" Then
                            .columnSortOrder = "Desc"
                        Else : .columnSortOrder = "Asc"
                        End If

                        .ActiveInActive = radActive.SelectedValue

                        GridColumnSearchCriteria()

                        .RegularSearchCriteria = RegularSearch
                        .SearchTxt = e.Text.Trim()
                        .CustomSearchCriteria = CustomSearch
                    End With

                    'btnDelete.Visible = IIf(ddlProjectStatus.SelectedValue = 27492, False, True)

                    Dim dsList As DataSet

                    dsList = objAccounts.GetProspects
                    dtAccounts = dsList.Tables(0)

                    InitializeSearchClientSideTemplate(dsList.Tables(1).AsEnumerable().Take(3).CopyToDataTable())

                    If Not dsList Is Nothing AndAlso dsList.Tables.Count > 0 Then
                        Dim endOffset As Integer

                        endOffset = Math.Min(itemOffset + radCmbSearch.ItemsPerRequest, dtAccounts.Rows.Count)
                        e.EndOfItems = endOffset = dtAccounts.Rows.Count
                        e.Message = IIf(dtAccounts.Rows.Count <= 0, "No matches", String.Format("Prospects <b>1</b>-<b>{0}</b> out of <b>{1}</b>", endOffset, dtAccounts.Rows.Count))

                        For Each dr As DataRow In dtAccounts.Rows
                            Dim item As New RadComboBoxItem()
                            item.Text = DirectCast(dr("vcCompanyName"), String)
                            item.Value = dr("numDivisionID").ToString() & "/" & dr("tintCRMType").ToString()
                            item.Attributes.Add("orderType", ddlSort.SelectedValue)

                            item.DataItem = dr
                            radCmbSearch.Items.Add(item)
                            item.DataBind()
                        Next
                    End If
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                e.Message = "<b style=""color:Red"">Error occured while searching.</b>"
            End Try
        End Sub
        Private Sub btnGotoRecord_Click(sender As Object, e As EventArgs) Handles btnGotoRecord.Click
            Try
                Dim profileid As Int32
                Dim data As String()
                data = radCmbSearch.SelectedValue.Split("/")
                profileid = 0
                If data(1) = "0" Then
                    Response.Redirect("~/Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=accountlistDivID=" & data(0) & "&profileid=" & profileid, False)
                ElseIf data(1) = "1" Then
                    Response.Redirect("~/prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=accountlist&DivID=" & data(0) & "&profileid=" & profileid, False)
                ElseIf data(1) = "2" Then
                    Response.Redirect("~/account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=accountlist&klds+7kldf=fjk-las&DivId=" & data(0) & "&profileid=" & profileid, False)
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlSort_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSort.SelectedIndexChanged
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
                Session("Sprospects") = ddlSort.SelectedIndex
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
            Try
                Dim strDivid As String() = txtDelContactIds.Text.Split(",")
                Dim i As Int16 = 0
                Dim lngDivID As Long
                Dim lngRecOwnID As Long
                Dim lngTerrID As Long
                For i = 0 To strDivid.Length - 1
                    lngDivID = strDivid(i).Split("~")(0)
                    lngRecOwnID = strDivid(i).Split("~")(1)
                    lngTerrID = strDivid(i).Split("~")(2)
                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 1 Then
                        Try
                            If lngRecOwnID = Session("UserContactID") Then
                                DeleteOrganization(lngDivID)
                            End If
                        Catch ex As Exception

                        End Try
                    ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 2 Then
                        Try
                            Dim j As Integer
                            Dim dtTerritory As New DataTable
                            dtTerritory = Session("UserTerritory")
                            Dim chkDelete As Boolean = False
                            If lngTerrID = 0 Then
                                chkDelete = False
                            Else
                                For j = 0 To dtTerritory.Rows.Count - 1
                                    If lngTerrID = dtTerritory.Rows(j).Item("numTerritoryId") Then chkDelete = True
                                Next
                            End If
                            If chkDelete = True Then
                                DeleteOrganization(lngDivID)
                            End If
                        Catch ex As Exception

                        End Try
                    ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 3 Then
                        DeleteOrganization(lngDivID)
                    End If
                Next
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub DeleteOrganization(ByVal lngDivID As Long)
            Try
                If ddlSort.SelectedItem.Value = 9 Then
                    Dim objContacts As New CContacts
                    objContacts.byteMode = 1
                    objContacts.UserCntID = Session("UserContactID")
                    objContacts.ContactID = lngDivID '' passing division id instead of contact if for deleting compnay from favorites
                    objContacts.ManageFavorites()
                    litMessage.Text = "Deleted from Favorites"
                Else
                    If lngDivID <> Session("UserDivisionID") Then
                        Dim objAccount As New CAccounts
                        With objAccount
                            .DivisionID = lngDivID
                            .UserCntID = Session("UserContactID")
                            .DomainID = Session("DomainID")
                        End With
                        If objAccount.DeleteOrg.Length > 2 Then litMessage.Text = "Some Companies cannot be deleted."
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                radCmbSearch.ClearSelection()
                radCmbSearch.Text = ""
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub radActive_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radActive.SelectedIndexChanged
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub GridColumnSearchCriteria()
            Try
                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")

                    Dim strIDValue(), strID(), strCustom As String
                    Dim strRegularCondition As New ArrayList
                    Dim strCustomCondition As New ArrayList


                    For i As Integer = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")
                        strID = strIDValue(0).Split("~")

                        If strID(0).Contains("CFW.Cust") Then
                            Select Case strID(3).Trim()
                                Case "TextBox", "TextArea"
                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & " ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                Case "CheckBox"
                                    If strIDValue(1).ToLower() = "yes" Then
                                        strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and COALESCE(CFW.Fld_Value,'0') " & "='1')")
                                    ElseIf strIDValue(1).ToLower() = "no" Then
                                        strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND 1 = (CASE WHEN (SELECT COUNT(*) FROM CFW_Fld_Values CFWInner WHERE CFWInner.RecId=DivisionMaster.numDivisionID AND CFWInner.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND CFWInner.Fld_Value='1') > 0 THEN 0 ELSE 1 END))")
                                    End If
                                Case "SelectBox"
                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & "=" & strIDValue(1) & ")")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " >= '" & fromDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " <= '" & toDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    End If
                                Case "CheckBoxList"
                                    Dim items As String() = strIDValue(1).Split(",")
                                    Dim searchString As String = ""

                                    For Each item As String In items
                                        searchString = searchString & If(searchString.Length > 0, " OR ", "") & " fn_GetCustFldStringValue(" & strID(0).Replace("CFW.Cust", "") & ",DivisionMaster.numDivisionID,CFW.Fld_Value) " & " ilike '%" & item.Replace("'", "''") & "%'"
                                    Next

                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND (" & searchString & "))")
                                Case Else
                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & strCustom & ")")
                            End Select
                        Else
                            Select Case strID(3).Trim()
                                Case "Website", "Email", "TextBox"
                                    If strID(0) = "AD.numShipState" Then
                                        strRegularCondition.Add("ShipState.vcState ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                    ElseIf strID(0) = "ADC.vcCompactContactDetails" Then
                                        strRegularCondition.Add("(ADC.vcFirstName ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR ADC.vcLastName ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR ADC.numPhone ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR ADC.numPhoneExtension ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                    ElseIf strID(0) = "AD.numBillState" Then
                                        strRegularCondition.Add("BillState.vcState ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                    Else
                                        strRegularCondition.Add(strID(0) & " ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                    End If
                                Case "SelectBox"
                                    If strID(0).Contains("numFollowUpStatus") Then
                                        strRegularCondition.Add(strID(0) & " IN(" & strIDValue(1) & ")")
                                    Else
                                        strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                                    End If
                                Case "TextArea"
                                    strRegularCondition.Add(" Cast(" & strID(0) & " as varchar(5000)) ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strRegularCondition.Add(strID(0) & " >= '" & fromDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strRegularCondition.Add(strID(0) & " <= '" & toDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    End If
                                Case "CheckBox"
                                    If strID(0).Contains("bitEcommerceAccess") Then
                                        If strIDValue(1).ToLower() = "yes" Or strIDValue(1).ToLower() = "true" Then
                                            strRegularCondition.Add("1 = (CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) >0 THEN 1 ELSE 0 END)")
                                        ElseIf strIDValue(1).ToLower() = "no" Or strIDValue(1).ToLower() = "false" Then
                                            strRegularCondition.Add("1 = (CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) = 0 THEN 1 ELSE 0 END)")
                                        End If
                                    Else
                                        If strIDValue(1).ToLower() = "yes" Or strIDValue(1).ToLower() = "true" Then
                                            strCustomCondition.Add("COALESCE(" & strID(0) & ",'0')='1'")
                                        ElseIf strIDValue(1).ToLower() = "no" Or strIDValue(1).ToLower() = "false" Then
                                            strCustomCondition.Add("COALESCE(" & strID(0) & ",'0')='0'")
                                        End If
                                    End If
                            End Select
                        End If
                    Next
                    RegularSearch = String.Join(" and ", strRegularCondition.ToArray())
                    CustomSearch = String.Join(" and ", strCustomCondition.ToArray())
                Else
                    RegularSearch = ""
                    CustomSearch = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)

            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
        Dim lngReportID As Long
        Protected Sub Button1_Click(sender As Object, e As EventArgs)
            lngReportID = 211
            BindReport()
            Dim stringBuilder As New StringBuilder()
            Dim stringWriter As New StringWriter(stringBuilder)
            Dim htmlWriter As New HtmlTextWriter(stringWriter)
            tblReport.RenderControl(htmlWriter)
            Dim dataGridHTML As String = stringBuilder.ToString()
            Dim strFileName As String
            Dim strFilePhysicalLocation As String
            Dim objHTMLToPDF As New HTMLToPDF
            strFileName = objHTMLToPDF.ConvertHTML2PDF(dataGridHTML, CCommon.ToLong(Session("DomainID")), numOrientation:=2, keepFooterAtBottom:=CCommon.ToBool(True))
            strFilePhysicalLocation = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
            objCommon.AddAttchmentToSession(strFileName, CCommon.GetDocumentPath(Session("DomainID")) & strFileName, strFilePhysicalLocation)
            Dim str As String
            str = "<script language='javascript'>window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&PickAtch=1&Lsemail=" & HttpUtility.JavaScriptStringEncode("noreply@bizautomation.com") & "','ComposeWindow','toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')</script>"
            'Response.Write(str)
            Page.ClientScript.RegisterClientScriptBlock(Me.GetType, "Email", str, False)
        End Sub
        Sub BindReport()
            Try
                Dim objReportManage As New CustomReportsManage
                Dim ds As DataSet

                objReportManage.ReportID = lngReportID
                objReportManage.DomainID = Session("DomainID")
                objReportManage.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objReportManage.UserCntID = Session("UserContactID")

                ds = objReportManage.GetReportListMasterDetail

                Dim trHeader As TableHeaderRow = New TableHeaderRow()
                Dim tc As TableHeaderCell = New TableHeaderCell()

                Dim trRow As TableRow = New TableRow()
                Dim cell As New TableCell()

                If ds.Tables.Count > 0 Then
                    'lblReportName.Text = ds.Tables(0).Rows(0)("vcReportName")
                    'lblReportDesc.Text = ds.Tables(0).Rows(0)("vcReportDescription")
                    'lblGeneratedOn.Text = FormattedDateFromDate(Now, Session("DateFormat"))

                    Dim objReport As ReportObject = objReportManage.GetDatasetToReportObject(ds)

                    Dim dsColumnList As DataSet
                    dsColumnList = objReportManage.GetReportModuleGroupFieldMaster

                    Try
                        objReportManage.textQuery = objReportManage.GetReportQuery(objReportManage, ds, objReport, False, True, dsColumnList)
                    Catch ex As Exception
                        If ex.Message = "ReportNoColumnFound" Then

                            trRow = New TableRow()

                            cell = New TableCell()
                            cell.CssClass = "errorInfo"
                            cell.Text = "Error: Can't display the report until you fix the error in Reports."
                            trRow.Cells.Add(cell)
                            tblReport.Rows.Add(trRow)
                            Exit Sub
                        End If
                    End Try
                    Dim dtData As DataTable = objReportManage.USP_ReportQueryExecute()

                    'Dim tblReport As New Table
                    'tblReport.ID = "tblReport"
                    tblReport.CssClass = "table table-responsive table-bordered"
                    tblReport.Width = Unit.Percentage(100)
                    tblReport.BorderWidth = Unit.Pixel(1)
                    tblReport.CellSpacing = 0
                    tblReport.CellPadding = 0
                    'phReport.Controls.Add(tblReport)
                    Dim spaceCharacter As String = ""

                    If ds.Tables(0).Rows(0)("tintReportType") = 0 Then 'Tabular

                        trHeader = New TableHeaderRow()

                        For Each str As String In objReport.ColumnList
                            Dim strColumn As String() = str.Split("_")

                            Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                            If dr.Length > 0 Then
                                tc = New TableHeaderCell()
                                tc.Text = dr(0)("vcFieldname")
                                trHeader.Cells.Add(tc)
                            End If
                        Next

                        tblReport.Rows.Add(trHeader)

                        For i As Integer = 0 To dtData.Rows.Count - 1
                            trRow = New TableRow()

                            For Each str As String In objReport.ColumnList
                                cell = New TableCell()
                                cell.Text = dtData.Rows(i)(str).ToString()
                                trRow.Cells.Add(cell)
                            Next
                            tblReport.Rows.Add(trRow)
                        Next
                    ElseIf ds.Tables(0).Rows(0)("tintReportType") = 1 Then 'Summary
                        trHeader = New TableHeaderRow()
                        Dim bitHideSummaryDetail As Boolean = CCommon.ToBool(ds.Tables(0).Rows(0)("bitHideSummaryDetail"))

                        For Each str As String In objReport.ColumnList
                            Dim strColumn As String() = str.Split("_")

                            Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                            If dr.Length > 0 Then
                                tc = New TableHeaderCell()
                                tc.Text = dr(0)("vcFieldname")
                                trHeader.Cells.Add(tc)
                            End If
                        Next

                        tblReport.Rows.Add(trHeader)

                        If objReport.GroupColumn.Count > 0 Then

                            Dim query = From row In dtData.AsEnumerable()
                                        Group row By GroupColumns = New With {Key .Key1 = row.Field(Of String)(objReport.GroupColumn(0))} Into GroupDetail = Group
                                        Select New With {
                                            Key GroupColumns.Key1,
                                            .CountTotal = GroupDetail.Count(),
                                            .Detail = GroupDetail
                                       }

                            If objReport.SortColumn.Length > 0 Then
                                Dim AggField = From row In objReport.Aggregate Where row.Column = objReport.SortColumn
                                If AggField.Count > 0 Then
                                    If objReport.SortDirection = "desc" Then
                                        query = From row In query.AsEnumerable() Order By (row.Detail.Sum(Function(r) r.Field(Of Decimal)(objReport.SortColumn))) Descending Take (objReport.NoRows)
                                    Else
                                        query = From row In query.AsEnumerable() Order By (row.Detail.Sum(Function(r) r.Field(Of Decimal)(objReport.SortColumn))) Ascending Take (objReport.NoRows)
                                    End If
                                End If
                            End If

                            If objReport.NoRows > 0 Then
                                query = query.Take(objReport.NoRows)
                            End If

                            Dim strColumn As String() = objReport.GroupColumn(0).Split("_")
                            Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))
                            If dr.Length > 0 AndAlso bitHideSummaryDetail Then
                                tc = New TableHeaderCell()
                                tc.Text = dr(0)("vcFieldname")
                                trHeader.Cells.AddAt(0, tc)
                            End If

                            For Each x In query
                                If bitHideSummaryDetail Then
                                    trRow = New TableRow()
                                    cell = New TableCell()
                                    cell.Text = String.Format("{0}&nbsp;(Total Records:{1})", x.Key1, x.CountTotal)
                                    trRow.Cells.Add(cell)
                                Else
                                    trRow = New TableRow()
                                    cell = New TableCell()
                                    cell.Text = String.Format("<strong>{0}&nbsp;&nbsp;&nbsp;(Total Records:{1})</strong>", x.Key1, x.CountTotal)
                                    cell.ColumnSpan = objReport.ColumnList.Count
                                    cell.CssClass = "Group0"
                                    trRow.Cells.Add(cell)
                                    tblReport.Rows.Add(trRow)
                                End If

                                If objReport.Aggregate.Count > 0 Then

                                    If Not bitHideSummaryDetail Then
                                        trRow = New TableRow()
                                    End If

                                    For Each str As String In objReport.ColumnList
                                        cell = New TableCell()
                                        cell.CssClass = IIf(bitHideSummaryDetail, "", "Group0")
                                        Dim strCellSummary As String = ""

                                        Dim AggField = From row In objReport.Aggregate Where row.Column = str
                                        If AggField.Count > 0 Then
                                            Dim total As Integer = x.CountTotal

                                            For Each str1 As AggregateObject In AggField

                                                Select Case str1.aggType
                                                    Case "sum"
                                                        If total > 0 Then
                                                            strCellSummary += String.Format("{0:#.00}", x.Detail.Sum(Function(r) r(str1.Column))) & "<br/>"
                                                        End If

                                                    Case "avg"
                                                        If total > 0 Then
                                                            strCellSummary += String.Format("Avg {0:#.00}", x.Detail.Average(Function(r) r(str1.Column))) & "<br/>"
                                                        End If

                                                    Case "max"
                                                        If total > 0 Then
                                                            strCellSummary += String.Format("Largest {0:#.00}", x.Detail.Max(Function(r) r(str1.Column))) & "<br/>"
                                                        End If

                                                    Case "min"
                                                        If total > 0 Then
                                                            strCellSummary += String.Format("Smallest {0:#.00}", x.Detail.Min(Function(r) r(str1.Column))) & "<br/>"
                                                        End If

                                                End Select
                                            Next

                                        End If

                                        cell.CssClass = "tdAggregate"
                                        cell.Text = strCellSummary
                                        trRow.Cells.Add(cell)
                                    Next
                                    tblReport.Rows.Add(trRow)
                                End If

                                If objReport.GroupColumn.Count > 1 AndAlso bitHideSummaryDetail = False Then
                                    Dim query1 = From row In x.Detail.AsEnumerable()
                                                 Group row By GroupColumns = New With {Key .Key2 = row.Field(Of String)(objReport.GroupColumn(1))} Into GroupDetail = Group
                                                 Select New With {
                                                     Key GroupColumns.Key2,
                                                     .CountTotal = GroupDetail.Count(),
                                                     .Detail = GroupDetail
                                                              }

                                    For Each x1 In query1
                                        trRow = New TableRow()
                                        cell = New TableCell()
                                        cell.Text = String.Format("<strong>{0}&nbsp;&nbsp;&nbsp;(Total Records:{1})</strong>", x1.Key2, x1.CountTotal)
                                        cell.ColumnSpan = objReport.ColumnList.Count
                                        cell.CssClass = "Group1"
                                        trRow.Cells.Add(cell)
                                        tblReport.Rows.Add(trRow)

                                        If objReport.GroupColumn.Count > 2 Then
                                            Dim query2 = From row In x1.Detail.AsEnumerable()
                                                         Group row By GroupColumns = New With {Key .Key3 = row.Field(Of String)(objReport.GroupColumn(2))} Into GroupDetail = Group
                                                         Select New With {
                                                             Key GroupColumns.Key3,
                                                             .CountTotal = GroupDetail.Count(),
                                                             .Detail = GroupDetail
                                                                      }

                                            For Each x2 In query2
                                                trRow = New TableRow()
                                                cell = New TableCell()
                                                cell.Text = String.Format("<strong>{0}&nbsp;&nbsp;&nbsp;(Total Records:{1})</strong>", x2.Key3, x2.CountTotal)
                                                cell.ColumnSpan = objReport.ColumnList.Count
                                                cell.CssClass = "Group2"
                                                trRow.Cells.Add(cell)
                                                tblReport.Rows.Add(trRow)

                                                For Each dr1 As DataRow In x2.Detail
                                                    trRow = New TableRow()

                                                    For Each str As String In objReport.ColumnList
                                                        cell = New TableCell()
                                                        cell.Text = dr1(str).ToString()
                                                        trRow.Cells.Add(cell)
                                                    Next
                                                    tblReport.Rows.Add(trRow)
                                                Next
                                            Next
                                        Else
                                            For Each dr1 As DataRow In x1.Detail
                                                trRow = New TableRow()

                                                For Each str As String In objReport.ColumnList
                                                    cell = New TableCell()
                                                    cell.Text = dr1(str).ToString()
                                                    trRow.Cells.Add(cell)
                                                Next
                                                tblReport.Rows.Add(trRow)
                                            Next
                                        End If
                                    Next
                                ElseIf bitHideSummaryDetail = False Then
                                    For Each dr1 As DataRow In x.Detail
                                        trRow = New TableRow()

                                        For Each str As String In objReport.ColumnList
                                            cell = New TableCell()
                                            cell.Text = dr1(str).ToString()
                                            trRow.Cells.Add(cell)
                                        Next
                                        tblReport.Rows.Add(trRow)
                                    Next
                                End If
                            Next
                        Else
                            For i As Integer = 0 To dtData.Rows.Count - 1
                                trRow = New TableRow()

                                For Each str As String In objReport.ColumnList
                                    cell = New TableCell()
                                    cell.Text = dtData.Rows(i)(str).ToString()
                                    trRow.Cells.Add(cell)
                                Next
                                tblReport.Rows.Add(trRow)
                            Next
                        End If

                    ElseIf ds.Tables(0).Rows(0)("tintReportType") = 2 Then 'Matrix

                        Dim dtColumns = New DataTable()

                        trHeader = New TableHeaderRow()

                        tc = New TableHeaderCell()
                        tc.Text = ""
                        trHeader.Cells.Add(tc)

                        If objReport.ColumnBreaks.Count > 0 Then
                            Dim NameGroups = dtData.AsEnumerable().GroupBy(Function(r) r.Field(Of String)(objReport.ColumnBreaks(0)))
                            dtColumns.Columns.Add("ColumnName", GetType(String))
                            For Each NameGroup In NameGroups
                                If NameGroup.Key Is Nothing Or NameGroup.Key.Trim.Length = 0 Then
                                    dtColumns.Rows.Add("")
                                Else
                                    dtColumns.Rows.Add(NameGroup.Key)
                                End If
                            Next

                            Dim strColumn As String() = objReport.ColumnBreaks(0).Split("_")

                            Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                            If dr.Length > 0 Then
                                tc = New TableHeaderCell()
                                tc.Text = dr(0)("vcFieldname")
                                trHeader.Cells.Add(tc)
                            End If

                            For Each dc As DataRow In dtColumns.Rows
                                tc = New TableHeaderCell()
                                tc.Text = dc(0)
                                trHeader.Cells.Add(tc)
                            Next
                        Else
                            tc = New TableHeaderCell()
                            tc.Text = ""
                            trHeader.Cells.Add(tc)
                        End If

                        tc = New TableHeaderCell()
                        tc.Text = "Grand Total"
                        tc.CssClass = "grandTotal"
                        trHeader.Cells.Add(tc)

                        tblReport.Rows.Add(trHeader)

                        If objReport.RowBreaks.Count > 0 Then
                            trHeader = New TableHeaderRow()
                            Dim strColumn As String() = objReport.RowBreaks(0).Split("_")

                            Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                            If dr.Length > 0 Then
                                tc = New TableHeaderCell()
                                tc.Text = dr(0)("vcFieldname")
                                trHeader.Cells.Add(tc)
                            End If

                            tc = New TableHeaderCell()
                            tc.Text = ""
                            tc.ColumnSpan = tblReport.Rows(0).Cells.Count - 1
                            trHeader.Cells.Add(tc)

                            tblReport.Rows.Add(trHeader)
                        End If

                        Dim strCell As String = ""
                        Dim strCellSummary As String = ""

                        For Each str As AggregateObject In objReport.Aggregate
                            Dim strColumn As String() = str.Column.Split("_")
                            Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                            If dr.Length > 0 Then
                                Select Case str.aggType

                                    Case "sum"
                                        strCell += "Sum of No. of " & dr(0)("vcFieldname") & "<br/>"

                                    Case "avg"
                                        strCell += "Average of No. of " & dr(0)("vcFieldname") & "<br/>"

                                    Case "max"
                                        strCell += "Largest of No. of " & dr(0)("vcFieldname") & "<br/>"

                                    Case "min"
                                        strCell += "Smallest of No. of " & dr(0)("vcFieldname") & "<br/>"

                                End Select
                            End If
                        Next

                        If objReport.RowBreaks.Count > 0 Then
                            Dim CompanyGroups = dtData.AsEnumerable().GroupBy(Function(r) r.Field(Of String)(objReport.RowBreaks(0)))
                            For Each CompanyGroup In CompanyGroups
                                trRow = New TableRow()

                                cell = New TableCell()
                                cell.Text = CompanyGroup.Key
                                trRow.Cells.Add(cell)

                                cell = New TableCell()
                                cell.Text = strCell & "Record Count"
                                trRow.Cells.Add(cell)

                                If objReport.ColumnBreaks.Count > 0 Then
                                    For Each col As DataRow In dtColumns.Rows
                                        Dim queryTotal = CompanyGroup.Where(Function(g) g.Field(Of String)(objReport.ColumnBreaks(0)) = col(0))
                                        strCellSummary = CalculateAggregateSummary(objReport, queryTotal)

                                        cell = New TableCell()
                                        cell.Text = strCellSummary
                                        trRow.Cells.Add(cell)
                                    Next
                                End If

                                strCellSummary = CalculateAggregateSummary(objReport, CompanyGroup)

                                cell = New TableCell()
                                cell.Text = strCellSummary
                                cell.CssClass = "grandTotal"
                                trRow.Cells.Add(cell)

                                tblReport.Rows.Add(trRow)
                            Next
                        End If

                        trRow = New TableRow()

                        cell = New TableCell()
                        cell.Text = "Grand Total"
                        cell.CssClass = "grandTotal"
                        trRow.Cells.Add(cell)

                        cell = New TableCell()
                        cell.Text = strCell & "Record Count"
                        cell.CssClass = "grandTotal"
                        trRow.Cells.Add(cell)

                        If objReport.ColumnBreaks.Count > 0 Then
                            For Each col As DataRow In dtColumns.Rows
                                Dim queryGrandTotal = dtData.AsEnumerable().Where(Function(g) g.Field(Of String)(objReport.ColumnBreaks(0)) = col(0))

                                strCellSummary = CalculateAggregateSummary(objReport, queryGrandTotal)

                                cell = New TableCell()
                                cell.Text = strCellSummary
                                cell.CssClass = "grandTotal"
                                trRow.Cells.Add(cell)
                            Next
                        End If

                        strCellSummary = CalculateAggregateSummary(objReport, dtData.AsEnumerable())

                        cell = New TableCell()
                        cell.Text = strCellSummary
                        cell.CssClass = "grandTotal"
                        trRow.Cells.Add(cell)

                        tblReport.Rows.Add(trRow)
                    ElseIf ds.Tables(0).Rows(0)("tintReportType") = 3 Then 'KPI
                        trHeader = New TableHeaderRow()

                        tc = New TableHeaderCell()
                        tc.Text = "Object of Measure"
                        trHeader.Cells.Add(tc)

                        tc = New TableHeaderCell()
                        tc.Text = "Date Measure"
                        trHeader.Cells.Add(tc)

                        tc = New TableHeaderCell()
                        tc.Text = "Type"
                        trHeader.Cells.Add(tc)

                        tc = New TableHeaderCell()
                        tc.Text = "Period"
                        trHeader.Cells.Add(tc)

                        tc = New TableHeaderCell()
                        tc.Text = "Current"
                        trHeader.Cells.Add(tc)

                        tc = New TableHeaderCell()
                        tc.Text = "Previous"
                        trHeader.Cells.Add(tc)

                        tc = New TableHeaderCell()
                        tc.Text = "Change"
                        trHeader.Cells.Add(tc)

                        tblReport.Rows.Add(trHeader)

                        trRow = New TableRow()

                        'Object of Measure
                        cell = New TableCell()
                        cell.Text = objReport.vcGroupName
                        trRow.Cells.Add(cell)

                        'Date Measure
                        Dim strColumn As String() = objReport.DateFieldFilter.Split("_")
                        Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                        cell = New TableCell()
                        If dr.Length > 0 Then
                            cell.Text = dr(0)("vcFieldName")
                        End If
                        trRow.Cells.Add(cell)

                        'Type
                        cell = New TableCell()
                        If objReport.KPIMeasureField = "0_0_False" Then
                            cell.Text = "Record Count"
                        Else
                            strColumn = objReport.KPIMeasureField.Split("_")
                            dr = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                            If dr.Length > 0 Then
                                cell.Text = dr(0)("vcFieldName")
                            End If
                        End If
                        trRow.Cells.Add(cell)

                        'Period
                        cell = New TableCell()
                        cell.Text = GetPeriodTitle(objReport)
                        trRow.Cells.Add(cell)

                        Dim decPrevious As Decimal = CCommon.ToDecimal(dtData.Rows(0)(0))
                        Dim decCurrent As Decimal = CCommon.ToDecimal(dtData.Rows(0)(1))

                        'Current
                        cell = New TableCell()
                        cell.Text = String.Format("{0:#,###.00}", decCurrent)
                        trRow.Cells.Add(cell)

                        'Previous
                        cell = New TableCell()
                        cell.Text = String.Format("{0:#,###.00}", decPrevious)
                        trRow.Cells.Add(cell)

                        'Change
                        Dim decVariance As Decimal = (decCurrent - decPrevious) / IIf(decPrevious = 0, 1, decPrevious)
                        cell = New TableCell()
                        cell.Text = String.Format("<font color='{1}'>{0:###.00%}</font>", decVariance, IIf(decVariance < 0, "red", "green"))
                        trRow.Cells.Add(cell)

                        Dim decValue1 As Decimal = CCommon.ToDecimal(ds.Tables(5).Rows(0)("decValue1"))
                        Dim strThresoldType As String = CCommon.ToString(ds.Tables(5).Rows(0)("vcThresoldType"))

                        If (strThresoldType = "lt" AndAlso decCurrent < decValue1) Or
                            (strThresoldType = "gt" AndAlso decCurrent > decValue1) Or
                            (strThresoldType = "le" AndAlso decCurrent <= decValue1) Or
                            (strThresoldType = "ge" AndAlso decCurrent >= decValue1) Then
                            trRow.Font.Bold = True
                            trRow.Cells(0).Text += " <img src='../images/star_16.png'/>"
                        End If

                        tblReport.Rows.Add(trRow)
                    ElseIf ds.Tables(0).Rows(0)("tintReportType") = 4 Then 'ScoreCard
                        trHeader = New TableHeaderRow()

                        tc = New TableHeaderCell()
                        tc.Text = "Object of Measure"
                        trHeader.Cells.Add(tc)

                        tc = New TableHeaderCell()
                        tc.Text = "Date Measure"
                        trHeader.Cells.Add(tc)

                        tc = New TableHeaderCell()
                        tc.Text = "Type"
                        trHeader.Cells.Add(tc)

                        tc = New TableHeaderCell()
                        tc.Text = "Period"
                        trHeader.Cells.Add(tc)

                        tc = New TableHeaderCell()
                        tc.Text = "Actual"
                        trHeader.Cells.Add(tc)

                        tc = New TableHeaderCell()
                        tc.Text = "Goal"
                        trHeader.Cells.Add(tc)

                        tc = New TableHeaderCell()
                        tc.Text = "Variance"
                        trHeader.Cells.Add(tc)

                        tc = New TableHeaderCell()
                        tc.Text = "Score"
                        trHeader.Cells.Add(tc)

                        tblReport.Rows.Add(trHeader)

                        trRow = New TableRow()

                        'Object of Measure
                        cell = New TableCell()
                        cell.Text = objReport.vcGroupName
                        trRow.Cells.Add(cell)

                        'Date Measure
                        Dim strColumn As String() = objReport.DateFieldFilter.Split("_")
                        Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                        cell = New TableCell()
                        If dr.Length > 0 Then
                            cell.Text = dr(0)("vcFieldName")
                        End If
                        trRow.Cells.Add(cell)

                        'Type
                        cell = New TableCell()
                        If objReport.KPIMeasureField = "0_0_False" Then
                            cell.Text = "Record Count"
                        Else
                            strColumn = objReport.KPIMeasureField.Split("_")
                            dr = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                            If dr.Length > 0 Then
                                cell.Text = dr(0)("vcFieldName")
                            End If
                        End If
                        trRow.Cells.Add(cell)

                        'Period
                        cell = New TableCell()
                        cell.Text = GetPeriodTitle(objReport)
                        trRow.Cells.Add(cell)

                        Dim decActual As Decimal = CCommon.ToDecimal(dtData.Rows(0)(0))
                        Dim decValue1 As Decimal = CCommon.ToDecimal(ds.Tables(5).Rows(0)("decValue1"))
                        Dim decValue2 As Decimal = CCommon.ToDecimal(ds.Tables(5).Rows(0)("decValue2"))

                        'Actual
                        cell = New TableCell()
                        cell.Text = String.Format("{0:#,###.00}", decActual)
                        trRow.Cells.Add(cell)

                        'Goal
                        cell = New TableCell()
                        cell.Text = String.Format("{0:#,###.00}", decValue1)
                        trRow.Cells.Add(cell)

                        'Variance
                        Dim decVariance As Decimal = decActual - decValue1
                        cell = New TableCell()
                        cell.Text = String.Format("<font color='{1}'>{0:#,###.00}</font>", decVariance, IIf(decVariance < 0, "red", "green"))
                        trRow.Cells.Add(cell)

                        'Score
                        cell = New TableCell()

                        If decActual >= decValue1 Then
                            cell.Text = " <img src='../images/bullet-green_16.png'/>"
                        ElseIf decActual < decValue1 AndAlso decActual >= decValue2 Then
                            cell.Text = " <img src='../images/triangle_yellow_16.png'/>"
                        Else
                            cell.Text = " <img src='../images/diamond_red_16.png'/>"
                        End If

                        trRow.Cells.Add(cell)

                        tblReport.Rows.Add(trRow)
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Function GetPeriodTitle(objReport As ReportObject)
            Try
                Select Case objReport.DateFieldValue
                    Case "AllTime" : GetPeriodTitle = "All Time"
                    Case "Custom"
                        If objReport.FromDate.Length > 0 AndAlso objReport.ToDate.Length > 0 Then
                            Dim fromDate As DateTime = objReport.FromDate
                            Dim toDate As DateTime = objReport.ToDate
                            GetPeriodTitle = String.Format("{0:MM/dd/YYYY} - {1:MM/dd/YYYY}", objReport.FromDate, objReport.ToDate)
                        End If
                        'Year
                    Case "CurYear" : GetPeriodTitle = "Current CY"
                    Case "PreYear" : GetPeriodTitle = "Previous CY"
                    Case "Pre2Year" : GetPeriodTitle = "Previous 2 CY"
                    Case "Ago2Year" : GetPeriodTitle = "2 FY Ago"
                    Case "NextYear" : GetPeriodTitle = "Next CY"
                    Case "CurPreYear" : GetPeriodTitle = "Current and Previous CY"
                    Case "CurPre2Year" : GetPeriodTitle = "Current and Previous 2 CY"
                    Case "CurNextYear" : GetPeriodTitle = "Current and Next CY"
                        'Quarter
                    Case "CuQur" : GetPeriodTitle = "Current CQ"
                    Case "CurNextQur" : GetPeriodTitle = "Current and Next CQ"
                    Case "CurPreQur" : GetPeriodTitle = "Current and Previous CQ"
                    Case "NextQur" : GetPeriodTitle = "Next CQ"
                    Case "PreQur" : GetPeriodTitle = "Previous CQ"
                        'Month
                    Case "LastMonth" : GetPeriodTitle = "Last Month"
                    Case "ThisMonth" : GetPeriodTitle = "This Month"
                    Case "NextMonth" : GetPeriodTitle = "Next Month"
                    Case "CurPreMonth" : GetPeriodTitle = "Current and Previous Month"
                    Case "CurNextMonth" : GetPeriodTitle = "Current and Next Month"
                        'Week
                    Case "LastWeek" : GetPeriodTitle = "Last Week"
                    Case "ThisWeek" : GetPeriodTitle = "This Week"
                    Case "NextWeek" : GetPeriodTitle = "Next Week"
                        'Day
                    Case "Yesterday" : GetPeriodTitle = "Yesterday"
                    Case "Today" : GetPeriodTitle = "Today"
                    Case "Tomorrow" : GetPeriodTitle = "Tomorrow"
                    Case "Last7Day" : GetPeriodTitle = "Last 7 Days"
                    Case "Last30Day" : GetPeriodTitle = "Last 30 Days"
                    Case "Last60Day" : GetPeriodTitle = "Last 60 Days"
                    Case "Last90Day" : GetPeriodTitle = "Last 90 Days"
                    Case "Last120Day" : GetPeriodTitle = "Last 120 Days"
                    Case "Next7Day" : GetPeriodTitle = "Next 7 Days"
                    Case "Next30Day" : GetPeriodTitle = "Next 30 Days"
                    Case "Next60Day" : GetPeriodTitle = "Next 60 Days"
                    Case "Next90Day" : GetPeriodTitle = "Next 90 Days"
                    Case "Next120Day" : GetPeriodTitle = "Next 120 Days"
                        'KPI Period
                    Case "Year" : GetPeriodTitle = "This Year vs. Last Year"
                    Case "Quarter" : GetPeriodTitle = "This Quarter vs. Last Quarter"
                    Case "Month" : GetPeriodTitle = "This Month vs. Last Month"
                    Case "Week" : GetPeriodTitle = "This Week vs. Last Week"

                End Select
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Function CalculateAggregateSummary(objReport As ReportObject, queryTotal As IEnumerable(Of System.Data.DataRow)) As String
            Try
                Dim strCellSummary As String = ""

                Dim total As Integer = queryTotal.Count()

                For Each str As AggregateObject In objReport.Aggregate

                    Select Case str.aggType
                        Case "sum"
                            If total = 0 Then
                                strCellSummary += "0" & "<br/>"
                            Else
                                strCellSummary += String.Format("{0:#.00}", queryTotal.Sum(Function(r) r.Field(Of Decimal)(str.Column))) & "<br/>"
                            End If

                        Case "avg"
                            If total = 0 Then
                                strCellSummary += "0" & "<br/>"
                            Else
                                strCellSummary += String.Format("{0:#.00}", queryTotal.Average(Function(r) r.Field(Of Decimal)(str.Column))) & "<br/>"
                            End If

                        Case "max"
                            If total = 0 Then
                                strCellSummary += "0" & "<br/>"
                            Else
                                strCellSummary += String.Format("{0:#.00}", queryTotal.Max(Function(r) r.Field(Of Decimal)(str.Column))) & "<br/>"
                            End If

                        Case "min"
                            If total = 0 Then
                                strCellSummary += "0" & "<br/>"
                            Else
                                strCellSummary += String.Format("{0:#.00}", queryTotal.Min(Function(r) r.Field(Of Decimal)(str.Column))) & "<br/>"
                            End If

                    End Select
                Next

                strCellSummary = strCellSummary & total
                Return strCellSummary
            Catch ex As Exception
                Throw ex
            End Try
        End Function
    End Class

End Namespace