Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts

Namespace BACRM.UserInterface.Prospects
    Partial Public Class frmConfCompanyList : Inherits BACRMPage

        
        Dim objContact As CContacts

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                btnSave.Attributes.Add("onclick", "Save()")
                If Not IsPostBack Then

                    hfFormId.Value = CCommon.ToLong(GetQueryStringVal("FormId"))


                    If hfFormId.Value = 34 Or hfFormId.Value = 35 Or hfFormId.Value = 36 Then
                        lblName.Text = "Organization Layout"

                        objCommon.sb_FillComboFromDBwithSel(ddlRelationship, 5, Session("DomainID"))


                        ddlRelationship.Items.Remove(ddlRelationship.Items.FindByValue("46"))
                        ddlRelationship.Items.Insert(1, "Leads")
                        ddlRelationship.Items.FindByText("Leads").Value = "1"
                        ddlRelationship.Items.Insert(2, "Accounts")
                        ddlRelationship.Items.FindByText("Accounts").Value = "2"
                        ddlRelationship.Items.Insert(3, "Prospects")
                        ddlRelationship.Items.FindByText("Prospects").Value = "3"

                        If GetQueryStringVal("RelId") <> "" Then
                            If Not ddlRelationship.Items.FindByValue(GetQueryStringVal("RelId")) Is Nothing Then
                                ddlRelationship.ClearSelection()
                                ddlRelationship.Items.FindByValue(GetQueryStringVal("RelId")).Selected = True
                            End If
                        End If

                    ElseIf hfFormId.Value = 12 Then
                        lblName.Text = "Cases Layout"
                        tblRelationship.Visible = False
                    ElseIf hfFormId.Value = 13 Then
                        lblName.Text = "Projects Layout"
                        tblRelationship.Visible = False
                    ElseIf hfFormId.Value = 145 Then
                        lblName.Text = "Work Orders Layout"
                        tblRelationship.Visible = False
                    End If
                    BindLists()

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindLists()
            Try
                Dim ds As DataSet
                objContact = New CContacts
                objContact.DomainID = Session("DomainId")
                objContact.FormId = hfFormId.Value
                objContact.UserCntID = Session("UserContactId")
                If hfFormId.Value = 12 Or hfFormId.Value = 13 Or hfFormId.Value = 145 Then
                    objContact.ContactType = 0
                Else : objContact.ContactType = ddlRelationship.SelectedValue
                End If

                ds = objContact.GetColumnConfiguration()
                lstAvailablefld.DataSource = ds.Tables(0)
                lstAvailablefld.DataTextField = "vcFieldName"
                lstAvailablefld.DataValueField = "numFieldID"
                lstAvailablefld.DataBind()
                lstSelectedfld.DataSource = ds.Tables(1)
                lstSelectedfld.DataValueField = "numFieldID"
                lstSelectedfld.DataTextField = "vcFieldName"
                lstSelectedfld.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlRelationship_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRelationship.SelectedIndexChanged
            Try
                If hfFormId.Value = 34 Or hfFormId.Value = 35 Or hfFormId.Value = 36 Then
                    Select Case ddlRelationship.SelectedValue
                        Case 1
                            hfFormId.Value = "34"
                        Case 3
                            hfFormId.Value = "35"
                        Case Else
                            hfFormId.Value = "36"
                    End Select
                End If
                BindLists()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                If objContact Is Nothing Then objContact = New CContacts
                Dim dsNew As New DataSet
                Dim dtTable As New DataTable
                dtTable.Columns.Add("numFieldID")
                dtTable.Columns.Add("bitCustom")
                dtTable.Columns.Add("tintOrder")

                Dim i As Integer
                Dim dr As DataRow
                Dim str As String()
                str = hdnCol.Value.Split(",")
                For i = 0 To str.Length - 2
                    dr = dtTable.NewRow
                    dr("numFieldID") = str(i).Split("~")(0)
                    dr("bitCustom") = str(i).Split("~")(1)
                    dr("tintOrder") = i
                    dtTable.Rows.Add(dr)
                Next

                dtTable.TableName = "Table"
                dsNew.Tables.Add(dtTable.Copy)
                If hfFormId.Value = 12 Or hfFormId.Value = 13 Or hfFormId.Value = 145 Then
                    objContact.ContactType = 0
                Else : objContact.ContactType = ddlRelationship.SelectedValue
                End If

                objContact.DomainID = Session("DomainId")
                objContact.UserCntID = Session("UserContactId")
                objContact.FormId = hfFormId.Value
                objContact.strXml = dsNew.GetXml
                objContact.SaveContactColumnConfiguration()
                dsNew.Tables.Remove(dsNew.Tables(0))

                BindLists()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace