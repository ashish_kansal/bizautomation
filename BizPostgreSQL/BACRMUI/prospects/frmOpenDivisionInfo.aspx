﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmOpenDivisionInfo.aspx.vb"
    Inherits=".frmOpenDivisionInfo" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        function Close() {
            window.close()
            return false;
        }

        function OpenWindow(a, type) {
            var str;
            if (type == 1)
                str = "../projects/frmProjects.aspx?frm=Prospects&ProID=" + a;
            else if (type == 2)
                str = "../cases/frmCases.aspx?CaseID=" + a;

            window.opener.opener.frames.document.location.href = str;
        }

        function OpenOpp(a, b) {
            var str;
            str = "../opportunity/frmOpportunities.aspx?frm=&OpID=" + a + "&OppType=" + b;

            if (document.getElementById("hdnType").value == "5" || document.getElementById("hdnType").value == "6")
                window.opener.frames.document.location.href = str;
            else
                window.opener.opener.frames.document.location.href = str;
        }
  
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <table cellspacing="2" cellpadding="2" width="100%">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="window.close()"
                            Text="Close" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblTitle" runat="server">Projects</asp:Label>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="Table9" Width="100%" runat="server" GridLines="None" BorderColor="black"
        CssClass="aspTable" BorderWidth="1" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:DataGrid ID="dgProjectsOpen" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Name">
                            <ItemTemplate>
                                <a href="javascript:OpenWindow(<%# Eval("numProId") %>,1)">
                                    <%# Eval("Name")%></a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="CustJobContact" HeaderText="Cust. Prime Job Contact">
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="vcResource" HeaderText="Resource(Project Role)"></asp:BoundColumn>
                    </Columns>
                    <PagerStyle Visible="False" ForeColor="#000066" BackColor="White" Mode="NumericPages">
                    </PagerStyle>
                </asp:DataGrid>
                <asp:DataGrid ID="dgOpenCases" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn DataField="" Visible="false"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Subject" DataField="textSubject"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Case No.">
                            <ItemTemplate>
                                <a href="javascript:OpenWindow(<%# Eval("numCaseid") %>,2)">
                                    <%# Eval("vcCaseNumber")%></a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Target Resolve Date">
                            <ItemTemplate>
                                <%#ReturnDateTime(DataBinder.Eval(Container.DataItem, "intTargetResolveDate"))%>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn HeaderText="Status" DataField="vcCaseStatus"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Description" DataField="textDesc"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Comments" DataField="textInternalComments"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
                <asp:DataGrid ID="dgOppOrder" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False">
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:TemplateColumn HeaderText="Order ID">
                            <ItemTemplate>
                                <a href="javascript:OpenOpp('<%# Eval("numOppId") %>','<%# Eval("tintopptype") %>')">
                                    <%# Eval("vcPOppName") %>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="bintCreatedDate" HeaderText="Date Created"></asp:BoundColumn>
                        <%--<asp:BoundColumn HeaderText="Customer P.O.#" DataField="vcRefOrderNo"></asp:BoundColumn>--%>
                        <asp:BoundColumn HeaderText="Amount" DataFormatString="{0:#,###.00}" DataField="monDealAmount">
                        </asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Open/Completed" DataField="vcDealStatus"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Status" DataField="vcStatus"></asp:BoundColumn>
                    </Columns>
                </asp:DataGrid>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:HiddenField ID="hdnType" runat="server" ClientIDMode="Static" />
</asp:Content>
