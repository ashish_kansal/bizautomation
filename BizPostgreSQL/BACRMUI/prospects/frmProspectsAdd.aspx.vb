Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Prospects
    Public Class frmProspectsAdd : Inherits BACRMPage

        Dim strAdd As String = ""
        Dim lngDivID As Integer

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                lngDivID = GetQueryStringVal("rtyWR")
                hdnVendor.Value = lngDivID

                'Author:Sachin Sadhu||Date:17-Jan-2014
                'Purpose: To add facility to save Billing/Shipping address on New Record Froms
               If CCommon.ToInteger(GetQueryStringVal("Reload")) <> 2 AndAlso lngDivID = 0 Then
                    GetLastInsertedDivision()
                End If
                'End of code by Sachin

                If Not IsPostBack Then
                    ' = "Organization"
                    hdnSmartyStreetsAPIKeys.Value = CCommon.ToString(Session("vcSmartyStreetsAPIKeys"))
                    If (CCommon.ToBool(Session("bitEnableSmartyStreets")) = True) Then
                        divBillingAddrVerification.Visible = True
                        divShippingAddrVerification.Visible = True
                    Else
                        divBillingAddrVerification.Visible = False
                        divShippingAddrVerification.Visible = False
                    End If
                    objCommon.sb_FillComboFromDBwithSel(ddlBillCountry, 40, Session("DomainID"))
                        objCommon.sb_FillComboFromDBwithSel(ddlShipCountry, 40, Session("DomainID"))
                        If Not ddlBillCountry.Items.FindByValue(Session("DefCountry")) Is Nothing Then
                            ddlBillCountry.Items.FindByValue(Session("DefCountry")).Selected = True
                            If ddlBillCountry.SelectedIndex > 0 Then
                                FillState(ddlBillState, ddlBillCountry.SelectedItem.Value, Session("DomainID"))
                            End If
                        End If
                        If Not ddlShipCountry.Items.FindByValue(Session("DefCountry")) Is Nothing Then
                            ddlShipCountry.Items.FindByValue(Session("DefCountry")).Selected = True
                            If ddlShipCountry.SelectedIndex > 0 Then
                                FillState(ddlShipState, ddlShipCountry.SelectedItem.Value, Session("DomainID"))
                            End If
                        End If
                        BindContact()
                        BindAddressName()
                        LoadAddress()
                    End If
                    litMessage.Text = ""
                btnCancel.Attributes.Add("onclick", "return Close();")

                If Not String.IsNullOrEmpty(hdnShipState.Value) AndAlso ddlShipState.SelectedItem.Text <> hdnShipState.Value Then
                    If Not ddlShipState.Items.FindByText(hdnShipState.Value) Is Nothing Then
                        ddlShipState.Items.FindByText(hdnShipState.Value).Selected = True
                    End If
                End If

                If Not String.IsNullOrEmpty(hdnBillState.Value) AndAlso ddlBillState.SelectedItem.Text <> hdnBillState.Value Then
                    If Not ddlBillState.Items.FindByText(hdnBillState.Value) Is Nothing Then
                        ddlBillState.Items.FindByText(hdnBillState.Value).Selected = True
                    End If
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub BindContact()
            Try
                Dim objOpp As New BACRM.BusinessLogic.Opportunities.COpportunities
                objOpp.DivisionID = lngDivID
                Dim ds As DataSet = objOpp.ListContact()

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim listItemBilling As ListItem
                    Dim listItemShipping As ListItem
                    For Each dr As DataRow In ds.Tables(0).Rows
                        listItemBilling = New ListItem()
                        listItemBilling.Text = CCommon.ToString(dr("Name"))
                        listItemBilling.Value = CCommon.ToString(dr("numcontactId"))
                        If CCommon.ToBool(dr("bitPrimaryContact")) Then
                            listItemBilling.Selected = True
                        End If
                        ddlBillingContact.Items.Add(listItemBilling)

                        listItemShipping = New ListItem()
                        listItemShipping.Text = CCommon.ToString(dr("Name"))
                        listItemShipping.Value = CCommon.ToString(dr("numcontactId"))
                        If CCommon.ToBool(dr("bitPrimaryContact")) Then
                            listItemShipping.Selected = True
                        End If
                        ddlShippingContact.Items.Add(listItemShipping)
                    Next
                End If

                ddlBillingContact.Items.Insert(0, New ListItem("---Select One---", "0"))
                ddlShippingContact.Items.Insert(0, New ListItem("---Select One---", "0"))
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub BindAddressName()
            Try
                Dim objContact As New CContacts
                objContact.AddressType = CContacts.enmAddressType.BillTo
                objContact.DomainID = Session("DomainID")
                objContact.RecordID = lngDivID
                objContact.AddresOf = CContacts.enmAddressOf.Organization
                objContact.byteMode = 2
                ddlBillAddressName.DataValueField = "numAddressID"
                ddlBillAddressName.DataTextField = "vcAddressName"
                ddlBillAddressName.DataSource = objContact.GetAddressDetail()
                ddlBillAddressName.DataBind()
                ddlBillAddressName.Items.Add(New ListItem("--Add New--", "0"))

                objContact.AddressType = CContacts.enmAddressType.ShipTo
                objContact.DomainID = Session("DomainID")
                objContact.RecordID = lngDivID
                objContact.AddresOf = CContacts.enmAddressOf.Organization
                objContact.byteMode = 2
                ddlShipAddressName.DataValueField = "numAddressID"
                ddlShipAddressName.DataTextField = "vcAddressName"
                ddlShipAddressName.DataSource = objContact.GetAddressDetail()
                ddlShipAddressName.DataBind()
                ddlShipAddressName.Items.Add(New ListItem("--Add New--", "0"))

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        'Author:Sachin Sadhu||Date:17-Jan-2014
        'Purpose: To add facility to save Billing/Shipping address on New Record Froms
        Private Sub GetLastInsertedDivision()
            Try
                Dim objContact As New CContacts
                lngDivID = objContact.GetLastDivisonID()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        'End of Code by sachin
        Sub LoadAddress()
            Try
                Dim dtTable As DataTable
                Dim objContact As New CContacts
                objContact.DomainID = Session("DomainID")
                objContact.AddressID = ddlBillAddressName.SelectedValue
                objContact.byteMode = 1
                dtTable = objContact.GetAddressDetail

                If dtTable.Rows.Count > 0 Then
                    txtAddressName.Text = CCommon.ToString(dtTable.Rows(0).Item("vcAddressName"))
                    txtStreet.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcStreet")), "", dtTable.Rows(0).Item("vcStreet"))
                    txtCity.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcCity")), "", dtTable.Rows(0).Item("vcCity"))
                    If Not IsDBNull(dtTable.Rows(0).Item("numCountry")) Then
                        If Not ddlBillCountry.Items.FindByValue(dtTable.Rows(0).Item("numCountry")) Is Nothing Then
                            ddlBillCountry.ClearSelection() ' reset selected index
                            ddlBillCountry.Items.FindByValue(dtTable.Rows(0).Item("numCountry")).Selected = True
                        End If
                    End If
                    If ddlBillCountry.SelectedIndex > 0 Then
                        FillState(ddlBillState, ddlBillCountry.SelectedItem.Value, Session("DomainID"))
                    End If
                    If Not IsDBNull(dtTable.Rows(0).Item("numState")) Then
                        If Not ddlBillState.Items.FindByValue(dtTable.Rows(0).Item("numState")) Is Nothing Then
                            ddlBillState.ClearSelection()  ' reset selected index
                            ddlBillState.Items.FindByValue(dtTable.Rows(0).Item("numState")).Selected = True
                        End If
                    End If
                    txtPostal.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcPostalCode")), "", dtTable.Rows(0).Item("vcPostalCode"))
                    chkIsPrimary.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitIsPrimary"))

                    If CCommon.ToBool(dtTable.Rows(0).Item("bitAltContact")) Then
                        rbBillingContactAlt.Checked = True
                        txtBillingContactAlt.Text = CCommon.ToString(dtTable.Rows(0).Item("vcAltContact"))
                    ElseIf Not ddlBillingContact.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0).Item("numContact"))) Is Nothing Then
                        ddlBillingContact.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0).Item("numContact"))).Selected = True
                    End If

                    Dim objOrgOppAddressModificationHistory As New BACRM.BusinessLogic.Opportunities.OrgOppAddressModificationHistory
                    objOrgOppAddressModificationHistory.DomainID = CCommon.ToLong(Session("DomainID"))
                    objOrgOppAddressModificationHistory.RecordID = lngDivID
                    objOrgOppAddressModificationHistory.AddressOfRecord = 1
                    objOrgOppAddressModificationHistory.AddressType = 1
                    Dim dtModificationHistory As DataTable = objOrgOppAddressModificationHistory.GetModificationDetail()

                    If Not dtModificationHistory Is Nothing AndAlso dtModificationHistory.Rows.Count > 0 Then
                        lblBillHistory.Text = CCommon.ToString(dtModificationHistory.Rows(0)("vcModifiedBy")) & ", " & CCommon.ToString(dtModificationHistory.Rows(0)("dtModifiedDate"))
                    Else
                        lblBillHistory.Text = ""
                    End If
                Else
                    txtAddressName.Text = ""
                    txtStreet.Text = ""
                    txtPostal.Text = ""
                    txtCity.Text = ""
                    ddlBillState.SelectedValue = 0
                    'ddlBillCountry.SelectedValue = 0
                    chkIsPrimary.Checked = False
                    lblBillHistory.Text = ""
                End If

                objContact.DomainID = Session("DomainID")
                objContact.AddressID = ddlShipAddressName.SelectedValue
                objContact.byteMode = 1
                dtTable = objContact.GetAddressDetail

                If dtTable.Rows.Count > 0 Then

                    If (chkDropShip.Checked = False) Then
                        txtShipAddressName.Text = CCommon.ToString(dtTable.Rows(0).Item("vcAddressName"))
                        txtShipStreet.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcStreet")), "", dtTable.Rows(0).Item("vcStreet"))
                        txtShipCity.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcCity")), "", dtTable.Rows(0).Item("vcCity"))
                        If Not IsDBNull(dtTable.Rows(0).Item("numCountry")) Then
                            If Not ddlShipCountry.Items.FindByValue(dtTable.Rows(0).Item("numCountry")) Is Nothing Then
                                ddlShipCountry.ClearSelection() ' reset selected index
                                ddlShipCountry.Items.FindByValue(dtTable.Rows(0).Item("numCountry")).Selected = True
                            End If
                        End If
                        If ddlShipCountry.SelectedIndex > 0 Then
                            FillState(ddlShipState, ddlShipCountry.SelectedItem.Value, Session("DomainID"))
                        End If
                        If Not IsDBNull(dtTable.Rows(0).Item("numState")) Then
                            If Not ddlShipState.Items.FindByValue(dtTable.Rows(0).Item("numState")) Is Nothing Then
                                ddlShipState.ClearSelection() ' reset selected index
                                ddlShipState.Items.FindByValue(dtTable.Rows(0).Item("numState")).Selected = True
                            End If
                        End If
                        txtShipPostal.Text = IIf(IsDBNull(dtTable.Rows(0).Item("vcPostalCode")), "", dtTable.Rows(0).Item("vcPostalCode"))
                        chkShipIsPrimary.Checked = CCommon.ToBool(dtTable.Rows(0).Item("bitIsPrimary"))

                        If CCommon.ToBool(dtTable.Rows(0).Item("bitAltContact")) Then
                            rbShippingContactAlt.Checked = True
                            txtShippingContactAlt.Text = CCommon.ToString(dtTable.Rows(0).Item("vcAltContact"))
                        ElseIf Not ddlShippingContact.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0).Item("numContact"))) Is Nothing Then
                            ddlShippingContact.Items.FindByValue(CCommon.ToLong(dtTable.Rows(0).Item("numContact"))).Selected = True
                        End If

                        Dim objOrgOppAddressModificationHistory As New BACRM.BusinessLogic.Opportunities.OrgOppAddressModificationHistory
                        objOrgOppAddressModificationHistory.DomainID = CCommon.ToLong(Session("DomainID"))
                        objOrgOppAddressModificationHistory.RecordID = lngDivID
                        objOrgOppAddressModificationHistory.AddressOfRecord = 1
                        objOrgOppAddressModificationHistory.AddressType = 2
                        Dim dtModificationHistory As DataTable = objOrgOppAddressModificationHistory.GetModificationDetail()

                        If Not dtModificationHistory Is Nothing AndAlso dtModificationHistory.Rows.Count > 0 Then
                            lblShipHistory.Text = CCommon.ToString(dtModificationHistory.Rows(0)("vcModifiedBy")) & ", " & CCommon.ToString(dtModificationHistory.Rows(0)("dtModifiedDate"))
                        Else
                            lblShipHistory.Text = ""
                        End If
                    End If

                Else
                    txtShipAddressName.Text = ""
                    txtShipStreet.Text = ""
                    txtShipPostal.Text = ""
                    txtShipCity.Text = ""
                    ddlShipState.SelectedValue = 0
                    'ddlShipState.SelectedValue = 0
                    chkShipIsPrimary.Checked = False
                    lblShipHistory.Text = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                If save() Then
                    BindAddressName()
                    LoadAddress()
                    AssignDropShipValues()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                If save() Then
                    BindAddressName()

                    AssignDropShipValues()

                    If CCommon.ToInteger(GetQueryStringVal("Reload")) = 1 Then
                        Dim strScript As String = "<script language='javascript'>opener.__doPostBack('btnReloadAddress', ''); self.close();</script>"
                        If (Not ClientScript.IsStartupScriptRegistered("clientScript")) Then ClientScript.RegisterStartupScript(Me.GetType, "clientScript", strScript)
                    Else
                        Dim strScript As String = "<script language=JavaScript>self.close()</script>"
                        If (Not ClientScript.IsStartupScriptRegistered("clientScript")) Then ClientScript.RegisterStartupScript(Me.GetType, "clientScript", strScript)
                    End If

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Function save() As Boolean
            Try
                If txtAddressName.Text.Trim() = "" Then
                    litMessage.Text = "Please enter billing address name"
                    Return False
                End If

                If (chkDropShip.Checked = False) Then
                    If txtShipAddressName.Text.Trim() = "" Then
                        litMessage.Text = "Please enter shipping address name"
                        Return False
                    End If
                End If
                '  ClientScript.RegisterClientScriptBlock(Me.GetType(), "popup", "PopupScript();", True)
                'Update values
                Dim objContacts As New CContacts
                objContacts.AddressID = ddlBillAddressName.SelectedValue
                objContacts.AddressName = IIf(String.IsNullOrEmpty(txtAddressName.Text.Trim), "Billing Address", txtAddressName.Text.Trim)
                objContacts.Street = txtStreet.Text.Trim
                objContacts.City = txtCity.Text.Trim
                objContacts.Country = ddlBillCountry.SelectedItem.Value
                objContacts.PostalCode = txtPostal.Text.Trim
                objContacts.State = ddlBillState.SelectedItem.Value
                objContacts.AddresOf = CContacts.enmAddressOf.Organization
                objContacts.AddressType = CContacts.enmAddressType.BillTo
                objContacts.IsPrimaryAddress = chkIsPrimary.Checked
                objContacts.RecordID = lngDivID
                objContacts.DomainID = Session("DomainID")
                objContacts.UserCntID = Session("UserContactID")
                If rbBillingContactAlt.Checked Then
                    objContacts.ContactID = 0
                    objContacts.IsAltContact = True
                    objContacts.AltContact = txtBillingContactAlt.Text.Trim()
                Else
                    objContacts.IsAltContact = False
                    objContacts.ContactID = CCommon.ToLong(ddlBillingContact.SelectedValue)
                End If
                objContacts.ManageAddress()
                hdnBillToAddressID.Value = objContacts.AddressID

                If (chkDropShip.Checked = False) Then

                    objContacts.AddressID = ddlShipAddressName.SelectedValue
                    objContacts.AddressName = IIf(String.IsNullOrEmpty(txtShipAddressName.Text.Trim), "Shipping Address", txtShipAddressName.Text.Trim)
                    objContacts.Street = txtShipStreet.Text.Trim
                    objContacts.City = txtShipCity.Text.Trim
                    objContacts.Country = ddlShipCountry.SelectedValue
                    objContacts.PostalCode = txtShipPostal.Text.Trim
                    objContacts.State = ddlShipState.SelectedValue
                    objContacts.AddresOf = CContacts.enmAddressOf.Organization
                    objContacts.AddressType = CContacts.enmAddressType.ShipTo
                    objContacts.IsPrimaryAddress = chkShipIsPrimary.Checked
                    objContacts.RecordID = lngDivID
                    objContacts.DomainID = Session("DomainID")
                    objContacts.UserCntID = Session("UserContactID")
                    If rbShippingContactAlt.Checked Then
                        objContacts.ContactID = 0
                        objContacts.IsAltContact = True
                        objContacts.AltContact = txtShippingContactAlt.Text.Trim()
                    Else
                        objContacts.IsAltContact = False
                        objContacts.ContactID = CCommon.ToLong(ddlShippingContact.SelectedValue)
                    End If
                    objContacts.ManageAddress()
                    hdnShipToAddressID.Value = objContacts.AddressID
                End If

                If CCommon.ToInteger(GetQueryStringVal("Reload")) = 2 Then
                    Dim strScript As String = "<script language='javascript'>window.opener.UpdateAddress(" & hdnBillToAddressID.Value & "," & hdnShipToAddressID.Value & ");</script>"
                    If (Not ClientScript.IsStartupScriptRegistered("clientScript")) Then ClientScript.RegisterStartupScript(Me.GetType, "clientScript", strScript)
                End If

                Return True
                'objContacts.BillingAddress = IIf(chkSameAddr.Checked = True, 1, 0)
                'objContacts.ShipStreet = txtShipStreet.Text
                'objContacts.ShipState = ddlShipState.SelectedItem.Value
                'objContacts.ShipPostal = txtShipPostal.Text
                'objContacts.ShipCountry = ddlShipCountry.SelectedItem.Value
                'objContacts.ShipCity = txtShipCity.Text
                'objContacts.DivisionID = lngDivID
                'objContacts.UpdateCompanyAddress()

                'If txtStreet.Text <> "" Then strAdd = txtStreet.Text
                'If txtCity.Text <> "" Then
                '    If strAdd = "" Then
                '        strAdd = txtCity.Text
                '    Else : strAdd = strAdd & "," & txtCity.Text
                '    End If
                'End If
                'If ddlBillState.SelectedValue <> 0 Then
                '    If strAdd = "" Then
                '        strAdd = ddlBillState.SelectedItem.Text
                '    Else : strAdd = strAdd & "," & ddlBillState.SelectedItem.Text
                '    End If
                'End If
                'If txtPostal.Text <> "" Then
                '    If strAdd = "" Then
                '        strAdd = txtPostal.Text
                '    Else : strAdd = strAdd & "," & txtPostal.Text
                '    End If
                'End If
                'If ddlBillCountry.SelectedValue <> 0 Then
                '    If strAdd = "" Then
                '        strAdd = ddlBillCountry.SelectedItem.Text
                '    Else : strAdd = strAdd & "," & ddlBillCountry.SelectedItem.Text
                '    End If
                'End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub ddlShipCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShipCountry.SelectedIndexChanged
            Try
                FillState(ddlShipState, ddlShipCountry.SelectedItem.Value, Session("DomainID"))
                If Not String.IsNullOrEmpty(hdnShipState.Value) Then
                    If Not ddlShipState.Items.FindByText(hdnShipState.Value) Is Nothing Then
                        ddlShipState.Items.FindByText(hdnShipState.Value).Selected = True
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlBillCountry_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBillCountry.SelectedIndexChanged
            Try
                FillState(ddlBillState, ddlBillCountry.SelectedItem.Value, Session("DomainID"))
                If Not String.IsNullOrEmpty(hdnBillState.Value) Then
                    If Not ddlBillState.Items.FindByText(hdnBillState.Value) Is Nothing Then
                        ddlBillState.Items.FindByText(hdnBillState.Value).Selected = True
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub chkSameAddr_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkSameAddr.CheckedChanged
            Try
                If chkSameAddr.Checked Then
                    txtShipStreet.Text = txtStreet.Text
                    txtShipCity.Text = txtCity.Text
                    txtShipPostal.Text = txtPostal.Text
                    ddlShipCountry.SelectedValue = ddlBillCountry.SelectedValue
                    If ddlShipCountry.SelectedIndex > 0 Then
                        FillState(ddlShipState, CCommon.ToLong(ddlShipCountry.SelectedValue), CCommon.ToLong(Session("DomainID")))
                    End If
                    ddlShipState.SelectedValue = ddlBillState.SelectedValue
                Else
                    txtShipStreet.Text = ""
                    txtShipCity.Text = ""
                    txtShipPostal.Text = ""
                    ddlShipCountry.SelectedIndex = 0
                    ddlShipState.SelectedIndex = 0
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlBillAddressName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBillAddressName.SelectedIndexChanged
            Try
                LoadAddress()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlShipAddressName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlShipAddressName.SelectedIndexChanged
            Try
                LoadAddress()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                BindAddressName()
                LoadAddress()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lbBillDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbBillDelete.Click
            Try
                If ddlBillAddressName.SelectedValue > 0 Then
                    Dim objContact As New CContacts
                    objContact.DomainID = Session("DomainID")
                    objContact.AddressID = ddlBillAddressName.SelectedValue
                    Try
                        objContact.DeleteAddress()
                    Catch ex As Exception
                        If ex.Message = "PRIMARY" Then
                            litMessage.Text = "Can not delete Primary Address!!"
                        Else
                            Throw ex
                        End If
                    End Try
                    BindAddressName()
                    LoadAddress()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lbShipDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbShipDelete.Click
            Try
                If ddlShipAddressName.SelectedValue > 0 Then
                    Dim objContact As New CContacts
                    objContact.DomainID = Session("DomainID")
                    objContact.AddressID = ddlShipAddressName.SelectedValue
                    Try
                        objContact.DeleteAddress()
                    Catch ex As Exception
                        If ex.Message = "PRIMARY" Then
                            litMessage.Text = "Can not delete Primary Address!!"
                        Else
                            Throw ex
                        End If
                    End Try
                    BindAddressName()
                    LoadAddress()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub chkDropShip_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles chkDropShip.CheckedChanged
            Try
                If chkDropShip.Checked = True Then
                    EnableDisableOnDropShip(False)
                    rbShippingContactAlt.Checked = True
                    rbShippingContact.Checked = False
                Else
                    EnableDisableOnDropShip(True)
                    rbShippingContactAlt.Checked = False
                    rbShippingContact.Checked = True
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Protected Function EnableDisableOnDropShip(ByVal CheckUncheckValue As Boolean)

            ddlBillAddressName.Enabled = CheckUncheckValue
            lbBillDelete.Enabled = CheckUncheckValue
            ddlShipAddressName.Enabled = CheckUncheckValue
            lbShipDelete.Enabled = CheckUncheckValue
            chkSameAddr.Enabled = CheckUncheckValue
            rbBillingContact.Enabled = CheckUncheckValue
            ddlBillingContact.Enabled = CheckUncheckValue
            rbBillingContactAlt.Enabled = CheckUncheckValue
            txtBillingContactAlt.Enabled = CheckUncheckValue
            rbShippingContact.Enabled = CheckUncheckValue
            ddlShippingContact.Enabled = CheckUncheckValue
            txtAddressName.Enabled = CheckUncheckValue
            txtShipAddressName.Enabled = CheckUncheckValue
            txtStreet.Enabled = CheckUncheckValue
            txtCity.Enabled = CheckUncheckValue
            ddlBillState.Enabled = CheckUncheckValue
            txtPostal.Enabled = CheckUncheckValue
            ddlBillCountry.Enabled = CheckUncheckValue
            chkIsPrimary.Enabled = CheckUncheckValue
            chkShipIsPrimary.Enabled = CheckUncheckValue
            ddlBillingContact.Enabled = CheckUncheckValue
        End Function

        Protected Function AssignDropShipValues()
            Dim DropShipValue As String = "0"
            Dim ShipState As String = ""
            If chkDropShip.Checked Then
                DropShipValue = "1"
            End If
            If (ddlShipState.SelectedValue <> "0") Then
                ShipState = ddlShipState.SelectedItem.Text
            End If

            Dim strScript1 As String = "<script language='javascript'>opener.document.getElementById('hdnDropShp').value = '" & DropShipValue & "' ; opener.document.getElementById('hdnDropShpStreet').value = '" & txtShipStreet.Text & "' ; " _
            & "opener.document.getElementById('hdnDropShpCity').value = '" & txtShipCity.Text & "' ; opener.document.getElementById('hdnDropShpStateID').value = '" & ddlShipState.SelectedValue & "' ; " _
            & "opener.document.getElementById('hdnDropShpStateName').value = '" & ShipState & "' ; opener.document.getElementById('hdnDropShpPostal').value = '" & txtShipPostal.Text & "' ; " _
            & "opener.document.getElementById('hdnDropShpCountryId').value = '" & ddlShipCountry.SelectedValue & "' ; opener.document.getElementById('hdnDropShpCountryName').value = '" & ddlShipCountry.SelectedItem.Text & "' ; " _
            & "opener.document.getElementById('hdnDropShipAltShippingcontact').value = '" & txtShippingContactAlt.Text & "' ; </script>"
            If (Not ClientScript.IsStartupScriptRegistered("clientScript1")) Then
                ClientScript.RegisterStartupScript(Me.GetType, "clientScript1", strScript1)
            End If

        End Function

    End Class
End Namespace
