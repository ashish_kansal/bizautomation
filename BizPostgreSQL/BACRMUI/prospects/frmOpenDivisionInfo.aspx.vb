﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Account

Public Class frmOpenDivisionInfo
    Inherits BACRMPage

    Public lngDivID As Long
    Public intType As Integer

    Public intYear As Integer
    Public intQuarter As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngDivID = CCommon.ToLong(GetQueryStringVal( "DivID"))
            intType = CCommon.ToInteger(GetQueryStringVal( "type"))

            intQuarter = CCommon.ToInteger(GetQueryStringVal( "Quarter"))
            intYear = CCommon.ToInteger(GetQueryStringVal( "Year"))

            hdnType.Value = intType
            If Not IsPostBack Then
                If intType = 1 Then
                    lblTitle.Text = "Open Projects"
                    GetProjects()
                ElseIf intType = 2 Then
                    lblTitle.Text = "Open Cases"
                    GetCases()
                ElseIf intType = 3 Then
                    lblTitle.Text = "Open Sales Opportunities/Orders"
                    GetOppOrder(1)
                ElseIf intType = 4 Then
                    lblTitle.Text = "Open Purchase Opportunities/Orders"
                    GetOppOrder(2)
                ElseIf intType = 5 Then
                    lblTitle.Text = "Sales Orders"
                    GetForcastingOrder(1, 1, 3)
                ElseIf intType = 6 Then
                    lblTitle.Text = "Sales Opportunities"
                    GetForcastingOrder(1, 0, 4)
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub GetForcastingOrder(ByVal Opptype As Integer, ByVal OppStatus As Integer, ByVal mode As Integer)
        Try
            Dim objProspects As New CProspects
            objProspects.DomainID = Session("DomainID")

            objProspects.OppType = Opptype
            objProspects.ByteMode = mode
            objProspects.OppStatus = OppStatus
            Dim dtOrders As DataTable
            dtOrders = objProspects.GetOppDetailsForOrg(intQuarter, intYear)

            dgOppOrder.DataSource = dtOrders
            dgOppOrder.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub GetOppOrder(ByVal Opptype As Integer)
        Try
            Dim objProspects As New CProspects

            objProspects.DivisionID = lngDivID
            objProspects.DomainID = Session("DomainID")

            'Order
            objProspects.OppType = Opptype
            objProspects.ByteMode = 0
            Dim dtOrders As DataTable
            dtOrders = objProspects.GetOppDetailsForOrg

            'Oppertunity
            objProspects.OppStatus = 0
            objProspects.ByteMode = 2
            Dim dtOpportunities As DataTable
            dtOpportunities = objProspects.GetOppDetailsForOrg()

            dtOrders.Merge(dtOpportunities)

            dgOppOrder.DataSource = dtOrders
            dgOppOrder.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub GetProjects()
        Try
            Dim objProspects As New CProspects

            objProspects.DivisionID = lngDivID
            objProspects.ProjectStatus = 0
            objProspects.DomainID = Session("DomainID")
            objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            dgProjectsOpen.DataSource = objProspects.GetProjectsForOrg
            dgProjectsOpen.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub GetCases()
        Try
            Dim objAccounts As New CAccounts

            objAccounts.DivisionID = lngDivID
            Dim dtCases As DataTable
            dtCases = objAccounts.GetOpenCases
            dgOpenCases.DataSource = dtCases
            dgOpenCases.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Function ReturnDateTime(ByVal CloseDate) As String
        Try
            Dim strTargetResolveDate As String = ""
            If Not IsDBNull(CloseDate) And IsDate(CloseDate) = True Then
                strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
            End If
            Return strTargetResolveDate
        Catch ex As Exception
            Throw ex
        End Try
    End Function
End Class