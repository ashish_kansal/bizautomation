<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmProspectsAdd.aspx.vb"
    Inherits="BACRM.UserInterface.Prospects.frmProspectsAdd" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Add/Edit Address</title>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKZ2Kph5NQXTnPc1xEzM4Bq0nhzXjONS0&libraries=places" type="text/javascript"></script>
    <script src="../JavaScript/GooglePlace.js" type="text/javascript"></script>
    <script src="../JavaScript/smartystreetsAPI/VerifyAddress.js"></script>
    <script language="javascript" type="text/javascript">
          function ChkUncheckDropShip() {

              var chkDropShip = document.getElementById("chkDropShp").checked;              

              if (chkDropShip == true) {
                  
                  document.getElementById("ddlBillAddressName").setAttribute('disabled', true);
                  document.getElementById("lbBillDelete").style.pointerEvents = 'none';
                  document.getElementById("ddlShipAddressName").setAttribute('disabled', true);
                  document.getElementById("lbShipDelete").style.pointerEvents = 'none';
                  document.getElementById("hplLeadTimes").style.pointerEvents = 'none';                

                  document.getElementById("chkSameAddr").setAttribute('disabled', true);
                  document.getElementById("rbBillingContact").setAttribute('disabled', true);
                  document.getElementById("ddlBillingContact").setAttribute('disabled', true);
                  document.getElementById("rbBillingContactAlt").setAttribute('disabled', true);
                  document.getElementById("txtBillingContactAlt").setAttribute('disabled', true);
                  document.getElementById("rbShippingContact").setAttribute('disabled', true);
                  document.getElementById("ddlShippingContact").setAttribute('disabled', true);
                  document.getElementById("txtAddressName").setAttribute('disabled', true);
                  document.getElementById("txtShipAddressName").setAttribute('disabled', true);
                  document.getElementById("txtStreet").setAttribute('disabled', true);
                  document.getElementById("txtCity").setAttribute('disabled', true);
                  document.getElementById("ddlBillState").setAttribute('disabled', true);
                  document.getElementById("txtPostal").setAttribute('disabled', true);
                  document.getElementById("ddlBillCountry").setAttribute('disabled', true);
                  document.getElementById("chkIsPrimary").setAttribute('disabled', true);
                  document.getElementById("chkShipIsPrimary").setAttribute('disabled', true);
                  document.getElementById("ddlBillingContact").setAttribute('disabled', true);
                  document.getElementById("rbShippingContactAlt").checked = true;
                    
              }
              else {

                  document.getElementById("ddlBillAddressName").disabled = false;
                  document.getElementById("lbBillDelete").style.pointerEvents = 'auto';
                  document.getElementById("ddlShipAddressName").setAttribute('disabled', false);
                  document.getElementById("lbShipDelete").style.pointerEvents = 'auto';
                  document.getElementById("hplLeadTimes").style.pointerEvents = 'auto';
                  document.getElementById("chkSameAddr").setAttribute('disabled', false);
                  document.getElementById("rbBillingContact").setAttribute('disabled', false);
                  document.getElementById("ddlBillingContact").setAttribute('disabled', false);
                  document.getElementById("rbBillingContactAlt").setAttribute('disabled', false);
                  document.getElementById("txtBillingContactAlt").setAttribute('disabled', false);
                  document.getElementById("rbShippingContact").setAttribute('disabled', false);
                  document.getElementById("ddlShippingContact").setAttribute('disabled', false);
                  document.getElementById("txtAddressName").setAttribute('disabled', false);
                  document.getElementById("txtShipAddressName").setAttribute('disabled', false);
                  document.getElementById("txtStreet").setAttribute('disabled', false);
                  document.getElementById("txtCity").setAttribute('disabled', false);
                  document.getElementById("ddlBillState").setAttribute('disabled', false);
                  document.getElementById("txtPostal").setAttribute('disabled', false);
                  document.getElementById("ddlBillCountry").setAttribute('disabled', false);
                  document.getElementById("chkIsPrimary").setAttribute('disabled', false);
                  document.getElementById("chkShipIsPrimary").setAttribute('disabled', false);
                  document.getElementById("ddlBillingContact").setAttribute('disabled', false);

              }
            
        }

    </script>
    <script type="text/javascript">
        function NewAddress(a, b, c) {
            window.open("../Contact/frmNewAddress.aspx?RecordID=" + a + "&AddressOf=" + b + "&AddressType=" + c, 'WebLinkLabel', 'width=500,height=275,status=no,scrollbar=yes,top=110,left=150');
            return false;
        }
        function Reload() {
            document.ge("btnGo").click();
            return true;
        }
        function VerifyAddress(type) {
            //var smartystreetsKey = "3359218178746496";hdnSmartyStreetsAPIKeys
            var smartystreetsKey = document.getElementById("hdnSmartyStreetsAPIKeys").value;
            if (type == "0") {
                var selectBillCountryoption = document.getElementById("ddlBillCountry");
                var selectBillStateoption = document.getElementById("ddlBillState");
                
                getVerifyAddress(selectBillCountryoption.options[selectBillCountryoption.selectedIndex].text, document.getElementById("txtStreet").value, document.getElementById("txtPostal").value,
                    document.getElementById("txtCity").value, selectBillStateoption.options[selectBillStateoption.selectedIndex].text, type, smartystreetsKey);
            } else {

                var selectBillCountryoption = document.getElementById("ddlShipCountry");
                var selectBillStateoption = document.getElementById("ddlShipState");
                getVerifyAddress(selectBillCountryoption.options[selectBillCountryoption.selectedIndex].text, document.getElementById("txtShipStreet").value, document.getElementById("txtShipPostal").value,
                    document.getElementById("txtShipCity").value, selectBillStateoption.options[selectBillStateoption.selectedIndex].text, type, smartystreetsKey);
            }
        }
        $(document).ready(function () {
            initializeaddress("txtStreet", "txtStreet", "txtCity", "hdnBillState", "txtPostal", "ddlBillCountry");
            initializeshipaddress("txtShipStreet", "txtShipStreet", "txtShipCity", "hdnShipState", "txtShipPostal", "ddlShipCountry");
        });

    </script>
    <style type="text/css">
        #tblAddress td:nth-child(2n+1) {
            font-weight: bold;
            padding-right: 5px;
        }

        #tblAddress tr {
            height: 30px;
        }

        .disabledlink {
            cursor: not-allowed;
            opacity: 0.5;
            text-decoration: none;
        }
        .textDesign {
    display: inline;
    padding: .2em .6em .3em;
    font-size: 13px;
    font-weight: 500;
    line-height: 1;
    color: #fff;
    text-align: center;
    white-space: nowrap;
    vertical-align: baseline;
    border-radius: .25em;
}
        .text-default {
    background-color: #0278de4f;
    color: #000;
}.text-success {
    background-color: #26d22652;
    color: #000;
}.text-danger {
    background-color: #cc201b99;
    color: #000;
}
    </style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save"></asp:Button>
            <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save &amp; Close"></asp:Button>
            <asp:Button ID="btnCancel" runat="server" CssClass="button" Width="50" Text="Close"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblHeader" runat="server" Text="Add/Edit Address"></asp:Label>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table id="tblAddress" style="white-space:nowrap" width="1024" border="0">
        <tr>
            <td class="normal4" align="center" colspan="4">
                <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
            </td>
        </tr>
        <tr>
            <td class="text_bold" align="right">&nbsp;Billing Address
            </td>
            <td class="text">
                <asp:DropDownList runat="server" ID="ddlBillAddressName" CssClass="signup" AutoPostBack="true">
                </asp:DropDownList>
                &nbsp;<asp:LinkButton Text="Delete" ID="lbBillDelete" runat="server" CssClass="signup" />
                <asp:HiddenField ID="hdnSmartyStreetsAPIKeys" runat="server" />
                <span id="divBillingAddrVerification" runat="server">
                <button class="btn btn-sm btn-primary" type="button" value="Verify Addreess" onclick="VerifyAddress(0)">Verify Addreess</button>
                <label id="verifyBillingAdressStatus"></label>
                    </span>
            </td>
            <td class="text_bold" align="right">Shipping Address
            </td>
            <td class="text">
                <asp:DropDownList runat="server" ID="ddlShipAddressName" CssClass="signup" AutoPostBack="true">
                </asp:DropDownList>
                &nbsp;<asp:LinkButton Text="Delete" ID="lbShipDelete" runat="server" CssClass="signup" />
                <asp:HiddenField ID="hdnVendor" runat="server" />
                <span id="divShippingAddrVerification" runat="server">
                <button class="btn btn-sm btn-primary" type="button" value="Verify Addreess" onclick="VerifyAddress(1)">Verify Addreess</button>
                <label id="verifyShippingAdressStatus"></label></span>
            </td>
        </tr>
        <tr>
            <td colspan="3"></td>         
           <td>
                <asp:CheckBox ID="chkSameAddr" Text="Same as Billing" runat="server" AutoPostBack="true"
                    CssClass="signup" TabIndex="8" Font-Bold="true"></asp:CheckBox> &nbsp;
               <%-- <label>
                <input type="checkbox" id="chkDropShp" runat="server" onchange="ChkUncheckDropShip();"  />Drop-ship
                </label>--%>
                <asp:CheckBox ID="chkDropShip" Text="Drop-ship" runat="server" AutoPostBack="true"
                    CssClass="signup" TabIndex="8" Font-Bold="true"></asp:CheckBox>
            </td>
        </tr>
        <tr>
            <td></td>
            <td>
                <table style="border-spacing: 0px;">
                    <tr>
                        <td>
                            <asp:RadioButton ID="rbBillingContact" runat="server" Font-Bold="true" GroupName="BillingContact" Checked="true" Text="Billing Contact" />
                            <br />
                            <asp:DropDownList ID="ddlBillingContact" runat="server" Width="150"></asp:DropDownList>
                        </td>
                        <td>
                             <asp:RadioButton ID="rbBillingContactAlt" runat="server" Font-Bold="true" GroupName="BillingContact" Checked="false" Text="Billing Contact Alt" />
                            <br />
                            <asp:TextBox ID="txtBillingContactAlt" runat="server" Width="150"></asp:TextBox>
                        </td>
                    </tr>
                           
                </table>

            </td>
            <td></td>
            <td>
                <table style="border-spacing: 0px;">
                    <tr>
                        <td>
                            <asp:RadioButton ID="rbShippingContact" runat="server" Font-Bold="true" GroupName="ShippingContact" Checked="true" Text="Shipping Contact" />
                            <br />
                            <asp:DropDownList ID="ddlShippingContact" runat="server" Width="150"></asp:DropDownList>
                        </td>
                        <td>
                            <asp:RadioButton ID="rbShippingContactAlt" runat="server" Font-Bold="true" GroupName="ShippingContact" Checked="false" Text="Shipping Contact Alt" />
                            <br />
                            <asp:TextBox ID="txtShippingContactAlt" runat="server" Width="150"></asp:TextBox>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
        <tr>
            <td class="text" align="right">Address Name<font color="#ff3333"> *</font>
            </td>
            <td>
                <asp:TextBox ID="txtAddressName" runat="server" CssClass="signup" Width="307" TabIndex="1"></asp:TextBox>
            </td>
            <td class="text" align="right">Address Name<font color="#ff3333"> *</font>
            </td>
            <td class="text">
                <asp:TextBox ID="txtShipAddressName" runat="server" CssClass="signup" Width="307"
                    TabIndex="9"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="text" align="right">Street
            </td>
            <td>
                <asp:TextBox ID="txtStreet" runat="server" CssClass="signup" TextMode="MultiLine"
                    Width="303" TabIndex="2"></asp:TextBox>
            </td>
            <td class="text" align="right">Street
            </td>
            <td class="text">
                <asp:TextBox ID="txtShipStreet" runat="server" TextMode="MultiLine" CssClass="signup"
                    Width="303" TabIndex="10"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="text" align="right">City
            </td>
            <td>
                <asp:TextBox ID="txtCity" TabIndex="3" runat="server" CssClass="signup" Width="307"></asp:TextBox>
            </td>
            <td class="text" align="right">City
            </td>
            <td class="text">
                <asp:TextBox ID="txtShipCity" TabIndex="11" runat="server" CssClass="signup" Width="307"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="text" align="right">State
            </td>
            <td>
                <asp:DropDownList ID="ddlBillState" runat="server" TabIndex="4" Width="307" CssClass="signup">
                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                </asp:DropDownList>
                <asp:HiddenField ID="hdnBillState" runat="server" />
            </td>
            <td class="text" align="right">State
            </td>
            <td class="text">
                <asp:DropDownList ID="ddlShipState" TabIndex="12" runat="server" Width="307" CssClass="signup">
                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                </asp:DropDownList>
                <asp:HiddenField ID="hdnShipState" runat="server" />
            </td>
        </tr>
        <tr>
            <td class="text" align="right">Postal
            </td>
            <td>
                <asp:TextBox ID="txtPostal" TabIndex="5" runat="server" CssClass="signup" Width="307"></asp:TextBox>
            </td>
            <td class="text" align="right">Postal
            </td>
            <td class="text">
                <asp:TextBox ID="txtShipPostal" TabIndex="13" runat="server" CssClass="signup" Width="307"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="text" align="right">Country
            </td>
            <td>
                <asp:DropDownList ID="ddlBillCountry" TabIndex="6" AutoPostBack="True" runat="server"
                    Width="307" CssClass="signup">
                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td class="text" align="right">Country
            </td>
            <td class="text">
                <asp:DropDownList ID="ddlShipCountry" TabIndex="14" AutoPostBack="True" runat="server"
                    Width="307" CssClass="signup">
                    <asp:ListItem Value="0">--Select One--</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="text" align="right">
                <label for="chkIsPrimary">
                    Is Primary Address?</label>
            </td>
            <td>
                <asp:CheckBox Text="" runat="server" ID="chkIsPrimary" TabIndex="7" />
            </td>
            <td class="text" align="right">
                <label for="chkShipIsPrimary">
                    Is Primary Address?</label>
            </td>
            <td class="text">
                <asp:CheckBox Text="" runat="server" ID="chkShipIsPrimary" TabIndex="15" />
            </td>
        </tr>
        <tr>
            <td class="text" align="right">Last Modified By</td>
            <td>
                <asp:Literal runat="server" ID="lblBillHistory"></asp:Literal>
            </td>
            <td class="text" align="right">Last Modified By</td>
            <td>
                <asp:Literal runat="server" ID="lblShipHistory"></asp:Literal>
            </td>
        </tr>
    </table>
    <asp:Button Text="Go" runat="server" ID="btnGo" Style="display: none" />
    <asp:HiddenField ID="hdnBillToAddressID" runat="server" />
    <asp:HiddenField ID="hdnShipToAddressID" runat="server" />
     <asp:HiddenField ID="hdndropship" Value="0" runat="server" />
</asp:Content>
