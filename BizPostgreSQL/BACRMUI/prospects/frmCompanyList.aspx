<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" MasterPageFile="~/common/GridMaster.Master"
    CodeBehind="frmCompanyList.aspx.vb" Inherits="BACRM.UserInterface.Prospects.frmCompanyList" %>

<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Prospects</title>
    <script language="javascript">
        function OpenSetting() {
            var relId = document.getElementById('txtRelId').value;
            var formId = document.getElementById('txtFormId').value;
            window.open('../Prospects/frmConfCompanyList.aspx?RelId=' + relId + '&FormId=' + formId, '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function DeleteRecord() {
            var RecordIDs = GetCheckedRowValues()
            document.getElementById('txtDelContactIds').value = RecordIDs;
            if (RecordIDs.length > 0)
                return true
            else {
                alert('Please select atleast one record!!');
                return false
            }
        }

        function OpenContact(a, b) {
            var relId = document.getElementById('txtRelId').value;
            var ProfileId = document.getElementById('ddlProfile').value;

            var str;

            str = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CompanyList&ft6ty=oiuy&CntId=" + a + "&RelID=" + relId + "&profileid=" + ProfileId;


            document.location.href = str;

        }

        function OpenWindow(a, b, c) {
            var relId = document.getElementById('txtRelId').value;

            var ProfileId = document.getElementById('ddlProfile').value;
            var str;
            if (b == 0) {

                str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CompanyList&DivID=" + a + "&RelID=" + relId + "&profileid=" + ProfileId;

            }
            else if (b == 1) {

                str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CompanyList&DivID=" + a + "&RelID=" + relId + "&profileid=" + ProfileId;

            }
            else if (b == 2) {

                str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CompanyList&klds+7kldf=fjk-las&DivId=" + a + "&RelID=" + relId + "&profileid=" + ProfileId;

            }

            document.location.href = str;

        }

        function GoImport() {
            window.location.href = "../admin/importfromputlook.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospects";
            return false;
        }

        function OpenActionItem(a) {
            window.open('../Leads/frmActionItemList.aspx?DivID=' + a + '', '', 'toolbar=no,titlebar=no,top=200,left=200,width=650,height=370,scrollbars=yes,resizable=yes')
        }

        function OpenEmail(a) {
            window.open('../Leads/frmOpenEmail.aspx?DivID=' + a + '', '', 'toolbar=no,titlebar=no,top=200,left=200,width=650,height=370,scrollbars=yes,resizable=yes')
        }
        function openDrip(a) {
            window.open("../Marketing/frmConECampHstr.aspx?ConECampID=" + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=300,scrollbars=no,resizable=no')
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row" style="margin-bottom: 10px;">
    <div class="col-md-12">
            <div class="pull-left">
                <div class="form-inline">
                <label>
                View</label>
            <asp:DropDownList ID="ddlFilter" runat="server" CssClass="form-control" AutoPostBack="True">
                <asp:ListItem Value="0">--Select One--</asp:ListItem>
                <asp:ListItem Value="3" Selected="True">My Prospects</asp:ListItem>
                <asp:ListItem Value="4">All Prospects</asp:ListItem>
                <asp:ListItem Value="1">Active Prospects</asp:ListItem>
                <asp:ListItem Value="2">Inactive Prospects</asp:ListItem>
                <asp:ListItem Value="5">Rating</asp:ListItem>
                <asp:ListItem Value="6">Added in Last 7 days</asp:ListItem>
                <asp:ListItem Value="7">Last 20 Added by me</asp:ListItem>
                <asp:ListItem Value="8">Last 20 Modified by me</asp:ListItem>
                <asp:ListItem Value="9">Favorites</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;<asp:DropDownList ID="ddlRelationship" runat="server" CssClass="form-control" AutoPostBack="True"
                            Visible="False">
                        </asp:DropDownList>
                </div>
                </div>
         <div class="pull-right">
              <div class="form-inline">
                        <div class="form-group">
                            <label>Profile</label>
                            <asp:DropDownList runat="server" AutoPostBack="true" CssClass="form-control" ID="ddlProfile">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <div>
                                <button id="btnAddNewAccount" runat="server" class="btn btn-primary" ><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New</button>
                                <asp:LinkButton ID="btnAddActionItem" runat="server" CssClass="btn btn-primary" Visible="false"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Add Action Item</asp:LinkButton>
                                <asp:LinkButton ID="btnSendAnnounceMent" runat="server" CssClass="btn btn-primary" Visible="false"><i class="fa fa-envelope"></i>&nbsp;&nbsp;Send Announcement</asp:LinkButton>
                                <asp:LinkButton ID="btnGo" runat="server" style="display:none" CssClass="btn btn-primary"><i class="fa fa-share-square-o"></i>&nbsp;&nbsp;Go</asp:LinkButton>
                                 
                                <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
                        </div>
                    </div>
            </div>
             </div>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <table cellspacing="0" cellpadding="2" width="100%" border="0">
        <tr style="line-height: 43px">
            <td class="normal4" align="center">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblRelationship" runat="server"></asp:Label>&nbsp;<a href="#" onclick="return OpenHelpPopUp('relationships')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
   <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
       ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="table-responsive">
    <asp:GridView ID="gvSearch" runat="server" EnableViewState="true" AutoGenerateColumns="false"
        CssClass="table table-responsive table-bordered  table-striped" UseAccessibleHeader="true" Width="100%" ShowHeaderWhenEmpty="true">
        <AlternatingRowStyle CssClass="ais" />
        <RowStyle CssClass="is" />
        <HeaderStyle CssClass="hs" />
        <Columns>
        </Columns>
    </asp:GridView>
        </div>
 <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
     </asp:UpdateProgress>
    <asp:TextBox ID="txtDelContactIds" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtRelId" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtFormId" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
</asp:Content>
