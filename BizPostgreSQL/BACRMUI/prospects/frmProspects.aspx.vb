Imports System.IO
Imports System.Text
Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Prospects

Namespace BACRM.UserInterface.Prospects
    Public Class frmProspects : Inherits BACRMPage

        Dim objContacts As CContacts
        Dim objItems As CItems
        Dim objProspects As CProspects
        Dim objLeads As New LeadsIP
        Dim ObjCus As CustomFields
        Dim objAccounts As CAccounts
        Dim dtCompanyTaxTypes As DataTable
        Dim strColumn As String
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim boolIntermediatoryPage As Boolean = False
        Dim dtCustomFieldTable As DataTable
        Dim dtTableInfo As DataTable
        Dim objPageControls As New PageControls
        Dim lngDivID As Long
        Dim m_aryRightsForActItem(), m_aryRightsForOpp(), m_aryRightsForCustFlds(), m_aryRightsForContacts(),
       m_aryRightsForTransfer(), m_aryRightsForDemote(), m_aryRightsForFav(), m_aryRightsForAccounting(), m_aryRightsForProject(), m_aryRightsForViewLayoutButton() As Integer

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                    DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                    If Session("EnableIntMedPage") = 1 Then
                        ViewState("IntermediatoryPage") = True
                    Else
                        ViewState("IntermediatoryPage") = False
                    End If

                    m_aryRightsForViewLayoutButton = GetUserRightsForPage_Other(3, 14)
                    If m_aryRightsForViewLayoutButton(RIGHTSTYPE.VIEW) = 0 Then
                        btnLayout.Visible = False
                    End If
                End If
                boolIntermediatoryPage = ViewState("IntermediatoryPage")
                ControlSettings()

                If GetQueryStringVal("SI") <> "" Then
                    SI = GetQueryStringVal("SI")
                End If
                If GetQueryStringVal("SI1") <> "" Then
                    SI1 = GetQueryStringVal("SI1")
                Else : SI1 = 0
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    SI2 = GetQueryStringVal("SI2")
                Else : SI2 = 0
                End If
                If GetQueryStringVal("frm") <> "" Then
                    frm = GetQueryStringVal("frm")
                Else : frm = ""
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = GetQueryStringVal("frm1")
                Else : frm1 = ""
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    frm2 = GetQueryStringVal("frm2")
                Else : frm2 = ""
                End If
                If GetQueryStringVal("frm") = "SurveyRespondents" Or GetQueryStringVal("frm") = "DuplicateSearch" Then    'This special section is introduced because the screen has to open in a new window without any menus
                    'Added by Debasish Nag on 6th Jan 2006
                    Dim objForm As Object                                   'The generic object
                    objForm = Page.FindControl("webmenu1")                  'Get a holder to the web control
                    objForm.visible = False                                 'Hide the web control
                    btnCancel.Attributes.Add("onclick", "javascript: window.close();return false;") 'Write code to close the window
                End If
                lngDivID = CCommon.ToLong(GetQueryStringVal("DivID"))
                hdnDivisionID.Value = lngDivID
                Session("DivisionID") = lngDivID
                Correspondence1.lngRecordID = lngDivID
                Correspondence1.Mode = 7
                'Added this code to Hide the Asset Tab.. 18 Feb 10 - Sojan
                'radOppTab.Tabs(7).Visible = False
                GetUserRightsForPage(3, 4)

                If Not IsPostBack Then
                    lblOrganizationID.Text = "(ID:" & lngDivID.ToString() & ")"
                    objCommon = New CCommon
                    FillNetDays()
                    'objCommon.sb_FillComboFromDB(ddlNetDays, 296, Session("DomainID"))

                    objCommon.sb_FillComboFromDBwithSel(ddlProjectStatus, 439, Session("DomainID"))

                    AddToRecentlyViewed(RecetlyViewdRecordType.Account_Pospect_Lead, lngDivID)
                    objCommon.DivisionID = lngDivID
                    objCommon.charModule = "D"
                    objCommon.GetCompanySpecificValues1()
                    txtContId.Text = CStr(objCommon.ContactID)
                    ' = "Organization"
                    ' txtCurrrentPage.Text = 1
                    LoadAssociationInformation()
                    'checking rights to view the page
                    ''''sub-tab management  - added on 16/09/2009 by chintan
                    objCommon.GetAuthorizedSubTabs(radOppTab, Session("DomainID"), 12, Session("UserGroupID"))

                    Dim objCurrency As New CurrencyRates
                    objCurrency.DomainID = Session("DomainID")
                    objCurrency.GetAll = 0
                    ddlCurrency.DataSource = objCurrency.GetCurrencyWithRates()
                    ddlCurrency.DataTextField = "vcCurrencyDesc"
                    ddlCurrency.DataValueField = "numCurrencyID"
                    ddlCurrency.DataBind()
                    ddlCurrency.Items.Insert(0, "--Select One--")
                    ddlCurrency.Items.FindByText("--Select One--").Value = "0"

                    If Not ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")) Is Nothing Then
                        ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")).Selected = True
                    End If

                    '''''
                    If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                        btnSave.Visible = False
                        btnSaveClose.Visible = False
                    End If
                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnActDelete.Visible = False

                    Dim dtTab As DataTable
                    dtTab = Session("DefaultTab")
                    If dtTab.Rows.Count > 0 Then
                        radOppTab.Tabs(0).Text = IIf(IsDBNull(dtTab.Rows(0).Item("vcProspect")), "Prospect Details", dtTab.Rows(0).Item("vcProspect").ToString & " Details")
                    Else : radOppTab.Tabs(0).Text = "Prospect Details"
                    End If

                    PersistTable.Load(boolOnlyURL:=True)
                    If PersistTable.Count > 0 Then
                        radOppTab.SelectedIndex = CCommon.ToLong(PersistTable(PersistKey.SelectedTab))
                    End If

                    If radOppTab.SelectedTab IsNot Nothing Then
                        If radOppTab.SelectedTab.Visible = False Then
                            radOppTab.SelectedIndex = 0
                            radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                        Else
                            radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                        End If
                    Else
                        radOppTab.SelectedIndex = 0
                        radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                    End If

                    LoadTabDetails()
                    'txtNetDays.Attributes.Add("onkeypress", "CheckNumber(2)")
                    txtInterest.Attributes.Add("onkeypress", "CheckNumber(1)")
                    hplTransfer.Attributes.Add("onclick", "return OpenTransfer('" & "../admin/transferrecord.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospects&rtyWR=" & lngDivID & "')")
                    btnLayout.Attributes.Add("onclick", "return ShowLayout('P','" & lngDivID & "','" & IIf(CCommon.ToLong(objLeads.CompanyType) > 0, CCommon.ToLong(objLeads.CompanyType), 3) & "','35');")
                    btnSave.Attributes.Add("onclick", "return Save()")
                    btnSaveClose.Attributes.Add("onclick", "return Save()")
                    If lngDivID = Session("UserDivisionID") Then
                        btnActDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    Else : btnActDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If

                    If radOppTab.Tabs.Count > SI AndAlso SI > 0 Then radOppTab.SelectedIndex = SI
                End If
                If objLeads.CompanyType <> 0 Then
                    Session("Relation") = objLeads.CompanyType
                Else : Session("Relation") = 0
                End If
                If IsPostBack Then
                    sb_CompanyInfo()

                    If radOppTab.SelectedIndex = 1 Then
                        GetContacts()
                    End If
                End If
                OpportunitiesTab1.DivisionID = lngDivID
                m_aryRightsForCustFlds = GetUserRightsForPage_Other(3, 9)
                If m_aryRightsForCustFlds(RIGHTSTYPE.VIEW) <> 0 Then DisplayDynamicFlds()

                'IMPORTANT - KEEP IT BELOW sb_CompanyInfo call in Page_Load method
                If Not Page.IsPostBack Then
                    m_aryRightsForTransfer = GetUserRightsForPage_Other(3, 10)
                    If m_aryRightsForTransfer(RIGHTSTYPE.VIEW) = 0 Then
                        hplTransfer.Visible = False
                        hplTransferNonVis.Visible = True
                    ElseIf m_aryRightsForTransfer(RIGHTSTYPE.VIEW) = 1 Then
                        Try
                            If CCommon.ToLong(hfOwner.Value) <> Session("UserContactID") Then
                                hplTransfer.Visible = False
                                hplTransferNonVis.Visible = True
                            End If
                        Catch ex As Exception
                        End Try
                    ElseIf m_aryRightsForTransfer(RIGHTSTYPE.VIEW) = 2 Then
                        Dim i As Integer
                        Dim dtTerritory As DataTable
                        dtTerritory = Session("UserTerritory")
                        If CCommon.ToLong(hfTerritory.Value) = 0 Then
                            hplTransfer.Visible = False
                            hplTransferNonVis.Visible = True
                        Else
                            Dim chkDelete As Boolean = False
                            For i = 0 To dtTerritory.Rows.Count - 1
                                If CCommon.ToLong(hfTerritory.Value) = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                    chkDelete = True
                                End If
                            Next
                            If chkDelete = False Then
                                hplTransfer.Visible = False
                                hplTransferNonVis.Visible = True
                            End If
                        End If
                    End If

                    m_aryRightsForDemote = GetUserRightsForPage_Other(3, 11)
                    If m_aryRightsForDemote(RIGHTSTYPE.VIEW) = 0 Then
                        lbtnDemote.Visible = False
                        lbtnPromote.Visible = False
                    ElseIf m_aryRightsForDemote(RIGHTSTYPE.VIEW) = 1 Then
                        Try
                            If CCommon.ToLong(hfOwner.Value) <> Session("UserContactID") Then
                                lbtnDemote.Visible = False
                                lbtnPromote.Visible = False
                            End If
                        Catch ex As Exception
                        End Try
                    ElseIf m_aryRightsForDemote(RIGHTSTYPE.VIEW) = 2 Then
                        Dim i As Integer
                        Dim dtTerritory As DataTable
                        dtTerritory = Session("UserTerritory")
                        If CCommon.ToLong(hfTerritory.Value) = 0 Then
                            lbtnDemote.Visible = False
                            lbtnPromote.Visible = False
                        Else
                            Dim chkDelete As Boolean = False
                            For i = 0 To dtTerritory.Rows.Count - 1
                                If CCommon.ToLong(hfTerritory.Value) = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                    chkDelete = True
                                End If
                            Next
                            If chkDelete = False Then
                                lbtnDemote.Visible = False
                                lbtnPromote.Visible = False
                            End If
                        End If
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ShowDocuments(ByRef tblDocuments As HtmlTable, ByVal dtDocLinkName As DataTable, ByVal dtDisplayFields As DataTable, ByVal intSection As Integer)
            Try
                tblDocuments.Controls.Clear()
                Dim countId = 0
                Dim tr As HtmlTableRow
                For Each drDocument In dtDocLinkName.Rows
                    If (intSection = 0 AndAlso (CCommon.ToLong(drDocument("numFormFieldGroupId")) = 0 Or dtDisplayFields.Select("numFormFieldGroupId=" & CCommon.ToLong(drDocument("numFormFieldGroupId"))).Length = 0)) Or
                        (intSection <> 0 AndAlso CCommon.ToLong(drDocument("numFormFieldGroupId")) = intSection) Then
                        If countId = 0 Then
                            tr = New HtmlTableRow
                        End If

                        If CCommon.ToString(drDocument("cUrlType")) = "L" Then
                            Dim strOriginalFileName = CCommon.ToString(drDocument("VcDocName")) & CCommon.ToString(drDocument("vcFileType"))
                            Dim strDocName = CCommon.ToString(drDocument("VcFileName"))
                            Dim fileID = CCommon.ToString(drDocument("numGenericDocID"))
                            Dim strFileLogicalPath = String.Empty
                            If File.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName) Then
                                Dim f As FileInfo = New FileInfo(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName)
                                Dim strFileSize As String = f.Length.ToString
                                strFileLogicalPath = CCommon.GetDocumentPath(Session("DomainID")) & HttpUtility.UrlEncode(strDocName).Replace("+", " ")
                            End If

                            Dim docLink As New HyperLink

                            Dim imgDelete As New ImageButton
                            imgDelete.ImageUrl = "../images/Delete24.png"
                            imgDelete.Height = 13
                            imgDelete.ToolTip = "Delete"
                            imgDelete.ID = "imgDelete" + CCommon.ToString(drDocument("numGenericDocID"))
                            imgDelete.Attributes.Add("onclick", "DeleteDocumentOrLink(" & fileID & ");return false;")

                            Dim Space As New LiteralControl
                            Space.Text = "&nbsp;"

                            docLink.Text = strOriginalFileName
                            docLink.NavigateUrl = strFileLogicalPath
                            docLink.ID = "hpl" + CCommon.ToString(drDocument("numGenericDocID"))
                            docLink.Target = "_blank"
                            Dim td As New HtmlTableCell
                            td.Style.Add("border-bottom", "0px !important")
                            td.Style.Add("padding-left", "5px")
                            td.Style.Add("padding-right", "5px")
                            td.Controls.Add(docLink)
                            td.Controls.Add(Space)
                            td.Controls.Add(imgDelete)
                            tr.Controls.Add(td)
                        ElseIf CCommon.ToString(drDocument("cUrlType")) = "U" Then
                            Dim uriBuilder As UriBuilder = New UriBuilder(CCommon.ToString(drDocument("VcFileName")))

                            Dim link As New HyperLink
                            Dim imgLinkDelete As New ImageButton
                            imgLinkDelete.ImageUrl = "../images/Delete24.png"
                            imgLinkDelete.Height = 13
                            imgLinkDelete.ToolTip = "Delete"
                            imgLinkDelete.ID = "imgLinkDelete" + CCommon.ToString(drDocument("numGenericDocID"))
                            imgLinkDelete.Attributes.Add("onclick", "DeleteDocumentOrLink(" & CCommon.ToString(drDocument("numGenericDocID")) & ");return false;")

                            link.Text = CCommon.ToString(drDocument("VcDocName"))
                            link.NavigateUrl = uriBuilder.Uri.AbsoluteUri
                            link.ID = "hplLink" + CCommon.ToString(drDocument("numGenericDocID"))
                            link.Target = "_blank"

                            Dim td As New HtmlTableCell
                            td.Style.Add("border-bottom", "0px !important")
                            td.Style.Add("padding-left", "5px")
                            td.Style.Add("padding-right", "5px")
                            td.Controls.Add(link)
                            td.Controls.Add(imgLinkDelete)
                            tr.Controls.Add(td)
                        End If

                        countId = countId + 1

                        If countId = 3 Then
                            countId = 0
                            tblDocuments.Controls.Add(tr)
                        End If
                    End If
                Next

                If countId <> 0 Then
                    tblDocuments.Controls.Add(tr)
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub FillNetDays()
            Try
                Dim objOpp As New BusinessLogic.Opportunities.OppotunitiesIP
                Dim dtTerms As New DataTable
                With objOpp
                    .DomainID = Session("DomainID")
                    .TermsID = 0
                    .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    dtTerms = .GetTerms()
                End With

                If dtTerms IsNot Nothing Then
                    ddlNetDays.DataSource = dtTerms
                    ddlNetDays.DataTextField = "vcTerms"
                    ddlNetDays.DataValueField = "numTermsID"
                    ddlNetDays.DataBind()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Sub GetContacts()
            Try
                m_aryRightsForContacts = GetUserRightsForPage_Other(3, 5)
                If m_aryRightsForContacts(RIGHTSTYPE.VIEW) = 0 Then Exit Sub
                If objProspects Is Nothing Then objProspects = New CProspects
                objProspects.DivisionID = lngDivID
                objProspects.DomainID = Session("DomainID")
                objProspects.UserCntID = Session("UserContactID")
                objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objProspects.UserGroupID = Session("UserGroupID")

                Dim dsList As DataSet
                Dim dtContact As DataTable

                dsList = objProspects.GetContactInfo1
                dtContact = dsList.Tables(0)

                Dim dtTableInfo As DataTable
                dtTableInfo = dsList.Tables(1)

                Dim bField As BoundField
                Dim Tfield As TemplateField

                gvContacts.Columns.Clear()

                For Each drRow As DataRow In dtTableInfo.Rows
                    Tfield = New TemplateField

                    Tfield.HeaderTemplate = New AccountProspectContactGrid(ListItemType.Header, drRow)

                    Tfield.ItemTemplate = New AccountProspectContactGrid(ListItemType.Item, drRow)
                    gvContacts.Columns.Add(Tfield)
                Next

                Dim dr As DataRow

                'dr = dtTableInfo.NewRow()
                'dr("vcFieldName") = "New Action Item"
                'dr("vcDbColumnName") = "NewActionItem"
                'dr("vcOrigDbColumnName") = "NewActionItem"
                'dr("vcAssociatedControlType") = "NewActionItem"

                'Tfield = New TemplateField
                'Tfield.HeaderTemplate = New AccountProspectContactGrid(ListItemType.Header, dr)
                'Tfield.ItemTemplate = New AccountProspectContactGrid(ListItemType.Item, dr)
                'gvContacts.Columns.Add(Tfield)

                'dr = dtTableInfo.NewRow()
                'dr("vcFieldName") = "Last 10 Action Item Opened"
                'dr("vcDbColumnName") = "Last10ActionItemOpened"
                'dr("vcOrigDbColumnName") = "Last10ActionItemOpened"
                'dr("vcAssociatedControlType") = "Last10ActionItemOpened"

                'Tfield = New TemplateField
                'Tfield.HeaderTemplate = New AccountProspectContactGrid(ListItemType.Header, dr)
                'Tfield.ItemTemplate = New AccountProspectContactGrid(ListItemType.Item, dr)
                'gvContacts.Columns.Add(Tfield)

                'dr = dtTableInfo.NewRow()
                'dr("vcFieldName") = "Last 10 Action Item Closed"
                'dr("vcDbColumnName") = "Last10ActionItemClosed"
                'dr("vcOrigDbColumnName") = "Last10ActionItemClosed"
                'dr("vcAssociatedControlType") = "Last10ActionItemClosed"

                'Tfield = New TemplateField
                'Tfield.HeaderTemplate = New AccountProspectContactGrid(ListItemType.Header, dr)
                'Tfield.ItemTemplate = New AccountProspectContactGrid(ListItemType.Item, dr)
                'gvContacts.Columns.Add(Tfield)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Delete"
                dr("vcDbColumnName") = "Delete"
                dr("vcOrigDbColumnName") = "Delete"
                dr("vcAssociatedControlType") = "Delete"

                Tfield = New TemplateField
                Tfield.HeaderTemplate = New AccountProspectContactGrid(ListItemType.Header, dr)
                Tfield.ItemTemplate = New AccountProspectContactGrid(ListItemType.Item, dr)
                gvContacts.Columns.Add(Tfield)

                gvContacts.DataSource = dtContact
                gvContacts.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub sc_GetProjects()
            Try
                m_aryRightsForProject = GetUserRightsForPage_Other(3, 8)
                If m_aryRightsForProject(RIGHTSTYPE.VIEW) = 0 Then Exit Sub
                If objProspects Is Nothing Then objProspects = New CProspects
                objProspects.DivisionID = lngDivID
                objProspects.ProjectStatus = ddlProjectStatus.SelectedValue
                objProspects.DomainID = Session("DomainID")
                objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dgProjectsOpen.DataSource = objProspects.GetProjectsForOrg
                dgProjectsOpen.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub sb_CompanyInfo()
            Try
                objLeads.DivisionID = lngDivID
                objLeads.DomainID = Session("DomainID")
                objLeads.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objLeads.ContactID = txtContId.Text
                objLeads.GetCompanyDetails()


                If objProspects Is Nothing Then objProspects = New CProspects
                Dim dtComInfo As DataTable
                objProspects.DivisionID = lngDivID
                objProspects.DomainID = Session("DomainID")
                objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtComInfo = objProspects.GetCompanyInfoForEdit
                If Not IsPostBack Then
                    objCommon.sb_FillComboFromDBwithSel(ddlCreditLimit, 3, Session("DomainID"))
                    If dtComInfo.Rows.Count > 0 Then
                        If CCommon.ToLong(dtComInfo.Rows(0)("numCompanyType")) = 47 Then
                            Me.Page.Title = "Vendor"
                        Else
                            Me.Page.Title = "Prospect"
                            If Not radOppTab.FindTabByValue("VendorLeadTimes") Is Nothing Then
                                radOppTab.FindTabByValue("VendorLeadTimes").Visible = False
                            End If
                        End If

                        txtROwner.Text = dtComInfo.Rows(0).Item("numRecOwner")
                        lblRecordOwner.Text = dtComInfo.Rows(0).Item("RecOwner")
                        lblCreatedBy.Text = dtComInfo.Rows(0).Item("vcCreatedBy")
                        lblLastModifiedBy.Text = dtComInfo.Rows(0).Item("vcModifiedBy")
                        hfOwner.Value = CCommon.ToLong(dtComInfo.Rows(0).Item("numRecOwner"))
                        hfTerritory.Value = CCommon.ToLong(dtComInfo.Rows(0).Item("numTerID"))

                        If CCommon.ToLong(dtComInfo.Rows(0).Item("numAssignedTo")) <> CCommon.ToLong(Session("UserContactID")) AndAlso CCommon.ToLong(Session("UserDivisionID")) <> lngDivID Then
                            GetUserRightsForRecord(MODULEID.Prospects, 4, CCommon.ToLong(hfOwner.Value), CCommon.ToLong(hfTerritory.Value))
                        End If

                        If Not IsDBNull(dtComInfo.Rows(0).Item("fltInterest")) Then
                            txtInterest.Text = String.Format("{0:#,##0.00}", dtComInfo.Rows(0).Item("fltInterest"))
                        End If
                        If Not IsDBNull(dtComInfo.Rows(0).Item("numBillingDays")) Then
                            'txtNetDays.Text = String.Format("{0:#,###}", dtComInfo.Rows(0).Item("numBillingDaysName"))

                            If Not ddlNetDays.Items.FindByValue(dtComInfo.Rows(0).Item("numBillingDays")) Is Nothing Then
                                ddlNetDays.Items.FindByValue(dtComInfo.Rows(0).Item("numBillingDays")).Selected = True
                            End If
                        End If


                        If Not IsDBNull(dtComInfo.Rows(0).Item("numCurrencyID")) AndAlso CCommon.ToLong(dtComInfo.Rows(0).Item("numCurrencyID")) > 0 Then
                            If Not ddlCurrency.Items.FindByValue(dtComInfo.Rows(0).Item("numCurrencyID")) Is Nothing Then
                                ddlCurrency.ClearSelection()
                                ddlCurrency.Items.FindByValue(dtComInfo.Rows(0).Item("numCurrencyID")).Selected = True
                            End If
                        End If
                        'If Not IsDBNull(dtComInfo.Rows(0).Item("tintBillingTerms")) Then
                        '    If dtComInfo.Rows(0).Item("tintBillingTerms") = 0 Then
                        '        chkBillinTerms.Checked = False
                        '    Else
                        '        chkBillinTerms.Checked = True
                        '        txtSummary.Text = "Net " & dtComInfo.Rows(0).Item("numBillingDaysName") & " , " & IIf(dtComInfo.Rows(0).Item("tintInterestType") = 0, "-", "+") & dtComInfo.Rows(0).Item("fltInterest") & " %"
                        '    End If
                        'End If

                        'Division

                        ''lblCustID.Text = "C" & Format(drdrCompany("numDivisionID"), "000000")
                        'lblCustomerId.Text = lngDivID.ToString & " (" & dtComInfo.Rows(0).Item("vcCompanyName") & ")"
                        lblCustomerId.Text = IIf(CCommon.ToString(objLeads.CompanyTypeName) = "", "<b>Customer :&nbsp;&nbsp;</b>" & dtComInfo.Rows(0).Item("vcCompanyName"), "<b>" & objLeads.CompanyTypeName & " :&nbsp;&nbsp;</b>" & dtComInfo.Rows(0).Item("vcCompanyName"))
                        ' txtDivision.Text = dtComInfo.Rows(0).Item("vcDivisionName")
                        'Territory
                        If objLeads.CompanyType > 0 And objLeads.CompanyType <> 46 Then 'bug id 420
                            radOppTab.Tabs(0).Text = "&nbsp;&nbsp;" & objLeads.CompanyTypeName & " Detail&nbsp;&nbsp;"
                            lblTitle.Text = objLeads.CompanyTypeName & " Details"

                            If objLeads.CompanyTypeName = "Employer" Then
                                radOppTab.Tabs.FindTabByText("Correspondence").Visible = False
                            End If
                        End If

                        If Not IsDBNull(dtComInfo.Rows(0).Item("numCompanyCredit")) Then
                            If Not ddlCreditLimit.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyCredit")) Is Nothing Then
                                ddlCreditLimit.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyCredit")).Selected = True
                            End If
                        End If

                        objProspects.DomainID = Session("DomainID")
                        dtCompanyTaxTypes = objProspects.GetCompanyTaxTypes
                        Dim dr As DataRow
                        dr = dtCompanyTaxTypes.NewRow
                        dr("numTaxItemID") = 0
                        dr("vcTaxName") = "Sales Tax(Default)"
                        dr("bitApplicable") = IIf(dtComInfo.Rows(0).Item("bitNoTax") = True, False, True)
                        dtCompanyTaxTypes.Rows.Add(dr)

                        chkTaxItems.DataTextField = "vcTaxName"
                        chkTaxItems.DataValueField = "numTaxItemID"
                        chkTaxItems.DataSource = dtCompanyTaxTypes
                        chkTaxItems.DataBind()
                        Dim i As Integer
                        For i = 0 To dtCompanyTaxTypes.Rows.Count - 1
                            If Not IsDBNull(dtCompanyTaxTypes.Rows(i).Item("bitApplicable")) Then
                                If dtCompanyTaxTypes.Rows(i).Item("bitApplicable") = True Then
                                    chkTaxItems.Items.FindByValue(dtCompanyTaxTypes.Rows(i).Item("numTaxItemID")).Selected = True
                                Else
                                    chkTaxItems.Items.FindByValue(dtCompanyTaxTypes.Rows(i).Item("numTaxItemID")).Selected = False
                                End If
                            End If
                        Next
                        'Commented by Neelam - Obsolete functionality
                        'hplDocumentCount.Attributes.Add("onclick", "return OpenDocuments(" & lngDivID & ")")
                        'hplDocumentCount.Text = "(" & CCommon.ToLong(dtComInfo.Rows(0)("DocumentCount")) & ")"
                        'imgOpenDocument.Attributes.Add("onclick", "return OpenDocuments(" & lngDivID & ")")
                        imgOpenDocument.Attributes.Add("onclick", "OpenDocumentFiles(" & lngDivID.ToString() & ");return false;")
                        hplOpenDocument.Attributes.Add("onclick", "OpenDocumentFiles(" & lngDivID.ToString() & ");return false;")
                    End If
                End If
                LoadControls()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Sub LoadControls()
            Try
                tblMain.Controls.Clear()
                Dim ds As DataSet
                Dim objPageLayout As New CPageLayout
                Dim fields() As String

                objPageLayout.CoType = "P"
                objPageLayout.UserCntID = Session("UserContactId")
                objPageLayout.RecordId = lngDivID
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.PageId = 1
                objPageLayout.numRelCntType = IIf(CCommon.ToLong(objLeads.CompanyType) > 0, CCommon.ToLong(objLeads.CompanyType), 3)
                If objPageLayout.numRelCntType <> 46 Then
                    objPageLayout.FormId = 36
                Else
                    objPageLayout.FormId = 35
                End If
                objPageLayout.PageType = 3

                ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
                dtTableInfo = ds.Tables(0)

                If Not Page.IsPostBack Then
                    Dim dsFieldGroups As New DataSet()
                    Dim objBizFormWizardFormConf As New BizFormWizardMasterConfiguration
                    objBizFormWizardFormConf.DomainID = CCommon.ToLong(Session("DomainID"))
                    objBizFormWizardFormConf.numFormID = 101
                    'numGroupID is always 0 in table so line is commented
                    'objBizFormWizardFormConf.numGroupID = CCommon.ToLong(Session("UserGroupID"))
                    objBizFormWizardFormConf.tintPageType = 4
                    dsFieldGroups = objBizFormWizardFormConf.ManageFormFieldGroup()
                    ddlSectionFile.DataSource = dsFieldGroups.Tables(0)
                    ddlSectionFile.DataValueField = "numFormFieldGroupId"
                    ddlSectionFile.DataTextField = "vcGroupName"
                    ddlSectionFile.DataBind()
                    ddlSectionFile.Items.Insert(0, New ListItem("-- Select One --", "0"))

                    ddlSectionLink.DataSource = dsFieldGroups.Tables(0)
                    ddlSectionLink.DataValueField = "numFormFieldGroupId"
                    ddlSectionLink.DataTextField = "vcGroupName"
                    ddlSectionLink.DataBind()
                    ddlSectionLink.Items.Insert(0, New ListItem("-- Select One --", "0"))
                End If

                Dim objOpp As New BusinessLogic.Opportunities.OppotunitiesIP
                Dim dtDocLinkName As New DataTable
                With objOpp
                    .DomainID = Session("DomainID")
                    .DivisionID = lngDivID
                    dtDocLinkName = .GetDocumentFilesLink("P")
                End With

                If Not dtDocLinkName Is Nothing AndAlso dtDocLinkName.Rows.Count > 0 Then
                    ShowDocuments(tblDocuments, dtDocLinkName, dtTableInfo, 0)
                End If

                Dim intCol1Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=1")
                Dim intCol2Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=2")
                Dim intCol1 As Int32 = 0
                Dim intCol2 As Int32 = 0
                Dim tblCell As TableCell

                Dim tblRow As TableRow
                Const NoOfColumns As Short = 2
                Dim ColCount As Int32 = 0
                ''If intermediatory Page is enabled then Add the text "Name" to dropdown fields
                objPageControls.CreateTemplateRow(tblMain)

                objPageControls.ContactID = objLeads.ContactID
                objPageControls.DivisionID = objLeads.DivisionID
                objPageControls.TerritoryID = objLeads.TerritoryID
                objPageControls.strPageType = "Organization"
                objPageControls.EditPermission = m_aryRightsForPage(RIGHTSTYPE.UPDATE)
                Dim dv As DataView = dtTableInfo.DefaultView
                Dim dvExcutedView As DataView = dtTableInfo.DefaultView
                Dim dvExcutedGroupsView As DataView = dtTableInfo.DefaultView
                Dim fieldGroups = (From dRow In dtTableInfo.Rows
                                    Select New With {Key .groupId = dRow("numFormFieldGroupId"), Key .groupName = dRow("vcGroupName"), Key .order = dRow("numOrder")}).Distinct()

                For Each drGroup In fieldGroups.OrderBy(Function(x) x.order)
                    tblCell = New TableCell()
                    tblRow = New TableRow
                    tblCell.ColumnSpan = 4
                    tblCell.CssClass = "tableGroupHeader"
                    tblCell.Text = CCommon.ToString(drGroup.groupName)
                    tblRow.Cells.Add(tblCell)
                    tblMain.Rows.Add(tblRow)

                    If drGroup.groupId > 0 AndAlso Not dtDocLinkName Is Nothing AndAlso dtDocLinkName.Rows.Count > 0 Then
                        If dtDocLinkName.Select("numFormFieldGroupId=" & drGroup.groupId).Length > 0 Then
                            Dim tableDocs As New HtmlTable
                            tableDocs.Style.Add("float", "right")
                            ShowDocuments(tableDocs, dtDocLinkName, dtTableInfo, drGroup.groupId)

                            tblCell = New TableCell()
                            tblRow = New TableRow
                            tblCell.ColumnSpan = 4
                            tblCell.Controls.Add(tableDocs)
                            tblRow.Cells.Add(tblCell)
                            tblMain.Rows.Add(tblRow)
                        End If
                    End If

                    dvExcutedView.RowFilter = "numFormFieldGroupId = " & drGroup.groupId
                    tblRow = New TableRow
                    tblCell = New TableCell
                    intCol1Count = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=1 AND numFormFieldGroupId='" & drGroup.groupId & "'")
                    intCol2Count = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=2 AND numFormFieldGroupId='" & drGroup.groupId & "'")
                    intCol1 = 0
                    intCol2 = 0
                    For Each drData As DataRowView In dvExcutedView
                        Dim dr As DataRow
                        dr = drData.Row
                        If boolIntermediatoryPage = True Then
                            If Not IsDBNull(dr("vcPropertyName")) And (dr("fld_type") = "SelectBox") And dr("bitCustomField") = False AndAlso dr("vcPropertyName") <> "numPartenerSource" Then
                                dr("vcPropertyName") = dr("vcPropertyName") & "Name"
                            End If
                        End If

                        If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False Then
                            If CCommon.ToString(dr("vcPropertyName")) = "vcCompactContactDetails" Then
                                Dim strData As New StringBuilder()
                                strData.Append("<a class='text-yellow' href='javascript:OpenContact(" & objLeads.ContactID & ",1)'>" & objLeads.GivenName & "</a> &nbsp;")
                                strData.Append(objLeads.ContactPhone & "&nbsp;")
                                If objLeads.PhoneExt <> "" Then
                                    strData.Append("&nbsp;(" & objLeads.PhoneExt & ")")
                                End If
                                If objLeads.Email <> "" Then
                                    strData.Append("<img src='../images/msg_unread_small.gif' email=" & objLeads.Email & " id='imgEmailPopupId' onclick='return OpemParticularEmail(" & objLeads.ContactID & ")' />")
                                End If
                                dr("vcValue") = strData
                            ElseIf CCommon.ToString(dr("vcPropertyName")) = "Purchaseincentives" Then
                                Dim strData As New StringBuilder()
                                strData.Append("<Span>" & HttpUtility.HtmlDecode(objLeads.Purchaseincentives) & "</Span>")
                                dr("vcValue") = strData
                            Else
                                dr("vcValue") = objLeads.GetType.GetProperty(dr("vcPropertyName")).GetValue(objLeads, Nothing)
                            End If
                        End If

                        If ColCount = 0 Then
                            tblRow = New TableRow
                        End If

                        If intCol1Count = intCol1 And intCol2Count > intCol1Count Then
                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)
                            ColCount = 1
                        End If

                        If (dr("fld_type") = "ListBox" Or dr("fld_type") = "SelectBox") Then
                            Dim dtData As DataTable
                            Dim dtSelOpportunity As DataTable

                            If Not IsDBNull(dr("vcPropertyName")) Then
                                If dr("vcPropertyName") = "AssignedTo" Then ' dr("numFieldId") = 64 Then
                                    Dim m_aryRightsForAssignedTo() As Integer = GetUserRightsForPage_Other(3, 128)
                                    If m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 0 Then
                                        dr("bitCanBeUpdated") = False
                                    ElseIf m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 1 Then
                                        Try
                                            If CCommon.ToLong(hfOwner.Value) <> Session("UserContactID") Then
                                                dr("bitCanBeUpdated") = False
                                            End If
                                        Catch ex As Exception
                                        End Try
                                    ElseIf m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 2 Then
                                        Dim i As Integer
                                        Dim dtTerritory As DataTable
                                        dtTerritory = Session("UserTerritory")
                                        If CCommon.ToLong(hfTerritory.Value) = 0 Then
                                            dr("bitCanBeUpdated") = False
                                        Else
                                            Dim chkDelete As Boolean = False
                                            For i = 0 To dtTerritory.Rows.Count - 1
                                                If CCommon.ToLong(hfTerritory.Value) = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                                    chkDelete = True
                                                End If
                                            Next
                                            If chkDelete = False Then
                                                dr("bitCanBeUpdated") = False
                                            End If
                                        End If
                                    Else
                                        dr("bitCanBeUpdated") = True
                                    End If

                                    If boolIntermediatoryPage = False Then
                                        If Session("PopulateUserCriteria") = 1 Then

                                            dtData = objCommon.ConEmpListFromTerritories(Session("DomainID"), 0, 0, objLeads.TerritoryID)

                                        ElseIf Session("PopulateUserCriteria") = 2 Then
                                            dtData = objCommon.ConEmpList(Session("DomainID"), Session("UserContactID"))
                                        Else
                                            dtData = objCommon.ConEmpList(Session("DomainID"), 0, 0)
                                        End If

                                    End If
                                ElseIf dr("vcPropertyName") = "AssignedToName" Then
                                    Dim m_aryRightsForAssignedTo() As Integer = GetUserRightsForPage_Other(3, 128)
                                    If m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 0 Then
                                        dr("bitCanBeUpdated") = False
                                    ElseIf m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 1 Then
                                        Try
                                            If CCommon.ToLong(hfOwner.Value) <> Session("UserContactID") Then
                                                dr("bitCanBeUpdated") = False
                                            End If
                                        Catch ex As Exception
                                        End Try
                                    ElseIf m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 2 Then
                                        Dim i As Integer
                                        Dim dtTerritory As DataTable
                                        dtTerritory = Session("UserTerritory")
                                        If CCommon.ToLong(hfTerritory.Value) = 0 Then
                                            dr("bitCanBeUpdated") = False
                                        Else
                                            Dim chkDelete As Boolean = False
                                            For i = 0 To dtTerritory.Rows.Count - 1
                                                If CCommon.ToLong(hfTerritory.Value) = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                                    chkDelete = True
                                                End If
                                            Next
                                            If chkDelete = False Then
                                                dr("bitCanBeUpdated") = False
                                            End If
                                        End If
                                    Else
                                        dr("bitCanBeUpdated") = True
                                    End If
                                ElseIf dr("vcPropertyName") = "CampaignID" Then ' dr("numFieldId") = 67 Then 'Campaign
                                    If boolIntermediatoryPage = False Then
                                        Dim objCampaign As New BusinessLogic.Reports.PredefinedReports
                                        objCampaign.byteMode = 2
                                        objCampaign.DomainID = Session("DomainID")
                                        dtData = objCampaign.GetCampaign()
                                    End If
                                ElseIf dr("vcPropertyName") = "numPartenerSource" Then
                                    objCommon.DomainID = Session("DomainID")
                                    dtData = objCommon.GetPartnerSource()
                                ElseIf Not IsDBNull(dr("numListID")) And dr("numListID") <> 0 Then
                                    If boolIntermediatoryPage = False Then
                                        If dr("ListRelID") > 0 Then
                                            objCommon.Mode = 3
                                            objCommon.DomainID = Session("DomainID")
                                            If dr("ListRelID") = 834 Then 'Live Database ID is 834 = Shipping Zone
                                                objCommon.PrimaryListItemID = objLeads.ShippingZone
                                            Else
                                                objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objLeads)
                                            End If
                                            objCommon.SecondaryListID = dr("numListId")
                                            dtData = objCommon.GetFieldRelationships.Tables(0)
                                        Else
                                            dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                        End If
                                    End If
                                End If
                            End If

                            If dr("vcDbColumnName") = "numCompanyDiff" Then
                                If boolIntermediatoryPage Then
                                    tblCell = New TableCell
                                    tblCell.Font.Bold = True
                                    tblCell.Text = IIf(dr("vcValue").ToString.Trim.Length = 0, "Company Differentiation", dr("vcValue").ToString)
                                    tblCell.HorizontalAlign = HorizontalAlign.Right

                                    tblCell.Attributes.Add("id", objPageControls.strPageType & "~" & dr("numFieldId").ToString & "~" & dr("bitCustomField").ToString & "~" & objPageControls.ContactID & "~" & objPageControls.DivisionID & "~" & objPageControls.TerritoryID & "~" & dr("ListRelID").ToString)
                                    tblCell.Attributes.Add("class", "editable_select")
                                    tblCell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                                    tblCell.Attributes.Add("onmouseout", "bgColor=''")

                                    tblRow.Cells.Add(tblCell)

                                    tblCell = New TableCell
                                    tblCell.Text = objLeads.CompanyDiffValue
                                    tblCell.Attributes.Add("id", objPageControls.strPageType & "~" & "2040" & "~" & "0" & "~" & objPageControls.ContactID & "~" & objPageControls.DivisionID & "~" & objPageControls.TerritoryID & "~" & dr("ListRelID").ToString)
                                    tblCell.Attributes.Add("class", "click")
                                    tblCell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                                    tblCell.Attributes.Add("onmouseout", "bgColor=''")

                                    tblRow.Cells.Add(tblCell)
                                Else
                                    Dim ddl As New DropDownList

                                    tblCell = New TableCell
                                    dr("vcFieldName") = dr("vcFieldName").ToString.Replace("$", "")
                                    ddl.CssClass = "signup"
                                    ddl.Width = 200
                                    ddl.ID = dr("numFieldId").ToString & dr("vcFieldName").ToString & "~" & IIf(IsDBNull(dr("numListId")), 0, dr("numListId").ToString).ToString
                                    ddl.DataSource = dtData
                                    ddl.DataValueField = dtData.Columns(0).ColumnName
                                    ddl.DataTextField = dtData.Columns(1).ColumnName
                                    ddl.DataBind()
                                    ddl.Items.Insert(0, "-- Select One --")
                                    ddl.Items.FindByText("-- Select One --").Value = "0"
                                    ddl.Enabled = True
                                    ddl.EnableViewState = False
                                    If Not IsDBNull(dr("vcValue")) Then
                                        If Not ddl.Items.FindByValue(dr("vcValue").ToString) Is Nothing Then
                                            ddl.Items.FindByValue(dr("vcValue").ToString).Selected = True
                                        End If
                                    End If
                                    tblCell.CssClass = "normal1"
                                    tblCell.Controls.Add(ddl)
                                    tblCell.HorizontalAlign = HorizontalAlign.Right
                                    tblRow.Cells.Add(tblCell)


                                    tblCell = New TableCell
                                    tblCell.Width = Unit.Percentage(25)
                                    Dim txt As TextBox
                                    txt = New TextBox
                                    txt.Width = 200
                                    txt.CssClass = "signup"
                                    txt.Text = objLeads.CompanyDiffValue
                                    txt.EnableViewState = False
                                    txt.ID = "2040" & dr("vcFieldName").ToString
                                    tblCell.CssClass = "normal1"
                                    tblCell.Controls.Add(txt)

                                    tblRow.Cells.Add(tblCell)

                                    If CLng(dr("DependentFields")) > 0 And Not boolIntermediatoryPage Then
                                        ddl.AutoPostBack = True
                                        AddHandler ddl.SelectedIndexChanged, AddressOf PopulateDependentDropdown
                                    End If
                                End If
                            ElseIf boolIntermediatoryPage Then
                                objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData)
                            Else
                                Dim ddl As DropDownList

                                If dr("vcPropertyName") = "AssignedTo" Then
                                    ddl = objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData, boolEnabled:=CCommon.ToBool(dr("bitCanBeUpdated")))
                                Else
                                    ddl = objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData)
                                End If

                                If CLng(dr("DependentFields")) > 0 And Not boolIntermediatoryPage Then
                                    ddl.AutoPostBack = True
                                    AddHandler ddl.SelectedIndexChanged, AddressOf PopulateDependentDropdown
                                End If
                            End If
                        ElseIf dr("fld_type") = "CheckBoxList" Then
                            If Not IsDBNull(dr("vcPropertyName")) Then
                                Dim dtData As DataTable

                                If boolIntermediatoryPage = False Then
                                    If dr("ListRelID") > 0 Then
                                        objCommon.Mode = 3
                                        objCommon.DomainID = Session("DomainID")
                                        If dr("ListRelID") = 834 Then 'Live Database ID is 834 = Shipping Zone
                                            objCommon.PrimaryListItemID = objLeads.ShippingZone
                                        Else
                                            objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objLeads)
                                        End If

                                        objCommon.SecondaryListID = dr("numListId")
                                        dtData = objCommon.GetFieldRelationships.Tables(0)
                                    Else
                                        dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                    End If
                                End If

                                objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData)
                            End If
                        Else
                            objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, RecordID:=lngDivID)
                        End If

                        If intCol2Count = intCol2 And intCol1Count > intCol2Count Then
                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            ColCount = 1
                        End If


                        If dr("intcoulmn") = 2 Then
                            If intCol1 <> intCol1Count Then
                                intCol1 = intCol1 + 1
                            End If

                            If intCol2 <> intCol2Count Then
                                intCol2 = intCol2 + 1
                            End If
                        End If

                        ColCount = ColCount + 1
                        If NoOfColumns = ColCount Then
                            ColCount = 0
                            tblMain.Rows.Add(tblRow)
                        End If
                    Next
                Next
                If ColCount > 0 Then
                    tblMain.Rows.Add(tblRow)
                End If

                'objPageControls.CreateComments(tblMain, objLeads.Comments, boolIntermediatoryPage)
                'Add Client Side validation for custom fields
                If Not boolIntermediatoryPage Then
                    Dim strValidation As String = objPageControls.GenerateValidationScript(dtTableInfo)
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "CFvalidation", strValidation, True)
                ElseIf Session("InlineEdit") = True And m_aryRightsForPage(RIGHTSTYPE.UPDATE) <> 0 Then
                    Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtTableInfo)
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "InlineEditValidation", strValidation, True)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveTaxTypes()
            Try
                dtCompanyTaxTypes = New DataTable
                dtCompanyTaxTypes.Columns.Add("numTaxItemID")
                dtCompanyTaxTypes.Columns.Add("bitApplicable")
                Dim dr As DataRow
                Dim i As Integer
                For i = 0 To chkTaxItems.Items.Count - 1
                    If chkTaxItems.Items(i).Selected = True Then
                        dr = dtCompanyTaxTypes.NewRow
                        dr("numTaxItemID") = chkTaxItems.Items(i).Value
                        dr("bitApplicable") = 1
                        dtCompanyTaxTypes.Rows.Add(dr)
                    End If
                Next
                Dim ds As New DataSet
                Dim strdetails As String
                dtCompanyTaxTypes.TableName = "Table"
                ds.Tables.Add(dtCompanyTaxTypes)
                strdetails = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))
                If objProspects Is Nothing Then
                    objProspects = New CProspects
                End If
                objProspects.DivisionID = lngDivID
                objProspects.strCompanyTaxTypes = strdetails
                objProspects.ManageCompanyTaxTypes()
                ds.Dispose()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                objCommon.DivisionID = lngDivID
                objCommon.charModule = "D"
                objCommon.GetCompanySpecificValues1()
                SaveProspects(objCommon.CompID)
                SaveTaxTypes()
                'sb_CompanyInfo()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                objCommon.DivisionID = lngDivID
                objCommon.charModule = "D"
                objCommon.GetCompanySpecificValues1()
                SaveProspects(objCommon.CompID)
                SaveTaxTypes()
                PageRedirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub PageRedirect()
            Try
                If GetQueryStringVal("frm1") = "ActionItem" Then
                    Response.Redirect("../admin/ActionItemDetailsOld.aspx?CommId=" & GetQueryStringVal("CommID") & "&frm=" & GetQueryStringVal("frm"))
                End If
                If GetQueryStringVal("frm") = "ActItem" Then
                    Response.Redirect("../admin/ActionItemDetailsOld.aspx?CommId=" & GetQueryStringVal("CommID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "contactlist" Then
                    Response.Redirect("../contact/frmContactList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2 & "&ContactType=" & GetQueryStringVal("ContactType"))
                ElseIf GetQueryStringVal("frm") = "opportunitylist" Then
                    Response.Redirect("../opportunity/frmOpportunityList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "tickler" Then
                    Response.Redirect("../common/frmTicklerDisplay.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "Caselist" Then
                    Response.Redirect("../cases/frmCaseList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "BestAccounts" Then
                    Response.Redirect("../reports/frmBestAccounts.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "ContactRole" Then
                    Response.Redirect("../reports/frmContactRole.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "CasesAgent" Then
                    Response.Redirect("../reports/frmCases.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "BizDocReport" Then
                    Response.Redirect("../reports/frmBizDocReport.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "Ecosystem" Then
                    Response.Redirect("../reports/frmEcosystemReport.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "BestPartners" Then
                    Response.Redirect("../reports/frmBestPartners.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "Portal" Then
                    Response.Redirect("../reports/frmSelfServicePortal.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "DealHistory" Then
                    Response.Redirect("../reports/frmRepDealHistory.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "CampDetails" Then
                    Response.Redirect("../Marketing/frmCampaignDetails.aspx?CampID=" & GetQueryStringVal("CampID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "oppdetail" Then
                    Response.Redirect("../opportunity/frmOpportunities.aspx?frm=" & GetQueryStringVal("frm1") & "&opId=" & GetQueryStringVal("opId") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "CaseDetail" Then
                    Response.Redirect("../cases/frmCases.aspx?frm=" & GetQueryStringVal("frm1") & "&CaseID=" & GetQueryStringVal("CaseID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "contactdetail" Then
                    Response.Redirect("../contact/frmContacts.aspx?frm=" & GetQueryStringVal("frm1") & "&CntId=" & GetQueryStringVal("CntID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "ProDetail" Then
                    Response.Redirect("../projects/frmProjects.aspx?frm=" & GetQueryStringVal("frm1") & "&ProId=" & GetQueryStringVal("ProId") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "CaseReport" Then
                    Response.Redirect("../reports/frmCases.aspx")
                ElseIf GetQueryStringVal("frm") = "CompanyList" Then
                    Response.Redirect("../prospects/frmCompanyList.aspx?RelId=" & GetQueryStringVal("RelId") & "&profileid=" & GetQueryStringVal("profileId") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "AvgSalesCycle1" Then
                    Response.Redirect("../reports/frmAvgSalesCycle1.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "AdvSearchSur" Then
                    Response.Redirect("../Admin/FrmAdvSurveyRes.aspx")
                ElseIf GetQueryStringVal("frm") = "doclist" Then
                    Response.Redirect("../Documents/frmDocList.aspx?Status=" & SI1 & "&Category=" & SI2)
                Else
                    'If ddlRelationhip.SelectedValue = 0 Or ddlRelationhip.SelectedValue = 46 Then
                    Response.Redirect("../prospects/frmProspectList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2 & "&RelId=" & GetQueryStringVal("RelID") & "&profileId=" & GetQueryStringVal("profileid"))
                    'Else : Response.Redirect("../prospects/frmCompanyList.aspx?RelId=" & ddlRelationhip.SelectedValue & "&profileid=" & GetQueryStringVal( "profileId") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                    'End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveProspects(ByVal CompID As Long)
            Try
                With objLeads
                    .CompanyID = CompID

                    .DivisionID = lngDivID

                    .CompanyCredit = ddlCreditLimit.SelectedItem.Value

                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                    .CRMType = 1
                    .LeadBoxFlg = 1
                    .DivisionName = ""
                    'txtDivision.Text()

                    '.BillingTerms = 0 'IIf(chkBillinTerms.Checked = True, 1, 0)
                    '.BillingDays = ddlNetDays.SelectedValue
                    '.BillingDays = IIf(txtNetDays.Text = "", 0, txtNetDays.Text)
                    .BillingTerms = IIf(ddlNetDays.SelectedValue > 0, 1, 0)  'IIf(chkBillinTerms.Checked = True, 1, 0)
                    .BillingDays = ddlNetDays.SelectedValue 'IIf(txtNetDays.Text = "", 0, txtNetDays.Text)
                    .InterestType = IIf(radPlus.Checked = True, 1, 0)
                    .Interest = IIf(Replace(txtInterest.Text, ",", "") = "", 0, Replace(txtInterest.Text, ",", ""))
                    .NoTax = IIf(chkTaxItems.Items.FindByValue(0).Selected = True, False, True)

                    For Each dr As DataRow In dtTableInfo.Rows
                        If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False And dr("bitCanBeUpdated") = True Then
                            If dr("vcDbColumnName") = "numCompanyDiff" Then
                                dr("vcFieldName") = dr("vcFieldName").ToString.Replace("$", "")

                                objLeads.CompanyDiff = CType(radOppTab.MultiPage.FindControl(dr("numFieldId").ToString & dr("vcFieldName").ToString & "~" & IIf(IsDBNull(dr("numListId")), 0, dr("numListId").ToString).ToString), DropDownList).SelectedValue
                                objLeads.CompanyDiffValue = CType(radOppTab.MultiPage.FindControl("2040" & dr("vcFieldName").ToString), TextBox).Text
                            Else
                                If dr("vcPropertyName") = "numPartenerSource" Then
                                    dr("vcPropertyName") = "PartenerSource"
                                End If
                                If dr("vcPropertyName") = "numPartenerContact" Then
                                    dr("vcPropertyName") = "PartenerContact"
                                End If
                                objPageControls.SetValueForStaticFields(dr, objLeads, radOppTab.MultiPage)
                                If dr("vcPropertyName") = "PartenerSource" Then
                                    dr("vcPropertyName") = "numPartenerSource"
                                End If
                                If dr("vcPropertyName") = "PartenerContact" Then
                                    dr("vcPropertyName") = "numPartenerContact"
                                End If
                            End If
                        End If
                    Next

                    .CurrencyID = ddlCurrency.SelectedValue

                    'objPageControls.SetValueForComments(objLeads.Comments, radOppTab)

                End With
                objLeads.vcPartnerCode = objLeads.PartnerCode
                objLeads.CompanyID = objLeads.CreateRecordCompanyInfo
                Dim lngtempDivId As Long = 0
                lngtempDivId = objLeads.DivisionID
                objLeads.DivisionID = objLeads.ManageCompanyDivisionsInfo1

                Dim objWfA As New BACRM.BusinessLogic.Workflow.Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = objLeads.DivisionID
                objWfA.SaveWFOrganizationQueue()

                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If objLeads.DivisionID > 0 Then
                    SaveCusField()
                Else
                    objLeads.DivisionID = lngtempDivId
                    DisplayError("Partner Code already Used for another organization")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub DisplayDynamicFlds()
            Try
                objPageControls.DisplayDynamicFlds(lngDivID, IIf(CCommon.ToLong(objLeads.CompanyType) > 0, CCommon.ToLong(objLeads.CompanyType), 3), Session("DomainID"), objPageControls.Location.Prospects, radOppTab, 35)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveCusField()
            Try
                objPageControls.SaveCusField(lngDivID, IIf(CCommon.ToLong(objLeads.CompanyType) > 0, CCommon.ToLong(objLeads.CompanyType), 3), Session("DomainID"), objPageControls.Location.Organization, radOppTab.MultiPage)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub gvContacts_RowCommand(sender As Object, e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvContacts.RowCommand
            Try
                If e.CommandName = "DeleteContact" Then
                    If objContacts Is Nothing Then objContacts = New CContacts

                    objContacts.ContactID = CCommon.ToLong(e.CommandArgument)
                    objContacts.DomainID = Session("DomainID")
                    Dim strError As String = objContacts.DelContact()
                    If strError = "" Then
                        GetContacts()
                    Else
                        litMessage.Text = strError
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub gvContacts_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvContacts.RowDataBound
            Try
                If e.Row.RowType = DataControlRowType.DataRow Then
                    Dim btnDelete As Button
                    Dim lnkDelete As LinkButton
                    Dim IsPrimaryContact As Boolean

                    btnDelete = e.Row.FindControl("btnDelete")
                    lnkDelete = e.Row.FindControl("lnkdelete")
                    IsPrimaryContact = DataBinder.Eval(e.Row.DataItem, "bitPrimaryContact")

                    If DataBinder.Eval(e.Row.DataItem, "numContactId") = 1 Then
                        btnDelete.Visible = False
                        lnkDelete.Visible = True
                        lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                        Exit Sub
                    End If
                    If m_aryRightsForContacts(RIGHTSTYPE.DELETE) = 0 Then
                        btnDelete.Visible = False
                        lnkDelete.Visible = True
                        lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    ElseIf m_aryRightsForContacts(RIGHTSTYPE.DELETE) = 1 Then
                        Try
                            If DataBinder.Eval(e.Row.DataItem, "numCreatedBy") = Session("UserContactID") Then
                                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                            Else
                                btnDelete.Visible = False
                                lnkDelete.Visible = True
                                lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                            End If
                        Catch ex As Exception
                        End Try
                    ElseIf m_aryRightsForContacts(RIGHTSTYPE.DELETE) = 2 Then
                        Dim i As Integer
                        Dim dtTerritory As DataTable
                        dtTerritory = Session("UserTerritory")
                        If DataBinder.Eval(e.Row.DataItem, "numTerid") = 0 Then
                            btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                        Else
                            Dim chkDelete As Boolean = False
                            For i = 0 To dtTerritory.Rows.Count - 1
                                If DataBinder.Eval(e.Row.DataItem, "numTerid") = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                    chkDelete = True
                                End If
                            Next
                            If chkDelete = True Then
                                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                            Else
                                btnDelete.Visible = False
                                lnkDelete.Visible = True
                                lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                            End If
                        End If
                    ElseIf m_aryRightsForContacts(RIGHTSTYPE.DELETE) = 3 Then
                        btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If

                    If IsPrimaryContact = True Then
                        btnDelete.Visible = False
                        lnkDelete.Visible = True
                        lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                PageRedirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        'Private Sub dgClosedOpp_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgClosedOpp.ItemCommand
        '    Try
        '        Dim lngOppID As Long
        '        lngOppID = e.Item.Cells(0).Text
        '        If e.CommandName = "Name" Then Response.Redirect("../opportunity/frmOpportunities.aspx?frm=Prospects&Opid=" & lngOppID & "&SI=" & SI1 & "&SI1=" & radOppTab.SelectedIndex & "&frm1=" & frm1 & "&frm2=" & frm2)
        '        If e.CommandName = "Contact" Then
        '            objCommon.OppID = lngOppID
        '            objCommon.charModule = "O"
        '            objCommon.GetCompanySpecificValues1()
        '            Response.Redirect("../contact/frmContacts.aspx?frm=Prospects&CntId=" & objCommon.ContactID & "&SI=" & SI1 & "&SI1=" & radOppTab.SelectedIndex & "&frm1=" & frm1 & "&frm2=" & frm2)
        '        End If
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub dgOpenOpportunty_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgOpenOpportunty.ItemCommand
        '    Try
        '        Dim lngOppID As Long
        '        lngOppID = e.Item.Cells(0).Text
        '        If e.CommandName = "Name" Then Response.Redirect("../opportunity/frmOpportunities.aspx?frm=Prospects&Opid=" & lngOppID & "&SI=" & SI1 & "&SI1=" & radOppTab.SelectedIndex & "&frm1=" & frm1 & "&frm2=" & frm2)
        '        If e.CommandName = "Contact" Then
        '            objCommon.OppID = lngOppID
        '            objCommon.charModule = "O"
        '            objCommon.GetCompanySpecificValues1()
        '            Response.Redirect("../contact/frmContacts.aspx?frm=Prospects&CntId=" & objCommon.ContactID & "&SI=" & SI1 & "&SI1=" & radOppTab.SelectedIndex & "&frm1=" & frm1 & "&frm2=" & frm2)
        '        End If
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        Private Sub dgProjectsOpen_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgProjectsOpen.ItemCommand
            Try
                Dim lngProID As Long
                lngProID = e.Item.Cells(0).Text
                If e.CommandName = "Name" Then Response.Redirect("../projects/frmProjects.aspx?frm=Prospects&ProID=" & lngProID & "&SI=" & SI1 & "&SI1=" & radOppTab.SelectedIndex & "&frm1=" & frm1 & "&frm2=" & frm2)
                If e.CommandName = "Contact" Then
                    objCommon.ProID = lngProID
                    objCommon.charModule = "P"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../contact/frmContacts.aspx?frm=Prospects&CntId=" & objCommon.ContactID & "&SI=" & SI1 & "&SI1=" & radOppTab.SelectedIndex & "&frm1=" & frm1 & "&frm2=" & frm2)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to set the Association Information.
        ''' </summary>
        ''' <remarks>
        '''     This function calls the imprints the association information on a screen label.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	01/18/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        ''' 
        Public Sub LoadAssociationInformation()
            Try
                Dim objAssociationInfo As New OrgAssociations                   'Create a new object of association
                objAssociationInfo.DomainID = Session("DomainID")               'Set the Domain Id
                objAssociationInfo.DivisionID = lngDivID                        'Set the Division Id
                Dim dtAssociationInfo As DataTable                              'Declare a new DataTable
                dtAssociationInfo = objAssociationInfo.getParentOrgForCurrentOrg 'Get the Association Info
                If dtAssociationInfo.Rows.Count > 0 Then                        'Check if association exists where there is a parent-child relationship
                    lblAssociation.Text = dtAssociationInfo.Rows(0).Item("vcData") & " of: " & "<a href=""javascript: GoOrgDetails(" & dtAssociationInfo.Rows(0).Item("numDivisionId") & "," & dtAssociationInfo.Rows(0).Item("tintCRMType") & ");""><b>" & dtAssociationInfo.Rows(0).Item("vcCompanyName") & "</b></a>, " & dtAssociationInfo.Rows(0).Item("vcDivisionName")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function ReturnName(ByVal CloseDate) As String
            Try
                Dim strTargetResolveDate As String = ""
                If Not IsDBNull(CloseDate) Then strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
                Return strTargetResolveDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActDelete.Click
            Try
                If objAccounts Is Nothing Then objAccounts = New CAccounts
                With objAccounts
                    .DivisionID = lngDivID
                    .DomainID = Session("DomainID")
                End With
                Dim strError As String = objAccounts.DeleteOrg()
                If strError = "" Then
                    'Response.Redirect("../prospects/frmProspectList.aspx", False)
                    Response.Redirect("../prospects/frmCompanyList.aspx?RelId=" & GetQueryStringVal("RelId") & "&profileid=" & GetQueryStringVal("profileid") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                Else
                    litMessage.Text = strError
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        'Private Sub ddlRelationhip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        '    Try
        '        If sender.SelectedValue > 0 Then radOppTab.Tabs(0).Text = "&nbsp;&nbsp;" & sender.SelectedItem.Text & " Detail&nbsp;&nbsp;"
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        Private Sub dlWebAnlys_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlWebAnlys.ItemCommand
            Try
                If e.CommandName = "Pages" Then
                    Dim cmd As String = CType(e.CommandSource, LinkButton).CommandName
                    dlWebAnlys.SelectedIndex = e.Item.ItemIndex
                    Session("dlIndex") = CInt(e.Item.ItemIndex) + 1
                    createMainLink()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub createMainLink()
            Try
                objLeads.DivisionID = lngDivID
                objLeads.DomainID = Session("DomainID")
                Dim ds As DataSet
                ds = objLeads.GetWebAnlysDtl
                If Session("dlIndex") <> 0 Then dlWebAnlys.SelectedIndex = Session("dlIndex") - 1
                dlWebAnlys.DataSource = ds.Tables(1)
                dlWebAnlys.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dlWebAnlys_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlWebAnlys.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.SelectedItem Then
                    objLeads.TrackingID = CType(e.Item.FindControl("lblID"), Label).Text
                    Dim dg As DataGrid
                    dg = e.Item.FindControl("dgWebAnlys")
                    dg.DataSource = objLeads.GetPagesVisited
                    dg.DataBind()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlTerriory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                Dim lngAssignTo As Long
                lngAssignTo = objLeads.AssignedTo
                Dim ddl As DropDownList
                If Not radOppTab.MultiPage.FindControl("64Assigned To") Is Nothing Then
                    ddl = radOppTab.MultiPage.FindControl("64Assigned To")
                    objCommon.sb_FillConEmpFromTerritories(ddl, Session("DomainID"), 1, 0, sender.SelectedValue)
                    If Not ddl.Items.FindByValue(lngAssignTo) Is Nothing Then ddl.Items.FindByValue(lngAssignTo).Selected = True
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        'Private Sub uwItem_ClickCellButton(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.CellEventArgs) Handles uwItem.ClickCellButton
        '    Try
        '        Dim numAItemCode As Integer = CInt(e.Cell.Tag)
        '        If objItems Is Nothing Then objItems = New CItems
        '        objItems.DivisionID = lngDivID
        '        objItems.DomainID = Session("DomainId")
        '        objItems.ItemCode = numAItemCode
        '        objItems.DeleteItemFromCmpAsset()
        '        'LoadAssets()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub uwItem_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwItem.InitializeRow
        '    Try
        '        If e.Row.HasParent = False Then
        '            e.Row.Cells.FromKey("Action").Tag = e.Row.Cells.FromKey("numAItemCode").Value
        '            e.Row.Cells.FromKey("Action").Value = "r"
        '            e.Row.Cells.FromKey("Action").Column.CellButtonStyle.CssClass = "Delete"
        '            e.Row.Cells.FromKey("Action").Column.Width = Unit.Pixel(20)
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        Private Sub radOppTab_TabClick(sender As Object, e As Telerik.Web.UI.RadTabStripEventArgs) Handles radOppTab.TabClick
            Try
                PersistTable.Clear()
                PersistTable.Add(PersistKey.SelectedTab, radOppTab.SelectedIndex)
                PersistTable.Save(boolOnlyURL:=True)

                LoadTabDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        'Modified by Neelam on 02/16/2018 - Added functionality to attach files*/
        Protected Sub btnAttach_Click(sender As Object, e As EventArgs)
            Try
                If (fileupload.PostedFile.ContentLength > 0) Then
                    Dim strFName As String()
                    Dim strFilePath, strFileName, strFileType As String
                    strFileName = Path.GetFileName(fileupload.PostedFile.FileName)
                    If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                        Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
                    End If
                    Dim newFileName As String = Path.GetFileNameWithoutExtension(strFileName) & DateTime.UtcNow.ToString("yyyyMMddhhmmssfff") & Path.GetExtension(strFileName)
                    strFilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & newFileName
                    strFName = Split(strFileName, ".")
                    strFileType = "." & strFName(strFName.Length - 1)                     'Getting the Extension of the File
                    fileupload.PostedFile.SaveAs(strFilePath)
                    UploadFile("L", strFileType, newFileName, strFName(0), "", 0, 0, "A")
                    Dim script As String = "function f(){$find(""" + RadWinDocumentFiles.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, True)
                    Response.Redirect(Request.RawUrl, False)
                Else
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "NoFiles", "alert('Please attach a file to upload.');return false;", True)
                End If
                'If rdUploadFile.UploadedFiles.Count > 0 Then '
                '    For Each postedFile As Telerik.Web.UI.UploadedFile In rdUploadFile.UploadedFiles
                '        Dim strFName As String()
                '        Dim strFilePath, strFileName, strFileType As String
                '        If Not [Object].Equals(postedFile, Nothing) Then
                '            If postedFile.ContentLength > 0 Then
                '                strFileName = postedFile.FileName
                '                If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                '                    Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
                '                End If
                '                strFilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strFileName
                '                strFName = Split(strFileName, ".")
                '                strFileType = "." & strFName(strFName.Length - 1)                     'Getting the Extension of the File
                '                postedFile.SaveAs(strFilePath)
                '                UploadFile("L", strFileType, strFileName, strFName(0), "", 0, 0, "A")
                '            Else
                '                ClientScript.RegisterClientScriptBlock(Me.GetType, "NoFiles", "alert('Please attach a file to upload.');return false;", True)
                '            End If
                '        Else
                '            ClientScript.RegisterClientScriptBlock(Me.GetType, "NoFiles", "alert('Please attach a file to upload.');return false;", True)
                '        End If
                '    Next
                '    Dim script As String = "function f(){$find(""" + RadWinDocumentFiles.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
                '    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, True)
                '    'ScriptManager.RegisterStartupScript(Page, Page.GetType(), "close", "CloseDocumentFiles();", True)
                '    GetDocumentFilesLink()
                'End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        'Modified by Neelam on 02/16/2018 - Added functionality to save link*/
        'Protected Sub btnLink_Click(sender As Object, e As EventArgs)
        '    Try
        '        RadAjaxManager1.ResponseScripts.Add("CloseDocumentFiles();")
        '        'btnLinkClose_Click(Nothing, Nothing)
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        Protected Sub btnLinkClose_Click(sender As Object, e As EventArgs)
            UploadFile("U", "", txtLinkURL.Text, txtLinkName.Text, "", 0, 0, "A")
            Response.Redirect(Request.RawUrl, False)
        End Sub

        'Modified by Neelam on 02/16/2018 - Added function to save File/Link data in the database*/
        Sub UploadFile(ByVal URLType As String, ByVal strFileType As String, ByVal strFileName As String, ByVal DocName As String, ByVal DocDesc As String,
                       ByVal DocumentStatus As Long, DocCategory As Long, ByVal strType As String)
            Try
                Dim arrOutPut As String()
                Dim objDocuments As New DocumentList()
                With objDocuments
                    .DomainID = Session("DomainId")
                    .UserCntID = Session("UserContactID")
                    .UrlType = URLType
                    .DocumentStatus = DocumentStatus
                    .DocCategory = DocCategory
                    .FileType = strFileType
                    .DocName = DocName
                    .DocDesc = DocDesc
                    .FileName = strFileName
                    .DocumentSection = strType
                    .RecID = lngDivID
                    .DocumentType = IIf(lngDivID > 0, 2, 1) '1=generic,2=specific
                    .FormFieldGroupId = IIf(URLType = "L", ddlSectionFile.SelectedValue, ddlSectionLink.SelectedValue)
                    arrOutPut = .SaveDocuments()
                End With
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Protected Sub btnDeleteDocOrLink_Click(sender As Object, e As EventArgs)
            Dim objDocuments As New DocumentList()
            objDocuments.GenDocID = CCommon.ToLong(hdnFileId.Value)
            objDocuments.DeleteDocumentOrLink()
            LoadControls()
        End Sub

        Private Sub GetWebAnalysis()
            Try
                objLeads.DivisionID = lngDivID
                objLeads.DomainID = Session("DomainID")
                Dim ds As DataSet
                ds = objLeads.GetWebAnlysDtl
                If ds.Tables(0).Rows.Count > 0 Then
                    lblDatesvisited.Text = ds.Tables(0).Rows(0).Item("StartDate") & " - " & ds.Tables(0).Rows(0).Item("EndDate")
                    lblReferringPage.Text = ds.Tables(0).Rows(0).Item("vcOrginalRef")
                    lblKeyword.Text = ds.Tables(0).Rows(0).Item("vcSearchTerm")
                    lblNoofTimes.Text = ds.Tables(0).Rows(0).Item("Count")
                Else
                    lblDatesvisited.Text = "-"
                    lblReferringPage.Text = "-"
                    lblKeyword.Text = "-"
                    lblNoofTimes.Text = "-"
                End If
                dlWebAnlys.DataSource = ds.Tables(1)
                dlWebAnlys.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadTabDetails()
            Try
                sb_CompanyInfo()
                'Added this code to Hide the Asset Tab.. 18 Feb 10 - Sojan
                'radOppTab.Tabs(7).Visible = False
                If radOppTab.SelectedIndex = 0 Then
                    'sb_CompanyInfo()
                ElseIf radOppTab.SelectedIndex = 1 Then
                    GetContacts()
                    'ElseIf radOppTab.SelectedIndex = 2 Then
                    '    sc_GetOppDetails()
                ElseIf radOppTab.SelectedIndex = 3 Then
                    sc_GetProjects()
                ElseIf radOppTab.SelectedIndex = 5 Then
                    GetWebAnalysis()
                ElseIf radOppTab.SelectedIndex = 6 Then
                    Correspondence1.lngRecordID = lngDivID
                    Correspondence1.Mode = 7
                    Correspondence1.CorrespondenceModule = 3
                    Correspondence1.getCorrespondance()
                ElseIf radOppTab.SelectedIndex = 7 Then
                    'LoadAssets()
                    AssetList.DivisionID = lngDivID
                    AssetList.FormName = "frmAccounts"
                    AssetList.LoadAssets()
                ElseIf radOppTab.SelectedIndex = 8 Then
                    If Not ClientScript.IsStartupScriptRegistered("loadframe") Then
                        ClientScript.RegisterStartupScript(Me.GetType, "loadframe", "document.getElementById('ifAssociation').src='../admin/frmcomAssociationTo.aspx?numDivisionID=" + lngDivID.ToString + "';", True)
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlProjectStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProjectStatus.SelectedIndexChanged
            Try
                LoadTabDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
            ViewState("IntermediatoryPage") = False
            boolIntermediatoryPage = False
            tblMain.Controls.Clear()
            sb_CompanyInfo()
            ControlSettings()
        End Sub

        Sub ControlSettings()
            If boolIntermediatoryPage = True Then
                btnSave.Visible = False
                btnSaveClose.Visible = False
                btnEdit.Visible = True
            Else
                'objCommon.CheckdirtyForm(Page)

                btnSave.Visible = True
                btnSaveClose.Visible = True
                btnEdit.Visible = False
            End If
        End Sub

        Sub PopulateDependentDropdown(ByVal sender As Object, ByVal e As EventArgs)
            objPageControls.PopulateDropdowns(CType(sender, DropDownList), dtTableInfo, objCommon, radOppTab.MultiPage, Session("DomainID"))
        End Sub

        Private Sub lbtnDemote_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnDemote.Click
            Try
                objLeads.DivisionID = lngDivID
                objLeads.byteMode = 0
                objLeads.DomainID = Session("DomainID")
                objLeads.UserCntID = Session("UserContactID")
                objLeads.DemoteOrg()
                Response.Redirect("../Leads/frmLeads.aspx?DivID=" & lngDivID & "&SI=" & SI1 & "&SI1=" & radOppTab.SelectedIndex & "&frm1=" & frm1 & "&frm2=" & frm2)

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub lbtnPromote_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnPromote.Click
            Try
                objLeads.DivisionID = lngDivID
                objLeads.byteMode = 2
                objLeads.DomainID = Session("DomainID")
                objLeads.UserCntID = Session("UserContactID")
                objLeads.DemoteOrg()
                Response.Redirect("../account/frmaccounts.aspx?DivID=" & lngDivID & "&SI=" & SI1 & "&SI1=" & radOppTab.SelectedIndex & "&frm1=" & frm1 & "&frm2=" & frm2)

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
    End Class
End Namespace
