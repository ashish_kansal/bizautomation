<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmConfCompanyList.aspx.vb"
    Inherits="BACRM.UserInterface.Prospects.frmConfCompanyList" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Contacts Column Customization</title>
    <script language="javascript" src="../javascript/AdvSearchScripts.js"></script>
    <script language="javascript" type="text/javascript">
        function Save() {
            var str = '';
            for (var i = 0; i < document.getElementById("lstSelectedfld").options.length; i++) {
                var SelectedValue;
                SelectedValue = document.getElementById("lstSelectedfld").options[i].value;
                str = str + SelectedValue + ','
            }
            document.getElementById("hdnCol").value = str;
            // alert( document.getElementById("hdnCol.value)
        }
		
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="button" Width="50" />
            <input class="button" id="btnClose" style="width: 50" onclick="javascript:opener.location.reload(true);window.close()"
                type="button" value="Close">&nbsp;&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label runat="server" ID="lblName"></asp:Label>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <br />
    <table cellspacing="2" cellpadding="2" id="tblRelationship" runat="server">
        <tr>
            <td>
                Relationship
            </td>
            <td>
                <asp:DropDownList ID="ddlRelationship" runat="server" AutoPostBack="true" CssClass="signup">
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <asp:Table ID="tblAdvSearchFieldCustomization" runat="server" Width="600" GridLines="None"
        CssClass="aspTable" BorderColor="black" BorderWidth="1">
        <asp:TableRow>
            <asp:TableCell>
                <table cellpadding="0" cellspacing="0" width="100%">
                    <tr>
                        <td align="center" class="normal1" valign="top">
                            Available Fields<br>
                            &nbsp;&nbsp;
                            <asp:ListBox ID="lstAvailablefld" runat="server" Width="150" Height="200" CssClass="signup"
                                EnableViewState="False"></asp:ListBox>
                        </td>
                        <td align="center" class="normal1" valign="middle">
                            <input type="button" id="btnAdd" class="button" value="Add >" onclick="javascript:move(document.getElementById('lstAvailablefld'),document.getElementById('lstSelectedfld'))">
                            <br>
                            <br>
                            <input type="button" id="btnRemove" class="button" value="< Remove" onclick="javascript:remove1(document.getElementById('lstSelectedfld'),document.getElementById('lstAvailablefld'));">
                        </td>
                        <td align="center" class="normal1">
                            Selected Fields/ Choose Order<br>
                            <asp:ListBox ID="lstSelectedfld" runat="server" Width="150" Height="200" CssClass="signup"
                                EnableViewState="False"></asp:ListBox>
                        </td>
                        <td align="center" class="normal1" valign="middle">
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <img id="btnMoveupOne" src="../images/upArrow.gif" onclick="javascript:MoveUp(document.getElementById('lstSelectedfld'));" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <br>
                            <br>
                            <img id="btnMoveDownOne" src="../images/downArrow1.gif" onclick="javascript:MoveDown(document.getElementById('lstSelectedfld'));" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            (Max 15)
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
        <%-- <asp:TableRow>
            <asp:TableCell>
                <asp:Panel runat="server" ID="pnlRelation">
                    <table width="100%">
                        <tr>
                            <td colspan="2" class="normal7">
                                When you click selected relationship, the list will default based on your settings
                                here:
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1">
                                Filter :
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlCompanyFilter" runat="server" CssClass="signup">
                                    <asp:ListItem Value="3" Selected="True">My Organization</asp:ListItem>
                                    <asp:ListItem Value="4">All Organization</asp:ListItem>
                                    <asp:ListItem Value="1">Active Organization</asp:ListItem>
                                    <asp:ListItem Value="2">Inactive Organization</asp:ListItem>
                                    <asp:ListItem Value="5">Rating</asp:ListItem>
                                    <asp:ListItem Value="6">Added in Last 7 days</asp:ListItem>
                                    <asp:ListItem Value="7">Last 20 Added by me</asp:ListItem>
                                    <asp:ListItem Value="8">Last 20 Modified by me</asp:ListItem>
                                    <asp:ListItem Value="9">Favorites</asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlProspectFilter" runat="server" CssClass="signup">
                                    <asp:ListItem Value="3" Selected="True">My Prospects</asp:ListItem>
                                    <asp:ListItem Value="4">All Prospects</asp:ListItem>
                                    <asp:ListItem Value="1">Active Prospects</asp:ListItem>
                                    <asp:ListItem Value="2">Inactive Prospects</asp:ListItem>
                                    <asp:ListItem Value="5">Rating</asp:ListItem>
                                    <asp:ListItem Value="6">Added in Last 7 days</asp:ListItem>
                                    <asp:ListItem Value="7">Last 20 Added by me</asp:ListItem>
                                    <asp:ListItem Value="8">Last 20 Modified by me</asp:ListItem>
                                    <asp:ListItem Value="9">Favorites</asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlAccountFilter" runat="server" CssClass="signup">
                                    <asp:ListItem Value="3" Selected="true">My Accounts</asp:ListItem>
                                    <asp:ListItem Value="4">All Accounts</asp:ListItem>
                                    <asp:ListItem Value="1">Active Accounts</asp:ListItem>
                                    <asp:ListItem Value="2">Inactive Accounts</asp:ListItem>
                                    <asp:ListItem Value="5">Rating</asp:ListItem>
                                    <asp:ListItem Value="6">Added in Last 7 days</asp:ListItem>
                                    <asp:ListItem Value="7">Last 20 Added by me</asp:ListItem>
                                    <asp:ListItem Value="8">Last 20 Modified by me</asp:ListItem>
                                    <asp:ListItem Value="9">Favorites</asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList ID="ddlFollow" runat="server" CssClass="signup" Width="130px">
                                </asp:DropDownList>
                                <span id="spnLead" runat="server" class="normal1">&nbsp;&nbsp;&nbsp;Group</span>
                                <asp:DropDownList ID="ddlLeadType" runat="server" CssClass="signup" Width="130px">
                                    <asp:ListItem Text="My Leads" Value="5"></asp:ListItem>
                                    <asp:ListItem Text="Web Leads" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Public Leads" Value="2"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="normal7">
                                When you click on the Relationships tab, is this the list you want to see ?
                                <asp:RadioButton ID="radY" GroupName="Group" runat="server" Text="Yes" />
                                <asp:RadioButton ID="radN" GroupName="Group" runat="server" Checked="true" Text="No" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlCase">
                    <table width="100%">
                        <tr>
                            <td colspan="2" class="normal7">
                                When you click selected case, the list will default based on your settings here:
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1">
                                Filter :
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlCase" runat="server" CssClass="signup">
                                    <asp:ListItem Value="0">My Cases</asp:ListItem>
                                    <asp:ListItem Value="1">All Cases</asp:ListItem>
                                    <asp:ListItem Value="2">Added in Last 7 Days</asp:ListItem>
                                    <asp:ListItem Value="3">Last 20 Added by me</asp:ListItem>
                                    <asp:ListItem Value="4">Last 20 Modified by me</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlPro">
                    <table width="100%">
                        <tr>
                            <td colspan="2" class="normal7">
                                When you click selected case, the list will default based on your settings here:
                            </td>
                        </tr>
                        <tr>
                            <td class="normal1">
                                Filter :
                            </td>
                            <td align="left">
                                <asp:DropDownList ID="ddlPro" runat="server" CssClass="signup">
                                    <asp:ListItem Value="1">My Projects</asp:ListItem>
                                    <asp:ListItem Value="3">All Projects</asp:ListItem>
                                    <asp:ListItem Value="2">My Subordinates</asp:ListItem>
                                    <asp:ListItem Value="4">Added in Last 7 days</asp:ListItem>
                                    <asp:ListItem Value="5">Last 20 Added by me</asp:ListItem>
                                    <asp:ListItem Value="6">Last 20 Modified by me</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </asp:TableCell>
        </asp:TableRow>--%>
    </asp:Table>
    <input id="hdnCol" type="hidden" name="hdXMLString" runat="server" value="" />
    <input id="hdSave" type="hidden" name="hdSave" runat="server" value="False" />
    <asp:HiddenField ID="hfFormId" runat="server" />
</asp:Content>
