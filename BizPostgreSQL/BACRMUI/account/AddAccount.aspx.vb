
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Leads

Namespace BACRM.UserInterface.Accounts
    Public Class AddAccount
        Inherits BACRMPage


#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
 

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Dim lngDivisionId As Long 'For holding the division id

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            ' Checking Rights for Add
            Try
                
                btnSave.Attributes.Add("onclick", "return Save();")
                btnClose.Attributes.Add("onclick", "return Close()")
                lblContact.Text = ""
                If Not IsPostBack Then
                    Session("Help") = "Organization"
                    'Calling the respective procedures for filling in the respective
                    Dim objCommon As New CCommon
                    Dim m_aryRightsForPage() As Integer
                    m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "AddAccount.aspx", Session("userID"), 4, 1)
                    If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                        btnSave.Visible = False
                    Else
                        btnSave.Visible = True
                    End If
                    objCommon.sb_FillComboFromDBwithSel(ddlRelationShip, 5, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlTerritory, 78, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlInfoSource, 18, Session("DomainID"))
                End If
            Catch ex As Exception
                Response.Write(ex)
            End Try
        End Sub
       
        Private Function InsertProspect() As Boolean
            Dim objLeads As New CLeads
            Try
                With objLeads
                    If Len(txtCompany.Text) < 1 Then
                        txtCompany.Text = txtLastName.Text & "," & txtFirstName.Text
                    End If
                    .CompanyName = txtCompany.Text.Trim
                    .How = ddlInfoSource.SelectedItem.Value
                    .TerritoryID = ddlTerritory.SelectedItem.Value
                    .CustName = txtCompany.Text.Trim
                    .CompanyType = ddlRelationShip.SelectedValue
                    .CRMType = 2
                    .DivisionName = "-"
                    .LeadBoxFlg = 1
                    .UserCntID = Session("UserContactId")
                    .Country = Session("DefCountry")
                    .SCountry = Session("DefCountry")
                    .FirstName = txtFirstName.Text
                    .LastName = txtLastName.Text
                    .Phone = txtPhone.Text
                    .PhoneExt = txtextn.Text
                    .Email = txtEmail.Text
                    .DomainID = Session("DomainID")
                    .ContactType = 70
                End With
                objLeads.CompanyID = objLeads.CreateRecordCompanyInfo
                lngDivisionId = objLeads.CreateRecordDivisionsInfo
                objLeads.DivisionID = lngDivisionId
                objLeads.CreateRecordAddContactInfo()
                Return True
            Catch ex As Exception
                Throw ex
            End Try
            
        End Function
        'to check if company is already there ,it return true if company is there 
        'else return false


        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                If InsertProspect() = True Then
                    Dim strScript As String = "<script language=JavaScript>"
                    strScript += "window.opener.reDirectPage('../account/frmAccounts.aspx?DivID=" & lngDivisionId & "'); self.close();"
                    strScript += "</script>"
                    If (Not Page.IsStartupScriptRegistered("clientScript")) Then
                        Page.RegisterStartupScript("clientScript", strScript)
                    End If
                End If

            Catch ex As Exception
                Response.Write(ex)
            End Try
        End Sub
    End Class
End Namespace