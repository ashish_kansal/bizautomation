' Created By Anoop Jayaraj
Imports System.IO
Imports System.Text
Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Contract
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Prospects
Imports Telerik.Web.UI

Namespace BACRM.UserInterface.Accounts
    Public Class frmAccounts
        Inherits BACRMPage

        Dim objContacts As CContacts
        Dim objProspects As CProspects
        Dim objAccounts As CAccounts
        Dim objLeads As New LeadsIP
        Dim objItems As CItems
        Dim objOpportunity As BACRM.BusinessLogic.Opportunities.MOpportunity
        Dim strColumn As String
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Public lngDivId As Long

        Dim strDtTo As Nullable(Of DateTime) = Nothing
        Dim strDtFrom As Nullable(Of DateTime) = Nothing
        Dim dtCompanyTaxTypes As DataTable
        Public m_aryRightsForAccounting(), m_aryRightsForCustFlds(), m_aryRightsForActItem(), m_aryRightsForContacts() As Integer
        Dim m_aryRightsForOpp(), m_aryRightsForProject(), m_aryRightsForItems(), m_aryRightsOrder(), m_aryRightsForCases(), m_aryRightsForTransOwn(), m_aryRightsForFav(), m_aryRightsForDemote(), m_aryRightsForSupKey(), m_aryRightsForViewLayoutButton() As Integer

        Dim boolIntermediatoryPage As Boolean = False
        Dim dtCustomFieldTable As DataTable
        Dim dtTableInfo As DataTable
        Dim objPageControls As New PageControls
        Dim strItemColumn As String

        Public Enum Inbound850PickItem
            SKU = 1
            UPC = 2
            ItemName = 3
            BizItemID = 4
            ASIN = 5
            CustomerPart = 6
            VendorPart = 7
        End Enum

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Error1(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Error

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                    DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                    If Session("EnableIntMedPage") = 1 Then
                        ViewState("IntermediatoryPage") = True
                    Else
                        ViewState("IntermediatoryPage") = False
                    End If

                    If Session("BtoGContact") = True Then
                        'btnGoogle.Visible = True
                    End If

                    m_aryRightsForViewLayoutButton = GetUserRightsForPage_Other(4, 16)
                    If m_aryRightsForViewLayoutButton(RIGHTSTYPE.VIEW) = 0 Then
                        btnLayout.Visible = False
                    End If
                End If
                boolIntermediatoryPage = ViewState("IntermediatoryPage")
                ControlSettings()

                If GetQueryStringVal("frm") = "SurveyRespondents" Or GetQueryStringVal("frm") = "DuplicateSearch" Then    'This special section is introduced because the screen has to open in a new window without any menus
                    'Added by Debasish Nag on 6th Jan 2006
                    Dim objForm As Object                                   'The generic object
                    objForm = Page.FindControl("webmenu1")                  'Get a holder to the web control
                    objForm.visible = False                                 'Hide the web control
                    btnCancel.Attributes.Add("onclick", "javascript: window.close();return false;") 'Write code to close the window
                End If
                lngDivId = CCommon.ToLong(GetQueryStringVal("DivId"))

                Dim hdnContractsDomainId As HiddenField = DirectCast(ContractsLog.FindControl("hdnContractsDomainId"), HiddenField)
                Dim hdnContractsDivisonId As HiddenField = DirectCast(ContractsLog.FindControl("hdnContractsDivisonId"), HiddenField)
                Dim hdnContractsRecordId As HiddenField = DirectCast(ContractsLog.FindControl("hdnContractsRecordId"), HiddenField)
                hdnContractsDomainId.Value = Session("DomainID")
                hdnContractsDivisonId.Value = lngDivId
                hdnContractsRecordId.Value = 0
                Session("DivisionID") = lngDivId
                Correspondence1.lngRecordID = lngDivId
                Correspondence1.Mode = 7
                ' Checking the rights to view Contacts
                If GetQueryStringVal("SI") <> "" Then
                    SI = GetQueryStringVal("SI")
                End If
                If GetQueryStringVal("SI1") <> "" Then
                    SI1 = GetQueryStringVal("SI1")
                Else
                    SI1 = 0
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    SI2 = GetQueryStringVal("SI2")
                Else
                    SI2 = 0
                End If
                If GetQueryStringVal("frm") <> "" Then
                    frm = GetQueryStringVal("frm")
                Else
                    frm = ""
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = GetQueryStringVal("frm1")
                Else
                    frm1 = ""
                End If
                If GetQueryStringVal("frm2") <> "" Then
                    frm2 = GetQueryStringVal("frm2")
                Else
                    frm2 = ""
                End If

                GetUserRightsForPage(4, 4)

                If Not IsPostBack Then
                    lblOrganizationID.Text = "(ID:" & lngDivId.ToString() & ")"
                    hdnDivID.Value = lngDivId
                    hdnDivisionID.Value = lngDivId
                    objProspects = New CProspects
                    AddToRecentlyViewed(RecetlyViewdRecordType.Account_Pospect_Lead, lngDivId)

                    objCommon.DivisionID = lngDivId
                    objCommon.charModule = "D"
                    objCommon.GetCompanySpecificValues1()
                    txtContId.Text = CStr(objCommon.ContactID)

                    ' txtCurrentPageCorr.Text = 1
                    LoadAssociationInformation()                            'Added by Debasish for displayign the Association information
                    'checking rights to view the page

                    ''''sub-tab management  - added on 16/09/2009 by chintan
                    objCommon.GetAuthorizedSubTabs(radOppTab, Session("DomainID"), 13, Session("UserGroupID"))
                    '''''
                    ''''sub-tab management  - added on 02/01/2014 by sachin sadhu
                    ''Purpose:To authorize sub tabs of Item tab
                    objCommon.GetAuthorizedSubTabs(radStripItems, Session("DomainID"), 15, Session("UserGroupID"))
                    'ended by sachin
                    If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                        btnSave.Visible = False
                        btnSaveClose.Visible = False
                    End If
                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                        btnActDelete.Visible = False
                    End If
                    sb_loadDropDowns()

                    Dim dtTab As DataTable
                    dtTab = Session("DefaultTab")
                    If dtTab.Rows.Count > 0 Then
                        radOppTab.Tabs(0).Text = IIf(IsDBNull(dtTab.Rows(0).Item("vcAccount")), "Account Details", dtTab.Rows(0).Item("vcAccount").ToString & " Details")
                    Else
                        radOppTab.Tabs(0).Text = "Account Details"
                    End If

                    FillNetDays()
                    BindShippingCompany()
                    FillAccounts()
                    FillClass()
                    FillPriceLevel()
                    FillInbound850PickItem()

                    objCommon = New CCommon
                    'objCommon.sb_FillComboFromDB(ddlNetDays, 296, Session("DomainID"))
                    objCommon.sb_FillComboFromDBwithSel(ddlProjectStatus, 439, Session("DomainID"))

                    Dim objCurrency As New CurrencyRates
                    objCurrency.DomainID = Session("DomainID")
                    objCurrency.GetAll = 0
                    ddlCurrency.DataSource = objCurrency.GetCurrencyWithRates()
                    ddlCurrency.DataTextField = "vcCurrencyDesc"
                    ddlCurrency.DataValueField = "numCurrencyID"
                    ddlCurrency.DataBind()
                    ddlCurrency.Items.Insert(0, "--Select One--")
                    ddlCurrency.Items.FindByText("--Select One--").Value = "0"

                    If Not ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")) Is Nothing Then
                        ddlCurrency.Items.FindByValue(Session("BaseCurrencyID")).Selected = True
                    End If

                    PersistTable.Load(boolOnlyURL:=True)

                    If PersistTable.Count > 0 Then
                        radOppTab.SelectedIndex = CCommon.ToLong(PersistTable(PersistKey.SelectedTab))

                    End If
                    'Author:Sachin Sadhu||Date:4thJan2014
                    'Purpose:To Load Persisted Items tab's Sub tabs-Bought & vended
                    PersistTable.Load()
                    If PersistTable.Count > 0 Then

                        radStripItems.SelectedIndex = CCommon.ToLong(PersistTable(PersistKey.SelectedTab & "Items"))
                    End If

                    If radStripItems.SelectedTab IsNot Nothing Then
                        If radStripItems.SelectedTab.Visible = False Then
                            radStripItems.SelectedIndex = 0
                            radStripItems.MultiPage.FindPageViewByID(radStripItems.SelectedTab.PageViewID).Selected = True
                        Else
                            radStripItems.MultiPage.FindPageViewByID(radStripItems.SelectedTab.PageViewID).Selected = True
                        End If
                    Else
                        radStripItems.SelectedIndex = 0
                        radStripItems.MultiPage.FindPageViewByID(radStripItems.SelectedTab.PageViewID).Selected = True
                    End If
                    'End of code by sachin
                    If radOppTab.SelectedTab IsNot Nothing Then
                        If radOppTab.SelectedTab.Visible = False Then
                            radOppTab.SelectedIndex = 0
                            radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                        Else
                            radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                        End If
                    Else
                        radOppTab.SelectedIndex = 0
                        radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                    End If

                    objCommon.sb_FillComboFromDBwithSel(ddlPaymentMethod, 31, Session("DomainID"))
                    BindCreditCard()

                    sb_CompanyInfo()
                    LoadTabDetails()

                    If radOppTab.Tabs.Count > SI AndAlso SI > 0 Then radOppTab.SelectedIndex = SI

                    m_aryRightsForTransOwn = GetUserRightsForPage_Other(4, 12)
                    If m_aryRightsForTransOwn(RIGHTSTYPE.VIEW) = 0 Then
                        hplTransfer.Visible = False
                        hplTransferNonVis.Visible = True
                    ElseIf m_aryRightsForTransOwn(RIGHTSTYPE.VIEW) = 1 Then
                        Try
                            If CCommon.ToLong(hfOwner.Value) <> Session("UserContactID") Then
                                hplTransfer.Visible = False
                                hplTransferNonVis.Visible = True
                            End If
                        Catch ex As Exception
                        End Try
                    ElseIf m_aryRightsForTransOwn(RIGHTSTYPE.VIEW) = 2 Then
                        Dim i As Integer
                        Dim dtTerritory As DataTable
                        dtTerritory = Session("UserTerritory")
                        If CCommon.ToLong(hfTerritory.Value) = 0 Then
                            hplTransfer.Visible = False
                            hplTransferNonVis.Visible = True
                        Else
                            Dim chkDelete As Boolean = False
                            For i = 0 To dtTerritory.Rows.Count - 1
                                If CCommon.ToLong(hfTerritory.Value) = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                    chkDelete = True
                                End If
                            Next
                            If chkDelete = False Then
                                hplTransfer.Visible = False
                                hplTransferNonVis.Visible = True
                            End If
                        End If
                    End If

                    m_aryRightsForDemote = GetUserRightsForPage_Other(4, 13)
                    If m_aryRightsForDemote(RIGHTSTYPE.VIEW) = 0 Then
                        lbtnDemote.Visible = False
                    ElseIf m_aryRightsForDemote(RIGHTSTYPE.VIEW) = 1 Then
                        Try
                            If CCommon.ToLong(hfOwner.Value) <> Session("UserContactID") Then
                                lbtnDemote.Visible = False
                            End If
                        Catch ex As Exception
                        End Try
                    ElseIf m_aryRightsForDemote(RIGHTSTYPE.VIEW) = 2 Then
                        Dim i As Integer
                        Dim dtTerritory As DataTable
                        dtTerritory = Session("UserTerritory")
                        If CCommon.ToLong(hfTerritory.Value) = 0 Then
                            lbtnDemote.Visible = False
                        Else
                            Dim chkDelete As Boolean = False
                            For i = 0 To dtTerritory.Rows.Count - 1
                                If CCommon.ToLong(hfTerritory.Value) = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                    chkDelete = True
                                End If
                            Next
                            If chkDelete = False Then
                                lbtnDemote.Visible = False
                            End If
                        End If
                    End If
                End If
                If IsPostBack Then
                    'If txtCurrentPageCorr.Text = "" Then
                    '    txtCurrentPageCorr.Text = "1"
                    'End If
                    sb_CompanyInfo()
                    If radOppTab.SelectedIndex = 5 Then
                        sb_DisplayAccounting()
                    End If

                    If radOppTab.SelectedIndex = 1 Then
                        GetContacts()
                    End If

                End If

                m_aryRightsForCustFlds = GetUserRightsForPage_Other(4, 11)
                If m_aryRightsForCustFlds(RIGHTSTYPE.VIEW) <> 0 Then
                    DisplayDynamicFlds()
                End If

                lblCompanyType.Text = objLeads.CompanyType
                OpportunitiesTab1.DivisionID = lngDivId
                btnLayout.Attributes.Add("onclick", "return ShowLayout('P','" & lngDivId & "','" & objLeads.CompanyType & "','36');")
                'txtNetDays.Attributes.Add("onkeypress", "CheckNumber(2)")
                txtInterest.Attributes.Add("onkeypress", "CheckNumber(1)")
                hplTransfer.Attributes.Add("onclick", "return OpenTransfer('" & "../admin/transferrecord.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=accounts&rtyWR=" & lngDivId & "')")
                btnSave.Attributes.Add("onclick", "return Save();")
                btnSaveClose.Attributes.Add("onclick", "return Save();")

                hplARReport.Attributes.Add("onclick", "return OpemARDetails('" & lngDivId & "');")

                If lngDivId = Session("UserDivisionID") Then
                    btnActDelete.Attributes.Add("onclick", "return DeleteMessage()")
                Else : btnActDelete.Attributes.Add("onclick", "return DeleteRecord()")
                End If
                hrfGL.Attributes.Add("onclick", "return OpenGL('" & lngDivId & "')")
                imgCreditCard.Attributes.Add("onclick", "return OpenTransactionDetails('" & lngDivId & "')")
                hrfTransactionDetail.Attributes.Add("onclick", "return OpenTransactionDetails('" & lngDivId & "')")
                hrfcc.Attributes.Add("onclick", "return OpenManageCC('" & lngDivId & "','" & txtContId.Text & "')")
                BindCreditCard()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ShowDocuments(ByRef tblDocuments As HtmlTable, ByVal dtDocLinkName As DataTable, ByVal dtDisplayFields As DataTable, ByVal intSection As Integer)
            Try
                tblDocuments.Controls.Clear()
                Dim countId = 0
                Dim tr As HtmlTableRow
                For Each drDocument In dtDocLinkName.Rows
                    If (intSection = 0 AndAlso (CCommon.ToLong(drDocument("numFormFieldGroupId")) = 0 Or dtDisplayFields.Select("numFormFieldGroupId=" & CCommon.ToLong(drDocument("numFormFieldGroupId"))).Length = 0)) Or
                        (intSection <> 0 AndAlso CCommon.ToLong(drDocument("numFormFieldGroupId")) = intSection) Then
                        If countId = 0 Then
                            tr = New HtmlTableRow
                        End If

                        If CCommon.ToString(drDocument("cUrlType")) = "L" Then
                            Dim strOriginalFileName = CCommon.ToString(drDocument("VcDocName")) & CCommon.ToString(drDocument("vcFileType"))
                            Dim strDocName = CCommon.ToString(drDocument("VcFileName"))
                            Dim fileID = CCommon.ToString(drDocument("numGenericDocID"))
                            Dim strFileLogicalPath = String.Empty
                            If File.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName) Then
                                Dim f As FileInfo = New FileInfo(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName)
                                Dim strFileSize As String = f.Length.ToString
                                strFileLogicalPath = CCommon.GetDocumentPath(Session("DomainID")) & HttpUtility.UrlEncode(strDocName).Replace("+", " ")
                            End If

                            Dim docLink As New HyperLink

                            Dim imgDelete As New ImageButton
                            imgDelete.ImageUrl = "../images/Delete24.png"
                            imgDelete.Height = 13
                            imgDelete.ToolTip = "Delete"
                            imgDelete.ID = "imgDelete" + CCommon.ToString(drDocument("numGenericDocID"))
                            imgDelete.Attributes.Add("onclick", "DeleteDocumentOrLink(" & fileID & ");return false;")

                            Dim Space As New LiteralControl
                            Space.Text = "&nbsp;"

                            docLink.Text = strOriginalFileName
                            docLink.NavigateUrl = strFileLogicalPath
                            docLink.ID = "hpl" + CCommon.ToString(drDocument("numGenericDocID"))
                            docLink.Target = "_blank"
                            Dim td As New HtmlTableCell
                            td.Style.Add("border-bottom", "0px !important")
                            td.Style.Add("padding-left", "5px")
                            td.Style.Add("padding-right", "5px")
                            td.Controls.Add(docLink)
                            td.Controls.Add(Space)
                            td.Controls.Add(imgDelete)
                            tr.Controls.Add(td)
                        ElseIf CCommon.ToString(drDocument("cUrlType")) = "U" Then
                            Dim uriBuilder As UriBuilder = New UriBuilder(CCommon.ToString(drDocument("VcFileName")))

                            Dim link As New HyperLink
                            Dim imgLinkDelete As New ImageButton
                            imgLinkDelete.ImageUrl = "../images/Delete24.png"
                            imgLinkDelete.Height = 13
                            imgLinkDelete.ToolTip = "Delete"
                            imgLinkDelete.ID = "imgLinkDelete" + CCommon.ToString(drDocument("numGenericDocID"))
                            imgLinkDelete.Attributes.Add("onclick", "DeleteDocumentOrLink(" & CCommon.ToString(drDocument("numGenericDocID")) & ");return false;")

                            link.Text = CCommon.ToString(drDocument("VcDocName"))
                            link.NavigateUrl = uriBuilder.Uri.AbsoluteUri
                            link.ID = "hplLink" + CCommon.ToString(drDocument("numGenericDocID"))
                            link.Target = "_blank"

                            Dim td As New HtmlTableCell
                            td.Style.Add("border-bottom", "0px !important")
                            td.Style.Add("padding-left", "5px")
                            td.Style.Add("padding-right", "5px")
                            td.Controls.Add(link)
                            td.Controls.Add(imgLinkDelete)
                            tr.Controls.Add(td)
                        End If

                        countId = countId + 1

                        If countId = 3 Then
                            countId = 0
                            tblDocuments.Controls.Add(tr)
                        End If
                    End If
                Next

                If countId <> 0 Then
                    tblDocuments.Controls.Add(tr)
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Protected Sub btnDeleteDocOrLink_Click(sender As Object, e As EventArgs)
            Dim objDocuments As New DocumentList()
            objDocuments.GenDocID = CCommon.ToLong(hdnFileId.Value)
            objDocuments.DeleteDocumentOrLink()
            LoadControls()
        End Sub

        Private Sub BindShippingCompany()
            Try
                If objCommon Is Nothing Then objCommon = New CCommon
                radShipCompany.DataSource = objCommon.GetMasterListItems(82, Session("DomainID"))
                radShipCompany.DataTextField = "vcData"
                radShipCompany.DataValueField = "numListItemID"
                radShipCompany.DataBind()

                Dim listItem As Telerik.Web.UI.RadComboBoxItem
                If radShipCompany.Items.FindItemByText("Fedex") IsNot Nothing Then
                    radShipCompany.Items.Remove(radShipCompany.Items.FindItemByText("Fedex"))
                End If
                If radShipCompany.Items.FindItemByText("UPS") IsNot Nothing Then
                    radShipCompany.Items.Remove(radShipCompany.Items.FindItemByText("UPS"))
                End If
                If radShipCompany.Items.FindItemByText("USPS") IsNot Nothing Then
                    radShipCompany.Items.Remove(radShipCompany.Items.FindItemByText("USPS"))
                End If

                listItem = New Telerik.Web.UI.RadComboBoxItem
                listItem.Text = "-- Select One --"
                listItem.Value = "0"
                radShipCompany.Items.Insert(0, listItem)

                listItem = New Telerik.Web.UI.RadComboBoxItem
                listItem.Text = "Fedex"
                listItem.Value = "91"
                listItem.ImageUrl = "../images/FedEx.png"
                radShipCompany.Items.Insert(1, listItem)

                listItem = New Telerik.Web.UI.RadComboBoxItem
                listItem.Text = "UPS"
                listItem.Value = "88"
                listItem.ImageUrl = "../images/UPS.png"
                radShipCompany.Items.Insert(2, listItem)

                listItem = New Telerik.Web.UI.RadComboBoxItem
                listItem.Text = "USPS"
                listItem.Value = "90"
                listItem.ImageUrl = "../images/USPS.png"
                radShipCompany.Items.Insert(3, listItem)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindCreditCard()
            Try
                Dim dsCCInfo As DataTable
                Dim objOppInvoice As New OppInvoice
                objOppInvoice.DomainID = Session("DomainID")
                objOppInvoice.DivisionID = lngDivId
                objOppInvoice.numCCInfoID = 0
                objOppInvoice.IsDefault = 1
                objOppInvoice.bitflag = 1
                dsCCInfo = objOppInvoice.GetCustomerCCInfo(0, Convert.ToInt32(txtContId.Text), 1)
                Dim strCard As String = ""
                Dim objEncryption As New QueryStringValues
                For i As Integer = 0 To dsCCInfo.Rows.Count - 1
                    strCard = objEncryption.Decrypt(dsCCInfo.Rows(i)("vcCreditCardNo").ToString())
                    If strCard.Length > 4 Then
                        lblcc.Text = objEncryption.Decrypt(dsCCInfo.Rows(i)("vcCardHolder").ToString()) & ", ************" & strCard.Substring(strCard.Length - 4, 4)
                    Else
                        lblcc.Text = "NA"
                    End If
                Next
                'ddlDefaultCreditCard.DataTextField = "vcCreditCardNo"
                'ddlDefaultCreditCard.DataValueField = "numCCInfoID"
                'ddlDefaultCreditCard.DataSource = dsCCInfo
                'ddlDefaultCreditCard.DataBind()
                'ddlDefaultCreditCard.Items.Insert(0, New ListItem("--Select One--", "0"))

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub FillNetDays()
            Try
                Dim objOpp As New BusinessLogic.Opportunities.OppotunitiesIP
                Dim dtTerms As New DataTable
                With objOpp
                    .DomainID = Session("DomainID")
                    .TermsID = 0
                    .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    dtTerms = .GetTerms()
                End With

                For Each dr As DataRow In dtTerms.Rows
                    dr("vcTerms") = dr("vcTerms") & " (" & CCommon.ToLong(dr("numNetDueInDays")) & ", " & CCommon.ToLong(dr("numDiscount")) & ", " & CCommon.ToLong(dr("numDiscountPaidInDays")) & ")"
                Next

                If dtTerms IsNot Nothing Then
                    ddlNetDays.DataSource = dtTerms
                    ddlNetDays.DataTextField = "vcTerms"
                    ddlNetDays.DataValueField = "numTermsID"
                    ddlNetDays.DataBind()
                End If

                ddlNetDays.Items.Insert(0, New ListItem("--Select One--", "0"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub FillAccounts()
            Try
                Dim dtChartAcntDetails As DataTable
                Dim objCOA As New ChartOfAccounting

                If objCOA Is Nothing Then objCOA = New ChartOfAccounting
                objCOA.DomainID = Session("DomainID")
                objCOA.AccountCode = "0104" 'Expense
                dtChartAcntDetails = objCOA.GetParentCategory()

                Dim dt1 As DataTable
                objCOA.AccountCode = "0106" ' COGS
                dt1 = objCOA.GetParentCategory()
                dtChartAcntDetails.Merge(dt1)

                objCOA.AccountCode = "0103" ' Income
                dt1 = objCOA.GetParentCategory()
                dtChartAcntDetails.Merge(dt1)

                objCOA.AccountCode = "0101" ' Asset
                dt1 = objCOA.GetParentCategory()
                'Exclude AR 
                Dim drArray() As DataRow = dt1.Select("vcAccountTypeCode <> '01010105'")
                If drArray.Length > 0 Then
                    dt1 = drArray.CopyToDataTable()
                End If
                dtChartAcntDetails.Merge(dt1)

                objCOA.AccountCode = "0102" ' Liability
                dt1 = objCOA.GetParentCategory()
                'Exclude AP
                drArray = dt1.Select("vcAccountTypeCode <>'01020102'")
                If drArray.Length > 0 Then
                    dt1 = drArray.CopyToDataTable()
                End If
                dtChartAcntDetails.Merge(dt1)

                Dim item As ListItem
                For Each dr As DataRow In dtChartAcntDetails.Rows
                    item = New ListItem()
                    item.Text = dr("vcAccountName")
                    item.Value = dr("numAccountID")
                    item.Attributes("OptionGroup") = dr("vcAccountType")
                    ddlDefaultExpense.Items.Add(item)
                Next
                ddlDefaultExpense.Items.Insert(0, New ListItem("--Select One --", "0"))

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub FillClass()
            Try
                Dim dtClass As DataTable
                Dim objAdmin As New CAdmin
                objAdmin.DomainID = Session("DomainID")
                objAdmin.Mode = 1
                dtClass = objAdmin.GetClass()

                If dtClass.Rows.Count > 0 Then
                    ddlClass.DataTextField = "ClassName"
                    ddlClass.DataValueField = "numChildClassID"
                    ddlClass.DataSource = dtClass
                    ddlClass.DataBind()
                End If

                ddlClass.Items.Insert(0, New ListItem("--Select One --", "0"))
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub FillPriceLevel()
            Try
                Dim objCommon As New CCommon
                objCommon.DomainID = Session("DomainID")
                Dim dtPriceLevel As DataTable = objCommon.GetNamedPriceLevel()

                If Not dtPriceLevel Is Nothing AndAlso dtPriceLevel.Rows.Count > 0 Then
                    ddlPriceLevel.DataTextField = "Value"
                    ddlPriceLevel.DataValueField = "Id"
                    ddlPriceLevel.DataSource = dtPriceLevel
                    ddlPriceLevel.DataBind()
                End If

                ddlPriceLevel.Items.Insert(0, New ListItem("--Select One --", "0"))
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub FillInbound850PickItem()
            Try
                Dim itemValues As Array = System.Enum.GetValues(GetType(Inbound850PickItem))
                Dim itemNames As Array = System.Enum.GetNames(GetType(Inbound850PickItem))

                For i As Integer = 0 To itemNames.Length - 1
                    Dim item As New ListItem(itemNames(i), itemValues(i))
                    ddlInbound850PickItem.Items.Add(item)
                Next
                ddlInbound850PickItem.Items.Insert(0, New ListItem("--Select One --", "0"))
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Function GetMonth(ByVal intMonth As Integer) As String
            Try
                If intMonth = 1 Then Return "January"
                If intMonth = 2 Then Return "February"
                If intMonth = 3 Then Return "March"
                If intMonth = 4 Then Return "April"
                If intMonth = 5 Then Return "May"
                If intMonth = 6 Then Return "June"
                If intMonth = 7 Then Return "July"
                If intMonth = 8 Then Return "August"
                If intMonth = 9 Then Return "Septemper"
                If intMonth = 10 Then Return "October"
                If intMonth = 11 Then Return "November"
                If intMonth = 12 Then Return "December"
                Return String.Empty
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub sb_loadDropDowns()
            Try
                objCommon.sb_FillComboFromDBwithSel(ddlCreditLimit, 3, Session("DomainID")) ''Company Credit
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub sb_DisplayAccounting()
            Try
                m_aryRightsForAccounting = GetUserRightsForPage_Other(4, 10)
                If m_aryRightsForAccounting(RIGHTSTYPE.VIEW) = 0 Then Exit Sub

                If CCommon.ToLong(lblCompanyType.Text) = 46 Then
                    Dim mobjGeneralLedger As New GeneralLedger
                    mobjGeneralLedger.DomainID = CCommon.ToLong(Session("DomainID"))
                    mobjGeneralLedger.Year = CInt(Now.Year)

                    Dim lobjVendorPayment As New VendorPayment
                    Dim dtARAging As DataTable
                    lobjVendorPayment.DomainID = Session("DomainId")
                    lobjVendorPayment.CompanyID = lngDivId
                    lobjVendorPayment.UserCntID = Session("UserContactID")
                    lobjVendorPayment.dtFromDate = mobjGeneralLedger.GetFiscalDate()
                    lobjVendorPayment.dtTodate = CDate(DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)) 'DateAdd(DateInterval.Day, 1, CDate(calTo.SelectedDate)) ''calTo.SelectedDate

                    dtARAging = lobjVendorPayment.GetAccountReceivableAging

                    If dtARAging.Rows.Count > 0 Then
                        tblARDetail.Visible = True

                        lblARThirtyDays.Text = String.Format("{0:#,##0.00}", dtARAging(0)("numThirtyDays"))
                        lblARSixtyDays.Text = String.Format("{0:#,##0.00}", dtARAging(0)("numSixtyDays"))
                        lblARNinetyDays.Text = String.Format("{0:#,##0.00}", dtARAging(0)("numNinetyDays"))
                        lblAROverNinetyDays.Text = String.Format("{0:#,##0.00}", dtARAging(0)("numOverNinetyDays"))

                        lblARThirtyDaysOverDue.Text = String.Format("{0:#,##0.00}", dtARAging(0)("numThirtyDaysOverDue"))
                        lblARSixtyDaysOverDue.Text = String.Format("{0:#,##0.00}", dtARAging(0)("numSixtyDaysOverDue"))
                        lblARNinetyDaysOverDue.Text = String.Format("{0:#,##0.00}", dtARAging(0)("numNinetyDaysOverDue"))
                        lblAROverNinetyDaysOverDue.Text = String.Format("{0:#,##0.00}", dtARAging(0)("numOverNinetyDaysOverDue"))
                    End If
                End If

                If objItems Is Nothing Then objItems = New CItems

                Dim dtAccountDtls As DataTable
                objItems.DivisionID = lngDivId
                dtAccountDtls = objItems.GetAmountDue
                If dtAccountDtls.Rows.Count > 0 Then
                    lblPOCreditMemo.Text = String.Format("{0:#,##0.00}", dtAccountDtls.Rows(0).Item("PCreditMemo"))
                    lblAmountDuePO.Text = String.Format("{0:#,##0.00}", dtAccountDtls.Rows(0).Item("AmountDuePO"))
                    lblAmountPastDuePO.Text = String.Format("{0:#,##0.00}", dtAccountDtls.Rows(0).Item("AmountPastDuePO"))

                    lblSOCreditMemo.Text = String.Format("{0:#,##0.00}", dtAccountDtls.Rows(0).Item("SCreditMemo"))
                    lblAmountDueSO.Text = String.Format("{0:#,##0.00}", dtAccountDtls.Rows(0).Item("AmountDueSO"))
                    lblAmountPastDueSO.Text = String.Format("{0:#,##0.00}", dtAccountDtls.Rows(0).Item("AmountPastDueSO"))
                End If

                objAccounts = New CAccounts
                Dim strAccountPerformanceSales As String
                Dim strAccountPerformancePurchase As String
                Dim ds As DataSet
                objAccounts.DivisionID = lngDivId
                ds = objAccounts.GetAccountPerformance()

                LoadTransactionGrid()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function GetColor(ByVal i As Integer) As String
            Try
                If i = 0 Then Return "LightSkyBlue"
                If i = 1 Then Return "blue"
                If i = 2 Then Return "green"
                If i = 3 Then Return "yellow"
                If i = 4 Then Return "black"
                If i = 5 Then Return "gray"
                If i = 6 Then Return "red"
                If i = 7 Then Return "HotPink"
                If i = 8 Then Return "Violet"
                If i = 9 Then Return "brown"
                If i = 10 Then Return "LawnGreen"
                If i = 11 Then Return "LightBlue"
                Return String.Empty
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Sub GetCases()
            Try
                m_aryRightsForCases = GetUserRightsForPage_Other(4, 9)
                If m_aryRightsForCases(RIGHTSTYPE.VIEW) = 0 Then Exit Sub
                If objAccounts Is Nothing Then objAccounts = New CAccounts

                objAccounts.DivisionID = lngDivId
                Dim dtCases As DataTable
                If radCase.Checked = True Then
                    dtCases = objAccounts.GetOpenCases
                ElseIf radCaseHst.Checked = True Then
                    dtCases = objAccounts.GetCaseHstr
                End If
                dgOpenCases.DataSource = dtCases
                dgOpenCases.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub GetContacts()
            Try
                m_aryRightsForContacts = GetUserRightsForPage_Other(4, 5)
                If m_aryRightsForContacts(RIGHTSTYPE.VIEW) = 0 Then Exit Sub
                If objProspects Is Nothing Then objProspects = New CProspects

                objProspects.DivisionID = lngDivId
                objProspects.DomainID = Session("DomainID")
                objProspects.UserCntID = Session("UserContactID")
                objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objProspects.UserGroupID = Session("UserGroupID")

                Dim dsList As DataSet
                Dim dtContact As DataTable

                dsList = objProspects.GetContactInfo1
                dtContact = dsList.Tables(0)

                Dim dtTableInfo As DataTable
                dtTableInfo = dsList.Tables(1)

                Dim bField As BoundField
                Dim Tfield As TemplateField

                gvContacts.Columns.Clear()

                For Each drRow As DataRow In dtTableInfo.Rows
                    Tfield = New TemplateField

                    Tfield.HeaderTemplate = New AccountProspectContactGrid(ListItemType.Header, drRow)

                    Tfield.ItemTemplate = New AccountProspectContactGrid(ListItemType.Item, drRow)
                    gvContacts.Columns.Add(Tfield)
                Next

                Dim dr As DataRow

                'dr = dtTableInfo.NewRow()
                'dr("vcFieldName") = "New Action Item"
                'dr("vcDbColumnName") = "NewActionItem"
                'dr("vcOrigDbColumnName") = "NewActionItem"
                'dr("vcAssociatedControlType") = "NewActionItem"

                'Tfield = New TemplateField
                'Tfield.HeaderTemplate = New AccountProspectContactGrid(ListItemType.Header, dr)
                'Tfield.ItemTemplate = New AccountProspectContactGrid(ListItemType.Item, dr)
                'gvContacts.Columns.Add(Tfield)

                'dr = dtTableInfo.NewRow()
                'dr("vcFieldName") = "Last 10 Action Item Opened"
                'dr("vcDbColumnName") = "Last10ActionItemOpened"
                'dr("vcOrigDbColumnName") = "Last10ActionItemOpened"
                'dr("vcAssociatedControlType") = "Last10ActionItemOpened"

                'Tfield = New TemplateField
                'Tfield.HeaderTemplate = New AccountProspectContactGrid(ListItemType.Header, dr)
                'Tfield.ItemTemplate = New AccountProspectContactGrid(ListItemType.Item, dr)
                'gvContacts.Columns.Add(Tfield)

                'dr = dtTableInfo.NewRow()
                'dr("vcFieldName") = "Last 10 Action Item Closed"
                'dr("vcDbColumnName") = "Last10ActionItemClosed"
                'dr("vcOrigDbColumnName") = "Last10ActionItemClosed"
                'dr("vcAssociatedControlType") = "Last10ActionItemClosed"

                'Tfield = New TemplateField
                'Tfield.HeaderTemplate = New AccountProspectContactGrid(ListItemType.Header, dr)
                'Tfield.ItemTemplate = New AccountProspectContactGrid(ListItemType.Item, dr)
                'gvContacts.Columns.Add(Tfield)

                dr = dtTableInfo.NewRow()
                dr("vcFieldName") = "Delete"
                dr("vcDbColumnName") = "Delete"
                dr("vcOrigDbColumnName") = "Delete"
                dr("vcAssociatedControlType") = "Delete"

                Tfield = New TemplateField
                Tfield.HeaderTemplate = New AccountProspectContactGrid(ListItemType.Header, dr)
                Tfield.ItemTemplate = New AccountProspectContactGrid(ListItemType.Item, dr)
                gvContacts.Columns.Add(Tfield)

                gvContacts.DataSource = dtContact

                gvContacts.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub sc_GetProjects()
            Try
                m_aryRightsForProject = GetUserRightsForPage_Other(4, 8)
                If m_aryRightsForProject(RIGHTSTYPE.VIEW) = 0 Then Exit Sub
                If objProspects Is Nothing Then objProspects = New CProspects
                objProspects.DivisionID = lngDivId
                objProspects.ProjectStatus = ddlProjectStatus.SelectedValue
                objProspects.DomainID = Session("DomainID")
                objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dgProjectsOpen.DataSource = objProspects.GetProjectsForOrg

                dgProjectsOpen.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub sb_CompanyInfo()
            Try
                objLeads.DivisionID = lngDivId
                objLeads.DomainID = Session("DomainID")
                objLeads.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objLeads.ContactID = txtContId.Text
                objLeads.GetCompanyDetails()

                If objProspects Is Nothing Then objProspects = New CProspects
                Dim dtComInfo As DataTable
                objProspects.DivisionID = lngDivId
                objProspects.DomainID = Session("DomainID")
                objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtComInfo = objProspects.GetCompanyInfoForEdit
                If Not IsPostBack Then
                    If dtComInfo.Rows.Count > 0 Then
                        If CCommon.ToLong(dtComInfo.Rows(0)("numCompanyType")) = 47 Then
                            Me.Page.Title = "Vendor"
                        Else
                            Me.Page.Title = "Account"
                            If Not radOppTab.FindTabByValue("VendorLeadTimes") Is Nothing Then
                                radOppTab.Tabs.FindTabByValue("VendorLeadTimes").Visible = False
                            End If
                        End If

                        lblRecordOwner.Text = Convert.ToString(dtComInfo.Rows(0).Item("RecOwner"))
                        lblCreatedBy.Text = Convert.ToString(dtComInfo.Rows(0).Item("vcCreatedBy"))
                        lblLastModifiedBy.Text = Convert.ToString(dtComInfo.Rows(0).Item("vcModifiedBy"))
                        hfOwner.Value = CCommon.ToLong(dtComInfo.Rows(0).Item("numRecOwner"))
                        hfTerritory.Value = CCommon.ToLong(dtComInfo.Rows(0).Item("numTerID"))

                        If CCommon.ToLong(dtComInfo.Rows(0).Item("numAssignedTo")) <> CCommon.ToLong(Session("UserContactID")) AndAlso CCommon.ToLong(Session("UserDivisionID")) <> lngDivId Then
                            GetUserRightsForRecord(MODULEID.Accounts, 4, CCommon.ToLong(hfOwner.Value), CCommon.ToLong(hfTerritory.Value))
                        End If

                        If Not IsDBNull(dtComInfo.Rows(0).Item("numCompanyCredit")) Then
                            If Not ddlCreditLimit.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyCredit")) Is Nothing Then
                                ddlCreditLimit.ClearSelection()
                                ddlCreditLimit.Items.FindByValue(dtComInfo.Rows(0).Item("numCompanyCredit")).Selected = True
                            End If
                        End If

                        lblCustomerId.Text = IIf(CCommon.ToString(objLeads.CompanyTypeName) <> "", "<b>" & objLeads.CompanyTypeName & " :&nbsp;&nbsp;</b>" & dtComInfo.Rows(0).Item("vcCompanyName"), "<b>Customer :&nbsp;&nbsp;</b>" & dtComInfo.Rows(0).Item("vcCompanyName"))
                        '  txtDivision.Text = dtComInfo.Rows(0).Item("vcDivisionName")
                        'Territory
                        If objLeads.CompanyType > 0 And objLeads.CompanyType <> 46 Then 'bug id 420
                            radOppTab.Tabs(0).Text = "&nbsp;&nbsp;" & objLeads.CompanyTypeName & " Detail&nbsp;&nbsp;"
                            lblTitle.Text = objLeads.CompanyTypeName & " Details"
                            If objLeads.CompanyTypeName = "Employer" Then
                                radOppTab.Tabs.FindTabByText("Correspondence").Visible = False
                            End If
                        End If


                        If Not IsDBNull(dtComInfo.Rows(0).Item("fltInterest")) Then
                            txtInterest.Text = String.Format("{0:#,##0.00}", dtComInfo.Rows(0).Item("fltInterest"))
                        End If

                        If Not ddlDefaultExpense.Items.FindByValue(dtComInfo.Rows(0).Item("numDefaultExpenseAccountID")) Is Nothing Then
                            ddlDefaultExpense.ClearSelection()
                            ddlDefaultExpense.Items.FindByValue(dtComInfo.Rows(0).Item("numDefaultExpenseAccountID")).Selected = True
                        End If

                        If Not ddlClass.Items.FindByValue(dtComInfo.Rows(0).Item("numAccountClassID")) Is Nothing Then
                            ddlClass.ClearSelection()
                            ddlClass.Items.FindByValue(dtComInfo.Rows(0).Item("numAccountClassID")).Selected = True
                        End If

                        If Not ddlPriceLevel.Items.FindByValue(dtComInfo.Rows(0).Item("tintPriceLevel")) Is Nothing Then
                            ddlPriceLevel.ClearSelection()
                            ddlPriceLevel.Items.FindByValue(dtComInfo.Rows(0).Item("tintPriceLevel")).Selected = True
                        End If

                        If Not ddlInbound850PickItem.Items.FindByValue(dtComInfo.Rows(0).Item("tintInbound850PickItem")) Is Nothing Then
                            ddlInbound850PickItem.ClearSelection()
                            ddlInbound850PickItem.Items.FindByValue(dtComInfo.Rows(0).Item("tintInbound850PickItem")).Selected = True
                        End If

                        If Not IsDBNull(dtComInfo.Rows(0).Item("intShippingCompany")) AndAlso CCommon.ToInteger(dtComInfo.Rows(0).Item("intShippingCompany")) > 0 Then
                            If Not radShipCompany.Items.FindItemByValue(dtComInfo.Rows(0).Item("intShippingCompany")) Is Nothing Then
                                radShipCompany.ClearSelection()
                                radShipCompany.Items.FindItemByValue(dtComInfo.Rows(0).Item("intShippingCompany")).Selected = True
                            End If
                        End If

                        chkShippingLabel.Checked = CCommon.ToBool(dtComInfo.Rows(0).Item("bitShippingLabelRequired"))
                        chkAutoCheckCustomerPart.Checked = CCommon.ToBool(dtComInfo.Rows(0).Item("bitAutoCheckCustomerPart"))

                        ddlDefaultShippingMethod.Items.Clear()
                        ddlDefaultShippingMethod.Items.Add(New ListItem("-- Select One --", "0"))
                        OppBizDocs.LoadServiceTypes(CCommon.ToLong(radShipCompany.SelectedValue), ddlDefaultShippingMethod)

                        If Not ddlDefaultShippingMethod.Items.FindByValue(dtComInfo.Rows(0).Item("numDefaultShippingServiceID")) Is Nothing Then
                            ddlDefaultShippingMethod.ClearSelection()
                            ddlDefaultShippingMethod.Items.FindByValue(dtComInfo.Rows(0).Item("numDefaultShippingServiceID")).Selected = True
                        End If

                        If Not IsDBNull(dtComInfo.Rows(0).Item("numBillingDays")) Then
                            If Not ddlNetDays.Items.FindByValue(dtComInfo.Rows(0).Item("numBillingDays")) Is Nothing Then
                                ddlNetDays.ClearSelection()
                                ddlNetDays.Items.FindByValue(dtComInfo.Rows(0).Item("numBillingDays")).Selected = True
                            End If
                        End If

                        If Not IsDBNull(dtComInfo.Rows(0).Item("numCurrencyID")) AndAlso CCommon.ToLong(dtComInfo.Rows(0).Item("numCurrencyID")) > 0 Then
                            If Not ddlCurrency.Items.FindByValue(dtComInfo.Rows(0).Item("numCurrencyID")) Is Nothing Then
                                ddlCurrency.ClearSelection()
                                ddlCurrency.Items.FindByValue(dtComInfo.Rows(0).Item("numCurrencyID")).Selected = True
                            End If
                        End If

                        If Not IsDBNull(dtComInfo.Rows(0).Item("numDefaultPaymentMethod")) AndAlso CCommon.ToLong(dtComInfo.Rows(0).Item("numDefaultPaymentMethod")) > 0 Then
                            If Not ddlPaymentMethod.Items.FindByValue(dtComInfo.Rows(0).Item("numDefaultPaymentMethod")) Is Nothing Then
                                ddlPaymentMethod.ClearSelection()
                                ddlPaymentMethod.Items.FindByValue(dtComInfo.Rows(0).Item("numDefaultPaymentMethod")).Selected = True
                            End If
                        End If

                        If Not IsDBNull(dtComInfo.Rows(0).Item("bitSaveCreditCardInfo")) AndAlso CCommon.ToBool(dtComInfo.Rows(0).Item("bitSaveCreditCardInfo")) = True Then
                            hrfcc.Enabled = False
                        End If
                        lblcc.Text = ""
                        chkOnCreditHold.Checked = CCommon.ToBool(dtComInfo.Rows(0).Item("bitOnCreditHold"))
                        chkAllocateInventoryFromPickList.Checked = CCommon.ToBool(dtComInfo.Rows(0).Item("bitAllocateInventoryOnPickList"))

                        objProspects.DomainID = Session("DomainID")
                        dtCompanyTaxTypes = objProspects.GetCompanyTaxTypes
                        Dim dr As DataRow
                        dr = dtCompanyTaxTypes.NewRow
                        dr("numTaxItemID") = 0
                        dr("vcTaxName") = "Sales Tax(Default)"
                        dr("bitApplicable") = IIf(dtComInfo.Rows(0).Item("bitNoTax") = True, False, True)
                        dtCompanyTaxTypes.Rows.Add(dr)


                        chkTaxItems.DataTextField = "vcTaxName"
                        chkTaxItems.DataValueField = "numTaxItemID"
                        chkTaxItems.DataSource = dtCompanyTaxTypes
                        chkTaxItems.DataBind()
                        Dim i As Integer
                        For i = 0 To dtCompanyTaxTypes.Rows.Count - 1
                            If Not IsDBNull(dtCompanyTaxTypes.Rows(i).Item("bitApplicable")) Then
                                If dtCompanyTaxTypes.Rows(i).Item("bitApplicable") = True Then
                                    chkTaxItems.Items.FindByValue(dtCompanyTaxTypes.Rows(i).Item("numTaxItemID")).Selected = True
                                Else
                                    chkTaxItems.Items.FindByValue(dtCompanyTaxTypes.Rows(i).Item("numTaxItemID")).Selected = False
                                End If
                            End If
                        Next

                        'Commented by Neelam - Obsolete functionality
                        'hplDocumentCount.Attributes.Add("onclick", "return OpenDocuments(" & lngDivId & ")")
                        'hplDocumentCount.Text = "(" & CCommon.ToLong(dtComInfo.Rows(0)("DocumentCount")) & ")"
                        'imgOpenDocument.Attributes.Add("onclick", "return OpenDocuments(" & lngDivId & ")")
                        imgOpenDocument.Attributes.Add("onclick", "OpenDocumentFiles(" & lngDivId.ToString() & ");return false;")
                        hplOpenDocument.Attributes.Add("onclick", "OpenDocumentFiles(" & lngDivId.ToString() & ");return false;")
                        LoadShippingConfiguration()
                    End If
                End If
                LoadControls()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub

        Private Sub LoadShippingConfiguration()
            Try
                Dim objDMSC As New DivisionMasterShippingConfiguration
                objDMSC.DomainID = CCommon.ToLong(Session("DomainID"))
                objDMSC.DivisionID = lngDivId
                Dim dt As DataTable = objDMSC.GetByDivision()

                If CCommon.ToLong(radShipCompany.SelectedValue) = 91 Then 'fedex
                    divSignatureType.Visible = True
                    divCOD1.Visible = False

                    chkOther.Items.Clear()
                    chkOther.Items.Add(New ListItem("COD", "COD"))
                    chkOther.Items.Add(New ListItem("Home Delivery Premium", "Home Delivery Premium"))
                    chkOther.Items.Add(New ListItem("Inside Delivery", "Inside Delivery"))
                    chkOther.Items.Add(New ListItem("Inside Pickup", "Inside Pickup"))
                    chkOther.Items.Add(New ListItem("Saturday Delivery", "Saturday Delivery"))
                    chkOther.AutoPostBack = True

                    ddlCODType.Items.Clear()
                    ddlCODType.Items.Add(New ListItem("-- Select One --", "-- Select One --"))
                    ddlCODType.Items.Add(New ListItem("Any", "Any"))
                    ddlCODType.Items.Add(New ListItem("Cash", "Cash"))
                    ddlCODType.Items.Add(New ListItem("Guaranted Funds", "Guaranted Funds"))

                    If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                        If Not ddlSignatureType.Items.FindByValue(CCommon.ToString(dt.Rows(0)("vcSignatureType"))) Is Nothing Then
                            ddlSignatureType.Items.FindByValue(dt.Rows(0)("vcSignatureType")).Selected = True
                        End If

                        If CCommon.ToBool(dt.Rows(0)("IsCOD")) Then
                            chkOther.Items.FindByValue("COD").Selected = True
                        End If

                        If CCommon.ToBool(dt.Rows(0)("IsHomeDelivery")) Then
                            chkOther.Items.FindByValue("Home Delivery Premium").Selected = True
                        End If

                        If CCommon.ToBool(dt.Rows(0)("IsInsideDelevery")) Then
                            chkOther.Items.FindByValue("Inside Delivery").Selected = True
                        End If

                        If CCommon.ToBool(dt.Rows(0)("IsInsidePickup")) Then
                            chkOther.Items.FindByValue("Inside Pickup").Selected = True
                        End If

                        If CCommon.ToBool(dt.Rows(0)("IsSaturdayDelivery")) Then
                            chkOther.Items.FindByValue("Saturday Delivery").Selected = True
                        End If

                        If Not ddlCODType.Items.FindByValue(CCommon.ToString(dt.Rows(0)("vcCODType"))) Is Nothing Then
                            ddlCODType.Items.FindByValue(dt.Rows(0)("vcCODType")).Selected = True
                        End If

                        txtDescription.Text = CCommon.ToString(dt.Rows(0)("vcDescription"))
                    End If
                ElseIf CCommon.ToLong(radShipCompany.SelectedValue) = 88 Then 'UPS
                    divSignatureType.Visible = True
                    divCOD1.Visible = True

                    chkOther.Items.Clear()
                    chkOther.Items.Add(New ListItem("Saturday Delivery", "Saturday Delivery"))
                    chkOther.Items.Add(New ListItem("Saturday Pickup", "Saturday Pickup"))
                    chkOther.Items.Add(New ListItem("Additional Handling", "Additional Handling"))
                    chkOther.Items.Add(New ListItem("Large Package", "Large Package"))
                    chkOther.AutoPostBack = False

                    ddlCODType.Items.Clear()
                    ddlCODType.Items.Add(New ListItem("-- Select One --", "-- Select One --"))
                    ddlCODType.Items.Add(New ListItem("Any Check", "Any Check"))
                    ddlCODType.Items.Add(New ListItem("Cashier's Check Or Money Order", "Cashier's Check Or Money Order"))

                    If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                        If CCommon.ToBool(dt.Rows(0)("IsSaturdayDelivery")) Then
                            chkOther.Items.FindByValue("Saturday Delivery").Selected = True
                        End If

                        If CCommon.ToBool(dt.Rows(0)("IsSaturdayPickup")) Then
                            chkOther.Items.FindByValue("Saturday Pickup").Selected = True
                        End If

                        If CCommon.ToBool(dt.Rows(0)("IsAdditionalHandling")) Then
                            chkOther.Items.FindByValue("Additional Handling").Selected = True
                        End If

                        If CCommon.ToBool(dt.Rows(0)("IsLargePackage")) Then
                            chkOther.Items.FindByValue("Large Package").Selected = True
                        End If

                        If Not ddlSignatureType.Items.FindByValue(CCommon.ToString(dt.Rows(0)("vcSignatureType"))) Is Nothing Then
                            ddlSignatureType.Items.FindByValue(dt.Rows(0)("vcSignatureType")).Selected = True
                        End If

                        txtDescription.Text = CCommon.ToString(dt.Rows(0)("vcDescription"))

                        If Not ddlCODType.Items.FindByValue(CCommon.ToString(dt.Rows(0)("vcCODType"))) Is Nothing Then
                            ddlCODType.Items.FindByValue(dt.Rows(0)("vcCODType")).Selected = True
                        End If
                    End If
                Else
                    divSignatureType.Visible = True
                    divCOD1.Visible = False

                    If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                        txtDescription.Text = CCommon.ToString(dt.Rows(0)("vcDescription"))

                        If Not ddlSignatureType.Items.FindByValue(CCommon.ToString(dt.Rows(0)("vcSignatureType"))) Is Nothing Then
                            ddlSignatureType.Items.FindByValue(dt.Rows(0)("vcSignatureType")).Selected = True
                        End If
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub LoadControls()
            Try
                tblMain.Controls.Clear()
                Dim ds As DataSet
                Dim objPageLayout As New CPageLayout
                Dim fields() As String

                objPageLayout.CoType = "P"
                objPageLayout.UserCntID = Session("UserContactId")
                objPageLayout.RecordId = lngDivId
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.PageId = 1
                objPageLayout.numRelCntType = objLeads.CompanyType

                objPageLayout.FormId = 36
                objPageLayout.PageType = 3

                ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
                dtTableInfo = ds.Tables(0)

                If Not Page.IsPostBack Then
                    Dim dsFieldGroups As New DataSet()
                    Dim objBizFormWizardFormConf As New BizFormWizardMasterConfiguration
                    objBizFormWizardFormConf.DomainID = CCommon.ToLong(Session("DomainID"))
                    objBizFormWizardFormConf.numFormID = 101
                    'numGroupID is always 0 in table so line is commented
                    'objBizFormWizardFormConf.numGroupID = CCommon.ToLong(Session("UserGroupID"))
                    objBizFormWizardFormConf.tintPageType = 4
                    dsFieldGroups = objBizFormWizardFormConf.ManageFormFieldGroup()
                    ddlSectionFile.DataSource = dsFieldGroups.Tables(0)
                    ddlSectionFile.DataValueField = "numFormFieldGroupId"
                    ddlSectionFile.DataTextField = "vcGroupName"
                    ddlSectionFile.DataBind()
                    ddlSectionFile.Items.Insert(0, New ListItem("-- Select One --", "0"))

                    ddlSectionLink.DataSource = dsFieldGroups.Tables(0)
                    ddlSectionLink.DataValueField = "numFormFieldGroupId"
                    ddlSectionLink.DataTextField = "vcGroupName"
                    ddlSectionLink.DataBind()
                    ddlSectionLink.Items.Insert(0, New ListItem("-- Select One --", "0"))
                End If

                Dim objOpp As New BusinessLogic.Opportunities.OppotunitiesIP
                Dim dtDocLinkName As New DataTable
                With objOpp
                    .DomainID = Session("DomainID")
                    .DivisionID = lngDivId
                    dtDocLinkName = .GetDocumentFilesLink("P")
                End With

                If Not dtDocLinkName Is Nothing AndAlso dtDocLinkName.Rows.Count > 0 Then
                    ShowDocuments(tblDocuments, dtDocLinkName, dtTableInfo, 0)
                End If

                Dim intCol1Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=1")
                Dim intCol2Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=2")
                Dim intCol1 As Int32 = 0
                Dim intCol2 As Int32 = 0
                Dim tblCell As TableCell

                Dim tblRow As TableRow
                Const NoOfColumns As Short = 2
                Dim ColCount As Int32 = 0
                ''If intermediatory Page is enabled then Add the text "Name" to dropdown fields
                objPageControls.CreateTemplateRow(tblMain)

                objPageControls.ContactID = objLeads.ContactID
                objPageControls.DivisionID = objLeads.DivisionID
                objPageControls.TerritoryID = objLeads.TerritoryID
                objPageControls.strPageType = "Organization"
                objPageControls.EditPermission = m_aryRightsForPage(RIGHTSTYPE.UPDATE)
                Dim dv As DataView = dtTableInfo.DefaultView
                Dim dvExcutedView As DataView = dtTableInfo.DefaultView
                Dim dvExcutedGroupsView As DataView = dtTableInfo.DefaultView
                Dim fieldGroups = (From dRow In dtTableInfo.Rows
                                    Select New With {Key .groupId = dRow("numFormFieldGroupId"), Key .groupName = dRow("vcGroupName"), Key .order = dRow("numOrder")}).Distinct()

                For Each drGroup In fieldGroups.OrderBy(Function(x) x.order)
                    tblCell = New TableCell()
                    tblRow = New TableRow
                    tblCell.ColumnSpan = 4
                    tblCell.CssClass = "tableGroupHeader"
                    tblCell.Text = CCommon.ToString(drGroup.groupName)
                    tblRow.Cells.Add(tblCell)
                    tblMain.Rows.Add(tblRow)

                    If drGroup.groupId > 0 AndAlso Not dtDocLinkName Is Nothing AndAlso dtDocLinkName.Rows.Count > 0 Then
                        If dtDocLinkName.Select("numFormFieldGroupId=" & drGroup.groupId).Length > 0 Then
                            Dim tableDocs As New HtmlTable
                            tableDocs.Style.Add("float", "right")
                            ShowDocuments(tableDocs, dtDocLinkName, dtTableInfo, drGroup.groupId)

                            tblCell = New TableCell()
                            tblRow = New TableRow
                            tblCell.ColumnSpan = 4
                            tblCell.Controls.Add(tableDocs)
                            tblRow.Cells.Add(tblCell)
                            tblMain.Rows.Add(tblRow)
                        End If
                    End If

                    dvExcutedView.RowFilter = "numFormFieldGroupId = " & drGroup.groupId
                    tblRow = New TableRow
                    tblCell = New TableCell
                    intCol1Count = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=1 AND numFormFieldGroupId='" & drGroup.groupId & "'")
                    intCol2Count = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=2 AND numFormFieldGroupId='" & drGroup.groupId & "'")
                    intCol1 = 0
                    intCol2 = 0
                    For Each drData As DataRowView In dvExcutedView
                        Dim dr As DataRow
                        dr = drData.Row

                        If boolIntermediatoryPage = True Then
                            If Not IsDBNull(dr("vcPropertyName")) And (dr("fld_type") = "SelectBox" Or dr("fld_type") = "SelectBox") And dr("bitCustomField") = False AndAlso dr("vcPropertyName") <> "numPartenerSource" Then
                                dr("vcPropertyName") = dr("vcPropertyName") & "Name"
                            End If
                        End If


                        If CCommon.ToString(dr("vcPropertyName")) = "vcCreditCards" Then
                            Dim dsCCInfo As DataTable
                            Dim objOppInvoice As New OppInvoice
                            objOppInvoice.DomainID = Session("DomainID")
                            objOppInvoice.DivisionID = lngDivId
                            dsCCInfo = objOppInvoice.GetCustomerCCInfo(numCCInfoID:=0, numContactId:=0, isdefault:=0)

                            If Not dsCCInfo Is Nothing AndAlso dsCCInfo.Rows.Count > 0 Then
                                Dim objEncryption As New QueryStringValues
                                Dim strCard As String = objEncryption.Decrypt(dsCCInfo.Rows(0)("vcCreditCardNo").ToString())
                                If strCard.Length > 4 Then
                                    dr("vcValue") = objEncryption.Decrypt(dsCCInfo.Rows(0)("vcCardHolder").ToString()) & ", ************" & strCard.Substring(strCard.Length - 4, 4)
                                Else
                                    dr("vcValue") = "NA"
                                End If
                            End If
                        ElseIf CCommon.ToString(dr("vcPropertyName")) = "vcCompactContactDetails" Then
                            Dim strData As New StringBuilder()
                            strData.Append("<a class='text-yellow' href='javascript:OpenContact(" & objLeads.ContactID & ",1)'>" & objLeads.GivenName & "</a> &nbsp;")
                            strData.Append(objLeads.ContactPhone & "&nbsp;")
                            If objLeads.PhoneExt <> "" Then
                                strData.Append("&nbsp;(" & objLeads.PhoneExt & ")")
                            End If
                            If objLeads.Email <> "" Then
                                strData.Append("<img src='../images/msg_unread_small.gif' email=" & objLeads.Email & " id='imgEmailPopupId' onclick='return OpemParticularEmail(" & objLeads.ContactID & ")' />")
                            End If
                            dr("vcValue") = strData
                        ElseIf CCommon.ToString(dr("vcPropertyName")) = "Purchaseincentives" Then
                            Dim strData As New StringBuilder()
                            strData.Append("<Span>" & HttpUtility.HtmlDecode(objLeads.Purchaseincentives) & "</Span>")
                            dr("vcValue") = strData
                        ElseIf CCommon.ToString(dr("vcPropertyName")) = "intDropShipName" Then
                            dr("vcValue") = objLeads.vcFropShip
                        ElseIf Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False Then
                            dr("vcValue") = objLeads.GetType.GetProperty(dr("vcPropertyName")).GetValue(objLeads, Nothing)
                        End If

                        If ColCount = 0 Then
                            tblRow = New TableRow
                        End If

                        If intCol1Count = intCol1 And intCol2Count > intCol1Count Then
                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)
                            ColCount = 1
                        End If

                        If (dr("fld_type") = "ListBox" Or dr("fld_type") = "SelectBox") Then
                            Dim dtData As DataTable

                            If Not IsDBNull(dr("vcPropertyName")) Then
                                If dr("vcPropertyName") = "AssignedTo" Then ' dr("numFieldId") = 64 Then
                                    Dim m_aryRightsForAssignedTo() As Integer = GetUserRightsForPage_Other(4, 128)
                                    If m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 0 Then
                                        dr("bitCanBeUpdated") = False
                                    ElseIf m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 1 Then
                                        Try
                                            If CCommon.ToLong(hfOwner.Value) <> Session("UserContactID") Then
                                                dr("bitCanBeUpdated") = False
                                            End If
                                        Catch ex As Exception
                                        End Try
                                    ElseIf m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 2 Then
                                        Dim i As Integer
                                        Dim dtTerritory As DataTable
                                        dtTerritory = Session("UserTerritory")
                                        If CCommon.ToLong(hfTerritory.Value) = 0 Then
                                            dr("bitCanBeUpdated") = False
                                        Else
                                            Dim chkDelete As Boolean = False
                                            For i = 0 To dtTerritory.Rows.Count - 1
                                                If CCommon.ToLong(hfTerritory.Value) = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                                    chkDelete = True
                                                End If
                                            Next
                                            If chkDelete = False Then
                                                dr("bitCanBeUpdated") = False
                                            End If
                                        End If
                                    Else
                                        dr("bitCanBeUpdated") = True
                                    End If

                                    If boolIntermediatoryPage = False Then
                                        If Session("PopulateUserCriteria") = 1 Then
                                            dtData = objCommon.ConEmpListFromTerritories(Session("DomainID"), 0, 0, objLeads.TerritoryID)
                                        ElseIf Session("PopulateUserCriteria") = 2 Then
                                            dtData = objCommon.ConEmpList(Session("DomainID"), Session("UserContactID"))
                                        Else
                                            dtData = objCommon.ConEmpList(Session("DomainID"), 0, 0)
                                        End If

                                    End If
                                ElseIf dr("vcPropertyName") = "AssignedToName" Then
                                    Dim m_aryRightsForAssignedTo() As Integer = GetUserRightsForPage_Other(4, 128)
                                    If m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 0 Then
                                        dr("bitCanBeUpdated") = False
                                    ElseIf m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 1 Then
                                        Try
                                            If CCommon.ToLong(hfOwner.Value) <> Session("UserContactID") Then
                                                dr("bitCanBeUpdated") = False
                                            End If
                                        Catch ex As Exception
                                        End Try
                                    ElseIf m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 2 Then
                                        Dim i As Integer
                                        Dim dtTerritory As DataTable
                                        dtTerritory = Session("UserTerritory")
                                        If CCommon.ToLong(hfTerritory.Value) = 0 Then
                                            dr("bitCanBeUpdated") = False
                                        Else
                                            Dim chkDelete As Boolean = False
                                            For i = 0 To dtTerritory.Rows.Count - 1
                                                If CCommon.ToLong(hfTerritory.Value) = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                                    chkDelete = True
                                                End If
                                            Next
                                            If chkDelete = False Then
                                                dr("bitCanBeUpdated") = False
                                            End If
                                        End If
                                    Else
                                        dr("bitCanBeUpdated") = True
                                    End If
                                ElseIf dr("vcPropertyName") = "CampaignID" Then ' dr("numFieldId") = 67 Then 'Campaign
                                    If boolIntermediatoryPage = False Then
                                        Dim objCampaign As New BusinessLogic.Reports.PredefinedReports
                                        objCampaign.byteMode = 2
                                        objCampaign.DomainID = Session("DomainID")
                                        dtData = objCampaign.GetCampaign()
                                    End If
                                ElseIf dr("vcPropertyName") = "intDropShip" Then
                                    objCommon.DomainID = Session("DomainID")
                                    dtData = objCommon.GetDropShip()
                                ElseIf dr("vcPropertyName") = "numPartenerSource" Then
                                    objCommon.DomainID = Session("DomainID")
                                    dtData = objCommon.GetPartnerSource()
                                ElseIf Not IsDBNull(dr("numListID")) And dr("numListID") <> 0 Then
                                    If boolIntermediatoryPage = False Then
                                        If dr("ListRelID") > 0 Then
                                            objCommon.Mode = 3
                                            objCommon.DomainID = Session("DomainID")
                                            If dr("ListRelID") = 834 Then 'Live Database ID is 834
                                                objCommon.PrimaryListItemID = objLeads.ShippingZone
                                            Else
                                                objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objLeads)
                                            End If
                                            objCommon.SecondaryListID = dr("numListId")
                                            dtData = objCommon.GetFieldRelationships.Tables(0)
                                        Else
                                            dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                        End If

                                    End If
                                End If
                            End If

                            If dr("vcDbColumnName") = "numCompanyDiff" Then
                                If boolIntermediatoryPage Then
                                    tblCell = New TableCell
                                    tblCell.Font.Bold = True
                                    tblCell.Text = IIf(dr("vcValue").ToString.Trim.Length = 0, "Company Differentiation", dr("vcValue").ToString)
                                    tblCell.HorizontalAlign = HorizontalAlign.Right

                                    tblCell.Attributes.Add("id", objPageControls.strPageType & "~" & dr("numFieldId").ToString & "~" & dr("bitCustomField").ToString & "~" & objPageControls.ContactID & "~" & objPageControls.DivisionID & "~" & objPageControls.TerritoryID & "~" & dr("ListRelID").ToString)
                                    tblCell.Attributes.Add("class", "editable_select")
                                    tblCell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                                    tblCell.Attributes.Add("onmouseout", "bgColor=''")

                                    tblRow.Cells.Add(tblCell)

                                    tblCell = New TableCell
                                    tblCell.Text = objLeads.CompanyDiffValue
                                    tblCell.Attributes.Add("id", objPageControls.strPageType & "~" & "2042" & "~" & "0" & "~" & objPageControls.ContactID & "~" & objPageControls.DivisionID & "~" & objPageControls.TerritoryID & "~" & dr("ListRelID").ToString)
                                    tblCell.Attributes.Add("class", "click")
                                    tblCell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                                    tblCell.Attributes.Add("onmouseout", "bgColor=''")

                                    tblRow.Cells.Add(tblCell)
                                Else
                                    Dim ddl As New DropDownList

                                    tblCell = New TableCell
                                    dr("vcFieldName") = dr("vcFieldName").ToString.Replace("$", "")
                                    ddl.CssClass = "signup"
                                    ddl.Width = 200
                                    ddl.ID = dr("numFieldId").ToString & dr("vcFieldName").ToString & "~" & IIf(IsDBNull(dr("numListId")), 0, dr("numListId").ToString).ToString
                                    ddl.DataSource = dtData
                                    ddl.DataValueField = dtData.Columns(0).ColumnName
                                    ddl.DataTextField = dtData.Columns(1).ColumnName
                                    ddl.DataBind()
                                    ddl.Items.Insert(0, "-- Select One --")
                                    ddl.Items.FindByText("-- Select One --").Value = "0"
                                    ddl.Enabled = True
                                    ddl.EnableViewState = False
                                    If Not IsDBNull(dr("vcValue")) Then
                                        If Not ddl.Items.FindByValue(dr("vcValue").ToString) Is Nothing Then
                                            ddl.Items.FindByValue(dr("vcValue").ToString).Selected = True
                                        End If
                                    End If
                                    tblCell.CssClass = "normal1"
                                    tblCell.Controls.Add(ddl)
                                    tblCell.HorizontalAlign = HorizontalAlign.Right
                                    tblRow.Cells.Add(tblCell)


                                    tblCell = New TableCell
                                    tblCell.Width = Unit.Percentage(25)
                                    Dim txt As TextBox
                                    txt = New TextBox
                                    txt.Width = 200
                                    txt.Height = 35
                                    txt.CssClass = "form-control"
                                    txt.Text = objLeads.CompanyDiffValue
                                    txt.EnableViewState = False
                                    txt.ID = "2042" & dr("vcFieldName").ToString
                                    tblCell.CssClass = "normal1"
                                    tblCell.Controls.Add(txt)

                                    tblRow.Cells.Add(tblCell)

                                    If CLng(dr("DependentFields")) > 0 And Not boolIntermediatoryPage Then
                                        ddl.AutoPostBack = True
                                        AddHandler ddl.SelectedIndexChanged, AddressOf PopulateDependentDropdown
                                    End If
                                End If
                            ElseIf boolIntermediatoryPage Then
                                objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData)
                            Else
                                Dim ddl As DropDownList

                                If dr("vcPropertyName") = "AssignedTo" Then
                                    ddl = objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData, boolEnabled:=CCommon.ToBool(dr("bitCanBeUpdated")))
                                Else
                                    ddl = objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData)
                                End If

                                If CLng(dr("DependentFields")) > 0 And Not boolIntermediatoryPage Then
                                    ddl.AutoPostBack = True
                                    AddHandler ddl.SelectedIndexChanged, AddressOf PopulateDependentDropdown
                                End If
                            End If
                        ElseIf dr("fld_type") = "CheckBoxList" Then
                            If Not IsDBNull(dr("vcPropertyName")) Then
                                Dim dtData As DataTable

                                If boolIntermediatoryPage = False Then
                                    If dr("ListRelID") > 0 Then
                                        objCommon.Mode = 3
                                        objCommon.DomainID = Session("DomainID")
                                        If dr("ListRelID") = 834 Then 'Live Database ID is 834
                                            objCommon.PrimaryListItemID = objLeads.ShippingZone
                                        Else
                                            objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objLeads)
                                        End If
                                        objCommon.SecondaryListID = dr("numListId")
                                        dtData = objCommon.GetFieldRelationships.Tables(0)
                                    Else
                                        dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                    End If
                                End If

                                objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData)
                            End If
                        Else
                            objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, RecordID:=lngDivId)
                        End If

                        If intCol2Count = intCol2 And intCol1Count > intCol2Count Then
                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            ColCount = 1
                        End If


                        If dr("intcoulmn") = 2 Then
                            If intCol1 <> intCol1Count Then
                                intCol1 = intCol1 + 1
                            End If

                            If intCol2 <> intCol2Count Then
                                intCol2 = intCol2 + 1
                            End If
                        End If

                        ColCount = ColCount + 1
                        If NoOfColumns = ColCount Then
                            ColCount = 0
                            tblMain.Rows.Add(tblRow)
                        End If
                    Next
                Next
                If ColCount > 0 Then
                    tblMain.Rows.Add(tblRow)
                End If

                'objPageControls.CreateComments(tblMain, objLeads.Comments, boolIntermediatoryPage)
                'Add Client Side validation for custom fields
                If Not boolIntermediatoryPage Then
                    Dim strValidation As String = objPageControls.GenerateValidationScript(dtTableInfo)
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "CFvalidation", strValidation, True)
                ElseIf Session("InlineEdit") = True And m_aryRightsForPage(RIGHTSTYPE.UPDATE) <> 0 Then
                    Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtTableInfo)
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "InlineEditValidation", strValidation, True)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveTaxTypes()
            Try
                dtCompanyTaxTypes = New DataTable
                dtCompanyTaxTypes.Columns.Add("numTaxItemID")
                dtCompanyTaxTypes.Columns.Add("bitApplicable")
                Dim dr As DataRow
                Dim i As Integer
                For i = 0 To chkTaxItems.Items.Count - 1
                    If chkTaxItems.Items(i).Selected = True Then
                        dr = dtCompanyTaxTypes.NewRow
                        dr("numTaxItemID") = chkTaxItems.Items(i).Value
                        dr("bitApplicable") = 1
                        dtCompanyTaxTypes.Rows.Add(dr)
                    End If
                Next
                Dim ds As New DataSet
                Dim strdetails As String
                dtCompanyTaxTypes.TableName = "Table"
                ds.Tables.Add(dtCompanyTaxTypes)
                strdetails = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))
                If objProspects Is Nothing Then
                    objProspects = New CProspects
                End If
                objProspects.DivisionID = lngDivId
                objProspects.strCompanyTaxTypes = strdetails
                objProspects.ManageCompanyTaxTypes()
                ds.Dispose()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub lbtnDemote_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbtnDemote.Click
            Try
                If objAccounts Is Nothing Then objAccounts = New CAccounts
                Dim intCount As Integer
                objAccounts.DivisionID = lngDivId
                intCount = objAccounts.CheckDealsWon
                If intCount = 0 Then
                    objLeads.DivisionID = lngDivId
                    objLeads.byteMode = 1
                    objLeads.DomainID = Session("DomainID")
                    objLeads.UserCntID = Session("UserContactID")
                    objLeads.DemoteOrg()
                    Response.Redirect("../prospects/frmProspects.aspx?frm=prospectlist&DivID=" & lngDivId)
                Else : litMessage.Text = "This record can not be demoted because it contains completed deals. To demote this record, delete all deals from the Deal History section, then try demoting again."
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                objCommon.DivisionID = lngDivId
                objCommon.charModule = "D"
                objCommon.GetCompanySpecificValues1()
                SaveProspects(objCommon.CompID)
                SaveTaxTypes()
                If radOppTab.SelectedIndex = 5 Then
                    sb_DisplayAccounting()
                End If

                Dim objWfA As New BACRM.BusinessLogic.Workflow.Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = lngDivId
                objWfA.SaveWFOrganizationQueue()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnSaveTerms_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveTerms.Click
            Try
                objCommon.DivisionID = lngDivId
                objCommon.charModule = "D"
                objCommon.GetCompanySpecificValues1()

                With objLeads
                    .CompanyID = objCommon.CompID
                    .DomainID = Session("DomainID")
                    .DivisionID = lngDivId
                    .CompanyCredit = ddlCreditLimit.SelectedItem.Value
                    .UserCntID = Session("UserContactID")
                    .CRMType = 2
                    .DivisionName = ""
                    .BillingTerms = IIf(ddlNetDays.SelectedValue > 0, 1, 0)  'IIf(chkBillinTerms.Checked = True, 1, 0)
                    .BillingDays = ddlNetDays.SelectedValue 'IIf(txtNetDays.Text = "", 0, txtNetDays.Text)
                    .InterestType = IIf(radPlus.Checked = True, 1, 0)
                    .Interest = IIf(Replace(txtInterest.Text, ",", "") = "", 0, Replace(txtInterest.Text, ",", ""))
                    .NoTax = IIf(chkTaxItems.Items.Count > 0 AndAlso chkTaxItems.Items.FindByValue(0).Selected = True, False, True)
                    .CurrencyID = ddlCurrency.SelectedValue
                    .DefaultPaymentMethod = ddlPaymentMethod.SelectedValue
                    ' .DefaultCreditCard = ddlDefaultCreditCard.SelectedValue
                    .OnCreditHold = chkOnCreditHold.Checked
                    .IsAllocateInventoryOnPickList = chkAllocateInventoryFromPickList.Checked
                    .ShippingCompany = CCommon.ToInteger(radShipCompany.SelectedValue)
                    .DefaultExpenseAccountID = CCommon.ToLong(ddlDefaultExpense.SelectedValue)
                    .AccountClass = CCommon.ToLong(ddlClass.SelectedValue)
                    .PriceLevel = CCommon.ToInteger(ddlPriceLevel.SelectedValue)
                    .DefaultShippingService = CCommon.ToInteger(ddlDefaultShippingMethod.SelectedValue)
                    .IsShippingLabelRequired = CCommon.ToBool(chkShippingLabel.Checked)
                    .InBound850PickItem = CCommon.ToInteger(ddlInbound850PickItem.SelectedValue)
                    .IsAutoCheckCustomerPart = CCommon.ToInteger(chkAutoCheckCustomerPart.Checked)
                End With
                objLeads.UpdateBillingTerms()
                SaveTaxTypes()
                SaveShippingConfiguration()

                Dim objWfA As New BACRM.BusinessLogic.Workflow.Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = lngDivId
                objWfA.SaveWFOrganizationQueue()

                litTermsMessage.Text = "Details saved successfully"
            Catch ex As Exception
                If ex.Message.Contains("NOT_ALLOWED_CHANGE_ALLOCATE_INVENTORY_FROM_PACKING_SLIP_OPEN_SALES_ORDER") Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "OpenSalesOrder", "alert('Before you can change Allocate inventory from Packing Slip (Pick List), you have to close all open sales order(s)');", True)
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    DisplayError(ex.Message)
                End If
            End Try
        End Sub

        Private Sub SaveShippingConfiguration()
            Try
                Dim objDMSC As New DivisionMasterShippingConfiguration
                objDMSC.DomainID = CCommon.ToLong(Session("DomainID"))
                objDMSC.DivisionID = lngDivId

                If CCommon.ToLong(radShipCompany.SelectedValue) = 91 Then 'fedex
                    objDMSC.SignatureType = ddlSignatureType.SelectedValue

                    If chkOther.Items.FindByValue("COD").Selected Then
                        objDMSC.IsCOD = True
                    End If

                    If chkOther.Items.FindByValue("Home Delivery Premium").Selected Then
                        objDMSC.IsHomeDelivery = True
                    End If

                    If chkOther.Items.FindByValue("Inside Delivery").Selected Then
                        objDMSC.IsInsideDelevery = True
                    End If

                    If chkOther.Items.FindByValue("Inside Pickup").Selected Then
                        objDMSC.IsInsidePickup = True
                    End If

                    If chkOther.Items.FindByValue("Saturday Delivery").Selected Then
                        objDMSC.IsSaturdayDelivery = True
                    End If

                    objDMSC.Description = txtDescription.Text.Trim()
                ElseIf CCommon.ToLong(radShipCompany.SelectedValue) = 88 Then 'UPS
                    If chkOther.Items.FindByValue("Saturday Delivery").Selected Then
                        objDMSC.IsSaturdayDelivery = True
                    End If

                    If chkOther.Items.FindByValue("Saturday Pickup").Selected Then
                        objDMSC.IsSaturdayPickup = True
                    End If

                    If chkOther.Items.FindByValue("Additional Handling").Selected Then
                        objDMSC.IsAdditionalHandling = True
                    End If

                    If chkOther.Items.FindByValue("Large Package").Selected Then
                        objDMSC.IsLargePackage = True
                    End If

                    objDMSC.Description = txtDescription.Text.Trim()
                    objDMSC.CODType = ddlCODType.SelectedValue
                    objDMSC.SignatureType = ddlSignatureType.SelectedValue
                Else
                    objDMSC.Description = txtDescription.Text.Trim()
                    objDMSC.SignatureType = ddlSignatureType.SelectedValue
                End If

                objDMSC.Save()
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                objCommon.DivisionID = lngDivId
                objCommon.charModule = "D"
                objCommon.GetCompanySpecificValues1()
                SaveProspects(objCommon.CompID)
                SaveTaxTypes()

                Dim objWfA As New BACRM.BusinessLogic.Workflow.Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = lngDivId
                objWfA.SaveWFOrganizationQueue()

                PageRedirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub PageRedirect()
            Try
                If GetQueryStringVal("frm1") = "ActionItem" Then
                    Response.Redirect("../admin/ActionItemDetailsOld.aspx?CommId=" & GetQueryStringVal("CommID") & "&frm=" & GetQueryStringVal("frm"))
                End If
                If GetQueryStringVal("frm") = "ActItem" Then
                    Response.Redirect("../admin/ActionItemDetailsOld.aspx?CommId=" & GetQueryStringVal("CommID"))
                ElseIf GetQueryStringVal("frm") = "contactlist" Then
                    Response.Redirect("../contact/frmContactList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2 & "&ContactType=" & GetQueryStringVal("ContactType"))
                ElseIf GetQueryStringVal("frm") = "opportunitylist" Then
                    Response.Redirect("../opportunity/frmOpportunityList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "Caselist" Then
                    Response.Redirect("../cases/frmCaseList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "tickler" Then
                    Response.Redirect("../common/frmTicklerDisplay.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "ProductsShipped" Then
                    Response.Redirect("../reports/frmProductShipped.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "BestAccounts" Then
                    Response.Redirect("../reports/frmBestAccounts.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "ContactRole" Then
                    Response.Redirect("../reports/frmContactRole.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "CasesAgent" Then
                    Response.Redirect("../reports/frmCases.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "BizDocReport" Then
                    Response.Redirect("../reports/frmBizDocReport.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "Ecosystem" Then
                    Response.Redirect("../reports/frmEcosystemReport.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "BestPartners" Then
                    Response.Redirect("../reports/frmBestPartners.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "Portal" Then
                    Response.Redirect("../reports/frmSelfServicePortal.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "DealHistory" Then
                    Response.Redirect("../reports/frmRepDealHistory.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "CampDetails" Then
                    Response.Redirect("../Marketing/frmCampaignDetails.aspx?CampID=" & GetQueryStringVal("CampID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "oppdetail" Then
                    Response.Redirect("../opportunity/frmOpportunities.aspx?frm=" & GetQueryStringVal("frm1") & "&opId=" & GetQueryStringVal("opId") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "CaseDetail" Then
                    Response.Redirect("../cases/frmCases.aspx?frm=" & GetQueryStringVal("frm1") & "&CaseID=" & GetQueryStringVal("CaseID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "contactdetail" Then
                    Response.Redirect("../contact/frmContacts.aspx?CntId=" & GetQueryStringVal("CntID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "ProDetail" Then
                    Response.Redirect("../projects/frmProjects.aspx?frm=" & GetQueryStringVal("frm1") & "&ProId=" & GetQueryStringVal("ProId") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "CaseReport" Then
                    Response.Redirect("../reports/frmCases.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "CompanyList" Then
                    Response.Redirect("../prospects/frmCompanyList.aspx?RelId=" & GetQueryStringVal("RelID") & "&profileId=" & GetQueryStringVal("profileId") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "AvgSalesCycle1" Then
                    Response.Redirect("../reports/frmAvgSalesCycle1.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "AdvSearchSur" Then
                    Response.Redirect("../Admin/FrmAdvSurveyRes.aspx")
                ElseIf GetQueryStringVal("frm") = "doclist" Then
                    Response.Redirect("../Documents/frmDocList.aspx?Status=" & SI1 & "&Category=" & SI2)
                Else : Response.Redirect("../account/frmAccountList.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2 & "&profileId=" & GetQueryStringVal("profileid"))
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveProspects(ByVal CompID As Long)
            Try
                With objLeads
                    .CompanyID = CompID
                    .DomainID = Session("DomainID")
                    .DivisionID = lngDivId
                    .CompanyCredit = ddlCreditLimit.SelectedItem.Value
                    .UserCntID = Session("UserContactID")
                    .CRMType = 2
                    .LeadBoxFlg = 1
                    .DivisionName = ""
                    'txtDivision.Text
                    .BillingTerms = IIf(ddlNetDays.SelectedValue > 0, 1, 0)  'IIf(chkBillinTerms.Checked = True, 1, 0)
                    .BillingDays = ddlNetDays.SelectedValue 'IIf(txtNetDays.Text = "", 0, txtNetDays.Text)
                    .InterestType = IIf(radPlus.Checked = True, 1, 0)
                    .Interest = IIf(Replace(txtInterest.Text, ",", "") = "", 0, Replace(txtInterest.Text, ",", ""))
                    .NoTax = IIf(chkTaxItems.Items.FindByValue(0).Selected = True, False, True)
                    For Each dr As DataRow In dtTableInfo.Rows
                        If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False And dr("bitCanBeUpdated") = True Then
                            If dr("vcDbColumnName") = "numCompanyDiff" Then
                                dr("vcFieldName") = dr("vcFieldName").ToString.Replace("$", "")

                                objLeads.CompanyDiff = CType(radOppTab.MultiPage.FindControl(dr("numFieldId").ToString & dr("vcFieldName").ToString & "~" & IIf(IsDBNull(dr("numListId")), 0, dr("numListId").ToString).ToString), DropDownList).SelectedValue
                                objLeads.CompanyDiffValue = CType(radOppTab.MultiPage.FindControl("2042" & dr("vcFieldName").ToString), TextBox).Text
                            Else
                                If dr("vcPropertyName") = "numPartenerSource" Then
                                    dr("vcPropertyName") = "PartenerSource"
                                End If
                                If dr("vcPropertyName") = "numPartenerContact" Then
                                    dr("vcPropertyName") = "PartenerContact"
                                End If
                                objPageControls.SetValueForStaticFields(dr, objLeads, radOppTab.MultiPage)
                                If dr("vcPropertyName") = "PartenerSource" Then
                                    dr("vcPropertyName") = "numPartenerSource"
                                End If
                                If dr("vcPropertyName") = "PartenerContact" Then
                                    dr("vcPropertyName") = "numPartenerContact"
                                End If
                            End If
                        End If


                    Next

                    .CurrencyID = ddlCurrency.SelectedValue
                    .ShippingCompany = CCommon.ToInteger(radShipCompany.SelectedValue)
                    .IsShippingLabelRequired = chkShippingLabel.Checked
                    .DefaultExpenseAccountID = CCommon.ToLong(ddlDefaultExpense.SelectedValue)
                    .PriceLevel = CCommon.ToInteger(ddlPriceLevel.SelectedValue)
                    .AccountClass = CCommon.ToLong(ddlClass.SelectedValue)
                    .InBound850PickItem = CCommon.ToInteger(ddlInbound850PickItem.SelectedValue)
                    'objPageControls.SetValueForComments(objLeads.Comments,  radOppTab)
                End With
                objLeads.vcPartnerCode = objLeads.PartnerCode
                objLeads.CompanyID = objLeads.CreateRecordCompanyInfo
                Dim lngtempDivId As Long = 0
                lngtempDivId = objLeads.DivisionID
                objLeads.DivisionID = objLeads.ManageCompanyDivisionsInfo1
                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If objLeads.DivisionID > 0 Then
                    SaveCusField()
                Else
                    objLeads.DivisionID = lngtempDivId
                    DisplayError("Partner Code already Used for another organization")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub DisplayDynamicFlds()
            Try
                objPageControls.DisplayDynamicFlds(lngDivId, objLeads.CompanyType, Session("DomainID"), objPageControls.Location.Accounts, radOppTab, 36)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveCusField()
            Try
                objPageControls.SaveCusField(lngDivId, objLeads.CompanyType, Session("DomainID"), objPageControls.Location.Organization, radOppTab.MultiPage)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub gvContacts_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvContacts.RowCommand
            Try
                If e.CommandName = "DeleteContact" Then
                    If objContacts Is Nothing Then objContacts = New CContacts

                    objContacts.ContactID = CCommon.ToLong(e.CommandArgument)
                    objContacts.DomainID = Session("DomainID")
                    Dim strError As String = objContacts.DelContact()
                    If strError = "" Then
                        GetContacts()
                    Else
                        litMessage.Text = strError
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub gvContacts_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvContacts.RowDataBound
            Try
                If e.Row.RowType = DataControlRowType.DataRow Then
                    Dim btnDelete As Button
                    Dim lnkDelete As LinkButton
                    Dim IsPrimaryContact As Boolean

                    btnDelete = e.Row.FindControl("btnDelete")
                    lnkDelete = e.Row.FindControl("lnkdelete")
                    IsPrimaryContact = DataBinder.Eval(e.Row.DataItem, "bitPrimaryContact")

                    If DataBinder.Eval(e.Row.DataItem, "numContactId") = 1 Then
                        btnDelete.Visible = False
                        lnkDelete.Visible = True
                        lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                        Exit Sub
                    End If
                    If m_aryRightsForContacts(RIGHTSTYPE.DELETE) = 0 Then
                        btnDelete.Visible = False
                        lnkDelete.Visible = True
                        lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    ElseIf m_aryRightsForContacts(RIGHTSTYPE.DELETE) = 1 Then
                        Try
                            If DataBinder.Eval(e.Row.DataItem, "numCreatedBy") = Session("UserContactID") Then
                                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                            Else
                                btnDelete.Visible = False
                                lnkDelete.Visible = True
                                lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                            End If
                        Catch ex As Exception
                        End Try
                    ElseIf m_aryRightsForContacts(RIGHTSTYPE.DELETE) = 2 Then
                        Dim i As Integer
                        Dim dtTerritory As DataTable
                        dtTerritory = Session("UserTerritory")
                        If DataBinder.Eval(e.Row.DataItem, "numTerid") = 0 Then
                            btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                        Else
                            Dim chkDelete As Boolean = False
                            For i = 0 To dtTerritory.Rows.Count - 1
                                If DataBinder.Eval(e.Row.DataItem, "numTerid") = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                    chkDelete = True
                                End If
                            Next
                            If chkDelete = True Then
                                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                            Else
                                btnDelete.Visible = False
                                lnkDelete.Visible = True
                                lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                            End If
                        End If
                    ElseIf m_aryRightsForContacts(RIGHTSTYPE.DELETE) = 3 Then
                        btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If

                    If IsPrimaryContact = True Then
                        btnDelete.Visible = False
                        lnkDelete.Visible = True
                        lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                PageRedirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dgProjectsOpen_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgProjectsOpen.ItemCommand
            Try
                Dim lngProID As Long
                lngProID = e.Item.Cells(0).Text
                If e.CommandName = "Name" Then
                    Response.Redirect("../projects/frmProjects.aspx?frm=accounts&ProID=" & lngProID & "&SI=" & SI1 & "&SI1=" & radOppTab.SelectedIndex & "&frm1=" & frm1 & "&frm2=" & frm2)
                End If
                If e.CommandName = "Contact" Then
                    objCommon.ProID = lngProID
                    objCommon.charModule = "P"
                    objCommon.GetCompanySpecificValues1()
                    Response.Redirect("../contact/frmContacts.aspx?frm=accounts&CntId=" & objCommon.ContactID & "&SI=" & SI1 & "&SI1=" & radOppTab.SelectedIndex & "&frm1=" & frm1 & "&frm2=" & frm2)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        'Private Sub ddlDropdownlist_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDropdownlist.SelectedIndexChanged
        '    Try
        '        tblAccounting.Rows.Clear()
        '        sb_DisplayAccounting()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to set the Association Information.
        ''' </summary>
        ''' <remarks>
        '''     This function calls the imprints the association information on a screen label.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	01/18/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------

        Public Sub LoadAssociationInformation()
            Try
                Dim objAssociationInfo As New OrgAssociations                   'Create a new object of association
                objAssociationInfo.DomainID = Session("DomainID")               'Set the Domain Id
                objAssociationInfo.DivisionID = lngDivId                        'Set the Division Id
                Dim dtAssociationInfo As DataTable                              'Declare a new DataTable
                dtAssociationInfo = objAssociationInfo.getParentOrgForCurrentOrg 'Get the Association Info
                If dtAssociationInfo.Rows.Count > 0 Then                        'Check if association exists where there is a parent-child relationship
                    lblAssociation.Text = dtAssociationInfo.Rows(0).Item("vcData") & " of: " & "<a href=""javascript: GoOrgDetails(" & dtAssociationInfo.Rows(0).Item("numDivisionId") & "," & dtAssociationInfo.Rows(0).Item("tintCRMType") & ");""><b>" & dtAssociationInfo.Rows(0).Item("vcCompanyName") & "</b></a>, " & dtAssociationInfo.Rows(0).Item("vcDivisionName")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgOpenCases_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgOpenCases.ItemCommand
            Try
                If e.CommandName = "Cases" Then
                    Response.Redirect("../cases/frmCases.aspx?frm=accounts&CaseID=" & e.Item.Cells(0).Text & "&SI=" & SI1 & "&SI1=" & radOppTab.SelectedIndex & "&frm1=" & frm1 & "&frm2=" & frm2)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Function ReturnName(ByVal CloseDate) As String
            Try
                Dim strTargetResolveDate As String = ""
                If Not IsDBNull(CloseDate) Then
                    strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
                End If
                Return strTargetResolveDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActDelete.Click
            Try
                If objAccounts Is Nothing Then objAccounts = New CAccounts
                With objAccounts
                    .DivisionID = lngDivId
                    .DomainID = Session("DomainID")
                End With
                Dim strError As String = objAccounts.DeleteOrg()
                If strError = "" Then
                    Response.Redirect("../account/frmAccountList.aspx", False)
                Else
                    litMessage.Text = strError
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlRelationhip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                ' LoadProfile()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub dlWebAnlys_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlWebAnlys.ItemCommand
            Try
                If e.CommandName = "Pages" Then
                    Dim cmd As String = CType(e.CommandSource, LinkButton).CommandName
                    dlWebAnlys.SelectedIndex = e.Item.ItemIndex
                    Session("dlIndex") = CInt(e.Item.ItemIndex) + 1
                    createMainLink()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub createMainLink()
            Try
                objLeads.DivisionID = lngDivId
                objLeads.DomainID = Session("DomainID")
                Dim ds As DataSet
                ds = objLeads.GetWebAnlysDtl
                If Session("dlIndex") <> 0 Then dlWebAnlys.SelectedIndex = Session("dlIndex") - 1
                dlWebAnlys.DataSource = ds.Tables(1)
                dlWebAnlys.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dlWebAnlys_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlWebAnlys.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.SelectedItem Then
                    objLeads.TrackingID = CType(e.Item.FindControl("lblID"), Label).Text
                    Dim dg As DataGrid
                    dg = e.Item.FindControl("dgWebAnlys")
                    dg.DataSource = objLeads.GetPagesVisited
                    dg.DataBind()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlTerriory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                Dim lngAssignTo As Long
                lngAssignTo = objLeads.AssignedTo
                Dim ddl As DropDownList
                If Not radOppTab.FindControl("64Assigned To") Is Nothing Then
                    ddl = radOppTab.FindControl("64Assigned To")
                    objCommon.sb_FillConEmpFromTerritories(ddl, Session("DomainID"), 1, 0, sender.SelectedValue)
                    If Not ddl.Items.FindByValue(lngAssignTo) Is Nothing Then ddl.Items.FindByValue(lngAssignTo).Selected = True
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub LoadTabDetails()
            Try
                btnAddNewContact.Visible = False
                'radOppTab.Tabs(8).Visible = False
                If radOppTab.SelectedIndex = 1 Then
                    GetContacts()
                    btnAddNewContact.Visible = True
                    btnAddNewContact.Attributes.Add("onclick", "return OpenPopUp('../contact/newcontact.aspx?rtyWR=" & lngDivId & "');")
                ElseIf radOppTab.SelectedIndex = 2 Then
                    radPageView_Accounting.Visible = False
                    radPageView_Opportunities.Visible = True
                ElseIf radOppTab.SelectedIndex = 3 Then
                    sc_GetProjects()
                ElseIf radOppTab.SelectedIndex = 4 Then
                    GetCases()
                ElseIf radOppTab.SelectedIndex = 5 Then
                    sb_DisplayAccounting()
                    radPageView_Opportunities.Visible = False
                    radPageView_Accounting.Visible = True
                ElseIf radOppTab.SelectedIndex = 6 Then
                    objLeads.DivisionID = lngDivId
                    objLeads.DomainID = Session("DomainID")
                    Dim ds As DataSet
                    ds = objLeads.GetWebAnlysDtl
                    If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                        lblFirstDateVisited.Text = CCommon.ToString(ds.Tables(0).Rows(0).Item("StartDate"))
                        lblLastDateVisited.Text = CCommon.ToString(ds.Tables(0).Rows(0).Item("EndDate"))
                        lblReferringPage.Text = CCommon.ToString(ds.Tables(0).Rows(0).Item("vcOrginalRef"))
                        lblLastPageVisited.Text = CCommon.ToString(ds.Tables(0).Rows(0).Item("vcLastPageVisited"))
                        lblIpAddress.Text = CCommon.ToString(ds.Tables(0).Rows(0).Item("vcUserHostAddress"))
                        lblIPLocation.Text = CCommon.ToString(ds.Tables(0).Rows(0).Item("vcLocation"))
                    Else
                        lblFirstDateVisited.Text = "-"
                        lblLastDateVisited.Text = "-"
                        lblReferringPage.Text = "-"
                        lblLastPageVisited.Text = "-"
                        lblIpAddress.Text = "-"
                        lblIPLocation.Text = "-"
                    End If
                    dlWebAnlys.DataSource = ds.Tables(1)
                    dlWebAnlys.DataBind()

                ElseIf radOppTab.SelectedIndex = 7 Then
                    Correspondence1.lngRecordID = lngDivId
                    Correspondence1.Mode = 7
                    Correspondence1.CorrespondenceModule = 4
                    Correspondence1.getCorrespondance()
                ElseIf radOppTab.SelectedIndex = 8 Then
                    AssetList.DivisionID = lngDivId
                    AssetList.FormName = "frmAccounts"
                    AssetList.LoadAssets()
                ElseIf radOppTab.SelectedIndex = 9 Then
                    LoadContracts()
                ElseIf radOppTab.SelectedIndex = 10 Then
                    ifAssociation.Attributes.Add("src", "../admin/frmcomAssociationTo.aspx?numDivisionID=" + lngDivId.ToString)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub LoadContracts()
            Try
                Dim objContract As New CContracts
                objContract.DivisionId = lngDivId
                objContract.DomainID = Session("DomainId")
                objContract.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                dgContracts.DataSource = objContract.GetContactListFromDiv()
                dgContracts.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dgContracts_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgContracts.ItemCommand
            Try
                'If Not e.CommandName = "Sort" Then
                '    lngContractId = e.Item.Cells(0).Text()
                'End If
                If e.CommandName = "Name" Then
                    Response.Redirect("../ContractManagement/frmContract.aspx?frm=Account&contractId=" & e.Item.Cells(0).Text() & "&DivId=" & lngDivId)
                ElseIf e.CommandName = "Delete" Then
                    Dim objContract As New CContracts
                    objContract.ContractID = e.Item.Cells(0).Text()
                    objContract.DomainID = Session("DomainId")
                    objContract.DeleteContract()
                    LoadContracts()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        'Private Sub rdbFinancialOverview_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbFinancialOverview.CheckedChanged
        '    Try
        '        If rdbFinancialOverview.Checked = True Then
        '            PnlFinancialOverview.Visible = True
        '            PnlTransaction.Visible = False
        '        Else
        '            PnlTransaction.Visible = True
        '            PnlFinancialOverview.Visible = False
        '        End If
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub rdbTransaction_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbTransaction.CheckedChanged
        '    Try
        '        If rdbTransaction.Checked = True Then
        '            PnlTransaction.Visible = True
        '            LoadTransactionGrid()
        '            PnlFinancialOverview.Visible = False
        '        Else
        '            PnlFinancialOverview.Visible = True
        '            PnlTransaction.Visible = False
        '        End If
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        Private Sub LoadTransactionGrid()
            Try
                'If objProspects Is Nothing Then objProspects = New CProspects
                'Dim dtTransaction As DataTable
                'objProspects.DomainID = Session("DomainId")
                'objProspects.DivisionID = lngDivId
                'objProspects.PageSize = Session("PagingRows")
                'objProspects.TotalRecords = 0

                'If txtCurrentPageCorr.Text.Trim = "" Then txtCurrentPageCorr.Text = 1
                'objProspects.CurrentPage = txtCurrentPageCorr.Text.Trim()
                'dtTransaction = objProspects.GetTransactionDetails

                'bizPager.PageSize = Session("PagingRows")
                'bizPager.RecordCount = objProspects.TotalRecords
                'bizPager.CurrentPageIndex = Val(txtCurrentPageCorr.Text)

                'dgTransaction.DataSource = dtTransaction
                'dgTransaction.DataBind()

                'If objProspects.TotalRecords = 0 Then
                '    hidePaging.Visible = False
                '    lblRecords.Text = 0
                'Else
                '    hidePaging.Visible = True
                '    lblRecords.Text = String.Format("{0:#,###}", objProspects.TotalRecords)
                '    Dim strTotalPage As String()
                '    Dim decTotalPage As Decimal
                '    decTotalPage = lblRecords.Text / Session("PagingRows")
                '    decTotalPage = Math.Round(decTotalPage, 2)
                '    strTotalPage = CStr(decTotalPage).Split(".")
                '    If (lblRecords.Text Mod Session("PagingRows")) = 0 Then
                '        'lblTotal.Text = strTotalPage(0)
                '        txtTotalPage.Text = strTotalPage(0)
                '    Else
                '        'lblTotal.Text = strTotalPage(0) + 1
                '        txtTotalPage.Text = strTotalPage(0) + 1
                '    End If
                '    txtTotalRecords.Text = lblRecords.Text
                'End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Private Sub txtCurrentPageCorr_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtCurrentPageCorr.TextChanged
        '    Try
        '        LoadTransactionGrid()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub lnkFirst_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFirst.Click
        '    Try
        '        txtCurrentPageCorr.Text = 1
        '        LoadTransactionGrid()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub lnkLast_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLast.Click
        '    Try
        '        txtCurrentPageCorr.Text = txtTotalPage.Text
        '        LoadTransactionGrid()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub lnkNext_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNext.Click
        '    Try
        '        If txtCurrentPageCorr.Text = txtTotalPage.Text Then
        '            Exit Sub
        '        Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 1
        '        End If
        '        LoadTransactionGrid()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub lnkPrevious_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click
        '    Try
        '        If txtCurrentPageCorr.Text = 1 Then
        '            Exit Sub
        '        Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text - 1
        '        End If
        '        LoadTransactionGrid()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub lnk2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2.Click
        '    Try
        '        If txtCurrentPageCorr.Text + 1 = txtTotalPage.Text Or txtCurrentPageCorr.Text + 1 > txtTotalPage.Text Then
        '            Exit Sub
        '        Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 2
        '        End If
        '        LoadTransactionGrid()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub lnk3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3.Click
        '    Try
        '        If txtCurrentPageCorr.Text + 2 = txtTotalPage.Text Or txtCurrentPageCorr.Text + 2 > txtTotalPage.Text Then
        '            Exit Sub
        '        Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 3
        '        End If
        '        LoadTransactionGrid()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub lnk4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4.Click
        '    Try
        '        If txtCurrentPageCorr.Text + 3 = txtTotalPage.Text Or txtCurrentPageCorr.Text + 3 > txtTotalPage.Text Then
        '            Exit Sub
        '        Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 4
        '        End If
        '        LoadTransactionGrid()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub lnk5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5.Click
        '    Try
        '        If txtCurrentPageCorr.Text + 4 = txtTotalPage.Text Or txtCurrentPageCorr.Text + 4 > txtTotalPage.Text Then
        '            Exit Sub
        '        Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 5
        '        End If
        '        LoadTransactionGrid()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        Function ReturnMoney(ByVal Money)
            Try
                If Not IsDBNull(Money) Then Return String.Format("{0:#,###.00}", Money)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Private Sub btnCorresGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCorresGo.Click
        '    Try
        '        getCorrespondance()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub rptCorr_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.RepeaterCommandEventArgs) Handles rptCorr.ItemCommand
        '    Try
        '        If e.CommandName = "Sort" Then
        '            If e.CommandSource.ID = "lnkDate" Then
        '                strColumn = "bintCreatedOn"
        '            ElseIf e.CommandSource.ID = "lnkType" Then
        '                strColumn = "Type"
        '            ElseIf e.CommandSource.ID = "lnkFrom" Then
        '                strColumn = "[From]"
        '            ElseIf e.CommandSource.ID = "lnkName" Then
        '                strColumn = "phone"
        '            ElseIf e.CommandSource.ID = "lnkAssigned" Then
        '                strColumn = "assignedto"
        '            End If
        '        End If
        '        If Session("Column") <> strColumn Then
        '            Session("Column") = strColumn
        '            Session("Asc") = 0
        '        Else
        '            If Session("Asc") = 0 Then
        '                Session("Asc") = 1
        '            Else : Session("Asc") = 0
        '            End If
        '        End If
        '        'getCorrespondance()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub ddlFilterCorr_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFilterCorr.SelectedIndexChanged
        '    Try
        '        getCorrespondance()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub btnCorrDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCorrDelete.Click
        '    Try
        '        If objContacts Is Nothing Then objContacts = New CContacts
        '        Dim chk As CheckBox
        '        Dim lbl As Label
        '        For Each r As RepeaterItem In rptCorr.Items
        '            chk = r.FindControl("chkADelete")
        '            If chk.Checked = True Then
        '                lbl = r.FindControl("lblDelete")
        '                If lbl.Text.Split("~")(1) = 1 Then
        '                    objContacts.EmailHstrID = lbl.Text.Split("~")(0)
        '                    objContacts.tinttype = 1
        '                Else
        '                    objContacts.EmailHstrID = lbl.Text.Split("~")(0)
        '                    objContacts.tinttype = 2
        '                End If
        '                objContacts.DelCorrespondence()
        '            End If
        '        Next
        '        getCorrespondance()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Sub getCorrespondance()
        '    Try
        '        m_aryRightsForActItem = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmAccounts.aspx", Session("userID"), 4, 6)
        '        If m_aryRightsForActItem(RIGHTSTYPE.VIEW) = 0 Then Exit Sub
        '        If m_aryRightsForActItem(RIGHTSTYPE.DELETE) = 0 Then btnCorrDelete.Visible = False
        '        If objContacts Is Nothing Then objContacts = New CContacts
        '        Dim dttable As DataTable
        '        objContacts.FromDate = Calendar1.SelectedDate
        '        objContacts.ToDate = DateAdd(DateInterval.Day, 1, CDate(Calendar2.SelectedDate))
        '        objContacts.ContactID = CInt(txtContId.Text)
        '        objContacts.DivisionID = lngDivId
        '        objContacts.MessageFrom = ""
        '        objContacts.SortOrder1 = ddlFilterCorr.SelectedValue
        '        objContacts.UserCntID = Session("UserContactID")
        '        objContacts.SortOrder = ddlSrchCorr.SelectedValue
        '        objContacts.KeyWord = txtSearchCorr.Text
        '        objContacts.DomainID = Session("DomainID")
        '        objContacts.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
        '        If txtCurrentPageCorr.Text.Trim <> "" Then
        '            objContacts.CurrentPage = txtCurrentPageCorr.Text
        '        Else : objContacts.CurrentPage = 1
        '        End If
        '        objContacts.PageSize = Session("PagingRows")
        '        objContacts.TotalRecords = 0
        '        If strColumn <> "" Then
        '            objContacts.columnName = strColumn
        '        Else : objContacts.columnName = "bintCreatedOn"
        '        End If
        '        If Session("Asc") = 1 Then
        '            objContacts.columnSortOrder = "Desc"
        '        Else : objContacts.columnSortOrder = "Asc"
        '        End If
        '        dttable = objContacts.getCorres()
        '        If objContacts.TotalRecords = 0 Then
        '            tdCorr.Visible = False
        '            lblNoOfRecordsCorr.Text = objContacts.TotalRecords
        '        Else
        '            tdCorr.Visible = True
        '            lblNoOfRecordsCorr.Text = String.Format("{0:#,###}", objContacts.TotalRecords)
        '            Dim strTotalPage As String()
        '            Dim decTotalSalesPage As Decimal
        '            decTotalSalesPage = lblNoOfRecordsCorr.Text / Session("PagingRows")
        '            decTotalSalesPage = Math.Round(decTotalSalesPage, 2)
        '            strTotalPage = CStr(decTotalSalesPage).Split(".")
        '            If (lblNoOfRecordsCorr.Text Mod Session("PagingRows")) = 0 Then
        '                lblTotalCorr.Text = strTotalPage(0)
        '                txtCorrTotalPage.Text = strTotalPage(0)
        '            Else
        '                lblTotalCorr.Text = strTotalPage(0) + 1
        '                txtCorrTotalPage.Text = strTotalPage(0) + 1
        '            End If
        '            txtCorrTotalRecords.Text = lblNoOfRecordsCorr.Text
        '        End If
        '        rptCorr.DataSource = dttable
        '        rptCorr.DataBind()
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Private Sub lnk2Corr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2Corr.Click
        '    Try
        '        If txtCurrentPageCorr.Text + 1 = txtCorrTotalPage.Text Or txtCurrentPageCorr.Text + 1 > txtCorrTotalPage.Text Then
        '            Exit Sub
        '        Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 2
        '        End If
        '        getCorrespondance()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub lnk3Corr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3Corr.Click
        '    Try
        '        If txtCurrentPageCorr.Text + 2 = txtCorrTotalPage.Text Or txtCurrentPageCorr.Text + 2 > txtCorrTotalPage.Text Then
        '            Exit Sub
        '        Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 3
        '        End If
        '        getCorrespondance()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub lnk4Corr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4Corr.Click
        '    Try
        '        If txtCurrentPageCorr.Text + 3 = txtCorrTotalPage.Text Or txtCurrentPageCorr.Text + 3 > txtCorrTotalPage.Text Then
        '            Exit Sub
        '        Else
        '            txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 4
        '        End If
        '        getCorrespondance()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub lnk5Corr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5Corr.Click
        '    Try
        '        If txtCurrentPageCorr.Text + 4 = txtCorrTotalPage.Text Or txtCurrentPageCorr.Text + 4 > txtCorrTotalPage.Text Then
        '            Exit Sub
        '        Else
        '            txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 5
        '        End If
        '        getCorrespondance()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub lnkFirstCorr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFirstCorr.Click
        '    Try
        '        txtCurrentPageCorr.Text = 1
        '        getCorrespondance()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub lnkLastCorr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkLastCorr.Click
        '    Try
        '        txtCurrentPageCorr.Text = txtCorrTotalPage.Text
        '        getCorrespondance()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub lnkNextCorr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNextCorr.Click
        '    Try
        '        If txtCurrentPageCorr.Text = txtCorrTotalPage.Text Then
        '            Exit Sub
        '        Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 1
        '        End If
        '        getCorrespondance()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub lnkPreviousCorr_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreviousCorr.Click
        '    Try
        '        If txtCurrentPageCorr.Text = 1 Then
        '            Exit Sub
        '        Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text - 1
        '        End If
        '        getCorrespondance()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub rptCorr_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptCorr.ItemDataBound
        '    Try
        '        If e.Item.ItemType = ListItemType.Item Then
        '            Dim chk As CheckBox
        '            Dim lbl As Label
        '            chk = e.Item.FindControl("chkADelete")
        '            lbl = e.Item.FindControl("lblDelete")
        '            If m_aryRightsForActItem(RIGHTSTYPE.DELETE) = 1 Then
        '                If lbl.Text.Split("~")(2) <> Session("UserContactID") Then chk.Visible = False
        '            End If
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        'Private Sub radOppClose_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radOppClose.CheckedChanged
        '    Try
        '        LoadTabDetails()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub radOppOpen_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radOppOpen.CheckedChanged
        '    Try
        '        LoadTabDetails()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        Private Sub ddlProjectStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProjectStatus.SelectedIndexChanged
            Try
                LoadTabDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub radCase_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radCase.CheckedChanged
            Try
                LoadTabDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub radCaseHst_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radCaseHst.CheckedChanged
            Try
                LoadTabDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Function ReturnDateTime(ByVal CloseDate) As String
            Try
                Dim strTargetResolveDate As String = ""
                If Not IsDBNull(CloseDate) And IsDate(CloseDate) = True Then
                    strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
                End If
                Return strTargetResolveDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ReturnDate(ByVal CloseDate) As String
            Try
                If CloseDate Is Nothing Then Exit Function
                Dim strTargetResolveDate As String = ""
                Dim temp As String = ""
                If Not IsDBNull(CloseDate) Then
                    strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))

                    ' check Today date components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                    ' if both are same it is today
                    Dim strNow As Date
                    strNow = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    If (CloseDate.Date = strNow.Date And CloseDate.Month = strNow.Month And CloseDate.Year = strNow.Year) Then
                        strTargetResolveDate = "<font color=red><b>Today</b></font>"
                        Return strTargetResolveDate

                        ' check [ TodayDate + 1 ] Date.... components [ Date , Month , Year ] with Parameter CloseDate components [ Date , Month , Year ] 
                        ' if both are same it was Yesterday
                    ElseIf (CloseDate.Date.AddDays(1).Date = strNow.Date And CloseDate.AddDays(1).Month = strNow.Month And CloseDate.AddDays(1).Year = strNow.Year) Then
                        strTargetResolveDate = "<font color=purple><b>Yesterday</b></font>"
                        Return strTargetResolveDate

                        ' check TodayDate .... components [ Date , Month , Year ] with Parameter [ CloseDate + 1 ] Date....  components [ Date , Month , Year ] 
                        ' if both are same it will Tomorrow
                    ElseIf (CloseDate.Date = strNow.AddDays(1).Date And CloseDate.Month = strNow.AddDays(1).Month And CloseDate.Year = strNow.AddDays(1).Year) Then
                        temp = CloseDate.Hour.ToString + ":" + CloseDate.Minute.ToString
                        strTargetResolveDate = "<font color=orange><b>Tomorrow</b></font>"
                        Return strTargetResolveDate
                    Else
                        strTargetResolveDate = strTargetResolveDate
                        Return strTargetResolveDate
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
            ViewState("IntermediatoryPage") = False
            boolIntermediatoryPage = False
            tblMain.Controls.Clear()
            sb_CompanyInfo()
            If radOppTab.SelectedIndex = 5 Then
                sb_DisplayAccounting()
            End If
            ControlSettings()
        End Sub

        Sub ControlSettings()
            If boolIntermediatoryPage = True Then
                btnSave.Visible = False
                btnSaveClose.Visible = False
                btnEdit.Visible = True
            Else
                'Dim objCommon As New CCommon()
                'objCommon.CheckdirtyForm(Page)

                btnSave.Visible = True
                btnSaveClose.Visible = True
                btnEdit.Visible = False
            End If
        End Sub

        Sub PopulateDependentDropdown(ByVal sender As Object, ByVal e As EventArgs)
            objPageControls.PopulateDropdowns(CType(sender, DropDownList), dtTableInfo, objCommon, radOppTab.MultiPage, Session("DomainID"))
        End Sub

        'Private Sub lnkLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLast.Click
        '    Try
        '        txtCurrentPageCorr.Text = txtTotalPage.Text
        '        LoadAssets()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub lnkPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click
        '    Try
        '        If txtCurrentPageCorr.Text = 1 Then
        '            Exit Sub
        '        Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text - 1
        '        End If
        '        LoadAssets
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub lnkFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFirst.Click
        '    Try
        '        txtCurrentPageCorr.Text = 1
        '        LoadAssets
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub lnkNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkNext.Click
        '    Try
        '        If txtCurrentPageCorr.Text = txtTotalPage.Text Then
        '            Exit Sub
        '        Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 1
        '        End If
        '        LoadAssets
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub lnk2_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2.Click
        '    Try
        '        If txtCurrentPageCorr.Text + 1 = txtTotalPage.Text Or txtCurrentPageCorr.Text + 1 > txtTotalPage.Text Then
        '            Exit Sub
        '        Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 2
        '        End If
        '        LoadAssets
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub lnk3_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3.Click
        '    Try
        '        If txtCurrentPageCorr.Text + 2 = txtTotalPage.Text Or txtCurrentPageCorr.Text + 2 > txtTotalPage.Text Then
        '            Exit Sub
        '        Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 3
        '        End If
        '        LoadAssets
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub lnk4_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4.Click
        '    Try
        '        If txtCurrentPageCorr.Text + 3 = txtTotalPage.Text Or txtCurrentPageCorr.Text + 3 > txtTotalPage.Text Then
        '            Exit Sub
        '        Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 4
        '        End If
        '        LoadAssets
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub lnk5_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5.Click
        '    Try
        '        If txtCurrentPageCorr.Text + 4 = txtTotalPage.Text Or txtCurrentPageCorr.Text + 4 > txtTotalPage.Text Then
        '            Exit Sub
        '        Else : txtCurrentPageCorr.Text = txtCurrentPageCorr.Text + 5
        '        End If
        '        LoadAssets
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub txtCurrentPageCorr_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrentPageCorr.TextChanged
        '    Try
        '        LoadAssets
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        Private Sub btnGoogle_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGoogle.Click
            Try
                Dim objGoogle As New GoogleContact
                Dim strMessage As String

                If objProspects Is Nothing Then objProspects = New CProspects

                objProspects.DivisionID = lngDivId
                objProspects.DomainID = Session("DomainID")
                objProspects.UserCntID = Session("UserContactID")
                objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                Dim dtContact As DataTable
                Dim objCommon As New GoogleContact

                Dim dsList As DataSet

                dsList = objProspects.GetContactInfo1
                dtContact = dsList.Tables(0)

                For i As Integer = 0 To dtContact.Rows.Count - 1
                    strMessage = objGoogle.BizToGoogle(Session("UserContactID"), Session("DomainID"), dtContact.Rows(i)("numContactId"))
                Next

                litMessage.Text = strMessage
                litMessage.Visible = True
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub radOppTab_TabClick(ByVal sender As Object, ByVal e As Telerik.Web.UI.RadTabStripEventArgs) Handles radOppTab.TabClick
            Try
                PersistTable.Clear()
                PersistTable.Add(PersistKey.SelectedTab, radOppTab.SelectedIndex)
                PersistTable.Save(boolOnlyURL:=True)

                LoadTabDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        'Added by Sachin Sadhu||Date:27thDec2013
        'Purpose:For Items Tabs :"Bought & Sold " & Vended

        Public Sub BindDatagrid(ByVal imode As Integer, ByVal strExpression As String)
            Try
                Dim dtItems As DataTable
                Dim objItems As New CItems
                Dim strCondition As String = ""
                Dim dsItems As DataSet
                Dim tab1 As RadTab = radStripItems.FindTabByValue("Bought & Sold")
                Dim tab2 As RadTab = radStripItems.FindTabByValue("Vended")
                If txtCurrentPageBuyItems.Text.Trim = "" Then txtCurrentPageBuyItems.Text = 1
                If txtCurrentPageVendedItems.Text.Trim = "" Then txtCurrentPageVendedItems.Text = 1
                With objItems
                    .ItemClassification = 0 'CCommon.ToLong(ddlFilter.SelectedValue) 'ddlFilter.SelectedItem.Value
                    .DomainID = Session("DomainID")
                    .PageSize = Convert.ToInt32(Session("PagingRows"))
                    .TotalRecords = 0
                    .DivisionID = lngDivId
                    .UserCntID = Session("UserContactID")
                    .KeyWord = txtSearchModel.Text.Trim()

                    .columnName = strItemColumn
                    'Create filter conditions as per selection within rad dropdowns

                End With
                If tab1.Selected Then
                    objItems.CurrentPage = Convert.ToInt32(txtCurrentPageBuyItems.Text.Trim())
                Else
                    objItems.CurrentPage = Convert.ToInt32(txtCurrentPageVendedItems.Text.Trim())
                End If

                If calFromDate.Enabled Then

                    strDtFrom = CDate(calFromDate.SelectedDate)
                    strDtTo = DateAdd(DateInterval.Day, 1, CDate(calToDate.SelectedDate))
                    objItems.StartDate = strDtFrom
                    objItems.EndDate = strDtTo
                Else
                    objItems.StartDate = Nothing
                    objItems.EndDate = Nothing
                End If
                objItems.strCondition = strCondition
                objItems.bitArchieve = False
                objItems.byteMode = 0
                dsItems = objItems.GetItemsAccountwise(imode)
                dtItems = dsItems.Tables(0)
                Dim dv As DataView = dtItems.DefaultView

                If imode = 1 Then
                    If Not radPageView_BoughtSold.FindControl("gvItemsBought") Is Nothing Then
                        Dim gvItemsBought As GridView = CType(radPageView_BoughtSold.FindControl("gvItemsBought"), GridView)
                        dv.Sort = strExpression & " " & GetSortDirection(strExpression)
                        gvItemsBought.DataSource = dv
                        gvItemsBought.DataBind()

                        If tab1.Selected Then
                            PgrBuyItems.PageSize = Session("PagingRows")
                            PgrBuyItems.CurrentPageIndex = txtCurrentPageBuyItems.Text
                            PgrBuyItems.RecordCount = objItems.TotalRecords
                        End If
                    End If
                Else
                    If Not radPageView_Vended.FindControl("gvItemsVended") Is Nothing Then
                        Dim gvItemsVended As GridView = CType(radPageView_Vended.FindControl("gvItemsVended"), GridView)
                        dv.Sort = strExpression & " " & GetSortDirectionVended(strExpression)
                        gvItemsVended.DataSource = dv
                        gvItemsVended.DataBind()
                        If tab2.Selected Then
                            PgrVenedItems.PageSize = Session("PagingRows")
                            PgrVenedItems.CurrentPageIndex = txtCurrentPageVendedItems.Text
                            PgrVenedItems.RecordCount = objItems.TotalRecords
                        End If
                    End If
                    
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub radPageView_Items_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles radPageView_Items.Load

            If Not Page.IsPostBack Then
                Dim tab1 As RadTab = radStripItems.FindTabByValue("Bought & Sold")
                Dim tab2 As RadTab = radStripItems.FindTabByValue("Vended")
                If tab1.Visible = False Or tab2.Visible = False Then

                    If tab1.Visible = True Then
                        If ViewState("SortExpression") Is Nothing Then
                            ViewState("SortExpression") = "ItemName"
                        End If

                        tab1.Selected = True
                        BindDatagrid(1, ViewState("SortExpression"))
                        radPageView_BoughtSold.Selected = True
                    End If
                    If tab2.Visible = True Then
                        If ViewState("VSortExpression") Is Nothing Then
                            ViewState("VSortExpression") = "ItemName"
                        End If
                        tab2.Selected = True
                        BindDatagrid(2, ViewState("VSortExpression"))
                        radPageView_Vended.Selected = True
                    End If
                Else

                    If tab1.Visible = True Then
                        If ViewState("SortExpression") Is Nothing Then
                            ViewState("SortExpression") = "ItemName"
                        End If


                        BindDatagrid(1, ViewState("SortExpression"))
                    End If
                    If tab2.Visible = True Then
                        If ViewState("VSortExpression") Is Nothing Then
                            ViewState("VSortExpression") = "ItemName"
                        End If
                        BindDatagrid(2, ViewState("VSortExpression"))
                    End If
                End If
            End If





        End Sub

        'Modified by Neelam on 02/16/2018 - Added functionality to attach files*/
        Protected Sub btnAttach_Click(sender As Object, e As EventArgs)
            Try
                If (fileupload.PostedFile.ContentLength > 0) Then
                    Dim strFName As String()
                    Dim strFilePath, strFileName, strFileType As String
                    strFileName = Path.GetFileName(fileupload.PostedFile.FileName)
                    If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                        Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
                    End If
                    Dim newFileName As String = Path.GetFileNameWithoutExtension(strFileName) & DateTime.UtcNow.ToString("yyyyMMddhhmmssfff") & Path.GetExtension(strFileName)
                    strFilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & newFileName
                    strFName = Split(strFileName, ".")
                    strFileType = "." & strFName(strFName.Length - 1)                     'Getting the Extension of the File
                    fileupload.PostedFile.SaveAs(strFilePath)
                    UploadFile("L", strFileType, newFileName, strFName(0), "", 0, 0, "A")
                    Dim script As String = "function f(){$find(""" + RadWinDocumentFiles.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, True)
                    Response.Redirect(Request.RawUrl, False)
                Else
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "NoFiles", "alert('Please attach a file to upload.');return false;", True)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub btnLinkClose_Click(sender As Object, e As EventArgs)
            UploadFile("U", "", txtLinkURL.Text, txtLinkName.Text, "", 0, 0, "A")
            Response.Redirect(Request.RawUrl, False)
        End Sub

        'Modified by Neelam on 02/16/2018 - Added function to save File/Link data in the database*/
        Sub UploadFile(ByVal URLType As String, ByVal strFileType As String, ByVal strFileName As String, ByVal DocName As String, ByVal DocDesc As String,
                       ByVal DocumentStatus As Long, DocCategory As Long, ByVal strType As String)
            Try
                Dim arrOutPut As String()
                Dim objDocuments As New DocumentList()
                With objDocuments
                    .DomainID = Session("DomainId")
                    .UserCntID = Session("UserContactID")
                    .UrlType = URLType
                    .DocumentStatus = DocumentStatus
                    .DocCategory = DocCategory
                    .FileType = strFileType
                    .DocName = DocName
                    .DocDesc = DocDesc
                    .FileName = strFileName
                    .DocumentSection = strType
                    .RecID = lngDivId
                    .FormFieldGroupId = IIf(URLType = "L", ddlSectionFile.SelectedValue, ddlSectionLink.SelectedValue)
                    .DocumentType = IIf(lngDivId > 0, 2, 1) '1=generic,2=specific
                    arrOutPut = .SaveDocuments()
                End With
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Private Sub radPageView_BoughtSold_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles radPageView_BoughtSold.Load
        '    Dim tab1 As RadTab = radStripItems.FindTabByValue("Bought & Sold")

        '    If tab1.Visible = True Then
        '        If ViewState("SortExpression") Is Nothing Then
        '            ViewState("SortExpression") = "ItemName"
        '        End If

        '        tab1.Selected = True
        '        BindDatagrid(1, ViewState("SortExpression"))
        '        radPageView_BoughtSold.Selected = True
        '    End If
        'End Sub

        'Private Sub radPageView_Vended_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles radPageView_Vended.Load
        '    Dim tab2 As RadTab = radStripItems.FindTabByValue("Vended")
        '    If tab2.Visible = True Then
        '        If ViewState("VSortExpression") Is Nothing Then
        '            ViewState("VSortExpression") = "ItemName"
        '        End If
        '        tab2.Selected = True
        '        BindDatagrid(2, ViewState("VSortExpression"))
        '        radPageView_Vended.Selected = True
        '    End If
        'End Sub

        Protected Sub RadTabStrip1_TabClick(ByVal sender As Object, ByVal e As RadTabStripEventArgs) Handles radStripItems.TabClick
            Dim TabClicked As Telerik.Web.UI.RadTab = e.Tab
            If TabClicked.Value = "Bought & Sold" Then
                If ViewState("SortExpression") Is Nothing Then
                    ViewState("SortExpression") = "ItemName"
                End If
                BindDatagrid(1, ViewState("SortExpression"))

            Else
                If ViewState("VSortExpression") Is Nothing Then
                    ViewState("VSortExpression") = "ItemName"
                End If

                BindDatagrid(2, ViewState("VSortExpression"))

            End If
            'Author:Sachin Sadhu||Date:4thJan2014
            'Purpose:To  Persist Items tab's Sub tabs-Bought & vended
            PersistTable.Clear()
            PersistTable.Add(PersistKey.SelectedTab & "Items", radStripItems.SelectedIndex)
            PersistTable.Save()
            'End of code by sachin
        End Sub


        Private Sub btnGo_CLick() Handles btnGo.Click

            Dim tab1 As RadTab = radStripItems.FindTabByValue("Bought & Sold")
            Dim tab2 As RadTab = radStripItems.FindTabByValue("Vended")
            If tab1.Selected = True Then
                If ViewState("SortExpression") Is Nothing Then
                    ViewState("SortExpression") = "ItemName"
                End If

                BindDatagrid(1, ViewState("SortExpression"))
            End If
            If tab2.Selected = True Then
                If ViewState("VSortExpression") Is Nothing Then
                    ViewState("VSortExpression") = "ItemName"
                End If

                BindDatagrid(2, ViewState("VSortExpression"))
            End If

        End Sub
        Private Sub gvItemsBought_Sorting(ByVal sender As Object, ByVal e As GridViewSortEventArgs) Handles gvItemsBought.Sorting
            ViewState("SortExpression") = e.SortExpression
            BindDatagrid(1, e.SortExpression)
        End Sub
        Private Sub gvItemsVended_Sorting(ByVal sender As Object, ByVal e As GridViewSortEventArgs) Handles gvItemsVended.Sorting
            ViewState("VSortExpression") = e.SortExpression
            BindDatagrid(2, e.SortExpression)
        End Sub
        Private Function GetSortDirection(ByVal column As String) As String
            ' By default, set the sort direction to ascending.
            Dim sortDirection = "ASC"

            ' Retrieve the last column that was sorted.
            Dim sortExpression = TryCast(ViewState("SortExpression"), String)

            If sortExpression IsNot Nothing Then
                ' Check if the same column is being sorted.
                ' Otherwise, the default value can be returned.
                If sortExpression = column Then
                    Dim lastDirection = TryCast(ViewState("SortDirection"), String)
                    If lastDirection IsNot Nothing _
                      AndAlso lastDirection = "ASC" Then
                        sortDirection = "DESC"
                    End If
                End If
            End If

            ' Save new values in ViewState.
            ViewState("SortDirection") = sortDirection
            ViewState("SortExpression") = column
            'PersistData()
            Return sortDirection

        End Function
        Private Function GetSortDirectionVended(ByVal column As String) As String

            ' By default, set the sort direction to ascending.
            Dim sortDirection = "ASC"

            ' Retrieve the last column that was sorted.
            Dim sortExpression = TryCast(ViewState("VSortExpression"), String)

            If sortExpression IsNot Nothing Then
                ' Check if the same column is being sorted.
                ' Otherwise, the default value can be returned.
                If sortExpression = column Then
                    Dim lastDirection = TryCast(ViewState("VSortDirection"), String)
                    If lastDirection IsNot Nothing _
                      AndAlso lastDirection = "ASC" Then

                        sortDirection = "DESC"

                    End If
                End If
            End If

            ' Save new values in ViewState.
            ViewState("VSortDirection") = sortDirection
            ViewState("VSortExpression") = column

            ' PersistData()
            Return sortDirection

        End Function
        Protected Sub rdbFilterDate_clicked(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbFilterDate.CheckedChanged
            If rdbFilterDate.Checked Then
                calFromDate.Enabled = True
                calToDate.Enabled = True
                calFromDate.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                calToDate.SelectedDate = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), DateAdd(DateInterval.Month, 1, Date.UtcNow))
            End If

        End Sub
        Sub PersistData()
            PersistTable.Clear()

            If ViewState("SortExpression") = Nothing Then
                ViewState("SortExpression") = "ItemName"
            End If

            If ViewState("VSortExpression") = Nothing Then
                ViewState("VSortExpression") = "ItemName"
            End If


            PersistTable.Add(PersistKey.SortColumnName & "BoughtItems", ViewState("SortExpression"))
            PersistTable.Add(PersistKey.SortOrder & "BoughtItems", ViewState("SortDirection"))

            PersistTable.Add(PersistKey.SortColumnName & "VendedItems", ViewState("VSortExpression"))
            PersistTable.Add(PersistKey.SortOrder & "VendedItems", ViewState("VSortDirection"))


            'PersistTable.Add(PersistKey.FilterBy, ddlFilterby.SelectedValue)

            'PersistTable.Add(ddlTickler.ID, ddlTickler.SelectedValue)
            'PersistTable.Add(calFrom.ID, calFrom.SelectedDate)
            'PersistTable.Add(calTo.ID, calTo.SelectedDate)
            'PersistTable.Add(chkFilter.ID, chkFilter.Checked)

            PersistTable.Save()
        End Sub

        Protected Sub gvItemsBought_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvItemsBought.RowDataBound
            If e.Row.DataItem IsNot Nothing Then
                Dim lblItemOrderNo As Label = DirectCast(e.Row.FindControl("lblItemOrderNo"), Label)
                Dim llb As Label = DirectCast(e.Row.FindControl("llb"), Label)
                Dim hplItemOrderNo As HyperLink = DirectCast(e.Row.FindControl("hplItemOrderNo"), HyperLink)
                m_aryRightsOrder = GetUserRightsForPage(10, 3)
                If m_aryRightsOrder(RIGHTSTYPE.VIEW) = 0 Then
                    lblItemOrderNo.Visible = True
                    hplItemOrderNo.Visible = False
                Else
                    lblItemOrderNo.Visible = False
                    hplItemOrderNo.Visible = True
                    hplItemOrderNo.Attributes.Add("onclick", "return OpenOpp('" & llb.Text & "','" & llb.Text & "')")
                End If


                'Cast DataItem into your data object that you bound with Grid. I am assuming that you bound you grid with DataTable and CustomerName is a column in that table.


            End If
        End Sub
        Protected Sub gvItemsVended_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvItemsVended.RowDataBound
            If e.Row.DataItem IsNot Nothing Then
                Dim lblVItemOrderNo As Label = DirectCast(e.Row.FindControl("lblVItemOrderNo"), Label)
                Dim llbV As Label = DirectCast(e.Row.FindControl("llbV"), Label)
                Dim hplVItemOrderNo As HyperLink = DirectCast(e.Row.FindControl("hplVItemOrderNo"), HyperLink)
                m_aryRightsForItems = GetUserRightsForPage_Other(10, 5)
                If m_aryRightsForItems(RIGHTSTYPE.VIEW) = 0 Then
                    lblVItemOrderNo.Visible = True
                    hplVItemOrderNo.Visible = False
                Else
                    lblVItemOrderNo.Visible = False
                    hplVItemOrderNo.Visible = True
                    hplVItemOrderNo.Attributes.Add("onclick", "return OpenItem('" & llbV.Text & "','" & llbV.Text & "')")
                End If


                'Cast DataItem into your data object that you bound with Grid. I am assuming that you bound you grid with DataTable and CustomerName is a column in that table.


            End If
        End Sub

        'End of Code

        Protected Sub PgrBuyItems_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrentPageBuyItems.Text = PgrBuyItems.CurrentPageIndex

                Dim tab1 As RadTab = radStripItems.FindTabByValue("Bought & Sold")
                Dim tab2 As RadTab = radStripItems.FindTabByValue("Vended")
                If tab1.Selected = True Then
                    If ViewState("SortExpression") Is Nothing Then
                        ViewState("SortExpression") = "ItemName"
                    End If
                    BindDatagrid(1, ViewState("SortExpression"))
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub PgrVenedItems_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrentPageVendedItems.Text = PgrVenedItems.CurrentPageIndex

                Dim tab1 As RadTab = radStripItems.FindTabByValue("Bought & Sold")
                Dim tab2 As RadTab = radStripItems.FindTabByValue("Vended")
                If tab2.Selected = True Then
                    If ViewState("VSortExpression") Is Nothing Then
                        ViewState("VSortExpression") = "ItemName"
                    End If

                    BindDatagrid(2, ViewState("VSortExpression"))
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub radShipCompany_SelectedIndexChanged(sender As Object, e As RadComboBoxSelectedIndexChangedEventArgs)
            Try
                ddlDefaultShippingMethod.Items.Clear()
                ddlDefaultShippingMethod.Items.Add(New ListItem("-- Select One --", "0"))
                OppBizDocs.LoadServiceTypes(radShipCompany.SelectedValue, ddlDefaultShippingMethod)

                ddlSignatureType.SelectedValue = "0"
                txtDescription.Text = ""
                chkOther.Items.Clear()
                ddlCODType.Items.Clear()
                If CCommon.ToLong(radShipCompany.SelectedValue) = 91 Then 'fedex
                    divSignatureType.Visible = True
                    divCOD1.Visible = False

                    chkOther.Items.Add(New ListItem("COD", "COD"))
                    chkOther.Items.Add(New ListItem("Home Delivery Premium", "Home Delivery Premium"))
                    chkOther.Items.Add(New ListItem("Inside Delivery", "Inside Delivery"))
                    chkOther.Items.Add(New ListItem("Inside Pickup", "Inside Pickup"))
                    chkOther.Items.Add(New ListItem("Saturday Delivery", "Saturday Delivery"))
                    chkOther.AutoPostBack = True

                    ddlCODType.Items.Add(New ListItem("-- Select One --", "-- Select One --"))
                    ddlCODType.Items.Add(New ListItem("Any", "Any"))
                    ddlCODType.Items.Add(New ListItem("Cash", "Cash"))
                    ddlCODType.Items.Add(New ListItem("Guaranted Funds", "Guaranted Funds"))
                ElseIf CCommon.ToLong(radShipCompany.SelectedValue) = 88 Then 'UPS
                    divSignatureType.Visible = True
                    divCOD1.Visible = True

                    chkOther.Items.Add(New ListItem("Saturday Delivery", "Saturday Delivery"))
                    chkOther.Items.Add(New ListItem("Saturday Pickup", "Saturday Pickup"))
                    chkOther.Items.Add(New ListItem("Additional Handling", "Additional Handling"))
                    chkOther.Items.Add(New ListItem("Large Package", "Large Package"))
                    chkOther.AutoPostBack = False

                    ddlCODType.Items.Add(New ListItem("-- Select One --", "-- Select One --"))
                    ddlCODType.Items.Add(New ListItem("Any Check", "Any Check"))
                    ddlCODType.Items.Add(New ListItem("Cashier's Check Or Money Order", "Cashier's Check Or Money Order"))
                Else
                    divSignatureType.Visible = True
                    divCOD1.Visible = False
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
    End Class



End Namespace
