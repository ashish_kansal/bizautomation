﻿Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Common

Public Class frmCreditBalance
    Inherits BACRMPage

    Dim lngDivisionID As Long
    Dim iOppType As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngDivisionID = GetQueryStringVal( "DivisionID")
            iOppType = CCommon.ToInteger(GetQueryStringVal( "type"))

            If Not IsPostBack Then
                
                BindGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub BindGrid()
        Try
            Dim objAccounts As New CAccounts

            objAccounts.DomainID = Session("DomainID")
            objAccounts.DivisionID = lngDivisionID
            objAccounts.OppType = iOppType

            gvCreditBalance.DataSource = objAccounts.GetDivisionCreditBalanceHistory
            gvCreditBalance.DataBind()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub gvCreditBalance_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvCreditBalance.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then

                'Dim hplOrderBizDoc As HyperLink = CType(e.Row.FindControl("hplOrderBizDoc"), HyperLink)
                'If DataBinder.Eval(e.Row.DataItem, "numOppBizDocsId") > 0 Then
                '    hplOrderBizDoc.Text = DataBinder.Eval(e.Row.DataItem, "vcBizDocID")
                '    hplOrderBizDoc.Attributes.Add("onclick", String.Format("return OpenBizInvoice('{0}','{1}')", DataBinder.Eval(e.Row.DataItem, "numOppId"), DataBinder.Eval(e.Row.DataItem, "numOppBizDocsId")))
                'Else
                '    hplOrderBizDoc.Text = DataBinder.Eval(e.Row.DataItem, "vcPOppname")
                '    hplOrderBizDoc.Attributes.Add("onclick", String.Format("return OpenOpp('{0}')", DataBinder.Eval(e.Row.DataItem, "numOppId")))
                'End If

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class