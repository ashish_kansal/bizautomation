﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="OpportunitiesTab.ascx.vb"
    Inherits="BACRM.UserInterface.Accounts.OpportunitiesTab" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<script type="text/javascript">
    function OpenOpp(a, b) {

        var str;
        str = "../opportunity/frmOpportunities.aspx?frm=&OpID=" + a + "&OppType=" + b;
        document.location.href = str;
    }
    function OpenBizInvoice(a, b) {
        window.open('../opportunity/frmBizInvoice.aspx?OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
        return false;
    }

    function OpenBill(a, b, c, d) {
        if (d == 2 || d == 4) {
            if (parseInt(c) > 0) {
                window.open('../Accounting/frmBillPayment.aspx?BizDocsPaymentDetId=' + c, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=750,height=450,scrollbars=yes,resizable=yes');
                return false;
            }
        }
        else {
            if (parseInt(a) > 0 && parseInt(b) > 0) {
                window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
                return false;
            }
        }
    }

    function OrderPaging(pgno) {
        $('#txtCurrrentPageDeals').val(pgno);
        if ($('#txtCurrrentPageDeals').val() != 0 || $('#txtCurrrentPageDeals').val() > 0) {
            $('#btnOppTabGo').trigger('click');
        }
    }


    //$(document).ready(function () {
    //    alert('Order Tab');
    //    fn_doPage = function (pgno, CurrentPageWebControlClientID, PostbackButtonClientID) {
    //        console.log('OrderSub tab');
    //        //$('#txtCurrrentPageDeals').val(pgno);
    //        //if ($('#txtCurrrentPageDeals').val() != 0 || $('#txtCurrrentPageDeals').val() > 0) {
    //        //    $('#btnOppTabGo').trigger('click');
    //        //}
    //        var currentPage;
    //        var postbackButton;

    //        if (CurrentPageWebControlClientID != '') {
    //            currentPage = CurrentPageWebControlClientID;
    //        }
    //        else {
    //            currentPage = 'txtCurrrentPageDeals';
    //        }

    //        if (PostbackButtonClientID != '') {
    //            postbackButton = PostbackButtonClientID;
    //        }
    //        else {
    //            postbackButton = 'btnOppTabGo';
    //        }

    //        $('#' + currentPage + '').val(pgno);
    //        if ($('#' + currentPage + '').val() != 0 || $('#' + currentPage + '').val() > 0) {
    //            $('#' + postbackButton + '').trigger('click');
    //        }
    //    }
    //});

    function TransactionTypeAllChanged(item) {
        var Combo = $find("<%= radTransactionType.ClientID%>");
         var items = Combo.get_items();

         for (var x = 0; x < Combo.get_items().get_count() ; x++) {
             if ($(item).is(':checked'))
                 items._array[x].check();
             else
                 items._array[x].uncheck();
         }

         return true;
     }
</script>
<div class="row">
    <div class="col-md-4">
        <div class="form-group">
                        <label>Show</label>
            <telerik:RadComboBox ID="radTransactionType"
                Skin="Default" runat="server" CheckBoxes="true" Width="100%">
                <Items>
                    <telerik:RadComboBoxItem Text="Sales Order" Value="1" />
                    <telerik:RadComboBoxItem Text="Sales Order Invoices(BizDocs)" Value="2" />
                    <telerik:RadComboBoxItem Text="Sales Opportunity" Value="3" />
                    <telerik:RadComboBoxItem Text="Purchase Order" Value="4" />
                    <telerik:RadComboBoxItem Text="Purchase Order Bill(BizDocs)" Value="5" />
                    <telerik:RadComboBoxItem Text="Purchase Opportunity" Value="6" />
                    <telerik:RadComboBoxItem Text="Sales Return" Value="7" />
                    <telerik:RadComboBoxItem Text="Purchase Return" Value="8" />
                    <telerik:RadComboBoxItem Text="Credit Memo" Value="9" />
                    <telerik:RadComboBoxItem Text="Refund Receipt" Value="10" />
                    <telerik:RadComboBoxItem Text="Bills" Value="11" />
                    <telerik:RadComboBoxItem Text="UnApplied Bill Payments" Value="12" />
                    <telerik:RadComboBoxItem Text="Checks" Value="13" />
                    <telerik:RadComboBoxItem Text="Deposits" Value="14" />
                    <telerik:RadComboBoxItem Text="Unapplied Payments" Value="15" />
                    <telerik:RadComboBoxItem Text="Landed Cost" Value="16" />
                </Items>
                <FooterTemplate>
                    <asp:CheckBox ID="chkTransactionTypeAll" runat="server" Text="Select All" onclick="return TransactionTypeAllChanged(this);" />&nbsp;
                        <asp:Button ID="btnTransactionType" CssClass="button" runat="server" Text="Search" OnClick="btnTransactionType_Click"></asp:Button>
                </FooterTemplate>
            </telerik:RadComboBox>
                    </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
                        <label>Date</label>
            <asp:DropDownList ID="ddlTransactionDate" runat="server" CssClass="form-control" AutoPostBack="True">
                <asp:ListItem Value="1">All Dates</asp:ListItem>
                <asp:ListItem Value="2" Selected="True">Today</asp:ListItem>
                <asp:ListItem Value="3">Yesterday</asp:ListItem>
                <asp:ListItem Value="4">This Week</asp:ListItem>
                <asp:ListItem Value="5">This Month</asp:ListItem>
                <asp:ListItem Value="6">This Quarter</asp:ListItem>
                <asp:ListItem Value="7">This Year</asp:ListItem>
                <asp:ListItem Value="8">Last Week</asp:ListItem>
                <asp:ListItem Value="9">Last Month</asp:ListItem>
                <asp:ListItem Value="10">Last Quarter</asp:ListItem>
                <asp:ListItem Value="11">Last Year</asp:ListItem>
            </asp:DropDownList>
                    </div> 
        
    </div>
    <div class="col-md-4">
         <div class="form-group">
                        <label>&nbsp;</label>
        <webdiyer:AspNetPager ID="bizPager" runat="server"
                PagingButtonSpacing="0"
                CssClass="bizgridpager"
                AlwaysShow="true"
                CurrentPageButtonClass="active"
                PagingButtonUlLayoutClass="pagination"
                PagingButtonLayoutType="UnorderedList"
                FirstPageText="<<"
                LastPageText=">>"
                NextPageText=">"
                PrevPageText="<"
                Width="100%"
                UrlPaging="false"
                NumericButtonCount="8"
            ShowPageIndexBox="Never"
                ShowCustomInfoSection="Left"
                OnPageChanged="bizPager_PageChanged"
                CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
                CustomInfoClass="bizpagercustominfo">
            </webdiyer:AspNetPager>
            <asp:TextBox ID="txtCurrrentPageDeals" runat="server" Style="display: none"></asp:TextBox>
            <asp:Button ID="btnOppTabGo" runat="server" CssClass="button" Text="Go" Style="display: none" />
             </div>
    </div>
</div>
<div class="col-md-12">
    <asp:GridView ID="gvTransaction" AutoGenerateColumns="False" runat="server" Width="100%"
                CssClass="dg table table-responsive table-bordered" AllowSorting="true">
                <AlternatingRowStyle CssClass="ais" />
                <RowStyle CssClass="is" />
                <HeaderStyle CssClass="hs" VerticalAlign="Middle" />
                <FooterStyle CssClass="hs" />
                <Columns>
                    <asp:TemplateField HeaderText="Type">
                        <ItemTemplate>
                            <asp:Label ID="lblTransactionType" runat="server" Text='<%#Eval("vcTransactionType")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date" SortExpression="dtCreatedDate">
                        <ItemTemplate>
                            <asp:Label ID="lblCreatedDate" runat="server" Text='<%#Eval("dtCreatedDate")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Transaction">
                        <ItemTemplate>
                            <a href="javascript:OpenTransaction('<%#Eval("intTransactionType")%>','<%# Eval("numRecordID")%>','<%# Eval("numRecordID2")%>','<%# Eval("numStatus")%>')">
                                <%# Eval("vcRecordName")%></a>&nbsp;<asp:Label runat="server" ID="lblBizDocID"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="monTotalAmount" HeaderText="Amount" HtmlEncode="false" DataFormatString="{0:#,###.00}" ItemStyle-CssClass="money" SortExpression="monTotalAmount"></asp:BoundField>
                    <asp:BoundField DataField="monPaidAmount" HeaderText="Paid Amount" HtmlEncode="false" DataFormatString="{0:#,###.00}" ItemStyle-CssClass="money" SortExpression="monPaidAmount"></asp:BoundField>
                    <asp:BoundField DataField="vcStatus" HeaderText="Status" SortExpression="vcStatus"></asp:BoundField>
                    <asp:BoundField DataField="vcMemo" HeaderText="Memo" SortExpression="vcMemo"></asp:BoundField>
                    <%-- <asp:TemplateField HeaderText="Amount" ItemStyle-CssClass="money">
                        <ItemTemplate>
                            <asp:Label ID="lblTotalAmount" runat="server" Text='<%#Eval("monTotalAmount")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Paid Amount" ItemStyle-CssClass="money">
                        <ItemTemplate>
                            <asp:Label ID="lblPaidAmount" runat="server" Text='<%#Eval("monPaidAmount")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status">
                        <ItemTemplate>
                            <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("vcStatus")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Memo">
                        <ItemTemplate>
                            <asp:Label ID="lblMemo" runat="server" Text='<%#Eval("vcMemo")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <ItemTemplate>
                            <asp:Label ID="lblDescription" runat="server" Text='<%#Eval("vcDescription")%>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>--%>
                    <%-- <asp:BoundField DataField="vcTransactionType" ></asp:BoundField>
                    <asp:TemplateField HeaderText="Transaction">
                        <ItemTemplate>
                            <a href="javascript:OpenTransaction('<%#Eval("intTransactionType")%>','<%# Eval("numRecordID")%>','<%# Eval("numRecordID2")%>','<%# Eval("numStatus")%>')">
                                <%# Eval("vcRecordName")%>
                            </a>&nbsp;<asp:Label runat="server" ID="lblBizDocID"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="monTotalAmount" HeaderText="Amount" HtmlEncode="false" DataFormatString="{0:#,###.00}" ItemStyle-CssClass="money"></asp:BoundField>
                    <asp:BoundField DataField="monPaidAmount" HeaderText="Paid Amount" HtmlEncode="false" DataFormatString="{0:#,###.00}" ItemStyle-CssClass="money"></asp:BoundField>
                    <asp:BoundField DataField="vcStatus" HeaderText="Status"></asp:BoundField>
                    <asp:BoundField DataField="vcMemo" HeaderText="Memo"></asp:BoundField>--%>
                    <%--<asp:BoundField DataField="vcDescription" HeaderText="Description"></asp:BoundField>--%>
                </Columns>
                <EmptyDataTemplate>No Records Found.</EmptyDataTemplate>
            </asp:GridView>
</div>

<%--<table cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td valign="top">
            <telerik:RadTabStrip ID="radOppTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
                Skin="Default" ClickSelectedTab="True" AutoPostBack="True" SelectedIndex="0"
                MultiPageID="radMultiPage_OppTab">
                <Tabs>
                    <telerik:RadTab Text="&nbsp;&nbsp;Sales Orders&nbsp;&nbsp;" Value="SalesOrders" PageViewID="radPageView_Blank">
                    </telerik:RadTab>
                    <telerik:RadTab Text="&nbsp;&nbsp;Invoices(BizDocs)&nbsp;&nbsp;" Value="InvoicesBizDocs"
                        PageViewID="radPageView_BizDocs">
                    </telerik:RadTab>
                    <telerik:RadTab Text="&nbsp;&nbsp;Purchase Orders&nbsp;&nbsp;" Value="PurchaseOrders"
                        PageViewID="radPageView_Blank">
                    </telerik:RadTab>
                    <telerik:RadTab Text="&nbsp;&nbsp;Bills&nbsp;&nbsp;" Value="POBizDocs" PageViewID="radPageView_BizDocs">
                    </telerik:RadTab>
                    <telerik:RadTab Text="&nbsp;&nbsp;Opportunities&nbsp;&nbsp;" Value="Opportunities"
                        PageViewID="radPageView_Opportunities">
                    </telerik:RadTab>
                </Tabs>
            </telerik:RadTabStrip>
            <telerik:RadMultiPage ID="radMultiPage_OppTab" runat="server" SelectedIndex="0" CssClass="pageView">
                <telerik:RadPageView ID="radPageView_Blank" runat="server">
                    <asp:DataGrid ID="dgSalesOrder" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False">
                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                        <ItemStyle CssClass="is"></ItemStyle>
                        <HeaderStyle CssClass="hs"></HeaderStyle>
                        <Columns>
                            <asp:BoundColumn DataField="numOppId" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Order ID">
                                <ItemTemplate>
                                    <a href="javascript:OpenOpp('<%# Eval("numOppId") %>','<%# Eval("tintopptype") %>')">
                                        <%# Eval("vcPOppName") %>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="bintCreatedDate" HeaderText="Date Created"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="Amount" DataFormatString="{0:#,###.00}" DataField="monDealAmount"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="Open/Completed" DataField="vcDealStatus"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="Status" DataField="vcStatus"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </telerik:RadPageView>
                <telerik:RadPageView ID="radPageView_BizDocs" runat="server">
                    <asp:DataGrid ID="dgBizDocs" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False">
                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                        <ItemStyle CssClass="is"></ItemStyle>
                        <HeaderStyle CssClass="hs"></HeaderStyle>
                        <Columns>
                            <asp:BoundColumn DataField="numOppId" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="BizDoc ID">
                                <ItemTemplate>
                                    <a href="javascript:void(0)" onclick="OpenBizInvoice('<%# Eval("numOppId") %>','<%# Eval("numoppbizdocsid") %>');">
                                        <%#Eval("vcbizdocid")%>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="dtCreatedDate" HeaderText="Date Created"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="Grand Total" DataFormatString="{0:#,###.00}" DataField="monDealAmount"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="Balance Due" DataFormatString="{0:#,###.00}" DataField="baldue"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="Due Date" DataField="dtDueDate"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="BizDoc Status" DataField="vcStatus"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                    <table id="tblBills" runat="server" width="100%">
                        <tr>
                            <td class="rowHeader" align="left">
                                <br />
                                Bills
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DataGrid ID="dgVendorPayment" runat="server" CssClass="dg" Width="100%" AutoGenerateColumns="False">
                                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                    <ItemStyle CssClass="is"></ItemStyle>
                                    <HeaderStyle CssClass="hs"></HeaderStyle>
                                    <Columns>
                                        <asp:BoundColumn DataField="DueDate" HeaderText="Due Date"></asp:BoundColumn>
                                        <asp:TemplateColumn HeaderText="<font>Bill</font>">
                                            <ItemTemplate>
                                                <a href="javascript:void(0)" onclick="OpenBill('<%# Eval("numOppId") %>','<%# Eval("numBizdocsId") %>','<%# Eval("numBizDocsPaymentDetId") %>','<%# Eval("tintPaymentType") %>');">
                                                    <asp:Label ID="lblBizDocName" runat="server" Text='<%#Eval("Name")%>'></asp:Label>
                                                </a>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn Visible="false" HeaderText="Amount">
                                            <ItemTemplate>
                                                <asp:Label ID="lblOrgAmount" runat="server" Text='<%# ReturnMoney(DataBinder.Eval(Container,"DataItem.Amount")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Amount">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmount" runat="server" Text='<%# ReturnMoney(DataBinder.Eval(Container,"DataItem.ConAmt")) %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Payment Method">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaymentMethod" runat="server" Text='<%# Eval("paymentmethod") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:BoundColumn DataField="CreatedBy" HeaderText="Entered By, on"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Memo" HeaderText="Memo"></asp:BoundColumn>
                                        <asp:BoundColumn DataField="Reference" HeaderText="Reference #"></asp:BoundColumn>
                                    </Columns>
                                </asp:DataGrid>
                            </td>
                        </tr>
                    </table>
                </telerik:RadPageView>
                <telerik:RadPageView ID="radPageView_Opportunities" runat="server">
                    <asp:RadioButtonList ID="radDeal" runat="server" CssClass="normal1" AutoPostBack="true"
                        RepeatDirection="Horizontal">
                        <asp:ListItem Selected="True" Value="0">Deal Open</asp:ListItem>
                        <asp:ListItem Value="2">Deal Lost</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:DataGrid ID="dgOpportunities" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False">
                        <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                        <ItemStyle CssClass="is"></ItemStyle>
                        <HeaderStyle CssClass="hs"></HeaderStyle>
                        <Columns>
                            <asp:BoundColumn DataField="numOppId" Visible="false"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="ID">
                                <ItemTemplate>
                                    <a href="javascript:OpenOpp('<%# Eval("numOppId") %>','<%# Eval("tintopptype") %>')">
                                        <%# Eval("vcPOppName") %>
                                    </a>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="OppType" HeaderText="Type"></asp:BoundColumn>
                            <asp:BoundColumn DataField="bintCreatedDate" HeaderText="Date Created"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="Total Progress" DataField="intTotalProgress" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="Amount" DataFormatString="{0:#,###.00}" DataField="monDealAmount"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="Due Date" DataField="intPEstimatedCloseDate"></asp:BoundColumn>
                            <asp:BoundColumn HeaderText="Deal Status" DataField="vcDealStatus"></asp:BoundColumn>
                        </Columns>
                    </asp:DataGrid>
                </telerik:RadPageView>
            </telerik:RadMultiPage>
        </td>
    </tr>
</table>
<asp:TextBox ID="txtTotalPageDeals" runat="server" Style="display: none">
</asp:TextBox>--%>
