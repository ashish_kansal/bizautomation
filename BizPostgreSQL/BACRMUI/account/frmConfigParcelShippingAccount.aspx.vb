﻿Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Accounts

    Public Class frmConfigParcelShippingAccount
        Inherits BACRMPage

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lblError.Text = ""
                divError.Style.Add("display", "none")

                If Not Page.IsPostBack Then
                    objCommon.sb_FillComboFromDBwithSel(ddlCountry, 40, Session("DomainID"))
                    LoadShippingAccountDetail()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub LoadShippingAccountDetail()
            Try
                txtAccountNumber.Text = ""
                txtStreet.Text = ""
                txtCity.Text = ""
                ddlCountry.SelectedValue = "0"
                ddlState.Items.Clear()
                ddlState.Items.Add(New ListItem("-- Select One --", "0"))
                txtZipCode.Text = ""

                Dim objDivisionMasterShippingAccount As New DivisionMasterShippingAccount
                objDivisionMasterShippingAccount.DivisionID = CCommon.ToLong(GetQueryStringVal("DivID"))
                objDivisionMasterShippingAccount.ShipViaID = CCommon.ToLong(hdnShipViaID.Value)
                Dim dt As DataTable = objDivisionMasterShippingAccount.GetByDividionAndShipViaID()

                If Not dt Is Nothing AndAlso dt.Rows.Count > 0 Then
                    txtAccountNumber.Text = CCommon.ToString(dt.Rows(0)("vcAccountNumber"))
                    txtStreet.Text = CCommon.ToString(dt.Rows(0)("vcStreet"))
                    txtCity.Text = CCommon.ToString(dt.Rows(0)("vcCity"))

                    If Not ddlCountry.Items.FindByValue(dt.Rows(0)("numCountry")) Is Nothing Then
                        ddlCountry.Items.FindByValue(dt.Rows(0)("numCountry")).Selected = True
                        FillState(ddlState, CCommon.ToLong(dt.Rows(0)("numCountry")), Session("DomainID"))

                        If Not ddlState.Items.FindByValue(dt.Rows(0)("numState")) Is Nothing Then
                            ddlState.Items.FindByValue(dt.Rows(0)("numState")).Selected = True
                        End If
                    End If

                    txtZipCode.Text = CCommon.ToString(dt.Rows(0)("vcZipCode"))
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub DisplayError(ByVal errorMessage As String)
            Try
                lblError.Text = errorMessage
                divError.Style.Add("display", "")
            Catch ex As Exception
                'DO NOT THROW ERROR
            End Try
        End Sub

        Protected Sub ddlCountry_SelectedIndexChanged(sender As Object, e As EventArgs)
            Try
                If CCommon.ToLong(ddlCountry.SelectedValue) > 0 Then
                    Dim tempState As String = ddlState.SelectedValue
                    FillState(ddlState, CCommon.ToLong(ddlCountry.SelectedValue), Session("DomainID"))

                    If Not ddlState.Items.FindByValue(tempState) Is Nothing Then
                        ddlState.Items.FindByValue(tempState).Selected = True
                    End If
                Else
                    ddlState.Items.Clear()
                    ddlState.Items.Add(New ListItem("-- Select One --", "0"))
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub btnSave_Click(sender As Object, e As EventArgs)
            Try
                If Not String.IsNullOrEmpty(txtAccountNumber.Text.Trim) AndAlso _
                    Not String.IsNullOrEmpty(txtStreet.Text.Trim) AndAlso _
                    Not String.IsNullOrEmpty(txtCity.Text.Trim) AndAlso _
                    Not CCommon.ToLong(ddlCountry.SelectedValue) = 0 AndAlso _
                    Not CCommon.ToLong(ddlState.SelectedValue) = 0 AndAlso _
                    Not String.IsNullOrEmpty(txtZipCode.Text.Trim) Then

                    Dim objDivisionMasterShippingAccount As New DivisionMasterShippingAccount
                    objDivisionMasterShippingAccount.DivisionID = CCommon.ToLong(GetQueryStringVal("DivID"))
                    objDivisionMasterShippingAccount.ShipViaID = CCommon.ToLong(hdnShipViaID.Value)
                    objDivisionMasterShippingAccount.AccountNumber = CCommon.ToString(txtAccountNumber.Text)
                    objDivisionMasterShippingAccount.Street = CCommon.ToString(txtStreet.Text)
                    objDivisionMasterShippingAccount.City = CCommon.ToString(txtCity.Text)
                    objDivisionMasterShippingAccount.Country = CCommon.ToLong(ddlCountry.SelectedValue)
                    objDivisionMasterShippingAccount.State = CCommon.ToLong(ddlState.SelectedValue)
                    objDivisionMasterShippingAccount.ZipCode = CCommon.ToString(txtZipCode.Text)
                    objDivisionMasterShippingAccount.Save()

                    If GetQueryStringVal("IsFromNewOrder") = "1" Then
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "RecordSaved", "CloseAndUpdateNewOrder('" & CCommon.ToString(txtAccountNumber.Text) & "');", True)
                    Else
                        ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "RecordSaved", "alert('Record saved successfully.')", True)
                    End If
                Else
                        DisplayError("All fields are required")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub btnGoHid_Click(sender As Object, e As EventArgs)
            Try
                LoadShippingAccountDetail()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
    End Class


End Namespace

