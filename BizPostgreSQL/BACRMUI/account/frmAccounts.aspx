<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmAccounts.aspx.vb"
    Inherits="BACRM.UserInterface.Accounts.frmAccounts" MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Src="../include/AssetList.ascx" TagName="AssetList" TagPrefix="uc1" %>
<%@ Register Src="../common/frmCorrespondence.ascx" TagName="frmCorrespondence" TagPrefix="uc1" %>
<%@ Register Src="OpportunitiesTab.ascx" TagName="OpportunitiesTab" TagPrefix="uc1" %>
<%@ Register Src="../ContractManagement/ContractsLog.ascx" TagName="ContractsLog" TagPrefix="uc1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script type='text/javascript' src="../JavaScript/biz.VendorLeadTimes.js"></script>
    <style>
        #divLinkUpload:before {
            content: "";
            background-color: #dcdcdc;
            position: absolute;
            width: 1px;
            height: 100%;
            top: 10px;
            left: 0%;
            display: block;
        }

        #tblMain tr td {
            border-bottom: 1px solid #e1e1e1 !important;
        }

        #tblMain tbody tr:first-child td {
            border-top: 1px solid #fff !important;
            border-left: 1px solid #fff !important;
            border-right: 1px solid #fff !important;
        }

        .tableGroupHeader {
            background-color: #f5f5f5 !important;
            font-weight: bold;
        }

        .hs {
            border: 1px solid #e4e4e4;
            border-bottom-width: 2px;
            background-color: #ebebeb;
            padding: 8px;
            line-height: 1.42857143;
            vertical-align: top;
            text-align: center;
            font-weight: bold;
        }

        .RadComboBox_Vista .rcbInputCellLeft {
            background-image: none !important;
        }

        .RadComboBoxDropDown_Vista {
            width: 17%;
        }
    </style>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <script type="text/javascript">
        function OpenNewAsset(a, b) {
            window.location.href = "../Items/frmKitDetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=frmAccounts&AssetCode=" + a + "&DivisionID=" + b;
            return false;
        }

        function OpenContactSetting() {
            window.open('../Contact/frmConfContactList.aspx?FormId=56', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        function OpemParticularEmail(b) {

            var a = $("#imgEmailPopupId").attr("email")
            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenContact(a, b) {
            var str;
            str = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactlist&ft6ty=oiuy&CntId=" + a;
            document.location.href = str;
        }
        function OpenContact(a, b) {
            var str;
            str = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=accounts&SI1=1&CntId=" + a;

            document.location.href = str;
        }
        function OpenOpp(a, b) {

            var str;

            str = "../opportunity/frmOpportunities.aspx?frm=deallist&OpID=" + a;

            document.location.href = str;
        }
        function OpenNewAction(a) {
            var str;
            str = "../admin/ActionItemDetailsOld.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Accounts&CntID=" + a;

            document.location.href = str;
        }

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }

        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }

        function Save() {
            return validateCustomFields();
        }

        function ShowWindow(Page, q, att) {

            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                return false;

            }
        }

        function openFollow(a) {
            //            document.getElementById('cntDoc').src = "../Leads/frmFollowUpHstr.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&rtyWR=" + a;
            //            document.getElementById('divFollow').style.visibility = "visible";
            //            return false;

            window.open("../Leads/frmFollowUpHstr.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&rtyWR=" + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=no,resizable=no')
            return false;

        }

        function fn_GoToURL(varURL) {
            var url = document.getElementById(varURL).value
            if ((url != '') && (url.substr(0, 7) == 'http://') && (url.length > 7)) {
                var LoWindow = window.open(url, "", "");
                LoWindow.focus();
            }
            return false;
        }

        function fn_EditLabels(URL) {
            window.open(URL, 'WebLinkLabel', 'width=500,height=200,status=no,scrollbar=yes,top=300,left=200');
            return false;
        }

        function fn_ItemDetails(a, b) {

            document.location.href = '../Items/frmItemList.aspx?CompanyName=' + a + '&SearchText=' + b, '', 'toolbar=no,titlebar=no,top=100,left=100,width=1000,height=300,scrollbars=yes,resizable=yes'
            return false;
        }


        function OpenTransfer(url) {
            window.open(url, '', "width=340,height=150,status=no,top=100,left=150");
            return false;
        }

        function OpenLst(url) {
            window.open(url, '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,left=200,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenAdd(a) {
            window.open("../prospects/frmProspectsAdd.aspx?pwer=dfsdfdsfdsfdsf&rtyWR=" + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=300,scrollbars=no,resizable=no')
            return false;
        }

        /*Commented by Neelam - Obsolete function*/
        function OpenDocuments(a) {
            window.open("../Documents/frmSpecDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=A&yunWE=" + a, '', 'toolbar=no,titlebar=no,top=10,width=1000,height=450,left=10,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenTo(a) {
            window.open("../admin/frmcomAssociationTo.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&boolAssociateDiv=true&numDivisionId=" + a, '', 'toolbar=no,left=200,titlebar=no,top=300,width=800,height=500,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenFrom(a) {
            window.open("../admin/frmCompanyAssociationFrom.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&rtyWR=" + a, '', 'toolbar=no,titlebar=no,top=300,left=200,width=700,height=200,scrollbars=yes,resizable=yes')
            return false;
        }

        function CheckNumber(cint) {
            if (cint == 1) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58 || window.event.keyCode == 44 || window.event.keyCode == 46)) {
                    window.event.keyCode = 0;
                }
            }
            if (cint == 2) {
                if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                    window.event.keyCode = 0;
                }
            }

        }

        function GoOrgDetails(a, b) {
            var str;
            if (b == 0) {

                str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=LeadsList&DivID=" + a;

            }
            else if (b == 1) {

                str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=LeadsList&DivID=" + a;

            }
            else if (b == 2) {

                str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=LeadsList&klds+7kldf=fjk-las&DivId=" + a;

            }
            document.location.href = str;
        }

        function OpemEmail(a, b) {
            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
            return false;
        }

        function ShowLayout(a, b, c, d) {
            window.open("../pagelayout/frmCustomisePageLayout.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Ctype=" + a + "&type=" + c + "&PType=3&FormId=" + d, '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenBizInvoice(a, b) {
            window.open('../opportunity/frmBizInvoice.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenGL(lngDivisionID) {
            window.open("../Accounting/frmGeneralLedger.aspx?DivisionID=" + lngDivisionID + "&Mode=2", '_blank')
            window.focus();
            return false;
        }

        function OpenManageCC(lngDivisionID, lngContactId) {
            //window.open("../account/frmCreditCardInfo.aspx?DivisionID=" + lngDivisionID, 'width=700,height=600,status=no,top=50,left=150')
            var pop = window.open('../account/frmCreditCardInfo.aspx?DivisionID=' + lngDivisionID + '&ContactID=' + lngContactId, 'Manage Credit Card', "width=1200,height=700,status=no,top=50,left=50");
            pop.focus()
            return false;
        }

        function OpenCreditBalance(lngDivisionID, type) {
            window.open("../account/frmCreditBalance.aspx?DivisionID=" + lngDivisionID + '&type=' + type, '', 'toolbar=no,titlebar=no,top=100,left=100,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpemARDetails(a) {
            window.open('../Accounting/frmAccountsReceivableInvoice.aspx?DivId=' + a, '', 'toolbar=no,titlebar=no,top=100,left=100,width=1000,height=300,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenShareRecord(a) {
            window.open("../common/frmShareRecord.aspx?RecordID=" + a + "&ModuleID=1", '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
            return false;
        }

        //        function OpenTransactionDetails(lngDivisionID) {
        //            window.open("../opportunity/frmTransactionDetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&DivisionID=" + lngDivisionID + '', 'toolbar=no,titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes')
        //            return false;
        //        }

        function OpenTransactionDetails(lngDivisionID) {
            window.open("../opportunity/frmTransactionDetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Name=0&DivisionID=" + lngDivisionID + "", '', 'toolbar=no,titlebar=no,left=200, top=300,width=1024,height=350,scrollbars=no,resizable=yes')
            return false;
        }
        function OpenItem(a, b) {
            document.location = '../Items/frmKitDetails.aspx?ItemCode=' + a + '&frm=' + b;
        }
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);


            fn_doPage = function (pgno, CurrentPageWebControlClientID, PostbackButtonClientID) {
                // console.log('Accounts Page');
                var currentPage;
                var postbackButton;

                if (CurrentPageWebControlClientID != '') {
                    currentPage = CurrentPageWebControlClientID;
                }
                else {
                    currentPage = 'txtCurrrentPage';
                    currentPage = 'txtCurrentPageBuyItems';

                }

                if (PostbackButtonClientID != '') {
                    postbackButton = PostbackButtonClientID;
                }
                else {
                    postbackButton = 'btnGo1';
                    postbackButton = 'btnGo';
                }

                //alert(currentPage);
                //alert(postbackButton);
                //alert($('#' + currentPage + '').val());
                $('#' + currentPage + '').val(pgno);
                if ($('#' + currentPage + '').val() != 0 || $('#' + currentPage + '').val() > 0) {
                    $('#' + postbackButton + '').trigger('click');
                }
                //alert($('#' + currentPage + '').val());
            }


        });

        function pageLoaded() {
            var tab = $find("<%= radOppTab.ClientID%>").get_selectedTab()
            if (tab.get_value() == "VendorLeadTimes") {
                LoadVendorLeadTimes();
            }

            $("td.customerpartno").each(function () {
                $(this).attr("id", $(this).find(">:first-child").attr("id"));
                $(this).find(">:first-child").removeAttr("id");
                $(this).text($(this).find(">:first-child").text().trim())
                $(this).find(">:first-child").remove();
            });


        }
        function openPurchaseincentivesPopup(DivID) {
            window.open('../account/frmPurchaseIncentives.aspx?DivId=' + DivID, '', 'toolbar=no,titlebar=no,top=200,left=200,width=800,height=500,scrollbars=yes,resizable=yes')
            return false
        }
        function OpenHelpforOrganizationsDetail() {
            window.open('../Help/Organizations_Detail.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        function OpenHelpforOrganizationsContactssubtabToolTip() {
            window.open('../Help/Organizations_Contacts_sub-tab_Tool-Tip_.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
        function OpenHelpforOrganizationsAccountingSubtab() {
            window.open('../Help/Organizations_Accounting_Sub-tab_.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        /*Function added by Neelam on 10/07/2017 - Added functionality to open Child Record window in modal pop up*/
        function OpenDocumentFiles(divID) {
            $find("<%= RadWinDocumentFiles.ClientID%>").show();
        }

        /*Function added by Neelam on 02/14/2018 - Added functionality to close document modal pop up*/
        function CloseDocumentFiles(mode) {

            if (mode == 'L') {
                if ($("[id$=txtLinkURL]").val() == "" || $("[id$=txtLinkURL]").val() == null || $("[id$=txtLinkName]").val() == "" || $("[id$=txtLinkName]").val() == null) {
                    alert("Please enter Link Name & URL.");
                    return false;
                }
                else {
                    var radwindow = $find('<%=RadWinDocumentFiles.ClientID %>');
                    radwindow.close();
                    __doPostBack("<%= btnLinkClose.UniqueID%>", "");
                }
            }

            if (mode == 'C') {
                var radwindow = $find('<%=RadWinDocumentFiles.ClientID %>');
                radwindow.close();
                return false;
            }

        }

        function DeleteDocumentOrLink(fileId) {
            if (confirm('Are you sure, you want to delete the selected item?')) {
                $('#hdnFileId').val(fileId);
                __doPostBack("<%= btnDeleteDocOrLink.UniqueID%>", "");
            }
            else {
                return false;
            }
        }

        function OpenCustomerStatement() {
            window.open('../Accounting/frmCustomerStatement.aspx?DivID=' + $("[id$=lblOrganizationID]").text().replace("(ID:", "").replace(")", "") + '', '', 'toolbar=no,titlebar=no,top=10,left=100,width=1000,height=300,scrollbars=yes,resizable=yes')
            return false;
        }

        $(document).ready(function ($) {
            requestStart = function (target, arguments) {
                if (arguments.get_eventTarget().indexOf("btnAttach") > -1) {
                    arguments.set_enableAjax(false);
                }
                if (arguments.get_eventTarget().indexOf("btnLinkClose") > -1) {
                    arguments.set_enableAjax(false);
                }
            }
        });

        /*function OpenPriceLevelNames() {
            window.open('../Items/frmPricingNamesTable.aspx', '', 'toolbar=no,titlebar=no,left=200,top=250,width=750,height=550,scrollbars=yes,resizable=yes');
            return false;
        }*/

        function ConfigureParcelShippingAccount() {
            var myWidth = 750;
            var myHeight = 700;
            var left = (screen.width - myWidth) / 2;
            var top = (screen.height - myHeight) / 4;
            window.open('../account/frmConfigParcelShippingAccount.aspx?DivID=' + $("[id$=hdnDivID]").val(), '', 'toolbar=no,titlebar=no,left=' + left + ',top=' + top + ',width=' + myWidth + ',height=' + myHeight + ',scrollbars=yes,resizable=yes');
            return false;
        }
    </script>
    <style>
        .tblNoBorder td, .tblNoBorder th {
            border: 0px solid #fff !important;
        }

        @media (max-width:40em) {
            .tblNoBorder table, .tblNoBorder thead, .tblNoBorder tbody, .tblNoBorder tfoot, .tblNoBorder th, .tblNoBorder td, .tblNoBorder tr {
                display: block;
            }

            .tblNoBorder .editable_select {
                min-height: 30px;
            }

            .tblNoBorder td, .tblNoBorder th {
                text-align: left;
            }
        }

        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }

        div.RadUpload .ruFakeInput {
            visibility: hidden;
            width: 0;
            padding: 0;
        }

        div.RadUpload .ruFileInput {
            width: 1;
        }

        .hidden {
            display: none;
        }

        .VerticalAligned {
            vertical-align: top;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
    </asp:UpdatePanel>
    <asp:Button ID="btnDeleteDocOrLink" OnClick="btnDeleteDocOrLink_Click" runat="server" CssClass="hidden"></asp:Button>
    <asp:HiddenField runat="server" ID="hdnFileId" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-3">
                <div class="pull-left callout bg-theme">
                    <%--<strong>Organization ID : </strong>--%>
                    <asp:Label ID="lblCustomerId" runat="server" Font-Size="Larger"></asp:Label>
                    <%--<asp:HyperLink ID="HprCustomerId" runat="server" />--%>
                    <asp:Label ID="lblAssociation" runat="Server" Font-Size="Larger"></asp:Label>
                </div>
            </div>
            <div class="col-xs-12 col-sm-5">

                <div class="record-small-box record-small-group-box createdBySingleSection">
                    <asp:HyperLink ID="hplTransfer" runat="server" Visible="true" Text="Record Owner:"
                        ToolTip="Transfer Ownership">Owner
                    </asp:HyperLink>
                    <a href="#" id="hplTransferNonVis" runat="server" visible="false">Owner </a>
                    <span class="innerCreated">
                        <asp:Label ID="lblRecordOwner" runat="server"></asp:Label>
                    </span>
                    <a href="#">Created</a>
                    <span class="innerCreated">
                        <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
                    </span>
                    <a href="#">Modified</a>
                    <span class="innerCreated">
                        <asp:Label ID="lblLastModifiedBy" runat="server"></asp:Label>
                    </span>
                </div>

            </div>
            <div class="col-md-4">
                <div class="pull-right">
                    <asp:LinkButton ID="btnAddNewContact" Visible="false" runat="server" CssClass="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New</asp:LinkButton>
                    <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                    <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save &amp; Close</asp:LinkButton>
                    <asp:LinkButton ID="btnEdit" runat="server" CssClass="btn btn-primary"><i class="fa fa-pencil-square-o"></i>&nbsp;&nbsp;Edit</asp:LinkButton>
                    <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-primary"><i class="fa fa-times"></i>&nbsp;&nbsp;Cancel</asp:LinkButton>
                    <asp:LinkButton ID="btnGoogle" Visible="false" runat="server" CssClass="btn btn-primary"><i class="fa fa-upload"></i>&nbsp;&nbsp;Update Google</asp:LinkButton>
                    <asp:LinkButton ID="btnActDelete" runat="server" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <telerik:RadTabStrip ID="radOppTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="True"
        Skin="Default" ClickSelectedTab="True" AutoPostBack="True" SelectedIndex="0"
        MultiPageID="radMultiPage_OppTab">
        <Tabs>
            <telerik:RadTab Text="&nbsp;&nbsp;Account Details&nbsp;&nbsp;" Value="Details" PageViewID="radPageView_Details">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Contacts&nbsp;&nbsp;" Value="Contacts" PageViewID="radPageView_Contacts">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Transactions&nbsp;&nbsp;" Value="Opportunities" PageViewID="radPageView_Opportunities">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Projects&nbsp;&nbsp;" Value="Projects" PageViewID="radPageView_Projects">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Cases&nbsp;&nbsp;" Value="Cases" PageViewID="radPageView_Cases">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Setup&nbsp;&nbsp;" Value="Setup" PageViewID="radPageView_Accounting">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Web Analysis&nbsp;&nbsp;" Value="WebAnalysis" PageViewID="radPageView_WebAnalysis">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Correspondence&nbsp;&nbsp;" Value="Correspondence"
                PageViewID="radPageView_Correspondence">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Assets&nbsp;" Value="Assets" PageViewID="radPageView_Assets" Visible="false">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Contracts&nbsp;" Value="Contracts" PageViewID="radPageView_Contracts">
            </telerik:RadTab>
            <telerik:RadTab Text="Associations" Value="Associations" PageViewID="radPageView_Associations">
            </telerik:RadTab>
            <telerik:RadTab Text="Items" Value="Items" PageViewID="radPageView_Items">
            </telerik:RadTab>
            <telerik:RadTab Text="Vendor Lead-Times" Value="VendorLeadTimes" PageViewID="radPageView_VendorLeadTimes">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage_OppTab" runat="server" SelectedIndex="0" CssClass="pageView">
        <telerik:RadPageView ID="radPageView_Details" runat="server">
            <div class="row">
                <div class="col-xs-12">
                    <div class="pull-left">
                        <asp:ImageButton ID="imgOrder" runat="server" ImageUrl="~/images/Building-48.gif" />
                    </div>
                    <div class="pull-right">
                        <table border="0" style="width: 100%; padding-right: 5px;">
                            <tr>
                                <td style="text-align: left;">
                                    <table id="tblDocuments" runat="server">
                                    </table>
                                </td>
                                <td>
                                    <asp:ImageButton ID="imgOpenDocument" runat="server" ImageUrl="~/images/icons/drawer.png" Style="height: 30px;" />
                                </td>
                                <td style="text-align: left;">
                                    <asp:HyperLink ID="hplOpenDocument" runat="server" Font-Bold="true" Width="100px" Font-Underline="true" Text="Attach or Link" Style="display: inline-block; vertical-align: top; line-height: normal;"></asp:HyperLink>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <asp:Table ID="tblMain" CellPadding="3" CellSpacing="3" align="center" CssClass="table table-responsive tblNoBorder" GridLines="none" border="0" runat="server">
                        </asp:Table>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Contacts" runat="server">
            <div class="table-responsive">
                <div class="pull-right">
                    &nbsp;<a href="#" onclick="return OpenHelpforOrganizationsContactssubtabToolTip()"><label class="badge bg-yellow">?</label></a>
                </div>
                <asp:Table ID="Table1" CellPadding="0" CellSpacing="0" Height="300" runat="server"
                    Width="100%" GridLines="None">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:GridView ID="gvContacts" runat="server" EnableViewState="true" AutoGenerateColumns="false"
                                CssClass="table table-responsive table-bordered" Width="100%" ShowHeaderWhenEmpty="true">
                                <Columns>
                                </Columns>
                            </asp:GridView>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Opportunities" runat="server">
            <div class="table-responsive">
                <div class="col-lg-9">
                    <asp:Table ID="Table5" runat="server" Height="300" Width="100%" GridLines="None">
                        <asp:TableRow>
                            <asp:TableCell VerticalAlign="Top">
                                <uc1:OpportunitiesTab ID="OpportunitiesTab1" runat="server" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </div>
                <div class="col-md-3" style="margin-top: 20px;">
                    <ul class="nav nav-stacked" style="border: 1px solid #f4f4f4;">
                        <li><a href="#">Sales Credit Balance <span class="pull-right badge bg-blue">
                            <asp:Label ID="lblSOCreditMemo" runat="server"></asp:Label></span></a></li>
                        <li><a href="#">Sales Order Balance Due <span class="pull-right badge bg-aqua">
                            <asp:Label ID="lblAmountDueSO" runat="server"></asp:Label></span></a></li>
                        <li><a href="#">Sales Order Amount Past Due <span class="pull-right badge bg-green">
                            <asp:Label ID="lblAmountPastDueSO" runat="server"></asp:Label></span></a></li>
                        <li><a href="#">Purchase Credit Balance <span class="pull-right badge bg-red">
                            <asp:Label ID="lblPOCreditMemo" runat="server"></asp:Label></span></a></li>
                        <li><a href="#">Purchase Order Balance Due <span class="pull-right badge bg-purple">
                            <asp:Label ID="lblAmountDuePO" runat="server"></asp:Label></span></a></li>
                        <li><a href="#">Purchase Order Amount Past Due <span class="pull-right badge bg-orange">
                            <asp:Label ID="lblAmountPastDuePO" runat="server"></asp:Label></span></a></li>
                    </ul>
                    <div class="col-md-12">
                        <asp:HyperLink ID="hrfGL" runat="server" Text="" Style="cursor: pointer">
                                                                <img style="vertical-align:sub" src="../images/open_new_window_notify.gif" > GL Transaction (Fiscal YTD)</asp:HyperLink>
                    </div>
                    <div class="col-md-12">
                        <asp:Image runat="server" ID="imgCreditCard" ImageUrl="~/images/creditcards.png"
                            Style="cursor: pointer; vertical-align: sub" />
                        <asp:HyperLink ID="hrfTransactionDetail" runat="server" Text="CreditCard Transactions"></asp:HyperLink>
                    </div>
                    <div class="col-md-12">
                        <asp:HyperLink ID="hlCustomerStatement" Style="cursor: pointer;" onclick="return OpenCustomerStatement();" runat="server"><img src="../images/GLReport.png" border="0" /> Customer Statement</asp:HyperLink>
                    </div>
                    <div class="col-md-12" id="tblARDetail" runat="server" style="margin-top: 20px; display: none">
                        <table class="table table-responsive">
                            <tr>
                                <td colspan="4">
                                    <asp:HyperLink ID="hplARReport" runat="server" CssClass="hyperlink">A/R Report</asp:HyperLink>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="color: Green">0-30</span>
                                </td>
                                <td>
                                    <asp:Label ID="lblARThirtyDays" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <span style="color: red">0-30</span>
                                </td>
                                <td>
                                    <asp:Label ID="lblARThirtyDaysOverDue" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="color: Green">31�60</span>
                                </td>
                                <td>
                                    <asp:Label ID="lblARSixtyDays" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <span style="color: red">31�60</span>
                                </td>
                                <td>
                                    <asp:Label ID="lblARSixtyDaysOverDue" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="color: Green">61�90 </span>
                                </td>
                                <td>
                                    <asp:Label ID="lblARNinetyDays" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <span style="color: red">61�90 </span>
                                </td>
                                <td>
                                    <asp:Label ID="lblARNinetyDaysOverDue" runat="server"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <span style="color: Green">Over 91</span>
                                </td>
                                <td>
                                    <asp:Label ID="lblAROverNinetyDays" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <span style="color: red">Past Due Over 91</span>
                                </td>
                                <td>
                                    <asp:Label ID="lblAROverNinetyDaysOverDue" runat="server"></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Projects" runat="server">
            <div class="table-responsive">
                <asp:Table ID="Table8" CellPadding="0" CellSpacing="1" Height="300" runat="server"
                    Width="100%" GridLines="None">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <div class="col-lg-3">
                                <div class="form-inline">
                                    <label>Project Status</label>
                                    <asp:DropDownList ID="ddlProjectStatus" runat="server" CssClass="form-control" AutoPostBack="True">
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <asp:DataGrid ID="dgProjectsOpen" runat="server" Width="100%" CssClass="table table-responsive table-bordered" AutoGenerateColumns="False">
                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                <ItemStyle CssClass="is"></ItemStyle>
                                <HeaderStyle CssClass="hs"></HeaderStyle>
                                <Columns>
                                    <asp:BoundColumn Visible="False" DataField="numProId"></asp:BoundColumn>
                                    <asp:BoundColumn Visible="False" DataField="numContactId"></asp:BoundColumn>
                                    <asp:ButtonColumn DataTextField="Name" HeaderText="Name" CommandName="Name"></asp:ButtonColumn>
                                    <asp:BoundColumn DataField="DealID" HeaderText="Opportunity Or Deal ID"></asp:BoundColumn>
                                    <asp:ButtonColumn DataTextField="CustJobContact" HeaderText="Cust. Prime Job Contact"
                                        CommandName="Contact"></asp:ButtonColumn>
                                    <asp:BoundColumn DataField="vcResource" HeaderText="Resource(Project Role)"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="LstCmtMilestone" HeaderText="Last Completed Milestone"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="lstCompStage" HeaderText="Last completed Stage, Completed vs. Due Date"></asp:BoundColumn>
                                </Columns>
                                <PagerStyle Visible="False" ForeColor="#000066" BackColor="White" Mode="NumericPages"></PagerStyle>
                            </asp:DataGrid>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Cases" runat="server">
            <div class="table-responsive">
                <asp:Table ID="Table15" runat="server" Width="100%" GridLines="None" Height="300">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="top">
                            <div class="col-lg-3">
                                <asp:RadioButton ID="radCase" runat="server" Text="Open Cases" AutoPostBack="true" GroupName="radCase"
                                    Checked="true" />
                                <asp:RadioButton ID="radCaseHst" Text="Case History" runat="server" AutoPostBack="true" GroupName="radCase" />
                            </div>

                            <asp:Table ID="Table10" BorderWidth="0" CellPadding="0" CellSpacing="0" runat="server"
                                Width="100%" GridLines="None" Height="250">
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="top">
                                        <asp:DataGrid ID="dgOpenCases" runat="server" Width="100%" CssClass="dg table table-responsive table-bordered" AutoGenerateColumns="False">
                                            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                            <ItemStyle CssClass="is"></ItemStyle>
                                            <HeaderStyle CssClass="hs"></HeaderStyle>
                                            <Columns>
                                                <asp:BoundColumn DataField="numCaseid" Visible="false"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderText="Subject" DataField="textSubject"></asp:BoundColumn>
                                                <asp:ButtonColumn HeaderText="Case No." CommandName="Cases" DataTextField="vcCaseNumber"></asp:ButtonColumn>
                                                <asp:TemplateColumn HeaderText="Target Resolve Date">
                                                    <ItemTemplate>
                                                        <%#ReturnDateTime(DataBinder.Eval(Container.DataItem, "intTargetResolveDate"))%>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn HeaderText="Status" DataField="vcCaseStatus"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderText="Description" DataField="textDesc"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderText="Comments" DataField="textInternalComments"></asp:BoundColumn>
                                            </Columns>
                                        </asp:DataGrid>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Accounting" runat="server">
            <div class="pull-right">
                &nbsp;<a href="#" onclick="return OpenHelpforOrganizationsAccountingSubtab()"><label class="badge bg-yellow">?</label></a>
            </div>
            <asp:Panel ID="PnlFinancialOverview" runat="server">
                <div class="row">
                    <div class="col-xs-12">
                        <asp:UpdatePanel ID="upnlSaveProspects" runat="server" ChildrenAsTriggers="true">
                            <ContentTemplate>
                                <div class="box box-solid box-default">
                                    <div class="box-header">
                                        <h3 class="box-title">Default Configuration</h3>
                                    </div>
                                    <div class="box-body">
                                        <div class="col-lg-12">
                                            <asp:Literal ID="litTermsMessage" runat="server" EnableViewState="False"></asp:Literal>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="pull-right">
                                                <asp:LinkButton ID="btnSaveTerms" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 col-md-3 col-lg-4">
                                                <div class="form-group">
                                                    <label>Credit Limit</label>
                                                    <asp:DropDownList ID="ddlCreditLimit" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-4">
                                                <div class="form-group">
                                                    <label>Net Terms</label>
                                                    <div class="input-group">
                                                        <asp:DropDownList ID="ddlNetDays" runat="server" CssClass="form-control">
                                                        </asp:DropDownList><span class="input-group-addon">Days</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-4">
                                                <div class="col-xs-12 col-md-4">
                                                    <div class="form-group">
                                                        <label>On Credit Hold</label>
                                                        <div class="form-control" style="border: none;">
                                                            <asp:CheckBox runat="server" ID="chkOnCreditHold" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-md-8">
                                                    <div class="form-group">
                                                        <label>
                                                            <asp:Label ID="Label88" Text="[?]" CssClass="tip" runat="server" ToolTip="IF �Commit allocation to Sales Orders� is set to �When Fulfillment Order (Packing Slip) is added� from Global Settings | Order Mgt. then checking this option will change allocation for this customer to allocate based on qty added to Packing Slip instead." />Allocate inventory from Packing Slip (Pick List)</label>
                                                        <div class="form-control" style="border: none;">
                                                            <asp:CheckBox runat="server" ID="chkAllocateInventoryFromPickList" />
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-4">
                                                <div class="form-group">
                                                    <label>Default Expense Account</label>
                                                    <asp:DropDownList ID="ddlDefaultExpense" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-4">
                                                <div class="form-group">
                                                    <label>Preferred payment method</label>
                                                    <asp:DropDownList ID="ddlPaymentMethod" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-4">
                                                <div class="form-group">
                                                    <label>Trading Currency</label>
                                                    <asp:DropDownList ID="ddlCurrency" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-4">
                                                <div class="form-group">
                                                    <asp:HyperLink ID="hrfcc" runat="server" Text="Credit Cards" Style="text-decoration: underline">
                                                    </asp:HyperLink>
                                                    <div class="form-control" style="border: none;">
                                                        <asp:Label ID="lblcc" runat="server"></asp:Label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-4">
                                                <div class="form-group">
                                                    <label>Class</label>
                                                    <asp:DropDownList ID="ddlClass" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-4">
                                                <div class="form-group">
                                                    <label>Item Price Level</label>&nbsp;&nbsp;
                                                    <asp:DropDownList ID="ddlPriceLevel" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-4">
                                                <div class="form-group">
                                                    <label>Field used to pick items on inbound (850) orders</label>&nbsp;&nbsp;
                                                    <asp:DropDownList ID="ddlInbound850PickItem" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-3 col-lg-4">
                                                <div class="form-group">
                                                    <asp:CheckBox ID="chkAutoCheckCustomerPart" runat="server" />
                                                    &nbsp;<label>Auto-check "Customer Part #" when adding line items to Opportunities & Orders</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4 ">
                                                <div class="form-group">
                                                    <label>&nbsp;</label>
                                                    <asp:RadioButton ID="radPlus" GroupName="rad" Visible="false" Checked="True" runat="server" Text="Plus"></asp:RadioButton>
                                                    <asp:RadioButton ID="radMinus" runat="server" Visible="false" GroupName="rad" Text="Minus"></asp:RadioButton>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <label>&nbsp;</label>
                                                    <asp:TextBox ID="txtInterest" runat="server" Visible="false" CssClass="form-control">
                                                    </asp:TextBox>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <fieldset>
                                                    <legend><b>Tax Details</b></legend>
                                                    <asp:CheckBoxList ID="chkTaxItems" CssClass="normal1" runat="server" RepeatDirection="Horizontal">
                                                    </asp:CheckBoxList>

                                                </fieldset>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12">
                                                <br />
                                                <br />
                                                <legend><b>Shipping Details</b></legend>
                                            </div>
                                            <div class="col-xs-12 col-md-4">
                                                <div class="form-group">
                                                    <label style="margin-bottom: 12px;">Preferred Ship Via</label>
                                                    <div>
                                                        <telerik:RadComboBox runat="server" ID="radShipCompany" AccessKey="I" Skin="Vista" Height="130px" Width="100%" DropDownWidth="290"
                                                            EnableVirtualScrolling="false" AutoPostBack="true" ChangeTextOnKeyBoardNavigation="false" ClientIDMode="Static" OnSelectedIndexChanged="radShipCompany_SelectedIndexChanged">
                                                        </telerik:RadComboBox>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-4">
                                                <div class="form-group">
                                                    <label style="margin-bottom: 12px;">Preferred Parcel Shipping Service</label>
                                                    <asp:DropDownList ID="ddlDefaultShippingMethod" runat="server" CssClass="form-control">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-4">
                                                <div class="form-group">
                                                    <label style="width: 100%">
                                                        <a href="javascript:ConfigureParcelShippingAccount()">Set Parcel Shipping Account #</a>

                                                        <div class="pull-right">
                                                            <asp:CheckBox ID="chkShippingLabel" runat="server" Text="Shipping labels required" />
                                                        </div>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-8">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-6">
                                                        <div class="form-group">
                                                            <label>Shipping instructions</label>
                                                            <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" CssClass="form-control"></asp:TextBox>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-12 col-md-6" id="divSignatureType" runat="server">
                                                        <div class="form-group">
                                                            <label>Signature Type</label>
                                                            <asp:DropDownList runat="server" ID="ddlSignatureType" CssClass="form-control">
                                                                <asp:ListItem Text="Service Default" Value="0"></asp:ListItem>
                                                                <asp:ListItem Text="Adult Signature Required" Value="1"></asp:ListItem>
                                                                <asp:ListItem Text="Direct Signature" Value="2"></asp:ListItem>
                                                                <asp:ListItem Text="InDirect Signature" Value="3"></asp:ListItem>
                                                                <asp:ListItem Text="No Signature Required" Value="4"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            <!-- If new value is added than make change in procedure also USP_GetSalesFulfillmentOrder-->
                                                        </div>
                                                    </div>
                                                    <div class="clearfix visible-md-block"></div>
                                                    <div class="col-sm-12 col-md-6" id="divCOD1" runat="server" visible="false">
                                                        <div class="form-group">
                                                            <label>COD Type</label>
                                                            <asp:DropDownList CssClass="form-control" ID="ddlCODType" runat="server">
                                                            </asp:DropDownList>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix visible-md-block"></div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-md-4">
                                                <div class="col-xs-12">
                                                    <asp:CheckBoxList ID="chkOther" runat="server" CellPadding="2" CellSpacing="2" RepeatDirection="Vertical">
                                                    </asp:CheckBoxList>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </asp:Panel>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_WebAnalysis" runat="server">
            <div class="row">
                <div class="col-sm-12 col-md-2">
                    <img alt="Web Analysis" src="../images/WebAnalysis.png" />
                </div>
                <div class="col-sm-12 col-md-10">
                    <table style="width: 100%">
                        <tr style="height: 45px">
                            <td style="font-weight: bold; text-align: right; white-space: nowrap">Referring Page:</td>
                            <td style="width: 100%">
                                <asp:Label ID="lblReferringPage" runat="server"></asp:Label></td>
                        </tr>
                        <tr style="height: 45px">
                            <td style="font-weight: bold; text-align: right; white-space: nowrap">First date visited:</td>
                            <td>
                                <asp:Label ID="lblFirstDateVisited" runat="server"></asp:Label></td>
                        </tr>
                        <tr style="height: 45px">
                            <td style="font-weight: bold; text-align: right; white-space: nowrap">Last date visited:</td>
                            <td>
                                <asp:Label ID="lblLastDateVisited" runat="server"></asp:Label></td>
                        </tr>
                        <tr style="height: 45px">
                            <td style="font-weight: bold; text-align: right; white-space: nowrap">Last page visited:</td>
                            <td>
                                <asp:Label ID="lblLastPageVisited" runat="server"></asp:Label></td>
                        </tr>
                        <tr style="height: 45px">
                            <td style="font-weight: bold; text-align: right; white-space: nowrap">Visit history (all):</td>
                            <td>
                                <img src="../images/Audit.png" alt="" data-toggle="modal" data-target="#divVisitHistory" style="cursor: hand" /></td>
                        </tr>
                        <tr style="height: 45px">
                            <td style="font-weight: bold; text-align: right; white-space: nowrap">IP Address:</td>
                            <td>
                                <asp:Label ID="lblIpAddress" runat="server"></asp:Label></td>
                        </tr>
                        <tr style="height: 45px">
                            <td style="font-weight: bold; text-align: right; white-space: nowrap">IP Location:</td>
                            <td>
                                <asp:Label ID="lblIPLocation" runat="server"></asp:Label></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="divVisitHistory" class="modal fade" role="dialog">
                <div class="modal-dialog modal-sm modal-lg">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" style="font-size: 27px">&times;</button>
                            <h4 class="modal-title">Visit History</h4>
                        </div>
                        <div class="modal-body">
                            <div class="table-responsive">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DataList ID="dlWebAnlys" runat="server" Width="100%" CellPadding="0" CellSpacing="2">
                                            <ItemTemplate>
                                                <table style="width: 100%" class="table table-bordered table-striped">
                                                    <tr>
                                                        <th style="text-align: right">Date Visitied:
                                                        </th>
                                                        <td style="text-align: left">
                                                            <asp:Label ID="lblCreated" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.dtCreated") %>'>
                                                            </asp:Label>
                                                        </td>
                                                        <th style="text-align: right">
                                                            <asp:Label ID="Label4" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.NoOfTimes") %>'>
                                                            </asp:Label>&nbsp;Pages Viewed :
                                                        </th>
                                                        <td style="text-align: left">
                                                            <asp:LinkButton ID="lnk" ClientIDMode="AutoID" CommandName="Pages" runat="server" Style="text-decoration: none">
													<font>Click Here</font></asp:LinkButton>
                                                        </td>
                                                        <th style="text-align: right">Total Time on site :
                                                        </th>
                                                        <td style="text-align: left">
                                                            <asp:Label ID="lblTotalTime" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcTotalTime") %>'>
                                                            </asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <SelectedItemTemplate>
                                                <table class="table table-bordered table-striped">
                                                    <tr class="hs">
                                                        <th style="text-align: right">
                                                            <asp:Label ID="lblID" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.numTrackingID") %>'
                                                                Visible="false">
                                                            </asp:Label>
                                                            Date Visitied:
                                                        </th>
                                                        <td style="text-align: left">
                                                            <asp:Label ID="lblCreated" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.dtCreated") %>'>
                                                            </asp:Label>
                                                        </td>
                                                        <th style="text-align: right">Pages Viewed :
                                                        </th>
                                                        <td style="text-align: left">
                                                            <asp:Label ID="Label4" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.NoOfTimes") %>'></asp:Label>
                                                        </td>
                                                        <th style="text-align: right">Total Time on site :
                                                        </th>
                                                        <td style="text-align: left">
                                                            <asp:Label ID="lblTotalTime" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcTotalTime") %>'>
                                                            </asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="6">
                                                            <asp:DataGrid ID="dgWebAnlys" runat="server" AllowSorting="True" CssClass="table table-bordered table-striped" Width="100%"
                                                                AutoGenerateColumns="False" UseAccessibleHeader="true">
                                                                <Columns>
                                                                    <asp:BoundColumn Visible="False" DataField="numTracVisitorsHDRID"></asp:BoundColumn>
                                                                    <asp:BoundColumn HeaderText="Page" DataField="vcPageName"></asp:BoundColumn>
                                                                    <asp:BoundColumn HeaderText="Time Spent on the Page" DataField="vcElapsedTime"></asp:BoundColumn>
                                                                    <asp:BoundColumn HeaderText="Time Visited" DataField="TimeVisited"></asp:BoundColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </SelectedItemTemplate>
                                        </asp:DataList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Correspondence" runat="server">
            <div class="table-responsive">
                <asp:Table ID="Table4" CellPadding="0" CellSpacing="0" runat="server" Width="100%"
                    GridLines="None" Height="300">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <uc1:frmCorrespondence ID="Correspondence1" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Assets" runat="server">
            <div class="table-responsive">
                <asp:Table ID="Table2" CellPadding="0" CellSpacing="0" runat="server" Width="100%"
                    GridLines="None" Height="300">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <uc1:AssetList ID="AssetList" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Contracts" runat="server">
            <div class="table-responsive">
                <asp:Table ID="Table6" CellPadding="0" CellSpacing="0" runat="server" Width="100%"
                    GridLines="None" Height="300">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <uc1:ContractsLog ID="ContractsLog" runat="server" />
                            <asp:DataGrid ID="dgContracts" AllowSorting="false" runat="server" Width="100%" CssClass="dg" Style="display: none"
                                AutoGenerateColumns="false">
                                <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                                <ItemStyle CssClass="is"></ItemStyle>
                                <HeaderStyle CssClass="hs"></HeaderStyle>
                                <Columns>
                                    <asp:BoundColumn Visible="False" DataField="numContractID"></asp:BoundColumn>
                                    <asp:TemplateColumn SortExpression="bintCreatedOn" HeaderText="<font>Created On</font>">
                                        <ItemTemplate>
                                            <%#DataBinder.Eval(Container.DataItem, "bintCreatedOn")%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:ButtonColumn DataTextField="vcContractName" SortExpression="vcContractName"
                                        HeaderText="<font>Contract Name</font>" CommandName="Name"></asp:ButtonColumn>
                                    <asp:BoundColumn DataField="Days" SortExpression="bintStartDate" HeaderText="<font>Days used</font>"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Incidents" SortExpression="numIncidents" HeaderText="<font>Incidents used</font>"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Hours" SortExpression="numhours" HeaderText="<font>Hours used</font>"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="Amount" SortExpression="numAmount" HeaderText="<font>Amount used</font>"></asp:BoundColumn>
                                    <asp:TemplateColumn>
                                        <ItemTemplate>
                                            <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete"></asp:Button>
                                            <asp:LinkButton ID="lnkdelete" runat="server" Visible="false">
											                                <font color="#730000">*</font></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Associations" runat="server">
            <div class="table-responsive">
                <asp:Table ID="tbl5" runat="server" Width="100%" CellSpacing="0" CellPadding="0"
                    GridLines="None" Height="380">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <iframe id="ifAssociation" runat="server" frameborder="0" width="100%" scrolling="auto"
                                            height="380"></iframe>
                                    </td>
                                </tr>
                            </table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Items" runat="server">
            <div class="row padbottom10">
                <div class="col-xs-12 col-sm-6">
                    <div class="form-inline">
                        <label>Order Date:</label>
                        From
                        <asp:RadioButton ID="rdbFilterDate" runat="server" AutoPostBack="true" />
                        <BizCalendar:Calendar ID="calFromDate" runat="server" ClientIDMode="AutoID" Enabled="false" />
                        To 
                        <BizCalendar:Calendar ID="calToDate" runat="server" ClientIDMode="AutoID" Enabled="false" />
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="form-inline pull-right">
                        <label>Item/Model:</label>
                        <asp:TextBox ID="txtSearchModel" runat="server" CssClass="form-control" Width="200" MaxLength="20" AutoComplete="OFF"></asp:TextBox>
                        <asp:Button ID="btnGo" runat="server" Text="Go" CssClass="btn btn-primary" />
                    </div>
                </div>
            </div>
            <telerik:RadTabStrip ID="radStripItems" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
                Skin="Default" ClickSelectedTab="True" AutoPostBack="True" SelectedIndex="0"
                MultiPageID="radMultiPage_TabItems" Height="100%">
                <Tabs>
                    <telerik:RadTab Text="Bought & Sold" Value="Bought & Sold" PageViewID="radPageView_BoughtSold">
                    </telerik:RadTab>
                    <telerik:RadTab Text="Vended" Value="Vended" PageViewID="radPageView_Vended">
                    </telerik:RadTab>
                </Tabs>
            </telerik:RadTabStrip>
            <telerik:RadMultiPage ID="radMultiPage_TabItems" runat="server"
                CssClass="pageView" Height="100%" SelectedIndex="0">
                <telerik:RadPageView ID="radPageView_BoughtSold" runat="server">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="pull-left">
                            </div>
                            <div class="pull-right">
                                <webdiyer:AspNetPager ID="PgrBuyItems" runat="server"
                                    PagingButtonSpacing="0"
                                    CssClass="bizgridpager"
                                    AlwaysShow="true"
                                    CurrentPageButtonClass="active"
                                    PagingButtonUlLayoutClass="pagination"
                                    PagingButtonLayoutType="UnorderedList"
                                    FirstPageText="<<"
                                    LastPageText=">>"
                                    NextPageText=">"
                                    PrevPageText="<"
                                    Width="100%"
                                    UrlPaging="false"
                                    NumericButtonCount="8"
                                    ShowPageIndexBox="Never"
                                    ShowCustomInfoSection="Left"
                                    OnPageChanged="PgrBuyItems_PageChanged"
                                    CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
                                    CustomInfoClass="bizpagercustominfo">
                                </webdiyer:AspNetPager>
                                <asp:TextBox ID="txtCurrentPageBuyItems" runat="server" Style="display: none"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <asp:TextBox ID="txtSortColumnBought" runat="server" Style="display: none"></asp:TextBox>
                                <asp:TextBox ID="txtSortOrderBought" runat="server" Style="display: none"></asp:TextBox>
                                <asp:GridView ID="gvItemsBought" runat="server" EnableViewState="true" AutoGenerateColumns="false" CssClass="table table-bordered table-striped" Width="100%" ShowHeaderWhenEmpty="true" AllowSorting="true">
                                    <Columns>
                                        <asp:BoundField DataField="ItemName" HeaderText="Item Name" SortExpression="ItemName" />
                                        <asp:BoundField DataField="vcSKU" HeaderText="SKU" SortExpression="SKU" />
                                        <asp:BoundField DataField="vcUPC" HeaderText="UPC" SortExpression="UPC" />
                                        <asp:TemplateField HeaderText="Customer Part#" SortExpression="CustomerPartNo" ItemStyle-CssClass="customerpartno click">
                                            <ItemTemplate>
                                                <div id="CustomerPartNo~899~False~<%# Eval("numItemCode")%>~<%# Eval("numDivisionID")%>~0~0">
                                                    <%# Eval("CustomerPartNo")%>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Order ID" SortExpression="OrderNo">
                                            <ItemTemplate>

                                                <asp:HyperLink ID="hplItemOrderNo" runat="server" Text='<%# Eval("OrderNo")%>'></asp:HyperLink>
                                                <asp:Label runat="server" ID="lblItemOrderNo" Text='<%# Eval("OrderNo")%>' Visible="false"></asp:Label>
                                                <asp:Label runat="server" ID="llb" Text='<%# Eval("OppId")%>' Visible="false"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="OrderDate" HeaderText="Created On" SortExpression="OrderNo" />
                                        <asp:BoundField DataField="Units" HeaderText="Units" SortExpression="Units" />

                                        <asp:TemplateField HeaderText="Rate/Unit" SortExpression="BasePrice">
                                            <ItemTemplate>
                                                <%#ReturnMoney(DataBinder.Eval(Container.DataItem, "BasePrice"))%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amount" SortExpression="ChargedAmount">
                                            <ItemTemplate>
                                                <%#ReturnMoney(DataBinder.Eval(Container.DataItem, "ChargedAmount"))%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        No records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                                <br />
                            </div>
                        </div>
                    </div>
                </telerik:RadPageView>
                <telerik:RadPageView ID="radPageView_Vended" runat="server" Height="100%">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="pull-left">
                            </div>
                            <div class="pull-right">
                                <webdiyer:AspNetPager ID="PgrVenedItems" runat="server"
                                    PagingButtonSpacing="0"
                                    CssClass="bizgridpager"
                                    AlwaysShow="true"
                                    CurrentPageButtonClass="active"
                                    PagingButtonUlLayoutClass="pagination"
                                    PagingButtonLayoutType="UnorderedList"
                                    FirstPageText="<<"
                                    LastPageText=">>"
                                    NextPageText=">"
                                    PrevPageText="<"
                                    Width="100%"
                                    UrlPaging="false"
                                    NumericButtonCount="8"
                                    ShowPageIndexBox="Never"
                                    ShowCustomInfoSection="Left"
                                    OnPageChanged="PgrVenedItems_PageChanged"
                                    CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
                                    CustomInfoClass="bizpagercustominfo">
                                </webdiyer:AspNetPager>
                                <asp:TextBox ID="txtCurrentPageVendedItems" runat="server" Style="display: none"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <asp:GridView ID="gvItemsVended" runat="server" EnableViewState="true" AutoGenerateColumns="false" CssClass="table table-bordered table-striped" Width="100%" ShowHeaderWhenEmpty="true" AllowSorting="true">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Item Name" SortExpression="ItemName">
                                            <ItemTemplate>

                                                <asp:HyperLink ID="hplVItemOrderNo" runat="server" Text='<%# Eval("ItemName")%>'></asp:HyperLink>
                                                <asp:Label runat="server" ID="lblVItemOrderNo" Text='<%# Eval("ItemName")%>' Visible="false"></asp:Label>
                                                <asp:Label runat="server" ID="llbV" Text='<%# Eval("numItemCode")%>' Visible="false"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="vcSKU" HeaderText="SKU" SortExpression="SKU" />
                                        <asp:BoundField DataField="vcUPC" HeaderText="UPC" SortExpression="UPC" />
                                        <asp:BoundField DataField="CustomerPartNo" HeaderText="Customer Part#" SortExpression="CustomerPartNo" />
                                        <asp:BoundField DataField="PartNo" HeaderText="Part #" SortExpression="PartNo" />

                                        <asp:TemplateField HeaderText="Cost" SortExpression="MinCost">
                                            <ItemTemplate>
                                                <%#ReturnMoney(DataBinder.Eval(Container.DataItem, "MinCost"))%>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="MinOrderQty" HeaderText="Min Order Qty" SortExpression="MinOrderQty" />


                                    </Columns>
                                    <EmptyDataTemplate>
                                        No records Found.
                                    </EmptyDataTemplate>
                                </asp:GridView>
                                <br />
                            </div>
                        </div>
                    </div>
                </telerik:RadPageView>
            </telerik:RadMultiPage>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_VendorLeadTimes" runat="server">
            <!--#include file="~/common/VendorLeadTimes.html"-->
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <table width="100%">
    </table>
    <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtHidden" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtROwner" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtCorrTotalPage" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtCorrTotalRecords" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtContactType" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtContId" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:Label ID="lblRecords" Style="display: none" runat="server"></asp:Label>
    <asp:TextBox ID="lblCompanyType" Style="display: none" runat="server">
    </asp:TextBox>
    
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    <asp:Label ID="lblTitle" runat="server">Account Details</asp:Label>&nbsp;&nbsp;<asp:Label ID="lblOrganizationID" runat="server" ForeColor="Gray" />&nbsp;<a href="#" onclick="return OpenHelpPopUp('relationshipsdetail')"><label class="badge bg-yellow">?</label></a>
    <asp:HiddenField ID="hdnDivID" runat="server" />
    <asp:HiddenField ID="hfOwner" runat="server" />
    <asp:HiddenField ID="hfTerritory" runat="server" />
    <asp:HiddenField runat="server" ID="hdnDivisionID" />
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    <div class="btn btn-default btn-sm" runat="server" id="btnLayout">
        <span><i class="fa fa-columns"></i>&nbsp;&nbsp;Layout</span>
    </div>
    <asp:LinkButton runat="server" ID="lbtnDemote" CssClass="btn btn-primary btn-sm"><i class="fa fa-thumbs-down"></i>&nbsp;&nbsp;Demote to Prospect 
    </asp:LinkButton>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <telerik:RadWindow RenderMode="Lightweight" runat="server" ID="RadWinDocumentFiles" Title="Documents / Files" RestrictionZoneID="ContentTemplateZone" Modal="true" Behaviors="Resize,Close,Move" Width="800" Height="530">
        <ContentTemplate>
            <asp:UpdatePanel ID="Updatepanel3" runat="server" UpdateMode="Conditional" class="row" style="margin-right: 0px; margin-left: 0px">
                <ContentTemplate>
                    <br />
                    <%--<div class="row">--%>
                    <div class="col-xs-12 padbottom10">
                        <div class="pull-right">
                            <input type="button" id="btnCloseDocumentFiles" onclick="return CloseDocumentFiles('C');" class="btn btn-primary" style="color: white" value="Close" />
                        </div>
                    </div>
                    <%-- </div>
                    <div class="row">--%>
                    <div class="col-sm-6">
                        <h4>
                            <img src="../images/icons/Attach.png" height="24">
                            Upload a Document from your Desktop
                        </h4>
                        <hr style="color: #dcdcdc" />
                        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel1" ClientEvents-OnRequestStart="requestStart">
                            <div class="form-group">
                                <div class="custom-file">
                                    <input class="custom-file-input" id="fileupload" type="file" name="fileupload" runat="server">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Section</label>
                                <asp:DropDownList ID="ddlSectionFile" runat="server" CssClass="form-control"></asp:DropDownList>
                            </div>
                            <asp:Button ID="btnAttach" OnClick="btnAttach_Click" runat="server" Text="Attach & Close" CssClass="btn btn-primary"></asp:Button>
                            <asp:Button ID="btnLinkClose" OnClick="btnLinkClose_Click" runat="server" CssClass="hidden"></asp:Button>
                        </telerik:RadAjaxPanel>
                        <telerik:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server" InitialDelayTime="0" Skin="Default">
                        </telerik:RadAjaxLoadingPanel>
                    </div>

                    <div class="col-sm-6" id="divLinkUpload">
                        <h4>
                            <img src="../images/icons/Link.png">
                            Link to a Document</h4>
                        <hr style="color: #dcdcdc" />
                        <div class="form-group">
                            <label>Link Name</label>
                            <asp:TextBox runat="server" ID="txtLinkName" EmptyMessage="Link Name" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Link URL</label>
                            <asp:TextBox runat="server" ID="txtLinkURL" EmptyMessage="Paste link here" TextMode="MultiLine" CssClass="form-control" Rows="3"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Section</label>
                            <asp:DropDownList ID="ddlSectionLink" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                        <asp:Button ID="btnLink" runat="server" OnClientClick="return CloseDocumentFiles('L');" Text="Link & Close" CssClass="btn btn-primary"></asp:Button>
                        <asp:Label ID="Label2" Text="[?]" CssClass="tip" runat="server" ToolTip="Linking to a file on Google Drive or any other file repository is a good way to preserve your BizAutomation storage space. To avoid login credentials when clicking the  link, just make it public (e.g. On Google Drive you have to make sure that �Link Sharing� is set to �Public on the web�)." />
                    </div>
                    <%--</div>--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </telerik:RadWindow>
</asp:Content>

