﻿Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports System.Reflection
Imports BACRM.BusinessLogic.Contract
Imports Telerik.Web.UI

Namespace BACRM.UserInterface.Accounts
    Public Class OpportunitiesTab
        Inherits BACRMUserControl

        Dim objPage As New BACRM.BusinessLogic.Common.BACRMPage
        Dim m_aryRightsForOpp() As Integer
        Dim objProspects As CProspects

        'Dim objAccounts As CAccounts
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""

        Private _DivisionID As Long
        Public Property DivisionID() As Long
            Get
                Return _DivisionID
            End Get
            Set(ByVal value As Long)
                _DivisionID = value
            End Set
        End Property

        Private _ContactID As Long
        Public Property ContactID() As Long
            Get
                Return _ContactID
            End Get
            Set(ByVal value As Long)
                _ContactID = value
            End Set
        End Property

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    If ViewState("SortTransExpression") = Nothing Then
                        ViewState("SortTransExpression") = "dtCreatedDate"
                        ViewState("SortTransDirection") = "DESC"
                    End If

                    objPage.PersistTable.Load(boolOnlyURL:=True, strParam:="TransactionInfo")
                    If objPage.PersistTable.Count > 0 Then
                        ddlTransactionDate.SelectedValue = objPage.PersistTable(ddlTransactionDate.ID) 'CCommon.ToLong(objPage.PersistTable())
                        Dim strValues As String = ""
                        strValues = objPage.PersistTable(radTransactionType.ID)
                        If strValues IsNot Nothing Then
                            If strValues.Length > 0 Then
                                For Each item As Integer In strValues.Split(",")
                                    If radTransactionType.Items.FindItemByValue(item) IsNot Nothing Then
                                        radTransactionType.Items.FindItemByValue(item).Checked = True
                                    End If
                                Next
                            End If
                        End If
                    End If

                    BindDatagrid(CCommon.ToString(ViewState("SortTransExpression")), 0)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        'Private Sub dgSalesOrder_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSalesOrder.ItemCommand
        '    Try
        '        Dim lngOppID As Long
        '        lngOppID = e.Item.Cells(0).Text
        '        If e.CommandName = "Name" Then
        '            Response.Redirect("../opportunity/frmOpportunities.aspx?frm=accounts&Opid=" & lngOppID & "&SI=" & SI1 & "&SI1=" & radOppTab.SelectedIndex & "&frm1=" & frm1 & "&frm2=" & frm2)
        '        End If
        '        If e.CommandName = "Contact" Then
        '            objCommon.OppID = lngOppID
        '            objCommon.charModule = "O"
        '            objCommon.GetCompanySpecificValues1()
        '            Response.Redirect("../contact/frmContacts.aspx?frm=accounts&CntId=" & objCommon.ContactID & "&SI=" & SI1 & "&SI1=" & radOppTab.SelectedIndex & "&frm1=" & frm1 & "&frm2=" & frm2)
        '        End If
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub

        'Private Sub dgBizDocs_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgBizDocs.ItemCommand
        '    Try
        '        Dim lngOppID As Long
        '        lngOppID = e.Item.Cells(0).Text
        '        If e.CommandName = "Name" Then
        '            Response.Redirect("../opportunity/frmOpportunities.aspx?frm=accounts&Opid=" & lngOppID & "&SI=" & SI1 & "&SI1=" & radOppTab.SelectedIndex & "&frm1=" & frm1 & "&frm2=" & frm2)
        '        End If
        '        If e.CommandName = "Contact" Then
        '            objCommon.OppID = lngOppID
        '            objCommon.charModule = "O"
        '            objCommon.GetCompanySpecificValues1()
        '            Response.Redirect("../contact/frmContacts.aspx?frm=accounts&CntId=" & objCommon.ContactID & "&SI=" & SI1 & "&SI1=" & radOppTab.SelectedIndex & "&frm1=" & frm1 & "&frm2=" & frm2)
        '        End If
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub

        'Private Sub radOppTab_TabClick(sender As Object, e As Telerik.Web.UI.RadTabStripEventArgs) Handles radOppTab.TabClick
        '    Try
        '        LoadData()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub
        'Private Sub LoadData()
        '    Try

        '        'tblPaging.Visible = True
        '        'tblOppDetail.Visible = True

        '        objProspects = New CProspects()
        '        objProspects.DomainID = Session("DomainID")
        '        objProspects.DivisionID = _DivisionID
        '        objProspects.ContactId = _ContactID
        '        If txtCurrrentPageDeals.Text.Trim = "" Then txtCurrrentPageDeals.Text = 1
        '        objProspects.CurrentPage = txtCurrrentPageDeals.Text.Trim()

        '        objProspects.PageSize = Session("PagingRows")
        '        objProspects.TotalRecords = 0

        '        If radOppTab.SelectedIndex = 0 Or radOppTab.SelectedIndex = 2 Then
        '            objProspects.OppType = IIf(radOppTab.SelectedIndex = 2, 2, 1)
        '            objProspects.ByteMode = 0
        '            dgSalesOrder.DataSource = objProspects.GetOppDetailsForOrg()
        '            dgSalesOrder.DataBind()
        '            dgBizDocs.Visible = False
        '            dgSalesOrder.Visible = True
        '            dgOpportunities.Visible = False
        '        ElseIf radOppTab.SelectedIndex = 1 Or radOppTab.SelectedIndex = 3 Then
        '            objProspects.OppType = IIf(radOppTab.SelectedIndex = 3, 2, 1)
        '            objProspects.ByteMode = 1
        '            dgBizDocs.DataSource = objProspects.GetOppDetailsForOrg()
        '            dgBizDocs.DataBind()
        '            dgBizDocs.Visible = True
        '            dgSalesOrder.Visible = False
        '            dgOpportunities.Visible = False
        '        ElseIf radOppTab.SelectedIndex = 4 Then
        '            objProspects.OppType = 0
        '            objProspects.OppStatus = radDeal.SelectedValue
        '            objProspects.ByteMode = 2
        '            dgOpportunities.DataSource = objProspects.GetOppDetailsForOrg()
        '            dgOpportunities.DataBind()
        '            dgBizDocs.Visible = False
        '            dgSalesOrder.Visible = False
        '            dgOpportunities.Visible = True
        '        End If

        '        bizPager.PageSize = Session("PagingRows")
        '        bizPager.CurrentPageIndex = txtCurrrentPageDeals.Text
        '        bizPager.RecordCount = objProspects.TotalRecords

        '        If radOppTab.SelectedIndex = 3 Then
        '            objProspects = New CProspects()
        '            objProspects.DomainID = Session("DomainID")
        '            objProspects.DivisionID = _DivisionID
        '            objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
        '            dgVendorPayment.DataSource = objProspects.GetDivisionBillPaymentDetails
        '            dgVendorPayment.DataBind()

        '            tblBills.Visible = True
        '        Else
        '            tblBills.Visible = False
        '        End If
        '        'If objProspects.TotalRecords = 0 Then
        '        '    'tblPaging.Visible = False
        '        '    lblRecordsDeals.Text = 0
        '        'Else
        '        '    '  tblPaging.Visible = True
        '        '    lblRecordsDeals.Text = String.Format("{0:#,###}", objProspects.TotalRecords)
        '        '    Dim strTotalPage As String()
        '        '    Dim decTotalPage As Decimal
        '        '    decTotalPage = lblRecordsDeals.Text / Session("PagingRows")
        '        '    decTotalPage = Math.Round(decTotalPage, 2)
        '        '    strTotalPage = CStr(decTotalPage).Split(".")
        '        '    If (lblRecordsDeals.Text Mod Session("PagingRows")) = 0 Then
        '        '        ' lblTotalDeals.Text = strTotalPage(0)
        '        '        txtTotalPageDeals.Text = strTotalPage(0)
        '        '    Else
        '        '        ' lblTotalDeals.Text = strTotalPage(0) + 1
        '        '        txtTotalPageDeals.Text = strTotalPage(0) + 1
        '        '    End If
        '        '    'txtTotalRecordsDeals.Text = lblRecordsDeals.Text
        '        'End If

        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub
        Public Function ReturnMoney(ByVal Money)
            Try
                If Not IsDBNull(Money) Then Return String.Format("{0:#,##0.00}", Money)
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        'Private Sub radDeal_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radDeal.SelectedIndexChanged
        '    Try
        '        LoadData()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        Response.Write(ex)
        '    End Try
        'End Sub

        Private Sub btnOppTabGo_Click(sender As Object, e As System.EventArgs) Handles btnOppTabGo.Click
            Try
                'LoadData()
                BindDatagrid(CCommon.ToString(ViewState("SortTransExpression")), 0)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Protected Sub btnTransactionType_Click(sender As Object, e As EventArgs)
            Try
                BindDatagrid(CCommon.ToString(ViewState("SortTransExpression")), 0)
                SaveFilter()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub ddlTransactionDate_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlTransactionDate.SelectedIndexChanged
            Try
                BindDatagrid(CCommon.ToString(ViewState("SortTransExpression")), 0)
                SaveFilter()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindDatagrid(ByVal strExpression As String, ByVal isSortChanged As Boolean)
            Try
                Dim strTransactionType As String = ""

                objProspects = New CProspects()
                objProspects.DomainID = Session("DomainID")
                objProspects.DivisionID = _DivisionID
                objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")

                If txtCurrrentPageDeals.Text.Trim = "" Then txtCurrrentPageDeals.Text = 1
                objProspects.CurrentPage = txtCurrrentPageDeals.Text.Trim()

                objProspects.PageSize = Session("PagingRows")
                objProspects.TotalRecords = 0

                For Each item As RadComboBoxItem In radTransactionType.CheckedItems
                    If CCommon.ToLong(item.Value) > 0 Then
                        strTransactionType = strTransactionType & item.Value & ","
                    End If
                Next
                objProspects.strTransactionType = strTransactionType.TrimEnd(",")

                Dim dtStartDate As DateTime
                Dim dtEndDate As DateTime

                Select Case ddlTransactionDate.SelectedValue
                    Case 1 'All Dates
                        dtStartDate = SqlTypes.SqlDateTime.MinValue
                        dtEndDate = SqlTypes.SqlDateTime.MaxValue
                    Case 2 'Today
                        dtStartDate = DateTime.Now.Date & " 00:00:00"
                        dtEndDate = DateTime.Now.Date & " 23:59:59"
                    Case 3 'Yesterday
                        dtStartDate = DateAdd("d", -1, DateTime.Now).Date & " 00:00:00"
                        dtEndDate = DateAdd("d", -1, DateTime.Now).Date & " 23:59:59"
                    Case 4 'This Week
                        dtStartDate = DateAdd("d", 0 - DateTime.Now.DayOfWeek, DateTime.Now).Date & " 00:00:00"
                        dtEndDate = DateAdd("d", 6, dtStartDate).Date & " 23:59:59"
                    Case 5 'This Month
                        dtStartDate = DateAdd("d", 0 - DateTime.Now.Day + 1, DateTime.Now).Date & " 00:00:00"
                        dtEndDate = DateAdd("d", DateTime.DaysInMonth(dtStartDate.Year, dtStartDate.Month) - 1, dtStartDate).Date & " 23:59:59"
                    Case 6 'This Quarter
                        Dim intMonthsInQuarter As Integer = 3
                        dtStartDate = DateSerial(Year(DateTime.Now), Int((Month(DateTime.Now) - 1) / intMonthsInQuarter) * intMonthsInQuarter + 1, 1).Date & " 00:00:00"
                        dtEndDate = DateSerial(Year(DateTime.Now), Int((Month(DateTime.Now) - 1) / intMonthsInQuarter) * intMonthsInQuarter + (intMonthsInQuarter + 1), 0).Date & " 23:59:59"
                    Case 7 'This Year
                        dtStartDate = DateSerial(Year(DateTime.Now), 1, 1).Date & " 00:00:00"
                        dtEndDate = DateSerial(Year(DateTime.Now), 12, 31).Date & " 23:59:59"
                    Case 8 'Last Week
                        dtStartDate = DateAdd("d", 0 - DateTime.Now.DayOfWeek, DateTime.Now.AddDays(-7)).Date & " 00:00:00"
                        dtEndDate = DateAdd("d", 6, dtStartDate).Date & " 23:59:59"
                    Case 9 'Last Month
                        dtStartDate = DateAdd("d", 0 - DateTime.Now.Day + 1, DateTime.Now.AddMonths(-1)).Date & " 00:00:00"
                        dtEndDate = DateAdd("d", DateTime.DaysInMonth(dtStartDate.Year, dtStartDate.Month) - 1, dtStartDate).Date & " 23:59:59"
                    Case 10 'Last Quarter
                        Dim intMonthsInQuarter As Integer = 3
                        Dim TodayDate As DateTime = DateTime.Now.AddMonths(-intMonthsInQuarter)

                        dtStartDate = DateSerial(Year(TodayDate), Int((Month(TodayDate) - 1) / intMonthsInQuarter) * intMonthsInQuarter + 1, 1).Date & " 00:00:00"
                        dtEndDate = DateSerial(Year(TodayDate), Int((Month(TodayDate) - 1) / intMonthsInQuarter) * intMonthsInQuarter + (intMonthsInQuarter + 1), 0).Date & " 23:59:59"
                    Case 11 'Last Year
                        dtStartDate = DateSerial(Year(DateTime.Now) - 1, 1, 1).Date & " 00:00:00"
                        dtEndDate = DateSerial(Year(DateTime.Now) - 1, 12, 31).Date & " 23:59:59"
                End Select

                objProspects.FromDate = dtStartDate
                objProspects.ToDate = dtEndDate

                Dim sortDirection As String
                sortDirection = GetSortDirection(strExpression, isSortChanged)

                If String.IsNullOrEmpty(strExpression) Then
                    objProspects.SortOrder = 2
                ElseIf strExpression = "dtCreatedDate" Then
                    If sortDirection = "ASC" Then
                        objProspects.SortOrder = 1
                    Else
                        objProspects.SortOrder = 2
                    End If
                End If

                Dim dtTransaction As DataTable = objProspects.GetOrganizationTransaction

                'Group records as per Transaction Type
                Dim drNewRow As DataRow
                Dim dvTransaction As DataView = dtTransaction.DefaultView
                Dim dtFinal As New DataTable
                Dim dtTemp As New DataTable
                dtFinal = dtTransaction.Clone
                dtTemp = dtTransaction.Clone
                strTransactionType = strTransactionType.TrimEnd(",")
                If strExpression <> "dtCreatedDate" Then
                    dvTransaction.Sort = strExpression & " " & sortDirection
                End If

                For Each strItem As String In strTransactionType.Split(",")
                    dtTemp = New DataTable
                    drNewRow = dtFinal.NewRow
                    drNewRow("intTransactionType") = CCommon.ToInteger(strItem.Trim())
                    Select Case CCommon.ToInteger(strItem.Trim())
                        Case 1
                            drNewRow("vcTransactionType") = "<b>Sales Order</b>"
                        Case 2
                            drNewRow("vcTransactionType") = "<b>Sales Order Invoices(BizDocs)</b>"
                        Case 3
                            drNewRow("vcTransactionType") = "<b>Sales Opportunity</b>"
                        Case 4
                            drNewRow("vcTransactionType") = "<b>Purchase Order"
                        Case 5
                            drNewRow("vcTransactionType") = "<b>Purchase Order Bill(BizDocs)</b>"
                        Case 6
                            drNewRow("vcTransactionType") = "<b>Purchase Opportunity</b>"
                        Case 7
                            drNewRow("vcTransactionType") = "<b>Sales Return</b>"
                        Case 8
                            drNewRow("vcTransactionType") = "<b>Purchase Return</b>"
                        Case 9
                            drNewRow("vcTransactionType") = "<b>Credit Memo</b>"
                        Case 10
                            drNewRow("vcTransactionType") = "<b>Refund Receipt</b>"
                        Case 11
                            drNewRow("vcTransactionType") = "<b>Bills</b>"
                        Case 12
                            drNewRow("vcTransactionType") = "<b>UnApplied Bill Payments</b>"
                        Case 13
                            drNewRow("vcTransactionType") = "<b>Checks</b>"
                        Case 14
                            drNewRow("vcTransactionType") = "<b>Deposits</b>"
                        Case 15
                            drNewRow("vcTransactionType") = "<b>Unapplied Payments</b>"
                        Case 16
                            drNewRow("vcTransactionType") = "<b>Landed Cost</b>"
                    End Select

                    If dtFinal.Select("intTransactionType=" & CCommon.ToInteger(strItem.Trim())).Length > 0 Then
                    Else
                        dtFinal.Rows.Add(drNewRow)
                        dtFinal.AcceptChanges()
                    End If

                    dvTransaction.RowFilter = "intTransactionType = " & CCommon.ToInteger(strItem.Trim())
                    'Added by Sachin Sadhu||Date:3rdFeb2014
                    'Purpose:For Sorting  ||Customer:AudioGenex(JJ)
                    'dvTransaction.Sort = strExpression & " " & GetSortDirection(strExpression)
                    'Ended here
                    dtTemp = dvTransaction.ToTable()
                    For Each drRow As DataRow In dtTemp.Rows
                        drRow("vcTransactionType") = ""
                        dtFinal.ImportRow(drRow)
                        dtFinal.AcceptChanges()
                    Next
                Next
                gvTransaction.DataSource = dtFinal
                gvTransaction.DataBind()

                bizPager.PageSize = Session("PagingRows")
                bizPager.CurrentPageIndex = txtCurrrentPageDeals.Text
                bizPager.RecordCount = objProspects.TotalRecords

                ViewState("LastPage") = bizPager.CurrentPageIndex
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub SaveFilter()
            Try
                Dim strValues As String = ""
                objPage.PersistTable.Clear()
                objPage.PersistTable.Add(ddlTransactionDate.ID, CCommon.ToLong(ddlTransactionDate.SelectedValue))

                For Each item As RadComboBoxItem In radTransactionType.Items
                    If item.Checked = True Then
                        strValues = strValues & "," & CCommon.ToLong(item.Value)
                    End If
                Next
                strValues = strValues.Trim(",")
                objPage.PersistTable.Add(radTransactionType.ID, strValues)
                objPage.PersistTable.Save(boolOnlyURL:=True, strParam:="TransactionInfo")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub gvTransaction_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvTransaction.RowDataBound
            Try
                If e.Row.RowType = DataControlRowType.DataRow Then

                    'Dim lblTransactionType As Label = e.Row.FindControl("lblTransactionType")
                    'Dim strval As String = CType(lblTransactionType, Label).Text
                    'Dim TransactionType As String = ViewState("TransactionType")
                    'If TransactionType = strval Then
                    '    lblTransactionType.Visible = False
                    '    lblTransactionType.Text = String.Empty
                    'Else
                    '    TransactionType = strval
                    '    ViewState("TransactionType") = TransactionType
                    '    lblTransactionType.Visible = True
                    '    lblTransactionType.Text = "<br><b>" & TransactionType & "</b><br>"
                    'End If

                    Dim lblBizDocID As New Label
                    lblBizDocID = DirectCast(e.Row.FindControl("lblBizDocID"), Label)
                    If lblBizDocID IsNot Nothing Then
                        Dim strBizDocID As String
                        strBizDocID = CCommon.ToString(DataBinder.Eval(e.Row.DataItem, "vcBizDocID"))
                        strBizDocID = strBizDocID.Trim(",")
                        If String.IsNullOrEmpty(strBizDocID) = False Then
                            lblBizDocID.Text = "(" & strBizDocID & ")"
                        End If
                    End If

                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
        'Added by Sachin Sadhu||Date:3rdFeb2014
        'Purpose:For Sorting  ||Customer:AudioGenex(JJ)
        Private Function GetSortDirection(ByVal column As String, ByVal isSortChanged As Boolean) As String
            ' By default, set the sort direction to ascending.
            Dim sortDirection

            If column = "dtCreatedDate" Then
                sortDirection = "DESC"
            Else
                sortDirection = "ASC"
            End If


            ' Retrieve the last column that was sorted.
            Dim sortExpression = TryCast(ViewState("SortTransExpression"), String)

            If sortExpression IsNot Nothing Then
                Dim lastDirection = TryCast(ViewState("SortTransDirection"), String)
                ' Check if the same column is being sorted.
                ' Otherwise, the default value can be returned.
                If sortExpression = column AndAlso isSortChanged Then
                    If lastDirection IsNot Nothing AndAlso lastDirection = "ASC" Then
                        sortDirection = "DESC"
                    ElseIf lastDirection IsNot Nothing Then
                        sortDirection = "ASC"
                    End If
                Else
                    sortDirection = lastDirection
                End If
            End If

            ' Save new values in ViewState.
            ViewState("SortTransDirection") = sortDirection
            ViewState("SortTransExpression") = column
            'PersistData()
            Return sortDirection

        End Function
        Private Sub gvTransaction_Sorting(ByVal sender As Object, ByVal e As GridViewSortEventArgs) Handles gvTransaction.Sorting
            ViewState("SortTransExpression") = e.SortExpression
            BindDatagrid(e.SortExpression, 1)
        End Sub
        'End of code @Sachin Sadhu

        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPageDeals.Text = bizPager.CurrentPageIndex
                BindDatagrid(CCommon.ToString(ViewState("SortTransExpression")), 0)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub
    End Class
End Namespace
