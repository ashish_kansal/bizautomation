﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmConfigParcelShippingAccount.aspx.vb" Inherits="BACRM.UserInterface.Accounts.frmConfigParcelShippingAccount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #divShipVia ul.nav {
            position: relative;
            display: -ms-flexbox;
            display: -webkit-flex;
            display: -moz-flex;
            display: -ms-flex;
            display: flex;
            border-bottom: 0px;
        }

            #divShipVia ul.nav li {
                border: 1px solid #0073b7;
                -webkit-flex: 1;
                -moz-flex: 1;
                -ms-flex: 1;
                flex: 1;
                text-align: center;
            }

                #divShipVia ul.nav li:not(:last-child) {
                    border-right: none;
                }

                #divShipVia ul.nav li.active {
                    border-top-color: #0073b7;
                    border-bottom: none;
                    background-color: #0073b7;
                }

                 #divShipVia ul.nav li a, #divShipVia ul.nav li a:active, #divShipVia ul.nav li a:focus, #divShipVia ul.nav li a:hover {
                    border: none;
                    color: #3c8dbc;
                    border-radius: 0px;
                    margin: 0px;
                }

                 #divShipVia ul.nav li.active a {
                    background: none;
                    color: #FFF;
                    font-weight:bold;
                }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#divShipVia .nav li").removeClass("active");
            $("#li" + $("[id$=hdnShipViaID]").val()).addClass("active");
        });

        function ShipViaChanged(shipViaID) {
            $("[id$=hdnShipViaID]").val(shipViaID);
            $("#divShipVia .nav li").removeClass("active");
            $("#li" + shipViaID).addClass("active");
            $("[id$=btnGoHid]").click();
        }

        function CloseAndUpdateNewOrder(shipperAccountNo) {
            if (window.opener != null) {
                window.opener.UpdateShipperAccountNo(shipperAccountNo);
            }

            window.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:Button ID="btnClose" runat="server" CssClass="btn btn-primary" OnClientClick="return Close();" Text="Close" />
                <asp:Button ID="btnGoHid" runat="server" style="display:none" OnClick="btnGoHid_Click" />
                <asp:HiddenField ID="hdnShipViaID" runat="server" Value="91" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Configure Parcel Shipping Account
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <div class="row padbottom10" id="divError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>
                <p>
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12" id="divShipVia">
            <ul class="nav nav-tabs">
                <li id="li91"><a href="javascript:ShipViaChanged(91)">Fedex</a></li>
                <li id="li88"><a href="javascript:ShipViaChanged(88)">UPS</a></li>
            </ul>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="form-group">
                <label>Account Number<span style="color:red"> *</span></label>
                <asp:TextBox runat="server" ID="txtAccountNumber" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <label>Street<span style="color:red"> *</span></label>
                <asp:TextBox runat="server" ID="txtStreet" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <label>City<span style="color:red"> *</span></label>
                <asp:TextBox runat="server" ID="txtCity" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="form-group">
                <label>Country<span style="color:red"> *</span></label>
                <asp:DropDownList runat="server" ID="ddlCountry" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">

                </asp:DropDownList>
            </div>
            <div class="form-group">
                <label>State<span style="color:red"> *</span></label>
                <asp:DropDownList runat="server" ID="ddlState" CssClass="form-control">
                    <asp:ListItem Text="-- Select One --" Value="0"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="form-group">
                <label>ZipCode<span style="color:red"> *</span></label>
                <asp:TextBox runat="server" ID="txtZipCode" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="col-xs-12">
            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary btn-block" Text="Save" OnClick="btnSave_Click" />
        </div>
    </div>

</asp:Content>
