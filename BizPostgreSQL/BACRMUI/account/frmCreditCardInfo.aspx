﻿<%@ Page Title="Manage Credit Card" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmCreditCardInfo.aspx.vb" Inherits=".frmCreditCardInfo" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <script type="text/javascript">
  
        function save() {

            var contact = document.getElementById("<%= ddlContact.ClientID%>").selectedIndex;
            if (contact == 0) {
                alert("Select contact");
                document.getElementById("<%= ddlContact.ClientID %>").focus();
                return false;
            }

            var cctype = document.getElementById("<%= ddlcctype.ClientID %>").selectedIndex;
            if (cctype == 0) {
                alert("Select credit card type");
                document.getElementById("<%= ddlcctype.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("txtccno").value == "") {
                alert("Enter Credit card number");
                document.getElementById("<%= txtccno.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("txtccname").value == "") {
                alert("Enter Name");
                document.getElementById("<%= txtccname.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("txtcvv").value == "") {
                alert("Enter CVV/CVV2");
                document.getElementById("<%= txtcvv.ClientID%>").focus();
                return false;
            }
            if (document.getElementById("<%= ddlmonth.ClientID %>").selectedIndex == 0) {
                alert("Enter Expiration Month");
                document.getElementById("<%= ddlmonth.ClientID %>").focus();
                return false;
            }
            if (document.getElementById("<%= ddlyear.ClientID %>").selectedIndex == 0) {
                alert("Enter Expiration Year");
                document.getElementById("<%= ddlyear.ClientID %>").focus();
                return false;
            }

            return true;
        }

        function deletedata() {
            var r = confirm("Are you sure, Do you want to delete it?");
            if (r == true) {
                return true;
            } else {
                return false;
            }
        }

        function closewindow() {
            window.opener.location.href = window.opener.location.href;
            window.close();
            return false;
        }
    </script>
    <style type="text/css">
        tr.rgRow td, tr.rgAltRow td {
            padding: 3px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server">
    <asp:Literal ID="litTermsMessage" runat="server" EnableViewState="False"></asp:Literal>
    <asp:HiddenField ID="hdnCCInfoID" runat="server" />
    <table style="width: 800px">
        <tr style="height: 40px;">
            <td style="font-weight: bold; text-align: right">Primary:</td>
            <td>
                <asp:CheckBox runat="server" ID="chkprimary" /></td>
            <td></td>
            <td style="font-weight: bold; text-align: right">Contact:</td>
            <td>
                <asp:DropDownList ID="ddlContact" runat="server" CssClass="form-control" Width="200" AutoPostBack="true">
                </asp:DropDownList>
            </td>
        </tr>
        <tr style="height: 40px;">
            <td style="font-weight: bold; text-align: right">Credit Card Type:<span style="color:red">*</span></td>
            <td>
                <asp:DropDownList ID="ddlcctype" runat="server" Width="200">
                </asp:DropDownList></td>
            <td></td>
            <td style="font-weight: bold; text-align: right">New Card #:<span style="color:red">*</span></td>
            <td>
                <asp:TextBox ID="txtccno" runat="server" Width="200">
                </asp:TextBox>
                <asp:Label ID="lblccno" runat="server" Visible="false"></asp:Label></td>
        </tr>
        <tr style="height: 40px;">
            <td style="font-weight: bold; text-align: right">Name as it appears on card:<span style="color:red">*</span></td>
            <td>
                <asp:TextBox ID="txtccname" runat="server" Width="200">
                </asp:TextBox></td>
            <td></td>
            <td style="font-weight: bold; text-align: right" id="tdCvv1" runat="server">CVV/CVV2:<span style="color:red">*</span>
            </td>
            <td id="tdCvv2" runat="server">
                <asp:TextBox ID="txtcvv" runat="server" Width="200" TextMode="Password">
                </asp:TextBox>
            </td>
        </tr>
        <tr style="height: 40px;">
            <td style="font-weight: bold; text-align: right">Expiration:<span style="color:red">*</span></td>
            <td>
                <span style="display: inline-block !important; width: 200px!important">

                    <asp:DropDownList ID="ddlmonth" runat="server" Style="width: 60px; display: inline-block !important;">
                        <asp:ListItem Text="Month" Value="0"></asp:ListItem>
                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                        <asp:ListItem Text="6" Value="6"></asp:ListItem>
                        <asp:ListItem Text="7" Value="7"></asp:ListItem>
                        <asp:ListItem Text="8" Value="8"></asp:ListItem>
                        <asp:ListItem Text="9" Value="9"></asp:ListItem>
                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:DropDownList ID="ddlyear" runat="server" Style="width: 60px; display: inline-block !important;">
                        <asp:ListItem Text="Year" Value="0"></asp:ListItem>
                        <asp:ListItem Text="16" Value="16"></asp:ListItem>
                        <asp:ListItem Text="17" Value="17"></asp:ListItem>
                        <asp:ListItem Text="18" Value="18"></asp:ListItem>
                        <asp:ListItem Text="19" Value="19"></asp:ListItem>
                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                        <asp:ListItem Text="21" Value="21"></asp:ListItem>
                        <asp:ListItem Text="22" Value="22"></asp:ListItem>
                        <asp:ListItem Text="23" Value="23"></asp:ListItem>
                        <asp:ListItem Text="24" Value="24"></asp:ListItem>
                        <asp:ListItem Text="25" Value="25"></asp:ListItem>
                        <asp:ListItem Text="26" Value="26"></asp:ListItem>
                        <asp:ListItem Text="27" Value="27"></asp:ListItem>
                        <asp:ListItem Text="28" Value="28"></asp:ListItem>
                        <asp:ListItem Text="29" Value="29"></asp:ListItem>
                        <asp:ListItem Text="30" Value="30"></asp:ListItem>
                        <asp:ListItem Text="31" Value="31"></asp:ListItem>
                        <asp:ListItem Text="32" Value="32"></asp:ListItem>
                        <asp:ListItem Text="33" Value="33"></asp:ListItem>
                        <asp:ListItem Text="34" Value="34"></asp:ListItem>
                        <asp:ListItem Text="35" Value="35"></asp:ListItem>
                        <asp:ListItem Text="36" Value="36"></asp:ListItem>
                        <asp:ListItem Text="37" Value="37"></asp:ListItem>
                        <asp:ListItem Text="38" Value="38"></asp:ListItem>
                        <asp:ListItem Text="39" Value="39"></asp:ListItem>
                        <asp:ListItem Text="40" Value="40"></asp:ListItem>
                    </asp:DropDownList>
                </span>
            </td>
            <td></td>
            <td style="font-weight: bold; text-align: right">Card Category:</td>
            <td>
                <asp:DropDownList ID="ddlcccat" runat="server" CssClass="form-control" Width="200">
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td colspan="5">
                <asp:GridView ID="gvcc" runat="server" AutoGenerateColumns="False" DataKeyNames="numCCInfoID"
                    CssClass="dgNHover" Width="100%" ShowHeaderWhenEmpty="true" EmptyDataText="No Data Found">
                    <HeaderStyle CssClass="rgHeader" Height="30" />
                    <RowStyle CssClass="rgRow" Height="25" />
                    <AlternatingRowStyle CssClass="rgAltRow" Height="25" />
                    <Columns>
                        <asp:TemplateField HeaderText="Card Number">
                            <ItemTemplate>
                                <asp:Label ID="lblccno" runat="server" Text='<%# ccno(decrypt(Eval("vcCreditCardNo")).ToString()) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Name on Card">
                            <ItemTemplate>
                                <asp:Label ID="lblccnm" runat="server" Text='<%# decrypt(Eval("vcCardHolder")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Expiration ">
                            <ItemTemplate>
                                <asp:Label ID="lblccexp" runat="server" Text='<%# Eval("tintValidMonth").ToString() + "/" + Eval("intValidYear").ToString() %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category">
                            <ItemTemplate>
                                <asp:Label ID="lblcccat" runat="server" Text='<%# Eval("vcData").ToString() %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Primary">
                            <ItemTemplate>
                                <asp:Label ID="lblccpri" runat="server" Text='<%#If(Convert.ToInt16(Eval("bitIsDefault")) = 1, "Yes", "No") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="40px">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgbtnedit" runat="server" ImageUrl="~/images/edit.png" CommandArgument='<%# Eval("numCCInfoID") %>' OnClick="imgbtnedit_Click" />
                                <asp:ImageButton ID="imgbtndel" runat="server" ImageUrl="~/images/deleted.gif" CommandArgument='<%# Eval("numCCInfoID") %>' OnClick="imgbtndel_Click" OnClientClick="return deletedata();" />
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ContentPlaceHolderID="PageTitle" runat="server">
    Manage Credit Card
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div style="float: right">
        <asp:Button ID="btnadd" runat="server" CssClass="btn btn-primary" OnClientClick="return save();" Text="Add"></asp:Button>
        <asp:Button ID="btnclose" runat="server" CssClass="btn btn-default" OnClientClick="return closewindow();" Text="Close"></asp:Button>
    </div>
</asp:Content>
