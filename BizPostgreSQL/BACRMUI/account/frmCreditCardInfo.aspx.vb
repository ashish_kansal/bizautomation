﻿Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities

Public Class frmCreditCardInfo
    Inherits BACRMPage

    Dim objOppInvoice As OppInvoice

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If IsPostBack = False Then
                'Load card type
                objCommon.sb_FillComboFromDBwithSel(ddlcctype, 120, Session("DomainID"))
                FillContact()
                fillcreditcategory()
                fillgrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Sub FillContact()
        Try
            Dim fillCombo As New COpportunities
            With fillCombo
                .DivisionID = CCommon.ToLong(GetQueryStringVal("DivisionID"))
                ddlContact.DataSource = fillCombo.ListContact().Tables(0).DefaultView()
                ddlContact.DataTextField = "Name"
                ddlContact.DataValueField = "numcontactId"
                ddlContact.DataBind()
            End With
            ddlContact.Items.Insert(0, New ListItem("-- Select One --", "0"))

            If ddlContact.Items.Count >= 2 Then
                ddlContact.Items(1).Selected = True
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Sub fillcreditcategory()
        Try
            Dim dt As New DataTable
            dt = objCommon.GetMasterListItems(815, Session("DomainID"))
            If dt.Rows.Count > 0 Then
                ddlcccat.DataSource = dt
                ddlcccat.DataTextField = "vcData"
                ddlcccat.DataValueField = "numListItemID"
                ddlcccat.DataBind()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnadd_Click(sender As Object, e As EventArgs) Handles btnadd.Click
        Try

            Dim objEncryption As New QueryStringValues
            objOppInvoice = New OppInvoice
            With objOppInvoice
                .numCCInfoID = CCommon.ToLong(hdnCCInfoID.Value)
                .ContactID = ddlContact.SelectedValue
                .CardHolder = objEncryption.Encrypt(txtccname.Text.Trim())
                .CardTypeID = ddlcctype.SelectedValue
                If txtccno.Text.Contains("*") Then
                    .CreditCardNumber = objEncryption.Encrypt(lblccno.Text.Trim())
                Else
                    .CreditCardNumber = objEncryption.Encrypt(txtccno.Text.Trim())
                End If
                .CVV2 = IIf(txtcvv.Text.Trim() = String.Empty, "", objEncryption.Encrypt(txtcvv.Text.Trim()))
                .ValidMonth = Convert.ToInt16(ddlmonth.SelectedValue)
                .ValidYear = Convert.ToInt16(ddlyear.SelectedValue)
                .UserCntID = Session("UserContactID")
                .IsDefault = chkprimary.Checked
            End With
            objOppInvoice.SaveCustomerCreditCardInfo()
            reset()
            fillgrid()
            btnadd.Text = "Add"
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try

    End Sub

    Public Sub fillgrid()
        Dim dt As New DataTable
        objOppInvoice = New OppInvoice
        dt = objOppInvoice.GetCustomerCCInfo(0, ddlContact.SelectedValue)
        If dt.Rows.Count > 0 Then
            gvcc.DataSource = dt
            gvcc.DataBind()
        Else
            gvcc.DataSource = Nothing
            gvcc.DataBind()
        End If
    End Sub
    Public Function decrypt(ByVal prm As Object) As String
        Dim objEncryption As New QueryStringValues
        If prm.ToString.Trim = String.Empty Then
            Return ""
        Else
            Return objEncryption.Decrypt(prm.ToString.Trim())
        End If
    End Function

    Public Function ccno(ByVal strcard As String) As String

        If strcard.Length > 4 Then
            Return "************" & strcard.Substring(strcard.Length - 4, 4)
        Else
            Return "NA"
        End If
    End Function

    Public Sub reset()
        hdnCCInfoID.Value = ""
        txtccname.Text = ""
        txtccno.Text = ""
        txtcvv.Text = ""
        txtcvv.Attributes("value") = ""
        lblccno.Text = ""
        ddlmonth.SelectedIndex = 0
        ddlyear.SelectedIndex = 0
        fillcreditcategory()
        objCommon.sb_FillComboFromDBwithSel(ddlcctype, 120, Session("DomainID"))
        chkprimary.Checked = False
        ddlcctype.Focus()
    End Sub

    Protected Sub imgbtnedit_Click(sender As Object, e As ImageClickEventArgs)
        Try
            Dim imgbtn = CType(sender, ImageButton)
            getdata(Convert.ToInt32(imgbtn.CommandArgument.ToString))
            btnadd.Text = "Update"
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Sub getdata(ByVal confid As Integer)
        Try
            Dim dt As New DataTable
            Dim strcard As String
            objOppInvoice = New OppInvoice
            dt = objOppInvoice.GetCustomerCCInfo(confid, 0)
            If dt.Rows.Count > 0 Then
                hdnCCInfoID.Value = confid
                ddlContact.SelectedValue = CCommon.ToLong(dt.Rows(0)("numContactID"))
                ddlcctype.SelectedValue = dt.Rows(0)("numCardTypeID").ToString
                ddlmonth.SelectedValue = dt.Rows(0)("tintValidMonth").ToString
                ddlyear.SelectedValue = dt.Rows(0)("intValidYear").ToString
                chkprimary.Checked = CCommon.ToBool(dt.Rows(0)("bitIsDefault"))
                txtccname.Text = decrypt(dt.Rows(0)("vcCardHolder"))
                strcard = decrypt(dt.Rows(0)("vcCreditCardNo"))
                lblccno.Text = strcard
                If strcard.Length > 4 Then
                    txtccno.Text = "************" & strcard.Substring(strcard.Length - 4, 4)
                End If

                txtcvv.Attributes("value") = decrypt(dt.Rows(0)("vcCVV2"))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub imgbtndel_Click(sender As Object, e As ImageClickEventArgs)
        Try
            Dim imgbtn = CType(sender, ImageButton)
            objOppInvoice = New OppInvoice
            Dim flg = objOppInvoice.DeleteCustomerCCInfo(Convert.ToInt32(imgbtn.CommandArgument.ToString), ddlContact.SelectedValue)
            If flg > 0 Then
                reset()
                fillgrid()
                btnadd.Text = "Add"
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub ddlContact_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlContact.SelectedIndexChanged
        Try
            fillgrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class