﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master"
    CodeBehind="frmCreditBalance.aspx.vb" Inherits=".frmCreditBalance" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script type="text/javascript">
        function OpenOpp(a) {
            if (a.length > 0) {
                var str;
                str = "../opportunity/frmOpportunities.aspx?frm=SalesReturn&OpID=" + a;
                window.opener.location.href = str;
            }
        }

        function OpenBizInvoice(a, b) {
            if (a.length > 0 && b.length > 0) {
                window.open('../opportunity/frmBizInvoice.aspx?OpID=' + a + '&OppBizId=' + b, '', 'toolbar=no,menubar=yes,titlebar=no,left=100,top=15,width=680,height=700,scrollbars=yes,resizable=yes');
            }
        }

        function OpenItem(a) {
            window.opener.location.href = '../Items/frmKitDetails.aspx?ItemCode=' + a;
        }

        function SubmitForPayment(a, b, c) {
            var str;
            str = "../opportunity/frmSubmitReturnForPayment.aspx?OpID=" + a + "&ReturnID=" + b + "&type=" + c;;
            window.open(str, '', 'toolbar=no,titlebar=no,left=300,top=200,scrollbars=yes,resizable=yes');
        }
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnClose" runat="server" CssClass="button" OnClientClick="window.close()"
                Text="Close" CausesValidation="false" />&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Credit Balance History
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <table cellpadding="0" cellspacing="0">
        <tr valign="top">
            <td>
                <asp:GridView ID="gvCreditBalance" runat="server" AutoGenerateColumns="False" CssClass="tbl"
                    Width="100%" AllowSorting="false" DataKeyNames="numreturnid,numOppId">
                    <Columns>
                        <asp:TemplateField HeaderText="Type" SortExpression="">
                            <ItemTemplate>
                                <%# Eval("Type") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Return ID" SortExpression="">
                            <ItemTemplate>
                                <a href="javascript:SubmitForPayment('<%# Eval("numoppid") %>','<%# Eval("numreturnid") %>','<%# Eval("tintOpptype") %>')">
                                    <%# Eval("numreturnid") %></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Order/Memo" SortExpression="">
                            <ItemTemplate>
                                <%--<asp:HyperLink ID="hplOrderBizDoc" runat="server" CssClass="hyperlink" NavigateUrl=""></asp:HyperLink>--%>
                                <a href="javascript:OpenOpp('<%# Eval("numOppId") %>')">
                                    <%# Eval("vcPOppname")%>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="BizDoc/Ref#" SortExpression="">
                            <ItemTemplate>
                                <%--<asp:HyperLink ID="hplOrderBizDoc" runat="server" CssClass="hyperlink" NavigateUrl=""></asp:HyperLink>--%>
                                <a href="javascript:OpenBizInvoice('<%# Eval("numOppId") %>','<%# Eval("numOppBizDocsId") %>')">
                                    <%# Eval("vcBizDocID")%>
                                </a>
                            </ItemTemplate>

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Item" SortExpression="">
                            <ItemTemplate>
                                <a href="javascript:OpenItem('<%# Eval("numItemCode") %>')">
                                    <%# Eval("vcItemName")%>
                                </a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Credit" SortExpression="">
                            <ItemTemplate>
                                <%# String.Format("{0:#,##0.00}", Eval("monAmount"))%>
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <EmptyDataTemplate>
                        No records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
