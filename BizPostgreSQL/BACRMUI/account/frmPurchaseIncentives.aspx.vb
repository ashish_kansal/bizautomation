﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Prospects

Namespace BACRM.UserInterface.Accounts
    Public Class frmPurchaseIncentives
        Inherits BACRMPage
        Dim lngDivId As Long
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                lngDivId = CCommon.ToLong(GetQueryStringVal("DivId"))
                If Not IsPostBack Then
                    BindIncentiveGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub DisplayError(ByVal errorMessage As String)
            Try
                lblError.Text = errorMessage
                divError.Style.Add("display", "")
            Catch ex As Exception
                'DO NOT THROW ERROR
            End Try
        End Sub

        Sub BindIncentiveGrid()
            Try
                Dim objProspects As CProspects = New CProspects
                Dim dtComInfo As DataTable
                objProspects.DivisionID = lngDivId
                objProspects.DomainID = Session("DomainID")
                objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtComInfo = objProspects.GetCompanyPurchaseIncentives
                Dim MaxRecord As Long = 5
                If dtComInfo.Rows.Count < 5 Then
                    MaxRecord = MaxRecord - dtComInfo.Rows.Count
                    For value As Integer = 0 To MaxRecord + 3
                        Dim dr = dtComInfo.NewRow
                        dr("IsIncentives") = False
                        dr("intType") = 1
                        dtComInfo.Rows.Add(dr)
                        value = value + 1

                    Next
                End If

                grdIncentives.DataSource = dtComInfo
                grdIncentives.DataBind()
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Protected Sub btnClose_Click(sender As Object, e As EventArgs)
            Try
                Dim dtPurchaseIncentiveFields As New DataTable
                CCommon.AddColumnsToDataTable(dtPurchaseIncentiveFields, "BuyIncentives,IncentivesType,IncentiveDesc,DiscountType")
                dtPurchaseIncentiveFields.TableName = "PurchaseIncentives"
                For Each gvRow In grdIncentives.Rows
                    Dim chk As CheckBox
                    chk = CType(gvRow.FindControl("chkIncentives"), CheckBox)
                    If chk.Checked = True Then
                        Dim txtBuyIncentives As TextBox = CType(gvRow.FindControl("txtBuyIncentives"), TextBox)
                        Dim ddlIncentives As DropDownList = CType(gvRow.FindControl("ddlIncentives"), DropDownList)
                        Dim txtIncentiveDesc As TextBox = CType(gvRow.FindControl("txtIncentiveDesc"), TextBox)
                        Dim ddlDiscountType As DropDownList = CType(gvRow.FindControl("ddlDiscountType"), DropDownList)
                        Dim dr As DataRow = dtPurchaseIncentiveFields.NewRow
                        dr("BuyIncentives") = txtBuyIncentives.Text
                        dr("IncentivesType") = ddlIncentives.SelectedValue
                        dr("IncentiveDesc") = txtIncentiveDesc.Text
                        dr("DiscountType") = ddlDiscountType.SelectedValue
                        dtPurchaseIncentiveFields.Rows.Add(dr)
                    End If
                Next
                Dim ds As New DataSet
                ds.Tables.Add(dtPurchaseIncentiveFields)
                Dim strData As String
                strData = ds.GetXml()
                Dim objProspects As CProspects = New CProspects
                objProspects.DivisionID = lngDivId
                objProspects.DomainID = Session("DomainID")
                objProspects.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objProspects.strCompanyTaxTypes = strData
                objProspects.ManageCompanyPurchaseIncentives()
                ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "Alert", "window.opener.location.reload(true);window.close();", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub grdIncentives_RowDataBound(sender As Object, e As GridViewRowEventArgs)
            Try
                If e.Row.RowType = DataControlRowType.DataRow Then
                    Dim ddlIncentives As DropDownList = CType(e.Row.FindControl("ddlIncentives"), DropDownList)
                    Dim ddlDiscountType As DropDownList = CType(e.Row.FindControl("ddlDiscountType"), DropDownList)
                    Dim drv As DataRowView = DirectCast(e.Row.DataItem, DataRowView)

                    If Not ddlIncentives.Items.FindByValue(CCommon.ToInteger(drv("intType"))) Is Nothing Then
                        ddlIncentives.Items.FindByValue(CCommon.ToInteger(drv("intType"))).Selected = True
                    End If

                    If Not ddlDiscountType.Items.FindByValue(CCommon.ToInteger(drv("tintDiscountType"))) Is Nothing Then
                        ddlDiscountType.Items.FindByValue(CCommon.ToInteger(drv("tintDiscountType"))).Selected = True
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
    End Class
End Namespace