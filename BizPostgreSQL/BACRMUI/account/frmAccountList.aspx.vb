Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports Telerik.Web.UI


Namespace BACRM.UserInterface.Accounts
    Public Class frmAccountList
        Inherits BACRMPage

        Dim RegularSearch As String
        Dim CustomSearch As String
        Dim m_aryRightsForViewGridConfiguration() As Integer
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                ' Checking Rights for View
                GetUserRightsForPage(MODULEID.Accounts, 2)

                btnDelete.Attributes.Add("onclick", "return DeleteRecord()")
                If Not IsPostBack Then
                    DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                    DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                    ' Checking rights for Import from outlook
                    'm_aryRightsForOulook = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmAccountList.aspx", Session("userID"), 4, 3)
                    'If m_aryRightsForOulook(RIGHTSTYPE.VIEW) = 0 Then
                    '    '  btnImport.Visible = False
                    'End If
                    Dim dtTab As DataTable
                    dtTab = Session("DefaultTab")
                    If dtTab.Rows.Count > 0 Then
                        lbAccount.Text = IIf(IsDBNull(dtTab.Rows(0).Item("vcAccount")), "Accounts", dtTab.Rows(0).Item("vcAccount").ToString & "s")
                    Else
                        lbAccount.Text = "Accounts"
                    End If

                    If GetQueryStringVal("FilterId") <> "" Then
                        If Not ddlSort.Items.FindByValue(GetQueryStringVal("FilterId")) Is Nothing Then
                            ddlSort.ClearSelection()
                            ddlSort.Items.FindByValue(GetQueryStringVal("FilterId")).Selected = True
                        End If
                    End If

                    'Check if user has rights to edit grid configuration
                    m_aryRightsForViewGridConfiguration = GetUserRightsForPage_Other(4, 17)
                    If m_aryRightsForViewGridConfiguration(RIGHTSTYPE.VIEW) = 0 Then
                        Dim tdGridConfiguration As HtmlAnchor = CType(CCommon.FindControlRecursive(Page.Master, "tdGridConfiguration"), HtmlAnchor)

                        If Not tdGridConfiguration Is Nothing Then
                            tdGridConfiguration.Visible = False
                        End If
                    End If

                    'If Not (Session("SAccounts") Is Nothing) Then
                    '    ddlSort.SelectedIndex = Session("SAccounts")
                    'Else
                    '    SetDefaultFilter()
                    'End If
                    'Session("SAccounts") = ddlSort.SelectedIndex

                End If
                If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then
                    btnDelete.Visible = False

                End If
                '                btnDelete.Visible = False
                '                lnkDelete.Visible = True
                '                lnkDelete.Attributes.Add("onclick", "return DeleteMessage()")

                If Not IsPostBack Then

                    'If Session("List") <> "Accounts" Then
                    '    Session("ListDetails") = Nothing
                    '    txtCurrrentPage.Text = 1
                    '    Session("Asc") = 1
                    'Else
                    'Dim str As String()
                    'str = Session("ListDetails").split(",")
                    'txtSortColumn.Text = str(5)
                    'ViewState("SortChar") = str(0)
                    'txtCustomer.Text = str(3)
                    'txtFirstName.Text = str(1)
                    'txtLastName.Text = str(2)
                    'txtCurrrentPage.Text = str(4)
                    'Session("Asc") = str(6)
                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                        txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                        txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                        txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))
                        ddlSort.ClearSelection()
                        If Not ddlSort.Items.FindByValue(CCommon.ToString(PersistTable(PersistKey.FilterBy))) Is Nothing Then ddlSort.Items.FindByValue(CCommon.ToString(PersistTable(PersistKey.FilterBy))).Selected = True
                        txtGridColumnFilter.Text = CCommon.ToString(PersistTable(PersistKey.GridColumnSearch))
                    End If

                    'End If
                    'Session("List") = "Accounts"
                    BindDatagrid()
                End If
                Dim m_aryRightsForSendAnnounceMent() As Integer = GetUserRightsForPage_Other(43, 137)

                If m_aryRightsForSendAnnounceMent(RIGHTSTYPE.VIEW) <> 0 Then
                    btnSendAnnounceMent.Visible = True
                End If
                Dim m_aryRightsForAddActionItem() As Integer = GetUserRightsForPage_Other(44, 143)

                If m_aryRightsForAddActionItem(RIGHTSTYPE.VIEW) <> 0 Then
                    btnAddActionItem.Visible = True
                End If
                If Page.IsPostBack Then

                    BindDatagrid()
                End If
                '   btnImport.Attributes.Add("onclick", "return GoImport()")
                'ClientScript.RegisterClientScriptBlock(Me.GetType, "SetTab", "if (parent.parent.frames.length > 0) { parent.parent.SelectTabByValue('7');}else{ parent.SelectTabByValue('7'); } ", True)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub


        Private Function GetCheckedValues() As Boolean
            Try
                Session("ContIDs") = ""
                Dim isCheckboxchecked As Boolean = False
                Dim gvRow As GridViewRow
                For Each gvRow In gvSearch.Rows
                    If CType(gvRow.FindControl("chkSelect"), CheckBox).Checked = True Then
                        Session("ContIDs") = Session("ContIDs") & CType(gvRow.FindControl("lblContactId"), Label).Text & ","
                        isCheckboxchecked = True
                    End If
                Next
                Session("ContIDs") = Session("ContIDs").ToString.TrimEnd(",")
                Return isCheckboxchecked
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub btnAddActionItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddActionItem.Click
            Try
                Dim isCheckboxChecked As Boolean
                isCheckboxChecked = GetCheckedValues()
                If isCheckboxChecked Then
                    Response.Redirect("../Admin/ActionItemDetailsOld.aspx?selectedContactId=1", False)
                Else
                    DisplayError("Select atleast one checkbox For Action Item.")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Private Sub btnSendAnnounceMent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSendAnnounceMent.Click
            Try
                Dim isCheckboxChecked As Boolean
                isCheckboxChecked = GetCheckedValues()
                If isCheckboxChecked Then
                    Session("EMailCampCondition") = Session("WhereCondition")
                    Response.Redirect("../Marketing/frmEmailBroadCasting.aspx?PC=true&SearchID=0&ID=0&SAll=true", False)
                Else
                    DisplayError("Select atleast one checkbox to broadcast E-mail.")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Sub BindDatagrid(Optional ByVal CreateCol As Boolean = True)
            Try
                Dim dtAccounts As DataTable
                Dim objAccounts As New CAccounts


                With objAccounts
                    .CRMType = 2
                    .UserCntID = Session("UserContactID")
                    .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                    .SortOrder = ddlSort.SelectedItem.Value
                    '.FirstName = txtFirstName.Text.Trim()
                    '.LastName = txtLastName.Text.Trim()
                    .SortCharacter = txtSortChar.Text.Trim()
                    '.CustName = txtCustomer.Text.Trim()
                    .DomainID = Session("DomainID")

                    If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                    .CurrentPage = txtCurrrentPage.Text.Trim()
                    .PageSize = Session("PagingRows")
                    .TotalRecords = 0
                    .bitPartner = False
                    .Profile = CCommon.ToLong(GetQueryStringVal("numProfile"))
                    If txtSortColumn.Text <> "" Then
                        .columnName = txtSortColumn.Text
                    Else
                        .columnName = "DM.bintcreateddate"
                    End If
                    If txtSortOrder.Text = "D" Then
                        .columnSortOrder = "Desc"
                    Else : .columnSortOrder = "Asc"
                    End If

                    .ActiveInActive = radActive.SelectedValue

                    GridColumnSearchCriteria()

                    .RegularSearchCriteria = RegularSearch
                    .CustomSearchCriteria = CustomSearch
                    .SearchTxt = radCmbSearch.Text
                End With

                'Session("ListDetails") = ListDetails
                ''Session("ListDetails") = SortChar & "," & txtFirstName.Text & "," & txtLastName.Text & "," & txtCustomer.Text & "," & txtCurrrentPage.Text & "," & txtSortColumn.Text & "," & Session("Asc")
                objAccounts.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                Dim dsList As DataSet
                dsList = objAccounts.GetAccounts
                dtAccounts = dsList.Tables(0)
                Dim dtTableInfo As DataTable
                dtTableInfo = dsList.Tables(1)

                bizPager.PageSize = Session("PagingRows")
                bizPager.RecordCount = objAccounts.TotalRecords
                bizPager.CurrentPageIndex = txtCurrrentPage.Text

                'Persist Form Settings
                PersistTable.Clear()
                PersistTable.Add(PersistKey.CurrentPage, IIf(dtAccounts.Rows.Count > 0, txtCurrrentPage.Text, "1"))
                PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())
                PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
                PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text.Trim())
                PersistTable.Add(PersistKey.FilterBy, ddlSort.SelectedValue)
                PersistTable.Add(PersistKey.GridColumnSearch, txtGridColumnFilter.Text)
                PersistTable.Save()

                Dim m_aryRightsForInlineEdit() As Integer = GetUserRightsForPage_Other(4, 4)
                Dim i As Integer
                For i = 0 To dtAccounts.Columns.Count - 1
                    dtAccounts.Columns(i).ColumnName = dtAccounts.Columns(i).ColumnName.Replace(".", "")
                Next

                Dim htGridColumnSearch As New Hashtable

                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")
                    Dim strIDValue() As String

                    For i = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")

                        htGridColumnSearch.Add(strIDValue(0), strIDValue(1))
                    Next
                End If

                If CreateCol = True Then
                    Dim bField As BoundField
                    Dim Tfield As TemplateField

                    gvSearch.Columns.Clear()

                    For Each drRow As DataRow In dtTableInfo.Rows
                        If drRow("vcDbColumnName") = "numAssignedTo" Then
                            Dim m_aryRightsForAssignedTo() As Integer = GetUserRightsForPage_Other(4, 128)
                            If m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 3 Then
                                drRow("bitAllowEdit") = True
                            Else
                                drRow("bitAllowEdit") = False
                            End If
                        ElseIf drRow("vcDbColumnName") = "numBillState" Then
                            drRow("vcAssociatedControlType") = "TextBox"
                        ElseIf drRow("vcDbColumnName") = "numShipState" Then
                            drRow("vcAssociatedControlType") = "TextBox"
                        End If

                        Tfield = New TemplateField

                        Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, drRow, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 36, objAccounts.columnName, objAccounts.columnSortOrder)

                        Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, drRow, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 36, objAccounts.columnName, objAccounts.columnSortOrder)
                        gvSearch.Columns.Add(Tfield)
                    Next

                    Dim dr As DataRow
                    dr = dtTableInfo.NewRow()
                    dr("vcAssociatedControlType") = "DeleteCheckBox"
                    dr("intColumnWidth") = "30"

                    Tfield = New TemplateField
                    Tfield.HeaderTemplate = New GridTemplate(ListItemType.Header, dr, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 36)
                    Tfield.ItemTemplate = New GridTemplate(ListItemType.Item, dr, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch, 36)
                    gvSearch.Columns.Add(Tfield)
                End If
                InitializeSearchClientSideTemplate(dsList.Tables(1).AsEnumerable().Take(3).CopyToDataTable())
                gvSearch.DataSource = dtAccounts
                gvSearch.DataBind()

                Dim objPageControls As New PageControls
                Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtTableInfo)
                ClientScript.RegisterClientScriptBlock(Me.GetType, "InlineEditValidation", strValidation, True)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Private Sub InitializeSearchClientSideTemplate(dt As DataTable)
            Try
                'objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                'objCommon.UserCntID = CCommon.ToLong(Session("UserContactID"))
                'Dim ds As DataSet = objCommon.SearchOrders("", ddlSort.SelectedValue, IIf(ddlSort.SelectedValue = "1", m_aryRightsForPage(RIGHTSTYPE.VIEW), m_aryRightsForPage(RIGHTSTYPE.VIEW)), 0, 2)

                'If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                '    Dim dtTable As New DataTable
                '    dtTable = ds.Tables(0)

                'genrate header template dynamically
                radCmbSearch.HeaderTemplate = New SearchTemplate(ListItemType.Header, dt, DropDownWidth:=radCmbSearch.DropDownWidth.Value)

                'generate Clientside Template dynamically
                radCmbSearch.ItemTemplate = New SearchTemplate(ListItemType.Item, dt, DropDownWidth:=radCmbSearch.DropDownWidth.Value)
                'End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub
        Private Sub btnGotoRecord_Click(sender As Object, e As EventArgs) Handles btnGotoRecord.Click
            Try
                Dim profileid As Int32
                Dim data As String()
                data = radCmbSearch.SelectedValue.Split("/")
                profileid = 0
                If data(1) = "0" Then
                    Response.Redirect("~/Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=accountlistDivID=" & data(0) & "&profileid=" & profileid, False)
                ElseIf data(1) = "1" Then
                    Response.Redirect("~/prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=accountlist&DivID=" & data(0) & "&profileid=" & profileid, False)
                ElseIf data(1) = "2" Then
                    Response.Redirect("~/account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=accountlist&klds+7kldf=fjk-las&DivId=" & data(0) & "&profileid=" & profileid, False)
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

#Region "RadComboBox Template"
        Public Class SearchTemplate
            Implements ITemplate

            Dim ItemTemplateType As ListItemType
            Dim dtTable1 As DataTable
            Dim i As Integer = 0
            Dim _DropDownWidth As Double

            Sub New(ByVal type As ListItemType, ByVal dtTable As DataTable, Optional ByVal DropDownWidth As Double = 600)
                Try
                    ItemTemplateType = type
                    dtTable1 = dtTable
                    _DropDownWidth = DropDownWidth
                Catch ex As Exception
                    Throw ex
                End Try
            End Sub

            Public Sub InstantiateIn(ByVal container As Control) Implements ITemplate.InstantiateIn
                Try


                    i = 0
                    Dim label1 As Label
                    Dim label2 As Label
                    Dim img As System.Web.UI.WebControls.Image
                    Dim table1 As New HtmlTable
                    Dim tblCell As HtmlTableCell
                    Dim tblRow As HtmlTableRow
                    table1.Width = "100%"
                    Dim ul As New HtmlGenericControl("ul")

                    Select Case ItemTemplateType
                        Case ListItemType.Header
                            For Each dr As DataRow In dtTable1.Rows
                                Dim li As New HtmlGenericControl("li")
                                li.Style.Add("width", Unit.Pixel(CCommon.ToInteger((_DropDownWidth - 50) / dtTable1.Rows.Count)).Value & "px")
                                li.Style.Add("float", "left")
                                li.Style.Add("text-align", "center")
                                li.Style.Add("font-weight", "bold")
                                li.InnerText = dr("vcFieldName").ToString
                                ul.Controls.Add(li)
                            Next

                            container.Controls.Add(ul)
                        Case ListItemType.Item
                            For Each dr As DataRow In dtTable1.Rows
                                Dim li As New HtmlGenericControl("li")
                                Dim width As Integer = CCommon.ToInteger((_DropDownWidth - 50) / dtTable1.Rows.Count)
                                li.Style.Add("width", Unit.Pixel(width).Value & "px")
                                li.Style.Add("float", "left")

                                If dr("bitCustomField") = "1" Then
                                    label2 = New Label
                                    label2.CssClass = "normal1"
                                    label2.Attributes.Add("style", "display:inline-block;width:" & width)
                                    AddHandler label2.DataBinding, AddressOf label2_DataBinding
                                    li.Controls.Add(label2)
                                Else
                                    label1 = New Label
                                    label1.CssClass = "normal1"
                                    label1.Attributes.Add("style", "display:inline-block;width:" & width)
                                    AddHandler label1.DataBinding, AddressOf label1_DataBinding
                                    li.Controls.Add(label1)
                                End If

                                ul.Controls.Add(li)
                            Next

                            container.Controls.Add(ul)
                    End Select
                Catch ex As Exception
                    Throw
                End Try
            End Sub

            Private Sub label1_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
                Dim target As Label = CType(sender, Label)
                Dim item As RadComboBoxItem = CType(target.NamingContainer, RadComboBoxItem)
                Dim itemText As String = Convert.ToString(DirectCast(item.DataItem, DataRow)(dtTable1.Rows(i).Item("vcDbColumnName") & "~" & dtTable1.Rows(i).Item("numFieldId") & "~" & CCommon.ToInteger(dtTable1.Rows(i).Item("bitCustomField"))))
                'Dim itemText As String = CType(DataBinder.Eval(item.DataItem, dtTable1.Rows(i).Item("vcDbColumnName").ToString()), String)
                target.Text = itemText
                i = i + 1
            End Sub

            Private Sub label2_DataBinding(ByVal sender As Object, ByVal e As EventArgs)
                Dim target As Label = CType(sender, Label)
                Dim item As RadComboBoxItem = CType(target.NamingContainer, RadComboBoxItem)
                Dim itemText As String = Convert.ToString(DirectCast(item.DataItem, DataRow)(dtTable1.Rows(i).Item("vcDbColumnName") & "~" & dtTable1.Rows(i).Item("numFieldId") & "~" & CCommon.ToInteger(dtTable1.Rows(i).Item("bitCustomField"))))
                'Dim itemText As String = CType(DataBinder.Eval(item.DataItem, dtTable1.Rows(i).Item("vcFieldName").ToString()), String)
                target.Text = itemText
                i = i + 1
            End Sub

        End Class
#End Region
        Protected Sub radCmbSearch_ItemsRequested(sender As Object, e As Telerik.Web.UI.RadComboBoxItemsRequestedEventArgs)
            Try
                If e.Text.Trim().Length > 0 Then
                    Dim dtAccounts As DataTable
                    Dim objAccounts As New CAccounts
                    Dim searchCriteria As String
                    Dim itemOffset As Integer = e.NumberOfItems

                    objCommon.DomainID = CCommon.ToLong(Session("DomainID"))
                    objCommon.UserCntID = CCommon.ToLong(Session("UserContactID"))


                    With objAccounts
                        .CRMType = 2
                        .UserCntID = Session("UserContactID")
                        .UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                        .SortOrder = ddlSort.SelectedItem.Value
                        '.FirstName = txtFirstName.Text.Trim()
                        '.LastName = txtLastName.Text.Trim()
                        .SortCharacter = txtSortChar.Text.Trim()
                        '.CustName = txtCustomer.Text.Trim()
                        .DomainID = Session("DomainID")

                        If txtCurrrentPage.Text.Trim = "" Then txtCurrrentPage.Text = 1
                        .CurrentPage = txtCurrrentPage.Text.Trim()
                        .PageSize = Session("PagingRows")
                        .TotalRecords = 0
                        .bitPartner = False
                        If txtSortColumn.Text <> "" Then
                            .columnName = txtSortColumn.Text
                        Else
                            .columnName = "DM.bintcreateddate"
                        End If
                        If txtSortOrder.Text = "D" Then
                            .columnSortOrder = "Desc"
                        Else : .columnSortOrder = "Asc"
                        End If

                        .ActiveInActive = radActive.SelectedValue

                        GridColumnSearchCriteria()

                        .RegularSearchCriteria = RegularSearch
                        .CustomSearchCriteria = CustomSearch
                        .SearchTxt = e.Text.Trim
                    End With

                    'btnDelete.Visible = IIf(ddlProjectStatus.SelectedValue = 27492, False, True)

                    Dim dsList As DataSet

                    dsList = objAccounts.GetAccounts
                    dtAccounts = dsList.Tables(0)

                    InitializeSearchClientSideTemplate(dsList.Tables(1).AsEnumerable().Take(3).CopyToDataTable())

                    If Not dsList Is Nothing AndAlso dsList.Tables.Count > 0 Then
                        Dim endOffset As Integer

                        endOffset = Math.Min(itemOffset + radCmbSearch.ItemsPerRequest, dtAccounts.Rows.Count)
                        e.EndOfItems = endOffset = dtAccounts.Rows.Count
                        e.Message = IIf(dtAccounts.Rows.Count <= 0, "No matches", String.Format("Accounts <b>1</b>-<b>{0}</b> out of <b>{1}</b>", endOffset, dtAccounts.Rows.Count))

                        For Each dr As DataRow In dtAccounts.Rows
                            Dim item As New RadComboBoxItem()
                            item.Text = DirectCast(dr("vcGivenName"), String)
                            item.Value = dr("numDivisionID").ToString() & "/" & dr("tintCRMType").ToString()
                            item.Attributes.Add("orderType", ddlSort.SelectedValue)

                            item.DataItem = dr
                            radCmbSearch.Items.Add(item)
                            item.DataBind()
                        Next
                    End If
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                e.Message = "<b style=""color:Red"">Error occured while searching.</b>"
            End Try
        End Sub
        Private Sub btnGo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlSort_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlSort.SelectedIndexChanged
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
                ' Session("SAccounts") = ddlSort.SelectedIndex
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnGo1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo1.Click
            Try
                radCmbSearch.ClearSelection()
                radCmbSearch.Text = ""
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
            Try
                Dim strDivid As String() = txtDelContactIds.Text.Split(",")
                Dim i As Int16 = 0
                Dim lngDivID As Long
                Dim lngRecOwnID As Long
                Dim lngTerrID As Long
                Dim objAccount As New CAccounts
                For i = 0 To strDivid.Length - 1
                    lngDivID = strDivid(i).Split("~")(0)
                    lngRecOwnID = strDivid(i).Split("~")(1)
                    lngTerrID = strDivid(i).Split("~")(2)
                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 1 Then
                        Try
                            If lngRecOwnID = Session("UserContactID") Then
                                DeleteOrganization(lngDivID, objAccount)
                            End If
                        Catch ex As Exception
                            litMessage.Text = "Some Companies cannot be deleted."
                        End Try
                    ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 2 Then
                        Try
                            Dim j As Integer
                            Dim dtTerritory As New DataTable
                            dtTerritory = Session("UserTerritory")
                            Dim chkDelete As Boolean = False
                            If lngTerrID = 0 Then
                                chkDelete = False
                            Else

                                For j = 0 To dtTerritory.Rows.Count - 1
                                    If lngTerrID = dtTerritory.Rows(j).Item("numTerritoryId") Then
                                        chkDelete = True
                                    End If
                                Next

                            End If
                            If chkDelete = True Then
                                DeleteOrganization(lngDivID, objAccount)
                            End If
                        Catch ex As Exception
                            litMessage.Text = "Some Companies cannot be deleted."
                        End Try
                    ElseIf m_aryRightsForPage(RIGHTSTYPE.DELETE) = 3 Then
                        Try
                            DeleteOrganization(lngDivID, objAccount)
                        Catch ex As Exception
                            litMessage.Text = "Some Companies cannot be deleted."
                        End Try
                    End If
                Next
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DeleteOrganization(ByVal lngDivID As Long, ByVal objAccount As CAccounts)
            Try
                If ddlSort.SelectedItem.Value = 9 Then
                    Dim objContacts As New CContacts
                    objContacts.byteMode = 1
                    objContacts.UserCntID = Session("UserContactID")
                    objContacts.ContactID = lngDivID '' passing division id instead of contact if for deleting compnay from favorites
                    objContacts.ManageFavorites()
                    litMessage.Text = "Deleted from Favorites"
                Else
                    If lngDivID <> Session("UserDivisionID") Then
                        objAccount.DivisionID = lngDivID
                        objAccount.DomainID = Session("DomainID")
                        If objAccount.DeleteOrg().Length > 2 Then litMessage.Text = "Some Companies cannot be deleted."
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub radActive_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles radActive.SelectedIndexChanged
            Try
                txtCurrrentPage.Text = 1
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub GridColumnSearchCriteria()
            Try
                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")

                    Dim strIDValue(), strID(), strCustom As String
                    Dim strRegularCondition As New ArrayList
                    Dim strCustomCondition As New ArrayList


                    For i As Integer = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")
                        strID = strIDValue(0).Split("~")

                        If strID(0).Contains("CFW.Cust") Then
                            Select Case strID(3).Trim()
                                Case "TextBox", "TextArea"
                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & " ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                Case "CheckBox"
                                    If strIDValue(1).ToLower() = "yes" Then
                                        strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and COALESCE(CFW.Fld_Value,'0') " & "='1')")
                                    ElseIf strIDValue(1).ToLower() = "no" Then
                                        strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND 1 = (CASE WHEN (SELECT COUNT(*) FROM CFW_Fld_Values CFWInner WHERE CFWInner.RecId=DivisionMaster.numDivisionID AND CFWInner.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND CFWInner.Fld_Value='1') > 0 THEN 0 ELSE 1 END))")
                                    End If
                                Case "SelectBox"
                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & "=" & strIDValue(1) & ")")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " >= '" & fromDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " <= '" & toDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    End If
                                Case "CheckBoxList"
                                    Dim items As String() = strIDValue(1).Split(",")
                                    Dim searchString As String = ""

                                    For Each item As String In items
                                        searchString = searchString & If(searchString.Length > 0, " OR ", "") & " fn_GetCustFldStringValue(" & strID(0).Replace("CFW.Cust", "") & ",DivisionMaster.numDivisionID,CFW.Fld_Value) " & " ilike '%" & item.Replace("'", "''") & "%'"
                                    Next

                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND (" & searchString & "))")
                                Case Else
                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & strCustom & ")")
                            End Select
                        Else
                            Select Case strID(3).Trim()
                                Case "Label"
                                    If strID(0) = "cmp.vcPerformance" Then
                                        strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                                    End If
                                Case "Website", "Email", "TextBox"
                                    If strID(0) = "AD.numShipState" Then
                                        strRegularCondition.Add("ShipState.vcState ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                    ElseIf strID(0) = "ADC.vcCompactContactDetails" Then
                                        strRegularCondition.Add("(ADC.vcFirstName ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR ADC.vcLastName ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR ADC.numPhone ilike '%" & strIDValue(1).Replace("'", "''") & "%' OR ADC.numPhoneExtension ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                    ElseIf strID(0) = "AD.numBillState" Then
                                        strRegularCondition.Add("BillState.vcState ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                    Else
                                        strRegularCondition.Add(strID(0) & " ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                    End If
                                Case "SelectBox"
                                    If strID(0).Contains("numFollowUpStatus") Then
                                        strRegularCondition.Add(strID(0) & " IN(" & strIDValue(1) & ")")
                                    Else
                                        strRegularCondition.Add(strID(0) & "=" & strIDValue(1))
                                    End If
                                Case "TextArea"
                                    strRegularCondition.Add(" Cast(" & strID(0) & " as varchar(5000)) ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strRegularCondition.Add(strID(0) & " >= '" & fromDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strRegularCondition.Add(strID(0) & " <= '" & toDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    End If
                                Case "CheckBox"
                                    If strID(0).Contains("bitEcommerceAccess") Then
                                        If strIDValue(1).ToLower() = "yes" Or strIDValue(1).ToLower() = "true" Then
                                            strRegularCondition.Add("1 = (CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) >0 THEN 1 ELSE 0 END)")
                                        ElseIf strIDValue(1).ToLower() = "no" Or strIDValue(1).ToLower() = "false" Then
                                            strRegularCondition.Add("1 = (CASE WHEN (SELECt COUNT(*) FROM ExtarnetAccounts WHERE numDivisionID=DM.numDivisionID) = 0 THEN 1 ELSE 0 END)")
                                        End If
                                    Else
                                        If strIDValue(1).ToLower() = "yes" Or strIDValue(1).ToLower() = "true" Then
                                            strCustomCondition.Add("COALESCE(" & strID(0) & ",'0')='1'")
                                        ElseIf strIDValue(1).ToLower() = "no" Or strIDValue(1).ToLower() = "false" Then
                                            strCustomCondition.Add("COALESCE(" & strID(0) & ",'0')='0'")
                                        End If
                                    End If
                            End Select
                        End If
                    Next
                    RegularSearch = String.Join(" and ", strRegularCondition.ToArray())
                    CustomSearch = String.Join(" and ", strCustomCondition.ToArray())
                Else
                    RegularSearch = ""
                    CustomSearch = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
            Try
                txtCurrrentPage.Text = bizPager.CurrentPageIndex
                BindDatagrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub
    End Class

End Namespace
