﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmPurchaseIncentives.aspx.vb" MasterPageFile="~/common/PopupBootstrap.Master" Inherits="BACRM.UserInterface.Accounts.frmPurchaseIncentives" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <script>
        function validate() {
            var grid = $("#<%=grdIncentives.ClientID%>");
            var IsValidate = true;
            $('#<%=grdIncentives.ClientID%> tr').each(function (index, row) {
                if (index > 0) {
                    var controls = row.getElementsByTagName("*");
                    var IsRowChecked = false;
                    for (var i = 0; i < controls.length; i++) {

                        //Find the chkIncentives control.
                        if (controls[i].id.indexOf("chkIncentives") != -1) {
                            if ($("#" + controls[i].id + "").is(":checked")) {
                                IsRowChecked = true;
                            }
                        }

                        //Find the txtBuyIncentives control.
                        if (IsRowChecked == true) {
                            if (controls[i].id.indexOf("txtBuyIncentives") != -1) {
                                if ($("#" + controls[i].id + "").val() == "") {
                                    alert("Please enter mandatory fields");
                                    $("#" + controls[i].id + "").focus();
                                    IsValidate = false;
                                    return false;
                                }
                            }
                            if (controls[i].id.indexOf("txtIncentiveDesc") != -1) {
                                if ($("#" + controls[i].id + "").val() == "") {
                                    alert("Please enter mandatory fields");
                                    $("#" + controls[i].id + "").focus();
                                    IsValidate = false;
                                    return false;
                                }
                            }
                        }
                    }
                }
            });
            return IsValidate;
        }
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnClose" runat="server" OnClick="btnClose_Click" OnClientClick="return validate();" CssClass="button"
                Text="Save & Close" CausesValidation="false" />&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Purchase Incentives
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="row padbottom10" id="divError" runat="server" style="display: none">
            <div class="col-sm-12">
                <div class="alert alert-danger">
                    <h4><i class="icon fa fa-ban"></i>Error</h4>
                    <p>
                        <asp:Label ID="lblError" runat="server"></asp:Label>
                    </p>
                </div>
            </div>
        </div>
    <asp:GridView ID="grdIncentives" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered" OnRowDataBound="grdIncentives_RowDataBound">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:CheckBox ID="chkIncentives" runat="server" Checked='<%#DataBinder.Eval(Container.DataItem, "IsIncentives")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Buy">
                <ItemTemplate>
                    <asp:TextBox ID="txtBuyIncentives" CssClass="form-control" Width="120" Text='<%#DataBinder.Eval(Container.DataItem, "vcBuyingQty")%>' runat="server"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HiddenField ID="hdnIncentives" runat="server" Value='<%#DataBinder.Eval(Container.DataItem, "intType")%>' />
                    <asp:DropDownList ID="ddlIncentives" CssClass="form-control" runat="server">
                        <asp:ListItem Text="Amount" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Quantity" Value="2"></asp:ListItem>
                        <asp:ListItem Text="Lbs" Value="3"></asp:ListItem>
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Get">
                <ItemTemplate>
                    <asp:TextBox ID="txtIncentiveDesc" CssClass="form-control" Width="120" Text='<%#DataBinder.Eval(Container.DataItem, "vcIncentives")%>' runat="server"></asp:TextBox>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:DropDownList ID="ddlDiscountType" runat="server" CssClass="form-control">
                        <asp:ListItem Text="% off" Value="0"></asp:ListItem>
                        <asp:ListItem Text="$ off" Value="1"></asp:ListItem>
                    </asp:DropDownList>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
