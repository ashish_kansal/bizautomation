﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="WebForm1.aspx.vb" Inherits=".WebForm1" %>

<html>
<head runat="server">
    <title>Test Page For Treeview</title>
    <!-- Include the required JavaScript libraries: -->
    <script src="JavaScript/jquery.min.js" type="text/javascript"></script>
    <script src='JavaScript/jquery.js' type="text/javascript"></script>
    <script src='testjs/jquery-ui.custom.js' type="text/javascript"></script>
    <script src='testjs/jquery.cookie.js' type="text/javascript"></script>
    <link rel='stylesheet' type='text/css' href='testjs/ui.dynatree.css' />
    <script src='testjs/jquery.dynatree.js' type="text/javascript"></script>
    
    <script lang="javascript" type="text/javascript">
        //$(document).ready(function () {
        //    $("#ddlPageSize").change(function () {
        //        alert("Handler for ddlPageSize called.");
        //        $('#hdnPageSize').val($(this).val());
        //    });
        //});
        //$(function () {
        // Attach the dynatree widget to an existing <div id="tree"> element
        // and pass the tree options as an argument to the dynatree() function:
        //    $("#tree").dynatree({
        //        onActivate: function (node) {
        //            // A DynaTreeNode object is passed to the activation handler
        //            // Note: we also get this event, if persistence is on, and the page is reloaded.
        //            alert("You activated " + node.data.title);
        //        },
        //        persist: true,
        //        children: [ // Pass an array of nodes.
        //            { title: "Item 1" },
        //            {
        //                title: "Folder 2", isFolder: true,
        //                children: [
        //                    { title: "Sub-item 2.1" },
        //                    { title: "Sub-item 2.2" }
        //                ]
        //            },
        //            { title: "Item 3" }
        //        ]
        //    });
        //});

        $(document).ready(function () {
            $("#tree").dynatree({
                //onActivate: function (node) {
                //    alert("You activated " + node);
                //},
                persist: true,
                noLink: true
            });

            setInterval("slideSwitch()", 2000);
        });

        //$(function () {
        //    //// Initialize the tree in the onload event
        //    $("#tree").dynatree({
        //        //onActivate: function (node) {
        //        //    alert("You activated " + node);
        //        //},
        //        persist: true,
        //        noLink: true
        //    });

        //    //////////Now get the root node object
        //    ////////var rootNode = $("#tree").dynatree();
        //    //////////Call the DynaTreeNode.addChild() member function and pass options for the new node
        //    ////////var childNode = rootNode.addChild({
        //    ////////    title: "Child node 1",
        //    ////////    tooltip: "This child node was added programmatically.",
        //    ////////    isFolder: true
        //    ////////});
        //    //////////
        //    ////////childNode.addChild({
        //    ////////    title: "Document using a custom icon",
        //    ////////    icon: "customdoc1.gif"
        //    ////////});
        //});

        //$(function () {
        //    $("#tree").dynatree({
        //        minExpandLevel: 1, // 1: root node is not collapsible
        //        imagePath: null, // Path to a folder containing icons. Defaults to 'skin/' subdirectory.
        //        children: null, // Init tree structure from this object array.
        //        initId: null, // Init tree structure from a <ul> element with this ID.
        //        initAjax: null, // Ajax options used to initialize the tree strucuture.
        //        autoFocus: true, // Set focus to first child, when expanding or lazy-loading.
        //        keyboard: true, // Support keyboard navigation.
        //        persist: false, // Persist expand-status to a cookie
        //        autoCollapse: false, // Automatically collapse all siblings, when a node is expanded.
        //        clickFolderMode: 3, // 1:activate, 2:expand, 3:activate and expand
        //        activeVisible: true, // Make sure, active nodes are visible (expanded).
        //        checkbox: false, // Show checkboxes.
        //        selectMode: 2, // 1:single, 2:multi, 3:multi-hier
        //        fx: null, // Animations, e.g. null or { height: "toggle", duration: 200 }
        //        noLink: false, // Use <span> instead of <a> tags for all nodes
        //        // Low level event handlers: onEvent(dtnode, event): return false, to stop default processing
        //        onClick: null, // null: generate focus, expand, activate, select events.
        //        onDblClick: null, // (No default actions.)
        //        onKeydown: null, // null: generate keyboard navigation (focus, expand, activate).
        //        onKeypress: null, // (No default actions.)
        //        onFocus: null, // null: set focus to node.
        //        onBlur: null, // null: remove focus from node.

        //        // Pre-event handlers onQueryEvent(flag, dtnode): return false, to stop processing
        //        onQueryActivate: null, // Callback(flag, dtnode) before a node is (de)activated.
        //        onQuerySelect: null, // Callback(flag, dtnode) before a node is (de)selected.
        //        onQueryExpand: null, // Callback(flag, dtnode) before a node is expanded/collpsed.

        //        // High level event handlers
        //        onPostInit: null, // Callback(isReloading, isError) when tree was (re)loaded.
        //        onActivate: null, // Callback(dtnode) when a node is activated.
        //        onDeactivate: null, // Callback(dtnode) when a node is deactivated.
        //        onSelect: null, // Callback(flag, dtnode) when a node is (de)selected.
        //        onExpand: null, // Callback(flag, dtnode) when a node is expanded/collapsed.
        //        onLazyRead: null, // Callback(dtnode) when a lazy node is expanded for the first time.
        //        onCustomRender: null, // Callback(dtnode) before a node is rendered. Return a HTML string to override.
        //        onRender: null, // Callback(dtnode, nodeSpan) after a node was rendered.

        //        // Drag'n'drop support
        //        dnd: {
        //            // Make tree nodes draggable:
        //            onDragStart: null, // Callback(sourceNode), return true, to enable dnd
        //            onDragStop: null, // Callback(sourceNode)
        //            // Make tree nodes accept draggables
        //            autoExpandMS: 1000, // Expand nodes after n milliseconds of hovering.
        //            preventVoidMoves: true, // Prevent dropping nodes 'before self', etc.
        //            onDragEnter: null, // Callback(targetNode, sourceNode)
        //            onDragOver: null, // Callback(targetNode, sourceNode, hitMode)
        //            onDrop: null, // Callback(targetNode, sourceNode, hitMode)
        //            onDragLeave: null // Callback(targetNode, sourceNode)
        //        },
        //        ajaxDefaults: { // Used by initAjax option
        //            cache: false, // false: Append random '_' argument to the request url to prevent caching.
        //            dataType: "json" // Expect json format and pass json object to callbacks.
        //        },
        //        strings: {
        //            loading: "Loading…",
        //            loadError: "Load error!"
        //        },
        //        generateIds: false, // Generate id attributes like <span id='dynatree-id-KEY'>
        //        idPrefix: "dynatree-id-", // Used to generate node id's like <span id="dynatree-id-<key>">.
        //        keyPathSeparator: "/", // Used by node.getKeyPath() and tree.loadKeyPath().
        //        cookieId: "dynatree", // Choose a more unique name, to allow multiple trees.
        //        cookie: {
        //            expires: null // Days or Date; null: session cookie
        //        },
        //        debugLevel: 1 // 0:quiet, 1:normal, 2:debug
        //    });
        //});

        /*** 
       Simple jQuery Slideshow Script
       Released by Jon Raasch (jonraasch.com) under FreeBSD license: free to use or modify, 
       not responsible for anything, etc.  Please link out to me if you like it :)
   ***/

        function slideSwitch() {
            var $active = $('#slideshow IMG.active');

            if ($active.length == 0) $active = $('#slideshow IMG:last');

            // use this to pull the images in the order they appear in the markup
            var $next = $active.next().length ? $active.next()
                : $('#slideshow IMG:first');

            // uncomment the 3 lines below to pull the images in random order

            // var $sibs  = $active.siblings();
            // var rndNum = Math.floor(Math.random() * $sibs.length );
            // var $next  = $( $sibs[ rndNum ] );

            $active.addClass('last-active');

            $next.css({ opacity: 0.0 })
                .addClass('active')
                .animate({ opacity: 1.0 }, 1000, function () {
                    $active.removeClass('active last-active');
                });
        }
    </script>

    <style type="text/css">
        /*** set the width and height to match your images **/

        #slideshow
        {
            position: relative;
            height: 350px;
        }

            #slideshow IMG
            {
                position: absolute;
                top: 0;
                left: 0;
                z-index: 8;
                opacity: 0.0;
            }

                #slideshow IMG.active
                {
                    z-index: 10;
                    opacity: 1.0;
                }

                #slideshow IMG.last-active
                {
                    z-index: 9;
                }
    </style>

</head>
<body>
    <!-- Add a <div> element where the tree should appear: -->
    <div id="tree" runat="server">
        <asp:Literal ID="litULCollection" runat="server"></asp:Literal>
    </div>

    <div id="slideshow" runat="server" height="300px" width="300px"></div>
</body>
</html>
