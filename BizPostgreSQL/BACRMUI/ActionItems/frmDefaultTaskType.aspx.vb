﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common

Public Class frmDefaultTaskType
    Inherits BACRMPage
    Dim objCommon As CCommon

    Dim objUserAccess As UserAccess
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            If Not IsPostBack Then
                objCommon = New CCommon
                objCommon.sb_FillComboFromDBwithSel(ddlTaskType, 73, Session("DomainID"))

                If ddlTaskType.Items.FindByValue("974") IsNot Nothing Then
                    ddlTaskType.Items.Remove(ddlTaskType.Items.FindByValue("974"))
                End If

                objUserAccess = New UserAccess

                Dim dtUserAccessDetails As DataTable
                objUserAccess.UserId = Session("UserID")
                objUserAccess.DomainID = Session("DomainID")
                dtUserAccessDetails = objUserAccess.GetUserAccessDetails
                If dtUserAccessDetails.Rows.Count > 0 Then
                    If Not ddlTaskType.Items.FindByValue(dtUserAccessDetails.Rows(0).Item("numDefaultTaskType")) Is Nothing Then
                        ddlTaskType.ClearSelection()
                        ddlTaskType.Items.FindByValue(dtUserAccessDetails.Rows(0).Item("numDefaultTaskType")).Selected = True
                    End If
                End If
            End If
            btnClose.Attributes.Add("onclick", "return Close()")

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub btnSetDefault_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSetDefault.Click
        Try
            Save()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub Save()
        Try
            objUserAccess = New UserAccess
            objUserAccess.DomainID = Session("DomainID")

            objUserAccess.UserId = Session("UserID")
            objUserAccess.DefaultTaskType = ddlTaskType.SelectedValue
            objUserAccess.byteMode = 2

            objUserAccess.UpdateUserAccessDetailsDefault()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class