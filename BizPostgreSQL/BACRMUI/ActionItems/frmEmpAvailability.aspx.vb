Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Workflow
Namespace BACRM.UserInterface.Admin

    Partial Class frmEmpAvailability
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents txtDate As System.Web.UI.WebControls.TextBox

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                btnSelectTeams.Attributes.Add("onclick", "return OpenSelTeam()")
                If Not IsPostBack Then
                    If GetQueryStringVal("frm") = "Opportunities" Then
                        chkAddToCalendar.Checked = True
                        chkAddToCalendar.Enabled = False
                    Else
                        chkAddToCalendar.Checked = False
                        chkAddToCalendar.Enabled = True
                    End If

                    If GetQueryStringVal("Date") <> "" Then
                        cal.SelectedDate = DateFromFormattedDate(GetQueryStringVal("Date"), Session("DateFormat"))
                    Else
                        cal.SelectedDate = Now.AddMinutes(-CCommon.ToDouble(Session("ClientMachineUTCTimeOffset")))
                    End If

                    'Dim strNow As Date
                    'strNow = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                    'Dim k As Integer
                    'k = strNow.DayOfWeek
                    'ViewState("Date") = DateAdd(DateInterval.Day, -(k - 1), strNow)
                    radOppTab.SelectedIndex = 1

                    BindDropDown()
                    BindGrid()
                    radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                End If
                If radOppTab.SelectedIndex = 0 Then
                    tdDate1.Visible = True
                    tdWeek.Visible = False
                Else
                    btnShow.Visible = False
                    tdDate1.Visible = False
                    tdWeek.Visible = True
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub BindDropDown()
            Try
                'Filling Duration : ddlDuration
                ddlDuration.Items.Insert(0, New ListItem("--Select One--", "0"))
                ddlDuration.Items.Add(New ListItem("30 min", "30"))
                ddlDuration.Items.Add(New ListItem("1 Hour", "60"))
                ddlDuration.Items.Add(New ListItem("1.5 Hours", "90"))
                ddlDuration.Items.Add(New ListItem("2 Hours", "120"))
                ddlDuration.Items.Add(New ListItem("2.5 Hours", "150"))
                ddlDuration.Items.Add(New ListItem("3 Hours", "180"))
                ddlDuration.Items.Add(New ListItem("3.5 Hours", "210"))
                ddlDuration.Items.Add(New ListItem("4 Hours", "240"))
                ddlDuration.Items.Add(New ListItem("4.5 Hours", "270"))
                ddlDuration.Items.Add(New ListItem("5 Hours", "300"))
                ddlDuration.Items.Add(New ListItem("5.5 Hours", "330"))
                ddlDuration.Items.Add(New ListItem("6 Hours", "360"))
                ddlDuration.Items.Add(New ListItem("6.5 Hours", "390"))
                ddlDuration.Items.Add(New ListItem("7 Hours", "420"))
                ddlDuration.Items.Add(New ListItem("7.5 Hours", "450"))
                ddlDuration.Items.Add(New ListItem("8 Hours", "480"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub BindGrid()
            Try
                Dim objActItem As New ActionItem
                Dim dtTable As DataTable
                Dim dtTable1 As DataTable
                Dim ds As DataSet
                Dim strSchedule As String = ""
                objActItem.UserCntID = Session("UserContactID")
                objActItem.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objActItem.DomainID = Session("DomainID")
                objActItem.Type = 1
                objActItem.Duration = ddlDuration.SelectedValue

                Dim dtDate As DateTime = CDate(cal.SelectedDate)
                Dim dtWeekStartDate As DateTime = dtDate.Date

                If dtDate.DayOfWeek > 0 Then
                    dtWeekStartDate = dtDate.AddDays(-dtDate.DayOfWeek).AddDays(1)
                Else
                    dtWeekStartDate = dtDate.AddDays(-6)
                End If

                If radOppTab.SelectedIndex = 0 Then
                    objActItem.StartDateTime = dtDate.Date & " " & ddlStartTime.SelectedItem.Text & " " & ddlAMPM.SelectedItem.Text

                    If IsDBNull(objActItem.StartDateTime) = False Then
                        objActItem.StartDateTime = CCommon.ToSqlDate(objActItem.StartDateTime)
                    End If

                    ds = objActItem.GetEmpAvailability

                    Dim dr As DataRow
                    dtTable = ds.Tables(0)
                    dtTable1 = ds.Tables(1)
                    dtTable1.Merge(ds.Tables(2))

                    dtTable.Columns.Add("Comm")

                    Dim dv As New DataView(dtTable1)
                    Dim drv As DataRowView

                    For Each dr In dtTable.Rows
                        dv.RowFilter = "numContactID=" & dr("numContactID")
                        For Each drv In dv

                            If Not String.IsNullOrEmpty(CCommon.ToString(drv("vcData"))) AndAlso CCommon.ToString(drv("vcData")).Contains("Task") Then
                                strSchedule = strSchedule & "<a onclick=""return OpenActionItem(" & CCommon.ToLong(drv("numCommId")) & ", 0, 0, 0);"" title=""" & drv("ActivityDescription") & """ class='normal1' href='#'>" & drv("vcData") & "</a>" & IIf(CCommon.ToLong(drv("bitTask")) = 974, "", " - " & drv("Schedule")) & ",<br>"

                            ElseIf Not String.IsNullOrEmpty(CCommon.ToString(drv("vcData"))) AndAlso CCommon.ToString(drv("vcData")).Contains("Outlook") Then
                                strSchedule = strSchedule & "<a onclick=""return OpenCalendarItem(" & CCommon.ToLong(drv("ActivityID")) & ");"" title=""" & drv("ActivityDescription") & """ class='normal1' href='#'>" & drv("vcData") & "</a>" & IIf(CCommon.ToLong(drv("bitTask")) = 974, "", " - " & drv("Schedule")) & ",<br>"

                            End If

                        Next
                        strSchedule = strSchedule.TrimEnd(",<br>")
                        dr("Comm") = strSchedule
                        strSchedule = ""
                    Next

                    dgDaily.DataSource = dtTable
                    dgDaily.DataBind()
                Else
                    objActItem.StartDateTime = dtDate.Date & " " & ddlStartTime.SelectedItem.Text & " " & ddlAMPM.SelectedItem.Text

                    If IsDBNull(objActItem.StartDateTime) = False Then
                        objActItem.StartDateTime = CCommon.ToSqlDate(objActItem.StartDateTime)
                    End If

                    ds = objActItem.GetEmpWeeklyAvailability

                    Dim dr As DataRow
                    dtTable = ds.Tables(0)
                    dtTable1 = ds.Tables(1)
                    dtTable1.Merge(ds.Tables(2))

                    dtTable.Columns.Add("firstday")
                    dtTable.Columns.Add("secondday")
                    dtTable.Columns.Add("thirdday")
                    dtTable.Columns.Add("fourthday")
                    dtTable.Columns.Add("fifthday")
                    dtTable.Columns.Add("sixthday")
                    dtTable.Columns.Add("seventhday")

                    Dim dv As New DataView(dtTable1)
                    Dim drv As DataRowView

                    For Each dr In dtTable.Rows
                        Dim strDate As Date = dtWeekStartDate.Date

                        dr("firstday") = GetScheduleString(dr, dv, drv, strDate)
                        dr("secondday") = GetScheduleString(dr, dv, drv, strDate)
                        dr("thirdday") = GetScheduleString(dr, dv, drv, strDate)
                        dr("fourthday") = GetScheduleString(dr, dv, drv, strDate)
                        dr("fifthday") = GetScheduleString(dr, dv, drv, strDate)
                        dr("sixthday") = GetScheduleString(dr, dv, drv, strDate)
                        dr("seventhday") = GetScheduleString(dr, dv, drv, strDate)
                    Next
                    dgWeekly.DataSource = dtTable
                    dgWeekly.DataBind()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Function GetScheduleString(ByVal dr As DataRow, ByVal dv As DataView, ByVal drv As DataRowView, ByRef strDate As Date) As String
            Try
                Dim strSchedule As String = ""
                Dim dtSchedule As DataTable
                dtSchedule = dv.ToTable(True, "numCommId", "ActivityID", "dtStartTime", "dtEndTime", "numContactID", "vcdata", "Schedule", "ActivityDescription", "bitTask").Copy()

                dv = dtSchedule.DefaultView
                dv.RowFilter = "numContactID=" & dr("numContactID") & " and dtStartTime >= '" & strDate.ToString("MM/dd/yyyy 00:00:00 ") & "AM" & "' and dtEndTime <='" & strDate.ToString("MM/dd/yyyy 23:59:59 ") & "PM" & "'"
                If dv.Count > 0 Then
                    For Each drv In dv
                        If Not String.IsNullOrEmpty(CCommon.ToString(drv("vcData"))) AndAlso CCommon.ToString(drv("vcData")).Contains("Task") Then
                            strSchedule = strSchedule & "<a onclick=""return OpenActionItem(" & CCommon.ToLong(drv("numCommId")) & ", 0, 0, 0);"" title=""" & drv("ActivityDescription") & """ class='normal1' href='#'>" & drv("vcData") & "</a>" & IIf(CCommon.ToInteger(drv("bitTask")) = 974, "", " - " & drv("Schedule")) & ",<br>"

                        ElseIf Not String.IsNullOrEmpty(CCommon.ToString(drv("vcData"))) AndAlso CCommon.ToString(drv("vcData")).Contains("Outlook") Then
                            strSchedule = strSchedule & "<a onclick=""return  OpenCalendarItem(" & CCommon.ToLong(drv("ActivityID")) & ");""  title=""" & drv("ActivityDescription") & """ class='normal1' href='#'>" & drv("vcData") & "</a>" & IIf(CCommon.ToInteger(drv("bitTask")) = 974, "", " - " & drv("Schedule")) & ",<br>"

                        End If

                    Next

                Else

                    dv = dtSchedule.DefaultView
                    dv.RowFilter = "dtStartTime >= '" & strDate.ToString("MM/dd/yyyy 00:00:00 ") & "AM" & "' and dtEndTime <='" & strDate.ToString("MM/dd/yyyy 23:59:59 ") & "PM" & "'"
                    If dv.Count > 0 Then
                        For Each drv In dv
                            If Not String.IsNullOrEmpty(CCommon.ToString(drv("vcData"))) AndAlso CCommon.ToString(drv("vcData")).Contains("Task") Then
                                strSchedule = strSchedule & "<a onclick=""return OpenActionItem(" & CCommon.ToLong(drv("numCommId")) & ", 0, 0, 0);"" title=""" & drv("ActivityDescription") & """ class='normal1' href='#'>" & drv("vcData") & "</a>" & IIf(CCommon.ToInteger(drv("bitTask")) = 974, "", " - " & drv("Schedule")) & ",<br>"
                                If strSchedule.Contains("<a onclick=""return OpenActionItem(" & CCommon.ToLong(drv("numCommId")) & ", 0, 0, 0);"" title=""" & drv("ActivityDescription") & """ class='normal1' href='#'>" & drv("vcData") & "</a>" & IIf(CCommon.ToInteger(drv("bitTask")) = 974, "", " - " & drv("Schedule")) & ",<br>") Then
                                    strSchedule = strSchedule.Replace("<a onclick=""return OpenActionItem(" & CCommon.ToLong(drv("numCommId")) & ", 0, 0, 0);"" title=""" & drv("ActivityDescription") & """ class='normal1' href='#'>" & drv("vcData") & "</a>" & IIf(CCommon.ToInteger(drv("bitTask")) = 974, "", " - " & drv("Schedule")) & ",<br>", "")
                                End If

                            ElseIf Not String.IsNullOrEmpty(CCommon.ToString(drv("vcData"))) AndAlso CCommon.ToString(drv("vcData")).Contains("Outlook") Then
                                strSchedule = strSchedule & "<a onclick=""return  OpenCalendarItem(" & CCommon.ToLong(drv("ActivityID")) & ");""  title=""" & drv("ActivityDescription") & """ class='normal1' href='#'>" & drv("vcData") & "</a>" & IIf(CCommon.ToInteger(drv("bitTask")) = 974, "", " - " & drv("Schedule")) & ",<br>"
                                If strSchedule.Contains("<a onclick=""return  OpenCalendarItem(" & CCommon.ToLong(drv("ActivityID")) & ");""  title=""" & drv("ActivityDescription") & """ class='normal1' href='#'>" & drv("vcData") & "</a>" & IIf(CCommon.ToInteger(drv("bitTask")) = 974, "", " - " & drv("Schedule")) & ",<br>") Then
                                    strSchedule = strSchedule.Replace("<a onclick=""return  OpenCalendarItem(" & CCommon.ToLong(drv("ActivityID")) & ");""  title=""" & drv("ActivityDescription") & """ class='normal1' href='#'>" & drv("vcData") & "</a>" & IIf(CCommon.ToInteger(drv("bitTask")) = 974, "", " - " & drv("Schedule")) & ",<br>", "")
                                End If

                            End If
                        Next
                    End If
                    'If dtSchedule.Rows.Count > 0 AndAlso ddlDuration.SelectedValue > 0 Then
                    '    For intCnt As Integer = 0 To dtSchedule.Rows.Count - 1
                    '        If Not String.IsNullOrEmpty(CCommon.ToString(dtSchedule.Rows(intCnt)("vcData"))) AndAlso CCommon.ToString(dtSchedule.Rows(intCnt)("vcData")).Contains("Task") Then
                    '            strSchedule = strSchedule & "<a onclick=""return OpenActionItem(" & CCommon.ToLong(dtSchedule.Rows(intCnt)("numCommId")) & ", 0, 0, 0);"" title=""" & dtSchedule.Rows(intCnt)("ActivityDescription") & """ class='normal1' href='#'>" & dtSchedule.Rows(intCnt)("vcData") & "</a>" & IIf(CCommon.ToInteger(dtSchedule.Rows(intCnt)("bitTask")) = 974, "", " - " & CCommon.ToString(dtSchedule.Rows(intCnt)("Schedule"))) & ",<br>"

                    '        ElseIf Not String.IsNullOrEmpty(CCommon.ToString(dtSchedule.Rows(intCnt)("vcData"))) AndAlso CCommon.ToString(dtSchedule.Rows(intCnt)("vcData")).Contains("Outlook") Then
                    '            strSchedule = strSchedule & "<a onclick=""return  OpenCalendarItem(" & CCommon.ToLong(dtSchedule.Rows(intCnt)("ActivityID")) & ");""  title=""" & CCommon.ToString(dtSchedule.Rows(intCnt)("ActivityDescription")) & """ class='normal1' href='#'>" & CCommon.ToString(dtSchedule.Rows(intCnt)("vcData")) & "</a>" & IIf(CCommon.ToInteger(dtSchedule.Rows(intCnt)("bitTask")) = 974, "", " - " & CCommon.ToString(dtSchedule.Rows(intCnt)("Schedule"))) & ",<br>"

                    '        End If
                    '    Next

                    'End If

                End If

                strSchedule = strSchedule.TrimEnd(",<br>")
                strDate = strDate.AddDays(1)
                Return strSchedule
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub lnkNextDay_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNextDay.Click
            Try
                cal.SelectedDate = DateAdd(DateInterval.Day, 1, CDate(cal.SelectedDate))
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkNextWeek_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkNextWeek.Click
            Try
                Dim dtDate As DateTime = CDate(cal.SelectedDate)

                If dtDate.DayOfWeek > 0 Then
                    dtDate = dtDate.AddDays(-dtDate.DayOfWeek).AddDays(1)
                Else
                    dtDate = dtDate.AddDays(-6)
                End If

                cal.SelectedDate = dtDate.AddDays(7)
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkPrevious_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPrevious.Click
            Try
                cal.SelectedDate = DateAdd(DateInterval.Day, -1, CDate(cal.SelectedDate))
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub lnkPreviousWeek_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkPreviousWeek.Click
            Try
                Dim dtDate As DateTime = CDate(cal.SelectedDate)

                If dtDate.DayOfWeek > 0 Then
                    dtDate = dtDate.AddDays(-dtDate.DayOfWeek).AddDays(1)
                Else
                    dtDate = dtDate.AddDays(-6)
                End If

                    cal.SelectedDate = dtDate.AddDays(-7)
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgWeekly_ItemCommand(source As Object, e As DataGridCommandEventArgs) Handles dgWeekly.ItemCommand
            Try

                If e.CommandName = "Assign" Then

                    If ddlDuration.SelectedValue = 0 Then
                        litMessage.Text = "Select Work time duration."
                        Exit Sub
                    End If

                    Dim dtOpp As New DataTable
                    Dim objOpportunity As New OppotunitiesIP
                    Dim lblName As Label
                    Dim AssignToName As String
                    Dim AssignToId As Long
                    lblName = DirectCast(e.Item.FindControl("lblName"), Label)
                    If lblName IsNot Nothing Then
                        AssignToName = lblName.Text
                        AssignToId = e.CommandArgument
                    Else
                        AssignToName = ""
                        AssignToId = 0
                    End If

                    If GetQueryStringVal("frm") = "Opportunities" Then
                        lngOppId = CCommon.ToLong(GetQueryStringVal("OppID"))
                        objOpportunity.OpportunityId = lngOppId
                        objOpportunity.DomainID = Session("DomainID")
                        objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                        objOpportunity.OpportunityDetails()

                        CreateCalendarItem(AssignToId, AssignToName, objOpportunity)
                        CreateTaskActionItem(AssignToId, AssignToName, objOpportunity)

                    ElseIf GetQueryStringVal("frm") = "Tickler" Then
                        lngProID = CCommon.ToLong(GetQueryStringVal("ProID"))
                        Dim dtDetails As DataTable
                        Dim objProject As New ProjectIP

                        objProject.ProjectID = lngProID
                        objProject.DomainID = Session("DomainID")
                        objProject.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                        dtDetails = objProject.ProjectDetail(Of DataTable)()

                        If chkAddToCalendar.Checked = True Then
                            CreateCalendarItem(AssignToId, AssignToName, dtDetails)
                        End If
                        CreateTaskActionItem(AssignToId, AssignToName, dtDetails)

                    Else
                        If chkAddToCalendar.Checked = True Then
                            CreateCalendarItem(AssignToId, AssignToName, Nothing)
                        End If
                        CreateTaskActionItem(AssignToId, AssignToName, Nothing)
                    End If

                End If
                radOppTab_TabClick(Nothing, Nothing)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgWeekly_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgWeekly.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Header Then
                    Dim dtDate As DateTime = CDate(cal.SelectedDate)
                    If dtDate.DayOfWeek > 0 Then
                        dtDate = dtDate.AddDays(-dtDate.DayOfWeek).AddDays(1)
                    Else
                        dtDate = dtDate.AddDays(-6)
                    End If

                    e.Item.Cells(2).Text = "Mon (" & FormattedDateFromDate(dtDate, Session("DateFormat")) & ")"
                    e.Item.Cells(3).Text = "Tue (" & FormattedDateFromDate(DateAdd(DateInterval.Day, 1, dtDate), Session("DateFormat")) & ")"
                    e.Item.Cells(4).Text = "Wed (" & FormattedDateFromDate(DateAdd(DateInterval.Day, 2, dtDate), Session("DateFormat")) & ")"
                    e.Item.Cells(5).Text = "Thu (" & FormattedDateFromDate(DateAdd(DateInterval.Day, 3, dtDate), Session("DateFormat")) & ")"
                    e.Item.Cells(6).Text = "Fri (" & FormattedDateFromDate(DateAdd(DateInterval.Day, 4, dtDate), Session("DateFormat")) & ")"
                    e.Item.Cells(7).Text = "Sat (" & FormattedDateFromDate(DateAdd(DateInterval.Day, 5, dtDate), Session("DateFormat")) & ")"
                    e.Item.Cells(8).Text = "Sun (" & FormattedDateFromDate(DateAdd(DateInterval.Day, 6, dtDate), Session("DateFormat")) & ")"
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
            Try
                cal.SelectedDate = CDate(cal.SelectedDate)
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Dim lngOppId As Long
        Dim lngProID As Long

        Private Sub dgDaily_ItemCommand(source As Object, e As DataGridCommandEventArgs) Handles dgDaily.ItemCommand
            Try
                If e.CommandName = "Assign" Then

                    If ddlDuration.SelectedValue = 0 Then
                        litMessage.Text = "Select Work time duration."
                        Exit Sub
                    End If

                    Dim dtOpp As New DataTable
                    Dim objOpportunity As New OppotunitiesIP
                    Dim lblName As Label
                    Dim AssignToName As String
                    Dim AssignToId As Long
                    lblName = DirectCast(e.Item.FindControl("lblName"), Label)
                    If lblName IsNot Nothing Then
                        AssignToName = lblName.Text
                        AssignToId = e.CommandArgument
                    Else
                        AssignToName = ""
                        AssignToId = 0
                    End If

                    If GetQueryStringVal("frm") = "Opportunities" Then
                        lngOppId = CCommon.ToLong(GetQueryStringVal("OppID"))
                        objOpportunity.OpportunityId = lngOppId
                        objOpportunity.DomainID = Session("DomainID")
                        objOpportunity.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                        objOpportunity.OpportunityDetails()

                        CreateCalendarItem(AssignToId, AssignToName, objOpportunity)
                        CreateTaskActionItem(AssignToId, AssignToName, objOpportunity)

                    ElseIf GetQueryStringVal("frm") = "Tickler" Then
                        lngProID = CCommon.ToLong(GetQueryStringVal("ProID"))
                        Dim dtDetails As DataTable
                        Dim objProject As New ProjectIP

                        objProject.ProjectID = lngProID
                        objProject.DomainID = Session("DomainID")
                        objProject.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                        dtDetails = objProject.ProjectDetail(Of DataTable)()

                        If chkAddToCalendar.Checked = True Then
                            CreateCalendarItem(AssignToId, AssignToName, dtDetails)
                        End If
                        CreateTaskActionItem(AssignToId, AssignToName, dtDetails)

                    Else
                        If chkAddToCalendar.Checked = True Then
                            CreateCalendarItem(AssignToId, AssignToName, Nothing)
                        End If
                        CreateTaskActionItem(AssignToId, AssignToName, Nothing)
                    End If


                End If

                radOppTab_TabClick(Nothing, Nothing)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub dgDaily_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDaily.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                    If GetQueryStringVal("Date") <> "" Then
                        Dim btn As Button
                        btn = e.Item.FindControl("btnAssign")
                        btn.Attributes.Add("onclick", "return Assign(" & e.Item.Cells(2).Text & ")")
                    End If

                    'Dim hplSchedule As HyperLink
                    'hplSchedule = DirectCast(e.Item.FindControl("hplSchedule"), HyperLink)
                    'If hplSchedule IsNot Nothing Then
                    '    hplSchedule.Attributes.Add("onclick", "return OpenActionItem(" & DataBinder.Eval(e.Item.DataItem, "numCommId") & ", 0, 0, 0);")
                    'End If
                    'OpenActionItem(a, b, c, d)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        'Private Sub btnShow_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnShow.Click
        '    Dim dg As DataGrid
        '    Dim i, k As Integer
        '    dg = Page.FindControl("dgDaily")
        '    Dim dateFrom, dateTo, dateFrom1, dateTo1 As Date
        '    dateFrom = GetQueryStringVal( "Time").Split("-")(0)
        '    dateTo = GetQueryStringVal( "Time").Split("-")(1)
        '    For i = 0 To dg.Items.Count - 1
        '        Dim str, strTime As String()
        '        str = dg.Items(i).Cells(2).Text.Split(",")
        '        If str.Length > 0 Then
        '            For k = 0 To str.Length - 1
        '                strTime = str(k).Split("-")
        '                If strTime.Length = 2 Then
        '                    strTime(0) = strTime(0).Replace("P", " P")
        '                    strTime(0) = strTime(0).Replace("A", " A")
        '                    strTime(1) = strTime(1).Replace("P", " P")
        '                    strTime(1) = strTime(1).Replace("A", " A")
        '                    dateFrom1 = strTime(0)
        '                    dateTo1 = strTime(1)
        '                    If dateFrom1 <= dateFrom And dateFrom <= dateTo1 Then
        '                        dg.Items(i).Visible = False
        '                        Exit For
        '                    End If
        '                    If dateFrom1 <= dateTo And dateTo <= dateTo1 Then
        '                        dg.Items(i).Visible = False
        '                        Exit For
        '                    End If
        '                End If
        '            Next
        '        End If
        '        str = dg.Items(i).Cells(3).Text.Split(",")
        '        If str.Length > 0 Then
        '            For k = 0 To str.Length - 1
        '                strTime = str(k).Split("-")
        '                If strTime.Length = 2 Then
        '                    strTime(0) = strTime(0).Replace("P", " P")
        '                    strTime(0) = strTime(0).Replace("A", " A")
        '                    strTime(1) = strTime(1).Replace("P", " P")
        '                    strTime(1) = strTime(1).Replace("A", " A")
        '                    dateFrom1 = strTime(0)
        '                    dateTo1 = strTime(1)
        '                    If dateFrom1 <= dateFrom And dateFrom <= dateTo1 Then
        '                        dg.Items(i).Visible = False
        '                        Exit For
        '                    End If
        '                    If dateFrom1 <= dateTo And dateTo <= dateTo1 Then
        '                        dg.Items(i).Visible = False
        '                        Exit For
        '                    End If
        '                End If
        '            Next
        '        End If
        '        str = dg.Items(i).Cells(4).Text.Split(",")
        '        If str.Length > 0 Then
        '            For k = 0 To str.Length - 1
        '                strTime = str(k).Split("-")
        '                If strTime.Length = 2 Then
        '                    strTime(0) = strTime(0).Replace("P", " P")
        '                    strTime(0) = strTime(0).Replace("A", " A")
        '                    strTime(1) = strTime(1).Replace("P", " P")
        '                    strTime(1) = strTime(1).Replace("A", " A")
        '                    dateFrom1 = strTime(0)
        '                    dateTo1 = strTime(1)
        '                    If dateFrom1 <= dateFrom And dateFrom <= dateTo1 Then
        '                        dg.Items(i).Visible = False
        '                        Exit For
        '                    End If
        '                    If dateFrom1 <= dateTo And dateTo <= dateTo1 Then
        '                        dg.Items(i).Visible = False
        '                        Exit For
        '                    End If
        '                End If
        '            Next
        '        End If
        '    Next

        'End Sub

        Private Sub radOppTab_TabClick(sender As Object, e As Telerik.Web.UI.RadTabStripEventArgs) Handles radOppTab.TabClick
            Try
                'Dim strNow As Date
                'strNow = DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), Date.UtcNow)
                'If radOppTab.SelectedIndex = 0 Then
                '    If ViewState("Date") Is Nothing Then
                '        ViewState("Date") = strNow
                '        cal.SelectedDate = strNow
                '    Else
                '        cal.SelectedDate = ViewState("Date")
                '    End If
                'Else
                '    If ViewState("Date") Is Nothing Then
                '        Dim k As Integer
                '        k = strNow.DayOfWeek
                '        ViewState("Date") = DateAdd(DateInterval.Day, -(k - 1), strNow)
                '    End If
                'End If
                BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnFind_Click(sender As Object, e As EventArgs) Handles btnFind.Click
            Try
                btnGo_Click(sender, e)
                'BindGrid()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub CreateCalendarItem(ByVal AssignToID As Long, ByVal AssignToName As String, ByVal objMain As Object)
            Try
                Dim strStartTime, strEndTime, strDate As String
                strDate = cal.SelectedDate
                strStartTime = strDate.Trim & " " & ddlStartTime.SelectedItem.Text.Trim() & ddlAMPM.SelectedItem.Text

                Dim objOutLook As New COutlook
                If chkAddToCalendar.Checked = True Then

                    objOutLook.UserCntID = AssignToID
                    objOutLook.DomainID = Session("DomainId")
                    Dim dtTable As DataTable
                    dtTable = objOutLook.GetResourceId
                    If dtTable.Rows.Count > 0 Then

                        objOutLook.AllDayEvent = False

                        If GetQueryStringVal("frm") = "Opportunities" Then
                            objOutLook.ActivityDescription = AssignToName & Environment.NewLine & objMain.OppComments
                            'objOutLook.Location = If(String.IsNullOrEmpty(objMain.ShipStreet), "", objMain.ShipStreet & ",") & _
                            '                      If(String.IsNullOrEmpty(objMain.ShipCity), "", objMain.ShipCity & ",") & _
                            '                      If(String.IsNullOrEmpty(objMain.ShipState), "", objMain.ShipState) & _
                            '                      If(String.IsNullOrEmpty(objMain.ShipCountry), "", objMain.ShipCountry & " - ") & _
                            '                      If(String.IsNullOrEmpty(objMain.ShipPostal), "", objMain.ShipPostal)
                            objOutLook.Location = If(String.IsNullOrEmpty(objMain.ShippingAddress), "", objMain.ShippingAddress)
                            If objOutLook.Location.ToString.Contains("<pre>") OrElse objOutLook.Location.ToString.Contains("</pre>") OrElse objOutLook.Location.ToString.Contains("<br>") Then
                                objOutLook.Location = objOutLook.Location.ToString.Replace("<pre>", "")
                                objOutLook.Location = objOutLook.Location.ToString.Replace("</pre>", "")
                                objOutLook.Location = objOutLook.Location.ToString.Replace("<br>", "")

                            End If

                            objOutLook.Subject = CCommon.ToString(GetQueryStringVal("OrgName")) & " - " & CCommon.ToString(GetQueryStringVal("BProcessName"))

                        ElseIf GetQueryStringVal("frm") = "Tickler" Then
                            Dim dtProj As DataTable = CType(objMain, DataTable)
                            If dtProj IsNot Nothing AndAlso dtProj.Rows.Count > 0 Then
                                objOutLook.ActivityDescription = AssignToName & Environment.NewLine & dtProj.Rows(0)("txtComments")
                                objOutLook.Location = CCommon.ToString(dtProj.Rows(0)("ShippingAddress"))
                                objOutLook.Subject = CCommon.ToString(dtProj.Rows(0)("vcProjectName")) & " - " & CCommon.ToString(GetQueryStringVal("BProcessName"))

                            Else
                                objOutLook.ActivityDescription = AssignToName & Environment.NewLine & ""
                                objOutLook.Location = ""
                                objOutLook.Subject = ""

                            End If
                        Else
                            objOutLook.ActivityDescription = AssignToName & Environment.NewLine & ""
                            objOutLook.Location = ""

                        End If

                        objOutLook.Duration = ddlDuration.SelectedValue * 60
                        objOutLook.StartDateTimeUtc = CType(strStartTime, DateTime).AddMinutes(Session("ClientMachineUTCTimeOffset"))
                        objOutLook.EnableReminder = True
                        objOutLook.ReminderInterval = 900
                        objOutLook.ShowTimeAs = 3
                        objOutLook.Importance = 2
                        objOutLook.RecurrenceKey = -999
                        objOutLook.ResourceName = dtTable.Rows(0).Item("ResourceId")
                        txtActivityID.Text = CStr(objOutLook.AddActivity())

                    End If
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub CreateTaskActionItem(ByVal AssignToID As Long, ByVal AssignToName As String, ByVal objMain As Object)
            Try
                Dim strStartTime, strEndTime, strDate As String
                Dim lngCommId As Long
                strDate = cal.SelectedDate
                strStartTime = strDate.Trim & " " & ddlStartTime.SelectedItem.Text.Trim & ddlAMPM.SelectedItem.Text

                Dim objActionItem As New ActionItem

                With objActionItem

                    If GetQueryStringVal("frm") = "Opportunities" Then
                        .ContactID = objMain.ContactID
                        .DivisionID = objMain.DivisionID
                        .OppID = objMain.OpportunityId
                        .Details = objMain.OppComments

                    ElseIf GetQueryStringVal("frm") = "Tickler" Then
                        Dim dtProj As DataTable = CType(objMain, DataTable)
                        If dtProj IsNot Nothing AndAlso dtProj.Rows.Count > 0 Then
                            .ContactID = dtProj.Rows(0)("numCustPrjMgr")
                            .DivisionID = dtProj.Rows(0)("numDivisionId")
                            .OppID = 0
                            .Details = dtProj.Rows(0)("txtComments")

                        Else
                            .ContactID = Session("UserContactID")
                            .DivisionID = 0
                            .OppID = 0
                            .Details = ""

                        End If

                    Else
                        .ContactID = Session("UserContactID")
                        .DivisionID = 0
                        .OppID = 0
                        .Details = ""

                    End If

                    .CommID = 0
                    .Task = 972
                    .AssignedTo = AssignToID
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainID")
                    .BitClosed = 0
                    .CalendarName = ""
                    .StartTime = CType(strStartTime, DateTime) 'DateAdd(DateInterval.Minute, -Session("ClientMachineUTCTimeOffset"), CCommon.ToSqlDate(cal.SelectedDate))
                    .EndTime = DateAdd(DateInterval.Minute, CCommon.ToDouble(ddlDuration.SelectedValue), .StartTime)

                    .Activity = 0
                    .Status = 28661 'Normal
                    .Snooze = 0 '
                    .SnoozeStatus = 0
                    .Remainder = 0
                    .RemainderStatus = 1
                    .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    .bitOutlook = 1
                    .SendEmailTemplate = False
                    .EmailTemplate = 0
                    .Hours = 0
                    .Alert = 1
                    If GetQueryStringVal("frm") = "Cases" Then
                        .CaseID = CCommon.ToLong(GetQueryStringVal("CaseID"))
                    End If

                    .ActivityId = CInt(txtActivityID.Text)
                    .FollowUpAnyTime = 0
                    .strAttendee = ""
                    .StatusActionItem = 0
                End With

                lngCommId = objActionItem.SaveCommunicationinfo()

                AddToRecentlyViewed(RecetlyViewdRecordType.ActionItem, lngCommId)
                ''Add To Correspondense if Open record is selected

                'Added By Sachin Sadhu||Date:4thAug2014
                'Purpose :To Added Ticker data in work Flow queue based on created Rules
                '          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = lngCommId
                objWfA.SaveWFActionItemsQueue()
                'ss//end of code

                ''Add To Correspondense if Open record is selected

                If lngOppId > 0 AndAlso GetQueryStringVal("frm") = "Opportunities" Then
                    objActionItem.CorrespondenceID = 0
                    objActionItem.CommID = lngCommId
                    objActionItem.EmailHistoryID = 0
                    objActionItem.CorrType = 3
                    objActionItem.OpenRecordID = lngOppId
                    objActionItem.DomainID = Session("DomainID")

                    objActionItem.MRItemAmount = CCommon.ToDecimal(objMain.Amount)
                    objActionItem.ManageCorrespondence()
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

    End Class
End Namespace
