<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" Codebehind="frmLast10ActionItems.aspx.vb" MasterPageFile="~/common/Popup.Master" Inherits="BACRM.UserInterface.Admin.frmLast10ActionItems"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
		<title>Action Item</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
   
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Last 10 Action Items
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
<table class="aspTable" width="500px" style="height:300px"><tr><td valign="top">
			<asp:datagrid id="dgAction" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
				>
				<AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
				<ItemStyle CssClass="is"></ItemStyle>
				<HeaderStyle CssClass="hs"></HeaderStyle>
				<Columns>
					<asp:TemplateColumn HeaderText="Due Date" SortExpression="CloseDate">
						<ItemTemplate>
							<%# ReturnDateTime(DataBinder.Eval(Container.DataItem, "CloseDate")) %>
						</ItemTemplate>
					</asp:TemplateColumn>
					<asp:BoundColumn DataField="bitTask" HeaderText="Type"></asp:BoundColumn>
					<asp:BoundColumn DataField="textDetails" HeaderText="Description"></asp:BoundColumn>

				</Columns>
			</asp:datagrid>
			</td></tr></table> 
</asp:Content>
