﻿Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common

Public Class frmDefaultPopupRemainder
    Inherits BACRMPage
    Dim objUserAccess As UserAccess
    Dim iType As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            iType = CCommon.ToInteger(GetQueryStringVal("Type"))
            If Not IsPostBack Then
                objUserAccess = New UserAccess

                'Type 1:Have Popup Remind Assignee Prior to Event  2:Add to Calendar
                If iType = 1 Then
                    chkPopupRemainder.Text = "Have Popup Remind Assignee Prior to Event"
                    lblTitle.Text = "Action Popup Reminder"
                ElseIf iType = 2 Then
                    chkPopupRemainder.Text = "Add to Calendar"
                    lblTitle.Text = "Default Add to Calendar"
                    Me.Title = "Default Add to Calendar"
                End If

                Dim dtUserAccessDetails As DataTable
                objUserAccess.UserId = Session("UserID")
                objUserAccess.DomainID = Session("DomainID")
                dtUserAccessDetails = objUserAccess.GetUserAccessDetails
                If dtUserAccessDetails.Rows.Count > 0 Then
                    If iType = 1 Then
                        If dtUserAccessDetails.Rows(0).Item("tintDefaultRemStatus") = 0 Then
                            chkPopupRemainder.Checked = False
                        Else : chkPopupRemainder.Checked = True
                        End If
                    ElseIf iType = 2 Then
                        chkPopupRemainder.Checked = dtUserAccessDetails.Rows(0).Item("bitOutlook")
                    End If
                End If
            End If
            btnClose.Attributes.Add("onclick", "return Close()")

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Protected Sub btnSetDefault_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSetDefault.Click
        Try
            Save()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub Save()
        Try
            objUserAccess = New UserAccess
            objUserAccess.DomainID = Session("DomainID")

            objUserAccess.UserId = Session("UserID")

            If iType = 1 Then
                objUserAccess.byteMode = 1
                objUserAccess.DefaultRemStatus = IIf(chkPopupRemainder.Checked = True, 1, 0)
            ElseIf iType = 2 Then
                objUserAccess.byteMode = 3
                objUserAccess.bitOutlook = chkPopupRemainder.Checked
            End If

            objUserAccess.UpdateUserAccessDetailsDefault()

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class