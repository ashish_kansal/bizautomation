<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmEmpAvailability.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmEmpAvailability" MasterPageFile="~/common/Popup.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Availablity</title>
    <link href="~/images/BizSkin/TabStrip.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../CSS/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="../CSS/border-radius.css" />
    <script type="text/javascript" src="../JavaScript/jscal2.js"></script>
    <script type="text/javascript" src="../JavaScript/en.js"></script>
    <script language="javascript">
        function Assign(a) {
            window.opener.AssignTo(a, document.getElementById("ctl00_Content_cal_txtDate").value)
            window.close();
            return false;
        }

        function OpenSelUser(a) {
            window.open("../Admin/frmSelectUsers.aspx?Type=" + a, '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }

        function OpenSelTeam() {
            window.open("../Forecasting/frmSelectTeams.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=1", '',

'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }

        function ConfirmAddToCalendar(ResourceName, Subject, Date, StartTime, EndTime, Location, Description) {
            if (confirm('you�re about to create a Calendar entry for �' + ResourceName + '� with the following details (' + Subject + ',' + Date + ', ' + StartTime / EndTime + ',' + Location + ' and ' + Description + '), Are you sure (Yes or no).') == true) {
                return true;
            }
            else {
                return false;
            }
        }

        function OpenActionItem(a, b, c, d) {
            window.opener.reDirect("../admin/ActionItemDetailsOld.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&CommId=" + a + "&frm=tickler&CaseId=" + b + "&CaseTimeId=" + c + "&CaseExpId=" + d + "&SI1=" + 0);
            return false;
            //window.location.href = "../admin/actionitemdetails.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&CommId=" + a + "&frm=tickler&CaseId=" + b + "&CaseTimeId=" + c +
            //                       "&CaseExpId=" + d + "&SI1=" + 0
            //return false;
        }

        function OpenCalendarItem(a) {
            window.open("../OutlookCalendar/EditCalendar.aspx?id=" + a + "&rs=0" + "&Mode=1", '', 'toolbar=no,titlebar=no,top=100,left=100,width=600,height=400,scrollbars=yes,resizable=yes')
            return false
        }

        $(document).ready(function () {
            $('#litMessage').fadeIn().delay(1000).fadeOut();
        });

        function PopupCheck() {
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnclose" runat="server" OnClientClick="javascript:self.close()"
                Text="Close" CssClass="button" />&nbsp;&nbsp;
        </div>
    </div>
    <table width="100%">
        <tr>
            <td class="normal4" align="center">
                <asp:Label ID="litMessage" EnableViewState="false" runat="server"></asp:Label>
            </td>
        </tr>
    </table>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Availablity
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager ID="Scriptmanager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <table cellspacing="2" cellpadding="2" width="100%">
        <tr>
            <td colspan="5">
                <table>
                    <tr>
                        <td id="tdDate" runat="server" align="right">
                            <BizCalendar:Calendar ID="cal" runat="server" ClientIDMode="AutoID" />
                        </td>
                        <td>
                            <asp:Button ID="btnGo" runat="server" CssClass="button" Text="Go"></asp:Button>&nbsp;&nbsp;
                        </td>
                        <td>
                            <asp:Button ID="btnShow" Width="180" runat="server" Visible="false" CssClass="button"
                                Text="Show Only Available Employees"></asp:Button>&nbsp;&nbsp;
                        </td>
                        <td id="tdDate1" runat="server" class="normal1">
                            <asp:LinkButton ID="lnkPrevious" runat="server">Previous Day</asp:LinkButton>&nbsp;&nbsp;
                            <asp:LinkButton ID="lnkNextDay" runat="server">Next Day</asp:LinkButton>
                        </td>
                        <td id="tdWeek" runat="server" class="normal1">
                            <asp:LinkButton ID="lnkPreviousWeek" runat="server">Previous Week</asp:LinkButton>&nbsp;&nbsp;<asp:LinkButton
                                ID="lnkNextWeek" runat="server">Next Week</asp:LinkButton>&nbsp;&nbsp;
                        </td>
                        <td class="normal1">
                            <a href="javascript:void(0);" onclick="javascript:OpenSelUser(1)">Users</a>&nbsp;&nbsp;
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="5">
                <table>
                    <tr>
                        <td class="normal1" align="right">
                            <asp:CheckBox runat="server" ID="chkAddToCalendar" />
                            <label for="chkAddToCalendar">
                                Add To Calender</label>&nbsp;
                        </td>
                        <td class="normal1">
                            <asp:Button ID="btnSelectTeams" Width="90" runat="server" CssClass="button" Text="Select Teams"></asp:Button>&nbsp;
                        </td>
                        <td class="normal1">Start Time :
                <asp:DropDownList runat="server" ID="ddlStartTime">
                    <%--<asp:ListItem Selected="False" Value="0">-- Select One --</asp:ListItem>--%>
                    <asp:ListItem Selected="False" Value="23">12:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="24">12:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="1">1:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="2">1:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="3">2:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="4">2:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="5">3:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="6">3:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="7">4:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="8">4:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="9">5:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="10">5:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="11">6:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="12">6:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="13">7:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="14">7:30</asp:ListItem>
                    <asp:ListItem Selected="True" Value="15">8:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="16">8:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="17">9:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="18">9:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="19">10:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="20">10:30</asp:ListItem>
                    <asp:ListItem Selected="False" Value="21">11:00</asp:ListItem>
                    <asp:ListItem Selected="False" Value="22">11:30</asp:ListItem>
                </asp:DropDownList>&nbsp;
                <asp:DropDownList runat="server" ID="ddlAMPM">
                    <asp:ListItem Selected="True" Value="0">AM</asp:ListItem>
                    <asp:ListItem Selected="False" Value="1">PM</asp:ListItem>
                </asp:DropDownList>&nbsp;
                        </td>
                        <td class="normal1">Duration :
                <asp:DropDownList runat="server" ID="ddlDuration">
                </asp:DropDownList>&nbsp;
                        </td>
                        <td class="normal1">
                            <asp:Button ID="btnFind" Width="150" runat="server" CssClass="button" Text="Find Available Resources"></asp:Button>
                        </td>
                    </tr>
                </table>
            </td>

        </tr>
    </table>
    <br />
    <telerik:RadTabStrip ID="radOppTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
        Skin="Default" ClickSelectedTab="True" AutoPostBack="True" SelectedIndex="0"
        MultiPageID="radMultiPage_OppTab">
        <Tabs>
            <telerik:RadTab Text="&nbsp;&nbsp;Daily&nbsp;&nbsp;" Value="Daily" PageViewID="radPageView_Daily">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Weekly&nbsp;&nbsp;" Value="Weekly" PageViewID="radPageView_Weekly">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage_OppTab" runat="server" SelectedIndex="0" CssClass="pageView"
        Width="1000px">
        <telerik:RadPageView ID="radPageView_Daily" runat="server">
            <asp:Table ID="Table1" Width="95%" runat="server" Height="350" GridLines="None"
                BorderColor="black" CssClass="aspTable" BorderWidth="1" CellSpacing="0" CellPadding="0">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <asp:DataGrid ID="dgDaily" runat="server" Width="100%" AutoGenerateColumns="False"
                            CssClass="dg">
                            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                            <ItemStyle CssClass="is"></ItemStyle>
                            <HeaderStyle CssClass="hs"></HeaderStyle>
                            <Columns>
                                <asp:TemplateColumn HeaderText="Employee">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblName" Text='<%#Eval("Name")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <%--<asp:BoundColumn DataField="Comm" HeaderText="Schedule"></asp:BoundColumn>--%>
                                <asp:TemplateColumn HeaderText="Schedule">
                                    <ItemTemplate>
                                        <%--<a href="#" onclick="return OpenActionItem(a, b, c, d)">
                                            <asp:Label runat="server" ID="lblName" Text='<%#Eval("Comm")%>'></asp:Label></a>--%>
                                        <asp:HyperLink ID="hplSchedule" runat="server" Text='<%#Eval("Comm")%>'></asp:HyperLink>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="numContactID" Visible="False"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:Button ID="btnAssign" runat="server" Text="Assign" CommandArgument='<%#Eval("numContactID")%>' CommandName="Assign" CssClass="button"></asp:Button>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Weekly" runat="server">
            <asp:Table ID="Table2" Width="95%" runat="server" Height="350" GridLines="None"
                BorderColor="black" CssClass="aspTable" BorderWidth="1" CellSpacing="0" CellPadding="0">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Top">
                        <asp:DataGrid ID="dgWeekly" runat="server" Width="100%" AutoGenerateColumns="False"
                            CssClass="dg" AllowSorting="True">
                            <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                            <ItemStyle CssClass="is"></ItemStyle>
                            <HeaderStyle CssClass="hs"></HeaderStyle>
                            <Columns>
                                <asp:BoundColumn DataField="numContactID" Visible="False"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Employee">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblName" Text='<%#Eval("Name")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <%--<asp:BoundColumn DataField="Name" HeaderText="Employee"></asp:BoundColumn>--%>
                                <asp:BoundColumn DataField="firstday"></asp:BoundColumn>
                                <asp:BoundColumn DataField="secondday"></asp:BoundColumn>
                                <asp:BoundColumn DataField="thirdday"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fourthday"></asp:BoundColumn>
                                <asp:BoundColumn DataField="fifthday"></asp:BoundColumn>
                                <asp:BoundColumn DataField="sixthday"></asp:BoundColumn>
                                <asp:BoundColumn DataField="seventhday"></asp:BoundColumn>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <asp:Button ID="btnAssign" runat="server" Text="Assign" CommandArgument='<%#Eval("numContactID")%>' CommandName="Assign" CssClass="button"></asp:Button>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                            </Columns>
                        </asp:DataGrid>
                        <asp:TextBox runat="server" ID="txtActivityID" Style="display: none" Text="0"></asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
</asp:Content>
