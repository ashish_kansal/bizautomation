<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmActionItemTemplate.aspx.vb"
    Inherits="BACRM.UserInterface.Admin.frmActionItemTemplate" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Action Item Template</title>
    <%--<style type="text/css">
        .bold_span
        {
            font-weight: bold;
        }
    </style>--%>
    <script language="javascript">
        function CheckSave() {
            var objName = document.getElementById('templateName')
            if (objName != null && objName.value.length <= 0) {
                objName.focus();
                alert('Please enter a template name')
                return false;
            }

            if (CheckStatusList() == false) {
                return false;
            }

            if (CheckStatusType() == false) {
                return false;
            }

            if (CheckStatusActivity() == false) {
                return false;
            }
        }

        function CheckDelete() {
            var objName = document.getElementById('templateName')
            if (objName != null && objName.value.length <= 0) {
                alert('Please select a template')
                objName.focus();
                return false;
            }
        }

        function CheckStatusList() {
            var objList = document.getElementById('listStatus')
            if (objList != null && objList.selectedIndex <= 0) {

                alert('Please select a Priority')
                objList.focus();
                return false;
            }
        }

        function CheckStatusType() {
            var objList = document.getElementById('listType')
            if (objList != null && objList.selectedIndex <= 0) {
                alert('Please select a Type')
                objList.focus();
                return false;
            }
        }

        function CheckStatusActivity() {
            var objList = document.getElementById('listActivity')
            if (objList != null && objList.selectedIndex <= 0) {
                alert('Please select a Activity')
                objList.focus();
                return false;
            }
        }

        function CheckAndClosePopUp(key) {
            alert(key);
        }

        function Save() {
            window.opener.location.reload();
            window.close();
        }

        function SaveClose() {
            var btnReload = window.opener.document.getElementById("btnReloadTempateData");
            if (btnReload != null) {
                btnReload.click();
            }

            window.close();
        }

        function Close() {
            window.close();
            return false;
        }

        $(document).ready(function () {
            InitializeValidation();

            $('input[type=radio]').css('vertical-align', 'middle')
            $('label').css('vertical-align', 'middle')
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <ul id="messagebox" class="errorInfo" style="display: none">
    </ul>
    <div class="input-part">
        <div class="right-input">
            <asp:CheckBox runat="server" Style="vertical-align: middle;" ID="chkRemindMe" /><label for="chkRemindMe"><span class="bold_span">Remind me</span>&nbsp;
            <asp:DropDownList runat="server" ID="ddlMinutes" Style="width: 40px!important;">
                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                <asp:ListItem Text="10" Value="10"></asp:ListItem>
                <asp:ListItem Text="15" Value="15"></asp:ListItem>
                <asp:ListItem Text="20" Value="20"></asp:ListItem>
                <asp:ListItem Text="30" Value="30"></asp:ListItem>
                <asp:ListItem Text="45" Value="45"></asp:ListItem>
                <asp:ListItem Text="60" Value="60"></asp:ListItem>
            </asp:DropDownList>
                &nbsp;<span class="bold_span">minutes prior to event</span></label>&nbsp;
            <asp:Button ID="saveAndClose" runat="server" Text="Save & Close" CssClass="button" />&nbsp;
            <asp:Button ID="cancel" runat="server" Text="Close" CssClass="button" />&nbsp;
            <asp:Button ID="delete" runat="server" CssClass="button Delete" Text="Delete" />&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Action Item Template
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table style="width: 700px; margin-top: 5px" cellpadding="2">
        <tr>
            <td class="normal1" align="right" style="width: 16%;">
                <span class="bold_span">Template Name</span>
            </td>
            <td class="normal1">
                <asp:TextBox Style="width: 126px!important" ID="templateName" CssClass="normal1" runat="server"></asp:TextBox>
                &nbsp;<span class="bold_span">Set due date</span><span style="vertical-align:middle!important;margin-left: 4px!important" class="tip" title="This field can contain a number from 1 through 99. Its purpose is to set the Action item�s due date based from the day in which the template is selected. For example, if the number is 7 and one selects the template on 1/20 the result will be an Action Item with a 1/27 due date.">[?]</span>
                <asp:TextBox CssClass="normal1 required_integer {required:false ,number:true, max:99 messages:{number:'provide valid value!', max:'Please enter a value less than or equal to 99.!'}}" Style="width: 30px!important" ID="dueDays" runat="server"></asp:TextBox>
                &nbsp;<span class="bold_span">days from action item creation date.</span>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                <span class="bold_span">Priority</span>
            </td>
            <td>
                <asp:DropDownList CssClass="signup" ID="listStatus" Style="width: 126px!important" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                <span class="bold_span">Type</span>
            </td>
            <td>
                <asp:DropDownList Style="width: 126px!important" runat="server" CssClass="signup" ID="listType">
                    <%--   <asp:ListItem Value="-1" Text="---Select One---" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="1" Text="Communication"></asp:ListItem>
                    <asp:ListItem Value="3" Text="Task"></asp:ListItem>
                    <asp:ListItem Value="4" Text="Notes"></asp:ListItem>
                    <asp:ListItem Value="5" Text="Follow-up Anytime"></asp:ListItem>--%>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                <span class="bold_span">Activity</span>
            </td>
            <td>
                <asp:DropDownList Style="width: 126px!important" runat="server" CssClass="signup" ID="listActivity">
                </asp:DropDownList>
            </td>
        </tr>
        <tr style="display:none">
            <td class="normal1" align="right">
                <span class="bold_span">Email Alert</span>
            </td>
            <td colspan="3" class="normal1">
                <table>
                    <tr>
                        <td>
                            <asp:CheckBox ID="chkAlert" runat="server" />
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlEmailTemplate" CssClass="signup" runat="server" Style="width: 126px!important"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:TextBox ID="txtHours" runat="server" CssClass="signup" Style="width: 30px!important"></asp:TextBox>&nbsp;hours&nbsp;
                        </td>
                        <td>
                            <asp:RadioButtonList ID="rdlEmailTemplate" Style="vertical-align: middle" runat="server" RepeatDirection="Horizontal">
                                <asp:ListItem Value="1" Selected="true" Text="after due date"></asp:ListItem>
                                <asp:ListItem Value="0" Text="before due date"></asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="normal1" align="right">
                <span class="bold_span">Comments</span>
            </td>
            <td colspan="3">
                <asp:TextBox ID='comments' Style="width: 500px!important" Height="140" runat="server" CssClass="normal1"
                    TextMode="MultiLine" Wrap="true"></asp:TextBox>
            </td>
        </tr>
    </table>
    <asp:HiddenField ID="hdnActionTemplateID" runat="server" />
</asp:Content>
