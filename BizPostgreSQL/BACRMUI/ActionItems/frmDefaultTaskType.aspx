﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmDefaultTaskType.aspx.vb"
    Inherits=".frmDefaultTaskType" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Action Task Type</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSetDefault" Text="Set Default" CssClass="button" runat="server">
            </asp:Button>&nbsp;
            <asp:Button ID="btnClose" Text="Close" CssClass="button" runat="server" Width="50">
            </asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Action Task Type
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="tblMile" runat="server" Width="600px" GridLines="None" BorderColor="black"
        BorderWidth="1" CssClass="aspTable">
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Right" CssClass="normal1">Task Type:
            </asp:TableCell>
            <asp:TableCell HorizontalAlign="left">
                <asp:DropDownList ID="ddlTaskType" CssClass="signup" runat="server" Width="180">
                </asp:DropDownList>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
