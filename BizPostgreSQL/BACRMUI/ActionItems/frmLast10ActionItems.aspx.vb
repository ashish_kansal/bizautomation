Imports BACRM.BusinessLogic.Prospects
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Admin
    Public Class frmLast10ActionItems
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents dgAction As System.Web.UI.WebControls.DataGrid

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                Dim dtActionItems As DataTable
                Dim objProspects As New CProspects
                With objProspects
                    .ContactId = CCommon.ToLong(GetQueryStringVal("CntID"))
                    .KeyWord = ""
                    .ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    .CurrentPage = 1
                    .PageSize = 10
                    .TotalRecords = 0
                    .ByteMode = IIf(CCommon.ToInteger(GetQueryStringVal("Type")) = 0, 0, 1)
                End With
                dtActionItems = objProspects.GetActionItems
                dgAction.DataSource = dtActionItems
                dgAction.DataBind()
                
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Function ReturnDateTime(ByVal CloseDate) As String
            Try
                Dim strTargetResolveDate As String = ""
                strTargetResolveDate = FormattedDateTimeFromDate(CloseDate, Session("DateFormat"))
                If Format(CloseDate, "yyyyMMdd") = Format(Now(), "yyyyMMdd") Then
                    strTargetResolveDate = "<font color=red>Today</font>"
                ElseIf Format(CloseDate, "yyyyMMdd") = Format(DateAdd(DateInterval.Day, 1, Now()), "yyyyMMdd") Then
                    strTargetResolveDate = "<font color=orange>" & strTargetResolveDate & "</font>"
                End If
                Return strTargetResolveDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

    End Class
End Namespace

