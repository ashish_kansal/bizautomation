﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmDefaultPopupRemainder.aspx.vb"
    Inherits=".frmDefaultPopupRemainder" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Action Popup Reminder</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnSetDefault" Text="Set Default" CssClass="button" runat="server">
            </asp:Button>&nbsp;
            <asp:Button ID="btnClose" Text="Close" CssClass="button" runat="server"></asp:Button>&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblTitle" runat="server">Action Popup Reminder</asp:Label>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="tblMile" runat="server" Width="600px" GridLines="None" BorderColor="black"
        BorderWidth="1" CssClass="aspTable">
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Left" CssClass="normal1">
                <asp:CheckBox ID="chkPopupRemainder" runat="server" CssClass="text" Text="Have Popup Remind Assignee Prior to Event">
                </asp:CheckBox>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
