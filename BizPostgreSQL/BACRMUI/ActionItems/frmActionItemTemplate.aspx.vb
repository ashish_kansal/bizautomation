Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Marketing
Imports BACRM.BusinessLogic.Common

Namespace BACRM.UserInterface.Admin

    Partial Public Class frmActionItemTemplate
        Inherits BACRMPage

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object
        Protected WithEvents form1 As System.Web.UI.HtmlControls.HtmlForm
        Protected WithEvents newTemplate As System.Web.UI.WebControls.Button
        Protected WithEvents save As System.Web.UI.WebControls.Button
        Protected WithEvents saveAndClose As System.Web.UI.WebControls.Button
        Protected WithEvents cancel As System.Web.UI.WebControls.Button
        Protected WithEvents delete As System.Web.UI.WebControls.Button
        Protected WithEvents templateName As System.Web.UI.WebControls.TextBox
        Protected WithEvents dueDays As System.Web.UI.WebControls.TextBox
        Protected WithEvents listStatus As System.Web.UI.WebControls.DropDownList
        Protected WithEvents listType As System.Web.UI.WebControls.DropDownList
        Protected WithEvents listActivity As System.Web.UI.WebControls.DropDownList
        Protected WithEvents comments As System.Web.UI.WebControls.TextBox

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                ' call once while page load
                If IsPostBack = False Then
                    hdnActionTemplateID.Value = CCommon.ToDouble(GetQueryStringVal("ID"))
                    cancel.Attributes.Add("onclick", "return Close()")
                    saveAndClose.Attributes.Add("onClick", "return CheckSave();")
                    delete.Attributes.Add("onClick", "return CheckDelete();")
                    BindTemplateUIData()    ' will bind onl drop down list
                End If

                If (ViewState("FormPopulated") Is Nothing) Then ViewState("FormPopulated") = "No"
                If (ViewState("FormPopulated").ToString() = "Yes") Then Return ' do not bind form other wise we will not be able to Button_Click

                If CCommon.ToLong(hdnActionTemplateID.Value) > 0 Then
                    Dim actionItemID As Int32
                    Dim thisActionItemTable As DataTable
                    actionItemID = CCommon.ToLong(hdnActionTemplateID.Value)
                    thisActionItemTable = LoadThisActionItemData(Convert.ToInt32(actionItemID))
                    BindTemplateData(thisActionItemTable)
                    ViewState("FormPopulated") = "Yes"
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub BindTemplateData(ByVal thisActionItemTable As DataTable)
            Try
                templateName.Text = thisActionItemTable.Rows(0)("TemplateName")
                dueDays.Text = thisActionItemTable.Rows(0)("DueDays")
                comments.Text = thisActionItemTable.Rows(0)("Comments")
                listStatus.SelectedValue = thisActionItemTable.Rows(0)("Priority")
                listActivity.SelectedValue = thisActionItemTable.Rows(0)("Activity")
                If Not ddlEmailTemplate.Items.FindByValue(thisActionItemTable.Rows(0).Item("numEmailTemplate")) Is Nothing Then
                    ddlEmailTemplate.Items.FindByValue(thisActionItemTable.Rows(0).Item("numEmailTemplate")).Selected = True
                End If
                If thisActionItemTable.Rows(0).Item("bitAlert") = False Then
                    chkAlert.Checked = False
                Else : chkAlert.Checked = True
                End If
                rdlEmailTemplate.SelectedValue = IIf(thisActionItemTable.Rows(0).Item("bitSendEmailTemp") = True, 1, 0)
                txtHours.Text = thisActionItemTable.Rows(0).Item("tintHours")
                Dim _type As String = thisActionItemTable.Rows(0)("Type")
                listType.SelectedIndex = -1
                listType.Items.FindByText(_type.Trim()).Selected = True
                If Not ddlMinutes.Items.FindByValue(thisActionItemTable.Rows(0)("numRemindBeforeMinutes")) Is Nothing Then
                    ddlMinutes.Items.FindByValue(thisActionItemTable.Rows(0)("numRemindBeforeMinutes")).Selected = True
                End If
                chkRemindMe.Checked = CCommon.ToBool(thisActionItemTable.Rows(0)("bitRemind"))
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub InsertTemplateData()
            Try
                Dim objActionItem As New ActionItem
                With objActionItem
                    .TemplateName = templateName.Text
                    If (Me.dueDays.Text.Trim() = "") Then
                        Me.dueDays.Text = "0"
                    End If
                    .DueDays = Convert.ToInt32(Me.dueDays.Text)
                    .StatusActionItem = Convert.ToInt32(Me.listStatus.SelectedItem.Value)
                    .TypeActionItem = Me.listType.SelectedItem.Text
                    .ActivityActionItem = Convert.ToInt32(Me.listActivity.SelectedItem.Value)
                    .Comments = Me.comments.Text
                    .SendEmailTemplate = rdlEmailTemplate.SelectedValue
                    .EmailTemplate = ddlEmailTemplate.SelectedValue
                    .Hours = IIf(txtHours.Text = "", 0, txtHours.Text)
                    .Alert = IIf(chkAlert.Checked = True, 1, 0)
                    .UserCntID = Session("UserContactID")
                    .DomainID = Session("DomainId")
                    .Remainder = ddlMinutes.SelectedValue
                    .RemainderStatus = CCommon.ToShort(chkRemindMe.Checked)
                    .Task = listType.SelectedValue
                End With
                objActionItem.InsertActionTemplateData()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub UpdateTemplateData()
            Try
                If CCommon.ToLong(hdnActionTemplateID.Value) = 0 Then Return
                Dim objActionItem As New ActionItem
                Dim actionItemID As Int32
                actionItemID = CCommon.ToLong(hdnActionTemplateID.Value)
                With objActionItem
                    .RowID = actionItemID
                    .TemplateName = templateName.Text
                    If (Me.dueDays.Text.Trim() = "") Then
                        Me.dueDays.Text = "0"
                    End If
                    .DueDays = Convert.ToInt32(Me.dueDays.Text)
                    .StatusActionItem = Convert.ToInt32(Me.listStatus.SelectedItem.Value)
                    .TypeActionItem = Me.listType.SelectedItem.Text
                    .ActivityActionItem = Convert.ToInt32(Me.listActivity.SelectedItem.Value)
                    .Comments = Me.comments.Text
                    .SendEmailTemplate = rdlEmailTemplate.SelectedValue
                    .EmailTemplate = ddlEmailTemplate.SelectedValue
                    .Hours = IIf(txtHours.Text = "", 0, txtHours.Text)
                    .Alert = IIf(chkAlert.Checked = True, 1, 0)
                    .UserCntID = Session("UserContactId")
                    .DomainID = Session("domainId")
                    .Remainder = ddlMinutes.SelectedValue
                    .RemainderStatus = CCommon.ToShort(chkRemindMe.Checked)
                    .Task = listType.SelectedValue
                End With
                objActionItem.UpdateActionTemplateData()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Function LoadThisActionItemData(ByVal actionItemID As Int32) As DataTable
            Try
                Dim objActionItem As New ActionItem
                With objActionItem
                    .RowID = actionItemID
                End With
                Return objActionItem.LoadThisActionItemTemplateData()
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub BindTemplateUIData()
            Try
                Dim objActionItem As New BACRM.BusinessLogic.Admin.ActionItem
                Dim dsActionItems As DataSet
                dsActionItems = objActionItem.GetActionTemplateUIData()
                Dim objCommon As CCommon
                objCommon = New CCommon
                objCommon.sb_FillComboFromDBwithSel(listStatus, 447, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(listActivity, 32, Session("DomainID"))

                objCommon.sb_FillComboFromDBwithSel(listType, 73, Session("DomainID"))

                If listType.Items.FindByValue("974") IsNot Nothing Then
                    listType.Items.Remove(listType.Items.FindByValue("974"))
                End If

                If dsActionItems.Tables.Count >= 1 Then
                    'Me.listStatus.DataSource = dsActionItems.Tables(0)
                    'Me.listStatus.DataTextField = "vcData"
                    'Me.listStatus.DataValueField = "numListItemID"
                    'Me.listStatus.DataBind()
                    'Me.listStatus.Items.Insert(0, New ListItem("---Select One---", "-1"))

                    'Me.listActivity.DataSource = dsActionItems.Tables(1)
                    'Me.listActivity.DataTextField = "vcData"
                    'Me.listActivity.DataValueField = "numListItemID"
                    'Me.listActivity.DataBind()
                    'Me.listActivity.Items.Insert(0, New ListItem("---Select One---", "-1"))

                    Dim objCampaign As New Campaign
                    objCampaign.DomainID = Session("DomainID")
                    ddlEmailTemplate.DataSource = objCampaign.GetEmailTemplates
                    ddlEmailTemplate.DataTextField = "VcDocName"
                    ddlEmailTemplate.DataValueField = "numGenericDocID"
                    ddlEmailTemplate.DataBind()
                    ddlEmailTemplate.Items.Insert(0, "--Select One--")
                    ddlEmailTemplate.Items.FindByText("--Select One--").Value = 0
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub SaveAndClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles saveAndClose.Click
            Try
                If CCommon.ToLong(hdnActionTemplateID.Value) > 0 Then
                    UpdateTemplateData()
                    Page.RegisterStartupScript("Page_Closed", "<Script>SaveClose();</Script>")
                    Return
                Else
                    InsertTemplateData() ' this will insert data into tlActionItemData 
                    Page.RegisterStartupScript("Page_Closed", "<Script>Save();</Script>")
                    Return
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub Delete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles delete.Click
            Try
                DeleteTemplateData()
                Page.RegisterStartupScript("Page_Closed_Item_Deleted", "<script>Save();</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub DeleteTemplateData()
            Try
                If CCommon.ToLong(hdnActionTemplateID.Value) = 0 Then Return

                Dim objActionItem As New ActionItem
                Dim actionItemID As Int32
                actionItemID = CCommon.ToLong(hdnActionTemplateID.Value)
                With objActionItem
                    .RowID = actionItemID
                End With
                objActionItem.DeleteActionTemplateData() ' delete this item data
                templateName.Text = ""
                dueDays.Text = ""
                comments.Text = ""
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub Cancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cancel.Click
            Try
                templateName.Text = ""
                dueDays.Text = ""
                comments.Text = ""
                Response.Redirect("../ActionItems/frmActionItemTemplate.aspx")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
