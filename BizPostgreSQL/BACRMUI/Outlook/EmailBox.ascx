﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="EmailBox.ascx.vb" Inherits="BACRM.UserInterface.EmailInBox.EmailBox" %>

<style>
    #EmailBox1_gvInbox > tbody > tr > td {
        border:none;
    }
   
</style>

<div class="table-responsive mailbox-messages">
    
    <asp:GridView ID="gvInbox" AutoGenerateColumns="false" runat="server" Width="100%" DataKeyNames="numEmailHstrID" class="table table-bordered" GridLines="None">
        <Columns>
            
        </Columns>
    </asp:GridView>
</div>

<asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
<asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
<asp:TextBox ID="txtCurrrentPage" runat="server" Width="28px" CssClass="signup" Text="1" MaxLength="5" Visible="false"></asp:TextBox>
<asp:HiddenField ID="hdnCurrentPage" runat="server" />
<asp:Label ID="lblCurrentPage" runat="server" Text="1" ForeColor="White"></asp:Label>
<asp:Label ID="lblTotal" runat="server" ForeColor="White"></asp:Label>
<asp:Label ID="lblPageSize" runat="server"></asp:Label>
