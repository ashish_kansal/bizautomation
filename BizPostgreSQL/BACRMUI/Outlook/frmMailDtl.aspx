<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmMailDtl.aspx.vb" Inherits=".frmMailDtl" ValidateRequest="false" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Message</title>
    <link rel="stylesheet" href="~/CSS/bootstrap.min.css" />
    <link rel="stylesheet" href="~/CSS/font-awesome.min.css" />
    <link rel="stylesheet" href="~/CSS/biz.css" />
 <style type="text/css">
     #tblFollowup {
         display:none;
     }
 </style>
</head>
    
      
<body>
        
    <script type="text/javascript" language="javascript">
        function updateReadUnreadStatus(isRead) {
            var strUpdateRecordId = '<%=Request.QueryString("numEmailHstrId")%>'
            var isreadStatus = true;
            if (isRead == "0") {
                isreadStatus = false;
            }
             $("#javascriptUpdateProgress").css("display", "block")
                $.ajax({
                    type: "POST",
                    url: "../outlook/frmInboxItems.aspx/WebMethodUpdateReadUnreadStatus",
                    data: "{strEmailHstrId:'" + strUpdateRecordId + "',isRead:'" + isreadStatus + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                         $("#javascriptUpdateProgress").css("display", "none")
                        UpdateMsgStatus("Status Updated Successfully.");
                        BackToMail();
                    },
                    failure: function (response) {
                        $("#javascriptUpdateProgress").css("display", "none");
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $("#javascriptUpdateProgress").css("display", "none");
                    }, complete: function () {
                        $("#javascriptUpdateProgress").css("display", "none");
                    }
                });
        }
        $(document).ready(function () {

            var ContactId = $('#hdnContactId').val(); 
            var DivisionId = $('#hdnDivisionId').val(); 

            $("#divFP").attr("id", "Contact~74~False~" + ContactId + "~" + DivisionId + "~0~0");
        });
        
    </script>
       
        <script type="text/javascript" language="javascript">
            $(document).ready(function(){ 
                if(window.opener){
                    $("#backMsg").css("display","none");
                }
               
            });
            function CopyMoveEmailsInEmailDtl() {
                var IsCopied = 1;
            var SourceFolder = $('[id$=lblInboxTitle]').text();
            var DestinationFolder = $("#<%=ddlEmailDtlFoldersStructure.ClientID%> option:selected").text();
            var strUpdateRecordId = '<%=Request.QueryString("numEmailHstrId")%>'
            $("#UpdateProgress").css("display","inline")
            $("#ProgressBar").css("display","inline")

            if (strUpdateRecordId == '')
                UpdateMsgStatus("Error:No Email selected.");
            else if ($("#<%=ddlEmailDtlFoldersStructure.ClientID%> option:selected").val() == 0) {
                UpdateMsgStatus("Error:Please select destination folder.");
            } else if (DestinationFolder == "Sent Mail") {
                UpdateMsgStatus("Error:Can not copy/move to selected folder.");
            } else {
                var dataParam = "{strEmailId:'" + strUpdateRecordId + "',sourceFolder:'" + SourceFolder + "',destinationFolder:'" + $("#<%=ddlEmailDtlFoldersStructure.ClientID%> option:selected").text() + "',destinationFolderId:'" + $("#<%=ddlEmailDtlFoldersStructure.ClientID%> option:selected").val() + "',IsCopied:'" + IsCopied + "'}";
                $.ajax({
                    type: "POST",
                    url: "../Outlook/frmInboxItems.aspx/WebMethodCopyMoveEmails",
                    data: dataParam,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        $("#UpdateProgress").css("display", "none");
                        var Jresponse = $.parseJSON(response.d);
                        if (parseFloat(Jresponse) == 1) {
                            BackToMail();
                        } else {
                            UpdateMsgStatus("Error:Some Error Occured.");
                        }
                    },
                    failure: function (response) {
                        $("#UpdateProgress").css("display", "none");
                        $("#ProgressBar").css("display", "none");
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $("#UpdateProgress").css("display", "none");
                    }, complete: function () {
                        $("#UpdateProgress").css("display", "none");
                    }
                });
                // var win = window.open('../outlook/frmoutlooktree.aspx?frm=Move&numEmailHstrId=' + strUpdateRecordId + "&NodeId=" + varnode, 'OutlookTree', 'toolbar=no,titlebar=no,top=200,left=500,width=320,height=410,scrollbars=yes,resizable=yes');
                //var win = window.open('../outlook/frmoutlooktree.aspx?frm=Move&numEmailHstrId=' + strUpdateRecordId + "&NodeId=" + varnode + "&sourceFolder=" + SourceFolder, 'OutlookTree', 'toolbar=no,titlebar=no,top=200,left=500,width=320,height=410,scrollbars=yes,resizable=yes');
                //win.focus();
            }
        }
            function GetSelectedMails() {
                window.parent.frames.frames(0).Delete()
                return window.parent.frames.frames(0).document.getElementById('txtDelEmailIds').value
            }

            function fn_Mail(txtMailAddr, a, b) {
                if (txtMailAddr != '') {
                    if (a == 1) {
                        window.open('mailto:' + txtMailAddr);
                    }
                    else if (a == 2) {
                        window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail=' + txtMailAddr + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
                    }

                }
                return false;
            }

            function fnReply(a, b, c, d, e, reply) {
                window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&reply=' + reply + '&PickAtch=' + d + '&frm=outlook&LsEmail=' + a + '&Subj=' + b + '&EmailHstrId=' + c + '&MailMode=' + e, 'ComposeWindow', 'titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes');
                return false;
            }

            function OpenAttach(a) {
                window.open(a, '', '')
                return false;
            }

            function OpenContact(a, b) {
                debugger;
                if (a == 0) {
                    alert("Email is not Present in Contacts");
                    return false;
                }
                else {
                    var str;
                    str = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&ft6ty=oiuy&CntId=" + a

                    if (window.opener) {
                        window.opener.location.href = str;
                    } else {
                        document.location.href = str;
                    }
                    return false;
                }
            }

            function OpenWindow(a, b, c) {
                if (a == 0) {
                    alert("Email is not Present in Contacts");
                    return false;
                }
                else {
                    var str;
                    if (b == 0) {
                        str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CompanyList&DivID=" + a;
                    }
                    else if (b == 1) {
                        str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CompanyList&DivID=" + a;
                    }
                    else if (b == 2) {
                        str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CompanyList&klds+7kldf=fjk-las&DivId=" + a;
                    }

                    if (window.opener) {
                        window.opener.location.href = str;
                    } else {
                        document.location.href = str;
                    }
                    return false;
                }
            }

            function NewActionItem(a, b) {
                if (a == 0) {
                    alert("Email is not Present in Contacts");
                }
                else {
                    if (window.opener) {
                        window.opener.location.href = "../Admin/ActionItemDetailsOld.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=outlook&CntID=" + a + '&EmailHstrId=' + b;
                    } else {
                        window.open("../Admin/ActionItemDetailsOld.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=outlook&CntID=" + a + '&EmailHstrId=' + b, '', 'toolbar=no,titlebar=no,left=250, top=150,width=1200,height=600,scrollbars=yes,resizable=no');
                    }
                    return false;
                }

                return false;
            }

            function NewCase(a, b, c) {
                if (a == 0) {
                    alert("Email is not Present in Contacts");
                }
                else {
                    if (window.opener) {
                        window.opener.location.href = "../cases/frmAddCases.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=outlook&rtyWR=" + b + "&uihTR=" + a + '&EmailHstrId=' + c;
                    } else {
                        window.open("../cases/frmAddCases.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=outlook&rtyWR=" + b + "&uihTR=" + a + '&EmailHstrId=' + c, '', 'toolbar=no,titlebar=no,left=250, top=150,width=850,height=450,scrollbars=no,resizable=no');
                    }
                }

                return false;
            }

            function NewProject(a, b, c) {
                if (a == 0) {
                    alert("Email is not Present in Contacts");
                }
                else {
                    if (window.opener) {
                        window.opener.location.href = "../Projects/frmProjectAdd.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=outlook&rtyWR=" + b + "&uihTR=" + a + '&EmailHstrId=' + c;
                    } else {
                    window.open("../Projects/frmProjectAdd.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=outlook&rtyWR=" + b + "&uihTR=" + a + '&EmailHstrId=' + c, '', 'toolbar=no,titlebar=no,left=250, top=150,width=850,height=450,scrollbars=no,resizable=no');
                    }
                }
                return false;
            }

            function windowResize() {
                var width = document.getElementById("resizeDiv").offsetWidth;
                var height = document.getElementById("resizeDiv").offsetHeight;
                window.resizeTo(width + 35, height + 100);
            }
            function UpdateEmailStatusinDetails() {
            $("#UpdateProgress").css("display","inline")
            $("#ProgressBar").css("display","inline")
                   $.ajax({
                    type: "POST",
                    url: "../common/Common.asmx/UpdateEmailStatus",
                    data: "{intUpdateRecordId:'<%=Request.QueryString("numEmailHstrID")%>',intUpdatevalue:'" + $("#<%=ddlEmailDtlEmailStatus.ClientID%> option:selected").val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                       success: function (msg) {
                           UpdateMsgStatus("Success:Record Updated successfully");
            $("#UpdateProgress").css("display","none")
            $("#ProgressBar").css("display","none")
                    }
                });
            }
            function deleteEmailDetailsSelectedEmails(strUpdateRecordId) {
            if (strUpdateRecordId == '') {
                UpdateMsgStatus("Error:No Email selected.");
            } else {
                
            $("#UpdateProgress").css("display","inline")
            $("#ProgressBar").css("display","inline")
                $.ajax({
                    type: "POST",
                    url: "../common/Common.asmx/DeleteMultipleEmail",
                    data: "{strUpdateRecordId:'" + strUpdateRecordId + "',NodeId:'<%=Request.QueryString("NodeId")%>'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
            $("#UpdateProgress").css("display","none")
            $("#ProgressBar").css("display","none")
                        BackToMail();
                    }
                });
            }
            }
            function markasSpamDetails() {
                
            $("#UpdateProgress").css("display","inline")
            $("#ProgressBar").css("display","inline")
                $.ajax({
                    type: "POST",
                    url: "../common/Common.asmx/MarkAsSpam",
                    data: "{strUpdateRecordId:'<%=Request.QueryString("numEmailHstrID")%>'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        UpdateMsgStatus(msg.d);

                        
            $("#UpdateProgress").css("display","none")
            $("#ProgressBar").css("display","none")
                        BackToMail()
                    }
                });
            }
        </script>
        
       
        <div class="row">
            <div class="col-xs-12">
                <div id="mailDtl" runat="server"  class="box box-primary">
                    <div class="box-header with-border">
                        <div class="pull-left" id="tdEmailButton" runat="server">
                            <div class="form-inline">
                                
                                <div style="margin-top: 5px;float: left;padding-right: 2px;">
                            <asp:ImageButton runat="server" ID="btnDeleteMail" ImageUrl="~/images/Icon/iconDeleteMail.png" style="width:23px;" />
                                    </div>
                                <a id="backMsg" title="Previous message" onclick="javascript:BackToMail(1)" class="btn btn-xs btn-default"><i class="fa fa-backward"></i>&nbsp;&nbsp;Back to message list</a>
                            <asp:LinkButton runat="server" Text="Forward" ID="btnForward" CssClass="btn btn-xs btn-success"><i class="fa fa-share"></i>&nbsp; &nbsp;Forward</asp:LinkButton>
                            <asp:LinkButton runat="server" ID="btnReplyAll" CssClass="btn btn-xs btn-primary"><i class="fa fa-reply-all"></i>&nbsp;&nbsp;Reply to All</asp:LinkButton>
                            <asp:LinkButton runat="server" ID="btnReply" CssClass="btn btn-xs btn-primary"><i class="fa fa-reply"></i>&nbsp;&nbsp;Reply</asp:LinkButton>
                            &nbsp;&nbsp;<label>Assign Email Status:</label> 
                            <asp:DropDownList ID="ddlEmailDtlEmailStatus" onchange="UpdateEmailStatusinDetails()" runat="server" CssClass="form-control" Style="width: 150px"></asp:DropDownList>
                            &nbsp;&nbsp;<label>Move to:</label> 
                            
                            <asp:DropDownList ID="ddlEmailDtlFoldersStructure" onchange="CopyMoveEmailsInEmailDtl()" runat="server" CssClass="form-control" Style="width: 150px;"></asp:DropDownList>
                                 <a href="#" onclick="updateReadUnreadStatus(1)" title="Mark message(s) as read">
                        <img src="../images/Icon/iconEmailRead.png" style="height: 23px" /></a>
                    <a href="#" onclick="updateReadUnreadStatus(0)" title="Mark message(s) as Unread">
                        <img src="../images/Icon/iconEmailUnRead.png" style="height: 23px" /></a>
                            </div>
                            <div class="btn-group" style="display:none">
                                <asp:LinkButton runat="server" Text="Print" ID="btnPrintMail" CssClass="btn btn-default"><i class="fa fa-print"></i>&nbsp; &nbsp;Print</asp:LinkButton>
                                <asp:LinkButton runat="server" ToolTip="Open New Window" ID="btnOpenNewWindow" CssClass="btn btn-default"><i class="fa fa-external-link"></i></asp:LinkButton>
                            </div>
                           
                        </div>
                        <div class="pull-right" id="trOutlook" runat="server">
                            
                                <a href="#" onclick="markasSpamDetails()" class="btn btn-xs btn-default">SPAM</a>
                            <div class="btn-group" style="display:none">
                                <asp:HyperLink ID="PrevEmail" class="btn btn-default" runat="server" data-original-title="Previous" Visible="false"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Previous</asp:HyperLink>
                                <asp:HyperLink ID="NextEmail" class="btn btn-default" runat="server" data-original-title="Next" Visible="false">Next&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></asp:HyperLink>
                            </div>


                            
                            <asp:LinkButton style="display:none" runat="server" CssClass="btn btn-default" ID="btnClose" class="btn btn-default"><i class="fa fa-times"></i>&nbsp;&nbsp;Close</asp:LinkButton>
                        </div>
                        
                    </div>
                     <div style="vertical-align:top; text-align:left; border:1px;">
                         <table style="vertical-align:top; border:1px; text-align:left; width:100%">
                             <tr>
                                 <td style="vertical-align:top; width:80px; ">
                                     <asp:Label ID="lblFpTooltip" Text="[?]" CssClass="tip" ToolTip="Following up with new leads, prospects, and sometimes even accounts (heck even fellow employees) is an important part of anyone�s work day. With automated follow-ups you can schedule automated email templates and action items such as calls and tasks, to be created / sent out on a schedule, so you never have to remember to follow-up with a contact again�well, action items do require you look at your action item list, but emails are sent automatically by BizAutomation on your behalf, and you can always set an email alert to ping you from workflow automation.  You can disengage follow-up from the contact record itself, or by opening the compose message window, and clicking on the follow-up campaign �Disengage from Follow-up campaign� button.<\n>To setup an Automation Template, go to Marketing | Automated Follow-up. " runat="server"></asp:Label>
                                    <asp:Label ID="lblFollowup" Font-Bold="true" Text="Follow-up Campaign:" runat="server"></asp:Label>                                     
                                 </td>
                                 <td class="editable_select" id="divFP"  style="width:20%; vertical-align:top; text-align:left;"  >
                                    <asp:Label ID="lblFollowupCampaign" runat="server" ></asp:Label>
                                 </td>
                                 <td style="vertical-align:top; width:30%; text-align:left;">
                                     <asp:Label ID="lblLastFp" Font-Bold="true" Text="Last Follow-up:" runat="server"></asp:Label>
                                    <asp:Label ID="lblLastFollowUp" runat="server"></asp:Label>
                                 </td>
                                 <td style="vertical-align:top; width:30%; text-align:left;" >
                                    <asp:Label ID="lblNextFp" Font-Bold="true" Text="Next Follow-up:" runat="server"></asp:Label>
                                    <asp:Label ID="lblNextFollowUp" runat="server"></asp:Label>
                                 </td>
                             </tr>
                            <%-- <tr>
                                 <td colspan="4">
                                    <asp:Label ID="lblEmailStatus" Font-Bold="true" Text="Last Followup Email Status:" runat="server"></asp:Label>
                                    <asp:Label ID="lblLastEmailStatus" runat="server"></asp:Label>
                                 </td>
                             </tr>--%>
                         </table>
                        
                         </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="mailbox-read-info">
                            <h3>
                                <asp:Label Text="Inbox" runat="server" ID="lblSubject" />
                            </h3>
                        </div>
                        <!-- /.mailbox-read-info -->
                        <div class="mailbox-controls with-border">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="pull-left">
                                        <table id="Table1" class="normal7" runat="server" style="color: Gray;">
                                            <tr valign="top">
                                                <td align="right">From :
                                                </td>
                                                <td>
                                                    <asp:HyperLink ID="hypFrom" runat="server" CssClass="hyperlink"></asp:HyperLink>
                                                </td>
                                            </tr>
                                            <tr valign="top">
                                                <td align="right">To :
                                                </td>
                                                <td id="cellTO"></td>
                                            </tr>
                                            <tr valign="top" id="trCC" runat="server" visible="false">
                                                <td align="right">Cc :
                                                </td>
                                                <td id="cellCC"></td>
                                            </tr>
                                            <tr valign="top">
                                                <td align="right">Sent On :
                                                </td>
                                                <td>
                                                    <asp:Label runat="server" ID="lblSentOn" CssClass="normal1"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr valign="top" id="trAttach" runat="server" visible="false">
                                                <td align="right">Attachments :
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="pull-right" runat="server" id="trNewRecordLinks">
                                        <div class="btn-group">
                                            <asp:ImageButton runat="server" Visible="false" BorderWidth="1" ToolTip="Open Contact" ID="btnContact" CssClass="btn btn-default" ImageUrl="../images/Contact-16.gif" />&nbsp;
                                            <asp:ImageButton runat="server" Visible="false" BorderWidth="1" ToolTip="Open Organization" ID="btnCompany" CssClass="btn btn-default" ImageUrl="../images/Building-16.gif" />&nbsp;
                                            <asp:ImageButton runat="server" Visible="false" BorderWidth="1" ToolTip="New Action Item" ID="btnAct" CssClass="btn btn-default" ImageUrl="../images/MasterList-16.gif" />&nbsp;
                                            <asp:ImageButton runat="server" Visible="false" BorderWidth="1" ToolTip="New Case" ID="btnCase" CssClass="btn btn-default" ImageUrl="../images/briefcase-16.gif" />&nbsp;
                                            <asp:ImageButton runat="server" Visible="false" BorderWidth="1" ToolTip="New Project" ID="btnProj" CssClass="btn btn-default" ImageUrl="../images/Compass-16.gif" />&nbsp;
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="mailbox-read-message">
                            <table id="Table4" class="normal1" runat="server">
                                <tr valign="top" enableviewstate="true" id="trBody">
                                    <td>
                                        <asp:Literal ID="litBody" runat="server"></asp:Literal>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <!-- /.mailbox-read-message -->

                        <!-- /.box-body -->
                        <!-- /.box-footer -->
                        <div class="box-footer">
                            <div class="form-inline" id="pinto" runat="server">
                                <div class="form-group">
                                    <label>Pin To</label>
                                    <div style="display: inline">
                                        <asp:DropDownList ID="ddlAssignTO1" CssClass="form-control" runat="server">
                                            <asp:ListItem Text="--Select One--" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="Sales Opportunity" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="Purchase Opportunity" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Sales Order" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="Purchase Order" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="Project" Value="5"></asp:ListItem>
                                            <asp:ListItem Text="Cases" Value="6"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:DropDownList ID="ddlOpenRecord" CssClass="form-control" runat="server">
                                        </asp:DropDownList>
                                        <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-default" />
                                        <asp:Label ID="Label1" Text="[?]" CssClass="tip" ForeColor="Blue" Style="cursor: pointer" runat="server" ToolTip="Will add this email to selected record's correspondance." />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                </div>
                <asp:TextBox runat="server" ID="txtEmailHstrId" Style="display: none" Text="0"></asp:TextBox>
                <asp:HiddenField ID="hdnToEmail" runat="server" />
                <asp:HiddenField runat="server" ID="hdnContactId" />
                <asp:HiddenField runat="server" ID="hdnDivisionId" />
                <asp:HiddenField runat="server" ID="hdnDomainId" />
                <asp:HiddenField runat="server" ID="hdnEmailId" />
            </div>
        </div>

    <asp:Literal ID="litResizeScript" Text="" runat="server" />
</body>
</html>
