Imports WebReference
Imports System.Net
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Imports System.Web.Services
Imports Newtonsoft.Json
Imports Telerik.Web.UI

Namespace BACRM.UserInterface.Outlook
    Partial Public Class frmInboxItems : Inherits BACRMPage

        Dim objOutlook As New COutlook

        Dim dv As DataView
        Dim strColumn As String
        Dim numFixId As Long
        Dim RegularSearch As String
        Dim CustomSearch As String
        Dim m_aryRightsForAct(), m_aryRightsForPro(), m_aryRightsForCase(), m_aryRightsForImp() As Integer
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                'CLEAR ERROR ON RELOAD
                DirectCast(Page.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

                GetUserRightsForPage(33, 1)

                If Not IsPostBack Then

                    PersistTable.Load()

                    If Session("List") <> "Inbox" Then
                        Session("ListDetails") = Nothing
                        Session("Asc") = 1
                    Else
                        Dim str As String()
                    End If
                    Session("List") = "Inbox"

                    If CCommon.ToString(Session("EmailSearchText")).Length > 0 Then
                        hdnSimpleSearch.Value = CCommon.ToString(Session("EmailSearchText"))
                    End If

                    'hdnSelectedDate.Value = DateTime.UtcNow.AddMinutes(Session("ClientMachineUTCTimeOffset") * -1).ToString("yyyy/MM/dd")
                    If GetQueryStringVal("Name") IsNot Nothing AndAlso CCommon.ToString(GetQueryStringVal("Name")) <> "" Then
                        lblInboxTitle.Text = GetQueryStringVal("Name")
                    Else
                        lblInboxTitle.Text = "Inbox"
                    End If

                    If CCommon.ToInteger(GetQueryStringVal("NewEmail")) = 1 Then
                        Dim objEmails As New Email
                        objEmails.GetNewEmails(Session("UserContactId"), Session("DomainId"), "")
                    End If
                    BindTree()
                    BindEmailstatus()
                    BindRelationship()
                    BindFollowupstatus()
                    BindInboxTree()
                    IsIMAPEnable()
                    PersistTable.Load()
                    If PersistTable.Count > 0 Then
                        txtSortColumn.Text = CCommon.ToString(PersistTable(PersistKey.SortColumnName))
                        txtSortChar.Text = CCommon.ToString(PersistTable(PersistKey.SortCharacter))
                        txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                        txtSortOrder.Text = CCommon.ToString(PersistTable(PersistKey.SortOrder))
                        txtGridColumnFilter.Text = CCommon.ToString(PersistTable(PersistKey.GridColumnSearch))

                        hdnFollowupstatus.Value = CCommon.ToString(PersistTable(hdnFollowupstatus.ID))
                        chkGroupBySubjectThread.Checked = CCommon.ToBool(PersistTable(chkGroupBySubjectThread.ID))
                        If ddlRelationship.Items.FindByValue(CCommon.ToString(PersistTable(ddlRelationship.ID))) IsNot Nothing Then
                            ddlRelationship.Items.FindByValue(CCommon.ToString(PersistTable(ddlRelationship.ID))).Selected = True
                            BindProfile()
                        End If
                        If ddlProfile.Items.FindByValue(CCommon.ToString(PersistTable(ddlProfile.ID))) IsNot Nothing Then
                            ddlProfile.Items.FindByValue(CCommon.ToString(PersistTable(ddlProfile.ID))).Selected = True
                        End If
                        If ddlContactsFilterType.Items.FindByValue(CCommon.ToString(PersistTable(ddlContactsFilterType.ID))) IsNot Nothing Then
                            ddlContactsFilterType.Items.FindByValue(CCommon.ToString(PersistTable(ddlContactsFilterType.ID))).Selected = True
                        End If
                        If ddlEmailStatus.Items.FindByValue(CCommon.ToString(PersistTable(ddlEmailStatus.ID))) IsNot Nothing Then
                            ddlEmailStatus.Items.FindByValue(CCommon.ToString(PersistTable(ddlEmailStatus.ID))).Selected = True
                        End If
                        hdnIsNextPreviousDaysEmail.Value = CCommon.ToString(PersistTable(hdnIsNextPreviousDaysEmail.ID))
                        hdnSelectedDate.Value = CCommon.ToString(PersistTable(hdnSelectedDate.ID))
                        hdnSortOrder.Value = CCommon.ToString(PersistTable(hdnSortOrder.ID))
                    End If
                    If CCommon.ToInteger(txtCurrrentPage.Text) = 0 Then
                        txtCurrrentPage.Text = "1"
                    End If
                    BindGrid()
                    hfDateFormat.Value = CCommon.GetValidationDateFormat()
                    'When first time outlook tab is clicked node is 0 and Inbox is loaded
                    If CCommon.ToLong(GetQueryStringVal("node")) = 0 Or CCommon.ToLong(GetQueryStringVal("numFixID")) = 1 Then
                        btnArchive.Visible = True
                        btnSpam.Visible = True
                        btnDeleteEmail.Visible = False
                    Else
                        btnArchive.Visible = False
                        btnSpam.Visible = False
                        btnDeleteEmail.Visible = True
                    End If

                    ' 2 - Deleted Mails
                    If CCommon.ToLong(GetQueryStringVal("numFixID")) = 2 Then
                        PanelDeleteRange.Visible = True
                    Else
                        PanelDeleteRange.Visible = False
                    End If

                    hdnFixID.Value = IIf(CCommon.ToLong(GetQueryStringVal("node")) = 0, "1", CCommon.ToLong(GetQueryStringVal("numFixID")))


                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(CCommon.ToString(ex))
            End Try
        End Sub

        Private Sub BindEmailstatus()
            Try
                Dim dtEmailStatus As DataTable
                objCommon.DomainID = CCommon.ToInteger(Session("DomainId"))
                objCommon.ListID = 385
                dtEmailStatus = objCommon.GetMasterListItemsWithRights()
                ddlEmailStatus.DataSource = dtEmailStatus
                ddlEmailStatus.DataTextField = "vcData"
                ddlEmailStatus.DataValueField = "numListItemId"
                ddlEmailStatus.DataBind()
                ddlEmailStatus.Items.Insert(0, New ListItem("--Filter by--", "0"))
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Private Sub BindProfile()
            ddlProfile.Items.Clear()
            Dim dtddlContact As DataTable
            Dim dsContact As DataSet
            objCommon.DomainID = Session("DomainID")
            objCommon.Mode = 3
            objCommon.PrimaryListItemID = ddlRelationship.SelectedValue
            objCommon.SecondaryListID = 21
            dsContact = objCommon.GetFieldRelationships()
            If dsContact.Tables.Count > 0 Then
                dtddlContact = dsContact.Tables(0)
            End If
            ddlProfile.Items.Add(New ListItem("No Filter Set", "0"))
            For Each dr As DataRow In dtddlContact.Rows
                ddlProfile.Items.Add(New ListItem(dr("SecondaryListItem"), dr("numListItemID")))
            Next


            If ddlProfile.Items.FindByValue(hdnProfileId.Value) IsNot Nothing Then
                ddlProfile.Items.FindByValue(hdnProfileId.Value).Selected = True
            End If
        End Sub
        Private Sub BindRelationship()
            Dim dtddlContact As DataTable
            dtddlContact = objCommon.GetMasterListItems(5, Session("DomainID"))
            ddlRelationship.Items.Add(New ListItem("No Filter Set", "0"))
            ddlRelationship.Items.Add(New ListItem("Leads", "1"))
            ddlRelationship.Items.Add(New ListItem("Accounts", "2"))
            ddlRelationship.Items.Add(New ListItem("Prospects", "3"))
            For Each dr As DataRow In dtddlContact.Rows
                ddlRelationship.Items.Add(New ListItem(dr("vcdata"), dr("numListItemID")))
            Next
            dtddlContact = objCommon.GetMasterListItems(8, Session("DomainID"))
            For Each dr As DataRow In dtddlContact.Rows
                ddlRelationship.Items.Add(New ListItem(dr("vcdata"), dr("numListItemID")))
            Next
        End Sub
        Private Sub BindFollowupstatus()
            Try
                ddlFollowUpStatus.Items.Clear()
                ddlFollowUpStatus.Width = Unit.Percentage(92)
                Dim dt As New DataTable()
                objCommon.DomainID = CLng(HttpContext.Current.Session("DomainID"))
                objCommon.ListID = CCommon.ToLong(30)
                dt = objCommon.GetMasterListItemsWithRights()
                Dim item As ListItem = New ListItem("-- All --", "0")
                ddlFollowUpStatus.Items.Add(item)
                For Each dr As DataRow In dt.Rows
                    item = New ListItem(CCommon.ToString(dr("vcData")), CCommon.ToString(dr("numListItemID")))
                    item.Attributes("OptionGroup") = CCommon.ToString(dr("vcListItemGroupName"))
                    item.Attributes("class") = CCommon.ToString(dr("vcColorScheme"))
                    If (hdnFollowupstatus.Value.Contains(CCommon.ToString(dr("numListItemID")))) Then
                        item.Selected = True
                    End If
                    ddlFollowUpStatus.Items.Add(item)
                Next
            Catch ex As Exception
                Throw
            End Try
        End Sub


        Private Sub BindInboxTree()
            Try
                objOutlook = New COutlook
                Dim dtTable As DataTable
                objOutlook.UserCntID = Session("UserContactId")
                objOutlook.DomainID = Session("DomainId")
                objOutlook.ModeType = 10
                objOutlook.NodeId = 0
                objOutlook.ParentId = 0
                objOutlook.NodeName = ""
                dtTable = objOutlook.GetTree()

                ddlFoldersStructure.Items.Clear()
                ddlFoldersStructure.DataSource = dtTable
                ddlFoldersStructure.DataTextField = "vcNodeName"
                ddlFoldersStructure.DataValueField = "numNodeID"
                ddlFoldersStructure.DataBind()

                ddlFoldersStructure.Items.Insert(0, New ListItem("-Select Folder-", "0"))
                'If CCommon.ToInteger(GetQueryStringVal("node")) = 0 Then
                '    If dtTable.Select("vcNodeName='Inbox'").Length > 0 Then
                '        Page.RegisterStartupScript("BindInitialGrid", "<script>clearvalue();BindGrid(" & dtTable.Select("vcNodeName='Inbox'")(0)("numNodeID") & ")</script>")
                '    End If
                'Else
                '    Dim iNode As Integer = CCommon.ToInteger(GetQueryStringVal("node"))
                '    Page.RegisterStartupScript("BindInitialGrid", "<script>clearvalue();BindGrid(" & iNode & ")</script>")
                'End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Public Sub BindTree()
            Try
                RadTreeView1.Nodes.Clear()
                objOutlook = New COutlook
                Dim dtTable As DataTable
                Dim ds As DataSet = New DataSet()
                objOutlook.UserCntID = Session("UserContactId")
                objOutlook.DomainID = Session("DomainId")
                objOutlook.ModeType = 0
                objOutlook.NodeId = 0
                objOutlook.ParentId = 0
                objOutlook.NodeName = ""
                ds = objOutlook.GetTreeOrderList()

                ds.Relations.Add("NodeRelation", ds.Tables(0).Columns("numNodeID"), ds.Tables(0).Columns("numParentID"))

                For Each dbRow As DataRow In ds.Tables(0).Rows
                    If dbRow.IsNull("numParentID") AndAlso CCommon.ToBool(dbRow("bitHidden")) = False Then
                        Dim node As RadTreeNode = CreateNode(dbRow("vcNodeName").ToString(), True, dbRow("numSortOrder").ToString(), dbRow("numNodeID").ToString(), CCommon.ToLong(dbRow("numFixID")))
                        RadTreeView1.Nodes.Add(node)
                        RecursivelyPopulate(dbRow, node)
                    End If
                Next
                If CCommon.ToString(GetQueryStringVal("nodeID")) <> "" Then
                    Dim node As RadTreeNode
                    node = RadTreeView1.FindNodeByValue(CCommon.ToString(GetQueryStringVal("nodeID")))
                    node.Selected = True
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        'Added By Sachin Sadhu for Ordering MailBox :Fetching Child recursively  ||Case No:516 ||Date:7th-dec-2013
        Private Sub RecursivelyPopulate(dbRow As DataRow, node As RadTreeNode)
            For Each childRow As DataRow In dbRow.GetChildRows("NodeRelation")
                If CCommon.ToBool(childRow("bitHidden")) = False Then
                    Dim childNode As RadTreeNode = CreateNode(childRow("vcNodeName").ToString(), True, childRow("numSortOrder").ToString(), childRow("numNodeID").ToString(), CCommon.ToLong(childRow("numFixID")))
                    node.Nodes.Add(childNode)
                    RecursivelyPopulate(childRow, childNode)
                End If
            Next
        End Sub
        'Added By Sachin Sadhu for Ordering MailBox :Creating node  ||Case No:516 ||Date:7th-dec-2013
        Private Function CreateNode(text As String, expanded As Boolean, order As String, id As String, numFixID As Long) As RadTreeNode
            Dim node As New RadTreeNode(text, order)
            'node.Attributes("numNodeID") = id
            'If (GetQueryStringVal("node") = id) Then
            '    node.Selected = True
            'End If
            node.Value = id
            If (text.ToLower() = "inbox") Then
                'node.Attributes.Add("onclick", "BindGridNode(" & node.Attributes("numNodeID").ToString & ",'Inbox'," & numFixID & ");")
                node.Selected = True

                hdnNodeId.Value = CCommon.ToLong(node.Value)
                node.Attributes.Add("class", "nodeSelected")
                'node.Text = "<i class=""fa fa-inbox""></i>&nbsp;&nbsp;" & node.Text & "<a href='#' class='btn btn-xs btn-primary' style='float:right' onclick='return GetNewMessage();'><i class='fa fa-refresh'></i></a>"
                node.Text = "<img src='../images/Icon/iconFolders.png' class='sideIcon' />&nbsp;&nbsp;" & node.Text
            ElseIf (text.ToLower() = "email archive") Then
                'node.Attributes.Add("onclick", "BindGridNode(" & node.Attributes("numNodeID").ToString & "," & HttpUtility.JavaScriptStringEncode(node.Text, True) & "," & numFixID & ");")
                node.Font.Bold = True
                'node.Text = "<i class=""fa fa-archive""></i>&nbsp;&nbsp;Email Archive"
                node.Text = "<img src='../images/Icon/iconFolders.png' class='sideIcon' />&nbsp;&nbsp;Email Archive"
            ElseIf (text.ToLower() = "sent messages") Then
                'node.Attributes.Add("onclick", "BindGridNode(" & node.Attributes("numNodeID").ToString & "," & HttpUtility.JavaScriptStringEncode(node.Text, True) & "," & numFixID & ");")
                node.Font.Bold = True
                'node.Text = "<i class=""fa fa-envelope-o""></i>&nbsp;&nbsp;Sent Messages"
                node.Text = "<img src='../images/Icon/iconFolders.png' class='sideIcon' />&nbsp;&nbsp;Sent Messages"
            ElseIf (text.ToLower() = "calendar") Then
                node.Font.Bold = True
                'node.Attributes.Add("onclick", "BindCalender(" & node.Attributes("numNodeID").ToString & ");")
                'node.Text = "<i class=""fa fa-calendar""></i>&nbsp;&nbsp;Calendar"
                node.Text = "<img src='../images/Icon/iconFolders.png' class='sideIcon' />&nbsp;&nbsp;Calendar"
            ElseIf (text.ToLower() = "custom folders") Then
                node.Attributes.Add("onclick", "void(0);")
                'node.Text = "<i class=""fa fa-folder""></i>&nbsp;&nbsp;" & "<b>Custom Folders</b><a href='#' onclick='return OpenTree();' class='btn btn-xs btn-primary pull-right'><i class='fa fa-pencil'></i></a>"
                node.Text = "<img src='../images/Icon/iconFolders.png' class='sideIcon' />&nbsp;&nbsp;" & "<b>Custom Folders</b><a href='#' onclick='return OpenTree();' class='btn btn-xs btn-primary pull-right'><i class='fa fa-pencil'></i></a>"
            ElseIf (text.ToLower() = "settings") Then
                node.Font.Italic = True
                'node.Attributes.Add("onclick", "BindOutlookSettings(" & node.Attributes("numNodeID").ToString & ");")
                node.Text = "<i class=""fa fa-gear""></i>&nbsp;&nbsp;Settings"
            Else
                'node.Attributes.Add("onclick", "BindGridNode(" & node.Attributes("numNodeID").ToString & "," & HttpUtility.JavaScriptStringEncode(node.Text, True) & "," & numFixID & ");")
                'node.Text = "<i class=""fa fa-folder-o""></i>&nbsp;&nbsp;" & node.Text
                node.Text = "<img src='../images/Icon/iconFolders.png' class='sideIcon' />&nbsp;&nbsp;" & node.Text
            End If
            node.Expanded = expanded

            Return node
        End Function


        Function FormatDate(ByVal EntryDate) As String
            Try
                Return FormattedDateFromDate(EntryDate, Session("DateFormat"))
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Function ReturnDate(ByVal CloseDate) As String
            Try
                Dim strTargetResolveDate As String = ""
                If Not IsDBNull(CloseDate) Then strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
                Return strTargetResolveDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub IsIMAPEnable()
            Try
                Dim dtUserAccessDetails As DataTable
                Dim objUserAccess As New UserAccess
                objUserAccess.UserId = Session("UserID")
                objUserAccess.DomainID = Session("DomainID")
                dtUserAccessDetails = objUserAccess.GetUserAccessDetails
                If dtUserAccessDetails.Rows.Count > 0 Then
                    If Not CCommon.ToBool(dtUserAccessDetails.Rows(0)("bitImap")) Then
                        lblmsg.Text = "Your IMAP settings are not configured, Please configure it, Click <a href='#' onclick='return openIMAP(" & Session("UserID").ToString & ")'>here</a> to start fetching new mails."
                        lblmsg.CssClass = "errorStatus"
                    End If
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub BindEmailStatus(ByVal ddlStatus As DropDownList)

            objCommon.DomainID = Session("DomainID")
            objCommon.ListID = 340 ' CCommon.ToLong(ddlMasterList.SelectedValue)
            ddlStatus.DataSource = objCommon.GetMasterListItemsWithRights()
            ddlStatus.DataTextField = "vcData"
            ddlStatus.DataValueField = "numListItemID"
            ddlStatus.DataBind()
            'ddlStatus.Items.Insert(-1, "")
            ddlStatus.Items.Insert(0, "")
        End Sub

        Private Sub ddlRelationship_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
            BindGrid()
            BindProfile()
        End Sub
        Private Sub ddlEmailStatus_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs)
            'Dim intX As Integer = 0
            Response.Write("<script language='javascript'>alert('hi') </script>")
        End Sub

        Private Sub btnUpdateNewEmails_Click(sender As Object, e As EventArgs) Handles btnUpdateNewEmails.Click
            hdnNodeId.Value = CCommon.ToLong(RadTreeView1.SelectedNode.Value)
            BindGrid()
            BindFollowupstatus()
            If ddlRelationship.SelectedValue > 0 Then
                BindProfile()
            End If
        End Sub

        Private Sub btnSaveAdvanceSearchState_Click(sender As Object, e As EventArgs) Handles btnSaveAdvanceSearchState.Click
            Try
                PersistTable.Clear()
                If hdnVisibility.Value = "none" Then
                    PersistTable.Add(PersistKey.EmailTabAdvanceSearchVisible, False)
                Else
                    PersistTable.Add(PersistKey.EmailTabAdvanceSearchVisible, True)
                End If
                PersistTable.Save()
            Catch ex As Exception

            End Try
        End Sub

        Function SetBytes(ByVal Bytes) As String
            Try
                If Bytes >= 1073741824 Then
                    SetBytes = Format(Bytes / 1024 / 1024 / 1024, "#0.00") & " GB"
                ElseIf Bytes >= 1048576 Then
                    SetBytes = Format(Bytes / 1024 / 1024, "#0.00") & " MB"
                ElseIf Bytes >= 1024 Then
                    SetBytes = Format(Bytes / 1024, "#0.00") & " KB"
                ElseIf Bytes < 1024 Then
                    SetBytes = Fix(Bytes) & " B"
                End If
                Exit Function
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError("EmailBox: Error Ocuured")
            End Try
        End Function
        Sub GridColumnSearchCriteria()
            Try
                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")

                    Dim strIDValue(), strID(), strCustom As String
                    Dim strRegularCondition As New ArrayList
                    Dim strCustomCondition As New ArrayList


                    For i As Integer = 0 To strValues.Length - 1
                        strIDValue = strValues(i).Split(":")
                        strID = strIDValue(0).Split("~")

                        If strID(0).Contains("CFW.Cust") Then
                            Select Case strID(6).Trim()
                                Case "TextBox", "TextArea"
                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & " ilike '%" & strIDValue(1).Replace("'", "''") & "%')")
                                Case "CheckBox"
                                    If strIDValue(1).ToLower() = "yes" Then
                                        strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and COALESCE(CFW.Fld_Value,'0') " & "='1')")
                                    ElseIf strIDValue(1).ToLower() = "no" Then
                                        strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND 1 = (CASE WHEN (SELECT COUNT(*) FROM CFW_Fld_Values CFWInner WHERE CFWInner.RecId=DivisionMaster.numDivisionID AND CFWInner.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND CFWInner.Fld_Value='1') > 0 THEN 0 ELSE 1 END))")
                                    End If
                                Case "SelectBox"
                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & "=" & strIDValue(1) & ")")
                                Case "DateField"
                                    If strID(4) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " >= '" & fromDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    ElseIf strID(4) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and (CASE WHEN IS_DATE(CFW.Fld_Value) THEN CAST(CFW.Fld_Value AS DATE) ELSE NULL::DATE END) " & " <= '" & toDate.ToString("MM/dd/yyyy") & "')")
                                        End If
                                    End If
                                Case "CheckBoxList"
                                    Dim items As String() = strIDValue(1).Split(",")
                                    Dim searchString As String = ""

                                    For Each item As String In items
                                        searchString = searchString & If(searchString.Length > 0, " OR ", "") & " fn_GetCustFldStringValue(" & strID(0).Replace("CFW.Cust", "") & ",DivisionMaster.numDivisionID,CFW.Fld_Value) " & " ilike '%" & item.Replace("'", "''") & "%'"
                                    Next

                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " AND (" & searchString & "))")
                                Case Else
                                    strCustomCondition.Add(" DM.numDivisionID in (select distinct DivisionMaster.numDivisionID from DivisionMaster left join CFW_Fld_Values CFW ON DivisionMaster.numDivisionID=CFW.RecId where DivisionMaster.numDomainId=" & Session("DomainID") & " AND CFW.fld_id =" & strID(0).Replace("CFW.Cust", "") & " and CFW.Fld_Value " & strCustom & ")")
                            End Select
                        Else
                            Select Case strID(6).Trim()
                                Case "Website", "Email", "TextBox"
                                    strRegularCondition.Add(strID(1) & " ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                Case "SelectBox"
                                    strRegularCondition.Add(strID(1) & "=" & strIDValue(1))
                                Case "TextArea"
                                    strRegularCondition.Add(" Cast(" & strID(1) & " as varchar(5000)) ilike '%" & strIDValue(1).Replace("'", "''") & "%'")
                                Case "DateField"
                                    If strID(9) = "From" Then
                                        Dim fromDate As Date
                                        If Date.TryParse(strIDValue(1), fromDate) Then
                                            strRegularCondition.Add(strID(1) & " >= '" & fromDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    ElseIf strID(9) = "To" Then
                                        Dim toDate As Date
                                        If Date.TryParse(strIDValue(1), toDate) Then
                                            strRegularCondition.Add(strID(1) & " <= '" & toDate.ToString("MM/dd/yyyy") & "'")
                                        End If
                                    End If
                                Case "CheckBox"
                                    If strIDValue(1).ToLower() = "yes" Or strIDValue(1).ToLower() = "true" Then
                                        strCustomCondition.Add("COALESCE(" & strID(1) & ",'0')='1'")
                                    ElseIf strIDValue(1).ToLower() = "no" Or strIDValue(1).ToLower() = "false" Then
                                        strCustomCondition.Add("COALESCE(" & strID(1) & ",'0')='0'")
                                    End If
                            End Select
                        End If
                    Next
                    RegularSearch = String.Join(" and ", strRegularCondition.ToArray())
                    CustomSearch = String.Join(" and ", strCustomCondition.ToArray())
                Else
                    RegularSearch = ""
                    CustomSearch = ""
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        Public Sub BindGrid()
            Try
                Dim dtTable As DataTable
                Dim dsEmail As DataSet
                Dim CreateCol As Boolean = True

                GetUserRightsForPage(33, 1)

                objOutlook.PageSize = CInt(Session("PagingRows"))
                If txtCurrrentPage.Text = "" Then
                    txtCurrrentPage.Text = "1"
                End If
                objOutlook.CurrentPage = txtCurrrentPage.Text '
                objOutlook.SearchText = "" 'IIf(ddlSearch.SelectedValue = 1, txtSearchInbox.Text.Trim, "")
                If hdnIsNextPreviousDaysEmail.Value = "1" Then
                    objOutlook.FilterDate = CCommon.ToSqlDate(CCommon.ToString(hdnSelectedDate.Value))
                Else
                    objOutlook.FilterDate = Nothing
                End If
                'If String.IsNullOrEmpty(CCommon.ToString(Request.QueryString("filterDate"))) Then
                '    objOutlook.FilterDate = Nothing
                'Else
                '    objOutlook.FilterDate = CCommon.ToSqlDate(CCommon.ToString(Request.QueryString("filterDate")))
                'End If

                objOutlook.SearchTextFrom = ""
                objOutlook.SearchTextTo = ""
                objOutlook.SearchTextSubject = ""
                objOutlook.SearchTextHasWords = ""
                objOutlook.SearchHasAttachment = False
                objOutlook.SearchIsAdvancedsrch = False
                objOutlook.SearchInNode = 0
                objOutlook.ExcludeEmailFromNonBizContact = 0
                objOutlook.FromDate = Nothing
                objOutlook.ToDate = Nothing
                'If Not String.IsNullOrEmpty(CCommon.ToString(Request.QueryString("fromDate"))) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(Request.QueryString("toDate"))) Then
                '    objOutlook.FromDate = CCommon.ToSqlDate(CCommon.ToString(Request.QueryString("fromDate")))
                '    objOutlook.ToDate = CCommon.ToSqlDate(CCommon.ToString(Request.QueryString("toDate")))
                'End If

                objOutlook.ToEmail = Session("UserEmail")
                objOutlook.DomainID = Session("DomainId")
                objOutlook.UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                objOutlook.EmailStatus = ddlEmailStatus.SelectedValue
                objOutlook.intContactFilterType = ddlContactsFilterType.SelectedValue
                objOutlook.numRelationShipId = ddlRelationship.SelectedValue
                objOutlook.numProfileId = ddlProfile.SelectedValue
                objOutlook.bitGroupBySubjectThread = chkGroupBySubjectThread.Checked
                objOutlook.vcFollowupStatusIds = hdnFollowupstatus.Value
                objOutlook.columnName = "emailhistory.dtReceivedOn"
                objOutlook.columnSortOrder = hdnSortOrder.Value

                GridColumnSearchCriteria()
                objOutlook.RegularSearchCriteria = RegularSearch
                objOutlook.CustomSearchCriteria = CustomSearch
                hdnGridFilterRegularSearh.Value = RegularSearch
                hdnGridFilterCustomSearh.Value = CustomSearch
                objOutlook.chrSource = "I"
                objOutlook.NodeId = CCommon.ToLong(RadTreeView1.SelectedNode.Value) 'Session("NodeId")
                objOutlook.UserCntID = Session("UserContactId")
                objOutlook.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dsEmail = objOutlook.getInbox()
                PersistTable.Clear()
                PersistTable.Add(PersistKey.CurrentPage, IIf(dsEmail.Tables(1).Rows.Count > 0, txtCurrrentPage.Text, "1"))
                PersistTable.Add(PersistKey.SortCharacter, txtSortChar.Text.Trim())
                PersistTable.Add(PersistKey.SortColumnName, txtSortColumn.Text.Trim())
                PersistTable.Add(PersistKey.SortOrder, txtSortOrder.Text.Trim())
                PersistTable.Add(PersistKey.GridColumnSearch, txtGridColumnFilter.Text)
                PersistTable.Add(ddlEmailStatus.ID, ddlEmailStatus.SelectedValue)
                PersistTable.Add(ddlContactsFilterType.ID, ddlContactsFilterType.SelectedValue)
                PersistTable.Add(ddlRelationship.ID, ddlRelationship.SelectedValue)
                PersistTable.Add(ddlProfile.ID, ddlProfile.SelectedValue)
                PersistTable.Add(chkGroupBySubjectThread.ID, chkGroupBySubjectThread.Checked)
                PersistTable.Add(hdnFollowupstatus.ID, hdnFollowupstatus.Value)
                PersistTable.Add(hdnIsNextPreviousDaysEmail.ID, hdnIsNextPreviousDaysEmail.Value)
                PersistTable.Add(hdnSelectedDate.ID, hdnSelectedDate.Value)
                PersistTable.Add(hdnSortOrder.ID, hdnSortOrder.Value)
                PersistTable.Save()
                'If dsEmail.Tables(1).Rows.Count <= 0 Then
                '    litMessage.Text = "<script>UpdateMsgStatus('Error: No emails found.');</script>"
                '    Exit Sub
                'End If
                dtTable = dsEmail.Tables(1)
                Dim htGridColumnSearch As New Hashtable

                If txtGridColumnFilter.Text.Trim.Length > 0 Then
                    Dim strValues() As String = txtGridColumnFilter.Text.Trim(";").Split(";")
                    Dim strIDValue() As String

                    For k = 0 To strValues.Length - 1
                        strIDValue = strValues(k).Split(":")

                        htGridColumnSearch.Add(strIDValue(0), strIDValue(1))
                    Next
                End If
                Dim dtTableInfo As DataTable
                dtTableInfo = dsEmail.Tables(0)

                Dim i As Integer
                For i = 0 To dtTable.Columns.Count - 1
                    dtTable.Columns(i).ColumnName = dtTable.Columns(i).ColumnName.Replace(".", "")
                Next
                Dim m_aryRightsForInlineEdit() As Integer = GetUserRightsForPage_Other(3, 4)
                gvInbox.DataKeyNames = New String() {"numEmailHstrID"}
                If CreateCol = True Then
                    Dim bField As BoundField
                    Dim Tfield As TemplateField
                    gvInbox.Columns.Clear()
                    Tfield = New TemplateField
                    Tfield.HeaderTemplate = New MyTemp(ListItemType.Header, "~~0~~0~0~DeleteCheckBox", m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch)
                    Tfield.ItemTemplate = New MyTemp(ListItemType.Item, "~~0~~0~0~DeleteCheckBox", m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch)
                    gvInbox.Columns.Add(Tfield)
                    'gvInbox.DataKeyNames = "numHstrEmailId"
                    For i = 0 To dtTable.Columns.Count - 1
                        Dim str As String()
                        str = dtTable.Columns(i).ColumnName.Split("~")
                        If str.Length > 1 Then
                            Tfield = New TemplateField
                            Tfield.HeaderTemplate = New MyTemp(ListItemType.Header, dtTable.Columns(i).ColumnName, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch)
                            Tfield.ItemTemplate = New MyTemp(ListItemType.Item, dtTable.Columns(i).ColumnName, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE), htGridColumnSearch)

                            gvInbox.Columns.Add(Tfield)
                        End If
                    Next

                End If
                gvInbox.DataSource = dtTable
                gvInbox.DataBind()
                '*************Paging****************************************

                Session("ListDetails") = txtCurrrentPage.Text & "," & ViewState("Column") & "," & Session("Asc")
                Dim TotalRec As Integer = 0
                If dtTable.Rows.Count > 0 Then
                    TotalRec = dtTable.Rows(0).Item("TotalRowCount")
                    litMessage.Text += "<script>UpdateTotalSize('" & IIf(dtTable.Rows(0).Item("TotalSize") > 0, "You are using " & SetBytes(dtTable.Rows(0).Item("TotalSize")) & " space.", "") & "');</script>"
                    litMessage.Text += "<script>UpdateTotalRecords('" & dtTable.Rows(0).Item("TotalRowCount") & "');</script>"
                End If

                If TotalRec = 0 Then
                    litMessage.Text += "<script>UpdateTotalSize('');</script>"
                    litMessage.Text += "<script>UpdateTotalRecords('0');</script>"
                    txtCurrrentPage.Text = 1
                Else
                    Dim strTotalPage As String()
                    Dim decTotalPage As Decimal
                    decTotalPage = TotalRec / Session("PagingRows")
                    decTotalPage = Math.Round(decTotalPage, 2)
                    strTotalPage = CStr(decTotalPage).Split(".")
                    If (TotalRec Mod Session("PagingRows")) = 0 Then
                        lblTotal.Text = strTotalPage(0)
                        txtTotalPage.Text = strTotalPage(0)
                    Else
                        lblTotal.Text = strTotalPage(0) + 1
                        txtTotalPage.Text = strTotalPage(0) + 1
                    End If
                    txtTotalRecords.Text = TotalRec
                End If

                lblCurrentPage.Text = CCommon.ToString(Request.QueryString("CurrentPage"))
                If lblCurrentPage.Text <> "" Then
                    If TotalRec <= CInt(Session("PagingRows")) * CInt(lblCurrentPage.Text) Then
                        litMessage.Text += "<script>UpdateDisplayedRecords('" & TotalRec & "');</script>"
                    Else
                        litMessage.Text += "<script>UpdateDisplayedRecords('" & CCommon.ToString(CInt(Session("PagingRows")) * CInt(lblCurrentPage.Text)) & "');</script>"
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Protected Sub RadTreeView1_NodeClick(ByVal sender As Object, ByVal e As RadTreeNodeEventArgs)
            e.Node.Selected = True
            hdnNodeId.Value = RadTreeView1.SelectedNode.Value
            e.Node.Attributes.Add("class", "nodeSelected")
            BindGrid()
        End Sub
        Dim dtItemList As DataTable
        Private Sub gvInbox_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvInbox.RowDataBound

            If e.Row.RowType = DataControlRowType.Header Then
                For Each cell As TableCell In e.Row.Cells
                    If String.IsNullOrEmpty(cell.Text) AndAlso cell.Controls.Count = 0 Then
                        cell.Attributes.Add("class", "hidecolumn")
                    End If
                Next
            ElseIf e.Row.RowType = DataControlRowType.DataRow Then

                Dim dr As DataRowView = CType(e.Row.DataItem, DataRowView)
                Dim ddlEmailHistory As DropDownList = CType(e.Row.FindControl("ddlEmailHistory"), DropDownList) 'CType(sender, DropDownList)

                If ddlEmailHistory IsNot Nothing Then
                    If dtItemList Is Nothing Then
                        dtItemList = objCommon.GetMasterListItems(385, Session("DomainId"))
                    End If
                    ddlEmailHistory.DataSource = dtItemList
                    ddlEmailHistory.DataTextField = "vcData"
                    ddlEmailHistory.DataValueField = "numListItemID"
                    ddlEmailHistory.DataBind()
                    ddlEmailHistory.Items.Insert(0, New ListItem("", "0"))
                    If CCommon.ToLong(dr("numListItemID")) > 0 Then
                        If Not ddlEmailHistory.Items.FindByValue(CCommon.ToLong(dr("numListItemID"))) Is Nothing Then
                            ddlEmailHistory.Items.FindByValue(CCommon.ToLong(dr("numListItemID"))).Selected = True
                        End If
                    End If
                End If


                If CCommon.ToBool(dr("bitIsRead")) = False Then
                    Dim lblvcSubject As Label = CType(e.Row.FindControl("vcSubject"), Label)
                    Dim lblvcFrom As Label = CType(e.Row.FindControl("vcFrom"), Label)
                    Dim lblvcTo As Label = CType(e.Row.FindControl("vcTo"), Label)
                    'If lblvcSubject.Text.Trim.Length > 100 Then
                    '    lblvcSubject.Text = lblvcSubject.Text.TrimLength(100)
                    'End If
                    If lblvcSubject IsNot Nothing Then
                        lblvcSubject.Font.Bold = True
                    End If
                    If lblvcFrom IsNot Nothing Then
                        lblvcFrom.Font.Bold = True
                    End If
                    If lblvcTo IsNot Nothing Then
                        lblvcTo.Font.Bold = True
                    End If
                End If

                Dim hdnContactId As HiddenField = CType(e.Row.FindControl("hdnContactID"), HiddenField)
                If e.Row.FindControl("lnkRecentCorr") IsNot Nothing Then
                    Dim lnk As HyperLink = CType(e.Row.FindControl("lnkRecentCorr"), HyperLink)
                    If hdnContactId.Value = 0 Then
                        lnk.Text = ""
                        lnk.Enabled = False
                    Else
                        lnk.ToolTip = "Recent Correspondance"
                        lnk.Attributes.Add("onClick", "javascript:OpenCorresPondance('" & hdnContactId.Value & "');")
                        lnk.CssClass = "hyperlink"
                    End If
                End If
                If e.Row.FindControl("lblAlertPanel") IsNot Nothing Then
                    Dim lblAlert As Label = CType(e.Row.FindControl("lblAlertPanel"), Label)
                End If

                If e.Row.Cells.Count > 4 Then
                    e.Row.BorderStyle = BorderStyle.None
                    Dim row As GridViewRow
                    Dim cell As TableCell
                    Dim tbl As Table = CType(e.Row.Parent, Table)

                    cell = New TableCell
                    row = New GridViewRow(-1, -1, DataControlRowType.DataRow, DataControlRowState.Normal)
                    'cell.ColumnSpan = 2
                    row.Cells.Add(cell)

                    cell = New TableCell
                    cell.ColumnSpan = Me.gvInbox.Columns.Count
                    Dim span As New HtmlGenericControl("span")
                    span.Attributes("style") = "color:gray;"
                    span.InnerHtml = CCommon.ToString(dr("vcBodyText"))
                    cell.Controls.Add(span)

                    row.Cells.Add(cell)
                    row.CssClass = "Row-border"

                    'Change color of row for Sent messages (if the mail has read by receiver) (numFixId is 4 for Sent Messages) // Added by Priya
                    Dim hdnNoofTimesOpened As HiddenField = CType(e.Row.FindControl("hdnnumNoofTimes"), HiddenField)
                    If (CCommon.ToInteger(hdnNoofTimesOpened.Value) > 0) And numFixId = 4 Then
                        e.Row.BackColor = Drawing.Color.FromArgb(219, 249, 220)
                    End If

                    'Change color of Row
                    If e.Row.DataItem.DataView.Table.Columns("vcColorScheme") IsNot Nothing Then
                        If ((DataBinder.Eval(e.Row.DataItem, "vcColorScheme").ToString.Length > 0) And (numFixId <> 4)) Then
                            row.CssClass = "Row-border " & DataBinder.Eval(e.Row.DataItem, "vcColorScheme")
                        ElseIf ((DataBinder.Eval(e.Row.DataItem, "vcColorScheme").ToString.Length > 0) And (CCommon.ToInteger(dr("NoOfTimeOpened")) > 0 And dr("NoOfTimeOpened") IsNot Nothing And numFixId = 4)) Then
                            e.Row.BackColor = Drawing.Color.FromArgb(219, 249, 220)
                        End If
                    End If

                    tbl.Rows.Add(row)
                End If

                'Change color of Row
                If e.Row.DataItem.DataView.Table.Columns("vcColorScheme") IsNot Nothing Then
                    If ((DataBinder.Eval(e.Row.DataItem, "vcColorScheme").ToString.Length > 0) And (numFixId <> 4)) Then
                        e.Row.CssClass = DataBinder.Eval(e.Row.DataItem, "vcColorScheme")
                    ElseIf ((DataBinder.Eval(e.Row.DataItem, "vcColorScheme").ToString.Length > 0) And (CCommon.ToInteger(dr("NoOfTimeOpened")) > 0 And dr("NoOfTimeOpened") IsNot Nothing And numFixId = 4)) Then
                        e.Row.BackColor = Drawing.Color.FromArgb(219, 249, 220)
                    End If
                End If


            End If
        End Sub


        Private Sub DisplayError(ByVal exception As String)
            Try
                DirectCast(Page.Master.FindControl("lblError"), Label).Text = exception
                DirectCast(Page.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
                DirectCast(Page.Master.FindControl("divError"), HtmlGenericControl).Focus()
            Catch ex As Exception

            End Try
        End Sub
        <WebMethod()>
        Public Shared Function WebMethodCopyMoveEmails(ByVal strEmailId As String, ByVal sourceFolder As String, ByVal destinationFolder As String, ByVal destinationFolderId As Long, ByVal IsCopied As Long) As String
            Try
                Dim intCount As Integer = 0
                If strEmailId.Length > 0 Then
                    strEmailId = strEmailId.TrimEnd(",")
                    'IsCopied 1 Move 0
                    Dim objEmail As New Email
                    objEmail.MoveCopyGmail(strEmailId, sourceFolder, destinationFolder, destinationFolderId, IsCopied)
                End If
                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject("1", Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function

        <WebMethod()>
        Public Shared Function WebMethodBindProfile(ByVal RelationshipId As Long)
            Dim dtddlContact As DataTable
            Dim dsContact As DataSet
            Dim objCommon As New CCommon
            objCommon.DomainID = HttpContext.Current.Session("DomainID")
            objCommon.Mode = 3
            objCommon.PrimaryListItemID = RelationshipId
            objCommon.SecondaryListID = 21
            dsContact = objCommon.GetFieldRelationships()
            Dim json As String = String.Empty
            If dsContact.Tables.Count > 0 Then
                dtddlContact = dsContact.Tables(0)
                json = JsonConvert.SerializeObject(dtddlContact, Formatting.None)
            End If
            Return json
        End Function
        <WebMethod()>
        Public Shared Function WebMethodSearchInbox(ByVal text As String, ByVal NodeId As Int32, ByVal EmailStatus As Long, ByVal intContactFilterType As Int32, ByVal numRelationShipId As Long, ByVal numProfileId As Long, ByVal bitGroupBySubjectThread As Boolean, ByVal vcFollowupStatusIds As String, ByVal RegularSearchCriteria As String, ByVal CustomSearchCriteria As String)
            Dim dtTable As DataTable
            Dim dsEmail As DataSet
            Dim CreateCol As Boolean = True
            Dim objOutlook As New COutlook()

            objOutlook.PageSize = 30
            objOutlook.CurrentPage = 1
            objOutlook.SearchText = text
            objOutlook.FilterDate = Nothing

            objOutlook.SearchTextFrom = ""
            objOutlook.SearchTextTo = ""
            objOutlook.SearchTextSubject = ""
            objOutlook.SearchTextHasWords = ""
            objOutlook.SearchHasAttachment = False
            objOutlook.SearchIsAdvancedsrch = False
            objOutlook.SearchInNode = 0
            objOutlook.ExcludeEmailFromNonBizContact = 0
            objOutlook.FromDate = Nothing
            objOutlook.ToDate = Nothing
            objOutlook.EmailStatus = EmailStatus
            objOutlook.intContactFilterType = intContactFilterType
            objOutlook.numRelationShipId = numRelationShipId
            objOutlook.numProfileId = numProfileId
            objOutlook.bitGroupBySubjectThread = bitGroupBySubjectThread
            objOutlook.vcFollowupStatusIds = vcFollowupStatusIds
            objOutlook.ToEmail = ""
            objOutlook.DomainID = HttpContext.Current.Session("DomainId")
            objOutlook.columnName = "emailhistory.dtReceivedOn"
            objOutlook.columnSortOrder = "Desc"
            objOutlook.RegularSearchCriteria = RegularSearchCriteria
            objOutlook.CustomSearchCriteria = CustomSearchCriteria


            objOutlook.chrSource = "I"
            objOutlook.NodeId = NodeId
            objOutlook.UserCntID = HttpContext.Current.Session("UserContactId")
            objOutlook.ClientTimeZoneOffset = HttpContext.Current.Session("ClientMachineUTCTimeOffset")
            dsEmail = objOutlook.getInbox()
            dtTable = dsEmail.Tables(1)
            Dim json As String = String.Empty
            json = JsonConvert.SerializeObject(dtTable, Formatting.None)
            Return json
        End Function

        <WebMethod()>
        Public Shared Function WebMethodCheckAnyNewMessage(ByVal NodeId As Long) As String
            Try
                Dim objEmail = New Email
                Dim strOutput As String
                strOutput = ""
                strOutput = objEmail.GetAnyNewMessage(CCommon.ToLong(HttpContext.Current.Session("UserContactID")), CCommon.ToLong(HttpContext.Current.Session("DomainID")), NodeId)

                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(strOutput, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function
        <WebMethod()>
        Public Shared Function WebMethodUpdateReadUnreadStatus(ByVal strEmailHstrId As String, ByVal isRead As Boolean) As String
            Try
                Dim objEmail = New Email
                Dim strOutput As String
                strOutput = "1"
                objEmail.UpdateReadStatus(strEmailHstrId, CCommon.ToLong(HttpContext.Current.Session("DomainID")), isRead)

                Dim json As String = String.Empty
                json = JsonConvert.SerializeObject(strOutput, Formatting.None)
                Return json
            Catch ex As Exception
                Dim strError As String = ""

                strError = ex.Message
                Throw New Exception(strError)
                'Throw ex
            End Try
        End Function
    End Class

    Public Class MyTemp : Implements ITemplate

        Dim TemplateType As ListItemType
        Dim ColumnName, FieldName, DBColumnName, AllowSorting, FormFieldId, AllowEdit, ControlType As String
        Dim AllowFiltering As Boolean
        Dim Custom As Boolean
        Dim EditPermission As Integer

        Dim htGridColumnSearch As Hashtable
        Sub New(ByVal type As ListItemType, ByVal fld1 As String, ByVal EPermission As Integer, GridColumnSearch As Hashtable)
            Try
                TemplateType = type
                ColumnName = fld1

                Dim str As String()
                str = ColumnName.Split("~")

                FieldName = str(0)
                DBColumnName = str(1)
                AllowSorting = str(2)
                FormFieldId = str(3)
                AllowEdit = str(4)
                Custom = str(5)
                ControlType = str(6)
                htGridColumnSearch = GridColumnSearch
                If (str.Length > 7) Then
                    AllowFiltering = str(7)
                End If
                EditPermission = EPermission
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub InstantiateIn(ByVal Container As Control) Implements ITemplate.InstantiateIn
            Try
                Dim lbl1 As Label = New Label()
                Dim lbl2 As Label = New Label()
                Dim lbl3 As Label = New Label()
                Dim hdn1 As New HiddenField
                Dim hdn2 As New HiddenField
                Dim hdn3 As New HiddenField
                Dim ph1 As New PlaceHolder
                Dim lnkButton As New LinkButton
                Dim lnk As New HyperLink
                Dim ddlEmailHistory As New DropDownList
                Dim txtSearch As TextBox = New TextBox()
                Dim ddlSearch As DropDownList = New DropDownList()
                Select Case TemplateType
                    Case ListItemType.Header
                        If ControlType <> "DeleteCheckBox" Then

                            If DBColumnName = "bitIsRead" Then
                                lbl1.Text = "<img src='../images/hdr_message.gif' title='Read/Unread' />"
                                Container.Controls.Add(lbl1)
                            ElseIf DBColumnName = "bitHasAttachments" Then
                                lbl1.Text = "<img src='../images/hdr_attachment.gif' title='Has Attachment' />"
                                Container.Controls.Add(lbl1)
                            ElseIf DBColumnName = "numContactID" Then
                            ElseIf ControlType <> "HiddenField" AndAlso DBColumnName <> "RecentCorrespondance" AndAlso DBColumnName <> "AlertPanel" AndAlso DBColumnName <> "dtReceivedOn" AndAlso DBColumnName <> "bintCreatedOn" Then
                                lbl1.ID = DBColumnName
                                lbl1.Text = FieldName
                                lbl1.ForeColor = Color.Black
                                'lnkButton.Attributes.Add("onclick", "return SortColumn('" & DBColumnName & "')")
                                Container.Controls.Add(lbl1)
                            ElseIf DBColumnName = "AlertPanel" Then
                                lbl1.Text = "Alert Panel"     'Changed by Priya(18Jan 2018)
                                Container.Controls.Add(lbl1)
                            ElseIf DBColumnName = "dtReceivedOn" Then
                                lbl1.Text = "<a href='#' onclick='sortOrder()'>Created</a>"     'Added by Priya(15Feb 2018)
                                Container.Controls.Add(lbl1)
                            ElseIf DBColumnName = "bintCreatedOn" Then
                                lbl1.Text = "Created"     'Added by Priya(15Feb 2018)
                                Container.Controls.Add(lbl1)
                            End If
                            If AllowFiltering = True Then
                                If ControlType = "TextArea" Or ControlType = "TextBox" Then
                                    Container.Controls.Add(New LiteralControl("<br />"))
                                    txtSearch.ClientIDMode = ClientIDMode.Static
                                    txtSearch.ID = ColumnName & "~" & "TextBox"
                                    Dim strAutoCompleteClass As String = ""

                                    If DBColumnName = "vcSubject" Then
                                        txtSearch.CssClass = "txtSelect2Subject"
                                    ElseIf DBColumnName = "vcFrom" Then
                                        txtSearch.CssClass = "txtFromAutoSuggestEmail"
                                        txtSearch.Attributes.Add("autocomplete", "off")
                                    ElseIf DBColumnName = "vcTo" Then
                                        txtSearch.CssClass = "txtToAutoSuggestEmail"
                                        txtSearch.Attributes.Add("autocomplete", "off")
                                    Else
                                        txtSearch.CssClass = "signup"
                                    End If
                                    txtSearch.Width = Unit.Percentage(92)
                                    If htGridColumnSearch.ContainsKey(txtSearch.ID) Then
                                        txtSearch.Text = htGridColumnSearch(txtSearch.ID).ToString
                                    End If

                                    Container.Controls.Add(txtSearch)
                                    If DBColumnName = "vcFrom" Then
                                        Container.Controls.Add(New LiteralControl("<div id='txtFromAutoSuggestEmailautocomplete-list' class='autocomplete-items' style='display: none'><div>No record found</div></div>"))
                                    End If
                                    If DBColumnName = "vcTo" Then
                                        Container.Controls.Add(New LiteralControl("<div id='txtToAutoSuggestEmailautocomplete-list' class='autocomplete-items' style='display: none'><div>No record found</div></div>"))
                                    End If
                                ElseIf ControlType = "DateField" Then
                                    Container.Controls.Add(New LiteralControl("<br />"))

                                    Dim table As New System.Web.UI.HtmlControls.HtmlGenericControl("table")
                                    Dim tr As New System.Web.UI.HtmlControls.HtmlGenericControl("tr")

                                    Dim td1 As New System.Web.UI.HtmlControls.HtmlGenericControl("td")
                                    Dim td2 As New System.Web.UI.HtmlControls.HtmlGenericControl("td")
                                    'td2.Style.Add("padding-left", "5px")

                                    'td1.InnerHtml = "From<br/>"
                                    'td2.InnerHtml = "To<br/>"

                                    Dim dtFrom As New TextBox
                                    dtFrom.ID = ColumnName & "~" & ControlType & "~From"
                                    dtFrom.ClientIDMode = ClientIDMode.Static
                                    dtFrom.Attributes.Add("placeholder", "From")
                                    dtFrom.Width = New Unit(80, UnitType.Pixel)
                                    If htGridColumnSearch.ContainsKey(dtFrom.ID) Then
                                        dtFrom.Text = htGridColumnSearch(dtFrom.ID).ToString
                                    End If
                                    td1.Controls.Add(dtFrom)

                                    Dim ajaxFrom As New AjaxControlToolkit.CalendarExtender()
                                    ajaxFrom.TargetControlID = dtFrom.ID
                                    ajaxFrom.Format = "MM/dd/yyyy"
                                    td1.Controls.Add(ajaxFrom)

                                    Dim dtTo As New TextBox
                                    dtTo.ClientIDMode = ClientIDMode.Static
                                    dtTo.Attributes.Add("placeholder", "To")
                                    dtTo.ID = ColumnName & "~" & ControlType & "~To"
                                    dtTo.Width = New Unit(80, UnitType.Pixel)
                                    If htGridColumnSearch.ContainsKey(dtTo.ID) Then
                                        dtTo.Text = htGridColumnSearch(dtTo.ID).ToString
                                    End If
                                    td2.Controls.Add(dtTo)

                                    Dim ajaxTo As New AjaxControlToolkit.CalendarExtender()
                                    ajaxTo.TargetControlID = dtTo.ID
                                    ajaxTo.Format = "MM/dd/yyyy"
                                    td1.Controls.Add(ajaxTo)

                                    tr.Controls.Add(td1)
                                    tr.Controls.Add(td2)

                                    table.Controls.Add(tr)

                                    Container.Controls.Add(table)
                                End If
                            End If
                        Else
                            Dim chk As New CheckBox
                            chk.ID = "chk"
                            'chk.Attributes.Add("onclick", "return SelectAll('EmailBox1_gvInbox_ctl01_chk')") '
                            Container.Controls.Add(chk)
                        End If
                    Case ListItemType.Item
                        If ControlType <> "DeleteCheckBox" AndAlso ControlType <> "SelectBox" AndAlso ControlType <> "HiddenField" Then
                            If DBColumnName <> "vcBodyText" AndAlso DBColumnName <> "RecentCorrespondance" AndAlso DBColumnName <> "AlertPanel" Then
                                AddHandler lbl1.DataBinding, AddressOf BindStringColumn
                                ' lbl1.ID = "BodyText1"
                                'If DBColumnName = "vcSubject" Then
                                lbl1.ID = DBColumnName
                                'End If
                                Container.Controls.Add(lbl1)
                                'Else
                                'AddHandler hdn1.DataBinding, AddressOf BindBodyText
                                ' hdn1.ID = "hdnBodyText"
                                ' Container.Controls.Add(hdn1)
                            End If
                        ElseIf ControlType = "SelectBox" AndAlso DBColumnName = "vcData" Then
                            ddlEmailHistory.ID = "ddlEmailHistory"
                            ddlEmailHistory.CssClass = "target"
                            ddlEmailHistory.Style.Add("border", "0px")
                            Container.Controls.Add(ddlEmailHistory)
                        ElseIf ControlType <> "HiddenField" Then
                            Dim chk As New CheckBox
                            chk.ID = "chk"
                            Container.Controls.Add(chk)
                        End If

                        If ControlType = "Image" AndAlso DBColumnName = "bitIsRead" Then
                            AddHandler hdn1.DataBinding, AddressOf BindReadValueStatus
                            hdn1.ID = "hdnIsread"
                            Container.Controls.Add(hdn1)

                            AddHandler hdn1.DataBinding, AddressOf BindKeyValue
                            hdn1.ID = "numEmailHstrID"
                            Container.Controls.Add(hdn1)

                            hdn2.ID = "hdnContactId"
                            ph1.Controls.Add(hdn2)
                            AddHandler ph1.DataBinding, AddressOf BindNewActionItem
                            Container.Controls.Add(ph1)


                            AddHandler hdn3.DataBinding, AddressOf BindNumOfTimeOpened
                            hdn3.ID = "hdnnumNoofTimes"
                            Container.Controls.Add(hdn3)
                        End If
                        If ControlType = "HiddenField" AndAlso DBColumnName = "numEmailHstrID" Then
                            AddHandler hdn1.DataBinding, AddressOf BindKeyValue
                            hdn1.ID = "numEmailHstrID"
                            Container.Controls.Add(hdn1)
                        End If
                        If ControlType = "HiddenField" AndAlso DBColumnName = "numContactID" Then
                            hdn1.ID = "hdnContactId"
                            ph1.Controls.Add(hdn1)
                            AddHandler ph1.DataBinding, AddressOf BindNewActionItem
                            Container.Controls.Add(ph1)
                        End If
                        If ControlType = "TextBox" AndAlso DBColumnName = "AlertPanel" Then
                            AddHandler lbl1.DataBinding, AddressOf BindAlertMsg
                            lbl1.ID = "lblAlertPanel"
                            lbl1.ForeColor = Color.Red
                            lbl1.CssClass = "Alert"
                            Container.Controls.Add(lbl1)
                        End If
                        If ControlType = "HiddenField" AndAlso DBColumnName = "numNoofTimes" Then
                            AddHandler hdn1.DataBinding, AddressOf BindNumOfTimeOpened
                            hdn1.ID = "hdnnumNoofTimes"
                            Container.Controls.Add(hdn1)
                        End If
                End Select
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Sub BindAlertMsg(ByVal sender As Object, ByVal e As EventArgs)
            Dim lbl As Label = CType(sender, Label)
            Dim ControlClass As String = ""

            Dim Container As GridViewRow = CType(lbl.NamingContainer, GridViewRow)
            Dim rowView As DataRowView = CType(Container.DataItem, DataRowView)
            If DBColumnName = "AlertPanel" Then

                lbl.Text = DataBinder.Eval(Container.DataItem, ColumnName).ToString
            End If
        End Sub

        Sub BindKeyValue(ByVal sender As Object, ByVal e As EventArgs)
            Dim hdnListItemId As HiddenField = CType(sender, HiddenField)
            Dim ControlClass As String = ""

            Dim Container As GridViewRow = CType(hdnListItemId.NamingContainer, GridViewRow)
            hdnListItemId.Value = DataBinder.Eval(Container.DataItem, "numEmailHstrID").ToString
        End Sub

        Sub BindNewActionItem(ByVal sender As Object, ByVal e As EventArgs)
            Dim ph As PlaceHolder = CType(sender, PlaceHolder)
            Dim ControlClass As String = ""

            Dim Container As GridViewRow = CType(ph.NamingContainer, GridViewRow)

            CType(ph.FindControl("hdnContactId"), HiddenField).Value = DataBinder.Eval(Container.DataItem, "numContactId").ToString
        End Sub

        Sub BindNumOfTimeOpened(ByVal sender As Object, ByVal e As EventArgs)
            Dim hdnListItemId As HiddenField = CType(sender, HiddenField)
            Dim ControlClass As String = ""

            Dim Container As GridViewRow = CType(hdnListItemId.NamingContainer, GridViewRow)
            hdnListItemId.Value = DataBinder.Eval(Container.DataItem, "numNoofTimes").ToString
        End Sub

        Sub BindReadValueStatus(ByVal sender As Object, ByVal e As EventArgs)
            Dim hdnListItemId As HiddenField = CType(sender, HiddenField)
            Dim ControlClass As String = ""

            Dim Container As GridViewRow = CType(hdnListItemId.NamingContainer, GridViewRow)
            If DBColumnName = "bitIsRead" Then
                hdnListItemId.Value = DataBinder.Eval(Container.DataItem, ColumnName).ToString
            End If
        End Sub

        Sub BindBodyText(ByVal sender As Object, ByVal e As EventArgs)
            Dim hdnListItemId As HiddenField = CType(sender, HiddenField)
            Dim ControlClass As String = ""

            Dim Container As GridViewRow = CType(hdnListItemId.NamingContainer, GridViewRow)
            If DBColumnName = "vcBodyText" Then
                hdnListItemId.Value = DataBinder.Eval(Container.DataItem, ColumnName).ToString
            End If
        End Sub

        Sub BindnumListItem(ByVal sender As Object, ByVal e As EventArgs)
            Dim hdnListItemId As HiddenField = CType(sender, HiddenField)
            Dim ControlClass As String = ""

            Dim Container As GridViewRow = CType(hdnListItemId.NamingContainer, GridViewRow)
            If DBColumnName = "numListItemId" Then
                hdnListItemId.Value = DataBinder.Eval(Container.DataItem, ColumnName).ToString
            End If
        End Sub

        Function SetBytes(ByVal Bytes) As String
            Try
                If Bytes >= 1073741824 Then
                    SetBytes = Format(Bytes / 1024 / 1024 / 1024, "#0.00") & " GB"
                ElseIf Bytes >= 1048576 Then
                    SetBytes = Format(Bytes / 1024 / 1024, "#0.00") & " MB"
                ElseIf Bytes >= 1024 Then
                    SetBytes = Format(Bytes / 1024, "#0.00") & " KB"
                ElseIf Bytes < 1024 Then
                    SetBytes = Fix(Bytes) & " B"
                End If
                Exit Function
            Catch ex As Exception
                Throw
            End Try
        End Function

        Sub BindStringColumn(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim ControlClass As String = ""

                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
                If DBColumnName = "vcFrom" Then
                    Dim str As String() = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName)), "", DataBinder.Eval(Container.DataItem, ColumnName)).ToString.Split(New String() {"$^$"}, StringSplitOptions.None)
                    If str.Length > 1 Then
                        lbl1.Text = IIf(str(1).ToString = "", str(2).ToString, str(1).ToString)
                    Else
                        lbl1.Text = str(0).ToString
                    End If
                Else
                    If ControlType = "CheckBox" Then
                        lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName)), "", IIf(DataBinder.Eval(Container.DataItem, ColumnName), "Yes", "No"))
                    Else
                        If DBColumnName = "vcSize" Then
                            lbl1.Text = SetBytes(IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName)), "0", DataBinder.Eval(Container.DataItem, ColumnName)))
                        ElseIf DBColumnName = "vcSubject" Then
                            lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName)), "", DataBinder.Eval(Container.DataItem, ColumnName))
                        Else
                            lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName)), "", DataBinder.Eval(Container.DataItem, ColumnName)).ToString()
                        End If
                    End If
                End If

                If DBColumnName = "vcTo" Then
                    Dim str As String() = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName)), "", DataBinder.Eval(Container.DataItem, ColumnName)).ToString.Split(New String() {"$^$"}, StringSplitOptions.None)
                    If str.Length > 1 Then
                        lbl1.Text = IIf(str(1).ToString = "", str(2).ToString, str(1).ToString)
                    Else
                        lbl1.Text = str(0).ToString
                    End If
                End If

                If DBColumnName = "bitIsRead" Then
                    Dim strReplyAll As String = ""
                    Dim strSubject As String = ""
                    strSubject = "RE: " & HttpUtility.UrlEncode(DataBinder.Eval(Container.DataItem, "vcSubject").ToString)
                    strReplyAll = "<a href='#' class='btn btn-xs btn-primary btnReplyAll' onclick=""fnReply('', '" & strSubject & "','" & DataBinder.Eval(Container.DataItem, "numEmailHstrID").ToString & "',0,'replyall',1)"">Reply all</a>"
                    'If CCommon.ToBool(DataBinder.Eval(Container.DataItem, ColumnName)) = True Then
                    '    lbl1.Text = "<img src='../images/msg_read.gif' height=""14px"" width=""16px""  />" & strReplyAll
                    'Else
                    '    lbl1.Text = "<img src='../images/msg_unread.gif' height=""14px"" width=""16px"" />" & strReplyAll
                    'End If
                    lbl1.Text = strReplyAll
                    lbl1.CssClass = "ReplyMSGStatus"
                End If

                If DBColumnName = "IsReplied" Then
                    If CCommon.ToBool(DataBinder.Eval(Container.DataItem, ColumnName)) = True Then
                        lbl1.Text = "<img src='../images/reply.png' height=""14px"" width=""16px""  />"
                    Else
                        lbl1.Text = ""
                    End If
                End If

                If DBColumnName = "bitHasAttachments" Then
                    If CCommon.ToBool(DataBinder.Eval(Container.DataItem, ColumnName)) = True Then
                        lbl1.Text = "<img src='../images/hdr_attachment.gif' align=""BASELINE"" style=""float:none;""/>"
                    Else
                        lbl1.Text = ""
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub BindStringColumn1(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
            Catch ex As Exception
                Throw
            End Try
        End Sub



    End Class
End Namespace