﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Outlook

Public Class AlertDetail
    Inherits BACRMPage

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                hdnAlterType.Value = CCommon.ToString(GetQueryStringVal("numAlertType"))
                hdnDivisionID.Value = CCommon.ToString(GetQueryStringVal("numDivisionID"))
                hdnContactID.Value = CCommon.ToString(GetQueryStringVal("ContactId"))

                Select Case hdnAlterType.Value
                    Case "1"
                        lblPageTitle.Text = "Open A/R & A/P records"
                    Case "2"
                        lblPageTitle.Text = "Open Sales & Purchase Orders"
                    Case "3"
                        lblPageTitle.Text = "Open Cases"
                    Case "4"
                        lblPageTitle.Text = "Open Projects"
                    Case "5"
                        lblPageTitle.Text = "Open Action Items"
                End Select

                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try    
    End Sub
#End Region

#Region "Private Methods"
    Private Sub BindData()
        Try
            Dim dt As New DataTable
            Dim objOutlook As New COutlook
            objOutlook.DomainID = CCommon.ToLong(Session("DomainID"))
            objOutlook.DivisionID = CCommon.ToLong(hdnDivisionID.Value)
            dt = objOutlook.GetAlertDetail(CCommon.ToInteger(hdnAlterType.Value), CCommon.ToLong(hdnContactID.Value))


            rgAlertDetail.DataSource = dt
            rgAlertDetail.DataBind()

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#End Region

End Class