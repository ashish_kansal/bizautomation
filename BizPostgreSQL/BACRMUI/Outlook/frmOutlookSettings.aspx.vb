﻿Imports BACRM.BusinessLogic.Common
Public Class frmOutlookSettings
    Inherits System.Web.UI.Page
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            If Not IsPostBack Then
                Session("Help") = Request.Url.Segments(Request.Url.Segments.Length - 1)
                oEditHtml.Content = Session("Signature")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Dim objCommon As New CCommon
            objCommon.UserID = Session("UserID")
            objCommon.Signature = oEditHtml.Content
            objCommon.UpdateSignature()
            Session("Signature") = oEditHtml.Content
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub
End Class