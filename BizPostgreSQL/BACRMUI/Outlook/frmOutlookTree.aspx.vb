Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Common
Imports MailBee.ImapMail

Namespace BACRM.UserInterface.Outlook
    Partial Public Class frmOutlookTree : Inherits BACRMPage

        Dim objOutlook As COutlook

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                btnAdd.Attributes.Add("onclick", "return CheckName()")
                btnDelete.Attributes.Add("onclick", "return CheckSelectedNode()")
                btnMove.Attributes.Add("onclick", "return CheckNode()")
                btnCopy.Attributes.Add("onclick", "return CheckNode()")
                If Not IsPostBack Then
                    If GetQueryStringVal("frm") = "Move" Then
                        pnlMove.Visible = True
                        pnlTree.Visible = False
                    Else
                        pnlMove.Visible = False
                        pnlTree.Visible = True
                    End If
                    BindTree()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Public Sub BindTree()
            Try
                RadTreeView1.Nodes.Clear()
                objOutlook = New COutlook
                Dim ds As DataSet = New DataSet()
                Dim dtTable As DataTable
                objOutlook.UserCntID = Session("UserContactId")
                objOutlook.DomainID = Session("DomainId")
                objOutlook.ModeType = 0
                objOutlook.NodeId = 0
                objOutlook.ParentId = 0
                objOutlook.NodeName = ""
                ds = objOutlook.GetTreeOrderList()

                'Commented By Sachin || Case No:516
                'Dim dr1() As DataRow = dtTable.Select(" numNodeID=190 ")
                'dr1(0).Delete()
                ds.Relations.Add("NodeRelation", ds.Tables(0).Columns("numNodeID"), ds.Tables(0).Columns("numParentID"))

                For Each dbRow As DataRow In ds.Tables(0).Rows
                    If dbRow.IsNull("numParentID") Then
                        Dim node As RadTreeNode = CreateNode(dbRow("vcNodeName").ToString(), True, dbRow("numSortOrder").ToString(), dbRow("numNodeID").ToString(), CCommon.ToBool(dbRow("bitSystem")), CCommon.ToLong(dbRow("numFixID")))
                        RadTreeView1.Nodes.Add(node)
                        RecursivelyPopulate(dbRow, node)
                    End If
                Next
                'dtTable.AcceptChanges()
                'End of Comment By Sachin

                'Dim treeNodeA As RadTreeNode
                ''treeNodeA = New RadTreeNode
                ''treeNodeA.Text = Session("ContactName")
                ''treeNodeA.Value = "0"
                ''treeNodeA.ImageUrl = "../images/root.gif"
                ''RadTreeView1.Nodes.Add(treeNodeA)
                ''RadTreeView1.Nodes(0).Expanded = True
                'For Each dr As DataRow In dtTable.Rows
                '    treeNodeA = New RadTreeNode
                '    treeNodeA.Text = dr("vcNodeName")
                '    treeNodeA.Value = dr("numNodeID").ToString
                '    Dim nodeid As Long = dr.Item("numNodeID")
                '    'Added By Sachin || case no:502
                '    Dim nodeName As String = dr.Item("vcNodeName")
                '    'End of addition by Sachin
                '    'Changed by sachin-from nodeid to nodeName
                '    Select Case nodeName
                '        Case "Inbox" : treeNodeA.ImageUrl = "../images/inbox.gif"
                '        Case "Deleted Mails" : treeNodeA.ImageUrl = "../images/deleted.gif"
                '        Case "Sent Mails" : treeNodeA.ImageUrl = "../images/sentitems.gif"
                '        Case "Settings" : treeNodeA.ImageUrl = "../images/Settings.png"
                '        Case Else
                '            treeNodeA.ImageUrl = "../images/folder.gif"
                '    End Select
                '    If nodeid <> 5 Then 'Don't show Calender node
                '        If Not RadTreeView1.FindNodeByValue(dr.Item("numParentID").ToString) Is Nothing Then
                '            RadTreeView1.FindNodeByValue(dr.Item("numParentID").ToString).Nodes.Add(treeNodeA)
                '        Else
                '            RadTreeView1.Nodes.Add(treeNodeA)
                '        End If
                '    End If
                'Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        'Added By Sachin Sadhu for Ordering MailBox :Fetching Child node  recursively ||Case No:516 ||Date:7th-dec-2013
        Private Sub RecursivelyPopulate(dbRow As DataRow, node As RadTreeNode)
            For Each childRow As DataRow In dbRow.GetChildRows("NodeRelation")
                Dim childNode As RadTreeNode = CreateNode(childRow("vcNodeName").ToString(), True, childRow("numSortOrder").ToString(), childRow("numNodeID").ToString(), CCommon.ToBool(childRow("bitSystem").ToString()), CCommon.ToLong(childRow("numFixID")))
                node.Nodes.Add(childNode)
                RecursivelyPopulate(childRow, childNode)
            Next
        End Sub
        'Added By Sachin Sadhu for Ordering MailBox :Creating node  ||Case No:516 ||Date:7th-dec-2013
        Private Function CreateNode(text As String, expanded As Boolean, order As String, id As String, bitSystem As Boolean, numFixID As Long) As RadTreeNode
            Dim node As New RadTreeNode(text, id)
            node.Attributes("numNodeID") = id
            node.Attributes("numFixID") = numFixID
            node.Attributes("isSystemFolder") = bitSystem
            If (text = "Inbox") Then
                node.ImageUrl = "../images/inbox.gif"
            ElseIf (text = "Email Archive") Then
                node.ImageUrl = "../images/deleted.gif"
            ElseIf (text = "Sent Messages") Then
                node.ImageUrl = "../images/sentitems.gif"
            ElseIf (text = "Settings") Then
                node.ImageUrl = "../images/Settings.png"
            ElseIf (text = "Calendar") Then
                node.ImageUrl = "../images/calendar.gif"
            Else
                node.ImageUrl = "../images/folder.gif"
            End If
            node.Expanded = expanded

            Return node
        End Function

        Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Try
                'Dim parentid As Long = 0
                'If Not RadTreeView1.SelectedNode Is Nothing Then
                '    parentid = RadTreeView1.SelectedNode.Tag
                'End If
                objOutlook = New COutlook
                objOutlook.UserCntID = Session("UserContactId")
                objOutlook.DomainID = Session("DomainId")
                objOutlook.ModeType = 1
                objOutlook.NodeId = 0
                objOutlook.ParentId = txtSelectedNode.Text
                objOutlook.NodeName = txtName.Text
                objOutlook.GetTree()
                'Added By Rajesh it Create folder and it should get created in Gmail in realtime.||Date:27th-Nov-2014   
                objOutlook.Create_Folder(txtSelectedNode.Text, txtName.Text)
                txtName.Text = ""
                BindTree()
            Catch ex As Exception
                If ex.Message.Contains("FOLDER_ALREADY_EXISTS") Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "Error", "alert('The folder name you have chosen already exists. Please try another name');", True)
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Response.Write(ex)
                End If
            End Try
        End Sub

        Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
            Try
                Dim lngSelectedNode As Long = CCommon.ToLong(RadTreeView1.SelectedNode.Value)
                If RadTreeView1.SelectedNode.Attributes("isSystemFolder") = "True" Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "Error", "alert('Can not delete default node');", True)
                Else
                    objOutlook = New COutlook
                    objOutlook.UserCntID = Session("UserContactId")
                    objOutlook.DomainID = Session("DomainId")
                    objOutlook.ModeType = 2
                    objOutlook.NodeId = lngSelectedNode
                    objOutlook.NodeName = ""
                    objOutlook.ParentId = 0
                    objOutlook.GetTree()
                    objOutlook.DeleteFolder(txtSelectedNode.Text, txtSelectedFolder.Text)
                End If
                txtName.Text = ""
                BindTree()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
            Try
                Dim lngSelectedNode As Long = CCommon.ToLong(RadTreeView1.SelectedNode.Value)
                Dim numFixID As Long = CCommon.ToLong(RadTreeView1.SelectedNode.Attributes("numFixID"))
                If numFixID = 4 Or
                    numFixID = 5 Or
                    numFixID = 6 Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "Error", "alert('Can not copy to selected folder.');", True)
                Else

                    Dim strEmailId As String
                    Dim strEmailHstrId As String()
                    Dim intCount As Integer = 0
                    strEmailId = GetQueryStringVal("numEmailHstrId")
                    If strEmailId.Length > 0 Then
                        strEmailId = strEmailId.TrimEnd(",")
                        Dim objEmail As New Email
                        objEmail.MoveCopyGmail(strEmailId, GetQueryStringVal("sourceFolder"), txtSelectedFolder.Text, CCommon.ToInteger(txtSelectedNode.Text), 1)
                    End If

                    Response.Write("<script>window.opener.BindGrid(" & CCommon.ToLong(GetQueryStringVal("NodeId")) & ");self.close();</script>")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            Finally
                txtSelectedNode.Text = ""
                txtSelectedMails.Text = ""
                txtFixID.Text = ""
            End Try
        End Sub

        Private Sub btnMove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMove.Click
            Try
                Dim lngSelectedNode As Long = CCommon.ToLong(RadTreeView1.SelectedNode.Value)
                Dim numFixID As Long = CCommon.ToLong(RadTreeView1.SelectedNode.Attributes("numFixID"))
                If numFixID = 4 Or
                    numFixID = 5 Or
                    numFixID = 6 Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "Error", "alert('Can not move to selected folder.');", True)
                Else
                    objOutlook = New COutlook
                    Dim strEmailId As String
                    Dim strEmailHstrId As String()
                    Dim intCount As Integer = 0
                    strEmailId = GetQueryStringVal("numEmailHstrId")
                    If strEmailId.Length > 0 Then
                        strEmailId = strEmailId.TrimEnd(",")
                        Dim objEmail As New Email
                        objEmail.MoveCopyGmail(strEmailId, GetQueryStringVal("sourceFolder"), txtSelectedFolder.Text, CCommon.ToInteger(txtSelectedNode.Text), 0)
                    End If
                    strEmailHstrId = strEmailId.Split(",")

                    Response.Write("<script>window.opener.BindGrid(" & CCommon.ToLong(GetQueryStringVal("NodeId")) & ");self.close();</script>")
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            Finally
                txtSelectedNode.Text = ""
                txtSelectedMails.Text = ""
                txtFixID.Text = ""
            End Try
        End Sub

        Private Sub btnRename_Click(sender As Object, e As EventArgs) Handles btnRename.Click
            Try
                Dim lngSelectedNode As Long = CCommon.ToLong(RadTreeView1.SelectedNode.Value)
                Dim numFixID As Long = CCommon.ToLong(RadTreeView1.SelectedNode.Attributes("numFixID"))
                If numFixID = 2 Or
                    numFixID = 4 Or
                    numFixID = 5 Or
                    numFixID = 6 Or
                    numFixID = 7 Or
                    numFixID = 190 Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "Error", "alert('System folders can not be renamed.');", True)
                ElseIf Not String.IsNullOrEmpty(txtName.Text) Then
                    objOutlook = New COutlook
                    objOutlook.DomainID = CCommon.ToLong(Session("DomainID"))
                    objOutlook.UserCntID = CCommon.ToLong(Session("UserContactID"))
                    objOutlook.NodeId = lngSelectedNode
                    objOutlook.RenameFolder(txtName.Text.Trim())
                    BindTree()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            Finally
                txtName.Text = "Enter Folder Name"
                txtSelectedNode.Text = ""
                txtSelectedMails.Text = ""
                txtFixID.Text = ""
            End Try
        End Sub
    End Class
End Namespace
