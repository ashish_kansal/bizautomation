﻿'Imports Infragistics.WebUI.UltraWebNavigator
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Common
Partial Public Class InboxTree
    Inherits BACRMUserControl

    Dim objOutlook As COutlook
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                BindTree()
                btnCompose.Attributes.Add("onclick", "return Compose()")
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Public Sub BindTree()
        Try
            RadTreeView1.Nodes.Clear()
            objOutlook = New COutlook
            Dim dtTable As DataTable
            Dim ds As DataSet = New DataSet()
            objOutlook.UserCntID = Session("UserContactId")
            objOutlook.DomainId = Session("DomainId")
            objOutlook.ModeType = 0
            objOutlook.NodeId = 0
            objOutlook.ParentId = 0
            objOutlook.NodeName = ""
            ds = objOutlook.GetTreeOrderList()

            ds.Relations.Add("NodeRelation", ds.Tables(0).Columns("numNodeID"), ds.Tables(0).Columns("numParentID"))

            For Each dbRow As DataRow In ds.Tables(0).Rows
                If dbRow.IsNull("numParentID") Then
                    Dim node As RadTreeNode = CreateNode(dbRow("vcNodeName").ToString(), True, dbRow("numSortOrder").ToString(), dbRow("numNodeID").ToString(), CCommon.ToLong(dbRow("numFixID")))
                    RadTreeView1.Nodes.Add(node)
                    RecursivelyPopulate(dbRow, node)
                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    'Added By Sachin Sadhu for Ordering MailBox :Fetching Child recursively  ||Case No:516 ||Date:7th-dec-2013
    Private Sub RecursivelyPopulate(dbRow As DataRow, node As RadTreeNode)
        For Each childRow As DataRow In dbRow.GetChildRows("NodeRelation")
            Dim childNode As RadTreeNode = CreateNode(childRow("vcNodeName").ToString(), True, childRow("numSortOrder").ToString(), childRow("numNodeID").ToString(), CCommon.ToLong(childRow("numFixID")))
            node.Nodes.Add(childNode)
            RecursivelyPopulate(childRow, childNode)
        Next
    End Sub
    Protected Sub RadTreeView1_NodeClick(ByVal sender As Object, ByVal e As RadTreeNodeEventArgs)
        Response.Redirect("~/Outlook/frmInboxItems.aspx?nodeId=" & RadTreeView1.SelectedNode.Value & "")
    End Sub
    'Added By Sachin Sadhu for Ordering MailBox :Creating node  ||Case No:516 ||Date:7th-dec-2013
    Private Function CreateNode(text As String, expanded As Boolean, order As String, id As String, numFixID As Long) As RadTreeNode
        Dim node As New RadTreeNode(text, order)
        node.Attributes("numNodeID") = id
        If (GetQueryStringVal("node") = id) Then
            node.Selected = True
        End If
        node.Value = id
        If (text = "Inbox") Then
            node.Attributes.Add("onclick", "BindGridNode(" & node.Attributes("numNodeID").ToString & ",'Inbox'," & numFixID & ");")
            node.Font.Bold = True
            If CCommon.ToInteger(GetQueryStringVal("node")) = 0 Then
                node.Selected = True
                node.Attributes.Add("class", "nodeSelected")
            End If
            'node.Text = "<i class=""fa fa-inbox""></i>&nbsp;&nbsp;" & node.Text & "<a href='#' class='btn btn-xs btn-primary' style='float:right' onclick='return GetNewMessage();'><i class='fa fa-refresh'></i></a>"
            node.Text = "<img src='../images/Icon/iconFolders.png' class='sideIcon' />&nbsp;&nbsp;" & node.Text
        ElseIf (text = "Email Archive") Then
            node.Attributes.Add("onclick", "BindGridNode(" & node.Attributes("numNodeID").ToString & "," & HttpUtility.JavaScriptStringEncode(node.Text, True) & "," & numFixID & ");")
            node.Font.Bold = True
            'node.Text = "<i class=""fa fa-archive""></i>&nbsp;&nbsp;Email Archive"
            node.Text = "<img src='../images/Icon/iconFolders.png' class='sideIcon' />&nbsp;&nbsp;Email Archive"
        ElseIf (text = "Sent Messages") Then
            node.Attributes.Add("onclick", "BindGridNode(" & node.Attributes("numNodeID").ToString & "," & HttpUtility.JavaScriptStringEncode(node.Text, True) & "," & numFixID & ");")
            node.Font.Bold = True
            'node.Text = "<i class=""fa fa-envelope-o""></i>&nbsp;&nbsp;Sent Messages"
            node.Text = "<img src='../images/Icon/iconFolders.png' class='sideIcon' />&nbsp;&nbsp;Sent Messages"
        ElseIf (text = "Calendar") Then
            node.Font.Bold = True
            node.Attributes.Add("onclick", "BindCalender(" & node.Attributes("numNodeID").ToString & ");")
            'node.Text = "<i class=""fa fa-calendar""></i>&nbsp;&nbsp;Calendar"
            node.Text = "<img src='../images/Icon/iconFolders.png' class='sideIcon' />&nbsp;&nbsp;Calendar"
        ElseIf (text = "Custom Folders") Then
            node.Attributes.Add("onclick", "void(0);")
            'node.Text = "<i class=""fa fa-folder""></i>&nbsp;&nbsp;" & "<b>Custom Folders</b><a href='#' onclick='return OpenTree();' class='btn btn-xs btn-primary pull-right'><i class='fa fa-pencil'></i></a>"
            node.Text = "<img src='../images/Icon/iconFolders.png' class='sideIcon' />&nbsp;&nbsp;" & "<b>Custom Folders</b><a href='#' onclick='return OpenTree();' class='btn btn-xs btn-primary pull-right'><i class='fa fa-pencil'></i></a>"
        ElseIf (text = "Settings") Then
            node.Font.Italic = True
            node.Attributes.Add("onclick", "BindOutlookSettings(" & node.Attributes("numNodeID").ToString & ");")
            node.Text = "<i class=""fa fa-gear""></i>&nbsp;&nbsp;Settings"
        Else
            node.Attributes.Add("onclick", "BindGridNode(" & node.Attributes("numNodeID").ToString & "," & HttpUtility.JavaScriptStringEncode(node.Text, True) & "," & numFixID & ");")
            'node.Text = "<i class=""fa fa-folder-o""></i>&nbsp;&nbsp;" & node.Text
            node.Text = "<img src='../images/Icon/iconFolders.png' class='sideIcon' />&nbsp;&nbsp;" & node.Text
        End If
        node.Expanded = expanded

        Return node
    End Function

    Private Sub btnGo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGo.Click
        Try
            BindTree()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class