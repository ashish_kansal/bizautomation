﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmOutlookSettings.aspx.vb" Inherits=".frmOutlookSettings" MasterPageFile="~/common/BizMaster.Master" %>

<%@ Register Src="../Outlook/InboxTree.ascx" TagName="InboxTree" TagPrefix="uc1" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<asp:Content ContentPlaceHolderID="head" runat="server">
    <title>Outlook Settings</title>
    <script type="text/javascript">
        function OpenSetting() {
            window.open('../Items/frmConfItemList.aspx?FormID=44', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false;
        }
        function ShowAlertConfig() {
            window.open('../admin/frmAlertConfig.aspx', '', 'toolbar=no,titlebar=no,left=200,top=250,width=580,height=448,scrollbars=yes,resizable=yes');
            return false;
        }
        function ManageInbox() {
            var win = window.open('../outlook/frmSortoutlooktree.aspx', 'OutlookTree', 'toolbar=no,titlebar=no,top=200,left=500,width=320,height=410,scrollbars=yes,resizable=yes');
            win.focus();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <div class="col-md-3">
            <uc1:InboxTree ID="InboxTree1" runat="server" />
        </div>
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Settings</h3>
                    <div class="box-tools pull-right">
                        <asp:Button ID="btnInboxColumn" runat="server" OnClientClick="return OpenSetting();" CssClass="btn btn-xs btn-primary" Text="Set Your Columns" />
                        <asp:Button ID="btnSort" runat="server" OnClientClick="return ManageInbox();" CssClass="btn btn-xs btn-primary" Text="Organise Your MailBox" />
                        <asp:Button ID="btnAlert" runat="server" OnClientClick="return ShowAlertConfig();" CssClass="btn btn-xs btn-primary" Text="Set Email Alert Configuration" />
                        <asp:Button ID="btnSave" runat="server" CssClass="btn btn-xs btn-primary" Text="Update Your Signature" />
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <telerik:RadEditor ID="oEditHtml" runat="server" Height="200px" Width="100%">
                                <Tools>
                                    <telerik:EditorToolGroup Tag="Top">
                                        <telerik:EditorTool Name="Bold" />
                                        <telerik:EditorTool Name="Italic" />
                                        <telerik:EditorTool Name="Underline" />
                                        <telerik:EditorTool Name="StrikeThrough" />
                                        <telerik:EditorSeparator Visible="true" />
                                        <telerik:EditorTool Name="JustifyLeft" />
                                        <telerik:EditorTool Name="JustifyCenter" />
                                        <telerik:EditorTool Name="JustifyRight" />
                                        <telerik:EditorTool Name="JustifyFull" />
                                        <telerik:EditorSeparator Visible="true" />
                                        <telerik:EditorTool Name="InsertOrderedList" />
                                        <telerik:EditorTool Name="InsertUnorderedList" />
                                        <telerik:EditorTool Name="Outdent" />
                                        <telerik:EditorTool Name="Indent" />
                                        <telerik:EditorSeparator Visible="true" />
                                        <telerik:EditorTool Name="FontName" />
                                        <telerik:EditorTool Name="FontSize" />
                                        <telerik:EditorTool Name="ForeColor" />
                                        <telerik:EditorTool Name="BackColor" />
                                    </telerik:EditorToolGroup>
                                </Tools>
                            </telerik:RadEditor>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

