﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="AlertDetail.aspx.vb" Inherits=".AlertDetail" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <asp:Button ID="btnClose" runat="server" Text="Close" OnClientClick="return Close();" CssClass="button" Style="float: right" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="lblPageTitle" runat="server" Text="Label"></asp:Label>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <div style="width: 850px">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <telerik:RadGrid ID="rgAlertDetail" runat="server" Width="100%">
            <HeaderStyle Font-Bold="true" />
            <MasterTableView>
                <NoRecordsTemplate>
                    <div>
                        There are no records to display
                    </div>
                </NoRecordsTemplate>
            </MasterTableView>
        </telerik:RadGrid>
    </div>
    <asp:HiddenField ID="hdnAlterType" runat="server" />
    <asp:HiddenField ID="hdnDivisionID" runat="server" />
    <asp:HiddenField ID="hdnContactID" runat="server" />
</asp:Content>
