<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmOutlookTree.aspx.vb"
    Inherits="BACRM.UserInterface.Outlook.frmOutlookTree" MasterPageFile="~/common/Popup.Master" %>

<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Outlook Folders</title>
    <script language="javascript" type="text/javascript">
        function CheckName() {
            if (document.getElementById('txtName').value == '') {
                alert('Enter folder name')
                return false
            }
            else {
                var tree = $find("RadTreeView1");
                var node = tree.get_selectedNode();
                if (node != null) {
                    var b = node.get_value();
                    document.getElementById('txtSelectedNode').value = b
                    //                    var a = node.get_Level();
                    //                    if (a >= 2) {
                    //                        alert('Can not Add more Subnodes to this node');
                    //                        return false
                    //                    }
                }
                else {
                    document.getElementById('txtSelectedNode').value = 0
                }
            }

        }

        function ClientNodeClicked(sender, eventArgs) {
            var node = eventArgs.get_node();
            document.getElementById('txtSelectedFolder').value = node.get_text();
            document.getElementById('txtSelectedNode').value = node.get_value();
            document.getElementById('txtFixID').value = node.get_attributes().getAttribute('numFixID');

            if (node.get_attributes().getAttribute('isSystemFolder') == "False") {
                document.getElementById('txtName').value = node.get_text();
                document.getElementById('btnRename').style.visibility = "visible";
            } else {
                document.getElementById('txtName').value = "Enter Folder Name";
                document.getElementById('btnRename').style.visibility = "hidden";
            }
        }

        function CheckSelectedNode() {
            var FolderName = document.getElementById('txtDelFolder').value;
            var tree = $find("RadTreeView1");
            var node = tree.get_selectedNode();

            if (node == null) {
                alert('Select a folder in tree')
                return false
            }
            else {

                if (node.get_attributes().getAttribute("isSystemFolder") == "True") {
                    alert('Can not delete default node');
                    return false
                }

            }

            return confirm("Deleting folder will move all emails to 'Deleted Mails' section, Do you want to continue?");
        }

        function CheckNode() {
            var tree = $find("RadTreeView1");
            var node = tree.get_selectedNode();
            if (node == null) {
                alert('Select a folder in tree')
                return false
            }
            else {
                var NodeId = document.getElementById('txtSelectedNode').value;
                var FolderName = document.getElementById('txtSelectedFolder').value;
                var fixID = document.getElementById('txtFixID').value;
                 
                //2	- Deleted Mails
                //4	- Sent Mails
                //5	- Calendar
                //6	- Folders
                //7 - Email Archive
                //190 - Settings
                if (fixID == 2 || fixID == 7 || fixID == 5 || fixID == 6 || fixID == 190 || fixID == 7) {
                    alert('Can not copy/move to selected folder.');
                    return false
                }
                else {
                    var Ids = ""
                    Ids = window.opener.GetSelectedMails()
                    if (Ids != "") {
                        document.getElementById('txtSelectedMails').value = Ids
                    }
                }
            }
        }

        function RenameFolder() {
            if (confirm('Folder name is used while copy/move message from one folder to another in gmail when mail is copied/moved in bizautomation. Do you want to Proceed?')) {
                return true;
            } else {
                document.getElementById('txtName').value = "Enter Folder Name";
                document.getElementById('btnRename').style.visibility = "hidden";
                return false;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnclose" runat="server" CssClass="button" OnClientClick="javascript:window.opener.location.reload(true);self.close();"
                Text="Close" />&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Outlook Folders
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager runat="server" ID="scManager" />
    <asp:Table runat="server" Width="600px" CssClass="aspTable" BorderWidth="1" BorderColor="black">
        <asp:TableRow VerticalAlign="Top">
            <asp:TableCell HorizontalAlign="left">
                <asp:Panel runat="server" ID="pnlTree">
                    &nbsp;
                    <asp:TextBox runat="server" ID="txtName" CssClass="signup" MaxLength="15" Text="Enter Folder Name"
                        onfocus="if (this.value == 'Enter Folder Name') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Enter Folder Name';}"></asp:TextBox>&nbsp;
                    <asp:Button runat="server" ID="btnAdd" Text="Add New Folder" CssClass="button" />&nbsp;
                    <asp:Button ID="btnDelete" runat="server" Text="Delete Selected" CssClass="button" />&nbsp;
                    <asp:Button ID="btnRename" runat="server" Text="Rename" CssClass="button" style="visibility:hidden" OnClientClick="return RenameFolder();" />
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlMove" CssClass="normal1" Width="300px">
                    <asp:Button runat="server" ID="btnMove" Text="Move" CssClass="button" />&nbsp;
                    <asp:Button ID="btnCopy" runat="server" Text="Copy" CssClass="button" />&nbsp;
                    <br />
                    <span style="float: left">Select a folder in tree</span>
                </asp:Panel>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow VerticalAlign="Top">
            <asp:TableCell HorizontalAlign="Center">
                <table width="100%">
                    <tr valign="top">
                        <td>
                            <telerik:RadTreeView ID="RadTreeView1" runat="server" AllowNodeEditing="false" Skin="Vista" OnClientNodeClicked="ClientNodeClicked"
                                ClientIDMode="Static">
                            </telerik:RadTreeView>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:TextBox runat="server" ID="txtDelFolder" Text="0" Style="display: none"></asp:TextBox>
    <asp:TextBox runat="server" ID="txtSelectedNode" Text="0" Style="display: none"></asp:TextBox>
     <asp:TextBox runat="server" ID="txtSelectedFolder" Text="0" Style="display: none"></asp:TextBox>
    <asp:TextBox runat="server" ID="txtSelectedMails" Text="" Style="display: none"></asp:TextBox>
    <asp:TextBox runat="server" ID="txtFixID" Text="" Style="display: none"></asp:TextBox>
</asp:Content>
