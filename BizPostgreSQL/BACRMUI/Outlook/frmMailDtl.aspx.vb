Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Common
Imports WebReference
Imports System.Net
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Projects
Imports BACRM.BusinessLogic.Case
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Marketing
Imports System.Web.Services


Partial Public Class frmMailDtl : Inherits BACRMPage

    Dim numEmailHstrID As Long = 0
    Dim objOutlook As COutlook

    Dim m_aryRightsForAct(), m_aryRightsForPro(), m_aryRightsForCase(), m_aryRightsForImp() As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Session("FromDetail") = Nothing
            numEmailHstrID = CCommon.ToLong(Request.QueryString("numEmailHstrID"))
            txtEmailHstrId.Text = numEmailHstrID
            GetUserRightsForPage(33, 1)
            m_aryRightsForAct = GetUserRightsForPage(33, 2)
            If m_aryRightsForAct(RIGHTSTYPE.VIEW) = 0 Then btnAct.Visible = False
            m_aryRightsForPro = GetUserRightsForPage(33, 4)
            If m_aryRightsForPro(RIGHTSTYPE.VIEW) = 0 Then btnProj.Visible = False
            m_aryRightsForCase = GetUserRightsForPage(33, 3)
            If m_aryRightsForCase(RIGHTSTYPE.VIEW) = 0 Then btnCase.Visible = False


            If Not IsPostBack Then
                BindInboxTree()
                BindEmailstatus()
                Bindmessage()

                If Request.QueryString("Print") IsNot Nothing Then
                    tdEmailButton.Visible = False
                    btnClose.Visible = False
                    trNewRecordLinks.Visible = False
                    pinto.Visible = False
                    Page.ClientScript.RegisterClientScriptBlock(Page.GetType, "PrintEmail", "window.print();", True)
                End If
            End If

            If Request.QueryString("NodeId") Is Nothing Then
                If Not IsPostBack Then
                    litResizeScript.Text = "<script type='text/javascript'>windowResize();</script>"
                Else
                    litResizeScript.Text = ""
                End If
            End If


        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            'Response.Write(ex)
        End Try
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
    Sub Bindmessage()
        Try
            If numEmailHstrID > 0 Then
                Dim dtTable As DataTable
                Dim arrAllMail As String = ""
                objOutlook = New COutlook
                objOutlook.numEmailHstrID = numEmailHstrID
                hdnEmailId.Value = numEmailHstrID
                objOutlook.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dtTable = objOutlook.getMail()

                If dtTable.Rows.Count > 0 Then
                    lblSubject.Text = CCommon.ToString(dtTable.Rows(0).Item("Subject"))
                    lblSentOn.Text = FormattedDateTimeFromDate(dtTable.Rows(0).Item("Sent"), Session("DateFormat"))

                    litBody.Text = dtTable.Rows(0).Item("Body").ToString.Replace("<base href=""http://bizautomation.dyndns.biz/btnet/""/>", "")
                    litBody.Text = System.Text.RegularExpressions.Regex.Replace(litBody.Text, "<style>[\s\S]+?</style>", "", System.Text.RegularExpressions.RegexOptions.IgnoreCase)
                    If ddlEmailDtlEmailStatus.Items.FindByValue(CCommon.ToString(dtTable.Rows(0).Item("numEmailStatusId"))) IsNot Nothing Then
                        ddlEmailDtlEmailStatus.Items.FindByValue(CCommon.ToString(dtTable.Rows(0).Item("numEmailStatusId"))).Selected = True
                    End If
                    If ddlEmailDtlFoldersStructure.Items.FindByValue(CCommon.ToString(dtTable.Rows(0).Item("numNodeId"))) IsNot Nothing Then
                        ddlEmailDtlFoldersStructure.Items.FindByValue(CCommon.ToString(dtTable.Rows(0).Item("numNodeId"))).Selected = True
                    End If
                    Session("MailBody") = litBody.Text

                    'IEnd Pinkal Patel Date:12-Oct-2011
                    If Not IsDBNull(dtTable.Rows(0).Item("ToEmail")) Then
                        Dim toName As String() = dtTable.Rows(0).Item("ToEmail").ToString.Split(New String() {"#^#"}, StringSplitOptions.RemoveEmptyEntries)
                        cellTO.Controls.Clear()
                        If toName.Length > 0 Then
                            Dim toLength As Integer = 0
                            For toLength = 0 To toName.Length - 1
                                Dim toNameAddress As String() = toName(toLength).ToString.Split(New String() {"$^$"}, StringSplitOptions.None)

                                If (toNameAddress.Length >= 3) Then
                                    Dim h As New HyperLink
                                    Dim email As New HyperLink

                                    h.CssClass = "hyperlink"
                                    email.CssClass = "hyperlink"

                                    'Mstart Pinkal Patel Date:12-Oct-2010
                                    h.Text = HttpUtility.HtmlEncode(IIf(toNameAddress(1).ToString = "", toNameAddress(2).ToString, toNameAddress(1).ToString)) ' HttpUtility.HtmlEncode(toName(toLength))

                                    email.Text = "&nbsp;&nbsp;&nbsp;<br/>"
                                    email.Text = HttpUtility.HtmlEncode(toNameAddress(2).ToString)

                                    h.ToolTip = HttpUtility.HtmlEncode(toNameAddress(2).ToString) ' HttpUtility.HtmlEncode(toName(toLength))
                                    email.ToolTip = HttpUtility.HtmlEncode(toNameAddress(2).ToString)

                                    h.Attributes.Add("onclick", "return fn_Mail('" & toNameAddress(2).ToString() & "','" & Session("CompWindow") & "','" & Session("UserContactID") & "');")
                                    email.Attributes.Add("onclick", "return fn_Mail('" & toNameAddress(2).ToString() & "','" & Session("CompWindow") & "','" & Session("UserContactID") & "');")

                                    'h.Attributes.Add("onClick()", "ComposeMail('" & ToEmail(toLength) & "')")
                                    arrAllMail = arrAllMail & (toNameAddress(2).ToString) & ","
                                    Dim l As New Label
                                    l.Text = "&nbsp;&nbsp;&nbsp;"
                                    cellTO.Controls.Add(h)
                                    '' cellTO.Controls.Add(email)
                                    cellTO.Controls.Add(l)
                                End If
                            Next
                            If toName.Length > 0 Then
                                hdnToEmail.Value = (toName(0).ToString.Split(New String() {"$^$"}, StringSplitOptions.None))(2).ToString
                            End If
                        End If

                        If (arrAllMail.Length > 0) Then
                            arrAllMail = arrAllMail.Remove(arrAllMail.Length - 1, 1)
                        End If
                    End If

                    Dim replyMail As String = ""

                    If Not IsDBNull(dtTable.Rows(0).Item("FromEmail")) Then
                        If CCommon.ToString(dtTable.Rows(0).Item("FromEmail")) <> "" Then
                            Dim fromNameAddress As String() = dtTable.Rows(0).Item("FromEmail").ToString.Split(New String() {"$^$"}, StringSplitOptions.None)

                            If fromNameAddress.Length >= 3 Then


                                If fromNameAddress(1) <> "" Then
                                    hypFrom.Text = HttpUtility.HtmlEncode(fromNameAddress(1))
                                Else
                                    hypFrom.Text = HttpUtility.HtmlEncode(fromNameAddress(2))
                                End If

                                Dim strFrom As String() = {"", ""}
                                strFrom(0) = fromNameAddress(1)
                                strFrom(1) = fromNameAddress(2)
                                Session("FromDetail") = strFrom

                                hypFrom.ToolTip = fromNameAddress(2) 'dtTable.Rows(0).Item("FromEmail")
                                hypFrom.Attributes.Add("onclick", "return fn_Mail('" & fromNameAddress(2) & "','" & Session("CompWindow") & "','" & Session("UserContactID") & "');")

                                replyMail = fromNameAddress(2)
                            End If
                            'Else : hypFrom.Text = HttpUtility.HtmlEncode(dtTable.Rows(0).Item("FromName"))
                        End If
                    End If

                    If Not IsDBNull(dtTable.Rows(0).Item("CCEmail")) Then
                        Dim ccName As String() = dtTable.Rows(0).Item("CCEmail").ToString.Split(New String() {"#^#"}, StringSplitOptions.RemoveEmptyEntries)
                        cellCC.Controls.Clear()
                        If ccName.Length > 0 Then
                            Dim CCLength As Integer = 0

                            If ccName.Length > 0 Then
                                trCC.Visible = True
                            End If
                            For CCLength = 0 To ccName.Length - 1
                                Dim ccNameAddress As String() = ccName(CCLength).ToString.Split(New String() {"$^$"}, StringSplitOptions.None)

                                If ccNameAddress.Length >= 3 Then
                                    Dim h As New HyperLink
                                    h.CssClass = "hyperlink"
                                    h.Text = HttpUtility.HtmlEncode(IIf(ccNameAddress(1).ToString = "", ccNameAddress(2).ToString, ccNameAddress(1).ToString)) 'HttpUtility.HtmlEncode(ccName(CCLength))
                                    h.ToolTip = HttpUtility.HtmlEncode(ccNameAddress(2)) ' HttpUtility.HtmlEncode(ccEmail(CCLength))
                                    h.Attributes.Add("onclick", "return fn_Mail('" & ccNameAddress(2).ToString() & "','" & Session("CompWindow") & "','" & Session("UserContactID") & "');")
                                    ' h.Attributes.Add("onClick()", "ComposeMail('" & ToEmail(toLength) & "')")
                                    Dim l As New Label
                                    l.Text = "&nbsp;&nbsp;&nbsp;"

                                    cellCC.Controls.Add(h)
                                    cellCC.Controls.Add(l)
                                    arrAllMail = arrAllMail & "," & (ccNameAddress(2).ToString)
                                End If
                            Next
                        End If
                    End If
                    ' arrAllMail = arrAllMail.Remove(arrAllMail.Length - 1, 1)

                    arrAllMail = arrAllMail & "," & replyMail

                    'Modified by Sachin Sadhu on 3rd-Dec-2013 || Case No:00000000511 -Remove attached items in reply
                    'Changes: Just Change PickAttach 1 to 0
                    btnReply.Attributes.Add("onclick", "return fnReply('','RE: " & Server.UrlEncode(lblSubject.Text) & "','" & numEmailHstrID & "',0,'reply',1)")
                    'Remove below Comment to insert attachmet while replying any mail||for inserting attachmet- PickAtch=1 else 0 
                    'btnReply.Attributes.Add("onclick", "return fnReply('','RE: " & Server.UrlEncode(lblSubject.Text) & "','" & numEmailHstrID & "',1,'reply',1)")
                    'End of Modification by Sachin

                    Dim replyall As String = arrAllMail.ToString  'dtTable.Rows(0).Item("ToEmail").ToString & "," & dtTable.Rows(0).Item("CCEmail").ToString & "," & dtTable.Rows(0).Item("FromEmail").ToString
                    btnReplyAll.Attributes.Add("onclick", "return fnReply('','RE: " & Server.UrlEncode(lblSubject.Text) & "','" & numEmailHstrID & "',0,'replyall',1)")
                    btnForward.Attributes.Add("onclick", "return fnReply('','FWD: " & Server.UrlEncode(lblSubject.Text) & "','" & numEmailHstrID & "',1,'forward',0)")

                    Dim objCell As HtmlTableCell
                   
                    'set hidden field value 

                    If dtTable.Rows.Count > 0 Then
                        hdnContactId.Value = dtTable.Rows(0).Item("numContactId")
                        hdnDivisionId.Value = CCommon.ToLong(dtTable.Rows(0).Item("numDivisionId"))
                        hdnDomainId.Value = Session("DomainID")
                    End If

                    If CCommon.ToLong(hdnContactId.Value) = 0 Then
                        pinto.Visible = False
                    End If

                    Dim intCountAttachment As Integer = 0
                    Dim objRow As HtmlTableRow
                    objRow = New HtmlTableRow
                    objCell = New HtmlTableCell
                    objCell.VAlign = "Left"
                    For intCountAttachment = 0 To dtTable.Rows.Count - 1
                        If dtTable.Rows(intCountAttachment).Item("HasAttachments") = True Then
                            If Not (IsDBNull(dtTable.Rows(0).Item("AttachmentPath")) AndAlso IsDBNull(dtTable.Rows(0).Item("vcLocation"))) Then
                                Dim AttachName As String() = dtTable.Rows(intCountAttachment).Item("AttachmentPath").ToString.Split("|")
                                Dim OriginalAttachName As String() = dtTable.Rows(intCountAttachment).Item("vcLocation").ToString.Split("|")
                                Dim i As Integer = 0

                                If AttachName.Length > 0 Then
                                    trAttach.Visible = True
                                End If

                                For i = 0 To AttachName.Length - 1
                                    Dim h As New HyperLink
                                    h.Text = AttachName(i)
                                    h.ToolTip = AttachName(i)
                                    h.CssClass = "hyperlink"
                                    'h.Attributes.Add("onClick", "OpenAttach('" & CCommon.GetDocumentPath(CCommon.ToLong(Session("DomainID"))) & OriginalAttachName(i) & "')")
                                    h.Target = "_blank "
                                    h.NavigateUrl = "../Documents/frmViewAttachment.aspx?OrgFName=" & OriginalAttachName(i).Trim & "&FName=" & Server.UrlEncode(AttachName(i).Trim)
                                    Dim l As New Label
                                    l.Text = "&nbsp;"
                                    l.CssClass = "normal1"
                                    objCell.Controls.Add(h)
                                    objCell.Controls.Add(l)
                                Next
                                trAttach.Cells.Add(objCell)
                            End If
                        End If
                    Next
                    If dtTable.Rows(0).Item("type") = "1" Or dtTable.Rows(0).Item("type") = "2" Or dtTable.Rows(0).Item("type") = "3" Then
                        btnContact.Visible = True
                        btnCompany.Visible = True
                        btnAct.Visible = True
                        btnCase.Visible = True
                        btnProj.Visible = True
                        ' btnMove.Visible = True
                        objOutlook.Email = HttpUtility.HtmlDecode(hypFrom.Text) '.Split("<")(1).Split(">")(0)
                        objOutlook.DomainID = Session("DomainId")
                        ''Dim dttableChk As DataTable = objOutlook.getDetailsFromEmail()
                        If dtTable.Rows.Count > 0 Then
                            If CCommon.ToLong(dtTable.Rows(0).Item("numContactId")) > 0 Then
                                btnContact.Attributes.Add("onclick", "return OpenContact('" & dtTable.Rows(0).Item("numContactId") & "','" & Session("EnableIntMedPage") & "')")
                                btnCompany.Attributes.Add("onclick", "return OpenWindow('" & dtTable.Rows(0).Item("numDivisionId") & "','" & dtTable.Rows(0).Item("tintCRMtype") & "','" & Session("EnableIntMedPage") & "')")
                                btnAct.Attributes.Add("onclick", "return NewActionItem('" & dtTable.Rows(0).Item("numContactId") & "','" & numEmailHstrID & "')")
                                btnCase.Attributes.Add("onclick", "return NewCase('" & dtTable.Rows(0).Item("numContactId") & "','" & dtTable.Rows(0).Item("numDivisionId") & "','" & numEmailHstrID & "')")
                                btnProj.Attributes.Add("onclick", "return NewProject('" & dtTable.Rows(0).Item("numContactId") & "','" & dtTable.Rows(0).Item("numDivisionId") & "','" & numEmailHstrID & "')")

                                'Added by Priya (18 Jan 2018)(Follow up details)

                                FetchAutomatedFollowUpDetails(CCommon.ToLong(Session("DomainId")), CCommon.ToLong(dtTable.Rows(0).Item("numContactId")))
                                'BindFollowupCamapaign(CCommon.ToLong(Session("DomainId")), CCommon.ToLong(dtTable.Rows(0).Item("numContactId")))

                            Else
                                trNewRecordLinks.Visible = False
                            End If
                        Else
                            trNewRecordLinks.Visible = False
                        End If
                    End If
                End If
            Else
                Dim objCell As HtmlTableCell
                objCell = New HtmlTableCell
                objCell.InnerHtml = "No Message Selected"
                trBody.Cells.Add(objCell)
            End If

            If numEmailHstrID > 0 Then
                btnClose.Visible = False
                btnDeleteMail.Attributes.Add("onclick", "deleteEmailDetailsSelectedEmails('" & numEmailHstrID & "');return false;")
                btnPrintMail.Attributes.Add("onclick", "PrintEMail('" & numEmailHstrID & "');return false;")
                btnOpenNewWindow.Attributes.Add("onclick", "OpenNewWindowEMail('" & numEmailHstrID & "');return false;")
            Else
                btnOpenNewWindow.Visible = False
                btnPrintMail.Visible = False
                btnDeleteMail.Visible = False
                ddlAssignTO1.AutoPostBack = True
                btnClose.Visible = True
                trOutlook.Visible = False
            End If

            'For Next - Previous Email , not required if its search or advance search
            If numEmailHstrID > 0 AndAlso Request.QueryString("NodeId") IsNot Nothing AndAlso CCommon.ToString(Request.QueryString("srch")).Trim().Length = 0 Then

                objOutlook = New COutlook
                objOutlook.numEmailHstrID = numEmailHstrID
                objOutlook.DomainID = Session("DomainId")
                objOutlook.NodeId = CCommon.ToLong(Request.QueryString("NodeId")) 'Session("NodeId")
                objOutlook.UserCntID = Session("UserContactId")
                objOutlook.EmailStatus = CCommon.ToLong(Request.QueryString("EmailStatus"))

                Dim dtTableInfo As New DataTable

                dtTableInfo = objOutlook.getNextPrevMailID()
                If dtTableInfo.Rows.Count > 0 Then
                    If dtTableInfo.Rows(0).Item("numPrevEmailHstrID") > 0 Then
                        NextEmail.Visible = True
                        NextEmail.Attributes.Add("onclick", String.Format("javascript:MovePrevNextEmail('{0}','{1}','{2}','{3}')",
                                                                            dtTableInfo.Rows(0).Item("numPrevEmailHstrID").ToString(),
                                                                            CCommon.ToString(Request.QueryString("srch")).Trim(),
                                                                            CCommon.ToLong(Request.QueryString("NodeId")).ToString(),
                                                                            CCommon.ToLong(Request.QueryString("EmailStatus")).ToString()))
                    End If

                    If dtTableInfo.Rows(0).Item("numNextEmailHstrID") > 0 Then
                        PrevEmail.Visible = True
                        PrevEmail.Attributes.Add("onclick", String.Format("javascript:MovePrevNextEmail('{0}','{1}','{2}','{3}')",
                                                                            dtTableInfo.Rows(0).Item("numNextEmailHstrID").ToString(),
                                                                            CCommon.ToString(Request.QueryString("srch")).Trim(),
                                                                            CCommon.ToLong(Request.QueryString("NodeId")).ToString(),
                                                                            CCommon.ToLong(Request.QueryString("EmailStatus")).ToString()))
                    End If
                End If

            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub BindEmailstatus()
        Try
            Dim dtEmailStatus As DataTable
            objCommon.DomainID = CCommon.ToInteger(Session("DomainId"))
            objCommon.ListID = 385
            dtEmailStatus = objCommon.GetMasterListItemsWithRights()
            ddlEmailDtlEmailStatus.DataSource = dtEmailStatus
            ddlEmailDtlEmailStatus.DataTextField = "vcData"
            ddlEmailDtlEmailStatus.DataValueField = "numListItemId"
            ddlEmailDtlEmailStatus.DataBind()
            ddlEmailDtlEmailStatus.Items.Insert(0, New ListItem("", "0"))
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub BindInboxTree()
        Try
            objOutlook = New COutlook
            Dim dtTable As DataTable
            objOutlook.UserCntID = Session("UserContactId")
            objOutlook.DomainID = Session("DomainId")
            objOutlook.ModeType = 0
            objOutlook.NodeId = 0
            objOutlook.ParentId = 0
            objOutlook.NodeName = ""
            dtTable = objOutlook.GetTree()

            ddlEmailDtlFoldersStructure.Items.Clear()
            ddlEmailDtlFoldersStructure.DataSource = dtTable
            ddlEmailDtlFoldersStructure.DataTextField = "vcNodeName"
            ddlEmailDtlFoldersStructure.DataValueField = "numNodeID"
            ddlEmailDtlFoldersStructure.DataBind()

            'ddlEmailDtlFoldersStructure.Items.Insert(0, New ListItem("-Select Folder-", "0"))
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub ddlAssignTO1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlAssignTO1.SelectedIndexChanged
        Try
            LoadOpenRecords()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Public Sub LoadOpenRecords()
        Try
            ddlOpenRecord.Items.Clear()
            If ddlAssignTO1.SelectedValue > 0 And CCommon.ToLong(hdnDivisionId.Value) > 0 Then
                Dim objContacts As New CContacts
                objContacts.FillOpenRecords(hdnDivisionId.Value, Session("DomainId"), ddlOpenRecord, ddlAssignTO1.SelectedValue)
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Session("MailBody") = Nothing
            Response.Write("<script language='javascript'>self.close();</script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub



    Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Try
            ''Add To Correspondense if Open record is selected
            If CCommon.ToLong(ddlOpenRecord.SelectedValue) > 0 Then
                Dim objActionItem As New ActionItem
                objActionItem.CorrespondenceID = 0
                objActionItem.CommID = 0
                objActionItem.EmailHistoryID = numEmailHstrID
                objActionItem.CorrType = ddlAssignTO1.SelectedValue
                objActionItem.OpenRecordID = ddlOpenRecord.SelectedValue
                objActionItem.DomainID = Session("DomainID")
                objActionItem.ManageCorrespondence()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    ' Added by Priya (18 Jan 2018)(Follow up campaign details)
    Private Sub FetchAutomatedFollowUpDetails(ByVal numDomainId As Long, ByVal numContactId As Long)
        Try
            Dim dsFollowupdetails As New DataSet
            dsFollowupdetails = objCommon.FetchFollowUpCampaignDetails(numDomainId, numContactId)

            Dim dtFollowupTable As DataTable
            dtFollowupTable = dsFollowupdetails.Tables(0)
            If dtFollowupTable.Rows.Count > 0 Then
                lblFollowupCampaign.Text = dtFollowupTable.Rows(0).Item("FollowUpCampaign")
                lblLastFollowUp.Text = dtFollowupTable.Rows(0).Item("LastFollowUp")
                lblNextFollowUp.Text = dtFollowupTable.Rows(0).Item("NextFollowUp")
                ' lblLastEmailStatus.Text = dtFollowupTable.Rows(0).Item("LastEmailStatus")
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    'Private Sub BindFollowupCamapaign(ByVal numDomainId As Long, ByVal numContactId As Long)
    '    Try

    '        Dim dtData As New DataTable

    '        Dim objCampaign As New Campaign
    '        objCampaign.SortCharacter = "0"
    '        objCampaign.UserCntID = Session("UserContactID")
    '        objCampaign.PageSize = 100
    '        objCampaign.TotalRecords = 0
    '        objCampaign.DomainID = Session("DomainID")
    '        objCampaign.columnSortOrder = "Asc"
    '        objCampaign.CurrentPage = 1
    '        objCampaign.columnName = "vcECampName"

    '        dtData = objCampaign.ECampaignList
    '        Dim drow As DataRow = dtData.NewRow
    '        drow("numECampaignID") = -1
    '        drow("vcECampName") = "-- Disengaged --"
    '        dtData.Rows.Add(drow)

    '        ddlFollowupcampaign.DataTextField = "vcECampName"
    '        ddlFollowupcampaign.DataValueField = "numECampaignID"
    '        ddlFollowupcampaign.DataSource = dtData
    '        ddlFollowupcampaign.DataBind()

    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Sub
    'Protected Sub btnUpdateFollowup_Click(sender As Object, e As EventArgs)
    'Dim intStartDate As Date
    '    intStartDate = DateTime.Now.ToString("yyyy-MM-dd 00:00:00.000")

    '    Dim objCampaign As New Campaign
    '    objCampaign.ConEmailCampID = 0
    '    objCampaign.ContactId = hdnContactId.Value
    '    If (ddlFollowupcampaign.SelectedItem.Text = "-- Disengaged --") Then
    '        objCampaign.ECampaignID = 0
    '    Else
    '        objCampaign.ECampaignID = ddlFollowupcampaign.SelectedValue
    '    End If
    '    objCampaign.StartDate = intStartDate
    '    objCampaign.Engaged = 1
    '    objCampaign.UserCntID = Session("UserContactID")

    '    Dim ConCampaignID As Long
    '    ConCampaignID = objCampaign.ManageConECamp()


    'End Sub
End Class