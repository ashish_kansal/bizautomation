﻿Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Outlook


    Partial Public Class frmSortOutlookTree : Inherits BACRMPage


        Dim objOutlook As COutlook

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                btnAdd.Attributes.Add("onclick", "return CheckName()")
                btnDelete.Attributes.Add("onclick", "return CheckSelectedNode()")
                btnMove.Attributes.Add("onclick", "return CheckNode()")
                btnCopy.Attributes.Add("onclick", "return CheckNode()")
                If Not IsPostBack Then
                    'If GetQueryStringVal("frm") = "Move" Then
                    '    pnlMove.Visible = True
                    '    pnlTree.Visible = False
                    'Else
                    '    pnlMove.Visible = False
                    '    pnlTree.Visible = False
                    'End If
                    BindTree()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Public Sub BindTree()
            Try
                RadTreeView1.Nodes.Clear()
                objOutlook = New COutlook
                'Dim dtTable As DataTable
                Dim ds As DataSet = New DataSet()
                objOutlook.UserCntID = Session("UserContactId")
                objOutlook.DomainID = Session("DomainId")
                objOutlook.ModeType = 0
                objOutlook.NodeId = 0
                objOutlook.ParentId = 0
                objOutlook.NodeName = ""
                ds = objOutlook.GetTreeOrderList()
                'Dim dr As DataRow = ds.Tables(0).NewRow()
                'dr("numNodeID") = "0"
                'dr("numParentId") = vbNull
                'dr("vcNodeName") = ""
                'dr("numSortOrder") = "0"
                'dr("bitSystem") = True
                'ds.Tables(0).Rows.InsertAt(dr, 0)


                ds.Relations.Add("NodeRelation", ds.Tables(0).Columns("numNodeID"), ds.Tables(0).Columns("numParentID"))

                For Each dbRow As DataRow In ds.Tables(0).Rows
                    If dbRow.IsNull("numParentID") Then
                        Dim node As RadTreeNode = CreateNode(dbRow("vcNodeName").ToString(), True, dbRow("numSortOrder").ToString(), dbRow("numNodeID").ToString(), CCommon.ToLong(dbRow("numFixID")), CCommon.ToBool(dbRow("bitHidden")))
                        RadTreeView1.Nodes.Add(node)
                        RecursivelyPopulate(dbRow, node)
                    End If
                Next

                'Dim dr1() As DataRow = ds.Tables(0).Select(" numNodeID=190 ")
                'dr1(0).Delete()

                'ds.Tables(0).AcceptChanges()

                'Dim treeNodeA As RadTreeNode
                ''treeNodeA = New RadTreeNode
                ''treeNodeA.Text = Session("ContactName")
                ''treeNodeA.Value = "0"
                ''treeNodeA.ImageUrl = "../images/root.gif"
                ''RadTreeView1.Nodes.Add(treeNodeA)
                ''RadTreeView1.Nodes(0).Expanded = True
                'For Each dr As DataRow In ds.Tables(0).Rows
                '    treeNodeA = New RadTreeNode
                '    treeNodeA.Text = dr("vcNodeName")
                '    treeNodeA.Value = dr("numNodeID").ToString
                '    Dim nodeid As Long = dr.Item("numNodeID")
                '    Select Case nodeid
                '        Case 1 : treeNodeA.ImageUrl = "../images/inbox.gif"
                '        Case 2 : treeNodeA.ImageUrl = "../images/deleted.gif"
                '        Case 4 : treeNodeA.ImageUrl = "../images/sentitems.gif"
                '        Case 190 : treeNodeA.ImageUrl = "../images/Settings.png"
                '        Case Else
                '            treeNodeA.ImageUrl = "../images/folder.gif"
                '    End Select
                '    If nodeid <> 5 Then 'Don't show Calender node
                '        If Not RadTreeView1.FindNodeByValue(dr.Item("numParentID").ToString) Is Nothing Then
                '            RadTreeView1.FindNodeByValue(dr.Item("numParentID").ToString).Nodes.Add(treeNodeA)
                '        Else
                '            RadTreeView1.Nodes.Add(treeNodeA)
                '        End If
                '    End If
                'Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub
        'Added By Sachin Sadhu for Ordering MailBox:Fetch child recursively ||Case No:516 ||Date:7th-dec-2013
        Private Sub RecursivelyPopulate(dbRow As DataRow, node As RadTreeNode)
            For Each childRow As DataRow In dbRow.GetChildRows("NodeRelation")
                Dim childNode As RadTreeNode = CreateNode(childRow("vcNodeName").ToString(), True, childRow("numSortOrder").ToString(), childRow("numNodeID").ToString(), CCommon.ToLong(childRow("numFixID")), CCommon.ToLong(childRow("bitHidden")))
                node.Nodes.Add(childNode)
                RecursivelyPopulate(childRow, childNode)
            Next
        End Sub
        'Added By Sachin Sadhu for Ordering MailBox :Creating node  ||Case No:516 ||Date:7th-dec-2013
        Private Function CreateNode(text As String, expanded As Boolean, order As String, id As String, numFixID As Long, bitHidden As Boolean) As RadTreeNode
            Dim node As New RadTreeNode(text, order)
            node.Attributes("numNodeID") = id
            If (GetQueryStringVal("node") = id) Then
                node.Selected = True
            End If
            node.Value = id
            If (text = "Inbox") Then
                node.Attributes.Add("onclick", "BindGridNode(" & node.Attributes("numNodeID").ToString & ",'Inbox'," & numFixID & ");")
                node.Font.Bold = True
                If CCommon.ToInteger(GetQueryStringVal("node")) = 0 Then
                    node.Selected = True
                    node.Attributes.Add("class", "nodeSelected")
                End If
                'node.Text = "<i class=""fa fa-inbox""></i>&nbsp;&nbsp;" & node.Text & "<a href='#' class='btn btn-xs btn-primary' style='float:right' onclick='return GetNewMessage();'><i class='fa fa-refresh'></i></a>"
                node.Text = "<img src='../images/Icon/iconFolders.png' class='sideIcon' />&nbsp;&nbsp;<span class='sideText'>" & node.Text & "</span>"
            ElseIf (text = "Email Archive") Then
                node.Attributes.Add("onclick", "BindGridNode(" & node.Attributes("numNodeID").ToString & "," & HttpUtility.JavaScriptStringEncode(node.Text, True) & "," & numFixID & ");")
                node.Font.Bold = True
                'node.Text = "<i class=""fa fa-archive""></i>&nbsp;&nbsp;Email Archive"
                node.Text = "<img src='../images/Icon/iconFolders.png' class='sideIcon' />&nbsp;&nbsp;<span class='sideText'>Email Archive</span>"
            ElseIf (text = "Sent Messages") Then
                node.Attributes.Add("onclick", "BindGridNode(" & node.Attributes("numNodeID").ToString & "," & HttpUtility.JavaScriptStringEncode(node.Text, True) & "," & numFixID & ");")
                node.Font.Bold = True
                'node.Text = "<i class=""fa fa-envelope-o""></i>&nbsp;&nbsp;Sent Messages"
                node.Text = "<img src='../images/Icon/iconFolders.png' class='sideIcon' />&nbsp;&nbsp;<span class='sideText'>Sent Messages</span>"
            ElseIf (text = "Calendar") Then
                node.Font.Bold = True
                node.Attributes.Add("onclick", "BindCalender(" & node.Attributes("numNodeID").ToString & ");")
                'node.Text = "<i class=""fa fa-calendar""></i>&nbsp;&nbsp;Calendar"
                node.Text = "<img src='../images/Icon/iconFolders.png' class='sideIcon' />&nbsp;&nbsp;<span class='sideText'>Calendar</span>"
            ElseIf (text = "Custom Folders") Then
                node.Attributes.Add("onclick", "void(0);")
                'node.Text = "<i class=""fa fa-folder""></i>&nbsp;&nbsp;" & "<b>Custom Folders</b><a href='#' onclick='return OpenTree();' class='btn btn-xs btn-primary pull-right'><i class='fa fa-pencil'></i></a>"
                node.Text = "<img src='../images/Icon/iconFolders.png' class='sideIcon' />&nbsp;&nbsp;" & "<b>Custom Folders</b><a href='#' onclick='return OpenTree();' class='btn btn-xs btn-primary pull-right'><i class='fa fa-pencil'></i></a>"
            ElseIf (text = "Settings") Then
                node.Font.Italic = True
                node.Attributes.Add("onclick", "BindOutlookSettings(" & node.Attributes("numNodeID").ToString & ");")
                node.Text = "<i class=""fa fa-gear""></i>&nbsp;&nbsp;Settings"
            Else
                node.Attributes.Add("onclick", "BindGridNode(" & node.Attributes("numNodeID").ToString & "," & HttpUtility.JavaScriptStringEncode(node.Text, True) & "," & numFixID & ");")
                'node.Text = "<i class=""fa fa-folder-o""></i>&nbsp;&nbsp;" & node.Text
                Dim strHidden As String = ""
                If bitHidden = True Then
                    strHidden = "<span class='pull-right'>Hidden</span>"
                End If
                node.Text = "<img src='../images/Icon/iconFolders.png' class='sideIcon' />&nbsp;&nbsp;<span class='sideText'>" & node.Text & "</span>" & strHidden & ""
            End If
            node.Expanded = expanded

            Return node
        End Function
        'Added By Sachin Sadhu for Ordering MailBox :Creating node  ||Case No:516 ||Date:7th-dec-2013
        Protected Sub RadTreeView1_NodeDrop(o As Object, e As Telerik.Web.UI.RadTreeNodeDragDropEventArgs)
            Dim sourceNode As RadTreeNode = e.SourceDragNode
            Dim destNode As RadTreeNode = e.DestDragNode
            Dim dropPosition As RadTreeViewDropPosition = e.DropPosition

            If destNode IsNot Nothing Then
                'drag&drop is performed between trees
                If sourceNode.Equals(destNode) OrElse sourceNode.IsAncestorOf(destNode) Then
                    Return
                End If
                Dim sourceID As String = sourceNode.Attributes("numNodeID")
                'Order
                Dim sourceValue As String = sourceNode.Value
                Dim sourceParent As String = Nothing
                If IsNothing(sourceNode.ParentNode) Then
                    sourceParent = Nothing
                ElseIf IsNothing(sourceNode.ParentNode.Attributes("numNodeID")) Then
                    sourceParent = Nothing
                Else
                    sourceParent = sourceNode.ParentNode.Attributes("numNodeID")
                End If

                '  Dim sourceParent As String = sourceNode.ParentNode.Attributes("numNodeID").ToString()
                sourceNode.Owner.Nodes.Remove(sourceNode)

                Select Case dropPosition
                    Case RadTreeViewDropPosition.Over
                        ' child
                        If Not sourceNode.IsAncestorOf(destNode) Then
                            UpdateNodeOrder(sourceValue, destNode.Attributes("numNodeID"), sourceID)
                            destNode.Nodes.Add(sourceNode)
                        End If
                        Exit Select

                    Case RadTreeViewDropPosition.Above
                        ' sibling - above        

                        SwitchNodes(sourceValue, sourceParent, sourceID, destNode)
                        destNode.InsertBefore(sourceNode)
                        RadTreeView1.Nodes.Clear()
                        BindTree()
                        Exit Select

                    Case RadTreeViewDropPosition.Below


                        SwitchNodes(sourceValue, sourceParent, sourceID, destNode)
                        destNode.InsertAfter(sourceNode)

                        RadTreeView1.Nodes.Clear()
                        BindTree()
                        Exit Select

                End Select
            End If
        End Sub
        'Added By Sachin Sadhu for Ordering MailBox   ||Case No:516 ||Date:7th-dec-2013
        Private Sub SwitchNodes(sourceValue As String, sourceParent As String, sourceID As String, destNode As RadTreeNode)
            Dim strDestinationParentNode As String
            If IsNothing(destNode.ParentNode) Then
                strDestinationParentNode = Nothing
            Else
                strDestinationParentNode = destNode.ParentNode.Attributes("numNodeID")
            End If
            UpdateNodeOrder(sourceValue, strDestinationParentNode, destNode.Attributes("numNodeID"))
            UpdateNodeOrder(destNode.Value, sourceParent, sourceID)
        End Sub
        'Added By Sachin Sadhu for Ordering MailBox :Update Order   ||Case No:516 ||Date:7th-dec-2013
        Protected Sub UpdateNodeOrder(newOrder As String, newParentId As String, id As String)

            objOutlook = New COutlook

            Dim x As Integer = objOutlook.UpdateTreeOrder(Convert.ToInt32(newOrder), Convert.ToInt32(id), Convert.ToInt32(newParentId), Convert.ToInt32(Session("UserContactID")), Convert.ToInt32(Session("DomainID")))



        End Sub

        Private Sub btnHide_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHide.Click
            objOutlook = New COutlook
            objOutlook.bitHidden = True
            objOutlook.NodeId = RadTreeView1.SelectedNode.Value
            objOutlook.ModeType = 8
            objOutlook.UserCntID = Session("UserContactId")
            objOutlook.DomainID = Session("DomainId")
            objOutlook.GetTree()

            BindTree()
        End Sub

        Private Sub btnUnHide_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUnHide.Click
            objOutlook = New COutlook
            objOutlook.bitHidden = False
            objOutlook.NodeId = RadTreeView1.SelectedNode.Value
            objOutlook.ModeType = 8
            objOutlook.UserCntID = Session("UserContactId")
            objOutlook.DomainID = Session("DomainId")
            objOutlook.GetTree()
            BindTree()
        End Sub

        Private Sub btnMovetoRootFolder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMovetoRootFolder.Click
            objOutlook = New COutlook
            objOutlook.NodeId = RadTreeView1.SelectedNode.Value
            objOutlook.ModeType = 9
            objOutlook.UserCntID = Session("UserContactId")
            objOutlook.DomainID = Session("DomainId")
            objOutlook.GetTree()
            BindTree()
        End Sub

        Private Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
            Try
                'Dim parentid As Long = 0
                'If Not RadTreeView1.SelectedNode Is Nothing Then
                '    parentid = RadTreeView1.SelectedNode.Tag
                'End If
                objOutlook = New COutlook
                objOutlook.UserCntID = Session("UserContactId")
                objOutlook.DomainID = Session("DomainId")
                objOutlook.ModeType = 1
                objOutlook.NodeId = 0
                objOutlook.ParentId = txtSelectedNode.Text
                objOutlook.NodeName = txtName.Text
                objOutlook.GetTree()
                objOutlook.Create_Folder(txtSelectedNode.Text, txtName.Text)
                txtName.Text = ""
                BindTree()
            Catch ex As Exception
                If ex.Message.Contains("FOLDER_ALREADY_EXISTS") Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "Error", "alert('The folder name you have chosen already exists. Please try another name');", True)
                Else
                    ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                    Response.Write(ex)
                End If
            End Try
        End Sub

        Private Sub btnDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDelete.Click
            Try
                Dim lngSelectedNode As Long = CCommon.ToLong(RadTreeView1.SelectedNode.Value)
                If lngSelectedNode = 0 Or lngSelectedNode = 1 Or lngSelectedNode = 2 Or lngSelectedNode = 3 Or lngSelectedNode = 4 Or lngSelectedNode = 5 Or lngSelectedNode = 190 Then
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "Error", "alert('Can not delete default node');", True)
                Else
                    objOutlook = New COutlook
                    objOutlook.UserCntID = Session("UserContactId")
                    objOutlook.DomainID = Session("DomainId")
                    objOutlook.ModeType = 2
                    objOutlook.NodeId = lngSelectedNode
                    objOutlook.NodeName = ""
                    objOutlook.ParentId = 0
                    objOutlook.GetTree()
                    objOutlook.DeleteFolder(txtSelectedNode.Text, txtSelectedFolder.Text)
                End If
                txtName.Text = ""
                BindTree()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
            Try
                objOutlook = New COutlook
                Dim strEmailId As String
                Dim strEmailHstrId As String()
                Dim intCount As Integer = 0
                strEmailId = GetQueryStringVal("numEmailHstrId")
                If strEmailId.Length > 0 Then
                    strEmailId = strEmailId.TrimEnd(",")
                End If
                strEmailHstrId = strEmailId.Split(",")
                For intCount = 0 To strEmailHstrId.Length - 1
                    objOutlook.numEmailHstrID = CCommon.ToLong(strEmailHstrId(intCount)) ' GetQueryStringVal( "numEmailHstrId")
                    objOutlook.mode = False
                    objOutlook.NodeId = IIf(txtSelectedNode.Text = 1, 0, txtSelectedNode.Text)
                    objOutlook.MoveCopy()
                    If txtSelectedMails.Text <> "" Then
                        Dim Ids As String() = txtSelectedMails.Text.Split(",")
                        For i As Integer = 0 To Ids.Length - 1
                            objOutlook.numEmailHstrID = Ids(i)
                            objOutlook.mode = False
                            objOutlook.NodeId = IIf(txtSelectedNode.Text = 1, 0, txtSelectedNode.Text)
                            objOutlook.MoveCopy()
                        Next
                        txtSelectedMails.Text = ""
                    End If
                Next
                Response.Write("<script>window.opener.BindGrid(" & CCommon.ToLong(GetQueryStringVal("NodeId")) & ");self.close();</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Private Sub btnMove_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMove.Click
            Try
                objOutlook = New COutlook
                Dim strEmailId As String
                Dim strEmailHstrId As String()
                Dim intCount As Integer = 0
                strEmailId = GetQueryStringVal("numEmailHstrId")
                If strEmailId.Length > 0 Then
                    strEmailId = strEmailId.TrimEnd(",")
                End If
                strEmailHstrId = strEmailId.Split(",")
                For intCount = 0 To strEmailHstrId.Length - 1
                    objOutlook.numEmailHstrID = CCommon.ToLong(strEmailHstrId(intCount)) 'GetQueryStringVal( "numEmailHstrId")
                    objOutlook.mode = True
                    objOutlook.NodeId = IIf(txtSelectedNode.Text = 1, 0, txtSelectedNode.Text)
                    objOutlook.MoveCopy()
                    If txtSelectedMails.Text <> "" Then
                        Dim Ids As String() = txtSelectedMails.Text.Split(",")
                        For i As Integer = 0 To Ids.Length - 1
                            objOutlook.numEmailHstrID = Ids(i)
                            objOutlook.mode = True
                            objOutlook.NodeId = IIf(txtSelectedNode.Text = 1, 0, txtSelectedNode.Text)
                            objOutlook.MoveCopy()
                        Next
                        txtSelectedMails.Text = ""
                    End If
                Next
                Response.Write("<script>window.opener.BindGrid(" & CCommon.ToLong(GetQueryStringVal("NodeId")) & ");self.close();</script>")
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace