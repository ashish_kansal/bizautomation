﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="InboxTree.ascx.vb"
    Inherits=".InboxTree" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<script type="text/javascript">
    function OpenTree() {
        var win = window.open('../outlook/frmoutlooktree.aspx', 'OutlookTree', 'toolbar=no,titlebar=no,top=200,left=300,width=320,height=410,scrollbars=yes,resizable=yes')
        win.focus()
        return false
    }

    function Load() {
        document.getElementById('btnGo').click()
    }

    var NewEmail = 0;
    function GetNewMessage() {
        NewEmail = 1;
    }

    function Compose() {
        window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail=&pqwRT=', 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
        return false;
    }

    function BindCalender(node) {
        document.location.href = '../OutlookCalendar/CalendarList.aspx?node=' + node;
        return false;
    }

    function BindOutlookSettings(node) {
        document.location.href = '../outlook/frmOutlookSettings.aspx';
        return false;
    }

    //Function will be called after mail grid is bound
    function SetNodeSelected() {
        if (parseInt($('[id$=hdnBindID]').val()) > 0) {
            var treeView = $find("<%= RadTreeView1.ClientID %>");
            var node = treeView.findNodeByValue($('[id$=hdnBindID]').val());
            node.select();
        }
    }

    function BindGridNode(node, FName, numFixID) {
        if ($('[id$=hdnNodeId]') != undefined) {
            $('[id$=hdnNodeId]').val(node);
        }
        //document.location.href = '../outlook/frmInboxItems.aspx?node=' + node + '&NewEmail=' + NewEmail + '&Name=' + FName + '&numFixID=' + numFixID;
        NewEmail = 0;
        return false;
    }
    $(document).ready(function () {
        var windowHeight = $(window).height();
        windowHeight = windowHeight - 240;
            $('.fixedHeightScroll').slimScroll({
              height:windowHeight
            });
        })
</script>
<style>
    .RadTreeView .rtUL .rtLI .rtUL {
        padding: 0px !important;
    }

    .rtMid, .rtBot, .rtTop {
        border-bottom: 1px solid #f4f4f4;
    }

    .rtLI div {
        border-radius: 0;
        border-top: 0;
        border-left: 3px solid transparent;
        color: #444;
        position: relative;
        display: block;
    }

        .rtHover {
            color: #444 !important;
            background: rgba(248, 182, 63, 0.15) !important;
        }
        .rtHover span {
            color: #444 !important;
            background: rgba(248, 182, 63, 0.15) !important;
        }
        .rtSelected{
             color: #444 !important;
            background: rgba(248, 182, 63, 0.15) !important;
            border-left-color: #d48800 !important;
        }
    .rtSelected {
        background: transparent;
        color: #444;
        border-top: 0;
        border-left-color: #3c8dbc !important;
    }

    .RadTreeView_Vista .rtSelected .rtIn {
        background: none;
        border: none;
        padding: 7px;
    }

    .RadTreeView_Vista, .RadTreeView_Vista a.rtIn, .RadTreeView_Vista .rtEdit .rtIn input {
        font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
        font-weight: 400;
        font-size: 14px;
    }

    .RadTreeView .rtPlus, .RadTreeView .rtMinus {
        margin-right: 2px;
        margin-left: -13px;
        vertical-align: middle;
        margin: 0px 7px 0 -18px;
    }

    .RadTreeView .rtIn {
        display: inline-block !important;
        white-space: normal !important;
        width: 100% !important;
        height: 35px;
        padding: 7px;
    }

    .RadTreeView_Vista .rtHover .rtIn {
        color: #444;
        background: #f7f7f7;
        border: none;
        padding:7px;
    }
    .RadTreeView .rtLines .rtLI {
            background: #f3f3f352 !important;
            cursor: pointer;
        }

        .RadTreeView .rtMid, .RadTreeView .rtTop {
            padding: 1px 0 1px 5px !important;
        }

        .rtFirst .rtUL .rtLI :hover {
            background-color: #ea941330 !important;
        }

        .sideIcon {
            height: 30px;
            margin-top: -6px;
            margin-left: -10px;
        }

        .fixedHeightScroll {
            overflow-y: scroll;
            overflow-x: hidden;
            border: 1px solid #c1c1c1;
            height:calc(100vh - 240px);
        }

        .rtLI div {
            border-bottom-color: #e1e1e1;
        }

        .emailLeftSideWidget {
            padding-right: 5px !important;
            width: 20% !important;
        }

        .emailRightSideWidget {
            padding-left: 15px !important;
            width: 80% !important;
            margin-top: 0px !important;
        }

</style>

<div class="row" style="display:none">
    <div class="col-xs-12 padbottom10">
        <div class="pull-left" style="margin-top: 3px;">
            <a title="Settings" onclick="BindOutlookSettings();"><i class="fa fa-2x fa-cog"></i></a>
        </div>
        <div class="pull-right">
            <asp:Button CssClass="btn btn-primary" runat="server" ID="btnCompose" Text="Compose" />
            <asp:Button CssClass="btn btn-primary" runat="server" ID="btnGo" Style="display: none" />
        </div>
    </div>
</div>
<div class="box box-solid">
    <div class="box-header with-border"  style="display:none" >
        <h3 class="box-title">Folders</h3>

        <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body no-padding fixedHeightScroll" style="display: block;">
        <telerik:RadTreeView ID="RadTreeView1"  OnNodeClick="RadTreeView1_NodeClick" runat="server" AllowNodeEditing="false" Skin="Vista">
        </telerik:RadTreeView>
    </div>
    <!-- /.box-body -->
</div>
