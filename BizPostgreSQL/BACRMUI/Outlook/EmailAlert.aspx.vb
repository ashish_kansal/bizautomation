﻿
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common
Imports EmailAlertConfig
Imports System.IO
Public Class EmailAlert
    Inherits BACRMPage
    Dim objEmailAlertConfig As EmailAlertConfig
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            BindData()
        End If
    End Sub
    Private Sub BindData()
        Try
            objEmailAlertConfig = New EmailAlertConfig
            Dim dtEmailAlertConfig As DataTable
            objEmailAlertConfig.ContactId = GetQueryStringVal( "ContactId")
            objEmailAlertConfig.DomainId = CCommon.ToLong(Session("DomainId"))
            objEmailAlertConfig.UserCntID = CCommon.ToLong(Session("UserContactID"))
            dtEmailAlertConfig = objEmailAlertConfig.GetEmailAlert()
            With dtEmailAlertConfig
                If CCommon.ToBool(.Rows(0)("bitOpenActionItem")) = True Then
                    chkOpenAction.Checked = True
                    hyOpenActionCount.Text = CCommon.ToString(.Rows(0)("numOpenActionCount"))

                    hyOpenActionCount.Attributes.Add("onclick", "return OpenActionItem(" & objEmailAlertConfig.ContactId & ")")
                End If
                If CCommon.ToBool(.Rows(0)("bitOpenCases")) = True Then
                    chkOpenCases.Checked = True
                    hyOpenCasesCount.Text = CCommon.ToString(.Rows(0)("numOpenCasesCount"))

                    hyOpenCasesCount.Attributes.Add("onclick", "return OpenWindow(" & CCommon.ToLong(.Rows(0)("numDivisionID")) & ",2)")
                End If
                If CCommon.ToBool(.Rows(0)("bitOpenProject")) = True Then
                    chkOpenProject.Checked = True
                    hyOpenProjectCount.Text = CCommon.ToString(.Rows(0)("numOpenProjectCount"))

                    hyOpenProjectCount.Attributes.Add("onclick", "return OpenWindow(" & CCommon.ToLong(.Rows(0)("numDivisionID")) & ",1)")
                End If
                If CCommon.ToBool(.Rows(0)("bitOpenSalesOpp")) = True Then
                    chkOpenSalesOpp.Checked = True
                    hyOpenSalesOppCount.Text = CCommon.ToString(.Rows(0)("numOpenSalesOppCount"))

                    hyOpenSalesOppCount.Attributes.Add("onclick", "return OpenWindow(" & CCommon.ToLong(.Rows(0)("numDivisionID")) & ",3)")
                End If
                If CCommon.ToBool(.Rows(0)("bitOpenPurchaseOpp")) = True Then
                    chkOpenPurchaseOpp.Checked = True
                    hyOpenPurchaseOppCount.Text = CCommon.ToString(.Rows(0)("numOpenPurchaseOppCount"))

                    hyOpenPurchaseOppCount.Attributes.Add("onclick", "return OpenWindow(" & CCommon.ToLong(.Rows(0)("numDivisionID")) & ",4)")
                End If
                If CCommon.ToBool(.Rows(0)("bitBalancedue")) = True Then
                    chkBalanceDue.Checked = True
                    lblBalanceDueVal.Text = CCommon.ToString(.Rows(0)("monBalancedue"))
                    lblBalanceDueCount.Text = CCommon.ToString(.Rows(0)("monBalancedueCount"))
                End If
                If CCommon.ToBool(.Rows(0)("bitUnreadEmail")) = True Then
                    chkUnreadEmail.Checked = True
                    hyUnreadEmailCount.Text = CCommon.ToString(.Rows(0)("intUnreadEmailCount"))

                    hyUnreadEmailCount.Attributes.Add("onclick", "return OpenEmail(" & CCommon.ToLong(.Rows(0)("numDivisionID")) & ",1)")
                End If
                lblnumPurchasedPast.Text = CCommon.ToString(.Rows(0)("monPurchasedPast"))
                lblPurchasedPastCount.Text = "No"
                If CCommon.ToBool(.Rows(0)("bitPurchasedPast")) = True Then
                    chkPurchasedPast.Checked = True
                    If CCommon.ToDouble(.Rows(0)("monPurchasedPast")) <= CCommon.ToDouble(.Rows(0)("monPurchasedPastCount")) Then
                        lblPurchasedPastCount.Text = "Yes"
                    End If
                End If
                lblnumSoldPast.Text = CCommon.ToString(.Rows(0)("monSoldPast"))
                lblSoldPastCount.Text = "No"
                If CCommon.ToBool(.Rows(0)("bitSoldPast")) = True Then
                    chkSoldPast.Checked = True

                    If CCommon.ToDouble(.Rows(0)("monSoldPast")) <= CCommon.ToDouble(.Rows(0)("monSoldPastCount")) Then
                        lblSoldPastCount.Text = "Yes"
                    End If
                End If
            End With


        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class