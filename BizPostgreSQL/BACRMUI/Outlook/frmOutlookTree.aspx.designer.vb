'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Namespace BACRM.UserInterface.Outlook

    Partial Public Class frmOutlookTree

        '''<summary>
        '''btnclose control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnclose As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''scManager control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents scManager As Global.System.Web.UI.ScriptManager

        '''<summary>
        '''pnlTree control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents pnlTree As Global.System.Web.UI.WebControls.Panel

        '''<summary>
        '''txtName control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtName As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''btnAdd control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnAdd As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnDelete control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnDelete As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnRename control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnRename As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''pnlMove control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents pnlMove As Global.System.Web.UI.WebControls.Panel

        '''<summary>
        '''btnMove control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnMove As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''btnCopy control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents btnCopy As Global.System.Web.UI.WebControls.Button

        '''<summary>
        '''RadTreeView1 control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents RadTreeView1 As Global.Telerik.Web.UI.RadTreeView

        '''<summary>
        '''txtDelFolder control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtDelFolder As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtSelectedNode control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtSelectedNode As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtSelectedFolder control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtSelectedFolder As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtSelectedMails control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtSelectedMails As Global.System.Web.UI.WebControls.TextBox

        '''<summary>
        '''txtFixID control.
        '''</summary>
        '''<remarks>
        '''Auto-generated field.
        '''To modify move field declaration from designer file to code-behind file.
        '''</remarks>
        Protected WithEvents txtFixID As Global.System.Web.UI.WebControls.TextBox
    End Class
End Namespace
