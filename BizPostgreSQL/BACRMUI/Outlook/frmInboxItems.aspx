<%@ Page Language="vb" AutoEventWireup="true" CodeBehind="frmInboxItems.aspx.vb"
    Inherits="BACRM.UserInterface.Outlook.frmInboxItems" MasterPageFile="~/common/BizMaster.Master" %>


<%@ Register Assembly="Telerik.Web.UI, Version=2011.2.712.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<%--<%@ Register Src="InboxTree.ascx" TagName="InboxTree" TagPrefix="uc1" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Inbox</title>
    <script type="text/javascript" src="../JavaScript/jquery.tokeninput.js"></script>
    <link href="../Styles/token-input.css" rel="Stylesheet" type="text/css" />
    <link href="../Styles/token-input-facebook1.css" rel="Stylesheet" type="text/css" />
    <style type="text/css">
        .table > tbody > tr > td {
            line-height: 15px !important;
            vertical-align: middle !important;
        }
        #btnNewMove{
            display:none !important;
        }
        .txtSelect2Subject.select2-container {
            margin-top: 0px !important;
        }

        .RadTreeView_Vista .rtSelected .rtIn {
            background-color: transparent !important;
            background-image: none !important;
            border: 1px solid transparent !important;
        }

            .RadTreeView_Vista .rtSelected .rtIn:hover {
                background-color: transparent !important;
                background-image: none !important;
                border: 1px solid transparent !important;
            }

        .RadTreeView_Vista .rtHover .rtIn, .RadTreeView_Vista .rtSelected .rtIn {
            border: 1px solid transparent !important;
        }

        .autocomplete-items {
            border: 1px solid #d4d4d4;
            border-bottom: none;
            border-top: none;
            z-index: 99;
            /*top: 14%;
    left: 34%;
    right: 0;*/
            position: fixed;
            width: 30%;
        }

            .autocomplete-items div {
                padding: 10px;
                cursor: pointer;
                background-color: #fff;
                border-bottom: 1px solid #d4d4d4;
            }

                .autocomplete-items div:hover {
                    /*when hovering an item:*/
                    background-color: #e9e9e9;
                }

        .autocomplete-active {
            /*when navigating through the items using the arrow keys:*/
            background-color: DodgerBlue !important;
            color: #ffffff;
        }

        .tableSpacing tr td {
            padding: 2px;
        }

        #ctl00_MainContent_gvInbox tbody tr td {
            padding: 3px !important;
        }

        .tableSpacing tr th {
            padding: 5px;
            background-color: rgba(0, 0, 0, .15);
            border: 1px solid #e1e1e1;
        }

        .tableSpacing tr td {
            padding: 5px;
            border: 1px solid #e1e1e1;
            word-break: break-all;
        }

        .select2-results .select2-result-label {
            padding: 0px;
        }

        .bigdrop {
            width: 60% !important;
        }

            .bigdrop .col-md-* {
                padding: 0px;
            }

        .smalldrop {
            width: 30% !important;
        }

            .smalldrop .col-md-* {
                padding: 0px;
            }

        .multiselect-group {
            background-color: #3e3e3e !important;
        }

            .multiselect-group a:hover {
                background-color: #3e3e3e !important;
            }

            .multiselect-group a {
                color: #fff !important;
            }

        .multiselect-container > li.multiselect-group label {
            padding: 3px 20px 3px 5px !important;
        }

        .multiselect-container {
            min-width: 250px;
            border: 1px solid #e1e1e1;
        }

        #divEmailInbox {
            height: calc(100vh - 240px);
            overflow-y: scroll;
            background-color: #fff;
        }

        .error {
            color: #D8000C;
            background-color: #FFBABA;
            background-image: url('../images/error.png');
            border: 1px solid;
            margin: 10px 5px 0px 5px;
            padding: 15px 10px 15px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
        }


        .errorStatus {
            color: #D8000C;
            background-color: #FFBABA;
            background-image: url('../images/error.png');
            border: 1px solid;
            margin: 10px 5px 0px 5px;
            padding: 5px 5px 5px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
            background-size: 20px 20px;
        }

        .successStatus {
            border: 1px solid;
            margin: 10px 0px;
            padding: 5px 5px 5px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
            color: #4F8A10;
            background-color: #DFF2BF;
            background-image: url('../images/success.png');
            background-size: 20px 20px;
        }

        .header {
            background: url("../images/TitleBar.gif")repeat;
            font-size: 11px;
        }

        th {
            text-align: left;
        }

        .headerRow {
        }

        .highlightRow {
            background-color: gray;
            cursor: pointer;
            color: White;
        }

        .selectedRow {
            cursor: pointer;
        }

        .Row-border {
            border-bottom: 1px solid #E5E5E5;
        }

        #SearchForm {
            padding: 1px 5px 1px 3px;
            border: 1px solid #ebebeb;
            width: 300px;
        }

        .Searchinput {
            border-left: none !important;
            border-right: none !important;
            border-top: none !important;
            border-bottom: none !important;
            background: none !important;
            border-color: transparent !important; /*Fix IE*/
            width: 270px;
            outline: none !important;
            color: Gray !important;
        }

        .dropdownlistNew {
            border: none;
            font-family: Tahoma,Verdana,Arial,Helvetica;
            color: Gray;
            font-size: 8pt;
            height: 15pt;
        }

        .imageAlign {
            vertical-align: bottom;
        }

        .riTextBox {
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc !important;
        }

        .token-input-list-facebook {
            width: 200px !important;
        }

        .token-input-list-facebook1 {
            overflow: visible;
        }

        .hidecolumn {
            border-left: none !important;
            border-right: none !important;
        }

        .HiddenTextBox {
            width: 1px !important;
            border: 0 !important;
            margin: 0 !important;
            background: none transparent !important;
            visibility: hidden !important;
        }

        .EmailOpenedRow {
            background-color: pink;
        }

        .RadTreeView .rtUL .rtLI .rtUL {
            padding: 0px !important;
        }

        .rtMid, .rtBot, .rtTop {
            border-bottom: 1px solid #f4f4f4;
        }

        .rtLI div {
            border-radius: 0;
            border-top: 0;
            border-left: 3px solid transparent;
            color: #444;
            position: relative;
            display: block;
        }

        .rtHover {
            color: #444 !important;
            background: rgba(248, 182, 63, 0.15) !important;
        }

            .rtHover span {
                color: #444 !important;
                background: rgba(248, 182, 63, 0.15) !important;
            }

        .rtSelected {
            color: #444 !important;
            background-color: #ea941330 !important;
            border-left-color: #e29e35 !important
        }



        .RadTreeView_Vista .rtSelected .rtIn {
            background-color: #ea941330 !important;
            border: #ea941330 !important;
            padding: 7px;
            font-weight: bold;
            background-image: none !important;
        }

        .RadTreeView_Vista, .RadTreeView_Vista a.rtIn, .RadTreeView_Vista .rtEdit .rtIn input, .RadTreeView_Vista .rtIn {
            font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif;
            font-weight: 400;
            font-size: 14px;
        }

        .RadTreeView .rtPlus, .RadTreeView .rtMinus {
            margin-right: 2px;
            margin-left: -13px;
            vertical-align: middle;
            margin: 0px 7px 0 -18px;
        }

        .RadTreeView .rtIn {
            display: inline-block !important;
            white-space: normal !important;
            width: 100% !important;
            height: 35px;
            padding: 7px;
        }

        .RadTreeView_Vista .rtHover .rtIn {
            color: #444;
            background: #f7f7f7;
            border: none;
            padding: 7px;
        }

        .RadTreeView .rtLines .rtLI {
            background: #f3f3f352 !important;
            cursor: pointer;
        }

        .RadTreeView .rtMid, .RadTreeView .rtTop {
            padding: 1px 0 1px 5px !important;
        }

        .rtFirst .rtUL .rtLI :hover {
            background-color: #ea941330 !important;
        }

        .sideIcon {
            height: 30px;
            margin-top: -6px;
            margin-left: -10px;
        }

        .fixedHeightScroll {
            overflow-y: scroll;
            overflow-x: hidden;
            border: 1px solid #c1c1c1;
            height: calc(100vh - 240px);
        }

        .rtLI div {
            border-bottom-color: #e1e1e1;
        }

        .emailLeftSideWidget {
            padding-right: 5px !important;
            width: 20% !important;
        }

        .emailRightSideWidget {
            padding-left: 15px !important;
            width: 80% !important;
            margin-top: 0px !important;
        }

        #ctl00_MainContent_gvInbox > tbody > tr > td {
            border: none;
        }
    </style>
    <link href='<%# ResolveUrl("~/JavaScript/MultiSelect/bootstrap-multiselect.css")%>' rel="stylesheet" />
    <script src='<%# ResolveUrl("~/JavaScript/MultiSelect/bootstrap-multiselect.js")%>'></script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('.option-droup-multiSelection-Group').multiselect({
                enableClickableOptGroups: true,
                onSelectAll: function () {
                    console.log("select-all-nonreq");
                },
                optionClass: function (element) {
                    var value = $(element).attr("class");
                    return value;
                }
            });

        });
        var strSeatchMailConst = 'Search Email';
        function ComposeEmail() {

            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail=&pqwRT=', 'ComposeWindow', 'titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes');

            return false;
        }

        function fnReply(a, b, c) {

            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=outlook&LsEmail=' + a + '&Subj=' + b + '&EmailHstrId=' + c, 'ComposeWindow', 'titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes');

            return false;
        }

        function NewActionItem(a, b) {

            if (a == 0) {
                alert("Email is not Present in Contacts");

            }
            else {

                window.open("../Admin/ActionItemDetailsOld.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=outlook&CntID=" + a + '&EmailHstrId=' + b, '', 'toolbar=no,titlebar=no,left=250, top=150,width=1200,height=600,scrollbars=yes,resizable=no');
                return false;
            }

            return false;
        }

        function NewCase(a, b, c) {

            if (a == 0) {
                alert("Email is not Present in Contacts");

            }
            else {
                window.open("../cases/frmAddCases.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=outlook&DivID=" + b + "&CntID=" + a + '&EmailHstrId=' + c, '', 'toolbar=no,titlebar=no,left=250, top=150,width=850,height=450,scrollbars=no,resizable=no');

            }
            return false;
        }

        function NewProject(a, b, c) {

            if (a == 0) {
                alert("Email is not Present in Contacts");

            }
            else {
                window.open("../Projects/frmProjectAdd.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=outlook&DivID=" + b + "&CntID=" + a + '&EmailHstrId=' + c, '', 'toolbar=no,titlebar=no,left=250, top=150,width=850,height=450,scrollbars=no,resizable=no');

            }
            return false;
        }

        function fn_Mail(txtMailAddr, a, b) {
            if (txtMailAddr != '') {
                if (a == 1) {

                    window.open('mailto:' + txtMailAddr);
                }
                else if (a == 2) {
                    window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&LsEmail=' + txtMailAddr + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
                }

            }

            return false;
        }
        function ReloadMails() {

            document.getElementById('btnGo').click()
        }
        function OpenAttach(a) {
            //    alert(a);
            window.open(a, '', '')
            return false;
        }
        function openIMAP(a) {
            window.open('../admin/frmSMTPPopup.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&UserId=' + a + '&Mode=2', '', 'toolbar=no,titlebar=no,left=200,top=250,width=750,height=200,scrollbars=yes,resizable=yes');
            return false;
        }
        function SetTitle(FName) {
            $('<%# lblInboxTitle.ClientID%>').text(FName);
        }
        function OpenHelp() {
            window.open('../Help/Email.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

    </script>

    <script type="text/javascript">

        var token_count = 0;
        var saved_tokens = [];
        var varCurrentPage = 1;
        var varIsAdvancedsrch = false;
        var varsrch, varsrchFrom, varsrchTo, varsrchSubject, varsrchHas, varsrchAttachment, varsrchNode;
        var fromDate, toDate;
        var varcurrentnode = 1
        var EmailStatus = 0;
        var intUpdateValue;
        var intRecordValue;
        var strUpdateRecordId;
        var varnode;

        $('[id$=ProgressBar]').hide();

        function BindGrid(node) {
            varcurrentnode = node;
            if ($('<%# ddlEmailStatus.ClientID%>').text() != "") {
                EmailStatus = $('<%# ddlEmailStatus.ClientID%>').val();
            }
            else {
                EmailStatus = 0
            }

            $('[id$=ProgressBar]').show();

            if ($('[id$=EmailBox1_lblCurrentPage]').text() != "") {
                varCurrentPage = $('[id$=EmailBox1_lblCurrentPage]').text();
            }
            else {
                varCurrentPage = 1
            }

            varnode = node;

            var emailFrom = "";
            var emailTo = "";
            var dataFrom = $('#txtSearchFrom').val();
            var arrFrom = dataFrom.split(',');

            for (var i = 0; i < arrFrom.length; i++) {
                var arrFinal = arrFrom[i].split('~');

                if (arrFinal.length > 1) {
                    emailFrom = emailFrom == "" ? arrFinal[1] : emailFrom + "," + arrFinal[1];
                }
            }

            var dataTo = $('#txtSearchTo').val();
            var arrTo = dataTo.split(',');

            for (var i = 0; i < arrTo.length; i++) {
                var arrFinal = arrTo[i].split('~');

                if (arrFinal.length > 1) {
                    emailTo = emailTo == "" ? arrFinal[1] : emailTo + "," + arrFinal[1];
                }
            }

            varsrchFrom = emailFrom;
            varsrchTo = emailTo;
            varsrchSubject = $('<%# txtSearchSubject.ClientID%>').val() || '';
            varsrchHas = $('<%#txtSearchHas.ClientID%>').val() || '';
            varsrchAttachment = $('#chkSearchAttachment').length > 0 ? $('#chkSearchAttachment')[0].checked : false;
            varsrch = $("[id$=hdnSimpleSearch]").val() || '';
            var datePickerFrom = $find("<%# radDtFrom.ClientID%>");
            var datePickerTo = $find("<%# radDtTo.ClientID%>");

            if (datePickerFrom != null && datePickerFrom.get_selectedDate() != null) {
                fromDate = datePickerFrom.get_selectedDate().format("yyyy/MM/dd");
            }

            if (datePickerTo != null && datePickerFrom.get_selectedDate() != null) {
                toDate = datePickerTo.get_selectedDate().format("yyyy/MM/dd");
            }

            fromDate = fromDate || '';
            toDate = toDate || '';

            if (varsrch == strSeatchMailConst)
                varsrch = "";

            var numFixId = $("[id$=hdnFixID]").val();


            $.ajax({
                url: "frmEmailInBox.aspx?CurrentPage=" + varCurrentPage + "&NodeId=" + varnode + "&Emailstatus=" + EmailStatus
                    + "&srch=" + varsrch + "&From=" + varsrchFrom + "&To=" + varsrchTo + "&Subject=" + varsrchSubject + "&HasWords=" + varsrchHas
                    + "&Attachment=" + varsrchAttachment + "&IsAdvancedsrch=" + varIsAdvancedsrch + "&srchNode=" + varsrchNode + "&fromDate=" + fromDate + "&toDate=" + toDate + "&filterDate=" + $("[id$=hdnSelectedDate]").val() + "&excludeNonBizContact=" + ($("[id$=chkExcludeNonBizContact]").is(":checked") ? 1 : 0)
                    + "&numFixID=" + numFixId,
                context: document.body,
                success: function (html) {
                    //$('[id$=divEmailInbox]').html('');
                    //$("[id$=divEmailInbox]").html(html);
                    //$('[id$=divEmailInbox]').show();
                    //$('[id$=tblInboxHeader]').show();
                    $('[id$=MailDetail]').html('');
                    $('[id$=MailDetail]').hide();
                    $('[id$=ProgressBar]').hide();
                    BindEmailStatusEvent();
                    //SetNodeSelected();

                    //Bind Recent Correspondance & Alert Panel
                    $('#ctl00_MainContent_gvInbox > tbody > tr:not(:has(table, th))').filter(':has(:checkbox)').each(function () {
                        var numEmailHstrID = $(this).find("[id*='numEmailHstrID']").val();
                        var hdnContactId = $(this).find("[id*='hdnContactId']").val();

                        if (hdnContactId > 0) {
                            //Bind Recent Correspondance
                            if ($(this).find("[id*='lnkRecentCorr']") != null) {
                                var lnkRecentCorr = $(this).find("[id*='lnkRecentCorr']");

                                $.ajax({
                                    type: "POST",
                                    url: "../common/Common.asmx/EmailRecentCorrespondanceCount",
                                    data: "{ContactId:'" + hdnContactId + "'}",
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    success: function (msg) {
                                        $(lnkRecentCorr).text('(' + msg.d + ')');
                                    }
                                });
                            }
                        }
                    });

                    $('#ctl00_MainContent_gvInbox > tbody > tr > td:not(:has([id$=imgNewActionItem]))').css("cursor", "pointer").click(function () {
                        if ($(this).parent().hasClass('Row-border')) {
                            var prevnode = $(this).parent().prev();
                            var hiddenControls = $(this).parent().prev().find(':input');
                            hiddenControls.each(function () {
                                var txtId = $(this).attr("id");
                                var txtValue = $.trim($(this).val());
                                if (txtId.indexOf("numEmailHstrID") > 0) {
                                    intUpdateValue = txtValue;
                                    BindEmailDetail(intUpdateValue, varsrch, varnode, EmailStatus);
                                }
                            }
                            )

                        }
                        else {
                            if ($(this)[0].firstChild.type != 'checkbox' && $(this)[0].firstChild.type != 'select-one' && $(this)[0].firstChild.tagName != "A" && $(this)[0].firstChild.className != "Alert" && $(this)[0].firstChild.className != "ReplyMSGStatus") {
                                //intUpdateValue = $(this).parent().find(':input')[1].defaultValue
                                var hiddenControls = $(this).parent().find(':input')
                                hiddenControls.each(function () {
                                    var txtId = $(this).attr("id");
                                    var txtValue = $.trim($(this).val());
                                    if (txtId.indexOf("numEmailHstrID") > 0) {
                                        intUpdateValue = txtValue;
                                        BindEmailDetail(intUpdateValue, varsrch, varnode, EmailStatus);
                                    }
                                }
                                )
                                BindEmailDetail(intUpdateValue, varsrch, varnode, EmailStatus);
                            }
                        }

                    });

                    $('#ctl00_MainContent_gvInbox > tbody > tr:not(:has(table, th))').mouseover(function () {

                        var MainCurrent;
                        var SubCurrent;

                        if ($(this).hasClass('Row-border')) {
                            SubCurrent = this;
                            MainCurrent = $(this).prev();
                        }
                        else {
                            SubCurrent = $(this).next();
                            MainCurrent = this;
                        }
                        if ($(MainCurrent).children().find("INPUT[type='checkbox']").is(':checked')) {
                            $(MainCurrent).css("background-color", "#FFFFCC");
                            $(SubCurrent).css("background-color", "#FFFFCC");
                        } else {

                            $(MainCurrent).css("background-color", "#f3f4f5");

                            $(SubCurrent).css("background-color", "#f3f4f5");
                        }
                        //$(this).css("background-color", "#f3f4f5");
                        //$(this).next().css("background-color", "#f3f4f5");
                    });

                    $('#ctl00_MainContent_gvInbox > tbody > tr:not(:has(table, th))').mouseout(function () {

                        var MainCurrent;
                        var SubCurrent;

                        if ($(this).hasClass('Row-border')) {
                            SubCurrent = this;
                            MainCurrent = $(this).prev();
                        }
                        else {
                            SubCurrent = $(this).next();
                            MainCurrent = this;
                        }

                        var numNoofTimes = $(this).find("[id*='hdnnumNoofTimes']").val();

                        if (numNoofTimes > 0 && numFixId == 4) {
                            $(MainCurrent).css("background-color", "#DBF9DC");

                            $(SubCurrent).css("background-color", "#DBF9DC");
                        }
                        else {
                            $(MainCurrent).css("background-color", "#FFFFFF");
                            $(SubCurrent).css("background-color", "#FFFFFF");
                        }


                    });

                    var chkBox = $("input[id$='ctl00_MainContent_gvInbox_ctl01_chk']");
                    chkBox.click(
                        function () {
                            $("#ctl00_MainContent_gvInbox INPUT[type='checkbox']").each(function () {
                                $(this).prop('checked', chkBox.is(':checked'));
                                changeColor(this);
                            });
                            var strUpdateRecordId = '';
                             $('#ctl00_MainContent_gvInbox > tbody > tr:not(:has(table, th))').each(function () {
                                if ($(this)[0].attributes[0].nodeValue != "header selectedRow") {
                                    if (($(this).find('input:checkbox').prop("checked") == true)) {
                                        strUpdateRecordId = strUpdateRecordId + $(this).find('input:hidden')[0].value + ',';
                                    }
                                }
                            });
                            if (strUpdateRecordId == '') {
                                $("#sectionForCopyMoveEmails").css("display", "none");
                            } else {
                                
                                $("#sectionForCopyMoveEmails").css("display", "inline");
                            }
                        });
                    // To deselect CheckAll when a GridView CheckBox        // is unchecked
                    $("#ctl00_MainContent_gvInbox INPUT[type='checkbox']").click(
                        function (e) {
                            if (!$(this)[0].checked) {
                                chkBox.prop("checked", false);
                            }
                            changeColor(this);
                             var strUpdateRecordId = '';
                             $('#ctl00_MainContent_gvInbox > tbody > tr:not(:has(table, th))').each(function () {
                                if ($(this)[0].attributes[0].nodeValue != "header selectedRow") {
                                    if (($(this).find('input:checkbox').prop("checked") == true)) {
                                        strUpdateRecordId = strUpdateRecordId + $(this).find('input:hidden')[0].value + ',';
                                    }
                                }
                            });
                            if (strUpdateRecordId == '') {
                                $("#sectionForCopyMoveEmails").css("display", "none");
                            } else {
                                
                                $("#sectionForCopyMoveEmails").css("display", "inline");
                            }
                        });
                }

            });

            return false;
        }

        function OnFilterDateSelected(sender, e) {
            <%--if (e.get_newDate() != null) {
                var date = new Date(e.get_newDate().toDateString());
                $("[id$=hdnSelectedDate]").val(date.format("yyyy/MM/dd"));
              

                var dateControl = $find("<%=rdpDate.ClientID%>");
                dateControl.clear();
            }--%>
        }

        function GetNextDayEmail() {
            $("[id$=hdnIsNextPreviousDaysEmail]").val("1");
            if ($("[id$=hdnSelectedDate]").val() == "") {
                var d = new Date();
                var textBoxControls = $('[id$=gvInbox] input:text');
                textBoxControls.each(function () {
                    if ($.trim($(this).val()).length > 0) {
                        var txtId = $(this).attr("id");
                        var txtValue = $.trim($(this).val());
                        if (txtId.indexOf("DateField~From") > 0) {
                            d = new Date(txtValue);
                        }
                    }
                }
                )

                $("[id$=hdnSelectedDate]").val(d.format("yyyy/MM/dd"));
            }

            var date = new Date($("[id$=hdnSelectedDate]").val());
            date.setDate(date.getDate() + 1);

            if (date.format("yyyy/MM/dd") == new Date().format("yyyy/MM/dd")) {
                $("[id$=hdnSelectedDate]").val("");
                $("[id$=lnknext]").attr("disabled", "true");
            } else {
                $("[id$=hdnSelectedDate]").val(date.format("yyyy/MM/dd"));
            }

            $("#<%=btnUpdateNewEmails.ClientID%>").click();

            // $("[id$=hdnIsNextPreviousDaysEmail]").val("0");

            return false;
        }

        function GetPreviousDayEmail() {
            $("[id$=hdnIsNextPreviousDaysEmail]").val("1");
            if ($("[id$=hdnSelectedDate]").val() == "") {
                var d = new Date();
                var textBoxControls = $('[id$=gvInbox] input:text');
                textBoxControls.each(function () {
                    if ($.trim($(this).val()).length > 0) {
                        var txtId = $(this).attr("id");
                        var txtValue = $.trim($(this).val());
                        if (txtId.indexOf("DateField~From") > 0) {
                            d = new Date(txtValue);
                        }
                    }
                }
                )
                $("[id$=hdnSelectedDate]").val(d.format("yyyy/MM/dd"));
            }

            var date = new Date($("[id$=hdnSelectedDate]").val());
            date.setDate(date.getDate() - 1);
            $("[id$=hdnSelectedDate]").val(date.format("yyyy/MM/dd"));
            $("#<%=btnUpdateNewEmails.ClientID%>").click();
            $("[id$=lnknext]").removeAttr("disabled");
            //$("[id$=hdnIsNextPreviousDaysEmail]").val("0");
            return false;
        }

        function ExcludeNonBizContactChanged() {
            //BindGrid(varnode);
        }
        function sortOrder() {
            if ($("#<%=hdnSortOrder.ClientID%>").val() == "DESC") {
                $("#<%=hdnSortOrder.ClientID%>").val("ASC")
            } else {
                $("#<%=hdnSortOrder.ClientID%>").val("DESC")
            }
            $("#<%=btnUpdateNewEmails.ClientID%>").click();
        }
        function CopyMoveEmails(IsCopied) {
            var SourceFolder = $('[id$=lblInboxTitle]').text();
            var DestinationFolder = $("#<%=ddlFoldersStructure.ClientID%> option:selected").text();
            strUpdateRecordId = ''
            $('#ctl00_MainContent_gvInbox > tbody > tr:not(:has(table, th))').each(function () {
                if ($(this)[0].attributes[0].nodeValue != "header selectedRow") {
                    if (($(this).find('input:checkbox').prop("checked") == true)) {
                        strUpdateRecordId = strUpdateRecordId + $(this).find('input:hidden')[0].value + ',';
                    }
                }
            });

            if (strUpdateRecordId == '')
                UpdateMsgStatus("Error:No Email selected.");
            else if ($("#<%=ddlFoldersStructure.ClientID%> option:selected").val() == 0) {
                UpdateMsgStatus("Error:Please select destination folder.");
            } else if (DestinationFolder == "Sent Mail") {
                UpdateMsgStatus("Error:Can not copy/move to selected folder.");
            } else {
                $("#javascriptUpdateProgress").css("display", "block");
                var dataParam = "{strEmailId:'" + strUpdateRecordId + "',sourceFolder:'" + SourceFolder + "',destinationFolder:'" + $("#<%=ddlFoldersStructure.ClientID%> option:selected").text() + "',destinationFolderId:'" + $("#<%=ddlFoldersStructure.ClientID%> option:selected").val() + "',IsCopied:'" + IsCopied + "'}";
                $.ajax({
                    type: "POST",
                    url: "../Outlook/frmInboxItems.aspx/WebMethodCopyMoveEmails",
                    data: dataParam,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var Jresponse = $.parseJSON(response.d);
                        if (parseFloat(Jresponse) == 1) {
                            UpdateMsgStatus("Data Successfully Saved.");
                            
                            $("#ModalCopyMoveEmail").modal("hide");
                            $("#<%=btnUpdateNewEmails.ClientID%>").click();
                            $("#javascriptUpdateProgress").css("display", "none");
                        } else {
                            UpdateMsgStatus("Error:Some Error Occured.");
                        }
                    },
                    failure: function (response) {
                        $("#javascriptUpdateProgress").css("display", "none");
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $("#javascriptUpdateProgress").css("display", "none");
                    }, complete: function () {
                        $("#javascriptUpdateProgress").css("display", "none");
                    }
                });
                // var win = window.open('../outlook/frmoutlooktree.aspx?frm=Move&numEmailHstrId=' + strUpdateRecordId + "&NodeId=" + varnode, 'OutlookTree', 'toolbar=no,titlebar=no,top=200,left=500,width=320,height=410,scrollbars=yes,resizable=yes');
                //var win = window.open('../outlook/frmoutlooktree.aspx?frm=Move&numEmailHstrId=' + strUpdateRecordId + "&NodeId=" + varnode + "&sourceFolder=" + SourceFolder, 'OutlookTree', 'toolbar=no,titlebar=no,top=200,left=500,width=320,height=410,scrollbars=yes,resizable=yes');
                //win.focus();
            }
        }
        function checkAnyNewMessage() {
            var NodeId = $("#<%=hdnNodeId.ClientID%>").val();
            $.ajax({
                type: "POST",
                url: "../Outlook/frmInboxItems.aspx/WebMethodCheckAnyNewMessage",
                data: "{NodeId:" + NodeId + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var Jresponse = $.parseJSON(response.d);
                    if (Jresponse == "1") {
                        if ($("#MailDetail").is(':visible') == true) { } else {
                            $("#<%=btnUpdateNewEmails.ClientId%>").click();
                        }
                    }
                },
                failure: function (response) {
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }, complete: function () {
                }
            });
        }
        $(document).ready(function () {
            setInterval(function () {
                checkAnyNewMessage();
            }, 30000);
            pageLoaded();

        })
        var headerIsAppend = false;
        function formatListSelection(row) {
            return row["vcSubject"];
        }
        function formatItemEmailGridPage(row) {
            var numOfColumns = 0;
            var ui = '';
            ui = ui + "<table  style=\"width: 100%;\" class=\"tableSpacing\" cellpadding=\"0\" cellspacing=\"0\" >";
            if (!headerIsAppend) {
                ui = ui + "<thead>";
                ui = ui + "<tr>";
                ui = ui + "<th width='10%'>Date</th>";
                ui = ui + "<th width='15%'>From Email</th>";
                ui = ui + "<th width='15%'>To Email</th>";
                ui = ui + "<th width='2%'></th>";
                ui = ui + "<th width='25%'>Subject</th>";
                ui = ui + "<th width='30%'>Body</th>";

                ui = ui + "</tr>";
                ui = ui + "</thead>";
                headerIsAppend = true;
            }
          <%--  var userEmail = '<%=Session("UserEmail")%>';
            var fromIds = row["vcFrom"];
            var toIds = row["vcTo"];
            var fromArray = fromIds.split("$^$")
            var toArray = toIds.split("$^$")
            var fromText = '';
            var toText = '';

            var vcCompanyName = '-';
            if (row["vcCompanyName"] != "null" && row["vcCompanyName"] != null) {
                vcCompanyName = row["vcCompanyName"];
            }
            if (fromArray[2] == userEmail) {
                fromText = fromArray[2];
                if (fromArray[1] != "") {
                    fromText = fromText + "(" + fromArray[1] + ")";
                }
                toText = toArray[2];
                if (vcCompanyName != "-") {
                    toText = toText + "(" + vcCompanyName + ")";
                }
            } else {
                toText = toArray[2];
                if (toArray[1] != "") {
                    toText = toText + "(" + toArray[1] + ")";
                }
                fromText = fromArray[2];
                if (vcCompanyName != "-") {
                    fromText = fromText + "(" + vcCompanyName + ")";
                }
            }--%>
            ui = ui + "<tr>";
                 //ui = ui + "<div class='col-md-2'>"+formatDate(new Date(row["dtReceivedOn"]),$("#<%=hfDateFormat.ClientID%>").val())+"</div>";
            ui = ui + "<td width='10%'>" + formatDate(new Date(row.SearchDate_dtReceivedOn), $("#<%=hfDateFormat.ClientID%>").val()) + "</td>";
            //ui = ui + "<td width='10%'>" + row.SearchDate_dtReceivedOn + "</td>";
            ui = ui + "<td width='15%'>" + row.Search_vcFromEmail + "</td>";
            ui = ui + "<td width='15%'>" + row.Search_vcToEmail + "</td>";
            ui = ui + "<td width='2%' align=\"center\"> <a onclick=\"BindEmailDetail(" + row.id + ", 0, 0, 0)\" href=\"#\"><img src='../images/Icon/iconEmailUnRead.png' style=\"height: 15px\" /></a></td>";
            ui = ui + "<td width='25%'>" + row.Search_vcSubject + "</td>";
            var bodyText = '';
            if (row.Search_vcBodyText.length > 200) {
                bodyText = row.Search_vcBodyText.toString().substring(0, 200) + "...";
            } else {
                bodyText = row.Search_vcBodyText;
            }
            ui = ui + "<td width='30%'>" + bodyText  + "</td>";

            ui = ui + "</tr>";
            ui = ui + "</table>";
            return ui;
        }
        function formatFromItem(row) {
            var ui = '';
            var userEmail = '<%=Session("UserEmail")%>';
            var fromIds = row["vcFrom"];
            var toIds = row["vcTo"];
            var fromArray = fromIds.split("$^$")
            var toArray = toIds.split("$^$")
            var fromText = '';
            var toText = '';

            if (fromArray[2] == userEmail) {
                fromText = fromArray[2] + "(" + fromArray[1] + ")";
            } else {
                toText = toArray[2] + "(" + toArray[1] + ")";
            }
            ui = ui + "<p class='text text-info'>" + fromText + "</p>";
            if ($("<%=chkGroupBySubjectThread.ClientID%>").prop("checked") == true) {
                ui = ui + "<p class='text text-warning'>" + toText + "</p>";
            }
            return ui;
        }
        function formatToItem(row) {
            var ui = '';
            var userEmail = '<%=Session("UserEmail")%>';
            var fromIds = row["vcFrom"];
            var toIds = row["vcTo"];
            var fromArray = fromIds.split("$^$")
            var toArray = toIds.split("$^$")
            var fromText = '';
            var toText = '';

            if (fromArray[2] == userEmail) {
                fromText = fromArray[2] + "(" + fromArray[1] + ")";
            } else {
                toText = toArray[2] + "(" + toArray[1] + ")";
            }
            if ($("<%=chkGroupBySubjectThread.ClientID%>").prop("checked") == true) {
                ui = ui + "<p class='text text-info'>" + fromText + "</p>";
            }
            ui = ui + "<p class='text text-warning'>" + toText + "</p>";
            return ui;
        }
        function on_filter() {

            var filter_condition = '';

            debugger;
            var dropDownControls = $('[id$=gvInbox] tr th select');
            dropDownControls.each(function () {
                debugger;
                if ($(this).get(0).selectedIndex > 0) {
                    var ddId = $(this).attr("id");
                    var ddValue = $(this).val();
                    filter_condition += ddId + ':' + ddValue + ';';
                }
            }
            )

            var textBoxControls = $('[id$=gvInbox] input:text');
            textBoxControls.each(function () {
                if ($.trim($(this).val()).length > 0) {
                    var txtId = $(this).attr("id");
                    var txtValue = $.trim($(this).val());
                    filter_condition += txtId + ':' + txtValue + ';';
                }
            }
            )

            $('#<%=txtGridColumnFilter.ClientID%>').val(filter_condition);
            //console.log($('#txtGridColumnFilter').val());
            //$('#txtCurrrentPage').val('1');
            $('[id$=btnUpdateNewEmails]').trigger('click');
            return false;
        }

        $(document).keydown(function (event) {
            if (event.keyCode == 13) {
                on_filter();
            }

        });

        var FromEmailcurrentFocus = -1;
        var ToEmailcurrentFocus = -1;
        function addToEmailActive(x) {
            /*a function to classify an item as "active":*/
            if (!x) return false;
            /*start by removing the "active" class on all items:*/
            removeToEmailActive(x);
            if (ToEmailcurrentFocus >= x.length) ToEmailcurrentFocus = 0;
            if (ToEmailcurrentFocus < 0) ToEmailcurrentFocus = (x.length - 1);
            /*add class "autocomplete-active":*/
            x[ToEmailcurrentFocus].classList.add("autocomplete-active");
        }
        function removeToEmailActive(x) {
            /*a function to remove the "active" class from all autocomplete items:*/
            for (let i = 0; i < x.length; i++) {
                x[i].classList.remove("autocomplete-active");
            }
        }
        var toExistArray = [];
        function searchServerToEmailId() {
            $("#txtToAutoSuggestEmailautocomplete-list").css("display", "block");
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{NodeId:'" + $("#<%=hdnNodeId.ClientID%>").val() + "',EmailStatus:'" + $("#<%=ddlEmailStatus.ClientID%>").val() + "',intContactFilterType:'" + $("#<%=ddlContactsFilterType.ClientID%>").val() + "',numRelationShipId:'" + $("#<%=ddlRelationship.ClientID%>").val() + "',numProfileId:'" + $("#<%=ddlProfile.ClientID%>").val() + "',bitGroupBySubjectThread:" + $("#<%=chkGroupBySubjectThread.ClientID%>").prop("checked") + ",vcFollowupStatusIds:'" + $("#<%=hdnFollowupstatus.ClientID%>").val() + "',text:'" + $(".txtFromAutoSuggestEmail").val() + "',RegularSearchCriteria: '" + $("#<%=hdnGridFilterRegularSearh.ClientID%>").val() + "',CustomSearchCriteria: '" + $("#<%=hdnGridFilterCustomSearh.ClientID%>").val() + "'}";
            $.ajax({
                type: 'POST',
                url: '<%# ResolveUrl("~/Outlook/frmInboxItems.aspx/WebMethodSearchInbox")%>',
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var Jresponse = $.parseJSON(response.d);
                    $("#txtToAutoSuggestEmailautocomplete-list").html(""); toExistArray = [];
                    if (Jresponse.length > 0) {
                        $.each(Jresponse, function (index, iData) {
                            var ui = '';
                            var userEmail = '<%=Session("UserEmail")%>';
                            var fromIds = iData.vcFrom;
                            var toIds = iData.vcTo;
                            var fromArray = fromIds.split("$^$")
                            var toArray = toIds.split("$^$")
                            var fromText = '';
                            var toText = '';
                            if (toExistArray.indexOf(toArray[2]) == -1) {
                                toExistArray.push(toArray[2]);
                                fromText = fromArray[2];
                                toText = toArray[2];
                                if (toText != '') {
                                    ui = ui + "<p class='text text-warning'>" + toText + "</p>";
                                    if ($("#<%=chkGroupBySubjectThread.ClientID%>").prop("checked") == true) {
                                        ui = ui + "<p class='text text-info'>" + fromText + "</p>";
                                    }
                                    let indivItems =
                                        "<div id=" + iData.Id + " onclick=\"selectToEmail('" + toText + "')\">"
                                        + ui
                                        + "</div>";
                                    $("#txtToAutoSuggestEmailautocomplete-list").append(indivItems);
                                }
                            }
                        });

                    } else {
                        $("#txtToAutoSuggestEmailautocomplete-list").append('<div>No record found</div>');
                    }
                    //var offset = $(".emailRightSideWidget").offset();
                    //$("#txtToAutoSuggestEmailautocomplete-list").css({top: (offset.top-40), left: offset.left+200});
                }
            });
        }

        function addTaskActive(x) {
            /*a function to classify an item as "active":*/
            if (!x) return false;
            /*start by removing the "active" class on all items:*/
            removeTaskActive(x);
            if (FromEmailcurrentFocus >= x.length) FromEmailcurrentFocus = 0;
            if (FromEmailcurrentFocus < 0) FromEmailcurrentFocus = (x.length - 1);
            /*add class "autocomplete-active":*/
            x[FromEmailcurrentFocus].classList.add("autocomplete-active");
        }
        function removeTaskActive(x) {
            /*a function to remove the "active" class from all autocomplete items:*/
            for (let i = 0; i < x.length; i++) {
                x[i].classList.remove("autocomplete-active");
            }
        }
        var fromExistArray = [];
        function searchServerFromEmailId() {
            $("#txtFromAutoSuggestEmailautocomplete-list").css("display", "block");
            var DomainID = '<%= Session("DomainId")%>';
            var dataParam = "{NodeId:'" + $("#<%=hdnNodeId.ClientID%>").val() + "',EmailStatus:'" + $("#<%=ddlEmailStatus.ClientID%>").val() + "',intContactFilterType:'" + $("#<%=ddlContactsFilterType.ClientID%>").val() + "',numRelationShipId:'" + $("#<%=ddlRelationship.ClientID%>").val() + "',numProfileId:'" + $("#<%=ddlProfile.ClientID%>").val() + "',bitGroupBySubjectThread:" + $("#<%=chkGroupBySubjectThread.ClientID%>").prop("checked") + ",vcFollowupStatusIds:'" + $("#<%=hdnFollowupstatus.ClientID%>").val() + "',text:'" + $(".txtFromAutoSuggestEmail").val() + "',RegularSearchCriteria: '" + $("#<%=hdnGridFilterRegularSearh.ClientID%>").val() + "',CustomSearchCriteria: '" + $("#<%=hdnGridFilterCustomSearh.ClientID%>").val() + "'}";

            $.ajax({
                type: 'POST',
                url: '<%# ResolveUrl("~/Outlook/frmInboxItems.aspx/WebMethodSearchInbox")%>',
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var Jresponse = $.parseJSON(response.d);
                    $("#txtFromAutoSuggestEmailautocomplete-list").html(""); fromExistArray = [];
                    if (Jresponse.length > 0) {
                        $.each(Jresponse, function (index, iData) {
                            var ui = '';
                            var userEmail = '<%=Session("UserEmail")%>';
                            var fromIds = iData.vcFrom;
                            var toIds = iData.vcTo;
                            var fromArray = fromIds.split("$^$")
                            var toArray = toIds.split("$^$")
                            var fromText = '';
                            var toText = '';
                            if (fromExistArray.indexOf(fromArray[2]) == -1) {
                                fromExistArray.push(fromArray[2]);
                                fromText = fromArray[2];
                                toText = toArray[2];
                                if (fromText != '') {
                                    ui = ui + "<p class='text text-info'>" + fromText + "</p>";
                                    if ($("#<%=chkGroupBySubjectThread.ClientID%>").prop("checked") == true) {
                                        ui = ui + "<p class='text text-warning'>" + toText + "</p>";
                                    }
                                    let indivItems =
                                        "<div id=" + iData.Id + " onclick=\"selectFromEmail('" + fromText + "')\">"
                                        + ui
                                        + "</div>";
                                    $("#txtFromAutoSuggestEmailautocomplete-list").append(indivItems);
                                }
                            }
                        });

                    } else {
                        $("#txtFromAutoSuggestEmailautocomplete-list").append('<div>No record found</div>');
                    }
                    //var offset = $(".emailRightSideWidget").offset();
                    //$("#txtFromAutoSuggestEmailautocomplete-list").css({top: (offset.top-40), left: offset.left});
                }
            });
        }
        function selectFromEmail(txt) {
            $(".txtFromAutoSuggestEmail").val(txt);
            $("#txtFromAutoSuggestEmailautocomplete-list").css("display", "none");
        }
        function selectToEmail(txt) {
            $(".txtToAutoSuggestEmail").val(txt);
            $("#txtToAutoSuggestEmailautocomplete-list").css("display", "none");
        }
        function throttle(f, delay) {
            let timer = null;
            return function () {
                let context = this, args = arguments;
                clearTimeout(timer);
                timer = window.setTimeout(function () {
                    f.apply(context, args);
                },
                    delay || 300);
            };
        }
       <%-- function bindProfile() {
            var dataParam = "{RelationshipId:'" + $("#<%=ddlRelationship.ClientID%> option:selected").val() + "'}";
            $.ajax({
                type: 'POST',
                url: '<%# ResolveUrl("~/Outlook/frmInboxItems.aspx/WebMethodBindProfile")%>',
                data: dataParam,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var Jresponse = $.parseJSON(response.d);
                    $("#<%=ddlProfile.ClientID%>").empty();
                    $("#<%=ddlProfile.ClientID%>").html("");
                    $("#<%=ddlProfile.ClientID%>").append("<option values='0'>No Filter Set</option>");
                    if (Jresponse.length > 0) {
                        $.each(Jresponse, function (index, iData) {
                            $("#<%=ddlProfile.ClientID%>").append("<option values='" + iData.numListItemID + "'>" + iData.SecondaryListItem + "</option>");
                        });

                    }
                }
            });
        }--%>
        function throttleToEmail(f, delay) {
            let timer = null;
            return function () {
                let context = this, args = arguments;
                clearTimeout(timer);
                timer = window.setTimeout(function () {
                    f.apply(context, args);
                },
                    delay || 300);
            };
        }
        $(document).ready(function () {

        });
        function pageLoaded() {
            var FromEmailcurrentFocus = -1;
            var ToEmailcurrentFocus = -1;
            $(".txtToAutoSuggestEmail").keypress(throttleToEmail(function () {
                searchServerToEmailId();
            }));
            var chkBox = $("input[id$='ctl00_MainContent_gvInbox_ctl01_chk']");
            chkBox.click(
                function () {
                    $("#ctl00_MainContent_gvInbox INPUT[type='checkbox']").each(function () {
                        $(this).prop('checked', chkBox.is(':checked'));
                        changeColor(this);
                    });
                     var strUpdateRecordId = '';
                             $('#ctl00_MainContent_gvInbox > tbody > tr:not(:has(table, th))').each(function () {
                                if ($(this)[0].attributes[0].nodeValue != "header selectedRow") {
                                    if (($(this).find('input:checkbox').prop("checked") == true)) {
                                        strUpdateRecordId = strUpdateRecordId + $(this).find('input:hidden')[0].value + ',';
                                    }
                                }
                            });
                            if (strUpdateRecordId == '') {
                                $("#sectionForCopyMoveEmails").css("display", "none");
                            } else {
                                
                                $("#sectionForCopyMoveEmails").css("display", "inline");
                            }
                });
            // To deselect CheckAll when a GridView CheckBox        // is unchecked
            $("#ctl00_MainContent_gvInbox INPUT[type='checkbox']").click(
                function (e) {
                    if (!$(this)[0].checked) {
                        chkBox.prop("checked", false);
                    }
                    changeColor(this);
                     var strUpdateRecordId = '';
                             $('#ctl00_MainContent_gvInbox > tbody > tr:not(:has(table, th))').each(function () {
                                if ($(this)[0].attributes[0].nodeValue != "header selectedRow") {
                                    if (($(this).find('input:checkbox').prop("checked") == true)) {
                                        strUpdateRecordId = strUpdateRecordId + $(this).find('input:hidden')[0].value + ',';
                                    }
                                }
                            });
                            if (strUpdateRecordId == '') {
                                $("#sectionForCopyMoveEmails").css("display", "none");
                            } else {
                                
                                $("#sectionForCopyMoveEmails").css("display", "inline");
                            }
                });
            $(".txtToAutoSuggestEmail").keydown(function (e) {
                var x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");
                if (e.keyCode == 40) {
                    /*If the arrow DOWN key is pressed,
                    increase the FromEmailcurrentFocus variable:*/
                    ToEmailcurrentFocus++;
                    /*and and make the current item more visible:*/
                    addToEmailActive(x);
                } else if (e.keyCode == 38) { //up
                    /*If the arrow UP key is pressed,
                    decrease the FromEmailcurrentFocus variable:*/
                    ToEmailcurrentFocus--;
                    /*and and make the current item more visible:*/
                    addToEmailActive(x);
                } else if (e.keyCode == 13) {
                    /*If the ENTER key is pressed, prevent the form from being submitted,*/
                    e.preventDefault();
                    if (ToEmailcurrentFocus > -1) {
                        /*and simulate a click on the "active" item:*/
                        if (x) x[ToEmailcurrentFocus].click();
                    }
                }
            });
            $(".txtFromAutoSuggestEmail").keypress(throttle(function () {
                searchServerFromEmailId();
            }));
            $(".txtFromAutoSuggestEmail").keydown(function (e) {
                var x = document.getElementById(this.id + "autocomplete-list");
                if (x) x = x.getElementsByTagName("div");
                if (e.keyCode == 40) {
                    /*If the arrow DOWN key is pressed,
                    increase the FromEmailcurrentFocus variable:*/
                    FromEmailcurrentFocus++;
                    /*and and make the current item more visible:*/
                    addTaskActive(x);
                } else if (e.keyCode == 38) { //up
                    /*If the arrow UP key is pressed,
                    decrease the FromEmailcurrentFocus variable:*/
                    FromEmailcurrentFocus--;
                    /*and and make the current item more visible:*/
                    addTaskActive(x);
                } else if (e.keyCode == 13) {
                    /*If the ENTER key is pressed, prevent the form from being submitted,*/
                    e.preventDefault();
                    if (FromEmailcurrentFocus > -1) {
                        /*and simulate a click on the "active" item:*/
                        if (x) x[FromEmailcurrentFocus].click();
                    }
                }
            });
            $('[id$=lnkPageNext]').click(function () {
                debugger;
                var CurrentPage = $('#<%=txtCurrrentPage.ClientID%>').val();
                if (parseInt(CurrentPage) < parseInt($('#<%=lblTotal.ClientID%>').text())) {
                    CurrentPage = parseInt(CurrentPage) + 1;
                }
                $('#<%=txtCurrrentPage.ClientID%>').val(CurrentPage.toString());

                $('[id$=btnUpdateNewEmails]').trigger('click');

            });
            $('[id$=lnkPagePrevious]').click(function () {
                var CurrentPage = $('#<%=txtCurrrentPage.ClientID%>').val();
                if (parseInt(CurrentPage) > 1) {
                    CurrentPage = parseInt(CurrentPage) - 1;
                }


                $('#<%=txtCurrrentPage.ClientID%>').val(CurrentPage.toString());

                $('[id$=btnUpdateNewEmails]').trigger('click');
            });
            $("[id$=gvInbox] input:text").keydown(function (event) {
                if (event.keyCode == 13) {
                    console.log($('#txtGridColumnFilter').val());

                    debugger;
                    on_filter();
                    return false;
                }
            });
            $('.txtSelect2Subject').select2(
                {
                    placeholder: 'Enter search text...',
                    minimumInputLength: 1,
                    delay: 100,
                    multiple: false,
                    escapeMarkup: function (markup) { return markup; },
                    formatResult: formatItemEmailGridPage,
                    //formatSelection: formatListSelection,
                    width: "100%",
                    dropdownCssClass: 'bigdrop',
                    dropdownAutoWidth: false,
                    dataType: "json",
                    id: "numEmailHstrID",
                    allowClear: true,
                    ajax: {
                        quietMillis: 500,
                        url: '<%# ResolveUrl("~/common/Common.asmx/SimpleElasticSearch")%>',
                        type: 'POST',
                        params: {
                            contentType: 'application/json; charset=utf-8'
                        },
                        dataType: 'json',
                        data: function (term, page) {
                            headerIsAppend = false;
                            return JSON.stringify({
                                searchText: term.replace(/ /g, "\\ "),
                                pageIndex: page,
                                pageSize: 30,
                                startsWithSearch: false,
                                emailSearch: true
                            });
                        },
                        results: function (data, page) {
                            //headerIsAppend = false;
                            //var more = false;
                            //var args = $.parseJSON(data.d);
                            //debugger
                            //return { results: JSON.stringify(args.results), more: more };
                            if (data.hasOwnProperty("d")) {
                                if (data.d == "Session Expired") {
                                    alert("Session expired.");
                                    window.opener.location.href = window.opener.location.href;
                                    window.close();
                                } else {
                                    data = $.parseJSON(data.d)
                                }
                            }
                            else
                                data = $.parseJSON(data);


                            return { "results": $.parseJSON(data.results == "" ? null : data.results), "more": (page.page * 30) < data.Total };

                        }
                    }
                });


            //Auto Suggest Search Class teacher
            $('.txtSelect2Subject').on("change", function (e) {
                var item = $('.txtSelect2Subject').select2('data');
                if (item != null) {
                   BindEmailDetail(item.id, '', '', '')
                }
            });
            $('.option-droup-multiSelection-Group').multiselect({
                enableClickableOptGroups: true,
                onSelectAll: function () {
                    console.log("select-all-nonreq");
                },
                optionClass: function (element) {
                    var value = $(element).attr("class");
                    return value;
                }
            });

            //BindGrid(0);
            //ExcludeNonBizContactChanged();



            $('[id$=imgShowAdv]').click(function () {
                $('[id$=ContentAdvancedSearch]').toggle();
                $('[id$=hdnVisibility]').val($('[id$=ContentAdvancedSearch]').css('display'))

                //Script ended by Sachin
                if ($('[id$=ContentAdvancedSearch]').css('display') == 'none') {
                    $('[id$=imgShowAdv]').attr('src', '../images/Down-arrow-inv1.png');
                    $('[id$=lblInboxTitle]').text('Inbox');
                    varIsAdvancedsrch = false;
                }
                else {
                    $('[id$=imgShowAdv]').attr('src', '../images/Up-arrow-inv.png');
                    $('[id$=lblInboxTitle]').text('Advanced search');
                }

                $("[id$=btnSaveAdvanceSearchState]").click();
            })

            $('[id$=imgHideAdv]').click(function () {
                $('[id$=ContentAdvancedSearch]').hide();
                $('[id$=imgShowAdv]').attr('src', $('[id$=imgHideAdv]').attr('src'));
            })

            $('[id$=ddlReadstatus]').change(function () {

                if ($('[id$=ddlReadstatus]').val() > -1) {
                    strUpdateRecordId = ''
                    $('#ctl00_MainContent_gvInbox > tbody > tr:not(:has(table, th))').each(function () {
                        if ($(this)[0].attributes[0].nodeValue != "header selectedRow") {
                            if (($(this).find('input:checkbox').prop("checked") == true)) {
                                strUpdateRecordId = strUpdateRecordId + $(this).find('input:hidden')[0].value + ',';
                            }
                        }
                    });
                    if (strUpdateRecordId == '')
                        UpdateMsgStatus("Error:No Email selected.");
                    else
                        BindMultipleEmailStatusEvent();
                    //alert(strUpdateRecordId);
                }
            });

            $('[id$=btnDeleteEmail]').click(function () {
                strUpdateRecordId = ''
                $('#ctl00_MainContent_gvInbox > tbody > tr:not(:has(table, th))').each(function () {
                    if ($(this)[0].attributes[0].nodeValue != "header selectedRow") {
                        if (($(this).find('input:checkbox').prop("checked") == true)) {
                            strUpdateRecordId = strUpdateRecordId + $(this).find('input:hidden')[0].value + ',';
                        }
                    }
                });
                if (strUpdateRecordId == '') {
                    UpdateMsgStatus("Error:No Email selected.");
                }
                else {
                    var numFixID = $("[id$=hdnFixID]").val();

                    var message;

                    if (numFixID == 2) {
                        message = "Selected emails will be deleted permanently from biz and same will be moved to trash in Gmail. Do you want to proceed?"
                    }
                    else {
                        message = 'Selected emails will be moved to "Email Archive" folder. Do you want to proceed?'
                    }

                    if (confirm(message)) {
                        DeleteMultipleEmail(numFixID);
                    }
                }
            });


            $('[id$=btnArchive]').click(function () {
                strUpdateRecordId = ''
                $('#ctl00_MainContent_gvInbox > tbody > tr:not(:has(table, th))').each(function () {
                    if ($(this)[0].attributes[0].nodeValue != "header selectedRow") {
                        if (($(this).find('input:checkbox').prop("checked") == true)) {
                            strUpdateRecordId = strUpdateRecordId + $(this).find('input:hidden')[0].value + ',';
                        }
                    }
                });
                if (strUpdateRecordId == '')
                    UpdateMsgStatus("Error:No Email selected.");
                else
                    if (confirm('Selected emails will be moved to "Email Archive" folder. Do you want to proceed?')) {
                        ArchiveEmail();
                    }
            });

            $('[id$=ddlEmailStatus]').change(function () {
                $("#<%=btnUpdateNewEmails.ClientID%>").click();
            });
            $('[id$=ddlContactsFilterType]').change(function () {
                $("#<%=btnUpdateNewEmails.ClientID%>").click();
            });
            $('[id$=ddlRelationship]').change(function () {
                $("#<%=btnUpdateNewEmails.ClientID%>").click();
                bindProfile();
            });

            $('[id$=ddlProfile]').change(function () {
                $("#<%=hdnProfileId.ClientID%>").val($('[id$=ddlProfile] option:selected').val());
                $("#<%=btnUpdateNewEmails.ClientID%>").click();
            });

            $('[id$=chkGroupBySubjectThread]').change(function () {
                $("#<%=btnUpdateNewEmails.ClientID%>").click();
            });
            $("#<%=btnUpdateNewEmails.ClientID%>").click(function () {
                var followUpStatusId = "";
                var followUpStatusIdArray = $('[id$=ddlFollowUpStatus]').val();
                if (followUpStatusIdArray != null) {
                    followUpStatusId = followUpStatusIdArray.toString();
                }
                if (followUpStatusId.length > 0) {
                    $("#<%=hdnFollowupstatus.ClientID%>").val(followUpStatusId);
                }
            });

            $('[id$=btnGo]').click(function () {
                $('#txtSearchFrom').tokenInput("clear");
                $('#txtSearchTo').tokenInput("clear");
                $('[id$=txtSearchSubject]').val("");
                $('[id$=txtSearchHas]').val("");
                $('[id$=chkSearchAttachment]').prop('checked', false);

                varIsAdvancedsrch = false;
                //BindGrid(0);
                return false;
            });

            $('[id$=btnAdavancedSearch]').click(function () {

                var datePickerFrom = $find("<%# radDtFrom.ClientID%>");
                var datePickerTo = $find("<%#radDtTo.ClientID%>");
                fromDate = datePickerFrom.get_selectedDate();
                toDate = datePickerTo.get_selectedDate();

                if (!(fromDate == null && toDate == null)) {
                    if (fromDate == null || toDate == null) {
                        alert("From and To dates are required.");
                        return false;
                    } else if (toDate < fromDate) {
                        alert("To date should be same or after from date.");
                        return false;
                    }
                }

                varIsAdvancedsrch = true;
                //BindGrid(varnode);
            });


            $('[id$=openSort]').click(function () {
                var win = window.open('../outlook/frmSortoutlooktree.aspx', 'OutlookTree', 'toolbar=no,titlebar=no,top=200,left=500,width=320,height=410,scrollbars=yes,resizable=yes');
                win.focus();
            });
            $("[id$=chkDateRange]").change(function () {
                var datePickerFrom = $find("<%#radDtFrom.ClientID%>");
                var datePickerTo = $find("<%#radDtTo.ClientID%>");

                if (this.checked) {
                    if (datePickerFrom != null) {
                        datePickerFrom.set_enabled(true);
                    }
                    if (datePickerTo != null) {
                        datePickerTo.set_enabled(true);
                    }
                }
                else {
                    if (datePickerFrom != null) {
                        datePickerFrom.set_enabled(false);
                    }
                    if (datePickerTo != null) {
                        datePickerTo.set_enabled(false);
                    }
                }
            });

            $("#txtSearchFrom").tokenInput("../contact/EmailList.ashx", {
                txtInput: "SearchFrom",
                theme: "facebook1",
                //tokenDelete: "token-input-delete-token-facebook",
                propertyToSearch: "first_name",
                resultsFormatter: function (item) { return "<li><div style='display: inline-block; padding-left: 10px;'><div class='full_name'>" + item.first_name + " " + item.last_name + "</div><div class='email'>" + item.email + "</div></div></li>" },
                tokenFormatter: function (item) { return "<li><p title='" + item.email + "'>" + item.first_name + " " + item.last_name + "</p></li>" }

            });

            $("#txtSearchTo").tokenInput("../contact/EmailList.ashx", {
                txtInput: "SearchTo",
                theme: "facebook1",
                //tokenDelete: "token-input-delete-token-facebook",
                propertyToSearch: "first_name",
                resultsFormatter: function (item) { return "<li><div style='display: inline-block; padding-left: 10px;'><div class='full_name'>" + item.first_name + " " + item.last_name + "</div><div class='email'>" + item.email + "</div></div></li>" },
                tokenFormatter: function (item) { return "<li><p title='" + item.email + "'>" + item.first_name + " " + item.last_name + "</p></li>" }

            });
            $('#ctl00_MainContent_gvInbox > tbody > tr > td:not(:has([class$=imgNewActionItem]))').css("cursor", "pointer").click(function () {
                if ($(this).parent().hasClass('Row-border')) {
                    var prevnode = $(this).parent().prev();
                    var hiddenControls = $(this).parent().prev().find(':input');
                    hiddenControls.each(function () {
                        var txtId = $(this).attr("id");
                        var txtValue = $.trim($(this).val());
                        if (txtId.indexOf("numEmailHstrID") > 0) {
                            intUpdateValue = txtValue;
                            BindEmailDetail(intUpdateValue, varsrch, $("#<%=hdnNodeId.ClientID%>").val(), EmailStatus);
                        }
                    }
                    )

                }
                else {
                    if ($(this)[0].firstChild.type != 'checkbox' && $(this)[0].firstChild.type != 'select-one' && $(this)[0].firstChild.tagName != "A" && $(this)[0].firstChild.className != "Alert" && $(this)[0].firstChild.className != "ReplyMSGStatus") {
                        //intUpdateValue = $(this).parent().find(':input')[1].defaultValue
                        var hiddenControls = $(this).parent().find(':input')
                        hiddenControls.each(function () {
                            var txtId = $(this).attr("id");
                            var txtValue = $.trim($(this).val());
                            if (txtId.indexOf("numEmailHstrID") > 0) {
                                intUpdateValue = txtValue;
                                BindEmailDetail(intUpdateValue, varsrch, $("#<%=hdnNodeId.ClientID%>").val(), EmailStatus);
                            }
                        }
                        )
                        // BindEmailDetail(intUpdateValue, varsrch, varnode, EmailStatus);
                    }
                }

            });

            $('#ctl00_MainContent_gvInbox > tbody > tr:not(:has(table, th))').mouseover(function () {

                var MainCurrent;
                var SubCurrent;

                if ($(this).hasClass('Row-border')) {
                    SubCurrent = this;
                    MainCurrent = $(this).prev();
                }
                else {
                    SubCurrent = $(this).next();
                    MainCurrent = this;
                }
                if ($(MainCurrent).children().find("INPUT[type='checkbox']").is(':checked')) {
                    $(MainCurrent).css("background-color", "#FFFFCC");
                    $(SubCurrent).css("background-color", "#FFFFCC");
                } else {

                    $(MainCurrent).css("background-color", "#f3f4f5");

                    $(SubCurrent).css("background-color", "#f3f4f5");
                }
                //$(this).css("background-color", "#f3f4f5");
                //$(this).next().css("background-color", "#f3f4f5");
            });

            $('#ctl00_MainContent_gvInbox > tbody > tr:not(:has(table, th))').mouseout(function () {

                var MainCurrent;
                var SubCurrent;

                if ($(this).hasClass('Row-border')) {
                    SubCurrent = this;
                    MainCurrent = $(this).prev();
                }
                else {
                    SubCurrent = $(this).next();
                    MainCurrent = this;
                }

                var numNoofTimes = $(this).find("[id*='hdnnumNoofTimes']").val();

                if (numNoofTimes > 0 && numFixId == 4) {
                    $(MainCurrent).css("background-color", "#DBF9DC");

                    $(SubCurrent).css("background-color", "#DBF9DC");
                }
                else {
                    $(MainCurrent).css("background-color", "#FFFFFF");
                    $(SubCurrent).css("background-color", "#FFFFFF");
                }


            });
            BindEmailStatusEvent();
        }

        $(document).ready(function () {
            BindEmailStatusEvent();
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(pageLoaded);
            prm.add_endRequest(function () {
                $('.option-droup-multiSelection-Group').multiselect({
                    enableClickableOptGroups: true,
                    onSelectAll: function () {
                        console.log("select-all-nonreq");
                    },
                    optionClass: function (element) {
                        var value = $(element).attr("class");
                        return value;
                    }
                });
            });
            var docScrollTop;
            function BeginRequestHandler(sender, args) {

                docScrollTop = $("#divEmailInbox").scrollTop();
            }

            function EndRequestHandler(sender, args) {

                $("#divEmailInbox").scrollTop(docScrollTop);
            }

            prm.add_beginRequest(BeginRequestHandler);
            prm.add_endRequest(EndRequestHandler);
            //setTimeout(function () {

            //    $('.txtSelect2Subject').select2("open");
            //    $('.txtSelect2Subject').focus();
            //}, 100)
        });

        function changeColor(control) {
            if ($(control).is(':checked')) {
                $(control).parent().parent().next().css("background-color", "#FFFFCC");
                $(control).parent().parent().css("background-color", "#FFFFCC");
            }
            else {
                $(control).parent().parent().next().css("background-color", "#fff");
                $(control).parent().parent().css("background-color", "#fff");
            }
        }
        function updateReadUnreadStatus(isRead) {
            strUpdateRecordId = ''
            var isreadStatus = true;
            if (isRead == "0") {
                isreadStatus = false;
            }
            $('#<%=gvInbox.ClientID%> > tbody > tr:not(:has(table, th))').each(function () {
                if ($(this)[0].attributes[0].nodeValue != "header selectedRow") {
                    if (($(this).find('input:checkbox').prop("checked") == true)) {
                        strUpdateRecordId = strUpdateRecordId + $(this).find('input:hidden')[0].value + ',';
                    }
                }
            });
            if (strUpdateRecordId == '') {
                UpdateMsgStatus("Error:No Email selected.");
            } else {
                $("#javascriptUpdateProgress").css("display", "block")
                $.ajax({
                    type: "POST",
                    url: "../outlook/frmInboxItems.aspx/WebMethodUpdateReadUnreadStatus",
                    data: "{strEmailHstrId:'" + strUpdateRecordId + "',isRead:'" + isreadStatus + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        $("#javascriptUpdateProgress").css("display", "none")
                        $("#<%=btnUpdateNewEmails.ClientID%>").click();
                        UpdateMsgStatus("Status Updated Successfully.");
                    },
                    failure: function (response) {
                        $("#javascriptUpdateProgress").css("display", "none");
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                        $("#javascriptUpdateProgress").css("display", "none");
                    }, complete: function () {
                        $("#javascriptUpdateProgress").css("display", "none");
                    }
                });
            }
        }
        function deleteSelectedEmails() {
            strUpdateRecordId = ''
            $('#<%=gvInbox.ClientID%> > tbody > tr:not(:has(table, th))').each(function () {
                if ($(this)[0].attributes[0].nodeValue != "header selectedRow") {
                    if (($(this).find('input:checkbox').prop("checked") == true)) {
                        strUpdateRecordId = strUpdateRecordId + $(this).find('input:hidden')[0].value + ',';
                    }
                }
            });
            if (strUpdateRecordId == '') {
                UpdateMsgStatus("Error:No Email selected.");
            } else {
                $("#javascriptUpdateProgress").css("display", "block");
                $.ajax({
                    type: "POST",
                    url: "../common/Common.asmx/DeleteMultipleEmail",
                    data: "{strUpdateRecordId:'" + strUpdateRecordId + "',NodeId:'" + $("#<%=hdnNodeId.ClientID%>").val() + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {

                        $("#javascriptUpdateProgress").css("display", "none");
                        UpdateMsgStatus("Selected Emails are deleted successfully.");
                        $("#<%=btnUpdateNewEmails.ClientID%>").click();
                    }
                });
            }
        }
        function BindEmailDetail(intUpdateValue, varsrch, varnode, EmailStatus) {
            $('.txtSelect2Subject').select2("close");
            $('[id$=ProgressBar]').show();
            $.ajax({
                url: "frmMailDtl.aspx?numEmailHstrID=" + intUpdateValue + "&srch=" + varsrch + "&NodeId=" + varnode + "&Emailstatus=" + EmailStatus,
                context: document.body,
                success: function (html) {
                    $('[id$=MailDetail]').html('');
                    $("[id$=divEmailInbox]").hide();

                    $("#divTopFilter").hide();
                    //$('[id$=tblInboxHeader]').hide();
                    $("[id$=MailDetail]").show();
                    $("[id$=MailDetail]").html(html);
                    $('base').remove();

                    $("[id$=trBody] a").attr("target", "_blank");

                    $('[id$=ddlAssignTO1]').change(function () {
                        var varhypFrom = $('[id$=hypFrom]').text();
                        var lngDivisionId = $('[id$=hdnDivisionId]').val();
                        var lngDomainId = $('[id$=hdnDomainId]').val();
                        var type = $('[id$=ddlAssignTO1]').val();
                        $("[id$=ddlOpenRecord]").empty();
                        //alert(varhypFrom);
                        $.ajax({
                            type: "POST",
                            url: "../common/Common.asmx/FillOpenRecords",
                            data: "{lngDivisionID:'" + lngDivisionId + "',lngDomainID:'" + lngDomainId + "',Type:'" + type + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (msg) {
                                var json = eval(msg.d);

                                for (i = 0; i < json.length; i++) {
                                    $("[id$=ddlOpenRecord]").get(0).options[$("[id$=ddlOpenRecord]").get(0).options.length] = new Option(json[i].value, json[i].id);
                                }

                            }
                        });
                    });
                    $('[id$=btnAdd]').click(function () {
                        var lngOpenRecord = $("[id$=ddlOpenRecord]").val();
                        var lngEmailHistoryid = $("[id$=hdnEmailId]").val();
                        var lngAssignval = $('[id$=ddlAssignTO1]').val();
                        $.ajax({
                            type: "POST",
                            url: "../common/Common.asmx/ManageCorrespondence",
                            data: "{lngOpenRecord:'" + lngOpenRecord + "',lngEmailHistoryid:'" + lngEmailHistoryid + "',lngAssignval:'" + lngAssignval + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (msg) {
                                alert('Saved');
                            }
                        });
                    });

                    BindInlineEdit(); // Binding Followup Details
                }
            });


        }
        function BackToMail(type) {
            $('[id$=MailDetail]').html('');
            $('[id$=MailDetail]').hide();
            $('[id$=divEmailInbox]').show();
            $("#divTopFilter").show();
            if (type != "1") {
                $("#<%=btnUpdateNewEmails.ClientID%>").click();
            }
            //BindGrid(varcurrentnode);
        }

        function MovePrevNextEmail(intUpdateValue, varsrch, varnode, EmailStatus) {
            BindEmailDetail(intUpdateValue, varsrch, varnode, EmailStatus);
        }

        function deleteEMail(ERecordId) {
            $.ajax({
                type: "POST",
                url: "../common/Common.asmx/DeleteMultipleEmail",
                data: "{strUpdateRecordId:'" + ERecordId + "',NodeId:'" + $("[id$=hdnFixID]").val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                }
            });
            BackToMail();
        }

        function PrintEMail(ERecordId) {
            window.open("../outlook/frmMailDtl.aspx?Print=1&numEmailHstrID=" + ERecordId, 'Message', 'titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes');
        }

        function OpenNewWindowEMail(ERecordId) {
            window.open("../outlook/frmMailDtl.aspx?numEmailHstrID=" + ERecordId, 'Message', 'titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes');
        }

        function BindEmailStatusEvent() {
            $('.target').change(function () {
                debugger;
                //alert('hi');
                var $el = $(this).val();
                //alert($el);

                // var node = $(this).parent().parent();
                //intUpdateValue = node[0].cells[1].firstChild.value;
                var hiddenControls = $(this).parent().parent().find(':input')
                hiddenControls.each(function () {
                    var txtId = $(this).attr("id");
                    var txtValue = $.trim($(this).val());
                    if (txtId.indexOf("numEmailHstrID") > 0) {
                        intUpdateValue = txtValue;

                    }
                }
                )

                $.ajax({
                    type: "POST",
                    url: "../common/Common.asmx/UpdateEmailStatus",
                    data: "{intUpdateRecordId:'" + intUpdateValue + "',intUpdatevalue:'" + $el + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                    }
                });
            });
        }

        function BindMultipleEmailStatusEvent() {
            $.ajax({
                type: "POST",
                url: "../common/Common.asmx/UpdateMultipleEmailStatus",
                data: "{strUpdateRecordId:'" + strUpdateRecordId + "',intUpdatevalue:'" + $('[id$=ddlReadstatus]').val() + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    UpdateMsgStatus(msg.d);
                    $('[id$=ddlReadstatus]').val("-1");
                }
            });
            // BindGrid(varnode);
        }

        function DeleteMultipleEmail(numFixID) {
            $.ajax({
                type: "POST",
                url: "../common/Common.asmx/DeleteMultipleEmail",
                data: "{strUpdateRecordId:'" + strUpdateRecordId + "',NodeId:'" + numFixID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    UpdateMsgStatus(msg.d);
                }
            });
            //BindGrid(varnode);
        }

        function MarkAsSpam() {
            strUpdateRecordId = ''
            $('#<%=gvInbox.ClientID%> > tbody > tr:not(:has(table, th))').each(function () {
                if ($(this)[0].attributes[0].nodeValue != "header selectedRow") {
                    if (($(this).find('input:checkbox').prop("checked") == true)) {
                        strUpdateRecordId = strUpdateRecordId + $(this).find('input:hidden')[0].value + ',';
                    }
                }
            });
            if (strUpdateRecordId == '') {
                UpdateMsgStatus("Error:No Email selected.");
            } else {
                $.ajax({
                    type: "POST",
                    url: "../common/Common.asmx/MarkAsSpam",
                    data: "{strUpdateRecordId:'" + strUpdateRecordId + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        UpdateMsgStatus(msg.d);
                    }
                });
                $("#<%=btnUpdateNewEmails.ClientID%>").click();
            }
        }

        function ArchiveEmail() {
            $.ajax({
                type: "POST",
                url: "../common/Common.asmx/ArchiveEmail",
                data: "{strUpdateRecordId:'" + strUpdateRecordId + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    UpdateMsgStatus(msg.d);
                }
            });
            //BindGrid(varnode);
        }

        function Restore() {
            $.ajax({
                type: "POST",
                url: "../common/Common.asmx/Restore",
                data: "{strUpdateRecordId:'" + strUpdateRecordId + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    UpdateMsgStatus(msg.d);
                }
            });
            //BindGrid(varnode);
        }

        function UpdateMsgStatus(message) {
            $('[id$=lblmsg]').text(message);

            if (message.toLowerCase().indexOf("error") >= 0)
                $('[id$=lblmsg]').addClass('errorStatus');
            else
                $('[id$=lblmsg]').addClass('successStatus');

            $('[id$=divMessage]').fadeIn().delay(5000).fadeOut();
        }

        function clearvalue() {
            $('[id$=EmailBox1_lblCurrentPage]').text("1");
            $('#txtSearchFrom').val("");
            $('#txtSearchTo').val("");
            $('[id$=txtSearchSubject]').val("");
            $('[id$=txtSearchHas]').val("");
            $('[id$=chkSearchAttachment]').prop('checked', false);
            $('[id$=ddlReadstatus]').show();
            $('#btnNewMove').show();
            $('[id$=btnDeleteEmail]').show();

            varIsAdvancedsrch = false;

            $('[id$=ddlReadstatus]').val("-1");
            //$('[id$=ddlEmailStatus]').val("0");
        }

        function Clear() {
            $("#<%=ddlContactsFilterType.ClientID%>").val("0");
            $("#<%=ddlEmailStatus.ClientID%>").val("0");
            $("#<%=ddlRelationship.ClientID%>").val("0");
            $("#<%=ddlProfile.ClientID%>").val("0");
            $("#<%=chkGroupBySubjectThread.ClientID%>").removeAttr("checked");
            $("#<%=hdnSelectedDate.ClientID%>").val("");
            $("#<%=txtCurrrentPage.ClientID%>").val("1");
            $("#<%=txtGridColumnFilter.ClientID%>").val("");
            $("#<%=ddlFollowUpStatus.ClientID%>").val("0");
            $("#<%=hdnFollowupstatus.ClientID%>").val("0");
            $("[id$=hdnIsNextPreviousDaysEmail]").val("0");
            $("#<%=btnUpdateNewEmails.ClientID%>").click();
            return false;
        }

        function OpenCorresPondance(numContactId) {
            window.open('../admin/frmCorrespondense.aspx?RecordID=' + numContactId + '&Mode=8&bPage=True', '', 'toolbar=no,titlebar=no,top=200,left=200,width=850,height=350,scrollbars=yes,resizable=yes')
        }

        function OpenAlertDetail(numAlertType, numDivisionID, numContactID) {
            window.open('AlertDetail.aspx?numAlertType=' + numAlertType + '&numDivisionID=' + numDivisionID + '&ContactId=' + numContactID, '', 'toolbar=no,titlebar=no,top=200,left=200,width=270,height=240,scrollbars=yes,resizable=yes')
        }

        function OpenAlertPanel(numContactId, numDomainId) {
            window.open('EmailAlert.aspx?ContactId=' + numContactId, '', 'toolbar=no,titlebar=no,top=200,left=200,width=270,height=240,scrollbars=yes,resizable=yes')
        }

        function DeleteArchiveRange() {
            if (Page_ClientValidate('CustomRange')) {
                if (confirm("Archived emails received between selected dates will be deleted permanently from biz. Do you want to proceed?")) {

                    var datePickerFrom = $find("<%# dtFrom.ClientID%>");
                    var datePickerTo = $find("<%# dtTo.ClientID%>");

                    var fromDate = datePickerFrom.get_selectedDate().format("yyyy/MM/dd");;
                    var toDate = datePickerTo.get_selectedDate().format("yyyy/MM/dd");;

                    $.ajax({
                        type: "POST",
                        url: "../common/Common.asmx/DeleteArchivedMails",
                        data: "{fromDate:'" + fromDate + "',toDate:'" + toDate + "'}",
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (msg) {
                            UpdateMsgStatus(msg.d);
                            datePickerFrom.clear();
                            datePickerTo.clear();
                        }
                    });
                    //BindGrid(varnode);
                }
            }

            return false;
        }
        function OpenWindow(a, b, c) {
            if (a == 0) {
                alert("Email is not Present in Contacts");
                return false;
            }
            else {
                var str;
                if (b == 0) {
                    str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CompanyList&DivID=" + a;
                }
                else if (b == 1) {
                    str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CompanyList&DivID=" + a;
                }
                else if (b == 2) {
                    str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=CompanyList&klds+7kldf=fjk-las&DivId=" + a;
                }

                if (window.opener) {
                    window.opener.location.href = str;
                } else {
                    document.location.href = str;
                }
                return false;
            }
        }
        $('.token-input-delete-token-facebook').on('click', function () {
            var removeMe;
            $.each(saved_tokens, function (i, v) {
                if (v.id == $(this).attr('id')) {
                    removeMe = i;
                }
            });
            saved_tokens.splice(removeMe, 1);

        });

        function UpdateTotalRecords(records) {
            $("[id$=lblRecords").text(records);
        }

        function UpdateDisplayedRecords(records) {
            $("[id$=lbldisplayRec").text(records);
        }
        function rdnCopyMoveEmail(type) {
            var strUpdateRecordId = '';
             $('#ctl00_MainContent_gvInbox > tbody > tr:not(:has(table, th))').each(function () {
                if ($(this)[0].attributes[0].nodeValue != "header selectedRow") {
                    if (($(this).find('input:checkbox').prop("checked") == true)) {
                        strUpdateRecordId = strUpdateRecordId + $(this).find('input:hidden')[0].value + ',';
                    }
                }
            });

            if (strUpdateRecordId == '') {
                UpdateMsgStatus("Error:No Email selected.");
            } else {
                if (type == 0) {
                    $("#modalTitleAction").text("Move");
                } else {
                    
                    $("#modalTitleAction").text("Copy");
                }
                $("#hdnIsSaveCopyMoveEmail").val(type);
                $("#ModalCopyMoveEmail").modal("show");
            }
        }
        function saveCopyMoveEmails() {
            CopyMoveEmails($("#hdnIsSaveCopyMoveEmail").val());
        }
        function UpdateTotalSize(size) {
            $("[id$=lblSize").text(size);
        }
        function BindOutlookSettings() {
            document.location.href = '../outlook/frmOutlookSettings.aspx';
            return false;
        }
        function DisplayError(ex) {
            $("[id$=lblError]").text(ex);
            $("[id$=divError]").show();
            $("[id$=divError]").focus();
        }

        function ManageInbox() {
            var win = window.open('../outlook/frmSortoutlooktree.aspx', 'OutlookTree', 'toolbar=no,titlebar=no,top=200,left=500,width=320,height=410,scrollbars=yes,resizable=yes');
            win.focus();
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
        <div class="row padbottom10" id="divMessage" style="display: none">
            <div class="col-xs-12">
                <asp:Label ID="lblmsg" runat="server"></asp:Label>

            </div>
        </div>
        <div id="divTopFilter">
            <div class="col-md-2 emailLeftSideWidget">
                <a href="#" class="btn btn-primary" onclick="ComposeEmail();" style="padding: 4px 13px; width: 100%; font-size: 16px; font-weight: bold;">
                    <img style="height: 24px; float: left" src="../images/Icon/IconComposeMessage.png" />&nbsp; Compose</a>
                <div class="form-inline" style="margin-top: 10px;">
                    <div>
                        <div class="pull-left" id="sectionForCopyMoveEmails" style="display:none;margin-left:10px;">
                        <input type="radio" name="copymoveemail" onclick="rdnCopyMoveEmail(0)" id="MoveEmail" />&nbsp;&nbsp;<i><b>Move selected message(s) to a folder</b></i><br />
                        <input type="radio" name="copymoveemail" onclick="rdnCopyMoveEmail(1)" id="CopyEmail" />&nbsp;&nbsp;<i><b>Copy selected message(s) to a folder</b></i><br />
                       </div>
                        <div class="pull-right">
                            <a title="Settings" onclick="ManageInbox();" style="margin-top: 10px; color: gray; cursor: pointer"><i style="    padding-top: 8px;" class="fa fa-2x fa-cog"></i></a>
                       </div>
                            <a href="#" style="display: none" onclick="return ManageInbox();" class="btn btn-xs  btn-primary" title="Manage Folder"><i class="fa fa-folder"></i></a>
                        <a href="#" style="display: none" id="btnCopy" onclick="CopyMoveEmails(1)" title="Copy" class="btn btn-xs  btn-warning"><i class="fa fa-copy"></i></a>
                        <a href="#"  id="btnNewMove" onclick="CopyMoveEmails(0)" title="Move" class="btn btn-xs  btn-warning" ><i class="fa fa-arrows-alt"></i></a>
                        <asp:HiddenField ID="hdnSelectedEmails" runat="server" />
                    </div>
                </div>
            </div>
            <div class="col-md-10 emailRightSideWidget">
                <div id="ModalCopyMoveEmail" class="modal fade" role="dialog">
                    <div class="modal-dialog modal-sm">

                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"><span id="modalTitleAction">Copy</span> Emails</h4>
                            </div>
                            <div class="modal-body">
                                <label>Select a folder</label>
                                <asp:DropDownList ID="ddlFoldersStructure" runat="server" CssClass="form-control" Style="width: 100%;"></asp:DropDownList>
                                <input type="hidden" id="hdnIsSaveCopyMoveEmail" value="0" />
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-primary" onclick="saveCopyMoveEmails();">Save</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="form-inline" style="margin-bottom: 12px">
                    <asp:LinkButton ID="lnkprevious" runat="server" CssClass="btn btn-primary" OnClientClick="return GetPreviousDayEmail();">Previous Day</asp:LinkButton>&nbsp;
                <asp:LinkButton ID="lnknext" runat="server" CssClass="btn btn-primary" OnClientClick="return GetNextDayEmail();">Next Day</asp:LinkButton>&nbsp;
                <asp:HiddenField ID="hdnIsNextPreviousDaysEmail" Value="0" runat="server" />
                    <asp:HiddenField ID="hdnSelectedDate" runat="server" />
                    <asp:HiddenField ID="hdnSortOrder" Value="DESC" runat="server" />
                    <asp:HiddenField ID="hdnProfileId" runat="server" />
                    <asp:HiddenField ID="hdnFollowupstatus" runat="server" />
                    <asp:HiddenField ID="hdnSimpleSearch" runat="server" />
                    <asp:HiddenField ID="hfDateFormat" runat="server" />


                    &nbsp;
                <div class="btn-group">
                    <a href="#" id="lnkPagePrevious" class="btn btn-default btn-sm"><i class="fa fa-chevron-left"></i></a>
                    <a href="#" id="lnkPageNext" class="btn btn-default btn-sm"><i class="fa fa-chevron-right"></i></a>
                </div>

                    <label style="margin-left: 17px;">Contacts in Biz:</label>
                    <asp:DropDownList ID="ddlContactsFilterType" CssClass="form-control" Style="width: 15%" runat="server">
                        <asp:ListItem Value="0">-No filter set-</asp:ListItem>
                        <asp:ListItem Value="1">Only msgs from/to</asp:ListItem>
                        <asp:ListItem Value="2">Exclude msgs from/to</asp:ListItem>
                    </asp:DropDownList>
                    <label>Email Status:</label>
                    <asp:DropDownList ID="ddlEmailStatus" runat="server" CssClass="form-control" Style="width: 15%"></asp:DropDownList>
                    <div class="pull-right">
                        <a href="#" id="hplClearGridCondition" title="Clear All Filters" onclick="return Clear();" style="color: #afafaf; padding-right: 6px;"><i class="fa fa-filter fa-2x"></i></a>

                        <a title="Settings" onclick="BindOutlookSettings();" style="margin-top: 10px; color: gray; cursor: pointer"><i class="fa fa-2x fa-cog"></i></a>
                        &nbsp;<a href="#" onclick="return OpenHelpPopUp('outlook/frminboxitems.aspx')"><label class="badge bg-yellow">?</label></a>
                        <a style="display: none" onclick="return OpenHelp()">
                            <label class="badge bg-yellow">?</label></a>
                    </div>
                </div>
                <div class="form-inline" style="margin-bottom: 10px;">
                    <a href="#" onclick="deleteSelectedEmails()" title="Delete Emails">
                        <img src="../images/Icon/iconDeleteMail.png" style="height: 25px" /></a>
                    <a href="#" onclick="updateReadUnreadStatus(1)" title="Mark message(s) as read">
                        <img src="../images/Icon/iconEmailRead.png" style="height: 23px" /></a>
                    <a href="#" onclick="updateReadUnreadStatus(0)" title="Mark message(s) as Unread">
                        <img src="../images/Icon/iconEmailUnRead.png" style="height: 23px" /></a>
                    <asp:CheckBox ID="chkGroupBySubjectThread" runat="server" />
                    <label>Group by subject thread</label>
                    <label style="margin-left: 4px">Follow-up Status: </label>
                    <asp:DropDownList ID="ddlFollowUpStatus" multiple="multiple" runat="server" CssClass="option-droup-multiSelection-Group" AutoPostBack="false" ClientIDMode="Static" Style="width: 15%; max-width: 15%"></asp:DropDownList>

                    <label>Relationship</label>
                    <asp:DropDownList ID="ddlRelationship" runat="server" CssClass="form-control" Style="width: 15%"></asp:DropDownList>
                    <label>Profile</label>
                    <asp:DropDownList ID="ddlProfile" runat="server" CssClass="form-control" Style="width: 15%">
                        <asp:ListItem Value="0">No Filter Set</asp:ListItem>
                    </asp:DropDownList>
                    <div class="pull-right">

                        <span id="btnSpam" title="Spam" class="btn btn-default btn-sm" onclick="MarkAsSpam()" runat="server">SPAM</span>
                    </div>
                </div>


            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 emailLeftSideWidget">
            <div class="box box-solid">
                <div class="box-header with-border" style="display: none">

                    <asp:Button ID="btnUpdateNewEmails" runat="server" Text="" Style="width: 0px; height: 0px;" />
                    <h3 class="box-title">Folders</h3>

                    <div class="box-tools">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse">
                            <i class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="box-body no-padding fixedHeightScroll" style="display: block;">

                    <telerik:RadTreeView ID="RadTreeView1" OnNodeClick="RadTreeView1_NodeClick" runat="server" AllowNodeEditing="false" Skin="Vista">
                    </telerik:RadTreeView>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-10 emailRightSideWidget">
            <div id="tblInboxHeader">
                <!-- Panel is hidden now -->
                <div class="box box-primary collapsed-box" style="display: none">
                    <div class="box-header with-border">
                        <h3 class="box-title">Search</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                <i class="fa fa-plus"></i>
                            </button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body" style="display: none;">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <ul class="list-inline">
                                        <li>
                                            <asp:DropDownList ID="ddlReadstatus" runat="server" CssClass="form-control">
                                                <asp:ListItem Text="Mark as" Value="-1"></asp:ListItem>
                                                <asp:ListItem Text="Read" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="Unread " Value="0"></asp:ListItem>
                                            </asp:DropDownList>
                                        </li>
                                        <li></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label>Subject</label>
                                    <asp:TextBox runat="server" ID="txtSearchSubject" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label>Msg From</label>
                                    <div class="form-inline">
                                        <input type="text" id="txtSearchFrom" class="tokeninputclass form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label>From</label>
                                    <div>
                                        <telerik:RadDatePicker ID="radDtFrom" runat="server" Width="120"></telerik:RadDatePicker>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-4">
                                <div class="form-group">
                                    <label>Has Attachment</label>
                                    <div>
                                        <asp:CheckBox ID="chkSearchAttachment" runat="server" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label>Msg Body</label>
                                    <asp:TextBox runat="server" ID="txtSearchHas" CssClass="form-control"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-3">
                                <div class="form-group">
                                    <label>Msg To</label>
                                    <input type="text" id="txtSearchTo" class="tokeninputclass form-control" />
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6 col-md-2">
                                <div class="form-group">
                                    <label>To</label>
                                    <div>
                                        <telerik:RadDatePicker ID="radDtTo" runat="server" Width="120"></telerik:RadDatePicker>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <asp:LinkButton CssClass="btn btn-md btn-primary" runat="server" ID="btnAdavancedSearch" Text="Search" OnClientClick="javascript:return false;"><i class="fa fa-search"></i>&nbsp;&nbsp;Search</asp:LinkButton>
                                &nbsp;
                        
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <asp:Panel ID="PanelDeleteRange" runat="server" Font-Bold="True" Visible="false">
                    <div class="box box-primary collapsed-box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Delete Range</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </div>
                            <!-- /.box-tools -->
                        </div>
                        <div class="box-body" style="display: none;">

                            <div class="form-inline">
                                <div class="form-group">
                                    <label>From</label>
                                    <telerik:RadDatePicker ID="dtFrom" Width="120" runat="server"></telerik:RadDatePicker>
                                </div>
                                &nbsp;
                                        &nbsp;
                                        <div class="form-group">
                                            <label>To</label>
                                            <telerik:RadDatePicker ID="dtTo" Width="120" runat="server"></telerik:RadDatePicker>
                                        </div>

                                <asp:ValidationSummary ID="ValidationSummary1" ShowMessageBox="true" ShowSummary="false" runat="server" ValidationGroup="CustomRange" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="CustomRange" ControlToValidate="dtFrom" runat="server" ErrorMessage="From date is required." Display="None"></asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="CustomRange" ControlToValidate="dtTo" runat="server" ErrorMessage="To date is required." Display="None"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="dateCompareValidator" ValidationGroup="CustomRange" runat="server" ControlToValidate="dtTo"
                                    ControlToCompare="dtFrom" Operator="GreaterThanEqual" Type="Date"
                                    ErrorMessage="To date must be same or after the From date." Display="None">
                                </asp:CompareValidator>
                                <asp:LinkButton ID="btnDeleteRange" ToolTip="Delete" runat="server" ValidationGroup="CustomRange" OnClientClick="return DeleteArchiveRange();" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
                            </div>

                        </div>
                    </div>
                </asp:Panel>
                <asp:Label Text="Inbox" Style="display: none" runat="server" ID="lblInboxTitle" />
                <div class="pull-right">
                    <div id="javascriptUpdateProgress" style="display: none">
                        <div class="overlay">
                            <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                                <i class="fa fa-2x fa-refresh fa-spin"></i>
                                <h3>Processing Request</h3>
                            </div>
                        </div>
                    </div>
                    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
                        <ProgressTemplate>
                            <%--<div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>--%>

                            <div id="ProgressBar" class="progress progress-striped active" style="width: 150px; margin-bottom: 0px;">
                                <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                </div>
                            </div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </div>
                <div class="clearfix"></div>
                <div class="row">
                    <div id="divEmailInbox">
                        <div class="table-responsive mailbox-messages">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true">
                                <ContentTemplate>
                                    <asp:GridView ID="gvInbox" AutoGenerateColumns="false" runat="server" Width="100%" DataKeyNames="numEmailHstrID" ShowHeaderWhenEmpty="true" EmptyDataText="No Emails Found" class="table table-bordered" GridLines="None">
                                        <Columns>
                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>

                            </asp:UpdatePanel>

                            <asp:Literal ID="litMessage" runat="server" EnableViewState="False"></asp:Literal>
                            <asp:TextBox ID="txtTotalPage" runat="server" Style="display: none"></asp:TextBox>
                            <asp:TextBox ID="txtTotalRecords" runat="server" Style="display: none"></asp:TextBox>
                            <asp:TextBox ID="txtCurrrentPage" runat="server" Width="28px" CssClass="signup" Text="1" MaxLength="5" Style="display: none"></asp:TextBox>

                            <asp:HiddenField runat="server" ID="hdnGridFilterCustomSearh" />
                            <asp:HiddenField runat="server" ID="hdnGridFilterRegularSearh" />
                            <asp:HiddenField runat="server" ID="hdnNodeId" />
                            <asp:HiddenField ID="hdnCurrentPage" runat="server" />
                            <asp:Label ID="lblCurrentPage" runat="server" Text="1" ForeColor="White"></asp:Label>
                            <asp:Label ID="lblTotal" runat="server" ForeColor="White"></asp:Label>
                            <asp:Label ID="lblPageSize" runat="server"></asp:Label>

                        </div>

                    </div>
                    <div class="mailbox-controls">
                        <asp:Label runat="server" ID="lblSize" ForeColor="Gray"></asp:Label>
                    </div>
                </div>
                <div class="box box-primary" style="display: none">
                    <div class="box-header with-border">


                        <h3 class="box-title"></h3>
                        <div class="box-tools pull-right" style="display: inline-flexbox">
                            <ul class="list-inline">
                                <li></li>
                                <li style="display: none">
                                    <div>
                                        <div class="text-right">
                                            <asp:CheckBox ID="chkExcludeNonBizContact" runat="server" Text="Exclude messages from contacts not in BizAutomation" onchange="ExcludeNonBizContactChanged();" />
                                        </div>
                                        <div class="text-right">
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="row mailbox-controls">
                            <div class="col-xs-12" style="display: none">
                                <div class="btn-group">
                                    <span id="btnMove" title="Move/Copy" class="btn btn-default btn-sm"><i class="fa fa-folder"></i></span>
                                    <span id="btnArchive" class="btn btn-default btn-sm" title="Archive" runat="server"><i class="fa fa-archive"></i></span>
                                    <span id="btnDeleteEmail" class="btn btn-default btn-sm" title="Delete selected Email" runat="server"><i class="fa fa-trash-o"></i></span>
                                </div>
                                <div class="pull-right">
                                    <b>Pick day for messages</b>
                                    <telerik:RadDatePicker ID="rdpDate" runat="server" DateInput-DateFormat="MM/dd/yyyy" Width="40">
                                        <ClientEvents OnDateSelected="OnFilterDateSelected"></ClientEvents>
                                        <DateInput ID="DateInput1" runat="server" CssClass="HiddenTextBox" />
                                    </telerik:RadDatePicker>


                                </div>
                                <!-- /.pull-right -->
                            </div>

                        </div>

                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                    </div>
                </div>
            </div>
            <div id="MailDetail" style="display: none">
            </div>

        </div>
    </div>
    <asp:Button ID="btnBind" runat="server" Style="display: none" />
    <asp:HiddenField runat="server" ID="hdnBindID" />
    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox><asp:TextBox ID="txtSortChar" Text="0" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtDelEmailIds" runat="server" Style="display: none"></asp:TextBox>
    <asp:HiddenField ID="hdnFixID" runat="server" />
    <asp:Button ID="btnSaveAdvanceSearchState" runat="server" Style="display: none" />
    <asp:HiddenField ID="hdnVisibility" runat="server" />
    <script type="text/javascript">
        //<![CDATA[

        var arrFieldConfig = new Array();
        arrFieldConfig[0] = new InlineEditValidation('74', 'False', 'False', 'False', 'False', 'False', 'False', '0', '0', 'Drip Campaign');

    </script>

</asp:Content>
