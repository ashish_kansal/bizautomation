﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmSortOutlookTree.aspx.vb" Inherits="BACRM.UserInterface.Outlook.frmSortOutlookTree" MasterPageFile="~/common/Popup.Master" %>


<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content3" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Outlook Folders</title>
    <style>
        .form-control {
    border-radius: 0;
    box-shadow: none;
    border-color: #d2d6de;
    display: block;
    width: 100%;
    height: 34px;
    background-image:none !important;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    border :1px solid #e1e1e1;
}
    .RadTreeView .rtUL .rtLI .rtUL {
        padding: 0px !important;
    }

    .rtMid, .rtBot, .rtTop {
        border-bottom: 1px solid #f4f4f4;
    }

    .rtLI div {
        border-radius: 0;
        border-top: 0;
        border-left: 3px solid transparent;
        color: #444;
        position: relative;
        display: block;
    }

        .rtHover {
            color: #444 !important;
            background: rgba(248, 182, 63, 0.15) !important;
        }
        .rtHover span {
            color: #444 !important;
            background: rgba(248, 182, 63, 0.15) !important;
        }
        .rtSelected{
             color: #444 !important;
            background: rgba(248, 182, 63, 0.15) !important;
            border-left-color: #d48800 !important;
        }
    .rtSelected {
        background: transparent;
        color: #444;
        border-top: 0;
        border-left-color: #3c8dbc !important;
    }

    .RadTreeView_Vista .rtSelected .rtIn {
        background: none;
        border: none;
        padding: 7px;
    }

    .RadTreeView_Vista, .RadTreeView_Vista a.rtIn, .RadTreeView_Vista .rtEdit .rtIn input {
        font-family: 'Source Sans Pro', 'Helvetica Neue', Helvetica, Arial, sans-serif !important;
        font-weight: 400 !important;
        font-size: 14px !important;
    }

    .RadTreeView .rtPlus, .RadTreeView .rtMinus {
        margin-right: 2px;
        margin-left: -13px;
        vertical-align: middle;
        margin: 0px 7px 0 -18px;
    }

    .RadTreeView .rtIn {
        display: inline-block !important;
        white-space: normal !important;
        width: 100% !important;
        height: 35px;
        padding: 7px;
    }

    .RadTreeView_Vista .rtHover .rtIn {
        color: #444;
        background: #f7f7f7;
        border: none;
        padding:7px;
    }
    .RadTreeView .rtLines .rtLI {
            background: #f3f3f352 !important;
            cursor: pointer;
        }

        .RadTreeView .rtMid, .RadTreeView .rtTop {
            padding: 1px 0 1px 5px !important;
        }

        .rtFirst .rtUL .rtLI :hover {
            background-color: #ea941330 !important;
        }
        .sideText{
                          position: absolute;
                              margin-top: 8px;
                  }
        .sideIcon {    height: 35px;
    margin-top: -2px;
    margin-left: 0px;
        }

        .fixedHeightScroll {
            overflow-y: scroll;
            overflow-x: hidden;
            border: 1px solid #c1c1c1;
            height:calc(100vh - 240px);
        }

        .rtLI div {
            border-bottom-color: #e1e1e1;
        }

        .emailLeftSideWidget {
            padding-right: 5px !important;
            width: 20% !important;
        }

        .emailRightSideWidget {
            padding-left: 15px !important;
            width: 80% !important;
            margin-top: 0px !important;
        }
        .RadTreeView_Vista .rtSelected .rtIn {
            background-color: transparent !important;
            background-image: none !important;
            border: 1px solid transparent !important;
        }
         .RadTreeView_Vista .rtSelected .rtIn:hover {
            background-color: transparent !important;
            background-image: none !important;
            border: 1px solid transparent !important;
        }
         .RadTreeView_Vista .rtHover .rtIn, .RadTreeView_Vista .rtSelected .rtIn{
             
            border: 1px solid transparent !important;
         }
         .RadTreeView .rtLines .rtTop{
             display: block !important;
         }
         .pull-right{
                 float: right;
    margin-right: 15px;
    padding-top: 8px;
    font-style: italic;
         }
</style>
    
    <script language="javascript" type="text/javascript">
        function CheckName() {
            if (document.getElementById('txtName').value == '') {
                alert('Enter folder name')
                return false
            }
            else {
                var tree = $find("RadTreeView1");
                var node = tree.get_selectedNode();
                if (node != null) {
                    var b = node.get_value();
                    document.getElementById('txtSelectedNode').value = b
                    //                    var a = node.get_Level();
                    //                    if (a >= 2) {
                    //                        alert('Can not Add more Subnodes to this node');
                    //                        return false
                    //                    }
                }
                else {
                    document.getElementById('txtSelectedNode').value = 0
                }
            }

        }
         function ClientNodeClicked(sender, eventArgs) {
            var node = eventArgs.get_node();
            document.getElementById('txtSelectedFolder').value = node.get_text();
            document.getElementById('txtSelectedNode').value = node.get_value();
            document.getElementById('txtFixID').value = node.get_attributes().getAttribute('numFixID');

            if (node.get_attributes().getAttribute('isSystemFolder') == "False") {
                document.getElementById('txtName').value = node.get_text();
                document.getElementById('btnRename').style.visibility = "visible";
            } else {
                document.getElementById('txtName').value = "Enter Folder Name";
                document.getElementById('btnRename').style.visibility = "hidden";
            }
        }
        function CheckSelectedNode() {
            var FolderName = document.getElementById('txtDelFolder').value;
            var tree = $find("RadTreeView1");
            var node = tree.get_selectedNode();

            if (node == null) {
                alert('Select a folder in tree')
                return false
            }
            else {

                if (node.get_attributes().getAttribute("isSystemFolder") == "True") {
                    alert('Can not delete default node');
                    return false
                }

            }

            return confirm("Deleting folder will move all emails to 'Deleted Mails' section, Do you want to continue?");
        }
        function CheckNode() {
            var tree = $find("RadTreeView1");
            var node = tree.get_selectedNode();
            if (node == null) {
                alert('Select a folder in tree')
                return false
            }
            else {
                var b = node.get_value();
                document.getElementById('txtSelectedNode').value = b
                if (b == 0 || b == 5) {

                    alert('Can not move to This folder');
                    return false
                }
                else {
                    var Ids = ""
                    Ids = window.opener.GetSelectedMails()
                    if (Ids != "") {
                        document.getElementById('txtSelectedMails').value = Ids
                    }

                }
            }
        }
        $(document).ready(function () {

            $('.RadTreeView .rtLines .rtFirst >.rtTop').hide();
        });
        function RenameFolder() {
            if (confirm('Folder name is used while copy/move message from one folder to another in gmail when mail is copied/moved in bizautomation. Do you want to Proceed?')) {
                return true;
            } else {
                document.getElementById('txtName').value = "Enter Folder Name";
                document.getElementById('btnRename').style.visibility = "hidden";
                return false;
            }
        }
    </script>
    <%--<script tyoe="text/javascript">
        $(function () {
            $(".RadTreeView .rtUL").sortable();
            $(".RadTreeView .rtUL").disableSelection();
        });
  </script>--%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnclose" runat="server" CssClass="button" OnClientClick="javascript:window.opener.location.reload(true);self.close();"
                Text="Save & Close" />&nbsp;&nbsp;
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Re-Order Outlook Folders
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:ScriptManager runat="server" ID="scManager" />
    <asp:Table ID="Table1" runat="server" Width="600px" CssClass="aspTable" BorderWidth="1" BorderColor="black">
        <asp:TableRow VerticalAlign="Top">
            <asp:TableCell HorizontalAlign="left">
                <asp:Panel runat="server" ID="pnlTree">
                    &nbsp;
                    <asp:TextBox runat="server" ID="txtName" CssClass="form-control" MaxLength="15" Placeholder="Enter Folder Name"></asp:TextBox>&nbsp;<br />
                    <asp:Button runat="server" ID="btnAdd" Text="Add New Folder" CssClass="btn btn-primary" />&nbsp;
                    <asp:Button ID="btnDelete" runat="server" Text="Delete Selected" CssClass="btn btn-primary" />&nbsp;
                    <asp:Button ID="btnRename" runat="server" Text="Rename" CssClass="button" style="visibility:hidden" OnClientClick="return RenameFolder();" />
                    <asp:Button ID="btnHide" runat="server" Text="Hide" CssClass="btn btn-primary" />&nbsp;
                    <asp:Button ID="btnUnHide" runat="server" Text="Un-Hide" CssClass="btn btn-primary" />&nbsp;
                    <asp:Button ID="btnMovetoRootFolder" runat="server" Text="Move to 1st Level" CssClass="btn btn-primary" />&nbsp;
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlMove" CssClass="normal1" Width="300px" Visible="false">
                    <asp:Button runat="server" ID="btnMove" Text="Move" CssClass="button" />&nbsp;
                    <asp:Button ID="btnCopy" runat="server" Text="Copy" CssClass="button" />&nbsp;
                    <br />
                    <span style="float: left">Select a folder in tree</span>
                </asp:Panel>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow VerticalAlign="Top">
            <asp:TableCell HorizontalAlign="Center">
                <table width="100%">
                    <tr valign="top">
                        <td>
                            <telerik:RadTreeView ID="RadTreeView1" runat="server" AllowNodeEditing="false" Skin="Vista" 
                                ClientIDMode="Static" EnableDragAndDrop="true" EnableDragAndDropBetweenNodes="true" OnNodeDrop="RadTreeView1_NodeDrop" OnClientNodeClicked="ClientNodeClicked">
                            </telerik:RadTreeView>
                        </td>
                    </tr>
                </table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:TextBox runat="server" ID="txtSelectedNode" Text="0" Style="display: none"></asp:TextBox>
    <asp:TextBox runat="server" ID="txtSelectedMails" Text="" Style="display: none"></asp:TextBox>
    <asp:TextBox runat="server" ID="txtDelFolder" Text="0" Style="display: none"></asp:TextBox>
     <asp:TextBox runat="server" ID="txtSelectedFolder" Text="0" Style="display: none"></asp:TextBox>
    <asp:TextBox runat="server" ID="txtFixID" Text="" Style="display: none"></asp:TextBox>
</asp:Content>
