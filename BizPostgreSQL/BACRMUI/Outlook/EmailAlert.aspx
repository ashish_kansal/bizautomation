﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="EmailAlert.aspx.vb" Inherits=".EmailAlert"
    MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="stylesheet" href="../CSS/master.css" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Email Alert Occured
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Content" runat="server">
    <%--    <asp:Table ID="Table1" CellPadding="0" CellSpacing="0" BorderWidth="1" runat="server"
        Width="100%" CssClass="aspTable" BorderColor="black" GridLines="None" Height="300">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">--%>
    <script type="text/javascript" language="javascript">
        function OpenEmail(a, b) {
            window.open('../Leads/frmOpenEmail.aspx?DivID=' + a + '&bDivision=1', '', 'toolbar=no,titlebar=no,top=200,left=200,width=650,height=370,scrollbars=yes,resizable=yes')
        }

        function OpenActionItem(a) {
            window.open('../ActionItems/frmLast10ActionItems.aspx?type=0&cntid=' + a + '', '', 'toolbar=no,titlebar=no,top=200,left=200,width=650,height=370,scrollbars=yes,resizable=yes')
        }

        function OpenWindow(a, b) {
            window.open('../prospects/frmOpenDivisionInfo.aspx?DivID=' + a + '&type=' + b, '', 'toolbar=no,titlebar=no,top=200,left=200,scrollbars=yes,resizable=yes')
        }
    </script>
    <table width="100%" class="aspTable ">
        <tr>
            <td>
                <asp:CheckBox ID="chkOpenAction" runat="server" BorderStyle="None" CssClass="signup"
                    Text="Open Action Item : " Enabled="false" />
            </td>
            <td>
                <asp:HyperLink ID="hyOpenActionCount" runat="server" MaxLength="15" CssClass="hyperlink"
                    Text="0" TabIndex="1" NavigateUrl="#"></asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox ID="chkOpenCases" runat="server" CssClass="signup" Text="Open Cases:"
                    TabIndex="2" Enabled="false" />
            </td>
            <td>
                <asp:HyperLink ID="hyOpenCasesCount" runat="server" CssClass="hyperlink" Text="0"
                    MaxLength="15" class="hyperlink" TabIndex="3" NavigateUrl="#"></asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox ID="chkOpenProject" runat="server" CssClass="signup" Text="Open Projects:"
                    TabIndex="4" Enabled="false" />
            </td>
            <td>
                <asp:HyperLink ID="hyOpenProjectCount" runat="server" MaxLength="15" Text="0" class="hyperlink"
                    NavigateUrl="#" TabIndex="5"></asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox ID="chkOpenSalesOpp" runat="server" CssClass="signup" Text="Open Sales Opportunities/Orders:"
                    TabIndex="6" Enabled="false" />
            </td>
            <td>
                <asp:HyperLink ID="hyOpenSalesOppCount" runat="server" MaxLength="15" Text="0" class="hyperlink"
                    NavigateUrl="#" TabIndex="7"></asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox ID="chkOpenPurchaseOpp" CssClass="signup" runat="server" Text=" Open Purchase Opportunities/Orders:"
                    TabIndex="8" Enabled="false" />
            </td>
            <td>
                <asp:HyperLink ID="hyOpenPurchaseOppCount" runat="server" MaxLength="15" class="hyperlink"
                    Text="0" NavigateUrl="#" TabIndex="9"></asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td>
                <span>
                    <asp:CheckBox ID="chkBalanceDue" runat="server" CssClass="signup" Text="Balance due >"
                        TabIndex="10" Enabled="false" /></span> <span>
                            <asp:Label ID="lblBalanceDueVal" runat="server" Width="60px" Text="0" class="signup"
                                TabIndex="11"></asp:Label></span>
            </td>
            <td>
                <span>
                    <asp:Label ID="lblBalanceDueCount" runat="server" Width="60px" class="signup" TabIndex="11"
                        Text="0"></asp:Label></span>
            </td>
        </tr>
        <tr>
            <td>
                <asp:CheckBox ID="chkUnreadEmail" runat="server" CssClass="signup" Text="Unread email messages :"
                    Enabled="false" />
            </td>
            <td>
                <asp:HyperLink ID="hyUnreadEmailCount" runat="server" MaxLength="15" class="hyperlink"
                    Text="0" NavigateUrl="#"></asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td class="signup">
                <asp:CheckBox ID="chkPurchasedPast" runat="server" TabIndex="16" CssClass="signup"
                    Enabled="false" Text="Brought >" />
                <asp:Label ID="lblnumPurchasedPast" runat="server" Width="60px" class="signup" TabIndex="17" />
            </td>
            <td>
                <asp:Label ID="lblPurchasedPastCount" runat="server" MaxLength="15" Text="0" TabIndex="18"
                    CssClass="signup"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="signup">
                <asp:CheckBox ID="chkSoldPast" runat="server" TabIndex="19" CssClass="signup" Enabled="false"
                    Text="Sold >" />
                <asp:Label ID="lblnumSoldPast" runat="server" Width="60px" class="signup" TabIndex="20" />
            </td>
            <td>
                <asp:Label ID="lblSoldPastCount" runat="server" MaxLength="15" Text="0" TabIndex="21"
                    CssClass="signup"></asp:Label>
            </td>
        </tr>
        <%--<tr>
                        <td class="signup">
                            <asp:CheckBox ID="chkCustomField" runat="server" TabIndex="22" CssClass="signup" />The
                            organization has the following field &nbsp;<asp:DropDownList ID="ddlCustomField"
                                runat="server" class="signup" TabIndex="23" />
                            <br />
                            &nbsp;&nbsp;&nbsp;&nbsp; With the following value &nbsp;
                            <asp:DropDownList ID="ddlCustomFieldValue" runat="server" class="signup" TabIndex="24" />
                        </td>
                        <td>
                            <asp:TextBox ID="txtCustomFieldMsg" runat="server" MaxLength="15" class="signup"
                                TabIndex="25"></asp:TextBox>
                        </td>
                    </tr>--%>
    </table>
    <%--  </asp:TableCell></asp:TableRow></asp:Table>--%></asp:Content>
