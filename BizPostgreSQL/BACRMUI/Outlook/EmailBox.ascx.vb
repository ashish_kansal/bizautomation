﻿Imports System
Imports System.Data
Imports System.Net
Imports BACRM.BusinessLogic.Outlook
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Admin
Namespace BACRM.UserInterface.EmailInBox
    Public Class EmailBox
        Inherits BACRMUserControl
        Dim m_aryRightsForAct(), m_aryRightsForPro(), m_aryRightsForCase(), m_aryRightsForImp() As Integer

        Dim objOutlook As New COutlook
        Dim objAlertConfig As New EmailAlertConfig

        Dim numFixId As Integer
        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                GetUserRightsForPage(33, 1)
                If m_aryRightsForPage(RIGHTSTYPE.VIEW) = 0 Then Response.Redirect("../admin/authentication.aspx?mesg=AC")
                litMessage.Text = ""
                If Not IsPostBack Then
                    If Session("List") <> "Inbox" Then
                        Session("ListDetails") = Nothing
                        Session("Asc") = 1
                    End If

                    numFixId = CCommon.ToInteger(Request.QueryString("numFixID"))
                    BindGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError("EmailBox: Error Ocuured")
            End Try
        End Sub
        Function SetBytes(ByVal Bytes) As String
            Try
                If Bytes >= 1073741824 Then
                    SetBytes = Format(Bytes / 1024 / 1024 / 1024, "#0.00") & " GB"
                ElseIf Bytes >= 1048576 Then
                    SetBytes = Format(Bytes / 1024 / 1024, "#0.00") & " MB"
                ElseIf Bytes >= 1024 Then
                    SetBytes = Format(Bytes / 1024, "#0.00") & " KB"
                ElseIf Bytes < 1024 Then
                    SetBytes = Fix(Bytes) & " B"
                End If
                Exit Function
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError("EmailBox: Error Ocuured")
            End Try
        End Function
        Public Sub BindGrid()
            Try
                Dim dtTable As DataTable
                Dim dsEmail As DataSet
                Dim CreateCol As Boolean = True

                GetUserRightsForPage(33, 1)

                objOutlook.PageSize = CInt(Session("PagingRows"))
                objOutlook.CurrentPage = CCommon.ToString(Request.QueryString("CurrentPage")) '
                objOutlook.SearchText = CCommon.ToString(Request.QueryString("srch")).Trim() 'IIf(ddlSearch.SelectedValue = 1, txtSearchInbox.Text.Trim, "")
                If String.IsNullOrEmpty(CCommon.ToString(Request.QueryString("filterDate"))) Then
                    objOutlook.FilterDate = Nothing
                Else
                    objOutlook.FilterDate = CCommon.ToSqlDate(CCommon.ToString(Request.QueryString("filterDate")))
                End If

                objOutlook.SearchTextFrom = CCommon.ToString(Request.QueryString("From")).Trim()
                objOutlook.SearchTextTo = CCommon.ToString(Request.QueryString("To")).Trim()
                objOutlook.SearchTextSubject = CCommon.ToString(Request.QueryString("Subject")).Trim()
                objOutlook.SearchTextHasWords = CCommon.ToString(Request.QueryString("HasWords")).Trim()
                objOutlook.SearchHasAttachment = CCommon.ToBool(Request.QueryString("Attachment"))
                objOutlook.SearchIsAdvancedsrch = CCommon.ToBool(Request.QueryString("IsAdvancedsrch"))
                objOutlook.SearchInNode = CCommon.ToLong(Request.QueryString("srchNode"))
                objOutlook.ExcludeEmailFromNonBizContact = CCommon.ToBool(Convert.ToInt16(Request.QueryString("excludeNonBizContact")))
                If Not String.IsNullOrEmpty(CCommon.ToString(Request.QueryString("fromDate"))) AndAlso Not String.IsNullOrEmpty(CCommon.ToString(Request.QueryString("toDate"))) Then
                    objOutlook.FromDate = CCommon.ToSqlDate(CCommon.ToString(Request.QueryString("fromDate")))
                    objOutlook.ToDate = CCommon.ToSqlDate(CCommon.ToString(Request.QueryString("toDate")))
                End If

                objOutlook.ToEmail = Session("UserEmail")
                objOutlook.DomainID = Session("DomainId")
                objOutlook.UserRightType = m_aryRightsForPage(RIGHTSTYPE.VIEW)
                objOutlook.EmailStatus = CCommon.ToInteger(Request.QueryString("EmailStatus").Trim.ToString)
                If ViewState("Column") <> "" Then
                    objOutlook.columnName = ViewState("Column")
                Else : objOutlook.columnName = "emailhistory.dtReceivedOn"
                End If
                If Session("Asc") = 1 Then
                    objOutlook.columnSortOrder = "Desc"
                Else : objOutlook.columnSortOrder = "Asc"
                End If

                objOutlook.chrSource = "I"

                objOutlook.NodeId = CCommon.ToLong(Request.QueryString("NodeId")) 'Session("NodeId")
                objOutlook.UserCntID = Session("UserContactId")
                objOutlook.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                dsEmail = objOutlook.getInbox()
                If dsEmail.Tables(1).Rows.Count <= 0 Then
                    litMessage.Text = "<script>UpdateMsgStatus('Error: No emails found.');</script>"
                    Exit Sub
                End If
                dtTable = dsEmail.Tables(1)

                Dim dtTableInfo As DataTable
                dtTableInfo = dsEmail.Tables(0)

                Dim i As Integer
                For i = 0 To dtTable.Columns.Count - 1
                    dtTable.Columns(i).ColumnName = dtTable.Columns(i).ColumnName.Replace(".", "")
                Next
                Dim m_aryRightsForInlineEdit() As Integer = GetUserRightsForPage_Other(3, 4)
                gvInbox.DataKeyNames = New String() {"numEmailHstrID"}
                If CreateCol = True Then
                    Dim bField As BoundField
                    Dim Tfield As TemplateField
                    gvInbox.Columns.Clear()
                    Tfield = New TemplateField
                    Tfield.HeaderTemplate = New MyTemp(ListItemType.Header, "~~0~~0~0~DeleteCheckBox", m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE))
                    Tfield.ItemTemplate = New MyTemp(ListItemType.Item, "~~0~~0~0~DeleteCheckBox", m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE))
                    gvInbox.Columns.Add(Tfield)
                    'gvInbox.DataKeyNames = "numHstrEmailId"
                    For i = 0 To dtTable.Columns.Count - 1
                        Dim str As String()
                        str = dtTable.Columns(i).ColumnName.Split("~")
                        If str.Length > 1 Then
                            Tfield = New TemplateField
                            Tfield.HeaderTemplate = New MyTemp(ListItemType.Header, dtTable.Columns(i).ColumnName, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE))
                            Tfield.ItemTemplate = New MyTemp(ListItemType.Item, dtTable.Columns(i).ColumnName, m_aryRightsForInlineEdit(RIGHTSTYPE.UPDATE))

                            gvInbox.Columns.Add(Tfield)
                        End If
                    Next

                End If
                gvInbox.DataSource = dtTable
                gvInbox.DataBind()
                '*************Paging****************************************

                Session("ListDetails") = txtCurrrentPage.Text & "," & ViewState("Column") & "," & Session("Asc")
                Dim TotalRec As Integer = 0
                If dtTable.Rows.Count > 0 Then
                    TotalRec = dtTable.Rows(0).Item("TotalRowCount")
                    litMessage.Text += "<script>UpdateTotalSize('" & IIf(dtTable.Rows(0).Item("TotalSize") > 0, "You are using " & SetBytes(dtTable.Rows(0).Item("TotalSize")) & " space.", "") & "');</script>"
                    litMessage.Text += "<script>UpdateTotalRecords('" & dtTable.Rows(0).Item("TotalRowCount") & "');</script>"
                End If

                If TotalRec = 0 Then
                    litMessage.Text += "<script>UpdateTotalSize('');</script>"
                    litMessage.Text += "<script>UpdateTotalRecords('0');</script>"
                    txtCurrrentPage.Text = 1
                Else
                    Dim strTotalPage As String()
                    Dim decTotalPage As Decimal
                    decTotalPage = TotalRec / Session("PagingRows")
                    decTotalPage = Math.Round(decTotalPage, 2)
                    strTotalPage = CStr(decTotalPage).Split(".")
                    If (TotalRec Mod Session("PagingRows")) = 0 Then
                        lblTotal.Text = strTotalPage(0)
                        txtTotalPage.Text = strTotalPage(0)
                    Else
                        lblTotal.Text = strTotalPage(0) + 1
                        txtTotalPage.Text = strTotalPage(0) + 1
                    End If
                    txtTotalRecords.Text = TotalRec
                End If

                lblCurrentPage.Text = CCommon.ToString(Request.QueryString("CurrentPage"))
                If lblCurrentPage.Text <> "" Then
                    If TotalRec <= CInt(Session("PagingRows")) * CInt(lblCurrentPage.Text) Then
                        litMessage.Text += "<script>UpdateDisplayedRecords('" & TotalRec & "');</script>"
                    Else
                        litMessage.Text += "<script>UpdateDisplayedRecords('" & CCommon.ToString(CInt(Session("PagingRows")) * CInt(lblCurrentPage.Text)) & "');</script>"
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Dim dtItemList As DataTable
        Private Sub gvInbox_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvInbox.RowDataBound

            If e.Row.RowType = DataControlRowType.Header Then
                For Each cell As TableCell In e.Row.Cells
                    If String.IsNullOrEmpty(cell.Text) AndAlso cell.Controls.Count = 0 Then
                        cell.Attributes.Add("class", "hidecolumn")
                    End If
                Next
            ElseIf e.Row.RowType = DataControlRowType.DataRow Then

                Dim dr As DataRowView = CType(e.Row.DataItem, DataRowView)
                Dim ddlEmailHistory As DropDownList = CType(e.Row.FindControl("ddlEmailHistory"), DropDownList) 'CType(sender, DropDownList)

                If ddlEmailHistory IsNot Nothing Then
                    If dtItemList Is Nothing Then
                        dtItemList = objCommon.GetMasterListItems(385, Session("DomainId"))
                    End If
                    ddlEmailHistory.DataSource = dtItemList
                    ddlEmailHistory.DataTextField = "vcData"
                    ddlEmailHistory.DataValueField = "numListItemID"
                    ddlEmailHistory.DataBind()
                    ddlEmailHistory.Items.Insert(0, New ListItem("", "0"))
                    If CCommon.ToLong(dr("numListItemID")) > 0 Then
                        If Not ddlEmailHistory.Items.FindByValue(CCommon.ToLong(dr("numListItemID"))) Is Nothing Then
                            ddlEmailHistory.Items.FindByValue(CCommon.ToLong(dr("numListItemID"))).Selected = True
                        End If
                    End If
                End If


                If CCommon.ToBool(dr("bitIsRead")) = False Then
                    Dim lblvcSubject As Label = CType(e.Row.FindControl("vcSubject"), Label)
                    Dim lblvcFrom As Label = CType(e.Row.FindControl("vcFrom"), Label)
                    Dim lblvcTo As Label = CType(e.Row.FindControl("vcTo"), Label)

                    If lblvcSubject IsNot Nothing Then
                        lblvcSubject.Font.Bold = True
                    End If
                    If lblvcFrom IsNot Nothing Then
                        lblvcFrom.Font.Bold = True
                    End If
                    If lblvcTo IsNot Nothing Then
                        lblvcTo.Font.Bold = True
                    End If
                End If

                Dim hdnContactId As HiddenField = CType(e.Row.FindControl("hdnContactID"), HiddenField)
                If e.Row.FindControl("lnkRecentCorr") IsNot Nothing Then
                    Dim lnk As HyperLink = CType(e.Row.FindControl("lnkRecentCorr"), HyperLink)
                    If hdnContactId.Value = 0 Then
                        lnk.Text = ""
                        lnk.Enabled = False
                    Else
                        lnk.ToolTip = "Recent Correspondance"
                        lnk.Attributes.Add("onClick", "javascript:OpenCorresPondance('" & hdnContactId.Value & "');")
                        lnk.CssClass = "hyperlink"
                    End If
                End If
                If e.Row.FindControl("lblAlertPanel") IsNot Nothing Then
                    Dim lblAlert As Label = CType(e.Row.FindControl("lblAlertPanel"), Label)
                End If

                If e.Row.Cells.Count > 4 Then
                    e.Row.BorderStyle = BorderStyle.None
                    Dim row As GridViewRow
                    Dim cell As TableCell
                    Dim tbl As Table = CType(e.Row.Parent, Table)

                    cell = New TableCell
                    row = New GridViewRow(-1, -1, DataControlRowType.DataRow, DataControlRowState.Normal)
                    'cell.ColumnSpan = 2
                    row.Cells.Add(cell)

                    cell = New TableCell
                    cell.ColumnSpan = Me.gvInbox.Columns.Count
                    Dim span As New HtmlGenericControl("span")
                    span.Attributes("style") = "color:gray;"
                    span.InnerHtml = CCommon.ToString(dr("vcBodyText"))
                    cell.Controls.Add(span)

                    row.Cells.Add(cell)
                    row.CssClass = "Row-border"

                    'Change color of row for Sent messages (if the mail has read by receiver) (numFixId is 4 for Sent Messages) // Added by Priya
                    Dim hdnNoofTimesOpened As HiddenField = CType(e.Row.FindControl("hdnnumNoofTimes"), HiddenField)
                    If (CCommon.ToInteger(hdnNoofTimesOpened.Value) > 0) And numFixId = 4 Then
                        e.Row.BackColor = Drawing.Color.FromArgb(219, 249, 220)
                    End If

                    'Change color of Row
                    If e.Row.DataItem.DataView.Table.Columns("vcColorScheme") IsNot Nothing Then
                        If ((DataBinder.Eval(e.Row.DataItem, "vcColorScheme").ToString.Length > 0) And (numFixId <> 4)) Then
                            row.CssClass = "Row-border " & DataBinder.Eval(e.Row.DataItem, "vcColorScheme")
                        ElseIf ((DataBinder.Eval(e.Row.DataItem, "vcColorScheme").ToString.Length > 0) And (CCommon.ToInteger(dr("NoOfTimeOpened")) > 0 And dr("NoOfTimeOpened") IsNot Nothing And numFixId = 4)) Then
                            e.Row.BackColor = Drawing.Color.FromArgb(219, 249, 220)
                        End If
                    End If

                    tbl.Rows.Add(row)
                End If

                'Change color of Row
                If e.Row.DataItem.DataView.Table.Columns("vcColorScheme") IsNot Nothing Then
                    If ((DataBinder.Eval(e.Row.DataItem, "vcColorScheme").ToString.Length > 0) And (numFixId <> 4)) Then
                        e.Row.CssClass = DataBinder.Eval(e.Row.DataItem, "vcColorScheme")
                    ElseIf ((DataBinder.Eval(e.Row.DataItem, "vcColorScheme").ToString.Length > 0) And (CCommon.ToInteger(dr("NoOfTimeOpened")) > 0 And dr("NoOfTimeOpened") IsNot Nothing And numFixId = 4)) Then
                        e.Row.BackColor = Drawing.Color.FromArgb(219, 249, 220)
                    End If
                End If


            End If
        End Sub

        Private Sub DisplayError(ByVal ex As String)
            litMessage.Text = "<script>DisplayError('" & ex & "');</script>"
        End Sub
    End Class
    Public Class MyTemp : Implements ITemplate

        Dim TemplateType As ListItemType
        Dim ColumnName, FieldName, DBColumnName, AllowSorting, FormFieldId, AllowEdit, ControlType As String
        Dim AllowFiltering As Boolean
        Dim Custom As Boolean
        Dim EditPermission As Integer
        Sub New(ByVal type As ListItemType, ByVal fld1 As String, ByVal EPermission As Integer)
            Try
                TemplateType = type
                ColumnName = fld1

                Dim str As String()
                str = ColumnName.Split("~")

                FieldName = str(0)
                DBColumnName = str(1)
                AllowSorting = str(2)
                FormFieldId = str(3)
                AllowEdit = str(4)
                Custom = str(5)
                ControlType = str(6)

                If (str.Length > 7) Then
                    AllowFiltering = str(7)
                End If
                EditPermission = EPermission
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub InstantiateIn(ByVal Container As Control) Implements ITemplate.InstantiateIn
            Try
                Dim lbl1 As Label = New Label()
                Dim lbl2 As Label = New Label()
                Dim lbl3 As Label = New Label()
                Dim hdn1 As New HiddenField
                Dim hdn2 As New HiddenField
                Dim hdn3 As New HiddenField
                Dim ph1 As New PlaceHolder
                Dim lnkButton As New LinkButton
                Dim lnk As New HyperLink
                Dim ddlEmailHistory As New DropDownList
                Dim txtSearch As TextBox = New TextBox()
                Dim ddlSearch As DropDownList = New DropDownList()
                Select Case TemplateType
                    Case ListItemType.Header
                        If ControlType <> "DeleteCheckBox" Then

                            If DBColumnName = "bitIsRead" Then
                                lbl1.Text = "<img src='../images/hdr_message.gif' title='Read/Unread' />"
                                Container.Controls.Add(lbl1)
                            ElseIf DBColumnName = "bitHasAttachments" Then
                                lbl1.Text = "<img src='../images/hdr_attachment.gif' title='Has Attachment' />"
                                Container.Controls.Add(lbl1)
                            ElseIf DBColumnName = "numContactID" Then

                            ElseIf ControlType <> "HiddenField" AndAlso DBColumnName <> "RecentCorrespondance" AndAlso DBColumnName <> "AlertPanel" AndAlso DBColumnName <> "dtReceivedOn" AndAlso DBColumnName <> "bintCreatedOn" Then
                                lbl1.ID = DBColumnName
                                lbl1.Text = FieldName
                                lbl1.ForeColor = Color.Black
                                'lnkButton.Attributes.Add("onclick", "return SortColumn('" & DBColumnName & "')")
                                Container.Controls.Add(lbl1)
                            ElseIf DBColumnName = "AlertPanel" Then
                                lbl1.Text = "Alert Panel"     'Changed by Priya(18Jan 2018)
                                Container.Controls.Add(lbl1)
                            ElseIf DBColumnName = "dtReceivedOn" Then
                                lbl1.Text = "Received"     'Added by Priya(15Feb 2018)
                                Container.Controls.Add(lbl1)
                            ElseIf DBColumnName = "bintCreatedOn" Then
                                lbl1.Text = "Created"     'Added by Priya(15Feb 2018)
                                Container.Controls.Add(lbl1)
                            End If
                            If AllowFiltering = True Then
                                If ControlType = "TextArea" Or ControlType = "TextBox" Then
                                    Container.Controls.Add(New LiteralControl("<br />"))
                                    txtSearch.ClientIDMode = ClientIDMode.Static
                                    txtSearch.ID = ColumnName & "~" & "TextBox"
                                    txtSearch.CssClass = "signup"
                                    txtSearch.Width = Unit.Percentage(92)
                                    Container.Controls.Add(txtSearch)
                                ElseIf ControlType = "DateField" Then
                                    Container.Controls.Add(New LiteralControl("<br />"))

                                    Dim table As New System.Web.UI.HtmlControls.HtmlGenericControl("table")
                                    Dim tr As New System.Web.UI.HtmlControls.HtmlGenericControl("tr")

                                    Dim td1 As New System.Web.UI.HtmlControls.HtmlGenericControl("td")
                                    Dim td2 As New System.Web.UI.HtmlControls.HtmlGenericControl("td")
                                    td2.Style.Add("padding-left", "5px")

                                    td1.InnerHtml = "From<br/>"
                                    td2.InnerHtml = "To<br/>"

                                    Dim dtFrom As New TextBox
                                    dtFrom.ID = ColumnName & "~" & ControlType & "~From"
                                    dtFrom.ClientIDMode = ClientIDMode.Static
                                    dtFrom.Width = New Unit(80, UnitType.Pixel)

                                    td1.Controls.Add(dtFrom)

                                    Dim ajaxFrom As New AjaxControlToolkit.CalendarExtender()
                                    ajaxFrom.TargetControlID = dtFrom.ID
                                    ajaxFrom.Format = "MM/dd/yyyy"
                                    td1.Controls.Add(ajaxFrom)

                                    Dim dtTo As New TextBox
                                    dtTo.ClientIDMode = ClientIDMode.Static
                                    dtTo.ID = ColumnName & "~" & ControlType & "~To"
                                    dtTo.Width = New Unit(80, UnitType.Pixel)

                                    td2.Controls.Add(dtTo)

                                    Dim ajaxTo As New AjaxControlToolkit.CalendarExtender()
                                    ajaxTo.TargetControlID = dtTo.ID
                                    ajaxTo.Format = "MM/dd/yyyy"
                                    td1.Controls.Add(ajaxTo)

                                    tr.Controls.Add(td1)
                                    tr.Controls.Add(td2)

                                    table.Controls.Add(tr)

                                    Container.Controls.Add(table)
                                End If
                            End If
                        Else
                            Dim chk As New CheckBox
                            chk.ID = "chk"
                            'chk.Attributes.Add("onclick", "return SelectAll('EmailBox1_gvInbox_ctl01_chk')") '
                            Container.Controls.Add(chk)
                        End If
                    Case ListItemType.Item
                        If ControlType <> "DeleteCheckBox" AndAlso ControlType <> "SelectBox" AndAlso ControlType <> "HiddenField" Then
                            If DBColumnName <> "vcBodyText" AndAlso DBColumnName <> "RecentCorrespondance" AndAlso DBColumnName <> "AlertPanel" Then
                                AddHandler lbl1.DataBinding, AddressOf BindStringColumn
                                ' lbl1.ID = "BodyText1"
                                'If DBColumnName = "vcSubject" Then
                                lbl1.ID = DBColumnName
                                'End If
                                Container.Controls.Add(lbl1)
                                'Else
                                'AddHandler hdn1.DataBinding, AddressOf BindBodyText
                                ' hdn1.ID = "hdnBodyText"
                                ' Container.Controls.Add(hdn1)
                            End If
                        ElseIf ControlType = "SelectBox" AndAlso DBColumnName = "vcData" Then
                            ddlEmailHistory.ID = "ddlEmailHistory"
                            ddlEmailHistory.CssClass = "target"
                            ddlEmailHistory.Style.Add("border", "0px")
                            Container.Controls.Add(ddlEmailHistory)
                        ElseIf ControlType <> "HiddenField" Then
                            Dim chk As New CheckBox
                            chk.ID = "chk"
                            Container.Controls.Add(chk)
                        End If

                        If ControlType = "Image" AndAlso DBColumnName = "bitIsRead" Then
                            AddHandler hdn1.DataBinding, AddressOf BindReadValueStatus
                            hdn1.ID = "hdnIsread"
                            Container.Controls.Add(hdn1)

                            AddHandler hdn1.DataBinding, AddressOf BindKeyValue
                            hdn1.ID = "numEmailHstrID"
                            Container.Controls.Add(hdn1)

                            hdn2.ID = "hdnContactId"
                            ph1.Controls.Add(hdn2)
                            AddHandler ph1.DataBinding, AddressOf BindNewActionItem
                            Container.Controls.Add(ph1)


                            AddHandler hdn3.DataBinding, AddressOf BindNumOfTimeOpened
                            hdn3.ID = "hdnnumNoofTimes"
                            Container.Controls.Add(hdn3)
                        End If
                        If ControlType = "HiddenField" AndAlso DBColumnName = "numEmailHstrID" Then
                            AddHandler hdn1.DataBinding, AddressOf BindKeyValue
                            hdn1.ID = "numEmailHstrID"
                            Container.Controls.Add(hdn1)
                        End If
                        If ControlType = "HiddenField" AndAlso DBColumnName = "numContactID" Then
                            hdn1.ID = "hdnContactId"
                            ph1.Controls.Add(hdn1)
                            AddHandler ph1.DataBinding, AddressOf BindNewActionItem
                            Container.Controls.Add(ph1)
                        End If
                        If ControlType = "TextBox" AndAlso DBColumnName = "AlertPanel" Then
                            AddHandler lbl1.DataBinding, AddressOf BindAlertMsg
                            lbl1.ID = "lblAlertPanel"
                            lbl1.ForeColor = Color.Red
                            lbl1.CssClass = "Alert"
                            Container.Controls.Add(lbl1)
                        End If
                        If ControlType = "HiddenField" AndAlso DBColumnName = "numNoofTimes" Then
                            AddHandler hdn1.DataBinding, AddressOf BindNumOfTimeOpened
                            hdn1.ID = "hdnnumNoofTimes"
                            Container.Controls.Add(hdn1)
                        End If
                End Select
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub BindAlertMsg(ByVal sender As Object, ByVal e As EventArgs)
            Dim lbl As Label = CType(sender, Label)
            Dim ControlClass As String = ""

            Dim Container As GridViewRow = CType(lbl.NamingContainer, GridViewRow)
            Dim rowView As DataRowView = CType(Container.DataItem, DataRowView)
            If DBColumnName = "AlertPanel" Then

                lbl.Text = DataBinder.Eval(Container.DataItem, ColumnName).ToString
            End If
        End Sub

        Sub BindKeyValue(ByVal sender As Object, ByVal e As EventArgs)
            Dim hdnListItemId As HiddenField = CType(sender, HiddenField)
            Dim ControlClass As String = ""

            Dim Container As GridViewRow = CType(hdnListItemId.NamingContainer, GridViewRow)
            hdnListItemId.Value = DataBinder.Eval(Container.DataItem, "numEmailHstrID").ToString
        End Sub

        Sub BindNewActionItem(ByVal sender As Object, ByVal e As EventArgs)
            Dim ph As PlaceHolder = CType(sender, PlaceHolder)
            Dim ControlClass As String = ""

            Dim Container As GridViewRow = CType(ph.NamingContainer, GridViewRow)

            CType(ph.FindControl("hdnContactId"), HiddenField).Value = DataBinder.Eval(Container.DataItem, "numContactId").ToString
        End Sub

        Sub BindNumOfTimeOpened(ByVal sender As Object, ByVal e As EventArgs)
            Dim hdnListItemId As HiddenField = CType(sender, HiddenField)
            Dim ControlClass As String = ""

            Dim Container As GridViewRow = CType(hdnListItemId.NamingContainer, GridViewRow)
            hdnListItemId.Value = DataBinder.Eval(Container.DataItem, "numNoofTimes").ToString
        End Sub

        Sub BindReadValueStatus(ByVal sender As Object, ByVal e As EventArgs)
            Dim hdnListItemId As HiddenField = CType(sender, HiddenField)
            Dim ControlClass As String = ""

            Dim Container As GridViewRow = CType(hdnListItemId.NamingContainer, GridViewRow)
            If DBColumnName = "bitIsRead" Then
                hdnListItemId.Value = DataBinder.Eval(Container.DataItem, ColumnName).ToString
            End If
        End Sub

        Sub BindBodyText(ByVal sender As Object, ByVal e As EventArgs)
            Dim hdnListItemId As HiddenField = CType(sender, HiddenField)
            Dim ControlClass As String = ""

            Dim Container As GridViewRow = CType(hdnListItemId.NamingContainer, GridViewRow)
            If DBColumnName = "vcBodyText" Then
                hdnListItemId.Value = DataBinder.Eval(Container.DataItem, ColumnName).ToString
            End If
        End Sub

        Sub BindnumListItem(ByVal sender As Object, ByVal e As EventArgs)
            Dim hdnListItemId As HiddenField = CType(sender, HiddenField)
            Dim ControlClass As String = ""

            Dim Container As GridViewRow = CType(hdnListItemId.NamingContainer, GridViewRow)
            If DBColumnName = "numListItemId" Then
                hdnListItemId.Value = DataBinder.Eval(Container.DataItem, ColumnName).ToString
            End If
        End Sub

        Function SetBytes(ByVal Bytes) As String
            Try
                If Bytes >= 1073741824 Then
                    SetBytes = Format(Bytes / 1024 / 1024 / 1024, "#0.00") & " GB"
                ElseIf Bytes >= 1048576 Then
                    SetBytes = Format(Bytes / 1024 / 1024, "#0.00") & " MB"
                ElseIf Bytes >= 1024 Then
                    SetBytes = Format(Bytes / 1024, "#0.00") & " KB"
                ElseIf Bytes < 1024 Then
                    SetBytes = Fix(Bytes) & " B"
                End If
                Exit Function
            Catch ex As Exception
                Throw
            End Try
        End Function

        Sub BindStringColumn(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim ControlClass As String = ""

                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
                If DBColumnName = "vcFrom" Then
                    Dim str As String() = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName)), "", DataBinder.Eval(Container.DataItem, ColumnName)).ToString.Split(New String() {"$^$"}, StringSplitOptions.None)
                    If str.Length > 1 Then
                        lbl1.Text = IIf(str(1).ToString = "", str(2).ToString, str(1).ToString)
                    Else
                        lbl1.Text = str(0).ToString
                    End If
                Else
                    If ControlType = "CheckBox" Then
                        lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName)), "", IIf(DataBinder.Eval(Container.DataItem, ColumnName), "Yes", "No"))
                    Else
                        If DBColumnName = "vcSize" Then
                            lbl1.Text = SetBytes(IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName)), "0", DataBinder.Eval(Container.DataItem, ColumnName)))
                        ElseIf DBColumnName = "vcSubject" Then
                            lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName)), "", DataBinder.Eval(Container.DataItem, ColumnName)).ToString().TrimLength(100)
                        Else
                            lbl1.Text = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName)), "", DataBinder.Eval(Container.DataItem, ColumnName)).ToString()
                        End If
                    End If
                End If

                If DBColumnName = "vcTo" Then
                    Dim str As String() = IIf(IsDBNull(DataBinder.Eval(Container.DataItem, ColumnName)), "", DataBinder.Eval(Container.DataItem, ColumnName)).ToString.Split(New String() {"$^$"}, StringSplitOptions.None)
                    If str.Length > 1 Then
                        lbl1.Text = IIf(str(1).ToString = "", str(2).ToString, str(1).ToString)
                    Else
                        lbl1.Text = str(0).ToString
                    End If
                End If

                If DBColumnName = "bitIsRead" Then
                    If CCommon.ToBool(DataBinder.Eval(Container.DataItem, ColumnName)) = True Then
                        lbl1.Text = "<img src='../images/msg_read.gif' height=""14px"" width=""16px""  />"
                    Else
                        lbl1.Text = "<img src='../images/msg_unread.gif' height=""14px"" width=""16px"" />"
                    End If
                End If

                If DBColumnName = "IsReplied" Then
                    If CCommon.ToBool(DataBinder.Eval(Container.DataItem, ColumnName)) = True Then
                        lbl1.Text = "<img src='../images/reply.png' height=""14px"" width=""16px""  />"
                    Else
                        lbl1.Text = ""
                    End If
                End If

                If DBColumnName = "bitHasAttachments" Then
                    If CCommon.ToBool(DataBinder.Eval(Container.DataItem, ColumnName)) = True Then
                        lbl1.Text = "<img src='../images/hdr_attachment.gif' align=""BASELINE"" style=""float:none;""/>"
                    Else
                        lbl1.Text = ""
                    End If
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Sub BindStringColumn1(ByVal Sender As Object, ByVal e As EventArgs)
            Try
                Dim lbl1 As Label = CType(Sender, Label)
                Dim Container As GridViewRow = CType(lbl1.NamingContainer, GridViewRow)
            Catch ex As Exception
                Throw
            End Try
        End Sub

    End Class
End Namespace
