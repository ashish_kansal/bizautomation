﻿Imports BACRM.BusinessLogic.Common
Imports System.Text.RegularExpressions
Imports System.Globalization
Imports BACRM.BusinessLogic.Opportunities
Imports BACRM.BusinessLogic.Accounting
Imports System.Xml.Serialization
Imports System.Text
Imports System.IO
Imports System.Xml
Imports System.Collections.Generic
Imports System.Net.Mail
Imports System.Net
Imports BACRM.BusinessLogic.Admin
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.ShioppingCart


Partial Public Class WebForm1
    Inherits BACRMPage
    Dim lngRuleID As Long
    Dim strBuilMain As New StringBuilder
    'Dim objShippingRule As PromotionOffer

    Dim objShippingRule As ShippingRule
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                LoadDetails()

                Dim oDir As New DirectoryInfo(Server.MapPath("images/Facebook"))
                Dim fileList() As FileInfo = oDir.GetFiles("*.jpeg")
                Dim iFileCount As Integer = fileList.Count
                iFileCount -= 1
                Dim oImage As HtmlImage
                For i As Integer = 0 To iFileCount
                    oImage = New HtmlImage
                    With oImage
                        .Src = String.Format("images\Facebook\{0}", fileList(i))
                        If i = 0 Then .Attributes.Add("class", "active")
                    End With
                    slideshow.Controls.Add(oImage)
                Next

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
    Sub LoadDetails()
        Try
            Dim objItems As New CItems()
            Dim dt As DataTable = Nothing
            objItems.byteMode = 13
            objItems.SiteID = 90

            dt = objItems.SeleDelCategory()
            If dt.Rows.Count > 0 Then

                Dim StartUL As String = "<ul>"
                Dim EndUL As String = "</ul>"
                Dim StartLi As String = "<li>"
                Dim EndLi As String = "</li>"
                Dim startSpanText As String = "<span>text"
                Dim EndSpanText As String = "</span>"
                Dim startSpanValue As String = "<span id=""value"">"
                Dim EndSpanValue As String = "</span>"

                Dim intCnt As Integer = 0
                Dim drLevel As DataRow() = dt.Select("category = 0")
                Dim ul As New HtmlGenericControl("ul")
                Dim ulSub As New HtmlGenericControl("ul")
                Dim strLink As String = ""

                For Each dr As DataRow In drLevel
                    intCnt = intCnt + 1
                    Dim li As New HtmlGenericControl("li")

                    If intCnt = 1 Then
                        strBuilMain.Append("<ul id=""treeData"" style=""display: none;"" >")
                    End If

                    Dim drSubCategories As DataRow() = dt.Select("Category = " & CCommon.ToLong(dr("numCategoryID")))

                    strLink = "'" & "/SubCategory/" + Sites.GenerateSEOFriendlyURL(dr("vcCategoryName").ToString()) + "/" + dr("numCategoryID").ToString() & "'"

                    If drSubCategories.Length > 0 Then
                        Dim IsParent As Boolean = True

                        strBuilMain.Append(StartLi & _
                                          startSpanText.Replace("text", "<a onclick=""'openLink(" & strLink & ")'"">" & dr("vcCategoryName").ToString() & "</a>") & EndSpanText & _
                                          startSpanValue.Replace("value", dr("numCategoryID").ToString()) & EndSpanValue)

                        PopulateNode(drSubCategories, IsParent, dt)

                        strBuilMain.Append(EndUL)
                        strBuilMain.Append(EndLi)

                    Else
                        strBuilMain.Append(StartLi & _
                                          startSpanText.Replace("text", "<a onclick=""'openLink(" & strLink & ")'"">" & dr("vcCategoryName").ToString() & "</a>") & EndSpanText & _
                                          startSpanValue.Replace("value", dr("numCategoryID").ToString()) & EndSpanValue & _
                                          EndLi)
                    End If
                Next
                strBuilMain.Append(EndUL)

                litULCollection.Text = strBuilMain.ToString()
                tree.Controls.Add(litULCollection)

            End If

        Catch ex As Exception
            Throw ex
        End Try
       
    End Sub

    Private Sub PopulateNode(ByVal dr() As DataRow, ByVal IsParent As Boolean, ByVal dtMain As DataTable)
        Try

            Dim strMain As String = ""
            Dim StartUL As String = "<ul>"
            Dim EndUL As String = "</ul>"
            Dim StartLi As String = "<li>"
            Dim EndLi As String = "</li>"
            Dim startSpanText As String = "<span>text"
            Dim EndSpanText As String = "</span>"
            Dim startSpanValue As String = "<span id=""value"">"
            Dim EndSpanValue As String = "</span>"
            Dim IsFromInnerMethod As Boolean = False

            Dim strLink As String = ""
            For Each drSub As DataRow In dr

                If dtMain.Select("Category = " & CCommon.ToLong(drSub("numCategoryID"))).Length > 0 Then

                    strLink = "'" & "/SubCategory/" + Sites.GenerateSEOFriendlyURL(drSub("vcCategoryName").ToString()) + "/" + drSub("numCategoryID").ToString() & "'"

                    If IsParent = True Then
                        strBuilMain.Append(StartUL)
                    End If

                    strBuilMain.Append(StartLi & _
                                   startSpanText.Replace("text", "<a onclick=""'openLink(" & strLink & ")'"">" & drSub("vcCategoryName").ToString() & "</a>") & EndSpanText & _
                                   startSpanValue.Replace("value", drSub("numCategoryID").ToString()) & EndSpanValue)

                    Dim drSubSub As DataRow() = dtMain.Select("Category = " & CCommon.ToLong(drSub("numCategoryID")))

                    If drSubSub.Length > 0 Then
                        IsParent = True
                    Else
                        IsParent = False
                    End If

                    IsFromInnerMethod = True
                    PopulateNode(drSubSub, IsParent, dtMain)

                    If IsParent = True Then
                        strBuilMain.Append(EndUL)
                        strBuilMain.Append(EndLi)
                    End If

                Else

                    If IsParent = True AndAlso IsFromInnerMethod = False Then
                        strBuilMain.Append(StartUL)
                    End If

                    strBuilMain.Append(StartLi & _
                                   startSpanText.Replace("text", "<a onclick=""'openLink(" & strLink & ")'"">" & drSub("vcCategoryName").ToString() & "</a>") & EndSpanText & _
                                   startSpanValue.Replace("value", drSub("numCategoryID").ToString()) & EndSpanValue & _
                                   EndLi)

                    If dtMain.Select("Category = " & CCommon.ToLong(drSub("numCategoryID"))).Length > 0 Then
                        IsParent = True
                    Else
                        IsParent = False
                    End If

                End If
            Next
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Dim strlin As String = "<a onclick=""openLink('" & Str(1) & "')"">"
    'Private Sub PopulateMenuitem(dt As DataTable, TreeViewnode As TreeNodeCollection)
    '    Try

    '        For Each dr As DataRow In dt.Rows
    '            Dim tn As New TreeNode()
    '            tn.Text = dr("vcCategoryName").ToString()
    '            tn.Value = dr("numCategoryID").ToString()
    '            If dr("Type").ToString() = "0" Then
    '                tn.NavigateUrl = "/SubCategory/" & Sites.GenerateSEOFriendlyURL(dr("vcCategoryName").ToString()) & "/" & dr("numCategoryID").ToString()

    '            ElseIf dr("Type").ToString() = "1" Then
    '                tn.NavigateUrl = dr("vcLink").ToString()

    '            ElseIf dr("Type").ToString() = "2" Then
    '                tn.NavigateUrl = "/Product.aspx?ItemID=" & dr("numCategoryID").ToString()

    '            End If

    '            tn.Expanded = True

    '            'when Found then Set Node as expanded
    '            If Request.Cookies.[Get]("CatID") IsNot Nothing Then
    '                If Request.Cookies.[Get]("CatID").Value.Contains(tn.Value) Then
    '                    tn.Expanded = True
    '                End If
    '            End If


    '            If dr("Type").ToString() = "2" Then
    '                If (TreeViewCategory.FindNode(dr("Category").ToString()) IsNot Nothing) Then
    '                    TreeViewCategory.FindNode(dr("Category").ToString()).ChildNodes.Add(tn)
    '                End If
    '            Else
    '                If dr("Category").ToString() = "0" Then
    '                    TreeViewCategory.Nodes.Add(tn)
    '                Else
    '                    AddSubCategory(TreeViewCategory.Nodes, tn, CCommon.ToString(dr("Category")))
    '                End If
    '            End If
    '        Next
    '    Catch ex As Exception
    '        Throw ex
    '    End Try
    'End Sub

End Class