﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports Microsoft.AspNet.SignalR

Namespace DatabaseNotification
    Public Class NotificationsHub
        Inherits Hub
        Public Sub NotifyAllClients()
            Try
                Dim context As IHubContext = GlobalHost.ConnectionManager.GetHubContext(Of NotificationsHub)()
                context.Clients.All.displayNotification()
            Catch ex As Exception

            End Try
        End Sub
    End Class

    Public Class CaseHub
        Inherits Hub
        Public Sub NotifyCaseAllClients()
            Try
                Dim context1 As IHubContext = GlobalHost.ConnectionManager.GetHubContext(Of CaseHub)()
                context1.Clients.All.displayCaseNotification()
            Catch ex As Exception

            End Try
        End Sub
    End Class

    Public Class OpportunityHub
        Inherits Hub
        Public Sub NotifyOpportunityAllClients()
            Dim Oppcontext As IHubContext = GlobalHost.ConnectionManager.GetHubContext(Of OpportunityHub)()
            Oppcontext.Clients.All.displayOpportunityNotification()
        End Sub
    End Class

    Public Class ProjectHub
        Inherits Hub
        Public Sub NotifyProjectAllClients()
            Dim Projectcontext As IHubContext = GlobalHost.ConnectionManager.GetHubContext(Of ProjectHub)()
            Projectcontext.Clients.All.displayProjectNotification()
        End Sub
    End Class

    Public Class EmailHub
        Inherits Hub
        Public Sub NotifyEmailAllClients()
            Dim Emailcontext As IHubContext = GlobalHost.ConnectionManager.GetHubContext(Of EmailHub)()
            Emailcontext.Clients.All.displayEmailNotification()
        End Sub
    End Class

    Public Class TicklerHub
        Inherits Hub
        Public Sub NotifyTicklerAllClients()
            Dim Ticklercontext As IHubContext = GlobalHost.ConnectionManager.GetHubContext(Of TicklerHub)()
            Ticklercontext.Clients.All.displayTicklerNotification()
        End Sub
    End Class

    Public Class ProcessHub
        Inherits Hub
        Public Sub NotifyProcessAllClients()
            Dim Processcontext As IHubContext = GlobalHost.ConnectionManager.GetHubContext(Of ProcessHub)()
            Processcontext.Clients.All.displayProcessNotification()
        End Sub
    End Class

End Namespace
