﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="Test.aspx.vb" Inherits=".Test" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Basic usage</title>
    <script src="js/jquery.min.js"></script>
    <script src="js/kendo.web.min.js"></script>
    <link href="styles/kendo.common.min.css" rel="stylesheet" />
    <link href="styles/kendo.default.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
    <div id="grid">
    </div>
    <asp:Label ID="lblScript" runat="server"></asp:Label>
    <script language="javascript">
        //        var dtData = [
        //                                            { "numWareHouseItemID": 154237.0, "vcWarehouse": "West", "numWareHouseID": 58.0, "OnHand": 0.0, "PurchaseOnHand": 0.00, "SalesOnHand": 0.00, "Reorder": 0.0, "OnOrder": 5.0, "Allocation": 1.0, "BackOrder": 8.0, "Op_Flag": 0, "Location": "5", "Price": 0.0000, "SKU": "123456", "BarCode": "654321" },
        //                                            { "numWareHouseItemID": 154293.0, "vcWarehouse": "East", "numWareHouseID": 59.0, "OnHand": 0.0, "PurchaseOnHand": 0.00, "SalesOnHand": 0.00, "Reorder": 0.0, "OnOrder": 0.0, "Allocation": 0.0, "BackOrder": 1.0, "Op_Flag": 0, "Location": "0", "Price": 0.0000, "SKU": "", "BarCode": "0" },
        //                                            { "numWareHouseItemID": 154294.0, "vcWarehouse": "Shanghai", "numWareHouseID": 920.0, "OnHand": 0.0, "PurchaseOnHand": 0.00, "SalesOnHand": 0.00, "Reorder": 0.0, "OnOrder": 0.0, "Allocation": 0.0, "BackOrder": 0.0, "Op_Flag": 0, "Location": "0", "Price": 0.0000, "SKU": "", "BarCode": "0" }
        //                                            ];

        //        var dtSub = [
        //                            { "numWareHouseItmsDTLID": 82.0, "numWareHouseItemID": 154237.0, "vcSerialNo": "4543", "Op_Flag": 0, "Comments": "34534", "numQty": 1.0, "OldQty": 1.0, "vcWarehouse": "West", "Size": "14643", "Color": "17129" },
        //                            { "numWareHouseItmsDTLID": 96.0, "numWareHouseItemID": 154237.0, "vcSerialNo": "werwe", "Op_Flag": 0, "Comments": "erwe", "numQty": 1.0, "OldQty": 1.0, "vcWarehouse": "West", "Size": "14642", "Color": "17129" },
        //                            { "numWareHouseItmsDTLID": 97.0, "numWareHouseItemID": 154237.0, "vcSerialNo": "fgh", "Op_Flag": 0, "Comments": "fhfgh", "numQty": 1.0, "OldQty": 1.0, "vcWarehouse": "West", "Size": "14643", "Color": "17129" }
        //                            ];

        //        var dtColumns = [{ "field": "vcWarehouse", "title": "WareHouse" },
        //                         { "field": "OnHand", "title": "On Hand" },
        //                         { "field": "Reorder", "title": "Re Order" },
        //                         { "field": "Allocation", "title": "Allocation" },
        //                         { "field": "BackOrder", "title": "Back Order"}];
        var dtData = [];
        var dtSub = [];
        var dtColumns = [];

        $(document).ready(function () {

            createGetItemWareHouse();

            function createGetItemWareHouse() {
                $.ajax({
                    type: "POST",
                    url: "../common/Common.asmx/GetItemWareHouse",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: "{lngItemCode: 197563}",
                    success: function (msg) {
                        var tableData = JSON.parse(msg.d);

                        //                        $.each(tableData.Table, function (index, data) {
                        //                            dtData.push({
                        //                                numWareHouseItemID: data.numWareHouseItemID,
                        //                                numWareHouseID: data.numWareHouseID,
                        //                                vcWarehouse: data.vcWarehouse,
                        //                                OnHand: data.OnHand
                        //                            });
                        //                        });


                        //                        $.each(tableData.Table1, function (index, data) {
                        //                            dtSub.push({
                        //                                numWareHouseItemID: data.numWareHouseItemID,
                        //                                numWareHouseItemsDTLID: data.numWareHouseItemsDTLID,
                        //                                vcSerialNo: data.vcSerialNo,
                        //                                Comments: data.Comments
                        //                            });
                        //                        });

                        //                        $.each(tableData.Table3, function (index, data) {
                        //                            dtColumns.push({
                        //                                field: data.field,
                        //                                title: data.title
                        //                            });
                        //                        });

                        dtData = tableData.Table;
                        dtSub = tableData.Table1;
                        dtColumns = tableData.Table3;
                        //return dtData;
                        //                            var tableData = JSON.parse(msg.d);
                        //                            return tableData.Table;

                        initGrid();
                    }
                });
            }


            function initGrid() {
                var dataSource1 = new kendo.data.DataSource({
                    pageSize: 30,
                    data: dtData,
                    schema: {
                        model: {
                            id: "numWareHouseItemID",
                            fields: {
                                numWareHouseItemID: { type: "number" },
                                numWareHouseID: { type: "number" },
                                vcWarehouse: { type: "string" },
                                OnHand: { type: "number", validation: { min: 0} },
                                Reorder: { type: "number", validation: { min: 0 }, editable: false },
                                Allocation: { type: "number", validation: { min: 0 }, editable: false },
                                BackOrder: { type: "number", validation: { min: 0 }, editable: false },
                                Price: { type: "number", validation: { min: 0} }
                            }
                        }
                    }
                });

                $("#grid").kendoGrid({
                    //                    dataSource: {
                    //                        transport: {
                    //                            read: {
                    //                                dataType: "json",
                    //                                url: "../common/Common.asmx/GetItemWareHouse",
                    //                                contentType: "application/json; charset=utf-8",
                    //                                type: "POST"
                    //                            },
                    //                            parameterMap: function (data, operation) {
                    //                                return JSON.stringify({ lngItemCode: 197563 })
                    //                            }
                    //                        },
                    //                        //data: createGetItemWareHouse(),
                    //                        schema: {
                    //                            data: function (data) {
                    //                                //console.log(JSON.parse(data.d));
                    //                                var tableData = JSON.parse(data.d);
                    //                                return tableData.Table;
                    //                            },
                    //                            model: {
                    //                                id: "numWareHouseItemID",
                    //                                fields: {
                    //                                    numWareHouseItemID: { type: "number" },
                    //                                    numWareHouseID: { type: "number" },
                    //                                    vcWarehouse: { type: "string" },
                    //                                    OnHand: { type: "number" }
                    //                                }
                    //                            }
                    //                        },
                    //                        pageSize: 10
                    //                    },
                    dataSource: dataSource1,
                    detailInit: detailInit,
                    //                    dataBound: function () {
                    //                        this.expandRow(this.tbody.find("tr.k-master-row").first());
                    //                    },
                    height: 550,
                    groupable: true,
                    scrollable: true,
                    sortable: true,
                    pageable: true,
                    columns: dtColumns,
                    editable: true,
                    //                    navigatable: true,
                    //                    selectable: "row",
                    toolbar: ["create", "save", "cancel"]
                    //                columns: [
                    //                        { field: "numWareHouseItemID" },
                    //                        { field: "numWareHouseID" },
                    //                        { field: "vcWarehouse" }
                    //                        ]
                });

                //                var grid = $("#grid").data("kendoGrid");

                //                grid.dataSource.read();
            }

        });

        //            var dataSourceSub = new kendo.data.DataSource({
        //                pageSize: 30,
        //                data: dtSub,
        //                schema: {
        //                    model: {
        //                        id: "numWareHouseItemsDTLID",
        //                        fields: {
        //                            numWareHouseItemID: { type: "number" },
        //                            numWareHouseItemsDTLID: { type: "number" },
        //                            vcSerialNo: { type: "string" },
        //                            Comments: { type: "string" }
        //                        }
        //                    }
        //                },
        //                filter: { field: "numWareHouseItemID", operator: "eq", value: e.data.numWareHouseItemID }
        //            });

        function detailInit(e) {
            $("<div/>").appendTo(e.detailCell).kendoGrid({
                dataSource: { pageSize: 30,
                    data: dtSub,
                    schema: {
                        model: {
                            id: "numWareHouseItmsDTLID",
                            fields: {
                                numWareHouseItemID: { type: "number" },
                                numWareHouseItemsDTLID: { type: "number" },
                                vcSerialNo: { type: "string" },
                                Comments: { type: "string" }
                            }
                        }
                    },
                    filter: { field: "numWareHouseItemID", operator: "eq", value: e.data.numWareHouseItemID }
                },
                scrollable: false,
                sortable: true,
                pageable: true,
                editable: true,
                toolbar: ["create", "save", "cancel"],
                columns: [
                            { field: "numWareHouseItemID" },
                            { field: "vcSerialNo" },
                            { field: "Comments" }
                        ]
            });
        }


    </script>
    </form>
</body>
</html>
