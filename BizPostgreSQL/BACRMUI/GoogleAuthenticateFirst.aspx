﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="GoogleAuthenticateFirst.aspx.vb"
    Inherits=".GoogleAuthenticateFirst" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Google Authorization</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="input-part">
        <div class="right-input">
            <asp:Button ID="btnContinue" CssClass="button" runat="server" Text="Continue"></asp:Button>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    Google Authorization
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table id="tblAuthorization" runat="server" visible="false" width="600px">
        <tr>
            <td class="normal1">
                <br />
                <br />
                <asp:Label ID="lblMessage" runat="server" CssClass="normal1"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="normal1">
            <asp:Label id="lblError" runat="server" ForeColor="Crimson" Visible="False">Errors</asp:Label> <br />
			<asp:Table id="tblBalance" runat="server" Width="664px" Height="24px"></asp:Table> <br />
            </td>
        </tr>
    </table>
</asp:Content>
