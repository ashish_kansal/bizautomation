﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Outlook
Public Class frmOpenEmail
    Inherits BACRMPage

    Dim objOutlook As New COutlook
    Public lngDivID As Long
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngDivID = GetQueryStringVal( "DivID")

            If Not IsPostBack Then
                bindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub bindData()
        Dim dttable As DataTable

        objOutlook.DomainId = Session("DomainId")
        objOutlook.DivisionID = lngDivID

        Dim bDivision As Integer = CCommon.ToLong(GetQueryStringVal( "bDivision"))

        dttable = objOutlook.getInboxOpenEmailByDivision(bDivision)

        rptCorr.DataSource = dttable
        rptCorr.DataBind()
    End Sub
    Function SetBytes(ByVal Bytes) As String
        Try
            If Bytes >= 1073741824 Then
                SetBytes = Format(Bytes / 1024 / 1024 / 1024, "#0.00") & " GB"
            ElseIf Bytes >= 1048576 Then
                SetBytes = Format(Bytes / 1024 / 1024, "#0.00") & " MB"
            ElseIf Bytes >= 1024 Then
                SetBytes = Format(Bytes / 1024, "#0.00") & " KB"
            ElseIf Bytes < 1024 Then
                SetBytes = Fix(Bytes) & " B"
            End If
            Exit Function
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Protected Sub btnRead_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRead.Click
        Dim i As Integer = 0

        Dim dtEmail As New DataTable
        dtEmail.TableName = "EmailHistory"
        dtEmail.Columns.Add("numEmailHstrID")

        Dim dr As DataRow
        While i < rptCorr.Items.Count
            Dim chk As CheckBox = DirectCast(rptCorr.Items(i).FindControl("chkSelect"), CheckBox)
            If chk.Checked Then
                dr = dtEmail.NewRow
                dr("numEmailHstrID") = DirectCast(rptCorr.Items(i).FindControl("hdfId"), HiddenField).Value
                dtEmail.Rows.Add(dr)
            End If
            i += 1
        End While

        Dim dsNew As New DataSet
        dsNew.Tables.Add(dtEmail)
        objOutlook.strXml = dsNew.GetXml
        dsNew.Tables.Remove(dsNew.Tables(0))

        objOutlook.DomainId = Session("DomainId")
        objOutlook.ChangeEmailAsRead()

        bindData()
    End Sub
End Class