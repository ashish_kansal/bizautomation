<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmFollowUpHstr.aspx.vb"
    Inherits="BACRM.UserInterface.Leads.frmFollowUpHstr" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>FollowUp History</title>
    <script type="text/javascript" language="javascript">
        function ShowWindow(Page, q, att) {
            alert(Page)
            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                return false;

            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    FollowUp History
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <table cellspacing="0" cellpadding="0" width="600px" border="0">
        <tr>
            <td colspan="3">
                <asp:DataGrid ID="dgFollow" runat="server" Width="100%" CssClass="dg" AutoGenerateColumns="False"
                    >
                    <AlternatingItemStyle CssClass="ais"></AlternatingItemStyle>
                    <ItemStyle CssClass="is"></ItemStyle>
                    <HeaderStyle CssClass="hs"></HeaderStyle>
                    <Columns>
                        <asp:BoundColumn Visible="False" DataField="numFollowUpStatusID"></asp:BoundColumn>
                        <asp:BoundColumn HeaderText="Follow-Up Status" DataField="vcdata"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Changed On">
                            <ItemTemplate>
                                <%# ReturnName(DataBinder.Eval(Container.DataItem, "bintAddedDate")) %>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:Button ID="btnDelete" runat="server" CssClass="button Delete" Text="X" CommandName="Delete">
                                </asp:Button>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
</asp:Content>
