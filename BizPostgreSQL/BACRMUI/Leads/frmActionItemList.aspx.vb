﻿Imports BACRM.BusinessLogic.Common

Public Class frmActionItemList
    Inherits BACRMPage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Correspondence1.lngRecordID = GetQueryStringVal( "DivID")
            Correspondence1.Mode = 9
            Correspondence1.bPaging = False
            Correspondence1.bitOpenCommu = True
            Correspondence1.getCorrespondance()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

End Class