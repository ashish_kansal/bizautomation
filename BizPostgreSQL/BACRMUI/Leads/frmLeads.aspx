<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" CodeBehind="frmLeads.aspx.vb"
    Inherits="BACRM.UserInterface.Leads.frmLeads" MasterPageFile="~/common/DetailPage.Master" %>

<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Src="~/common/frmCorrespondence.ascx" TagName="frmCorrespondence" TagPrefix="uc1" %>
<%@ Register Src="../include/AssetList.ascx" TagName="AssetList" TagPrefix="uc1" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Lead Details</title>
    <meta http-equiv="Page-Enter" content="blendTrans(Duration=0.01)">
    <meta http-equiv="Page-Exit" content="blendTrans(Duration=0.01)">
    <script language="javascript">
        function openActionItem(a, b, c, d, e, f) {
            if (e == 'Email') {
                window.open("../outlook/frmMailDtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&numEmailHstrID=" + a, '', 'titlebar=no,top=100,left=250,width=850,height=550,scrollbars=yes,resizable=yes');
                return false;
            }
            else {
                window.location.href = "../admin/ActionItemDetailsOld.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=Leads&CommId=" + a + "&CaseId=" + b + "&CaseTimeId=" + c + "&CaseExpId=" + d;
                return false;
            }

        }
        function OpemParticularEmail(b) {

            var a = $("#imgEmailPopupId").attr("email")
            window.open('../contact/frmComposeWindow.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Lsemail=' + a + '&pqwRT=' + b, 'ComposeWindow', 'toolbar=no,titlebar=no,top=100,left=100,width=1200,height=645,scrollbars=yes,resizable=yes')
            return false;
        }
        function OpenContact(a, b) {
            var str;
            str = "../contact/frmContacts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=contactlist&ft6ty=oiuy&CntId=" + a;
            document.location.href = str;
        }
        function EditSurveyResult(numSurId, numRespondentId) {
            var sURL = '../contact/frmContactsSurveyResponses.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&numSurID=' + numSurId + '&numRespondentId=' + numRespondentId,
                hndSurveyResponsePopUpURL = window.open(sURL, '', 'toolbar=no,titlebar=no,left=190, top=350,width=800,height=270,scrollbars=yes,resizable=yes');
            hndSurveyResponsePopUpURL.focus();
        }
        function MappingContact() {
            var contact = 0;
            $('select[attr="numPartenerContact"]').each(function () {
                contact = parseInt($("option:selected", this).val());
            });
            console.log(contact);
            $.ajax({
                type: "POST",
                url: '../Leads/frmLeads.aspx/MappingContact',
                data: JSON.stringify({ "ContactID": contact }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success:
                    function (result) {
                        console.log("Success");

                    },
                error:
                    function (XMLHttpRequest, textStatus, errorThrown) {
                        console.log("Error");
                    },

            });
        }
        function BindPartnerContact(domain, oppid, dropId) {
            //var dropId = $(this).attr("id");
            console.log(dropId);
            var DivisionID = 0;
            $('select[attr="numPartenerSource"]').each(function () {

                DivisionID = $("option:selected", this).val();
            });
            oppid = 0;
            $.ajax({
                type: "POST",
                url: '../Leads/frmLeads.aspx/bindContact',
                data: JSON.stringify({ "domain": domain, "oppid": oppid, "DivisionID": DivisionID }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success:
                    function (result) {
                        var i = 0;
                        $('select[attr="numPartenerContact"]').each(function () {
                            if (i == 0) {
                                $(this).empty();
                                $(this).append("<option value='0'>-- Select One --</option>");
                                var id = $(this);
                                $.each(JSON.parse(result.d), function (idx, obj) {
                                    id.append("<option value='" + obj.numContactId + "'>" + obj.vcGivenName + "</option>");
                                });
                            }
                            i = 1;
                        });

                    },
                error:
                    function (XMLHttpRequest, textStatus, errorThrown) {
                        console.log("Error");
                    },

            });
        }
        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                return true;
            }
            else {
                return false;
            }
        }
        function DeleteMessage() {
            alert("You Are not Authorized to Delete the Selected Record !");
            return false;
        }
        function OpenTransfer(url) {
            window.open(url, '', "width=340,height=150,status=no,top=100,left=150");
            return false;
        }
        function ShowWindow(Page, q, att) {

            if (att == 'show') {
                document.getElementById(Page).style.visibility = "visible";
                return false;

            }
            if (att == 'hide') {
                document.getElementById(Page).style.visibility = "hidden";
                return false;

            }
        }

        function openDrip(a) {
            window.open("../Marketing/frmConECampHstr.aspx?ConECampID=" + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=300,scrollbars=no,resizable=no')
            return false;
        }
        function openFollow(a) {
            window.open("../Leads/frmFollowUpHstr.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&rtyWR=" + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=200,scrollbars=no,resizable=no')
            return false;
        }
        function OpenAdd(a) {
            window.open("../prospects/frmProspectsAdd.aspx?pwer=dfsdfdsfdsfdsf&rtyWR=" + a, '', 'toolbar=no,titlebar=no,top=300,width=700,height=300,scrollbars=no,resizable=no')
            return false;
        }


        function CheckNumber() {
            if (!(window.event.keyCode > 47 && window.event.keyCode < 58)) {
                window.event.keyCode = 0;
            }
        }
        function GoOrgDetails(a, b) {
            var str;
            if (b == 0) {

                str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=LeadsList&DivID=" + a;

            }
            else if (b == 1) {

                str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=LeadsList&DivID=" + a;

            }
            else if (b == 2) {

                str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=LeadsList&klds+7kldf=fjk-las&DivId=" + a;

            }
            document.location.href = str;
        }
        function Save() {
            //            if (document.getElementById('89Group~0').value == 0) {
            //                alert("Please Select Group")
            //                document.getElementById('89Group~0').focus();
            //                return false;
            //            }
            return validateCustomFields();
        }
        function ShowLayout(a, b, c, d) {
            window.open("../pagelayout/frmCustomisePageLayout.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Ctype=" + a + "&type=" + c + "&PType=3&FormId=" + d, '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
            return false;
        }

        function OpenShareRecord(a) {
            window.open("../common/frmShareRecord.aspx?RecordID=" + a + "&ModuleID=1", '', 'toolbar=no,titlebar=no,width=800,height=500,top=200,scrollbars=yes,resizable=yes');
            return false;
        }

        /*Commented by Neelam - Obsolete function*/
        function OpenDocuments(a) {
            window.open("../Documents/frmSpecDocuments.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=A&yunWE=" + a, '', 'toolbar=no,titlebar=no,top=10,width=1000,height=450,left=10,scrollbars=yes,resizable=yes')
            return false;
        }

        /*Function added by Neelam on 10/07/2017 - Added functionality to open Child Record window in modal pop up*/
        function OpenDocumentFiles(divID) {
            $find("<%= RadWinDocumentFiles.ClientID%>").show();
        }

        /*Function added by Neelam on 02/14/2018 - Added functionality to close document modal pop up*/
        function CloseDocumentFiles(mode) {
            var radwindow = $find('<%=RadWinDocumentFiles.ClientID %>');
            radwindow.close();

            if (mode == 'C') { return false; }
            else if (mode == 'L') {
                $("[id$=btnLinkClose]").click();
            }
    }

    function DeleteDocumentOrLink(fileId) {
        if (confirm('Are you sure, you want to delete the selected item?')) {
            $('#hdnFileId').val(fileId);
            __doPostBack("<%= btnDeleteDocOrLink.UniqueID%>", "");
        }
        else {
            return false;
        }
    }

    $(document).ready(function ($) {
        requestStart = function (target, arguments) {
            if (arguments.get_eventTarget().indexOf("btnAttach") > -1) {
                arguments.set_enableAjax(false);
            }
            if (arguments.get_eventTarget().indexOf("btnLinkClose") > -1) {
                arguments.set_enableAjax(false);
            }
        }
    }
    );
    </script>
    <style>
        #divLinkUpload:before {
            content: "";
            background-color: #dcdcdc;
            position: absolute;
            width: 1px;
            height: 100%;
            top: 10px;
            left: 0%;
            display: block;
        }

        .tblNoBorder td, .tblNoBorder th {
            border: 0px solid #fff !important;
        }

        #tblMain tr td {
            border-bottom: 1px solid #e1e1e1 !important;
        }

        #tblMain tbody tr:first-child td {
            border-top: 1px solid #fff !important;
            border-left: 1px solid #fff !important;
            border-right: 1px solid #fff !important;
        }

        .tableGroupHeader {
            background-color: #f5f5f5 !important;
            font-weight: bold;
        }

        @media (max-width:40em) {
            .tblNoBorder table, .tblNoBorder thead, .tblNoBorder tbody, .tblNoBorder tfoot, .tblNoBorder th, .tblNoBorder td, .tblNoBorder tr {
                display: block;
            }

            .tblNoBorder .editable_select {
                min-height: 30px;
            }

            .tblNoBorder td, .tblNoBorder th {
                text-align: left;
            }
        }

        .overlay {
            position: fixed;
            z-index: 98;
            top: 0px;
            left: 0px;
            right: 0px;
            bottom: 0px;
            background-color: rgba(170, 170, 170, 0.5);
            filter: alpha(opacity=80);
        }

        .overlayContent {
            z-index: 99;
            margin: 250px auto;
        }

        /*div.RadUpload .ruFakeInput {
            visibility: hidden;
            width: 0;
            padding: 0;
        }

        div.RadUpload .ruFileInput {
            width: 1;
        }*/

        .hidden {
            display: none;
        }

        .VerticalAligned {
            vertical-align: top;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="RecordInformationPanel" runat="server"
    ClientIDMode="Static">
    <asp:UpdatePanel ID="updatepanel" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional"
        EnableViewState="true">
        <ContentTemplate>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Button ID="btnDeleteDocOrLink" OnClick="btnDeleteDocOrLink_Click" runat="server" CssClass="hidden"></asp:Button>
    <asp:HiddenField runat="server" ID="hdnFileId" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="RecordActionPanel" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-3">
                <div class="pull-left callout bg-theme">
                    <%--<strong>Organization ID : </strong>--%>
                    <asp:Label ID="lblCustID" runat="server" Font-Size="Larger"></asp:Label>
                    <asp:Label ID="lblAssociation" runat="Server" Font-Size="Larger"></asp:Label>
                </div>
            </div>
            <div class="col-xs-12 col-sm-5">

                <div class="record-small-box record-small-group-box createdBySingleSection">
                    <asp:HyperLink ID="hplTransfer" runat="server" Visible="true" Text="Record Owner:"
                        ToolTip="Transfer Ownership">Owner
                    </asp:HyperLink>
                    <a href="#" id="hplTransferNonVis" runat="server" visible="false" class="small-box-footer">Record Owner <i class="fa fa-user"></i></a>
                    <span class="innerCreated">
                        <asp:Label ID="lblRecordOwner" runat="server"></asp:Label>
                    </span>
                    <a href="#">Created</a>
                    <span class="innerCreated">
                        <asp:Label ID="lblCreatedBy" runat="server"></asp:Label>
                    </span>
                    <a href="#">Modified</a>
                    <span class="innerCreated">
                        <asp:Label ID="lblLastModifiedBy" runat="server"></asp:Label>
                    </span>
                </div>

            </div>
            <div class="col-md-4">
                <div class="pull-right">
                    <asp:LinkButton ID="btnActionItem" runat="server" CssClass="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New Action Item</asp:LinkButton>
                    <asp:LinkButton ID="btnSave" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save</asp:LinkButton>
                    <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save &amp; Close</asp:LinkButton>
                    <asp:LinkButton ID="btnEdit" runat="server" CssClass="btn btn-primary"><i class="fa fa-pencil-square-o"></i>&nbsp;&nbsp;Edit</asp:LinkButton>
                    <asp:LinkButton ID="btnCancel" runat="server" CssClass="btn btn-primary"><i class="fa fa-times"></i>&nbsp;&nbsp;Cancel</asp:LinkButton>
                    <asp:LinkButton ID="btnActDelete" runat="server" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
    <table width="100%">
        <tr>
            <td colspan="2">
                <asp:Literal ID="litMessage" runat="server"></asp:Literal>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="TabsPlaceHolder" runat="server"
    ClientIDMode="Static">
    <telerik:RadTabStrip ID="radOppTab" runat="server" UnSelectChildren="True" EnableEmbeddedSkins="true"
        Skin="Default" ClickSelectedTab="True" AutoPostBack="True" SelectedIndex="0"
        MultiPageID="radMultiPage_OppTab">
        <Tabs>
            <telerik:RadTab Text="&nbsp;&nbsp;Lead Details&nbsp;&nbsp;" Value="LeadDetails" PageViewID="radPageView_LeadDetails">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Areas of Interest&nbsp;&nbsp;" Value="AreasOfInterest"
                PageViewID="radPageView_AreasOfInterest">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Web Analysis&nbsp;&nbsp;" Value="WebAnalysis" PageViewID="radPageView_WebAnalysis">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Correspondence&nbsp;&nbsp;" Value="Correspondence"
                PageViewID="radPageView_Correspondence">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Assets&nbsp;" Value="Assets" PageViewID="radPageView_Assets" Visible="false">
            </telerik:RadTab>
            <telerik:RadTab Text="Associations" Value="Associations" PageViewID="radPageView_Associations">
            </telerik:RadTab>
            <telerik:RadTab Text="&nbsp;&nbsp;Survey History&nbsp;&nbsp;" Value="SurveyHistory"
                PageViewID="radPageView_SurveyHistory">
            </telerik:RadTab>
        </Tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="radMultiPage_OppTab" runat="server" SelectedIndex="0" CssClass="pageView">
        <telerik:RadPageView ID="radPageView_LeadDetails" runat="server">

            <div class="row">
                <div class="col-xs-12">
                    <div class="pull-left">
                        <asp:ImageButton ID="imgOrder" runat="server" ImageUrl="~/images/Building-48.gif" />
                    </div>
                    <div class="pull-right">
                        <table border="0" style="width: 100%; padding-right: 5px;">
                            <tr>
                                <td style="text-align: left;">
                                    <table id="tblDocuments" runat="server">
                                    </table>
                                </td>
                                <td>
                                    <asp:ImageButton ID="imgOpenDocument" runat="server" ImageUrl="~/images/icons/drawer.png" Style="height: 30px;" />
                                </td>
                                <td style="text-align: left;">
                                    <asp:HyperLink ID="hplOpenDocument" runat="server" Font-Bold="true" Font-Underline="true" Text="Attach or Link" Style="display: inline-block; vertical-align: top; line-height: normal;"></asp:HyperLink>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <asp:Table ID="tblMain" CellPadding="3" CellSpacing="3" align="center" Width="100%" GridLines="none" border="0" runat="server" CssClass="table table-responsive tblNoBorder">
                        </asp:Table>
                    </div>
                </div>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_AreasOfInterest" runat="server">
            <div class="table-responsive">
                <asp:Table ID="Table2" runat="server" Width="100%" GridLines="None" Height="300">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top" CssClass="table table-responsive tblNoBorder">
                            <br>
                            <asp:CheckBoxList ID="chkAOI" CellSpacing="20" runat="server" RepeatColumns="3" RepeatDirection="Vertical">
                            </asp:CheckBoxList>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_WebAnalysis" runat="server">
            <div class="row">
                <div class="col-sm-12 col-md-2">
                    <img alt="Web Analysis" src="../images/WebAnalysis.png" />
                </div>
                <div class="col-sm-12 col-md-10">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">
                            <table style="width: 100%">
                                <tr style="height: 45px">
                                    <td style="font-weight: bold; text-align: right; white-space: nowrap">Referring Page:</td>
                                    <td style="width: 100%">
                                        <asp:Label ID="lblReferringPage" runat="server"></asp:Label></td>
                                </tr>
                                <tr style="height: 45px">
                                    <td style="font-weight: bold; text-align: right; white-space: nowrap">First date visited:</td>
                                    <td>
                                        <asp:Label ID="lblFirstDateVisited" runat="server"></asp:Label></td>
                                </tr>
                                <tr style="height: 45px">
                                    <td style="font-weight: bold; text-align: right; white-space: nowrap">Last date visited:</td>
                                    <td>
                                        <asp:Label ID="lblLastDateVisited" runat="server"></asp:Label></td>
                                </tr>
                                <tr style="height: 45px">
                                    <td style="font-weight: bold; text-align: right; white-space: nowrap">Last page visited:</td>
                                    <td>
                                        <asp:Label ID="lblLastPageVisited" runat="server"></asp:Label></td>
                                </tr>
                                <tr style="height: 45px">
                                    <td style="font-weight: bold; text-align: right; white-space: nowrap">Visit history (all):</td>
                                    <td>
                                        <img src="../images/Audit.png" alt="" data-toggle="modal" data-target="#divVisitHistory" style="cursor: hand" /></td>
                                </tr>
                                <tr style="height: 45px">
                                    <td style="font-weight: bold; text-align: right; white-space: nowrap">IP Address:</td>
                                    <td>
                                        <asp:Label ID="lblIpAddress" runat="server"></asp:Label></td>
                                </tr>
                                <tr style="height: 45px">
                                    <td style="font-weight: bold; text-align: right; white-space: nowrap">IP Location:</td>
                                    <td>
                                        <asp:Label ID="lblIPLocation" runat="server"></asp:Label></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-sm-12 col-md-6" id="divBuiltWith" runat="server" visible="false">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">Contact Information</div>
                                        <div class="panel-body">
                                            <div class="row padbottom10">
                                                <div class="col-sm-4"><b>Company Name</b></div>
                                                <div class="col-sm-8">
                                                    <asp:Label runat="server" ID="lblBWCompanyName"></asp:Label><br />
                                                    <a runat="server" id="hplBWLinkedin" href="" rel="nofollow noopener" target="_linkedin" class="small">Find People on LinkedIn</a>
                                                </div>
                                            </div>
                                            <div class="row padbottom10">
                                                <div class="col-sm-12 col-md-6">
                                                    <div class="form-group">
                                                        <label>Address</label>
                                                        <div id="divBWAddress" runat="server">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12 col-md-6">
                                                    <div class="form-group">
                                                        <label>Telephones</label>
                                                        <div id="divBWTelephones" runat="server">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-12">
                                                    <h4>Publicly Listed Contacts</h4>
                                                    <div class="table-responsive" runat="server" id="divBWContacts">
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">Website Information</div>
                                        <div class="panel-body">
                                            <div class="row padbottom10">
                                                <div class="col-sm-3 text-bold">Vertical</div>
                                                <div class="col-sm-8"><asp:Label runat="server" id="lblBWVertical"></asp:Label></div>
                                            </div>
                                            <div class="row padbottom10" style="border-top:2px solid #ddd">
                                                <div class="col-sm-4 text-bold">Sitemap URLs</div>
                                                <div class="col-sm-2"><asp:Label runat="server" id="lblBWSitemapURL" Text="-"></asp:Label></div>
                                                <div class="col-sm-4 text-bold">Referring IPs</div>
                                                <div class="col-sm-2"><asp:Label runat="server" id="lblBWReferringIP" Text="-"></asp:Label></div>
                                                <div class="col-sm-4 text-bold">Brand Followers</div>
                                                <div class="col-sm-2"><asp:Label runat="server" id="lblBWBrandFollowers" Text="-"></asp:Label></div>
                                                <div class="col-sm-4 text-bold">Referring Subnets</div>
                                                <div class="col-sm-2"><asp:Label runat="server" id="lblBWReferringSubnets" Text="-"></asp:Label></div>
                                            </div>
                                            <div class="row padbottom10" style="border-top:2px solid #ddd">
                                                <div class="col-sm-4 text-bold">Google Dimensions</div>
                                                <div class="col-sm-2"><asp:Label runat="server" id="lblBWGoogleDimensions" Text="-"></asp:Label></div>
                                                <div class="col-sm-4 text-bold">Google Metrics</div>
                                                <div class="col-sm-2"><asp:Label runat="server" id="lblBWGoogleMetrics" Text="-"></asp:Label></div>
                                                <div class="col-sm-4 text-bold">Google Goals</div>
                                                <div class="col-sm-2"><asp:Label runat="server" id="lblBWGoogleGoals" Text="-"></asp:Label></div>
                                                <div class="col-sm-4 text-bold">
                                                    <abbr title="Google Tag Manager">GTM</abbr>
                                                    Tags</div>
                                                <div class="col-sm-2"><asp:Label runat="server" id="lblBWGoogleTagManager" Text="-"></asp:Label></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div id="divVisitHistory" class="modal fade" role="dialog">
                <div class="modal-dialog modal-sm modal-lg">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" style="font-size: 27px">&times;</button>
                            <h4 class="modal-title">Visit History</h4>
                        </div>
                        <div class="modal-body">
                            <div class="table-responsive">
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:DataList ID="dlWebAnlys" runat="server" Width="100%" CellPadding="0" CellSpacing="2">
                                            <ItemTemplate>
                                                <table style="width: 100%" class="table table-bordered table-striped">
                                                    <tr>
                                                        <th style="text-align: right">Date Visitied:
                                                        </th>
                                                        <td style="text-align: left">
                                                            <asp:Label ID="lblCreated" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.dtCreated") %>'>
                                                            </asp:Label>
                                                        </td>
                                                        <th style="text-align: right">
                                                            <asp:Label ID="Label4" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.NoOfTimes") %>'>
                                                            </asp:Label>&nbsp;Pages Viewed :
                                                        </th>
                                                        <td style="text-align: left">
                                                            <asp:LinkButton ID="lnk" ClientIDMode="AutoID" CommandName="Pages" runat="server" Style="text-decoration: none">
													<font>Click Here</font></asp:LinkButton>
                                                        </td>
                                                        <th style="text-align: right">Total Time on site :
                                                        </th>
                                                        <td style="text-align: left">
                                                            <asp:Label ID="lblTotalTime" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcTotalTime") %>'>
                                                            </asp:Label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ItemTemplate>
                                            <SelectedItemTemplate>
                                                <table class="table table-bordered table-striped">
                                                    <tr>
                                                        <td colspan="6" class="text-right">
                                                            <asp:LinkButton ID="lkbBack" ClientIDMode="AutoID" runat="server" CssClass="btn btn-primary" OnClick="lkbBack_Click"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;Back to main list</asp:LinkButton>
                                                        </td>
                                                    </tr>
                                                    <tr class="hs">
                                                        <th style="text-align: right">
                                                            <asp:Label ID="lblID" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.numTrackingID") %>'
                                                                Visible="false">
                                                            </asp:Label>
                                                            Date Visitied:
                                                        </th>
                                                        <td style="text-align: left">
                                                            <asp:Label ID="lblCreated" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.dtCreated") %>'>
                                                            </asp:Label>
                                                        </td>
                                                        <th style="text-align: right">Pages Viewed :
                                                        </th>
                                                        <td style="text-align: left">
                                                            <asp:Label ID="Label4" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.NoOfTimes") %>'></asp:Label>
                                                        </td>
                                                        <th style="text-align: right">Total Time on site :
                                                        </th>
                                                        <td style="text-align: left">
                                                            <asp:Label ID="lblTotalTime" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.vcTotalTime") %>'>
                                                            </asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="6">
                                                            <asp:DataGrid ID="dgWebAnlys" runat="server" AllowSorting="True" CssClass="table table-bordered table-striped" Width="100%"
                                                                AutoGenerateColumns="False" UseAccessibleHeader="true">
                                                                <Columns>
                                                                    <asp:BoundColumn Visible="False" DataField="numTracVisitorsHDRID"></asp:BoundColumn>
                                                                    <asp:BoundColumn HeaderText="Page" DataField="vcPageName"></asp:BoundColumn>
                                                                    <asp:BoundColumn HeaderText="Time Spent on the Page" DataField="vcElapsedTime"></asp:BoundColumn>
                                                                    <asp:BoundColumn HeaderText="Time Visited" DataField="TimeVisited"></asp:BoundColumn>
                                                                </Columns>
                                                            </asp:DataGrid>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </SelectedItemTemplate>
                                        </asp:DataList>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Correspondence" runat="server">
            <div class="table-responsive">
                <asp:Table ID="Table4" CellPadding="0" CellSpacing="0" runat="server" Width="100%"
                    CssClass="aspTable" GridLines="None" Height="300">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <uc1:frmCorrespondence ID="Correspondence1" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Assets" runat="server">
            <div class="table-responsive">
                <asp:Table ID="Table1" CellPadding="0" CellSpacing="0" runat="server" Width="100%"
                    GridLines="None" Height="300">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <uc1:AssetList ID="AssetList" runat="server" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_Associations" runat="server">
            <div class="table-responsive">
                <asp:Table ID="tbl5" runat="server" Width="100%" CellSpacing="0" CellPadding="0"
                    GridLines="None" Height="380">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <table width="100%">
                                <tr>
                                    <td>
                                        <iframe id="ifAssociation" frameborder="0" width="100%" scrolling="auto" 
                                            height="380"></iframe>
                                    </td>
                                </tr>
                            </table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </telerik:RadPageView>
        <telerik:RadPageView ID="radPageView_SurveyHistory" runat="server">
            <div class="table-responsive">
                <asp:Table ID="Table3" runat="server" CellPadding="0" CellSpacing="0" GridLines="None"
                    Height="300" Width="100%">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right" VerticalAlign="Top">
                            <asp:Literal ID="litClientMessageSurveyHistory" runat="server"></asp:Literal>
                            <table class="table table-responsive" style="width: 15%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblNextSurveyHistory" runat="server" CssClass="Text_bold">Next:</asp:Label>
                                    </td>
                                    <td class="normal1">
                                        <asp:LinkButton ID="lnk2SurveyHistory" runat="server" CausesValidation="False">2</asp:LinkButton>
                                    </td>
                                    <td class="normal1">
                                        <asp:LinkButton ID="lnk3SurveyHistory" runat="server" CausesValidation="False">3</asp:LinkButton>
                                    </td>
                                    <td class="normal1">
                                        <asp:LinkButton ID="lnk4SurveyHistory" runat="server" CausesValidation="False">4</asp:LinkButton>
                                    </td>
                                    <td class="normal1">
                                        <asp:LinkButton ID="lnk5SurveyHistory" runat="server" CausesValidation="False">5</asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkFirstSurveyHistory" runat="server" CausesValidation="False">
                                                    <div class="LinkArrow">
                                                        &lt;&lt;</div>
                                        </asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkPreviousSurveyHistory" runat="server" CausesValidation="False">
                                                    <div class="LinkArrow">
                                                        &lt;</div>
                                        </asp:LinkButton>
                                    </td>
                                    <td class="normal1">
                                        <asp:Label ID="lblPageSurveyHistory" runat="server">Page</asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtCurrentPageSurveyHistory" runat="server" AutoPostBack="true"
                                            CssClass="signup" MaxLength="5" Text="1" Width="28px">
                                        </asp:TextBox>
                                    </td>
                                    <td class="normal1">
                                        <asp:Label ID="lblOfSurveyHistory" runat="server">of</asp:Label>
                                    </td>
                                    <td class="normal1">
                                        <asp:Label ID="lblTotalSurveyHistory" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkNextSurveyHistory" runat="server" CausesValidation="False"
                                            CssClass="LinkArrow">
                                                    <div class="LinkArrow">
                                                        &gt;</div>
                                        </asp:LinkButton>
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkLastSurveyHistory" runat="server" CausesValidation="False">
                                                    <div class="LinkArrow">
                                                        &gt;&gt;</div>
                                        </asp:LinkButton>
                                    </td>
                                </tr>
                            </table>
                            <asp:DataGrid ID="dgSurvey" runat="server" AlternatingItemStyle-CssClass="ais" AutoGenerateColumns="False"
                                BorderWidth="1px" CellPadding="2" CellSpacing="0" CssClass="dg" DataKeyField="numSurID"
                                HeaderStyle-CssClass="hs" ItemStyle-CssClass="is" ShowHeader="true" Width="100%">
                                <Columns>
                                    <asp:BoundColumn DataField="numSurID" HeaderText="Survey ID" ItemStyle-Width="70"
                                        Visible="true"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="numRespondantID" HeaderText="Respondent ID" ItemStyle-Width="70"
                                        Visible="False"></asp:BoundColumn>
                                    <%--<asp:BoundColumn Visible="true" HeaderText="Date Created" DataField="dateCreatedOn" ItemStyle-Width="100"></asp:BoundColumn>--%>
                                    <asp:TemplateColumn HeaderText="&lt;font &gt;Date Created&lt;/font&gt;" SortExpression="dateCreatedOn">
                                        <ItemTemplate>
                                            <%#ReturnName(DataBinder.Eval(Container.DataItem, "dateCreatedOn"))%>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:BoundColumn DataField="vcSurName" HeaderText="Survey Name" ItemStyle-HorizontalAlign="Left"
                                        ItemStyle-Width="300" Visible="true"></asp:BoundColumn>
                                    <asp:HyperLinkColumn DataNavigateUrlField="numContactSurResponseLink" DataNavigateUrlFormatString="javascript:EditSurveyResult({0});"
                                        HeaderText="Survey Results" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="100"
                                        Text="See Results"></asp:HyperLinkColumn>
                                    <asp:BoundColumn DataField="numSurRating" HeaderText="Survey Rating" ItemStyle-Width="100"
                                        SortExpression="numSurRating" Visible="true"></asp:BoundColumn>
                                    <asp:TemplateColumn ItemStyle-HorizontalAlign="Center" ItemStyle-Width="25">
                                        <ItemTemplate>
                                            <asp:Button ID="btnSurveyHistoryDelete" runat="server" CausesValidation="False" CommandName="Delete"
                                                CssClass="Delete" OnClick="btnSurveyHistoryDeleteAction_Command" Text="X" Visible="true" />
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </div>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
    <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtEmailTotalPage" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:TextBox ID="txtEmailTotalRecords" runat="server" Style="display: none">
    </asp:TextBox>
    <asp:TextBox ID="txtCorrTotalPage" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtCorrTotalRecords" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtContactType" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:TextBox ID="txtContId" Style="display: none" runat="server">
    </asp:TextBox>
    <asp:HiddenField ID="hdnDivID" runat="server" />
    <asp:HiddenField ID="hfOwner" runat="server" />
    <asp:HiddenField ID="hfTerritory" runat="server" />
    <asp:HiddenField ID="hdncustomerlink" runat="server" />
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DetailPageTitle" runat="server"
    ClientIDMode="Static">
    Lead Details&nbsp;&nbsp;<asp:Label ID="lblOrganizationID" runat="server" ForeColor="Gray" />&nbsp;<a href="#" onclick="return OpenHelpPopUp('relationshipsdetail')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="UtilityLinksPanel" runat="server">
    <div class="btn btn-default btn-sm" runat="server" id="btnLayout">
        <span><i class="fa fa-columns"></i>&nbsp;&nbsp;Layout</span>
    </div>
    <asp:LinkButton runat="server" ID="lbtnPromote" CssClass="btn btn-sm btn-primary"><i class="fa fa-thumbs-o-up"></i>&nbsp;&nbsp;Promote to Prospect
    </asp:LinkButton>
    <telerik:RadWindow RenderMode="Lightweight" runat="server" ID="RadWinDocumentFiles" Title="Documents / Files" RestrictionZoneID="ContentTemplateZone" Modal="true" Behaviors="Resize,Close,Move" Width="800" Height="530">
        <ContentTemplate>
            <asp:UpdatePanel ID="Updatepanel3" runat="server" UpdateMode="Conditional" class="row" style="margin-right: 0px; margin-left: 0px">
                <ContentTemplate>
                    <br />
                    <%--<div class="row">--%>
                    <div class="col-xs-12 padbottom10">
                        <div class="pull-right">
                            <input type="button" id="btnCloseDocumentFiles" onclick="return CloseDocumentFiles('C');" class="btn btn-primary" style="color: white" value="Close" />
                        </div>
                    </div>
                    <%-- </div>
                    <div class="row">--%>
                    <div class="col-sm-6">
                        <h4>
                            <img src="../images/icons/Attach.png" height="24">
                            Upload a Document from your Desktop
                        </h4>
                        <hr style="color: #dcdcdc" />
                        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" LoadingPanelID="LoadingPanel1" ClientEvents-OnRequestStart="requestStart">
                            <div class="form-group">
                                <div class="custom-file">
                                    <input class="custom-file-input" id="fileupload" type="file" name="fileupload" runat="server">
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Section</label>
                                <asp:DropDownList ID="ddlSectionFile" runat="server" CssClass="form-control"></asp:DropDownList>
                            </div>
                            <asp:Button ID="btnAttach" OnClick="btnAttach_Click" runat="server" Text="Attach & Close" CssClass="btn btn-primary"></asp:Button>
                            <asp:Button ID="btnLinkClose" OnClick="btnLinkClose_Click" runat="server" CssClass="hidden"></asp:Button>
                        </telerik:RadAjaxPanel>
                        <telerik:RadAjaxLoadingPanel ID="LoadingPanel1" runat="server" InitialDelayTime="0" Skin="Default">
                        </telerik:RadAjaxLoadingPanel>
                    </div>

                    <div class="col-sm-6" id="divLinkUpload">
                        <h4>
                            <img src="../images/icons/Link.png">
                            Link to a Document</h4>
                        <hr style="color: #dcdcdc" />
                        <div class="form-group">
                            <label>Link Name</label>
                            <asp:TextBox runat="server" ID="txtLinkName" EmptyMessage="Link Name" CssClass="form-control"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Link URL</label>
                            <asp:TextBox runat="server" ID="txtLinkURL" EmptyMessage="Paste link here" TextMode="MultiLine" CssClass="form-control" Rows="3"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Section</label>
                            <asp:DropDownList ID="ddlSectionLink" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                        <asp:Button ID="btnLink" runat="server" OnClientClick="return CloseDocumentFiles('L');" Text="Link & Close" CssClass="btn btn-primary"></asp:Button>
                        <asp:Label ID="Label2" Text="[?]" CssClass="tip" runat="server" ToolTip="Linking to a file on Google Drive or any other file repository is a good way to preserve your BizAutomation storage space. To avoid login credentials when clicking the  link, just make it public (e.g. On Google Drive you have to make sure that �Link Sharing� is set to �Public on the web�)." />
                    </div>
                    <%--</div>--%>
                </ContentTemplate>
            </asp:UpdatePanel>
        </ContentTemplate>
    </telerik:RadWindow>
</asp:Content>
