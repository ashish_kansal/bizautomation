﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmOpenEmail.aspx.vb"
    Inherits=".frmOpenEmail" MasterPageFile="~/common/Popup.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Open Action Item</title>
    <script src="../JavaScript/jquery.min.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        function openEmail(a) {
            window.open("../outlook/frmMailDtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=inbox&numEmailHstrID=" + a, 'Message', 'titlebar=no,top=100,left=100,width=850,height=550,scrollbars=yes,resizable=yes');
            window.close();
            return false;
        }

        $(document).ready(function () {
            var chkBox = $("input[id$='chkAll']");
            chkBox.click(
          function () {
              $("#Table2 INPUT[type='checkbox']").prop('checked', chkBox.is(':checked'));
          });
            // To deselect CheckAll when a GridView CheckBox        // is unchecked
            $("#Table2 INPUT[type='checkbox']").click(
        function (e) {
            if (!$(this)[0].checked) {
                chkBox.prop("checked", false);
            }
        });
        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="right-input">
        <div class="input-part">
            <table id="Table3" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td align="right">
                        <asp:Button ID="btnRead" CssClass="button" Text="Mark as read & Clear from list"
                            runat="server"></asp:Button>&nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblTab" runat="server" Text="Open Email"></asp:Label>&nbsp;&nbsp;&nbsp;
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
    <asp:Table ID="Table2" runat="server" GridLines="None" BorderColor="black" Width="500px"
        BorderWidth="1" CssClass="aspTable" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:Repeater ID="rptCorr" runat="server">
                    <HeaderTemplate>
                        <table cellspacing="0" class="dg" width="100%">
                            <tr class="hs" align="center">
                                <td>
                                    Subject
                                </td>
                                <td>
                                    Received On
                                </td>
                                <td>
                                    <asp:CheckBox ID="chkAll" runat="server" />
                                </td>
                            </tr>
                    </HeaderTemplate>
                    <AlternatingItemTemplate>
                        <tr id="Tr1" align="center" class="ais" runat="server">
                            <td align="center">
                                <a class="hyperlink" onclick="javascript:openEmail('<%#Container.DataItem("numEmailHstrId")%>')">
                                    <%# Container.DataItem("Subject")%></a>
                                <asp:HiddenField ID="hdfId" runat="server" Value='<%#Container.DataItem("numEmailHstrId")%>' />
                            </td>
                            <td align="center">
                                <%#Container.DataItem("dtReceivedOn")%>
                            </td>
                            <td align="center">
                                <asp:CheckBox ID="chkSelect" runat="server" />
                            </td>
                        </tr>
                        <tr id="Tr2" align="center" class="ais" runat="server">
                            <td align="left" colspan="3" style="color: Gray">
                                <%# Container.DataItem("vcBody")%>
                            </td>
                        </tr>
                    </AlternatingItemTemplate>
                    <ItemTemplate>
                        <tr id="Tr3" class="is" runat="server">
                            <td align="center">
                                <a class="hyperlink" onclick="javascript:openEmail('<%#Container.DataItem("numEmailHstrId")%>')">
                                    <%# Container.DataItem("Subject")%></a>
                                <asp:HiddenField ID="hdfId" runat="server" Value='<%#Container.DataItem("numEmailHstrId")%>' />
                            </td>
                            <td align="center">
                                <%#Container.DataItem("dtReceivedOn")%>
                            </td>
                            <td align="center">
                                <asp:CheckBox ID="chkSelect" runat="server" />
                            </td>
                        </tr>
                        <tr id="Tr4" class="is" runat="server">
                            <td align="left" colspan="3" style="color: Gray">
                                <%# Container.DataItem("vcBody")%>
                            </td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
