﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="frmActionItemList.aspx.vb"
    Inherits=".frmActionItemList" MasterPageFile="~/common/Popup.Master" %>

<%@ Register Src="../common/frmCorrespondence.ascx" TagName="frmCorrespondence" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">		
    <title>Open Action Item</title>
    <script language="javascript" type="text/javascript">
        function openActionItem(a, b, c, d, e, f) {
            if (e == 'Email') {
                window.open("../outlook/frmMailDtl.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&numEmailHstrID=" + a, '', 'titlebar=no,top=100,left=250,width=850,height=550,scrollbars=yes,resizable=yes');
                return false;
            }
            else {
                opener.location.href = "../admin/ActionItemDetailsOld.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospects&CommId=" + a + "&CaseId=" + b + "&CaseTimeId=" + c + "&CaseExpId=" + d;
                window.close();
                return false;
            }

        }
    </script>	
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
   
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="PageTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lblTab" runat="server" Text="Open Action Item"></asp:Label>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="Content" runat="server" ClientIDMode="Static">
 
    <asp:Table ID="Table2" runat="server" GridLines="None" BorderColor="black" Width="100%"
        BorderWidth="1" CssClass="aspTable" CellSpacing="0" CellPadding="0">
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <uc1:frmCorrespondence ID="Correspondence1" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>