' Created By Anoop Jayaraj
Imports System.IO
Imports System.Text
Imports System.Web.Services
Imports BACRM.BusinessLogic.Account
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Contacts
Imports BACRM.BusinessLogic.Documents
Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Survey

Namespace BACRM.UserInterface.Leads
    Public Class frmLeads
        Inherits BACRMPage
        Shared selectContactId As Integer = 0
        Dim objContacts As CContacts
        Dim objLeads As New LeadsIP
        Dim objItems As CItems
        Dim ObjCus As CustomFields
        Dim objAccounts As CAccounts

        Dim lngDivID As Long
        Dim m_aryRightsForEmails(), m_aryRightsForActItem(), m_aryRightsForShowAOIs(), m_aryRightsForPromote(), m_aryRightsForCustFlds(), m_aryRightsForTransferOwner(), m_aryRightsForCorr(), m_aryRightsForViewLayoutButton() As Integer
        Dim strColumn As String
        Dim SI As Integer = 0
        Dim SI1 As Integer = 0
        Dim SI2 As Integer = 0
        Dim frm As String = ""
        Dim frm1 As String = ""
        Dim frm2 As String = ""
        Dim boolIntermediatoryPage As Boolean = False
        Dim dtCustomFieldTable As DataTable
        Dim dtTableInfo As DataTable
        Dim objPageControls As New PageControls
        Dim lngCntID As Long

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                If Not IsPostBack Then
                    DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                    DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                    If Session("EnableIntMedPage") = 1 Then
                        ViewState("IntermediatoryPage") = True
                    Else
                        ViewState("IntermediatoryPage") = False
                    End If

                    m_aryRightsForViewLayoutButton = GetUserRightsForPage_Other(2, 11)
                    If m_aryRightsForViewLayoutButton(RIGHTSTYPE.VIEW) = 0 Then
                        btnLayout.Visible = False
                    End If
                End If
                'Added this code to Hide the Asset Tab.. 18 Feb 10 - Sojan
                'radOppTab.Tabs(4).Visible = False

                boolIntermediatoryPage = ViewState("IntermediatoryPage")
                ControlSettings()
                If GetQueryStringVal("SI") <> "" Then
                    SI = GetQueryStringVal("SI")
                End If
                If GetQueryStringVal("SI1") <> "" Then
                    SI1 = GetQueryStringVal("SI1")
                Else : SI1 = 0
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    SI2 = GetQueryStringVal("SI2")
                Else : SI2 = 0
                End If
                If GetQueryStringVal("frm") <> "" Then
                    frm = GetQueryStringVal("frm")
                Else : frm = ""
                End If
                If GetQueryStringVal("frm1") <> "" Then
                    frm1 = GetQueryStringVal("frm1")
                Else : frm1 = ""
                End If
                If GetQueryStringVal("SI2") <> "" Then
                    frm2 = GetQueryStringVal("frm2")
                Else : frm2 = ""
                End If
                If GetQueryStringVal("frm") = "SurveyRespondents" Or GetQueryStringVal("frm") = "DuplicateSearch" Then    'This special section is introduced because the screen has to open in a new window without any menus
                    'Added by Debasish Nag on 6th Jan 2006
                    Dim objForm As Object                                   'The generic object
                    objForm = Page.FindControl("webmenu1")                  'Get a holder to the web control
                    objForm.visible = False                                 'Hide the web control
                    btnCancel.Attributes.Add("onclick", "javascript: window.close();return false;") 'Write code to close the window
                End If

                Correspondence1.lngRecordID = lngDivID
                Correspondence1.Mode = 7

                GetUserRightsForPage(2, 3)
                If lngDivID = 0 Then
                    lngDivID = GetQueryStringVal("DivID")
                    Session("DivisionID") = lngDivID
                End If

                If Not IsPostBack Then
                    lblOrganizationID.Text = "(ID:" & lngDivID.ToString() & ")"

                    If radOppTab.Tabs.Count > SI Then radOppTab.SelectedIndex = SI
                    objCommon.DivisionID = lngDivID
                    objCommon.charModule = "D"
                    objCommon.GetCompanySpecificValues1()
                    txtContId.Text = CStr(objCommon.ContactID)

                    AddToRecentlyViewed(RecetlyViewdRecordType.Account_Pospect_Lead, lngDivID)
                    'Added by Debasish for displayign the Association information

                    ' txtCurrrentPage.Text = 1
                    ''''sub-tab management  - added on 16/09/2009 by chintan
                    objCommon.GetAuthorizedSubTabs(radOppTab, Session("DomainID"), 14, Session("UserGroupID"))
                    '''''
                    If m_aryRightsForPage(RIGHTSTYPE.UPDATE) = 0 Then
                        btnSave.Visible = False
                        btnSaveClose.Visible = False
                    End If
                    If m_aryRightsForPage(RIGHTSTYPE.DELETE) = 0 Then btnActDelete.Visible = False

                    PersistTable.Load(boolOnlyURL:=True)
                    If PersistTable.Count > 0 Then
                        radOppTab.SelectedIndex = CCommon.ToLong(PersistTable(PersistKey.SelectedTab))
                    End If

                    m_aryRightsForPromote = GetUserRightsForPage_Other(2, 7)
                    If m_aryRightsForPromote(RIGHTSTYPE.VIEW) = 0 Then lbtnPromote.Visible = False

                    m_aryRightsForActItem = GetUserRightsForPage_Other(2, 10)
                    If m_aryRightsForActItem(RIGHTSTYPE.VIEW) = 0 Then btnActionItem.Visible = False
                    LoadLeadDtls()
                    sb_ShowAOIs(txtContId.Text)
                    LoadTabDetails()
                    LoadAssociationInformation()
                    hplTransfer.Attributes.Add("onclick", "return OpenTransfer('" & "../admin/transferrecord.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospects&rtyWR=" & lngDivID & "')")
                    btnSave.Attributes.Add("onclick", "return Save()")
                    btnSaveClose.Attributes.Add("onclick", "return Save()")
                    btnLayout.Attributes.Add("onclick", "return ShowLayout('L','" & lngDivID & "','" & IIf(CCommon.ToLong(objLeads.CompanyType) > 0, objLeads.CompanyType, 1) & "','34');")

                    If lngDivID = Session("UserDivisionID") Then
                        btnActDelete.Attributes.Add("onclick", "return DeleteMessage()")
                    Else : btnActDelete.Attributes.Add("onclick", "return DeleteRecord()")
                    End If


                    radOppTab.MultiPage.FindPageViewByID(radOppTab.SelectedTab.PageViewID).Selected = True
                End If
                If IsPostBack Then
                    LoadLeadDtls()
                End If

                m_aryRightsForCustFlds = GetUserRightsForPage_Other(2, 8)
                If m_aryRightsForCustFlds(RIGHTSTYPE.VIEW) <> 0 Then DisplayDynamicFlds()
                If (hplTransfer.Visible = False) Then
                    hplTransferNonVis.Visible = True
                End If

                'IMPORTANT - KEEP IT BELOW sb_CompanyInfo call in Page_Load method
                If Not Page.IsPostBack Then
                    m_aryRightsForTransferOwner = GetUserRightsForPage_Other(2, 9)

                    If m_aryRightsForTransferOwner(RIGHTSTYPE.VIEW) = 0 Then
                        hplTransfer.Visible = False
                        hplTransferNonVis.Visible = True
                    ElseIf m_aryRightsForTransferOwner(RIGHTSTYPE.VIEW) = 1 Then
                        Try
                            If CCommon.ToLong(hfOwner.Value) <> Session("UserContactID") Then
                                hplTransfer.Visible = False
                                hplTransferNonVis.Visible = True
                            End If
                        Catch ex As Exception
                        End Try
                    ElseIf m_aryRightsForTransferOwner(RIGHTSTYPE.VIEW) = 2 Then
                        Dim i As Integer
                        Dim dtTerritory As DataTable
                        dtTerritory = Session("UserTerritory")
                        If CCommon.ToLong(hfTerritory.Value) = 0 Then
                            hplTransfer.Visible = False
                            hplTransferNonVis.Visible = True
                        Else
                            Dim chkDelete As Boolean = False
                            For i = 0 To dtTerritory.Rows.Count - 1
                                If CCommon.ToLong(hfTerritory.Value) = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                    chkDelete = True
                                End If
                            Next
                            If chkDelete = False Then
                                hplTransfer.Visible = False
                                hplTransferNonVis.Visible = True
                            End If
                        End If
                    End If
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ShowDocuments(ByRef tblDocuments As HtmlTable, ByVal dtDocLinkName As DataTable, ByVal dtDisplayFields As DataTable, ByVal intSection As Integer)
            Try
                tblDocuments.Controls.Clear()
                Dim countId = 0
                Dim tr As HtmlTableRow
                For Each drDocument In dtDocLinkName.Rows
                    If (intSection = 0 AndAlso (CCommon.ToLong(drDocument("numFormFieldGroupId")) = 0 Or dtDisplayFields.Select("numFormFieldGroupId=" & CCommon.ToLong(drDocument("numFormFieldGroupId"))).Length = 0)) Or
                        (intSection <> 0 AndAlso CCommon.ToLong(drDocument("numFormFieldGroupId")) = intSection) Then
                        If countId = 0 Then
                            tr = New HtmlTableRow
                        End If

                        If CCommon.ToString(drDocument("cUrlType")) = "L" Then
                            Dim strOriginalFileName = CCommon.ToString(drDocument("VcDocName")) & CCommon.ToString(drDocument("vcFileType"))
                            Dim strDocName = CCommon.ToString(drDocument("VcFileName"))
                            Dim fileID = CCommon.ToString(drDocument("numGenericDocID"))
                            Dim strFileLogicalPath = String.Empty
                            If File.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName) Then
                                Dim f As FileInfo = New FileInfo(CCommon.GetDocumentPhysicalPath(Session("DomainID")) & strDocName)
                                Dim strFileSize As String = f.Length.ToString
                                strFileLogicalPath = CCommon.GetDocumentPath(Session("DomainID")) & HttpUtility.UrlEncode(strDocName).Replace("+", " ")
                            End If

                            Dim docLink As New HyperLink

                            Dim imgDelete As New ImageButton
                            imgDelete.ImageUrl = "../images/Delete24.png"
                            imgDelete.Height = 13
                            imgDelete.ToolTip = "Delete"
                            imgDelete.ID = "imgDelete" + CCommon.ToString(drDocument("numGenericDocID"))
                            imgDelete.Attributes.Add("onclick", "DeleteDocumentOrLink(" & fileID & ");return false;")

                            Dim Space As New LiteralControl
                            Space.Text = "&nbsp;"

                            docLink.Text = strOriginalFileName
                            docLink.NavigateUrl = strFileLogicalPath
                            docLink.ID = "hpl" + CCommon.ToString(drDocument("numGenericDocID"))
                            docLink.Target = "_blank"
                            Dim td As New HtmlTableCell
                            td.Style.Add("border-bottom", "0px !important")
                            td.Style.Add("padding-left", "5px")
                            td.Style.Add("padding-right", "5px")
                            td.Controls.Add(docLink)
                            td.Controls.Add(Space)
                            td.Controls.Add(imgDelete)
                            tr.Controls.Add(td)
                        ElseIf CCommon.ToString(drDocument("cUrlType")) = "U" Then
                            Dim uriBuilder As UriBuilder = New UriBuilder(CCommon.ToString(drDocument("VcFileName")))

                            Dim link As New HyperLink
                            Dim imgLinkDelete As New ImageButton
                            imgLinkDelete.ImageUrl = "../images/Delete24.png"
                            imgLinkDelete.Height = 13
                            imgLinkDelete.ToolTip = "Delete"
                            imgLinkDelete.ID = "imgLinkDelete" + CCommon.ToString(drDocument("numGenericDocID"))
                            imgLinkDelete.Attributes.Add("onclick", "DeleteDocumentOrLink(" & CCommon.ToString(drDocument("numGenericDocID")) & ");return false;")

                            link.Text = CCommon.ToString(drDocument("VcDocName"))
                            link.NavigateUrl = uriBuilder.Uri.AbsoluteUri
                            link.ID = "hplLink" + CCommon.ToString(drDocument("numGenericDocID"))
                            link.Target = "_blank"

                            Dim td As New HtmlTableCell
                            td.Style.Add("border-bottom", "0px !important")
                            td.Style.Add("padding-left", "5px")
                            td.Style.Add("padding-right", "5px")
                            td.Controls.Add(link)
                            td.Controls.Add(imgLinkDelete)
                            tr.Controls.Add(td)
                        End If

                        countId = countId + 1

                        If countId = 3 Then
                            countId = 0
                            tblDocuments.Controls.Add(tr)
                        End If
                    End If
                Next

                If countId <> 0 Then
                    tblDocuments.Controls.Add(tr)
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Protected Sub btnDeleteDocOrLink_Click(sender As Object, e As EventArgs)
            Dim objDocuments As New DocumentList()
            objDocuments.GenDocID = CCommon.ToLong(hdnFileId.Value)
            objDocuments.DeleteDocumentOrLink()
            LoadControls()
        End Sub

        Sub LoadTabDetails()
            Try
                'Added this code to Hide the Asset Tab.. 18 Feb 10 - Sojan
                'radOppTab.Tabs(4).Visible = False
                If radOppTab.SelectedIndex = 2 Then
                    objLeads.DivisionID = lngDivID
                    objLeads.DomainID = Session("DomainID")
                    Dim ds As DataSet
                    ds = objLeads.GetWebAnlysDtl
                    If ds.Tables(0).Rows.Count > 0 Then
                        lblFirstDateVisited.Text = CCommon.ToString(ds.Tables(0).Rows(0).Item("StartDate"))
                        lblLastDateVisited.Text = CCommon.ToString(ds.Tables(0).Rows(0).Item("EndDate"))
                        lblReferringPage.Text = CCommon.ToString(ds.Tables(0).Rows(0).Item("vcOrginalRef"))
                        lblLastPageVisited.Text = CCommon.ToString(ds.Tables(0).Rows(0).Item("vcLastPageVisited"))
                        lblIpAddress.Text = CCommon.ToString(ds.Tables(0).Rows(0).Item("vcUserHostAddress"))
                        lblIPLocation.Text = CCommon.ToString(ds.Tables(0).Rows(0).Item("vcLocation"))
                    Else
                        lblFirstDateVisited.Text = "-"
                        lblLastDateVisited.Text = "-"
                        lblReferringPage.Text = "-"
                        lblLastPageVisited.Text = "-"
                        lblIpAddress.Text = "-"
                        lblIPLocation.Text = "-"
                    End If
                    dlWebAnlys.DataSource = ds.Tables(1)
                    dlWebAnlys.DataBind()
                ElseIf radOppTab.SelectedIndex = 3 Then
                    Correspondence1.lngRecordID = lngDivID
                    Correspondence1.Mode = 7
                    Correspondence1.CorrespondenceModule = 2
                    Correspondence1.getCorrespondance()
                ElseIf radOppTab.SelectedIndex = 4 Then
                    'LoadAssets()
                    AssetList.DivisionID = lngDivID
                    AssetList.FormName = "frmAccounts"
                    AssetList.LoadAssets()
                ElseIf radOppTab.SelectedIndex = 5 Then
                    If Not ClientScript.IsStartupScriptRegistered("loadframe") Then
                        ClientScript.RegisterStartupScript(Me.GetType, "loadframe", "document.getElementById('ifAssociation').src='../admin/frmcomAssociationTo.aspx?numDivisionID=" + lngDivID.ToString + "';", True)
                    End If
                ElseIf radOppTab.SelectedIndex = 6 Then
                    DisplaySurveyHistoryLists()
                End If

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Sub LoadAssets()
        '    Try
        '        If objItems Is Nothing Then objItems = New CItems
        '        Dim dtItems As DataTable
        '        objItems.DivisionID = lngDivID
        '        objItems.DomainID = Session("DomainId")
        '        dtItems = objItems.getCompanyAssets()
        '        uwItem.DataSource = dtItems
        '        uwItem.DataBind()
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub



        Sub LoadLeadDtls(Optional ByVal tempEvntId As Long = 0)
            Try
                objLeads.DivisionID = lngDivID
                objLeads.ContactID = txtContId.Text
                objLeads.DomainID = Session("DomainID")
                objLeads.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                objLeads.GetLeadDetails()


                If Not IsPostBack Then

                    Dim dtLeaddtls As DataTable
                    objLeads.ContactID = txtContId.Text
                    objLeads.DomainID = Session("DomainID")
                    objLeads.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                    dtLeaddtls = objLeads.GetLeadInfo
                    If dtLeaddtls.Rows.Count > 0 Then
                        'lblCustID.Text = lngDivID.ToString & " (" & dtLeaddtls.Rows(0).Item("vcCompanyName") & ")"
                        lblCustID.Text = "<b>Customer :&nbsp;&nbsp;</b>" & dtLeaddtls.Rows(0).Item("vcCompanyName")
                        lblRecordOwner.Text = dtLeaddtls.Rows(0).Item("vcRecordOwner")
                        lblCreatedBy.Text = dtLeaddtls.Rows(0).Item("vcCreatedBy")
                        lblLastModifiedBy.Text = dtLeaddtls.Rows(0).Item("vcModifiedBy")
                        hfOwner.Value = CCommon.ToLong(dtLeaddtls.Rows(0).Item("numRecOwner"))
                        hfTerritory.Value = CCommon.ToLong(dtLeaddtls.Rows(0).Item("numTerID"))

                        If CCommon.ToLong(dtLeaddtls.Rows(0).Item("numAssignedTo")) <> CCommon.ToLong(Session("UserContactID")) AndAlso CCommon.ToLong(Session("UserDivisionID")) <> lngDivID Then
                            GetUserRightsForRecord(MODULEID.Leads, 3, CCommon.ToLong(hfOwner.Value), CCommon.ToLong(hfTerritory.Value))
                        End If

                        Dim dtTab As DataTable
                        dtTab = Session("DefaultTab")
                        Dim grpid As Integer = IIf(IsDBNull(dtLeaddtls.Rows(0).Item("numgrpid")), "0", dtLeaddtls.Rows(0).Item("numgrpid"))
                        If dtTab.Rows.Count > 0 Then
                            If grpid = 5 Then
                                radOppTab.Tabs(0).Text = "My " & IIf(IsDBNull(dtTab.Rows(0).Item("vcLead")), "Lead Details", dtTab.Rows(0).Item("vcLead").ToString & " Details")
                            ElseIf grpid = 1 Then
                                radOppTab.Tabs(0).Text = "Web " & IIf(IsDBNull(dtTab.Rows(0).Item("vcLead")), "Lead Details", dtTab.Rows(0).Item("vcLead").ToString & " Details")
                            ElseIf grpid = 2 Then
                                radOppTab.Tabs(0).Text = "Public" & IIf(IsDBNull(dtTab.Rows(0).Item("vcLead")), "Lead Details", dtTab.Rows(0).Item("vcLead").ToString & " Details")
                            ElseIf grpid = 0 Then
                                radOppTab.Tabs(0).Text = "Lead Details"
                            End If
                        Else
                            radOppTab.Tabs(0).Text = "Lead Details"
                        End If
                        'For Each Item As ListItem In ddlGroup.Items
                        '    If Item.Text = "My Leads" Then
                        '        Item.Text = "My " & dtTab.Rows(0).Item("vcLead") & "s"
                        '    ElseIf Item.Text = "PublicLeads" Then
                        '        Item.Text = "Public " & dtTab.Rows(0).Item("vcLead") & "s"
                        '    ElseIf Item.Text = "WebLeads" Then
                        '        Item.Text = "Web " & dtTab.Rows(0).Item("vcLead") & "s"
                        '    End If
                        'Next

                        'Commented by Neelam - Obsolete functionality
                        'hplDocumentCount.Attributes.Add("onclick", "return OpenDocuments(" & lngDivID & ")")
                        'hplDocumentCount.Text = "(" & CCommon.ToLong(dtLeaddtls.Rows(0)("DocumentCount")) & ")"
                        'imgOpenDocument.Attributes.Add("onclick", "return OpenDocuments(" & lngDivID & ")")
                        imgOpenDocument.Attributes.Add("onclick", "OpenDocumentFiles(" & lngDivID.ToString() & ");return false;")
                        hplOpenDocument.Attributes.Add("onclick", "OpenDocumentFiles(" & lngDivID.ToString() & ");return false;")

                        If Not String.IsNullOrEmpty(CCommon.ToString(dtLeaddtls.Rows(0)("vcBuiltWithJson")).Trim()) Then
                            Dim objBuiltWith As New BuiltWith
                            objBuiltWith = objBuiltWith.FromJson(CCommon.ToString(dtLeaddtls.Rows(0)("vcBuiltWithJson")).Trim())

                            If Not objBuiltWith Is Nothing Then
                                If Not objBuiltWith.Results Is Nothing AndAlso objBuiltWith.Results.Count > 0 Then
                                    If Not objBuiltWith.Results(0).Meta Is Nothing Then
                                        lblBWCompanyName.Text = CCommon.ToString(objBuiltWith.Results(0).Meta.CompanyName)
                                        hplBWLinkedin.HRef = HttpUtility.UrlEncode("https://www.linkedin.com/search/results/people/?company=" & CCommon.ToString(objBuiltWith.Results(0).Meta.CompanyName))
                                        lblBWVertical.Text = objBuiltWith.Results(0).Meta.Vertical

                                        divBWAddress.InnerHtml = objBuiltWith.Results(0).Meta.City & ", <br/>" & objBuiltWith.Results(0).Meta.State & "<br/>" & objBuiltWith.Results(0).Meta.Country & " - " & objBuiltWith.Results(0).Meta.Postcode

                                        If Not objBuiltWith.Results(0).Meta.Telephones Is Nothing AndAlso objBuiltWith.Results(0).Meta.Telephones.Length > 0 Then
                                            divBWTelephones.InnerHtml = String.Join("<br/>", objBuiltWith.Results(0).Meta.Telephones)
                                        End If

                                        If Not objBuiltWith.Results(0).Meta.Names Is Nothing AndAlso objBuiltWith.Results(0).Meta.Names.Length > 0 Then
                                            Dim html As String = "<table class='table table-bordered table-striped'><thead><tr><th>Name</th><th>Email</th></tr></thead><tbody>"
                                            For Each objName As BuiltWithName In objBuiltWith.Results(0).Meta.Names
                                                html += "<tr><td>" & objName.NameName & "</td><td>" & objName.Email & "</td></tr>"
                                            Next
                                            html += "</tbody></table>"

                                            divBWContacts.InnerHtml = html
                                        End If
                                    End If

                                    If objBuiltWith.Results(0).Attributes.ContainsKey("Sitemap") Then
                                        lblBWSitemapURL.Text = CCommon.ToString(objBuiltWith.Results(0).Attributes("Sitemap"))
                                    End If
                                    If objBuiltWith.Results(0).Attributes.ContainsKey("RefIP") Then
                                        lblBWReferringIP.Text = CCommon.ToString(objBuiltWith.Results(0).Attributes("RefIP"))
                                    End If
                                    If objBuiltWith.Results(0).Attributes.ContainsKey("Followers") Then
                                        lblBWBrandFollowers.Text = CCommon.ToString(objBuiltWith.Results(0).Attributes("Followers"))
                                    End If
                                    If objBuiltWith.Results(0).Attributes.ContainsKey("RefSN") Then
                                        lblBWReferringSubnets.Text = CCommon.ToString(objBuiltWith.Results(0).Attributes("RefSN"))
                                    End If

                                    If objBuiltWith.Results(0).Attributes.ContainsKey("CDimensions") Then
                                        lblBWGoogleDimensions.Text = CCommon.ToString(objBuiltWith.Results(0).Attributes("CDimensions"))
                                    End If
                                    If objBuiltWith.Results(0).Attributes.ContainsKey("CMetrics") Then
                                        lblBWGoogleMetrics.Text = CCommon.ToString(objBuiltWith.Results(0).Attributes("CMetrics"))
                                    End If
                                    If objBuiltWith.Results(0).Attributes.ContainsKey("CGoals") Then
                                        lblBWGoogleGoals.Text = CCommon.ToString(objBuiltWith.Results(0).Attributes("CGoals"))
                                    End If
                                    If objBuiltWith.Results(0).Attributes.ContainsKey("CGoals") Then
                                        lblBWGoogleTagManager.Text = CCommon.ToString(objBuiltWith.Results(0).Attributes("CGoals"))
                                    End If

                                    divBuiltWith.Visible = True
                                End If

                            End If
                        End If
                    End If
                End If
                LoadControls(tempEvntId)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        'Modified by Neelam on 02/16/2018 - Added functionality to attach files*/
        Protected Sub btnAttach_Click(sender As Object, e As EventArgs)
            Try
                If (fileupload.PostedFile.ContentLength > 0) Then
                    Dim strFName As String()
                    Dim strFilePath, strFileName, strFileType As String
                    strFileName = Path.GetFileName(fileupload.PostedFile.FileName)
                    If Directory.Exists(CCommon.GetDocumentPhysicalPath(Session("DomainID"))) = False Then ' If Folder Does not exists create New Folder.
                        Directory.CreateDirectory(CCommon.GetDocumentPhysicalPath(Session("DomainID")))
                    End If
                    Dim newFileName As String = Path.GetFileNameWithoutExtension(strFileName) & DateTime.UtcNow.ToString("yyyyMMddhhmmssfff") & Path.GetExtension(strFileName)
                    strFilePath = CCommon.GetDocumentPhysicalPath(Session("DomainID")) & newFileName
                    strFName = Split(strFileName, ".")
                    strFileType = "." & strFName(strFName.Length - 1)                     'Getting the Extension of the File
                    fileupload.PostedFile.SaveAs(strFilePath)
                    UploadFile("L", strFileType, newFileName, strFName(0), "", 0, 0, "A")
                    Dim script As String = "function f(){$find(""" + RadWinDocumentFiles.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
                    ScriptManager.RegisterStartupScript(Page, Page.GetType(), "key", script, True)
                    Response.Redirect(Request.RawUrl, False)
                Else
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "NoFiles", "alert('Please attach a file to upload.');return false;", True)
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Protected Sub btnLinkClose_Click(sender As Object, e As EventArgs)
            UploadFile("U", "", txtLinkURL.Text, txtLinkName.Text, "", 0, 0, "A")
            Response.Redirect(Request.RawUrl, False)
        End Sub

        'Modified by Neelam on 02/16/2018 - Added function to save File/Link data in the database*/
        Sub UploadFile(ByVal URLType As String, ByVal strFileType As String, ByVal strFileName As String, ByVal DocName As String, ByVal DocDesc As String,
                       ByVal DocumentStatus As Long, DocCategory As Long, ByVal strType As String)
            Try
                Dim arrOutPut As String()
                Dim objDocuments As New DocumentList()
                With objDocuments
                    .DomainID = Session("DomainId")
                    .UserCntID = Session("UserContactID")
                    .UrlType = URLType
                    .DocumentStatus = DocumentStatus
                    .DocCategory = DocCategory
                    .FileType = strFileType
                    .DocName = DocName
                    .DocDesc = DocDesc
                    .FileName = strFileName
                    .DocumentSection = strType
                    '.RecID = lngDivID
                    If lngDivID > 0 Then
                        .RecID = lngDivID
                    ElseIf Session("DivisionID") IsNot Nothing And Session("DivisionID") > 0 Then
                        .RecID = Session("DivisionID")
                    Else
                        .RecID = 0
                    End If
                    .DocumentType = IIf(lngDivID > 0, 2, 1) '1=generic,2=specific
                    .FormFieldGroupId = IIf(URLType = "L", ddlSectionFile.SelectedValue, ddlSectionLink.SelectedValue)
                    arrOutPut = .SaveDocuments()
                End With
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        <WebMethod()>
        Public Shared Function bindContact(ByVal domain As String, ByVal oppid As String, ByVal DivisionID As String) As String
            Dim dtData As New DataTable()
            Dim objCommon As New CCommon
            objCommon.DomainID = Convert.ToInt64(domain)
            objCommon.RecordId = Convert.ToInt64(oppid)
            objCommon.DivisionID = Convert.ToInt64(DivisionID)
            dtData = objCommon.GetDivPartnerContacts()
            Dim jsTargets As [String] = Nothing
            jsTargets = DataTableToJsonObj(dtData)
            Return jsTargets
        End Function
        <WebMethod()>
        Public Shared Function MappingContact(ByVal ContactID As Integer) As String
            selectContactId = ContactID
            Return "1"
        End Function
        Public Shared Function DataTableToJsonObj(dt As DataTable) As String
            Dim ds As New DataSet()
            ds.Merge(dt)
            Dim JsonString As New StringBuilder()
            If ds IsNot Nothing AndAlso ds.Tables(0).Rows.Count > 0 Then
                JsonString.Append("[")
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    JsonString.Append("{")
                    For j As Integer = 0 To ds.Tables(0).Columns.Count - 1
                        If j < ds.Tables(0).Columns.Count - 1 Then
                            JsonString.Append("""" + ds.Tables(0).Columns(j).ColumnName.ToString() + """:" + """" + ds.Tables(0).Rows(i)(j).ToString() + """,")
                        ElseIf j = ds.Tables(0).Columns.Count - 1 Then
                            JsonString.Append("""" + ds.Tables(0).Columns(j).ColumnName.ToString() + """:" + """" + ds.Tables(0).Rows(i)(j).ToString() + """")
                        End If
                    Next
                    If i = ds.Tables(0).Rows.Count - 1 Then
                        JsonString.Append("}")
                    Else
                        JsonString.Append("},")
                    End If
                Next
                JsonString.Append("]")
                Return JsonString.ToString()
            Else
                Return Nothing
            End If
        End Function

        Sub LoadControls(Optional ByVal typeEvent As Long = 0)
            Try
                tblMain.Controls.Clear()
                Dim ds As DataSet
                Dim objPageLayout As New CPageLayout
                Dim fields() As String

                objPageLayout.CoType = "L"
                objPageLayout.UserCntID = Session("UserContactId")
                objPageLayout.RecordId = lngDivID
                objPageLayout.DomainID = Session("DomainID")
                objPageLayout.PageId = 1
                objPageLayout.numRelCntType = CCommon.ToLong(objLeads.CompanyType)

                objPageLayout.FormId = 34
                objPageLayout.PageType = 3

                ds = objPageLayout.GetTableInfoDefault()  ' getting the table structure 
                dtTableInfo = ds.Tables(0)

                If Not Page.IsPostBack Then
                    Dim dsFieldGroups As New DataSet()
                    Dim objBizFormWizardFormConf As New BizFormWizardMasterConfiguration
                    objBizFormWizardFormConf.DomainID = CCommon.ToLong(Session("DomainID"))
                    objBizFormWizardFormConf.numFormID = 99
                    'numGroupID is always 0 in table so line is commented
                    'objBizFormWizardFormConf.numGroupID = CCommon.ToLong(Session("UserGroupID"))
                    objBizFormWizardFormConf.tintPageType = 4
                    dsFieldGroups = objBizFormWizardFormConf.ManageFormFieldGroup()
                    ddlSectionFile.DataSource = dsFieldGroups.Tables(0)
                    ddlSectionFile.DataValueField = "numFormFieldGroupId"
                    ddlSectionFile.DataTextField = "vcGroupName"
                    ddlSectionFile.DataBind()
                    ddlSectionFile.Items.Insert(0, New ListItem("-- Select One --", "0"))

                    ddlSectionLink.DataSource = dsFieldGroups.Tables(0)
                    ddlSectionLink.DataValueField = "numFormFieldGroupId"
                    ddlSectionLink.DataTextField = "vcGroupName"
                    ddlSectionLink.DataBind()
                    ddlSectionLink.Items.Insert(0, New ListItem("-- Select One --", "0"))
                End If

                Dim objOpp As New BusinessLogic.Opportunities.OppotunitiesIP
                Dim dtDocLinkName As New DataTable
                With objOpp
                    .DomainID = Session("DomainID")
                    .DivisionID = lngDivID
                    dtDocLinkName = .GetDocumentFilesLink("P")
                End With

                If Not dtDocLinkName Is Nothing AndAlso dtDocLinkName.Rows.Count > 0 Then
                    ShowDocuments(tblDocuments, dtDocLinkName, dtTableInfo, 0)
                End If

                Dim intCol1Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=1")
                Dim intCol2Count As Int32 = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=2")
                Dim intCol1 As Int32 = 0
                Dim intCol2 As Int32 = 0
                Dim tblCell As TableCell

                Dim tblRow As TableRow
                Const NoOfColumns As Short = 2
                Dim ColCount As Int32 = 0

                ''If intermediatory Page is enabled then Add the text "Name" to dropdown fields
                objPageControls.CreateTemplateRow(tblMain)

                objPageControls.ContactID = objLeads.ContactID
                objPageControls.DivisionID = objLeads.DivisionID
                objPageControls.TerritoryID = objLeads.TerritoryID
                objPageControls.strPageType = "Organization"
                objPageControls.EditPermission = m_aryRightsForPage(RIGHTSTYPE.UPDATE)

                Dim dv As DataView = dtTableInfo.DefaultView
                Dim dvExcutedView As DataView = dtTableInfo.DefaultView
                Dim dvExcutedGroupsView As DataView = dtTableInfo.DefaultView
                Dim fieldGroups = (From dRow In dtTableInfo.Rows
                                    Select New With {Key .groupId = dRow("numFormFieldGroupId"), Key .groupName = dRow("vcGroupName"), Key .order = dRow("numOrder")}).Distinct()

                For Each drGroup In fieldGroups.OrderBy(Function(x) x.order)
                    tblCell = New TableCell()
                    tblRow = New TableRow
                    tblCell.ColumnSpan = 4
                    tblCell.CssClass = "tableGroupHeader"
                    tblCell.Text = CCommon.ToString(drGroup.groupName)
                    tblRow.Cells.Add(tblCell)
                    tblMain.Rows.Add(tblRow)

                    If drGroup.groupId > 0 AndAlso Not dtDocLinkName Is Nothing AndAlso dtDocLinkName.Rows.Count > 0 Then
                        If dtDocLinkName.Select("numFormFieldGroupId=" & drGroup.groupId).Length > 0 Then
                            Dim tableDocs As New HtmlTable
                            tableDocs.Style.Add("float", "right")
                            ShowDocuments(tableDocs, dtDocLinkName, dtTableInfo, drGroup.groupId)

                            tblCell = New TableCell()
                            tblRow = New TableRow
                            tblCell.ColumnSpan = 4
                            tblCell.Controls.Add(tableDocs)
                            tblRow.Cells.Add(tblCell)
                            tblMain.Rows.Add(tblRow)
                        End If
                    End If

                    dvExcutedView.RowFilter = "numFormFieldGroupId = " & drGroup.groupId

                    tblRow = New TableRow
                    tblCell = New TableCell
                    intCol1Count = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=1 AND numFormFieldGroupId='" & drGroup.groupId & "'")
                    intCol2Count = dtTableInfo.Compute("Count(intcoulmn)", "intcoulmn=2 AND numFormFieldGroupId='" & drGroup.groupId & "'")
                    intCol1 = 0
                    intCol2 = 0
                    For Each drData As DataRowView In dvExcutedView
                        Dim dr As DataRow
                        dr = drData.Row
                        If boolIntermediatoryPage = True Then
                            If Not IsDBNull(dr("vcPropertyName")) And (dr("fld_type") = "SelectBox") And dr("bitCustomField") = False Then
                                If dr("vcPropertyName") <> "numPartenerSource" And dr("vcPropertyName") <> "numPartenerContact" Then
                                    dr("vcPropertyName") = dr("vcPropertyName") & "Name"
                                End If
                            End If
                        End If

                        If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False Then

                            If dr("vcPropertyName") = "numPartenerSource" Then
                                If typeEvent = 1 Then
                                    dr("vcValue") = CCommon.ToString(objLeads.numPartenerSourceId)
                                Else
                                    dr("vcValue") = objLeads.GetType.GetProperty(dr("vcPropertyName")).GetValue(objLeads, Nothing)
                                End If
                            ElseIf CCommon.ToString(dr("vcPropertyName")) = "vcCompactContactDetails" Then
                                Dim strData As New StringBuilder()
                                strData.Append("<a class='text-yellow' href='javascript:OpenContact(" & objLeads.ContactID & ",1)'>" & objLeads.GivenName & "</a> &nbsp;")
                                strData.Append(objLeads.ContactPhone & "&nbsp;")
                                If objLeads.PhoneExt <> "" Then
                                    strData.Append("&nbsp;(" & objLeads.PhoneExt & ")")
                                End If
                                If objLeads.Email <> "" Then
                                    strData.Append("<img src='../images/msg_unread_small.gif' email=" & objLeads.Email & " id='imgEmailPopupId' onclick='return OpemParticularEmail(" & objLeads.ContactID & ")' />")
                                End If
                                dr("vcValue") = strData
                            ElseIf dr("vcPropertyName") = "numPartenerContact" Then
                                If typeEvent = 1 Then
                                    dr("vcValue") = CCommon.ToLong(objLeads.numPartenerContactId)
                                Else
                                    dr("vcValue") = objLeads.GetType.GetProperty(dr("vcPropertyName")).GetValue(objLeads, Nothing)
                                End If
                            Else
                                dr("vcValue") = objLeads.GetType.GetProperty(dr("vcPropertyName")).GetValue(objLeads, Nothing)
                            End If

                        End If
                        If ColCount = 0 Then
                            tblRow = New TableRow
                        End If

                        If intCol1Count = intCol1 And intCol2Count > intCol1Count Then
                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)
                            ColCount = 1
                        End If

                        If (dr("fld_type") = "ListBox" Or dr("fld_type") = "SelectBox") Then
                            Dim dtData As DataTable
                            'Dim dtSelOpportunity As DataTable

                            If Not IsDBNull(dr("vcPropertyName")) Then

                                If dr("vcPropertyName") = "AssignedTo" Then 'dr("numFieldId") = 84
                                    Dim m_aryRightsForAssignedTo() As Integer = GetUserRightsForPage_Other(2, 128)
                                    If m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 0 Then
                                        dr("bitCanBeUpdated") = False
                                    ElseIf m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 1 Then
                                        Try
                                            If CCommon.ToLong(hfOwner.Value) <> Session("UserContactID") Then
                                                dr("bitCanBeUpdated") = False
                                            End If
                                        Catch ex As Exception
                                        End Try
                                    ElseIf m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 2 Then
                                        Dim i As Integer
                                        Dim dtTerritory As DataTable
                                        dtTerritory = Session("UserTerritory")
                                        If CCommon.ToLong(hfTerritory.Value) = 0 Then
                                            dr("bitCanBeUpdated") = False
                                        Else
                                            Dim chkDelete As Boolean = False
                                            For i = 0 To dtTerritory.Rows.Count - 1
                                                If CCommon.ToLong(hfTerritory.Value) = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                                    chkDelete = True
                                                End If
                                            Next
                                            If chkDelete = False Then
                                                dr("bitCanBeUpdated") = False
                                            End If
                                        End If
                                    Else
                                        dr("bitCanBeUpdated") = True
                                    End If

                                    If boolIntermediatoryPage = False Then
                                        If Session("PopulateUserCriteria") = 1 Then

                                            dtData = objCommon.ConEmpListFromTerritories(Session("DomainID"), 0, 0, objLeads.TerritoryID)

                                        ElseIf Session("PopulateUserCriteria") = 2 Then
                                            dtData = objCommon.ConEmpList(Session("DomainID"), Session("UserContactID"))
                                        Else
                                            dtData = objCommon.ConEmpList(Session("DomainID"), 0, 0)
                                        End If

                                    End If
                                ElseIf dr("vcPropertyName") = "AssignedToName" Then
                                    Dim m_aryRightsForAssignedTo() As Integer = GetUserRightsForPage_Other(2, 128)
                                    If m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 0 Then
                                        dr("bitCanBeUpdated") = False
                                    ElseIf m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 1 Then
                                        Try
                                            If CCommon.ToLong(hfOwner.Value) <> Session("UserContactID") Then
                                                dr("bitCanBeUpdated") = False
                                            End If
                                        Catch ex As Exception
                                        End Try
                                    ElseIf m_aryRightsForAssignedTo(RIGHTSTYPE.UPDATE) = 2 Then
                                        Dim i As Integer
                                        Dim dtTerritory As DataTable
                                        dtTerritory = Session("UserTerritory")
                                        If CCommon.ToLong(hfTerritory.Value) = 0 Then
                                            dr("bitCanBeUpdated") = False
                                        Else
                                            Dim chkDelete As Boolean = False
                                            For i = 0 To dtTerritory.Rows.Count - 1
                                                If CCommon.ToLong(hfTerritory.Value) = dtTerritory.Rows(i).Item("numTerritoryId") Then
                                                    chkDelete = True
                                                End If
                                            Next
                                            If chkDelete = False Then
                                                dr("bitCanBeUpdated") = False
                                            End If
                                        End If
                                    Else
                                        dr("bitCanBeUpdated") = True
                                    End If
                                ElseIf dr("vcPropertyName") = "GroupID" Then ' dr("numFieldId") = 89 Then

                                    If boolIntermediatoryPage = False Then

                                        dtData = objCommon.GetGroups()
                                    End If
                                ElseIf dr("vcPropertyName") = "numPartenerSource" Then
                                    objCommon.DomainID = Session("DomainID")
                                    dtData = objCommon.GetPartnerSource()
                                ElseIf dr("vcPropertyName") = "numPartenerContact" Then
                                    objCommon.DomainID = Session("DomainID")
                                    objCommon.RecordId = objLeads.DivisionID
                                    dtData = objCommon.GetDivPartnerContacts()
                                ElseIf dr("vcPropertyName") = "CampaignID" Then ' dr("numFieldId") = 75 Then 'Campaign
                                    If boolIntermediatoryPage = False Then
                                        Dim objCampaign As New BusinessLogic.Reports.PredefinedReports
                                        objCampaign.byteMode = 2
                                        objCampaign.DomainID = Session("DomainID")
                                        dtData = objCampaign.GetCampaign()
                                    End If
                                ElseIf dr("vcPropertyName") = "DripCampaign" Then ' dr("numFieldId") = 291 Then
                                    If boolIntermediatoryPage = False Then
                                        Dim objCampaign As New BusinessLogic.Marketing.Campaign
                                        With objCampaign
                                            .SortCharacter = "0"
                                            .UserCntID = Session("UserContactID")
                                            .PageSize = 100
                                            .TotalRecords = 0
                                            .DomainID = Session("DomainID")
                                            .columnSortOrder = "Asc"
                                            .CurrentPage = 1
                                            .columnName = "vcECampName"
                                            dtData = objCampaign.ECampaignList
                                            Dim drow As DataRow = dtData.NewRow
                                            drow("numECampaignID") = -1
                                            drow("vcECampName") = "-- Disengaged --"
                                            dtData.Rows.Add(drow)

                                        End With
                                    End If
                                ElseIf Not IsDBNull(dr("numListID")) And dr("numListID") <> 0 Then
                                    If boolIntermediatoryPage = False Then
                                        If dr("ListRelID") > 0 Then
                                            objCommon.Mode = 3
                                            objCommon.DomainID = Session("DomainID")
                                            If dr("ListRelID") = 834 Then 'Live Database ID is 834 = Shipping Zone
                                                objCommon.PrimaryListItemID = objLeads.ShippingZone
                                            Else
                                                objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objLeads)
                                            End If
                                            objCommon.SecondaryListID = dr("numListId")
                                            dtData = objCommon.GetFieldRelationships.Tables(0)
                                        Else
                                            dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                        End If
                                    End If
                                End If
                            End If

                            If dr("vcDbColumnName") = "numCompanyDiff" Then
                                If boolIntermediatoryPage Then
                                    tblCell = New TableCell
                                    tblCell.Font.Bold = True
                                    tblCell.Text = IIf(dr("vcValue").ToString.Trim.Length = 0, "Company Differentiation", dr("vcValue").ToString)
                                    tblCell.HorizontalAlign = HorizontalAlign.Right

                                    tblCell.Attributes.Add("id", objPageControls.strPageType & "~" & dr("numFieldId").ToString & "~" & dr("bitCustomField").ToString & "~" & objPageControls.ContactID & "~" & objPageControls.DivisionID & "~" & objPageControls.TerritoryID & "~" & dr("ListRelID").ToString)
                                    tblCell.Attributes.Add("class", "editable_select")
                                    tblCell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                                    tblCell.Attributes.Add("onmouseout", "bgColor=''")

                                    tblRow.Cells.Add(tblCell)

                                    tblCell = New TableCell
                                    tblCell.Text = objLeads.CompanyDiffValue
                                    tblCell.Attributes.Add("id", objPageControls.strPageType & "~" & "2038" & "~" & "0" & "~" & objPageControls.ContactID & "~" & objPageControls.DivisionID & "~" & objPageControls.TerritoryID & "~" & dr("ListRelID").ToString)
                                    tblCell.Attributes.Add("class", "click")
                                    tblCell.Attributes.Add("onmousemove", "bgColor='lightgoldenRodYellow'")
                                    tblCell.Attributes.Add("onmouseout", "bgColor=''")

                                    tblRow.Cells.Add(tblCell)
                                Else
                                    Dim ddl As New DropDownList

                                    tblCell = New TableCell
                                    dr("vcFieldName") = dr("vcFieldName").ToString.Replace("$", "")
                                    ddl.CssClass = "signup"
                                    ddl.Width = 200
                                    ddl.ID = dr("numFieldId").ToString & dr("vcFieldName").ToString & "~" & IIf(IsDBNull(dr("numListId")), 0, dr("numListId").ToString).ToString
                                    ddl.DataSource = dtData
                                    ddl.DataValueField = dtData.Columns(0).ColumnName
                                    ddl.DataTextField = dtData.Columns(1).ColumnName
                                    ddl.DataBind()
                                    ddl.Items.Insert(0, "-- Select One --")
                                    ddl.Items.FindByText("-- Select One --").Value = "0"
                                    ddl.Enabled = True
                                    ddl.EnableViewState = False
                                    If Not IsDBNull(dr("vcValue")) Then
                                        If Not ddl.Items.FindByValue(dr("vcValue").ToString) Is Nothing Then
                                            ddl.Items.FindByValue(dr("vcValue").ToString).Selected = True
                                        End If
                                    End If
                                    tblCell.CssClass = "normal1"
                                    tblCell.Controls.Add(ddl)
                                    tblCell.HorizontalAlign = HorizontalAlign.Right
                                    tblRow.Cells.Add(tblCell)


                                    tblCell = New TableCell
                                    tblCell.Width = Unit.Percentage(25)
                                    Dim txt As TextBox
                                    txt = New TextBox
                                    txt.Width = 200
                                    txt.CssClass = "signup"
                                    txt.Text = objLeads.CompanyDiffValue
                                    txt.EnableViewState = False
                                    txt.ID = "2038" & dr("vcFieldName").ToString
                                    tblCell.CssClass = "normal1"
                                    tblCell.Controls.Add(txt)

                                    tblRow.Cells.Add(tblCell)

                                    If CLng(dr("DependentFields")) > 0 And Not boolIntermediatoryPage Then
                                        ddl.AutoPostBack = True
                                        AddHandler ddl.SelectedIndexChanged, AddressOf PopulateDependentDropdown
                                    End If
                                End If

                            ElseIf boolIntermediatoryPage Then
                                objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData, RecordID:=lngDivID)
                            Else
                                Dim ddl As DropDownList
                                ddl = objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData, RecordID:=lngDivID, boolEnabled:=CCommon.ToBool(dr("bitCanBeUpdated")))

                                If CLng(dr("DependentFields")) > 0 And Not boolIntermediatoryPage Then
                                    ddl.AutoPostBack = True
                                    AddHandler ddl.SelectedIndexChanged, AddressOf PopulateDependentDropdown
                                End If
                            End If
                        ElseIf dr("fld_type") = "CheckBoxList" Then
                            If Not IsDBNull(dr("vcPropertyName")) Then
                                Dim dtData As DataTable

                                If boolIntermediatoryPage = False Then
                                    If dr("ListRelID") > 0 Then
                                        objCommon.Mode = 3
                                        objCommon.DomainID = Session("DomainID")
                                        If dr("ListRelID") = 834 Then 'Live Database ID is 834 = Shipping Zone
                                            objCommon.PrimaryListItemID = objLeads.ShippingZone
                                        Else
                                            objCommon.PrimaryListItemID = objPageControls.GetPrimaryListItemID(dr("ListRelID"), dtTableInfo, objLeads)
                                        End If
                                        objCommon.SecondaryListID = dr("numListId")
                                        dtData = objCommon.GetFieldRelationships.Tables(0)
                                    Else
                                        dtData = objCommon.GetMasterListItems(dr("numListID"), Session("DomainID"))
                                    End If
                                End If

                                objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, dtData)
                            End If
                        ElseIf dr("fld_type") = "Popup" Then
                            If dr("vcDbColumnName") = "numConEmailCampID" Then ' dr("numFieldId") = 292 Then 'drip campaign
                                objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, RecordID:=objLeads.ContactECampaignID)
                            Else
                                objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, RecordID:=lngDivID)
                            End If
                        ElseIf dr("fld_type") = "Email" Then
                            objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, RecordID:=CCommon.ToLong(txtContId.Text))
                        Else
                            objPageControls.CreateCells(tblRow, dr, boolIntermediatoryPage, RecordID:=lngDivID)
                        End If


                        If intCol2Count = intCol2 And intCol1Count > intCol2Count Then
                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            tblCell = New TableCell
                            tblCell.CssClass = "normal1"
                            tblRow.Cells.Add(tblCell)

                            ColCount = 1
                        End If


                        If dr("intcoulmn") = 2 Then
                            If intCol1 <> intCol1Count Then
                                intCol1 = intCol1 + 1
                            End If

                            If intCol2 <> intCol2Count Then
                                intCol2 = intCol2 + 1
                            End If
                        End If

                        ColCount = ColCount + 1

                        If NoOfColumns = ColCount Then
                            ColCount = 0
                            tblMain.Rows.Add(tblRow)
                        End If
                    Next
                Next

                If ColCount > 0 Then
                    tblMain.Rows.Add(tblRow)
                End If

                'objPageControls.CreateComments(tblMain, objLeads.Comments, boolIntermediatoryPage)

                'Add Client Side validation for custom fields
                If Not boolIntermediatoryPage Then
                    Dim strValidation As String = objPageControls.GenerateValidationScript(dtTableInfo)
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "CFvalidation", strValidation, True)
                ElseIf Session("InlineEdit") = True And m_aryRightsForPage(RIGHTSTYPE.UPDATE) <> 0 Then
                    Dim strValidation As String = objPageControls.GenerateInlineEditValidationScript(dtTableInfo)
                    ClientScript.RegisterClientScriptBlock(Me.GetType, "InlineEditValidation", strValidation, True)
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub ddlRelationhip_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                If sender.SelectedValue > 0 Then radOppTab.Tabs(0).Text = "&nbsp;&nbsp;" & sender.SelectedItem.Text & " Detail&nbsp;&nbsp;"
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub ddlTerriory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                Dim lngAssignTo As Long
                lngAssignTo = objLeads.AssignedTo
                Dim ddl As DropDownList
                If Not radOppTab.MultiPage.FindControl("64Assigned To") Is Nothing Then
                    ddl = radOppTab.MultiPage.FindControl("64Assigned To")
                    objCommon.sb_FillConEmpFromTerritories(ddl, Session("DomainID"), 1, 0, sender.SelectedValue)
                    If Not ddl.Items.FindByValue(lngAssignTo) Is Nothing Then ddl.Items.FindByValue(lngAssignTo).Selected = True
                End If

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub sb_ShowAOIs(ByVal lngCntID As Long)
            Try
                m_aryRightsForShowAOIs = GetUserRightsForPage_Other(2, 4)
                If m_aryRightsForShowAOIs(RIGHTSTYPE.VIEW) = 0 Then chkAOI.Visible = False
                If objContacts Is Nothing Then objContacts = New CContacts
                Dim dtAOI As DataTable
                Dim i As Integer
                objContacts.ContactID = lngCntID
                objContacts.DomainID = Session("DomainID")
                dtAOI = objContacts.GetAOIDetails
                chkAOI.DataSource = dtAOI
                chkAOI.DataTextField = "vcAOIName"
                chkAOI.DataValueField = "numAOIId"
                chkAOI.DataBind()
                For i = 0 To dtAOI.Rows.Count - 1
                    If Not IsDBNull(dtAOI.Rows(i).Item("Status")) Then
                        If dtAOI.Rows(i).Item("Status") = 1 Then
                            chkAOI.Items.FindByValue(dtAOI.Rows(i).Item("numAOIId")).Selected = True
                        Else : chkAOI.Items.FindByValue(dtAOI.Rows(i).Item("numAOIId")).Selected = False
                        End If
                    End If
                Next
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveAOI(ByVal lngCntID As Long)
            Try
                If objContacts Is Nothing Then objContacts = New CContacts
                Dim dtTable As New DataTable
                dtTable.Columns.Add("numAOIId")
                dtTable.Columns.Add("Status")
                Dim dr As DataRow
                Dim i As Integer
                For i = 0 To chkAOI.Items.Count - 1
                    If chkAOI.Items(i).Selected = True Then
                        dr = dtTable.NewRow
                        dr("numAOIId") = chkAOI.Items(i).Value
                        dr("Status") = 1
                        dtTable.Rows.Add(dr)
                    End If
                Next
                Dim ds As New DataSet
                Dim strdetails As String
                dtTable.TableName = "Table"
                ds.Tables.Add(dtTable)
                strdetails = ds.GetXml
                ds.Tables.Remove(ds.Tables(0))

                objContacts.strAOI = strdetails
                objContacts.ContactID = lngCntID
                objContacts.SaveAOI()
                dtTable.Dispose()
                ds.Dispose()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            Try
                Save()
                SaveAOI(txtContId.Text)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnSaveClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
            Try
                Save()
                SaveAOI(txtContId.Text)
                PageRedirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub PageRedirect()
            Try
                If objLeads.GroupID <> 0 Then Session("grpID") = objLeads.GroupID

                If GetQueryStringVal("frm1") = "ActionItem" Then
                    Response.Redirect("../admin/ActionItemDetailsOld.aspx?CommId=" & GetQueryStringVal("CommId") & "&frm=" & GetQueryStringVal("frm") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm1=" & frm2)
                End If
                If GetQueryStringVal("frm") = "tickler" Then
                    Response.Redirect("../common/frmTicklerDisplay.aspx")
                ElseIf GetQueryStringVal("frm") = "ActItem" Then
                    Response.Redirect("../admin/ActionItemDetailsOld.aspx?CommId=" & GetQueryStringVal("CommID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "admin" Then
                    Response.Redirect("../admin/transferleads.aspx")
                ElseIf GetQueryStringVal("frm") = "search" Then
                    Response.Redirect("../common/searchdisplay.aspx?frm=advancedsearch&srchback=true&typ=" & GetQueryStringVal("typ") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "CaseReport" Then
                    Response.Redirect("../reports/frmCases.aspx")
                ElseIf GetQueryStringVal("frm") = "CampDetails" Then
                    Response.Redirect("../Marketing/frmCampaignDetails.aspx?CampID=" & GetQueryStringVal("CampID"))
                ElseIf GetQueryStringVal("frm") = "ProDetail" Then
                    Response.Redirect("../projects/frmProjects.aspx?frm=" & GetQueryStringVal("frm1") & "&ProId=" & GetQueryStringVal("ProId") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "contactdetail" Then
                    Response.Redirect("../contact/frmContacts.aspx?frm=" & GetQueryStringVal("frm1") & "&CntId=" & GetQueryStringVal("CntID") & "&SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "LeadActivity" Then
                    Response.Redirect("../reports/frmLeadActivity.aspx" & "?SI=" & SI1 & "&SI1=" & SI2 & "&frm=" & frm1 & "&frm1=" & frm2)
                ElseIf GetQueryStringVal("frm") = "AdvSearchSur" Then
                    Response.Redirect("../Admin/FrmAdvSurveyRes.aspx")
                ElseIf GetQueryStringVal("frm") = "doclist" Then
                    Response.Redirect("../Documents/frmDocList.aspx?Status=" & SI1 & "&Category=" & SI2)
                Else : Response.Redirect("../Leads/frmLeadList.aspx")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub


        Sub Save()
            Try
                With objLeads
                    .CompanyID = objCommon.CompID
                    .DomainID = Session("DomainID")
                    .DivisionID = lngDivID
                    .DomainID = Session("DomainID")
                    .UserCntID = Session("UserContactID")
                    .CRMType = 0
                    .LeadBoxFlg = 1
                    .DivisionName = ""
                    'txtDivisionName.Text()
                    .ContactID = txtContId.Text

                    For Each dr As DataRow In dtTableInfo.Rows
                        If Not IsDBNull(dr("vcPropertyName")) And dr("bitCustomField") = False And dr("bitCanBeUpdated") = True Then
                            If dr("vcDbColumnName") = "numCompanyDiff" Then
                                dr("vcFieldName") = dr("vcFieldName").ToString.Replace("$", "")

                                objLeads.CompanyDiff = CType(radOppTab.MultiPage.FindControl(dr("numFieldId").ToString & dr("vcFieldName").ToString & "~" & IIf(IsDBNull(dr("numListId")), 0, dr("numListId").ToString).ToString), DropDownList).SelectedValue
                                objLeads.CompanyDiffValue = CType(radOppTab.MultiPage.FindControl("2038" & dr("vcFieldName").ToString), TextBox).Text

                            Else
                                If dr("vcPropertyName") = "numPartenerSource" Then
                                    dr("vcPropertyName") = "numPartenerSourceId"
                                End If
                                If dr("vcPropertyName") = "numPartenerContact" Then
                                    dr("vcPropertyName") = "numPartenerContactId"
                                End If
                                objPageControls.SetValueForStaticFields(dr, objLeads, radOppTab.MultiPage)
                                If dr("vcPropertyName") = "numPartenerSourceId" Then
                                    dr("vcPropertyName") = "numPartenerSource"
                                End If
                                If dr("vcPropertyName") = "numPartenerContactId" Then
                                    dr("vcPropertyName") = "numPartenerContact"
                                End If

                            End If

                        End If

                    Next
                    'objPageControls.SetValueForComments(objLeads.Comments, radOppTab)

                End With
                objLeads.vcPartnerCode = objLeads.PartnerCode
                If objLeads.numPartenerContactId = 0 Then
                    objLeads.PartenerContact = selectContactId
                Else
                    objLeads.PartenerContact = objLeads.numPartenerContactId
                End If
                objLeads.PartenerSource = objLeads.numPartenerSourceId
                objLeads.CompanyID = objLeads.CreateRecordCompanyInfo
                Dim lngtempDivId As Long = 0
                lngtempDivId = objLeads.DivisionID
                objLeads.DivisionID = objLeads.ManageCompanyDivisionsInfo1

                Dim objWfA As New BACRM.BusinessLogic.Workflow.Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = objLeads.DivisionID
                objWfA.SaveWFOrganizationQueue()

                DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
                DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")
                If objLeads.DivisionID > 0 Then
                    objLeads.UpdateAddConInfo()
                    SaveCusField()
                Else
                    objLeads.DivisionID = lngtempDivId
                    DisplayError("Partner Code already Used for another organization")
                End If

                'Layer1.Attributes.Add("style", "VISIBILITY: hidden ;Z-INDEX: 2; LEFT: 200px; WIDTH: 450px; POSITION: absolute; TOP: 360px")
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
            Try
                PageRedirect()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnActionItem_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActionItem.Click
            Try
                Response.Redirect("../admin/ActionItemDetailsOld.aspx?frm=Leadedetails&CntID=" & txtContId.Text & "&SI=" & SI1 & "&SI1=" & radOppTab.SelectedIndex & "&frm1=" & frm1 & "&frm2=" & frm2)
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub



        Sub DisplayDynamicFlds()
            Try
                objPageControls.DisplayDynamicFlds(lngDivID, IIf(CCommon.ToLong(objLeads.CompanyType) > 0, objLeads.CompanyType, 1), Session("DomainID"), objPageControls.Location.Leads, radOppTab, 34)

            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Sub SaveCusField()
            Try
                objPageControls.SaveCusField(lngDivID, IIf(CCommon.ToLong(objLeads.CompanyType) > 0, objLeads.CompanyType, 1), Session("DomainID"), objPageControls.Location.Organization, radOppTab.MultiPage)
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This function is used to set the Association Information.
        ''' </summary>
        ''' <remarks>
        '''     This function calls the imprints the association information on a screen label.
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	01/18/2006	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Public Sub LoadAssociationInformation()
            Try
                Dim objAssociationInfo As New OrgAssociations                   'Create a new object of association
                objAssociationInfo.DomainID = Session("DomainID")               'Set the Domain Id
                objAssociationInfo.DivisionID = lngDivID                           'Set the Division Id
                Dim dtAssociationInfo As DataTable                              'Declare a new DataTable
                dtAssociationInfo = objAssociationInfo.getParentOrgForCurrentOrg 'Get the Association Info
                If dtAssociationInfo.Rows.Count > 0 Then                        'Check if association exists where there is a parent-child relationship
                    lblAssociation.Text = dtAssociationInfo.Rows(0).Item("vcData") & " of: " & "<a href=""javascript: GoOrgDetails(" & dtAssociationInfo.Rows(0).Item("numDivisionId") & "," & dtAssociationInfo.Rows(0).Item("tintCRMType") & ");""><b>" & dtAssociationInfo.Rows(0).Item("vcCompanyName") & "</b></a>, " & dtAssociationInfo.Rows(0).Item("vcDivisionName")
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub btnActDelete_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnActDelete.Click
            Try
                If objAccounts Is Nothing Then objAccounts = New CAccounts
                With objAccounts
                    .DivisionID = lngDivID
                    .DomainID = Session("DomainID")
                End With
                Dim strError As String = objAccounts.DeleteOrg()
                If strError = "" Then
                    Response.Redirect("../Leads/frmLeadList.aspx", False)
                Else
                    litMessage.Text = strError
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub




        Private Sub dlWebAnlys_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataListCommandEventArgs) Handles dlWebAnlys.ItemCommand
            Try
                If e.CommandName = "Pages" Then
                    Dim cmd As String = CType(e.CommandSource, LinkButton).CommandName
                    dlWebAnlys.SelectedIndex = e.Item.ItemIndex
                    Session("dlIndex") = CInt(e.Item.ItemIndex) + 1
                    createMainLink()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Sub createMainLink()
            Try
                objLeads.DivisionID = lngDivID
                objLeads.DomainID = Session("DomainID")
                Dim ds As DataSet
                ds = objLeads.GetWebAnlysDtl
                If Session("dlIndex") <> 0 Then dlWebAnlys.SelectedIndex = Session("dlIndex") - 1
                dlWebAnlys.DataSource = ds.Tables(1)
                dlWebAnlys.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Private Sub dlWebAnlys_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataListItemEventArgs) Handles dlWebAnlys.ItemDataBound
            Try
                If e.Item.ItemType = ListItemType.SelectedItem Then
                    objLeads.TrackingID = CType(e.Item.FindControl("lblID"), Label).Text
                    Dim dg As DataGrid
                    dg = e.Item.FindControl("dgWebAnlys")
                    dg.DataSource = objLeads.GetPagesVisited
                    dg.DataBind()
                End If
            Catch ex As Exception
                Throw ex
            End Try
        End Sub



        'Private Sub uwItem_ClickCellButton(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.CellEventArgs) Handles uwItem.ClickCellButton
        '    Try
        '        Dim numAItemCode As Integer = CInt(e.Cell.Tag)
        '        If objItems Is Nothing Then objItems = New CItems
        '        objItems.DivisionID = lngDivID
        '        objItems.DomainID = Session("DomainId")
        '        objItems.ItemCode = numAItemCode
        '        objItems.DeleteItemFromCmpAsset()
        '        'LoadAssets()
        '    Catch ex As Exception
        '        ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        '        DisplayError(ex.Message)
        '    End Try
        'End Sub

        'Private Sub uwItem_InitializeRow(ByVal sender As Object, ByVal e As Infragistics.WebUI.UltraWebGrid.RowEventArgs) Handles uwItem.InitializeRow
        '    Try
        '        If e.Row.HasParent = False Then
        '            e.Row.Cells.FromKey("Action").Tag = e.Row.Cells.FromKey("numAItemCode").Value
        '            e.Row.Cells.FromKey("Action").Value = "r"
        '            e.Row.Cells.FromKey("Action").Column.CellButtonStyle.CssClass = "Delete"
        '            e.Row.Cells.FromKey("Action").Column.Width = Unit.Pixel(20)
        '        End If
        '    Catch ex As Exception
        '        Throw ex
        '    End Try
        'End Sub

        Private Sub radOppTab_TabClick(sender As Object, e As Telerik.Web.UI.RadTabStripEventArgs) Handles radOppTab.TabClick
            Try
                PersistTable.Clear()
                PersistTable.Add(PersistKey.SelectedTab, radOppTab.SelectedIndex)
                PersistTable.Save(boolOnlyURL:=True)

                LoadTabDetails()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub btnEdit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnEdit.Click
            ViewState("IntermediatoryPage") = False
            boolIntermediatoryPage = False
            tblMain.Controls.Clear()
            LoadLeadDtls(1)
            ControlSettings()
        End Sub

        Sub ControlSettings()
            If boolIntermediatoryPage = True Then
                btnSave.Visible = False
                btnSaveClose.Visible = False
                btnEdit.Visible = True
            Else
                'objCommon.CheckdirtyForm(Page)

                btnSave.Visible = True
                btnSaveClose.Visible = True
                btnEdit.Visible = False
            End If
        End Sub


        Sub PopulateDependentDropdown(ByVal sender As Object, ByVal e As EventArgs)
            objPageControls.PopulateDropdowns(CType(sender, DropDownList), dtTableInfo, objCommon, radOppTab.MultiPage, Session("DomainID"))
        End Sub


        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to display the surveys history from the database for the selected contact
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/02/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Sub DisplaySurveyHistoryLists()
            Try
                objContacts = New CContacts
                objCommon.DivisionID = lngDivID
                objCommon.charModule = "D"
                objCommon.GetCompanySpecificValues1()
                lngCntID = objCommon.ContactID

                Dim objSurvey As New SurveyAdministration                   'Declare and create a object of SurveyAdministration
                objSurvey.DomainID = Session("DomainID")                    'Set the Doamin Id
                objSurvey.ContactId = lngCntID                              'Set the contact ID
                Dim dtSurveyHistorylist As DataTable                        'Declare a datatable
                dtSurveyHistorylist = GetRowsInPageRange(objSurvey.getSurveyHistoryList())  'call function to get the list of available surveys

                Dim drSurveyList As DataRow                               'Declare a DataRow object
                For Each drSurveyList In dtSurveyHistorylist.Rows                'Loop through the rows on the table containing the survey list
                    drSurveyList.Item("vcSurName") = Server.HtmlDecode(CStr(drSurveyList.Item("vcSurName")))
                Next

                dgSurvey.DataSource = dtSurveyHistorylist                   'set the datasource
                dgSurvey.DataBind()                                 'databind the datgrid
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to delete the Survey History
        ''' </summary>
        ''' <remarks>
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/02/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Protected Overridable Sub btnSurveyHistoryDeleteAction_Command(ByVal sender As Object, ByVal e As EventArgs)
            Try
                Dim btnSurveyHistoryDelete As Button = CType(sender, Button)                                'typecast to button
                Dim dgContainer As DataGridItem = CType(btnSurveyHistoryDelete.NamingContainer, DataGridItem) 'get the containeer object
                Dim gridIndex As Integer = dgContainer.ItemIndex                                            'Get the row idnex of datagrid
                Dim numSurId As Integer = CInt(dgSurvey.Items(gridIndex).Cells(0).Text)                     'Get the Survey Id from the column
                Dim numRespondentID As Integer = CInt(dgSurvey.Items(gridIndex).Cells(1).Text)              'Get the Respondent Id from the column

                Dim objSurvey As New SurveyAdministration                                                   'Declare and create a object of SurveyAdministration
                objSurvey.SurveyId = numSurId                                                               'Get the Survey Id
                objSurvey.SurveyExecution.SurveyExecution().RespondentID = numRespondentID                  'Get the Respondent ID
                objSurvey.DeleteSurveyHistoryForContact()                                                  'Call to delete the Survey History
                DisplaySurveyHistoryLists()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
            'Call to display the list of surveys for the contact
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to pick up a selected range of rows
        ''' </summary>
        ''' <remarks> Returns the table with limited rows
        ''' </remarks>
        ''' <param name="dtSurveyRespondentlist">Table which contains the search result to be displayed in the DataGrid</param>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	10/02/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Function GetRowsInPageRange(ByVal dtContactSurveylist As DataTable) As DataTable
            Try
                Dim numRowIndex As Integer                                                       'Declare an index variable
                Dim dtResultTableCopy As New DataTable                                           'Declare a datatable object and instantiate
                dtResultTableCopy = dtContactSurveylist.Clone()                                  'Clone the results table

                Dim iRowIndex As Integer = (txtCurrentPageSurveyHistory.Text * Session("PagingRows")) - Session("PagingRows") 'Declare a row index and set it to the first row in the table which has ot be displayed
                Dim iLastRowIndex As Integer = (iRowIndex + Session("PagingRows")) - 1 'Nos of records to be traversed
                For numRowIndex = iRowIndex To iLastRowIndex                                     'Loop through the row index from the table
                    If ((numRowIndex >= 0) And (dtContactSurveylist.Rows.Count > numRowIndex)) Then 'Ensure that the row exists
                        dtResultTableCopy.ImportRow(dtContactSurveylist.Rows(numRowIndex))       'import the range of rows
                    End If
                Next
                Return dtResultTableCopy
            Catch ex As Exception
                Throw ex
            End Try
        End Function
        Function ReturnName(ByVal CloseDate) As String
            Try
                Dim strTargetResolveDate As String = ""
                If Not IsDBNull(CloseDate) Then strTargetResolveDate = FormattedDateFromDate(CloseDate, Session("DateFormat"))
                Return strTargetResolveDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method is used to fill the search results for Adv.Search Screen from the XML file (intermediate search) when the page index changes
        ''' </summary>
        ''' <remarks> When the page number is specifically entered in the textbox, it executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/19/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub txtCurrentPageSurveyHistory_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCurrentPageSurveyHistory.TextChanged
            Try
                If IsNumeric(txtCurrentPageSurveyHistory.Text) Then
                    DisplaySurveyHistoryLists()                                                      'Call to execute the common steps to search and display results
                    litClientMessageSurveyHistory.Text = ""                                          'Clean the client side message
                Else : litClientMessageSurveyHistory.Text = "<script language=javascript>alert('Invalid page number entered, please re-enter.');</script>" 'Alert to user about invalid page number
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the last page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnkLastSurveyHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkLastSurveyHistory.Click
            Try
                txtCurrentPageSurveyHistory.Text = lblTotalSurveyHistory.Text                                 'Set the Page Index
                DisplaySurveyHistoryLists()                                                      'Call to execute the common steps to search and display results
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the previous page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnkPreviousSurveyHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkPreviousSurveyHistory.Click
            Try
                If Convert.ToInt64(txtCurrentPageSurveyHistory.Text) > 1 Then                               'If the first page is already displayed then exit
                    txtCurrentPageSurveyHistory.Text = txtCurrentPageSurveyHistory.Text - 1                               'decrement the page number
                End If
                DisplaySurveyHistoryLists()                                                                 'Call to execute the common steps to search and display results
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the first page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnkFirstSurveyHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkFirstSurveyHistory.Click
            Try
                txtCurrentPageSurveyHistory.Text = 1                                         'Set the first page index to 1
                DisplaySurveyHistoryLists()                                                  'Call to execute the common steps to search and display results
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the next page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnkNextSurveyHistory_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lnkNextSurveyHistory.Click
            Try
                If Convert.ToInt64(txtCurrentPageSurveyHistory.Text) < Convert.ToInt64(lblTotalSurveyHistory.Text) Then 'check if this is not the last page
                    txtCurrentPageSurveyHistory.Text = txtCurrentPageSurveyHistory.Text + 1 'increment the page number
                End If
                DisplaySurveyHistoryLists()                                                'Call to execute the common steps to search and display results
                litClientMessageSurveyHistory.Text = ""                                    'Clean the client side message
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the Next: 2 page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnk2SurveyHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk2SurveyHistory.Click
            Try
                If Convert.ToInt64(txtCurrentPageSurveyHistory.Text) + 2 <= Convert.ToInt64(lblTotalSurveyHistory.Text) Then 'check if incrementing the page counter exhausts the page
                    txtCurrentPageSurveyHistory.Text = txtCurrentPageSurveyHistory.Text + 2  'increment the page number by 2
                End If
                DisplaySurveyHistoryLists()                                                  'Call to Display the Results
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the Next: 3 page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnk3SurveyHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk3SurveyHistory.Click
            Try
                If Convert.ToInt64(txtCurrentPageSurveyHistory.Text) + 3 <= Convert.ToInt64(lblTotalSurveyHistory.Text) Then 'check if incrementing the page counter exhausts the page
                    txtCurrentPageSurveyHistory.Text = txtCurrentPageSurveyHistory.Text + 3      'increment the page number by 3
                End If
                DisplaySurveyHistoryLists()                                                      'Call to Display the Results
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the Next: 4 page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnk4SurveyHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk4SurveyHistory.Click
            Try
                If Convert.ToInt64(txtCurrentPageSurveyHistory.Text) + 4 <= Convert.ToInt64(lblTotalSurveyHistory.Text) Then  'check if incrementing the page counter exhausts the page
                    txtCurrentPageSurveyHistory.Text = txtCurrentPageSurveyHistory.Text + 4      'increment the page number by 4
                End If
                DisplaySurveyHistoryLists()                                                      'Call to Display the Results
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        ''' -----------------------------------------------------------------------------
        ''' <summary>
        '''     This method executed when the Next: 5 page icon is clicked on the DataGrid Page
        ''' </summary>
        ''' <remarks> When the page number is specifically changed by clicking on the last icon, 
        '''     this executes to display the records for the page
        ''' </remarks>
        ''' <history>
        ''' 	[Debasish Tapan Nag]	08/20/2005	Created
        ''' </history>
        ''' -----------------------------------------------------------------------------
        Private Sub lnk5SurveyHistory_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnk5SurveyHistory.Click
            Try
                If Convert.ToInt64(txtCurrentPageSurveyHistory.Text) + 5 <= Convert.ToInt64(lblTotalSurveyHistory.Text) Then 'check if incrementing the page counter exhausts the page
                    txtCurrentPageSurveyHistory.Text = txtCurrentPageSurveyHistory.Text + 5  'increment the page number by 5
                End If
                DisplaySurveyHistoryLists()                                                  'Call to Display the Results
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub

        Private Sub lbtnPromote_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnPromote.Click
            Try
                objLeads.UserCntID = Session("UserContactID")
                objLeads.ContactID = txtContId.Text
                objLeads.DivisionID = lngDivID
                objLeads.PromoteLead()
                Response.Redirect("../prospects/frmProspects.aspx?DivID=" & lngDivID & "&SI=" & SI1 & "&SI1=" & radOppTab.SelectedIndex & "&frm1=" & frm1 & "&frm2=" & frm2)

            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
        Private Sub DisplayError(ByVal ex As String)
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
        End Sub

        Protected Sub lkbBack_Click(sender As Object, e As EventArgs)
            Try
                Dim ds As DataSet
                objLeads.DivisionID = lngDivID
                objLeads.DomainID = Session("DomainID")
                ds = objLeads.GetWebAnlysDtl
                dlWebAnlys.SelectedIndex = -1
                dlWebAnlys.DataSource = ds.Tables(1)
                dlWebAnlys.DataBind()
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                DisplayError(ex.Message)
            End Try
        End Sub
    End Class
End Namespace
