Imports BACRM.BusinessLogic.Leads
Imports BACRM.BusinessLogic.Common
Namespace BACRM.UserInterface.Leads
    Public Class frmFollowUpHstr
        Inherits BACRMPage

        Dim objLeads As New CLeads

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents btnClose As System.Web.UI.WebControls.Button
        Protected WithEvents tblMile As System.Web.UI.WebControls.Table
        Protected WithEvents dgFollow As System.Web.UI.WebControls.DataGrid

        'NOTE: The following placeholder declaration is required by the Web Form Designer.
        'Do not delete or move it.
        Private designerPlaceholderDeclaration As System.Object

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Try
                If Not IsPostBack Then BindGrid()
                
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

        Sub BindGrid()
            Try
                objLeads.DivisionID = GetQueryStringVal( "rtyWR")
                objLeads.DomainID = Session("DomainID")
                dgFollow.DataSource = objLeads.GetFollowUpHstr
                dgFollow.DataBind()
            Catch ex As Exception
                Throw ex
            End Try
        End Sub

        Function ReturnName(ByVal bintCreatedDate) As String
            Try
                Dim strCreateDate As String
                If Not IsDBNull(bintCreatedDate) Then strCreateDate = FormattedDateFromDate(bintCreatedDate, Session("DateFormat"))
                Return strCreateDate
            Catch ex As Exception
                Throw ex
            End Try
        End Function

        Private Sub dgFollow_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgFollow.ItemCommand
            Try
                If e.CommandName = "Delete" Then
                    objLeads.FollowUpHstrID = e.Item.Cells(0).Text
                    objLeads.DomainID = Session("DomainID")
                    objLeads.DeleteFollowUpHstr()
                    BindGrid()
                End If
            Catch ex As Exception
                ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
                Response.Write(ex)
            End Try
        End Sub

    End Class
End Namespace
