' Modified By Anoop Jayaraj
Imports System.Data.SqlClient
Imports BACRMBUSSLOGIC.BussinessLogic
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Leads

Namespace BACRM.UserInterface.Leads
    Public Class frmAddLead
        Inherits BACRMPage
        Dim lngDivisionId As Long
        Dim objCommon As CCommon
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            btnSave.Attributes.Add("onclick", "return Save()")
            btnClose.Attributes.Add("onclick", "return Close()")
            If Not IsPostBack Then
                Session("Help") = "Organization"
                ' Checking Rights for Add
                objCommon = New CCommon
                Dim m_aryRightsForPage() As Integer
                m_aryRightsForPage = clsAuthorization.fn_GetPageListUserRights(objCommon, "frmAddLead.aspx", Session("userID"), 2, 1)
                If m_aryRightsForPage(RIGHTSTYPE.ADD) = 0 Then
                    btnSave.Visible = False
                    Exit Sub
                Else
                    btnSave.Visible = True
                End If
                objCommon.UserCntID = Session("UserContactID")
                objCommon.DomainID = Session("DomainID")
                objCommon.sb_FillGroupsFromDBSel(ddlGroup)
                objCommon.sb_FillComboFromDBwithSel(ddlTerritory, 78, Session("DomainID"))
                objCommon.sb_FillComboFromDBwithSel(howdidyouhear, 18, Session("DomainID"))
            End If

        End Sub

        Private Function InsertLeads() As Boolean
            Dim objLeads As New CLeads
            Try
                With objLeads
                    If Len(txtCompany.Text) < 1 Then
                        txtCompany.Text = txtLastName.Text & "," & txtFirstName.Text
                    End If
                    .CompanyName = txtCompany.Text.Trim
                    .How = howdidyouhear.SelectedItem.Value
                    .TerritoryID = ddlTerritory.SelectedItem.Value
                    .CustName = txtCompany.Text.Trim
                    .CompanyType = 0
                    .CRMType = 0
                    .DivisionName = "-"
                    .GroupID = ddlGroup.SelectedItem.Value
                    .UserCntID = Session("UserContactID")
                    .LeadBoxFlg = 1
                    .Country = Session("DefCountry")
                    .SCountry = Session("DefCountry")
                    .FirstName = txtFirstName.Text
                    .LastName = txtLastName.Text
                    .Phone = txtPhone.Text
                    .PhoneExt = txtextn.Text
                    .Email = txtEmail.Text
                    .DomainID = Session("DomainID")
                    .ContactType = 70

                End With
                objLeads.CompanyID = objLeads.CreateRecordCompanyInfo
                lngDivisionId = objLeads.CreateRecordDivisionsInfo
                objLeads.DivisionID = lngDivisionId
                objLeads.CreateRecordAddContactInfo()
                Return True
            Catch ex As Exception
                Throw ex
            End Try
        End Function


        Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
            If InsertLeads() = True Then
                Dim strScript As String = "<script language=JavaScript>"
                strScript += "window.opener.reDirectPage('../Leads/frmLeads.aspx?DivID=" & lngDivisionId & "'); self.close();"
                strScript += "</script>"
                If (Not Page.IsStartupScriptRegistered("clientScript")) Then
                    Page.RegisterStartupScript("clientScript", strScript)
                End If
            End If
        End Sub

    End Class
End Namespace