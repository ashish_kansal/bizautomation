<%@ Page Language="vb" AutoEventWireup="false" EnableEventValidation="false" MasterPageFile="~/common/GridMaster.Master"
    CodeBehind="frmLeadList.aspx.vb" Inherits="BACRM.UserInterface.Leads.frmLeadList" %>

<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server" ClientIDMode="Static">
    <title>Leads</title>
    <link href="../JavaScript/MultiSelect/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../JavaScript/MultiSelect/bootstrap-multiselect.js"></script>
    <script language="javascript">

        function OpenSetting() {
            window.open('../Prospects/frmConfCompanyList.aspx?RelId=1&FormId=34', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=370,scrollbars=yes,resizable=yes')
            return false
        }

      
        function GoImport() {
            window.location.href = "../admin/importfromputlook.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=prospects";
            return false;
        }
        function OpenPartner(a, b, c) {
            var str;
            if (b == 0) {

                str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylistDivID=" + a;

            }
            else if (b == 1) {

                str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylist&DivID=" + a;

            }
            else if (b == 2) {

                str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=opportunitylist&klds+7kldf=fjk-las&DivId=" + a;

            }

            document.location.href = str;

        }
        function BindPartnerContact(domain, oppid, dropId) {
            //var dropId = $(this).attr("id");
            console.log(dropId);
            var DivisionID = 0;
            $('select[attr="numPartenerSource"]').each(function () {

                DivisionID = $("option:selected", this).val();
            });
            oppid = 0;
            $.ajax({
                type: "POST",
                url: '../Leads/frmLeads.aspx/bindContact',
                data: JSON.stringify({ "domain": domain, "oppid": oppid, "DivisionID": DivisionID }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success:
                    function (result) {
                        var i = 0;
                        $('select[attr="numPartenerContact"]').each(function () {
                            if (i == 0) {
                                $(this).empty();
                                $(this).append("<option value='0'>-- Select One --</option>");
                                var id = $(this);
                                $.each(JSON.parse(result.d), function (idx, obj) {
                                    id.append("<option value='" + obj.numContactId + "'>" + obj.vcGivenName + "</option>");
                                });
                            }
                            i = 1;
                        });

                    },
                error:
                    function (XMLHttpRequest, textStatus, errorThrown) {
                        console.log("Error");
                    },

            });
        }
        function OpenSelTeam() {

            window.open("../Forecasting/frmSelectTeams.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&Type=2", '', 'toolbar=no,titlebar=no,left=200, top=300,width=600,height=300,scrollbars=no,resizable=yes')
            return false;
        }
        function PopupCheck() {
            document.Form1.btnGo.click()
        }

        function DeleteRecord() {
            if (confirm('Are you sure, you want to delete the selected record?')) {
                var RecordIDs = GetCheckedRowValues()
                document.getElementById('txtDelContactIds').value = RecordIDs;
                if (RecordIDs.length > 0)
                    return true;
                else {
                    alert('Please select atleast one record!!');
                    return false;
                }
            }
            else {
                return false;
            }
        }

        function OpenWindow(a, b, c) {
            var str;
            if (b == 0) {

                str = "../Leads/frmLeads.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=LeadsList&DivID=" + a;

            }
            else if (b == 1) {

                str = "../prospects/frmProspects.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=LeadsList&DivID=" + a;

            }
            else if (b == 2) {

                str = "../account/frmAccounts.aspx?pwer=oRJDw7By9RM5mn3Bku8m0rEpJ7werwetretrvnbtyuzsd6767&frm=LeadsList&klds+7kldf=fjk-las&DivId=" + a;

            }

            document.location.href = str;

        }


        function OpenActionItem(a) {
            window.open('../Leads/frmActionItemList.aspx?DivID=' + a + '', '', 'toolbar=no,titlebar=no,top=200,left=200,width=650,height=370,scrollbars=yes,resizable=yes')
        }

        function OpenEmail(a) {
            window.open('../Leads/frmOpenEmail.aspx?DivID=' + a + '', '', 'toolbar=no,titlebar=no,top=200,left=200,width=650,height=370,scrollbars=yes,resizable=yes')
        }

        function openDrip(cmpID, contactID) {
            window.open("../Marketing/frmConECampHstr.aspx?ConECampID=" + cmpID + "&pqwRT=" + contactID, '', 'toolbar=no,titlebar=no,top=250,left=250,width=700,height=300,scrollbars=no,resizable=no')
            return false;
        }
        function OpenHelp() {
            window.open('../Help/Initial_Grid_Organizations_Contacts_.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server"
    ClientIDMode="Static">
    <div class="row padbottom10">
        <div class="pull-left" id="tdRadios" runat="server">
            <div class="form-group" style="    margin-left: 14px;
    padding-top: 6px;">
            <asp:RadioButton ID="radMyself" Checked="true" AutoPostBack="true" GroupName="rad"
                runat="server" Text="Myself"></asp:RadioButton>
            <asp:RadioButton ID="radTeamSelected" AutoPostBack="true" GroupName="rad" runat="server"></asp:RadioButton>
            <a href="javascript:void(0);" onclick="javascript:document.getElementById('radTeamSelected').checked=true;OpenSelTeam();">Team Selected</a>

            <asp:DropDownList ID="ddlGroup" Style="display: none" runat="server" Visible="false" CssClass="signup" Width="130px" AutoPostBack="true">
            </asp:DropDownList>
                </div>
        </div>
        <div class="pull-right">
            <div class="form-inline">
                <div style="float:left;margin-right:10px;margin-top:6px;">
            <asp:RadioButtonList ID="radActive" AutoPostBack="true" CssClass="list-inline" runat="server"
                RepeatLayout="UnorderedList">
                <asp:ListItem Selected="True" Value="True">Active</asp:ListItem>
                <asp:ListItem Value="False">In-Active</asp:ListItem>
            </asp:RadioButtonList>
       </div>
            <button id="btnAddNewLead" onclick="return OpenPopUp('../include/frmAddOrganization.aspx?RelID=1&FormID=34');" class="btn btn-primary"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;New</button>
            <asp:LinkButton ID="btnAddActionItem" runat="server" CssClass="btn btn-primary" Visible="false"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Add Action Item</asp:LinkButton>
            <asp:LinkButton ID="btnSendAnnounceMent" runat="server" CssClass="btn btn-primary" Visible="false"><i class="fa fa-envelope"></i>&nbsp;&nbsp;Send Announcement</asp:LinkButton>
            <asp:LinkButton ID="btnGo" runat="server" CssClass="btn btn-primary" Style="display: none"><i class="fa fa-share-square-o"></i>&nbsp;&nbsp;Go</asp:LinkButton>
            
            <asp:LinkButton ID="btnDelete" runat="server" CssClass="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
                </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12 text-center" id="litMessageiv" runat="server">
            <asp:Literal ID="litMessage" runat="server"></asp:Literal>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server" ClientIDMode="Static">
    <asp:Label ID="lbLeads" runat="server"></asp:Label>&nbsp;<a href="#" onclick="return OpenHelpPopUp('relationships')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server" ClientIDMode="Static">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        NumericButtonCount="8"
        ShowPageIndexBox="Never"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridPlaceHolder" runat="server"
    ClientIDMode="Static">
    <div class="row">
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:GridView ID="gvSearch" runat="server" EnableViewState="true" AutoGenerateColumns="false"
                    CssClass="table table-striped table-bordered" Width="100%" ShowHeaderWhenEmpty="true" UseAccessibleHeader="true">
                    <Columns>
                    </Columns>
                    <EmptyDataTemplate>
                        No records Found.
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </div>
    </div>
    <asp:TextBox ID="txtDelContactIds" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortColumn" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtSortOrder" runat="server" Style="display: none"></asp:TextBox>
    <asp:TextBox ID="txtTotalPage" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtTotalRecords" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtSortChar" Text="0" Style="display: none" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtGridColumnFilter" Style="display: none" runat="server"></asp:TextBox>
    <asp:Button ID="btnGo1" Width="25" runat="server" Style="display: none" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>

    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
