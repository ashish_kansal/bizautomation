﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/BizMaster.Master" CodeBehind="NotFound.aspx.vb" Inherits=".NotFound" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <section class="content">
      <div class="error-page text-center">
        <h2 class="headline text-yellow">404</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>

          <p>
            We could not find the record or page you were looking for.
          </p>
        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>
</asp:Content>
