﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmDemandForecastDetails.aspx.vb" Inherits=".frmDemandForecastDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:Button runat="server" ID="btnClose" OnClientClick="return Close();" Text="Close" CssClass="btn btn-primary" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label runat="server" ID="lblPageTitle"></asp:Label>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <div class="row">
        <div class="col-xs-12">
            <div class="row padbottom10" id="divError" runat="server" style="display: none">
                <div class="col-sm-12">
                    <div class="alert alert-danger">
                        <h4><i class="icon fa fa-ban"></i>Error</h4>
                        <p>
                            <asp:Label ID="lblError" runat="server"></asp:Label>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12">
            <div class="table-responsive">
                <asp:Repeater runat="server" ID="rptRecords" OnItemDataBound="rptRecords_ItemDataBound">
                    <HeaderTemplate>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th><asp:Label runat="server" ID="lblTotalOrderedQtyHeader"></asp:Label></th>
                                    <th><asp:Label runat="server" ID="lblTotalTemainingdQtyHeader"></asp:Label></th>
                                    <th id="thHead1TotalOpp" runat="server"><asp:Label runat="server" ID="lblTotalForecastQtyHeader"></asp:Label></th>
                                    <th></th>
                                </tr>
                                <tr>
                                    <th>Sales Order / Work Order / Sales Opportunity (% complete)</th>
                                    <th>Qty (Eaches)</th>
                                    <th>Remaining Ship Qty</th>
                                    <th id="thHead2TotalOpp" runat="server">Opportunity Forecast Qty</th>
                                    <th>Release/Completion Date</th>
                                </tr>
                            </thead>
                            <tbody>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td><%# Eval("vcOppName")%></td>
                            <td style="text-align:right"><%# Eval("numOrderedQty")%></td>
                            <td style="text-align:right"><%# Eval("numQtyToShip")%></td>
                            <td id="tdOppForecast" runat="server" style="text-align:right"><%# Eval("numOpportunityForecastQty")%></td>
                            <td><%# Eval("dtReleaseDate")%></td>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                    <th><asp:Label runat="server" ID="lblTotalOrderedQtyFooter"></asp:Label></th>
                                    <th><asp:Label runat="server" ID="lblTotalTemainingdQtyFooter"></asp:Label></th>
                                    <th id="thFootTotalOpp" runat="server"><asp:Label runat="server" ID="lblTotalForecastQtyFooter"></asp:Label></th>
                                    <th></th>
                            </tr>
                        </tfoot>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</asp:Content>
