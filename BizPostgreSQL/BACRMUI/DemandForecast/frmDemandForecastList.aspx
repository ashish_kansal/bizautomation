﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmDemandForecastList.aspx.vb" Inherits=".frmDemandForecastList" %>

<%@ Register Assembly="AspNetPager, Version=7.4.4.0, Culture=neutral, PublicKeyToken=fb0a0fe055d40fd4" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function reDirectPage(a) {
            document.location = a;
        }

        function OpenDemandPlanWindow(id) {
            if (id == null || id == "") {
                window.open('../DemandForecast/frmNewDemandForecast.aspx', '', 'toolbar=no,titlebar=no,top=200,left=200,width=700,height=350,scrollbars=yes,resizable=yes')
            } else {
                window.open('../DemandForecast/frmNewDemandForecast.aspx?DFID=' + id, '', 'toolbar=no,titlebar=no,top=200,left=200,width=700,height=350,scrollbars=yes,resizable=yes')
            }

            return false;
        }

        function ExecuteDemandPlan(id) {
            var newUrl = "../Opportunity/frmPlangProcurement.aspx?DFID=" + id;
            document.location.href = newUrl;

            return false;
        }

        function SelectAll(id) {
            $("[id$=dgDemandForecast] > tbody").children("tr").not(":first").each(function () {
                var chk = $(this).find("[id$=ChkSelect]").first();
                if (id.checked == true) {
                    $(this).find("[id$=ChkSelect]").first().prop("checked",true);
                }
                else {
                    $(this).find("[id$=ChkSelect]").first().prop("checked", false);
                }
            });
        }

        function SelectionChanged(id) {
            if (id.checked == false) {
                var chkSelectAll = $('input[id$="ChkSelectAll"]');

                if (chkSelectAll != null && chkSelectAll[0].checked == true) {
                    chkSelectAll[0].checked = false;
                }
            }
        }

        function GetSelectedItems() {
            var DemandForecastIds = "";

            $("[id$=dgDemandForecast] > tbody").children("tr").not(":first").each(function () {
                if ($(this).find("[id$=ChkSelect]").first().prop("checked")) {
                    if (DemandForecastIds == "") {
                        DemandForecastIds = $(this).find("[id$=hdnDFID]").first().val();
                    }
                    else {
                        DemandForecastIds = DemandForecastIds + "," + $(this).find("[id$=hdnDFID]").first().val();
                    }
                }
            });

            if (DemandForecastIds == '') {
                alert("Please select atleast one item to delete.");
                return false;
            }
            else {
                document.getElementById('hdnSelectedItems').value = DemandForecastIds;
                return true;
            }
        }
        function OpenHelp() {
            window.open('../Help/Planning_Procurement.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right">
                <asp:LinkButton ID="btnNewDemandForecast" runat="server" CssClass="btn btn-primary" OnClientClick="return OpenDemandPlanWindow();">Create New Demand Plan</asp:LinkButton>
                <asp:LinkButton ID="btnDelete" CssClass="btn btn-danger" runat="server" OnClientClick="return GetSelectedItems();"><i class="fa fa-trash"></i>&nbsp;&nbsp;Delete</asp:LinkButton>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Demand Plan&nbsp;<a href="#" onclick="return OpenHelpPopUp('demandforecast/frmdemandforecastlist.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
    <webdiyer:AspNetPager ID="bizPager" runat="server"
        PagingButtonSpacing="0"
        CssClass="bizgridpager"
        AlwaysShow="true"
        CurrentPageButtonClass="active"
        PagingButtonUlLayoutClass="pagination"
        PagingButtonLayoutType="UnorderedList"
        FirstPageText="<<"
        LastPageText=">>"
        NextPageText=">"
        PrevPageText="<"
        Width="100%"
        UrlPaging="false"
        ShowPageIndexBox="Never"
        NumericButtonCount="8"
        ShowCustomInfoSection="Left"
        OnPageChanged="bizPager_PageChanged"
        CustomInfoHTML="Showing records %StartRecordIndex% to %EndRecordIndex% of %RecordCount% "
        CustomInfoClass="bizpagercustominfo">
    </webdiyer:AspNetPager>
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="UpdatePanelForecast" runat="server" ChildrenAsTriggers="true" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="dgDemandForecast" runat="server" Width="100%" AutoGenerateColumns="False" CssClass="table table-bordered table-responsive" UseAccessibleHeader="true">
                <Columns>
                    <asp:TemplateField>
                        <itemtemplate>
                                <a onclick="return ExecuteDemandPlan(<%# DataBinder.Eval(Container.DataItem, "numDFID")%>)" href="#" class="btn btn-xs btn-info">Execute Plan</a>
                            </itemtemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <itemtemplate>
                                <a onclick="return OpenDemandPlanWindow(<%# DataBinder.Eval(Container.DataItem, "numDFID")%>)" href="#" class="btn btn-xs btn-info">
                                    <i class="fa fa-pencil"></i>
                                </a>
                            </itemtemplate>
                    </asp:TemplateField>
                    <asp:BoundField HeaderText="Classifications" HeaderStyle-Width="40%" HeaderStyle-Font-Bold="true" ItemStyle-Width="40%" ItemStyle-Wrap="true" DataField="vcItemClassification"></asp:BoundField>
                    <asp:BoundField HeaderText="Item Groups" HeaderStyle-Width="20%" HeaderStyle-Font-Bold="true" ItemStyle-Width="20%" ItemStyle-Wrap="true" DataField="vcItemGroups"></asp:BoundField>
                    <asp:BoundField HeaderText="Warehouse Locations" HeaderStyle-Width="100%" HeaderStyle-Font-Bold="true" ItemStyle-Width="20%" ItemStyle-Wrap="true" DataField="vcWarehouse"></asp:BoundField>
                    <asp:TemplateField>
                        <itemtemplate>
                                <asp:CheckBox ID="ChkSelect" runat="server" onclick="SelectionChanged(this)" />
                                <asp:HiddenField ID="hdnDFID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "numDFID")%>' />
                            </itemtemplate>
                        <headertemplate>
                                <asp:CheckBox ID="ChkSelectAll" runat="server" onclick="SelectAll(this)" />
                            </headertemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:HiddenField ID="hdnSelectedItems" runat="server" />
    <asp:TextBox ID="txtCurrrentPage" runat="server" Style="display: none"></asp:TextBox>
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
