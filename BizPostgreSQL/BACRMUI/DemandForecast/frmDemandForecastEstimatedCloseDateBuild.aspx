﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmDemandForecastEstimatedCloseDateBuild.aspx.vb" Inherits="BACRM.UserInterface.DemandForecast.frmDemandForecastEstimatedCloseDateBuild" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link rel="Stylesheet" href="../CSS/bootstrap.min.css" />
    <link rel="Stylesheet" href="../CSS/biz.css" />
    <script type="text/javascript">
        $(document).ready(function () {
            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_pageLoaded(reloadScripts);
        });

        function reloadScripts() {
            $("#gvItems input[type=checkbox]").change(function () {
                if ($(this).is(":checked")) {
                    $("#hdnSelectedIDs").val(($("#hdnSelectedIDs").val() || "").length > 0 ? $("#hdnSelectedIDs").val() + "," + $(this).attr('id') : $(this).attr('id'));
                } else {
                    var ids = $("#hdnSelectedIDs").val().replace($(this).attr('id'), "");
                    var newArray = $.map(ids, function (v) {
                        return v === "" ? null : v;
                    });
                    $("#hdnSelectedIDs").val(ids.join());
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="pull-right">
        <asp:Button ID="btnLoadReleaseDates" class="btn btn-primary" runat="server" Text="Load Selected Quantities" />
        <asp:Button ID="btnClose" runat="server" CssClass="btn btn-default" Text="Close" style="margin-left:5px;" OnClientClick="return Close();" />
        
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Estimated Close-date list
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="row" id="divUCError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>
                <p>
                    <asp:Label ID="lblUCError" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <asp:UpdatePanel ID="UpdatePanelBuild" runat="server" class="table-responsive" ChildrenAsTriggers="true">
                <ContentTemplate>
                    <asp:GridView ID="gvItems" Width="1250" runat="server" AllowPaging="true" PageSize="10" AutoGenerateColumns="false" CssClass="table table-bordered table-striped" UseAccessibleHeader="true" ShowHeaderWhenEmpty="true">
                        <PagerStyle CssClass="gridview-paging" />
                        <PagerSettings FirstPageText="<<" LastPageText=">>" Mode="NumericFirstLast" PageButtonCount="5" />
                        <Columns>
                            <asp:BoundField DataField="numItemCode" Visible="false" />
                            <asp:BoundField DataField="numOppID" Visible="false" />
                            <asp:BoundField DataField="numOppItemID" Visible="false" />
                            <asp:BoundField DataField="numWarehouseItemID" Visible="false" />
                            <asp:BoundField DataField="bitKit" Visible="false" />
                            <asp:BoundField DataField="bitAssembly" Visible="false" />
                            <asp:BoundField DataField="fltUOMConversionFactor" Visible="false" />
                            <asp:BoundField DataField="vcItemName" HeaderText="Item Name" />
                            <asp:BoundField DataField="vcCustomer" HeaderText="Customer" />
                            <asp:BoundField DataField="vcOppName" HeaderText="Sales Opportunity" />
                            <asp:BoundField DataField="dtEstimatedClosedDate" HeaderText="Est. Close Date" />
                            <asp:BoundField DataField="vcTotalProgress" HeaderText="Total Progress" />
                            <asp:TemplateField HeaderText="Forecast Units">
                                <ItemTemplate>
                                    <asp:PlaceHolder ID="plhOrderRelease" runat="server"></asp:PlaceHolder>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="numPurchasedQty" HeaderText="Purchased" />
                            <asp:BoundField DataField="vcUOM" HeaderText="UOM" />
                        </Columns>
                    </asp:GridView>
                    <asp:HiddenField ID="hdnDemandForecastID" runat="server" />
                    <asp:HiddenField ID="hdnSelectedIDs" runat="server" />
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
