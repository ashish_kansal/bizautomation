﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmNewDemandForecast.aspx.vb" Inherits=".frmNewDemandForecast" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #tblDemandForecast td {
            padding: 2px;
        }

        #resizeDiv {
            min-width: 720px;
        }

        .cont2 {
            background: #fff;
            border: 1px solid #C7D2E4;
            padding: 0px;
        }

        .cont1 {
            background: #E8ECF9;
            border: 1px solid #C7D2E4;
        }
    </style>
    <script type="text/javascript">
        function AddClassification() {
            var selectedItem = $("#lbAvailableClassification > option:selected");

            if (selectedItem.length > 0) {
                selectedItem.remove().appendTo("#lbSelectedClassification");

                $("#lbAvailableClassification option:first-child").attr("selected", true);
                $("#lbSelectedClassification option:last-child").attr("selected", true);
            }
            else {
                alert("Select item classification");
            }
        }

        function RemoveClassification() {
            var selectedItem = $("#lbSelectedClassification > option:selected");

            if (selectedItem.length > 0) {
                selectedItem.remove().appendTo("#lbAvailableClassification");

                $("#lbAvailableClassification option:first-child").attr("selected", true);
                $("#lbSelectedClassification option:last-child").attr("selected", true);
            }
            else {
                alert("Select item classification");
            }
        }

        function AddGroup() {
            var selectedItem = $("#lbAvailableGroup > option:selected");

            if (selectedItem.length > 0) {
                selectedItem.remove().appendTo("#lblSelectedGroup");

                $("#lbAvailableGroup option:first-child").attr("selected", true);
                $("#lblSelectedGroup option:last-child").attr("selected", true);
            }
            else {
                alert("Select item group");
            }
        }

        function RemoveGroup() {
            var selectedItem = $("#lblSelectedGroup > option:selected");

            if (selectedItem.length > 0) {
                selectedItem.remove().appendTo("#lbAvailableGroup");

                $("#lbAvailableGroup option:first-child").attr("selected", true);
                $("#lblSelectedGroup option:last-child").attr("selected", true);
            }
            else {
                alert("Select item group.");
            }
        }

        function AddWarehouse() {
            var selectedItem = $("#lbAvailableWarehouse > option:selected");

            if (selectedItem.length > 0) {
                selectedItem.remove().appendTo("#lblSelectedWarehouse");

                $("#lbAvailableWarehouse option:first-child").attr("selected", true);
                $("#lblSelectedWarehouse option:last-child").attr("selected", true);
            }
            else {
                alert("Select warehouse");
            }
        }

        function RemoveWarehouse() {
            var selectedItem = $("#lblSelectedWarehouse > option:selected");

            if (selectedItem.length > 0) {
                selectedItem.remove().appendTo("#lbAvailableWarehouse");

                $("#lbAvailableWarehouse option:first-child").attr("selected", true);
                $("#lblSelectedWarehouse option:last-child").attr("selected", true);
            }
            else {
                alert("Select warehouse");
            }
        }

        function GetSelectedValues() {
            $("#hdnItemClassification").val($("#lbSelectedClassification > option").map(function () { return this.value; }).get().join(','));
            $("#hdnItemGroup").val($("#lblSelectedGroup > option").map(function () { return this.value; }).get().join(','));
            $("#hdnWarehouse").val($("#lblSelectedWarehouse > option").map(function () { return this.value; }).get().join(','));
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div style="float: right; margin-right: 10px;">
        <asp:Button ID="btnSaveClose" runat="server" CssClass="button" Text="Save & Close" OnClientClick="GetSelectedValues();" />
        <asp:Button ID="btnClose" runat="server" CssClass="button" Text="Close" OnClientClick="return Close();" />
    </div>
    <div style="margin-left: 10px; margin-top: 5px; font-weight: bold; color: red">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
   Item Filter
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <table id="tblDemandForecast" cellpadding="0" cellspacing="0">
        <tr>
            <td class="cont1">
                <table width="100%">
                    <tr>
                        <td style="float: left">
                            <b style="font-weight: bold; font-size: 14px">Filter 1. </b><b>Select item classifications to include</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="float: left">
                            <table>
                                <tr>
                                    <td style="vertical-align: top;">
                                        <table width="180">
                                            <tr>
                                                <td style="font-weight: bold; font-size: 12px; text-align: center">Available
                                                </td>
                                            </tr>
                                            <tr style="height: 100%">
                                                <td>
                                                    <asp:ListBox ID="lbAvailableClassification" runat="server" Height="100" Width="180" SelectionMode="Single"></asp:ListBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="80" style="text-align: center">
                                        <input type="button" id="btnAddClassification" class="button" value="Add >" style="margin-bottom: 10px" onclick="AddClassification();" />
                                        <input type="button" id="btnRemoveClassification" class="button" value="< Remove" style="margin-top: 10px" onclick="RemoveClassification();" />
                                    </td>
                                    <td style="vertical-align: top;">
                                        <table width="180">
                                            <tr>
                                                <td style="font-weight: bold; font-size: 12px; text-align: center">Selected
                                                </td>
                                            </tr>
                                            <tr style="height: 100%">
                                                <td>
                                                    <asp:ListBox ID="lbSelectedClassification" runat="server" Height="100" Width="180" SelectionMode="Single"></asp:ListBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="cont2">
                <table width="100%">
                    <tr>
                        <td style="float: left">
                            <b style="font-weight: bold; font-size: 14px">Filter 2. </b><b style="font-weight: bold;">Select item groups to include</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="float: left">
                            <table>
                                <tr>
                                    <td style="vertical-align: top;">
                                        <table width="180">
                                            <tr>
                                                <td style="font-weight: bold; font-size: 12px; text-align: center">Available
                                                </td>
                                            </tr>
                                            <tr style="height: 100%">
                                                <td>
                                                    <asp:ListBox ID="lbAvailableGroup" runat="server" Height="100" Width="180" SelectionMode="Single"></asp:ListBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="80" style="text-align: center">
                                        <input type="button" id="btnAddGroup" class="button" value="Add >" style="margin-bottom: 10px" onclick="AddGroup();" />
                                        <input type="button" id="btnRemoveGroup" class="button" value="< Remove" style="margin-top: 10px" onclick="RemoveGroup();" />
                                    </td>
                                    <td style="vertical-align: top;">
                                        <table width="180">
                                            <tr>
                                                <td style="font-weight: bold; font-size: 12px; text-align: center">Selected
                                                </td>
                                            </tr>
                                            <tr style="height: 100%">
                                                <td>
                                                    <asp:ListBox ID="lblSelectedGroup" runat="server" Height="100" Width="180" SelectionMode="Single"></asp:ListBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td class="cont1">
                <table width="100%">
                    <tr>
                        <td style="float: left">
                            <b style="font-weight: bold; font-size: 14px">Filter 3. </b><b style="font-weight: bold;">Select warehouse locations that items should belong to</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="float: left">
                            <table>
                                <tr>
                                    <td style="vertical-align: top;">
                                        <table width="180">
                                            <tr>
                                                <td style="font-weight: bold; font-size: 12px; text-align: center">Available
                                                </td>
                                            </tr>
                                            <tr style="height: 100%">
                                                <td>
                                                    <asp:ListBox ID="lbAvailableWarehouse" runat="server" Height="100" Width="180" SelectionMode="Single"></asp:ListBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="80" style="text-align: center">
                                        <input type="button" id="btnAddWarehouse" class="button" value="Add >" style="margin-bottom: 10px" onclick="AddWarehouse();" />
                                        <input type="button" id="btnRemoveWarehouse" class="button" value="< Remove" style="margin-top: 10px" onclick="RemoveWarehouse()" />
                                    </td>
                                    <td style="vertical-align: top;">
                                        <table width="180">
                                            <tr>
                                                <td style="font-weight: bold; font-size: 12px; text-align: center">Selected
                                                </td>
                                            </tr>
                                            <tr style="height: 100%">
                                                <td>
                                                    <asp:ListBox ID="lblSelectedWarehouse" runat="server" Height="100" Width="180" SelectionMode="Single"></asp:ListBox>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
       <%-- <tr>
            <td class="cont2">
                <table width="100%">
                    <tr>
                        <td style="float: left">
                            <b style="font-weight: bold; font-size: 14px">Step 4. </b><b>Which historical analysis pattern do you wish to use to forecast demand forecast?</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="float: left">
                            <table>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <asp:DropDownList ID="ddlAnalysisPatternLastWeek" runat="server"></asp:DropDownList>
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkBasedOnLastYear" runat="server" Text="Based on sales from last year" Font-Bold="true" />
                                                    <br />
                                                    <div style="font-style: italic; font-size: 10px">
                                                        (e.g. If “Sales for last week” is selected analysis will be based on data from last week a year ago)
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>--%>
       <%-- <tr>
            <td class="cont1">
                <table width="100%">
                    <tr>
                        <td style="float: left" colspan="2">
                            <b style="font-weight: bold; font-size: 14px">Step 5. </b><b>Release / Estimated Close Date Range</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td style="float: left;">
                                        <asp:DropDownList ID="ddlDemandForecastDays" runat="server"></asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:CheckBox ID="chkIncludeOpenReleaseDates" Font-Bold="true" runat="server" Text="Include Open Release / Estimated Close Dates before “Today”." />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>--%>
    </table>

    <asp:HiddenField ID="hdnItemClassification" runat="server" />
    <asp:HiddenField ID="hdnItemGroup" runat="server" />
    <asp:HiddenField ID="hdnWarehouse" runat="server" />
    <asp:HiddenField ID="hdnDFID" runat="server" />
</asp:Content>
