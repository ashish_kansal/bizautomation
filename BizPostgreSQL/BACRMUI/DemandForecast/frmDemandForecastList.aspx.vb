﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.DemandForecast
Imports Telerik.Web.UI

Public Class frmDemandForecastList
    Inherits BACRMPage

#Region "Member Variables"
    Private objDemandForecast As DemandForecast
#End Region

#Region "Constructor"
    Sub New()
        objDemandForecast = New DemandForecast
    End Sub
#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            If Not Page.IsPostBack Then
                PersistTable.Load()

                If PersistTable.Count > 0 Then
                    txtCurrrentPage.Text = CCommon.ToString(PersistTable(PersistKey.CurrentPage))
                End If

                BindGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)

        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub BindGrid()
        Try
            objDemandForecast.DomainID = Session("DomainID")

            objDemandForecast.PageSize = CCommon.ToInteger(Session("PagingRows"))
            objDemandForecast.CurrentPage = IIf(String.IsNullOrEmpty(txtCurrrentPage.Text.Trim()) Or txtCurrrentPage.Text.Trim() = "0", 1, CCommon.ToInteger(txtCurrrentPage.Text.Trim()))

            Dim dt As DataTable = objDemandForecast.GetByDomain()

            bizPager.PageSize = Session("PagingRows")
            bizPager.RecordCount = objDemandForecast.TotalRecords

            If CCommon.ToInteger(txtCurrrentPage.Text) > 0 Then
                bizPager.CurrentPageIndex = txtCurrrentPage.Text
            Else
                If bizPager.RecordCount = 0 Then
                    bizPager.CustomInfoHTML = "Showing records 0 to 0 of 0"
                End If
            End If

            PersistTable.Clear()
            PersistTable.Add(PersistKey.CurrentPage, IIf(dt.Rows.Count > 0, txtCurrrentPage.Text, "0"))
            PersistTable.Save()


            dgDemandForecast.DataSource = dt
            dgDemandForecast.DataBind()

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplayError(ByVal ex As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ex
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch exception As Exception

        End Try

    End Sub

#End Region

#Region "Event Handlers"

    Protected Sub bizPager_PageChanged(sender As Object, e As EventArgs)
        Try
            txtCurrrentPage.Text = bizPager.CurrentPageIndex
            BindGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Try
            If String.IsNullOrEmpty(hdnSelectedItems.Value) Then
                Page.ClientScript.RegisterStartupScript(GetType(Page), "clientScript", "alert('Please select atleast one item to delete.');", True)
            Else
                objDemandForecast.DomainID = Session("DomainID")
                objDemandForecast.SelectedIDs = hdnSelectedItems.Value
                objDemandForecast.Delete()
                BindGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        Finally
            hdnSelectedItems.Value = ""
        End Try
    End Sub

    Private Sub dgDemandForecast_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles dgDemandForecast.RowDataBound
        Try
            If e.Row.RowType = DataControlRowType.Header Then
                ScriptManager.GetCurrent(Me).RegisterAsyncPostBackControl(TryCast(e.Row.FindControl("ChkSelectAll"), CheckBox))
            End If

            If e.Row.RowType = DataControlRowType.DataRow Then
                ScriptManager.GetCurrent(Me).RegisterAsyncPostBackControl(TryCast(e.Row.FindControl("ChkSelect"), CheckBox))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region
    
    
    
    
End Class