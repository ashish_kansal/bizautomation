﻿Imports BACRM.BusinessLogic.Item
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.DemandForecast
Imports System.Collections.Generic
Imports System.Linq

Public Class frmNewDemandForecast
    Inherits BACRMPage

#Region "Member Variables"
    Private objItems As CItems
    Private objDemandForecast As DemandForecast
    Private objDemandForecastEdit As DemandForecast
#End Region

#Region "Constructor"
    Sub New()
        objCommon = New CCommon
        objItems = New CItems
        objDemandForecast = New DemandForecast
    End Sub
#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblMessage.Text = ""

            If Not Page.IsPostBack Then
                If Not String.IsNullOrEmpty(GetQueryStringVal("DFID")) Then
                    hdnDFID.Value = GetQueryStringVal("DFID")
                    LoadData()
                End If

                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub LoadData()
        Try
            objDemandForecast.numDFID = CCommon.ToLong(hdnDFID.Value)
            objDemandForecast.DomainID = Session("DomainID")
            objDemandForecastEdit = objDemandForecast.GetByID()

            'Record which user wants to edit does not exist
            If objDemandForecastEdit.numDFID = -1 Then
                objDemandForecastEdit = Nothing
                lblMessage.Text = "Record which u want to edit does not exist."
                hdnDFID.Value = "-1"
            Else
                ' chkBasedOnLastYear.Checked = CCommon.ToBool(objDemandForecastEdit.bitLastYear)
                ' chkIncludeOpenReleaseDates.Checked = CCommon.ToBool(objDemandForecastEdit.bitIncludeOpenReleaseDates)
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BindData()
        Try
            BindItemClassification()
            BindItemGroup()
            BindWarehouseLocation()
            ' BindAnalysisPattern()
            'BindForecastDays()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BindItemClassification()
        Try
            Dim dtItemClassification As DataTable = objCommon.GetMasterListItems(36, Session("DomainID"))

            If Not dtItemClassification Is Nothing AndAlso dtItemClassification.Rows.Count > 0 Then
                dtItemClassification.DefaultView.Sort = "vcData asc"
                lbAvailableClassification.DataSource = dtItemClassification
                lbAvailableClassification.DataTextField = "vcData"
                lbAvailableClassification.DataValueField = "numListItemID"
                lbAvailableClassification.DataBind()
            End If

            If Not objDemandForecastEdit Is Nothing AndAlso
                Not objDemandForecastEdit.listItemClassification Is Nothing AndAlso
                objDemandForecastEdit.listItemClassification.Count > 0 Then

                Dim listItem As ListItem

                For Each i As Long In objDemandForecastEdit.listItemClassification
                    listItem = lbAvailableClassification.Items.FindByValue(i)

                    If Not listItem Is Nothing Then
                        lbAvailableClassification.Items.Remove(listItem)
                        lbSelectedClassification.Items.Add(listItem)
                    End If
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BindItemGroup()
        Try
            objItems.DomainID = Session("DomainID")
            objItems.ItemCode = 0
            Dim dtItemGroup As DataTable = objItems.GetItemGroups.Tables(0)

            If Not dtItemGroup Is Nothing AndAlso dtItemGroup.Rows.Count > 0 Then
                dtItemGroup.DefaultView.Sort = "vcItemGroup asc"
                lbAvailableGroup.DataSource = dtItemGroup
                lbAvailableGroup.DataTextField = "vcItemGroup"
                lbAvailableGroup.DataValueField = "numItemGroupID"
                lbAvailableGroup.DataBind()
            End If

            If Not objDemandForecastEdit Is Nothing AndAlso
                Not objDemandForecastEdit.listItemGroup Is Nothing AndAlso
                objDemandForecastEdit.listItemGroup.Count > 0 Then

                Dim listItem As ListItem

                For Each i As Long In objDemandForecastEdit.listItemGroup
                    listItem = lbAvailableGroup.Items.FindByValue(i)

                    If Not listItem Is Nothing Then
                        lbAvailableGroup.Items.Remove(listItem)
                        lblSelectedGroup.Items.Add(listItem)
                    End If
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BindWarehouseLocation()
        Try
            objItems.DomainID = Session("DomainID")
            Dim dtWarehouse As DataTable = objItems.GetWareHouses()

            If Not dtWarehouse Is Nothing AndAlso dtWarehouse.Rows.Count > 0 Then
                dtWarehouse.DefaultView.Sort = "vcWarehouse asc"
                lbAvailableWarehouse.DataSource = dtWarehouse
                lbAvailableWarehouse.DataTextField = "vcWarehouse"
                lbAvailableWarehouse.DataValueField = "numWarehouseID"
                lbAvailableWarehouse.DataBind()
            End If

            If Not objDemandForecastEdit Is Nothing AndAlso
                Not objDemandForecastEdit.listWarehouse Is Nothing AndAlso
                objDemandForecastEdit.listWarehouse.Count > 0 Then

                Dim listItem As ListItem

                For Each i As Long In objDemandForecastEdit.listWarehouse
                    listItem = lbAvailableWarehouse.Items.FindByValue(i)

                    If Not listItem Is Nothing Then
                        lbAvailableWarehouse.Items.Remove(listItem)
                        lblSelectedWarehouse.Items.Add(listItem)
                    End If
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    'Private Sub BindAnalysisPattern()
    '    Try
    '        objDemandForecast.DomainID = Session("DomainID")
    '        Dim dtAnalysisPattern As DataTable = objDemandForecast.GetAnalysisPattern()

    '        If Not dtAnalysisPattern Is Nothing AndAlso dtAnalysisPattern.Rows.Count > 0 Then
    '            ddlAnalysisPatternLastWeek.DataSource = dtAnalysisPattern
    '            ddlAnalysisPatternLastWeek.DataTextField = "vcAnalysisPattern"
    '            ddlAnalysisPatternLastWeek.DataValueField = "numDFAPID"
    '            ddlAnalysisPatternLastWeek.DataBind()
    '        End If

    '        If Not objDemandForecastEdit Is Nothing Then
    '            ddlAnalysisPatternLastWeek.SelectedValue = CCommon.ToString(objDemandForecastEdit.numAnalysisPattern)
    '        End If
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Sub

    ''Private Sub BindForecastDays()
    ''    Try
    ''        objDemandForecast.DomainID = Session("DomainID")
    ''        Dim dtForecastDays As DataTable = objDemandForecast.GetForecastDays()

    ''        If Not dtForecastDays Is Nothing AndAlso dtForecastDays.Rows.Count > 0 Then
    ''            ddlDemandForecastDays.DataSource = dtForecastDays
    ''            ddlDemandForecastDays.DataTextField = "vcDays"
    ''            ddlDemandForecastDays.DataValueField = "numDFDaysID"
    ''            ddlDemandForecastDays.DataBind()
    ''        End If

    ''        If Not objDemandForecastEdit Is Nothing Then
    ''            ddlDemandForecastDays.SelectedValue = CCommon.ToString(objDemandForecastEdit.numForecastDays)
    ''        End If
    ''    Catch ex As Exception
    ''        Throw
    ''    End Try
    ''End Sub

#End Region

#Region "Event Handlers"

    Private Sub btnSaveClose_Click(sender As Object, e As EventArgs) Handles btnSaveClose.Click
        Try
            objDemandForecast = New DemandForecast
            objDemandForecast.numDFID = CCommon.ToLong(hdnDFID.Value)
            objDemandForecast.DomainID = Session("DomainID")
            objDemandForecast.UserCntID = Session("UserContactID")
            objDemandForecast.bitIncludeOpenReleaseDates = True 'chkIncludeOpenReleaseDates.Checked

            Dim listItemClassification As New List(Of Long)
            Dim listItemGroup As New List(Of Long)
            Dim listWarehouse As New List(Of Long)

            If Not String.IsNullOrEmpty(hdnItemClassification.Value) Then
                listItemClassification = hdnItemClassification.Value.Split(",").[Select](Function(x) Long.Parse(x)).ToList()
            End If
            objDemandForecast.listItemClassification = listItemClassification

            If Not String.IsNullOrEmpty(hdnItemGroup.Value) Then
                listItemGroup = hdnItemGroup.Value.Split(",").[Select](Function(x) Long.Parse(x)).ToList()
            End If
            objDemandForecast.listItemGroup = listItemGroup

            If Not String.IsNullOrEmpty(hdnWarehouse.Value) Then
                listWarehouse = hdnWarehouse.Value.Split(",").[Select](Function(x) Long.Parse(x)).ToList()
            End If
            objDemandForecast.listWarehouse = listWarehouse
            objDemandForecast.bitLastYear = True 'chkBasedOnLastYear.Checked
            objDemandForecast.numAnalysisPattern = 0 'CCommon.ToInteger(ddlAnalysisPatternLastWeek.SelectedValue)

            objDemandForecast.numForecastDays = 0 'CCommon.ToInteger(ddlDemandForecastDays.SelectedValue)

            objDemandForecast.Save()

            Page.ClientScript.RegisterStartupScript(GetType(Page), "clientScript", "opener.location.reload(true); self.close();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

#End Region

End Class