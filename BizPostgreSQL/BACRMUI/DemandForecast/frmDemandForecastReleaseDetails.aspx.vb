﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.DemandForecast

Public Class frmDemandForecastReleaseDetails
    Inherits BACRMPage

#Region "Memeber Variables"

    Private totalOrderedQty As Decimal
    Private totalRemainingQtyTOShip As Decimal

#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblError.Text = ""
            divError.Style.Add("display", "none")

            If Not Page.IsPostBack Then
                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub BindData()
        Try
            Dim objDemandForecast As New DemandForecast
            objDemandForecast.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            objDemandForecast.DomainID = Session("DomainID")
            objDemandForecast.ReleaseDate = Convert.ToDateTime(GetQueryStringVal("releaseDate"))
            Dim dt As DataTable = objDemandForecast.GetDemandForecastReleaseDateDetails(CCommon.ToLong(GetQueryStringVal("itemCode")), CCommon.ToLong(GetQueryStringVal("warehouseItemID")))

            totalOrderedQty = dt.Compute("Sum(numOrderedQty)", String.Empty)
            totalRemainingQtyTOShip = dt.Compute("Sum(numQtyToShip)", String.Empty)

            rptRecords.DataSource = dt
            rptRecords.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplayError(ByVal errorMessage As String)
        Try
            lblError.Text = errorMessage
            divError.Style.Add("display", "")
        Catch ex As Exception
            'DO NOT THROW ERROR
        End Try
    End Sub

#End Region

#Region "Event Handlers"

    Protected Sub rptRecords_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        Try
            If e.Item.ItemType = ListItemType.Footer Then
                DirectCast(e.Item.FindControl("lblTotalOrderedQtyFooter"), Label).Text = totalOrderedQty
                DirectCast(e.Item.FindControl("lblTotalRemainingQtyFooter"), Label).Text = totalRemainingQtyTOShip
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

End Class