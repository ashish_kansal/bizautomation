﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.Item
Imports System.Collections.Generic
Imports BACRM.BusinessLogic.Opportunities
Imports Telerik.Web.UI
Imports BACRM.BusinessLogic.Accounting
Imports BACRM.BusinessLogic.Admin
Imports BACRM.BusinessLogic.Workflow
Imports BACRM.BusinessLogic.Contacts

Public Class frmDemandForecastPO
    Inherits BACRMPage


#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not Page.IsPostBack Then
                If Not Session("DFItemsToPurchase") Is Nothing Then
                    Dim objCommon As New CCommon
                    objCommon.sb_FillComboFromDBwithSel(ddlOrderStatus, 176, Session("DomainID"), "2")

                    Dim objItem As New CItems
                    objItem.DomainID = Session("DomainID")
                    ddlWarehouse.DataSource = objItem.GetWareHouses
                    ddlWarehouse.DataTextField = "vcWareHouse"
                    ddlWarehouse.DataValueField = "numWareHouseID"
                    ddlWarehouse.DataBind()

                    BindItems()
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub BindItems()
        Try
            gvItems.DataSource = DirectCast(Session("DFItemsToPurchase"), List(Of OpportunityItemsReleaseDates))
            gvItems.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CreateOrder(ByVal DivisionID As Long, ByVal ContactID As Long, ByVal CompanyName As String)
        Try
            Using objTransctionScope As New System.Transactions.TransactionScope(Transactions.TransactionScopeOption.Required, New System.Transactions.TransactionOptions() With {.IsolationLevel = Transactions.IsolationLevel.ReadCommitted, .Timeout = Transactions.TransactionManager.MaximumTimeout})
                Dim i As Integer
                Dim objOpportunity As New MOpportunity
                objOpportunity.OpportunityId = 0
                objOpportunity.OppType = 2
                If hdnOppType.Value = "1" Then
                    objOpportunity.DealStatus = objOpportunity.EnmDealStatus.DealWon
                Else
                    objOpportunity.DealStatus = objOpportunity.EnmDealStatus.DealOpen
                End If
                objOpportunity.OpportunityName = CompanyName & "-PO-" & Format(Now(), "MMMM")
                objOpportunity.ContactID = ContactID
                objOpportunity.DivisionID = DivisionID
                objOpportunity.UserCntID = Session("UserContactID")
                objOpportunity.EstimatedCloseDate = Now
                objOpportunity.DomainID = Session("DomainId")
                objOpportunity.Active = 1
                objOpportunity.SourceType = 1
                objOpportunity.OrderStatus = CCommon.ToLong(ddlOrderStatus.SelectedValue)

                Dim objAdmin As New CAdmin
                Dim dtBillingTerms As DataTable
                objAdmin.DivisionID = DivisionID
                dtBillingTerms = objAdmin.GetBillingTerms()

                objOpportunity.boolBillingTerms = IIf(dtBillingTerms.Rows(0).Item("tintBillingTerms") = 1, True, False)
                objOpportunity.BillingDays = dtBillingTerms.Rows(0).Item("numBillingDays")
                objOpportunity.boolInterestType = IIf(dtBillingTerms.Rows(0).Item("tintInterestType") = 1, True, False)
                objOpportunity.Interest = dtBillingTerms.Rows(0).Item("fltInterest")

                'objOpportunity.strItems = dsTemp.GetXml
                objOpportunity.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                Dim lngOppID As Long = objOpportunity.Save()(0)
                objOpportunity.OpportunityId = lngOppID

                ''Added By Sachin Sadhu||Date:29thApril12014
                ''Purpose :To Added Opportunity data in work Flow queue based on created Rules
                ''          Using Change tracking
                Dim objWfA As New Workflow()
                objWfA.DomainID = Session("DomainID")
                objWfA.UserCntID = Session("UserContactID")
                objWfA.RecordID = lngOppID
                objWfA.SaveWFOrderQueue()
                'end of code

                Dim dg As GridViewRow
                For i = 0 To gvItems.Rows.Count - 1
                    dg = gvItems.Rows(i)

                    Dim ddlVendor As DropDownList
                    ddlVendor = dg.FindControl("ddlVendor")

                    Dim strValue As String() = ddlVendor.SelectedValue.Split(CChar("~"))

                    If CCommon.ToLong(strValue(0)) = DivisionID Then
                        Dim units As Double = CCommon.ToDouble(DirectCast(dg.FindControl("txtUnits"), TextBox).Text)

                        Dim js As New Script.Serialization.JavaScriptSerializer()
                        Dim listReleaseDates As List(Of ReleaseDates) = js.Deserialize(Of List(Of ReleaseDates))(DirectCast(dg.FindControl("hdnRequiredDate"), HiddenField).Value)

                        For Each releaseDate As ReleaseDates In listReleaseDates.Where(Function(x) x.IsSelected)
                            objOpportunity.ItemCode = CCommon.ToLong(DirectCast(dg.FindControl("hdnItemCode"), HiddenField).Value.Trim)
                            If units > CCommon.ToDouble(releaseDate.Quantity) Then
                                objOpportunity.Units = CCommon.ToInteger(releaseDate.Quantity)
                                units = units - CCommon.ToInteger(releaseDate.Quantity)
                            Else
                                objOpportunity.Units = units
                                units = 0
                            End If
                            objOpportunity.UnitPrice = CCommon.ToDecimal(CType(dg.FindControl("txtUnitCost"), TextBox).Text)
                            objOpportunity.Desc = ""
                            objOpportunity.ItemName = CCommon.ToString(DirectCast(dg.FindControl("lblItemName"), Label).Text.Trim)
                            objOpportunity.Notes = ""
                            objOpportunity.ModelName = ""
                            objOpportunity.ItemThumbImage = ""
                            objOpportunity.ItemType = CCommon.ToString(DirectCast(dg.FindControl("hdnCharItemType"), HiddenField).Value.Trim)
                            objOpportunity.Dropship = False
                            objOpportunity.WarehouseItemID = CCommon.ToLong(DirectCast(dg.FindControl("hdnWarehouseItemID"), HiddenField).Value)
                            objOpportunity.numUOM = 0
                            objOpportunity.ProjectID = 0
                            objOpportunity.ClassID = 0
                            objOpportunity.ItemRequiredDate = Convert.ToDateTime(releaseDate.ReleaseDate)
                            objOpportunity.UpdateOpportunityItems()

                            If units <= 0 Then
                                Exit For
                            End If
                        Next

                        If units > 0 Then
                            objOpportunity.ItemCode = CCommon.ToLong(DirectCast(dg.FindControl("hdnItemCode"), HiddenField).Value.Trim)
                            objOpportunity.Units = units
                            objOpportunity.UnitPrice = CCommon.ToDecimal(CType(dg.FindControl("txtUnitCost"), TextBox).Text)
                            objOpportunity.Desc = ""
                            objOpportunity.ItemName = CCommon.ToString(DirectCast(dg.FindControl("lblItemName"), Label).Text.Trim)
                            objOpportunity.Notes = ""
                            objOpportunity.ModelName = ""
                            objOpportunity.ItemThumbImage = ""
                            objOpportunity.ItemType = CCommon.ToString(DirectCast(dg.FindControl("hdnCharItemType"), HiddenField).Value.Trim)
                            objOpportunity.Dropship = False
                            objOpportunity.WarehouseItemID = CCommon.ToLong(DirectCast(dg.FindControl("hdnWarehouseItemID"), HiddenField).Value)
                            objOpportunity.numUOM = 0
                            objOpportunity.ProjectID = 0
                            objOpportunity.ClassID = 0
                            objOpportunity.ItemRequiredDate = DateTime.UtcNow.AddMinutes(-1 * CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")))
                            objOpportunity.UpdateOpportunityItems()
                        End If
                    End If
                Next

                If hdnOppType.Value = "1" Then
                    'Fix of Bug id 638 -by chintan, Update inventory counts
                    Dim objCustomReport As New BACRM.BusinessLogic.Reports.CustomReports
                    objCustomReport.DynamicQuery = " PERFORM USP_UpdatingInventoryonCloseDeal '" & objOpportunity.OpportunityId & "',0,'" & Session("UserContactID") & "'"
                    objCustomReport.ExecuteDynamicSql()
                End If

                Dim objContact As New CContacts
                objContact.AddressType = CContacts.enmAddressType.BillTo
                objContact.DomainID = CCommon.ToLong(Session("DomainID"))
                objContact.RecordID = CCommon.ToLong(Session("UserDivisionID"))
                objContact.AddresOf = CContacts.enmAddressOf.Organization
                objContact.byteMode = 2
                Dim dtAddress As DataTable = objContact.GetAddressDetail()

                objOpportunity.BillToType = 0
                If Not dtAddress Is Nothing AndAlso dtAddress.Rows.Count > 0 Then
                    objOpportunity.BillToAddressID = CCommon.ToLong(dtAddress.Rows(0)("numAddressID"))
                End If

                objContact.AddressType = CContacts.enmAddressType.ShipTo
                dtAddress = objContact.GetAddressDetail()
                objOpportunity.ShipToType = 0
                objOpportunity.ShipToAddressID = CCommon.ToLong(dtAddress.Rows(0)("numAddressID"))
                objOpportunity.UpdateOpportunityLinkingDtls()

                objTransctionScope.Complete()
            End Using
        Catch ex As Exception
            If ex.Message = "NOT_ALLOWED" Then
                Exit Sub
            Else
                Throw ex
            End If
        End Try
    End Sub

    Private Sub BindVendorAddresses(ByVal vendorID As Long, ByRef ddlVendorWarehouses As DropDownList)
        Try
            ddlVendorWarehouses.Items.Clear()

            Dim objItems As New CItems
            objItems.DomainID = Session("DomainID")
            objItems.VendorID = vendorID
            objItems.byteMode = 2
            Dim dtVendorsWareHouse As DataTable = objItems.GetVendorsWareHouse

            If dtVendorsWareHouse.Rows.Count > 0 Then
                ddlVendorWarehouses.DataSource = dtVendorsWareHouse
                ddlVendorWarehouses.DataTextField = "vcFullAddress"
                ddlVendorWarehouses.DataValueField = "numAddressID"
                ddlVendorWarehouses.DataBind()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BindVendorShipmentMethod(ByVal vendorID As Long, ByVal vendorWarehouse As Long, ByRef ddlVendorShipmentMethod As DropDownList)
        Try
            ddlVendorShipmentMethod.Items.Clear()

            Dim objItems As New CItems
            objItems.DomainID = Session("DomainID")
            objItems.ShipAddressId = vendorWarehouse
            objItems.VendorID = vendorID
            objItems.byteMode = 2
            Dim dtVendorShipmentMethod As DataTable = objItems.GetVendorShipmentMethod

            If Not dtVendorShipmentMethod Is Nothing AndAlso dtVendorShipmentMethod.Rows.Count > 0 Then
                ddlVendorShipmentMethod.DataSource = dtVendorShipmentMethod
                ddlVendorShipmentMethod.DataTextField = "ShipmentMethod"
                ddlVendorShipmentMethod.DataValueField = "vcListValue"
                ddlVendorShipmentMethod.DataBind()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplayError(ByVal errorMessage As String)
        Try
            lblError.Text = errorMessage
            divError.Style.Add("display", "")
        Catch ex As Exception
            'DO NOT THROW ERROR
        End Try
    End Sub

#End Region

#Region "Event handlers"

    Protected Sub ddlVendor_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            Dim ddlVendor As DropDownList = DirectCast(sender, DropDownList)
            Dim grv As GridViewRow = DirectCast(ddlVendor.NamingContainer, GridViewRow)

            If Not grv Is Nothing Then
                Dim ddlVendorWarehouses As DropDownList = DirectCast(grv.FindControl("ddlVendorWarehouses"), DropDownList)
                Dim ddlVendorShipmentMethod As DropDownList = DirectCast(grv.FindControl("ddlVendorShipmentMethod"), DropDownList)
                Dim lblLeadTimeDays As Label = DirectCast(grv.FindControl("lblLeadTimeDays"), Label)
                Dim lblMinOrderQty As Label = DirectCast(grv.FindControl("lblMinOrderQty"), Label)
                Dim txtUnitCost As TextBox = DirectCast(grv.FindControl("txtUnitCost"), TextBox)

                txtUnitCost.Text = CCommon.GetDecimalFormat(ddlVendor.SelectedValue.Split("~")(2))

                BindVendorAddresses(CCommon.ToLong(ddlVendor.SelectedValue.Split("~")(0)), ddlVendorWarehouses:=ddlVendorWarehouses)

                If ddlVendorWarehouses.Items.Count > 0 Then
                    lblMinOrderQty.Text = ddlVendor.SelectedValue.Split("~")(3)
                    BindVendorShipmentMethod(CCommon.ToLong(ddlVendor.SelectedValue.Split("~")(0)), CCommon.ToLong(ddlVendorWarehouses.SelectedValue), ddlVendorShipmentMethod:=ddlVendorShipmentMethod)

                    If ddlVendorShipmentMethod.Items.Count > 0 Then
                        ddlVendorShipmentMethod.Attributes.Add("onchange", "ChangeLeadTimeDays('" & lblLeadTimeDays.ClientID & "','" & ddlVendorShipmentMethod.ClientID & "') ")
                        lblLeadTimeDays.Text = ddlVendorShipmentMethod.SelectedValue.Split("~")(1)
                    Else
                        lblLeadTimeDays.Text = "-"
                    End If
                Else
                    lblMinOrderQty.Text = "-"
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub ddlVendorShipmentMethod_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            Dim ddlVendorShipmentMethod As DropDownList = DirectCast(sender, DropDownList)
            Dim grv As GridViewRow = DirectCast(ddlVendorShipmentMethod.NamingContainer, GridViewRow)

            If Not grv Is Nothing Then
                Dim lblLeadTimeDays As Label = DirectCast(grv.FindControl("lblLeadTimeDays"), Label)

                If ddlVendorShipmentMethod.Items.Count > 0 Then
                    lblLeadTimeDays.Text = ddlVendorShipmentMethod.SelectedValue.Split("~")(1)
                Else
                    lblLeadTimeDays.Text = "-"
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub ddlVendorWarehouses_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            Dim ddlVendorWarehouses As DropDownList = DirectCast(sender, DropDownList)
            Dim grv As GridViewRow = DirectCast(ddlVendorWarehouses.NamingContainer, GridViewRow)

            If Not grv Is Nothing Then
                Dim ddlVendor As DropDownList = DirectCast(grv.FindControl("ddlVendor"), DropDownList)
                Dim lblLeadTimeDays As Label = DirectCast(grv.FindControl("lblLeadTimeDays"), Label)
                Dim ddlVendorShipmentMethod As DropDownList = DirectCast(grv.FindControl("ddlVendorShipmentMethod"), DropDownList)

                BindVendorShipmentMethod(CCommon.ToLong(ddlVendor.SelectedValue.Split("~")(0)), ddlVendorWarehouses.SelectedValue, ddlVendorShipmentMethod)

                If ddlVendorShipmentMethod.Items.Count > 0 Then
                    lblLeadTimeDays.Text = ddlVendorShipmentMethod.SelectedValue.Split("~")(1)
                Else
                    lblLeadTimeDays.Text = "-"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Protected Sub gvItems_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        Try
            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim drv As OpportunityItemsReleaseDates = DirectCast(e.Row.DataItem, OpportunityItemsReleaseDates)
                Dim ddlVendor As DropDownList = DirectCast(e.Row.FindControl("ddlVendor"), DropDownList)
                Dim ddlVendorWarehouses As DropDownList = DirectCast(e.Row.FindControl("ddlVendorWarehouses"), DropDownList)
                Dim ddlVendorShipmentMethod As DropDownList = DirectCast(e.Row.FindControl("ddlVendorShipmentMethod"), DropDownList)
                Dim txtUnitCost As TextBox = DirectCast(e.Row.FindControl("txtUnitCost"), TextBox)
                Dim txtUnits As TextBox = DirectCast(e.Row.FindControl("txtUnits"), TextBox)
                Dim lblLeadTimeDays As Label = DirectCast(e.Row.FindControl("lblLeadTimeDays"), Label)
                'Dim dtRequiredDate As RadDatePicker = DirectCast(e.Row.FindControl("dtRequiredDate"), RadDatePicker)
                Dim chkSelect As CheckBox = DirectCast(e.Row.FindControl("chkSelect"), CheckBox)

                chkSelect.Checked = True
                txtUnits.Text = String.Format("{0:#,##0}", CCommon.ToInteger(drv.Quantity))

                'If Convert.ToDateTime(drv.ReleaseDate) <> DateTime.MinValue Then
                '    dtRequiredDate.SelectedDate = Convert.ToDateTime(drv.ReleaseDate)
                'Else
                '    dtRequiredDate.SelectedDate = DateTime.UtcNow.AddMinutes(-1 * Session("ClientMachineUTCTimeOffset"))
                'End If

                Dim objItems As New CItems
                objItems.DomainID = Session("DomainID")
                objItems.ItemCode = CCommon.ToLong(drv.ItemCode)
                Dim dtVendors As DataTable = objItems.GetVendors

                For Each drVendor As DataRow In dtVendors.Rows
                    Dim listItem = New ListItem(drVendor.Item("Vendor").ToString(), drVendor.Item("numVendorID").ToString() & "~" & drVendor.Item("numContactId").ToString() & "~" & CCommon.GetDecimalFormat(drVendor.Item("monCostEach")) & "~" & drVendor.Item("intMinQty").ToString())
                    If drVendor("numVendorID") = drv.VendorID Then
                        listItem.Selected = True
                    End If

                    ddlVendor.Items.Add(listItem)
                Next

                If Not ddlVendor.SelectedItem Is Nothing Then
                    txtUnitCost.Text = CCommon.GetDecimalFormat(ddlVendor.SelectedValue.Split("~")(2))
                    BindVendorAddresses(CCommon.ToLong(ddlVendor.SelectedValue.Split("~")(0)), ddlVendorWarehouses:=ddlVendorWarehouses)

                    If ddlVendorWarehouses.Items.Count > 0 Then
                        BindVendorShipmentMethod(CCommon.ToLong(ddlVendor.SelectedValue.Split("~")(0)), CCommon.ToLong(ddlVendorWarehouses.SelectedValue), ddlVendorShipmentMethod:=ddlVendorShipmentMethod)

                        If ddlVendorShipmentMethod.Items.Count > 0 Then
                            If Not ddlVendorShipmentMethod.Items.FindByValue(drv.ShipmentMethod) Is Nothing Then
                                ddlVendorShipmentMethod.Items.FindByValue(drv.ShipmentMethod).Selected = True
                            End If

                            lblLeadTimeDays.Text = ddlVendorShipmentMethod.SelectedValue.Split("~")(1)
                        Else
                            lblLeadTimeDays.Text = "-"
                        End If
                    End If
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Try
            Dim objJournal As New JournalEntry
            If ChartOfAccounting.GetDefaultAccount("CG", Session("DomainID")) = 0 Then
                DisplayError("Please Set Default COGs account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save.")
                Exit Sub
            End If

            If ChartOfAccounting.GetDefaultAccount("PC", Session("DomainID")) = 0 Then
                DisplayError("Please Set Default Purchase Clearing account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save")
                Exit Sub
            End If

            If ChartOfAccounting.GetDefaultAccount("PV", Session("DomainID")) = 0 Then
                DisplayError("Please Set Default Purchase Price Variance account from ""Administration->Global Settings->Accounting->Default Accounts"" To Save")
                Exit Sub
            End If

            'validate item income,asset , cogs account are mapped based on item type
            Dim objItem As New CItems
            Dim objOppBizDocs As New OppBizDocs
            For Each dgGridItem As GridViewRow In gvItems.Rows
                If DirectCast(dgGridItem.FindControl("chkSelect"), CheckBox).Checked Then
                    If Not objItem.ValidateItemAccount(CCommon.ToLong(DirectCast(dgGridItem.FindControl("hdnItemCode"), HiddenField).Value), Session("DomainID"), 2) = True Then
                        DisplayError("Please Set Income,Asset,COGs(Expense) Account for " & CCommon.ToLong(DirectCast(dgGridItem.FindControl("hdnItemCode"), HiddenField).Value) & " from Administration->Inventory->Item Details")
                        Exit Sub
                    End If

                    Dim ddlVendor As DropDownList
                    ddlVendor = dgGridItem.FindControl("ddlVendor")

                    If ddlVendor.SelectedItem Is Nothing Then
                        DisplayError("Please select vendor for all selected item(s).")
                        Exit Sub
                    End If

                    Dim strValue As String() = ddlVendor.SelectedValue.Split(CChar("~"))
                    If objOppBizDocs.ValidateARAP(CCommon.ToLong(strValue(0)), 0, Session("DomainID")) = False Then
                        DisplayError("Please Set AR and AP Relationship from ""Administration->Global Settings->Accounting->Accounts for RelationShip"" To Save")
                        Exit Sub
                    End If

                    Dim txtUnits As TextBox = DirectCast(dgGridItem.FindControl("txtUnits"), TextBox)
                    Dim units As Double = 0
                    If Not Double.TryParse(txtUnits.Text, units) Then
                        DisplayError("Enter units to purchase for all selected item(s).")
                        Exit Sub
                    ElseIf units = 0 Then
                        DisplayError("Enter units to purchase for all selected item(s).")
                        Exit Sub
                    End If

                    If chkWarehouse.Checked Then
                        'CHECK ITEM HAS WAREHOUSE MAPPED WITH SELECTED ADDRESS
                        objItem.ItemCode = CCommon.ToLong(DirectCast(dgGridItem.FindControl("hdnItemCode"), HiddenField).Value)
                        objItem.DomainID = CCommon.ToLong(Session("DomainID"))
                        objItem.WarehouseID = CCommon.ToLong(ddlWarehouse.SelectedValue)
                        Dim dtWarehouse As DataTable = objItem.GetItemWarehouseLocations()

                        If Not dtWarehouse Is Nothing AndAlso dtWarehouse.Rows.Count > 0 Then
                            DirectCast(dgGridItem.FindControl("hdnWarehouseItemID"), HiddenField).Value = dtWarehouse.Rows(0)("numWareHouseItemID")
                        Else
                            DisplayError("Some item(s) do not have selected Ship-to Warehouse.")
                            Exit Sub
                        End If
                    End If
                End If
            Next

            Dim dr As DataRow
            Dim gridRow As GridViewRow

            'Create table to group item by selected vendor
            Dim dtItem As New DataTable
            dtItem.Columns.Add("ItemCode")
            dtItem.Columns.Add("Units")
            dtItem.Columns.Add("UnitPrice")
            dtItem.Columns.Add("Desc")
            dtItem.Columns.Add("ItemName")
            dtItem.Columns.Add("ModelName")
            dtItem.Columns.Add("ItemThumbImage")
            dtItem.Columns.Add("ItemType")
            dtItem.Columns.Add("Dropship")
            dtItem.Columns.Add("WarehouseItemID")
            dtItem.Columns.Add("numVendorID")
            dtItem.Columns.Add("numVendorContactId")
            dtItem.Columns.Add("VendorCompanyName")
            dtItem.Columns.Add("numUOM")
            dtItem.Columns.Add("txtNotes")
            dtItem.Columns.Add("dtRequiredDate")


            Dim ErrorMessage As String = ""
            For i = 0 To gvItems.Rows.Count - 1
                gridRow = gvItems.Rows(i)

                If DirectCast(gridRow.FindControl("chkSelect"), CheckBox).Checked Then
                    Dim ddlVendor As DropDownList
                    ddlVendor = gridRow.FindControl("ddlVendor")

                    If ddlVendor.Items.Count > 0 Then
                        Dim units As Double = CCommon.ToDouble(DirectCast(gridRow.FindControl("txtUnits"), TextBox).Text)

                        Dim js As New Script.Serialization.JavaScriptSerializer()
                        Dim listReleaseDates As List(Of ReleaseDates) = js.Deserialize(Of List(Of ReleaseDates))(DirectCast(gridRow.FindControl("hdnRequiredDate"), HiddenField).Value)

                        For Each releaseDate As ReleaseDates In listReleaseDates.Where(Function(x) x.IsSelected)
                            dr = dtItem.NewRow

                            dr("ItemCode") = CCommon.ToLong(DirectCast(gridRow.FindControl("hdnItemCode"), HiddenField).Value)

                            If units > CCommon.ToDouble(releaseDate.Quantity) Then
                                dr("Units") = CCommon.ToDouble(releaseDate.Quantity)
                                units = units - CCommon.ToDouble(releaseDate.Quantity)
                            Else
                                dr("Units") = units
                                units = 0
                            End If

                            dr("UnitPrice") = CCommon.ToDecimal(CType(gridRow.FindControl("txtUnitCost"), TextBox).Text)
                            dr("Desc") = ""
                            dr("ItemName") = CCommon.ToString(DirectCast(gridRow.FindControl("lblItemName"), Label).Text.Trim)
                            dr("ModelName") = ""
                            dr("ItemThumbImage") = ""
                            dr("ItemType") = CCommon.ToString(DirectCast(gridRow.FindControl("hdnCharItemType"), HiddenField).Value.Trim)
                            dr("Dropship") = False
                            dr("WarehouseItemID") = CCommon.ToLong(DirectCast(gridRow.FindControl("hdnWarehouseItemID"), HiddenField).Value)
                            dr("txtNotes") = ""
                            dr("numUOM") = 0
                            dr("dtRequiredDate") = Convert.ToDateTime(releaseDate.ReleaseDate)

                            Dim strValue As String() = ddlVendor.SelectedValue.Split(CChar("~"))

                            dr("numVendorID") = CCommon.ToLong(strValue(0))
                            dr("numVendorContactId") = CCommon.ToLong(strValue(1))
                            dr("VendorCompanyName") = ddlVendor.SelectedItem.Text

                            dtItem.Rows.Add(dr)

                            If units <= 0 Then
                                Exit For
                            End If
                        Next

                        If units > 0 Then
                            dr = dtItem.NewRow

                            dr("ItemCode") = CCommon.ToLong(DirectCast(gridRow.FindControl("hdnItemCode"), HiddenField).Value)
                            dr("Units") = units
                            dr("UnitPrice") = CCommon.ToDecimal(CType(gridRow.FindControl("txtUnitCost"), TextBox).Text)
                            dr("Desc") = ""
                            dr("ItemName") = CCommon.ToString(DirectCast(gridRow.FindControl("lblItemName"), Label).Text.Trim)
                            dr("ModelName") = ""
                            dr("ItemThumbImage") = ""
                            dr("ItemType") = CCommon.ToString(DirectCast(gridRow.FindControl("hdnCharItemType"), HiddenField).Value.Trim)
                            dr("Dropship") = False
                            dr("WarehouseItemID") = CCommon.ToLong(DirectCast(gridRow.FindControl("hdnWarehouseItemID"), HiddenField).Value)
                            dr("txtNotes") = ""
                            dr("numUOM") = 0
                            dr("dtRequiredDate") = DateTime.UtcNow.AddMinutes(-1 * CCommon.ToInteger(Session("ClientMachineUTCTimeOffset")))

                            Dim strValue As String() = ddlVendor.SelectedValue.Split(CChar("~"))

                            dr("numVendorID") = CCommon.ToLong(strValue(0))
                            dr("numVendorContactId") = CCommon.ToLong(strValue(1))
                            dr("VendorCompanyName") = ddlVendor.SelectedItem.Text

                            dtItem.Rows.Add(dr)
                        End If
                    Else
                        If String.IsNullOrEmpty(ErrorMessage) Then
                            ErrorMessage = "PO can not be created, Your option is to select a vender for following item(s):"
                        Else
                            ErrorMessage += "<br/>=>" & CCommon.ToString(DirectCast(gridRow.FindControl("lblItemName"), Label).Text.Trim)
                        End If
                    End If
                End If
            Next

            If Not String.IsNullOrEmpty(ErrorMessage) Then
                DisplayError(ErrorMessage)
                Exit Sub
            ElseIf dtItem.Rows.Count = 0 Then
                DisplayError("Select atleast one item.")
                Exit Sub
            End If

            Dim dtVendor As DataTable
            dtVendor = dtItem.DefaultView.ToTable(True, "numVendorID")

            For Each drRow As DataRow In dtVendor.Select()

                Dim drItemColl() As DataRow
                'Create single order for all item which are referenced to same vendor
                drItemColl = dtItem.Select("(numVendorID = '" & drRow.Item("numVendorID") & "')")

                'Create Order against primary vendors
                CreateOrder(drItemColl(0).Item("numVendorID"), drItemColl(0).Item("numVendorContactId"), drItemColl(0).Item("VendorCompanyName"))
            Next

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "CloseWindow", "Close();", True)
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

    Private Class ReleaseDates
        Public Property ReleaseDate As String
        Public Property Quantity As Double
        Public Property IsSelected As Boolean
    End Class

End Class