﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.DemandForecast

Namespace BACRM.UserInterface.DemandForecast

    Public Class frmDemandForecastEstimatedCloseDateBuild
        Inherits BACRMPage

#Region "Page Events"

        Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
            Try
                ManageError("", False)

                If Not Page.IsPostBack Then
                    hdnDemandForecastID.Value = CCommon.ToString(GetQueryStringVal("DFID"))
                    hdnSelectedIDs.Value = CCommon.ToString(Session("DFEstimatedCloseBuild"))
                    BindData()
                End If
            Catch ex As Exception
                ManageError(ex.Message, True)
            End Try
        End Sub

#End Region

#Region "Private Methods"
        Private Sub ManageError(ByVal errorMessage As String, ByVal isVisible As Boolean)
            lblUCError.Text = errorMessage
            divUCError.Visible = isVisible
        End Sub
#End Region

#Region "Public Methods"

        Public Sub BindData()
            Try
                Dim objDemandForeCast As New BusinessLogic.DemandForecast.DemandForecast()
                objDemandForeCast.DomainID = CCommon.ToLong(Session("DomainID"))
                objDemandForeCast.numDFID = hdnDemandForecastID.Value
                Dim dt As DataTable = objDemandForeCast.GetTotalProgressItems()

                If Not dt Is Nothing Then
                    gvItems.DataSource = dt
                    gvItems.DataBind()
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

#End Region

#Region "Event Handlers"

        Private Sub btnLoadReleaseDates_Click(sender As Object, e As EventArgs) Handles btnLoadReleaseDates.Click
            Try
                Session("DFEstimatedCloseBuild") = hdnSelectedIDs.Value
                ScriptManager.RegisterClientScriptBlock(UpdatePanelBuild, UpdatePanelBuild.GetType(), "LoadData", "window.opener.document.getElementById('btnEstimatedCloseBuild').click(); window.close();", True)
            Catch ex As Exception
                ManageError(ex.Message, True)
            End Try
        End Sub

        Private Sub gvItems_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles gvItems.RowDataBound
            Try
                If e.Row.RowType = DataControlRowType.DataRow Then
                    Dim drv As DataRowView = DirectCast(e.Row.DataItem, DataRowView)
                    Dim plhOrderRelease As PlaceHolder = DirectCast(e.Row.FindControl("plhOrderRelease"), PlaceHolder)
                    Dim plhItemRelease As PlaceHolder = DirectCast(e.Row.FindControl("plhItemRelease"), PlaceHolder)
                    Dim fltUOMConversionFactor As Double = CCommon.ToDouble(drv("fltUOMConversionFactor"))

                    If Not plhOrderRelease Is Nothing AndAlso CCommon.ToDouble(drv("numOrderReleaseQty")) > 0 Then
                        Dim table As New HtmlGenericControl("table")
                        Dim tr As New HtmlGenericControl("tr")
                        Dim td1 As New HtmlGenericControl("td")
                        Dim td2 As New HtmlGenericControl("td")

                        Dim chk As New CheckBox
                        chk.ID = "chk_OrderRelease_" & drv("numItemCode") & "_" & drv("numWarehouseItemID") & "_" & drv("bitAssembly") & "_" & drv("bitKit") & "_" & drv("numOppID") & "_" & drv("numOppItemID") & "_0_" & "_" & (drv("numOrderReleaseQty") * fltUOMConversionFactor) & "_" & fltUOMConversionFactor
                        If hdnSelectedIDs.Value.Contains(chk.ID) Then
                            chk.Checked = True
                        End If
                        td1.Controls.Add(chk)

                        Dim label As New Label
                        label.ID = "lblOrderReleaseQty"
                        label.Text = " " & CCommon.ToString(drv("numOrderReleaseQty"))
                        td2.Controls.Add(label)

                        tr.Controls.Add(td1)
                        tr.Controls.Add(td2)
                        table.Controls.Add(tr)
                        plhOrderRelease.Controls.Add(table)
                    End If
                End If
            Catch ex As Exception
                ManageError(ex.Message, True)
            End Try
        End Sub

        Private Sub gvItems_PageIndexChanging(sender As Object, e As GridViewPageEventArgs) Handles gvItems.PageIndexChanging
            Try
                gvItems.PageIndex = e.NewPageIndex
                BindData()
            Catch ex As Exception
                ManageError(ex.Message, True)
            End Try
        End Sub

#End Region
        
    End Class

End Namespace