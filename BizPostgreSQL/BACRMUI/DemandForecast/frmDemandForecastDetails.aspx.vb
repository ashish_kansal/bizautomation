﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.DemandForecast

Public Class frmDemandForecastDetails
    Inherits BACRMPage

#Region "Memeber Variables"

    Private totalOrderedQty As Decimal
    Private totalRemainingQtyTOShip As Decimal
    Private totalOppoForecastQty As Decimal

#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblError.Text = ""
            divError.Style.Add("display", "none")

            If Not Page.IsPostBack Then
                BindData()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub BindData()
        Try
            Dim objDemandForecast As New DemandForecast
            objDemandForecast.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            objDemandForecast.DomainID = Session("DomainID")
            objDemandForecast.numForecastDays = CCommon.ToInteger(GetQueryStringVal("forecastDayes"))
            objDemandForecast.AnalysisPattern = CCommon.ToInteger(GetQueryStringVal("analysisDays"))
            objDemandForecast.IsBasedOnLastYear = CCommon.ToBool(GetQueryStringVal("isLastYear"))
            objDemandForecast.OpportunityPercentComplete = CCommon.ToInteger(GetQueryStringVal("opportunityPercentComplete"))
            Dim dt As DataTable = objDemandForecast.GetDemandForecastRecords(CCommon.ToShort(GetQueryStringVal("type")), CCommon.ToLong(GetQueryStringVal("itemCode")), CCommon.ToLong(GetQueryStringVal("warehouseItemID")))

            totalOrderedQty = dt.Compute("Sum(numOrderedQty)", String.Empty)
            totalRemainingQtyTOShip = dt.Compute("Sum(numQtyToShip)", String.Empty)
            totalOppoForecastQty = dt.Compute("Sum(numOpportunityForecastQty)", String.Empty)

            rptRecords.DataSource = dt
            rptRecords.DataBind()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplayError(ByVal errorMessage As String)
        Try
            lblError.Text = errorMessage
            divError.Style.Add("display", "")
        Catch ex As Exception
            'DO NOT THROW ERROR
        End Try
    End Sub

#End Region

#Region "Event Handlers"

    Protected Sub rptRecords_ItemDataBound(sender As Object, e As RepeaterItemEventArgs)
        Try
            If e.Item.ItemType = ListItemType.Header Then
                DirectCast(e.Item.FindControl("lblTotalOrderedQtyHeader"), Label).Text = totalOrderedQty
                DirectCast(e.Item.FindControl("lblTotalTemainingdQtyHeader"), Label).Text = totalRemainingQtyTOShip
                If CCommon.ToString(GetQueryStringVal("type")) = "3" Then
                    DirectCast(e.Item.FindControl("lblTotalForecastQtyHeader"), Label).Text = totalOppoForecastQty
                Else
                    e.Item.FindControl("thHead1TotalOpp").Visible = False
                    e.Item.FindControl("thHead2TotalOpp").Visible = False
                End If
            ElseIf e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                If CCommon.ToString(GetQueryStringVal("type")) <> "3" Then
                    e.Item.FindControl("tdOppForecast").Visible = False
                End If
            ElseIf e.Item.ItemType = ListItemType.Footer Then
                DirectCast(e.Item.FindControl("lblTotalOrderedQtyFooter"), Label).Text = totalOrderedQty
                DirectCast(e.Item.FindControl("lblTotalTemainingdQtyFooter"), Label).Text = totalRemainingQtyTOShip
                If CCommon.ToString(GetQueryStringVal("type")) <> "3" Then
                    e.Item.FindControl("thFootTotalOpp").Visible = False
                Else
                    DirectCast(e.Item.FindControl("lblTotalForecastQtyFooter"), Label).Text = totalOppoForecastQty
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(ex.Message)
        End Try
    End Sub

#End Region
    
End Class