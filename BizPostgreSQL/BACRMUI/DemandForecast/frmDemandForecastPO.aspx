﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmDemandForecastPO.aspx.vb" Inherits=".frmDemandForecastPO" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="../javascript/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="../CSS/jquery-ui.css" />
    <script type="text/javascript">
        function OpenConfirmWindow(oppType) {
            $("[id$=hdnOppType]").val(oppType);

            if (oppType == 1) {
                $("#spanOppType").html("<b>Purchase Orders</b>");
            } else {
                $("#spanOppType").html("<b>Purchase Requisitions</b> (Purchase Opportunities)");
            }

            $("[id$=divConfirm]").show();

            return false;
        }

        function HideConfirmWindow() {
            $("[id$=divConfirm]").hide();
            return false;
        }

        function CreateOrder() {
            $("[id$=divConfirm]").hide();
            $("[id$=btnSave]").click();
            return true;
        }

        function OpenRequiredDatePopup(image) {
            $("#tbodyReleaeDates").html("");
            $("#hdnIdOfRequiredDateHiddenField").val($(image).closest("tr").find("[id$=hdnRequiredDate]")[0].name);
            $("#lblQtyToPurchaseRPopup").text(parseFloat($(image).closest("tr").find("[id$=txtUnits]")[0].value));

            var selectedQty = 0;
            var releaseDates = JSON.parse($(image).closest("tr").find("[id$=hdnRequiredDate]")[0].value);

            if (releaseDates != null && releaseDates.length > 0) {
                releaseDates.forEach(function (objDate) {
                    var date = objDate.ReleaseDate;
                    var qty = parseFloat(objDate.Quantity);

                    selectedQty += (objDate.IsSelected ? qty : 0)

                    $("#tbodyReleaeDates").append("<tr><td><input type='text' id='datepicker' class='form-control' value='" + date + "' /></td><td><input type='text' class='form-control' id='txtUnits' onChange='return UpdateSelectedQtyTotal();' value='" + qty + "' /></td><td><input type='checkbox' id='chkSelect' onChange='return UpdateSelectedQtyTotal();' checked='" + (objDate.IsSelected ? "checked" : "") + "'/></td></tr>")
                });
            }

            $("#lblQtySelectedToPurchaseRPopup").text(selectedQty);
            $("#datepickerheader").datepicker({ dateFormat: 'mm/dd/yy' });
            $("#datepicker").datepicker({ dateFormat: 'mm/dd/yy' });
            $("#divReleaseDates").modal("show");

            return false;
        }

        function AddReleaseDates() {
            if (!Date.parse($("#datepickerheader").val())) {
                alert("Select required date");
                $("#datepickerheader").focus();
            } else if (parseFloat($("#txtUnitsHeader").val()) <= 0) {
                alert("Select quantity");
                $("#txtUnitsHeader").focus();
            }
            else {
                var date = $("#datepickerheader").val();
                var qty = parseFloat($("#txtUnitsHeader").val());

                $("#tbodyReleaeDates").append("<tr><td><input type='text' id='datepicker' class='form-control' value='" + date + "' /></td><td><input type='text' class='form-control' id='txtUnits' onChange='return UpdateSelectedQtyTotal();' value='" + qty + "' /></td><td><input type='checkbox' id='chkSelect' checked='checked' onChange='return UpdateSelectedQtyTotal();'/></td></tr>")

                $("#datepickerheader").val("");
                $("#txtUnitsHeader").val("");

                UpdateSelectedQtyTotal();
            }

            return false;
        }

        function UpdateSelectedQtyTotal() {
            var selectedQty = 0;

            $("#tbodyReleaeDates tr").each(function (obj) {
                if ($(this).find("[id$=chkSelect]")[0].checked) {
                    selectedQty += parseFloat($(this).find("[id$=txtUnits]")[0].value);
                }
            });

            $("#lblQtySelectedToPurchaseRPopup").text(selectedQty);

            return false;
        }

        function SaveRelaseDateSelection() {
            var qtySelected = parseFloat($("#lblQtySelectedToPurchaseRPopup").text());
            var qtyToPurchase = parseFloat($("#lblQtyToPurchaseRPopup").text());

            if (qtySelected != qtyToPurchase) {
                alert("Selected quantity is not same as quantity to purchase");
                return false;
            } else {
                var objSelectedDates = [];
                var vcReleaseDates = "";

                $("#tbodyReleaeDates tr").each(function (obj) {
                    if ($(this).find("[id$=chkSelect]")[0].checked) {

                        requiredDate = {}
                        requiredDate["ReleaseDate"] = $(this).find("[id$=datepicker]")[0].value;
                        requiredDate["Quantity"] = parseFloat($(this).find("[id$=txtUnits]")[0].value);
                        requiredDate["IsSelected"] = true;

                        vcReleaseDates = (vcReleaseDates.length > 0 ? vcReleaseDates + ", " : "")  + ($(this).find("[id$=datepicker]")[0].value + " (" + parseFloat($(this).find("[id$=txtUnits]")[0].value).toString() + ")");

                        objSelectedDates.push(requiredDate);
                    }
                });

                $('[name="' + $("#hdnIdOfRequiredDateHiddenField").val() + '"]').closest("tr").find("[id$=liReleaseDates]")[0].innerText = vcReleaseDates;
                $('[name="' + $("#hdnIdOfRequiredDateHiddenField").val() + '"]').val(JSON.stringify(objSelectedDates));

                $("#divReleaseDates").modal("hide");

                return true;
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="row padbottom10">
        <div class="col-sm-12">
            <div class="pull-right">
                <asp:Button ID="btnCreateOpportunity" runat="server" Text="Create Purchase Requisition(s)" CssClass="btn btn-primary" OnClientClick="return OpenConfirmWindow(0);" />
                <asp:Button ID="btnCreateOrder" runat="server" Text="Create Purchase Order(s)" CssClass="btn btn-primary" OnClientClick="return OpenConfirmWindow(1);" />
                <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-primary" OnClientClick="return Close();" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Create Purchase Requisition(s) or Purchase Order(s)
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="row padbottom10" id="divError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>
                <p>
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
    <asp:UpdateProgress ID="UpdateProgress1" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <div class="form-group">
                        <label>
                            <asp:CheckBox runat="server" ID="chkWarehouse" />
                            Ship-to this Warehouse instead:</label>
                        <asp:DropDownList ID="ddlWarehouse" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label>P.O. Status:</label>
                        <asp:DropDownList ID="ddlOrderStatus" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="pull-right">
            </div>
        </div>
    </div>
    <div class="row">
        <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" ChildrenAsTriggers="true" runat="server" class="col-xs-12">
            <ContentTemplate>
                <div class="table-responsive">
                    <asp:GridView ID="gvItems" runat="server" UseAccessibleHeader="true" CssClass="table table-bordered table-striped" AutoGenerateColumns="false" OnRowDataBound="gvItems_RowDataBound">
                        <Columns>
                            <asp:TemplateField  HeaderText="Item Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblItemName" runat="server" Text='<%# Eval("ItemName")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="SKU" DataField="SKU" />
                            <asp:BoundField HeaderText="Attributes" DataField="Attributes" />
                            <asp:TemplateField HeaderText="Vendor" HeaderStyle-Width="250">
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlVendor" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged"></asp:DropDownList>
                                    <b>Min Order Qty:</b>
                                    <asp:Label ID="lblMinOrderQty" runat="server" Text=""></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Vendor Warehouse" HeaderStyle-Width="200">
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlVendorWarehouses" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlVendorWarehouses_SelectedIndexChanged"></asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Shipment Method" HeaderStyle-Width="200">
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlVendorShipmentMethod" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlVendorShipmentMethod_SelectedIndexChanged"></asp:DropDownList>

                                    <b>Lead Time:</b>
                                    <asp:Label ID="lblLeadTimeDays" runat="server" Text='-'></asp:Label>
                                    Days
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText="Ship-to Warehouse" DataField="Warehouse" />
                            <asp:TemplateField HeaderText="Buy (Eaches)" ItemStyle-Wrap="false" HeaderStyle-Width="80">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtUnits" runat="server" CssClass="form-control"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Cost (Eaches)" HeaderStyle-Width="120">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtUnitCost" runat="server" CssClass="form-control"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Required Date (qty)" ItemStyle-Wrap="true" HeaderStyle-Width="170">
                                <ItemTemplate>
                                    <ul class="list-inline">
                                        <li id="liReleaseDates" style="width:82%">
                                            <%# Eval("ReleaseDates")%>
                                        </li>
                                        <li style="width:15%; vertical-align:top">
                                            <img src="../images/ActionItem-32.gif" style="height:20px" onclick="return OpenRequiredDatePopup(this)" />
                                        </li>
                                    </ul>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-Width="25">
                                <HeaderTemplate>
                                    <asp:CheckBox ID="chkSelectAll" runat="server" onchange="SelectAll(this)" />
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkSelect" runat="server" />
                                    <asp:HiddenField ID="hdnItemCode" runat="server" Value='<%# Eval("ItemCode")%>' />
                                    <asp:HiddenField ID="hdnWarehouseItemID" runat="server" Value='<%# Eval("WarehouseItemID")%>' />
                                    <asp:HiddenField ID="hdnCharItemType" runat="server" Value='<%# Eval("ItemType")%>' />
                                    <asp:HiddenField ID="hdnRequiredDate" runat="server" Value='<%# Eval("RequiredDates")%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EmptyDataTemplate>
                            <b>No Data Available</b>
                        </EmptyDataTemplate>
                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="overlay modal" id="divConfirm" runat="server" style="display: none">
        <div class="overlayContent" style="color: #000; background-color: #FFF; width: 570px; border: 1px solid #ddd; margin: 100px auto !important;">
            <div class="dialog-body" style="max-height: 200px; padding:10px; text-align:center">
                <p>You’re about to create <span id="spanOppType"></span>  for all items listed.</p>
                <p>Are you sure you want to do this?</p>
            </div>
            <div class="modal-footer">
                <asp:HiddenField ID="hdnOppType" runat="server" />
                <asp:Button ID="btnSave" runat="server" style="display:none"></asp:Button>
                <button type="button" class="btn btn-primary pull-left" onclick="return CreateOrder();">Yep let’s do it</button>
                <button type="button" class="btn btn-primary" onclick="return HideConfirmWindow();">Nope please cancel</button>
            </div>
        </div>
    </div>

    <div class="modal fade" id="divReleaseDates" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Select Required Dates</h4>
                    <input type="hidden" id="hdnIdOfRequiredDateHiddenField" />
                </div>
                <div class="modal-body" style="max-height:550px; overflow-y:auto">
                    <div class="row padbottom10">
                        <div class="col-sm-12 col-md-6">
                            <div class="form-inline">
                                <label>Qty to purchase:</label>
                                <label id="lblQtyToPurchaseRPopup" class="font-bold"></label>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <div class="form-inline">
                                <label>Qty selected:</label>
                                <label id="lblQtySelectedToPurchaseRPopup" class="font-bold"></label>
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered table-responsive" id="tblReleaeDates">
                        <thead>
                            <tr>
                                <td>
                                    <input type='text' id='datepickerheader' class='form-control' placeholder="Required Date" />
                                </td>
                                <td>
                                    <input type='text' class='form-control' id='txtUnitsHeader' placeholder="Quantity" />
                                </td>
                                <td>
                                    <button type="button" class="btn btn-primary" onclick="return AddReleaseDates();">Add</button>
                                </td>
                            </tr>
                        </thead>
                        <tbody id="tbodyReleaeDates">

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onclick="return SaveRelaseDateSelection();">Save changes</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
</asp:Content>
