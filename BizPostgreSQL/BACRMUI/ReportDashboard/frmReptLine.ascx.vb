Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports
Imports System.Collections.Generic
Imports System.Linq.Expressions
Imports System.Reflection
Imports DotNet.Highcharts

Partial Public Class frmNewReptLine
    Inherits BACRMUserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private _DashBoardID As Long
    Public Property DashBoardID() As Long
        Get
            Try
                Return _DashBoardID
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As Long)
            Try
                _DashBoardID = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property

    Private _EditRept As Boolean
    Public Property EditRept() As Boolean
        Get
            Try
                Return _EditRept
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As Boolean)
            Try
                _EditRept = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property

    Private _Width As Integer
    Public Property Width() As Integer
        Get
            Try
                Return _Width
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As Integer)
            Try
                _Width = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property


    Private _objReportManage As Object
    Public Property objReportManage() As Object
        Get
            Try
                Return _objReportManage
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As Object)
            Try
                _objReportManage = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property

    Public Function CreateReport() As Boolean
        Try
            If _EditRept = True Then tdEdit.Visible = True
            'UltraChart1.Width = _Width
            dvTable.ID = _DashBoardID

            _objReportManage.DomainID = Session("DomainID")
            _objReportManage.DashBoardID = _DashBoardID
            Dim dtTable As DataTable

            dtTable = objReportManage.GetReportDashboardDTL

            lblHeader.Text = dtTable.Rows(0).Item("vcHeaderText")
            lblFooter.Text = dtTable.Rows(0).Item("vcFooterText")

            hplExportToExcel.NavigateUrl = "~/ReportDashboard/frmManageDashBoard.aspx?Move=EX&DID=" & _DashBoardID
            hplDelete.NavigateUrl = "../ReportDashboard/frmManageDashBoard.aspx?Move=X&DID=" & _DashBoardID
            hplEdit.NavigateUrl = "../ReportDashboard/frmAddDashboardReport.aspx?DID=" & _DashBoardID
            pnlChart.Attributes.Add("ondblclick", "return Redirect(" & dtTable.Rows(0).Item("numReportID") & ")")

            Try
                BindReport(dtTable.Rows(0).Item("numReportID"), dtTable.Rows(0).Item("vcXAxis"), dtTable.Rows(0).Item("vcYAxis"), dtTable.Rows(0).Item("vcAggType"))
            Catch ex As Exception

            End Try

            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Sub BindReport(ByVal lngReportID As Long, ByVal vcXAxis As String, ByVal vcYAxis As String, ByVal vcAggType As String)
        Try
            Dim ds As DataSet

            _objReportManage.ReportID = lngReportID
            _objReportManage.DomainID = Session("DomainID")
            _objReportManage.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            _objReportManage.UserCntID = Session("UserContactID")

            ds = _objReportManage.GetReportListMasterDetail

            Dim trHeader As TableHeaderRow = New TableHeaderRow()
            Dim tc As TableHeaderCell = New TableHeaderCell()

            Dim trRow As TableRow = New TableRow()
            Dim cell As New TableCell()

            If ds.Tables.Count > 0 Then

                Dim objReport As ReportObject = _objReportManage.GetDatasetToReportObject(ds)

                Dim dsColumnList As DataSet
                dsColumnList = _objReportManage.GetReportModuleGroupFieldMaster

                _objReportManage.textQuery = _objReportManage.GetReportQuery(_objReportManage, ds, objReport, False, True, dsColumnList, boolChart:=True)
                Dim dtData As DataTable = _objReportManage.USP_ReportQueryExecute()

                If ds.Tables(0).Rows(0)("tintReportType") = 3 Then 'KPI

                    Dim dataXAxis As New List(Of String)()
                    For i As Integer = 1 To dtData.Columns.Count - 1
                        dataXAxis.Add(dtData.Columns(i).Caption)
                    Next

                    Dim allSeries As New List(Of DotNet.Highcharts.Options.Series)()

                    For Each dr As DataRow In dtData.Rows

                        Dim dataYAxis = New List(Of Object)()
                        For i As Integer = 1 To dtData.Columns.Count - 1
                            dataYAxis.Add(dr(i))
                        Next

                        allSeries.Add(New DotNet.Highcharts.Options.Series() With { _
                             .Name = dr(0), _
                            .Data = New DotNet.Highcharts.Helpers.Data(dataYAxis.ToArray) _
                        })
                    Next

                    Dim chart1 As DotNet.Highcharts.Highcharts = New DotNet.Highcharts.Highcharts("chart" & _DashBoardID)

                    chart1.SetCredits(New DotNet.Highcharts.Options.Credits() With { _
                 .Enabled = False _
             })
                    chart1.InitChart(New DotNet.Highcharts.Options.Chart() With { _
                        .DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Line _
                    })

                    'Dim palette_colors() As System.Drawing.Color = {Color.FromName("#910000"), Color.FromName("#8bbc21"), Color.FromName("#2f7ed8"),
                    '                                           Color.FromName("#0d233a"), Color.FromName("#1aadce"), Color.FromName("#492970"),
                    '                                            Color.FromName("#f28f43"), Color.FromName("#77a1e5"), Color.FromName("#c42525")}

                    'chart1.SetOptions(New DotNet.Highcharts.Helpers.GlobalOptions() With { _
                    '            .Colors = palette_colors _
                    '        })

                    chart1.SetTitle(New DotNet.Highcharts.Options.Title() With {.Text = ""})

                    'Dim strColumn As String() = vcXAxis.Split("_")
                    'Dim dr1() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                    chart1.SetXAxis(New DotNet.Highcharts.Options.XAxis() With { _
                        .Categories = dataXAxis.ToArray,
                        .Labels = New DotNet.Highcharts.Options.XAxisLabels() With { _
                            .Align = DotNet.Highcharts.Enums.HorizontalAligns.Center, _
                            .Style = "font: 'normal 10px Verdana, sans-serif'"}, _
                            .Title = New DotNet.Highcharts.Options.XAxisTitle() With { _
                            .Text = ""} _
                    })

                    'strColumn = vcYAxis.Split("_")
                    'dr1 = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                    'Dim yAxis As String = "Record Count"

                    'If dr1.Length > 0 Then
                    '    yAxis = dr1(0)("vcFieldname")
                    'End If

                    chart1.SetYAxis(New DotNet.Highcharts.Options.YAxis() With { _
                       .Labels = New DotNet.Highcharts.Options.YAxisLabels() With { _
                       .Style = "font: 'normal 10px Verdana, sans-serif'"}, _
                       .Title = New DotNet.Highcharts.Options.YAxisTitle() With { _
                       .Text = ""} _
                   })

                    chart1.SetSeries(allSeries.[Select](Function(s) New DotNet.Highcharts.Options.Series() With { _
     .Name = s.Name, _
     .Data = s.Data _
}).ToArray())

                    ltrChart.Text = chart1.ToHtmlString()

                    '                    Dim chart1 As DotNet.Highcharts.Highcharts = New DotNet.Highcharts.Highcharts("chart")

                    '                    chart1.InitChart(New DotNet.Highcharts.Options.Chart() With { _
                    '     .DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Column _
                    '})

                    '                    'Dim palette_colors() As System.Drawing.Color = {Color.Bisque, Color.SteelBlue, Color.MediumSlateBlue, Color.Plum, Color.SeaShell, Color.SkyBlue, Color.Green,
                    '                    ' Color.Red}

                    '                    '            chart1.SetPlotOptions(New DotNet.Highcharts.Options.PlotOptions() With { _
                    '                    '   .Series = New DotNet.Highcharts.Options.PlotOptionsSeries() With { _
                    '                    '       .Shadow = True}, _
                    '                    '   .Spline = New DotNet.Highcharts.Options.PlotOptionsSpline() With { _
                    '                    '    .Marker = New DotNet.Highcharts.Options.PlotOptionsSplineMarker() With { _
                    '                    '        .Enabled = False} _
                    '                    '}})

                    '                    'chart1.SetOptions(New DotNet.Highcharts.Helpers.GlobalOptions() With { _
                    '                    '            .Colors = palette_colors _
                    '                    '        })

                    '                    chart1.SetTitle(New DotNet.Highcharts.Options.Title() With {.Text = "Combiner History"})

                    '                    chart1.SetXAxis(New DotNet.Highcharts.Options.XAxis() With { _
                    '                        .Categories = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", _
                    '                            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"},
                    '                        .Labels = New DotNet.Highcharts.Options.XAxisLabels() With { _
                    '         .Rotation = -45, _
                    '         .Align = DotNet.Highcharts.Enums.HorizontalAligns.Right, _
                    '         .Style = "font: 'normal 10px Verdana, sans-serif'"}, _
                    '     .Title = New DotNet.Highcharts.Options.XAxisTitle() With { _
                    '         .Text = "Time(Hour)"} _
                    '                    })

                    '                    'chart1.SetSeries(New DotNet.Highcharts.Options.Series() With { _
                    '                    '   .Color = Color.Red, .UpColor = Color.Red, .Data = New DotNet.Highcharts.Helpers.Data(New Object() {29.9, 71.5, 106.4, 129.2, 144.0, 176.0, _
                    '                    '        135.6, 148.5, 216.4, 194.1, 95.6, 54.4}) _
                    '                    '})

                    '                    'http://stackoverflow.com/questions/10035367/using-the-dotnet-highcharts-library-to-create-chart-based-on-datatable-result-fr/10111589#10111589
                    '                    Dim allSeries As New List(Of DotNet.Highcharts.Options.Series)()

                    '                    allSeries.Add(New DotNet.Highcharts.Options.Series() With { _
                    '                         .Name = "2012", .Color = Color.Green, .UpColor = Color.Green, _
                    '                        .Data = New DotNet.Highcharts.Helpers.Data(New Object() {29.9, 71.5, 106.4, 129.2, 144.0, 176.0, _
                    '                           135.6, 148.5, 216.4, 194.1, 95.6, 54.4}) _
                    '                    })

                    '                    allSeries.Add(New DotNet.Highcharts.Options.Series() With { _
                    '                       .Name = "2015", .Color = Color.Red, .UpColor = Color.Red, _
                    '                      .Data = New DotNet.Highcharts.Helpers.Data(New Object() {32, 65, 95, 135, 114.0, 156.0, _
                    '                           185.6, 108.5, 266.4, 164.1, 15.6, 94.4}) _
                    '                                                          })

                    '                    chart1.SetSeries(allSeries.[Select](Function(s) New DotNet.Highcharts.Options.Series() With { _
                    '     .Name = s.Name, _
                    '     .Data = s.Data _
                    '}).ToArray())

                    '                    ltrChart.Text = chart1.ToHtmlString()
                Else
                    Dim dr As DataRow

                    Dim query = From row In dtData.AsEnumerable() _
                Group row By GroupColumns = New With {Key .Key1 = row.Field(Of String)(vcXAxis)} Into GroupDetail = Group _
                Select New With { _
                    Key GroupColumns.Key1, _
                    .CountTotal = GroupDetail.Count(), _
                    .Detail = GroupDetail _
               }

                    If ds.Tables(0).Rows(0)("tintReportType") = 1 Then 'Summary
                        If objReport.SortColumn.Length > 0 Then
                            Dim AggField = From row In objReport.Aggregate Where row.Column = objReport.SortColumn
                            If AggField.Count > 0 Then
                                If objReport.SortDirection = "desc" Then
                                    query = From row In query.AsEnumerable() Order By (row.Detail.Sum(Function(r) r(objReport.SortColumn))) Descending Take (objReport.NoRows)
                                Else
                                    query = From row In query.AsEnumerable() Order By (row.Detail.Sum(Function(r) r(objReport.SortColumn))) Ascending Take (objReport.NoRows)
                                End If
                            End If
                        End If

                        If objReport.NoRows > 0 Then
                            query = query.Take(objReport.NoRows)
                        End If
                    End If

                    Dim dtChartTable As New DataTable
                    dtChartTable.Columns.Add("Column1", GetType(String))
                    dtChartTable.Columns.Add("Column2", GetType(Decimal))

                    Dim dataYAxis As Object() = New Object(query.Count - 1) {}
                    Dim dataXAxis As String() = New String(query.Count - 1) {}
                    Dim j As Integer = 0

                    For Each x In query
                        dr = dtChartTable.NewRow

                        Dim total As Integer = x.Detail.Count()
                        Dim Aggregate As Decimal = 0

                        Select Case vcAggType
                            Case "sum"
                                If total = 0 Then
                                    Aggregate = 0
                                Else
                                    Aggregate = x.Detail.Sum(Function(r) r(vcYAxis))
                                End If

                            Case "avg"
                                If total = 0 Then
                                    Aggregate = 0
                                Else
                                    Aggregate = x.Detail.Average(Function(r) r(vcYAxis))
                                End If

                            Case "max"
                                If total = 0 Then
                                    Aggregate = 0
                                Else
                                    Aggregate = x.Detail.Max(Function(r) r(vcYAxis))
                                End If

                            Case "min"
                                If total = 0 Then
                                    Aggregate = 0
                                Else
                                    Aggregate = x.Detail.Min(Function(r) r(vcYAxis))
                                End If

                            Case Else
                                Aggregate = total

                        End Select

                        dr("Column1") = IIf(x.Key1 = "", "-", x.Key1)
                        dr("Column2") = String.Format("{0:#.00}", total)
                        dtChartTable.Rows.Add(dr)

                        dataXAxis(j) = IIf(x.Key1 = "", "[None]", x.Key1)
                        dataYAxis(j) = String.Format("{0:#.00}", total)
                        j += 1
                    Next

                    Dim chart1 As DotNet.Highcharts.Highcharts = New DotNet.Highcharts.Highcharts("chart" & _DashBoardID)

                    chart1.SetCredits(New DotNet.Highcharts.Options.Credits() With { _
                 .Enabled = False _
             })

                    chart1.InitChart(New DotNet.Highcharts.Options.Chart() With { _
                        .DefaultSeriesType = DotNet.Highcharts.Enums.ChartTypes.Line _
                    })

                    'Dim palette_colors() As System.Drawing.Color = {Color.FromName("#910000"), Color.FromName("#8bbc21"), Color.FromName("#2f7ed8"),
                    '                                           Color.FromName("#0d233a"), Color.FromName("#1aadce"), Color.FromName("#492970"),
                    '                                            Color.FromName("#f28f43"), Color.FromName("#77a1e5"), Color.FromName("#c42525")}

                    'chart1.SetOptions(New DotNet.Highcharts.Helpers.GlobalOptions() With { _
                    '            .Colors = palette_colors _
                    '        })

                    chart1.SetTitle(New DotNet.Highcharts.Options.Title() With {.Text = ""})


                    Dim strColumn As String() = vcXAxis.Split("_")
                    Dim dr1() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                    chart1.SetXAxis(New DotNet.Highcharts.Options.XAxis() With { _
                        .Categories = dataXAxis.ToArray,
                        .Labels = New DotNet.Highcharts.Options.XAxisLabels() With { _
                            .Align = DotNet.Highcharts.Enums.HorizontalAligns.Center, _
                            .Style = "font: 'normal 10px Verdana, sans-serif'"}, _
                            .Title = New DotNet.Highcharts.Options.XAxisTitle() With { _
                            .Text = dr1(0)("vcFieldname")} _
                    })

                    strColumn = vcYAxis.Split("_")
                    dr1 = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                    Dim yAxis As String = "Record Count"

                    If dr1.Length > 0 Then
                        yAxis = dr1(0)("vcFieldname")
                    End If

                    chart1.SetLegend(New Options.Legend() With { _
                    .Enabled = False _
               })

                    chart1.SetYAxis(New DotNet.Highcharts.Options.YAxis() With { _
                       .Labels = New DotNet.Highcharts.Options.YAxisLabels() With { _
                       .Style = "font: 'normal 10px Verdana, sans-serif'"}, _
                       .Title = New DotNet.Highcharts.Options.YAxisTitle() With { _
                       .Text = yAxis} _
                   })

                    chart1.SetSeries(New DotNet.Highcharts.Options.Series() With { _
                      .Data = New DotNet.Highcharts.Helpers.Data(dataYAxis.ToArray) _
                    })

                    ltrChart.Text = chart1.ToHtmlString()
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

End Class