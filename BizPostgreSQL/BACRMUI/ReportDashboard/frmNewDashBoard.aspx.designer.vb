﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmNewDashBoard1
    
    '''<summary>
    '''lblTemplateName control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTemplateName As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''ddlDashboardTemplate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ddlDashboardTemplate As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''btnUpdateTemplate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnUpdateTemplate As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''btnCreateTemplate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCreateTemplate As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''btnNewReport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnNewReport As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''btnRefresh control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnRefresh As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''hdnDashboardTemplateID control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hdnDashboardTemplateID As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''phColumn1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phColumn1 As Global.System.Web.UI.WebControls.PlaceHolder
End Class
