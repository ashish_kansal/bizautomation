﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports
Public Class frmPrebuildFourColumn
    Inherits BACRMUserControl

#Region "Public Properties"

    Public Property DashBoardID As Long
    Public Property EditRept As Boolean
    Public Property Width As Integer
    Public Property objReportManage As CustomReportsManage
    Public Property DefaultReportID As Integer
    Public Property IsRefresh As Boolean

#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#End Region

#Region "Public Methods"

    Public Function CreateReport() As Boolean
        Try
            hplExportToExcel.NavigateUrl = "~/ReportDashboard/frmManageDashBoard.aspx?Move=EX&DID=" & _DashBoardID
            hplEdit.NavigateUrl = "../ReportDashboard/frmAddDashboardReport.aspx?DID=" & _DashBoardID
            hplDelete.NavigateUrl = "~/ReportDashboard/frmManageDashBoard.aspx?Move=X&DID=" & _DashBoardID

            _objReportManage.DomainID = Session("DomainID")
            _objReportManage.DashBoardID = _DashBoardID
            Dim dtReportData As DataTable = objReportManage.GetReportDashboardDTL

            If Not dtReportData Is Nothing AndAlso dtReportData.Rows.Count > 0 Then
                lblHeader.Text = CCommon.ToString(dtReportData.Rows(0).Item("vcHeaderText"))
                lblFooter.Text = CCommon.ToString(dtReportData.Rows(0).Item("vcFooterText"))
            End If


            objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
            objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
            objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))

            If DefaultReportID = CustomReportsManage.PrebuildReportEnum.TopSourcesOfSalesOrders Then
                imgReportImage.ImageUrl = "~/images/Icon/salesorder70x70.png"

                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.TopSourcesOfSalesOrders
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    rptReport.DataSource = ds.Tables(0)
                    rptReport.DataBind()
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10SalesOppotunityByRevenue Then
                imgReportImage.ImageUrl = "~/images/Icon/salesopportunity70x70.png"

                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10SalesOppotunityByRevenue
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    rptReport.DataSource = ds.Tables(0)
                    rptReport.DataBind()
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10SalesOppotunityByProgress Then
                imgReportImage.ImageUrl = "~/images/Icon/salesopportunity70x70.png"

                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10SalesOppotunityByProgress
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    rptReport.DataSource = ds.Tables(0)
                    rptReport.DataBind()
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10SalesOpportunityByPastDue Then
                imgReportImage.ImageUrl = "~/images/Icon/salesopportunity70x70.png"

                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10SalesOpportunityByPastDue
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    rptReport.DataSource = ds.Tables(0)
                    rptReport.DataBind()
                End If
            End If

            Return True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            divReportContent.Visible = False
            lblError.Visible = True
        End Try
    End Function

#End Region

#Region "Event Handlers"

    Private Sub rptReport_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptReport.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Header Then
                Dim lblHeader1 As Label = DirectCast(e.Item.FindControl("lblHeader1"), Label)
                Dim lblHeader2 As Label = DirectCast(e.Item.FindControl("lblHeader2"), Label)
                Dim lblHeader3 As Label = DirectCast(e.Item.FindControl("lblHeader3"), Label)

                If Not lblHeader1 Is Nothing Then
                    If DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10SalesOppotunityByRevenue Or _
                        DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10SalesOppotunityByProgress Or _
                        DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10SalesOpportunityByPastDue Then
                        lblHeader1.Text = "Name"
                    End If
                End If

                If Not lblHeader2 Is Nothing Then
                    If DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10SalesOppotunityByRevenue Or _
                        DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10SalesOppotunityByProgress Or _
                        DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10SalesOpportunityByPastDue Then
                        lblHeader2.Text = "Amount"
                    End If
                End If

                If Not lblHeader3 Is Nothing Then
                    If DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10SalesOppotunityByRevenue Or _
                        DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10SalesOppotunityByProgress Or _
                        DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10SalesOpportunityByPastDue Then
                        lblHeader3.Text = "Progress"
                    End If
                End If
            End If
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim hplTitle As HyperLink = DirectCast(e.Item.FindControl("hplTitle"), HyperLink)
                Dim lblTitle As Label = DirectCast(e.Item.FindControl("lblTitle"), Label)
                Dim lblValue1 As Label = DirectCast(e.Item.FindControl("lblValue1"), Label)
                Dim lblValue2 As Label = DirectCast(e.Item.FindControl("lblValue2"), Label)
                Dim item As DataRowView = e.Item.DataItem
                If DefaultReportID = CustomReportsManage.PrebuildReportEnum.TopSourcesOfSalesOrders Then
                    hplTitle.Visible = False
                    lblTitle.Text = CCommon.ToString(item("vcSource"))
                    lblTitle.Style.Add("font-weight", "600")
                    lblValue1.Text = String.Format("{0:#,##0.##}", CCommon.ToDouble(item("TotalOrdersPercent"))) & "%"
                    lblValue2.Text = String.Format("{0:$#,##0.##}", CCommon.ToDouble(item("TotalOrdersAmount")))
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10SalesOppotunityByRevenue Or _
                        DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10SalesOppotunityByProgress Or _
                        DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10SalesOpportunityByPastDue Then
                    hplTitle.Text = CCommon.ToString(item("vcPOppName"))
                    hplTitle.NavigateUrl = ResolveUrl(CCommon.ToString(item("URL")))
                    lblTitle.Visible = False
                    lblValue1.Text = String.Format("{0:$#,##0.##}", CCommon.ToDouble(item("monTotAmount")))
                    lblValue2.Text = String.Format("{0:#,##0.##}", CCommon.ToDouble(item("numPercentageComplete"))) & "%"
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            divReportContent.Visible = False
            lblError.Visible = True
        End Try
    End Sub

#End Region

End Class