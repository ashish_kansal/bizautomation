﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmPrebuildMoneyInBank.ascx.vb" Inherits=".frmPrebuildMoneyInBank" %>

<table class="rptcontent bg-primary handle" style="white-space: nowrap; text-align: center;">
    <tr>
        <td style="padding:10px">
            <img src="../images/MoneyInBank.png" height="60" /></td>
        <td>
            <h2 style="font-weight: 500;">
                <asp:Label ID="lblMoneyInBank" runat="server"></asp:Label>
            </h2>
            <p>Money in the Bank</p>
        </td>
        <td style="vertical-align: top">
            <div class="box-tools pull-right">
                <asp:HyperLink ID="hplDelete" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/cancel.png" /></asp:HyperLink>
            </div>
        </td>
    </tr>
</table>
