﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/PopupBootstrap.Master" CodeBehind="frmManageDashboardTemplate.aspx.vb" Inherits=".frmManageDashboardTemplate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function ValidateInput() {
            if ($("[id$=divCreate]").length > 0 && $("[id$=txtTemplateName]").val() == "") {
                alert("Dashboard template name is required");
                $("[id$=txtTemplateName]").focus();
                return false;
            }

            return true;
        }

        function CloseAndRefreshParent() {
            if (window.opener != null) {
                window.opener.location.href = window.opener.location.href;
            }

            self.close();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="row padbottom10" id="divError" runat="server" style="display: none">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <h4><i class="icon fa fa-ban"></i>Error</h4>
                <p>
                    <asp:Label ID="lblError" runat="server"></asp:Label>
                </p>
            </div>
        </div>
    </div>
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-right" id="divButtons" runat="server" visible="false">
                <asp:Button ID="btnSaveClose" runat="server" Text="Save & Close" CssClass="btn btn-primary" OnClientClick="return ValidateInput();" OnClick="btnSaveClose_Click" />
                <asp:Button ID="btnClose" runat="server" Text="Close" OnClientClick="return Close();" CssClass="btn btn-primary" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    <asp:Label ID="lblTitle" runat="server" Text="Label"></asp:Label>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <asp:HiddenField ID="hdnDashboardTemplateID" runat="server" />
    <asp:HiddenField ID="hdnIsUpdate" runat="server" />
    <div class="row" id="divCreate" runat="server" visible="false">
        <div class="col-xs-12">
            <div class="form-inline">
                <label>Dashboard Template Name:<span style="color:red">*</span></label>
                <asp:TextBox ID="txtTemplateName" runat="server" Width="350" CssClass="form-control"></asp:TextBox>
            </div>
            <br />
            <p style="font-weight: 600;font-style: italic;">This will take the collection and configuration of portlets from your dashboard, and create a template so you can apply it to other users from Admin | User Mgt | (Click user name) User details</p>
        </div>
    </div>
    <div class="row" id="divUpdate" runat="server" visible="false">
        <div class="col-xs-12">
            <p style="font-weight: 600;font-style: italic;">
                Any changes made to YOUR dashboard will propagate to other users that have the same dashboard template (requires that they log out and back in).
                Are you SURE you want to do this ?
            </p>
        </div>
        <div class="col-xs-12 text-center">
            <b>Are you SURE you want to do this ?</b>
            <br />
            <br />
            <asp:Button ID="btnUpdate" runat="server" Text="Yes update the template" CssClass="btn btn-primary" />
            <asp:Button ID="btnCancelUpdate" runat="server" Text="Cancel" CssClass="btn btn-primary" OnClientClick="return Close();" />
        </div>
    </div>
</asp:Content>
