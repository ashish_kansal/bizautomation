﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmPrebuildFourColumn.ascx.vb" Inherits=".frmPrebuildFourColumn" %>
<div class="box box-primary">
    <div class="box-header with-border handle">
        <h3 class="box-title">
            <asp:Label Text="" ID="lblHeader" runat="server" />
        </h3>
        <div id="tdEdit" runat="server" class="box-tools pull-right">
            <asp:HyperLink ID="hplExportToExcel" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/excel.png" height="16" /></asp:HyperLink>
            <asp:HyperLink ID="hplEdit" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/pencil-24.gif" height="16" /></asp:HyperLink>
            <asp:HyperLink ID="hplDelete" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/cancel.png" /></asp:HyperLink>
        </div>
    </div>
    <div class="box-body rptcontent">
        <div class="row" id="divReportContent" runat="server" style="margin-right:0px;margin-left:0px; display: flex; align-items: center;">
            <div class="col-xs-2 text-center" style="padding-left: 0px;padding-right: 0px;">
                <asp:Image ID="imgReportImage" runat="server" />
            </div>
            <div class="col-xs-10" style="padding-left: 5px;padding-right: 5px;">
                <asp:Repeater ID="rptReport" runat="server">
                    <HeaderTemplate>
                        <div class="row" style="margin-right:0px;margin-left:0px">
                            <div class="col-xs-8 text-center">
                                <asp:Label ID="lblHeader1" runat="server" Text="" Font-Bold="true"></asp:Label>
                            </div>
                            <div class="col-xs-2 text-center">
                                <asp:Label ID="lblHeader2" runat="server" Text="" Font-Bold="true"></asp:Label>
                            </div>
                            <div class="col-xs-2 text-center">
                                <asp:Label ID="lblHeader3" runat="server" Text="" Font-Bold="true"></asp:Label>
                            </div>
                        </div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="row" style="margin-right:0px;margin-left:0px">
                            <div class="col-xs-8 text-center">
                                <asp:HyperLink ID="hplTitle" runat="server"></asp:HyperLink>
                                <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="col-xs-2 text-right">
                                <asp:Label ID="lblValue1" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="col-xs-2 text-right">
                                <asp:Label ID="lblValue2" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
        <asp:Label ID="lblError" runat="server" Text="Error ocurred while rendering this report" ForeColor="Red" Visible="false"></asp:Label>
    </div>
    <div class="box-footer footer">
        <asp:Label ID="lblFooter" runat="server" CssClass="text_bold"></asp:Label>
    </div>
</div>
