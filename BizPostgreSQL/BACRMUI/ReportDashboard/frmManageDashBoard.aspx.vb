Imports System.IO
Imports System.Text.RegularExpressions
Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports
Imports ClosedXML.Excel
Imports HtmlAgilityPack

Partial Public Class frmNewManageDashBoard
    Inherits BACRMPage
    Private _objDashboard As Object
    Private _objCustomReport As Object

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                If GetQueryStringVal("Move") = "EX" Then
                    ExportToExcel()

                Else

                    Dim objReportManage As New CustomReportsManage
                    objReportManage.DashBoardID = CCommon.ToLong(GetQueryStringVal("DID"))
                    objReportManage.DomainID = Session("DomainID")
                    objReportManage.ManageDashboardOpe(GetQueryStringVal("Move"))
                    Response.Redirect("../ReportDashboard/frmNewDashBoard.aspx")
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
#Region "Event Handle ExportExcel"
    Protected Sub ExportToExcel()

        Dim objReportManage As New CustomReportsManage
        objReportManage.DomainID = Session("DomainID")
        objReportManage.DashBoardID = CCommon.ToLong(GetQueryStringVal("DID"))
        Dim dtReportData As DataTable = objReportManage.GetReportDashboardDTL

        objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
        objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
        objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
        objReportManage.DefaultReportID = CCommon.ToInteger(dtReportData.Rows(0).Item("intDefaultReportID"))

        If objReportManage.DefaultReportID = 0 Then
            Dim stringWrite As IO.StringWriter = New System.IO.StringWriter()
            Dim htmlWrite As HtmlTextWriter = New HtmlTextWriter(stringWrite)
            Dim dt As DataSet
            Dim st As String = dtReportData.Rows(0)("numReportID")
            'objReportManage.DefaultReportID = str
            objReportManage.ReportID = st
            Dim trHeader As TableHeaderRow = New TableHeaderRow()
            Dim tc As TableHeaderCell = New TableHeaderCell()
            Dim tblReport As New Table
            Dim trRow As TableRow = New TableRow()
            Dim cell As New TableCell()

            objReportManage.DomainID = Session("DomainID")
            objReportManage.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
            objReportManage.UserCntID = Session("UserContactID")
            dt = objReportManage.GetReportListMasterDetail

            Dim ReportName As String = dtReportData.Rows(0)("vcHeaderText") 'dt.Tables(0).Rows(0)("vcReportName")
            Dim ReportDes As String = dt.Tables(0).Rows(0)("vcReportDescription")
            Dim GeneratedOn As String = FormattedDateFromDate(Now, Session("DateFormat"))

            Dim objReport As ReportObject = objReportManage.GetDatasetToReportObject(dt)

            Dim dsColumnList As DataSet
            objReportManage.IsReportRun = True
            dsColumnList = objReportManage.GetReportModuleGroupFieldMaster

            objReportManage.textQuery = objReportManage.GetReportQuery(objReportManage, dt, objReport, False, True, dsColumnList, 1)

            Dim dtData As DataTable = objReportManage.USP_ReportQueryExecute()

            'tblReport.Rows.Clear()
            tblReport.CssClass = "table table-responsive table-bordered"
            tblReport.Width = Unit.Percentage(100)
            tblReport.BorderWidth = Unit.Pixel(1)
            tblReport.CellSpacing = 0
            tblReport.CellPadding = 0
            'phReport.Controls.Add(tblReport)
            Dim spaceCharacter As String = ""

            trHeader = New TableHeaderRow()

            For Each str As String In objReport.ColumnList
                Dim strColumn As String() = str.Split("_")

                Dim dr() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                If dr.Length > 0 Then
                    tc = New TableHeaderCell()
                    tc.Text = dr(0)("vcFieldname")
                    trHeader.Cells.Add(tc)
                End If
            Next

            tblReport.Rows.Add(trHeader)

            Dim vcFieldDataType As String = ""
            Dim vcOrigDbColumnName As String = ""
            For i As Integer = 0 To dtData.Rows.Count - 1
                trRow = New TableRow()

                For Each str As String In objReport.ColumnList
                    vcFieldDataType = ""
                    vcOrigDbColumnName = ""

                    Dim strColumn As String() = str.Split("_")
                    Dim drColumn() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & strColumn(0) & " AND numFieldID=" & strColumn(1) & " AND bitCustom=" & strColumn(2))

                    If Not drColumn Is Nothing AndAlso drColumn.Length > 0 Then
                        vcFieldDataType = CCommon.ToString(drColumn(0)("vcFieldDataType"))
                        vcOrigDbColumnName = CCommon.ToString(drColumn(0)("vcOrigDbColumnName"))
                    End If

                    cell = New TableCell()
                    If IsNumeric(dtData.Rows(i)(str)) AndAlso (vcFieldDataType = "M" Or vcFieldDataType = "N") Then
                        If vcFieldDataType = "M" Then
                            cell.Text = CCommon.GetDecimalFormat(CCommon.ToDouble(dtData.Rows(i)(str)))
                        Else
                            cell.Text = String.Format("{0:#,##0.####################}", CCommon.ToDouble(dtData.Rows(i)(str)))
                        End If
                    Else


                        'Else

                        cell.Text = CCommon.ToString(dtData.Rows(i)(str))

                    End If

                    trRow.Cells.Add(cell)
                Next
                tblReport.Rows.Add(trRow)
            Next
            tblReport.RenderControl(htmlWrite)
            Dim reportHtml As String = stringWrite.ToString()

            If Not String.IsNullOrWhiteSpace(reportHtml) Then
                Dim fs As New MemoryStream()
                Dim workbook As New XLWorkbook
                Dim workSheet As IXLWorksheet = workbook.Worksheets.Add("Report Data")

                Dim doc As New HtmlAgilityPack.HtmlDocument
                doc.LoadHtml(reportHtml)

                Dim i As Int32 = 1
                Dim j As Int32 = 1
                Dim nodeCollection As HtmlAgilityPack.HtmlNodeCollection
                Dim htmlBody = doc.DocumentNode.SelectSingleNode("//table//tr")
                Dim TotalRecord As HtmlNode = HtmlNode.CreateNode("")

                If reportHtml.Contains("Total Records") Then
                    Dim nodeTotalRecord As HtmlNode = HtmlNode.CreateNode("<th>Total Records</th>")
                    htmlBody.AppendChild(nodeTotalRecord)
                End If


                For Each row As HtmlAgilityPack.HtmlNode In doc.DocumentNode.SelectNodes("//table//tr")
                    Dim isTdcollection As Boolean
                    If Not row.SelectNodes("th") Is Nothing Then
                        nodeCollection = row.SelectNodes("th")
                    Else
                        nodeCollection = row.SelectNodes("td")
                        isTdcollection = True
                    End If

                    If reportHtml.Contains("Total Records") And isTdcollection Then
                        For Each node In nodeCollection
                            If node.InnerHtml.Contains("Total Records") Then
                                Dim recordOwnerName As String = node.InnerHtml.Split(":")(1)
                                recordOwnerName = recordOwnerName.Remove(recordOwnerName.Length - 1, 1)
                                TotalRecord = HtmlNode.CreateNode(recordOwnerName)
                            End If
                        Next
                        nodeCollection.Add(TotalRecord)
                    End If

                    For Each col As HtmlAgilityPack.HtmlNode In nodeCollection
                        If Double.TryParse(col.InnerText, Nothing) Then
                            workSheet.Cell(i, j).SetValue(col.InnerText).SetDataType(XLCellValues.Number)
                        ElseIf IsDate(col.InnerText) Then
                            workSheet.Cell(i, j).SetValue(col.InnerText).SetDataType(XLCellValues.DateTime)
                        Else
                            workSheet.Cell(i, j).SetValue(col.InnerText).SetDataType(XLCellValues.Text)
                            If isTdcollection Then
                                If col.InnerText.Contains("Total Records") Then

                                    workSheet.Cell(i, j).SetValue(Regex.Replace(col.InnerText, "\([^)]*\)", "").Replace("&nbsp;", "").Trim()).SetDataType(XLCellValues.Text)
                                Else
                                    workSheet.Cell(i, j).SetValue(col.InnerText).SetDataType(XLCellValues.Text)
                                End If

                            End If
                        End If

                        If i = 1 Then
                            workSheet.Cell(i, j).Style.Font.Bold = True
                            workSheet.Cell(i, j).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center
                        End If

                        j = j + 1
                    Next
                    i = i + 1
                    j = 1
                Next
                workSheet.Columns.AdjustToContents()

                Dim httpResponse As HttpResponse = Response
                httpResponse.Clear()
                httpResponse.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                httpResponse.AddHeader("content-disposition", String.Format("attachment;filename={0}_{1}.xlsx", ReportName.Trim.Replace(" ", "_"), Format(Now, "MM.dd.yyyy")))


                Using MemoryStream As New MemoryStream
                    workbook.SaveAs(MemoryStream)
                    MemoryStream.WriteTo(httpResponse.OutputStream)
                    MemoryStream.Close()
                End Using

                httpResponse.Flush()
                httpResponse.End()
            End If




            'If Not dt Is Nothing AndAlso dt.Tables.Count > 0 AndAlso dt.Tables(0).Rows.Count > 0 Then
            '    objExport.DataSetToExcel(dt, file, CCommon.ToLong(Session("DomainID")))
            'End If
        Else
            Dim ds As DataSet
            ds = objReportManage.GetPrebuildReportData()

            Dim fileName As String = dtReportData.Rows(0)("vcHeaderText") & "_" & DateTime.Now.ToString("yyyyMMddHHmmss") & ".xlsx"

            Dim objCSVExport As New CSVExport

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                objCSVExport.DataSetToExcel(ds, fileName, CCommon.ToLong(Session("DomainID")))
            End If


            Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
            response.ClearContent()
            response.Clear()
            response.ContentType = "application/vnd.ms-excel"
            response.AddHeader("Content-Disposition", "attachment; filename=" & fileName & ";")
            response.TransmitFile(BACRM.BusinessLogic.Common.CCommon.GetDocumentPhysicalPath(CCommon.ToLong(Session("DomainID"))) & fileName)
            response.Flush()

            Try
                System.IO.File.Delete(BACRM.BusinessLogic.Common.CCommon.GetDocumentPhysicalPath(CCommon.ToLong(Session("DomainID"))) & fileName)
            Catch ex As Exception
                'DO NOT THROW ERROR
            End Try
        End If






    End Sub

#End Region
End Class