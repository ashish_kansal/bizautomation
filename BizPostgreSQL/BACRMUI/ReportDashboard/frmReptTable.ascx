<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmReptTable.ascx.vb"
    Inherits=".frmNewReptTable" %>
<div class="box box-primary">
    <div class="box-header handle">
        <h3 class="box-title">
            <asp:Label Text="" ID="lblHeader" runat="server" />
        </h3>

        <div id="tdEdit" runat="server" class="box-tools pull-right">
            <asp:HyperLink ID="hplExportToExcel" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/excel.png" height="16" /></asp:HyperLink>
            <asp:HyperLink ID="hplEdit" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/pencil-24.gif" height="16" /></asp:HyperLink>
            <asp:HyperLink ID="hplDelete" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/cancel.png" /></asp:HyperLink>
        </div>
    </div>
    <div class="box-body rptcontent" runat="server" id="dvTable" clientidmode="Static">
        <asp:Panel ID="pnlChart" runat="server" CssClass="pnlChart">
            <asp:Literal ID="litReport" runat="server"></asp:Literal>
        </asp:Panel>
    </div>
    <div class="box-footer footer">
        <asp:Label ID="lblFooter" runat="server" CssClass="text_bold"></asp:Label>
    </div>
</div>
