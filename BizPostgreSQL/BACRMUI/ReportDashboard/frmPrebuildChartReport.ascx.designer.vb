﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmPrebuildChartReport
    
    '''<summary>
    '''lblHeader control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblHeader As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''tdEdit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tdEdit As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''hplExportToExcel control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hplExportToExcel As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''hplEdit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hplEdit As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''hplDelete control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents hplDelete As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''divChart control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divChart As Global.System.Web.UI.HtmlControls.HtmlGenericControl
    
    '''<summary>
    '''divLegend control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents divLegend As Global.System.Web.UI.HtmlControls.HtmlGenericControl
End Class
