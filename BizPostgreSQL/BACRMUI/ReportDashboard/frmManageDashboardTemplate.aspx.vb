﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports

Public Class frmManageDashboardTemplate
    Inherits BACRMPage

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lblError.Text = ""
            divError.Style.Add("display", "none")

            If Not Page.IsPostBack Then
                hdnIsUpdate.Value = CCommon.ToBool(CCommon.ToShort(GetQueryStringVal("IsUpdate")))
                hdnDashboardTemplateID.Value = CCommon.ToLong(GetQueryStringVal("DashboardTemplateID"))

                If CCommon.ToBool(hdnIsUpdate.Value) Then
                    lblTitle.Text = "Update Dashboard Template"
                    divUpdate.Visible = True
                Else
                    lblTitle.Text = "Create a new Dashboard Template"
                    divCreate.Visible = True
                    divButtons.Visible = True
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

#End Region

#Region "Private Methods"

    Private Sub Save()
        Try
            If divCreate.Visible AndAlso txtTemplateName.Text.Trim() = "" Then
                DisplayError("Dashboard template name is required")
                txtTemplateName.Focus()
                Exit Sub
            End If

            Dim objDashboardTemplate As New DashboardTemplate
            objDashboardTemplate.DomainID = CCommon.ToLong(Session("DomainID"))
            objDashboardTemplate.UserCntID = CCommon.ToLong(Session("UserContactID"))
            objDashboardTemplate.TemplateName = txtTemplateName.Text.Trim()
            objDashboardTemplate.TemplateID = CCommon.ToLong(hdnDashboardTemplateID.Value)
            objDashboardTemplate.IsUpdate = CCommon.ToBool(hdnIsUpdate.Value)
            objDashboardTemplate.Save()

            ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "Close Window", "CloseAndRefreshParent();", True)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub DisplayError(ByVal errorMessage As String)
        Try
            lblError.Text = errorMessage
            divError.Style.Add("display", "")
        Catch ex As Exception
            'DO NOT THROW EXCEPTION
        End Try
    End Sub

#End Region

#Region "Event Handlers"

    Protected Sub btnSaveClose_Click(sender As Object, e As EventArgs)
        Try
            Save()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Try
            Save()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

#End Region
    
   
End Class