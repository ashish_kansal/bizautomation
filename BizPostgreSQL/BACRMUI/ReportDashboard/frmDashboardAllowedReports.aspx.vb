﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports
Imports BACRM.BusinessLogic.Admin

Public Class frmDashboardAllowedReports
    Inherits BACRMPage

    Dim objReportManage As New CustomReportsManage

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                LoadDropDown()

                BindDataGrid()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub LoadDropDown()
        Try
            Dim objUserGroups As New UserGroups
            Dim dtGroupName As DataTable
            objUserGroups.DomainID = Session("DomainID")
            objUserGroups.SelectedGroupTypes = "1,4"
            dtGroupName = objUserGroups.GetAutorizationGroup

            ddlGroup.DataSource = dtGroupName.DataSet.Tables(0).DefaultView
            ddlGroup.DataTextField = "vcGroupName"
            ddlGroup.DataValueField = "numGroupId"
            ddlGroup.DataBind()
            ddlGroup.Items.Insert(0, "--Select One--")
            ddlGroup.Items.FindByText("--Select One--").Value = 0

            If Session("UserGroupID") IsNot Nothing Then
                If ddlGroup.Items.FindByValue(Session("UserGroupID")) IsNot Nothing Then
                    ddlGroup.ClearSelection()
                    ddlGroup.Items.FindByValue(Session("UserGroupID")).Selected = True
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindDataGrid()
        Try
            Dim dtTable As DataTable

            If objReportManage Is Nothing Then objReportManage = New CustomReportsManage
            objReportManage.DomainID = Session("DomainId")
            objReportManage.GroupId = ddlGroup.SelectedValue
            dtTable = objReportManage.GetDashboardAllowedReports()

            gvReport.DataSource = dtTable
            gvReport.DataBind()

            objReportManage.tintMode = 2
            dtTable = objReportManage.GetDashboardAllowedReports()

            gvKPIReportGroup.DataSource = dtTable
            gvKPIReportGroup.DataBind()

            objReportManage.tintMode = 5
            dtTable = objReportManage.GetDashboardAllowedReports()

            gvPrebuildReports.DataSource = dtTable
            gvPrebuildReports.DataBind()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Try
            Save()
            BindDataGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Sub Save()
        Try
            If objReportManage Is Nothing Then objReportManage = New CustomReportsManage
            objReportManage.strText = txtReportValues.Text
            objReportManage.strKPIGroupReportIds = txtKPIGroupReportValues.Text
            objReportManage.strPrebuildReportIds = txtPrebuildReportValues.Text
            objReportManage.DomainID = Session("DomainId")
            objReportManage.GroupId = ddlGroup.SelectedValue
            objReportManage.ManageDashboardAllowedReports()
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub ddlGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlGroup.SelectedIndexChanged
        Try
            BindDataGrid()
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub

    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            Save()
            Response.Write("<script>self.close()</script>")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            Response.Write(ex)
        End Try
    End Sub
End Class