﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmDashBoard.aspx.vb" Inherits=".frmNewDashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Dashboard</title>
    <style type="text/css">
        .column
        {
            float: left;
            padding-bottom: 100px;
        }

        .portlet
        {
            margin: 0 1em 1em 0;
            background-color: #F3F4F5;
        }

        .portlet-header
        {
            margin: 0.3em;
            padding-bottom: 4px;
            padding-left: 0.2em;
            background-color: #7F8BAC;
            color: #fff;
            font-family: Sans-Serif, Arial, Tahoma;
            font-size: small;
            cursor: move;
        }

        .portlet-header a
        {
            color: #fff !important;
            font-size: 11px;
        }

            .portlet-header .ui-icon
            {
                float: right;
                cursor:pointer;
            }

        .portlet-content
        {
            padding: 0.4em;
        }

        .pnlChart
        {
            overflow-x: auto;
            overflow-y: auto;
            max-height: 500px;
        }

        .ui-sortable-placeholder
        {
            border: 1px dotted black;
            visibility: visible !important;
            height: 50px !important;
        }

            .ui-sortable-placeholder *
            {
                visibility: hidden;
            }

        .success
        {
            border: 1px solid;
            margin: 10px 0px;
            padding: 15px 10px 15px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
            color: #4F8A10;
            background-color: #DFF2BF;
            background-image: url('../images/success.png');
        }

        .descriptionPanelHeader
        {
            font-family: Arial;
            cursor: move;
        }
    </style>
    <script language="javascript" type="text/javascript">

        $(function () {
            $(".column").sortable({
                disable: true,
                connectWith: '.column',
                handle: '.portlet-header',
                revert: 300,
                delay: 100,
                opacity: 0.8,
                stop: function (event, ui) {
                },
                update: function (event, ui) {
                    var Column1;
                    var Column2;
                    var Column3;
                    Column1 = $('#Column1').sortable('toArray');
                    //                    console.log(Column1);
                    Column2 = $('#Column2').sortable('toArray');
                    //                    console.log(Column2);
                    Column3 = $('#Column3').sortable('toArray');
                    //                    console.log(Column3);
                    $.ajax({
                        type: "POST",
                        url: "frmDashBoard.aspx/UpdateOrder",
                        data: '{Column1Values:"' + Column1.toString() + '",Column2Values:"' + Column2.toString() + '",Column3Values:"' + Column3.toString() + '"}',
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        async: true,
                        cache: false,
                        success: function (msg) {
                            if (msg.d == 'True') {
                                $('#dvSucess').text('Widget order saved sucessfully');
                                FadeOut(2000);
                            }
                        }
                    })

                }

            });

            $("#Column1").css("width", $("#tblRept1").css("width"));
            $("#Column2").css("width", $("#tblRept2").css("width"));
            $("#Column3").css("width", $("#tblRept3").css("width"));

            $(".portlet").addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all")
			.find(".portlet-header")
				.addClass("ui-widget-header ui-corner-all")
				.prepend('<span class="ui-icon ui-icon-minusthick"></span>')
				.end()
			.find(".portlet-content");

            $(".portlet-header .ui-icon").click(function () {
                $(this).toggleClass("ui-icon-minusthick").toggleClass("ui-icon-plusthick");
                $(this).parents(".portlet:first").find(".portlet-content").toggle();
            });

                         $(".column").disableSelection();
        });

        function FadeOut(NoMSeconds) {
            $('#dvSucess').show();
            setTimeout(function () {
                $("#dvSucess").fadeOut("slow", function () {
                    $("#dvSucess").hide();
                });

            }, NoMSeconds);
        }

        function NewReport() {
            window.location.href = "../ReportDashboard/frmAddDashboardReport.aspx";
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td align="right">
                <asp:Button ID="btnEdit" runat="server" Text="Edit" CssClass="button" />
                <asp:Button ID="btnNewReport" Width="130px" runat="server" Text="Add New Report"
                    CssClass="button" UseSubmitBehavior="true" OnClientClick="return NewReport()" />&nbsp;&nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr>
            <td align="center">
                <div class="success" id="dvSucess" style="display: none">
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Dashboard
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <table cellspacing="0" cellpadding="0"  border="0">
        <tr>
            <td valign="top">
                <table id="tblRept1" enableviewstate="false" runat="server">
                    <tr>
                        <td class="normal1" runat="server" visible="false" id="td1">Column Size &nbsp;
                                <asp:LinkButton ID="lnkNarrow1" runat="server">Narrow</asp:LinkButton>
                            |
                                <asp:LinkButton ID="lnkMedium1" runat="server">Medium</asp:LinkButton>
                            |
                                <asp:LinkButton ID="lnkWide1" runat="server">Wide</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="column" id="Column1">
                                <asp:PlaceHolder ID="phColumn1" runat="server"></asp:PlaceHolder>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top">
                <table id="tblRept2" enableviewstate="false" runat="server">
                    <tr>
                        <td class="normal1" visible="false" runat="server" id="td3">Column Size &nbsp;
                                <asp:LinkButton ID="lnkNarrow2" runat="server">Narrow</asp:LinkButton>
                            |
                                <asp:LinkButton ID="lnkMedium2" runat="server">Medium</asp:LinkButton>
                            |
                                <asp:LinkButton ID="lnkWide2" runat="server">Wide</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="column" id="Column2">
                                <asp:PlaceHolder ID="phColumn2" runat="server"></asp:PlaceHolder>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
            <td valign="top">
                <table id="tblRept3" enableviewstate="false" runat="server">
                    <tr>
                        <td class="normal1" visible="false" runat="server" id="td5">Column Size &nbsp;
                                <asp:LinkButton ID="lnkNarrow3" runat="server">Narrow</asp:LinkButton>
                            |
                                <asp:LinkButton ID="lnkMedium3" runat="server">Medium</asp:LinkButton>
                            |
                                <asp:LinkButton ID="lnkWide3" runat="server">Wide</asp:LinkButton>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="column" id="Column3">
                                <asp:PlaceHolder ID="phColumn3" runat="server"></asp:PlaceHolder>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
