﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports

Public Class frmPrebuildHtmlReport
    Inherits BACRMUserControl

#Region "Public Properties"

    Public Property DashBoardID As Long
    Public Property objReportManage As CustomReportsManage
    Public Property DefaultReportID As Integer
    Public Property IsRefresh As Boolean
    Public Property EditRept As Boolean
    Public Property Width As Integer

#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#End Region

#Region "Public Methods"

    Public Function CreateReport() As Boolean
        Try
            Dim directoryPath As String = System.Configuration.ConfigurationManager.AppSettings("PortalPath") & Convert.ToInt64(Session("DomainID")) & "\Dashboard"

            If Not IsRefresh AndAlso System.IO.Directory.Exists(directoryPath) AndAlso System.IO.File.Exists(directoryPath & "\" & DashBoardID & ".html") Then
                litReportHtml.Text = System.IO.File.ReadAllText(directoryPath & "\" & DashBoardID & ".html")
            Else
                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = DefaultReportID
                objReportManage.DomainID = Session("DomainID")
                objReportManage.DashBoardID = _DashBoardID
                litReportHtml.Text = CCommon.ToString(objReportManage.GetPrebuildReprtHtml(False))

                If Not System.IO.Directory.Exists(directoryPath) Then
                    System.IO.Directory.CreateDirectory(directoryPath)
                End If


                System.IO.File.WriteAllText(directoryPath & "\" & DashBoardID & ".html", litReportHtml.Text)
            End If

            Return True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
        End Try
    End Function

#End Region

End Class