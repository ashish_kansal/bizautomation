Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports
Imports System.Linq.Expressions
Imports System.Reflection
Imports System.Collections.Generic

Partial Public Class frmNewReptTable
    Inherits BACRMUserControl

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private _DashBoardID As Long
    Public Property DashBoardID() As Long
        Get
            Try
                Return _DashBoardID
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As Long)
            Try
                _DashBoardID = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property

    Private _EditRept As Boolean
    Public Property EditRept() As Boolean
        Get
            Try
                Return _EditRept
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As Boolean)
            Try
                _EditRept = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property

    Private _Width As Integer
    Public Property Width() As Integer
        Get
            Try
                Return _Width
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As Integer)
            Try
                _Width = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property


    Private _objReportManage As Object
    Public Property objReportManage() As Object
        Get
            Try
                Return _objReportManage
            Catch ex As Exception
                Throw ex
            End Try
        End Get
        Set(ByVal Value As Object)
            Try
                _objReportManage = Value
            Catch ex As Exception
                Throw ex
            End Try
        End Set
    End Property

    Public Function CreateReport() As Boolean
        Try
            If _EditRept = True Then tdEdit.Visible = True
            'pnlChart.Width = _Width
            dvTable.ID = _DashBoardID

            _objReportManage.DomainID = Session("DomainID")
            _objReportManage.DashBoardID = _DashBoardID
            Dim dtTable As DataTable

            dtTable = objReportManage.GetReportDashboardDTL

            lblHeader.Text = dtTable.Rows(0).Item("vcHeaderText")
            lblFooter.Text = dtTable.Rows(0).Item("vcFooterText")

            hplExportToExcel.NavigateUrl = "~/ReportDashboard/frmManageDashBoard.aspx?Move=EX&DID=" & _DashBoardID

            hplDelete.NavigateUrl = "../ReportDashboard/frmManageDashBoard.aspx?Move=X&DID=" & _DashBoardID
            hplEdit.NavigateUrl = "../ReportDashboard/frmAddDashboardReport.aspx?DID=" & _DashBoardID
            Try
                If dtTable.Rows(0).Item("tintReportCategory") = 0 Then 'Custom Reports
                    BindReport(dtTable.Rows(0).Item("numReportID"))
                    pnlChart.Attributes.Add("ondblclick", "return Redirect(" & dtTable.Rows(0).Item("numReportID") & ")")
                ElseIf dtTable.Rows(0).Item("tintReportCategory") = 1 Then 'KPI Groups Reports
                    BindKPIGroupReport(dtTable.Rows(0).Item("numReportID"))
                End If
            Catch ex As Exception

            End Try

            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Sub BindKPIGroupReport(ByVal lngKPIGroupID As Long)
        Try
            _objReportManage.ReportKPIGroupID = lngKPIGroupID
            _objReportManage.DomainID = Session("DomainID")
            _objReportManage.tintMode = 0

            Dim dt As DataTable = _objReportManage.GetReportKPIGroupListMaster

            If dt.Rows.Count = 1 Then
                Dim tintKPIGroupReportType As Short = dt.Rows(0)("tintKPIGroupReportType")

                _objReportManage.tintMode = 1

                Dim dtKPIReports As DataTable = _objReportManage.GetReportKPIGroupListMaster
                Dim intReportCount As Integer = 0
                For Each dr As DataRow In dtKPIReports.Rows
                    BindReport(dr("numReportID"), IIf(intReportCount = 0, True, False))
                    intReportCount += 1
                Next
            End If

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Sub BindReport(ByVal lngReportID As Long, Optional ByVal boolKPIScoreCardHeaderTable As Boolean = True)
        Try
            Dim directoryPath As String = System.Configuration.ConfigurationManager.AppSettings("PortalPath") & Convert.ToInt64(Session("DomainID")) & "\Dashboard"

            If System.IO.File.Exists(directoryPath & "\" & DashBoardID & ".html") Then
                litReport.Text = System.IO.File.ReadAllText(directoryPath & "\" & DashBoardID & ".html")
            Else
                Dim ds As DataSet

                _objReportManage.ReportID = lngReportID
                _objReportManage.DomainID = Session("DomainID")
                _objReportManage.ClientTimeZoneOffset = Session("ClientMachineUTCTimeOffset")
                _objReportManage.UserCntID = Session("UserContactID")

                ds = _objReportManage.GetReportListMasterDetail

                Dim trHeader As TableHeaderRow = New TableHeaderRow()
                Dim tc As TableHeaderCell = New TableHeaderCell()

                Dim trRow As TableRow = New TableRow()
                Dim cell As New TableCell()

                If ds.Tables.Count > 0 Then

                    Dim objReport As ReportObject = _objReportManage.GetDatasetToReportObject(ds)

                    Dim dsColumnList As DataSet
                    dsColumnList = _objReportManage.GetReportModuleGroupFieldMaster

                    Try
                        _objReportManage.CurrentPage = 1
                        _objReportManage.textQuery = _objReportManage.GetReportQuery(_objReportManage, ds, objReport, False, True, dsColumnList)

                        Dim dtData As DataTable = _objReportManage.USP_ReportQueryExecute()

                        Dim html As String = "<table class='table table-responsive table-bordered'>"
                        html += objReportManage.GetCustomReportHtml(ds, dsColumnList, dtData, objReport, False)
                        html += "</table>"

                        litReport.Text = html

                        If Not System.IO.File.Exists(directoryPath & "\" & DashBoardID & ".html") Then
                            System.IO.File.Create(directoryPath & "\" & DashBoardID & ".html")
                        End If

                        Dim file As System.IO.StreamWriter
                        file = My.Computer.FileSystem.OpenTextFileWriter(directoryPath & "\" & DashBoardID & ".html", False)
                        file.WriteLine(html)
                        file.Close()
                    Catch ex As Exception
                        If ex.Message = "ReportNoColumnFound" Then
                            litReport.Text = "<span class='text-red'>Error: Can't display the report until you fix the error in Reports.</span>"
                            Exit Sub
                        End If
                    End Try
                End If
            End If

            
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class
