﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports
Public Class frmPrebuildAPReport
    Inherits BACRMUserControl

#Region "Public Properties"

    Public Property DashBoardID As Long
    Public Property EditRept As Boolean
    Public Property Width As Integer
    Public Property objReportManage As CustomReportsManage
    Public Property IsRefresh As Boolean

#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#End Region

#Region "Public Methods"

    Public Function CreateReport() As Boolean
        Try
            hplDelete.NavigateUrl = "~/ReportDashboard/frmManageDashBoard.aspx?Move=X&DID=" & _DashBoardID
            hplAPAging.NavigateUrl = "~/Accounting/frmAccountsPayable.aspx"

            objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
            objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
            objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
            objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.AP
            Dim ds As DataSet
            If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
            Else
                ds = objReportManage.GetPrebuildReportData()
                Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
            End If

            If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                lblTotalAP.Text = Session("Currency") & String.Format("{0:#,##0}", CCommon.ToInteger(ds.Tables(0).Rows(0)("TotalAP")))
                lbl30Days.Text = Session("Currency") & String.Format("{0:#,##0}", CCommon.ToInteger(ds.Tables(0).Rows(0)("APWithin30Days")))
                lbl60Days.Text = Session("Currency") & String.Format("{0:#,##0}", CCommon.ToInteger(ds.Tables(0).Rows(0)("APWithin31to60Days")))
                lbl90Days.Text = Session("Currency") & String.Format("{0:#,##0}", CCommon.ToInteger(ds.Tables(0).Rows(0)("APWithin61to90Days")))
                lblPastDue.Text = Session("Currency") & String.Format("{0:#,##0}", CCommon.ToInteger(ds.Tables(0).Rows(0)("APPastDue")))
            Else
                lblTotalAP.Text = Session("Currency") & String.Format("{0:#,##0.00}", 0)
                lbl30Days.Text = Session("Currency") & String.Format("{0:#,##0.00}", 0)
                lbl60Days.Text = Session("Currency") & String.Format("{0:#,##0.00}", 0)
                lbl90Days.Text = Session("Currency") & String.Format("{0:#,##0.00}", 0)
                lblPastDue.Text = Session("Currency") & String.Format("{0:#,##0.00}", 0)
            End If

            Return True
        Catch ex As Exception
            Throw ex
        End Try
    End Function

#End Region

End Class