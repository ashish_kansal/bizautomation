﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmPrebuildAccountingReports.ascx.vb" Inherits=".frmPrebuildAccountingReports" %>
<table class="rptcontent handle" style="white-space: nowrap; text-align: center;">
    <tr>
        <td style="padding:10px">
            <img src="../images/AccountingReport.png" height="60" /></td>
        <td>
            <a href="../Accounting/frmIncomeExpense.aspx" style="line-height:21px">Income Statement</a><br />
            <a href="../Accounting/frmBalanceSheetReport.aspx" style="line-height:21px">Balance Sheet</a><br />
            <a href="../Accounting/frmTrialBalanceReport.aspx" style="line-height:21px">Trial Balance</a><br />
            <a href="../Accounting/frmCashFlowStatement.aspx" style="line-height:21px">Cashflow Statement</a>
        </td>
        <td style="vertical-align: top">
            <div class="box-tools pull-right">
                <asp:HyperLink ID="hplDelete" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/cancel.png" /></asp:HyperLink>
            </div>
        </td>
    </tr>
</table>
