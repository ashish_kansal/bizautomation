﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/Popup.Master" CodeBehind="frmDashboardAllowedReports.aspx.vb" Inherits=".frmDashboardAllowedReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Dashboard Allowed Reports</title>
    <script type="text/javascript" language="javascript">
        function Save() {
            if (document.getElementById('ddlGroup').value == '0') {
                alert('Select Group')
                return false
            }

            var strVal = '', strKPIVal = '', strPrebuildVal = ''

            $("#gvReport tr").not(".hs,.fs").each(function () {

                if ($(this).find(".chkSelect input[type = 'checkbox']").is(':checked')) {
                    strVal = $(this).find('#lblRptId').text() + "," + strVal
                }

            });

            $("#gvKPIReportGroup tr").not(".hs,.fs").each(function () {

                if ($(this).find(".chkSelect1 input[type = 'checkbox']").is(':checked')) {
                    strKPIVal = $(this).find('#lblRptId').text() + "," + strKPIVal
                }

            });

            $("#gvPrebuildReports tr").not(".hs,.fs").each(function () {

                if ($(this).find(".chkSelectPrebuild input[type = 'checkbox']").is(':checked')) {
                    strPrebuildVal = $(this).find('#lblRptId').text() + "," + strPrebuildVal
                }

            });

            document.getElementById('txtReportValues').value = strVal;
            document.getElementById('txtKPIGroupReportValues').value = strKPIVal;
            document.getElementById('txtPrebuildReportValues').value = strPrebuildVal;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <table width="100%">
        <tr>
            <td align="right">
                <asp:Button runat="server" OnClientClick="return Save()" CssClass="button" Text="Save"
                    ID="btnSave" />
                <asp:Button runat="server" OnClientClick="return Save()" CssClass="button" Text="Save & Close"
                    ID="btnSaveClose" />
                <asp:Button runat="server" OnClientClick="javascript:self.close()" CssClass="button"
                    Text="Close" ID="Button1" />
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="PageTitle" runat="server">
    Dashboard Allowed Custom Reports
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="Content" runat="server">
    <table width="100%">
        <tr valign="bottom">
            <td align="center" valign="middle" class="normal1">Group
                <asp:DropDownList ID="ddlGroup" EnableViewState="true" runat="server" AutoPostBack="true"
                    CssClass="signup" Width="200">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td class="rowHeader" align="left">
                Predefined Reports
            </td>
        </tr>
        <tr valign="top">
            <td valign="top">
                <asp:GridView ID="gvPrebuildReports" runat="server" AutoGenerateColumns="false"
                    CssClass="dg" Width="100%" DataKeyNames="intDefaultReportID">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:CheckBox onclick="SelectAll('chkSelectHdrPrebuild','chkSelectPrebuild')" runat="server" ID="chkSelectHdrPrebuild" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="chkSelect" CssClass="chkSelectPrebuild" Checked='<%#Container.DataItem("bitAllowed")%>' />
                                <asp:Label runat="server" Style="display: none" ID="lblRptId" Text='<%#Container.DataItem("intDefaultReportID")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="vcReportName" SortExpression="vcReportName" HeaderText="Report Name"></asp:BoundField>
                        <asp:BoundField DataField="vcReportDescription" SortExpression="vcReportDescription" HeaderText="Report Description"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="rowHeader" align="left">Custom Reports
            </td>
        </tr>
        <tr valign="top">
            <td valign="top">
                <asp:GridView ID="gvReport" runat="server" AutoGenerateColumns="false"
                    CssClass="dg" Width="100%" DataKeyNames="numReportID">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:CheckBox onclick="SelectAll('chkSelectHdr','chkSelect')" runat="server" ID="chkSelectHdr" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="chkSelect" CssClass="chkSelect" Checked='<%#Container.DataItem("bitAllowed")%>' />
                                <asp:Label runat="server" Style="display: none" ID="lblRptId" Text='<%#Container.DataItem("numReportID")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="vcModuleName" HeaderText="Module"></asp:BoundField>
                        <asp:BoundField DataField="vcGroupName" HeaderText="Type"></asp:BoundField>
                        <asp:BoundField DataField="vcReportName" SortExpression="vcReportName" HeaderText="Report Name"></asp:BoundField>
                        <asp:BoundField DataField="vcReportDescription" SortExpression="vcReportDescription"
                            HeaderText="Report Description"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td class="rowHeader" align="left">KPI/Score Card Group Reports
            </td>
        </tr>
        <tr valign="top">
            <td valign="top">
                <asp:GridView ID="gvKPIReportGroup" runat="server" AutoGenerateColumns="false"
                    CssClass="dg" Width="100%" DataKeyNames="numReportKPIGroupID">
                    <AlternatingRowStyle CssClass="ais" />
                    <RowStyle CssClass="is" />
                    <HeaderStyle CssClass="hs" />
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:CheckBox onclick="SelectAll('chkSelectHdr1','chkSelect1')" runat="server" ID="chkSelectHdr1" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox runat="server" ID="chkSelect" CssClass="chkSelect1" Checked='<%#Container.DataItem("bitAllowed")%>' />
                                <asp:Label runat="server" Style="display: none" ID="lblRptId" Text='<%#Container.DataItem("numReportKPIGroupID")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:ButtonField DataTextField="vcKPIGroupName" CommandName="KPIDetail" SortExpression="vcRuleName"
                            HeaderText="KPI Group" ButtonType="Link"></asp:ButtonField>
                        <asp:BoundField DataField="vcKPIGroupDescription" SortExpression="vcKPIGroupDescription"
                            HeaderText="Description"></asp:BoundField>
                        <asp:BoundField DataField="vcKPIGroupReportType" SortExpression="vcKPIGroupReportType"
                            HeaderText="Type"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
    <asp:TextBox runat="server" ID="txtReportValues" Style="display: none"></asp:TextBox>
    <asp:TextBox runat="server" ID="txtKPIGroupReportValues" Style="display: none"></asp:TextBox>
    <asp:TextBox runat="server" ID="txtPrebuildReportValues" Style="display: none"></asp:TextBox>
</asp:Content>
