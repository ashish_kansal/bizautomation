﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports
Public Class frmPrebuildChartReport
    Inherits BACRMUserControl

#Region "Public Properties"

    Public Property DashBoardID As Long
    Public Property EditRept As Boolean
    Public Property Width As Integer
    Public Property objReportManage As CustomReportsManage
    Public Property DefaultReportID As Integer
    Public Property IsRefresh As Boolean

#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#End Region

#Region "Public Methods"

    Public Function CreateReport() As Boolean
        Try
            hplExportToExcel.NavigateUrl = "~/ReportDashboard/frmManageDashBoard.aspx?Move=EX&DID=" & _DashBoardID

            hplEdit.NavigateUrl = "../ReportDashboard/frmAddDashboardReport.aspx?DID=" & _DashBoardID
            hplDelete.NavigateUrl = "~/ReportDashboard/frmManageDashBoard.aspx?Move=X&DID=" & _DashBoardID

            _objReportManage.DomainID = Session("DomainID")
            _objReportManage.DashBoardID = _DashBoardID
            Dim dtReportData As DataTable = objReportManage.GetReportDashboardDTL

            If Not dtReportData Is Nothing AndAlso dtReportData.Rows.Count > 0 Then
                lblHeader.Text = dtReportData.Rows(0).Item("vcHeaderText")
            End If

            If DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10ItemsByProfitAmountCompanyWide Then
                divLegend.ID = "divLegend5-Top10ItemByProfit"
                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10ItemsByProfitAmountCompanyWide
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim labels As New System.Text.StringBuilder()
                    Dim data As New System.Text.StringBuilder()
                    For Each dr As DataRow In ds.Tables(0).Rows
                        Dim arrSplit As String() = CCommon.ToString(dr("vcItemName")).Split(" ")

                        If arrSplit.Length > 1 Then
                            labels.Append("[")
                            Dim i As Integer = 0
                            For Each item As String In arrSplit
                                If i > 0 Then
                                    labels.Append(",")
                                End If
                                labels.Append("'" & item & "'")
                                i += 1
                            Next
                            labels.Append("],")
                        Else
                            labels.Append("'" & CCommon.ToString(dr("vcItemName")) & "',")
                        End If


                        data.Append(CCommon.ToDouble(dr("Profit")) & ",")
                    Next

                    Dim chartScript As String = System.IO.File.ReadAllText(Server.MapPath("~/ReportDashboard/PrebuildReportTemplate/ChartJsMasterScriptBarChart.txt"))
                    chartScript = chartScript.Replace("##CHARTDATA##", "chartData" & _DashBoardID)
                    chartScript = chartScript.Replace("##LABELS##", labels.ToString.Trim(","))
                    chartScript = chartScript.Replace("##DATA##", data.ToString.Trim(","))
                    chartScript = chartScript.Replace("##BACKCOLOR##", "['#f56954','#00a65a','#3c8dbc','#b6d957','#fac364','#d998cb','#f2d249','#ccc5a8','#52bacc','#98aafb']")
                    chartScript = chartScript.Replace("##BORDERCOLOR##", "['#f56954','#00a65a','#3c8dbc','#b6d957','#fac364','#d998cb','#f2d249','#ccc5a8','#52bacc','#98aafb']")
                    chartScript = chartScript.Replace("##CHARTID##", "myChart" & _DashBoardID)
                    chartScript = chartScript.Replace("##CHARTVAR##", "myReportChart" & _DashBoardID)
                    chartScript = chartScript.Replace("##CHARTTYPE##", "bar")
                    chartScript = chartScript.Replace("##XAXISTITLE##", "Items")
                    chartScript = chartScript.Replace("##YAXISTITLE##", "Profit")
                    chartScript = chartScript.Replace("##LegendID##", "#" & divLegend.ID)
                    chartScript = chartScript.Replace("##BORDERWIDTH##", "1")

                    Dim canvas As New HtmlGenericControl("canvas")
                    canvas.ID = "myChart" & _DashBoardID
                    canvas.Style.Add("width", "100%")
                    canvas.Style.Add("heigth", "100%")
                    divChart.Controls.Add(canvas)

                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "5-Top10ItemByProfit", chartScript, True)
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10ItemsByRevenueSoldCompanyWide Then
                divLegend.ID = "divLegend6-Top10ItemByAmount"
                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10ItemsByRevenueSoldCompanyWide
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim labels As New System.Text.StringBuilder()
                    Dim data As New System.Text.StringBuilder()
                    For Each dr As DataRow In ds.Tables(0).Rows
                        Dim arrSplit As String() = CCommon.ToString(dr("vcItemName")).Split(" ")

                        If arrSplit.Length > 1 Then
                            labels.Append("[")
                            Dim i As Integer = 0
                            For Each item As String In arrSplit
                                If i > 0 Then
                                    labels.Append(",")
                                End If
                                labels.Append("'" & item & "'")
                                i += 1
                            Next
                            labels.Append("],")
                        Else
                            labels.Append("'" & CCommon.ToString(dr("vcItemName")) & "',")
                        End If


                        data.Append(CCommon.ToDouble(dr("TotalAmount")) & ",")
                    Next

                    Dim chartScript As String = System.IO.File.ReadAllText(Server.MapPath("~/ReportDashboard/PrebuildReportTemplate/ChartJsMasterScriptBarChart.txt"))
                    chartScript = chartScript.Replace("##CHARTDATA##", "chartData" & _DashBoardID)
                    chartScript = chartScript.Replace("##LABELS##", labels.ToString.Trim(","))
                    chartScript = chartScript.Replace("##DATA##", data.ToString.Trim(","))
                    chartScript = chartScript.Replace("##BACKCOLOR##", "['#f56954','#00a65a','#3c8dbc','#b6d957','#fac364','#d998cb','#f2d249','#ccc5a8','#52bacc','#98aafb']")
                    chartScript = chartScript.Replace("##BORDERCOLOR##", "['#f56954','#00a65a','#3c8dbc','#b6d957','#fac364','#d998cb','#f2d249','#ccc5a8','#52bacc','#98aafb']")
                    chartScript = chartScript.Replace("##CHARTID##", "myChart" & _DashBoardID)
                    chartScript = chartScript.Replace("##CHARTVAR##", "myReportChart" & _DashBoardID)
                    chartScript = chartScript.Replace("##CHARTTYPE##", "bar")
                    chartScript = chartScript.Replace("##XAXISTITLE##", "Items")
                    chartScript = chartScript.Replace("##YAXISTITLE##", "Revenue")
                    chartScript = chartScript.Replace("##LegendID##", "#" & divLegend.ID)
                    chartScript = chartScript.Replace("##BORDERWIDTH##", "1")

                    Dim canvas As New HtmlGenericControl("canvas")
                    canvas.ID = "myChart" & _DashBoardID
                    canvas.Style.Add("width", "100%")
                    canvas.Style.Add("heigth", "100%")
                    divChart.Controls.Add(canvas)


                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "6-Top10ItemByRevenue", chartScript, True)
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.ProfitMarginByItemClassificationCompanyWide Then
                divLegend.ID = "divLegend7-ProftByItemClassification"
                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.ProfitMarginByItemClassificationCompanyWide
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim labels As New System.Text.StringBuilder()
                    Dim data As New System.Text.StringBuilder()
                    For Each dr As DataRow In ds.Tables(0).Rows
                        labels.Append("'" & CCommon.ToString(dr("vcItemClassification")) & "',")
                        data.Append(CCommon.ToDouble(dr("BlendedProfit")) & ",")
                    Next

                    Dim chartScript As String = System.IO.File.ReadAllText(Server.MapPath("~/ReportDashboard/PrebuildReportTemplate/ChartJsMasterScriptPieChart.txt"))
                    chartScript = chartScript.Replace("##CHARTDATA##", "chartData" & _DashBoardID)
                    chartScript = chartScript.Replace("##LABELS##", labels.ToString.Trim(","))
                    chartScript = chartScript.Replace("##DATA##", data.ToString.Trim(","))
                    chartScript = chartScript.Replace("##CHARTID##", "myChart" & _DashBoardID)
                    chartScript = chartScript.Replace("##CHARTVAR##", "myReportChart" & _DashBoardID)
                    chartScript = chartScript.Replace("##CHARTTYPE##", "pie")
                    chartScript = chartScript.Replace("##LegendID##", "#" & divLegend.ID)
                    chartScript = chartScript.Replace("##BORDERWIDTH##", "0")

                    Dim canvas As New HtmlGenericControl("canvas")
                    canvas.ID = "myChart" & _DashBoardID
                    canvas.Style.Add("width", "100%")
                    canvas.Style.Add("heigth", "100%")
                    divChart.Controls.Add(canvas)


                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "7-ProftByItemClassification", chartScript, True)
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.SalesVsExpense Then
                divLegend.ID = "divLegend13-SalesVsExpenseLast12Months"
                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.SalesVsExpense
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim labels As New System.Text.StringBuilder()
                    Dim dataSales As New System.Text.StringBuilder()
                    Dim dataExpense As New System.Text.StringBuilder()
                    For Each dr As DataRow In ds.Tables(0).Rows
                        labels.Append("'" & CCommon.ToString(dr("MonthLabel")) & "',")
                        dataSales.Append(CCommon.ToDouble(dr("MonthSales")) & ",")
                        dataExpense.Append(CCommon.ToDouble(dr("MonthExpense")) & ",")
                    Next

                    Dim chartScript As String = System.IO.File.ReadAllText(Server.MapPath("~/ReportDashboard/PrebuildReportTemplate/ChartJsMasterScriptLineChart.txt"))
                    chartScript = chartScript.Replace("##LABELS##", labels.ToString.Trim(","))
                    chartScript = chartScript.Replace("##DATASETS##", "{label:'Sales',backgroundColor: transparentize(window.chartColors.green),borderColor: window.chartColors.green,data: [" & dataSales.ToString.Trim(", ") & "],fill: 'start'},{label:'Expense',backgroundColor: transparentize(window.chartColors.yellow),borderColor: window.chartColors.yellow,data: [" & dataExpense.ToString.Trim(", ") & "],fill: 'start'}")
                    chartScript = chartScript.Replace("##CHARTID##", "myChart" & _DashBoardID) 'transparentize(window.chartColors.green)
                    chartScript = chartScript.Replace("##CHARTVAR##", "myReportChart" & _DashBoardID)
                    chartScript = chartScript.Replace("##CHARTTYPE##", "line")
                    chartScript = chartScript.Replace("##XAXISTITLE##", "Month")
                    chartScript = chartScript.Replace("##YAXISTITLE##", "Value")
                    chartScript = chartScript.Replace("##LegendID##", "#" & divLegend.ID)

                    Dim canvas As New HtmlGenericControl("canvas")
                    canvas.ID = "myChart" & _DashBoardID
                    canvas.Style.Add("width", "100%")
                    canvas.Style.Add("heigth", "100%")
                    divChart.Controls.Add(canvas)


                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "13-SalesVsExpenseLast12Months", chartScript, True)
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.TopCustomersByPortionOfTotalSales Then
                divLegend.ID = "divLegend21-TopCustomersByPortionOfTotalSales"
                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.TopCustomersByPortionOfTotalSales
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim labels As New System.Text.StringBuilder()
                    Dim data As New System.Text.StringBuilder()
                    For Each dr As DataRow In ds.Tables(0).Rows
                        labels.Append("'" & CCommon.ToString(dr("vcCompanyName")) & "',")
                        data.Append(CCommon.ToDouble(dr("TotalSalesPercent")) & ",")
                    Next

                    Dim chartScript As String = System.IO.File.ReadAllText(Server.MapPath("~/ReportDashboard/PrebuildReportTemplate/ChartJsMasterScriptPieChart.txt"))
                    chartScript = chartScript.Replace("##CHARTDATA##", "chartData" & _DashBoardID)
                    chartScript = chartScript.Replace("##LABELS##", labels.ToString.Trim(","))
                    chartScript = chartScript.Replace("##DATA##", data.ToString.Trim(","))
                    chartScript = chartScript.Replace("##CHARTID##", "myChart" & _DashBoardID)
                    chartScript = chartScript.Replace("##CHARTVAR##", "myReportChart" & _DashBoardID)
                    chartScript = chartScript.Replace("##CHARTTYPE##", "horizontalBar")
                    chartScript = chartScript.Replace("##LegendID##", "#" & divLegend.ID)
                    chartScript = chartScript.Replace("##BORDERWIDTH##", "0")

                    Dim canvas As New HtmlGenericControl("canvas")
                    canvas.ID = "myChart" & _DashBoardID
                    canvas.Style.Add("width", "100%")
                    canvas.Style.Add("heigth", "100%")
                    divChart.Controls.Add(canvas)


                    ScriptManager.RegisterClientScriptBlock(Me, Me.GetType(), "divLegend21-TopCustomersByPortionOfTotalSales", chartScript, True)
                End If
            End If

            Return True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)

            divChart.Controls.Clear()
            Dim label As New HtmlGenericControl("label")
            label.Style.Add("color", "red")
            label.InnerText = "Error ocurred while rendering this report"
            divChart.Controls.Add(label)

            Return False
        End Try
    End Function

#End Region

End Class