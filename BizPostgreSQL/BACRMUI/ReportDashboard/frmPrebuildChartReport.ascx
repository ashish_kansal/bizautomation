﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmPrebuildChartReport.ascx.vb" Inherits=".frmPrebuildChartReport" %>
<div class="box box-primary">
    <div class="box-header handle">
        <h3 class="box-title">
            <asp:Label Text="" ID="lblHeader" runat="server" />
        </h3>
        <div id="tdEdit" runat="server" class="box-tools pull-right">
            <asp:HyperLink ID="hplExportToExcel" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/excel.png" height="16" /></asp:HyperLink>
            <asp:HyperLink ID="hplEdit" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/pencil-24.gif" height="16" /></asp:HyperLink>
            <asp:HyperLink ID="hplDelete" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/cancel.png" /></asp:HyperLink>
        </div>
    </div>
    <div class="box-body rptcontent">
        <div id="divChart" runat="server" clientidmode="Static">
        </div>
        <div id="divLegend" runat="server">
        </div>
    </div>
</div>
