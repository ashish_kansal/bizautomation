﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports
Public Class frmPrebuildFixFormatReport
    Inherits BACRMUserControl

#Region "Public Properties"

    Public Property DashBoardID As Long
    Public Property EditRept As Boolean
    Public Property Width As Integer
    Public Property objReportManage As CustomReportsManage
    Public Property DefaultReportID As Integer
    Public Property IsRefresh As Boolean

#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#End Region

#Region "Public Methods"

    Public Function CreateReport() As Boolean
        Dim reportHtml As String = ""

        Try
            _objReportManage.DomainID = Session("DomainID")
            _objReportManage.DashBoardID = _DashBoardID
            Dim dtReportData As DataTable = objReportManage.GetReportDashboardDTL

            If DefaultReportID = CustomReportsManage.PrebuildReportEnum.RPEYTDvsSamePeriodLastYear Then
                reportHtml = System.IO.File.ReadAllText(Server.MapPath("~/ReportDashboard/PrebuildReportTemplate/RPEYear.txt"))
                reportHtml = reportHtml.Replace("##EDITURL##", ResolveUrl("~/ReportDashboard/frmAddDashboardReport.aspx?DID=" & _DashBoardID))
                reportHtml = reportHtml.Replace("##DELETEURL##", ResolveUrl("~/ReportDashboard/frmManageDashBoard.aspx?Move=X&DID=" & _DashBoardID))

                If Not dtReportData Is Nothing AndAlso dtReportData.Rows.Count > 0 Then
                    reportHtml = reportHtml.Replace("##HEADER##", CCommon.ToString(dtReportData.Rows(0).Item("vcHeaderText")))
                    reportHtml = reportHtml.Replace("##FOOTER##", CCommon.ToString(dtReportData.Rows(0).Item("vcFooterText")))
                End If

                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.RPEYTDvsSamePeriodLastYear
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    If CCommon.ToDouble(ds.Tables(0).Rows(0)("RevenueDifference")) > 0 Then
                        reportHtml = reportHtml.Replace("##REVENUE##", "<lable style='color:#00a65a; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToInteger(ds.Tables(0).Rows(0)("RevenueDifference"))) & "% <i class=""fa fa-arrow-up"" aria-hidden=""true""></i></lable>")
                    Else
                        reportHtml = reportHtml.Replace("##REVENUE##", "<lable style='color:#dd4b39; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToInteger(ds.Tables(0).Rows(0)("RevenueDifference"))) * -1 & "% <i class=""fa fa-arrow-down"" aria-hidden=""true""></i></lable>")
                    End If

                    If CCommon.ToDouble(ds.Tables(0).Rows(0)("ProfitDifference")) > 0 Then
                        reportHtml = reportHtml.Replace("##PROFIT##", "<lable style='color:#00a65a; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToInteger(ds.Tables(0).Rows(0)("ProfitDifference"))) & "% <i class=""fa fa-arrow-up"" aria-hidden=""true""></i></lable>")
                    Else
                        reportHtml = reportHtml.Replace("##PROFIT##", "<lable style='color:#dd4b39; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToInteger(ds.Tables(0).Rows(0)("ProfitDifference"))) * -1 & "% <i class=""fa fa-arrow-down"" aria-hidden=""true""></i></lable>")
                    End If

                    If CCommon.ToDouble(ds.Tables(0).Rows(0)("ExpenseDifference")) > 0 Then
                        reportHtml = reportHtml.Replace("##EXPENSE##", "<lable style='color:#dd4b39; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToInteger(ds.Tables(0).Rows(0)("ExpenseDifference"))) & "% <i class=""fa fa-arrow-up"" aria-hidden=""true""></i></lable>")
                    Else
                        reportHtml = reportHtml.Replace("##EXPENSE##", "<lable style='color:#00a65a; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToInteger(ds.Tables(0).Rows(0)("ExpenseDifference"))) * -1 & "% <i class=""fa fa-arrow-down"" aria-hidden=""true""></i></lable>")
                    End If
                Else
                    reportHtml = reportHtml.Replace("##REVENUE##", "")
                    reportHtml = reportHtml.Replace("##PROFIT##", "")
                    reportHtml = reportHtml.Replace("##EXPENSE##", "")
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.RPELastMonthvsSamePeriodLastYear Then
                reportHtml = System.IO.File.ReadAllText(Server.MapPath("~/ReportDashboard/PrebuildReportTemplate/RPEYear.txt"))
                reportHtml = reportHtml.Replace("##EDITURL##", ResolveUrl("~/ReportDashboard/frmAddDashboardReport.aspx?DID=" & _DashBoardID))
                reportHtml = reportHtml.Replace("##DELETEURL##", ResolveUrl("~/ReportDashboard/frmManageDashBoard.aspx?Move=X&DID=" & _DashBoardID))

                If Not dtReportData Is Nothing AndAlso dtReportData.Rows.Count > 0 Then
                    reportHtml = reportHtml.Replace("##HEADER##", CCommon.ToString(dtReportData.Rows(0).Item("vcHeaderText")))
                    reportHtml = reportHtml.Replace("##FOOTER##", CCommon.ToString(dtReportData.Rows(0).Item("vcFooterText")))
                End If

                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.RPELastMonthvsSamePeriodLastYear
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    If CCommon.ToDouble(ds.Tables(0).Rows(0)("RevenueDifference")) > 0 Then
                        reportHtml = reportHtml.Replace("##REVENUE##", "<lable style='color:#00a65a; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToDouble(ds.Tables(0).Rows(0)("RevenueDifference"))) & "% <i class=""fa fa-arrow-up"" aria-hidden=""true""></i></lable>")
                    Else
                        reportHtml = reportHtml.Replace("##REVENUE##", "<lable  style='color:#dd4b39; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToDouble(ds.Tables(0).Rows(0)("RevenueDifference"))) * -1 & "% <i class=""fa fa-arrow-down"" aria-hidden=""true""></i></lable>")
                    End If

                    If CCommon.ToDouble(ds.Tables(0).Rows(0)("ProfitDifference")) > 0 Then
                        reportHtml = reportHtml.Replace("##PROFIT##", "<lable style='color:#00a65a; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToDouble(ds.Tables(0).Rows(0)("ProfitDifference"))) & "% <i class=""fa fa-arrow-up"" aria-hidden=""true""></i></lable>")
                    Else
                        reportHtml = reportHtml.Replace("##PROFIT##", "<lable  style='color:#dd4b39; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToDouble(ds.Tables(0).Rows(0)("ProfitDifference"))) * -1 & "% <i class=""fa fa-arrow-down"" aria-hidden=""true""></i></lable>")
                    End If

                    If CCommon.ToDouble(ds.Tables(0).Rows(0)("ExpenseDifference")) > 0 Then
                        reportHtml = reportHtml.Replace("##EXPENSE##", "<lable  style='color:#dd4b39; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToDouble(ds.Tables(0).Rows(0)("ExpenseDifference"))) & "% <i class=""fa fa-arrow-up"" aria-hidden=""true""></i></lable>")
                    Else
                        reportHtml = reportHtml.Replace("##EXPENSE##", "<lable style='color:#00a65a; font-size:30px'>" & String.Format("{0:#,##0.##}", CCommon.ToDouble(ds.Tables(0).Rows(0)("ExpenseDifference"))) * -1 & "% <i class=""fa fa-arrow-down"" aria-hidden=""true""></i></lable>")
                    End If
                Else
                    reportHtml = reportHtml.Replace("##REVENUE##", "")
                    reportHtml = reportHtml.Replace("##PROFIT##", "")
                    reportHtml = reportHtml.Replace("##EXPENSE##", "")
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.KPI Then
                reportHtml = System.IO.File.ReadAllText(Server.MapPath("~/ReportDashboard/PrebuildReportTemplate/Scorecard.txt"))
                reportHtml = reportHtml.Replace("##EDITURL##", ResolveUrl("~/ReportDashboard/frmAddDashboardReport.aspx?DID=" & _DashBoardID))
                reportHtml = reportHtml.Replace("##DELETEURL##", ResolveUrl("~/ReportDashboard/frmManageDashBoard.aspx?Move=X&DID=" & _DashBoardID))

                If Not dtReportData Is Nothing AndAlso dtReportData.Rows.Count > 0 Then
                    reportHtml = reportHtml.Replace("##HEADER##", CCommon.ToString(dtReportData.Rows(0).Item("vcHeaderText")))
                    reportHtml = reportHtml.Replace("##FOOTER##", CCommon.ToString(dtReportData.Rows(0).Item("vcFooterText")))
                End If

                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.KPI
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim num1 As Double = CCommon.ToDouble(ds.Tables(0).Select("ID=1")(0)("numChange"))
                    Dim num2 As Double = CCommon.ToDouble(ds.Tables(0).Select("ID=2")(0)("numChange"))
                    Dim num3 As Double = CCommon.ToDouble(ds.Tables(0).Select("ID=3")(0)("numChange"))
                    Dim num4 As Double = CCommon.ToDouble(ds.Tables(0).Select("ID=4")(0)("numChange"))
                    Dim num5 As Double = CCommon.ToDouble(ds.Tables(0).Select("ID=5")(0)("numChange"))
                    Dim num6 As Double = CCommon.ToDouble(ds.Tables(0).Select("ID=6")(0)("numChange"))
                    Dim num7 As Double = CCommon.ToDouble(ds.Tables(0).Select("ID=7")(0)("numChange"))

                    reportHtml = reportHtml.Replace("##1##", If(num1 = 0, "", "<lable style='color:" & If(num1 > 0, "#00a65a", "#dd4b39") & ";'>") & String.Format("{0:#,##0.##}", num1) & "% " & If(num1 = 0, "", "<i class=""fa " & If(num1 > 0, "fa-arrow-up", "fa-arrow-down") & """ aria-hidden=""true""></i>") & "</lable>")
                    reportHtml = reportHtml.Replace("##2##", If(num2 = 0, "", "<lable style='color:" & If(num2 > 0, "#00a65a", "#dd4b39") & ";'>") & String.Format("{0:#,##0.##}", num2) & "% " & If(num2 = 0, "", "<i class=""fa " & If(num2 > 0, "fa-arrow-up", "fa-arrow-down") & """ aria-hidden=""true""></i>") & "</lable>")
                    reportHtml = reportHtml.Replace("##3##", If(num3 = 0, "", "<lable style='color:" & If(num3 > 0, "#00a65a", "#dd4b39") & ";'>") & String.Format("{0:#,##0.##}", num3) & "% " & If(num3 = 0, "", "<i class=""fa " & If(num3 > 0, "fa-arrow-up", "fa-arrow-down") & """ aria-hidden=""true""></i>") & "</lable>")
                    reportHtml = reportHtml.Replace("##4##", If(num4 = 0, "", "<lable style='color:" & If(num4 > 0, "#00a65a", "#dd4b39") & ";'>") & String.Format("{0:#,##0.##}", num4) & "% " & If(num4 = 0, "", "<i class=""fa " & If(num4 > 0, "fa-arrow-up", "fa-arrow-down") & """ aria-hidden=""true""></i>") & "</lable>")
                    reportHtml = reportHtml.Replace("##5##", If(num5 = 0, "", "<lable style='color:" & If(num5 > 0, "#00a65a", "#dd4b39") & ";'>") & String.Format("{0:#,##0.##}", num5) & "% " & If(num5 = 0, "", "<i class=""fa " & If(num5 > 0, "fa-arrow-up", "fa-arrow-down") & """ aria-hidden=""true""></i>") & "</lable>")
                    reportHtml = reportHtml.Replace("##6##", If(num6 = 0, "", "<lable style='color:" & If(num6 > 0, "#00a65a", "#dd4b39") & ";'>") & String.Format("{0:#,##0.##}", num6) & "% " & If(num6 = 0, "", "<i class=""fa " & If(num6 > 0, "fa-arrow-up", "fa-arrow-down") & """ aria-hidden=""true""></i>") & "</lable>")
                    reportHtml = reportHtml.Replace("##7##", If(num7 = 0, "", "<lable style='color:" & If(num7 > 0, "#00a65a", "#dd4b39") & ";'>") & String.Format("{0:#,##0.##}", num7) & "% " & If(num7 = 0, "", "<i class=""fa " & If(num7 > 0, "fa-arrow-up", "fa-arrow-down") & """ aria-hidden=""true""></i>") & "</lable>")
                Else
                    reportHtml = reportHtml.Replace("##1##", "")
                    reportHtml = reportHtml.Replace("##2##", "")
                    reportHtml = reportHtml.Replace("##3##", "")
                    reportHtml = reportHtml.Replace("##4##", "")
                    reportHtml = reportHtml.Replace("##5##", "")
                    reportHtml = reportHtml.Replace("##6##", "")
                    reportHtml = reportHtml.Replace("##7##", "")
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10Email Then
                reportHtml = System.IO.File.ReadAllText(Server.MapPath("~/ReportDashboard/PrebuildReportTemplate/Email.txt"))
                reportHtml = reportHtml.Replace("##EDITURL##", ResolveUrl("~/ReportDashboard/frmAddDashboardReport.aspx?DID=" & _DashBoardID))
                reportHtml = reportHtml.Replace("##DELETEURL##", ResolveUrl("~/ReportDashboard/frmManageDashBoard.aspx?Move=X&DID=" & _DashBoardID))

                If Not dtReportData Is Nothing AndAlso dtReportData.Rows.Count > 0 Then
                    reportHtml = reportHtml.Replace("##HEADER##", CCommon.ToString(dtReportData.Rows(0).Item("vcHeaderText")))
                    reportHtml = reportHtml.Replace("##FOOTER##", CCommon.ToString(dtReportData.Rows(0).Item("vcFooterText")))
                End If

                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.Top10Email
                Dim ds As DataSet = objReportManage.GetPrebuildReportData()

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    Dim tempString As String = "<table style=width:100%>"

                    For Each dr As DataRow In ds.Tables(0).Rows
                        tempString += "<tr>"
                        tempString += "<td style=""color:#ffeb3b;padding-right:10px;""><i class=""fa fa-envelope"" aria-hidden=""true""></i></td>"
                        tempString += "<td style=""white-space:nowrap;padding-right: 3px;"">" & CCommon.ToString(dr("vcCompanyName")) & " | </td>"
                        tempString += "<td style=""white-space:nowrap;padding-right: 3px;"">" & CCommon.ToString(dr("vcFrom")) & " | </td>"
                        tempString += "<td style=""white-space:nowrap;padding-right: 3px;"">" & CCommon.ToString(dr("vcSubject")) & " | </td>"
                        tempString += "<td style=""width:100%"">" & CCommon.ToString(dr("vcBodyText")) & "</td>"
                        tempString += "</tr>"
                    Next

                    tempString += "</table>"
                    reportHtml = reportHtml.Replace("##REPORTDATA##", tempString)
                Else
                    reportHtml = reportHtml.Replace("##REPORTDATA##", "")
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.Reminders Then
                reportHtml = System.IO.File.ReadAllText(Server.MapPath("~/ReportDashboard/PrebuildReportTemplate/Reminders.txt"))
                reportHtml = reportHtml.Replace("##EDITURL##", ResolveUrl("~/ReportDashboard/frmAddDashboardReport.aspx?DID=" & _DashBoardID))
                reportHtml = reportHtml.Replace("##DELETEURL##", ResolveUrl("~/ReportDashboard/frmManageDashBoard.aspx?Move=X&DID=" & _DashBoardID))

                If Not dtReportData Is Nothing AndAlso dtReportData.Rows.Count > 0 Then
                    reportHtml = reportHtml.Replace("##HEADER##", CCommon.ToString(dtReportData.Rows(0).Item("vcHeaderText")))
                    reportHtml = reportHtml.Replace("##FOOTER##", CCommon.ToString(dtReportData.Rows(0).Item("vcFooterText")))
                End If


                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.Reminders
                Dim ds As DataSet = objReportManage.GetPrebuildReportData()

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    reportHtml = reportHtml.Replace("##VALUE1##", CCommon.ToInteger(ds.Tables(0).Select("ID=1")(0)("Value")))
                    reportHtml = reportHtml.Replace("##VALUE2##", CCommon.ToInteger(ds.Tables(0).Select("ID=2")(0)("Value")))
                    reportHtml = reportHtml.Replace("##VALUE3##", CCommon.ToInteger(ds.Tables(0).Select("ID=3")(0)("Value")))
                    reportHtml = reportHtml.Replace("##VALUE4##", CCommon.ToInteger(ds.Tables(0).Select("ID=4")(0)("Value")))
                    reportHtml = reportHtml.Replace("##VALUE5##", CCommon.ToInteger(ds.Tables(0).Select("ID=5")(0)("Value")))
                    reportHtml = reportHtml.Replace("##VALUE6##", CCommon.ToInteger(ds.Tables(0).Select("ID=6")(0)("Value")))
                    reportHtml = reportHtml.Replace("##VALUE7##", CCommon.ToInteger(ds.Tables(0).Select("ID=7")(0)("Value")))
                    reportHtml = reportHtml.Replace("##VALUE8##", CCommon.ToInteger(ds.Tables(0).Select("ID=8")(0)("Value")))
                    reportHtml = reportHtml.Replace("##VALUE9##", CCommon.ToInteger(ds.Tables(0).Select("ID=9")(0)("Value")))
                    reportHtml = reportHtml.Replace("##VALUE10##", CCommon.ToInteger(ds.Tables(0).Select("ID=10")(0)("Value")))
                    reportHtml = reportHtml.Replace("##VALUE11##", CCommon.ToInteger(ds.Tables(0).Select("ID=11")(0)("Value")))
                    reportHtml = reportHtml.Replace("##VALUE12##", CCommon.ToInteger(ds.Tables(0).Select("ID=12")(0)("Value")))
                    reportHtml = reportHtml.Replace("##VALUE13##", CCommon.ToInteger(ds.Tables(0).Select("ID=13")(0)("Value")))
                    reportHtml = reportHtml.Replace("##VALUE14##", CCommon.ToInteger(ds.Tables(0).Select("ID=14")(0)("Value")))
                    reportHtml = reportHtml.Replace("##VALUE15##", CCommon.ToInteger(ds.Tables(0).Select("ID=15")(0)("Value")))
                    reportHtml = reportHtml.Replace("##VALUE16##", CCommon.ToInteger(ds.Tables(0).Select("ID=16")(0)("Value")))
                    reportHtml = reportHtml.Replace("##VALUE17##", CCommon.ToInteger(ds.Tables(0).Select("ID=17")(0)("Value")))
                    reportHtml = reportHtml.Replace("##VALUE18##", CCommon.ToInteger(ds.Tables(0).Select("ID=18")(0)("Value")))
                    reportHtml = reportHtml.Replace("##VALUE19##", CCommon.ToInteger(ds.Tables(0).Select("ID=19")(0)("Value")))

                    If CCommon.ToInteger(ds.Tables(0).Select("ID=15")(0)("Value")) > 0 Then
                        reportHtml = reportHtml.Replace("##LABELCLASS15##", "bg-red")
                    Else
                        reportHtml = reportHtml.Replace("##LABELCLASS15##", "bg-primary")
                    End If
                Else
                    reportHtml = reportHtml.Replace("##VALUE1##", "0")
                    reportHtml = reportHtml.Replace("##VALUE2##", "0")
                    reportHtml = reportHtml.Replace("##VALUE3##", "0")
                    reportHtml = reportHtml.Replace("##VALUE4##", "0")
                    reportHtml = reportHtml.Replace("##VALUE5##", "0")
                    reportHtml = reportHtml.Replace("##VALUE6##", "0")
                    reportHtml = reportHtml.Replace("##VALUE7##", "0")
                    reportHtml = reportHtml.Replace("##VALUE8##", "0")
                    reportHtml = reportHtml.Replace("##VALUE9##", "0")
                    reportHtml = reportHtml.Replace("##VALUE10##", "0")
                    reportHtml = reportHtml.Replace("##VALUE11##", "0")
                    reportHtml = reportHtml.Replace("##VALUE12##", "0")
                    reportHtml = reportHtml.Replace("##VALUE13##", "0")
                    reportHtml = reportHtml.Replace("##VALUE14##", "0")
                    reportHtml = reportHtml.Replace("##VALUE15##", "0")
                    reportHtml = reportHtml.Replace("##VALUE16##", "0")
                    reportHtml = reportHtml.Replace("##VALUE17##", "0")
                    reportHtml = reportHtml.Replace("##VALUE18##", "0")
                    reportHtml = reportHtml.Replace("##VALUE19##", "0")
                    reportHtml = reportHtml.Replace("##LABELCLASS15##", "bg-primary")
                    reportHtml = reportHtml.Replace("##DISPLAY16##", "none")
                End If
            End If

            reportHtml = reportHtml.Replace("##ERROR##", "")
            litHtml.Text = reportHtml

            Return True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            reportHtml = reportHtml.Replace("##ERROR##", "<label style=""color:red"">Error ocurred while rendering this report</lable>")

            litHtml.Text = reportHtml
        End Try
    End Function

#End Region

End Class