﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports
Public Class frmPrebuildBackOrderValuation
    Inherits BACRMUserControl
    'Inherits System.Web.UI.UserControl

#Region "Public Properties"

    Public Property DashBoardID As Long
    Public Property EditRept As Boolean
    Public Property Width As Integer
    Public Property objReportManage As CustomReportsManage
    Public Property DefaultReportID As Integer
    Public Property IsRefresh As Boolean

#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
#End Region


#Region "Public Methods"

    Public Function CreateReport() As Boolean
        Try
            hplEdit.NavigateUrl = "../ReportDashboard/frmAddDashboardReport.aspx?DID=" & _DashBoardID
            hplDelete.NavigateUrl = "~/ReportDashboard/frmManageDashBoard.aspx?Move=X&DID=" & _DashBoardID

            _objReportManage.DomainID = Session("DomainID")
            _objReportManage.DashBoardID = _DashBoardID
            Dim dtReportData As DataTable = objReportManage.GetReportDashboardDTL

            If Not dtReportData Is Nothing AndAlso dtReportData.Rows.Count > 0 Then
                lblHeader.Text = CCommon.ToString(dtReportData.Rows(0).Item("vcHeaderText"))
                lblFooter.Text = CCommon.ToString(dtReportData.Rows(0).Item("vcFooterText"))
            End If

            objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
            objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
            objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))

            If DefaultReportID = CustomReportsManage.PrebuildReportEnum.BackOrderValuation Then
                'imgReportImage.ImageUrl = "~/images/Icon/salesopportunity70x70.png"

                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.BackOrderValuation

                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    rptReport.DataSource = ds.Tables(0)
                    rptReport.DataBind()
                    Dim FooterTemplate As Control = rptReport.Controls(rptReport.Controls.Count - 1).Controls(0)
                    Dim lblLabel As Label = TryCast(FooterTemplate.FindControl("lblLabel"), Label)
                    Dim lblTotal As Label = TryCast(FooterTemplate.FindControl("lblTotal"), Label)
                    Dim lblGrandTotal As Label = TryCast(FooterTemplate.FindControl("lblGrandTotal"), Label)
                    Dim hplGrandTotal As HyperLink = TryCast(FooterTemplate.FindControl("hplGrandTotal"), HyperLink)
                    Dim lblGrandTotal1 As Label = TryCast(FooterTemplate.FindControl("lblGrandTotal1"), Label)
                    Dim hplGrandTotal1 As HyperLink = TryCast(FooterTemplate.FindControl("hplGrandTotal1"), HyperLink)
                    lblLabel.Text = "Total"
                    lblTotal.Text = String.Format("{0:#,##0.##}", Convert.ToInt32(ds.Tables(0).Compute("SUM(TotalRecords)", String.Empty)))
                    lblGrandTotal.Text = "$ " + String.Format("{0:#,##0.##}", Convert.ToInt32(ds.Tables(0).Compute("SUM(SalesPrice)", String.Empty)))
                    hplGrandTotal.Text = "$ " + String.Format("{0:#,##0.##}", Convert.ToInt32(ds.Tables(0).Compute("SUM(SalesPrice)", String.Empty)))
                    hplGrandTotal.NavigateUrl = ResolveUrl("~/Opportunity/frmDealList.aspx?type=1&FilterBy=" & ds.Tables(0).Rows(0).Item("vcFilterBy") & "&InventorySort=3") '& "&SelectedID=0") '& ds.Tables(0).Rows(0).Item("ID"))
                    lblGrandTotal.Visible = False
                    lblGrandTotal1.Text = "(" + "$" + String.Format("{0:#,##0.##}", Convert.ToInt32(ds.Tables(0).Compute("SUM(CostPrice)", String.Empty))) + ")"
                    hplGrandTotal1.Text = "(" + "$" + String.Format("{0:#,##0.##}", Convert.ToInt32(ds.Tables(0).Compute("SUM(CostPrice)", String.Empty))) + ")"
                    hplGrandTotal1.NavigateUrl = ResolveUrl("~/Opportunity/frmDealList.aspx?type=1&FilterBy=" & ds.Tables(0).Rows(0).Item("vcFilterBy") & "&InventorySort=3")
                    lblGrandTotal1.Visible = False
                End If
            End If
            Return True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            divReportContent.Visible = False
            lblError.Visible = True
        End Try

    End Function

#End Region

#Region "Event Handlers"
    Private Sub rptReport_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptReport.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Header Then
                Dim lblHeader1 As Label = DirectCast(e.Item.FindControl("lblHeader1"), Label)
                Dim lblHeader2 As Label = DirectCast(e.Item.FindControl("lblHeader2"), Label)
                Dim lblHeader3 As Label = DirectCast(e.Item.FindControl("lblHeader3"), Label)


                If Not lblHeader1 Is Nothing Then
                    If DefaultReportID = CustomReportsManage.PrebuildReportEnum.BackOrderValuation Then
                        lblHeader1.Text = "Resource "
                    End If
                End If
                If Not lblHeader2 Is Nothing Then
                    If DefaultReportID = CustomReportsManage.PrebuildReportEnum.BackOrderValuation Then
                        lblHeader2.Text = "Total Records"
                    End If
                End If
                If Not lblHeader3 Is Nothing Then
                    If DefaultReportID = CustomReportsManage.PrebuildReportEnum.BackOrderValuation Then
                        lblHeader3.Text = "Sales(Cost)"
                    End If
                End If

            End If

            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim hplTitle As HyperLink = DirectCast(e.Item.FindControl("hplTitle"), HyperLink)
                Dim lblTitle As Label = DirectCast(e.Item.FindControl("lblTitle"), Label)
                Dim lblValue1 As Label = DirectCast(e.Item.FindControl("lblValue1"), Label)
                Dim lblValue2 As Label = DirectCast(e.Item.FindControl("lblValue2"), Label)
                Dim lblValue3 As Label = DirectCast(e.Item.FindControl("lblValue3"), Label)
                Dim hplValue2 As HyperLink = DirectCast(e.Item.FindControl("hplValue2"), HyperLink)
                Dim hplValue3 As HyperLink = DirectCast(e.Item.FindControl("hplValue3"), HyperLink)
                'Dim lblValue4 As Label = DirectCast(e.Item.FindControl("lblValue4"), Label)
                Dim item As DataRowView = e.Item.DataItem

                If DefaultReportID = CustomReportsManage.PrebuildReportEnum.BackOrderValuation Then
                    hplTitle.Text = CCommon.ToString(item("RecordOwner"))
                    hplTitle.NavigateUrl = ResolveUrl("~/Opportunity/frmDealList.aspx?type=1&FilterBy=" & CCommon.ToString(item("vcFilterBy")) & "&InventorySort=3" & "&SelectedID=" & CCommon.ToInteger(item("ID")))
                    lblTitle.Visible = False
                    lblValue1.Text = String.Format("{0:#,##0.##}", CCommon.ToDouble(item("TotalRecords")))
                    lblValue3.Text = "($" + String.Format("{0:#,##0.##}", CCommon.ToDouble(item("CostPrice"))) + ")"
                    hplValue3.Text = "($" + String.Format("{0:#,##0.##}", CCommon.ToDouble(item("CostPrice"))) + ")"
                    hplValue3.NavigateUrl = ResolveUrl("~/Opportunity/frmDealList.aspx?type=1&FilterBy=" & CCommon.ToString(item("vcFilterBy")) & "&InventorySort=3" & "&SelectedID=" & CCommon.ToInteger(item("ID")))
                    lblValue3.Visible = False
                    lblValue2.Text = "$" + String.Format("{0:#,##0.##}", CCommon.ToDouble(item("SalesPrice")))
                    hplValue2.Text = "$" + String.Format("{0:#,##0.##}", CCommon.ToDouble(item("SalesPrice")))
                    hplValue2.NavigateUrl = ResolveUrl("~/Opportunity/frmDealList.aspx?type=1&FilterBy=" & CCommon.ToString(item("vcFilterBy")) & "&InventorySort=3" & "&SelectedID=" & CCommon.ToInteger(item("ID")))
                    lblValue2.Visible = False

                End If

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            divReportContent.Visible = False
            lblError.Visible = True
        End Try
    End Sub
#End Region
End Class