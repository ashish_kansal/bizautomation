﻿window.onloadFuncs.push(function () {
    var ctx = document.getElementById('##CHARTID##').getContext('2d');
    window.##CHARTVAR## = new Chart(ctx, {
        type:'##CHARTTYPE##'
        ,data:{
			 labels: [##LABELS##],
			 datasets: [##DATASETS##]
		} 
        ,options: {
            responsive: true,
			elements: {
				line: {
					tension: 0.000001
				}
			},
			scales: {
				xAxes: [{
					ticks: {
					  autoSkip: false,
					  maxRotation: 90,
					  minRotation: 0
					},
					scaleLabel: {display: true, labelString: '##XAXISTITLE##', forecolor:'black', fontSize:15}}],
				yAxes: [{
					ticks: {
						beginAtZero:true,
						callback: function(label, index, labels) {
							return '$' + Number(label);
						}
					},
					stacked:true,
					scaleLabel: {display: true, labelString: '##YAXISTITLE##', forecolor:'black', fontSize:15}
				}]
			},
            legend: {display: true,position:'bottom'}
        }
    });

	$("##LegendID##").css("display","none");
});
