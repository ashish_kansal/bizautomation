﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class frmNewDashBoard

    '''<summary>
    '''btnEdit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnEdit As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnNewReport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnNewReport As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''tblRept1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblRept1 As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''td1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents td1 As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''lnkNarrow1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkNarrow1 As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lnkMedium1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkMedium1 As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lnkWide1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkWide1 As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''phColumn1 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phColumn1 As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''tblRept2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblRept2 As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''td3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents td3 As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''lnkNarrow2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkNarrow2 As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lnkMedium2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkMedium2 As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lnkWide2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkWide2 As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''phColumn2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phColumn2 As Global.System.Web.UI.WebControls.PlaceHolder

    '''<summary>
    '''tblRept3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents tblRept3 As Global.System.Web.UI.HtmlControls.HtmlTable

    '''<summary>
    '''td5 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents td5 As Global.System.Web.UI.HtmlControls.HtmlTableCell

    '''<summary>
    '''lnkNarrow3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkNarrow3 As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lnkMedium3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkMedium3 As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lnkWide3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkWide3 As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''phColumn3 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents phColumn3 As Global.System.Web.UI.WebControls.PlaceHolder
End Class
