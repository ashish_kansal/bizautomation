﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmPrebuildMonthToDateRevenue.ascx.vb"
    Inherits=".frmPrebuildMonthToDateRevenue" %>
<div class="box box-primary">
    <div class="box-header with-border handle">
        <h3 class="box-title">
            <asp:Label Text="" ID="lblHeader" runat="server" />
        </h3>
        <div id="tdEdit" runat="server" class="box-tools pull-right">
            <asp:HyperLink ID="hplExportToExcel" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/excel.png" height="16" /></asp:HyperLink>
            <asp:HyperLink ID="hplEdit" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/pencil-24.gif" height="16" /></asp:HyperLink>
            <asp:HyperLink ID="hplDelete" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/cancel.png" /></asp:HyperLink>
        </div>
    </div>
    <div class="box-body rptcontent">
        <div class="row" id="divReportContent" runat="server">
            <div class="col-xs-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped" style="margin-bottom: 0px;">
                        <asp:Repeater ID="rptReport" runat="server">
                            <HeaderTemplate>
                                <thead>
                                <tr>
                                    <th>
                                        <asp:Label ID="lblHeader1" runat="server" Text="" Font-Bold="true"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="lblHeader2" runat="server" Text="" Font-Bold="true"></asp:Label>
                                    </th>
                                    <th>
                                        <asp:Label ID="lblHeader3" runat="server" Text="" Font-Bold="true"></asp:Label>
                                    </th>
                                </tr>
                                </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:HyperLink ID="hplTitle" runat="server" Target="_blank"></asp:HyperLink>
                                        <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblValue1" runat="server" Text=""></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblValue2" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                <tfoot>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblLabel" runat="server" Font-Bold="true" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblTotal" runat="server" Font-Bold="true" />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblGrandTotal" runat="server" Font-Bold="true" />
                                        </td>
                                    </tr>
                                </tfoot>
                            </FooterTemplate>
                        </asp:Repeater>
                    </table>
                </div>
            </div>
        </div>
        <asp:Label ID="lblError" runat="server" Text="Error ocurred while rendering this report"
            ForeColor="Red" Visible="false"></asp:Label>
    </div>
    <div class="box-footer footer">
        <asp:Label ID="lblFooter" runat="server" CssClass="text_bold"></asp:Label>
    </div>
</div>
