﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports

Public Class frmPrebuildPendingOrderStatus
    Inherits BACRMUserControl

#Region "Public Properties"

    Public Property DashBoardID As Long
    Public Property EditRept As Boolean
    Public Property Width As Integer
    Public Property objReportManage As CustomReportsManage
    Public Property DefaultReportID As Integer
    Public Property IsRefresh As Boolean

#End Region
#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
#End Region
#Region "Public Methods"
    Public Function CreateReport() As Boolean
        Try
            hplEdit.NavigateUrl = "../ReportDashboard/frmAddDashboardReport.aspx?DID=" & _DashBoardID
            hplDelete.NavigateUrl = "~/ReportDashboard/frmManageDashBoard.aspx?Move=X&DID=" & _DashBoardID

            _objReportManage.DomainID = Session("DomainID")
            _objReportManage.DashBoardID = _DashBoardID
            Dim dtReportData As DataTable = objReportManage.GetReportDashboardDTL

            If Not dtReportData Is Nothing AndAlso dtReportData.Rows.Count > 0 Then
                lblHeader.Text = CCommon.ToString(dtReportData.Rows(0).Item("vcHeaderText"))
                lblFooter.Text = CCommon.ToString(dtReportData.Rows(0).Item("vcFooterText"))
            End If

            If DefaultReportID = CustomReportsManage.PrebuildReportEnum.PendingReportDetails Then
                lblHeader1.Text = "Received But Not Billed"
                lblHeader2.Text = "Billed But Not Received"
                lblHeader3.Text = "Sold But Not Shipped"

                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.PendingReportDetails


                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    rptReport1.DataSource = ds.Tables(0)
                    rptReport1.DataBind()
                    rpt1.Visible = dtReportData.Rows(0).Item("bitReceviedButNotBilled")
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 1 AndAlso ds.Tables(1).Rows.Count > 0 Then
                    rptReport2.DataSource = ds.Tables(1)
                    rptReport2.DataBind()
                    rpt2.Visible = dtReportData.Rows(0).Item("bitBilledButNotReceived")
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 2 AndAlso ds.Tables(2).Rows.Count > 0 Then
                    rptReport3.DataSource = ds.Tables(2)
                    rptReport3.DataBind()
                    rpt3.Visible = dtReportData.Rows(0).Item("bitSoldButNotShipped")
                End If

            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            divReportContent.Visible = False
            lblError.Visible = True
        End Try
    End Function
#End Region
    Private Sub rptReport1_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptReport1.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim lblTitle As Label = DirectCast(e.Item.FindControl("lblTitle"), Label)
                Dim hplValue As HyperLink = DirectCast(e.Item.FindControl("hplValue"), HyperLink)
                Dim item As DataRowView = e.Item.DataItem
                If DefaultReportID = CustomReportsManage.PrebuildReportEnum.PendingReportDetails Then
                    lblTitle.Text = String.Format("{0:$#,##0.##}", CCommon.ToDouble(item("PendingToBill")))
                    hplValue.Text = String.Format("{0:$#,##0.##}", CCommon.ToDouble(item("PendingToBill")))
                    hplValue.NavigateUrl = ResolveUrl("~/opportunity/frmMassPurchaseFulfillment.aspx?View=3&ShowReceivedButNotBilled=2")
                    lblTitle.Visible = False
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            divReportContent.Visible = False
            lblError.Visible = True
        End Try
    End Sub
    Private Sub rptReport2_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptReport2.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim lblTitle As Label = DirectCast(e.Item.FindControl("lblTitle"), Label)
                Dim hplValue As HyperLink = DirectCast(e.Item.FindControl("hplValue"), HyperLink)

                Dim item As DataRowView = e.Item.DataItem
                If DefaultReportID = CustomReportsManage.PrebuildReportEnum.PendingReportDetails Then
                    lblTitle.Text = String.Format("{0:$#,##0.##}", CCommon.ToDouble(item("BilledButNotReceived")))
                    hplValue.Text = String.Format("{0:$#,##0.##}", CCommon.ToDouble(item("BilledButNotReceived")))
                    hplValue.NavigateUrl = ResolveUrl("~/opportunity/frmMassPurchaseFulfillment.aspx?View=1&ShowBilled=True")
                    lblTitle.Visible = False
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            divReportContent.Visible = False
            lblError.Visible = True
        End Try
    End Sub
    Private Sub rptReport3_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptReport3.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim lblTitle As Label = DirectCast(e.Item.FindControl("lblTitle"), Label)
                Dim hplValue As HyperLink = DirectCast(e.Item.FindControl("hplValue"), HyperLink)

                Dim item As DataRowView = e.Item.DataItem
                If DefaultReportID = CustomReportsManage.PrebuildReportEnum.PendingReportDetails Then
                    lblTitle.Text = String.Format("{0:$#,##0.##}", CCommon.ToDouble(item("PendingSales")))
                    hplValue.Text = String.Format("{0:$#,##0.##}", CCommon.ToDouble(item("PendingSales")))
                    hplValue.NavigateUrl = ResolveUrl("~/opportunity/frmMassSalesFulfillment.aspx?View=2")
                    lblTitle.Visible = False
                End If
            End If

        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            divReportContent.Visible = False
            lblError.Visible = True
        End Try
    End Sub
End Class