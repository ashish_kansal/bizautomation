﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports
Imports BACRM.BusinessLogic.Admin
Imports Telerik.Web.UI

Public Class frmAddDashboardReport
    Inherits BACRMPage

#Region "Member Variables"
    Dim objReportManage As New CustomReportsManage
#End Region

#Region "Page Events"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'CLEAR ERROR ON RELOAD
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = ""
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "none")

            If Not IsPostBack Then
                hdnDashboardTemplate.Value = CCommon.ToLong(GetQueryStringVal("DashboardTemplateID"))

                LoadDropdown()
                If GetQueryStringVal("DID") <> "" Then LoadDashboardReptDTL()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
#End Region

#Region "Private Methods"
    Private Sub LoadDropdown()
        Try
            Dim dtTable As DataTable

            If objReportManage Is Nothing Then objReportManage = New CustomReportsManage
            objReportManage.DomainID = Session("DomainId")
            objReportManage.UserCntID = Session("UserContactId")
            objReportManage.GroupId = Session("UserGroupID")

            'Biz Default Reports
            objReportManage.tintMode = 4
            dtTable = objReportManage.GetDashboardAllowedReports()

            Dim seperator As New RadComboBoxItem
            seperator.Text = "Predefined Reports" '"Pre-build Reports"'
            seperator.IsSeparator = True
            ddlCustomReport.Items.Add(seperator)

            For Each dr As DataRow In dtTable.Rows
                ddlCustomReport.Items.Add(New RadComboBoxItem(dr("vcReportName"), dr("numReportID") & "~1" & "~" & dr("intDefaultReportID")))
            Next

            seperator = New RadComboBoxItem
            seperator.Text = "Custom Reports"
            seperator.IsSeparator = True
            ddlCustomReport.Items.Add(seperator)
            'Only Allowed Custom Reports
            objReportManage.tintMode = 1
            dtTable = objReportManage.GetDashboardAllowedReports()

            For Each dr As DataRow In dtTable.Rows
                ddlCustomReport.Items.Add(New RadComboBoxItem(dr("vcReportName"), dr("numReportID") & "~0~0"))
            Next

            'Only Allowed KPI Groups Reports
            objReportManage.tintMode = 3
            dtTable = objReportManage.GetDashboardAllowedReports()

            For Each dr As DataRow In dtTable.Rows
                ddlCustomReport.Items.Add(New RadComboBoxItem(dr("vcKPIGroupName"), dr("numReportKPIGroupID") & "~1~0"))
            Next

            ddlCustomReport.Items.Insert(0, New RadComboBoxItem("--SelectOne--", "0~0~0"))

        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub GetReportDetail()
        Try
            If ddlCustomReport.SelectedValue.Split("~")(1) = 1 Then
                radChart.Visible = False

                trChartType.Visible = False
                trXAxis.Visible = False
                trYAxis.Visible = False
                hfReportType.Value = 0

            Else
                Dim ds As DataSet

                If objReportManage Is Nothing Then objReportManage = New CustomReportsManage
                objReportManage.ReportID = ddlCustomReport.SelectedValue.Split("~")(0)
                objReportManage.DomainID = Session("DomainID")

                ds = objReportManage.GetReportListMasterDetail
                hfReportType.Value = ds.Tables(0).Rows(0)("tintReportType")

                If ds.Tables(0).Rows(0)("tintReportType") = 0 Or ds.Tables(0).Rows(0).Item("tintReportType") = 4 Then 'Tabular or ScoreCard
                    radChart.Visible = False

                    trChartType.Visible = False
                    trXAxis.Visible = False
                    trYAxis.Visible = False
                ElseIf ds.Tables(0).Rows(0).Item("tintReportType") = 3 Then 'KPI
                    radChart.Visible = True

                    trChartType.Visible = True

                    ddlChartType.Items.Clear()
                    ddlChartType.Items.Add(New ListItem("Column Chart", "1"))
                    ddlChartType.Items.Add(New ListItem("Bar Chart", "2"))
                    ddlChartType.Items.Add(New ListItem("Line Chart", "8"))

                    trXAxis.Visible = False
                    trYAxis.Visible = False
                Else
                    radChart.Visible = True
                    trChartType.Visible = True
                    trXAxis.Visible = True
                    trYAxis.Visible = True

                    ddlChartType.Items.Clear()
                    ddlChartType.Items.Add(New ListItem("Column Chart", "1"))
                    ddlChartType.Items.Add(New ListItem("Bar Chart", "2"))
                    ddlChartType.Items.Add(New ListItem("Funnel Chart", "5"))
                    ddlChartType.Items.Add(New ListItem("Line Chart", "8"))
                    ddlChartType.Items.Add(New ListItem("Pie Chart", "12"))

                    Dim dtReportSummaryGroup As DataTable = ds.Tables(3)
                    Dim dtReportMatrixAggregate As DataTable = ds.Tables(4)

                    Dim dsColumnList As DataSet
                    dsColumnList = objReportManage.GetReportModuleGroupFieldMaster

                    ddlXAxis.Items.Clear()

                    For Each dr As DataRow In dtReportSummaryGroup.Rows
                        Dim dr1() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & dr("numReportFieldGroupID") & " AND numFieldID=" & dr("numFieldID") & " AND bitCustom=" & dr("bitCustom"))

                        If dr1.Length > 0 Then
                            ddlXAxis.Items.Add(New ListItem(dr1(0)("vcFieldName"), dr("numReportFieldGroupID") & "_" & dr("numFieldID") & "_" & dr("bitCustom")))
                        End If
                    Next

                    ddlYAxis.Items.Clear()
                    ddlYAxis.Items.Insert(0, New ListItem("Record Count", "0_0_0_RC"))

                    For Each dr As DataRow In dtReportMatrixAggregate.Rows
                        Dim dr1() As DataRow = dsColumnList.Tables(0).Select("numReportFieldGroupID=" & dr("numReportFieldGroupID") & " AND numFieldID=" & dr("numFieldID") & " AND bitCustom=" & dr("bitCustom"))

                        If dr1.Length > 0 Then
                            ddlYAxis.Items.Add(New ListItem(dr("vcAggType").ToString.ToUpper() & " of " & dr1(0)("vcFieldName"), dr("numReportFieldGroupID") & "_" & dr("numFieldID") & "_" & dr("bitCustom") & "_" & dr("vcAggType")))
                        End If
                    Next

                    If ddlXAxis.Items.Count = 0 Then
                        radChart.Visible = False

                        trChartType.Visible = False
                        trXAxis.Visible = False
                        trYAxis.Visible = False
                    End If
                End If
            End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub LoadDashboardReptDTL()
        Try
            If objReportManage Is Nothing Then objReportManage = New CustomReportsManage

            objReportManage.DomainID = Session("DomainID")
            objReportManage.DashBoardID = CCommon.ToLong(GetQueryStringVal("DID"))
            Dim dtTable As DataTable

            dtTable = objReportManage.GetReportDashboardDTL

            If dtTable.Rows.Count > 0 Then
                If Not ddlCustomReport.Items.FindItemByValue(dtTable.Rows(0).Item("numReportID") & "~" & dtTable.Rows(0).Item("tintReportCategory") & "~" & dtTable.Rows(0).Item("intDefaultReportID")) Is Nothing Then
                    ddlCustomReport.ClearSelection()
                    ddlCustomReport.Items.FindItemByValue(dtTable.Rows(0).Item("numReportID") & "~" & dtTable.Rows(0).Item("tintReportCategory") & "~" & dtTable.Rows(0).Item("intDefaultReportID")).Selected = True
                End If
                SetControlVisibility()
                GetReportDetail()

                If dtTable.Rows(0).Item("tintReportType") = 1 Then
                    radTable.Checked = True
                Else
                    radChart.Checked = True

                    If hfReportType.Value = "1" Or hfReportType.Value = "2" Then
                        If Not ddlChartType.Items.FindByValue(dtTable.Rows(0).Item("tintChartType")) Is Nothing Then
                            ddlChartType.ClearSelection()
                            ddlChartType.Items.FindByValue(dtTable.Rows(0).Item("tintChartType")).Selected = True
                        End If

                        If Not ddlXAxis.Items.FindByValue(dtTable.Rows(0).Item("vcXAxis")) Is Nothing Then
                            ddlXAxis.ClearSelection()
                            ddlXAxis.Items.FindByValue(dtTable.Rows(0).Item("vcXAxis")).Selected = True
                        End If

                        If Not ddlYAxis.Items.FindByValue(dtTable.Rows(0).Item("vcYAxis") & "_" & dtTable.Rows(0).Item("vcAggType")) Is Nothing Then
                            ddlYAxis.ClearSelection()
                            ddlYAxis.Items.FindByValue(dtTable.Rows(0).Item("vcYAxis") & "_" & dtTable.Rows(0).Item("vcAggType")).Selected = True
                        End If
                    End If
                End If

                If dtTable.Rows(0).Item("bitDealWon") = True Then
                    chkDealWon.Checked = True
                End If
                If dtTable.Rows(0).Item("bitGrossRevenue") = True Then
                    chkGrossRevenue.Checked = True
                End If
                If dtTable.Rows(0).Item("bitRevenue") = True Then
                    chkRevenue.Checked = True
                End If
                If dtTable.Rows(0).Item("bitGrossProfit") = True Then
                    chkGrossProfit.Checked = True
                End If
                If dtTable.Rows(0).Item("bitReceviedButNotBilled") <> Nothing Then
                    If dtTable.Rows(0).Item("bitReceviedButNotBilled") = True Then
                        chkpendingtobill.Checked = True
                    End If
                End If
                If dtTable.Rows(0).Item("bitBilledButNotReceived") <> Nothing Then
                    If dtTable.Rows(0).Item("bitBilledButNotReceived") = True Then
                        chkpendingtoreceive.Checked = True
                    End If
                End If
                If dtTable.Rows(0).Item("bitSoldButNotShipped") <> Nothing Then
                    If dtTable.Rows(0).Item("bitSoldButNotShipped") = True Then
                        chkpendingtosale.Checked = True
                    End If
                End If
                If CCommon.ToShort(dtTable.Rows(0)("vcFilterBy")) = 1 Then
                    radYeartoDate.Checked = True
                ElseIf CCommon.ToShort(dtTable.Rows(0)("vcFilterBy")) = 2 Then
                    radMonthtoDate.Checked = True
                ElseIf CCommon.ToShort(dtTable.Rows(0)("vcFilterBy")) = 3 Then
                    radWeektoDate.Checked = True
                    End If

                    If Not String.IsNullOrEmpty(CCommon.ToString(dtTable.Rows(0).Item("vcHeaderText"))) Then
                        txtHeader.Text = dtTable.Rows(0).Item("vcHeaderText")
                    End If
                    If Not String.IsNullOrEmpty(CCommon.ToString(dtTable.Rows(0).Item("vcFooterText"))) Then
                        txtFooter.Text = dtTable.Rows(0).Item("vcFooterText")
                    End If


                    If Not ddlTimeline.Items.FindByValue(CCommon.ToString(dtTable.Rows(0)("vcTimeLine"))) Is Nothing Then
                        ddlTimeline.Items.FindByValue(CCommon.ToString(dtTable.Rows(0)("vcTimeLine"))).Selected = True

                        If CCommon.ToString(dtTable.Rows(0)("vcTimeLine")) = "Custom" Then
                            divFromDate.Visible = True
                            divToDate.Visible = True
                            rdpFromDate.SelectedDate = Convert.ToDateTime(dtTable.Rows(0)("dtFromDate"))
                            rdpToDate.SelectedDate = Convert.ToDateTime(dtTable.Rows(0)("dtToDate"))

                        End If

                    End If


                If dtTable.Rows(0)("bitCustomTimeline") <> Nothing Then

                    If dtTable.Rows(0)("bitCustomTimeline") = True Then
                        dtTable.Rows(0)("bitCustomTimeline") = True
                        chkCustomTimeline.Checked = True
                        CtmFromDate.SelectedDate = dtTable.Rows(0)("vcTimeLine").ToString().Split("-"c)(0) 'fromDate.ToString
                        CtmToDate.SelectedDate = dtTable.Rows(0)("vcTimeLine").ToString().Split("-"c)(1) 'todate.ToString
                    End If

                End If
                If Not ddlDueDate.Items.FindByValue(CCommon.ToString(dtTable.Rows(0)("vcDueDate"))) Is Nothing Then
                        ddlDueDate.Items.FindByValue(CCommon.ToString(dtTable.Rows(0)("vcDueDate"))).Selected = True

                        If CCommon.ToString(dtTable.Rows(0)("vcDueDate")) = "Custom" Then
                            divDueFromDate.Visible = True
                            divDueToDate.Visible = True
                            rpdDueFromDate.SelectedDate = Convert.ToDateTime(dtTable.Rows(0)("dtFromDate"))
                            rpdDueToDate.SelectedDate = Convert.ToDateTime(dtTable.Rows(0)("dtToDate"))
                        End If
                    End If

                    If Not ddlGroupBy.Items.FindByValue(CCommon.ToString(dtTable.Rows(0)("vcGroupBy"))) Is Nothing Then
                        ddlGroupBy.Items.FindByValue(CCommon.ToString(dtTable.Rows(0)("vcGroupBy"))).Selected = True
                    End If

                    If CCommon.ToShort(dtTable.Rows(0)("vcFilterBy")) > 0 AndAlso Not String.IsNullOrEmpty(CCommon.ToString(dtTable.Rows(0)("vcFilterValue"))) Then
                        rcbFilterBy.Items.Clear()

                        If CCommon.ToShort(dtTable.Rows(0)("vcFilterBy")) = 1 Then
                            rbAssignedTo.Checked = True
                            objCommon.sb_FillConEmpFromDBSel(rcbFilterBy, CCommon.ToLong(Session("DomainID")), False, 0)
                            rcbFilterBy.Items.Remove(rcbFilterBy.Items(0))
                        ElseIf CCommon.ToShort(dtTable.Rows(0)("vcFilterBy")) = 2 Then
                            rbRecordOwner.Checked = True
                            objCommon.sb_FillConEmpFromDBSel(rcbFilterBy, CCommon.ToLong(Session("DomainID")), False, 0)
                            rcbFilterBy.Items.Remove(rcbFilterBy.Items(0))
                        ElseIf CCommon.ToShort(dtTable.Rows(0)("vcFilterBy")) = 3 Then
                            objReportManage.FilterBy = True
                            objCommon.sb_FillComboFromDBwithSel(rcbFilterBy, 35, Session("DomainID"))
                            rcbFilterBy.Items.Remove(rcbFilterBy.Items(0))
                        End If

                        For Each item As RadComboBoxItem In rcbFilterBy.Items
                            If ("," & CCommon.ToString(dtTable.Rows(0)("vcFilterValue")) & ",").Contains(item.Value) Then
                                item.Checked = True
                            End If
                        Next
                    End If

                    If Not String.IsNullOrEmpty(CCommon.ToString(dtTable.Rows(0)("vcTeritorry"))) Then
                        For Each item As RadComboBoxItem In rcbTerritory.Items
                            If ("," & CCommon.ToString(dtTable.Rows(0)("vcTeritorry")) & ",").Contains(item.Value) Then
                                item.Checked = True
                            End If
                        Next
                    End If

                    If ddlTimeline.SelectedValue = "Custom" Then
                        objReportManage.FromDate = rdpFromDate.SelectedDate
                        objReportManage.ToDate = rdpToDate.SelectedDate
                    End If

                    If Not ddlTop.Items.FindByValue(CCommon.ToString(dtTable.Rows(0)("numRecordCount"))) Is Nothing Then
                        ddlTop.Items.FindByValue(CCommon.ToString(dtTable.Rows(0)("numRecordCount"))).Selected = True
                    End If

                    If Not ddlControlList.Items.FindByValue(CCommon.ToString(dtTable.Rows(0)("tintControlField"))) Is Nothing Then
                        ddlControlList.Items.FindByValue(CCommon.ToString(dtTable.Rows(0)("tintControlField"))).Selected = True
                    End If

                    If Not ddlTotalProgress.Items.FindByValue(CCommon.ToString(dtTable.Rows(0)("tintTotalProgress"))) Is Nothing Then
                        ddlTotalProgress.Items.FindByValue(CCommon.ToString(dtTable.Rows(0)("tintTotalProgress"))).Selected = True
                    End If

                    If Not String.IsNullOrEmpty(CCommon.ToString(dtTable.Rows(0).Item("tintMinNumber"))) Then
                        txtMinAmountRange.Text = CCommon.ToString(dtTable.Rows(0).Item("tintMinNumber"))
                    End If
                    If Not String.IsNullOrEmpty(CCommon.ToString(dtTable.Rows(0).Item("tintMaxNumber"))) Then
                        txtMaxAmountRange.Text = CCommon.ToString(dtTable.Rows(0).Item("tintMaxNumber"))
                    End If

                    If Not ddlQtyToDisplay.Items.FindByValue(CCommon.ToString(dtTable.Rows(0)("tintQtyToDisplay"))) Is Nothing Then
                        ddlQtyToDisplay.Items.FindByValue(CCommon.ToString(dtTable.Rows(0)("tintQtyToDisplay"))).Selected = True
                    End If

                    If Not String.IsNullOrEmpty(CCommon.ToString(dtTable.Rows(0)("vcDealAmount"))) Then
                        For Each item As RadComboBoxItem In rcbDealAmountBetween.Items
                            If ("," & CCommon.ToString(dtTable.Rows(0)("vcDealAmount")) & ",").Contains(item.Value) Then
                                item.Checked = True
                            End If
                        Next
                    End If

                    If Not String.IsNullOrEmpty(CCommon.ToString(dtTable.Rows(0)("lngPConclAnalysis"))) Then
                        For Each item As RadComboBoxItem In rcbConclusionReasons.Items
                            If ("," & CCommon.ToString(dtTable.Rows(0)("lngPConclAnalysis")) & ",").Contains(item.Value) Then
                                item.Checked = True
                            End If
                        Next
                    End If

                    If Not String.IsNullOrEmpty(CCommon.ToString(dtTable.Rows(0)("bitTask"))) Then
                        For Each item As RadComboBoxItem In rcbActionItemType.Items
                            If ("," & CCommon.ToString(dtTable.Rows(0)("bitTask")) & ",").Contains(item.Value) Then
                                item.Checked = True
                            End If
                        Next
                    End If

                    If Not ddlOpportunityType.Items.FindByValue(CCommon.ToString(dtTable.Rows(0)("tintOppType"))) Is Nothing Then
                        ddlOpportunityType.Items.FindByValue(CCommon.ToString(dtTable.Rows(0)("tintOppType"))).Selected = True
                    End If

                End If
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Private Sub DisplayError(ByVal exception As String)
        Try
            DirectCast(Page.Master.Master.FindControl("lblError"), Label).Text = exception
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Style.Add("display", "")
            DirectCast(Page.Master.Master.FindControl("divError"), HtmlGenericControl).Focus()
        Catch ex As Exception

        End Try
    End Sub
#End Region

#Region "Event Handlers"
    Private Sub btnSaveClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveClose.Click
        Try
            If ddlTimeline.SelectedValue = "Custom" Then
                If rdpFromDate.SelectedDate Is Nothing Or rdpToDate.SelectedDate Is Nothing Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "DateValidation", "alert('From and To date are required.');", True)
                    Exit Sub
                ElseIf rdpToDate.SelectedDate < rdpFromDate.SelectedDate Then
                    ScriptManager.RegisterClientScriptBlock(Me, Me.Page.GetType(), "DateValidation", "alert('To date must be greater than the from date.');", True)
                    Exit Sub
                End If
            End If

            If objReportManage Is Nothing Then objReportManage = New CustomReportsManage
            objReportManage.DomainID = Session("DomainId")
            objReportManage.DashBoardID = CCommon.ToLong(GetQueryStringVal("DID"))
            objReportManage.ReportID = ddlCustomReport.SelectedValue.Split("~")(0)
            objReportManage.tintReportCategory = ddlCustomReport.SelectedValue.Split("~")(1)

            objReportManage.UserCntID = Session("UserContactId")
            objReportManage.tintReportType = IIf(radTable.Checked = True, 1, 2)
            objReportManage.HeaderText = txtHeader.Text
            objReportManage.FooterText = txtFooter.Text

            objReportManage.GroupBy = ddlGroupBy.SelectedValue
            If chkCustomTimeline.Checked = True Then
                objReportManage.bitCustomTimeline = True
                objReportManage.Timeline = FormattedDateFromDate(CtmFromDate.SelectedDate, Session("DateFormat")) + "-" + FormattedDateFromDate(CtmToDate.SelectedDate, Session("DateFormat"))
            Else
                objReportManage.Timeline = ddlTimeline.SelectedValue
            End If
            If chkDealWon.Checked = True Then
                objReportManage.DealWon = True
            End If
            If chkGrossRevenue.Checked = True Then
                objReportManage.GrossRevenue = True
            End If
            If chkRevenue.Checked = True Then
                objReportManage.RevenueGrossProfit = True
            End If
            If chkGrossProfit.Checked = True Then
                objReportManage.GrossProfit = True
            End If

            If chkpendingtobill.Checked = True Then
                objReportManage.Receivedbutnotbilled = True
            End If
            If chkpendingtoreceive.Checked = True Then
                objReportManage.Billedbutnotreceived = True
            End If
            If chkpendingtosale.Checked = True Then
                objReportManage.Soldbutnotshipped = True
            End If

            If (ddlTop.SelectedValue <> "") Then
                objReportManage.Top = ddlTop.SelectedValue
            End If

            If (ddlControlList.SelectedValue <> "") Then
                objReportManage.TintControlField = ddlControlList.SelectedValue
            End If

            If (ddlOpportunityType.SelectedValue <> "") Then
                objReportManage.TintOppType = ddlOpportunityType.SelectedValue
            End If

            If (ddlDueDate.SelectedValue <> "") Then
                objReportManage.DueDate = ddlDueDate.SelectedValue
            End If
            If radYeartoDate.Checked = True Then
                objReportManage.FilterBy = 1
            ElseIf radMonthtoDate.Checked = True Then
                objReportManage.FilterBy = 2
            ElseIf radWeektoDate.Checked = True Then
                objReportManage.FilterBy = 3
            End If
            If rcbFilterBy.CheckedItems.Count > 0 Then
                If rbAssignedTo.Checked Then
                    objReportManage.FilterBy = 1
                    Dim vcAssignedTo As String = ""
                    For Each item As RadComboBoxItem In rcbFilterBy.CheckedItems
                        vcAssignedTo += If(vcAssignedTo = "", item.Value, "," & item.Value)
                    Next
                    objReportManage.FilterValue = vcAssignedTo
                ElseIf rbRecordOwner.Checked Then
                    objReportManage.FilterBy = 2
                    Dim vcRecordOwner As String = ""
                    For Each item As RadComboBoxItem In rcbFilterBy.CheckedItems
                        vcRecordOwner += If(vcRecordOwner = "", item.Value, "," & item.Value)
                    Next
                    objReportManage.FilterValue = vcRecordOwner
                ElseIf rbTeams.Checked Then
                    objReportManage.FilterBy = 3
                    Dim vcTeams As String = ""
                    For Each item As RadComboBoxItem In rcbFilterBy.CheckedItems
                        vcTeams += If(vcTeams = "", item.Value, "," & item.Value)
                    Next
                    objReportManage.FilterValue = vcTeams
                ElseIf rbEmployees.Checked Then
                    objReportManage.FilterBy = 4
                    Dim vcEmployees As String = ""
                    For Each item As RadComboBoxItem In rcbFilterBy.CheckedItems
                        vcEmployees += If(vcEmployees = "", item.Value, "," & item.Value)
                    Next
                    objReportManage.FilterValue = vcEmployees
                End If
            Else
                If (ddlPeriodsToCompare.SelectedValue <> "") Then
                    objReportManage.FilterValue = ddlPeriodsToCompare.SelectedValue
                End If
            End If

            If rcbTerritory.CheckedItems.Count > 0 Then
                Dim vcTerritories As String = ""

                For Each item As RadComboBoxItem In rcbTerritory.CheckedItems
                    vcTerritories = If(vcTerritories = "", item.Value, "," & item.Value)
                Next

                objReportManage.Teritorry = vcTerritories
            End If

            If rcbDealAmountBetween.CheckedItems.Count > 0 Then
                Dim vcDealAmount As String = ""

                For Each item As RadComboBoxItem In rcbDealAmountBetween.CheckedItems
                    vcDealAmount += If(vcDealAmount = "", item.Value, "," & item.Value)
                Next

                objReportManage.DealAmount = vcDealAmount
            End If

            If rcbConclusionReasons.CheckedItems.Count > 0 Then
                Dim lngPConclAnalysis As String = ""

                For Each item As RadComboBoxItem In rcbConclusionReasons.CheckedItems
                    lngPConclAnalysis += If(lngPConclAnalysis = "", item.Value, "," & item.Value)
                Next

                objReportManage.lngPConclAnalysis = lngPConclAnalysis
            End If

            If rcbActionItemType.CheckedItems.Count > 0 Then
                Dim bitTask As String = ""

                For Each item As RadComboBoxItem In rcbActionItemType.CheckedItems
                    bitTask += If(bitTask = "", item.Value, "," & item.Value)
                Next

                objReportManage.bitTask = bitTask
            End If

            If (ddlTotalProgress.SelectedValue <> "") Then
                objReportManage.tintTotalProgress = ddlTotalProgress.SelectedValue
            End If

            If (txtMinAmountRange.Text <> "") Then
                objReportManage.tintMinNumber = CCommon.ToInteger(txtMinAmountRange.Text)
            End If

            If (txtMaxAmountRange.Text <> "") Then
                objReportManage.tintMaxNumber = CCommon.ToInteger(txtMaxAmountRange.Text)
            End If

            If (ddlQtyToDisplay.SelectedValue <> "") Then
                objReportManage.tintQtyToDisplay = ddlQtyToDisplay.SelectedValue
            End If

            'If rcbTeams.CheckedItems.Count > 0 Then
            '    Dim vcTeams As String = ""

            '    For Each item As RadComboBoxItem In rcbTeams.CheckedItems
            '        vcTeams += If(vcTeams = "", item.Value, "," & item.Value)
            '    Next

            '    objReportManage.vcTeams = vcTeams
            'End If

            'If rcbEmploees.CheckedItems.Count > 0 Then
            '    Dim vcEmploees As String = ""

            '    For Each item As RadComboBoxItem In rcbEmploees.CheckedItems
            '        vcEmploees += If(vcEmploees = "", item.Value, "," & item.Value)
            '    Next

            '    objReportManage.vcEmploees = vcEmploees
            'End If

            If ddlTimeline.SelectedValue = "Custom" Then
                objReportManage.FromDate = rdpFromDate.SelectedDate
                objReportManage.ToDate = rdpToDate.SelectedDate
            End If

            If ddlDueDate.SelectedValue = "Custom" Then
                objReportManage.FromDate = rpdDueFromDate.SelectedDate
                objReportManage.ToDate = rpdDueToDate.SelectedDate
            End If

            If objReportManage.tintReportType = 2 Then
                If (hfReportType.Value = 1 Or hfReportType.Value = 2) Then
                    objReportManage.tintChartType = ddlChartType.SelectedValue
                    objReportManage.XAxis = ddlXAxis.SelectedValue

                    Dim strYAxis() As String = ddlYAxis.SelectedValue.Split("_")
                    objReportManage.YAxis = strYAxis(0) & "_" & strYAxis(1) & "_" & strYAxis(2)
                    objReportManage.AggType = strYAxis(3)
                ElseIf hfReportType.Value = 3 Then 'For KPI : Line Chart
                    objReportManage.tintChartType = ddlChartType.SelectedValue
                    objReportManage.XAxis = ""
                    objReportManage.YAxis = ""
                    objReportManage.AggType = ""
                End If
            Else
                objReportManage.tintChartType = 0
                objReportManage.XAxis = ""
                objReportManage.YAxis = ""
                objReportManage.AggType = ""
            End If
            objReportManage.DashboardTemplateID = CCommon.ToLong(hdnDashboardTemplate.Value)
            objReportManage.ManagegeReportDashboard()
            If CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = 34 Or CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = 35 Or
                 CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = 29 Or CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = 36 Or
                CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = 11 Or CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = 39 Then
                Cache.Remove("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & CCommon.ToLong(GetQueryStringVal("DID")))
            End If

            Response.Redirect("../ReportDashboard/frmNewDashBoard.aspx")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Try
            Response.Redirect("../ReportDashboard/frmNewDashBoard.aspx")
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub ddlCustomReport_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCustomReport.SelectedIndexChanged
        Try
            If ddlCustomReport.SelectedValue <> "0~0~0" Then
                SetControlVisibility()
                GetReportDetail()
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub rbAssignedTo_CheckedChanged(sender As Object, e As EventArgs)
        Try
            If rbAssignedTo.Checked Then
                rcbFilterBy.Items.Clear()

                objCommon.sb_FillConEmpFromDBSel(rcbFilterBy, CCommon.ToLong(Session("DomainID")), False, 0)
                rcbFilterBy.Items.Remove(rcbFilterBy.Items(0))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub rbRecordOwner_CheckedChanged(sender As Object, e As EventArgs)
        Try
            If rbRecordOwner.Checked Then
                rcbFilterBy.Items.Clear()

                objCommon.sb_FillConEmpFromDBSel(rcbFilterBy, CCommon.ToLong(Session("DomainID")), False, 0)
                rcbFilterBy.Items.Remove(rcbFilterBy.Items(0))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub rbTeams_CheckedChanged(sender As Object, e As EventArgs)
        Try
            If rbTeams.Checked Then
                rcbFilterBy.Items.Clear()
                objCommon.sb_FillComboFromDBwithSel(rcbFilterBy, 35, Session("DomainID"))
                rcbFilterBy.Items.Remove(rcbFilterBy.Items(0))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Protected Sub rbEmployees_CheckedChanged(sender As Object, e As EventArgs)
        Try
            If rbEmployees.Checked Then
                rcbFilterBy.Items.Clear()
                objCommon.sb_FillConEmpFromDBSel(rcbFilterBy, Session("DomainID"), 0, 0)
                rcbFilterBy.Items.Remove(rcbFilterBy.Items(0))
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Protected Sub ddlTimeline_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            If ddlTimeline.SelectedValue = "Custom" Then
                divFromDate.Visible = True
                divToDate.Visible = True
                chkCustomTimeline.Checked = False
            Else
                divFromDate.Visible = False
                divToDate.Visible = False
                chkCustomTimeline.Checked = False
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub

    Private Sub SetControlVisibility()
        Try
            If CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = "34" Or CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = "35" Then
                ClearItemsData()

                Dim listItem As New ListItem("Current CY", "CurYear")
                listItem.Attributes("OptionGroup") = "Calendar Year"
                listItem.Selected = True
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Previous CY", "PreYear")
                listItem.Attributes("OptionGroup") = "Calendar Year"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Previous 2 CY", "Pre2Year")
                listItem.Attributes("OptionGroup") = "Calendar Year"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("2 CY Ago", "Ago2Year")
                listItem.Attributes("OptionGroup") = "Calendar Year"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Current and Previous CY", "CurPreYear")
                listItem.Attributes("OptionGroup") = "Calendar Year"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Current and Previous 2 CY", "CurPre2Year")
                listItem.Attributes("OptionGroup") = "Calendar Year"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Current CQ", "CuQur")
                listItem.Attributes("OptionGroup") = "Calendar Quarter"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Previous CQ", "PreQur")
                listItem.Attributes("OptionGroup") = "Calendar Quarter"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Current and Previous CQ", "CurPreQur")
                listItem.Attributes("OptionGroup") = "Calendar Quarter"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("This Month", "ThisMonth")
                listItem.Attributes("OptionGroup") = "Calendar Month"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last Month", "LastMonth")
                listItem.Attributes("OptionGroup") = "Calendar Month"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Current and Previous Month", "CurPreMonth")
                listItem.Attributes("OptionGroup") = "Calendar Month"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last Week", "LastWeek")
                listItem.Attributes("OptionGroup") = "Calendar Week"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("This Week", "ThisWeek")
                listItem.Attributes("OptionGroup") = "Calendar Week"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Yesterday", "Yesterday")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Today", "Today")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 7 Days", "Last7Day")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 30 Days", "Last30Day")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 60 Days", "Last60Day")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 90 Days", "Last90Day")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 120 Days", "Last120Day")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)


                ddlTimeline.Items.Add(New ListItem("Custom", "Custom"))

                objCommon.sb_FillComboFromDBwithSel(rcbTerritory, 78, Session("DomainID"))
                rcbTerritory.Items.Remove(rcbTerritory.Items(0))

                divTimeLine.Visible = True
                divGroupBy.Visible = True
                divTerritory.Visible = True
                divFilterBy.Visible = True
                divTop.Visible = True
                divControlList.Visible = False


            ElseIf CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = "13" Then

                ClearItemsData()

                Dim listItem As New ListItem("Last 12 months", "Last12Months")
                listItem.Attributes("OptionGroup") = "Month"
                listItem.Selected = True
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 6 months", "Last6Months")
                listItem.Attributes("OptionGroup") = "Month"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 3 months", "Last3Months")
                listItem.Attributes("OptionGroup") = "Month"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 30 days", "Last30Days")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 7 days", "Last7Days")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                divTimeLine.Visible = True
                divGroupBy.Visible = False
                divTerritory.Visible = False
                divFilterBy.Visible = False
                divTop.Visible = False
                divControlList.Visible = False
            ElseIf CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = "23" Then
                ClearItemsData()

                lblTop.Text = "Top"

                Dim listItem As New ListItem("10", "10")
                listItem.Selected = True
                ddlTop.Items.Add(listItem)

                listItem = New ListItem("25", "25")
                ddlTop.Items.Add(listItem)

                listItem = New ListItem("50", "50")
                ddlTop.Items.Add(listItem)

                divTop.Visible = True
                divTimeLine.Visible = False
                divGroupBy.Visible = False
                divTerritory.Visible = False
                divFilterBy.Visible = False
                divControlList.Visible = False

            ElseIf CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = "28" Then
                ClearItemsData()

                lblTop.Text = "Top Reasons Given"

                Dim listItem As New ListItem("10", "10")
                listItem.Selected = True
                ddlTop.Items.Add(listItem)

                listItem = New ListItem("25", "25")
                ddlTop.Items.Add(listItem)

                listItem = New ListItem("50", "50")
                ddlTop.Items.Add(listItem)

                divTop.Visible = True
                divTimeLine.Visible = False
                divGroupBy.Visible = False
                divTerritory.Visible = False
                divFilterBy.Visible = False

            ElseIf CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = "18" Then
                ClearItemsData()

                lblTop.Text = "Top"

                Dim listItem As New ListItem("10", "10")
                listItem.Selected = True
                ddlTop.Items.Add(listItem)

                listItem = New ListItem("25", "25")
                ddlTop.Items.Add(listItem)

                listItem = New ListItem("50", "50")
                ddlTop.Items.Add(listItem)

                divTop.Visible = True
                divTimeLine.Visible = False
                divGroupBy.Visible = False
                divTerritory.Visible = False
                divFilterBy.Visible = False
                divControlList.Visible = False

            ElseIf CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = "5" Then

                ClearItemsData()

                Dim listItem As New ListItem("Last 12 months", "Last12Months")
                listItem.Attributes("OptionGroup") = "Month"
                listItem.Selected = True
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 6 months", "Last6Months")
                listItem.Attributes("OptionGroup") = "Month"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 3 months", "Last3Months")
                listItem.Attributes("OptionGroup") = "Month"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 30 days", "Last30Days")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 7 days", "Last7Days")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                lblTop.Text = "Top"

                Dim listItemTop As New ListItem("10", "10")
                listItemTop.Selected = True
                ddlTop.Items.Add(listItemTop)

                listItemTop = New ListItem("25", "25")
                ddlTop.Items.Add(listItemTop)

                listItemTop = New ListItem("50", "50")
                ddlTop.Items.Add(listItemTop)

                Dim listItemControlList As New ListItem("By Profit Amount", "1")
                listItemControlList.Selected = True
                ddlControlList.Items.Add(listItemControlList)

                listItemControlList = New ListItem("By Profit Margin", "2")
                ddlControlList.Items.Add(listItemControlList)

                listItemControlList = New ListItem("By Revenue Sold", "3")
                ddlControlList.Items.Add(listItemControlList)

                divTop.Visible = True
                divTimeLine.Visible = True
                divControlList.Visible = True
                divGroupBy.Visible = False
                divTerritory.Visible = False
                divFilterBy.Visible = False

            ElseIf CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = "14" Then

                ClearItemsData()
                divDealAmountBetween.Visible = True
                divTop.Visible = False
                divTimeLine.Visible = False
                divControlList.Visible = False
                divGroupBy.Visible = False
                divTerritory.Visible = False
                divFilterBy.Visible = False

            ElseIf CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = "16" Then

                ClearItemsData()

                Dim listItem As New ListItem("Last 12 months", "Last12Months")
                listItem.Attributes("OptionGroup") = "Month"
                listItem.Selected = True
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 6 months", "Last6Months")
                listItem.Attributes("OptionGroup") = "Month"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 3 months", "Last3Months")
                listItem.Attributes("OptionGroup") = "Month"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 30 days", "Last30Days")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 7 days", "Last7Days")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                lblTop.Text = "Top"

                Dim listItemTop As New ListItem("10", "10")
                listItemTop.Selected = True
                ddlTop.Items.Add(listItemTop)

                listItemTop = New ListItem("25", "25")
                ddlTop.Items.Add(listItemTop)

                listItemTop = New ListItem("50", "50")
                ddlTop.Items.Add(listItemTop)

                divDealAmountBetween.Visible = True
                divTop.Visible = True
                divTimeLine.Visible = True
                divControlList.Visible = False
                divGroupBy.Visible = False
                divTerritory.Visible = False
                divFilterBy.Visible = False

            ElseIf CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = "31" Or
                CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = "32" Then

                ClearItemsData()

                Dim listItem As New ListItem("Last 12 months", "Last12Months")
                listItem.Attributes("OptionGroup") = "Month"
                listItem.Selected = True
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 6 months", "Last6Months")
                listItem.Attributes("OptionGroup") = "Month"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 3 months", "Last3Months")
                listItem.Attributes("OptionGroup") = "Month"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 30 days", "Last30Days")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 7 days", "Last7Days")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Today", "Today")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Yesterday", "Yesterday")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                divTimeLine.Visible = True
                divFilterBy.Visible = True
                rbEmployees.Visible = False
                divDealAmountBetween.Visible = False
                divTop.Visible = False
                divControlList.Visible = False
                divGroupBy.Visible = False
                divTerritory.Visible = False

                'divCustomTimeline.Visible = True


            ElseIf CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = "29" Then


                ClearItemsData()

                Dim listItem As New ListItem("Last 12 months", "Last12Months")
                listItem.Attributes("OptionGroup") = "Month"
                listItem.Selected = True
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 6 months", "Last6Months")
                listItem.Attributes("OptionGroup") = "Month"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 3 months", "Last3Months")
                listItem.Attributes("OptionGroup") = "Month"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 30 days", "Last30Days")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 7 days", "Last7Days")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Today", "Today")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Yesterday", "Yesterday")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                divTimeLine.Visible = True
                divFilterBy.Visible = True
                rbEmployees.Visible = False
                divDealAmountBetween.Visible = False
                divTop.Visible = False
                divControlList.Visible = False
                divGroupBy.Visible = False
                divTerritory.Visible = False
                divRunDisplay.Visible = True
                divCustomTimeline.Visible = True

            ElseIf CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = "36" Then


                ClearItemsData()

                Dim listItem As New ListItem("Last 12 months", "Last12Months")
                listItem.Attributes("OptionGroup") = "Month"
                listItem.Selected = True
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 6 months", "Last6Months")
                listItem.Attributes("OptionGroup") = "Month"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 3 months", "Last3Months")
                listItem.Attributes("OptionGroup") = "Month"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 30 days", "Last30Days")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 7 days", "Last7Days")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Today", "Today")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Yesterday", "Yesterday")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                'divTimeLine.Visible = True
                divFilterBy.Visible = True
                rbEmployees.Visible = False
                divDealAmountBetween.Visible = False
                divTop.Visible = False
                divControlList.Visible = False
                divGroupBy.Visible = False
                divTerritory.Visible = False
                'divRunDisplay.Visible = True 
                'divCustomTimeline.Visible = True

            ElseIf CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = "11" Then


                ClearItemsData()

                divPeriodtoMeasure.Visible = True
                divFilterBy.Visible = False
                rbEmployees.Visible = False
                divDealAmountBetween.Visible = False
                divTop.Visible = False
                divControlList.Visible = False
                divGroupBy.Visible = False
                divTerritory.Visible = False

            ElseIf CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = "39" Then


                ClearItemsData()

                divPendingReport.Visible = True
                divPeriodtoMeasure.Visible = False
                divFilterBy.Visible = False
                rbEmployees.Visible = False
                divDealAmountBetween.Visible = False
                divTop.Visible = False
                divControlList.Visible = False
                divGroupBy.Visible = False
                divTerritory.Visible = False




            ElseIf CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = "19" Then

                ClearItemsData()

                Dim listItem As New ListItem("Last 12 months", "Last12Months")
                listItem.Attributes("OptionGroup") = "Month"
                listItem.Selected = True
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 6 months", "Last6Months")
                listItem.Attributes("OptionGroup") = "Month"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 3 months", "Last3Months")
                listItem.Attributes("OptionGroup") = "Month"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 30 days", "Last30Days")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 7 days", "Last7Days")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)


                ''ddlTotalProgress.Items.Insert(0, New ListItem("-- Select One --", "0"))
                ddlTotalProgress.Items.Add(New ListItem("50%", "50"))
                ddlTotalProgress.Items.Add(New ListItem("60%", "60"))
                ddlTotalProgress.Items.Add(New ListItem("70%", "70"))
                ddlTotalProgress.Items.Add(New ListItem("80%", "80"))
                ddlTotalProgress.Items.Add(New ListItem("90%", "90"))

                ''ddlQtyToDisplay.Items.Insert(0, New ListItem("-- Select --", "0"))
                ddlQtyToDisplay.Items.Add(New ListItem("First 5", "5"))
                ddlQtyToDisplay.Items.Add(New ListItem("First 10", "10"))
                ddlQtyToDisplay.Items.Add(New ListItem("First 20", "20"))
                ddlQtyToDisplay.Items.Add(New ListItem("First 30", "30"))
                ddlQtyToDisplay.Items.Add(New ListItem("First 50", "50"))
                ddlQtyToDisplay.Items.Add(New ListItem("First 100", "100"))


                Dim listItemddlDueDate As New ListItem("Current CY", "CurYear")
                listItemddlDueDate.Attributes("OptionGroup") = "Calendar Year"
                listItemddlDueDate.Selected = True
                ddlDueDate.Items.Add(listItemddlDueDate)

                listItemddlDueDate = New ListItem("Previous CY", "PreYear")
                listItemddlDueDate.Attributes("OptionGroup") = "Calendar Year"
                ddlDueDate.Items.Add(listItemddlDueDate)

                listItemddlDueDate = New ListItem("Previous 2 CY", "Pre2Year")
                listItemddlDueDate.Attributes("OptionGroup") = "Calendar Year"
                ddlDueDate.Items.Add(listItemddlDueDate)

                listItemddlDueDate = New ListItem("2 CY Ago", "Ago2Year")
                listItemddlDueDate.Attributes("OptionGroup") = "Calendar Year"
                ddlDueDate.Items.Add(listItemddlDueDate)

                listItemddlDueDate = New ListItem("Current and Previous CY", "CurPreYear")
                listItemddlDueDate.Attributes("OptionGroup") = "Calendar Year"
                ddlDueDate.Items.Add(listItemddlDueDate)

                listItemddlDueDate = New ListItem("Current and Previous 2 CY", "CurPre2Year")
                listItemddlDueDate.Attributes("OptionGroup") = "Calendar Year"
                ddlDueDate.Items.Add(listItemddlDueDate)

                listItemddlDueDate = New ListItem("Current CQ", "CuQur")
                listItemddlDueDate.Attributes("OptionGroup") = "Calendar Quarter"
                ddlDueDate.Items.Add(listItemddlDueDate)

                listItemddlDueDate = New ListItem("Previous CQ", "PreQur")
                listItemddlDueDate.Attributes("OptionGroup") = "Calendar Quarter"
                ddlDueDate.Items.Add(listItemddlDueDate)

                listItemddlDueDate = New ListItem("Current and Previous CQ", "CurPreQur")
                listItemddlDueDate.Attributes("OptionGroup") = "Calendar Quarter"
                ddlDueDate.Items.Add(listItemddlDueDate)

                listItemddlDueDate = New ListItem("This Month", "ThisMonth")
                listItemddlDueDate.Attributes("OptionGroup") = "Calendar Month"
                ddlDueDate.Items.Add(listItemddlDueDate)

                listItemddlDueDate = New ListItem("Last Month", "LastMonth")
                listItemddlDueDate.Attributes("OptionGroup") = "Calendar Month"
                ddlDueDate.Items.Add(listItemddlDueDate)

                listItemddlDueDate = New ListItem("Current and Previous Month", "CurPreMonth")
                listItemddlDueDate.Attributes("OptionGroup") = "Calendar Month"
                ddlDueDate.Items.Add(listItemddlDueDate)

                listItemddlDueDate = New ListItem("Last Week", "LastWeek")
                listItemddlDueDate.Attributes("OptionGroup") = "Calendar Week"
                ddlDueDate.Items.Add(listItemddlDueDate)

                listItemddlDueDate = New ListItem("This Week", "ThisWeek")
                listItemddlDueDate.Attributes("OptionGroup") = "Calendar Week"
                ddlDueDate.Items.Add(listItemddlDueDate)

                listItemddlDueDate = New ListItem("Yesterday", "Yesterday")
                listItemddlDueDate.Attributes("OptionGroup") = "Day"
                ddlDueDate.Items.Add(listItemddlDueDate)

                listItemddlDueDate = New ListItem("Today", "Today")
                listItemddlDueDate.Attributes("OptionGroup") = "Day"
                ddlDueDate.Items.Add(listItemddlDueDate)

                listItemddlDueDate = New ListItem("Last 7 Days", "Last7Day")
                listItemddlDueDate.Attributes("OptionGroup") = "Day"
                ddlDueDate.Items.Add(listItemddlDueDate)

                listItemddlDueDate = New ListItem("Last 30 Days", "Last30Day")
                listItemddlDueDate.Attributes("OptionGroup") = "Day"
                ddlDueDate.Items.Add(listItemddlDueDate)

                listItemddlDueDate = New ListItem("Last 60 Days", "Last60Day")
                listItemddlDueDate.Attributes("OptionGroup") = "Day"
                ddlDueDate.Items.Add(listItemddlDueDate)

                listItemddlDueDate = New ListItem("Last 90 Days", "Last90Day")
                listItemddlDueDate.Attributes("OptionGroup") = "Day"
                ddlDueDate.Items.Add(listItemddlDueDate)

                listItemddlDueDate = New ListItem("Last 120 Days", "Last120Day")
                listItemddlDueDate.Attributes("OptionGroup") = "Day"
                ddlDueDate.Items.Add(listItemddlDueDate)

                ddlDueDate.Items.Add(New ListItem("Custom", "Custom"))

                divOpportunityType.Visible = True
                divTimeLine.Visible = True
                divTotalProgress.Visible = True
                divFilterBy.Visible = True
                divQtyToDisplay.Visible = True
                divDealAmountBetween.Visible = True
                divAmountRange.Visible = True
                divDueDate.Visible = True
                divTerritory.Visible = True
                divFilterBy.Visible = True

                divTop.Visible = False
                divControlList.Visible = False
                divGroupBy.Visible = False

                rbAssignedTo.Visible = True
                rbRecordOwner.Visible = True
                rbTeams.Visible = True
                rbEmployees.Visible = False


            ElseIf CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = "12" Then

                ClearItemsData()

                divPeriodsToCompare.Visible = True
                divTimeLine.Visible = False
                divDealAmountBetween.Visible = False
                divTop.Visible = False
                divControlList.Visible = False
                divGroupBy.Visible = False
                divTerritory.Visible = False
                divFilterBy.Visible = False

            ElseIf CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = "8" Or CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = "9" Then

                ClearItemsData()

                Dim listItemControlList As New ListItem("By Profit Amount", "1")
                listItemControlList.Selected = True
                ddlControlList.Items.Add(listItemControlList)

                listItemControlList = New ListItem("By Profit Margin", "2")
                ddlControlList.Items.Add(listItemControlList)

                'listItemControlList = New ListItem("By Revenue Sold", "3")
                'ddlControlList.Items.Add(listItemControlList)

                lblTop.Text = "Top"

                Dim listItemTop As New ListItem("10", "10")
                listItemTop.Selected = True
                ddlTop.Items.Add(listItemTop)

                listItemTop = New ListItem("25", "25")
                ddlTop.Items.Add(listItemTop)

                listItemTop = New ListItem("50", "50")
                ddlTop.Items.Add(listItemTop)

                Dim listItem As New ListItem("Last 12 months", "Last12Months")
                listItem.Attributes("OptionGroup") = "Month"
                listItem.Selected = True
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 6 months", "Last6Months")
                listItem.Attributes("OptionGroup") = "Month"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 3 months", "Last3Months")
                listItem.Attributes("OptionGroup") = "Month"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 30 days", "Last30Days")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                listItem = New ListItem("Last 7 days", "Last7Days")
                listItem.Attributes("OptionGroup") = "Day"
                ddlTimeline.Items.Add(listItem)

                divControlList.Visible = True
                divTop.Visible = True
                divTimeLine.Visible = True
                divPeriodsToCompare.Visible = False
                divDealAmountBetween.Visible = False
                divGroupBy.Visible = False
                divTerritory.Visible = False
                divFilterBy.Visible = False

            ElseIf CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = "26" Then
                ClearItemsData()

                divOpportunityType.Visible = True
                divDealAmountBetween.Visible = True
                divTop.Visible = False
                divTimeLine.Visible = False
                divGroupBy.Visible = False
                divTerritory.Visible = False
                divFilterBy.Visible = False
                divControlList.Visible = False

            ElseIf CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = "27" Then
                ClearItemsData()

                objCommon.sb_FillComboFromDBwithSel(rcbConclusionReasons, 12, Session("DomainID"))
                rcbConclusionReasons.Items.Remove(rcbConclusionReasons.Items(0))

                divConclusionReasons.Visible = True
                divOpportunityType.Visible = True
                divDealAmountBetween.Visible = True
                divTop.Visible = False
                divTimeLine.Visible = False
                divGroupBy.Visible = False
                divTerritory.Visible = False
                divFilterBy.Visible = False
                divControlList.Visible = False

            ElseIf CCommon.ToInteger(ddlCustomReport.SelectedValue.Split("~")(2)) = "10" Then
                ClearItemsData()

                objCommon.sb_FillComboFromDBwithSel(rcbActionItemType, 73, Session("DomainID"))
                rcbActionItemType.Items.Remove(rcbActionItemType.Items(0))

                divTerritory.Visible = True
                divFilterBy.Visible = True
                divActionItemType.Visible = True
                divConclusionReasons.Visible = False
                divOpportunityType.Visible = False
                divDealAmountBetween.Visible = False
                divTop.Visible = False
                divTimeLine.Visible = False
                divGroupBy.Visible = False
                divControlList.Visible = False
            Else
                ClearItemsData()
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ClearItemsData()
        ddlTimeline.Items.Clear()
        rcbTerritory.Items.Clear()
        rcbFilterBy.Items.Clear()
        ddlTop.Items.Clear()
        ddlControlList.Items.Clear()
        ddlQtyToDisplay.Items.Clear()
        ddlTotalProgress.Items.Clear()
        ddlDueDate.Items.Clear()

        divConclusionReasons.Visible = False
        divTimeLine.Visible = False
        divGroupBy.Visible = False
        divTerritory.Visible = False
        divFilterBy.Visible = False
        divFromDate.Visible = False
        divToDate.Visible = False
        divTop.Visible = False
        divControlList.Visible = False
        divOpportunityType.Visible = False
        divPeriodsToCompare.Visible = False
        divDealAmountBetween.Visible = False
        divQtyToDisplay.Visible = False
        divDueDate.Visible = False
        divAmountRange.Visible = False
        divTotalProgress.Visible = False
        divActionItemType.Visible = False
    End Sub

    Protected Sub ddlDueDate_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            If ddlDueDate.SelectedValue = "Custom" Then
                divDueFromDate.Visible = True
                divDueToDate.Visible = True
            Else
                divDueFromDate.Visible = False
                divDueToDate.Visible = False
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            DisplayError(CCommon.ToString(ex))
        End Try
    End Sub
    Protected Sub ddlCustomTimeline_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try
            If chkCustomTimeline.Checked Then
                ddlTimeline.Enabled = False

            Else
                ddlTimeline.Enabled = True
            End If

        Catch ex As Exception

        End Try
    End Sub
#End Region

End Class