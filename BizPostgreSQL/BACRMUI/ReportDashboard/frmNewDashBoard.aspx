﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmNewDashBoard.aspx.vb" Inherits=".frmNewDashBoard1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Dashboard</title>
    <link href="../reports/jqTree/jquery.gridster.css" rel="stylesheet" />
    <script src="../reports/jqTree/jquery.gridster.js"></script>
    <script src="../reports/jqTree/highcharts.js"></script>
    <script src="../reports/jqTree/exporting.js"></script>
    <script src="<%# ResolveUrl("~/JavaScript/jquery-ui.min.js") %>" type="text/javascript"></script>
    <link rel="stylesheet" href="<%# ResolveUrl("~/CSS/jquery-ui.css") %>" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.0/Chart.min.js" type="text/javascript"></script>
    <style type="text/css">
        /*http://jsfiddle.net/maxgalbu/UfyjW/*/

        .additional_element {
            margin: 5px;
            padding: 5px;
            color: white;
            cursor: pointer;
        }

        .grid_elements {
            width: 300px;
            float: left;
        }

            .grid_elements table {
                border-collapse: separate;
                width: 200px;
                height: 200px;
            }

        .actions_block {
            padding: 9px;
            float: right;
        }

        .grid_elements table td {
            background-color: #CFCFCF;
            border-radius: 1px;
            -webkit-border-radius: 1px;
            -moz-border-radius: 1px;
        }

            .grid_elements table td.hover {
                background-color: #999;
            }

            .grid_elements table td:hover {
                cursor: pointer;
            }

        .layout_block .block_name {
            cursor: pointer;
        }

        .ui-resizable-resizing {
            z-index: 9999999 !important;
        }

        .layout_name_edit {
            background-color: inherit;
            background: url( '../media/images/icons/edit_input.gif' );
            background-repeat: no-repeat;
            background-position: 0 center;
            padding: 0px;
            margin: 0px;
            padding-left: 14px;
            border: none;
            font-size: 16px;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            color: white;
            outline: none;
            width: 90%;
            font-style: italic;
            cursor: text !important;
        }

        .layouts_grid .remove_element {
            float: right;
            color: black;
            font-size: 12px;
            font-weight: bold;
            padding: 2px 5px;
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            cursor: pointer;
            margin: 3px 0px 4px 4px;
        }

        .layouts_grid .layout_block .info1 {
            color: white;
            font-size: 16px;
            font-family: Helvetica;
            padding: 10px;
        }

        .layout_block {
            /*overflow: hidden;*/
        }



        /*! gridster.js - v0.1.0 - 2012-10-07
* http://gridster.net/
* Copyright (c) 2012 ducksboard; Licensed MIT */

        .layouts_grid {
            position: relative !important;
            /*width: 700px;*/
            min-height: 300px;
            float: left;
        }

            .layouts_grid > * {
                margin: 0 auto;
                -webkit-transition: height .4s;
                -moz-transition: height .4s;
                -o-transition: height .4s;
                -ms-transition: height .4s;
                transition: height .4s;
            }

            .layouts_grid > ul {
                list-style-type: none;
                min-height: 660px;
            }

            .layouts_grid .gs_w {
                z-index: 2;
                position: absolute;
            }

        .ready .gs_w:not(.preview-holder) {
            -webkit-transition: opacity .3s, left .3s, top .3s;
            -moz-transition: opacity .3s, left .3s, top .3s;
            -o-transition: opacity .3s, left .3s, top .3s;
            transition: opacity .3s, left .3s, top .3s;
        }

        .ready .gs_w:not(.preview-holder) {
            -webkit-transition: opacity .3s, left .3s, top .3s, width .3s, height .3s;
            -moz-transition: opacity .3s, left .3s, top .3s, width .3s, height .3s;
            -o-transition: opacity .3s, left .3s, top .3s, width .3s, height .3s;
            transition: opacity .3s, left .3s, top .3s, width .3s, height .3s;
        }

        .layouts_grid .preview-holder {
            z-index: 1;
            position: absolute;
            background-color: inherit;
            border: none;
            opacity: 0.3;
            background-color: #DDDDDD;
            border-color: #DDDDDD;
        }

        .layouts_grid .player-revert {
            z-index: 10!important;
            -webkit-transition: left .3s, top .3s!important;
            -moz-transition: left .3s, top .3s!important;
            -o-transition: left .3s, top .3s!important;
            transition: left .3s, top .3s!important;
        }

        .layouts_grid .dragging {
            z-index: 10!important;
            -webkit-transition: all 0s !important;
            -moz-transition: all 0s !important;
            -o-transition: all 0s !important;
            transition: all 0s !important;
        }

        /* Uncomment this if you set helper : "clone" in draggable options */
        /*.layouts_grid .player {
  opacity:0;
}*/

        .layouts_grid > ul {
            position: static !important;
        }

        .layouts_grid ul {
            margin: 5px;
            padding: 0;
        }

        .layouts_grid > ul > li {
            position: static;
            display: block;
            z-index: 10;
            padding: 5px;
            cursor: pointer;
            -webkit-box-shadow: 0 0 5px rgba(0, 0, 0, 0.3);
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.3);
        }

        .handle {
            cursor: move;
        }

        .rptcontent {
            overflow: auto;
            /*margin-bottom: 0.3em;
            margin-left: 0.3em;
            margin-right: 0.3em;
            margin-top: 0.3em;
            padding-bottom: 4px;
            padding-left: 0.2em;*/
        }

        .preview-holder {
            background: #2D2D2D;
            border: none;
            z-index: 5;
        }

        .success {
            border: 1px solid;
            margin: 10px 0px;
            padding: 15px 10px 15px 50px;
            background-repeat: no-repeat;
            background-position: 10px center;
            color: #4F8A10;
            background-color: #DFF2BF;
            background-image: url('../images/success.png');
        }

        .layouts_grid .box-title {
            margin-right: 46px;
        }

        #divLayout .box {
            box-shadow: none !important;
        }

        #divLayout .table {
            margin-bottom:0px;
        }

        #divLayout .box-footer {
            border-top-right-radius: 0px;
            border-top-left-radius: 0px;
        }

        #divLayout .box-body{
            border-bottom-right-radius: 0px;
            border-bottom-left-radius: 0px;
        }
    </style>

    <script type="text/javascript">
        window.onloadFuncs = [];

        window.onload = function () {
            for (var i in this.onloadFuncs) {
                this.onloadFuncs[i]();
            }
        }

        'use strict';

        window.chartColors = {
            red: 'rgb(255, 99, 132)',
            orange: 'rgb(255, 159, 64)',
            yellow: 'rgb(255, 205, 86)',
            green: 'rgb(75, 192, 192)',
            blue: 'rgb(54, 162, 235)',
            purple: 'rgb(153, 102, 255)',
            grey: 'rgb(201, 203, 207)'
        };

        function transparentize(color, opacity) {
            var alpha = opacity === undefined ? 0.2 : 1 - opacity;
            return Color(color).alpha(alpha).rgbString();
        }

        var default_colors = ['#f56954','#00a65a','#3c8dbc','#b6d957','#fac364','#d998cb','#f2d249','#ccc5a8','#52bacc','#98aafb','#3366CC', '#DC3912', '#FF9900', '#109618', '#990099', '#3B3EAC', '#0099C6', '#DD4477', '#66AA00', '#B82E2E', '#316395', '#994499', '#8B0707', '#329262', '#5574A6', '#3B3EAC', '#22AA99', '#AAAA11', '#6633CC', '#E67300']
        Chart.defaults.global.animation.duration = 2500;

        // Define a plugin to provide data labels
        Chart.plugins.register({
            afterDatasetsDraw: function (chart, easing) {
                // To only draw at the end of animation, check for easing === 1
                var ctx = chart.ctx;

                if (chart.config.type == "pie") {
                    chart.data.datasets.forEach(function (dataset, i) {
                        var meta = chart.getDatasetMeta(i);
                        if (!meta.hidden) {
                            meta.data.forEach(function (element, index) {
                                // Draw the text in black, with the specified font
                                ctx.fillStyle = 'rgb(0, 0, 0)';

                                var fontSize = 16;
                                var fontStyle = 'normal';
                                var fontFamily = 'Helvetica Neue';
                                ctx.font = Chart.helpers.fontString(fontSize, fontStyle, fontFamily);

                                // Just naively convert to string for now
                                var dataString = dataset.data[index].toString();
                                dataString = Number(dataset.data[index]).toFixed(2) + '%';
                                // Make sure alignment settings are correct
                                ctx.textAlign = 'center';
                                ctx.textBaseline = 'middle';

                                var padding = 5;
                                var position = element.tooltipPosition();
                                ctx.fillText(dataString, position.x, position.y - (fontSize / 2) - padding);
                            });
                        }
                    });
                }
            }
        });


        var layout;
        var grid_size = 50;
        var grid_margin = 5;
        var block_params = {
            max_width: 6,
            max_height: 6
        };
        $(function () {

            $('.layouts_grid').width($("#divLayout").outerWidth() + "px");

            layout = $('.layouts_grid > ul').gridster({
                widget_margins: [grid_margin, grid_margin],
                widget_base_dimensions: [grid_size, grid_size],
                serialize_params: function ($w, wgd) {
                    return {
                        col: wgd.col,
                        row: wgd.row,
                        width: wgd.size_x,
                        height: wgd.size_y,
                        id: $($w).attr('data-id'),
                        name: $($w).find('.block_name').html(),
                    };
                },
                min_rows: block_params.max_height,
                max_size_x: 30,
                autogrow_cols: true,
                draggable: {
                    handle: ".handle",
                    stop: function (event, ui) {
                        //changeContentHeight();
                        UpdatePosition();
                    },
                    update: function (event, ui) {

                    }
                }
            }).data('gridster');

            $('.layout_block').resizable({
                containment: '#layouts_grid ul',
                autoHide: true,
                stop: function (event, ui) {
                    var resized = $(this);
                    setTimeout(function () {
                        resizeBlock(resized);
                    }, 300);


                }
            });

            $('.ui-resizable-handle').hover(function () {
                layout.disable();
            }, function () {

                layout.enable();
            });

            //$('.portlet').click(function (e) {
            //    e.stopPropagation();
            //});

            function resizeBlock(elmObj) {

                var elmObj = $(elmObj);
                var w = elmObj.width() - grid_size;
                var h = elmObj.height() - grid_size;

                for (var grid_w = 1; w > 0; w -= (grid_size + (grid_margin * 2))) {

                    grid_w++;
                }

                for (var grid_h = 1; h > 0; h -= (grid_size + (grid_margin * 2))) {

                    grid_h++;
                }

                layout.resize_widget(elmObj, grid_w, grid_h);

                changeContentHeight(true);
                UpdatePosition();
            }

            //changeContentHeight(false);


            function UpdatePosition() {
                var positions = JSON.stringify(layout.serialize());

                //console.log(positions);

                $.ajax({
                    type: "POST",
                    url: "frmNewDashBoard.aspx/UpdateOrder",
                    data: "{positions:'" + positions + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (msg) {
                        if (msg.d == 'True') {
                            $('#dvSucess span').text('Widget order saved sucessfully');
                            FadeOut(2000);
                        }
                    }
                })
            }
        });

        function changeContentHeight(reSize) {
            $('.layouts_grid > ul > li').each(function () {
                var height = $(this).outerHeight() - $(this).find('.handle').outerHeight() - $(this).find('.footer').outerHeight() - 40;

                //console.log($(this).find('.content')[0].scrollHeight + "-->" + $(this).find('.content').outerHeight() + "-->" + height);
                //console.log($(this).find('.pnlChart')[0].scrollHeight + "-->" + $(this).find('.pnlChart').outerHeight() + "-->" + height);

                if (reSize) {
                    var chartID = 'chart' + $(this).find('.pnlChart').parent().attr('id');

                    if ($('#' + chartID + '_container').length != 0) {
                        obj = eval('{' + chartID + '}');
                        height = height - 10;
                        width = $(this).find('.rptcontent').width() - 10;

                        obj.setSize(width, height, true);
                        console.log('sdfdsf');
                    }
                }

                if ($(this).find('.box') != null && $(this).find('.box').length > 0) {
                    $(this).find('.box').width($(this).outerWidth() - 10);
                    $(this).find('.box').height($(this).outerHeight() - 20);

                    if ($(this).find('.rptcontent') != null && $(this).find('.rptcontent').length > 0) {
                        //$(this).find('.rptcontent').width($(this).outerWidth() - 30);
                        $(this).find('table.rptcontent').width($(this).outerWidth() - 30);
                        $(this).find('.rptcontent').height($(this).find('.box').outerHeight() - 20 - ($(this).find('.box-header').length > 0 ? $(this).find('.box-header').outerHeight() : 0) - ($(this).find('.box-footer').length > 0 ? $(this).find('.box-footer').outerHeight() : 0));
                    }
                }
                else if ($(this).find('.rptcontent') != null && $(this).find('.rptcontent').length > 0) {
                    //$(this).find('.rptcontent').width($(this).outerWidth() - 10);
                    $(this).find('table.rptcontent').width($(this).outerWidth() - 10);
                    $(this).find('.rptcontent').height($(this).outerHeight() - 10);
                } 
                else {
                    $(this).children(':first').width($(this).outerWidth() - 10);
                    $(this).children(':first').height($(this).outerHeight() - 10);
                }
            });
        }

        function FadeOut(NoMSeconds) {
            $('#dvSucess').show();
            setTimeout(function () {
                $("#dvSucess").fadeOut("slow", function () {
                    $("#dvSucess").hide();
                });

            }, NoMSeconds);
        }

        function NewReport() {
            window.location.href = "../ReportDashboard/frmAddDashboardReport.aspx?DashboardTemplateID=" + parseInt($("[id$=hdnDashboardTemplateID]").val() || 0);
            return false;
        }

        function Redirect(a) {
            document.location.href = '../reports/frmCustomReportRun.aspx?ReptID=' + a + '&frm=Dashboard';
        }
        function OpenHelp() {
            window.open('../Help/Reports.html', '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return false
        }

        function ManageDashboardTemplates(isUpdate) {
            window.open('../ReportDashboard/frmManageDashboardTemplate.aspx?IsUpdate=' + isUpdate + '&DashboardTemplateID=' + parseInt($("[id$=hdnDashboardTemplateID]").val() || 0), '', 'toolbar=no,titlebar=no,top=200,left=200,width=550,height=350,scrollbars=yes,resizable=yes')
            return true;
        }

        function OpenActionItem(a) {
            document.location.href = "../admin/ActionItemDetailsOld.aspx?CommId=" + a
            return false;
        }

        function OpenCalItem(a) {
            window.open("../OutlookCalendar/EditCalendar.aspx?id=" + a + "&rs=0" + "&PId=" + a + "&Mode=1", '','toolbar=no,titlebar=no,top=100,left=100,width=600,height=400,scrollbars=yes,resizable=yes')
            return false;
        }

        function OpenSearchResult(a, b) {
            if (b == 15) {
                document.location.href = '../admin/frmItemSearchRes.aspx?SearchID=' + a;
            }
            if (b == 1) {
                document.location.href = '../admin/frmAdvancedSearchRes.aspx?SearchID=' + a;
            }
            if (b == 29) {
                document.location.href = '../admin/frmAdvSerInvItemsRes.aspx?SearchID=' + a;
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12">
            <div class="pull-left">
                <div class="form-inline">
                    <label>Dashboard Template:</label>
                    <asp:Label ID="lblTemplateName" runat="server" Text="" Visible="false"></asp:Label>
                    <asp:DropDownList ID="ddlDashboardTemplate" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlDashboardTemplate_SelectedIndexChanged"></asp:DropDownList>
                </div>
            </div>
            <div class="pull-right">
                <asp:Button ID="btnUpdateTemplate" runat="server" Text="Update Dashboard Template" CssClass="btn btn-primary" OnClientClick="return ManageDashboardTemplates(1)" />
                <asp:Button ID="btnCreateTemplate" runat="server" Text="Create Dashboard Template" CssClass="btn btn-primary" OnClientClick="return ManageDashboardTemplates(0)" />
                <asp:Button ID="btnNewReport" runat="server" Text="Add New Report" CssClass="btn btn-primary" UseSubmitBehavior="true" OnClientClick="return NewReport()" />
                <asp:Button ID="btnRefresh" runat="server" Text="Refresh Dashboard" CssClass="btn btn-primary" OnClick="btnRefresh_Click" />
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
    <div class="alert alert-success" id="dvSucess" style="display: none">
        <h4><i class="icon fa fa-check"></i>Alert!</h4>
        <span></span>
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Dashboard&nbsp;<a href="#" onclick="return OpenHelpPopUp('reportdashboard/frmnewdashboard.aspx')"><label class="badge bg-yellow">?</label></a>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <asp:HiddenField ID="hdnDashboardTemplateID" runat="server" />
    <div id="divLayout" class="table-responsive">
        <div class="layouts_grid gridster" id="layouts_grid">
            <ul>
                <asp:PlaceHolder ID="phColumn1" runat="server"></asp:PlaceHolder>
            </ul>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            changeContentHeight(true);
        });
    </script>
</asp:Content>
