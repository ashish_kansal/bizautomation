﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmPrebuildThreeInOne.ascx.vb" Inherits=".frmPrebuildThreeInOne" %>
<div class="box box-primary">
    <div class="box-header with-border handle">
        <h3 class="box-title">
            <asp:Label Text="" ID="lblHeader" runat="server" />
        </h3>
        <div id="tdEdit" runat="server" class="box-tools pull-right">
            <asp:HyperLink ID="hplEdit" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/pencil-24.gif" height="16" /></asp:HyperLink>
            <asp:HyperLink ID="hplDelete" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/cancel.png" /></asp:HyperLink>
            <asp:HiddenField runat="server" ID="hdnFilterBy" />
        </div>
    </div>
    <div class="box-body rptcontent">
        <table id="divReportContent" runat="server" style="width:100%">
            <tr>
                <td runat="server" id="rpt4" style="vertical-align:top;padding-right: 5px;">
                    <%--<asp:Image ID="imgReportImage" runat="server" CssClass="img-responsive" />--%>
                    <table style="width:100%">
                        <tr>
                            <td style="text-align:center">
                                <asp:Label ID="lblHeader4" runat="server" Text="" Style="white-space: nowrap" Font-Bold="true"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Repeater ID="rptReport4" runat="server">
                                    <HeaderTemplate>
                                        <table style="width:100%;background-color: #f5f5f5;border: 1px solid #ddd;">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="width:100%; padding:5px;">
                                                <asp:Label ID="lblTitle" runat="server" Style="white-space: nowrap"></asp:Label>
                                                <asp:HyperLink ID="hplTitle" runat="server" Target="_blank"></asp:HyperLink>
                                            </td>
                                            <td style="white-space:nowrap; padding:5px; text-align:right">
                                                <asp:Label ID="lblValue" runat="server"></asp:Label>
                                                <asp:HyperLink ID="hplValue" runat="server" Target="_blank"></asp:HyperLink>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </td>
                <td runat="server" id="rpt1" style="vertical-align:top;padding-right: 5px;">
                    <table style="width:100%">
                        <tr>
                            <td style="text-align:center">
                                <asp:Label ID="lblHeader1" runat="server" Text="" Style="white-space: nowrap" Font-Bold="true"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Repeater ID="rptReport1" runat="server">
                                    <HeaderTemplate>
                                        <table style="width:100%;background-color: #f5f5f5;border: 1px solid #ddd;">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="width:100%; padding:5px;">
                                                <asp:Label ID="lblTitle" runat="server" Style="white-space: nowrap">
                                                </asp:Label>
                                                <asp:HyperLink ID="hplTitle" runat="server" Target="_blank"></asp:HyperLink>
                                                 
                                            </td>
                                            <td style="white-space:nowrap; padding:5px; text-align:right">
                                                <asp:Label ID="lblValue" runat="server"></asp:Label>
                                                <asp:HyperLink ID="hplValue" runat="server" Target="_blank"></asp:HyperLink>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </td>
                <td runat="server" id="rpt2" style="vertical-align:top;padding-right: 5px;">
                    <table style="width:100%">
                        <tr>
                            <td style="text-align:center">
                                <asp:Label ID="lblHeader2" runat="server" Text="" Style="white-space: nowrap" Font-Bold="true"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Repeater ID="rptReport2" runat="server">
                                   <HeaderTemplate>
                                        <table style="width:100%;background-color: #f5f5f5;border: 1px solid #ddd;">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="width:100%; padding:5px;">
                                                <asp:Label ID="lblTitle" runat="server" Style="white-space: nowrap">
                                                </asp:Label>
                                                <asp:HyperLink ID="hplTitle" runat="server" Target="_blank"></asp:HyperLink>
                                            </td>
                                            <td style="white-space:nowrap; padding:5px; text-align:right">
                                                <asp:Label ID="lblValue" runat="server"></asp:Label>
                                                <asp:HyperLink ID="hplValue" runat="server" Target="_blank"></asp:HyperLink>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </td>
                <td runat="server" id="rpt3" style="vertical-align:top">
                    <table style="width:100%">
                        <tr>
                            <td style="text-align:center">
                                <asp:Label ID="lblHeader3" runat="server" Text="" Style="white-space: nowrap" Font-Bold="true"></asp:Label></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Repeater ID="rptReport3" runat="server">
                                    <HeaderTemplate>
                                        <table style="width:100%;background-color: #f5f5f5;border: 1px solid #ddd;">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td style="width:100%; padding:5px;">
                                                <asp:Label ID="lblTitle" runat="server" Style="white-space: nowrap"></asp:Label>
                                                <asp:HyperLink ID="hplTitle" runat="server" Target="_blank"></asp:HyperLink>
                                            </td>
                                            <td style="white-space:nowrap; padding:5px; text-align:right">
                                                <asp:Label ID="lblValue" runat="server"></asp:Label>
                                                <asp:HyperLink ID="hplValue" runat="server" Target="_blank"></asp:HyperLink>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:Label ID="lblError" runat="server" Text="Error ocurred while rendering this report" ForeColor="Red" Visible="false"></asp:Label>
    </div>
    <div class="box-footer footer">
        <asp:Label ID="lblFooter" runat="server" CssClass="text_bold"></asp:Label>
    </div>
</div>
