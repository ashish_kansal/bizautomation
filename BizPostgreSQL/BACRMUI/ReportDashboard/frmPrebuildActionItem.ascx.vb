﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports

Public Class frmPrebuildActionItem
    Inherits BACRMUserControl

#Region "Public Properties"

    Public Property DashBoardID As Long
    Public Property EditRept As Boolean
    Public Property Width As Integer
    Public Property objReportManage As CustomReportsManage
    Public Property DefaultReportID As Integer

#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#End Region

#Region "Public Methods"

    Public Function CreateReport() As Boolean
        Try
            hplExportToExcel.NavigateUrl = "~/ReportDashboard/frmManageDashBoard.aspx?Move=EX&DID=" & _DashBoardID

            hplEdit.NavigateUrl = "../ReportDashboard/frmAddDashboardReport.aspx?DID=" & _DashBoardID
            hplDelete.NavigateUrl = "~/ReportDashboard/frmManageDashBoard.aspx?Move=X&DID=" & _DashBoardID

            _objReportManage.DomainID = Session("DomainID")
            _objReportManage.DashBoardID = _DashBoardID
            Dim dtReportData As DataTable = objReportManage.GetReportDashboardDTL

            If Not dtReportData Is Nothing AndAlso dtReportData.Rows.Count > 0 Then
                lblHeader.Text = CCommon.ToString(dtReportData.Rows(0).Item("vcHeaderText"))
                lblFooter.Text = CCommon.ToString(dtReportData.Rows(0).Item("vcFooterText"))
            End If

            If DefaultReportID = CustomReportsManage.PrebuildReportEnum.ActionItemsAndMeetingsCompanyWide Then
                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = 0
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.ActionItemsAndMeetingsCompanyWide
                Dim ds As DataSet = objReportManage.GetPrebuildReportData()

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    rptReport.DataSource = ds.Tables(0)
                    rptReport.DataBind()
                End If
            End If

            Return True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            divReportContent.Visible = False
            lblError.Visible = True
        End Try
    End Function

#End Region

End Class