﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/common/GridMasterRegular.Master" CodeBehind="frmAddDashboardReport.aspx.vb" Inherits=".frmAddDashboardReport" %>
<%@ Register TagPrefix="BizCalendar" TagName="Calendar" Src="../include/calandar.ascx" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <title>Dashboard Report</title>
    <script type="text/javascript">
        function Save() {
            
            if (document.getElementById("ddlCustomReport") && document.getElementById("ddlCustomReport").value == "0~0") {
                alert("Select Custom Report");
                document.getElementById('ddlCustomReport').focus();
                return false;
            }

            return true;
        }

        function MinMaxCompare() {
           
            var txtMinAmountRange = document.getElementById("txtMinAmountRange").value;
            var txtMaxAmountRange = document.getElementById("txtMaxAmountRange").value;

            if (parseInt(txtMinAmountRange, 10) > parseInt(txtMaxAmountRange, 10)) {
                alert("Min value should be less than or equals to Max Value.");
                $("txtMaxAmountRange").val('0');
                return false;
            }
            return true;
        }
        

       
    </script>
    <style>
        #rundisplay li {
    flex: 2;
}
div#rundisplay {
    display: table;
    width: 100%;
}

#rundisplay ul {
    padding: 0;
    -webkit-column-count: 2;
    -webkit-column-gap: 30px;
    -moz-column-count: 2;
    -moz-column-gap: 30px;
    column-count: 2;
    column-gap: 100px;
    width: 100%;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FiltersAndViews" runat="server">
    <div class="row padbottom10">
        <div class="col-xs-12 text-right">
            <asp:LinkButton ID="btnSaveClose" runat="server" CssClass="btn btn-primary" OnClientClick="javascript:return Save();"><i class="fa fa-floppy-o"></i>&nbsp;&nbsp;Save & Close</asp:LinkButton>
            <asp:LinkButton ID="btnClose" runat="server" CssClass="btn btn-primary"><i class="fa fa-times"></i>&nbsp;&nbsp;Close</asp:LinkButton>
            <asp:HiddenField ID="hdnDashboardTemplate" runat="server" />
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="FiltersAndViews1" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="GridTitle" runat="server">
    Dashboard Report
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="BizPager" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="GridSettingPopup" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="GridBizSorting" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="GridPlaceHolder" runat="server">
    <asp:UpdateProgress ID="UpdateProgress" runat="server" ClientIDMode="Static">
        <ProgressTemplate>
            <div class="overlay">
                <div class="overlayContent" style="color: #000; text-align: center; width: 250px; padding: 20px">
                    <i class="fa fa-2x fa-refresh fa-spin"></i>
                    <h3>Processing Request</h3>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label>Custom Report</label>
                <div>
                    <telerik:RadComboBox AutoPostBack="true" ID="ddlCustomReport" runat="server" Width="100%">
                    </telerik:RadComboBox>
                </div>
                <asp:HiddenField ID="hfReportType" runat="server" Value="0" />
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6">
            <div class="form-group col-md-4">
                <label>Component Type</label>
                <div class="form-inline">
                    <asp:RadioButton ID="radTable" GroupName="rad" runat="server" Text="Table" Checked="true" />
                    <asp:RadioButton ID="radChart" GroupName="rad" runat="server" Text="Chart" Visible="false" />
                </div>
            </div>
              <div ID="divPeriodtoMeasure" class=" col-md-8" runat="server" visible="false">
                <label>Period to Measure</label> <%--Period to Measure--%>
                <div>
                    <ul style="list-style-type:none;">
                        <li><asp:RadioButton ID="radYeartoDate" GroupName="period" runat="server" Checked />Year To Date</li>
                        <li><asp:RadioButton ID="radMonthtoDate" GroupName="period"  runat="server" />Month To Date</li>
                        <li><asp:RadioButton ID="radWeektoDate" GroupName="period" runat="server" />Week To Date</li>
                    </ul>
                </div>
            </div>
            <div ID="divPendingReport" class="col-md-8" runat="server" visible="false">
                <label>Run Report</label> <%--Period to Measure--%>
                <div>
                    <ul  style="list-style-type:none;">
                        <li><asp:CheckBox  ID="chkpendingtobill"  runat="server" Checked/>Received But Not Billed</li>
                        <li><asp:CheckBox  ID="chkpendingtoreceive" runat="server"/>Billed But Not Receieved</li>
                        <li><asp:CheckBox  ID="chkpendingtosale" runat="server"/>Sold But Not Shipped</li>
                    </ul>
                </div>
            </div>
        </div>
        
      
    </div>
    <div class="row" id="trChartType" runat="server" visible="false">
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label>Chart Type</label>
                <asp:DropDownList ID="ddlChartType" CssClass="form-control" runat="server">
                    <asp:ListItem Value="1">Column Chart</asp:ListItem>
                    <asp:ListItem Value="2">Bar Chart</asp:ListItem>
                    <asp:ListItem Value="5">Funnel Chart</asp:ListItem>
                    <asp:ListItem Value="8">Line Chart</asp:ListItem>
                    <asp:ListItem Value="12">Pie Chart</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
        </div>
    </div>
    <div class="row" id="trXAxis" runat="server" visible="false">
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label>X-Axis</label>
                <asp:DropDownList ID="ddlXAxis" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
        </div>
    </div>
    <div class="row" id="trYAxis" runat="server" visible="false">
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label>Y-Axis</label>
                <asp:DropDownList ID="ddlYAxis" runat="server" CssClass="form-control">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label>Header</label>
                <asp:TextBox ID="txtHeader" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form-group">
                <label>Footer</label>
                <asp:TextBox ID="txtFooter" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
        </div>
    </div>
    <div class="row">
     
       
        <div class="col-xs-12 col-sm-6">
            <div class="form-group" id="divTimeLine" runat="server" visible="false">
                <label>Timeline</label> 
                <div class ="pull-right"  id="divCustomTimeline"  runat="server" visible="false">
                    Custom Timeline : <asp:CheckBox id="chkCustomTimeline" AutoPostBack="true" runat ="server" OnCheckedChanged="ddlCustomTimeline_SelectedIndexChanged"  Checked ="false"/> 
                     
                                   <telerik:RadDatePicker runat="server" ID="CtmFromDate" DateInput-DateFormat="MM/dd/yyyy"></telerik:RadDatePicker>
                                    <telerik:RadDatePicker runat="server" ID="CtmToDate" DateInput-DateFormat="MM/dd/yyyy"></telerik:RadDatePicker> 
                 </div>
                <asp:DropDownList ID="ddlTimeline" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlTimeline_SelectedIndexChanged"></asp:DropDownList>
            </div>
        </div>
         <div class="col-xs-12 col-sm-6">
            <div class="form-group" id="divGroupBy" runat="server" visible="false">
                <label>Group By</label>
                <asp:DropDownList ID="ddlGroupBy" runat="server" CssClass="form-control">
                    <asp:ListItem Text="Month" Value="Month"></asp:ListItem>
                    <asp:ListItem Text="Quater" Value="Quater"></asp:ListItem>
                    <asp:ListItem Text="Year" Value="Year"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        
        <div class="col-xs-12 col-sm-6">
            <div class="form-group" id="divRunDisplay" runat="server" visible="false">
                <label>Run & Display</label>
                <div class="col-12 col-md-12" id="rundisplay" runat="server">
                <ul style="margin-bottom: 0px;list-style-type:none ">
                    
                    <li> <asp:CheckBox ID="chkDealWon"  runat="server"  Checked/>&nbsp;&nbsp;<label>Deal Won %</label> </li>
                    <li> <asp:CheckBox ID="chkGrossRevenue" runat="server"/>&nbsp;&nbsp;<label>Gross Revenue</label> </li>
                      <li> <asp:CheckBox ID="chkRevenue"  runat="server"/>&nbsp;&nbsp;<label>Revenue from Gross Profit</label> </li>
                    <li> <asp:CheckBox ID="chkGrossProfit"  runat="server"/>&nbsp;&nbsp;<label>Gross Profit Margin Avg.</label> </li>
                </ul>
              
                
                </div>
             </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="form-group" id="divFromDate" runat="server" visible="false">
                <label>From Date<span style="color: red"> *</span></label>
                <div>
                    <telerik:RadDatePicker runat="server" ID="rdpFromDate" DateInput-DateFormat="MM/dd/yyyy"></telerik:RadDatePicker>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form-group" id="divToDate" runat="server" visible="false">
                <label>To Date<span style="color: red"> *</span></label>
                <div>
                    <telerik:RadDatePicker runat="server" ID="rdpToDate" DateInput-DateFormat="MM/dd/yyyy"></telerik:RadDatePicker>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="form-group" id="divTerritory" runat="server" visible="false">
                <label style="margin-bottom: 6px;">Filter by Territory</label>
                <div>
                    <telerik:RadComboBox ID="rcbTerritory" runat="server" Width="100%" CheckBoxes="true"></telerik:RadComboBox>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form-group" id="divFilterBy" runat="server" visible="false">
                <ul class="list-inline" style="margin-bottom: 0px;">
                    <li>
                        <asp:RadioButton ID="rbAssignedTo" runat="server" Text="Asignees" AutoPostBack="true" GroupName="FilterBy" OnCheckedChanged="rbAssignedTo_CheckedChanged" /></li>
                    <li>
                        <asp:RadioButton ID="rbRecordOwner" runat="server" Text="Record Owners" AutoPostBack="true" GroupName="FilterBy" OnCheckedChanged="rbRecordOwner_CheckedChanged" /></li>
                    <li>
                        <asp:RadioButton ID="rbTeams" runat="server" Text="Teams (Based on Asignee)" AutoPostBack="true" GroupName="FilterBy" OnCheckedChanged="rbTeams_CheckedChanged" /></li>

                    <li>
                        <asp:RadioButton ID="rbEmployees" runat="server" Text="Employees" AutoPostBack="true" GroupName="FilterBy" Visible="false" OnCheckedChanged="rbEmployees_CheckedChanged" /></li>

                </ul>
                <telerik:RadComboBox ID="rcbFilterBy" runat="server" Width="100%" CheckBoxes="true"></telerik:RadComboBox>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="form-group" id="divTop" runat="server" visible="false">
                <%-- <label>Top</label>--%>
                <asp:Label ID="lblTop" runat="server"></asp:Label>
                <asp:DropDownList ID="ddlTop" runat="server" CssClass="form-control"></asp:DropDownList>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form-group" id="divControlList" runat="server" visible="false">
                <asp:Label ID="lblControlList" runat="server" Text="Control List"></asp:Label>
                <asp:DropDownList ID="ddlControlList" runat="server" CssClass="form-control">
                    <%-- <asp:ListItem Text="By Profit Amount" Value="1"></asp:ListItem>
                    <asp:ListItem Text="By Profit Margin" Value="2"></asp:ListItem>
                    <asp:ListItem Text="By Revenue Sold" Value="3"></asp:ListItem>--%>
                </asp:DropDownList>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="form-group" id="divDealAmountBetween" runat="server" visible="false">
                <label style="margin-bottom: 6px;">Deal Amount Between</label>
                <div>
                    <telerik:RadComboBox ID="rcbDealAmountBetween" runat="server" Width="100%" CheckBoxes="true">
                        <Items>
                            <telerik:RadComboBoxItem Text="$1-5K" Value="1-5K" />
                            <telerik:RadComboBoxItem Text="$5-10K" Value="5-10K" />
                            <telerik:RadComboBoxItem Text="$10-20K" Value="10-20K" />
                            <telerik:RadComboBoxItem Text="$20-50K" Value="20-50K" />
                            <telerik:RadComboBoxItem Text=">$50K" Value=">50K" />
                        </Items>
                    </telerik:RadComboBox>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form-group" id="divOpportunityType" runat="server" visible="false">
                <label style="margin-bottom: 6px;">Opportunity Type</label>
                <div>
                    <asp:DropDownList ID="ddlOpportunityType" runat="server" CssClass="form-control">
                        <asp:ListItem Text="Sales Opportunities" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Purchase Opportunities" Value="2"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="form-group" id="divPeriodsToCompare" runat="server" visible="false">
                <label style="margin-bottom: 6px;">Periods to compare</label>
                <div>
                    <asp:DropDownList ID="ddlPeriodsToCompare" runat="server" CssClass="form-control">
                        <asp:ListItem Text="This-week vs Last-week" Value="ThisWeekVSLastWeek"></asp:ListItem>
                        <asp:ListItem Text="This-month vs Last-month" Value="ThisMonthVSLastMonth"></asp:ListItem>
                        <asp:ListItem Text="This-QTD vs Last-QTD" Value="ThisQTDVSLastQTD"></asp:ListItem>
                        <asp:ListItem Text="This-YTD vs Last-YTD" Value="ThisYTDVSLastYTD"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form-group" id="divConclusionReasons" runat="server" visible="false">
                <label style="margin-bottom: 6px;">Conclusion Reasons</label>
                <div>
                    <telerik:RadComboBox ID="rcbConclusionReasons" runat="server" Width="100%" CheckBoxes="true"></telerik:RadComboBox>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="form-group" id="divActionItemType" runat="server" visible="false">
                <label style="margin-bottom: 6px;">Action Item Type</label>
                <div>
                    <telerik:RadComboBox ID="rcbActionItemType" runat="server" Width="100%" CheckBoxes="true"></telerik:RadComboBox>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form-group" id="divTotalProgress" runat="server" visible="false">
                <label style="margin-bottom: 6px;">Total Progress</label>
                <div>
                    <asp:DropDownList ID="ddlTotalProgress" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="form-group" id="divQtyToDisplay" runat="server" visible="false">
                <label style="margin-bottom: 6px;">Qty to Display</label>
                <div>
                    <asp:DropDownList ID="ddlQtyToDisplay" runat="server" CssClass="form-control">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form-group" id="divAmountRange" runat="server" visible="false">
                <label style="margin-bottom: 6px;">Amount Range</label>
                <div>
                    <asp:TextBox ID="txtMinAmountRange" runat="server" Width="100%" CssClass="form-control" PlaceHolder="Min Value" TextMode="Number" onChange="return MinMaxCompare();"></asp:TextBox>
                    <asp:Label ID="lblAmountRange" runat="server">To</asp:Label>
                    <asp:TextBox ID="txtMaxAmountRange" runat="server" Width="100%" CssClass="form-control" PlaceHolder="Max Value" TextMode="Number"></asp:TextBox>
                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="form-group" id="divDueDate" runat="server" visible="false">
                <label style="margin-bottom: 6px;">Due Date</label>
                <div>
                    <asp:DropDownList ID="ddlDueDate" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlDueDate_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form-group" id="div2" runat="server" visible="false">
                <label style="margin-bottom: 6px;">Amount Range</label>
                <div>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="form-group" id="divDueFromDate" runat="server" visible="false">
                <label>From Date<span style="color: red"> *</span></label>
                <div>
                    <telerik:RadDatePicker runat="server" ID="rpdDueFromDate" DateInput-DateFormat="MM/dd/yyyy"></telerik:RadDatePicker>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6">
            <div class="form-group" id="divDueToDate" runat="server" visible="false">
                <label>To Date<span style="color: red"> *</span></label>
                <div>
                    <telerik:RadDatePicker runat="server" ID="rpdDueToDate" DateInput-DateFormat="MM/dd/yyyy"></telerik:RadDatePicker>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
