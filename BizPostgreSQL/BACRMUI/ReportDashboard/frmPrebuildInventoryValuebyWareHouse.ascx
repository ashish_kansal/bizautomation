﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmPrebuildInventoryValuebyWareHouse.ascx.vb" Inherits=".frmPrebuildInventoryValuebyWareHouse" %>
<script>
    function OpenItemList()
    {

        window.open('../Items/frmItemList.aspx?Page=Inventory Items&ItemGroup=0&Export=True')
        return false;
    }
</script>
<div class="box box-primary">
    <div class="box-header with-border handle">
        <h3 class="box-title">
            <asp:Label Text="" ID="lblHeader" runat="server" />
        </h3>
        <div id="tdEdit" runat="server" class="box-tools pull-right">
            <asp:HyperLink ID="hplEdit" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/pencil-24.gif" height="16" /></asp:HyperLink>
            <asp:HyperLink ID="hplDelete" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/cancel.png" /></asp:HyperLink>
        </div>
        <div>
            <h3 runat="server" class=" pull-right">
                <%--<asp:Label Text="" ID="lblHeadertotal" runat="server" />--%>
               <i style="font-size:18px" > TOTAL :</i>  <b> <asp:LinkButton runat="server" ID="lblHeadertotal"  Text="" ForeColor="#00cc00" Font-Size="25px" Height="0.5px"  OnClientClick=" return OpenItemList();" ></asp:LinkButton> </b>
            </h3>
        </div>
    </div>

    <div class="box-body rptcontent">
        <div class="row" id="divReportContent" runat="server" style="margin-right: 0px; margin-left: 0px; display: flex; align-items: center;">
            <div class="col-xs-12 col-md-12 col-sm-12 col-lg-12" style="padding-left: 5px; padding-right: 2px;">
                <asp:Repeater ID="rptReport" runat="server">
                    <HeaderTemplate>
                        <div class="row" style="margin-right: 0px; margin-left: 0px">
                            <div class="col-xs-4 col-md-4 col-sm-4  col-lg-6 text-left">
                                <asp:Label ID="lblHeader1" runat="server" Text="" Font-Bold="true"></asp:Label>
                            </div>
                            <div class="col-xs-4 col-md-4 col-sm-4  col-lg-6 text-right">
                                <asp:Label ID="lblHeader2" runat="server" Text="" Font-Bold="true"></asp:Label>
                            </div>
                        </div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <div class="row" style="margin-right: 0px; margin-left: 0px">
                            <div class="col-xs-4 col-md-4 col-sm-4  col-lg-6 text-left">
                               <%-- <asp:HyperLink ID="hplTitle" runat="server" Target="_blank"></asp:HyperLink>--%>
                                <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label>
                            </div>
                            <div class="col-xs-4 col-md-4 col-sm-4  col-lg-6 text-right">
                                <asp:HyperLink ID="hplTitle" runat="server" Target="_blank"></asp:HyperLink>
                                <asp:Label ID="lblValue1" runat="server" Text=""></asp:Label>
                            </div>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        <div class="row" style="margin-right: 0px; margin-left: 0px">
                            <div class="col-xs-4 col-md-4 col-sm-4 col-lg-6 text-left">
                                <asp:Label ID="lblLabel" runat="server" Font-Bold="true" />

                            </div>
                            <div class="col-xs-4 col-md-4 col-sm-4 col-lg-6 text-right">
                                <asp:Label ID="lblTotal" runat="server" Font-Bold="true" />
                            </div>
                        </div>
                    </FooterTemplate>

                </asp:Repeater>
            </div>
        </div>
        <asp:Label ID="lblError" runat="server" Text="Error ocurred while rendering this report" ForeColor="Red" Visible="false"></asp:Label>
    </div>
    <div class="box-footer footer">
        <asp:Label ID="lblFooter" runat="server" CssClass="text_bold"></asp:Label>
    </div>
</div>
