﻿Imports BACRM.BusinessLogic.Common
Imports BACRM.BusinessLogic.CustomReports

Public Class frmPrebuildThreeInOne
    Inherits BACRMUserControl

#Region "Public Properties"

    Public Property DashBoardID As Long
    Public Property EditRept As Boolean
    Public Property Width As Integer
    Public Property objReportManage As CustomReportsManage
    Public Property DefaultReportID As Integer
    Public Property IsRefresh As Boolean

#End Region

#Region "Page Events"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

#End Region

#Region "Public Methods"

    Public Function CreateReport() As Boolean
        Try
            hplEdit.NavigateUrl = "../ReportDashboard/frmAddDashboardReport.aspx?DID=" & _DashBoardID
            hplDelete.NavigateUrl = "~/ReportDashboard/frmManageDashBoard.aspx?Move=X&DID=" & _DashBoardID

            _objReportManage.DomainID = Session("DomainID")
            _objReportManage.DashBoardID = _DashBoardID
            Dim dtReportData As DataTable = objReportManage.GetReportDashboardDTL

            If Not dtReportData Is Nothing AndAlso dtReportData.Rows.Count > 0 Then
                lblHeader.Text = CCommon.ToString(dtReportData.Rows(0).Item("vcHeaderText"))
                lblFooter.Text = CCommon.ToString(dtReportData.Rows(0).Item("vcFooterText"))
            End If

            If DefaultReportID = CustomReportsManage.PrebuildReportEnum.EmployeeSalesPerformancePanel1 Then

                lblHeader1.Text = "Deals won %"
                lblHeader2.Text = "Revenue from Gross Profits"
                lblHeader3.Text = "Gross Profit Margin Avg."
                lblHeader4.Text = "Gross Revenue"

                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.EmployeeSalesPerformancePanel1
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    rptReport1.DataSource = ds.Tables(0)
                    rptReport1.DataBind()
                    rpt1.Visible = dtReportData.Rows(0).Item("bitDealWon")
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 1 AndAlso ds.Tables(1).Rows.Count > 0 Then
                    rptReport2.DataSource = ds.Tables(1)
                    rptReport2.DataBind()
                    rpt2.Visible = dtReportData.Rows(0).Item("bitRevenue")
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 2 AndAlso ds.Tables(2).Rows.Count > 0 Then
                    rptReport3.DataSource = ds.Tables(2)
                    rptReport3.DataBind()
                    rpt3.Visible = dtReportData.Rows(0).Item("bitGrossProfit")
                End If
                If Not ds Is Nothing AndAlso ds.Tables.Count > 3 AndAlso ds.Tables(3).Rows.Count > 0 Then
                    rptReport4.DataSource = ds.Tables(3)
                    rptReport4.DataBind()
                    rpt4.Visible = dtReportData.Rows(0).Item("bitGrossRevenue")
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.PartnerRMPLast12Months Then

                lblHeader1.Text = "Revenue by Partner"
                lblHeader2.Text = "Profit Margin by Partner"
                lblHeader3.Text = "Profit by Partner"

                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.PartnerRMPLast12Months
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    rptReport1.DataSource = ds.Tables(0)
                    rptReport1.DataBind()
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 1 AndAlso ds.Tables(1).Rows.Count > 0 Then
                    rptReport2.DataSource = ds.Tables(1)
                    rptReport2.DataBind()
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 2 AndAlso ds.Tables(2).Rows.Count > 0 Then
                    rptReport3.DataSource = ds.Tables(2)
                    rptReport3.DataBind()
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.EmployeeSalesPerformance1 Then

                lblHeader1.Text = "Lead to Prospects"
                lblHeader2.Text = "Lead to Accounts"
                lblHeader3.Text = "Prospects to Accounts"

                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.EmployeeSalesPerformance1
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    rptReport1.DataSource = ds.Tables(0)
                    rptReport1.DataBind()
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 1 AndAlso ds.Tables(1).Rows.Count > 0 Then
                    rptReport2.DataSource = ds.Tables(1)
                    rptReport2.DataBind()
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 2 AndAlso ds.Tables(2).Rows.Count > 0 Then
                    rptReport3.DataSource = ds.Tables(2)
                    rptReport3.DataBind()
                End If
            ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.EmployeeBenefittoCompany Then

                lblHeader1.Text = "Hours worked"
                lblHeader2.Text = "Gross Payroll"
                lblHeader3.Text = "Profit Income minus Payroll"

                objReportManage.DomainID = CCommon.ToLong(Session("DomainID"))
                objReportManage.UserCntID = CCommon.ToLong(Session("UserContactID"))
                objReportManage.ClientTimeZoneOffset = CCommon.ToInteger(Session("ClientMachineUTCTimeOffset"))
                objReportManage.DefaultReportID = CustomReportsManage.PrebuildReportEnum.EmployeeBenefittoCompany
                Dim ds As DataSet
                If Not IsRefresh AndAlso Not Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID) Is Nothing Then
                    ds = DirectCast(Cache("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID), DataSet)
                Else
                    ds = objReportManage.GetPrebuildReportData()
                    Cache.Add("Prebuild_" & CCommon.ToLong(Session("DomainID")) & "_" & _DashBoardID, ds, Nothing, New DateTime(DateTime.Now.AddDays(1).Year, DateTime.Now.AddDays(1).Month, DateTime.Now.AddDays(1).Day, 0, 0, 0), Nothing, Caching.CacheItemPriority.Default, Nothing)
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 0 AndAlso ds.Tables(0).Rows.Count > 0 Then
                    rptReport1.DataSource = ds.Tables(0)
                    rptReport1.DataBind()
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 1 AndAlso ds.Tables(1).Rows.Count > 0 Then
                    rptReport2.DataSource = ds.Tables(1)
                    rptReport2.DataBind()
                End If

                If Not ds Is Nothing AndAlso ds.Tables.Count > 2 AndAlso ds.Tables(2).Rows.Count > 0 Then
                    rptReport3.DataSource = ds.Tables(2)
                    rptReport3.DataBind()
                End If
            End If

            Return True
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            divReportContent.Visible = False
            lblError.Visible = True
        End Try
    End Function

#End Region

    Private Sub rptReport1_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptReport1.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim hplTitle As HyperLink = DirectCast(e.Item.FindControl("hplTitle"), HyperLink)
                Dim hplValue As HyperLink = DirectCast(e.Item.FindControl("hplValue"), HyperLink)
                Dim lblValue As Label = DirectCast(e.Item.FindControl("lblValue"), Label)
                Dim lblTitle As Label = DirectCast(e.Item.FindControl("lblTitle"), Label)

                Dim item As DataRowView = e.Item.DataItem
                If DefaultReportID = CustomReportsManage.PrebuildReportEnum.EmployeeSalesPerformancePanel1 Then
                    lblTitle.Text = CCommon.ToString(item("vcUserName"))
                    hplTitle.Text = CCommon.ToString(item("vcUserName"))
                    hplTitle.NavigateUrl = ResolveUrl("~/Opportunity/frmDealList.aspx?type=1&FilterBy=" & CCommon.ToString(item("vcFilterBy")) & "&SelectedID=" & CCommon.ToInteger(item("numUserDetailId"))) & "&From=" & (CCommon.ToString(item("StartDate")).Split(" "))(0) & "&To=" & (CCommon.ToString(item("EndDate")).Split(" "))(0) & "&Export=True"
                    lblTitle.Visible = False
                    lblValue.Text = String.Format("{0:#,##0.##}", CCommon.ToDouble(item("WonPercent"))) & "%"
                    hplValue.Text = String.Format("{0:#,##0.##}", CCommon.ToDouble(item("WonPercent"))) & "%"
                    hplValue.NavigateUrl = ResolveUrl("~/Opportunity/frmDealList.aspx?type=1&FilterBy=" & CCommon.ToString(item("vcFilterBy")) & "&SelectedID=" & CCommon.ToInteger(item("numUserDetailId"))) & "&From=" & (CCommon.ToString(item("StartDate")).Split(" "))(0) & "&To=" & (CCommon.ToString(item("EndDate")).Split(" "))(0) & "&Export=True"
                    lblValue.Visible = False
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.PartnerRMPLast12Months Then
                    lblTitle.Text = CCommon.ToString(item("vcCompanyName"))
                    lblValue.Text = String.Format("{0:$#,##0.##}", CCommon.ToDouble(item("monDealAmount")))
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.EmployeeSalesPerformance1 Then
                    lblTitle.Text = CCommon.ToString(item("vcUsername"))
                    lblValue.Text = String.Format("{0:#,##0.##}", CCommon.ToDouble(item("LeadsToProspect"))) & "%"
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.EmployeeBenefittoCompany Then
                    lblTitle.Text = CCommon.ToString(item("vcUsername"))
                    lblValue.Text = String.Format("{0:#,##0.##}", CCommon.ToDouble(item("TotalHrsWorked")))
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            divReportContent.Visible = False
            lblError.Visible = True
        End Try
    End Sub

    Private Sub rptReport2_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptReport2.ItemDataBound
        Try

            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim hplTitle As HyperLink = DirectCast(e.Item.FindControl("hplTitle"), HyperLink)
                Dim hplValue As HyperLink = DirectCast(e.Item.FindControl("hplValue"), HyperLink)
                Dim lblValue As Label = DirectCast(e.Item.FindControl("lblValue"), Label)
                Dim lblTitle As Label = DirectCast(e.Item.FindControl("lblTitle"), Label)

                Dim item As DataRowView = e.Item.DataItem
                If DefaultReportID = CustomReportsManage.PrebuildReportEnum.EmployeeSalesPerformancePanel1 Then
                    lblTitle.Text = CCommon.ToString(item("vcUserName"))
                    hplTitle.Text = CCommon.ToString(item("vcUserName"))
                    hplTitle.NavigateUrl = ResolveUrl("~/Opportunity/frmDealList.aspx?type=1&FilterBy=" & CCommon.ToString(item("vcFilterBy")) & "&SelectedID=" & CCommon.ToInteger(item("numUserDetailId"))) & "&From=" & (CCommon.ToString(item("StartDate")).Split(" "))(0) & "&To=" & (CCommon.ToString(item("EndDate")).Split(" "))(0) & "&Export=True"
                    lblTitle.Visible = False
                    lblValue.Text = String.Format("{0:$#,##0.##}", CCommon.ToDouble(item("Profit")))
                    hplValue.Text = String.Format("{0:$#,##0.##}", CCommon.ToDouble(item("Profit")))
                    hplValue.NavigateUrl = ResolveUrl("~/Opportunity/frmDealList.aspx?type=1&FilterBy=" & CCommon.ToString(item("vcFilterBy")) & "&SelectedID=" & CCommon.ToInteger(item("numUserDetailId"))) & "&From=" & (CCommon.ToString(item("StartDate")).Split(" "))(0) & "&To=" & (CCommon.ToString(item("EndDate")).Split(" "))(0) & "&Export=True"
                    lblValue.Visible = False

                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.PartnerRMPLast12Months Then
                    lblTitle.Text = CCommon.ToString(item("vcCompanyName"))
                    lblValue.Text = String.Format("{0:#,##0.##}", CCommon.ToDouble(item("BlendedProfit"))) & "%"
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.EmployeeSalesPerformance1 Then
                    lblTitle.Text = CCommon.ToString(item("vcUsername"))
                    lblValue.Text = String.Format("{0:#,##0.##}", CCommon.ToDouble(item("LeadsToAccount"))) & "%"
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.EmployeeBenefittoCompany Then
                    lblTitle.Text = CCommon.ToString(item("vcUsername"))
                    lblValue.Text = String.Format("{0:$#,##0.##}", CCommon.ToDouble(item("TotalPayroll")))
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            divReportContent.Visible = False
            lblError.Visible = True
        End Try
    End Sub

    Private Sub rptReport3_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptReport3.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim hplTitle As HyperLink = DirectCast(e.Item.FindControl("hplTitle"), HyperLink)
                Dim hplValue As HyperLink = DirectCast(e.Item.FindControl("hplValue"), HyperLink)
                Dim lblValue As Label = DirectCast(e.Item.FindControl("lblValue"), Label)
                Dim lblTitle As Label = DirectCast(e.Item.FindControl("lblTitle"), Label)

                Dim item As DataRowView = e.Item.DataItem
                If DefaultReportID = CustomReportsManage.PrebuildReportEnum.EmployeeSalesPerformancePanel1 Then
                    lblTitle.Text = CCommon.ToString(item("vcUserName"))
                    hplTitle.Text = CCommon.ToString(item("vcUserName"))
                    hplTitle.NavigateUrl = ResolveUrl("~/Opportunity/frmDealList.aspx?type=1&FilterBy=" & CCommon.ToString(item("vcFilterBy")) & "&SelectedID=" & CCommon.ToInteger(item("numUserDetailId"))) & "&From=" & (CCommon.ToString(item("StartDate")).Split(" "))(0) & "&To=" & (CCommon.ToString(item("EndDate")).Split(" "))(0) & "&Export=True" 'ResolveUrl("~/contact/frmContacts.aspx?CntId=" & CCommon.ToInteger(item("numUserDetailId")))
                    lblTitle.Visible = False
                    lblValue.Text = String.Format("{0:#,##0.##}", CCommon.ToDouble(item("AvgGrossProfitMargin"))) & "%"
                    hplValue.Text = String.Format("{0:#,##0.##}", CCommon.ToDouble(item("AvgGrossProfitMargin"))) & "%"
                    hplValue.NavigateUrl = ResolveUrl("~/Opportunity/frmDealList.aspx?type=1&FilterBy=" & CCommon.ToString(item("vcFilterBy")) & "&SelectedID=" & CCommon.ToInteger(item("numUserDetailId"))) & "&From=" & (CCommon.ToString(item("StartDate")).Split(" "))(0) & "&To=" & (CCommon.ToString(item("EndDate")).Split(" "))(0) & "&Export=True"
                    lblValue.Visible = False
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.PartnerRMPLast12Months Then
                    lblTitle.Text = CCommon.ToString(item("vcCompanyName"))
                    lblValue.Text = String.Format("{0:$#,##0.##}", CCommon.ToDouble(item("Profit")))
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.EmployeeSalesPerformance1 Then
                    lblTitle.Text = CCommon.ToString(item("vcUsername"))
                    lblValue.Text = String.Format("{0:#,##0.##}", CCommon.ToDouble(item("ProspectToAccount"))) & "%"
                ElseIf DefaultReportID = CustomReportsManage.PrebuildReportEnum.EmployeeBenefittoCompany Then
                    lblTitle.Text = CCommon.ToString(item("vcUsername"))
                    lblValue.Text = String.Format("{0:$#,##0.##}", CCommon.ToDouble(item("ProfitMinusPayroll")))
                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            divReportContent.Visible = False
            lblError.Visible = True
        End Try
    End Sub
    Private Sub rptReport4_ItemDataBound(sender As Object, e As RepeaterItemEventArgs) Handles rptReport4.ItemDataBound
        Try
            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim hplTitle As HyperLink = DirectCast(e.Item.FindControl("hplTitle"), HyperLink)
                Dim hplValue As HyperLink = DirectCast(e.Item.FindControl("hplValue"), HyperLink)
                Dim lblValue As Label = DirectCast(e.Item.FindControl("lblValue"), Label)
                Dim lblTitle As Label = DirectCast(e.Item.FindControl("lblTitle"), Label)

                Dim item As DataRowView = e.Item.DataItem
                If DefaultReportID = CustomReportsManage.PrebuildReportEnum.EmployeeSalesPerformancePanel1 Then
                    lblTitle.Text = CCommon.ToString(item("vcUserName"))
                    hplTitle.Text = CCommon.ToString(item("vcUserName"))
                    hplTitle.NavigateUrl = ResolveUrl("~/Opportunity/frmDealList.aspx?type=1&FilterBy=" & CCommon.ToString(item("vcFilterBy")) & "&GroupBy=0" & "&SelectedID=" & CCommon.ToInteger(item("numUserDetailId"))) & "&From=" & (CCommon.ToString(item("StartDate")).Split(" "))(0) & "&To=" & (CCommon.ToString(item("EndDate")).Split(" "))(0) & "&Export=True"  'ResolveUrl("~/contact/frmContacts.aspx?CntId=" & CCommon.ToInteger(item("numUserDetailId")))
                    lblTitle.Visible = False
                    lblValue.Text = String.Format("{0:$#,##0.##}", CCommon.ToDouble(item("InvoiceTotal")))
                    hplValue.Text = String.Format("{0:$#,##0.##}", CCommon.ToDouble(item("InvoiceTotal")))
                    hplValue.NavigateUrl = ResolveUrl("~/Opportunity/frmDealList.aspx?type=1&FilterBy=" & CCommon.ToString(item("vcFilterBy")) & "&GroupBy=0" & "&SelectedID=" & CCommon.ToInteger(item("numUserDetailId"))) & "&From=" & (CCommon.ToString(item("StartDate")).Split(" "))(0) & "&To=" & (CCommon.ToString(item("EndDate")).Split(" "))(0) & "&Export=True"
                    lblValue.Visible = False

                End If
            End If
        Catch ex As Exception
            ExceptionModule.ExceptionPublish(ex, Session("DomainID"), Session("UserContactID"), Request)
            divReportContent.Visible = False
            lblError.Visible = True
        End Try
    End Sub
End Class