<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmReptPyramid.ascx.vb"
    Inherits=".frmNewReptPyramid" %>
<script type="text/javascript" language="javascript">
    function Redirect(a) {
        document.location.href = '../reports/frmCustomReportRun.aspx?ReptID=' + a + '&frm=Dashboard';
    }
</script>
<div class="box box-primary">
    <div class="box-header with-border handle">
        <h3 class="box-title">
            <asp:Label Text="" ID="lblHeader" runat="server" />
        </h3>

        <div id="tdEdit" runat="server" class="box-tools pull-right">
            <asp:HyperLink ID="hplExportToExcel" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/excel.png" height="16" /></asp:HyperLink>
            <asp:HyperLink ID="hplEdit" CssClass="btn btn-box-tool" runat="server"><i class="fa fa-pencil"></i></asp:HyperLink>
            <asp:HyperLink ID="hplDelete" CssClass="btn btn-box-tool" runat="server"><i class="fa fa-trash"></i></asp:HyperLink>
        </div>
    </div>
    <div class="box-body rptcontent" runat="server" id="dvTable" clientidmode="Static">
        <asp:Panel ID="pnlChart" runat="server" CssClass="pnlChart">
            <igchart:UltraChart ID="UltraChart1" runat="server" ChartType="PyramidChart" Width="400px"
                Height="300px" Version="7.3" BackgroundImageFileName="" Border-Color="Black" Border-Thickness="0"
                EmptyChartText="Data Not Available. Please call UltraChart.Data.DataBind() after setting valid Data.DataSource">
                <PyramidChart>
                    <Labels>
                        <LabelStyle Font="Microsoft Sans Serif, 7.8pt" />
                    </Labels>
                </PyramidChart>
                <Data>
                    <EmptyStyle>
                        <PointPE FillOpacity="150" FillStopOpacity="150" StrokeOpacity="150"></PointPE>
                        <LineStyle DrawStyle="Dash"></LineStyle>
                        <PE FillOpacity="150" FillStopOpacity="150" StrokeOpacity="150"></PE>
                    </EmptyStyle>
                </Data>
                <ColorModel ModelStyle="CustomLinear" AlphaLevel="150">
                    <Skin>
                        <PEs>
                            <igchartprop:PaintElement FillGradientStyle="Vertical" FillOpacity="150" FillStopOpacity="150" ElementType="Gradient" Fill="108, 162, 36" FillStopColor="148, 244, 17" StrokeOpacity="150" StrokeWidth="0"></igchartprop:PaintElement>
                            <igchartprop:PaintElement FillGradientStyle="Vertical" FillOpacity="150" FillStopOpacity="150" ElementType="Gradient" Fill="7, 108, 176" FillStopColor="53, 200, 255" StrokeOpacity="150" StrokeWidth="0"></igchartprop:PaintElement>
                            <igchartprop:PaintElement FillGradientStyle="Vertical" FillOpacity="150" FillStopOpacity="150" ElementType="Gradient" Fill="230, 190, 2" FillStopColor="255, 255, 81" StrokeOpacity="150" StrokeWidth="0"></igchartprop:PaintElement>
                            <igchartprop:PaintElement FillGradientStyle="Vertical" FillOpacity="150" FillStopOpacity="150" ElementType="Gradient" Fill="215, 0, 5" FillStopColor="254, 117, 16" StrokeOpacity="150" StrokeWidth="0"></igchartprop:PaintElement>
                            <igchartprop:PaintElement FillGradientStyle="Vertical" FillOpacity="150" FillStopOpacity="150" ElementType="Gradient" Fill="252, 122, 10" FillStopColor="255, 108, 66" StrokeOpacity="150" StrokeWidth="0"></igchartprop:PaintElement>
                        </PEs>
                    </Skin>
                </ColorModel>
                <Effects>
                    <Effects>
                        <igchartprop:GradientEffect Style="ForwardDiagonal"></igchartprop:GradientEffect>
                    </Effects>
                </Effects>
                <Axis>
                    <X2 Visible="False" TickmarkInterval="10" LineThickness="1" TickmarkStyle="Smart">
                        <StripLines>
                            <PE FillOpacity="150" FillStopOpacity="150" StrokeOpacity="150"></PE>
                        </StripLines>
                        <Labels ItemFormatString="" VerticalAlign="Center" HorizontalAlign="Far" Font="Verdana, 7pt" FontColor="Gray" Orientation="VerticalLeftFacing">
                            <SeriesLabels VerticalAlign="Center" HorizontalAlign="Far" Font="Verdana, 7pt" FontColor="Gray" Orientation="VerticalLeftFacing" FormatString="">
                                <Layout Behavior="Auto"></Layout>
                            </SeriesLabels>
                            <Layout Behavior="Auto"></Layout>
                        </Labels>
                        <MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True" Thickness="1"></MajorGridLines>
                        <MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False" Thickness="1"></MinorGridLines>
                    </X2>
                    <Z Visible="True" LineThickness="1" TickmarkStyle="Smart" TickmarkInterval="0">
                        <StripLines>
                            <PE FillOpacity="150" FillStopOpacity="150" StrokeOpacity="150"></PE>
                        </StripLines>
                        <Labels ItemFormatString="" VerticalAlign="Center" HorizontalAlign="Far" Font="Verdana, 7pt" FontColor="DimGray" Orientation="Horizontal">
                            <SeriesLabels VerticalAlign="Center" HorizontalAlign="Far" Font="Verdana, 7pt" FontColor="DimGray" Orientation="Horizontal">
                                <Layout Behavior="Auto"></Layout>
                            </SeriesLabels>
                            <Layout Behavior="Auto"></Layout>
                        </Labels>
                        <MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True" Thickness="1"></MajorGridLines>
                        <MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False" Thickness="1"></MinorGridLines>
                    </Z>
                    <Z2 Visible="True" LineThickness="1" TickmarkStyle="Smart" TickmarkInterval="0">
                        <StripLines>
                            <PE FillOpacity="150" FillStopOpacity="150" StrokeOpacity="150"></PE>
                        </StripLines>
                        <Labels ItemFormatString="" VerticalAlign="Center" HorizontalAlign="Near" Font="Verdana, 7pt" FontColor="Gray" Orientation="Horizontal">
                            <SeriesLabels VerticalAlign="Center" HorizontalAlign="Near" Font="Verdana, 7pt" FontColor="Gray" Orientation="Horizontal">
                                <Layout Behavior="Auto"></Layout>
                            </SeriesLabels>
                            <Layout Behavior="Auto"></Layout>
                        </Labels>
                        <MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True" Thickness="1"></MajorGridLines>
                        <MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False" Thickness="1"></MinorGridLines>
                    </Z2>
                    <X LineEndCapStyle="Flat" Visible="True" TickmarkInterval="10" LineThickness="1" Extent="40" TickmarkStyle="Smart">
                        <StripLines>
                            <PE FillOpacity="150" FillStopOpacity="150" StrokeOpacity="150"></PE>
                        </StripLines>
                        <Labels ItemFormatString="&lt;ITEM_LABEL&gt;" VerticalAlign="Center" HorizontalAlign="Near" Font="Verdana, 7pt" FontColor="DimGray" Orientation="VerticalLeftFacing">
                            <SeriesLabels VerticalAlign="Center" HorizontalAlign="Near" Font="Verdana, 7pt" FontColor="DimGray" Orientation="VerticalLeftFacing" FormatString="">
                                <Layout Behavior="Auto"></Layout>
                            </SeriesLabels>
                            <Layout Behavior="Auto"></Layout>
                        </Labels>
                        <MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True" Thickness="1"></MajorGridLines>
                        <MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False" Thickness="1"></MinorGridLines>
                    </X>
                    <Y LineEndCapStyle="Flat" Visible="True" TickmarkInterval="10" LineThickness="1" Extent="30" TickmarkStyle="Smart">
                        <StripLines>
                            <PE FillOpacity="150" FillStopOpacity="150" StrokeOpacity="150"></PE>
                        </StripLines>
                        <Labels ItemFormatString="&lt;DATA_VALUE:00.##&gt;" VerticalAlign="Center" HorizontalAlign="Far" Font="Verdana, 7pt" FontColor="DimGray" Orientation="Horizontal">
                            <SeriesLabels VerticalAlign="Center" HorizontalAlign="Far" Font="Verdana, 7pt" FontColor="DimGray" Orientation="Horizontal" FormatString="">
                                <Layout Behavior="Auto"></Layout>
                            </SeriesLabels>
                            <Layout Behavior="Auto"></Layout>
                        </Labels>
                        <MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True" Thickness="1"></MajorGridLines>
                        <MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False" Thickness="1"></MinorGridLines>
                    </Y>
                    <Y2 Visible="False" TickmarkInterval="10" LineThickness="1" TickmarkStyle="Smart">
                        <StripLines>
                            <PE FillOpacity="150" FillStopOpacity="150" StrokeOpacity="150"></PE>
                        </StripLines>
                        <Labels ItemFormatString="" VerticalAlign="Center" HorizontalAlign="Near" Font="Verdana, 7pt" FontColor="Gray" Orientation="Horizontal">
                            <SeriesLabels VerticalAlign="Center" HorizontalAlign="Near" Font="Verdana, 7pt" FontColor="Gray" Orientation="Horizontal" FormatString="">
                                <Layout Behavior="Auto"></Layout>
                            </SeriesLabels>
                            <Layout Behavior="Auto"></Layout>
                        </Labels>
                        <MajorGridLines AlphaLevel="255" DrawStyle="Dot" Color="Gainsboro" Visible="True" Thickness="1"></MajorGridLines>
                        <MinorGridLines AlphaLevel="255" DrawStyle="Dot" Color="LightGray" Visible="False" Thickness="1"></MinorGridLines>
                    </Y2>
                    <PE ElementType="None" Fill="Cornsilk" />
                </Axis>
                <TitleBottom Visible="False">
                </TitleBottom>
                <Tooltips Font-Bold="False" Font-Italic="False" Font-Overline="False" Font-Strikeout="False"
                    Font-Underline="False" />
            </igchart:UltraChart>
        </asp:Panel>
    </div>
    <div class="box-footer footer">
        <asp:Label ID="lblFooter" runat="server" CssClass="text_bold"></asp:Label>
    </div>
</div>

