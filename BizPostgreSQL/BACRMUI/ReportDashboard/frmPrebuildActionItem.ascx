﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="frmPrebuildActionItem.ascx.vb" Inherits=".frmPrebuildActionItem" %>
<div class="box box-primary">
    <div class="box-header with-border handle">
        <h3 class="box-title">
            <asp:Label Text="" ID="lblHeader" runat="server" />
        </h3>
        <div id="tdEdit" runat="server" class="box-tools pull-right">
            <asp:HyperLink ID="hplExportToExcel" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/excel.png" height="16" /></asp:HyperLink>
            <asp:HyperLink ID="hplEdit" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/pencil-24.gif" height="16" /></asp:HyperLink>
            <asp:HyperLink ID="hplDelete" CssClass="btn btn-box-tool" runat="server"><img alt="" src="../images/cancel.png" /></asp:HyperLink>
        </div>
    </div>
    <div class="box-body rptcontent">
        <div id="divReportContent" runat="server" class="pnlChart">
            <asp:Repeater ID="rptReport" runat="server">
                <HeaderTemplate>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Customer</th>
                                <th>Activity</th>
                                <th>Date/Time</th>
                            </tr>
                        </thead>
                </HeaderTemplate>
                <ItemTemplate>
                   <tr>
                       <td rowspan="2" style="vertical-align:middle"><i class="fa fa-check-circle fa-6" style="font-size:30px; color:#00a65a" aria-hidden="true"></i></td>
                       <td>
                           <asp:Label ID="lblOrganization" runat="server" Text='<%# Eval("vcOrganizationName")%>'>'></asp:Label>
                       </td>
                       <td>
                           <asp:HyperLink ID="hplActionItem" runat="server" Text='<%# Eval("vcType")%>' onclick='<%# If(Eval("tintType") = 2, "return OpenCalItem(" & Eval("ID") & ");", "return OpenActionItem(" & Eval("ID") & ");")%>'></asp:HyperLink>
                       </td>
                       <td>
                           <asp:Label ID="lblDateTime" runat="server" Text='<%# Eval("dtFormatted")%>'>'></asp:Label>
                       </td>
                   </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Label ID="lblDescription" runat="server" Text='<%# Eval("vcComments")%>'>'></asp:Label>
                        </td>
                    </tr>
                </ItemTemplate>
                <FooterTemplate>
                    </table>
                </FooterTemplate>
            </asp:Repeater>
        </div>
        <asp:Label ID="lblError" runat="server" Text="Error ocurred while rendering this report" ForeColor="Red" Visible="false"></asp:Label>
    </div>
    <div class="box-footer footer">
        <asp:Label ID="lblFooter" runat="server" CssClass="text_bold"></asp:Label>
    </div>
</div>
